/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ikegen.h,v 1.20 2014/01/25 13:54:11 siva Exp $
*
* Description:This file contains the macros, datastructures and
*             common prototypes required for IKE.
*
*******************************************************************/

#ifndef _IKEGEN_H_
#define _IKEGEN_H_

#include "lr.h"
#include "fsbuddy.h"
#include "fsike.h"
#include "iketrace.h"
#include "iketdfs.h"
#include "ikenm.h"
#include "ikedhgroup.h"
#include "ip6util.h"
#include "shaarinc.h"
#include "sha2.h"
#include "desarinc.h"
#include "aesarinc.h"
#include "arMD5_inc.h"
#include "arHmac_api.h"
#include "ikecrypto.h"
#include "iketmr.h"
#include "fssocket.h"
#include "ikecert.h"
#include "cfa.h"
#include "vpn.h"
#include "sec.h"
#include "auth_db.h"
#include "ikeextern.h"
#include "fssyslog.h"

#ifdef __IKE_INIT_C__
BOOLEAN             gbCMServer = FALSE;
#else
extern BOOLEAN      gbCMServer;
#endif
                                               
/* General Macros */
#define IKE_TASK_PRIORITY      18   
#define IKE_INTERFACE_Q_DEPTH  25

#define  IKE_INITIATOR   1
#define  IKE_RESPONDER   2
#define  IKE_COOKIE_LEN  8
#define  IKE_IPSEC_DOI   1

#define  IKE_IP4_ADDR_LEN           4
#define  IKE_IP6_ADDR_LEN          16
#define  IKE_MAX_ATTR_SIZE         200 /* Increased from 64 to accomodate XAUTH and 
                                          Config Mode */
#define  IKE_MAX_DATA_SIZE       4096
#define  IKE_MIN_DATA_SIZE         64                                         
#define  IKE_BZERO(ptr,len) MEMSET(ptr,0,len)

#define  MAX_IKE_DSA_KEY_INFO_BLOCKS   10

#define  IKE_CHUNK_SIZE   512 

#define  IKE_TS_MIN_PORT  0 
#define  IKE_TS_MAX_PORT  65535 

#define  IKE_MAX_DER_ID_LEN  255

/* Default IP Address for Policy */
#define  IKE_DEFAULT_IP   0x00000000

/* FSAP related macros */
#define IKE_MEMCPY      MEMCPY
#define IKE_STRCMP      STRCMP
#define IKE_STRLEN      STRLEN
#define IKE_STRCPY      STRCPY
#define IKE_SPRINTF     SPRINTF
#define IKE_SSCANF      SSCANF
#define IKE_STRNCPY     STRNCPY
#define IKE_SNPRINTF    SNPRINTF
#define IKE_MEMCMP      MEMCMP
#define IKE_MEM_MALLOC  MEM_MALLOC
#define IKE_MEM_FREE    MEM_FREE
#define IKE_MEM_REALLOC MEM_REALLOC
#define IKE_RELEASE(pu1Ptr) if(pu1Ptr != NULL) {\
                      MEM_FREE(pu1Ptr);\
                      pu1Ptr=NULL; }

#define IKE_HTONS(x)  ((UINT2)OSIX_HTONS(x))
#define IKE_NTOHS(x)  ((UINT2)OSIX_NTOHS(x))
#define IKE_HTONL(x)  ((UINT4)OSIX_HTONL(x))
#define IKE_NTOHL(x)  ((UINT4)OSIX_NTOHL(x))

#define IKE_SET_ERROR(pSession,u2ErrorCode) pSession->u2NotifyType = u2ErrorCode

#define IKE_SUBNET_SEPARATOR '/'
#define IKE_RANGE_SEPARATOR '-'

#define IKE_ATOI(x)             ATOI(x)
#define IKE_ATOL(x)             ATOL(x)

#define  IKE_ITOA(A,I)  sprintf((char *)A, "%d", I) 

#ifndef __STRING
#define __STRING(x) "x"
#endif /* __STRING */

#define IKE_ASSERT(x)   if (!(x))                   \
{                                                   \
    IkeAssertFail(__STRING(x), __FILE__, __LINE__); \
}

#define IKE_MAX_BUF_LEN         64

#define SNPRINTF UtlSnprintf

#define HexDump(Data, Ptr, Len)                               \
{                                                             \
    UINT4 u4Count;                                            \
    UINT1 *pTmp;                                              \
    UINT1 au1Buf[IKE_MAX_BUF_LEN] = "";                       \
    pTmp = au1Buf;                                            \
    for (u4Count = 0; u4Count < (UINT4) Len; u4Count++)       \
    {                                                         \
        IKE_SNPRINTF ((unsigned char *)pTmp, (IKE_MAX_BUF_LEN -        \
                    IKE_STRLEN(au1Buf)), "0x%2x ",            \
               (unsigned char)Ptr[u4Count]);                                \
        pTmp += IKE_STRLEN(pTmp);                             \
    }                                                         \
    IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",       \
                    "%s %s\n", Data, au1Buf);                 \
}




#define IKE_MAX_BASIC_ATTR_LEN 65535

#define IKE_CREATE_ENTRY_IF_NOT_EXISTING 1

/* Macros  to get attribute bit */
#define IKE_BASIC_ATTR 0x8000
#define IKE_VAR_ATTR   0x7fff

/* macros to avoid hard coded const */
#define  IKE_ZERO   0
#define  IKE_ONE    1
#define  IKE_TWO    2
#define  IKE_THREE  3
#define  IKE_FOUR   4
#define  IKE_FIVE   5
#define IKE_SIX     6
#define IKE_SEVEN   7

#define  IKE_EIGHT   8
#define  IKE_NINE    9
/* Macors for session status */
#define  IKE_SESSION_ACTIVE    0
#define  IKE_SESSION_IN_ACTIVE 1

/* Protocol Number related macros */
#define  IKE_ISAKMP         1
#define  IKE_IPSEC_AH       2
#define  IKE_IPSEC_ESP      3

/* IPSec protocol values as defined by RFC:2401 */
#define  IKE_IP_PROTO_AH           51
#define  IKE_IP_PROTO_ESP          50

/* SPI size of IPSec protocols AH and ESP */ 
#define IKE_IPSEC_SPI_SIZE            4
#define IKE_IPSEC_NON_ESP_MARKER_SIZE 4

/* For compatibility with Future IPSec */
#define  IKE_IPSEC_PROTO_ANY     9000
#define  IKE_IPSEC_PORT_ANY      9000

#define IKE_ESP_SA_INDEX    0
#define IKE_AH_SA_INDEX     1

/* Offsets */
#define IKE_SA_LEN_OFFSET     2
#define IKE_ATTTR_LEN_OFFSET  2
#define IKE_ISAKMP_LEN_OFFSET 24

/* Phase 1 Protocol  and PortNumber */
#define IKE_UDP              17

/* IPSec Situation */
#define SIT_IDENTITY_ONLY       0x01
#define SIT_SECRECY             0x02
#define SIT_INTEGRITY           0x04

/* Transform Id */
#define IKE_TRANS_ID_KEY_OAKLEY   1

/* Next Pay load macros */
#define  IKE_NONE_PAYLOAD         0
#define  IKE_SA_PAYLOAD           1
#define  IKE_PROPOSAL_PAYLOAD     2
#define  IKE_TRANSFORM_PAYLOAD    3
#define  IKE_KEY_PAYLOAD          4
#define  IKE_ID_PAYLOAD           5
#define  IKE_CERT_PAYLOAD         6 
#define  IKE_CR_PAYLOAD           7 
#define  IKE_HASH_PAYLOAD         8
#define  IKE_SIGNATURE_PAYLOAD    9 
#define  IKE_NONCE_PAYLOAD       10
#define  IKE_NOTIFY_PAYLOAD      11
#define  IKE_DELETE_PAYLOAD      12
#define  IKE_VENDORID_PAYLOAD    13
#define  IKE_ATTRIBUTE_PAYLOAD   14
#define  IKE_MAX_PAYLOADS            18
#define  IKE_NAT_ORIG_ADDR_PAYLOADS  21
#define  IKE_NAT_DETECT_PAYLOADS     20
#define  IKE_MAX_TRANS_ATTRIBUTES     6

/* For IKEv2 Transform Attributes
 * (Key Length attribute is 14)  */
#define  IKE_MAX_ATTRIBUTES          15

/* Ike status macros */
#define  IKE_IDLE                   0 /* Invalid state */
#define  IKE_MM_NO_STATE            1
#define  IKE_MM_SA_SETUP            2
#define  IKE_MM_KEY_EXCH            3
#define  IKE_MM_AUTH_EXCH           4
#define  IKE_AG_NO_STATE            5
#define  IKE_AG_KEY_EXCH            6
#define  IKE_QM_IDLE                7
#define  IKE_QM_WAIT                8
#define  IKE_XAUTH_IDLE_INIT_REQ    9
#define  IKE_CM_IDLE_INIT_REQ       10
#define  IKE_IDLE_TRANS_PKT_RECV    11
#define  IKE_XAUTH_REPLY_SENT       12
#define  IKE_CM_REPLY_SENT          13
#define  IKE_CM_ACK_SENT            14
#define  IKE_XAUTH_REQ_SENT         16
#define  IKE_XAUTH_SET_SENT         17
#define  IKE_CM_SET_SENT            18
#define  IKE_CM_REQ_SENT            19
#define  IKE_XAUTH_ACK_SENT         20

#define IKE_QM_WAIT_ON_IPSEC        30

/* IKE Exchange modes */
#define IKE_MAIN_MODE         2 
#define IKE_AGGRESSIVE_MODE   4
#define IKE_QUICK_MODE        32
#define IKE_ISAKMP_INFO       5
#define IKE_TRANSACTION_MODE  6


/* These values are to be used within the type field of an Attribute 
 * ISAKMP payload */
#define IKE_ISAKMP_CFG_REQUEST     1
#define IKE_ISAKMP_CFG_REPLY       2
#define IKE_ISAKMP_CFG_SET         3
#define IKE_ISAKMP_CFG_ACK         4

/* Extended Authentication  Types */

#define IKE_XAUTH_GENERIC        0

/* Ike Sa Attribute macros */
#define IKE_ENCR_ALGO_ATTRIB     1
#define IKE_HASH_ALGO_ATTRIB     2
#define IKE_AUTH_METHOD_ATTRIB   3
#define IKE_GROUP_DESC_ATTRIB    4
#define IKE_GROUP_TYPE_ATTRIB    5
#define IKE_GROUP_PRIME_ATTRIB   6
#define IKE_SA_LIFE_TYPE_ATTRIB  11
#define IKE_SA_LIFE_DUR_ATTRIB   12
#define IKE_SA_KEY_LEN_ATTRIB    14

/* IKE Attribute Payload Macos */

#define IKE_INTERNAL_IP4_ADDRESS     1
#define IKE_INTERNAL_IP4_NETMASK     2
#define IKE_INTERNAL_IP4_DNS         3
#define IKE_INTERNAL_ADDRESS_EXPIRY  5
#define IKE_INTERNAL_IP4_DHCP        6
#define IKE_APPLICATION_VERSION      7
#define IKE_INTERNAL_IP6_ADDRESS     8
#define IKE_INTERNAL_IP6_NETMASK     9
#define IKE_INTERNAL_IP6_DNS        10
#define IKE_INTERNAL_IP6_DHCP       12
#define IKE_INTERNAL_IP4_SUBNET     13
#define IKE_SUPPORTED_ATTRIBUTES    14
#define IKE_XAUTH_TYPE              16520
#define IKE_XAUTH_USER_NAME         16521
#define IKE_XAUTH_USER_PASSWORD     16522
#define IKE_XAUTH_PASSCODE          16523
#define IKE_XAUTH_MESSAGE           16524
#define IKE_XAUTH_CHALLENGE         16525
#define IKE_XAUTH_DOMAIN            16526
#define IKE_XAUTH_STATUS            16527

#define  IKE_XAUTH_OK               1
#define  IKE_XAUTH_FAIL             0

/* Transaction Pkt Types */
#define IKE_XAUTH_REQ_PKT       0x01
#define IKE_XAUTH_REP_PKT       0x02
#define IKE_XAUTH_SET_PKT       0x03
#define IKE_XAUTH_ACK_PKT       0x04
#define IKE_CM_REQ_PKT          0x05
#define IKE_CM_REP_PKT          0x06
#define IKE_CM_SET_PKT          0x07
#define IKE_CM_ACK_PKT          0x08

#define IKE_XAUTH_INIT_PRESHARED_KEY 65001
#define IKE_XAUTH_RESP_PRESHARED_KEY 65002
#define IKE_XAUTH_INIT_DSS_SIGNATURE 65003
#define IKE_XAUTH_RESP_DSS_SIGNATURE 65004
#define IKE_XAUTH_INIT_RSA_SIGNATURE 65005
#define IKE_XAUTH_RESP_RSA_SIGNATURE 65006


/* IKE Group Descriptipns */
#define IKE_DH_GROUP_1               1      /* 768-bit MODP */
#define IKE_DH_GROUP_2               2      /* 1024-bit MODP group */
#define IKE_DH_GROUP_3               3      /* EC2N - Not Supported */
#define IKE_DH_GROUP_4               4      /* EC2N - Not Supported */
#define IKE_DH_GROUP_5               5      /* 1536-bit MODP */

#define IKE_LIFE_SECONDS                         1
#define IKE_LIFE_KILOBYTES                       2
#define IKE_BYTES_PER_KB                      1024

/* IPSec Sa Attribute macros */
#define IKE_IPSEC_SA_LIFE_TYPE_ATTRIB            1
#define IKE_IPSEC_SA_LIFE_DUR_ATTRIB             2
#define IKE_IPSEC_GROUP_DESC_ATTRIB              3
#define IKE_IPSEC_ENCAPSULATION_MODE_ATTRIB      4
#define IKE_IPSEC_AUTH_ALGO_ATTRIB               5
#define IKE_IPSEC_KEY_LEN_ATTRIB                 6

#define IKE_MD5_LENGTH       16
#define IKE_XCBC_LENGTH      16
#define IKE_SHA_LENGTH       20
#define IKE_SHA224_LENGTH    28
#define IKE_SHA256_LENGTH    32
#define IKE_SHA384_LENGTH    48
#define IKE_SHA512_LENGTH    64
#define IKE_MAX_DIGEST_SIZE                     520
#define  IKE2_CERT_SIG_LEN_64                   64
#define  IKE2_CERT_SIG_LEN_128                  128
#define  IKE2_CERT_SIG_LEN_256                  256
#define  IKE2_CERT_SIG_LEN_512                  512
 
/*MAX encryption length */
#define IKE_MAX_ENCR_KEY_LEN                    32

#define IPV4ADDR                    IKE_IPSEC_ID_IPV4_ADDR
#define IPV6ADDR                    IKE_IPSEC_ID_IPV6_ADDR

/* Values for KeyID Type */
#define IKE_KEY_IPV4                 IKE_IPSEC_ID_IPV4_ADDR
#define IKE_KEY_IPV6                 IKE_IPSEC_ID_IPV6_ADDR
#define IKE_KEY_EMAIL                IKE_IPSEC_ID_USER_FQDN
#define IKE_KEY_FQDN                 IKE_IPSEC_ID_FQDN
#define IKE_KEY_DN                   IKE_IPSEC_ID_DER_ASN1_DN
#define IKE_KEY_KEYID                IKE_IPSEC_ID_KEY_ID

/*  Constants for encoding type in CR payload */
#define IKE_PKCS_WRAP_X509_CERT          1
#define IKE_PGP_CERT                     2
#define IKE_DNS_SIGNED_KEY               3
#define IKE_X509_SIGNATURE               4
#define IKE_X509_CERT_KE                 5
#define IKE_KERBEROS_TOKENS              6
#define IKE_CERT_REVOCATION_LIST         7
#define IKE_AUTHORITY_REVOCATION_LIST    8
#define IKE_SPKI_CERT                    9
#define IKE_X509_CERT_ATTR               10 

/* Constant for mapping certificate all the peers */
#define IKE_MAP_DEFAULT  0

/* Constant for identifying dynamic peer certificate */
#define IKE_DYNAMIC_PEERCERT 1

#define IKE_PHASE1_INFO  1
#define IKE_PHASE2_INFO  2

#define IKE_PHASE1       1
#define IKE_PHASE2       2

#define IKE_PHASE2_REQUIRED 1

/* Payload sizes */
#define IKE_GEN_PAYLOAD_SIZE       4 
#define IKE_NONCE_PAYLOAD_SIZE     4 
#define IKE_HASH_PAYLOAD_SIZE      4 
#define IKE_PROP_PAYLOAD_SIZE      8 
#define IKE_SA_PAYLOAD_SIZE        12 
#define IKE_TRAN_PAYLOAD_SIZE      8 
#define IKE_KEY_PAYLOAD_SIZE       4 
#define IKE_ID_PAYLOAD_SIZE        8
#define IKE_CERT_PAYLOAD_SIZE      5
#define IKE_SIG_PAYLOAD_SIZE       4
#define IKE_VID_PAYLOAD_SIZE       4
#define IKE_NATD_PAYLOAD_SIZE                  4
#define IKE_ATTR_PAYLOAD_SIZE      8

#define IKE_MAX_PACKET_SIZE        4096
#define IKE_IPV4_LENGTH     4
#define IKE_IPV6_LENGTH     16

#define IKE_ENCRYPTION                           1
#define IKE_DECRYPTION                           2

#define IKE_RETRANSMIT_COUNT                     3
#define IKE_TIMER_COUNT                          3

/* Timer durations in seconds */
#define IKE_RETRANSMIT_INTERVAL                 10
#define IKE_SELECT_POLL_DURATION                 1
#define IKE_DELAYED_SESSION_DELETE_INTERVAL     10
#define IKE_MIN_TIMER_INTERVAL                   1

#define IKE_MSGID_LEN       4

#define IKE_MIN_SOFTLIFETIME_FACTOR             80
#define IKE_MAX_SOFTLIFETIME_FACTOR             90
#define IKE_DEFAULT_SA_LIFETIME                 60  /* In Seconds */
#define IKE_DEFAULT_SESSION_LIFETIME            180 /* In seconds */ 

#define  IKE_INVALID_EXCHANGE_TYPE     7  
#define  IKE_INVALID_SPI               11 
#define  IKE_RESPONDER_LIFETIME        24576
#define  IKE_REPLAY_STATUS             24577
#define  IKE_INITIAL_CONTACT           24578

#define IKE_PROTOCOL_ID_SIZE 1

#define IKE_NONE            0

#define IKE_ENCRYPT_MASK 0x01
#define IKE_COMMIT_MASK  0x02
#define IKE_AUTH_MASK    0x04

/* Constants for u1Flag in tIkeCertDB */
#define IKE_PRELOADED_PEER_CERT 0x00000001
#define IKE_DYNAMIC_PEER_CERT   0x00000002

#define IKE_SET_ENCR_MASK(ptr) \
         if(ptr != NULL) { \
             ((tIsakmpHdr *)(void *)ptr)->u1Flags |= IKE_ENCRYPT_MASK;\
         }
 

#define IKE_TEST_ENCR_MASK(ptr) \
             (((tIsakmpHdr *)(void *)ptr)->u1Flags & IKE_ENCRYPT_MASK)

#define IKE_TEST_AUTH_MASK(ptr) \
             (((tIsakmpHdr *)(void *)ptr)->u1Flags & IKE_AUTH_MASK)

/* FSAP related Macros */
#define IKE_BUF_RELEASE        CRU_BUF_Release_MsgBufChain

#define IKE_BUF_ALLOCATE_CHAIN   CRU_BUF_Allocate_MsgBufChain

#define IKE_BMC_GET_DATA(pBufChain,pValue,u4Len)  \
      CRU_BUF_Copy_FromBufChain (pBufChain, (UINT1 *)pValue,0,u4Len)

#define IKE_BMC_ASSIGN_DATA(pBufChain,pValue,u4Len) \
   CRU_BUF_Copy_OverBufChain(pBufChain,(UINT1 *)pValue,0,u4Len)

/* Common Data Structures */
typedef tCRU_BUF_CHAIN_HEADER tBufChainHeader;

#define IKE_GET_MODULE_DATA_PTR(pBuf) &(pBuf->ModuleData) 
#define IKE_SET_COMMAND(pBuf,u1Cmd) CRU_BUF_Set_SourceModuleId(pBuf,u1Cmd)

/* Constants for u4ActionType in tIkePhase1PostProcessInfo */  
#define IKE_POST_P1_NONE           0
#define IKE_POST_P1_NOTIFY         1
#define IKE_POST_P1_NEW_SA         2 
#define IKE_POST_P1_DELETE         6000
#define IKE_POST_RA_REKEY          3


#define  IP6_TASK_IP_INPUT_QUEUE    "IPQ1"
#define  IP6_TASK_NAME    "IP6"
#define  IP6_DGRAM_RECD_EVENT      (0x00000002)

#define IKE_WLAN_RA_NAME  "wlanxauth"
#define IKE_DEFAULT_RA_NAME  "xauth"
#define IKE_MAX_CMD_STR_LEN   256

/************* IKEV2 related MACROS *********************/
/* IKEv2 Exchange  Types */
#define IKE2_INIT_EXCH                 34
#define IKE2_AUTH_EXCH                    35
#define IKE2_CHILD_EXCH                  36
#define IKE2_INFO_EXCH                 37

/* IKEv2 Header Flag */
#define IKE2_FLAG_INITIATOR                     (1 << 3)
#define IKE2_FLAG_VERSION                       (1 << 4)
#define IKE2_FLAG_RESPONSE                      (1 << 5)

/* ---------- IKEv1 Version constants --------- */
#define IKE_ISAKMP_VERSION                      0x10
/* ---------- IKEv2 Version constants --------- */
#define   IKE2_MAJOR_VERSION                   0x00000020
#define   IKE2_MINOR_VERSION                   0x00000000

/*-----Macros releated to Child Exch Types ------- */
#define  IKE2_IPSEC_REKEY_IKESA                 1
#define  IKE2_IPSEC_REKEY_CHILD                 2
#define  IKE2_IPSEC_NEW_CHILD                   3

/* Authentication types */
#define IKE2_AUTH_RSA                1
#define IKE2_AUTH_PSK                 2
#define IKE2_AUTH_DSA                 3

#define IKE2_DSA_HASH_LEN                  48

/* Message ids for Ikev2 INIT and Auth Msgs */
#define   IKE2_INIT_MSG_ID                      0
#define   IKE2_AUTH_MSG_ID                      1
/* ------ Macros related to IKEv2 FSM states------*/
#define   IKE2_IDLE                             0
#define   IKE2_INIT_IDLE                        1
#define   IKE2_INIT_WAIT                        2
#define   IKE2_INIT_DONE                        3
#define   IKE2_AUTH_IDLE                        4
#define   IKE2_AUTH_WAIT                        5
#define   IKE2_AUTH_DONE                        6
#define   IKE2_CHILD_IDLE                       7
#define   IKE2_CHILD_WAIT                       8
#define   IKE2_CHILD_DONE                       9
#define   IKE2_INFO_IDLE                       10
#define   IKE2_INFO_WAIT                       11
#define   IKE2_WAIT_ON_IPSEC                   12  



/*Certificate related error messages while processing*/
#define  IKE_INVALID_CERT_ENCODING             19 
#define  IKE_INVALID_CERTIFICATE               20 
#define  IKE_CERT_TYPE_UNSUPPORTED             21 
#define  IKE_INVALID_CERT_AUTHORITY            22 
#define  IKE_INVALID_HASH_INFO                 23 
#define  IKE_AUTH_FAILED                       24 
#define  IKE_INVALID_SIGNATURE                 25 

/* Macros for hardcoded values*/
#define IKE_INVALID     -1
#define IKE_INDEX_0     0
#define IKE_INDEX_1     1
#define IKE_INDEX_2     2
#define IKE_INDEX_3     3
#define IKE_INDEX_127   127
#define IKE_SEVEN       7
#define IKE_EIGHT       8
#define IKE_MAX_TYPE    10
#define IKE_FOURTEEN    14
#define IKE_SIXTEEN     16
#define IKE_SEVENTEEN   17
#define IKE_MAX_HASH    16
#define IKE_TWELVE      12
#define IKE_THIRTEEN    13



#define IKE_TWENTYFOUR  24
#define IKE_MAX_COUNT   34
#define IKE_DIVISOR_100 100

#define IKE_INDEX_127    127
#define IKE_MAX_LEN      129


#define IKE_YEAR_50            50
#define IKE_YEAR_100           100
#define IKE_YEAR_1900          1900


#define RSA_KEY_512            512
#define RSA_KEY_768            768
#define RSA_KEY_1024           1024
#define DSA_KEY_512            512
#define DSA_KEY_768            768
#define DSA_KEY_1024           1024

#define IKE_OFFSET_3f       0x3f
#define IKE_MASK            0xffffffff

#define IKE_IPV6_PREFIX_TO_MASK_00 0x00

#define IKE_IPV6_PREFIX_TO_MASK_80 0x80
#define IKE_IPV6_PREFIX_TO_MASK_c0 0xc0
#define IKE_IPV6_PREFIX_TO_MASK_e0 0xe0
#define IKE_IPV6_PREFIX_TO_MASK_f0 0xf0
#define IKE_IPV6_PREFIX_TO_MASK_f8 0xf8
#define IKE_IPV6_PREFIX_TO_MASK_fc 0xfc
#define IKE_IPV6_PREFIX_TO_MASK_fe 0xfe
#define IKE_IPV6_PREFIX_TO_MASK_ff 0xff



#define    IKE_IPV4_PREFIX_TO_MASK_00000000       0x00000000
#define    IKE_IPV4_PREFIX_TO_MASK_80000000       0x80000000 
#define    IKE_IPV4_PREFIX_TO_MASK_c0000000       0xc0000000
#define    IKE_IPV4_PREFIX_TO_MASK_e0000000       0xe0000000
#define    IKE_IPV4_PREFIX_TO_MASK_f0000000       0xf0000000
#define    IKE_IPV4_PREFIX_TO_MASK_f8000000       0xf8000000
#define    IKE_IPV4_PREFIX_TO_MASK_fc000000       0xfc000000
#define    IKE_IPV4_PREFIX_TO_MASK_fe000000       0xfe000000
#define    IKE_IPV4_PREFIX_TO_MASK_ff000000       0xff000000
#define    IKE_IPV4_PREFIX_TO_MASK_ff800000       0xff800000
#define    IKE_IPV4_PREFIX_TO_MASK_ffc00000       0xffc00000
#define    IKE_IPV4_PREFIX_TO_MASK_ffe00000       0xffe00000
#define    IKE_IPV4_PREFIX_TO_MASK_fff00000       0xfff00000
#define    IKE_IPV4_PREFIX_TO_MASK_fff80000       0xfff80000
#define    IKE_IPV4_PREFIX_TO_MASK_fffc0000       0xfffc0000
#define    IKE_IPV4_PREFIX_TO_MASK_fffe0000       0xfffe0000
#define    IKE_IPV4_PREFIX_TO_MASK_ffff0000       0xffff0000
#define    IKE_IPV4_PREFIX_TO_MASK_ffff8000       0xffff8000
#define    IKE_IPV4_PREFIX_TO_MASK_ffffc000       0xffffc000
#define    IKE_IPV4_PREFIX_TO_MASK_ffffe000       0xffffe000
#define    IKE_IPV4_PREFIX_TO_MASK_fffff000       0xfffff000
#define    IKE_IPV4_PREFIX_TO_MASK_fffff800       0xfffff800
#define    IKE_IPV4_PREFIX_TO_MASK_fffffc00       0xfffffc00
#define    IKE_IPV4_PREFIX_TO_MASK_fffffe00       0xfffffe00
#define    IKE_IPV4_PREFIX_TO_MASK_ffffff00       0xffffff00 
#define    IKE_IPV4_PREFIX_TO_MASK_ffffff80       0xffffff80
#define    IKE_IPV4_PREFIX_TO_MASK_ffffffc0       0xffffffc0
#define    IKE_IPV4_PREFIX_TO_MASK_ffffffe0       0xffffffe0
#define    IKE_IPV4_PREFIX_TO_MASK_fffffff0       0xfffffff0
#define    IKE_IPV4_PREFIX_TO_MASK_fffffff8       0xfffffff8
#define    IKE_IPV4_PREFIX_TO_MASK_fffffffc       0xfffffffc
#define    IKE_IPV4_PREFIX_TO_MASK_fffffffe       0xfffffffe
#define    IKE_IPV4_PREFIX_TO_MASK_ffffffff       0xffffffff


/* DES Weak Keys Macros */
#define IKE_DES_KEY_0x01    0x01
#define IKE_DES_KEY_0x1F    0x1F
#define IKE_DES_KEY_0xE0    0xE0
#define IKE_DES_KEY_0x0E    0x0E
#define IKE_DES_KEY_0xFE    0xFE
#define IKE_DES_KEY_0xF1    0xF1

/* OAKLEY Group (1,2,5,14) Prime Values*/

#define IKE_OAKLEY_0x00     0x00
#define IKE_OAKLEY_0x02     0x02
#define IKE_OAKLEY_0x03     0x03
#define IKE_OAKLEY_0x04     0x04
#define IKE_OAKLEY_0x05     0x05
#define IKE_OAKLEY_0x06     0x06
#define IKE_OAKLEY_0x07     0x07
#define IKE_OAKLEY_0x08     0x08
#define IKE_OAKLEY_0x0A     0x0A
#define IKE_OAKLEY_0x0B     0x0B
#define IKE_OAKLEY_0x0C     0x0C
#define IKE_OAKLEY_0x0E     0x0E
#define IKE_OAKLEY_0x0F     0x0F
#define IKE_OAKLEY_0x10     0x10
#define IKE_OAKLEY_0x11     0x11
#define IKE_OAKLEY_0x13     0x13
#define IKE_OAKLEY_0x14     0x14
#define IKE_OAKLEY_0x15     0x15
#define IKE_OAKLEY_0x16     0x16
#define IKE_OAKLEY_0x17     0x17
#define IKE_OAKLEY_0x18     0x18
#define IKE_OAKLEY_0x19     0x19
#define IKE_OAKLEY_0x1B     0x1B
#define IKE_OAKLEY_0x1C     0x1C
#define IKE_OAKLEY_0x1F     0x1F
#define IKE_OAKLEY_0x20     0x20
#define IKE_OAKLEY_0x21     0x21
#define IKE_OAKLEY_0x22     0x22
#define IKE_OAKLEY_0x23     0x23
#define IKE_OAKLEY_0x24     0x24
#define IKE_OAKLEY_0x26     0x26
#define IKE_OAKLEY_0x27     0x27
#define IKE_OAKLEY_0x28     0x28
#define IKE_OAKLEY_0x29     0x29
#define IKE_OAKLEY_0x2B     0x2B
#define IKE_OAKLEY_0x2C     0x2C
#define IKE_OAKLEY_0x2E     0x2E
#define IKE_OAKLEY_0x30     0x30
#define IKE_OAKLEY_0x32     0x32
#define IKE_OAKLEY_0x34     0x34
#define IKE_OAKLEY_0x35     0x35
#define IKE_OAKLEY_0x36     0x36
#define IKE_OAKLEY_0x37     0x37
#define IKE_OAKLEY_0x38     0x38
#define IKE_OAKLEY_0x39     0x39
#define IKE_OAKLEY_0x3A     0x3A
#define IKE_OAKLEY_0x3B     0x3B
#define IKE_OAKLEY_0x3D     0x3D
#define IKE_OAKLEY_0x3F     0x3F
#define IKE_OAKLEY_0x42     0x42
#define IKE_OAKLEY_0x43     0x43
#define IKE_OAKLEY_0x45     0x45
#define IKE_OAKLEY_0x46     0x46
#define IKE_OAKLEY_0x48     0x48
#define IKE_OAKLEY_0x49     0x49
#define IKE_OAKLEY_0x4A     0x4A
#define IKE_OAKLEY_0x4B     0x4B
#define IKE_OAKLEY_0x4C     0x4C
#define IKE_OAKLEY_0x4E     0x4E
#define IKE_OAKLEY_0x4F     0x4F
#define IKE_OAKLEY_0x51     0x51
#define IKE_OAKLEY_0x52     0x52
#define IKE_OAKLEY_0x53     0x53
#define IKE_OAKLEY_0x55     0x55
#define IKE_OAKLEY_0x56     0x56
#define IKE_OAKLEY_0x58     0x58
#define IKE_OAKLEY_0x5A     0x5A
#define IKE_OAKLEY_0x5B     0x5B
#define IKE_OAKLEY_0x5C     0x5C
#define IKE_OAKLEY_0x5D     0x5D
#define IKE_OAKLEY_0x5E     0x5E
#define IKE_OAKLEY_0x5F     0x5F
#define IKE_OAKLEY_0x62     0x62
#define IKE_OAKLEY_0x63     0x63
#define IKE_OAKLEY_0x65     0x65
#define IKE_OAKLEY_0x66     0x66
#define IKE_OAKLEY_0x67     0x67
#define IKE_OAKLEY_0x68     0x68
#define IKE_OAKLEY_0x69     0x69
#define IKE_OAKLEY_0x6A     0x6A
#define IKE_OAKLEY_0x6B     0x6B
#define IKE_OAKLEY_0x6C     0x6C
#define IKE_OAKLEY_0x6D     0x6D
#define IKE_OAKLEY_0x6F     0x6F
#define IKE_OAKLEY_0x70     0x70
#define IKE_OAKLEY_0x72     0x72
#define IKE_OAKLEY_0x73     0x73
#define IKE_OAKLEY_0x74     0x74
#define IKE_OAKLEY_0x76     0x76
#define IKE_OAKLEY_0x77     0x77
#define IKE_OAKLEY_0x79     0x79
#define IKE_OAKLEY_0x7C     0x7C
#define IKE_OAKLEY_0x7E     0x7E
#define IKE_OAKLEY_0x80     0x80
#define IKE_OAKLEY_0x81     0x81
#define IKE_OAKLEY_0x83     0x83
#define IKE_OAKLEY_0x85     0x85
#define IKE_OAKLEY_0x86     0x86
#define IKE_OAKLEY_0x89     0x89
#define IKE_OAKLEY_0x8A     0x8A
#define IKE_OAKLEY_0x8B     0x8B
#define IKE_OAKLEY_0x8E     0x8E
#define IKE_OAKLEY_0x8F     0x8F
#define IKE_OAKLEY_0x90     0x90
#define IKE_OAKLEY_0x95     0x95
#define IKE_OAKLEY_0x96     0x96
#define IKE_OAKLEY_0x98     0x98
#define IKE_OAKLEY_0x9A     0x9A
#define IKE_OAKLEY_0x9B     0x9B
#define IKE_OAKLEY_0x9E     0x9E
#define IKE_OAKLEY_0x9F     0x9F
#define IKE_OAKLEY_0xA1     0xA1
#define IKE_OAKLEY_0xA2     0xA2
#define IKE_OAKLEY_0xA3     0xA3
#define IKE_OAKLEY_0xA5     0xA5
#define IKE_OAKLEY_0xA6     0xA6
#define IKE_OAKLEY_0xA8     0xA8
#define IKE_OAKLEY_0xAA     0xAA
#define IKE_OAKLEY_0xAC     0xAC
#define IKE_OAKLEY_0xAD     0xAD
#define IKE_OAKLEY_0xAE     0xAE
#define IKE_OAKLEY_0xB3     0xB3
#define IKE_OAKLEY_0xB5     0xB5
#define IKE_OAKLEY_0xB6     0xB6
#define IKE_OAKLEY_0xB7     0xB7
#define IKE_OAKLEY_0xB8     0xB8
#define IKE_OAKLEY_0xBB     0xBB
#define IKE_OAKLEY_0xBC     0xBC
#define IKE_OAKLEY_0xBE     0xBE
#define IKE_OAKLEY_0xBF     0xBF
#define IKE_OAKLEY_0xC2     0xC2
#define IKE_OAKLEY_0xC4     0xC4
#define IKE_OAKLEY_0xC5     0xC5
#define IKE_OAKLEY_0xC6     0xC6
#define IKE_OAKLEY_0xC9     0xC9
#define IKE_OAKLEY_0xCA     0xCA
#define IKE_OAKLEY_0xCB     0xCB
#define IKE_OAKLEY_0xCC     0xCC
#define IKE_OAKLEY_0xCD     0xCD
#define IKE_OAKLEY_0xCE     0xCE
#define IKE_OAKLEY_0xCF     0xCF
#define IKE_OAKLEY_0xD1     0xD1
#define IKE_OAKLEY_0xD2     0xD2
#define IKE_OAKLEY_0xD3     0xD3
#define IKE_OAKLEY_0xD5     0xD5
#define IKE_OAKLEY_0xDA     0xDA
#define IKE_OAKLEY_0xDC     0xDC
#define IKE_OAKLEY_0xDD     0xDD
#define IKE_OAKLEY_0xDE     0xDE
#define IKE_OAKLEY_0xE1     0xE1
#define IKE_OAKLEY_0xE3     0xE3
#define IKE_OAKLEY_0xE4     0xE4
#define IKE_OAKLEY_0xE5     0xE5
#define IKE_OAKLEY_0xE6     0xE6
#define IKE_OAKLEY_0xE9     0xE9
#define IKE_OAKLEY_0xEA     0xEA
#define IKE_OAKLEY_0xEC     0xEC
#define IKE_OAKLEY_0xED     0xED
#define IKE_OAKLEY_0xEE     0xEE
#define IKE_OAKLEY_0xEF     0xEF
#define IKE_OAKLEY_0xF0     0xF0
#define IKE_OAKLEY_0xF1     0xF1
#define IKE_OAKLEY_0xF2     0xF2
#define IKE_OAKLEY_0xF3     0xF3
#define IKE_OAKLEY_0xF4     0xF4
#define IKE_OAKLEY_0xF6     0xF6
#define IKE_OAKLEY_0xFA     0xFA
#define IKE_OAKLEY_0xFB     0xFB
#define IKE_OAKLEY_0xFD     0xFD
#define IKE_OAKLEY_0xFF     0xFF


/* --------- IKEv1 Macros-----------------------------*/



#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_09 0x09
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_00 0x00
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_26 0x26
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_89 0x89
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_df 0xdf
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_d6 0xd6
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_b7 0xb7
#define    IKEV1_XAUTH_VENDOR_ID_PAYLOAD_12 0x12



#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_12 0x12
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_f5 0xf5
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_f2 0xf2
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_8c 0x8c
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_45 0x45
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_71 0x71
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_68 0x68
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_a9 0xa9
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_70 0x70
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_2d 0x2d
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_9f 0x9f
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_e2 0xe2
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_74 0x74
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_cc 0xcc
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_02 0x02
#define    IKEV1_CISCO_VENDOR_ID_PAYLOAD_d4 0xd4



#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_4a 0x4a
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_13 0x13
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_1c 0x1c
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_81 0x81
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_07 0x07
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_03 0x03
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_58 0x58
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_45 0x45
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_5c 0x5c
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_57 0x57
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_28 0x28
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_f2 0xf2
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_0e 0x0e
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_95 0x95
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_45 0x45
#define    IKEV1_NATT_VENDOR_ID_PAYLOAD_2f 0x2f


/* --------- IKEv2 Utililty Macros -------------------- */
#ifdef _IKE2_INIT_C_
INT4 gi4Ike2PktPoolId = 0;
tMemPoolId      gu4IkeDsaMemPoolId = 0;
tMemPoolId     gIke2TransMemPoolId = 0;
#else
extern INT4 gi4Ike2PktPoolId;
extern tMemPoolId   gu4IkeDsaMemPoolId;
extern tMemPoolId   gIke2TransMemPoolId;
#endif

#define IKE_BUDDY_ALLOC(size) \
    MemBuddyAlloc ( (UINT1) gi4Ike2PktPoolId, size)
    
#define IKE_BUDDY_FREE(ptr) \
    MemBuddyFree ( (UINT1) gi4Ike2PktPoolId, (UINT1 *) ptr)


#define IKE_NO_RESPONSE_MESSAGE(ppu1Output)\
   if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID,(UINT1 **) (VOID *) &(*ppu1Output))!= MEM_FAILURE)\
   {\
   if (*ppu1Output != NULL)\
   {\
   CLI_SET_RESP_STATUS (*ppu1Output, FALSE);\
   }\
   }

#define IKE_FORMAT_ERROR_MESSAGE(pString, ppu1Output)\
{\
      UINT1 *pTemp = NULL;\
\
         if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID,(UINT1 **) (VOID *) &(*ppu1Output)) != MEM_FAILURE)\
         {\
           if (*ppu1Output != NULL)\
           {\
            CLI_SET_RESP_STATUS (*ppu1Output, TRUE);\
               pTemp = CLI_GET_DATA_PTR (*ppu1Output);\
                  sprintf ((char *)pTemp, pString);\
           }\
  }\
}
/**  IKEv1 & IKEv2 Common definitions **/
typedef struct NOTIFYINFO {
UINT1 *pu1NotifyData;         /* Notification Data */
UINT2 u2NotifyType;           /* The Notify Messge as per Doi */
UINT2 u2NotifyDataLen;        /* Notification Data Len */
UINT2 u2SpiNum;
UINT1 u1Protocol;             /* Identifies the Protocol Isakmp/Ah/esp */
UINT1 u1SpiSize;              /* The Size of the Spi Field */  
UINT1 u1CryptFlag;            /* Flag to specify the required service *
                                 Encryption/Authentication/None */
UINT1 au1Pad[3];
}tIkeNotifyInfo;

typedef struct IKEPOSTPHASE1INFO {
    UINT4 u4ActionType;
    union 
    {
        tIkeNewSA  SaTriggerInfo;
        tIkeNotifyInfo NotifyInfo;
    }IkePostPhase1Info;

#define IPSecRequest IkePostPhase1Info.SaTriggerInfo
#define IkeNotifyInfo IkePostPhase1Info.NotifyInfo

} tIkePhase1PostProcessInfo;


/*IKEv2 SA Information */
typedef struct IKE2SA {
    UINT1              *pu1SaInitReqPkt; /*Pointer to the IKE SA INIT req */ 
    UINT1              *pu1SaInitRspPkt; /*Pointer to the IKE SA INIT resp*/ 
    UINT1              *pu1InitNonce;   /*Initiator Nonce */
    UINT1              *pu1RespNonce;   /*Responder Nonce */
    t_SLL              PeerCertReq;    /*List of certificate requests*/
    tDesKey            DesKeyRes1;     /* DES/3DES Key */
    tDesKey         DesKeyRes2;     /* DES/3DES Key */
    tDesKey         DesKeyRes3;     /* DES/3DES Key */
    UINT1              au1SKeyIdAr[IKE_MAX_DIGEST_SIZE]; /*
                             authentication key for responder-initiator */
    UINT1              au1SKeyIdEr[IKE_MAX_ENCR_KEY_LEN];/*
                             encryption/decryption key for 
                             responder to initiator  */
    UINT1              au1SKeyIdPi[IKE_MAX_DIGEST_SIZE];  /* 
                             key for generating AUTH payload for 
                             initiator to responder  */
    UINT1              au1SKeyIdPr[IKE_MAX_DIGEST_SIZE]; /*
                             key for generating AUTH payload for
                                          responder to Initiator  */
    UINT4              u4SaInitReqPktLen; /*Length of the IKE SA INIT req*/
    UINT4              u4SaInitRspPktLen; /*Length of the IKE SA INIT resp*/
    UINT4              u4TxMsgId;         /*Next MsgId to be of the packet to be 
                                          transmitted on this IKE SA */
    UINT4              u4RxMsgId;        /*Next MsgId expected to be 
                                          received on this IKE SA */
    UINT2              u2InitNonceLen; /*Initiator Nonce Length  */
    UINT2              u2RespNonceLen;  /*Responder Nonce Length */
    BOOLEAN            bAuthDone;
    BOOLEAN            bIsOrgInitiator; /* Flag to indicate if we are the
                                           Original Initiator*/
    UINT1              au1Pad[2];
}tIke2SA;

typedef struct IKESA {
t_SLL_NODE Next;
UINT1         *pSKeyId;                   /* Key generated from shared secret */
UINT1         *pSKeyIdD;                  /* Key generated for Phase 2 */
UINT1         *pSKeyIdA;                  /* Key for Phase 1 Authentication */
UINT1         *pSKeyIdE;                  /* Key for Phase 1 Encryption */
UINT1         *pu1FinalPhase1IV;          /* IV at the end of Phase 1 */
tIkeIpAddr    IpPeerAddr;           /* The peer address */
tIkeIpAddr    IpLocalAddr;          /* Ike Local Tunnel Endpoint */

tIkeNatT      IkeNattInfo;       /* NAT-T Info */
tIkeTimer     IkeNatKeepAliveTimer;      /* Timer to send NAT Keep-Alive Msg */
tIke2SA       Ike2SA;            /* Instance of the IKE2SA Info */

tDesKey       DesKey1;                 /* DES/3DES Key */
tDesKey       DesKey2;                 /* DES/3DES Key */
tDesKey       DesKey3;                 /* DES/3DES Key */
UINT1         au1AesEncrKey[240];        /* Aes Encr Key */
UINT1         au1AesDecrKey[240];        /* Aes Decr Key */
tIkeTimer     IkeSALifeTimeTimer;     /* Timer to handler SA lifetime expiry */

tIkePhase1ID  InitiatorID;        /* Initiator ID */
tIkePhase1ID  ResponderID;        /* Responder ID */

UINT1         au1InitiatorCookie[IKE_COOKIE_LEN];  /* Initiator Cookie value */
UINT1         au1ResponderCookie[IKE_COOKIE_LEN]; /* Responder Cookie value */

UINT4         u4IkeEngineIndex;   /* IKE Engine Index */
UINT4         u4RefCount;
UINT4         u4LifeTime;
UINT4         u4LifeTimeKB;
UINT4         u4ByteCount;
UINT4         u4AbsHardLifeTime;          /* The system time when SA is to be deleted */
UINT4         u4SoftLifeTimeBytes;        /* Soft Lifetime in bytes */

UINT2         u2AuthMethod;        /* Authentication method preshared */
UINT2         u2EncrKLen;          /*Encryption key len*/
UINT2         u2SKeyIdLen;         /* Generated Key Length */

UINT1         u1SAStatus;          /* Whether ACTIVE/NOTINSERVICE, This is
                                      checked only in timer processing */
UINT1         u1Phase1HashAlgo;
UINT1         u1Phase1EncrAlgo;

BOOLEAN       bXAuthEnabled;
BOOLEAN       bCMEnabled;
BOOLEAN       bXAuthDone;           /* XAuth done successfully */
BOOLEAN       bCMDone;              /* Config Mode done successfully */
BOOLEAN       bTransDone;
BOOLEAN       bInfoDelSent;         /* Informational msg with delete
                                     payload sent */
UINT1        u1IkeVer;              /* IkeSA for IKEv1/IKEv2 */
UINT1        bRekey;                 /* Phase1 Rekey for RAVPN */
UINT1        au1Pad[3];              /* Padding */
}tIkeSA;

typedef struct IKESESSION1INFO {
UINT2      u2NegKeyLen;                /* Negotiated AES key length*/
UINT2      u2PreSharedKeyLen;          /* Preshared Key Length */
tIkePolicy IkePolicy;             /* Configured Enc&Hash Algs */
UINT1      *pu1PreSharedKey;           /* Ike Preshared Key */
}tIkePhase1SessionInfo;

typedef struct IKETRANSINFO {
BOOLEAN       bXAuthServer;
BOOLEAN       bCMServer;
UINT1         u1P1AuthMethod;             /* Phase 1 Auth Method */
UINT1         u1TransPktType;             /* Valid only in Transaction mode */
tIkeNetwork   ProtectedNetwork[IKE_MAX_PROTECTED_NET];
tIkeIpAddr    InternalIpAddr;
tIkeIpAddr    InternalMask;
UINT2         au2XAuthRecvAttr[IKE_MAX_SUPPORTED_ATTR];
UINT2         au2CMAcptAttr[IKE_MAX_SUPPORTED_ATTR];
UINT2         au2CMRecvAttr[IKE_MAX_SUPPORTED_ATTR];
UINT1         au1XAuthRecvUsername[MAX_NAME_LENGTH];
UINT1         au1XAuthRecvPassword[MAX_NAME_LENGTH];
BOOLEAN       bUserAuthenticated; /* Initialize to FALSE, set it to 
                               TRUE when user is authenticated */
BOOLEAN       bXAuthReqSuccess; /* Default is false */
BOOLEAN       bCMSuccess; /* Default is false */
UINT1         u1Pad1;
UINT2         u2RequestId;                /* Last/Current Request ID */
UINT2         u2Pad2;
tIkeRemoteAccessInfo IkeRAInfo;   /* Remote Access Info */
}tIkeTransSessionInfo;

typedef struct IKESESSION2INFO {
UINT1         u1Protocol;
UINT1         u1NegPfs;                   /* The negotiated pfs value */
UINT2         u2NegKeyLen;                /* Negotiated key length*/
UINT1         *pu1Phase1SA;               /* Pointer to the phase 1 SA */
tIPSecBundle  IPSecBundle;         /* IPSec SAs resulted in phase2 */ 
tIkeCryptoMap IkeCryptoMap;       /* Phase 2 configuration  */
}tIkePhase2SessionInfo;

typedef struct IKEPAYLOAD {
t_SLL_NODE Next;
UINT2      u2PayLoadType;
UINT2      u2NxtPayLoadType;
UINT1      *pRawPayLoad;
UINT4      u4PayLoadLen;
UINT1      u1ProcessingStatus;
UINT1      u1Pad;
UINT2      u2Pad;
}tIkePayload;    

typedef struct IKEPROP
{
    t_SLL_NODE Next;
    UINT1 u1PropNum;
    UINT1 u1ProtocolId;
    UINT2 u2PayLoadLen;
    UINT4 u4Spi;
    UINT1 au1IkeCookie[IKE_COOKIE_LEN];
    UINT1 *pu1Proposal;
    t_SLL IkeTransPayLoadList;
    struct IKEPROP *pNextProposal;
}tIkeProposal;

typedef struct IKETRANS
{
    t_SLL_NODE Next;
    UINT1 u1TransId;
    UINT1 u1ProtocolId;
    UINT2 u2PayLoadLen;
    UINT1 *pu1Transform;
}tIkeTrans;

#define IKE_PAYLOAD_MALLOC()     MEM_MALLOC (sizeof(tIkePayload), tIkePayload)
#define IKE_PROPOSAL_MALLOC()    MEM_MALLOC (sizeof(tIkeProposal), tIkeProposal)
#define IKE_TRANSFORM_MALLOC()     MEM_MALLOC (sizeof(tIkeTrans), tIkeTrans)

#define IKE_MAX_VENDOR_ID_SIZE     64
typedef struct VENDORID {
    UINT4 u4Length;
    UINT1 au1VendorID [IKE_MAX_VENDOR_ID_SIZE];
}tIkeVendorId;

typedef struct IKEPACKET {
t_SLL      aPayload[IKE_MAX_PAYLOADS + 1];
UINT1      *pu1RawPkt;
UINT4      u4PacketLen;                /* Packet Length */
UINT4      u4BufLen;
}tIkePacket;


/*IKE_SA_INIT message Information */
typedef struct  IKE2INITSESSIONINFO { 
UINT1           au1ResponderCookie[IKE_COOKIE_LEN]; /*Responder cookie*/
tIkePolicy   IkePolicy;     /*IKE policy*/
UINT2       u2NegKeyLen;   /*Negotiated key Length*/
UINT2       u2Pad;         /*Padding bytes*/
} tIke2InitSessionInfo;

/*IKE_AUTH message Information */
typedef struct  IKE2AUTHSESSIONINFO{ 
tIPSecBundle         IPSecBundle;   /*IPSec SAs resulted in negotiation*/
tIkeCryptoMap         IkeCryptoMap;  /*IPSec configurations */
tIkeNetwork          ProtectedNetwork[IKE_MAX_PROTECTED_NET];
                                           /*Remote protected network*/
tIkeIpAddr          InternalIpAddr;/*IP address allocated for RAVPN client*/
tIkeIpAddr          InternalMask;  /*Address Mask for RAVPN client*/
tIkeRemoteAccessInfo    IkeRAInfo;     /*Specifies the RaVpn policy*/
UINT4                   u4ReKeyingSpi;    /* Old SPI Which is being rekeyed*/
UINT4                   u4ReKeyingSpiLen; /* Rekeying SPI length */
UINT2              au2CMAcptAttr[IKE_MAX_SUPPORTED_ATTR];
                                            /*Accepted attributes from 
                                              configuration payload */
UINT2              au2CMRecvAttr[IKE_MAX_SUPPORTED_ATTR];
                                            /*Received attributes from the 
                                              configuration payload*/
UINT2              u2NegKeyLen;   /*The negotiated key length */
UINT1               u1Protocol;    /*Received protocol in the IKE packet*/
UINT1              u1NegPfs;      /*The negotiated PFS value*/
BOOLEAN              bCMServer;     /*Flag indicates CM server is
                                              enabled or not */
BOOLEAN                 bTransportReqd;   /* Flag to indicate reception of 
                                          N(USE_TRANSPORT)*/
BOOLEAN                 bCMRequested;   /* Flag to indicate reception of CP (CFG_REQ)*/ 
BOOLEAN              bCMSuccess;    /*Indicates config payload processing 
                                      is successful or not */
} tIke2AuthSessionInfo;


/*INFORMATIONAL message Information */
typedef struct IKE2INFOSESSIONINFO{ 
    t_SLL     *pDelPayloads; /*List of Delete payloads*/
    void       *pCfgPayLoad;  /*List of config payloads*/
} tIke2InfoSessionInfo;

typedef struct IKESESSIONINFO {
t_SLL_NODE   Next;
UINT1        au1EngineName[MAX_NAME_LENGTH]; /* Ike engine name */
tDhGroupInfo *pDhInfo;            /* Contains information on the 
                                     configured DH group: prime,
                                     generator, length, type */
tHexNum      *pMyDhPub;                /* My DH Public key */
tHexNum      *pMyDhPrv;                /* My DH Private key */
tHexNum      *pHisDhPub;               /* His DH Shared secret */
tHexNum      *pDhSharedSecret;         /* DH Shared secret */
UINT1        *pSAOffered;                /* Offered Proposals */
UINT1        *pu1SaSelected;             /* SA's selected by the peer */
UINT1        *pu1InitiatorNonce;         /* Initiator Nonce */
UINT1        *pu1ResponderNonce;         /* Responder Nonce */
UINT1        *pu1LastRcvdPkt;            /* To check for retransmissions */
UINT1        *pu1LastRcvdGoodPkt;        /* To avoid replay attacks */
UINT1        *pu1IdPayloadSent;          /* The ID payload sent */
UINT1        *pu1IdPayloadRecd;          /* The ID payload recd */
tIkeSA       *pIkeSA;                /* phase1  information */
tIkeSA       *pIkeReKeySA;             /* Ike SA during Child Exch for REKEY IKE SA */
UINT1        *pu1CryptIV;            /* IV to be used for Encr/Decr */

t_SLL        IkeProposalList;            /* List for storing parsed proposals
                                    and transforms */
tIkePacket   IkeRxPktInfo;          /* Information of Packet received */
tIkePacket   IkeTxPktInfo;          /* Information of Packet transmitted */
tIkeIpAddr   RemoteTunnelTermAddr;  /* Remote tunnel termination address */ 
tIkeTimer    IkeRetransTimer;        /* retransmission Timer */
tIkeTimer    IkeSessionDeleteTimer;  /* Delay Timer before session deletion */

tIkePhase1PostProcessInfo PostPhase1Info; /* Information on what to be done 
                                             after Phase1 */

tIkeCertInfo    PeerCertInfo;     /* Certficate info of the peer */
t_SLL           PeerCertReq;      /* Info on Cert req obtained from the peer */ 
tIkeCertInfo    MyCert;           /* Info on the cert to be send to the peer */
tIkeRsaKeyInfo  MyCertKey;        /* The key associated with MyCert */
tIkeNotifyInfo NotifyInfo;         /* NotifyInfo structure to hold the 
                                      notify details (IKEv2) to be sent in 
                                      response */
union 
{ 
    tIkePhase1SessionInfo   Phase1Info; 
    tIkePhase2SessionInfo   Phase2Info;
    tIkeTransSessionInfo    TransInfo;
    tIke2InitSessionInfo  InitExchInfo;
    tIke2AuthSessionInfo  AuthExchInfo;
    tIke2InfoSessionInfo  InfoExchInfo;
}IkeExchInfo;

#define IkePhase1Info IkeExchInfo.Phase1Info
#define IkePhase2Info IkeExchInfo.Phase2Info
#define IkeTransInfo IkeExchInfo.TransInfo

#define Ike2InitInfo IkeExchInfo.InitExchInfo
#define Ike2AuthInfo IkeExchInfo.AuthExchInfo
#define Ike2InfoExchInfo IkeExchInfo.InfoExchInfo

#define IkePhase1Policy IkeExchInfo.Phase1Info.IkePolicy
#define IkePhase2Policy IkeExchInfo.Phase2Info.IkeCryptoMap

#define Ike2IkePolicy IkeExchInfo.InitExchInfo.IkePolicy
#define Ike2IPSecPolicy IkeExchInfo.AuthExchInfo.IkeCryptoMap

#define IKE_MAX_VENDOR_IDS     12
tIkeVendorId aInVendorIDs [IKE_MAX_VENDOR_IDS];


UINT4         u4MsgId;               /* Message Identification */
UINT4         u4RefCount;            /* Used by Timer */
UINT4         u4IkeEngineIndex;      /* IKE Engine Index */
UINT4         u4IdPayloadRecdLen;    /* The len of the ID payload recd */
UINT4         u4IdPayloadSentLen;    /* The len of the ID payload sent */
UINT4         au4LastPropAttrib[IKE_MAX_ATTRIBUTES];   /*Array for storing
                                            attribute values of the last Proposal*/
UINT4        u4NoofInVendorIDs;
UINT4        u4NoofOutVendorIDs;
UINT4        u4TimeStamp;

INT2          i2SocketId;            /* Socket Id copied from ikeengine */   
UINT2         u2NotifyType;          /* Notifcation error type */

UINT2         u2SALength;            /* Offered Proposal Length */
UINT2         u2SaSelectedLen;       /* Number of SA's selected */

UINT2         u2InitiatorNonceLen;   /* Initiator Nonce Length */
UINT2         u2ResponderNonceLen;   /* Responder Nonce Length */

UINT2         u2LastRcvdPktLen;
UINT2         u2LastRcvdGoodPktLen;

UINT1         u1FsmState;       /* State of the FSM */
UINT1         u1SessionStatus;  /* Whether sessoin ACTIVE/NOTINSERVICE 
                                   This is checked only when processing
                                   timers */
UINT1         u1ExchangeType;   /* Main Aggressive or Quick */
UINT1         u1Role;           /* Initiator or Responder */

UINT1         u1ReTransCount;   /* Ike Packet Retransmission Count */
UINT1         u1LifeTimeType;   /* whether secs or KB, used for both Phase1
                                   and phase2 */
UINT1         u1IkeVer;
UINT1         u1ChildType;      /* Child session type for IKE_V2 
                                (REKEY_IKESA/NEW_CHILD/REKEY_CHILD)*/

BOOLEAN       bDefaultMyCert;   /* Flag specifying mycert is a default cert
                                   or a configured cert for the peer. 
                                   TRUE for default cert else false */
BOOLEAN       bLastPropNumSame;  /*This flag is used to compare the attributes
                                   if last proposal was also of the same number*/
BOOLEAN       bInitialContact;   /* Flag to specify Initial Contact */
BOOLEAN       bPhase1Done;

BOOLEAN       bPhase2Done;
BOOLEAN       bInfoDone;       /* Info Session complete for Ikev2 */
BOOLEAN       bDelSession;     
BOOLEAN      bConfigReqSent;  /*This flag is used to denote if Config Request 
                                has been sent or not */
UINT1        bRekey;          /*RAVPN phase1 Rekey */
UINT1        au1Padding[3];
}tIkeSessionInfo;

/* Enum containing the list of supported Vendor Id Payloads */
typedef enum
{
    IKE_XAUTH_VENDOR_ID = 0,
    IKE_CISCO_UNITY_PEER_VENDOR_ID, 
    IKE_NATT_SUPPORT_VENDOR_ID,
    IKE_MAX_SUPPORTED_VENDOR_IDS
} tIkeVendorList;

typedef struct VENDORIDPAYLOAD
{
     UINT1        au1Payload [IKE_MAX_VENDOR_ID_SIZE];
     UINT4        u4Length;
} tIkeVendorIdPayload;


typedef struct ISAKMPHDR {
UINT1 au1InitCookie[IKE_COOKIE_LEN];
UINT1 au1RespCookie[IKE_COOKIE_LEN];
UINT1 u1NextPayload;
UINT1 u1Version;
UINT1 u1Exch;
UINT1 u1Flags;
UINT4 u4MessId;
UINT4 u4TotalLen;
}tIsakmpHdr;


typedef struct IKESTATISTICSINFO
{
tIkeIpAddr IpPeerAddr;              /* Peer with which key is negotiated */
UINT4    u4IkeInitSessions;         /* Total no. of Sessions Initiated so far */
UINT4    u4IkeAcceptSessions;       /* Total no. of Sessions Accepted so far */
UINT4    u4IkeActiveSessions;       /* Total no. of active sessions at a time */
UINT4    u4IkeInPkts;               /* Total no. of packets received */
UINT4    u4IkeOutPkts;              /* Total no. of packets */
UINT4    u4IkeDropPkts;             /* Total no. of packets dropped */
UINT4    u4IkeNoOfNotifySent;       /* Total no. of notifies sent to the peer */
UINT4    u4IkeNoOfNotifyReceive;    /* Total no. of notifies received */
UINT4    u4IkeNoOfPhase1SessionsSucceed;  /* Total no. of phase1 successful sessions */
UINT4    u4IkeNoOfPhase1SessionsFailed;  /* Total no. of phase1 failure sessions */
UINT4    u4IkeNoOfSessionsSucceed;  /* Total no. of phase2 successful sessions */
UINT4    u4IkeNoOfSessionsFailed;   /* Total no. of phase2 sessions that failed */
UINT4    u4IkePhase1Rekey;      /* Total no. of phase1 rekey */
UINT4    u4IkePhase2Rekey;      /* Total no. of phase2 rekey */
}
tIkeStatisticsInfo;

typedef struct SAPAYLOAD{    
    UINT1  u1NextPayLoad;  /* The Next PayLoad in the Isakmp Message */
    UINT1  u1Reserved;     /* Reserved Field */  
    UINT2  u2PayLoadLen;   /* The Length of the PayLoad */            
    UINT4  u4Doi;          /* The Domain of Interpretaion Specific */
    UINT4  u4Situation;    /* Identifies the situation */
}tSaPayLoad;


typedef struct PROPOSALPAYLOAD{    
    UINT1  u1NextPayLoad;  /* Identifies the next payload in the packet */
    UINT1  u1Reserved;     /* Reserved field */
    UINT2  u2PayLoadLen;   /* The length of the current payload */        
    UINT1  u1PropNum;      /* Identifies the proposal */        
    UINT1  u1ProtocolId;   /* Identifies the Protocol Isakmp/Ah/esp */
    UINT1  u1SpiSize;      /* The Size of the Spi Field */ 
    UINT1  u1NumTran;      /* The no of Transforms in a proposal */
}tProposalPayLoad;    


typedef struct TRANSFORMPAYLOAD{    
    UINT1  u1NextPayLoad;  /* Identifies the next payload in the packet */ 
    UINT1  u1Reserved;     /* Reserved field */
    UINT2  u2PayLoadLen;   /* The length of the payload packet */            
    UINT1  u1TransNum;     /* Identifies the transform */        
    UINT1  u1TransId;      /* Specifies MD5/SHA */
    UINT2  u2Reserved;     /* Reserved */ 
}tTransPayLoad;
    
typedef struct SABASICATTRIBUTE {
    UINT2 u2Attr;
    UINT2 u2BasicValue;
} tSaBasicAttribute;

typedef struct SAVPIATTRIBUTE {
    UINT2 u2Attr;
    UINT2 u2VpiLength;
} tSaVpiAttribute;


typedef struct IDPAYLOAD{    
 UINT1  u1NextPayLoad; /* Identifies the next payload in the packet */
 UINT1  u1Reserved;    /* reserved */
 UINT2  u2PayLoadLen;  /* The length of the current payload */        
 UINT1  u1IdType;      /*Specifies the identification type */        
 UINT1  u1Protocol;    /*Specifies the protocol */ 
 UINT2  u2Port;        /* Speciifes the port */
}tIdPayLoad;    

typedef struct NOTIFYPAYLOAD {    
 UINT1  u1NextPayLoad; /* Identifies the next payload in the packet */
 UINT1  u1Reserved;    /* reserved */
 UINT2  u2PayLoadLen;  /* The length of the current payload */        
 UINT4  u4Doi;         /* The Domain of Interpretaion Specific */    
 UINT1  u1ProtocolId;  /* Identifies the Protocol Isakmp/Ah/esp */
 UINT1  u1SpiSize;     /* The Size of the Spi Field */  
 UINT2  u2NotifyMsg;   /* The Notify Messge as per Doi */
}tNotifyPayLoad;    

typedef struct DELETEPAYLOAD {    
 UINT1 u1NextPayLoad; /* Identifies the next payload in the packet */
 UINT1 u1Reserved;    /* reserved */
 UINT2 u2PayLoadLen;  /* The length of the current payload */        
 UINT4 u4Doi;         /* The Domain of Interpretaion Specific */    
 UINT1 u1ProtocolId;  /* Identifies the Protocol Isakmp/Ah/esp */
 UINT1 u1SpiSize;     /* The Size of the Spi Field */
 UINT2 u2NSpis;       /* The No of Spi's */
}tDeletePayLoad;    

typedef struct ATTRIBUTEPAYLOAD {

   UINT1  u1NextPayLoad;  /* Identifies the next payload in the packet */
   UINT1  u1Reserved;     /* Reserved */
   UINT2  u2PayLoadLen;   /* The Length of the current PayLoad */
   UINT1  u1Type;         /* Type of messages represented by the Attributes */
   UINT1  u1Reserved1;     /* Reserved Field */
   UINT2  u2Id;           /* ID to Refer configuration transaction within the 
                             individual messages */
}tAttributePayLoad; 

typedef struct GENERICPAYLOAD {
    UINT1         u1NextPayLoad;     /* Refers to the Next PayLoad */
    UINT1         u1Reserved;        /* Reserved Field */
    UINT2         u2PayLoadLen;      /* PayLoad Length */
}tGenericPayLoad;

typedef struct CERTPAYLOAD {
    UINT1         u1NextPayLoad;     /* Refers to the Next PayLoad */
    UINT1         u1Reserved;        /* Reserved Field */
    UINT2         u2PayLoadLen;      /* PayLoad Length */
    UINT1         u1EncodeType;      /* encode type of the certficate */
    UINT1         au1Padding[3];
}tCertPayLoad;

typedef tCertPayLoad    tCertReqPayLoad;
typedef tGenericPayLoad tNoncePayLoad;
typedef tGenericPayLoad tHashPayLoad;
typedef tGenericPayLoad tKeyExcPayLoad;
typedef tGenericPayLoad tSigPayLoad;
typedef tGenericPayLoad tVendorIdPayLoad;
typedef tGenericPayLoad tNattDetectPayLoad;



typedef struct SAOFFER{
  struct SAOFFER *pNext;
  UINT4  u4Spi;
  UINT1  *pu1Atts;
  UINT1  u1Transform;
  UINT1  u1Protocol;
  UINT2  u2Natts;
}tSaOffer;

/* Common Global varaibles */
extern UINT4 gu4IkeTrace;
extern tTimerListId gu4TimerId; 
extern tIkeStatisticsInfo *gpIkeStatisticsInfo;
extern tTMO_SLL gIkeEngineList;


/* IKE Init Exporting Functions */
VOID  IkeSockSelectTaskMain (INT1 *);
VOID  IkeProcessIkePkt (tIkeEngine * pIkeEngine, UINT1 *pu1Packet,
                        UINT2 u2PacketLen, tIkeIpAddr * pPeerIpAddr,
                        UINT2 u2RemotePort, UINT2 u2LocalPort);
INT4  IkeLock   PROTO ((VOID));
INT4  IkeUnLock PROTO ((VOID));

/* IKE Main Exporting Functions */
INT1 IkeTrigger(UINT1,UINT2,tIkeNetwork *,tIkeNetwork *, tIkeIpAddr *);
INT1 IkeSendIPSecIfParam (tIkeIPSecIfParam *IPSecIfParam);
VOID IkeProcessIpsecRequest(tIkeIPSecIfParam *);
VOID IkeProcessConfigRequest (tIkeConfigIfParam *pIkeConfigIfParam);
INT1 IkeSendConfMsgToIke ( tIkeConfigIfParam *pIkeConfigIfParam );
VOID IkeProcessL2tpRequest (tIkeL2TPIfParam * pL2TPIfParam);
VOID TriggerIke (tIkeIpAddr *pIkeIpAddr,UINT1 u1IkeVersion);

/* IKE Core Exporting Functions */
INT1 IkeCoreProcess(tIkeSessionInfo *);
INT1 IkeMainMode(tIkeSessionInfo *,tIsakmpHdr *);
INT1 IkeQuickMode(tIkeSessionInfo *);
INT1 IkeAggressiveMode(tIkeSessionInfo *,tIsakmpHdr *);
INT1 IkeInformationExchMode (tIkeSessionInfo * pSessionInfo);
INT1 IkeTransactionExchMode (tIkeSessionInfo * pSessionInfo);

/* Ike SLI exporting functions */
VOID IkeSendToSocketLayer(tIkeSessionInfo *);
INT1 IkeStartListener(tIkeEngine *pIkeEngine);

/* Ike Construction Exporting Functions */
INT1 ConstructIsakmpMesg (tIkeSessionInfo *,INT4,UINT4); 
INT1 IkeConstructHeader(tIkeSessionInfo *,UINT1,UINT1);
INT1 IkeConstructKE(tIkeSessionInfo *,UINT1,UINT4*);
INT1 IkeConstructNonce(tIkeSessionInfo *,UINT1,UINT4*);
INT1 IkeConstructID(tIkeSessionInfo*,UINT1,UINT4*);
INT1 IkeConstructIsakmpSA(tIkeSessionInfo *,UINT1,UINT4*);
INT1 IkeConstructIpsecSA(tIkeSessionInfo *,UINT1,UINT4*);
INT1 IkeConstructHash(tIkeSessionInfo *,UINT1,UINT4*);          
INT1 IkeConstructNotify(tIkeSessionInfo *,UINT1,UINT4*, tIkeNotifyInfo *);
INT1 IkeConstructDelete(tIkeSessionInfo *,UINT1,UINT4*, tIkeNotifyInfo *);
INT1 IkeConstructQmHash1(tIkeSessionInfo *, UINT1 u1NextPayLoad);
INT1 IkeConstructQmHash2(tIkeSessionInfo *);
INT1 IkeConstructQmHash3(tIkeSessionInfo *, UINT1);
INT1 IkeConstructProxyID (tIkeSessionInfo *, tIkeNetwork *, UINT1,UINT4*); 
INT1 IkeSendNotifyInfo(tIkeSessionInfo *pSessionInfo);
INT1 IkeConstructInfoExchPacket (tIkeNotifyInfo *pNotifyInfo, tIkeSA *pIkeSA);
INT1 IkeConstructVendorId (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,UINT4 *pu4Pos);
INT1 IkeConstructVendorIdPayload (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, 
        UINT4 *pu4Pos, UINT4 u4Count);

/* Ike certificate construct functions */
INT1 IkeConstructCertRequest (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,UINT4 *pu4Pos);
INT1 IkeConstructCertificate (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,UINT4 *pu4Pos);
INT1 IkeConstructSignature (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,UINT4 *pu4Pos);

/* XAuth and CM Config Functions */
INT1 IkeConstructIsakmpCMCfgReqPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpCMCfgRepPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpCMCfgSetPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpCMCfgAckPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpXAuthCfgReqPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpXAuthCfgRepPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpXAuthCfgSetPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructIsakmpXAuthCfgAckPkt (tIkeSessionInfo * pSesInfo);
INT1 IkeConstructTransactionHash (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad);
INT1 IkeConstructNatdPayLoad (tIkeSessionInfo *, UINT1, UINT4 *);
INT1 IkeCopyRAPolicy (tIkeSessionInfo *pSessionInfo);

/* Ike Process Exporting Functions */
INT1 IkeProcessQmHash1 (tIkeSessionInfo *pSesInfo);
INT1 IkeProcessQmHash2 (tIkeSessionInfo *pSesInfo);
INT1 IkeProcessQmHash3 (tIkeSessionInfo *pSesInfo);
INT1 IkeProcessNonce (tIkeSessionInfo *,UINT1 *);
INT1 IkeProcessNotify (tIkeSessionInfo *,UINT1 *);
INT1 IkeProcessDelete (tIkeSessionInfo *,UINT1 *);
INT1 IkeProcessSa (tIkeSessionInfo *,UINT1 *);
INT1 IkeEncryptPayLoad (tIkeSessionInfo * pSessionInfo);
INT1 IkeDecryptPayLoad (tIkeSessionInfo * pSessionInfo);
INT1 IkeProcessId(tIkeSessionInfo *,UINT1 *);
INT1 IkeSetId (tIkeSessionInfo *,UINT1 *,UINT1 *);  
INT1 IkeProcessPhase2Id (tIkeSessionInfo *);
INT1 IkeProcess(UINT1,tIkeSessionInfo *,UINT1 *);
INT1 IkeProcessKE (tIkeSessionInfo *,UINT1 *);
INT1 IkeValidatePacket (tIkeSessionInfo *, UINT4,UINT1,UINT1 *);
INT1 IkeVerifyHash (tIkeSessionInfo *,UINT1 *);
INT1 IkeComputeHash (tIkeSessionInfo *,UINT1 *);
INT1 IkeProcessCertRequest (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad);
INT1 IkeProcessCertificate (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad);
INT1 IkeProcessSignature (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad);
INT1 IkeProcessVendorId (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad);
INT1 IkeProcessTransactionHash (tIkeSessionInfo * pSesInfo);
INT1 IkeProcessIsakmpCMCfgReqPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpCMCfgRepPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpCMCfgSetPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpCMCfgAckPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpXAuthCfgReqPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpXAuthCfgRepPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpXAuthCfgSetPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkeProcessIsakmpXAuthCfgAckPkt (tIkeSessionInfo * pSesInfo,UINT1 *pu1Pd);
INT1 IkePreProcessTransPkt (tIkeSessionInfo * pSesInfo);

/* Statistics Prototypes */
VOID IncIkeInPkts(tIkeIpAddr *);
VOID IncIkeOutPkts(tIkeIpAddr *);
VOID IncIkeDropPkts(tIkeIpAddr *);
VOID IncIkeNoOfNotifySent(tIkeIpAddr *);
VOID IncIkeNoOfNotifyRecv(tIkeIpAddr *);
VOID IncIkeNoOfSessionsSucc (tIkeIpAddr * pIpAddr);
VOID IncIkeNoOfSessionsFail (tIkeIpAddr * pIpAddr);
VOID DecIkeActiveSessions(tIkeIpAddr *);
VOID IncIkeActiveSessions(tIkeIpAddr *);
VOID IncIkePhase1ReKey(VOID);
VOID IncIkePhase2ReKey(VOID);
VOID IncIkeNoOfPhase1SessionsSucc (VOID);
VOID IncIkeNoOfPhase1SessionsFailed (VOID);

/* Ike utilities exporting functions */
VOID IkeGetRandom (UINT1 *pu1Ptr, INT4 u4Len);
INT1 IkeGenSkeyId (tIkeSessionInfo * pSessionInfo);
INT4 IkeGenIV (tIkeSessionInfo *pSessionInfo, UINT1 u1Phase);
INT1 IkeSetPhase1Key (tIkeSessionInfo *pSessionInfo, UINT1 *pu1KeyMat);
tIkeEngine * IkeGetIkeEngine (tIkeIpAddr *pLocalSrcAddr);
INT1 IkeGenIPSecKeys (tIkeSessionInfo * pSessionInfo);
INT1 IkeGenIPSecKeyMat (tIkeSessionInfo * pSessionInfo, tIPSecSA *pIPSecSA);
VOID IkeAssertFail(const char *pu1Expr, const char *pu1FileName, int pu1LineNum);
VOID IkeConvertToRangeForIpv4(tIkeNetwork *pNetwork, tIkeIpv4Range *pIpv4Range);
VOID IkeConvertToRangeForIpv6(tIkeNetwork *pNetwork, tIkeIpv6Range *pIpv6Range);
BOOLEAN IkeCompareNetworks (tIkeNetwork *pNetwork1, tIkeNetwork *pNetwork2, UINT1 u1IkeVer);
tIkeCryptoMap * IkeGetCryptoMapFromIDs ( tIkeEngine *pIkeEngine,tIkeNetwork *pLocNetwork,
                                         tIkeNetwork *pRemNetwork , UINT1 u1IkeVer);
tIkeCryptoMap * IkeGetCryptoMap (tIkeEngine *pIkeEngine, tIkeIpAddr *pSrcAddr, 
        tIkeIpAddr *pDestAddr);
INT4 IkeCheckv6Network(tIkeNetwork *pNetwork, tIkeIpAddr *pSrcAddr);
INT4 IkeCheckv4Network(tIkeNetwork *pNetwork, tIkeIpAddr *pSrcAddr);
INT4 IkeMatchv6Range(tIp6Addr *pIpv6Addr, tIp6Addr *pIp6StartAddr, 
                     tIp6Addr *pIp6EndAddr);
INT4 IkeMatchv4Range(UINT4 u4Ipv4Addr, UINT4 u4Ip4StartAddr, UINT4 u4Ip4EndAddr);
INT4 IkeMatchv4Network (UINT4 u4Ipv4Addr, UINT4 u4Ip4ConfAddr, UINT4 u4Ip4SubNet);
tIkeSA * IkeGetIkeSAFromCookies (tIkeEngine *pIkeEngine, UINT1 *pu1InitCookie, 
                        UINT1 *pu1RespCookie, tIkeIpAddr *pPeerTEAddr);
tIkeEngine * IkeGetIkeEngineFromIndex (UINT4 u4IkeEngineIndex);
INT1 IkeGetId ( tIdPayLoad *pId, UINT1 *pu1PayLoad, tIkeNetwork *pIkeNetwork );
tIkeSA * IkeGetIkeSAFromPeerAddress(tIkeEngine *pIkeEngine, tIkeIpAddr *pPeerAddr);
INT4 IkeDeleteIkeSA (tIkeSA *pIkeSA);
INT4 IkeProcessInitialContact (tIkeSessionInfo * pSessionInfo);
INT4 IkeDeleteIkeSAWithCookies(tIkeSessionInfo *pSessionInfo, UINT1 *pu1InitCookie,
                               UINT1 *pu1RespCookie);
VOID IkeFreePayload (t_SLL_NODE * pNode);
VOID IkeFreeProposal (t_SLL_NODE * pNode);
VOID IkeFreeTransform (t_SLL_NODE * pNode);
INT4 IkeDeriveKeysFromSkeyId (tIkeSessionInfo * pSessionInfo);
INT4 IkeComparePhase1IDs (tIkePhase1ID *pPhase1ID1, tIkePhase1ID *pPhase1ID2);
tIkeRsaKeyInfo * IkeGetRSAKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyName);
tIkeRsaKeyInfo * IkeGetRSAKeyEntryByEngine (tIkeEngine *pIkeEngine,UINT1 *pu1KeyName);
tIkeCertMapToPeer *IkeGetCertMapFromRemoteID(tIkeEngine *pIkeEngine,UINT1 *pu1Dn, UINT1 u1IdType);
tIkeCertMapToPeer * IkeGetCertMapFromPeerAddr(tIkeEngine *pIkeEngine, tIkeIpAddr *pIpPeerAddr);
tIkeRsaKeyInfo * IkeGetRSAKeyWithCertInfo (tIkeEngine * pIkeEngine, tIkeCertInfo *pCertInfo);
BOOLEAN IkeCheckSendingCertPayload (tIkeSessionInfo *pSesInfo);
BOOLEAN IkeCheckPeerCertDB (tIkeSessionInfo *pSesInfo);
UINT4 IkeSnprintf (signed char *pu1Buf, UINT4 u4BufSize, const signed char *pu1Format, ...);
INT1 IkeUpdtPostPhase1InfoFromRAInfo(tIkeSessionInfo *pSessionInfo);
INT1 IkeCreateDynCryptoMapForClient(tIkeSessionInfo *pSessionInfo);
INT1 IkeCreateDynCryptoMapForServer(tIkeSessionInfo *pSessionInfo);
INT1 IkeUtilSetCryptoAsDynamic (tSNMP_OCTET_STRING_TYPE *pEngineNameOctetStr,
                           tSNMP_OCTET_STRING_TYPE *pCryptoMapOctStr, BOOLEAN bIsDyanmic);
INT1 IkeUtilDelDynCryptoMapForServer (UINT4 u4IkeEngineIndex, UINT4 u4Spi);
INT1 IkeUtilDelAllCryptoMapforPolicy (tSNMP_OCTET_STRING_TYPE *pEngineNameOctetStr,
                                 tSNMP_OCTET_STRING_TYPE *pPolicyNameOctStr);
INT4 IkeUtilUpdatePolicyName(tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                      UINT4 u4IkePolicyPriority,
                      tSNMP_OCTET_STRING_TYPE * pPolicyNameOctStr);

INT4 IkeUtilGetPolicyPriotityFromName (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                                  tSNMP_OCTET_STRING_TYPE * pPolicyNameOctStr,
                                  UINT4 *pu4IkePolicyPriority);

tIkeRemoteAccessInfo *IkeGetRAInfoFromPeerIpAddr (tIkeEngine * pIkeEngine,tIkeIpAddr * pDestAddr);
INT1 IkeMaskToPrefix(UINT4 u4Mask, UINT1 *pu1Prefix);
INT1 Ikev6MaskToPrefix (UINT1 *pu1Mask, UINT1 *pu1Prefix);

VOID  IkeDeleteIkeKeyMsr(tIkeKey * pIkeKey);

INT1 IkeUpdateProtocolInfo (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                       tSNMP_OCTET_STRING_TYPE * pCryptoMapOctStr,
                                                UINT4 u4IkeHLProtocol);
INT1
IkeUpdateRaPortInfo (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                     tSNMP_OCTET_STRING_TYPE * pProcNetAddrOctStr,
                     UINT2 u2LocalStartPort, UINT2 u2LocalEndPort,
                     UINT4 u4IkeHLProtocol);
INT1
IkeCreateVpncInterface (tIkeSessionInfo *pSessionInfo);
INT4
IkeUtilGetFreeAddrFromPool (tIkeIpAddr *pIkePeerAddr, 
                            tIkeIpAddr *pIkeFreeIpAddr, UINT4 *pu4PrefixLen);

/* Ike session exporting functions */
INT4 IkeSetNonceInSessionInfo(tIkeSessionInfo *, UINT1 *, UINT2, UINT1);
INT4 IkeDeleteSession(tIkeSessionInfo *);
INT4 IkeStoreIkeSA(tIkeSessionInfo *pSessionInfo);
tIkeSessionInfo *IkePhase1SessionInit (UINT1 u1Role, tIkeEngine *pIkeEngine, 
        UINT1 u1Exch, tIkeIpAddr *pTEAddr);
INT4 IkeCopyPhase1SessionInfo(tIkeEngine *IkeEngine, tIkeSessionInfo *pSesInfo,
          UINT1 u1Exch); 
tIkeSessionInfo * IkePhase2SessionInit (tIkeSA *pIkeSA, tIkeNewSA *pIPSecReq,
        UINT4 u4MesgId, UINT1 u1Role);
tIkeSessionInfo * IkeInfoExchInit( tIkeSA *pIkeSA, UINT4 u4MsgId, 
                    UINT1 bEncrFlag);
INT4 IkeFreeSessionInfo (t_SLL_NODE *);
tIkeSessionInfo *IkeGetSessionFromCookie(tIkeEngine *, tIkeIpAddr *,
                                         UINT2 u2RemotePort, UINT2 u2LocalPort,
                                         tIsakmpHdr *);
INT4 IkeGetIDFromPacket (UINT1*, UINT1 *, UINT1 *);
tIkeSessionInfo * IkeGetPhase2SessionFromIkeSA(tIkeEngine *pIkeEngine, tIkeSA *pIkeSA);
INT1 IkeDelAllSessSAInEngine(tIkeEngine *pIkeEngine);
INT1 IkeDelAllSessSAInEngineWithPeer (tIkeEngine * pIkeEngine, 
                                      tIkePhase1ID * pPeerId);
INT1 IkeDelSessSAForPeer(tIkeEngine *pIkeEngine, tIkeIpAddr *pPeerIpAddr);
INT1 IkeDelPhase2SessSAForPeer(tIkeEngine *pIkeEngine, tIkeIpAddr *pPeerIpAddr);
INT1 IkeDelAllPhase2SessSA (tIkeEngine *pIkeEngine);
INT1 IkeDelPhase2Sa (tIkeIpAddr *pLocAddr,tIkeIpAddr *pRemAddr, UINT1 u1IkeVer);
INT1 IkeDelPhase2SaForLocal (tIkeIpAddr *pLocAddr);

tIkeSessionInfo    * IkeTransSessInit (tIkeSA * pIkeSA, UINT4 u4MsgId, 
        UINT1 bEncrFlag, UINT1 u1Role);
tIkeSessionInfo    * StartTransactionExchange (tIkeSA *pIkeSA, UINT1 u1Role);
INT1 IkeDelPhase1Session (tIkeSessionInfo *pSessionInfo);

/* Ike network management exporting functions */
INT1 IkeSnmpSendPolicyInfoToIke (UINT4 u4IkeEngineIndex, tIkePolicy *pIkePolicy,
                                 UINT4 u4Msg);
INT1 IkeSnmpSendTransformInfoToIke (UINT4 u4IkeEngineIndex, UINT4 u4Msg);
INT1 IkeSnmpSendDelTransformInfoToIke (tIkeConfCryptoMap IkeConfCryptoMap);
INT1 IkeSnmpSendCryptoMapInfoToIke (UINT4 u4IkeEngineIndex, 
        tIkeCryptoMap *pIkeCryptoMap, UINT4 u4Msg);

INT1 IkeSnmpSendRAInfoToIke (UINT4 u4IkeEngineIndex,
                             tIkeRemoteAccessInfo * pIkeRAInfo, UINT4 u4Msg);
INT1 IkeSnmpSendProtectNetInfoToIke (UINT4 u4IkeEngineIndex,
                                     tIkeRAProtectNet * pIkeRAInfo, UINT4 u4Msg);
VOID IkeDeleteRAInfo(tIkeEngine *pIkeEngine, tIkeRemoteAccessInfo *pIkeRAInfo);
VOID IkeDeleteProtectNet (tIkeEngine *pIkeEngine, 
            tIkeRAProtectNet *pIkeRAProtectNet);

VOID IkeFreeCertMapInfo (t_SLL_NODE * pNode);
VOID IkeFreeRsaKeyInfo (t_SLL_NODE *pNode);
VOID IkeFreeDsaKeyInfo (t_SLL_NODE *pNode);
INT1 IkeNmSendCertInfoToIke (UINT4 u4EngineIndex, UINT1 *pu1CertId, UINT4 u4Msg);
INT1 IkeNmSendMapCertInfoToIke (UINT4 u4EngineIndex, UINT1 *pu1CertId,
        tIkePhase1ID *pPhase1ID, UINT4 u4Msg);
INT1 IkeNmSendCertDeleteReqToIke (UINT4 u4EngineIndex, tIkeIpAddr *pPeerAddr, 
        UINT4 u4Msg);
INT1 IkeNmSendDelPeerCertReqToIke (UINT4 u4EngineIndex, tIkePhase1ID *pu1Phase1ID,
                             UINT1 u1Flag, UINT4 u4Msg);
INT1 IkeNmSendDelCaCertInfoToIke (UINT4 u4EngineIndex, UINT1 *pu1CertId, UINT4 u4Msg);

INT1 IkeValidatePeerCert (tIkeSessionInfo * pSesInfo, tIkeX509 * pX509Cert);

INT1 IkeUpdatePolicyPeerId (UINT1 *pu1EngineName, UINT1 *pu1PeerId, 
                       INT4 i4PeerIdType, UINT4 u4Priority);

INT1 IkeUpdatePolicyLocalId PROTO ((UINT1 *pu1EngineName, UINT1 *pu1LocalId,
                       INT4 i4LocalIdType, UINT4 u4Priority));

INT1 IkeUpdatePeerIPInPolicy PROTO ((tSNMP_OCTET_STRING_TYPE *pIkeEngineName, 
                                 UINT4 u4IkePolicyPriority,
                                 tSNMP_OCTET_STRING_TYPE *pPeerAddrOctetStr));
VOID IkeHandleCoreFailure PROTO ((tIkeSessionInfo *pSessionInfo));

VOID IkeHandleCoreSuccess PROTO ((tIkeSessionInfo *pSessionInfo));

INT1 IkeSendSecv4SADeleteMsgToIke (tIkeVpnPolicy * pIkeVpnPolicy);

VOID IkeInitVpnPolicy (VOID);
VOID IkeDeInitVpnPolicy (VOID);
VOID IkeInitDefaultVpnPolicy (tIkeVpnPolicy * pIkeVpnPolicy);
VOID IkeDeleteVpnPolicy (t_SLL_NODE * pNode);
VOID IkeFreeVpnPolicyInfo (t_SLL_NODE * pNode);
tIkeVpnPolicy *IkeSnmpGetVpnPolicy (UINT1 *pVpnPolicyName, INT4 NameLen);
INT1 IkeCheckVpnPolicyAttributes (tIkeVpnPolicy * pIkeVpnPolicy);
INT1 IkeSnmpSetVpnPolicyKeyMode (tIkeVpnPolicy *pIkeVpnPolicy,
                                 UINT4 u4IpsecKeyMode, UINT1 *pu1KeyString);
INT1 IkeSnmpSetVpnPolicyRemotePeer (tIkeVpnPolicy *pIkeVpnPolicy, 
                                    UINT4 u4PeerIdType, UINT1 *pu1PeerIdVal, 
                                    UINT1 *pu1RemTunnTermAddr);
INT1 IkeSnmpSetVpnPolicyIkeProposal (tIkeVpnPolicy *pIkeVpnPolicy, 
                                     tIkePolicy  *pIkeProposal);
INT1 IkeSnmpSetVpnPolicyIpsecProposal (tIkeVpnPolicy *pIkeVpnPolicy, 
                                       tIkePolicy *pIpsecProposal);
INT1 IkeSnmpSetVpnPolicyNetworks (tIkeVpnPolicy *pIkeVpnPolicy, 
                                  UINT1 *pLocalNetwork, UINT1 *pRemoteNetwork);


/* ------------IKEv2 Related Prototypes---------- */
/* ---------- IKEv2 Init  module Prototypes ------ */
INT1 Ike2InitMemPoolInit PROTO ((VOID));
VOID Ike2InitMemPoolDeInit PROTO ((VOID));

/*--------------- IKEv2 Process Module prototypes ----- */
INT1 Ike2ConstructEncryptPayLoad PROTO((tIkeSessionInfo *pSessInfo));

/* -------------- IKEv2 Utility Module Prototypes ---------*/
INT1 IkeSetIkeVersion PROTO((tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT1));
INT1 IkeUpdateAccessPortInfo PROTO((tSNMP_OCTET_STRING_TYPE *,
                           tSNMP_OCTET_STRING_TYPE *, UINT2, UINT2,
                           UINT2, UINT2));
INT1 Ike2UtilIkeSADeleteRequest PROTO((tIkeSA * ));

/* ------- IKEv2 Session Management ------------ */
tIkeSessionInfo  *
Ike2SessInitExchSessInit PROTO ((tIkeEngine *pIkeEngine, UINT1 u1Role,
                             tIkeIpAddr * pPeerTEAddr));
tIkeSessionInfo *
Ike2SessAuthExchSessInit PROTO((tIkeSA *pIkeSA, tIkeNewSA *pIPSecReq,
                                UINT4 u4MsgId, UINT1 u1Role));

tIkeSessionInfo *
Ike2SessChildExchSessInit PROTO ((tIkeSA * pIkeSA, tIkeNewSA * pIPSecReq, 
                                  UINT4 u4MsgId, UINT1 u1Role, 
                                  UINT1 u1ChildType));
tIkeSessionInfo  *
Ike2SessInfoExchSessInit PROTO ((tIkeSA * pIkeSA, UINT4 u4MsgId, 
                                 UINT1 u1Role));

/* ------- IKEv2 Main  Module ------------------- */
VOID Ike2MainProcessIkePkt PROTO ((tIkeEngine * pIkeEngine, UINT1 *pu1Packet,
                       UINT2 u2PacketLen, tIkeIpAddr * pPeerIpAddr,
                       UINT2 u2RemotePort, UINT2 u2LocalPort));

/* ------- IKEv2 Core Module ------------------- */
INT1
Ike2CoreProcess PROTO((tIkeSessionInfo *pSessionInfo));

INT1
IkeProcessNatDPayLoad PROTO((tIkeSessionInfo *, UINT1 *, UINT1));

INT1
IkeRsaSign PROTO((UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT4 , UINT1 **ppu1OutBuf,
                      UINT4 *pu4EncrDataLen, tIkePKey * pKey, UINT4 *pu4ErrNo));

/* This function is used to verify the Auth digest sent by the peer. */
INT1
IkeRsaVerify PROTO((UINT1 *pu1InBuf, UINT1 *pu1SigBuf, tIkePKey * pKey,
                    UINT4 u4SignAlgo, UINT4 , UINT4 *pu4ErrNo));



/* -------------End of IKEv2 Prototypes  -----------------*/

INT1         IkeEngineInit (UINT1 *pu1InterfaceName);
VOID IkeSocketInit (VOID);
VOID
IkeGetActiveSessions PROTO((INT4 ,UINT4 *));
VOID
IkeGetNoOfSessionsSucc PROTO((INT4 ,UINT4 *));
VOID
IkeGetNoOfPhase1SessionsSucc PROTO((INT4 , UINT4 *));
VOID
IkeGetNoOfPhase1SessionFailed PROTO((INT4 , UINT4 *)); 
VOID
IkeGetNoOfIkePhase1Rekeys PROTO((INT4 , UINT4 *));
VOID
IkeGetNoOfIkePhase2Rekeys PROTO((INT4 , UINT4 *));
VOID
IkeGetActiveSACount PROTO((UINT4 *));
VOID
IkeGetNoOfSessionsFail (INT4 ,UINT4 *);

INT1
VpnUtilFillIkeParams ARG_LIST ((tVpnPolicy * pVpnPolicy));
VOID
VpnUtilDelIkeParams ARG_LIST ((tVpnPolicy * pVpnPolicy));
INT1
VpnUtilDeleteKeyDB ARG_LIST ((tVpnIdInfo *pVpnIdInfo)); 

INT4 
VpnIkeCreateAndUpdatePolicyDB  ARG_LIST ((tVpnPolicy * pVpnPolicy));
INT1
VpnIkeCreateAndUpdateKeyDB ARG_LIST ((tVpnIdInfo *pVpnIdInfo, UINT1 *pu1IkeEngineName));
INT4
VpnIkeCreateAndUpdtTrnsfmSetDB ARG_LIST ((tVpnPolicy * pVpnPolicy));
INT4
VpnIkeCreateAndUpdateCryptoMapDB ARG_LIST ((tVpnPolicy * pVpnPolicy));
INT4
VpnIkeCreateAndUpdateRaInfoDB ARG_LIST ((tVpnPolicy *pVpnPolicy));
INT1
VpnUtilIkeUpdateRaUserDetails ARG_LIST ((tVpnRaUserInfo *pVpnRaUserInfoNode, 
                                         INT4 i4AddDelFlag));
INT4
VpnIkeCreateAndUpdateProtNetDB ARG_LIST ((tVpnPolicy * pVpnPolicy, 
                                          tSNMP_OCTET_STRING_TYPE *ProcNetAddrOctStr));

INT1
nmhGetFsVpnIkePhase1PeerIdValue ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVpnIkePhase1LocalIdValue ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

tVpnRaAddressPool  *
VpnGetRaAddrPool ARG_LIST ((CONST UINT1 *pRaAddrPoolName, INT4 i4NameLen));

/*IKE_IPSEC inteaction related API's*/
INT1 IkeHandleMsgToSecv4 ARG_LIST ((tIkeIPSecQMsg *IpSecMsg));

INT1
IkeFreeCertReq PROTO((t_SLL_NODE * pNode));

/*Certificates related function prototypes - Start */
INT4
IkeVpnGenKeyPair ARG_LIST((UINT1*, UINT1*, UINT4, UINT4));

INT4
IkeVpnImportKey ARG_LIST((UINT1*, UINT1*, UINT1*, UINT4));

INT4
IkeVpnShowKeys ARG_LIST((UINT1*, UINT4));

INT4
IkeVpnDelKeys ARG_LIST((UINT1*, UINT1*, UINT4));

INT4
IkeVpnGenCertReq ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*, UINT4));

INT4
IkeVpnImportCert ARG_LIST((UINT1*, UINT1*, UINT4, UINT1*));

INT4
IkeVpnImportPeerCert ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1, UINT1));

INT4
IkeVpnImportCACert ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1));

INT4
IkeVpnShowCerts ARG_LIST((UINT1*, UINT4, UINT1*));

INT4
IkeVpnDelCerts ARG_LIST((UINT1*, UINT4, UINT1*));

INT4
IkeVpnCertMapPeer ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*));

INT4
IkeVpnDelCertMap ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*));

INT4
IkeVpnSaveCerts ARG_LIST((UINT1 *));

INT4
IkeCallBackForNVRAMStorage (UINT4 );

/*Certificates related function prototypes - End */
#endif /* _IKEGEN_H_ */
