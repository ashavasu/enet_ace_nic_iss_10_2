/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikenm.h,v 1.5 2013/11/12 11:16:39 siva Exp $
 *
 * Description: This has prototypes for functions in ikenm.c
 *
 ***********************************************************************/

/* Function prototypes */
VOID IkeEngineInitialise (VOID);
VOID IkeEngineDeInit (VOID);
VOID IkeFreePolicyInfo (t_SLL_NODE *pNode);
VOID IkeFreeKeyInfo (t_SLL_NODE *pNode);
VOID IkeFreeTransformSetInfo (t_SLL_NODE *pNode);
VOID IkeFreeCryptoMapInfo (t_SLL_NODE *pNode);
INT4 IkeFreeSAInfo (t_SLL_NODE * pNode);
VOID IkeDeleteEngine (t_SLL_NODE *pNode);
VOID IkeInitEngine (tIkeEngine *pIkeEngine);
tIkeEngine *IkeSnmpGetEngine ( CONST UINT1 *pEngineName, INT4 NameLen );
INT1 IkeCheckEngineAttributes ( tIkeEngine *pIkeEngine );

VOID IkeDeletePolicy (tIkeEngine *pIkeEngine, tIkePolicy *pIkePolicy);
VOID IkeInitPolicy (tIkePolicy *pIkePolicy);
UINT4 IkeGetPhase2LifeTime (tIkeCryptoMap *pIkeCryptoMap);
UINT4 IkeGetPhase1LifeTime (tIkePolicy *pIkePolicy);
tIkePolicy *IkeSnmpGetPolicy ( tIkeEngine *pIkeEngine, UINT4 u4FsIkePolicyPriority );
tIkePolicy *IkeSnmpGetPolicyFromName PROTO ((tIkeEngine * pIkeEngine, UINT1 *pIkePolicyName, INT4 NameLen));

VOID IkeAddToPolicyList ( tIkeEngine *pIkeEngine, tIkePolicy *pIkePolicy );

VOID IkeDeleteKey (tIkeEngine *pIkeEngine, tIkeKey *pIkeKey );
tIkeKey *IkeSnmpGetKey ( tIkeEngine *pIkeEngine, UINT1 *pKeyID, INT4 KeyLen );
INT1 IkeCheckKeyAttributes ( tIkeKey *pIkeKey );

VOID IkeDeleteTransformSet (tIkeEngine *pIkeEngine, 
        tIkeTransformSet *pIkeTransformSet);
tIkeTransformSet *IkeSnmpGetTransformSet ( tIkeEngine *pIkeEngine, UINT1 *pTSName, INT4 NameLen );
INT1 IkeCheckTransformSetAttributes ( tIkeTransformSet *pIkeTransformSet );

VOID IkeInitCryptoMap ( tIkeCryptoMap *pIkeCryptoMap );
VOID IkeDeleteCryptoMap (tIkeEngine *pIkeEngine, tIkeCryptoMap *pIkeCryptoMap);
tIkeCryptoMap *IkeSnmpGetCryptoMap ( tIkeEngine *pIkeEngine, UINT1 *pCryptoMapName, INT4 NameLen );
tIkeRemoteAccessInfo      *
IkeSnmpGetRAInfo (tIkeEngine * pIkeEngine, UINT1 *pRAInfoName,
                  INT4 NameLen);
tIkeRAProtectNet   *
IkeSnmpGetProtectNet (tIkeEngine * pIkeEngine, UINT1 *pProtectNetName, INT4 NameLen);

INT1 IkeCheckCryptoMapAttributes ( tIkeCryptoMap *pIkeCryptoMap );
INT1 IkeCheckRAInfoAttributes ( tIkeRemoteAccessInfo *pIkeRAInfo );
INT1 IkeCheckProtectNetAttributes ( tIkeRAProtectNet *pIkeRAProtectNet );
INT4 IkeCryptoSetNetworkAddress ( tIkeNetwork *pIkeNetwork, UINT1 *pu1Addr, INT4 i4AddrLen );
INT4 IkeProtectNetSetNetworkAddress (tIkeNetwork * pIkeNetwork, UINT1 *pu1Addr,
                                     INT4 i4AddrLen);
INT1 IkeCryptoSetTransformSetBundle ( tIkeEngine *pIkeEngine, tIkeCryptoMap *pIkeCryptoMap, UINT1 *pu1TSBundle, INT4 i4BundleLen );
INT1
IkeRAInfoSetProtectNetBundle (tIkeEngine * pIkeEngine,
                              tIkeRemoteAccessInfo * pIkeRAInfo,
                              UINT1 *pu1ProtectNetBundle,
                              INT4 i4BundleLen);
INT1
IkeRAInfoSetTransformSetBundle  (tIkeEngine * pIkeEngine,
                              tIkeRemoteAccessInfo * pIkeRAInfo,
                              UINT1 *pu1ProtectNetBundle,
                              INT4 i4BundleLen);

UINT1 IkeParseAddrString ( UINT1 *pAddrString );
UINT1
IkeParseNetworkAddr (UINT1 *pu1AddrString, INT4 i4Len);
VOID IkeGetIpAddrFromString(UINT1 *pPassAddr, tIkeIpAddr *pIpAddr);
INT1
IkeGetPolicyPeerIdStatus (UINT1 *pEngineName, INT4 NameLen, 
                                 UINT4 u4FsIkePolicyPriority);
VOID IkeDeleteKeyBasedOnPeerID PROTO ((tIkeEngine * pIkeEngine,tIkePhase1ID *pPeerID));

#define IKE_MIN_KEYLEN 8
#define IKE_MAX_IPV4_PREFIX 32
#define IKE_MAX_IPV6_PREFIX 128
#define IKE_KEY_DUMMY_STRING "********"



#define IKE_CONVERT_LIFETIME_TO_SECS(LifeTimeType, Lifetime)     \
{                                                                \
    switch (LifeTimeType)                                        \
    {                                                            \
        case (FSIKE_LIFETIME_SECS):                              \
            break;                                               \
        case (FSIKE_LIFETIME_MINS):                              \
            Lifetime *= FSIKE_SECS_PER_MIN;                      \
            break;                                               \
        case (FSIKE_LIFETIME_HRS):                               \
            Lifetime *= FSIKE_SECS_PER_HOUR;                     \
            break;                                               \
        case (FSIKE_LIFETIME_DAYS):                              \
            Lifetime *= FSIKE_SECS_PER_DAY;                      \
            break;       \
 default:         \
     break;                                               \
    }                                                            \
}
