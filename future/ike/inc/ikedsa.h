/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikedsa.h,v 1.2 2011/06/03 06:00:59 siva Exp $
 *
 * Description:This file contains the #define constants and data structure
 *             used by the dsa module
 *
 ********************************************************************/
#ifndef _IKEDSAWRAP_H_
#define _IKEDSAWRAP_H_

#include <openssl/x509v3.h>

/*Following are the wrapper functions which uses openssl calls relating to key 
  generation,storage and retrieval and also for generating the certificate
  request*/

/*For generating the key pair*/ 
INT1 IkeDsaGenKeyPair(UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                      UINT2 u2NBits, UINT4 *pu4ErrNo);

/*For Importing Key pair*/ 
INT1 IkeImportDsaKey(UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                     UINT1 *pu1FileName, UINT4 *pu4ErrNo);

/*Check if key id already exist*/
INT1 IkeDsaVerifyKeyId(UINT1 *pu1EngineId, UINT1 *pu1KeyId);

/*get key entry from the database*/
tIkeDsaKeyInfo *IkeGetDsaKeyEntry(UINT1 *pu1EngineId, UINT1 *pu1KeyId);

/*Add key entry in the database*/
INT1 IkeAddDsaKeyEntryToDB (UINT1 * pu1EngineId, UINT1 *pu1KeyId,
                            tIkeDSA * pIkeDSA);

INT1 
IkeDsaGetKeyLength(tIkeEngine *pIkeEngine, tIkeCertInfo * pCertInfo,
                   UINT4 *u4KeySize);

INT1
IkeDsaSign (UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **ppu1OutBuf,
            UINT4 *pu4EncrDataLen, tIkePKey * pKey, UINT4 *pu4ErrNo);

INT1
IkeDsaVerify (UINT1 *pu1InBuf, UINT1 *pu1SigBuf, tIkePKey * pKey,
              UINT4 *pu4ErrNo);

INT1
IkeDeleteDsaCertFromDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                        UINT4 *pu4ErrNo, tIkeX509Name ** ppCAName,
                        tIkeASN1Integer ** ppCASerialNum,
                        UINT1 *pu1ReturnInfo);

tIkeDsaKeyInfo     *
IkeGetDSAKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyName);

tIkeDsaKeyInfo     *
IkeGetDSAKeyEntryByEngine (tIkeEngine * pIkeEngine, UINT1 *pu1KeyName);

tIkeDsaKeyInfo     *
IkeGetDSAKeyWithCertInfo (tIkeEngine * pIkeEngine, tIkeCertInfo * pCertInfo);

PUBLIC INT4 DsaCbitForSignAndVerify(VOID *);

#endif /* _IKEDSAWRAP_H_ */
