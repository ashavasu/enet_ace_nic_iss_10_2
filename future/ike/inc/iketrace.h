/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iketrace.h,v 1.4 2014/04/23 12:21:40 siva Exp $
 *
 * Description: Provides general purpose macros for tracing
 *
 ***********************************************************************/

#ifndef _IKETRACE_H_
#define _IKETRACE_H_

#define INIT_SHUT_TRC                   0x00000001
#define MGMT_TRC                        0x00000002
#define DATA_PATH_TRC                   0x00000004
#define CNTRL_PLANE_TRC                 0x00000008
#define DUMP_TRC                        0x00000010
#define OS_RESOURCE_TRC                 0x00000020
#define ALL_FAILURE_TRC                 0x00000040
#define BUFFER_TRC                      0x00000080
#define IKE_ENABLE_ALL_TRC              0x0000ffff
#define IKE_DISABLE_ALL_TRC             0x00000000


#ifdef TRACE_WANTED
#define IKE_MOD_TRC  UtlTrcLog
#define IKE_MOD_TRC1  UtlTrcLog
#define IKE_MOD_TRC2  UtlTrcLog
#define IKE_MOD_TRC3  UtlTrcLog
#else
#define IKE_MOD_TRC(u4DbgVar,mask, moduleName,fmt)
#define IKE_MOD_TRC1(u4DbgVar,mask, moduleName,fmt,Arg1)
#define IKE_MOD_TRC2(u4DbgVar,mask, moduleName,fmt,Arg1,Arg2)
#define IKE_MOD_TRC3(u4DbgVar,mask, moduleName,fmt,Arg1,Arg2,Arg3)
#endif
 

#endif

