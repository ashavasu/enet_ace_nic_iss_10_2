/*******************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:ikestat.h 
*
* Description:This file contains the get and set macros for all the
*             elements in the configuration and statistics table.
*
*******************************************************************/
#ifndef _IKESTAT_H_
#define _IKESTAT_H_

#define IKE_MAX_STATISTICS_ENTRY       100

/* Statistics Table GET Macros */
#define GetIkeIpPeerAddr(x)       gpIkeStatisticsInfo[x].IpPeerAddr
#define GetIkeInitSessions(x)     gpIkeStatisticsInfo[x].u4IkeInitSessions
#define GetIkeAcceptSessions(x)   gpIkeStatisticsInfo[x].u4IkeAcceptSessions
#define GetIkeActiveSessions(x)   gpIkeStatisticsInfo[x].u4IkeActiveSessions
#define GetIkeInPkts(x)           gpIkeStatisticsInfo[x].u4IkeInPkts
#define GetIkeOutPkts(x)          gpIkeStatisticsInfo[x].u4IkeOutPkts
#define GetIkeDropPkts(x)         gpIkeStatisticsInfo[x].u4IkeDropPkts
#define GetIkeNoOfNotifySent(x)   gpIkeStatisticsInfo[x].u4IkeNoOfNotifySent
#define GetIkeNoOfNotifyRecv(x)   gpIkeStatisticsInfo[x].u4IkeNoOfNotifyReceive


/* proto types */
INT1 IkeTableInitialise(VOID);
INT4 IkeGetIndexFromStatisticsTable(tIkeIpAddr *, UINT1);
#endif
