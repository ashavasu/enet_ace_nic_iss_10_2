/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: iketmr.h,v 1.3 2010/11/02 09:38:38 prabuc Exp $
*
* Description:This file contains the macros, datastructures and
*             function prototypes required for IKE Timer Routines.
*
*******************************************************************/

#ifndef _IKETMR_H_
#define _IKETMR_H_

typedef enum { 
    IKE_MIN_TIMER_ID      =   0,
    IKE_RETRANS_TIMER_ID = 1,
    IKE_REKEY_TIMER_ID = 2,
    IKE_DELETESA_TIMER_ID = 3,
    IKE_DELETE_SESSION_TIMER_ID = 4,
    IKE_NAT_KEEP_ALIVE_TIMER_ID = 5,
    IKE_MAX_TIMER_ID = 6
} tIkeTimerId;

typedef struct IKETIMER {
    tTmrAppTimer Node;
    FS_ULONG u4Context;
    UINT1    u1TimerId;
    UINT1    u1Status;
    UINT1    u1IkeVer;
    UINT1    u1Pad;
} tIkeTimer;

#define IKE_TIMER_ACTIVE     1
#define IKE_TIMER_INACTIVE   0

#define IKE_INIT_TIMER(pIkeTimer)      \
    ((pIkeTimer)->u1Status = IKE_TIMER_INACTIVE)
#define IKE_ACTIVATE_TIMER(pIkeTimer)      \
    ((pIkeTimer)->u1Status = IKE_TIMER_ACTIVE)
#define IKE_IS_TIMER_ACTIVE(pIkeTimer) \
    (((pIkeTimer)->u1Status == IKE_TIMER_INACTIVE)?FALSE:TRUE)

#define IKE_NAT_KEEP_ALIVE_MSG              0xFF
#define IKE_NAT_KEEP_ALIVE_TIMER_INTERVAL     20

VOID IkeProcessTimer (VOID);
INT4 IkeStartTimer (tIkeTimer *pIkeTimer, tIkeTimerId TimerId, 
        UINT4 u4TimeOut, VOID *pTmrContext);
INT4 IkeStopTimer (tIkeTimer *pIkeTimer);
VOID IkeProcessRetransmissionTimer (VOID *pTmrContext);
VOID IkeProcessReKeyTimer (VOID *pTmrContext);
VOID IkeProcessDeleteSATimer (VOID *pTmrContext);
VOID IkeProcessDeleteSessionTimer (VOID *pTmrContext);
VOID IkeProcessNatKeepAliveTimer (VOID *pTmrContext);

#endif
