/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikeprocess.h,v 1.3 2010/11/02 09:38:00 prabuc Exp $
 *
 * Description:This file contains the #define constants and data structure
 *             used by the process module
 *
 ********************************************************************/
#ifndef _IKEPROCESS_H_
#define _IKEPROCESS_H_

#define  IKE_GENERATOR_2   2

#define  IKE_MIN_NOTIFY_TYPE           1 
#define  IKE_INVALID_PAYLOAD           1      
#define  IKE_DOI_NOT_SUPPORTED         2  
#define  IKE_SITUATION_NOT_SUPPORTED   3  
#define  IKE_INVALID_COOKIE            4  
#define  IKE_INVALID_MAJOR_VERSION     5  
#define  IKE_INVALID_MINOR_VERSION     6  
#define  IKE_INVALID_EXCHANGE_TYPE     7  
#define  IKE_INVALID_FLAGS             8  
#define  IKE_INVALID_MESSAGE_ID        9  
#define  IKE_INVALID_PROTOCOL_ID       10 
#define  IKE_INVALID_SPI               11 
#define  IKE_INVALID_TRANSFORM_ID      12 
#define  IKE_ATTRIBUTES_NOT_SUPPORTED  13 
#define  IKE_NO_PROPOSAL_CHOSEN        14 
#define  IKE_PROPOSAL_BAD_SYNTAX       15 
#define  IKE_PAYLOAD_MALFORMED         16 
#define  IKE_INVALID_KEY_INFO          17 
#define  IKE_INVALID_ID_INFO           18 
#define  IKE_INVALID_CERT_ENCODING     19 
#define  IKE_INVALID_CERTIFICATE       20 
#define  IKE_CERT_TYPE_UNSUPPORTED     21 
#define  IKE_INVALID_CERT_AUTHORITY    22 
#define  IKE_INVALID_HASH_INFO         23 
#define  IKE_AUTH_FAILED               24 
#define  IKE_INVALID_SIGNATURE         25 
#define  IKE_ADDRESS_NOTIFICATION      26 
#define  IKE_NOTIFY_SA_LIFETIME        27
#define  IKE_UNEQUAL_PAYLOAD_LEN       30
#define  IKE_MAX_NOTIFY_TYPE           30 

#define IKE_INVALID_PROPOSAL_NUMBER    0xff

/* Values defined under RFC:2409 */
#define IKE_MIN_NONCE_LENGTH           8
#define IKE_MAX_NONCE_LENGTH           256

INT1  IkeProcessHash (tIkeSessionInfo *,UINT1 *);

INT1  IkeValidTransform(tIkeSessionInfo *, UINT2,UINT1 **);
INT1  IkeValidSa (tIkeSessionInfo *, UINT4 *,UINT4,UINT1 *);
INT1  IkeValidPayLoad (tIkeSessionInfo *, UINT4 *,UINT4,UINT1 *,UINT1 *);
INT1  IkeParsePhase2Proposal(UINT1 *, tIkeSessionInfo *, UINT2);
INT1  IkePhase2CheckProposal(tIkeSessionInfo *, tIkeTrans *, tIkeTrans *,
                       tIkeProposal *, tIkeProposal *);
INT1  IkePhase2SelectProposal(tIkeSessionInfo *);
INT1  IkeSetPhase2ResponseProposal(tIkeSessionInfo *, tIkeTrans *, tIkeTrans *,
                         tIkeProposal *, tIkeProposal *, BOOLEAN);
VOID  IkeSetIPsecBundle(tIkeSessionInfo *,tIkeTrans *, tIkeTrans *, UINT4 ,
                       UINT4, BOOLEAN);
UINT1 *IkeCopyProposal(tProposalPayLoad *pProp, tTransPayLoad *pTrans, UINT4
                       u4Spi, tIkeTrans *pIkeTrans, UINT1 *pu1TempPtr);
UINT4 IkeGetInSpi(UINT1, tIkeSessionInfo *);
INT1  IkePhase1CheckTransAtts(tIkeSessionInfo *,UINT1 *,UINT2);
INT4  IkeGetAtts(UINT1 *,UINT4 *,UINT2 *,UINT2 *,UINT1 *);
INT1  IkePhase1CheckAtts(tIkeSessionInfo *,UINT4,UINT2, UINT2,UINT1 *,UINT2);
INT1  IkePhase2CheckAtts(UINT1,tIkeSessionInfo *,UINT4,UINT2,UINT2,UINT1 *,
        UINT2, UINT1,UINT1);
INT1  IkePhase2CheckTransAtts(UINT1 ,tIkeSessionInfo *,UINT1 *,UINT2, UINT1, UINT1);
INT1  IkePhase1CheckProposal(tIkeSessionInfo *,UINT1 *,UINT1 **,UINT2 *, UINT2);
INT1  IkeVerifyQmHash(tIkeSessionInfo *,tIsakmpHdr *,UINT1 **,UINT4);
INT1 IkeSendInvalidSpiToIPSec (tIkeSessionInfo *pSesInfo, UINT1 *pu1PayLoad, 
        tNotifyPayLoad *pNotifyPayLoad);

INT1  IkeCheckCMCfgReqOrAckAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                       UINT4 u4Len);
INT1  IkeCheckCMCfgRepOrSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                       UINT4 u4Len);
INT1  IkeCheckXAuthCfgReqAttributes  (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                      UINT4 u4Len);
INT1  IkeCheckXAuthCfgRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                     UINT4 u4Len);
INT1  IkeCheckXAuthCfgSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                     UINT4 u4Len);
INT1
IkeCheckTransExchAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                             UINT4 u4Len,UINT1 u1PktType);
INT1 IkeCMCfgRepOrSetCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                                UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                                UINT2 u2Len);
INT1 IkeXAuthCfgReqCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                              UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                              UINT2 u2Len);
INT1 IkeXAuthCfgRepCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                              UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                              UINT2 u2Len);
INT1 IkeXAuthCfgSetCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                              UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                              UINT2 u2Len);
INT1 IkeTransExchCheckAtts (tIkeSessionInfo * pSessionInfo, UINT2 u2Attr,
                            UINT1 u1PktType);
#endif
