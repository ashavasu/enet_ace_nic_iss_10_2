/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikeextern.h,v 1.11 2013/12/12 11:45:09 siva Exp $
 *
 * Description: This has exported function prototypes 
 *
 ***********************************************************************/
#ifndef __IKEEXTERN_H__
#define __IKEEXTERN_H__

extern BOOLEAN      gbXAuthServer;
extern UINT4        gaIp4PrefixToMask[];

extern UINT4 gau4TransP1HashAlgo[];
extern UINT4 gau4TransP2HashAlgo[];
extern INT4  gi4IkeSysLogId;

extern UINT4 CfaValidateIpAddress PROTO ((UINT4 u4IpAddr));
extern INT4 VpnUtilGetFreeAddrFromPool PROTO ((tVpnIpAddr *pPeerAddr,tVpnIpAddr *pu4IntAddress,
                                        UINT4 *pu4IntNetmask));
extern INT4 VpnUtilDelAddrFromAllocList PROTO ((tVpnIpAddr  *pIpaddr));

#endif /* __IKEEXTERN_H__ */
