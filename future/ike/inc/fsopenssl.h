/*******************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsopenssl.h,v 1.3 2010/11/02 09:38:16 prabuc Exp $ 
*
* Description:This file contains the prototypes for interface functions
*             with OpenSSL                                         
*
*******************************************************************/

#ifndef _FSOPENSSL_H
#define _FSOPENSSL_H


#include "ikedhgroup.h"
#include "openssl/bn.h"
#include "openssl/dh.h"

#define FS_SSL_ERROR             0
#define FS_SSL_INVALID_KEY_LEN  -1

VOID FSOpenSslInit (VOID);
INT4 FSOpenSslDhGenerateKeyPair(tDhGroupInfo *pDhInfo, tHexNum *pub, tHexNum *prv);
INT4 FSOpenSslDhCompute(tDhGroupInfo *pDhInfo, tHexNum *pPub1, tHexNum *pPrv1, 
        tHexNum *pPub2, tHexNum *pKey);

#endif /* _FSOPENSSL_H */
