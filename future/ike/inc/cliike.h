/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id:cliike.h
 *
 * Description: This has functions for macros related to cli module
 *
 ***********************************************************************/

#ifndef __IKECLI_H__
#define __IKECLI_H__

#define CLI_IKE_CREATE_NEW_ENGINE                     1
#define CLI_IKE_LOCAL_TUNNEL_TERM_ADDR                2
#define CLI_IKE_SHOW_TUNNEL_TERM_ADDR                 3
#define CLI_IKE_CREATE_NEW_POLICY                     4
#define CLI_IKE_POLICY_ENCRYPTION                     5
#define CLI_IKE_SHOW_POLICY_ENCRYPTION                6
#define CLI_IKE_POLICY_HASH                           7
#define CLI_IKE_SHOW_POLICY_HASH                      8
#define CLI_IKE_POLICY_AUTHMODE                       9
#define CLI_IKE_SHOW_POLICY_AUTHMODE                  10
#define CLI_IKE_POLICY_DH_GRP                         11
#define CLI_IKE_SHOW_POLICY_DH_GRP                    12
#define CLI_IKE_POLICY_MODE                           13
#define CLI_IKE_SHOW_POLICY_MODE                      14
#define CLI_IKE_POLICY_LIFETIME                       15
#define CLI_IKE_SHOW_POLICY_LIFETIME                  16
#define CLI_IKE_SHOW_POLICY                           17
#define CLI_IKE_DEL_POLICY                            18
#define CLI_IKE_CREATE_NEW_CRYPTO_MAP                 19
#define CLI_IKE_CM_PEER_ADDR                          20
#define CLI_IKE_SHOW_CM_PEER_ADDR                     21
#define CLI_IKE_CM_NETWORKS                           22
#define CLI_IKE_CM_TRANSFORMS                         23
#define CLI_IKE_CM_MODE                               24
#define CLI_IKE_CM_PFS                                25
#define CLI_IKE_CM_LIFETIME                           26
#define CLI_IKE_SHOW_CRYPTOMAP                        27
#define CLI_IKE_DEL_CRYPTOMAP                         28
#define CLI_IKE_CREATE_NEW_KEY                        29
#define CLI_IKE_SHOW_KEY                              30
#define CLI_IKE_CREATE_TRANSFORM_SET                  31
#define CLI_IKE_TRANSFORM_ESP                         32
#define CLI_IKE_TRANSFORM_AH                          33
#define CLI_IKE_SHOW_TRANSFORM                        34
#define CLI_IKE_DEL_TRANSFORM_SET                     35
#define CLI_IKE_SHOW_ENGINE                           36
#define CLI_IKE_DEL_KEY                               37
#define CLI_IKE_SHOW_STATS                            38
#define CLI_IKE_SHOW_SAS                              39
#define CLI_IKE_NO_ENGINE                             40
#define CLI_IKE_GEN_RSA_KEYPAIR                       41
#define CLI_IKE_SHOW_RSA_KEYS                         42
#define CLI_IKE_DEL_RSA_KEYPAIR                       43
#define CLI_IKE_GEN_CERT_REQ                          44
#define CLI_IKE_IMPORT_CERT                           45
#define CLI_IKE_SHOW_CERTS                            46
#define CLI_IKE_DEL_CERT                              47 
#define CLI_IKE_IMPORT_PEER_CERT                      48
#define CLI_IKE_SHOW_PEER_CERTS                       49
#define CLI_IKE_DEL_PEER_CERT                         50
#define CLI_IKE_IMPORT_CA_CERT                        51
#define CLI_IKE_SHOW_CA_CERTS                         52
#define CLI_IKE_DEL_CA_CERT                           53
#define CLI_IKE_CERT_MAP_PEER                         54
#define CLI_IKE_SHOW_CERT_MAP                         55
#define CLI_IKE_DEL_CERT_MAP                          56
#define CLI_IKE_SAVE_CERT                             57
#define CLI_IKE_DEBUG                                 58
#define CLI_IKE_TRIGGER                               59
#define CLI_IKE_RA_GLOBAL                             60
#define CLI_IKE_RA_CONF                               61
#define CLI_IKE_RA_PEERID                             62
#define CLI_IKE_RA_XAUTH                              63
#define CLI_IKE_RA_XAUTH_SERVER                       64
#define CLI_IKE_RA_INTERNAL_ADDR                      65  
#define CLI_IKE_RA_XAUTH_CLIENT_TYPE                  66
#define CLI_IKE_RA_PROTECTED_NET                      67
#define CLI_IKE_RA_TRANSFORM_BUNDLE                   68
#define CLI_IKE_RA_BUNDLE                             69 
#define CLI_IKE_RA_MODE                               70
#define CLI_IKE_RA_PFS                                71
#define CLI_IKE_RA_LIFETIME                           72 
#define CLI_IKE_NO_RA_CONF                            73 
#define CLI_IKE_NO_RA_PROTECTED_NET                   74
#define CLI_IKE_RA_SHOW                               75
#define CLI_IKE_PN_SHOW                               76
#define CLI_IKE_RA_PEERIP                             77
#define CLI_IKE_POLICY_PEER_ID                        78 

#define CLI_IKE_VPN_POLICY                            79
#define CLI_IKE_NO_VPN_POLICY                         80
#define CLI_IKE_VPN_KEY_MODE                          81
#define CLI_IKE_VPN_REMOTE_PEER                       82
#define CLI_IKE_VPN_IKE_PROPOSAL                      83
#define CLI_IKE_VPN_IPSEC_PROPOSAL                    84
#define CLI_IKE_VPN_NETWORKS                          85
#define CLI_IKE_VPN_IPSEC_MANUAL_SA                   86
#define CLI_IKE_VPN_XAUTH_PROTECT_NET                 87   
#define CLI_IKE_VPN_XAUTH_ENABLE                      88   
#define CLI_IKE_SHOW_VPN_POLICY                       89

#define CLI_MAX_IKE_COMMANDS                          89




#define CLI_IKE_MAX_CERT_SIZE            IKE_MAX_CERT_SIZE+1024
/* LifeTime (Type) */
#define CLI_IKE_LIFETIME_SECS            "1"
#define CLI_IKE_LIFETIME_KB              "2"

#define CLI_IKE_LIFETIME_MINS            "3"
#define CLI_IKE_LIFETIME_HRS             "4"
#define CLI_IKE_LIFETIME_DAYS            "5"
#define CLI_IKE_NO_LIFETIME              "6"
#define CLI_IKE_NO_LIFETIME_KB           "7"

#define CLI_IKE_SHOW_ALL                 "0"
#define CLI_IKE_SHOW_DYNAMIC             "1"
#define CLI_IKE_DEL_DYNAMIC              "1"
#define CLI_IKE_DEL_ALL                  "2"

#define CLI_IKE_MAP_ALL                  "0"
#define CLI_IKE_MAP_DEFAULT              "default"
#define CLI_IKE_DEL_MAP_ALL              "0"

#define CLI_PEER_CERT_UNTRUSTED          "1"
#define CLI_PEER_CERT_TRUSTED            "2"

#define CLI_RSA_NBITS_512                "512"
#define CLI_RSA_NBITS_1024               "1024"
#define DEF_TUNNEL_TERM_ADDR             "0.0.0.0"


    

#define CLI_IKE_SERVER                   "server"
#define CLI_IKE_CLIENT                   "client"
#define CLI_IKE_ENABLE                   "enable"
#define CLI_IKE_DISABLE                  "disable"
#define CLI_IKE_GENERIC                  "generic"
#define CLI_IKE_IPV4NET                  "ipv4"
#define CLI_IKE_STATIC                   "static"
#define CLI_IKE_TRANSPORT                "transport"
#define CLI_IKE_TUNNEL                   "tunnel"
#define CLI_IKE_DH_GROUP1                "group1"
#define CLI_IKE_DH_GROUP2                "group2"
#define CLI_IKE_DH_GROUP5                "group5"
#define CLI_IKE_TIME_SEC                 "secs"
#define CLI_IKE_TIME_MIN                 "mins"
#define CLI_IKE_TIME_HRS                 "hrs"
#define CLI_IKE_TIME_DAYS                "days"

#define CLI_IKE_KEY_IPV4                 1
#define CLI_IKE_KEY_IPV6                 5
#define CLI_IKE_KEY_EMAIL                3
#define CLI_IKE_KEY_FQDN                 2
#define CLI_IKE_KEY_DN                   9

#define CLI_IKE_VP_KEY_MODE_MANUAL            "0"
#define CLI_IKE_VP_KEY_MODE_PRESHARED         "1"
#define CLI_IKE_VP_KEY_MODE_CERTIFICATE       "3"

/* IKE VPN IPSEC SA PARAMS */
#define CLI_IPSEC_MANUAL_SA_TRANSPORT             2
#define CLI_IPSEC_MANUAL_SA_TUNNEL                1
#define CLI_IPSEC_MANUAL_SA_AH                   51
#define CLI_IPSEC_MANUAL_SA_ESP                  50
#define CLI_IPSEC_MANUAL_SA_NULL                  0
#define CLI_IPSEC_MANUAL_SA_MD5                   4
#define CLI_IPSEC_MANUAL_SA_HMACMD5               1
#define CLI_IPSEC_MANUAL_SA_HMACSHA               2
#define CLI_IPSEC_MANUAL_SA_KEYEDMD5              3
#define CLI_IPSEC_MANUAL_SA_DESCBC                2
#define CLI_IPSEC_MANUAL_SA_3DESCBC               3
#define CLI_IPSEC_MANUAL_SA_AES                   12
#define CLI_IPSEC_MANUAL_SA_ESP_NULL              11

typedef struct CLIIKECreateVpnPolicy {
    UINT1    *pu1VpnPolicyName;
} tCliIkeCreateVpnPolicy;

typedef tCliIkeCreateVpnPolicy tCliIkeDelVpnPolicy;

typedef struct CLIIKEVpnPolicyKeyMode {
    UINT1    *pu1VpnPolicyName;
    UINT1    *pu1KeyString;
    UINT4     u4VpnPolicyKeyMode;
} tCliIkeVpnPolicyKeyMode;

typedef struct CLIIKEVpnPolicyPeerId {
    UINT1    *pu1VpnPolicyName;
    UINT1    *pu1RemTunnTermAddr;
    UINT1    *pu1KeyId;
    UINT1     u1KeyIdType;
    UINT1     au1Pad[3];
} tCliIkeVpnPolicyPeerId;

typedef struct CLIIKEVpnPolicyIkeProposal {
    UINT1    *pu1VpnPolicyName;
    UINT4     u4EncrAlgo;
    UINT4     u4HashAlgo;
    UINT4     u4DHGrp;
    UINT4     u4ExchMode;
    UINT4     u4LifetimeType;
    UINT4     u4Lifetime;
} tCliIkeVpnPolicyIkeProposal;

typedef struct CLIIKEVpnPolicyIPsecProposal {
    UINT1    *pu1VpnPolicyName;
    UINT4     u4EncrAlgo;
    UINT4     u4HashAlgo;
    UINT4     u4PfsDHGrp;
    UINT4     u4IpsecMode;
    UINT4     u4LifetimeType;
    UINT4     u4Lifetime;
} tCliIkeVpnPolicyIpsecProposal;

typedef struct CLIIKEVpnNetworks {
    UINT1    *pu1VpnPolicyName;
    UINT1    *pu1VpnLocAddr;
    UINT1    *pu1VpnRemAddr;
} tCliIkeVpnNetworks;

typedef struct CLIIKEVpnIpsecManualSa {
   UINT1    *pu1VpnPolicyName;
   UINT1    *pu1AhKey; /* hmac-md5/hmac-sha1 */
   UINT1    *pu1EspKey; /* des|aes */
   UINT1    *pu1EspTripleDesKey[3]; /*Triple Des */

   UINT1    u1AesKeyLen;   /* Aes key len 128|192|256 */
   UINT1    u1IPSecProtocol;  /* AH or ESP */
   UINT1    u1HashAlgo;       /* Hash Algo: HMAC-MD5 | HMAC-SHA1 */
   UINT1    u1EncryptionAlgo; /* Encr Algo: DES | 3-DES | AES */

   UINT4    u4InboundSpi;     /* Inbound Security Parameter Index */
   UINT4    u4OutboundSpi;     /* Outbound Security Parameter Index */

   UINT1    u1IpsecMode;      /* tunnel or transport */
   UINT1    au1Pad[3];        /* Pad bytes */

} tCliIkeVpnIpsecManualSa;

typedef struct CLIIKEDebug {
    UINT1    u1Debug;
    UINT1    u1Pad;
    UINT2    u2Pad;
} tCliIkeDebug;


typedef struct CLIIKECreateEngine {
    UINT1    *pu1EngineName;
} tCliIkeCreateEngine;

typedef struct CLIIKETunnTermAddr {
    UINT1    *pu1EngineName;
    UINT1    *pu1Addr;
} tCliIkeTunnTermAddr;

typedef struct CLIIKECreatePolicy {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
} tCliIkeCreatePolicy;

typedef struct CLIIKEPolicyPeerId {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT1    *pu1PeerId;
    UINT1     u1PeerIdType;
    UINT1     u1Pad;
    UINT2     u2Pad;
} tCliIkePolicyPeerId;

typedef struct CLIIKEPolicyEncr {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT4     u4EncrAlgo;
} tCliIkePolicyEncr;

typedef struct CLIIKEPolicyHash {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT4     u4HashAlgo;
} tCliIkePolicyHash;

typedef struct CLIIKEPolicyAuthMode {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT4     u4AuthMode;
} tCliIkePolicyAuthMode;

typedef struct CLIIKEPolicyDHGrp {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT4     u4DHGrp;
} tCliIkePolicyDHGrp;

typedef struct CLIIKEPolicyMode {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT4     u4Mode;
} tCliIkePolicyMode;

typedef struct CLIIKEPolicyLifetime {
    UINT1    *pu1EngineName;
    UINT4     u4Priority;
    UINT1     u1LifeTimeType;
    BOOLEAN   bLifeTimeKB;
    UINT2     u2Pad;
    UINT4     u4LifeTime;
    UINT4     u4LifeTimeKB;
} tCliIkePolicyLifetime;

typedef struct CLIIKECreateCryptoMap {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
} tCliIkeCreateCryptoMap;

typedef struct CLIIKECMPeerAddr {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT1    *pu1PeerAddr;
} tCliIkeCMPeerAddr;

typedef struct CLIIKECMNetworks {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT1    *pu1LocAddr;
    UINT1    *pu1RemAddr;
} tCliIkeCMNetworks;

typedef struct CLIIKECMTransforms {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT1    *pu1TransformSets;
} tCliIkeCMTransforms;

typedef struct CLIIKECMMode {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT4     u4Mode;
    BOOLEAN   bTunnelEspOnly;
    UINT1     u1Pad;
    UINT2     u2Pad;
} tCliIkeCMMode;

typedef struct CLIIKECMPfs {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT4     u4Pfs;
} tCliIkeCMPfs;

typedef struct CLIIKECMLifetime {
    UINT1    *pu1EngineName;
    UINT1    *pu1CryptoMapName;
    UINT1     u1LifeTimeType;
    BOOLEAN   bLifeTimeKB;
    UINT2     u2Pad;
    UINT4     u4LifeTime;
    UINT4     u4LifeTimeKB;
} tCliIkeCMLifeTime;

typedef struct CLIIKECreateKey {
    UINT1    *pu1EngineName;
    UINT1    *pu1KeyId;
    UINT1    *pu1KeyString;
    UINT1     u1KeyIdType;
    UINT1     u1Pad;
    UINT2     u2Pad;
} tCliIkeCreateKey;

/*typedef struct CLIIKEPhase1Id {
    UINT1    *pu1EngineName;
    UINT1    *pu1KeyId;
    UINT1     u1KeyIdType;
    UINT1     u1Pad;
    UINT2     u2Pad;
    UINT4     u4Pad;
} tCliIkePhase1Id;*/

typedef struct CLIIKECreateTransformSet {
    UINT1    *pu1EngineName;
    UINT1    *pu1TransformName;
} tCliIkeCreateTransformSet;

typedef struct CLIIKETransformSetEsp {
    UINT1    *pu1EngineName;
    UINT1    *pu1TransformName;
    UINT1     u1EspEncrAlgo;
    UINT1     u1EspHashAlgo;
    UINT2     u2Pad;
} tCliIkeTransformSetEsp;

typedef struct CLIIKETransformSetAh {
    UINT1    *pu1EngineName;
    UINT1    *pu1TransformName;
    UINT1     u1AhHashAlgo;
    UINT1     u1Pad;
    UINT2     u2Pad;
} tCliIkeTransformSetAh;

typedef struct CLIIKEStat {

    UINT1    *pu1PeerAddr;
}tCliIkeStats;

typedef struct CLIIKEShowSA {

    UINT1    *pu1EngineName;
}tCliIkeEngineId;

typedef struct CLIIKEGenRsaKey {

    UINT1    *pu1EngineName;
    UINT1    *pu1KeyName;
    INT4     i4RsaBits;
}tCliIkeGenRsaKey;

typedef struct CLIIKEGenCertReq {

    UINT1    *pu1EngineName;
    UINT1    *pu1KeyName;
    UINT1    *pu1SubjectName;
    UINT1    *pu1SubjectAltName;
}tCliIkeGenCertReq;

typedef struct CLIIKEImportCert {

    UINT1    *pu1EngineName;
    UINT1    *pu1FileName;
    UINT1    *pu1KeyName;
    UINT1     u1EncodeType;
    UINT1     u1Pad;
    UINT2     u2Pad;
}tCliIkeImportCert;

typedef struct CLIIKEImportPeerCert {

    UINT1    *pu1EngineName;
    UINT1    *pu1FileName;
    UINT1    *pu1CertName;
    UINT1     u1EncodeType;
    UINT1     u1Flag;
    UINT1     u1DelFlag;
    UINT1     u1Pad;
}tCliIkeImportPeerCert;

typedef struct CLIIKEMapCertToPeer {

    UINT1    *pu1EngineName;
    UINT1    *pu1KeyName;
    UINT1    *pu1KeyId;
    UINT1     u1KeyIdType;
    UINT1     u1Pad;
    UINT2     u2Pad;
}tCliIkeMapCertToPeer;

typedef struct {
    UINT1    *pu1EngineName;
    UINT1    *pu1AccessName;
    UINT1    *pu1PeerId;
    UINT1    *pu1Addr;
    UINT1    *pu1Mask;
    UINT1    *pu1UserName;
    UINT1    *pu1UserPasswd;
    UINT4    u4Addr;
    UINT4    u4Time;
    UINT1    u1AddrType;
    UINT1    u1XAuthType;
    UINT1    u1Mode;
    UINT1    u1XAuthMode;
    UINT1    u1CMMode;
    UINT1    u1Pfs;
    UINT1    u1TimeType;
    UINT1    u1Pad;
}tCliIkeRemoteAccess;

typedef struct CLIIKEVpnXauthStatus {
    UINT1    *pu1VpnPolicyName;
    UINT1    *pu1EngineName;
    UINT1    *pu1AccessName;
    UINT1    *pu1PeerId;
    UINT1    *pu1Addr;
    UINT1    *pu1Mask;
    UINT1    *pu1UserName;
    UINT1    *pu1UserPasswd;
    UINT4    u4Addr;
    UINT4    u4Time;
    UINT1    u1AddrType;
    UINT1    u1XAuthType;
    UINT1    u1Mode;
    UINT1    u1XAuthMode;
    UINT1    u1CMMode;
    UINT1    u1Pfs;
    UINT1    u1TimeType;
    UINT1    u1Pad;
} tCliIkeVpnXauthStatus;


typedef tCliIkeImportPeerCert tCliIkeImportCaCert;
typedef tCliIkeEngineId tCliIkeShowSAs;
typedef tCliIkeEngineId tCliIkeSaveCert;

typedef union
{
    tCliIkeCreateVpnPolicy   CliIkeCreateVpnPolicy;
    tCliIkeDelVpnPolicy      CliIkeDelVpnPolicy;
    tCliIkeVpnPolicyKeyMode  CliIkeVpnPolicyKeyMode;
    tCliIkeVpnPolicyPeerId   CliIkeVpnPolicyPeerId;
    tCliIkeVpnPolicyIkeProposal CliIkeVpnPolicyIkeProposal;
    tCliIkeVpnPolicyIpsecProposal CliIkeVpnPolicyIpsecProposal;
    tCliIkeVpnNetworks  CliIkeVpnNetworks;
    tCliIkeVpnIpsecManualSa CliIkeVpnIpsecManualSa;
    tCliIkeCreateEngine     CliIkeCreateEngine;
    tCliIkeTunnTermAddr     CliIkeTunnTermAddr;
    tCliIkeCreatePolicy     CliIkeCreatePolicy;
    tCliIkePolicyPeerId     CliIkePolicyPeerId;  
    tCliIkePolicyEncr       CliIkePolicyEncr;
    tCliIkePolicyHash       CliIkePolicyHash;
    tCliIkePolicyAuthMode   CliIkePolicyAuthMode;
    tCliIkePolicyDHGrp      CliIkePolicyDHGrp;
    tCliIkePolicyMode       CliIkePolicyMode;
    tCliIkePolicyLifetime   CliIkePolicyLifetime;
    tCliIkeCreateCryptoMap  CliIkeCreateCryptoMap;
    tCliIkeCMPeerAddr       CliIkeCMPeerAddr;
    tCliIkeCMNetworks       CliIkeCMNetworks;
    tCliIkeCMTransforms     CliIkeCMTransforms;
    tCliIkeCMMode           CliIkeCMMode;
    tCliIkeCMPfs            CliIkeCMPfs;
    tCliIkeCMLifeTime       CliIkeCMLifeTime;
    tCliIkeCreateKey        CliIkeCreateKey;
 /*   tCliIkePhase1Id         CliIkePhase1Id;    */
    tCliIkeCreateTransformSet CliIkeCreateTransformSet;
    tCliIkeTransformSetEsp    CliIkeTransformSetEsp;
    tCliIkeTransformSetAh     CliIkeTransformSetAh;
    tCliIkeStats              CliIkeStat;
    tCliIkeShowSAs            CliIkeShowSA;
    tCliIkeGenRsaKey          CliIkeGenRsaKey;
    tCliIkeGenCertReq         CliIkeGenCertReq;
    tCliIkeImportCert         CliIkeImportCert;
    tCliIkeImportPeerCert     CliIkeImportPeerCert; 
    tCliIkeImportCaCert       CliIkeImportCaCert; 
    tCliIkeMapCertToPeer      CliIkeMapCertToPeer;
    tCliIkeSaveCert           CliIkeSaveCert;
    tCliIkeDebug              CliIkeDebug;
    tCliIkeRemoteAccess       CliIkeRemoteAccess;
    tCliIkeVpnXauthStatus     CliIkeVpnXauthStatus;
} tIkeCliConfigParams;

INT1 IkeGetVpnPolicyPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 IkeGetTransformSetPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 IkeGetPolicyPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 IkeGetCryptoMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 IkeGetRAPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
#endif /* __IKECLI_H__ */
