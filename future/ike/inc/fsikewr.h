/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsikewr.h,v 1.3 2010/11/02 09:38:18 prabuc Exp $
*
* Description: Function prototypes for IKE snmp module
*********************************************************************/
#ifndef _FSIKEWR_H
#define _FSIKEWR_H

VOID RegisterFSIKE(VOID);

VOID UnRegisterFSIKE(VOID);
INT4 FsikeGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsikeRAXAuthModeGet(tSnmpIndex *, tRetVal *);
INT4 FsikeRACMModeGet(tSnmpIndex *, tRetVal *);
INT4 FsikeGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsikeRAXAuthModeSet(tSnmpIndex *, tRetVal *);
INT4 FsikeRACMModeSet(tSnmpIndex *, tRetVal *);
INT4 FsikeGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsikeRAXAuthModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsikeRACMModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeEngineTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeEngineTunnelTermAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineTunnelTermAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineTunnelTermAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineTunnelTermAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeEngineRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkePolicyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkePolicyAuthMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyHashAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyEncryptionAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyDHGroupGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeKBGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyModeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyAuthMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyHashAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyEncryptionAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyDHGroupSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeKBSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyModeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyAuthMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyHashAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyEncryptionAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyDHGroupTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyLifeTimeKBTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkePolicyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeRAInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeRAInfoPeerIdTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoPeerIdGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRACMEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserNameGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserPasswdGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAIPAddressAllocMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoProtectedNetBundleGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRATransformSetBundleGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAModeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAPfsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoPeerIdTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoPeerIdSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRACMEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserNameSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserPasswdSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAIPAddressAllocMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoProtectedNetBundleSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRATransformSetBundleSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAModeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAPfsSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoPeerIdTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoPeerIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRACMEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAXAuthUserPasswdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAIPAddressAllocMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInternalIPNetMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoProtectedNetBundleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRATransformSetBundleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAPfsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRALifeTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeRAInfoStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeProtectNetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeProtectNetTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeProtectNetStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeKeyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeKeyIdTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStringGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyIdTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStringSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyIdTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeTransformSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeTSEspHashAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSEspEncryptionAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSAhHashAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSEspHashAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSEspEncryptionAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSAhHashAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeTSEspHashAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeTSEspEncryptionAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeTSAhHashAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeTSStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeCryptoMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeCMLocalAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLocalAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMRemAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMRemAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPeerAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPeerAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMTransformSetBundleGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMModeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPfsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeKBGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLocalAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMRemAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPeerAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMTransformSetBundleSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMModeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPfsSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeKBSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLocalAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMRemAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPeerAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMTransformSetBundleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMPfsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMLifeTimeKBTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIkeCMStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIkeStatTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIkeSAsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeInitSessionsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeAcceptSessionsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeActiveSessionsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeInPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeOutPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeDropPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeNoOfNotifySentGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeNoOfNotifyRecvGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeNoOfSessionsSuccGet(tSnmpIndex *, tRetVal *);
INT4 FsIkeNoOfSessionsFailureGet(tSnmpIndex *, tRetVal *);
#endif /* _FSIKEWR_H */
