/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id:authcli.h
 *
 * Description: This has functions prototypes for auth module
 *
 ***********************************************************************/
#ifndef __COMMON_AUTH_H__
#define __COMMON_AUTH_H__

#include "auth_db.h"

/* Opcodes for CLI commands.
 * When a new command is added, add a new definition */
#define AUTH_ADD_USER           1  /* Add user */
#define AUTH_DELETE_USER        2  /* delete user */
#define AUTH_MODIFY_USER        3  /* modify user */
#define AUTH_SHOW_STATUS        4  /* search user */
#define AUTH_CLI_MAX_COMMANDS   4

/* prototypes */

INT4 cli_process_commom_auth_cmd (UINT4 u4Command, ...);

VOID AuthCliCmdActionRoutine  PROTO ((UINT1 *, UINT1 **));
VOID AuthProcessCliMsg        PROTO ((UINT1 *, UINT1 **));

VOID AuthCliAddUser      PROTO ((tUserAuth *, UINT1 **));
VOID AuthCliDeleteUser       PROTO ((tUserAuth *, UINT1 **));
VOID AuthCliModifyUser      PROTO ((tUserAuth *, UINT1 **));
VOID AuthCliShowStatus       PROTO ((tUserAuth *, UINT1 **));

#endif  /* COMMON_AUTH */
