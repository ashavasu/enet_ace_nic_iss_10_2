/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecert.h,v 1.3 2010/11/02 09:38:14 prabuc Exp $
 *
 * Description:This file contains the prototype definitions of functions
 *             in ikecert.c
 *
 ********************************************************************/

#ifndef _IKECERT_H_
#define _IKECERT_H_

VOID IkeFreeCertDBEntry (t_SLL_NODE * pNode);

/* For MY Certs */
INT1 IkeAddCertToMyCerts (UINT1 *pu1EngineId, UINT1 *pu1CertId, 
        tIkeX509 *pCert);
tIkeCertDB *IkeGetMyCertById (UINT1 *pu1EngineId, UINT1 *pu1KeyId);
tIkeCertDB *IkeGetCertFromMyCerts (UINT1 *pu1EngineId, 
        tIkeX509Name *pCAName, tIkeASN1Integer *pCASerialNum);
INT1 IkeDeleteCertInMyCerts (UINT1 *pu1EngineId, tIkeCertDB *pIkeCertEntry);
INT1 IkeDelAllMyCerts (UINT1 *pu1EngineId);

/* For PEER Certs */
INT1 IkeAddCertToPeerCerts (UINT1 *pu1EngineId, UINT1 *pu1CertId, 
        tIkeX509 *pCert, UINT4 u4Flag);
tIkeCertDB *IkeGetPeerCertById (UINT1 *pu1EngineId, UINT1 *pu1CertId);
tIkeCertDB *IkeGetCertFromPeerCerts (UINT1 *pu1EngineId, 
        tIkeX509Name *pCAName, tIkeASN1Integer *pCASerialNum);
tIkeCertDB *IkeGetPeerCertByPhase1Id (UINT4 u4EngineIdx, 
        tIkePhase1ID *pPhase1Id);
INT1 IkeDeleteCertInPeerCerts(UINT1 *pu1EngineId, 
        tIkeCertDB *pIkeCertEntry);
INT1 IkeDelAllPeerCerts (UINT1 *pu1EngineId);
INT1 IkeDelDynamicPeerCerts (UINT1 *pu1EngineId);

/* For CA Certs */
INT1 IkeAddCertToCACerts (UINT1 *pu1EngineId, UINT1 *pu1CertId, 
        tIkeX509 *pCert);
tIkeCertDB *IkeGetCaCertById (UINT1 *pu1EngineId, UINT1 *pu1CertId);
tIkeCertDB *IkeGetCertFromCaCerts (UINT1 *pu1EngineId, 
        tIkeX509Name *pCAName, 
        tIkeASN1Integer *pCASerialNum);
INT1 IkeDeleteCertInCaCerts(UINT1 *pu1EngineId, tIkeCertDB *pIkeCertEntry);
INT1 IkeDelAllCACerts (UINT1 *pu1EngineId);

/* General */
INT1 IkeGetIdFromCert (tIkeX509 *pCert, tIkePhase1ID *pPhase1Id);
INT1 IkeDelPeerCert (UINT1 *pu1EngineId, UINT1 *pu1CertId, UINT1 u1Flag, 
        tIkePhase1ID * pPhase1ID, UINT4 *pu4ErrNo);
/* Save certificates */
INT1 IkeSaveCertConfig (UINT1 *pu1EngineId);

#endif /* _IKECERT_H_ */
