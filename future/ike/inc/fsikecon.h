/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id:fsikecon.h
 *
 * Description: This has macros ike module 
 *
 ***********************************************************************/
# ifndef fsikeOCON_H
# define fsikeOCON_H
/*
 *  The Constant Declarations for
 *  fsikeScalars
 */

# define FSIKEGLOBALDEBUG                                  (1)

/*
 *  The Constant Declarations for
 *  fsIkeEngineTable
 */

# define FSIKEENGINENAME                                   (1)
# define FSIKEENGINETUNNELTERMADDRTYPE                     (2)
# define FSIKEENGINETUNNELTERMADDR                         (3)
# define FSIKEENGINEROWSTATUS                              (4)

/*
 *  The Constant Declarations for
 *  fsIkePolicyTable
 */

# define FSIKEPOLICYPRIORITY                               (1)
# define FSIKEPOLICYAUTHMETHOD                             (2)
# define FSIKEPOLICYHASHALGO                               (3)
# define FSIKEPOLICYENCRYPTIONALGO                         (4)
# define FSIKEPOLICYDHGROUP                                (5)
# define FSIKEPOLICYLIFETIMETYPE                           (6)
# define FSIKEPOLICYLIFETIME                               (7)
# define FSIKEPOLICYLIFETIMEKB                             (8)
# define FSIKEPOLICYMODE                                   (9)
# define FSIKEPOLICYSTATUS                                 (10)

/*
 *  The Constant Declarations for
 *  fsIkeKeyTable
 */

# define FSIKEKEYID                                        (1)
# define FSIKEKEYIDTYPE                                    (2)
# define FSIKEKEYSTRING                                    (3)
# define FSIKEKEYSTATUS                                    (4)

/*
 *  The Constant Declarations for
 *  fsIkeTransformSetTable
 */

# define FSIKETRANSFORMSETNAME                             (1)
# define FSIKETSESPHASHALGO                                (2)
# define FSIKETSESPENCRYPTIONALGO                          (3)
# define FSIKETSAHHASHALGO                                 (4)
# define FSIKETSSTATUS                                     (5)

/*
 *  The Constant Declarations for
 *  fsIkeCryptoMapTable
 */

# define FSIKECRYPTOMAPNAME                                (1)
# define FSIKECMLOCALADDRTYPE                              (2)
# define FSIKECMLOCALADDR                                  (3)
# define FSIKECMREMADDRTYPE                                (4)
# define FSIKECMREMADDR                                    (5)
# define FSIKECMPEERADDRTYPE                               (6)
# define FSIKECMPEERADDR                                   (7)
# define FSIKECMTRANSFORMSETBUNDLE                         (8)
# define FSIKECMMODE                                       (9)
# define FSIKECMPFS                                        (10)
# define FSIKECMLIFETIMETYPE                               (11)
# define FSIKECMLIFETIME                                   (12)
# define FSIKECMLIFETIMEKB                                 (13)
# define FSIKECMSTATUS                                     (14)

/*
 *  The Constant Declarations for
 *  fsIkeStatTable
 */

# define FSTUNNELTERMINATIONADDR                           (1)
# define FSIKESAS                                          (2)
# define FSIKEINITSESSIONS                                 (3)
# define FSIKEACCEPTSESSIONS                               (4)
# define FSIKEACTIVESESSIONS                               (5)
# define FSIKEINPACKETS                                    (6)
# define FSIKEOUTPACKETS                                   (7)
# define FSIKEDROPPACKETS                                  (8)
# define FSIKENOOFNOTIFYSENT                               (9)
# define FSIKENOOFNOTIFYRECV                               (10)
# define FSIKENOOFSESSIONSSUCC                             (11)
# define FSIKENOOFSESSIONSFAILURE                          (12)

#endif /*  fsikeOCON_H  */
