/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iketdfs.h,v 1.11 2013/11/12 11:16:39 siva Exp $
 *
 * Description: This has macros and typedefs used by IKE     
 *
 ***********************************************************************/

#ifndef _IKETDFS_H
#define _IKETDFS_H

/* #include <sys/socket.h> */
/* #include <arpa/inet.h> */
#include "ikex509.h"

/* SLL related Macros */
#define t_SLL                              tTMO_SLL
#define t_SLL_NODE                         tTMO_SLL_NODE
#define   SLL_INIT(node)               TMO_SLL_Init(node)
#define   SLL_INIT_NODE(node)          TMO_SLL_Init_Node(node)
#define   SLL_SCAN(List,node,Type)     TMO_SLL_Scan(List,node,Type)
#define   SLL_DYN_OFFSET_SCAN(List,node,TempNode,Type) \
    UTL_SLL_OFFSET_SCAN(List,node,TempNode,Type)
#define   SLL_DELETE(List,node)        TMO_SLL_Delete(List,node)
#define   SLL_ADD(List,Node)           TMO_SLL_Add(List,Node)
#define   SLL_INSERT(List,Prev,node)   TMO_SLL_Insert(List,Prev,node)
#define   SLL_INSERT_IN_FRONT(List,node) TMO_SLL_Insert(List,NULL,node)
#define   SLL_COUNT(List)              TMO_SLL_Count(List)
#define   SLL_PREV(List,Node)          TMO_SLL_Previous(List,Node)
#define   SLL_FIRST(List)              TMO_SLL_First(List)
#define   SLL_LAST(List)               TMO_SLL_Last(List)
#define   SLL_NEXT(List,Node)          TMO_SLL_Next(List,Node)
#define   SLL_CONCAT(pDstLst,pAddLst)  TMO_SLL_Concat(pDstLst,pAddLst)
#define   SLL_GET(List)                   TMO_SLL_Get(List)
#define   SLL_DELETE_LIST(pList, DelFuncOrMacro)    {   \
            while (SLL_COUNT(pList) != 0)                     \
            {                                       \
                DelFuncOrMacro(SLL_GET(pList));     \
            }                                       \
          }

#define   SLL_DELETE_TILL_NODE(List,Start,End,Fn)  \
            TMO_SLL_Delete_Till_Node(List,Start,End,Fn)

#define IKE_UNUSED(x)                      (x=x)

#define MAX_PRE_SHARED_KEY_LENGTH          256
#define MAX_TRANSFORMS                     4
#define MAX_TRANSFORM_INDEX                3
#define IKE_MAX_SUPPORTED_ATTR             12 /* Must be multiple of 4 */
#define IKE_MAX_PROTECTED_NET              8

#define IKE_TRUE                           1
#define IKE_FALSE                          0

/* Macros for SNMP Row Status Values */
#define IKE_ACTIVE                         1
#define IKE_NOTINSERVICE                   2
#define IKE_NOTREADY                       3
#define IKE_CREATEANDGO                    4
#define IKE_CREATEANDWAIT                  5
#define IKE_DESTROY                        6

/* LifeTime (Type) */
#define FSIKE_LIFETIME_SECS            1
#define FSIKE_LIFETIME_KB              2
/* Propreitary */
#define FSIKE_LIFETIME_MINS            3
#define FSIKE_LIFETIME_HRS             4
#define FSIKE_LIFETIME_DAYS            5

#define FSIKE_SECS_PER_MIN             60
#define FSIKE_SECS_PER_HOUR          3600
#define FSIKE_SECS_PER_DAY          86400

/* To guard against Denial-of-Service attack */
#define FSIKE_MIN_LIFETIME_SECS        300
#define FSIKE_MIN_LIFETIME_KB          100 /* 100 KB */

#define IKE_DEFAULT_LIFETIME_TYPE    FSIKE_LIFETIME_HRS
#define IKE_DEFAULT_LIFETIME         8

#define IKE_ERR_MSG_SIZE             128
#define IKE_ADDR_SIZE                128
#define IKE_TABLE_SIZE               640

#define IKE_CALLBACK_FN             gaIkeCallBack
#define IKE_CALLBACK_MAX_ARGS       4

typedef struct IKECERTDB {
    t_SLL_NODE Next;
    UINT1      au1CertId[MAX_NAME_LENGTH];
    tIkeX509  *pCert;
    UINT4      u4Flag;
} tIkeCertDB;

typedef struct IKECERTINFO {
    tIkeX509Name        *pCAName;
    tIkeASN1Integer     *pCASerialNum;
}tIkeCertInfo;


typedef struct IKECERTREQINFO {
    t_SLL_NODE Next;
    UINT1* pu1HashList;                     /* Refers to the CA PubKey HashList*/
    UINT4  u4HashListLen;                   /* Length of the Hash List */
    UINT1 u1EncodeType;                    /* Encoding type requested */
    UINT1 au1Pad[3];
    tIkeX509Name *pCAName;                  /* CA name requested */
} tIkeCertReqInfo;

typedef struct IKECERTMAPTOPEER{
    t_SLL_NODE    Next;
    UINT1         au1EngineName[MAX_NAME_LENGTH];  
    UINT1         au1KeyName[MAX_NAME_LENGTH];  
    tIkeCertInfo  MyCert;                          /* Certificate to send to peer */
    tIkePhase1ID  PeerID;                           /* Peer identification */
}tIkeCertMapToPeer;

typedef struct IkeEngine {
    t_SLL_NODE    Next;
    UINT1         au1EngineName[MAX_NAME_LENGTH]; /* Name of the engine, to be 
                                                     used for configuration */
    UINT4         u4IkeIndex;
    tIkeIpAddr Ip4TunnelTermAddr;                             /* IPv4 address*/
    tIkeIpAddr Ip6TunnelTermAddr;                             /* IPv6 address */
/*    tIkePhase1ID  InitiatorID;                   * Phase1 Id added */ 
    t_SLL         IkePolicyList;                  /* Table of Phase1 policies */
    t_SLL         IkeTransformSetList;            /* Table of Transform-sets, 
                                                     linked to Crypto Maps */
    t_SLL         IkeCryptoMapList;               /* Table of Crypto Maps for 
                                                     Phase2 negotiation */
    t_SLL         IkeRAPolicyList;                /* Table of Remote Access Policy */
    t_SLL         IkeProtectNetList;              /* List of Protected 
                                                     Networks  XXX msb */ 
    t_SLL         IkeKeyList;                     /* Table of Key-ID pairs */
    t_SLL         IkeCertMapList;                 /* Table of certificate to */
                                                  /* be used for peer */

    t_SLL         IkeSessionList;
    t_SLL         IkeSAList;

    t_SLL         IkeRsaKeyList;                  /* Table containing RSA keys */
    t_SLL         IkeDsaKeyList;             /* Table containing DSA keys */
    t_SLL         CACerts;                        /* DB of Trusted Peer certs */
    t_SLL         TrustedPeerCerts;
    t_SLL         UntrustedPeerCerts;
    t_SLL         MyCerts;
    tIkeCertInfo  DefaultCert;
    INT2          i2SocketId;                     /* socket descriptor for this
                                                     engine */
    INT2          i2NattSocketFd;                 /* Socket descriptor for NAT-T 
                                                     support */
    INT2          i2V6SocketFd;                   /* Socket descriptor for IPv6 
                                                     support */
    INT2          i2V6NattSocketFd;               /* Socket descriptor for IPv6 
                                                     NATT support */
    UINT1         u1Status;                       /* Row Status */
    UINT1         au1Padding[3];                  /* Alignment */  
} tIkeEngine;

/* 
 * This is the IKE Policy structure used to store the configured values
 * to be used for Phase1 Negotiation
 */
typedef struct IkePolicy {
    t_SLL_NODE    Next;

    UINT1         au1EngineName[MAX_NAME_LENGTH]; /* Name of the engine, to be 
                                                     used for configuration */
    UINT1         au1PolicyName[MAX_NAME_LENGTH]; /* Name of the , VPN Policy */
    tIkePhase1ID  PolicyPeerID;            /* peer ID for policy  */  
    tIkePhase1ID  PolicyLocalID;            /*Local ID for policy  */  
    tIkeIpAddr    PeerAddr;                /* The IPSEC peer address */
    UINT4         u4Priority;              /* Priority, This is the index */
    UINT4         u4LifeTime;              /* Lifetime in units of 
                                              LifeTimeType */
    UINT4         u4LifeTimeKB;            /* Lifetime in KB */
    UINT2         u2AuthMethod;            /* Auth Method */
    UINT2         u2KeyLen;                /* Key length in case of AES*/
    UINT1         u1HashAlgo;              /* Hash Algo, md5|sha */
    UINT1         u1EncryptionAlgo;        /* Encr Algo, des|3des|aes */
    UINT1         u1DHGroup;               /* Diffie-Helman Group 1|2 */
    UINT1         u1LifeTimeType;          /* secs, mins, hrs or days */
    UINT1         u1Status;                /* Row Status */
    UINT1         u1Mode;                  /* Main | Aggressive mode */
    UINT1         u1PeerIdMask;           /*  Masks the Peer Id in 
                                                       policy list*/
    UINT1         u1LocalIdMask;           /*  Masks the Local Id in 
                                                       policy list*/
    UINT1         u1IkeVersion;           /* Ike Version (V1/V2) */
    UINT1         au1Pad[3];              /* Alignment padding */
} tIkePolicy;

/* This is the Key-ID association structure */
typedef struct IkeKey {
    t_SLL_NODE    Next;
    tIkePhase1ID  KeyID;
    UINT1         au1EngineName[MAX_NAME_LENGTH];  /* The other Index */
    UINT1         au1DisplayKeyId[MAX_NAME_LENGTH]; /* For search and display */
    /* XXX Add key length */
    UINT1         au1KeyString[MAX_PRE_SHARED_KEY_LENGTH];  /* PreShared Key */
    BOOLEAN       bKeyStringSet;              /* Whether the key is set */
    UINT1         u1Status;                   /* Row Status */
    UINT2         u2Pad;                      /* Padding for 4-byte alignment */
} tIkeKey;

/* The Transform-set structure */
typedef struct IkeTransformSet {
    t_SLL_NODE    Next;
    UINT1         au1TransformSetName[MAX_NAME_LENGTH]; /* Name used to 
                 index the table */
    UINT1         au1EngineName[MAX_NAME_LENGTH];       /* The other Index 
                 to this table */
    UINT4         u4RefCount;                           /* Count of how 
                 many Crypto Maps 
                 use this TS */
    UINT2         u2KeyLen;       /*Key length in case of AES*/
    UINT1         u1EspHashAlgo;                        /* ESP Hash Algo 
                 md5|sha */
    UINT1         u1EspEncryptionAlgo;                  /* ESP Encr Algo 
                 des|3des|aes|null */
    UINT1         u1AhHashAlgo;                         /* AH Hash Algo 
                 md5|sha */
    UINT1         u1Status;                             /* Row Status */
    UINT2         u2Pad;          /*Alignment bytes*/
} tIkeTransformSet;

typedef struct IkeCryptoMap {
    t_SLL_NODE    Next;
    UINT1         au1CryptoMapName[IKE_MAX_ADDR_STR_LEN];  /* Name used to 
               index the table */
    UINT1         au1EngineName[MAX_NAME_LENGTH];     /* The other Index 
               to this table */
    tIkeNetwork   LocalNetwork;                       /* The local network 
               whose traffic is to 
               be secured */
    UINT1         au1LocalNetworkDisplay[MAX_NAME_LENGTH]; /* To be used 
                 for show */
    tIkeNetwork   RemoteNetwork;                      /* The remote network 
               whose traffic is to 
               be secured */
    UINT1         au1RemoteNetworkDisplay[MAX_NAME_LENGTH]; /* To be used 
                  for show */
    tIkeIpAddr    PeerAddr;                           /* The IPSEC peer 
               address */

    UINT4         u4CurrSpi;                           /* Current Phase 2 SPI */

    UINT4         u4LifeTime;                         /* Lifetime in units 
               of u1LifeTimeType */
    UINT4         u4LifeTimeKB;                       /* Lifetime in 
               kilo-bytes */
    tIkeTransformSet *aTransformSetBundle[MAX_TRANSFORMS];   /* The 
                  transform-
                  set's linked 
                  to this Crypto 
                  Map */
    UINT1         au1TSBundleDisplay[MAX_NAME_LENGTH]; /* To be used for show */
    UINT1         u1Pfs;                              /* DH group1 | group2 */
    UINT1         u1LifeTimeType;                     /* secs, mins, hrs or 
               days */
    UINT1         u1Mode;                             /* tunnel | transport */
    UINT1         u1Status;                           /* Row Status */
    BOOLEAN       bIsCryptoDynamic;                     /* Whether dynamically installed 
                                                         or manually (site-site) */
    BOOLEAN       bModeForEspOnly;                    /* Whether to tunnel both
        esp and ah or just esp
        */
    UINT1         au1Pad[2];
} tIkeCryptoMap;

typedef struct IkeXAuthServerInfo {
    UINT1         u1XAuthType; /* Only Generic is supported. Use CLI User DB
                                  for authentication */
    UINT1         au1Pad[3];
} tIkeXAuthServerInfo;

typedef struct IkeXAuthClientInfo {
    UINT1 u1XAuthType; /* Only Generic is supported. Send User name and 
                          password in clear. */
    UINT1         au1Pad[3];
    UINT1 au1UserName[MAX_NAME_LENGTH];
    UINT1 au1UserPasswd[MAX_NAME_LENGTH];
} tIkeXAuthClientInfo;

typedef struct IkeCMServerInfo {
    UINT2 au2ConfiguredAttr[IKE_MAX_SUPPORTED_ATTR];
    UINT1 u1AddrType;
    UINT1 u1ConfiAttrIndex;
    UINT1 au1Pad[2];
    tIkeNetwork ProtectedNetwork[IKE_MAX_PROTECTED_NET];
    tIkeIpAddr InternalIpAddr;
    tIkeIpAddr InternalMask;
} tIkeCMServerInfo;

typedef struct IkeCMClientInfo {
    UINT2 au2ConfiguredAttr[IKE_MAX_SUPPORTED_ATTR];
} tIkeCMClientInfo;

typedef struct IkeRemoteAccessProtectNet {
   t_SLL_NODE       Next;
   UINT1 u1Status; 
   UINT1 au1Pad[3];
   UINT1 au1EngineName[MAX_NAME_LENGTH]; /* The other Index to this table */
   UINT1 au1LocalNetworkDisplay[MAX_NAME_LENGTH]; /* To be used for show */
   UINT1 au1ProtectNetName[MAX_NAME_LENGTH];
   tIkeNetwork  ProtectedNetwork;
   UINT4 u4RefCount;
}tIkeRAProtectNet;

typedef struct IkeRemoteAccessInfo {
    t_SLL_NODE       Next;
    tIkePhase1ID     Phase1ID;
    tIkeIpAddr       PeerAddr;
    tIkeIpAddr       LocalAddr;
    UINT1            au1EngineName[MAX_NAME_LENGTH];     /* The other Index 
               to this table */
    UINT1            au1RAInfoName[MAX_NAME_LENGTH];
    UINT1            au1DisplayRAInfoPeerId[MAX_NAME_LENGTH];
    UINT1            au1ProtectNetBundleDisplay[MAX_NAME_LENGTH];
                          /* The Protected Network linked to this Crypto Map */
    tIkeRAProtectNet *aProtectNetBundle[IKE_MAX_PROTECTED_NET];   
    UINT1            au1TransformSetBundleDisplay[MAX_NAME_LENGTH];
    tIkeTransformSet *aTransformSetBundle[MAX_TRANSFORMS];
    UINT1            u1Mode;
    UINT1            u1Pfs;
    UINT1            u1Status;
    UINT1            u1LifeTimeType;
    UINT4            u4LifeTime;
    UINT4            u4LifeTimeKB;
    BOOLEAN          bXAuthEnabled;
    BOOLEAN          bCMEnabled;
    BOOLEAN          bVPNPolicyXAuthEnabled;    /* Flag Enabled when XAUTH
                                                   Configured using VPN 
                                                   Policy Commands */
    UINT1            u1Pad;
    UINT4            u4VpnPolicyPriority; /* Used when Policy Commands 
                                            are used for configuring XAUTH */

    union {
        tIkeXAuthServerInfo XAuthServer;
        tIkeXAuthClientInfo XAuthClient;
    } uIkeXAuthInfo;

    union {
        tIkeCMServerInfo CMServerInfo;
        tIkeCMClientInfo CMClientInfo;
    } uIkeCMInfo;

#define XAuthServer uIkeXAuthInfo.XAuthServer
#define XAuthClient uIkeXAuthInfo.XAuthClient
#define CMServerInfo uIkeCMInfo.CMServerInfo
#define CMClientInfo uIkeCMInfo.CMClientInfo
} tIkeRemoteAccessInfo;

/* This is the Rsa key pair structure */
typedef struct IKERSAKEYINFO {
    t_SLL_NODE    Next;
    UINT1         au1EngineName[MAX_NAME_LENGTH];  /* The engine name to which this key is associated */
    UINT1         au1RsaKeyName[MAX_NAME_LENGTH];  /* Key name */
    tIkePKey      RsaKey;
    tIkeCertInfo  CertInfo;
} tIkeRsaKeyInfo;


/* This is the Dsa key pair structure */
typedef struct IKEDSAKEYINFO {
    t_SLL_NODE    Next;
    UINT1         au1EngineName[MAX_NAME_LENGTH];  /* The engine name to which
                                                      this key is associated */
    UINT1         au1DsaKeyName[MAX_NAME_LENGTH];  /* Key name */
    tIkePKey      DsaKey;
    tIkeCertInfo  CertInfo;
} tIkeDsaKeyInfo;



/* D A T A   S T R U C T U R E S    F O R    V P N    P O L I C Y */

typedef struct IPSECAUTODB {
   tIkePolicy     IkeProposal;    /* IKE Phase I Proposal */
   tIkePolicy     IpsecProposal;  /* IPSec Phase II Proposal */
   tIkePhase1ID   PeerID;            /* Peer identification   */
   UINT2 u2AuthMethod;            /* Auth Method */
   UINT2 u2Pad;
   union 
   {
      UINT1 au1PreSharedKey [MAX_PRE_SHARED_KEY_LENGTH];/*PreShared Key*/
      UINT1 au1CertId [MAX_NAME_LENGTH]; /* Certificate ID */
   } uId;

   /* Following indices are auto-generated internally */
   UINT1          au1CryptoMapName[MAX_NAME_LENGTH];  /* Crypto Map */
   UINT1          au1TransformSetName[MAX_NAME_LENGTH];  /* Transform Set */
} tIpsecAutoDB;

/* ----------------------------------------
 * Data structures for IPSec Keying Mode:
 * 
 * 1. Manual Key 
 * 2. IKE 
 * ----------------------------------------
 */

#define IPSEC_MANUAL_KEY_LEN 68

/* IPSec Keying mode: manual */
typedef struct IPSECMANUALKEY {
  
   UINT1        au1AhKey[IPSEC_MANUAL_KEY_LEN]; /* hmac-md5/hmac-sha1 */
   
   /* XXX Note: Strucutre is aligned (IPSEC_MANUAL_KEY_LEN is 32) */
   UINT1        au1EspKey[IPSEC_MANUAL_KEY_LEN]; /* des/3des/aes */
   UINT1        u1IPSecProtocol;  /* AH or ESP */
   UINT1        u1IPSecMode;      /* tunnel or transport */
   UINT1        u1HashAlgo;    /* Hash Algo: HMAC-MD5 | HMAC-SHA1 */

   UINT1        au1EspTripleDesKey[3][IPSEC_MANUAL_KEY_LEN];
   UINT1        u1EncryptionAlgo; /* Encr Algo: DES | 3-DES | AES */
   UINT1        u1AesKeyLen;      /* AES key len */
   UINT1        au1Padding[3];          /* Pad bytes */

   UINT4        u4InboundSpi;      /* Inbound Security Parameter Index */
   UINT4        u4OutboundSpi;     /* Outbound Security Parameter Index */

   
} tIpsecManualKey;


/* Utility macros for accessing different members 
 * of the data structures.
 * vp -> vpn policy 
 */
#define IKE_VP_NAME(vp)                (vp)->au1VpnPolicyName
#define IKE_VP_INDEX(vp)               (vp)->u4VpnPolicyIndex

#define IKE_VP_OUT_INDEX(vp)           (2 * IKE_VP_INDEX(vp)) - 1
#define IKE_VP_IN_INDEX(vp)            (2 * IKE_VP_INDEX(vp)) 

#define IKE_VP_KEY_TYPE(vp)            (vp)->u4IpsecKeyType
#define IKE_VP_KEY_MODE(vp)            (vp)->uIpsecKeyMode

#define IKE_VP_KEY_MODE_AUTO(vp)       IKE_VP_KEY_MODE(vp).IpsecAutoDB
#define IKE_VP_KEY_MODE_MANUAL(vp)     IKE_VP_KEY_MODE(vp).IpsecManualKey


#define IKE_VP_KEY_PRESHARED(vp)       IKE_VP_KEY_MODE_AUTO(vp).uId.\
                                                  au1PreSharedKey

#define IKE_VP_ID_CERT(vp)             IKE_VP_KEY_MODE_AUTO(vp).uId.au1CertId

#define IKE_VP_GET_AUTODB(vp, ptr) \
if (IKE_PRESHARED_KEY == IKE_VP_KEY_TYPE(vp))\
   ptr = &(IKE_VP_KEY_MODE_AUTO(vp));\
else if (IKE_RSA_SIGNATURE == IKE_VP_KEY_TYPE(vp))\
   ptr = &(IKE_VP_KEY_MODE_AUTO(vp));\
else \
   ptr = NULL;


#define IKE_VP_PEERID(pAutoDB)           (pAutoDB)->PeerID
#define IKE_VP_CRYPTO_MAP(pAutoDB)       (pAutoDB)->au1CryptoMapName
#define IKE_VP_TRANSFORM_SET(pAutoDB)    (pAutoDB)->au1TransformSetName
#define IKE_VP_IKE_PROPOSAL(pAutoDB)     (pAutoDB)->IkeProposal
#define IKE_VP_IPSEC_PROPOSAL(pAutoDB)   (pAutoDB)->IpsecProposal
                                                    
/* VPN Policy Database */
typedef struct IKEVPNPOLICY {
   
   t_SLL_NODE     Next;
   
   UINT1          au1VpnPolicyName[MAX_NAME_LENGTH]; /* VPN Policy Name */
   UINT4          u4VpnPolicyIndex; /* Index to identify the vpn policy */
   
   UINT4    u4IpsecKeyType;
   union
   {
      tIpsecManualKey   IpsecManualKey;  /* Mode: Manual Keying */ 
      tIpsecAutoDB      IpsecAutoDB;     /* IKE phase 1 and IPSec phase 2
                                            proposals*/
   } uIpsecKeyMode;

   tIkeNetwork    LocalNetwork;      /* Local Network */  
   tIkeNetwork    RemoteNetwork;     /* Remote Network*/

   tIkeIpAddr     LocTunnTermAddr;  /* Local tunnel termination addr(v4/v6)  */
   tIkeIpAddr     RemTunnTermAddr;  /* Remote tunnel termination addr(v4/v6) */
   
   UINT1          u1Status;       /* Row status (for SNMP) */
   UINT1          u1VpnPolicyAttrMask; /* Flag Mask to get phase 1 & 2 attrs */
   BOOLEAN        bVPNPolicyXAuthEnabled;    /* Flag Enabled when XAUTH
                                                   Configured using VPN 
                                                   Policy Commands */
   UINT1          u1Pad;         /* Alignment bytes       */
} tIkeVpnPolicy;

/* To handle callbacks for customization in IKE */
typedef union IkeCallBackEntry {
        INT4 (*pIkeWritePreSharedKeyToNVRAM) PROTO((VOID));
        INT4 (*pIkeCustSecureMemAccess) PROTO((UINT4, UINT4, ...));
                
}unIkeCallBackEntry;


#endif /* _IKETDFS_H */

