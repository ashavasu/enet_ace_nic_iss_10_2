/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsikeogi.h,v 1.3 2010/11/02 09:38:20 prabuc Exp $
*
* Description: Macros for IKE snmp module
*********************************************************************/
# ifndef fsikeOGP_H
# define fsikeOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSIKESCALARS                                 (0)
# define SNMP_OGP_INDEX_FSIKEENGINETABLE                             (1)
# define SNMP_OGP_INDEX_FSIKEPOLICYTABLE                             (2)
# define SNMP_OGP_INDEX_FSIKEKEYTABLE                                (3)
# define SNMP_OGP_INDEX_FSIKETRANSFORMSETTABLE                       (4)
# define SNMP_OGP_INDEX_FSIKECRYPTOMAPTABLE                          (5)
# define SNMP_OGP_INDEX_FSIKESTATTABLE                               (6)

#endif /*  fsikeOGP_H  */
