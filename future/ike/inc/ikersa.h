/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikersa.h,v 1.6 2011/12/05 14:37:33 siva Exp $
 *
 * Description:This file contains the #define constants and data structure
 *             used by the rsa module
 *
 ********************************************************************/
#ifndef _IKERSAWRAP_H_
#define _IKERSAWRAP_H_


#include <openssl/x509v3.h>

#define  IKE_MAX_FILE_NAME_LEN            200
#define  MAX_DIR_NAME_LEN                 200
#define  DIRECTORY_NOT_EXIST              2
#define  DIRECTORY_ALREADY_EXISTING       17
#define  IKE_NID_UNDEF                    0
/*Return Info for no rsa key pair*/
#define  NO_CERT_EXIST                    0
#define  CERT_EXIST                       1
#define  NO_CERT_MAP_INFO                 1
#define  DEL_ALL_CERT_SA                  2
#define  IKE_SUB_NAME                     12
#define  IKE_MAX_CERT_REQ_LEN             4096
/*Error Codes */

/*Key Generation Error Codes*/
#define IKE_ERR_DIRECTORY_CREATION_FAILED 2
#define IKE_ERR_KEY_ID_ALREADY_EXIST      3
#define IKE_ERR_KEY_GEN_FAILED            4
#define IKE_ERR_DIR_NAME_TOO_LONG         5
#define IKE_ERR_FILE_NAME_TOO_LONG        6

/*Encryption and decryption error codes*/
#define IKE_ERR_GET_RSA_FAILED            7
#define IKE_ERR_RSA_SIGN_FAILED           8
#define IKE_ERR_RSA_PUB_DECRYPT_FAILED    9

/*file system error codes*/
#define IKE_ERR_CHANGE_DIR_FAILED         10

/*Delete Key Pair*/
#define IKE_KEY_ID_DOES_NOT_EXIST         11
#define IKE_ERR_DEL_ALL_KEY_INFO          12
#define IKE_ERR_DEL_ALL_MY_CERTS          13
#define IKE_ERR_NO_KEY_INFO_FOR_KEY_ID    14
#define IKE_ERR_DEL_CERT_FAILED           15
#define IKE_ERR_CERTDB_UNUPDATED          16
#define IKE_ERR_NO_ENGINE_WITH_ID         17
#define IKE_ERR_DEL_KEY_INFO_FAILED       18


/*Memory related error codes*/
#define IKE_ERR_MEM_ALLOC                 19

/*Generate Certificate Error Codes*/
#define IKE_ERR_KEY_ID_NOT_EXIST          20
#define IKE_ERR_CONSTRUCT_PKCS10_CERT_REQ 21
#define IKE_ERR_ADD_X509V3_EXT            22
#define IKE_ERR_KEY_READ_FAILED           23
#define IKE_ERR_ADD_TO_DB_FAILED          24
#define IKE_ERR_X509_REQ_SIGN             25
#define IKE_ERR_DEL_CERT_MAP_FAILED       26

/*Encryption and decryption error codes*/
#define IKE_ERR_GET_DSA_FAILED            27
#define IKE_ERR_DSA_SIGN_FAILED           28
#define IKE_ERR_DSA_PUB_DECRYPT_FAILED    29


/*Following are the wrapper functions which uses openssl calls relating to key 
  generation,storage and retrieval and also for generating the certificate
  request*/

/*For generating the key pair*/ 
INT1 IkeRsaGenKeyPair(UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT2 u2NBits, UINT4 *pu4ErrNo);

/*For Importing Key pair*/ 
INT1 IkeImportRsaKey(UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT1 *pu1FileName, UINT4 *pu4ErrNo);

/*For generating the certificate request*/
INT1 IkeGenerateCertificateRequest(UINT1 *pu1EngineId, UINT1 *pu1KeyId, 
                                   UINT1 *pu1SubjAltName, 
                                   UINT1 *pu1SubjName,
                                   UINT1 **ppTextOut, UINT4, UINT4 *pu4ErrNo);

/*For encrypting using private key(Signing)*/
INT1 IkeRsaPrivateEncrypt(UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **pu1OutBuf
                          ,INT4 *pi4EncrDataLen, tIkePKey *pKey,
                          UINT4 *pu4ErrNo);

/*For Decrypting using public key*/
INT1 IkeRsaPublicDecrypt(UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **pu1OutBuf,
                         INT4 *pi4DecryptDataLen, tIkePKey *pKey, 
                         UINT4 *pu4ErrNo);

/*For RSA key-pair delete*/
INT1 IkeDeleteKeyPair(UINT1 *pu1EngineId, UINT1 *pu1KeyId, 
                         tIkeX509Name **pCAName,
                         tIkeASN1Integer **pCASerialNum, UINT1 *pu1ReturnInfo,
                      UINT4 u4EncrType, UINT4 *pu4ErrNo);

/*Deleting key and cert from the Engine structure*/
INT1 IkeDeleteRsaCertFromDB(UINT1 *pu1EngineId, UINT1 *pu1KeyId, 
                            UINT4 *pu4ErrNo,
                            tIkeX509Name **pCAName, 
                            tIkeASN1Integer **pCASerialNum,
                            UINT1 *pu1ReturnInfo);

/*Delete CertMap info from the list*/
INT1 IkeDeleteCertMapFromDB(UINT1 *pu1EngineId, tIkeX509Name *pCAName,
                            tIkeASN1Integer *pCASerialNum);

/*Get CertMap from DB based on CAName and serial number*/
tIkeCertMapToPeer *IkeGetCertMapFromDB(UINT1 *pu1EngineId, tIkeX509Name *pCAName,
                         tIkeASN1Integer *pCASerialNum, UINT1 *pu1RetInfo);

/*This function constructs the request*/
INT1 IkeConstructPKCS10CertRequest(X509_REQ *pCertReq, tIkePKey *pKey, 
                                   UINT1 *pu1SubjName);

/*This functions addd the subject altname extension to the request*/
INT1 IkeAddX509V3Ext( X509_REQ *pCertReq, X509V3_CTX *pCtx, 
                      UINT1 *pu1SubjectAltName);

/*Delete All keys from the database*/
INT1 IkeDeleteAllKeyInfoFromDB(UINT1 *pu1EngineId, UINT4 u4EncrType);

/*Delete particular key info entry from the database*/
INT1 IkeDeleteKeyEntryFromDB(UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                             UINT4 u4EncrType, UINT4 *pu4ErrNo);

/*Check if key id already exist*/
INT1 IkeVerifyKeyId(UINT1 *pu1EngineId, UINT1 *pu1KeyId);

/*get key entry from the database*/
tIkeRsaKeyInfo *IkeGetRsaKeyEntry(UINT1 *pu1EngineId, UINT1 *pu1KeyId);

/*Add key entry in the database*/
INT1 IkeAddRsaKeyEntryToDB (UINT1 * pu1EngineId, UINT1 *pu1KeyId,
                         tIkeRSA * pIkeRSA);
                         
/*Convert certificate request to text format*/
INT1
IkeConvertX509RequestToText (X509_REQ * pX509CertReq, UINT1 **ppu1OutText);

INT1 
IkeRsaGetKeyLength(tIkeEngine *pIkeEngine, tIkeCertInfo * pCertInfo, UINT4 *u4KeySize);

PUBLIC INT4 RsaCBitForEncrAndDecr(VOID *);
PUBLIC INT4 RsaCBitForSignAndVerify(VOID *);

#endif /* _IKERSAWRAP_H_ */
