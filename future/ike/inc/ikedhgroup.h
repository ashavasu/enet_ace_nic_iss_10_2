/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikedhgroup.h,v 1.6 2014/08/22 11:03:19 siva Exp $
 *
 * Description: This has structures and prototypes for ikedhgroup.c 
 *
 ***********************************************************************/

#ifndef _IKEDHGROUP_H
#define _IKEDHGROUP_H


/* Group Types: Currently only MODP is supported */
#define FS_DH_TYPE_MODP     1
#define FS_DH_TYPE_EC2N     2
#define FS_DH_TYPE_ECP      3

/* Oakley DH Group Numbers as defined in RFC 2409 */
#define FS_DH_GROUP_NUM_1   1
#define FS_DH_GROUP_NUM_2   2
#define FS_DH_GROUP_NUM_5   5

#define FS_MAX_DH_KEY_SIZE  256

typedef struct HEXNUM {
    INT4  i4Length;
    UINT1 *pu1Value; 
}tHexNum;

typedef struct DHGROUP {
    UINT4   u4Generator;
    tHexNum pPrime;
    UINT4   u4Length;           /* Length of the prime */
    UINT1   u1Type;            /* Group Type */
    UINT1   u1Group;
    UINT2   u2Pad;
} tDhGroupInfo;

tHexNum * HexNumNew(UINT4 u4Length);
INT4 HexNumFree(tHexNum *pHexNum);
INT4 FSDhGenerateKeyPair(tDhGroupInfo *pDhInfo, tHexNum *pub, tHexNum *prv);
INT4 FSDhCompute(tDhGroupInfo *pDhInfo, tHexNum *pPub1, tHexNum *pPrv1, 
        tHexNum *pPub2, tHexNum *pKey);

extern tDhGroupInfo gDhGroupInfo768;
extern tDhGroupInfo gDhGroupInfo1024;
extern tDhGroupInfo gDhGroupInfo1536;
extern tDhGroupInfo gDhGroupInfo2048;

#endif /* _IKEDHGROUP_H */

