/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikex509.h,v 1.8 2013/10/25 10:53:01 siva Exp $
 *
 * Description:This file contains the prototype definitions and #define 
 *             constants of the IKE X509 Wrapper module
 *
 ********************************************************************/
#ifndef _IKEX509WRAP_H_
#define _IKEX509WRAP_H_

#include <openssl/rsa.h>
#include <openssl/x509_vfy.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>

#include <time.h>

/* Values for ERRORs */
#define IKE_ERR_FILE_NOT_EXIST          1
#define IKE_ERR_INVALID_FORMAT          2
#define IKE_ERR_CERT_KEY_MISMATCH       3
#define IKE_ERR_CA_NOT_TRUSTED          4
#define IKE_ERR_CERT_NOT_EXIST          5
#define IKE_ERR_KEY_NOT_EXIST           6
#define IKE_ERR_CERT_EXIST              7
#define IKE_ERR_GEN_ERR                 8

#define IKE_MAX_SIGN_NAME_SIZE 30
#define IKE_MAX_PATH_SIZE 500
#define IKE_MAX_CERT_SIZE 16384
#define IKE_MAX_STRING_LEN  255
/* SubjectAltName types */
#define IKE_X509_USER_FQDN 1
#define IKE_X509_DNS_NAME  2
#define IKE_X509_IP_ADDR   7

/* Type of encoding of Certificate */
#define IKE_PEM_ENCODED    1
#define IKE_DER_ENCODED    2

/* Flag values for whether Cert is trusted */
#define IKE_UNTRUSTED_CERT 1
#define IKE_TRUSTED_CERT   2

/* Flag value for whether to delete ALL certs or only DYNAMIC-ally 
   learnt ones */
#define IKE_DEL_DYNAMIC    1
#define IKE_DEL_ALL        2

/* Compare the string from key file to find the algorithm (RSA/DSA) */
#define IKE_RSA_ALGO_STRING_IN_KEY_FILE   (UINT1 *)"-----BEGIN RSA"
#define IKE_DSA_ALGO_STRING_IN_KEY_FILE   (UINT1 *)"-----BEGIN DSA"

typedef X509_NAME tIkeX509Name;
typedef ASN1_INTEGER tIkeASN1Integer;
typedef ASN1_TIME    tIkeASN1Time;

typedef EVP_PKEY tIkePKey;
typedef RSA tIkeRSA;

typedef DSA tIkeDSA;

typedef X509_STORE tIkeX509Store;
typedef X509 tIkeX509;

typedef time_t tIkeTime;

INT1 IkeImportMyCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1KeyId, 
        UINT1 *pu1FileName, UINT1 u1EncType, UINT4 *pu4ErrNo);
INT1 IkeImportCaCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1CertId, 
        UINT1 *pu1FileName, UINT1 u1EncType, UINT4 *pu4ErrNo);
INT1 IkeImportPeerCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1CertId, 
        UINT1 *pu1FileName, UINT1 u1EncodeType, UINT1 u1Flag, UINT4 *pu4ErrNo);

/* NO commands */
/* All the following commands are to be called at the CLI side 
 * The commands to be called on the IKE side for deleting SAs have to be
 * defined seperately as they do not have any interaction with openssl
 */
/* The CA name and serial number is outputted so that this can be sent to IKE task for deleting the necessary SA's */
INT1 IkeDelMyCert (UINT1 *pu1EngineId, UINT1 *pu1KeyId, 
        tIkeX509Name **ppCAName, tIkeASN1Integer **ppCASerialNum, 
        UINT4 *pu4ErrNo);
/* In this case, all SA's created using certificates should be deleted by IKE task */
INT1 IkeDelCaCert (UINT1 *pu1EngineId, UINT1 *pu1CertId, UINT4 *pu4ErrNo);

INT4 IkeX509NameCmp (tIkeX509Name *pName1, tIkeX509Name *pName2);
INT4 IkeASN1IntegerCmp (tIkeASN1Integer *pASN1Int1, tIkeASN1Integer *pASN1Int2);
tIkeASN1Integer *IkeGetX509SerialNum (tIkeX509 *pCert);
VOID IkeX509Free (tIkeX509 *pCert);
tIkeASN1Integer *IkeASN1IntegerDup (tIkeASN1Integer *pASN1Integer);
VOID IkeASN1IntegerFree (tIkeASN1Integer *pASN1Integer);
tIkeX509Name *IkeX509NameDup (tIkeX509Name *pX509Name);
VOID IkeX509NameFree (tIkeX509Name *pX509Name);
INT1 IkeVerifyCert (UINT1 *pu1EngineId, tIkeX509 *pCert);
tIkePKey *IkeGetX509PubKey (tIkeX509 *pCert);
tIkeX509Name *IkeGetX509IssuerName (tIkeX509 *pCert);
INT1 IkeGetX509SubjectName (tIkeX509 *pCert, UINT1 **ppu1SubjName, 
        UINT4 *pu4Namelen);
INT4 IkeGetX509SubjectAltName (tIkeX509 *pCert, UINT1 **ppu1SubjAltName, 
        UINT4 *pu4Namelen);
tIkeASN1Time *IkeGetNotBefore (tIkeX509 *pCert);
tIkeASN1Time *IkeGetNotAfter (tIkeX509 *pCert);
tIkeX509 *IkeGetX509FromDER (UINT1 *pu1DER, UINT4 u4Len);
INT1 IkeConvertX509ToDER (tIkeX509 *pCert, UINT1 **ppu1DER, UINT4 *pu4DERLen);
INT1 IkeConvertX509ToASCII (tIkeX509 *pX509Cert, UINT1 **ppu1ASCII, 
        UINT4 *pu4ASCIILen);
INT1 IkeConvertDERToX509Name(UINT1 *u1DERName, UINT4 u4Length, 
        tIkeX509Name **pX509Name);
INT1 IkeGetTimeFromASN1Time (tIkeASN1Time *pASN1Time, tIkeTime *pIkeTime);
VOID IkeGetCurTime (tIkeTime *pIkeTime);
INT4 IkeX509TimeCmp (tIkeASN1Time *pASN1Time, tIkeTime *pIkeTime);
INT4 IkeCompareDEREncDNs (UINT1 *pu1DN1, UINT4 u4Len1, UINT1 *pu1DN2, 
        UINT4 u4Len2);
INT4 IkeConvertPublicKeyToDER (tIkePKey* pPubkey, UINT1 **ppu1Public);

UINT4 IkeGetSignatureAlgo PROTO ((tIkeX509 *));
INT4 IkeConvertDERToStr (UINT1 * pi1DERBuf, INT4 i4Len, INT1 * pi1Buf);
INT4 IkeConvertStrToDER (INT1 * pi1Str, INT1 * pi1DERBuf, INT4 * pi4BufLen);

#undef SHA1_HASH_SIZE
#endif /* _IKEX509WRAP_H_ */
