/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 
 * $Id: auth_db.h,v 1.4 2013/11/12 11:16:39 siva Exp $

 * $Id:auth_db.h
 *
 * Description: This has macros and functions prototypes for auth module
 *
 ***********************************************************************/
#ifndef _AUTH_DB_H
#define _AUTH_DB_H

#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"

#define MAX_USER                100
#define COMMON_USER_FILE_NAME   "usersDB.txt"
#define AUTH_NO_EOF             1
#define AUTH_EOF                2
#define AUTH_MAX_USERS_LINE_LEN 250
#define AUTH_MAX_USER_SIZE  32
#define MAX_PROTO_LENGTH    12

#define USER_AUTH_FAILURE   0
#define USER_AUTH_SUCCESS   1

extern tTMO_SLL gCommonAuthList;

typedef struct auth_user
{
       tTMO_SLL_NODE Link;
       UINT1   au1UserName[AUTH_MAX_USER_SIZE+1];
       UINT1   au1Password[AUTH_MAX_USER_SIZE+1];
       UINT1   au1Protocol[MAX_PROTO_LENGTH+1];
       UINT1   au1Padding;
}tUserAuth;


/* function prototypes */

VOID AuthInit PROTO ((VOID));
INT1 AuthDbAddUser PROTO ((UINT1 *pu1UserName,UINT1 *pu1Password, UINT1 *pu1Protocol));
INT1 AuthDbDeleteUser PROTO ((UINT1 *pu1UserName));
INT1 AuthDbModifyUser PROTO ((UINT1 *pu1UserName,UINT1 *pu1Password,
                      UINT1 *pu1protocol));
INT1 AuthDbValidateUser PROTO ((UINT1 *pu1UserName, UINT1* pu1Password,
                        UINT1 *pu1Protocol));

INT1 auth_add_user_list PROTO ((UINT1 *pu1UserName,UINT1 *pu1Password, 
                        UINT1 *pu1Protocol));

INT1 auth_write_file PROTO ((INT4  i4Fd, UINT1 *pu1UserName, UINT1 *pu1Password, UINT1 *pu1Protocol));

INT1 auth_upload_list PROTO ((INT1 *pi1Buff));

INT1 AuthReadLineFromFile PROTO ((INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                            INT2 *pi2ReadLen));

INT1 auth_user_update_file PROTO ((VOID));

#endif
