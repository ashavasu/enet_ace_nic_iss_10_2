/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsikemid.h,v 1.3 2010/11/02 09:38:22 prabuc Exp $
*
* Description: Proto types for wrapper functions
*********************************************************************/
/*  Prototype for Get Test & Set for fsikeScalars.  */
tSNMP_VAR_BIND*
fsikeScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsikeScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsikeScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkeEngineTable.  */
tSNMP_VAR_BIND*
fsIkeEngineEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsIkeEngineEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsIkeEngineEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkePolicyTable.  */
tSNMP_VAR_BIND*
fsIkePolicyEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsIkePolicyEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsIkePolicyEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkeKeyTable.  */
tSNMP_VAR_BIND*
fsIkeKeyEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsIkeKeyEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsIkeKeyEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkeTransformSetTable.  */
tSNMP_VAR_BIND*
fsIkeTransformSetEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsIkeTransformSetEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsIkeTransformSetEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkeCryptoMapTable.  */
tSNMP_VAR_BIND*
fsIkeCryptoMapEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsIkeCryptoMapEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsIkeCryptoMapEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIkeStatTable.  */
tSNMP_VAR_BIND*
fsIkeStatEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));

