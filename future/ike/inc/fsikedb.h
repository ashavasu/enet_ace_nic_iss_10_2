/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsikedb.h,v 1.3 2010/11/02 09:38:28 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIKEDB_H
#define _FSIKEDB_H

UINT1 FsIkeEngineTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkePolicyTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsIkeRAInfoTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkeProtectNetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkeKeyTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkeTransformSetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkeCryptoMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIkeStatTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsike [] ={1,3,6,1,4,1,2076,66,1};
tSNMP_OID_TYPE fsikeOID = {9, fsike};


UINT4 FsikeGlobalDebug [ ] ={1,3,6,1,4,1,2076,66,1,1};
UINT4 FsikeRAXAuthMode [ ] ={1,3,6,1,4,1,2076,66,1,2};
UINT4 FsikeRACMMode [ ] ={1,3,6,1,4,1,2076,66,1,3};
UINT4 FsIkeEngineName [ ] ={1,3,6,1,4,1,2076,66,4,1,1};
UINT4 FsIkeEngineTunnelTermAddrType [ ] ={1,3,6,1,4,1,2076,66,4,1,2};
UINT4 FsIkeEngineTunnelTermAddr [ ] ={1,3,6,1,4,1,2076,66,4,1,3};
UINT4 FsIkeEngineRowStatus [ ] ={1,3,6,1,4,1,2076,66,4,1,4};
UINT4 FsIkePolicyPriority [ ] ={1,3,6,1,4,1,2076,66,5,1,1};
UINT4 FsIkePolicyAuthMethod [ ] ={1,3,6,1,4,1,2076,66,5,1,2};
UINT4 FsIkePolicyHashAlgo [ ] ={1,3,6,1,4,1,2076,66,5,1,3};
UINT4 FsIkePolicyEncryptionAlgo [ ] ={1,3,6,1,4,1,2076,66,5,1,4};
UINT4 FsIkePolicyDHGroup [ ] ={1,3,6,1,4,1,2076,66,5,1,5};
UINT4 FsIkePolicyLifeTimeType [ ] ={1,3,6,1,4,1,2076,66,5,1,6};
UINT4 FsIkePolicyLifeTime [ ] ={1,3,6,1,4,1,2076,66,5,1,7};
UINT4 FsIkePolicyLifeTimeKB [ ] ={1,3,6,1,4,1,2076,66,5,1,8};
UINT4 FsIkePolicyMode [ ] ={1,3,6,1,4,1,2076,66,5,1,9};
UINT4 FsIkePolicyStatus [ ] ={1,3,6,1,4,1,2076,66,5,1,10};
UINT4 FsIkeRAInfoName [ ] ={1,3,6,1,4,1,2076,66,6,1,1};
UINT4 FsIkeRAInfoPeerIdType [ ] ={1,3,6,1,4,1,2076,66,6,1,2};
UINT4 FsIkeRAInfoPeerId [ ] ={1,3,6,1,4,1,2076,66,6,1,3};
UINT4 FsIkeRAXAuthEnable [ ] ={1,3,6,1,4,1,2076,66,6,1,4};
UINT4 FsIkeRACMEnable [ ] ={1,3,6,1,4,1,2076,66,6,1,5};
UINT4 FsIkeRAXAuthType [ ] ={1,3,6,1,4,1,2076,66,6,1,6};
UINT4 FsIkeRAXAuthUserName [ ] ={1,3,6,1,4,1,2076,66,6,1,7};
UINT4 FsIkeRAXAuthUserPasswd [ ] ={1,3,6,1,4,1,2076,66,6,1,8};
UINT4 FsIkeRAIPAddressAllocMethod [ ] ={1,3,6,1,4,1,2076,66,6,1,9};
UINT4 FsIkeRAInternalIPAddrType [ ] ={1,3,6,1,4,1,2076,66,6,1,10};
UINT4 FsIkeRAInternalIPAddr [ ] ={1,3,6,1,4,1,2076,66,6,1,11};
UINT4 FsIkeRAInternalIPNetMaskType [ ] ={1,3,6,1,4,1,2076,66,6,1,12};
UINT4 FsIkeRAInternalIPNetMask [ ] ={1,3,6,1,4,1,2076,66,6,1,13};
UINT4 FsIkeRAInfoProtectedNetBundle [ ] ={1,3,6,1,4,1,2076,66,6,1,14};
UINT4 FsIkeRATransformSetBundle [ ] ={1,3,6,1,4,1,2076,66,6,1,15};
UINT4 FsIkeRAMode [ ] ={1,3,6,1,4,1,2076,66,6,1,16};
UINT4 FsIkeRAPfs [ ] ={1,3,6,1,4,1,2076,66,6,1,17};
UINT4 FsIkeRALifeTimeType [ ] ={1,3,6,1,4,1,2076,66,6,1,18};
UINT4 FsIkeRALifeTime [ ] ={1,3,6,1,4,1,2076,66,6,1,19};
UINT4 FsIkeRAInfoStatus [ ] ={1,3,6,1,4,1,2076,66,6,1,20};
UINT4 FsIkeProtectNetName [ ] ={1,3,6,1,4,1,2076,66,7,1,1};
UINT4 FsIkeProtectNetType [ ] ={1,3,6,1,4,1,2076,66,7,1,2};
UINT4 FsIkeProtectNetAddr [ ] ={1,3,6,1,4,1,2076,66,7,1,3};
UINT4 FsIkeProtectNetStatus [ ] ={1,3,6,1,4,1,2076,66,7,1,4};
UINT4 FsIkeKeyId [ ] ={1,3,6,1,4,1,2076,66,8,1,1};
UINT4 FsIkeKeyIdType [ ] ={1,3,6,1,4,1,2076,66,8,1,2};
UINT4 FsIkeKeyString [ ] ={1,3,6,1,4,1,2076,66,8,1,3};
UINT4 FsIkeKeyStatus [ ] ={1,3,6,1,4,1,2076,66,8,1,4};
UINT4 FsIkeTransformSetName [ ] ={1,3,6,1,4,1,2076,66,9,1,1};
UINT4 FsIkeTSEspHashAlgo [ ] ={1,3,6,1,4,1,2076,66,9,1,2};
UINT4 FsIkeTSEspEncryptionAlgo [ ] ={1,3,6,1,4,1,2076,66,9,1,3};
UINT4 FsIkeTSAhHashAlgo [ ] ={1,3,6,1,4,1,2076,66,9,1,4};
UINT4 FsIkeTSStatus [ ] ={1,3,6,1,4,1,2076,66,9,1,5};
UINT4 FsIkeCryptoMapName [ ] ={1,3,6,1,4,1,2076,66,10,1,1};
UINT4 FsIkeCMLocalAddrType [ ] ={1,3,6,1,4,1,2076,66,10,1,2};
UINT4 FsIkeCMLocalAddr [ ] ={1,3,6,1,4,1,2076,66,10,1,3};
UINT4 FsIkeCMRemAddrType [ ] ={1,3,6,1,4,1,2076,66,10,1,4};
UINT4 FsIkeCMRemAddr [ ] ={1,3,6,1,4,1,2076,66,10,1,5};
UINT4 FsIkeCMPeerAddrType [ ] ={1,3,6,1,4,1,2076,66,10,1,6};
UINT4 FsIkeCMPeerAddr [ ] ={1,3,6,1,4,1,2076,66,10,1,7};
UINT4 FsIkeCMTransformSetBundle [ ] ={1,3,6,1,4,1,2076,66,10,1,8};
UINT4 FsIkeCMMode [ ] ={1,3,6,1,4,1,2076,66,10,1,9};
UINT4 FsIkeCMPfs [ ] ={1,3,6,1,4,1,2076,66,10,1,10};
UINT4 FsIkeCMLifeTimeType [ ] ={1,3,6,1,4,1,2076,66,10,1,11};
UINT4 FsIkeCMLifeTime [ ] ={1,3,6,1,4,1,2076,66,10,1,12};
UINT4 FsIkeCMLifeTimeKB [ ] ={1,3,6,1,4,1,2076,66,10,1,13};
UINT4 FsIkeCMStatus [ ] ={1,3,6,1,4,1,2076,66,10,1,14};
UINT4 FsTunnelTerminationAddr [ ] ={1,3,6,1,4,1,2076,66,11,1,1};
UINT4 FsIkeSAs [ ] ={1,3,6,1,4,1,2076,66,11,1,2};
UINT4 FsIkeInitSessions [ ] ={1,3,6,1,4,1,2076,66,11,1,3};
UINT4 FsIkeAcceptSessions [ ] ={1,3,6,1,4,1,2076,66,11,1,4};
UINT4 FsIkeActiveSessions [ ] ={1,3,6,1,4,1,2076,66,11,1,5};
UINT4 FsIkeInPackets [ ] ={1,3,6,1,4,1,2076,66,11,1,6};
UINT4 FsIkeOutPackets [ ] ={1,3,6,1,4,1,2076,66,11,1,7};
UINT4 FsIkeDropPackets [ ] ={1,3,6,1,4,1,2076,66,11,1,8};
UINT4 FsIkeNoOfNotifySent [ ] ={1,3,6,1,4,1,2076,66,11,1,9};
UINT4 FsIkeNoOfNotifyRecv [ ] ={1,3,6,1,4,1,2076,66,11,1,10};
UINT4 FsIkeNoOfSessionsSucc [ ] ={1,3,6,1,4,1,2076,66,11,1,11};
UINT4 FsIkeNoOfSessionsFailure [ ] ={1,3,6,1,4,1,2076,66,11,1,12};


tMbDbEntry fsikeMibEntry[]= {

{{10,FsikeGlobalDebug}, NULL, FsikeGlobalDebugGet, FsikeGlobalDebugSet, FsikeGlobalDebugTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{10,FsikeRAXAuthMode}, NULL, FsikeRAXAuthModeGet, FsikeRAXAuthModeSet, FsikeRAXAuthModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{10,FsikeRACMMode}, NULL, FsikeRACMModeGet, FsikeRACMModeSet, FsikeRACMModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{11,FsIkeEngineName}, GetNextIndexFsIkeEngineTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeEngineTableINDEX, 1},

{{11,FsIkeEngineTunnelTermAddrType}, GetNextIndexFsIkeEngineTable, FsIkeEngineTunnelTermAddrTypeGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIkeEngineTableINDEX, 1},

{{11,FsIkeEngineTunnelTermAddr}, GetNextIndexFsIkeEngineTable, FsIkeEngineTunnelTermAddrGet, FsIkeEngineTunnelTermAddrSet, FsIkeEngineTunnelTermAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeEngineTableINDEX, 1},

{{11,FsIkeEngineRowStatus}, GetNextIndexFsIkeEngineTable, FsIkeEngineRowStatusGet, FsIkeEngineRowStatusSet, FsIkeEngineRowStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeEngineTableINDEX, 1},

{{11,FsIkePolicyPriority}, GetNextIndexFsIkePolicyTable, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyAuthMethod}, GetNextIndexFsIkePolicyTable, FsIkePolicyAuthMethodGet, FsIkePolicyAuthMethodSet, FsIkePolicyAuthMethodTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyHashAlgo}, GetNextIndexFsIkePolicyTable, FsIkePolicyHashAlgoGet, FsIkePolicyHashAlgoSet, FsIkePolicyHashAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyEncryptionAlgo}, GetNextIndexFsIkePolicyTable, FsIkePolicyEncryptionAlgoGet, FsIkePolicyEncryptionAlgoSet, FsIkePolicyEncryptionAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyDHGroup}, GetNextIndexFsIkePolicyTable, FsIkePolicyDHGroupGet, FsIkePolicyDHGroupSet, FsIkePolicyDHGroupTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyLifeTimeType}, GetNextIndexFsIkePolicyTable, FsIkePolicyLifeTimeTypeGet, FsIkePolicyLifeTimeTypeSet, FsIkePolicyLifeTimeTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyLifeTime}, GetNextIndexFsIkePolicyTable, FsIkePolicyLifeTimeGet, FsIkePolicyLifeTimeSet, FsIkePolicyLifeTimeTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyLifeTimeKB}, GetNextIndexFsIkePolicyTable, FsIkePolicyLifeTimeKBGet, FsIkePolicyLifeTimeKBSet, FsIkePolicyLifeTimeKBTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyMode}, GetNextIndexFsIkePolicyTable, FsIkePolicyModeGet, FsIkePolicyModeSet, FsIkePolicyModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkePolicyStatus}, GetNextIndexFsIkePolicyTable, FsIkePolicyStatusGet, FsIkePolicyStatusSet, FsIkePolicyStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkePolicyTableINDEX, 2},

{{11,FsIkeRAInfoName}, GetNextIndexFsIkeRAInfoTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInfoPeerIdType}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInfoPeerIdTypeGet, FsIkeRAInfoPeerIdTypeSet, FsIkeRAInfoPeerIdTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInfoPeerId}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInfoPeerIdGet, FsIkeRAInfoPeerIdSet, FsIkeRAInfoPeerIdTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAXAuthEnable}, GetNextIndexFsIkeRAInfoTable, FsIkeRAXAuthEnableGet, FsIkeRAXAuthEnableSet, FsIkeRAXAuthEnableTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRACMEnable}, GetNextIndexFsIkeRAInfoTable, FsIkeRACMEnableGet, FsIkeRACMEnableSet, FsIkeRACMEnableTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAXAuthType}, GetNextIndexFsIkeRAInfoTable, FsIkeRAXAuthTypeGet, FsIkeRAXAuthTypeSet, FsIkeRAXAuthTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAXAuthUserName}, GetNextIndexFsIkeRAInfoTable, FsIkeRAXAuthUserNameGet, FsIkeRAXAuthUserNameSet, FsIkeRAXAuthUserNameTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAXAuthUserPasswd}, GetNextIndexFsIkeRAInfoTable, FsIkeRAXAuthUserPasswdGet, FsIkeRAXAuthUserPasswdSet, FsIkeRAXAuthUserPasswdTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAIPAddressAllocMethod}, GetNextIndexFsIkeRAInfoTable, FsIkeRAIPAddressAllocMethodGet, FsIkeRAIPAddressAllocMethodSet, FsIkeRAIPAddressAllocMethodTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInternalIPAddrType}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInternalIPAddrTypeGet, FsIkeRAInternalIPAddrTypeSet, FsIkeRAInternalIPAddrTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInternalIPAddr}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInternalIPAddrGet, FsIkeRAInternalIPAddrSet, FsIkeRAInternalIPAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInternalIPNetMaskType}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInternalIPNetMaskTypeGet, FsIkeRAInternalIPNetMaskTypeSet, FsIkeRAInternalIPNetMaskTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInternalIPNetMask}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInternalIPNetMaskGet, FsIkeRAInternalIPNetMaskSet, FsIkeRAInternalIPNetMaskTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInfoProtectedNetBundle}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInfoProtectedNetBundleGet, FsIkeRAInfoProtectedNetBundleSet, FsIkeRAInfoProtectedNetBundleTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRATransformSetBundle}, GetNextIndexFsIkeRAInfoTable, FsIkeRATransformSetBundleGet, FsIkeRATransformSetBundleSet, FsIkeRATransformSetBundleTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAMode}, GetNextIndexFsIkeRAInfoTable, FsIkeRAModeGet, FsIkeRAModeSet, FsIkeRAModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAPfs}, GetNextIndexFsIkeRAInfoTable, FsIkeRAPfsGet, FsIkeRAPfsSet, FsIkeRAPfsTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRALifeTimeType}, GetNextIndexFsIkeRAInfoTable, FsIkeRALifeTimeTypeGet, FsIkeRALifeTimeTypeSet, FsIkeRALifeTimeTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRALifeTime}, GetNextIndexFsIkeRAInfoTable, FsIkeRALifeTimeGet, FsIkeRALifeTimeSet, FsIkeRALifeTimeTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeRAInfoStatus}, GetNextIndexFsIkeRAInfoTable, FsIkeRAInfoStatusGet, FsIkeRAInfoStatusSet, FsIkeRAInfoStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeRAInfoTableINDEX, 2},

{{11,FsIkeProtectNetName}, GetNextIndexFsIkeProtectNetTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeProtectNetTableINDEX, 2},

{{11,FsIkeProtectNetType}, GetNextIndexFsIkeProtectNetTable, FsIkeProtectNetTypeGet, FsIkeProtectNetTypeSet, FsIkeProtectNetTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeProtectNetTableINDEX, 2},

{{11,FsIkeProtectNetAddr}, GetNextIndexFsIkeProtectNetTable, FsIkeProtectNetAddrGet, FsIkeProtectNetAddrSet, FsIkeProtectNetAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeProtectNetTableINDEX, 2},

{{11,FsIkeProtectNetStatus}, GetNextIndexFsIkeProtectNetTable, FsIkeProtectNetStatusGet, FsIkeProtectNetStatusSet, FsIkeProtectNetStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeProtectNetTableINDEX, 2},

{{11,FsIkeKeyId}, GetNextIndexFsIkeKeyTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeKeyTableINDEX, 2},

{{11,FsIkeKeyIdType}, GetNextIndexFsIkeKeyTable, FsIkeKeyIdTypeGet, FsIkeKeyIdTypeSet, FsIkeKeyIdTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeKeyTableINDEX, 2},

{{11,FsIkeKeyString}, GetNextIndexFsIkeKeyTable, FsIkeKeyStringGet, FsIkeKeyStringSet, FsIkeKeyStringTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeKeyTableINDEX, 2},

{{11,FsIkeKeyStatus}, GetNextIndexFsIkeKeyTable, FsIkeKeyStatusGet, FsIkeKeyStatusSet, FsIkeKeyStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeKeyTableINDEX, 2},

{{11,FsIkeTransformSetName}, GetNextIndexFsIkeTransformSetTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeTransformSetTableINDEX, 2},

{{11,FsIkeTSEspHashAlgo}, GetNextIndexFsIkeTransformSetTable, FsIkeTSEspHashAlgoGet, FsIkeTSEspHashAlgoSet, FsIkeTSEspHashAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeTransformSetTableINDEX, 2},

{{11,FsIkeTSEspEncryptionAlgo}, GetNextIndexFsIkeTransformSetTable, FsIkeTSEspEncryptionAlgoGet, FsIkeTSEspEncryptionAlgoSet, FsIkeTSEspEncryptionAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeTransformSetTableINDEX, 2},

{{11,FsIkeTSAhHashAlgo}, GetNextIndexFsIkeTransformSetTable, FsIkeTSAhHashAlgoGet, FsIkeTSAhHashAlgoSet, FsIkeTSAhHashAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeTransformSetTableINDEX, 2},

{{11,FsIkeTSStatus}, GetNextIndexFsIkeTransformSetTable, FsIkeTSStatusGet, FsIkeTSStatusSet, FsIkeTSStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeTransformSetTableINDEX, 2},

{{11,FsIkeCryptoMapName}, GetNextIndexFsIkeCryptoMapTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMLocalAddrType}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMLocalAddrTypeGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMLocalAddr}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMLocalAddrGet, FsIkeCMLocalAddrSet, FsIkeCMLocalAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMRemAddrType}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMRemAddrTypeGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMRemAddr}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMRemAddrGet, FsIkeCMRemAddrSet, FsIkeCMRemAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMPeerAddrType}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMPeerAddrTypeGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMPeerAddr}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMPeerAddrGet, FsIkeCMPeerAddrSet, FsIkeCMPeerAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMTransformSetBundle}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMTransformSetBundleGet, FsIkeCMTransformSetBundleSet, FsIkeCMTransformSetBundleTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMMode}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMModeGet, FsIkeCMModeSet, FsIkeCMModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMPfs}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMPfsGet, FsIkeCMPfsSet, FsIkeCMPfsTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMLifeTimeType}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMLifeTimeTypeGet, FsIkeCMLifeTimeTypeSet, FsIkeCMLifeTimeTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMLifeTime}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMLifeTimeGet, FsIkeCMLifeTimeSet, FsIkeCMLifeTimeTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMLifeTimeKB}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMLifeTimeKBGet, FsIkeCMLifeTimeKBSet, FsIkeCMLifeTimeKBTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsIkeCMStatus}, GetNextIndexFsIkeCryptoMapTable, FsIkeCMStatusGet, FsIkeCMStatusSet, FsIkeCMStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIkeCryptoMapTableINDEX, 2},

{{11,FsTunnelTerminationAddr}, GetNextIndexFsIkeStatTable, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIkeStatTableINDEX, 1},

{{11,FsIkeSAs}, GetNextIndexFsIkeStatTable, FsIkeSAsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeInitSessions}, GetNextIndexFsIkeStatTable, FsIkeInitSessionsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeAcceptSessions}, GetNextIndexFsIkeStatTable, FsIkeAcceptSessionsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeActiveSessions}, GetNextIndexFsIkeStatTable, FsIkeActiveSessionsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeInPackets}, GetNextIndexFsIkeStatTable, FsIkeInPacketsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeOutPackets}, GetNextIndexFsIkeStatTable, FsIkeOutPacketsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeDropPackets}, GetNextIndexFsIkeStatTable, FsIkeDropPacketsGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeNoOfNotifySent}, GetNextIndexFsIkeStatTable, FsIkeNoOfNotifySentGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeNoOfNotifyRecv}, GetNextIndexFsIkeStatTable, FsIkeNoOfNotifyRecvGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeNoOfSessionsSucc}, GetNextIndexFsIkeStatTable, FsIkeNoOfSessionsSuccGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},

{{11,FsIkeNoOfSessionsFailure}, GetNextIndexFsIkeStatTable, FsIkeNoOfSessionsFailureGet, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIkeStatTableINDEX, 1},
};
tMibData fsikeEntry = { 76, fsikeMibEntry };
#endif /* _FSIKEDB_H */

