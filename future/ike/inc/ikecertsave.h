/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecertsave.h,v 1.5 2011/10/28 10:42:20 siva Exp $
 *
 * Description:This file contains the prototype definitions of functions
 *             in ikecert.c
 *
 ********************************************************************/
#ifndef _IKESAVECERT_H_
#define _IKESAVECERT_H_

#include "ikegen.h"

#define IKE_MAX_PEM_CERT_LENGTH 4096
#define IKE_MAX_PEM_KEY_LENGTH  4096

#define IKE_CERT_RECORD_MYCERT      1
#define IKE_CERT_RECORD_PEERCERT    2
#define IKE_CERT_RECORD_CACERT      3

typedef struct IkeCertRecord 
{
    UINT4 u4Flag;
    UINT1 au1CertId[MAX_NAME_LENGTH];
    UINT1 au1Key[IKE_MAX_PEM_KEY_LENGTH];
    UINT1 au1Cert[IKE_MAX_PEM_CERT_LENGTH];
} tIkeCertRecord;

INT1 IkeSaveMyCertConfig (UINT1 *pu1EngineId, FILE * pFp);
INT1 IkeSaveCaCertConfig (UINT1 *pu1EngineId, FILE * pFp);
INT1 IkeSavePeerCertConfig (UINT1 *pu1EngineId, FILE * pFp);
INT1 IkeReadCertConfig (UINT1 *pu1EngineId);
INT1 IkeReadMyCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord);
INT1 IkeReadPeerCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord);
INT1 IkeReadCaCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord);
INT1 IkeConvertX509ToPEM (tIkeX509 * pCert, UINT1 *pu1PEM, UINT4 u4PEMLen);
INT1 IkeConvertRsaKeyToPEM (tIkeRsaKeyInfo * pRsaInfo, UINT1 *pu1PEM, 
        UINT4 u4PEMLen);
INT1 IkeConvertDsaKeyToPEM (tIkeDsaKeyInfo * pDsaInfo, UINT1 *pu1PEM, 
                            UINT4 u4PEMLen);
#endif /* _IKESAVECERT_H_ */

