/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecrypto.h,v 1.7 2011/04/25 09:35:05 siva Exp $
 *
 * Description: This has structures and prototypes for ikecrypto.c 
 *
 ***********************************************************************/
#ifndef _IKECRYPTO_H_
#define _IKECRYPTO_H_


/* Macros */
#define IKE_DES_KEY_LEN             8
#define IKE_AES_KEY_LEN            24 
#define IKE_DES_MAX_WEAK_KEYS      16
#define AES_DERIVED_KEY_LEN        64

#define IKE_BITS2BYTES(x)           ((x)/8)
#define IKE_BYTES2BITS(x)           ((x)*8)

#define DES_KEY_TYPE               UINT1 (*)[IKE_DES_KEY_LEN]

typedef unsigned long long tDesKey[16];

/* Structure for holding all the HMAC algorithms */
typedef struct HmacAlgorithm {
    UINT4 u4KeyLen;
 INT4 (*HmacAlgo)(UINT1 *, INT4, UINT1 *, INT4, UINT1 *);
} tIkeHmacAlgo;

/* Structure for holding all the HASH algorithms */
typedef struct HashAlgorithm {
    UINT4 u4KeyLen;
 VOID (*HashAlgo)(UINT1 *, INT4, UINT1 *);
} tIkeHashAlgo;

/* Structure for holding all the Cipher algorithms */
typedef struct CipherAlgorithm {
    UINT4 u4BlockLen;
    UINT4 u4KeyLen;
} tIkeCipherAlgo;

extern tIkeHmacAlgo gaIkeHmacAlgoList[];
extern tIkeHashAlgo gaIkeHashAlgoList[];
extern tIkeCipherAlgo gaIkeCipherAlgoList[];
extern UINT1 gau1IKEToIPSecEncrAlgo[];
extern UINT1 gau1IPSecToIKEEncrAlgo[];
extern UINT1 gau1ESPToIKEHashAlgo[];
extern UINT1 gau1AHToIKEHashAlgo[];
extern tIkeCipherAlgo gaIke2CipherAlgoList[];

/* Hash Algorithm function prototypes */
VOID IkeMd5HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeSha1HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeSha224HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeSha256HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeSha384HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeSha512HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

VOID IkeInvalidHashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest); 

/* Hmac Algorithm function prototypes */
INT4
IkeHmacMd5Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr, 
                     INT4 i4BufLen, UINT1 *pu1Digest);
INT4
IkeHmacSha1Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr, 
                     INT4 i4BufLen, UINT1 *pu1Digest);
INT4
IkeXcbcMacAlgo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
    INT4 i4BufLen, UINT1 *pu1Digest);

INT4
IkeHmacSha256Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
    INT4 i4BufLen, UINT1 *pu1Digest);

INT4
IkeHmacSha384Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
    INT4 i4BufLen, UINT1 *pu1Digest);

INT4
IkeHmacSha512Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
    INT4 i4BufLen, UINT1 *pu1Digest);

INT1 IkeDesSetKey (UINT1 *pu1KeyMat, tDesKey *pDesKey);
INT1 IkeTripleDesSetKey (UINT1 *pu1KeyMat, tDesKey *pDesKey1,
        tDesKey *pDesKey2,tDesKey *pDesKey3);

VOID IkeDesCbcEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
        tDesKey Key, UINT1 *pu1InitVector);

VOID IkeDesCbcDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
        tDesKey Key, UINT1 *pu1InitVector);


INT4 
IkeInvalidHmacAlgo (UINT1 *pu1BufPtr, INT4 i4BufLen, UINT1 *pu1Key, 
                           INT4 i4KeyLen, UINT1 *pu1Digest);

/* Encryption Algorithm function prototypes */

VOID IkeTripleDesCbcDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
        tDesKey Key1, tDesKey Key2, tDesKey Key3,
        UINT1 *pu1InitVector);



VOID IkeTripleDesCbcEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
        tDesKey Key1, tDesKey Key2, tDesKey Key3,
        UINT1 *pu1InitVector);

BOOLEAN IkeDesIsWeakKey (UINT1 *pu1Key);


/* API's to set the AES Encr and Decrypt keys for IKEv1 */
INT1
IkeAesSetKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesEncrKey,
             UINT1 *au1AesDecrKey);

/* API's to set the AES Encr and Decrypt keys for IKEv2*/
INT1
IkeAesSetEncrKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesEncrKey);

INT1
IkeAesSetDecrKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesDecrKey);

/* APIS to encrypt and decrypt packets for IKEv1 and IKEv2 */
VOID
IkeAesEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize, UINT1 u1InKeyLen, UINT1 *au1AesEncrKey,
               UINT1 *pu1InitVector);
VOID
IkeAesDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize, UINT1 u1InKeyLen,
                               UINT1 *au1AesDecrKey, UINT1 *pu1InitVector);

#endif /*  #ifndef _IKECRYPTO_H_ */
