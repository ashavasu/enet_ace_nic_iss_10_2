/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ikeconstruct.h,v 1.1 2010/11/03 08:48:19 prabuc Exp $
 *
 * Description:This file contains the #define constants and data structure
 *             used by the constructor module
 *
 ********************************************************************/
#ifndef _IKECONSTRUCT_H_
#define _IKECONSTRUCT_H_

INT1 IkeSetProposal(tIkeSessionInfo *,INT4, UINT4 *, UINT1, UINT2, UINT1, UINT1, UINT1); 
INT1 IkeSetPhase2Atts(tIkeSessionInfo *, UINT1 **, INT4 *, UINT1, UINT1); 
INT1 IkeSetIsakmpAtts(tIkeSessionInfo *, UINT1 **, INT4 *); 
INT1 BasicAtt(UINT2, INT4, UINT1 **, INT4 *, INT4 *);
INT4 IkeSetIpsecProposals(tIkeSessionInfo *, INT4, UINT4 *, UINT1); 
INT1 VpiAtt(UINT2,UINT1 *,UINT2,UINT1 **,INT4 *,INT4 *);
INT1 IkeSetTransform(tIkeSessionInfo *, INT4, UINT4 *, UINT1, UINT1, UINT1,
                     UINT1 *, INT4);
INT1 IkeSetCMReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                            INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetCMRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                            INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetCMSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                            INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetCMAckAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                            INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetXAuthReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                               INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetXAuthRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                               INT4 *pi4AttsLen, INT4 *pi4Len);
INT1 IkeSetXAuthSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                               INT4 *pi4AttsLen, INT4 *pi4Len);
#endif
