
 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * 
 *  $Id: ikeconstruct.c,v 1.18 2014/01/25 13:54:09 siva Exp $
 * 
 *  Description:This file contains functions for constructing Isakmp 
 *               Message.
 * 
 ********************************************************************/

#include "ikegen.h"
#include "ikeconstruct.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikecert.h"
#include "ikememmac.h"

/* Globale structure to hold the supprted vendor Ids */

tIkeVendorIdPayload gaIkeVendorIdPayload[] = {

    {{IKEV1_XAUTH_VENDOR_ID_PAYLOAD_09,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_00,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_26,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_89,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_df,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_d6,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_b7,
      IKEV1_XAUTH_VENDOR_ID_PAYLOAD_12}
     , IKE_EIGHT}
    ,
    /* XAUTH Vendor Payload (0x09002689DFD6B712) */

    {{IKEV1_CISCO_VENDOR_ID_PAYLOAD_12,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_f5,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_f2,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_8c,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_45,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_71,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_68,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_a9,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_70,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_2d,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_9f,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_e2,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_74,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_cc,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_02,
      IKEV1_CISCO_VENDOR_ID_PAYLOAD_d4}
     , IKE_SIXTEEN}
    ,
    /* CISCO UNITY PEER COMPLIANCE Vendor Paylod  
     *  - (0x12f5f28c457168a9702d9fe274cc02d4) */

    {{IKEV1_NATT_VENDOR_ID_PAYLOAD_4a,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_13,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_1c,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_81,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_07,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_03,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_58,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_45,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_5c,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_57,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_28,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_f2,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_0e,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_95,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_45,
      IKEV1_NATT_VENDOR_ID_PAYLOAD_2f}
     , IKE_SIXTEEN}
    /* Check whether NATT is supported by peer
     *  - (0x4a131c81070358455c5728f20e95452f) */
};

/************************************************************************/
/*  Function Name   :IkeConstructHeader                                 */
/*  Description     :This function is used to construct Isakmp Header   */
/*                  :in an Isakmp Message                               */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info             */
/*                  :u1Exch   : Identifies the mode of Exchange         */
/*                  : (MainMode,QuickMode,AgressiveMode)                */
/*                  :u4MesgId :Distinguishes Phase-1 and Phase-2        */
/*                  :u1NextPayLoad : Identifies the Successive payload  */
/*                  :in an Isakmp Message                               */
/*  Output(s)       :Appends the Isakmp Header in the Packet            */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructHeader (tIkeSessionInfo * pSesInfo,
                    UINT1 u1Exch, UINT1 u1NextPayLoad)
{
    tIsakmpHdr          Hdr;

    /*Initialize the Isakmp Header */

    IKE_BZERO ((UINT1 *) &Hdr, sizeof (tIsakmpHdr));

    /*Fill the Isakmp Header with the appropriate fields */

    IKE_MEMCPY ((UINT1 *) &Hdr.au1InitCookie,
                (UINT1 *) pSesInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    IKE_MEMCPY ((UINT1 *) &Hdr.au1RespCookie,
                (UINT1 *) pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);

    Hdr.u1Version = IKE_ISAKMP_VERSION;
    Hdr.u1Exch = u1Exch;
    Hdr.u1NextPayload = u1NextPayLoad;
    Hdr.u4MessId = IKE_HTONL (pSesInfo->u4MsgId);

    /*Allocate Space for the Isakmp Header in the Outgoing Isakmp Mesg */

    if (ConstructIsakmpMesg (pSesInfo, IKE_ZERO,
                             sizeof (tIsakmpHdr)) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Copy the Isakmp Header to the PayLoad */
    IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt, (UINT1 *) &Hdr,
                sizeof (tIsakmpHdr));
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :ConstructIsakmpMesg                                */
/*                  :                                                   */
/*  Description     :This function is invoked to allocate the required  */
/*                  :memory  for the Isakmp Packet                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to session info data base which */
/*                  :contains the outgoining packet                     */
/*                  :i4Pos : Specifies the position in the packet where */
/*                  :the payload is to be appended                      */
/*                  :u4NumBytes:Specifies the Number of Bytes to be     */
/*                  :in an Isakmp Message                               */
/*                  :                                                   */
/*  Output(s)       :Allocates memory for specified no of bytes         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
ConstructIsakmpMesg (tIkeSessionInfo * pSesInfo, INT4 i4Pos, UINT4 u4NumBytes)
{
    UINT1              *pu1Temp = NULL;

    if (pSesInfo->IkeTxPktInfo.u4BufLen > (UINT4) ((UINT4) i4Pos + u4NumBytes))
    {
        pSesInfo->IkeTxPktInfo.u4PacketLen += u4NumBytes;
        return IKE_SUCCESS;
    }

    /* Reallocate Memory to the Payload with the specified
       no of bytes at the said position */

    while (pSesInfo->IkeTxPktInfo.u4BufLen < ((UINT4) i4Pos + u4NumBytes))
    {
        pSesInfo->IkeTxPktInfo.u4BufLen += IKE_CHUNK_SIZE;
    }

    pSesInfo->IkeTxPktInfo.u4PacketLen += u4NumBytes;
    IKE_ASSERT (pSesInfo->IkeTxPktInfo.u4BufLen != IKE_ZERO);
    if (pSesInfo->IkeTxPktInfo.pu1RawPkt == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Temp) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "ConstructIsakmpMesg: unable to allocate " "buffer\n");
            return IKE_FAILURE;
        }
        if (pu1Temp == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "ConstructIsakmpMesg: Realloc fails\n");
            return IKE_FAILURE;
        }
        pSesInfo->IkeTxPktInfo.pu1RawPkt = pu1Temp;
        IKE_BZERO ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4Pos),
                   ((INT4) pSesInfo->IkeTxPktInfo.u4BufLen - i4Pos));
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructKE                                     */
/*  Description     :This function is used to construct the KeyExchange */
/*                  :payload in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :u1NextPayLoad :Identifies the next payload in an   */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos :Specifies the position for the payload to  */
/*                  :be in the Isakmp Packet                            */
/*                  :                                                   */
/*  Output(s)       :Appends the KeyExchange Payload in the Isakmp Mesg */
/*                  : at the specified position                         */
/*                  :                                                   */
/*  Returns         :Returns Success or Failure                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructKE (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tKeyExcPayLoad      KE;
    UINT4               u4Len = IKE_ZERO, u4NBytes = IKE_ZERO;

    /* Moved this up here as calculation of u4NBytes relies on this */
    if (pSesInfo->pMyDhPub == NULL)
    {
        pSesInfo->pMyDhPub = HexNumNew (pSesInfo->pDhInfo->u4Length);
        pSesInfo->pMyDhPrv = HexNumNew (pSesInfo->pDhInfo->u4Length);

        if ((pSesInfo->pMyDhPub == NULL) || (pSesInfo->pMyDhPrv == NULL))
        {
            IKE_ASSERT (pSesInfo->pMyDhPub == NULL);
            IKE_ASSERT (pSesInfo->pMyDhPrv == NULL);
            return (IKE_FAILURE);
        }

        if (FSDhGenerateKeyPair (pSesInfo->pDhInfo, pSesInfo->pMyDhPub,
                                 pSesInfo->pMyDhPrv) < IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessKE: Failed to Generate DH Key pair\n");
            return IKE_FAILURE;
        }
    }

    u4NBytes = sizeof (tKeyExcPayLoad) + (UINT2) pSesInfo->pMyDhPub->i4Length;

    /*Initialize the Key Exchange PayLoad */
    IKE_BZERO ((UINT1 *) &KE, sizeof (tKeyExcPayLoad));
    KE.u1NextPayLoad = u1NextPayLoad;
    KE.u2PayLoadLen = (UINT2) u4NBytes;
    KE.u2PayLoadLen = IKE_HTONS (KE.u2PayLoadLen);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Copy the KeyExch PayLoad to the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &KE, sizeof (tKeyExcPayLoad));
    *pu4Pos += sizeof (tKeyExcPayLoad);

    u4Len = (UINT4) (KE.u2PayLoadLen - IKE_KEY_PAYLOAD_SIZE);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) pSesInfo->pMyDhPub->pu1Value,
                pSesInfo->pMyDhPub->i4Length);
    *pu4Pos += (UINT4) (pSesInfo->pMyDhPub->i4Length);

    UNUSED_PARAM (u4Len);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructHash                                   */
/*  Description     :This function is used to construct the hash payload*/
/*                  :in an Isakmp Mesg                                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info database    */
/*                  :u1NextPayLoad : Identifies the Next PayLoad in an  */
/*                  : Isakmp Message                                    */
/*                  :pu4Pos : Pointer to the position in an Isakmp Mesg */
/*                  :where the paylaod is to be appended                */
/*                  :                                                   */
/*  Output(s)       :Appends the Hash PayLoad in the Isakmp Mesg        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructHash (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                  UINT4 *pu4Pos)
{
    tHashPayLoad        Hash;
    UINT1               au1HashData[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    UINT4               u4NBytes = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4NBytes = sizeof (tHashPayLoad);

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u4NBytes += u4HashLen;
    IKE_BZERO ((UINT1 *) au1HashData, u4HashLen);
    IKE_BZERO ((UINT1 *) &Hash, sizeof (tHashPayLoad));

    /* Compute the Digest Value */
    if (IkeComputeHash (pSesInfo, au1HashData) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructHash:Compute Hash fails\n");
        return (IKE_FAILURE);
    }

    Hash.u1NextPayLoad = u1NextPayLoad;
    Hash.u2PayLoadLen = (UINT2) u4NBytes;
    Hash.u2PayLoadLen = IKE_HTONS (Hash.u2PayLoadLen);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Hash, sizeof (tHashPayLoad));
    *pu4Pos += sizeof (tHashPayLoad);

    /* Copy the computed digest value to the Isakmp 
       Mesg after the Hash Payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                au1HashData, u4HashLen);
    *pu4Pos += u4HashLen;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeComputeHash                                     */
/*  Description     :This function is used to compute hash for phase-1  */
/*                  :negotiation                                        */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :pu1Hash  :Pointer to the Computed Digest Value     */
/*                  :                                                   */
/*  Output(s)       :The Computed Digest Value                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeComputeHash (tIkeSessionInfo * pSesInfo, UINT1 *pu1Hash)
{
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Ptr = NULL, *pu1Temp = NULL;

    u4Len =
        (UINT4) (((UINT4) pSesInfo->pMyDhPub->i4Length +
                  (UINT4) pSesInfo->pHisDhPub->i4Length) +
                 (UINT4) (IKE_TWO * IKE_COOKIE_LEN) + pSesInfo->u2SALength +
                 pSesInfo->u4IdPayloadSentLen);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeComputeHash: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeComputeHash:malloc fails\n");
        return IKE_FAILURE;
    }

    pu1Temp = pu1Ptr;

    /* If we are Initiator, generate HASH_I to send 
     * RFC 2409: g^xi | g ^xr | CKY-I | CKY-R | SAi_b | IDii_b
     * We are Responder, generate HASH_R to send 
     * RFC 2409: g^xr | g ^xi | CKY-R | CKY-I | SAi_b | IDir_b */

    /* Whether we are Initiator or Responder, pMyDhPub will come first */
    IKE_MEMCPY (pu1Temp, pSesInfo->pMyDhPub->pu1Value,
                pSesInfo->pMyDhPub->i4Length);
    pu1Temp += pSesInfo->pMyDhPub->i4Length;

    IKE_MEMCPY (pu1Temp, pSesInfo->pHisDhPub->pu1Value,
                pSesInfo->pHisDhPub->i4Length);
    pu1Temp += pSesInfo->pHisDhPub->i4Length;

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1InitiatorCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1ResponderCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;
    }
    else
    {
        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1ResponderCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1InitiatorCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;
    }

    IKE_MEMCPY (pu1Temp, pSesInfo->pSAOffered, pSesInfo->u2SALength);
    pu1Temp += pSesInfo->u2SALength;

    /* Copy Sent ID Payload - Generic Header */
    /* The ID payload is always the Sent ID Payload, regardless of
     * whether we are Initiator or Responder
     */
    IKE_MEMCPY (pu1Temp, pSesInfo->pu1IdPayloadSent,
                pSesInfo->u4IdPayloadSentLen);

    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyId,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1Ptr, (INT4) u4Len,
         pu1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeComputeHash:Unsupported Hash algorithm\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return (IKE_FAILURE);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructNonce                                  */
/*  Description     :This functions is used to construct the Nonce      */
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Appends the KeyExchange PayLoad in an Isakmp Mesg  */
/*                  : at the specified position                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructNonce (tIkeSessionInfo * pSesInfo,
                   UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    UINT1               au1MyNonce[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tNoncePayLoad       Nonce;
    UINT4               u4HashLen = IKE_ZERO;

    /* Get the Hash Algorithm length */
    if (pSesInfo->u1ExchangeType == IKE_QUICK_MODE)
    {
        u4HashLen = gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
            u4KeyLen;

        if (u4HashLen == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructNonce: Unsupported Hmac Algorithm\n");
            return IKE_FAILURE;
        }
    }
    else
    {
        u4HashLen = gaIkeHmacAlgoList[pSesInfo->IkePhase1Policy.u1HashAlgo].
            u4KeyLen;

        if (u4HashLen == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructNonce: Unsupported Hmac Algorithm\n");
            return IKE_FAILURE;
        }
    }

    /* Generate the Nonce Randomly */
    /* Using the Hash algorithm length as the Nonce Length */
    IkeGetRandom (au1MyNonce, (INT4) u4HashLen);
    IKE_BZERO ((UINT1 *) &Nonce, sizeof (tNoncePayLoad));

    /* The fourth param is to tell the function whether to set the Initiator or
     * Responder Nonce
     */

    if (IkeSetNonceInSessionInfo (pSesInfo, au1MyNonce, (UINT2) u4HashLen,
                                  pSesInfo->u1Role) == IKE_FAILURE)

    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNonce: Out Of Memory\n");
        return IKE_FAILURE;
    }

    Nonce.u1NextPayLoad = u1NextPayLoad;
    Nonce.u2PayLoadLen = (UINT2) (u4HashLen + sizeof (tNoncePayLoad));
    Nonce.u2PayLoadLen = IKE_HTONS (Nonce.u2PayLoadLen);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (u4HashLen +
                                                        sizeof (tNoncePayLoad)))
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNonce: Out Of Memory\n");
        return IKE_FAILURE;
    }
    /* Copy the Nonce PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Nonce, sizeof (tNoncePayLoad));
    *pu4Pos += sizeof (tNoncePayLoad);

    /* Copy the generated nonce to the Nonce payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) au1MyNonce, u4HashLen);
    *pu4Pos += u4HashLen;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructID                                     */
/*  Description     :This function is used to construct ID in an Isakmp */
/*                  :Mesg for Phase-1 Exchg                             */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the Position in an Isakmp Mesg */
/*                  :where this payload is to be appended               */
/*                  :                                                   */
/*  Output(s)       :Appends the paylaod at the specified position in   */
/*                  : an Isakmp Mesg                                    */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                    */
/************************************************************************/

INT1
IkeConstructID (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIdPayLoad          ID;
    tIkePhase1ID        IdValue;
    INT1                i1DerId[IKE_MAX_DER_ID_LEN];
    UINT4               u4NBytes = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &ID, sizeof (tIdPayLoad));
    IKE_BZERO (i1DerId, IKE_MAX_DER_ID_LEN);

    if (pSesInfo->pIkeSA->InitiatorID.i4IdType == IKE_IPSEC_ID_DER_ASN1_DN)
    {
        if (IkeConvertStrToDER
            ((INT1 *) &pSesInfo->pIkeSA->InitiatorID.uID.au1Dn, i1DerId,
             &i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructID: Converting string to DN format Failed\n");
            return (IKE_FAILURE);
        }

        IdValue.i4IdType = IKE_IPSEC_ID_DER_ASN1_DN;
        IdValue.u4Length = (UINT4) i4Len;
    }
    else
    {
        IKE_MEMCPY (&IdValue, &pSesInfo->pIkeSA->InitiatorID,
                    sizeof (tIkePhase1ID));
    }
    u4NBytes = (UINT4) (sizeof (tIdPayLoad) + IdValue.u4Length);

    if (IdValue.i4IdType == IKE_IPSEC_ID_IPV4_ADDR)
    {
        IdValue.uID.Ip4Addr = IKE_HTONL (IdValue.uID.Ip4Addr);
    }

    ID.u1NextPayLoad = u1NextPayLoad;
    ID.u2PayLoadLen = (UINT2) IKE_HTONS (u4NBytes);

    /* Fill the ID payload with standard values for phase 1 */
    ID.u1IdType = (UINT1) IdValue.i4IdType;

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructID: Out Of Memory\n");
        return IKE_FAILURE;
    }

    /* pu1IdPayloadSent to be used in computing hash */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1IdPayloadSent) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructID: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pSesInfo->pu1IdPayloadSent == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeConstructID: Out Of Memory\n");
        return IKE_FAILURE;
    }

    /* Copy the ID payload to the Isakmp Mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &ID, sizeof (tIdPayLoad));

    /* Copy ID Payload header pu1IdPayloadSent */
    IKE_MEMCPY (pSesInfo->pu1IdPayloadSent,
                (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos +
                 sizeof (tGenericPayLoad)),
                (sizeof (tIdPayLoad) - sizeof (tGenericPayLoad)));

    /* Advance the pos */
    *pu4Pos += sizeof (tIdPayLoad);

    if (pSesInfo->pIkeSA->InitiatorID.i4IdType == IKE_IPSEC_ID_DER_ASN1_DN)
    {
        /* Copy the ID Value to Message */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) (i1DerId), IdValue.u4Length);

        /* Copy ID Value to pu1IdPayloadSent and set the length */
        IKE_MEMCPY ((pSesInfo->pu1IdPayloadSent + sizeof (tIdPayLoad) -
                     sizeof (tGenericPayLoad)),
                    (UINT1 *) (i1DerId), IdValue.u4Length);

    }
    else
    {
        /* Copy the ID Value to Isakmp Mesg */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) (&IdValue.uID), IdValue.u4Length);

        /* Copy ID Value to pu1IdPayloadSent */
        IKE_MEMCPY ((pSesInfo->pu1IdPayloadSent + sizeof (tIdPayLoad) -
                     sizeof (tGenericPayLoad)),
                    (UINT1 *) (&IdValue.uID), IdValue.u4Length);
    }
    /* Set len of pu1IdPayloadSent */
    pSesInfo->u4IdPayloadSentLen = (UINT2) u4NBytes - sizeof (tGenericPayLoad);

    *pu4Pos += IdValue.u4Length;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructProxyID                                */
/*  Description     :This function is used to construct an Proxy Id for */
/*                  :a phase-2 exchange in an Isakmp Mesg               */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :Ident    :Contains Identity Information            */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the Position in an Isakmp Mesg */
/*                  :where the paylaod is to be appended                */
/*                  :                                                   */
/*  Output(s)       :Appends the Payload in the Isakmp Mesg             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT1
IkeConstructProxyID (tIkeSessionInfo * pSesInfo, tIkeNetwork * pPhase2Id,
                     UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIdPayLoad          ID;
    UINT2               u2NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &ID, sizeof (tIdPayLoad));

    u2NBytes = sizeof (tIdPayLoad);

    switch (pPhase2Id->u4Type)
    {
        case IKE_IPSEC_ID_IPV4_ADDR:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIp4Addr));
            pPhase2Id->uNetwork.Ip4Addr =
                IKE_HTONL (pPhase2Id->uNetwork.Ip4Addr);
            break;

        case IKE_IPSEC_ID_IPV6_ADDR:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIp6Addr));
            break;

        case IKE_IPSEC_ID_IPV4_SUBNET:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIkeIpv4Subnet));
            pPhase2Id->uNetwork.Ip4Subnet.Ip4Addr =
                IKE_HTONL (pPhase2Id->uNetwork.Ip4Subnet.Ip4Addr);
            pPhase2Id->uNetwork.Ip4Subnet.Ip4SubnetMask =
                IKE_HTONL (pPhase2Id->uNetwork.Ip4Subnet.Ip4SubnetMask);
            break;

        case IKE_IPSEC_ID_IPV6_SUBNET:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIkeIpv6Subnet));
            break;

        case IKE_IPSEC_ID_IPV4_RANGE:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIkeIpv4Range));
            pPhase2Id->uNetwork.Ip4Range.Ip4StartAddr =
                IKE_HTONL (pPhase2Id->uNetwork.Ip4Range.Ip4StartAddr);
            pPhase2Id->uNetwork.Ip4Range.Ip4EndAddr =
                IKE_HTONL (pPhase2Id->uNetwork.Ip4Range.Ip4EndAddr);
            break;

        case IKE_IPSEC_ID_IPV6_RANGE:
            u2NBytes = (UINT2) (u2NBytes + sizeof (tIkeIpv6Range));
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "Not Supported!\n");
            return (IKE_FAILURE);
    }

    /* Fill the ID payload with the Recieved values from the data structure 
       tIdentity */

    ID.u1NextPayLoad = u1NextPayLoad;
    ID.u2PayLoadLen = IKE_HTONS (u2NBytes);
    ID.u1IdType = (UINT1) pPhase2Id->u4Type;
    ID.u1Protocol = pPhase2Id->u1HLProtocol;

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u2NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Fill the Isakmp Message with the Proxy ID */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &ID, sizeof (tIdPayLoad));
    *pu4Pos += sizeof (tIdPayLoad);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) (&pPhase2Id->uNetwork),
                u2NBytes - sizeof (tIdPayLoad));
    *pu4Pos += u2NBytes - sizeof (tIdPayLoad);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructNotify                                 */
/*  Description     :This function is used to construct notify paylaod  */
/*                  :in an Isakmp Mesg                                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  : Pointer to the Session Info DataBase   */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the Position in an Isakmp Mesg */
/*                  :where the payload is to be appended                */
/*                  :pNotifyInfo - pointer to notification information  */
/*                  :                                                   */
/*  Output(s)       :Appends the Payload in an Isakmp Message           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructNotify (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                    UINT4 *pu4Pos, tIkeNotifyInfo * pNotifyInfo)
{
    tNotifyPayLoad      Notify;
    INT4                i4NBytes = IKE_ZERO;
    UINT4               u4TempLen = IKE_ZERO;
    i4NBytes = (INT4) (sizeof (tNotifyPayLoad) + pNotifyInfo->u2NotifyDataLen);
    IKE_BZERO ((UINT1 *) &Notify, sizeof (tNotifyPayLoad));

    /*  Fill the Notify PayLoad with the Recieved values */
    Notify.u4Doi = IKE_HTONL (IKE_IPSEC_DOI);
    Notify.u1NextPayLoad = u1NextPayLoad;
    Notify.u1SpiSize = pNotifyInfo->u1SpiSize;
    Notify.u1ProtocolId = pNotifyInfo->u1Protocol;
    Notify.u2NotifyMsg = IKE_HTONS (pNotifyInfo->u2NotifyType);
    Notify.u2PayLoadLen = (UINT2) IKE_HTONS (i4NBytes);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) i4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNotify:Out Of Memory\n");
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Notify, sizeof (tNotifyPayLoad));
    *pu4Pos += sizeof (tNotifyPayLoad);

    /* Copy the Error or Informational Mesg Recieved in addition to
     * Notify Mesg */
    if (pNotifyInfo->pu1NotifyData != NULL)
    {
        u4TempLen = MEM_MAX_BYTES ((UINT4) pNotifyInfo->u2NotifyDataLen,
                                   STRLEN (pNotifyInfo->pu1NotifyData));
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) pNotifyInfo->pu1NotifyData, u4TempLen);

        *pu4Pos += pNotifyInfo->u2NotifyDataLen;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructDelete                                 */
/*  Description     :This function is used to construct a delete paylaod*/
/*                  :in an Isakmp message                               */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to session Info data base       */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Identifies the position in an Isakmp Mesg */
/*                  :where the packet is to be appended                 */
/*                  :pDeleteInfo : Information for constructing delete  */
/*                  :payload                                            */
/*                  :                                                   */
/*  Output(s)       :Appends the Delete PayLoad in an Isakmp Message    */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructDelete (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                    UINT4 *pu4Pos, tIkeNotifyInfo * pDeleteInfo)
{
    tDeletePayLoad      DelPayload;
    UINT4               u4TempLen = IKE_ZERO;
    IKE_BZERO ((UINT1 *) &DelPayload, sizeof (tDeletePayLoad));

    /* Fill the Delete PayLoad with the Recieved values */

    DelPayload.u4Doi = IKE_HTONL (IKE_IPSEC_DOI);
    DelPayload.u1NextPayLoad = u1NextPayLoad;
    DelPayload.u2PayLoadLen = (UINT2)
        (sizeof (tDeletePayLoad) + pDeleteInfo->u2NotifyDataLen);
    DelPayload.u1ProtocolId = pDeleteInfo->u1Protocol;
    DelPayload.u1SpiSize = pDeleteInfo->u1SpiSize;
    DelPayload.u2NSpis = IKE_HTONS (pDeleteInfo->u2SpiNum);

    DelPayload.u2PayLoadLen = IKE_HTONS (DelPayload.u2PayLoadLen);

    if (ConstructIsakmpMesg
        (pSesInfo, (INT4) *pu4Pos,
         (sizeof (tDeletePayLoad) + (pDeleteInfo->u2NotifyDataLen))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Copy the delete payload to the Isakmp mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &DelPayload, sizeof (tDeletePayLoad));
    *pu4Pos += sizeof (tDeletePayLoad);
    if (pDeleteInfo->pu1NotifyData != NULL)
    {
        u4TempLen = MEM_MAX_BYTES ((UINT4) pDeleteInfo->u2NotifyDataLen,
                                   STRLEN (pDeleteInfo->pu1NotifyData));
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) pDeleteInfo->pu1NotifyData, u4TempLen);
        *pu4Pos += pDeleteInfo->u2NotifyDataLen;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpSA                               */
/*  Description     :This function is used to construct SA Payload for  */
/*                  :Isakmp Message (Phase-1 exchange)                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :which contains the outgoining packet               */
/*                  :u1NextPayLoad : Identifies the next paylaod in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in Isakmp Mesg     */
/*                  :where the payload is to be appended                */
/*                  :                                                   */
/*  Output(s)       :Appends the Sa PayLoad in the Isakmp Mesg for      */
/*                  : Phase-1                                           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpSA (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                      UINT4 *pu4Pos)
{

    tSaPayLoad          IsakSa;
    INT4                i4SavePos = IKE_ZERO,
        i4FixPos = IKE_ZERO, i4NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &IsakSa, sizeof (tSaPayLoad));
    i4SavePos = (INT4) *pu4Pos;
    if (ConstructIsakmpMesg (pSesInfo, i4SavePos,
                             sizeof (tSaPayLoad)) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    *pu4Pos = (UINT4) (*pu4Pos + sizeof (tSaPayLoad));
    i4FixPos = (INT4) *pu4Pos;

    IsakSa.u1NextPayLoad = u1NextPayLoad;
    IsakSa.u2PayLoadLen = IKE_HTONS (sizeof (tSaPayLoad));
    IsakSa.u4Doi = IKE_HTONL (IKE_IPSEC_DOI);
    IsakSa.u4Situation = IKE_HTONL (SIT_IDENTITY_ONLY);

    IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos),
                (UINT1 *) &IsakSa, sizeof (tSaPayLoad));

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        i4NBytes = IkeSetProposal (pSesInfo, i4FixPos, pu4Pos, IKE_PHASE1,
                                   IKE_ONE, IKE_NONE_PAYLOAD, IKE_NONE,
                                   IKE_NONE);
        if (i4NBytes == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

        IsakSa.u2PayLoadLen =
            (UINT2) IKE_HTONS (((UINT2) i4NBytes + sizeof (tSaPayLoad)));
        IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt + (UINT2) i4SavePos +
                    IKE_SA_LEN_OFFSET, &IsakSa.u2PayLoadLen,
                    sizeof (IsakSa.u2PayLoadLen));

        /* Copy the Offered/Selected SA into pSesInfo->pSAOffered 
         * This will be used for Hash Computation
         */
        pSesInfo->u2SALength = (UINT2)
            ((UINT2) i4NBytes + sizeof (tSaPayLoad) - sizeof (tGenericPayLoad));
        if (pSesInfo->pSAOffered == NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pSesInfo->pSAOffered) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeConstructIsakmpSA: unable to allocate "
                             "buffer\n");
                return IKE_FAILURE;
            }
        }
        if (pSesInfo->pSAOffered == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIsakmpSA:Out Of Memory\n");
            return (IKE_FAILURE);
        }
        IKE_MEMCPY (pSesInfo->pSAOffered,
                    (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                     sizeof (tGenericPayLoad)), pSesInfo->u2SALength);
    }
    else
    {
        if (ConstructIsakmpMesg (pSesInfo,
                                 sizeof (tIsakmpHdr) + sizeof (tSaPayLoad),
                                 pSesInfo->u2SaSelectedLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIsakmpSA:Out Of Memory\n");
            return IKE_FAILURE;
        }
        IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr)
                    + sizeof (tSaPayLoad),
                    pSesInfo->pu1SaSelected, pSesInfo->u2SaSelectedLen);
        IsakSa.u2PayLoadLen = IKE_HTONS ((sizeof (tSaPayLoad) +
                                          pSesInfo->u2SaSelectedLen));
        IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                     IKE_SA_LEN_OFFSET),
                    &IsakSa.u2PayLoadLen, sizeof (IsakSa.u2PayLoadLen));
        *pu4Pos += pSesInfo->u2SaSelectedLen;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeSetProposal                                     */
/*  Description     :This function sets the proposal payload based on   */
/*                  :the data base                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :i4FixPos : The position in the Isakmp Message      */
/*                  :pu4Pos   : The position of the last payload in the */
/*                  :Isakmp Message                                     */
/*                  :u1Num      : Refres to Phase 1 or 2  Protocol      */
/*                  :u2PropNum  : Identifies the Proposal               */
/*                  :                                                   */
/*  Output(s)       :Construct Proposal PayLoad                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetProposal (tIkeSessionInfo * pSessionInfo, INT4 i4FixPos,
                UINT4 *pu4Pos, UINT1 u1Num, UINT2 u2PropNum,
                UINT1 u1NextPayLoad, UINT1 u1Protocol, UINT1 u1TransNum)
{

    tProposalPayLoad    SaProp;
    UINT1              *pu1Atts = NULL;
    UINT4               u4Spi = IKE_ZERO;
    INT4                i4Count = IKE_ONE, i4NBytes = IKE_ZERO,
        i4NTrans = IKE_ONE;
    INT4                i4AttsLen = IKE_ZERO, i4Offset = IKE_ZERO;
    UINT1               u1Algo = IKE_ZERO, u1TransAlgo = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &SaProp, sizeof (tProposalPayLoad));

    if (ConstructIsakmpMesg (pSessionInfo, (INT4) *pu4Pos,
                             sizeof (tProposalPayLoad)) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Not enough memory!\n");
        return IKE_FAILURE;
    }

    *pu4Pos += sizeof (tProposalPayLoad);
    i4NBytes = sizeof (tProposalPayLoad);

    if (u1Num != IKE_PHASE1)
    {
        if (ConstructIsakmpMesg (pSessionInfo, (INT4) *pu4Pos,
                                 sizeof (UINT4)) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetProposal: Not enough memory!\n");
            return IKE_FAILURE;
        }
        *pu4Pos = (UINT4) (*pu4Pos + sizeof (UINT4));
        i4NBytes = (INT4) (i4NBytes + (INT4) sizeof (UINT4));
    }

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (u1Num == IKE_PHASE1)
    {
        u1Algo = pSessionInfo->IkePhase1Policy.u1HashAlgo;
        if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
        {
            if (pSessionInfo->IkePhase1Policy.u1HashAlgo == HMAC_SHA_256)
            {
                u1Algo = HMAC_PH1_SHA_256;

            }
            else if (pSessionInfo->IkePhase1Policy.u1HashAlgo == HMAC_SHA_384)
            {
                u1Algo = HMAC_PH1_SHA_384;
            }
            else if (pSessionInfo->IkePhase1Policy.u1HashAlgo == HMAC_SHA_512)
            {
                u1Algo = HMAC_PH1_SHA_512;
            }

        }
        if (IkeSetIsakmpAtts (pSessionInfo, &pu1Atts, u1Algo, &i4AttsLen)
            == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetProposal: IkeSetIsakmpAtts failed!\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
            return IKE_FAILURE;
        }
        /* XXX : When we support more than one transforms
         * in Phase1, we have to set Next Payload to Transform
         * for all transforms other than the last one
         */
        i4Offset =
            IkeSetTransform (pSessionInfo, (i4FixPos + i4NBytes),
                             pu4Pos, IKE_NONE_PAYLOAD,
                             (UINT1) i4Count, IKE_TRANS_ID_KEY_OAKLEY,
                             pu1Atts, i4AttsLen);
        if (i4Offset == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetProposal: IkeSetTransform failed!\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
            return IKE_FAILURE;
        }
    }
    else
    {
        pSessionInfo->IkePhase2Info.u1Protocol = u1Protocol;

        switch (u1Protocol)
        {
            case IKE_IPSEC_AH:
                if (pSessionInfo->IkePhase2Policy.
                    aTransformSetBundle[u1TransNum]->
                    u1AhHashAlgo == IKE_IPSEC_AH_SHA1)
                {
                    u1Algo = IKE_IPSEC_HMAC_SHA1;
                    u1TransAlgo = IKE_IPSEC_AH_SHA1;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1AhHashAlgo == IKE_IPSEC_AH_MD5)
                {
                    u1Algo = IKE_IPSEC_HMAC_MD5;
                    u1TransAlgo = IKE_IPSEC_AH_MD5;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1AhHashAlgo == SEC_XCBCMAC)
                {
                    u1Algo = SEC_XCBCMAC;
                    u1TransAlgo = SEC_XCBCMAC;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1AhHashAlgo == HMAC_SHA_256)
                {
                    u1Algo = HMAC_SHA_256;
                    u1TransAlgo = HMAC_SHA_256;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1AhHashAlgo == HMAC_SHA_384)
                {
                    u1Algo = HMAC_SHA_384;
                    u1TransAlgo = HMAC_SHA_384;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1AhHashAlgo == HMAC_SHA_512)
                {
                    u1Algo = HMAC_SHA_512;
                    u1TransAlgo = HMAC_SHA_512;
                }

                if (IkeSetPhase2Atts (pSessionInfo, &pu1Atts, &i4AttsLen,
                                      u1Algo, u1TransNum) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetProposal: IkeSetPhase2Atts failed!\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return IKE_FAILURE;
                }

                i4Offset =
                    IkeSetTransform (pSessionInfo, (i4FixPos + i4NBytes),
                                     pu4Pos, IKE_NONE_PAYLOAD,
                                     (UINT1) i4Count, u1TransAlgo, pu1Atts,
                                     i4AttsLen);

                if (i4Offset == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetProposal: IkeSetTransform failed!\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return IKE_FAILURE;
                }
                break;

            case IKE_IPSEC_ESP:
                u1Algo = pSessionInfo->IkePhase2Policy.
                    aTransformSetBundle[u1TransNum]->u1EspHashAlgo;

                if (pSessionInfo->IkePhase2Policy.
                    aTransformSetBundle[u1TransNum]->
                    u1EspHashAlgo == HMAC_SHA_256)
                {
                    u1Algo = HMAC_PH2_SHA_256;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1EspHashAlgo == HMAC_SHA_384)
                {
                    u1Algo = HMAC_PH2_SHA_384;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1EspHashAlgo == HMAC_SHA_512)
                {
                    u1Algo = HMAC_PH2_SHA_512;
                }
                else if (pSessionInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum]->
                         u1EspHashAlgo == SEC_XCBCMAC)
                {
                    u1Algo = HMAC_PH2_XCBCMAC;
                }

                if (IkeSetPhase2Atts (pSessionInfo, &pu1Atts, &i4AttsLen,
                                      u1Algo, u1TransNum) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetProposal: IkeSetPhase2Atts failed!\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return IKE_FAILURE;
                }

                i4Offset =
                    IkeSetTransform (pSessionInfo, (i4FixPos + i4NBytes),
                                     pu4Pos, IKE_NONE_PAYLOAD,
                                     (UINT1) i4Count,
                                     pSessionInfo->IkePhase2Policy.
                                     aTransformSetBundle[u1TransNum]->
                                     u1EspEncryptionAlgo, pu1Atts, i4AttsLen);

                if (i4Offset == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetProposal: IkeSetTransform failed!\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return IKE_FAILURE;
                }
                break;

            default:
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetProposal:Bad Protocol number\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                return IKE_FAILURE;
        }
    }
    i4NBytes += i4Offset;
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    SaProp.u1NextPayLoad = u1NextPayLoad;
    SaProp.u2PayLoadLen = (UINT2) i4NBytes;
    SaProp.u1PropNum = (UINT1) u2PropNum;
    if (u1Num == IKE_PHASE1)
    {
        SaProp.u1ProtocolId = IKE_ISAKMP;
    }
    else
    {
        SaProp.u1ProtocolId = u1Protocol;
    }
    SaProp.u1NumTran = (UINT1) i4NTrans;

    if (SaProp.u1ProtocolId != IKE_ISAKMP)
    {
        SaProp.u1SpiSize = sizeof (UINT4);
        /* Store the InBound SPI */
        if (u1Protocol == IKE_IPSEC_AH)
        {
            u4Spi = pSessionInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4Spi;
        }
        else if (u1Protocol == IKE_IPSEC_ESP)
        {
            u4Spi = pSessionInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetProposal:Bad Protocol ID\n");
            return (IKE_FAILURE);
        }
        u4Spi = IKE_HTONL (u4Spi);
    }
    else
    {
        SaProp.u1SpiSize = IKE_ZERO;
    }

    SaProp.u2PayLoadLen = IKE_HTONS (SaProp.u2PayLoadLen);
    IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos),
                (UINT1 *) &SaProp, sizeof (tProposalPayLoad));
    if (SaProp.u1ProtocolId != IKE_ISAKMP)
    {
        IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos +
                     sizeof (tProposalPayLoad)), (UINT1 *) &u4Spi,
                    sizeof (UINT4));
    }

    return (INT1) (i4NBytes);
}

/************************************************************************/
/*  Function Name   :IkeSetIsakmpAtts                                   */
/*  Description     :This function is used to set Phase-1 parameters    */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1PayLoad : Pointer refering to the pointer      */
/*                  :pointing to the payload                            */
/*                  :pi4PayLoadLen : Pointer to the payload len         */
/*                  :                                                   */
/*  Output(s)       :Constructs the phase-1 attributes                  */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetIsakmpAtts (tIkeSessionInfo * pSesInfo, UINT1 **ppu1PayLoad,
                  UINT1 u1Algo, INT4 *pi4PayLoadLen)
{
    INT4                i4Len = IKE_ZERO;
    UINT4               u4LifeTime = IKE_ZERO;
    UINT2               u2AuthMethod = IKE_ZERO;

    IKE_BZERO ((UINT1 *) *ppu1PayLoad,
               ((UINT2) *pi4PayLoadLen * sizeof (UINT1)));

    if (pSesInfo->IkePhase1Policy.u4LifeTime != IKE_ZERO)
    {
        /* Sa Life Type */
        if (BasicAtt (IKE_SA_LIFE_TYPE_ATTRIB, IKE_LIFE_SECONDS, ppu1PayLoad,
                      &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

        /* Sa Life Duration */
        u4LifeTime = IkeGetPhase1LifeTime (&pSesInfo->IkePhase1Policy);

        if (u4LifeTime < IKE_MAX_BASIC_ATTR_LEN)
        {
            UINT2               u2LifeTime = IKE_ZERO;

            u2LifeTime = (UINT2) u4LifeTime;
            if (BasicAtt (IKE_SA_LIFE_DUR_ATTRIB, u2LifeTime,
                          ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetIsakmpAtts: Unable to set Basic Att\n");
                return IKE_FAILURE;
            }
        }
        else
        {
            u4LifeTime = IKE_HTONL (u4LifeTime);
            if (VpiAtt
                (IKE_SA_LIFE_DUR_ATTRIB,
                 (UINT1 *) &u4LifeTime, sizeof (UINT4),
                 ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetIsakmpAtts: Unable to set Var Att\n");
                return IKE_FAILURE;
            }
        }
    }

    if (pSesInfo->IkePhase1Policy.u4LifeTimeKB != IKE_ZERO)
    {
        if (BasicAtt (IKE_SA_LIFE_TYPE_ATTRIB, IKE_LIFE_KILOBYTES,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

        /* Sa Life Duration */
        if (VpiAtt
            (IKE_SA_LIFE_DUR_ATTRIB, (UINT1 *) &pSesInfo->IkePhase2Policy.
             u4LifeTimeKB, sizeof (UINT4), ppu1PayLoad, &i4Len,
             pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    /* Diffie Hellman Group */
    if (BasicAtt (IKE_GROUP_DESC_ATTRIB, pSesInfo->IkePhase1Policy.u1DHGroup,
                  ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Encryption Algorithm */
    if (BasicAtt (IKE_ENCR_ALGO_ATTRIB,
                  pSesInfo->IkePhase1Policy.u1EncryptionAlgo,
                  ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Hash Algorithm */
    if (u1Algo != IKE_ZERO)
        if (BasicAtt (IKE_HASH_ALGO_ATTRIB, u1Algo,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

    /* Authentication Method */
    u2AuthMethod = pSesInfo->IkePhase1Policy.u2AuthMethod;
    if (pSesInfo->IkePhase1Policy.u2AuthMethod == IKE_PRESHARED_KEY)
    {
        if (pSesInfo->pIkeSA->bXAuthEnabled)
        {
            if (gbXAuthServer == TRUE)
            {
                IKE_ASSERT (pSesInfo->u1Role == IKE_RESPONDER);
                u2AuthMethod = IKE_XAUTH_INIT_PRESHARED_KEY;
            }
            else
            {
                IKE_ASSERT (pSesInfo->u1Role == IKE_INITIATOR);
                u2AuthMethod = IKE_XAUTH_INIT_PRESHARED_KEY;
            }
        }
    }

    if (BasicAtt
        (IKE_AUTH_METHOD_ATTRIB, u2AuthMethod,
         ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /*KEY LENGTH IN CASE OF AES */
    if (pSesInfo->IkePhase1Policy.u1EncryptionAlgo == IKE_AES)
    {
        if (BasicAtt
            (IKE_SA_KEY_LEN_ATTRIB, pSesInfo->IkePhase1Policy.u2KeyLen,
             ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

    }

    *pi4PayLoadLen = i4Len;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeSetTransform                                    */
/*  Description     :This function constructs the transform payload     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :i4FixPos : Refers to the position in Message       */
/*                  :pu4Pos : Refers to the position of the last        */
/*                  :payload in the isakmp message                      */
/*                  :u1TransNum : Identifies the Transform              */
/*                  :u1Transform : Specifies the Algorithm              */
/*                  :pu1Atts : Pointer to the Attributes                */
/*                  :i4AttsLen : Length of the Attributes               */
/*                  :                                                   */
/*  Output(s)       :Constructs the transform payload                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSetTransform (tIkeSessionInfo * pSesInfo, INT4 i4FixPos, UINT4 *pu4Pos,
                 UINT1 u1NextPayLoad, UINT1 u1TransNum,
                 UINT1 u1Transform, UINT1 *pu1Atts, INT4 i4AttsLen)
{
    tTransPayLoad       Trans;
    UINT4               u4NBytes = IKE_ZERO;

    u4NBytes = (UINT4) ((INT4) sizeof (tTransPayLoad) + i4AttsLen);
    IKE_BZERO ((UINT1 *) &Trans, sizeof (tTransPayLoad));

    Trans.u1NextPayLoad = u1NextPayLoad;
    Trans.u2PayLoadLen = (UINT2) u4NBytes;
    Trans.u1TransNum = u1TransNum;
    Trans.u1TransId = u1Transform;

    Trans.u2PayLoadLen = IKE_HTONS (Trans.u2PayLoadLen);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    *pu4Pos = (UINT4) (*pu4Pos + u4NBytes);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos),
                (UINT1 *) &Trans, sizeof (tTransPayLoad));
    i4FixPos = (INT4) (i4FixPos + (INT4) sizeof (tTransPayLoad));

    if (i4AttsLen != IKE_ZERO)
    {
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos),
                    (UINT1 *) pu1Atts, i4AttsLen);
    }

    return (INT1) (u4NBytes);
}

/************************************************************************/
/*  Function Name   :IkeConstructIpsecSA                                */
/*  Description     : This function is used to construct Ipsec Sa       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :pNode : Pointer to the session List                */
/*                  :                                                   */
/*  Output(s)       :Construct the Ipsec SA PayLoad                     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIpsecSA (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                     UINT4 *pu4Pos)
{
    tSaPayLoad          SaPayLoad;
    INT4                i4SavePos = IKE_ZERO, i4FixPos = IKE_ZERO,
        i4NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &SaPayLoad, sizeof (tSaPayLoad));
    i4SavePos = (INT4) *pu4Pos;
    if (ConstructIsakmpMesg (pSesInfo, i4SavePos, sizeof (tSaPayLoad))
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIpsecSA: ConstructIsakmpMesg failed!\n");
        return IKE_FAILURE;
    }

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        *pu4Pos += sizeof (tSaPayLoad);
        i4FixPos = (INT4) *pu4Pos;
        i4NBytes = IkeSetIpsecProposals (pSesInfo, i4FixPos, pu4Pos,
                                         IKE_NONE_PAYLOAD);
        if (i4NBytes == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIpsecSA: IkeSetIpsecProposals failed\n");
            return IKE_FAILURE;
        }
    }
    else
    {
        *pu4Pos += sizeof (tSaPayLoad);
        if (ConstructIsakmpMesg
            (pSesInfo, (INT4) *pu4Pos,
             pSesInfo->u2SaSelectedLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIpsecSA: Not enough memory\n");
            return IKE_FAILURE;
        }

        IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                     sizeof (tSaPayLoad)), pSesInfo->pu1SaSelected,
                    pSesInfo->u2SaSelectedLen);
        i4NBytes = pSesInfo->u2SaSelectedLen;
        *pu4Pos = (UINT4) (*pu4Pos + (UINT4) i4NBytes);
    }

    SaPayLoad.u1NextPayLoad = u1NextPayLoad;
    SaPayLoad.u2PayLoadLen = (UINT2) ((UINT2) i4NBytes + sizeof (tSaPayLoad));
    SaPayLoad.u2PayLoadLen = IKE_HTONS (SaPayLoad.u2PayLoadLen);
    SaPayLoad.u4Doi = IKE_HTONL (IKE_IPSEC_DOI);
    SaPayLoad.u4Situation = IKE_HTONL (SIT_IDENTITY_ONLY);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos),
                (UINT1 *) &SaPayLoad, sizeof (tSaPayLoad));
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeSetIpsecProposals                               */
/*  Description     :This function is used to construct Ipsec Proposals */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :i4FixPos : Refers to the fixed position in the     */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the position in the Isakmp Mesg*/
/*                  :u1NextPayLoad: Next payload type                   */
/*                  :                                                   */
/*  Output(s)       :Constructs Ipsec Proposals                         */
/*                  :                                                   */
/*  Returns         : The no of Bytes added to the message              */
/*                  :                                                   */
/************************************************************************/

INT4
IkeSetIpsecProposals (tIkeSessionInfo * pSesInfo, INT4 i4FixPos, UINT4 *pu4Pos,
                      UINT1 u1NextPayload)
{
    INT4                i4TotBytes = IKE_ZERO, i4NBytes = IKE_ZERO;
    UINT2               u2PropNum = IKE_ZERO;
    UINT1               u1TransNum = IKE_ZERO;

    for (u1TransNum = IKE_ZERO; u1TransNum < MAX_TRANSFORMS; u1TransNum++)
    {
        u2PropNum = (UINT2) (u1TransNum + IKE_ONE);
        if (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum] != NULL)
        {

            if (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                u1EspEncryptionAlgo != IKE_ZERO)
            {

                if (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                    u1AhHashAlgo != IKE_ZERO)
                {
                    u1NextPayload = IKE_PROPOSAL_PAYLOAD;
                }
                else
                {
                    u1NextPayload = IKE_NONE_PAYLOAD;
                }
                if (u1TransNum < (MAX_TRANSFORM_INDEX - IKE_ONE))
                {
                    if ((pSesInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum + IKE_ONE]
                         != NULL) &&
                        (pSesInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum +
                                             IKE_ONE]->u1Status == IKE_ACTIVE))
                    {
                        u1NextPayload = IKE_PROPOSAL_PAYLOAD;
                    }
                }
                i4NBytes = IkeSetProposal (pSesInfo, (i4FixPos + i4TotBytes),
                                           pu4Pos, IKE_PHASE2, u2PropNum,
                                           u1NextPayload, IKE_IPSEC_ESP,
                                           u1TransNum);
                if (i4NBytes == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetIpsecProposals: IkeSetProposal "
                                 "returned zero!\n");
                    return IKE_ZERO;
                }
                i4TotBytes += i4NBytes;
            }
            if (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                u1AhHashAlgo != IKE_ZERO)
            {
                if (u1TransNum < MAX_TRANSFORM_INDEX)
                {
                    if ((pSesInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum + IKE_ONE]
                         != NULL) &&
                        (pSesInfo->IkePhase2Policy.
                         aTransformSetBundle[u1TransNum +
                                             IKE_ONE]->u1Status == IKE_ACTIVE))
                    {
                        u1NextPayload = IKE_PROPOSAL_PAYLOAD;
                    }
                    else
                    {
                        u1NextPayload = IKE_NONE_PAYLOAD;
                    }
                }
                else
                {
                    u1NextPayload = IKE_NONE_PAYLOAD;
                }
                i4NBytes = IkeSetProposal (pSesInfo, (i4FixPos + i4TotBytes),
                                           pu4Pos, IKE_PHASE2, u2PropNum,
                                           u1NextPayload, IKE_IPSEC_AH,
                                           u1TransNum);
                if (i4NBytes == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetIpsecProposals: IkeSetProposal "
                                 "returned zero!\n");
                    return IKE_ZERO;
                }
                i4TotBytes += i4NBytes;
            }
        }

    }
    return i4TotBytes;
}

/************************************************************************/
/*  Function Name   :BasicAtt                                           */
/*  Description     :This function is used to construct basic attributes*/
/*                  :                                                   */
/*  Input(s)        :i4Class : Refers to the attribute class            */
/*                  :i4Type : Refers to the attribute type              */
/*                  :ppu1P  :Pointer to the PayLoad                     */
/*                  :pi4Len : Refers to the length of attributes        */
/*                  :pu1PayLoadLen : Length of the PayLoad              */
/*                  :                                                   */
/*  Output(s)       :Constructs the basic attributes                    */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
BasicAtt (UINT2 u2Class, INT4 i4Type, UINT1 **ppu1P, INT4 *pi4Len,
          INT4 *pu1PayLoadLen)
{

    tSaBasicAttribute   SaBasicAtt;

    if ((UINT1) ((UINT2) *pi4Len + sizeof (tSaBasicAttribute)) > *pu1PayLoadLen)
    {
        UINT4               u4NewLength = IKE_ZERO;

        u4NewLength =
            (UINT2) *pi4Len + sizeof (tSaBasicAttribute) + IKE_MAX_ATTR_SIZE;
        if (*ppu1P == NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &(*ppu1P)) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "BasicAtt: unable to allocate " "buffer\n");
                return IKE_FAILURE;
            }
            if ((*ppu1P) == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "BasicAtt: Realloc fails\n");
                return IKE_FAILURE;
            }
        }
        *pu1PayLoadLen = (UINT1) u4NewLength;
    }

    IKE_BZERO ((UINT1 *) &SaBasicAtt, sizeof (tSaBasicAttribute));

    SaBasicAtt.u2Attr = u2Class;
    SaBasicAtt.u2Attr = SaBasicAtt.u2Attr | IKE_BASIC_ATTR;
    SaBasicAtt.u2Attr = IKE_HTONS (SaBasicAtt.u2Attr);

    SaBasicAtt.u2BasicValue = (UINT2) i4Type;

    SaBasicAtt.u2BasicValue = IKE_HTONS (SaBasicAtt.u2BasicValue);

    IKE_MEMCPY ((*ppu1P + *pi4Len), (UINT1 *) &SaBasicAtt,
                sizeof (tSaBasicAttribute));
    *pi4Len = (INT4) ((UINT2) *pi4Len + sizeof (tSaBasicAttribute));
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :VpiAtt                                             */
/*  Description     :This function is used to construct vpi attributes  */
/*                  :                                                   */
/*  Input(s)        :i4Class : Refers to the attribute class            */
/*                  :pu1Val : Refers to the attribute type              */
/*                  :ppu1P   : Pointer to the PayLoad                   */
/*                  :pi4Len : Refers to the length of attributes        */
/*                  :pu1PayLoadLen : Length of the PayLoad              */
/*                  :                                                   */
/*  Output(s)       :Constructs the Vpi attributes                      */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/*                  :                                                   */
/************************************************************************/

INT1
VpiAtt (UINT2 u2Class, UINT1 *pu1Val, UINT2 u2Size, UINT1 **ppu1P,
        INT4 *pi4Len, INT4 *pi4PayLoadLen)
{
    tSaVpiAttribute     SaVpiAtt;
    UINT2               u2NBytes = IKE_ZERO;

    u2NBytes = u2Size;
    while ((u2NBytes % sizeof (UINT4)) != IKE_ZERO)
    {
        u2NBytes++;
    }

    if ((INT4) ((UINT2) *pi4Len + sizeof (tSaVpiAttribute) + u2NBytes) >
        *pi4PayLoadLen)
    {
        UINT4               u4NewLength = IKE_ZERO;

        u4NewLength = (UINT2) *pi4Len + sizeof (tSaVpiAttribute) + u2NBytes +
            IKE_MAX_ATTR_SIZE;

        if (*ppu1P == NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &(*ppu1P)) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "BasicAtt: unable to allocate " "buffer\n");
                return IKE_FAILURE;
            }
            if ((*ppu1P) == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "BasicAtt: Realloc fails\n");
                return IKE_FAILURE;
            }
        }
        *pi4PayLoadLen = (INT4) u4NewLength;
    }
    IKE_BZERO ((UINT1 *) (*ppu1P + *pi4Len),
               (u2NBytes + sizeof (tSaVpiAttribute)));
    IKE_BZERO ((UINT1 *) &SaVpiAtt, sizeof (tSaVpiAttribute));

    SaVpiAtt.u2Attr = u2Class;
    SaVpiAtt.u2Attr = IKE_VAR_ATTR & u2Class;
    SaVpiAtt.u2Attr = IKE_HTONS (SaVpiAtt.u2Attr);
    SaVpiAtt.u2VpiLength = u2NBytes;
    SaVpiAtt.u2VpiLength = IKE_HTONS (SaVpiAtt.u2VpiLength);

    IKE_MEMCPY ((UINT1 *) (*ppu1P + *pi4Len), (UINT1 *) &SaVpiAtt,
                sizeof (tSaVpiAttribute));
    *pi4Len = (INT4) ((UINT2) *pi4Len + sizeof (tSaVpiAttribute));
    IKE_MEMCPY ((UINT1 *) (*ppu1P + *pi4Len +
                           (u2NBytes - u2Size)), (UINT1 *) pu1Val, u2NBytes);
    *pi4Len += u2NBytes;
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetPhase2Atts                                   */
/*  Description     :This function is used to construct Phase2          */
/*                   attributes                                         */
/*                  :                                                   */
/*  Input(s)        :pSessionInfo : Pointer to Session data structure   */
/*                  :ppu1PayLoad : Pointer to the payload               */
/*                  :pi4PayLoadLen : Pointer to the length of payload   */
/*                  :u1Algo : Authentication algorithm                  */
/*                  :                                                   */
/*  Output(s)       :Constructs Phase-2 Attributes                      */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetPhase2Atts (tIkeSessionInfo * pSessionInfo, UINT1 **ppu1PayLoad,
                  INT4 *pi4PayLoadLen, UINT1 u1Algo, UINT1 u1TransNum)
{

    INT4                i4Len = IKE_ZERO;
    UINT4               u4LifeTime = IKE_ZERO;
    UINT1               u1Mode = IKE_ZERO;

    IKE_BZERO ((UINT1 *) *ppu1PayLoad,
               ((UINT2) *pi4PayLoadLen * sizeof (UINT1)));

    if (u1TransNum >= MAX_TRANSFORMS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetPhase2Atts: Invalid TransformSetBundle Index\n");
        return IKE_FAILURE;
    }

    /* Sa Life Type */
    if (pSessionInfo->IkePhase2Policy.u4LifeTime != IKE_ZERO)
    {
        if (BasicAtt (IKE_IPSEC_SA_LIFE_TYPE_ATTRIB, IKE_LIFE_SECONDS,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetPhase2Atts: Unable to set Basic Att\n");
            return IKE_FAILURE;
        }

        /* Sa Life Duration */
        u4LifeTime = IkeGetPhase2LifeTime (&pSessionInfo->IkePhase2Policy);
        if (u4LifeTime < IKE_MAX_BASIC_ATTR_LEN)
        {
            UINT2               u2LifeTime = IKE_ZERO;

            u2LifeTime = (UINT2) u4LifeTime;
            if (BasicAtt (IKE_IPSEC_SA_LIFE_DUR_ATTRIB, u2LifeTime,
                          ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase2Atts: Unable to set Basic Att\n");
                return IKE_FAILURE;
            }
        }
        else
        {

            u4LifeTime = IKE_HTONL (u4LifeTime);
            if (VpiAtt (IKE_IPSEC_SA_LIFE_DUR_ATTRIB, (UINT1 *) &u4LifeTime,
                        sizeof (UINT4), ppu1PayLoad, &i4Len, pi4PayLoadLen)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase2Atts: Unable to set Var Att\n");
                return IKE_FAILURE;
            }
        }
    }

    if (pSessionInfo->IkePhase2Policy.u4LifeTimeKB != IKE_ZERO)
    {
        if (BasicAtt (IKE_IPSEC_SA_LIFE_TYPE_ATTRIB, IKE_LIFE_KILOBYTES,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

        /* Sa Life Duration */
        u4LifeTime = IKE_HTONL (pSessionInfo->IkePhase2Policy.u4LifeTimeKB);
        if (VpiAtt
            (IKE_IPSEC_SA_LIFE_DUR_ATTRIB, (UINT1 *) &u4LifeTime,
             sizeof (UINT4), ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    /* ESP MODE */

    u1Mode = pSessionInfo->IkePhase2Policy.u1Mode;
    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT)
    {
        if (u1Mode == IKE_IPSEC_TUNNEL_MODE)
        {
            u1Mode = IKE_IPSEC_UDP_TUNNEL_MODE;
        }
    }

    if ((pSessionInfo->IkePhase2Info.u1Protocol == IKE_IPSEC_AH) &&
        (pSessionInfo->IkePhase2Policy.bModeForEspOnly == IKE_TRUE) &&
        (pSessionInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
         u1EspEncryptionAlgo != IKE_ZERO))
    {
        if (BasicAtt
            (IKE_IPSEC_ENCAPSULATION_MODE_ATTRIB,
             u1Mode, ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    else
    {
        if (BasicAtt
            (IKE_IPSEC_ENCAPSULATION_MODE_ATTRIB,
             u1Mode, ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    /* Authentication Algorithm */
    if (u1Algo != IKE_ZERO)
    {
        if (BasicAtt (IKE_IPSEC_AUTH_ALGO_ATTRIB, u1Algo,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    /* pfs */
    if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
    {
        if (BasicAtt (IKE_IPSEC_GROUP_DESC_ATTRIB,
                      pSessionInfo->IkePhase2Policy.u1Pfs,
                      ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    if (pSessionInfo->IkePhase2Info.u1Protocol == IKE_IPSEC_ESP)
    {
        if ((pSessionInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
             u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES) ||
            (pSessionInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
             u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES_CTR))

        {
            /* If the encryption Algorithm is AES/AES-CTR then key length 
               also should be set as one of the basic attributes */

            if (BasicAtt (IKE_IPSEC_KEY_LEN_ATTRIB,
                          pSessionInfo->IkePhase2Policy.
                          aTransformSetBundle[u1TransNum]->u2KeyLen,
                          ppu1PayLoad, &i4Len, pi4PayLoadLen) == IKE_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
    }

    *pi4PayLoadLen = i4Len;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructQmHash1                                */
/*  Description     :This function is used to construct QM Hash1        */
/*                  :                                                   */
/*  Input(s)        :pSessionInfo : Pointer to Session data structure   */
/*                  :u1NextPayLoad : The next payload to be added       */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructQmHash1 (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad)
{
    UINT4               u4HashLen = IKE_ZERO;
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL;
    UINT1               au1Hash[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    UINT4               u4MesgId = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeTxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash1: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeConstructQmHash1: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 1 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | SA | Ni [ | KE ] [ | IDci | IDcr ] */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4),
                pSesInfo->IkeTxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         u2PayLoadLen + (UINT2) sizeof (UINT4), au1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash1: Unsupported Hmac algorithm\n");

        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr) + sizeof (tHashPayLoad)),
                au1Hash, u4HashLen);

    HashPayLoad.u1NextPayLoad = u1NextPayLoad;
    HashPayLoad.u1Reserved = IKE_ZERO;
    HashPayLoad.u2PayLoadLen = (UINT2) (sizeof (tHashPayLoad) + u4HashLen);
    HashPayLoad.u2PayLoadLen = IKE_HTONS (HashPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr)), &HashPayLoad,
                sizeof (tHashPayLoad));

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructQmHash2                                */
/*  Description     :This function is used to construct QM Hash2        */
/*  Input(s)        :pSessionInfo : Pointer to Session data structure   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT1
IkeConstructQmHash2 (tIkeSessionInfo * pSesInfo)
{
    UINT4               u4HashLen = IKE_ZERO;
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL;
    UINT1               au1Hash[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    UINT4               u4MesgId = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeTxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash2: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeConstructQmHash2: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 1 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | Ni_b | SA | Ni [ | KE ] [ | IDci | IDcr ] */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4), pSesInfo->pu1InitiatorNonce,
                pSesInfo->u2InitiatorNonceLen);

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4) + pSesInfo->u2InitiatorNonceLen,
                pSesInfo->IkeTxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         u2PayLoadLen + (UINT2) sizeof (UINT4) + pSesInfo->u2InitiatorNonceLen,
         au1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash2: Unsupported Hmac algorithm\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr) + sizeof (tHashPayLoad)),
                au1Hash, u4HashLen);

    HashPayLoad.u1NextPayLoad = IKE_SA_PAYLOAD;
    HashPayLoad.u1Reserved = IKE_ZERO;
    HashPayLoad.u2PayLoadLen = (UINT2) (sizeof (tHashPayLoad) + u4HashLen);
    HashPayLoad.u2PayLoadLen = IKE_HTONS (HashPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr)), &HashPayLoad,
                sizeof (tHashPayLoad));
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructQmHash3                                */
/*  Description     :This function is used to construct QM Hash3        */
/*  Input(s)        :pSessionInfo : Pointer to Session data structure   */
/*                  :u1NextPayLoad : The next payload to be added       */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT1
IkeConstructQmHash3 (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad)
{
    UINT2               u2BufLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL;
    UINT1               au1Hash[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    UINT4               u4MesgId = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u2BufLen =
        (UINT2) (IKE_ONE + sizeof (u4MesgId) + pSesInfo->u2ResponderNonceLen +
                 pSesInfo->u2InitiatorNonceLen);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash3: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeConstructQmHash3: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 1 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * 0 | M-ID | Ni_b | Nr_b */

    pu1BufPtr[IKE_INDEX_0] = IKE_ZERO;

    IKE_MEMCPY (pu1BufPtr + IKE_ONE, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + IKE_ONE +
                sizeof (UINT4), pSesInfo->pu1InitiatorNonce,
                pSesInfo->u2InitiatorNonceLen);

    IKE_MEMCPY (pu1BufPtr + IKE_ONE
                + sizeof (UINT4) + pSesInfo->u2InitiatorNonceLen,
                pSesInfo->pu1ResponderNonce, pSesInfo->u2ResponderNonceLen);

    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr, u2BufLen,
         au1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructQmHash3: Unsupported Hmac algorithm\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr) + sizeof (tHashPayLoad)),
                au1Hash, u4HashLen);

    HashPayLoad.u1NextPayLoad = u1NextPayLoad;
    HashPayLoad.u1Reserved = IKE_ZERO;
    HashPayLoad.u2PayLoadLen = (UINT2) (sizeof (tHashPayLoad) + u4HashLen);
    HashPayLoad.u2PayLoadLen = IKE_HTONS (HashPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr)), &HashPayLoad,
                sizeof (tHashPayLoad));

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSendNotifyInfo                                  */
/*  Description     :This function is called from ikecoreprocess when   */
/*                   notify error message is set during packet process  */
/*                   This function constructs notify packet and sends as*/
/*                   as informational exchange                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  : Pointer to the Session Info DataBase   */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the Position in an Isakmp Mesg */
/*                  :where the payload is to be appended                */
/*                  :pNotifyInfo - pointer to notification information  */
/*                  :                                                   */
/*  Output(s)       :Appends the Payload in an Isakmp Message           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSendNotifyInfo (tIkeSessionInfo * pSessionInfo)
{
    tIkeNotifyInfo     *pNotifyInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSA             *pIkeSA = NULL;

    /* Allocate memory for holding Notification information */
    if (MemAllocateMemBlock
        (IKE_NOTIFY_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pNotifyInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSendNotifyInfo: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pNotifyInfo == NULL)
    {
        return IKE_FAILURE;
    }

    IKE_BZERO (pNotifyInfo, sizeof (tIkeNotifyInfo));

    pNotifyInfo->u1Protocol = IKE_ISAKMP;
    pNotifyInfo->u2NotifyType = pSessionInfo->u2NotifyType;
    pNotifyInfo->u1SpiSize = IKE_NONE;

    if ((pSessionInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSessionInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        pNotifyInfo->u1CryptFlag = IKE_NONE;
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else if (pSessionInfo->u1ExchangeType == IKE_QUICK_MODE)
    {
        pNotifyInfo->u1CryptFlag = IKE_ENCRYPTION;
        /* Check if Phase1 IkeSA is existing */
        pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);

        if (pIkeEngine == NULL)
        {
            MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo);
            return IKE_FAILURE;
        }

        pIkeSA =
            IkeGetIkeSAFromPeerAddress (pIkeEngine,
                                        &pSessionInfo->IkePhase2Policy.
                                        PeerAddr);
        if (pIkeSA == NULL)
        {
            MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo);
            return IKE_FAILURE;
        }
    }
    /* IkeSA is existing, Proceed constructing notification payload */
    if (IkeConstructInfoExchPacket (pNotifyInfo, pIkeSA) == IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
        return IKE_FAILURE;
    }

    MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructInfoExchPacket                         */
/*  Description     :This function is used to construct notify/Delete   */
/*                   packet using information exchange                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  : Pointer to the Session Info DataBase   */
/*                  :u1NextPayLoad : Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Pointer to the Position in an Isakmp Mesg */
/*                  :where the payload is to be appended                */
/*                  :pNotifyInfo - pointer to notification information  */
/*                  :                                                   */
/*  Output(s)       :Appends the Payload in an Isakmp Message           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructInfoExchPacket (tIkeNotifyInfo * pNotifyInfo, tIkeSA * pIkeSA)
{

    tIkeSessionInfo    *pSessionInfo = NULL;
    UINT4               u4Size = IKE_ZERO, u4MsgId = IKE_ZERO,
        u4HashLen = IKE_ZERO;
    UINT1               u1NextPayLoad = IKE_ZERO;

    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructInfoExchPacket: IkeSA is NULL \n");
        return IKE_FAILURE;
    }
    if (pNotifyInfo->u1CryptFlag == IKE_ENCRYPTION)
    {
        /* If we are sending the message under IkeSA, it should be sent
         * under protection, in which case we have generate the message Id */
        IkeGetRandom ((UINT1 *) &u4MsgId, IKE_MSGID_LEN);
        pSessionInfo = IkeInfoExchInit (pIkeSA, u4MsgId, IKE_TRUE);

        if (pSessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Session Initialization "
                         "failed %d\n");
            return IKE_FAILURE;
        }
        u1NextPayLoad = IKE_HASH_PAYLOAD;
    }
    else
    {
        /* This session is sent under no protection, so generate the cookies 
         * fresh and the message in the cleartext */
        pSessionInfo = IkeInfoExchInit (pIkeSA, IKE_ZERO, IKE_FALSE);

        if (pSessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Session Initialization "
                         "failed %d\n");
            return IKE_FAILURE;
        }
        IkeGetRandom ((UINT1 *) &pSessionInfo->pIkeSA->au1InitiatorCookie,
                      IKE_COOKIE_LEN);
        IKE_BZERO (&pSessionInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
        u1NextPayLoad = IKE_NOTIFY_PAYLOAD;
    }

    if (IkeConstructHeader (pSessionInfo, IKE_ISAKMP_INFO,
                            u1NextPayLoad) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructInfoExchPacket: Isakmp Header construct "
                     "Failed %d\n");
        IkeDeleteSession (pSessionInfo);
        return IKE_FAILURE;
    }

    u4Size = sizeof (tIsakmpHdr);

    if (pNotifyInfo->u1CryptFlag == IKE_ENCRYPTION)
    {
        u4HashLen =
            gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
        u4Size += u4HashLen + sizeof (tHashPayLoad);

        if (ConstructIsakmpMesg (pSessionInfo,
                                 sizeof (tIsakmpHdr),
                                 (u4HashLen + sizeof (tHashPayLoad))) ==
            IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Memory not available\n");
            IkeDeleteSession (pSessionInfo);
            return IKE_FAILURE;
        }

    }

    if (pNotifyInfo->u2NotifyType != IKE_POST_P1_DELETE)
    {
        if (IkeConstructNotify (pSessionInfo, IKE_NONE_PAYLOAD,
                                &u4Size, pNotifyInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Notify Header "
                         "construct Failed %d\n");
            IkeDeleteSession (pSessionInfo);
            return IKE_FAILURE;
        }
        u1NextPayLoad = IKE_NOTIFY_PAYLOAD;
    }
    else
    {
        if (IkeConstructDelete (pSessionInfo, IKE_NONE_PAYLOAD,
                                &u4Size, pNotifyInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Delete Header "
                         "construct Failed %d\n");
            IkeDeleteSession (pSessionInfo);
            return IKE_FAILURE;
        }
        u1NextPayLoad = IKE_DELETE_PAYLOAD;
    }

    if (pNotifyInfo->u1CryptFlag == IKE_ENCRYPTION)
    {
        if (IkeConstructQmHash1 (pSessionInfo, u1NextPayLoad) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructInfoExchPacket: Qm Hash1 Header "
                         "construct Failed %d\n");
            IkeDeleteSession (pSessionInfo);
            return IKE_FAILURE;
        }
    }

    /* Update Packet total length in the Isakmp hdr */
    IKE_ASSERT (pSessionInfo->IkeTxPktInfo.u4PacketLen == u4Size);

    u4Size = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);

    ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->u4TotalLen
        = u4Size;

    if (pNotifyInfo->u1CryptFlag == IKE_ENCRYPTION)
    {
        IKE_SET_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
        IkeEncryptPayLoad (pSessionInfo);
    }

    IkeSendToSocketLayer (pSessionInfo);

    IncIkeNoOfNotifySent (&pSessionInfo->pIkeSA->IpLocalAddr);

    /* Info packet is sent out, delete the session */
    IkeDeleteSession (pSessionInfo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructCertRequest                            */
/*  Description     :This functions is used to construct the CertReq    */
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Appends the cert request PayLoad in an Isakmp Mesg */
/*                  : at the specified position                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructCertRequest (tIkeSessionInfo * pSesInfo,
                         UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tCertReqPayLoad     CertReq;

    IKE_BZERO (&CertReq, sizeof (tCertReqPayLoad));

    /* Empty CA field is sent in the CR payload currently *
     * in section 3.10 of rfc 2408, it is mentioned that CA should
     * not be sent if no specific CA is requested */
    CertReq.u1NextPayLoad = u1NextPayLoad;
    CertReq.u2PayLoadLen = IKE_CERT_PAYLOAD_SIZE;
    CertReq.u2PayLoadLen = IKE_HTONS (CertReq.u2PayLoadLen);
    CertReq.u1EncodeType = IKE_X509_SIGNATURE;

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, IKE_CERT_PAYLOAD_SIZE)
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertReq: Out Of Memory\n");
        return IKE_FAILURE;
    }
    /* Copy the CertReq PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &CertReq, IKE_CERT_PAYLOAD_SIZE);
    *pu4Pos += IKE_CERT_PAYLOAD_SIZE;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructCertificate                            */
/*  Description     :This functions is used to construct the Certificate*/
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Appends the certificate PayLoad in an Isakmp Mesg  */
/*                  :at the specified position                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructCertificate (tIkeSessionInfo * pSesInfo,
                         UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tCertPayLoad        Cert;
    tIkeCertDB         *pCertNode = NULL;
    UINT1              *pu1Cert = NULL;
    UINT4               u4CertLen = IKE_ZERO;

    pCertNode = IkeGetCertFromMyCerts (pSesInfo->au1EngineName,
                                       pSesInfo->MyCert.pCAName,
                                       pSesInfo->MyCert.pCASerialNum);

    if (pCertNode == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertificate: \
                     No certificate with default cert information\n");
        return IKE_FAILURE;
    }

    if (IkeConvertX509ToDER (pCertNode->pCert, &pu1Cert, &u4CertLen) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertificate:\
                     Unable to convert to DER format\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (&Cert, sizeof (tCertPayLoad));

    Cert.u1NextPayLoad = u1NextPayLoad;
    Cert.u2PayLoadLen = (UINT2) (IKE_CERT_PAYLOAD_SIZE + u4CertLen);
    Cert.u2PayLoadLen = IKE_HTONS (Cert.u2PayLoadLen);
    Cert.u1EncodeType = IKE_X509_SIGNATURE;

    if (ConstructIsakmpMesg
        (pSesInfo, (INT4) *pu4Pos,
         IKE_CERT_PAYLOAD_SIZE + u4CertLen) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertificate: Out Of Memory\n");
        return IKE_FAILURE;
    }

    /* Copy the CertReq PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Cert, IKE_CERT_PAYLOAD_SIZE);
    *pu4Pos += IKE_CERT_PAYLOAD_SIZE;

    /* Copy the DER formatted certificate to the payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                pu1Cert, u4CertLen);
    *pu4Pos += u4CertLen;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructSignature                              */
/*  Description     :This functions is used to construct the signature  */
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Appends the signature PayLoad in an Isakmp Mesg     */
/*                  : at the specified position                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConstructSignature (tIkeSessionInfo * pSesInfo,
                       UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tSigPayLoad         Signature;
    UINT1               au1HashData[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    UINT4               u4HashLen = IKE_ZERO, u4ErrorCode = IKE_ZERO;
    UINT1              *pu1EncrBuf = NULL;
    INT4                i4EncrBufLen = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    IKE_BZERO ((UINT1 *) au1HashData, IKE_MAX_DIGEST_SIZE);

    /* Compute the Digest Value */
    if (IkeComputeHash (pSesInfo, au1HashData) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructSignature : Compute Hash fails\n");
        return (IKE_FAILURE);
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructSignature : IkeGetIkeEngineFromIndex "
                     "fails\n");
        return (IKE_FAILURE);
    }

    switch (pSesInfo->pIkeSA->u2AuthMethod)
    {
        case IKE_RSA_SIGNATURE:
        {
            pIkeRsa = IkeGetRSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);
            if (pIkeRsa == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeConstructSignature : "
                             "IkeGetRSAKeyWithCertInfo fails\n");
                return (IKE_FAILURE);
            }
            if (IkeRsaPrivateEncrypt (au1HashData, (UINT2) u4HashLen,
                                      &pu1EncrBuf, &i4EncrBufLen,
                                      &pIkeRsa->RsaKey, &u4ErrorCode)
                == IKE_FAILURE)
            {
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_RSA_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : Unable "
                                     "to get rsa key\n");
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : Memory "
                                     "allocation failed\n");
                        break;
                    }
                    case IKE_ERR_RSA_SIGN_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : Rsa "
                                     "signing failed\n");
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : Rsa "
                                     "Signing failed\n");
                        break;
                    }
                }
                if (pu1EncrBuf != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1EncrBuf);
                }
                return (IKE_FAILURE);
            }
            break;
        }
        case IKE_DSS_SIGNATURE:
        {
            pIkeDsa = IkeGetDSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);
            if (pIkeDsa == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeConstructSignature: IkeGetDSAKeyWithCertInfo"
                             " fails\n");
                return (IKE_FAILURE);
            }

            /* This function will calculate the Signature over the Hashdata 
             * provided using the DSA key inputed */
            if (IkeDsaSign (au1HashData, (UINT2) u4HashLen, &pu1EncrBuf,
                            (UINT4 *) &i4EncrBufLen, &pIkeDsa->DsaKey,
                            &u4ErrorCode) == IKE_FAILURE)
            {
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_DSA_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : "
                                     "Unable to get Dsa key\n");
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : "
                                     "Memory allocation failed\n");
                        break;
                    }
                    case IKE_ERR_DSA_SIGN_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : "
                                     "Dsa signing failed\n");
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeConstructSignature : "
                                     "Dsa Signing failed\n");
                        break;
                    }
                }
                if (pu1EncrBuf != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1EncrBuf);
                }
                return (IKE_FAILURE);
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructSignature : Unsupported "
                         "authentication method\n");
            return (IKE_FAILURE);
        }
    }

    IKE_BZERO (&Signature, sizeof (tSigPayLoad));

    Signature.u1NextPayLoad = u1NextPayLoad;
    Signature.u2PayLoadLen =
        (UINT2) (sizeof (tSigPayLoad) + (UINT2) i4EncrBufLen);
    Signature.u2PayLoadLen = IKE_HTONS (Signature.u2PayLoadLen);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos,
                             (sizeof (tSigPayLoad) + (UINT2) i4EncrBufLen)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructSignature: Out Of Memory\n");
        return IKE_FAILURE;
    }

    /* Copy the signature PayLoad to the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Signature, sizeof (tSigPayLoad));
    *pu4Pos += sizeof (tSigPayLoad);

    /* Copy the encrypted hash to the payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                pu1EncrBuf, i4EncrBufLen);
    *pu4Pos = (UINT4) (*pu4Pos + (UINT4) i4EncrBufLen);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1EncrBuf);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructVendorId                               */
/*  Description     :This functions is used to construct the Vendor ID  */
/*                  :PayLoad(s) in an Isakmp Message                    */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Appends the Vendor ID PayLoad(s) in an Isakmp Mesg */
/*                  :at the specified position                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConstructVendorId (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                      UINT4 *pu4Pos)
{
    UINT4               u4Count = IKE_ZERO;
    UINT1               u1NextVendorID = IKE_ZERO;

    /* Filling all the Supported Vendor Ids to aOutVendorIDs Array 
     * As of now we support XAUTH and CISCO UNITY COMP */

    for (u4Count = IKE_ZERO; u4Count < IKE_MAX_SUPPORTED_VENDOR_IDS; u4Count++)
    {
        if (u4Count < IKE_MAX_SUPPORTED_VENDOR_IDS - IKE_ONE)
        {
            u1NextVendorID = IKE_VENDORID_PAYLOAD;
        }
        else
        {
            u1NextVendorID = u1NextPayLoad;
        }

        if (IkeConstructVendorIdPayload (pSesInfo, u1NextVendorID,
                                         pu4Pos, u4Count) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructVendorId: IkeConstructVendorIdPayload "
                         " Failed\n");
            return IKE_FAILURE;
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConstructVendorIDPayload                        */
/*  Description     :This functions is used to construct the Unity Comp */
/*                  :Vendor ID Payload                                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u1NextpayLoad : Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos : Specifies the position in an Isakmp Mesg  */
/*                  :where the payload can be appended                  */
/*                  :                                                   */
/*  Output(s)       :Constructs the Config Mode Req Pkt                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConstructVendorIdPayload (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                             UINT4 *pu4Pos, UINT4 u4Count)
{
    tVendorIdPayLoad    VendorId;
    UINT2               u2Length = IKE_ZERO;

    IKE_BZERO (&VendorId, sizeof (tVendorIdPayLoad));
    VendorId.u1NextPayLoad = u1NextPayLoad;

    u2Length =
        (UINT2) (sizeof (tVendorIdPayLoad) +
                 gaIkeVendorIdPayload[u4Count].u4Length);

    VendorId.u2PayLoadLen = IKE_HTONS (u2Length);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u2Length) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructVendorId: Out Of Memory\n");
        return IKE_FAILURE;
    }
    /* Copy the VendorId PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &VendorId, sizeof (tVendorIdPayLoad));

    *pu4Pos += sizeof (tVendorIdPayLoad);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                gaIkeVendorIdPayload[u4Count].au1Payload,
                gaIkeVendorIdPayload[u4Count].u4Length);

    *pu4Pos += gaIkeVendorIdPayload[u4Count].u4Length;
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpCMCfgReqPkt                      */
/*  Description     :This functions is used to construct the  Config    */
/*                  :Config Mode Req Pkt                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the Config Mode Req Pkt                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpCMCfgReqPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_REQUEST;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;            /* save the starting position of attribute payload */
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Allocation for "
                     "Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (IkeSetCMReqAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgReq:IkeSetCMReqAttributes  !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpCMCfgRepPkt                      */
/*  Description     :This functions is used to construct the  Config    */
/*                  :Config Mode Rep Pkt                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the Config Mode Rep Pkt                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpCMCfgRepPkt (tIkeSessionInfo * pSesInfo)
{
    tAttributePayLoad   AttrPayLoad;
    tIkeIpAddr          IntAddress;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;
    UINT4               u4IntNetmask = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgRepPkt: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_REPLY;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT2) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgRepPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgRepPkt:\
                     Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    /* Get a free IP address from the address pool and 
     * assign it to the Remote CLIENT */
    if (IkeUtilGetFreeAddrFromPool (&pSesInfo->RemoteTunnelTermAddr,
                                    &IntAddress, &u4IntNetmask) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgRepPkt:  Cannot Get Free IP "
                     "Address attribute from the pool !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (IntAddress.u4AddrType == IPV4ADDR)
    {
        /* Set the IP address attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP4_ADDRESS;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.Ipv4Addr =
            IntAddress.Ipv4Addr;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
            u4AddrType = IPV4ADDR;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;

        /* Set the Netmask Attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP4_NETMASK;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalMask.Ipv4Addr =
            gaIp4PrefixToMask[u4IntNetmask];
    }
    else
    {
        /* Configure the IP address attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP6_ADDRESS;
        IKE_MEMCPY (&pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr, &IntAddress, sizeof (tIp6Addr));
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
            u4AddrType = IPV6ADDR;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        /* Configure the Netmask attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP6_NETMASK;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        IKE_MEMCPY (&
                    (pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalMask.
                     Ipv6Addr), gaIp6PrefixToMask[u4IntNetmask],
                    sizeof (tIp6Addr));

    }

    if (IkeSetCMRepAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgRep:IkeSetCMRepAttributes  !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpCMCfgSetPkt                      */
/*  Description     :This functions is used to construct the  Config    */
/*                  :Config Mode Set Pkt                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the Config Mode Set Pkt                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpCMCfgSetPkt (tIkeSessionInfo * pSesInfo)
{
    tAttributePayLoad   AttrPayLoad;
    tIkeIpAddr          IntAddress;
    UINT4               u4IntNetmask = IKE_ZERO;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_SET;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgSetPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    /* Get a free IP address from the address pool and 
     * assign it to the Remote CLIENT */
    if (IkeUtilGetFreeAddrFromPool (&pSesInfo->RemoteTunnelTermAddr,
                                    &IntAddress, &u4IntNetmask) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal:  Cannot Get Free IP Address attribute "
                     " from the pool !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }
    if (IntAddress.u4AddrType == IPV4ADDR)
    {
        /* Configure the IP address attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP4_ADDRESS;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.Ipv4Addr =
            IntAddress.Ipv4Addr;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
            u4AddrType = IPV4ADDR;

        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        /* Configure the Netmask attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP4_NETMASK;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalMask.Ipv4Addr =
            gaIp4PrefixToMask[u4IntNetmask];
    }
    else
    {
        /* Configure the IP address attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP6_ADDRESS;
        IKE_MEMCPY (&pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr, &IntAddress, sizeof (tIkeIpAddr));
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
            u4AddrType = IPV6ADDR;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        /* Configure the Netmask attribute */
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                              u1ConfiAttrIndex] = IKE_INTERNAL_IP6_NETMASK;
        pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.u1ConfiAttrIndex++;
        IKE_MEMCPY (&
                    (pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.InternalMask.
                     Ipv6Addr), gaIp6PrefixToMask[u4IntNetmask],
                    sizeof (tIp6Addr));

    }

    if (IkeSetCMSetAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgSet:IkeSetCMSetAttributes  !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpCMCfgAckPkt                      */
/*  Description     :This functions is used to construct the  Config    */
/*                  :Config Mode Ack Pkt                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the Config Mode Ack Pkt                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpCMCfgAckPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);

    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_ACK;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgAckPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (IkeSetCMAckAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgAck:IkeSetCMAckAttributes  !\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }

    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpXAuthCfgReqPkt                   */
/*  Description     :This functions is used to construct the  XAuth     */
/*                  :Confi Req Pkt                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the XAuth Config Req Pkt                */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpXAuthCfgReqPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);
/*  Allocate  Memory for Transaction Hash PayLoad*/
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_REQUEST;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgAckPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (IkeSetXAuthReqAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeSetXAuthReqAttributes failed!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpXAuthCfgRepPkt                   */
/*  Description     :This functions is used to construct the  XAuth     */
/*                  :Confi Rep Pkt                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the XAuth Config Rep Pkt                */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpXAuthCfgRepPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);
/*  Allocate  Memory for Transaction Hash PayLoad*/
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_REPLY;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgRepPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (IkeSetXAuthRepAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgRepPkt:"
                     "IkeSetXAuthRepAttributes failed!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpXAuthCfgSetPkt                   */
/*  Description     :This functions is used to construct the  XAuth     */
/*                  :Confi Set Pkt                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the XAuth Config Set Pkt                */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpXAuthCfgSetPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4SavePos = IKE_ZERO;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_UNUSED (u4Len);
    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);
    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_SET;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4SavePos = u4Pos;
    u4Pos += sizeof (tAttributePayLoad);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgSetPkt: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetProposal: Allocation for Attributes failed!\n");
        return IKE_FAILURE;
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    if (IkeSetXAuthSetAttributes (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCMCfgReq: IkeSetCMAtts failed!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, (UINT4) i4Len) ==
        IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    AttrPayLoad.u2PayLoadLen =
        (UINT2) (sizeof (tAttributePayLoad) + (UINT2) i4Len);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4SavePos +
                           IKE_ATTTR_LEN_OFFSET), &(AttrPayLoad.u2PayLoadLen),
                sizeof (AttrPayLoad.u2PayLoadLen));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }

    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructIsakmpXAuthCfgAckPkt                   */
/*  Description     :This functions is used to construct the  XAuth     */
/*                  :Confi Set Pkt                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Constructs the XAuth Config Ack Pkt                */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructIsakmpXAuthCfgAckPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    UINT4               u4Pos = sizeof (tIsakmpHdr);
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;

    IKE_BZERO (&(AttrPayLoad), sizeof (tAttributePayLoad));

    if (IkeConstructHeader (pSesInfo, IKE_TRANSACTION_MODE,
                            IKE_HASH_PAYLOAD) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpCfgReq: Construct Hdr Failed\n");
        return IKE_FAILURE;
    }
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    u4Pos = (u4Pos + sizeof (tHashPayLoad) + u4HashLen);
    /*  Allocate  Memory for Transaction Hash PayLoad */
    if (ConstructIsakmpMesg (pSesInfo, sizeof (tIsakmpHdr),
                             (u4HashLen + sizeof (tHashPayLoad))) ==
        IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    AttrPayLoad.u1NextPayLoad = IKE_NONE;
    AttrPayLoad.u2PayLoadLen = sizeof (tAttributePayLoad);
    AttrPayLoad.u2PayLoadLen = IKE_HTONS (AttrPayLoad.u2PayLoadLen);
    AttrPayLoad.u1Type = IKE_ISAKMP_CFG_ACK;
    AttrPayLoad.u2Id = IKE_HTONS (pSesInfo->IkeTransInfo.u2RequestId);

    u4NBytes = sizeof (tAttributePayLoad);

    if (ConstructIsakmpMesg (pSesInfo, (INT4) u4Pos, u4NBytes) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + u4Pos),
                (UINT1 *) &AttrPayLoad, sizeof (tAttributePayLoad));

    u4TotLen = IKE_HTONL (pSesInfo->IkeTxPktInfo.u4PacketLen);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           IKE_ISAKMP_LEN_OFFSET), &(u4TotLen), sizeof (UINT4));

    if (IkeConstructTransactionHash (pSesInfo, IKE_ATTRIBUTE_PAYLOAD) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructIsakmpXAuthCfgReqPkt:"
                     "IkeConstructTransactionHash failed!\n");
        return IKE_FAILURE;

    }
    IKE_SET_ENCR_MASK (pSesInfo->IkeTxPktInfo.pu1RawPkt);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetCMReqAttribute                               */
/*  Description     :This function is used to set CM Req parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Req attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetCMReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                       INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT1               u1Value = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->IkeTransInfo.IkeRAInfo.CMClientInfo.
            au2ConfiguredAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->IkeTransInfo.IkeRAInfo.CMClientInfo.
                au2ConfiguredAttr[u1Index])
        {

            case IKE_SUPPORTED_ATTRIBUTES:
                if (VpiAtt
                    (IKE_SUPPORTED_ATTRIBUTES,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set"
                                 "Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            default:
                break;
        }
        u1Index++;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetCMRepAttribute                               */
/*  Description     :This function is used to set CM Rep parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetCMRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                       INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT1               u1Index = IKE_ZERO;
    UINT1               au1Network[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT4               u4Address = IKE_ZERO;

    IKE_BZERO (au1Network, IKE_MAX_ADDR_STR_LEN);

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index])
        {

            case IKE_SUPPORTED_ATTRIBUTES:
            {
                if (pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr.u4AddrType == IPV4ADDR)
                {
                    u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                        InternalIpAddr.Ipv4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    if (VpiAtt
                        (IKE_INTERNAL_IP4_ADDRESS,
                         (UINT1 *) &(u4Address), sizeof (UINT4),
                         ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSetCMRepAttributes: Unable to set"
                                     "Var Att IPv4 Address\n");
                        return IKE_FAILURE;
                    }
                    u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                        InternalMask.Ipv4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    if (VpiAtt
                        (IKE_INTERNAL_IP4_NETMASK,
                         (UINT1 *) &(u4Address), sizeof (UINT4),
                         ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSetCMReqAttributes: Unable to "
                                     "set Var Att  IPv4 Netmask\n");
                        return IKE_FAILURE;
                    }
                    u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                        ProtectedNetwork[IKE_INDEX_0].
                        uNetwork.Ip4Subnet.Ip4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    IKE_MEMCPY (au1Network, &(u4Address), sizeof (UINT4));
                    u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                        ProtectedNetwork[IKE_INDEX_0].
                        uNetwork.Ip4Subnet.Ip4SubnetMask;
                    u4Address = IKE_HTONL (u4Address);
                    IKE_MEMCPY ((au1Network + IKE_FOUR),
                                &(u4Address), sizeof (UINT4));
                    if (VpiAtt
                        (IKE_INTERNAL_IP4_SUBNET,
                         (UINT1 *) au1Network, IKE_EIGHT, ppu1Atts, pi4Len,
                         pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSetCMReqAttributes: Unable to set "
                                     "Var Att  IPv4 Subnet\n");
                        return IKE_FAILURE;
                    }
                }
                else
                {
                    if (VpiAtt
                        (IKE_INTERNAL_IP6_ADDRESS,
                         (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.
                                     CMServerInfo.InternalIpAddr.Ipv6Addr),
                         sizeof (tIp6Addr), ppu1Atts, pi4Len,
                         pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSetCMReqAttributes: Unable to set "
                                     "Var Att  IPv6 Address\n");
                        return IKE_FAILURE;
                    }
                    if (VpiAtt
                        (IKE_INTERNAL_IP6_NETMASK,
                         (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.
                                     CMServerInfo.InternalMask.Ipv6Addr),
                         sizeof (tIp6Addr), ppu1Atts, pi4Len,
                         pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSetCMReqAttributes: Unable to "
                                     "set Var Att  IPv6 Netmask \n");
                        return IKE_FAILURE;
                    }
                }
                return (IKE_SUCCESS);
            }
            case IKE_INTERNAL_IP4_ADDRESS:
            {
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_ADDRESS,
                     (UINT1 *) &(u4Address), sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to "
                                 "set Var Att  IPv4 Address\n");
                    return IKE_FAILURE;
                }
                break;
            }
            case IKE_INTERNAL_IP4_NETMASK:
            {
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalMask.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_NETMASK,
                     (UINT1 *) &(u4Address), sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to "
                                 "set Var Att  IPv6 Address \n");
                    return IKE_FAILURE;
                }
                break;
            }
            case IKE_INTERNAL_IP4_SUBNET:
            {
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.Ip4Addr;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY (au1Network, &(u4Address), sizeof (UINT4));
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].
                    uNetwork.Ip4Subnet.Ip4SubnetMask;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY ((au1Network + IKE_FOUR),
                            &(u4Address), sizeof (UINT4));
                if (VpiAtt
                    (IKE_INTERNAL_IP4_SUBNET,
                     (UINT1 *) au1Network, IKE_EIGHT, ppu1Atts, pi4Len,
                     pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set "
                                 "Var Att IPv4 Subnet\n");
                    return IKE_FAILURE;
                }
                break;
            }
            case IKE_INTERNAL_IP6_ADDRESS:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                                 InternalIpAddr.Ipv6Addr),
                     sizeof (tIp6Addr),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set "
                                 "Var Att IPv6 Address \n");
                    return IKE_FAILURE;
                }
                break;
            }
            case IKE_INTERNAL_IP6_NETMASK:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_NETMASK,
                     (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                                 InternalMask.Ipv6Addr), sizeof (tIp6Addr),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to "
                                 "set Var Att IPv6 Netmask\n");
                    return IKE_FAILURE;
                }
                break;
            }
            default:

                break;

        }
        u1Index++;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetCMSetAttribute                               */
/*  Description     :This function is used to set CM Set parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetCMSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                       INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT1               u1Index = IKE_ZERO;
    UINT1               au1Network[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT4               u4Address = IKE_ZERO;

    IKE_BZERO (au1Network, IKE_MAX_ADDR_STR_LEN);

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            au2ConfiguredAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                au2ConfiguredAttr[u1Index])
        {

            case IKE_SUPPORTED_ATTRIBUTES:
                break;

            case IKE_INTERNAL_IP4_ADDRESS:
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_ADDRESS,
                     (UINT1 *) &(u4Address),
                     sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set "
                                 "Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP4_NETMASK:
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    InternalMask.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_NETMASK,
                     (UINT1 *) &(u4Address), sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to "
                                 "set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP4_SUBNET:
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.Ip4Addr;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY (au1Network, &(u4Address), sizeof (UINT4));
                u4Address = pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].
                    uNetwork.Ip4Subnet.Ip4SubnetMask;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY ((au1Network + IKE_FOUR),
                            &(u4Address), sizeof (UINT4));
                if (VpiAtt
                    (IKE_INTERNAL_IP4_SUBNET,
                     (UINT1 *) au1Network, IKE_EIGHT, ppu1Atts, pi4Len,
                     pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set "
                                 "Var Att\n");
                    return IKE_FAILURE;
                }
                break;
            case IKE_INTERNAL_IP6_ADDRESS:
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                                 InternalIpAddr.Ipv6Addr),
                     sizeof (tIp6Addr),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to set "
                                 "Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP6_NETMASK:
                if (VpiAtt
                    (IKE_INTERNAL_IP6_NETMASK,
                     (UINT1 *) &(pSesInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                                 InternalMask.Ipv6Addr), sizeof (tIp6Addr),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable to "
                                 "set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            default:
                break;
        }
        u1Index++;
    }
    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   :IkeSetCMAckAttribute                               */
/*  Description     :This function is used to set CM Ack parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetCMAckAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                       INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT1               u1Value = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->IkeTransInfo.au2CMAcptAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->IkeTransInfo.IkeRAInfo.CMClientInfo.
                au2ConfiguredAttr[u1Index])
        {

            case IKE_INTERNAL_IP4_ADDRESS:
                if (VpiAtt
                    (IKE_INTERNAL_IP4_ADDRESS,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable "
                                 "to set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP4_NETMASK:
                if (VpiAtt
                    (IKE_INTERNAL_IP4_NETMASK,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable "
                                 "to set Var Att\n");
                    return IKE_FAILURE;
                }
                break;
            case IKE_INTERNAL_IP4_SUBNET:
                if (VpiAtt
                    (IKE_INTERNAL_IP4_SUBNET,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable "
                                 "to set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP6_ADDRESS:
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable "
                                 "to set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            case IKE_INTERNAL_IP6_NETMASK:
                if (VpiAtt
                    (IKE_INTERNAL_IP6_NETMASK,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetCMReqAttributes: Unable "
                                 "to set Var Att\n");
                    return IKE_FAILURE;
                }
                break;

            default:
                break;
        }
        u1Index++;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetXAuthReqAttributes                           */
/*  Description     :This function is used to set XAuth Req parameters  */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetXAuthReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                          INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT1               u1Value = IKE_ZERO;

    if (pSesInfo->IkeTransInfo.IkeRAInfo.XAuthServer.u1XAuthType ==
        IKE_XAUTH_GENERIC)
    {
        if (VpiAtt
            (IKE_XAUTH_USER_NAME,
             (UINT1 *) &u1Value, IKE_ZERO,
             ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetXAuthReqAttributes: Unable to set Var Att\n");
            return IKE_FAILURE;
        }

        if (VpiAtt
            (IKE_XAUTH_USER_PASSWORD,
             (UINT1 *) &u1Value, IKE_ZERO,
             ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
        {

            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetXAuthReqAttributes: Unable to set Var Att\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetXAuthRepAttributes                           */
/*  Description     :This function is used to set XAuth Rep parameters  */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetXAuthRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                          INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT2               u2Status = IKE_XAUTH_FAIL;

    if (pSesInfo->IkeTransInfo.bXAuthReqSuccess != TRUE)
    {

        if (BasicAtt (IKE_XAUTH_STATUS, u2Status, ppu1Atts,
                      pi4Len, pi4AttsLen) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
        return IKE_SUCCESS;
    }

    if (VpiAtt
        (IKE_XAUTH_USER_NAME,
         (UINT1 *) pSesInfo->IkeTransInfo.IkeRAInfo.XAuthClient.au1UserName,
         sizeof (pSesInfo->IkeTransInfo.IkeRAInfo.XAuthClient.au1UserName),
         ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetXAuthRepAttributes: Unable to set Var Att\n");
        return IKE_FAILURE;
    }

    if (VpiAtt
        (IKE_XAUTH_USER_PASSWORD,
         (UINT1 *) pSesInfo->IkeTransInfo.IkeRAInfo.XAuthClient.au1UserPasswd,
         sizeof (pSesInfo->IkeTransInfo.IkeRAInfo.XAuthClient.au1UserPasswd),
         ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetXAuthReqAttributes: Unable to set Var Att\n");
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetXAuthSetAttributes                           */
/*  Description     :This function is used to set XAuth Set parameters  */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the session info data base   */
/*                  :ppu1Atts : Pointer refering to the pointer         */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen : Pointer to the payload len            */
/*                  :                                                   */
/*  Output(s)       :Constructs the XAuth Set attributes                */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetXAuthSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                          INT4 *pi4AttsLen, INT4 *pi4Len)
{

    UINT2               u2Status = IKE_XAUTH_FAIL;

    if (pSesInfo->IkeTransInfo.bUserAuthenticated)
    {
        u2Status = IKE_XAUTH_OK;
    }

    if (BasicAtt (IKE_XAUTH_STATUS, u2Status, ppu1Atts,
                  pi4Len, pi4AttsLen) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructTransactionHash                        */
/*  Description     :This function is used to construct Transaction     */
/*                  :Hash                                               */
/*                  :                                                   */
/*  Input(s)        :pSessionInfo : Pointer to Session data structure   */
/*                  :u1NextPayLoad : The next payload to be added       */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructTransactionHash (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad)
{
    UINT4               u4HashLen = IKE_ZERO;
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL;
    UINT1               au1Hash[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    UINT4               u4MesgId = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeTxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructTransactionHash: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeConstructTransactionHash: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 1 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | ATTR */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4),
                pSesInfo->IkeTxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         u2PayLoadLen + (UINT2) sizeof (UINT4), au1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructTransactionHash: Unsupported Hmac algorithm\n");

        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr) + sizeof (tHashPayLoad)),
                au1Hash, u4HashLen);

    HashPayLoad.u1NextPayLoad = u1NextPayLoad;
    HashPayLoad.u1Reserved = IKE_ZERO;
    HashPayLoad.u2PayLoadLen = (UINT2) (sizeof (tHashPayLoad) + u4HashLen);
    HashPayLoad.u2PayLoadLen = IKE_HTONS (HashPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt +
                           sizeof (tIsakmpHdr)), &HashPayLoad,
                sizeof (tHashPayLoad));

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructNatdPayLoad                            */
/*  Description     :This function is used to construct NatD Payload    */
/*                  :                                                   */
/*  Input(s)        :pSesinfo : Pointer to Session data structure       */
/*                  :u1NextPayLoad : The next payload to be added       */
/*                  :pu4Pos : Position, where constructed payload to be */
/*                  :         appended                                  */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeConstructNatdPayLoad (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                         UINT4 *pu4Pos)
{
    tVendorIdPayLoad    IkeNatD;
    UINT1               au1NatDHash[IKE_SHA512_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN]
        = { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Length = IKE_ZERO;

    IKE_BZERO (au1NatdData, (IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                             IKE_IP6_ADDR_LEN + IKE_PORT_LEN));
    IKE_BZERO (&IkeNatD, sizeof (tNattDetectPayLoad));
    IkeNatD.u1NextPayLoad = IKE_NAT_DETECT_PAYLOADS;

    /* Add Initiator Cookie and Responder Cookie */
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0],
                pSesInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN,
                pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    u2Length = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);

    /* Add Peer Ip address */
    if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] +
                    u2Length,
                    &(pSesInfo->pIkeSA->IpPeerAddr.Ipv6Addr), IKE_IP6_ADDR_LEN);
        u2Length = (UINT2) (u2Length + IKE_IP6_ADDR_LEN);
    }
    else
    {
        u4IpAddr = pSesInfo->pIkeSA->IpPeerAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] +
                    u2Length, &u4IpAddr, IKE_IP4_ADDR_LEN);
        u2Length = (UINT2) (u2Length + IKE_IP4_ADDR_LEN);
    }

    /* Add Remote Port */
    u2IkePort = IKE_HTONS (pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Length, &u2IkePort, IKE_PORT_LEN);
    u2Length = (UINT2) (u2Length + IKE_PORT_LEN);

    /* Calculate Hash and Add to payload */
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_MD5)
    {
        IkeMd5HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_MD5_LENGTH;
    }
    else if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_SHA1)
    {
        IkeSha1HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA_LENGTH;
    }

    else if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_256)
    {
        IkeSha256HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA256_LENGTH;
    }
    else if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_384)
    {
        IkeSha384HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA384_LENGTH;
    }
    else if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_512)
    {
        IkeSha512HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA512_LENGTH;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNatdPayload: \
                     Out of Memory neither MD5,SHA1 nor HMAC_SHA");
        return (IKE_FAILURE);
    }
    IkeNatD.u2PayLoadLen = IKE_HTONS ((u2Length + sizeof (tNattDetectPayLoad)));

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos,
                             (sizeof (tNattDetectPayLoad) + u2Length)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNatdPayLoad:Out Of Memory\n");
        return (IKE_FAILURE);
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &IkeNatD, sizeof (tNattDetectPayLoad));
    *pu4Pos = *pu4Pos + sizeof (tNattDetectPayLoad);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &au1NatDHash[IKE_INDEX_0], u2Length);
    *pu4Pos = *pu4Pos + u2Length;

    IKE_BZERO (&IkeNatD, sizeof (tNattDetectPayLoad));
    IkeNatD.u1NextPayLoad = u1NextPayLoad;

    IKE_BZERO (au1NatdData, (IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                             IKE_IP6_ADDR_LEN + IKE_PORT_LEN));
    /* Add Initiator Cookie and Responder Cookie */
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0],
                pSesInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN,
                pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    u2Length = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);

    /* Add local Ip address */
    if (pSesInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0]
                    + u2Length, &
                    (pSesInfo->pIkeSA->IpLocalAddr.Ipv6Addr), IKE_IP6_ADDR_LEN);
        u2Length = (UINT2) (u2Length + IKE_IP6_ADDR_LEN);
    }
    else
    {
        u4IpAddr = pSesInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Length,
                    &u4IpAddr, IKE_IP4_ADDR_LEN);
        u2Length = (UINT2) (u2Length + IKE_IP4_ADDR_LEN);
    }

    /* Add Local Port */
    u2IkePort = IKE_HTONS (IKE_UDP_PORT);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Length, &u2IkePort, IKE_PORT_LEN);
    u2Length = (UINT2) (u2Length + IKE_PORT_LEN);

    /* Calculate Hash and Add to payload */
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_MD5)
    {
        IkeMd5HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_MD5_LENGTH;
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_SHA1)
    {
        IkeSha1HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA_LENGTH;
    }

    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_256)
    {
        IkeSha256HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA256_LENGTH;
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_384)
    {
        IkeSha384HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA384_LENGTH;
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_512)
    {
        IkeSha512HashAlgo (au1NatdData, u2Length, au1NatDHash);
        u2Length = (UINT2) IKE_SHA512_LENGTH;
    }

    IkeNatD.u2PayLoadLen = IKE_HTONS ((u2Length + sizeof (tNattDetectPayLoad)));

    if (ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos,
                             (sizeof (tNattDetectPayLoad) + u2Length)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructNatdPayLoad:Out Of Memory\n");
        return (IKE_FAILURE);
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &IkeNatD, sizeof (tNattDetectPayLoad));
    *pu4Pos = *pu4Pos + sizeof (tNattDetectPayLoad);
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &au1NatDHash[IKE_INDEX_0], u2Length);
    *pu4Pos = *pu4Pos + u2Length;

    return IKE_SUCCESS;
}
