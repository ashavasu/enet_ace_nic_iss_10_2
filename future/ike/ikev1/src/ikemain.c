/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ikemain.c,v 1.10 2014/07/16 12:47:03 siva Exp $
 *
 * Description: This has functions for IKE Main SubModule
 *
 ***********************************************************************/

#include "ikegen.h"
#include "iketdfs.h"
#include "ikecertsave.h"
#include "ikersa.h"
#include "ikestat.h"
#include "fssocket.h"
#include "fsopenssl.h"
#include "ikememmac.h"
#ifdef SNMP_WANTED
#include "include.h"
#include "fsikemdb.h"
#endif

#define IkeDelTransSession IkeDeleteSession

extern t_SLL        gIkeVpnPolicyList;

static INT1         SendInitialContactPayload (tIkeSessionInfo * pSessionInfo);
static tIkeSessionInfo *DoPostPhase1Action (tIkeSessionInfo * pSessionInfo);

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIkePkt 
 *  Description   : Called from ike IkeProcessPkt to process data from  
 *                  socket layer for key negotiation.
 *  Input(s)      : pIkeEngien - Pointer to the Ike Engine.        
 *                : pu1Packet - Pointer to the Recieved pkt.
 *                : u2PacketLen - received pkt len.
 *                : pPeerIpAddr = Pointer to the Peer Address
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */

VOID
IkeProcessIkePkt (tIkeEngine * pIkeEngine, UINT1 *pu1Packet, UINT2 u2PacketLen,
                  tIkeIpAddr * pPeerIpAddr, UINT2 u2RemotePort,
                  UINT2 u2LocalPort)
{
    tIsakmpHdr          IkeIsakmpHdr;
    tIkeSessionInfo    *pSessionInfo = NULL;
    INT4                i4Pos = IKE_ZERO;
    tIkeSessionInfo    *pTransSessInfo = NULL;
    UINT4               u4EncrFlag = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    /* Copy Isakmp Header to Isakmp Structure from packet */
    IKE_MEMCPY (&IkeIsakmpHdr, pu1Packet, sizeof (tIsakmpHdr));
    IkeIsakmpHdr.u4TotalLen = IKE_NTOHL (IkeIsakmpHdr.u4TotalLen);

    /* Get Session Info from Session Data base based on cookie */
    pSessionInfo =
        IkeGetSessionFromCookie (pIkeEngine, pPeerIpAddr, u2RemotePort,
                                 u2LocalPort, &IkeIsakmpHdr);
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: IkeGetSessionFromCookie failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
        return;
    }

    if (pPeerIpAddr->u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop (&pPeerIpAddr->Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr, pPeerIpAddr->Ipv4Addr);
    }

    if (u2LocalPort == IKE_UDP_PORT)
    {
        if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessIkePkt: \
                         Natt is enable received pkt on 500 from: %s\n",
                          pu1PeerAddrStr);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
            return;
        }
    }
    else
    {
        if (!
            (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT))
        {
            pSessionInfo->pIkeSA->IkeNattInfo.u2RemotePort = u2RemotePort;
            pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSessionInfo->pIkeSA->IkeNattInfo.
                u1NattFlags | IKE_USE_NATT_PORT;
        }
    }

    /* Check whether the packet recd is a retransmitted one */
    if (pSessionInfo->pu1LastRcvdPkt != NULL)
    {
        if ((u2PacketLen == pSessionInfo->u2LastRcvdPktLen) &&
            (IKE_MEMCMP (pu1Packet, pSessionInfo->pu1LastRcvdPkt,
                         u2PacketLen) == IKE_FALSE))
        {
            /* Retransmitted pkt, send the reply pkt if there is one */
            if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
            {
                IkeSendToSocketLayer (pSessionInfo);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
            return;
        }

        /* Store the last rcvd good packet in case the one
         * received now is junk or replay
         */
        pSessionInfo->pu1LastRcvdGoodPkt = pSessionInfo->pu1LastRcvdPkt;
        pSessionInfo->u2LastRcvdGoodPktLen = pSessionInfo->u2LastRcvdPktLen;
    }

    /* Copy the recd pkt into pu1LastRcvdPkt */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pu1LastRcvdPkt) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
        return;
    }

    if (pSessionInfo->pu1LastRcvdPkt == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeSocketHandler: Unable allocate Memory for "
                     "Packet\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
        return;
    }

    IKE_MEMCPY (pSessionInfo->pu1LastRcvdPkt, pu1Packet, u2PacketLen);
    pSessionInfo->u2LastRcvdPktLen = u2PacketLen;

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        pSessionInfo->IkeRxPktInfo.pu1RawPkt = NULL;
    }
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_DELETE_LIST (&(pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]),
                         IkeFreePayload);
    }
    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }
    pSessionInfo->IkeRxPktInfo.pu1RawPkt = pu1Packet;
    pSessionInfo->IkeRxPktInfo.u4PacketLen = u2PacketLen;

    if (pSessionInfo->u1FsmState == IKE_QM_WAIT_ON_IPSEC)
    {
        /* We are waiting for IPSec to reply on Duplicate SPI
           request */
        return;
    }

    if (IkeCoreProcess (pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: IkeCoreProcess Failed\n");
        if (pSessionInfo->bDelSession == IKE_TRUE)
        {
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeDeleteSession Failed\n");
                return;
            }
        }
        else
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1LastRcvdPkt);
            pSessionInfo->pu1LastRcvdPkt = pSessionInfo->pu1LastRcvdGoodPkt;
            pSessionInfo->u2LastRcvdPktLen = pSessionInfo->u2LastRcvdGoodPktLen;
            pSessionInfo->pu1LastRcvdGoodPkt = NULL;
            pSessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }
        return;
    }
    else
    {
        /* IkeCoreProcess() returned IKE_SUCCESS */

        /* If the packet exchange type is info exchange, then we
         * don't require the session beyond this point */
        if (pSessionInfo->u1ExchangeType == IKE_ISAKMP_INFO)
        {
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeDeleteSession Failed\n");
            }
            return;
        }

        /* Processing of packet succeeded, free the last rcvd
         * good packet
         */
        if (pSessionInfo->pu1LastRcvdGoodPkt != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1LastRcvdGoodPkt);
            pSessionInfo->pu1LastRcvdGoodPkt = NULL;
            pSessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }

        if (pSessionInfo->bPhase1Done == IKE_TRUE)
        {
            /* 
             * For Aggressive mode, the last packet is sent by
             * Initiator
             * For Main Mode, the last packet is sent by the
             * Responder
             */
            if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
            {
                u4EncrFlag = (UINT4)
                    IKE_TEST_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
                if (u4EncrFlag != IKE_ZERO)
                {
                    IkeEncryptPayLoad (pSessionInfo);
                }
                IkeSendToSocketLayer (pSessionInfo);
            }
            /* Indicates RAVPN Rekey which is required to avoid phase2 rekey 
               at the time of phase1 Rekey */
            if (pSessionInfo->bRekey == IKE_POST_RA_REKEY)
            {
                pSessionInfo->pIkeSA->bRekey = IKE_POST_RA_REKEY;
            }

            if (IkeStoreIkeSA (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeStoreIkeSA Failed\n");
                return;
            }

            /* If initial contact is required to be sent, Send Info exch
             * with notify payload */
            if (pSessionInfo->bInitialContact == IKE_TRUE)
            {
                SendInitialContactPayload (pSessionInfo);
            }

            if (((pSessionInfo->pIkeSA->bXAuthEnabled == TRUE) &&
                 (pSessionInfo->pIkeSA->bXAuthDone == FALSE)) ||
                ((pSessionInfo->pIkeSA->bCMEnabled == TRUE) &&
                 (pSessionInfo->pIkeSA->bCMDone == FALSE)))
            {

                pTransSessInfo =
                    StartTransactionExchange (pSessionInfo->pIkeSA,
                                              pSessionInfo->u1Role);

                IkeDelPhase1Session (pSessionInfo);
                pSessionInfo = NULL;

                if (pTransSessInfo == NULL)
                {
                    /* StartTransactionExchange() logs error if any */
                    return;
                }

                pSessionInfo = pTransSessInfo;
                goto TransactionDoneCheck;
            }

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                /* Responder, nothing to do. Delete session and return */
                IkeDelPhase1Session (pSessionInfo);
                pSessionInfo = NULL;
                return;
            }

          DoPostPhase1Action:
            pSessionInfo = DoPostPhase1Action (pSessionInfo);

            if (pSessionInfo == NULL)
            {
                /* Post Phase1 Action was NOTIFY MSG or failure */
                return;
            }

            /* pSessionInfo != NULL, it is phase2 packet */
            goto Phase2DoneCheck;
        }                        /* Phase1Done == TRUE */

      TransactionDoneCheck:
        if ((pSessionInfo->pIkeSA->bTransDone == IKE_TRUE) &&
            (pSessionInfo->u1ExchangeType == IKE_TRANSACTION_MODE))
        {
            if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
            {
                u4EncrFlag = IKE_TEST_ENCR_MASK
                    (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
                if (u4EncrFlag != IKE_ZERO)
                {
                    IkeEncryptPayLoad (pSessionInfo);
                }
                IkeSendToSocketLayer (pSessionInfo);
            }

            goto DoPostPhase1Action;
        }

      Phase2DoneCheck:
        if (pSessionInfo->bPhase2Done == IKE_TRUE)
        {
            /* Start Timer to delete the session */
            if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               IKE_DELAYED_SESSION_DELETE_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: IkeStartTimer for Delete session "
                             "Failed\n");
            }
        }

        if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
        {
            u4EncrFlag = IKE_TEST_ENCR_MASK
                (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            if (u4EncrFlag != IKE_ZERO)
            {
                IkeEncryptPayLoad (pSessionInfo);
            }
            IkeSendToSocketLayer (pSessionInfo);
        }

        if ((pSessionInfo->u1FsmState == IKE_XAUTH_ACK_SENT) &&
            (pSessionInfo->pIkeSA->bCMEnabled == TRUE) &&
            (pSessionInfo->pIkeSA->bCMDone == FALSE))
        {
            /* Release the Xauth ACK packet and transmit the 
             * CM_REQUEST packet */
            if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo->IkeTxPktInfo.
                                    pu1RawPkt);
                pSessionInfo->IkeTxPktInfo.pu1RawPkt = NULL;
            }
            IKE_BZERO (&pSessionInfo->IkeTxPktInfo, sizeof (tIkePacket));

            /* Change Msg ID as this is new session */
            IkeGetRandom ((UINT1 *) &pSessionInfo->u4MsgId, IKE_MSGID_LEN);

            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1CryptIV);
            if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
            {
                MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: IkeGenIV failed\n");
                return;
            }

            if (IkeConstructIsakmpCMCfgReqPkt (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "Ack payload failed!!\n");
                return;
            }
            pSessionInfo->u1FsmState = IKE_CM_REQ_SENT;
            u4EncrFlag =
                IKE_TEST_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            if (u4EncrFlag != IKE_ZERO)
            {
                IkeEncryptPayLoad (pSessionInfo);
            }
            IkeSendToSocketLayer (pSessionInfo);
        }

    }                            /* IkeCoreProcess == IKE_SUCCESS */
}

INT1
SendInitialContactPayload (tIkeSessionInfo * pSessionInfo)
{
    tIkeNotifyInfo     *pNotifyInfo = NULL;
    UINT1               au1Phase1Spi[(IKE_TWO * IKE_COOKIE_LEN) + 1] =
        { IKE_ZERO };

    /* Allocate memory for holding Notification information */
    if (MemAllocateMemBlock
        (IKE_NOTIFY_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pNotifyInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pNotifyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeSocketHandler: Not enough memory\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (pNotifyInfo, sizeof (tIkeNotifyInfo));

    pNotifyInfo->u1Protocol = IKE_ISAKMP;
    pNotifyInfo->u2NotifyType = IKE_INITIAL_CONTACT;
    pNotifyInfo->u1SpiSize = IKE_TWO * IKE_COOKIE_LEN;
    pNotifyInfo->u2NotifyDataLen = IKE_TWO * IKE_COOKIE_LEN;
    pNotifyInfo->u1CryptFlag = IKE_ENCRYPTION;

    IKE_BZERO (&au1Phase1Spi, (IKE_TWO * IKE_COOKIE_LEN));

    IKE_MEMCPY (&au1Phase1Spi,
                pSessionInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);

    IKE_MEMCPY (&(au1Phase1Spi[IKE_COOKIE_LEN]),
                pSessionInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);

    pNotifyInfo->pu1NotifyData = au1Phase1Spi;

    /* Construct Notify PayLoad */
    if (IkeConstructInfoExchPacket
        (pNotifyInfo, pSessionInfo->pIkeSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: Construct "
                     "Informational exchange Message Failed\n");
        MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
    return IKE_SUCCESS;
}

tIkeSessionInfo    *
DoPostPhase1Action (tIkeSessionInfo * pSessionInfo)
{
    BOOLEAN             IsPhase1Exch = IKE_FALSE;
    tIkeSessionInfo    *pPhase2SessionInfo = NULL;

    if ((pSessionInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSessionInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        IsPhase1Exch = IKE_TRUE;
    }

    if ((pSessionInfo->pIkeSA->bTransDone == TRUE) &&
        (pSessionInfo->u1ExchangeType == IKE_TRANSACTION_MODE))
    {
        if (((pSessionInfo->pIkeSA->bCMDone == TRUE) &&
             (pSessionInfo->IkeTransInfo.IkeRAInfo.bCMEnabled == TRUE) &&
             (gbCMServer == FALSE)) ||
            ((pSessionInfo->IkeTransInfo.IkeRAInfo.bCMEnabled == FALSE) &&
             (gbCMServer == FALSE)))
        {
            if (IkeUpdtPostPhase1InfoFromRAInfo (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "DoPostPhase1Action: IkeUpdatePostPhase1InfoFromRA"
                             "Info Failed\n");
                IkeDeleteSession (pSessionInfo);
                pSessionInfo = NULL;
                return NULL;
            }

            if ((gbXAuthServer == FALSE) && (gbCMServer == FALSE))
            {
                if (IkeCreateDynCryptoMapForClient (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "DoPostPhase1Action: "
                                 "IkeCreateDynCryptoMapFor" "Client Failed\n");
                    IkeDeleteSession (pSessionInfo);
                    pSessionInfo = NULL;
                    return NULL;

                }
            }
        }
    }

    if ((pSessionInfo->PostPhase1Info.u4ActionType ==
         IKE_POST_P1_NOTIFY)
        || (pSessionInfo->PostPhase1Info.u4ActionType == IKE_POST_P1_DELETE))
    {
        if (IkeConstructInfoExchPacket
            (&pSessionInfo->PostPhase1Info.IkeNotifyInfo,
             pSessionInfo->pIkeSA) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "DoPostPhase1Action: Construct "
                         "Informational exchange Message Failed\n");
            IkeDeleteSession (pSessionInfo);
            return NULL;
        }

        (IsPhase1Exch == TRUE) ? IkeDelPhase1Session (pSessionInfo) :
            IkeDelTransSession (pSessionInfo);
        return NULL;
    }

    if (pSessionInfo->PostPhase1Info.u4ActionType == IKE_POST_P1_NEW_SA)
    {
        pPhase2SessionInfo =
            IkePhase2SessionInit (pSessionInfo->pIkeSA,
                                  &pSessionInfo->PostPhase1Info.
                                  IPSecRequest, IKE_NONE, IKE_INITIATOR);
        if (pPhase2SessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "DoPostPhase1Action: Phase2 Session "
                         "Initialization Failed\n");
            IkeDeleteSession (pSessionInfo);
            pSessionInfo = NULL;
            return NULL;
        }

        /* delete the current session */
        (IsPhase1Exch == TRUE) ? IkeDelPhase1Session (pSessionInfo) :
            IkeDelTransSession (pSessionInfo);
        pSessionInfo = NULL;

        if (pPhase2SessionInfo->u1FsmState == IKE_QM_WAIT_ON_IPSEC)
        {
            /* Phase2SessionInit would have sent a message to
               IPSec to check for duplicate SPI, will have to
               wait for the reply before starting the Phase2
               negotiation */
            return NULL;
        }

        if (IkeCoreProcess (pPhase2SessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "DoPostPhase1Action: IkeCoreProcess " "failed!\n");
            if (pPhase2SessionInfo != NULL)
            {
                if (pPhase2SessionInfo->bDelSession == IKE_TRUE)
                {
                    if (IkeDeleteSession (pPhase2SessionInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSocketHandler:\
                                     IkeDeleteSession Failed\n");
                        return NULL;
                    }
                }
                else
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pPhase2SessionInfo->
                                        pu1LastRcvdPkt);
                    pPhase2SessionInfo->pu1LastRcvdPkt =
                        pPhase2SessionInfo->pu1LastRcvdGoodPkt;
                    pPhase2SessionInfo->u2LastRcvdPktLen =
                        pPhase2SessionInfo->u2LastRcvdGoodPktLen;
                    pPhase2SessionInfo->pu1LastRcvdGoodPkt = NULL;
                    pPhase2SessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
                }
            }
            return NULL;
        }

        /* Packet will be sent below, no need to duplicate code */
        return (pPhase2SessionInfo);
    }

    /* Phase1/Transaction Session is Done. Delete the current session */
    (IsPhase1Exch == TRUE) ? IkeDelPhase1Session (pSessionInfo) :
        IkeDelTransSession (pSessionInfo);

    return NULL;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeTrigger
 *  Description   : Called form ipsec to post message to ike.
 *  Input(s)      : u2ConfIndex - Phase 2 conf table index.
 *                  u1Protocol - Higher Layer Protocol.
 *                  u2PortNumber - Higher Layer Port Number.
 *                  pSrcNet - The source network
 *                  pDstNet - The destination network
 *                  pTEAddr - The local tunnel end-point address
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS. 
 * ---------------------------------------------------------------
 */

INT1
IkeTrigger (UINT1 u1Protocol, UINT2 u2PortNumber,
            tIkeNetwork * pSrcNet, tIkeNetwork * pDstNet, tIkeIpAddr * pTEAddr)
{
    tBufChainHeader    *pOutBufChain = NULL;
    tIkeQMsg            IkeQueue;
    tIkeNetwork        *pIkeSrcNet = NULL;
    tIkeNetwork        *pIkeDestNet = NULL;

    tIkeIpAddr         *pIkeLocalTEAddr = NULL;

    /* IkeQueue.u1Addr = IKE_ZERO; */
    pOutBufChain = IKE_BUF_ALLOCATE_CHAIN (sizeof (tIkeQMsg), IKE_ZERO);

    if (pOutBufChain == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTrigger:unable to allocate buffer\n");
        return IKE_FAILURE;
    }

    IkeQueue.u4MsgType = IPSEC_IF_MSG;
    IkeQueue.IkeIfParam.IkeIPSecParam.u4MsgType = IKE_IPSEC_NEW_KEY;
    IkeQueue.IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1Protocol =
        u1Protocol;
    IkeQueue.IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u2PortNumber =
        u2PortNumber;
    pIkeSrcNet =
        &IkeQueue.IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet;
    pIkeDestNet =
        &IkeQueue.IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet;
    pIkeLocalTEAddr =
        &IkeQueue.IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr;
    if (pSrcNet != NULL)
    {
        IKE_MEMCPY ((UINT1 *) pIkeSrcNet, (UINT1 *) pSrcNet,
                    sizeof (tIkeNetwork));
    }

    if (pDstNet != NULL)
    {
        IKE_MEMCPY ((UINT1 *) pIkeDestNet, (UINT1 *) pDstNet,
                    sizeof (tIkeNetwork));
    }

    if (pTEAddr != NULL)
    {
        IKE_MEMCPY ((UINT1 *) pIkeLocalTEAddr, (UINT1 *) pTEAddr,
                    sizeof (tIkeIpAddr));
    }

    if (IKE_BMC_ASSIGN_DATA (pOutBufChain, &IkeQueue, sizeof (tIkeQMsg))
        != CRU_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTrigger:unable to copy to CRU\n");
        IKE_BUF_RELEASE (pOutBufChain, IKE_ZERO);
        return IKE_FAILURE;
    }

    if (OsixSendToQ ((UINT4) ZERO, IKE_QUEUE_NAME,
                     pOutBufChain, OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTrigger:unable to send it to IKE Q\n");
        IKE_BUF_RELEASE (pOutBufChain, IKE_ZERO);
        return IKE_FAILURE;
    }

    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTrigger:unable to send Event to IKE\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}
