/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * 
 *  $Id: ikeprocess.c,v 1.30 2015/07/31 05:46:20 siva Exp $
 * 
 *  Description:This file contains functions for processing the isakmp
 *              Message. 
 * 
 ********************************************************************/

#include "ikegen.h"
#include "ikeprocess.h"
#include "ikedhgroup.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikecert.h"
#include "auth_db.h"
#include "utilrand.h"
#include "ikememmac.h"

/* #include "cligen.h" */

extern tIkeVendorIdPayload gaIkeVendorIdPayload[];
UINT4               gau4TransP1HashAlgo[IKE_MAX_HASH] =
    { IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO,
    IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO,
    IKE_ZERO, IKE_FOUR, IKE_FIVE, IKE_SIX, IKE_SEVEN
};
UINT4               gau4TransP2HashAlgo[IKE_MAX_HASH] =
    { IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO,
    IKE_NINE, IKE_ZERO, IKE_ZERO, IKE_ZERO, IKE_ZERO,
    IKE_ZERO, IKE_ZERO, IKE_FIVE, IKE_SIX, IKE_SEVEN, IKE_ZERO
};

static INT1         (*gaiIkeProcessFunc[]) (tIkeSessionInfo *, UINT1 *) =
{
NULL,
        IkeProcessSa,
        NULL,
        NULL,
        IkeProcessKE,
        IkeProcessId,
        IkeProcessCertificate,
        IkeProcessCertRequest,
        IkeProcessHash,
        IkeProcessSignature,
        IkeProcessNonce, IkeProcessNotify, IkeProcessDelete,
        IkeProcessVendorId, NULL, NULL, NULL, NULL};

/************************************************************************/
/*  Function Name   :IkeProcess                                         */
/*                  :                                                   */
/*  Description     :This function is used to process all the payloads  */
/*                  :in an Isakmp Message                               */
/*                  :                                                   */
/*  Input(s)        :u1PayLoadType : Identifies the first payload in an */
/*                  :Isakmp Message                                     */
/*                  :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :pu1PayLoad : The received Isakmp message to be     */
/*                  :processed                                          */
/*                  :belongs to phase-1 or phase-2                      */
/*                  :                                                   */
/*  Output(s)       :Processed Status                                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcess (UINT1 u1PayLoadType, tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tGenericPayLoad     Dummy;
    UINT2               u2Len = IKE_ZERO;
    UINT1               u1NumOfNatdPayLoad = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_ASSERT (pSesInfo->IkeRxPktInfo.pu1RawPkt != NULL);
    /* Process the Isakmp Message until the Last payload is 
     * encountered */

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    while (u1PayLoadType != IKE_ZERO)
    {
        if ((u1PayLoadType >= IKE_MAX_PAYLOADS) &&
            (u1PayLoadType != IKE_NAT_DETECT_PAYLOADS))
        {
            IKE_SET_ERROR (pSesInfo, IKE_INVALID_PAYLOAD);
            IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcess: Packet Type Invalid(%d) from peer: %s\n",
                          u1PayLoadType, pu1PeerAddrStr);
            IncIkeDropPkts (&pSesInfo->pIkeSA->IpLocalAddr);
            return (IKE_FAILURE);
        }
        IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, IKE_GEN_PAYLOAD_SIZE);

        Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);

        if (u1PayLoadType == IKE_NAT_DETECT_PAYLOADS)
        {
            IkeProcessNatDPayLoad (pSesInfo, pu1PayLoad, u1NumOfNatdPayLoad);
            u1NumOfNatdPayLoad = (UINT1) (u1NumOfNatdPayLoad + IKE_ONE);
        }
        else
        {
            if (((gaiIkeProcessFunc[u1PayLoadType]) != NULL) &&
                ((*gaiIkeProcessFunc[u1PayLoadType]) (pSesInfo, pu1PayLoad)
                 == IKE_FAILURE))
            {
                IncIkeDropPkts (&pSesInfo->pIkeSA->IpLocalAddr);
                return (IKE_FAILURE);
            }
        }

        u2Len = Dummy.u2PayLoadLen;
        u1PayLoadType = Dummy.u1NextPayLoad;
        pu1PayLoad += u2Len;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeValidatePacket                                 */
/*  Description     :This function is used to validate payloads in an   */
/*                  :Isakmp Message                                     */
/*                  :                                                   */
/*  Input(s)        :i4TotLen : The total length of the Isakmp Message  */
/*                  :u1PayLoadType : Identifies the type of payload     */
/*                  :pu1PayLoad : Pointer to the payload to be validated*/
/*                  :pi4Len : Length of the PayLoad validated           */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeValidatePacket (tIkeSessionInfo * pSessionInfo, UINT4 u4TotLen,
                   UINT1 u1PayLoadType, UINT1 *pu1PayLoad)
{
    UINT1              *pu1TempPayLoad = pu1PayLoad;
    UINT4               u4Len = IKE_ZERO;
    /*    UINT1               u1EncrMask = 0 ; */
    /*    Commented to Interop with strongswan */

    if (pu1PayLoad == NULL)
    {
        IKE_SET_ERROR (pSessionInfo, IKE_PAYLOAD_MALFORMED);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidatePacket: Malformed Pkt\n");
        IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
        return (IKE_FAILURE);
    }

    if ((u4TotLen <= sizeof (tIsakmpHdr)) || (u4TotLen > IKE_MAX_PACKET_SIZE))
    {
        IKE_SET_ERROR (pSessionInfo, IKE_PAYLOAD_MALFORMED);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidatePacket: Malformed Pkt\n");
        IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
        return (IKE_FAILURE);
    }

    /* Validate Payloads to ensure that the Length of the Individual
       payloads do not exceed the size of the Isakmp Message */
    while (u1PayLoadType != IKE_NONE_PAYLOAD)
    {
        if (IkeValidPayLoad (pSessionInfo, &u4Len, u4TotLen, &u1PayLoadType,
                             pu1TempPayLoad) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeValidatePacket: Malformed Pkt\n");
            IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
            return (IKE_FAILURE);
        }
        pu1TempPayLoad = pu1PayLoad + u4Len;
    }

    if ((u4Len + sizeof (tIsakmpHdr)) == u4TotLen)
    {
        return IKE_SUCCESS;
    }

    if ((u4Len + sizeof (tIsakmpHdr)) < u4TotLen)
    {
        /* This code is commented to interop with strongswan.
         * strongswan a sending 3 bytes of extra padding data with Cert Request
         * payload. Need to findout why it is doing so and put proper fix.
         * 
         u1EncrMask = IKE_TEST_ENCR_MASK (pSessionInfo->IkeRxPktInfo.pu1RawPkt);

         if (u1EncrMask == 0)
         {
         Not an encrypted packet, Lengths should have been equal 
         IKE_SET_ERROR (pSessionInfo, IKE_UNEQUAL_PAYLOAD_LEN);
         IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
         "IkeValidatePacket: Malformed Pkt\n");
         IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
         return (IKE_FAILURE);
         }
         */

        /* Check for padding in case of encrypted packet */
        pSessionInfo->IkeRxPktInfo.u4PacketLen = u4Len + sizeof (tIsakmpHdr);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidatePacket: Malformed Pkt\n");
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeValidPayLoad                                    */
/*  Description     : This function validates the payload in the Isakmp */
/*                  : Message                                           */
/*                  :                                                   */
/*  Input(s)        :pi4CurLen :The Length of the Current payload in    */
/*                  : the Isakmp Message                                */
/*                  :i4TotLen : Total length of the Isakmp Message      */
/*                  :pu1PayloadType : The payload type                  */
/*                  :pu1PayLoad : Pointer to the payload to be validated*/
/*                  :                                                   */
/*  Output(s)       :Validates the payload and returns their status     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeValidPayLoad (tIkeSessionInfo * pSessionInfo, UINT4 *pu4CurLen,
                 UINT4 u4TotLen, UINT1 *pu1PayloadType, UINT1 *pu1PayLoad)
{
    tGenericPayLoad     Dummy;
    tIkePayload        *pIkePayload = NULL;
    UINT1               u1Flag = IKE_FALSE;

    if ((*pu1PayloadType >= IKE_MAX_PAYLOADS) &&
        (*pu1PayloadType != IKE_NAT_DETECT_PAYLOADS) &&
        (*pu1PayloadType != IKE_NAT_ORIG_ADDR_PAYLOADS))
    {
        IKE_SET_ERROR (pSessionInfo, IKE_INVALID_PAYLOAD);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidPayLoad:Invalid Payload Type\n");
        return (IKE_FAILURE);
    }

    /* Copy the first payload to Dummy payload for validation */
    IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, IKE_GEN_PAYLOAD_SIZE);

    Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);

    if ((Dummy.u2PayLoadLen > u4TotLen) || (Dummy.u2PayLoadLen == IKE_ZERO))
    {
        IKE_SET_ERROR (pSessionInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidPayLoad:Pakcet Len mismatch\n");
        return (IKE_FAILURE);
    }

    /* Store the payloads in the Payload array */
    if (MemAllocateMemBlock
        (IKE_PAYLOAD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkePayload) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidPayLoad: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pIkePayload == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidPayLoad:allocate failed!\n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pIkePayload, sizeof (tIkePayload));
    pIkePayload->u2PayLoadType = *pu1PayloadType;
    pIkePayload->u2NxtPayLoadType = Dummy.u1NextPayLoad;
    pIkePayload->pRawPayLoad = pu1PayLoad;
    pIkePayload->u4PayLoadLen = Dummy.u2PayLoadLen;
    if ((*pu1PayloadType > IKE_ZERO) && (*pu1PayloadType < IKE_MAX_PAYLOADS))
    {
        SLL_INSERT_IN_FRONT (&pSessionInfo->IkeRxPktInfo.
                             aPayload[*pu1PayloadType],
                             (t_SLL_NODE *) pIkePayload);
        u1Flag = IKE_TRUE;
    }

    /* If the payload is SA,  validate proposal and transform payloads too */
    if (*pu1PayloadType == IKE_SA_PAYLOAD)
    {
        if (IkeValidSa (pSessionInfo, pu4CurLen, u4TotLen, pu1PayLoad) ==
            IKE_FAILURE)
        {
            return (IKE_FAILURE);
        }
    }
    else
    {
        *pu4CurLen += Dummy.u2PayLoadLen;
    }
    *pu1PayloadType = Dummy.u1NextPayLoad;

    if (u1Flag == IKE_FALSE)
    {
        MemReleaseMemBlock (IKE_PAYLOAD_MEMPOOL_ID, (UINT1 *) pIkePayload);
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeValidSa 
 *  Description   : Check the SA pay load validity.
 *  Input(s)      : pu4CurLen - processed length of the packet.
 *                  u4TotalLen - Total length of the packet.
 *                  pu1Packet - packet array pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS
 * ---------------------------------------------------------------
 */

INT1
IkeValidSa (tIkeSessionInfo * pSesInfo, UINT4 *pu4CurLen,
            UINT4 u4TotalLen, UINT1 *pu1Packet)
{
    tSaPayLoad          Sa;
    tProposalPayLoad    Prop;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4Len = IKE_ZERO, u4Count = IKE_ZERO;

    u4Len = (*pu4CurLen);
    IKE_MEMCPY ((UINT1 *) &Sa, pu1Packet, IKE_SA_PAYLOAD_SIZE);
    Sa.u2PayLoadLen = IKE_NTOHS (Sa.u2PayLoadLen);
    if ((u4Len + Sa.u2PayLoadLen > u4TotalLen) || (Sa.u2PayLoadLen == IKE_ZERO))
    {
        IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidSa: Packet Length mismatch\n");
        return IKE_FAILURE;
    }

    if (Sa.u2PayLoadLen == IKE_SA_PAYLOAD_SIZE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidSa: Empty SA payload\n");
        return IKE_FAILURE;
    }

    u4Len = IKE_SA_PAYLOAD_SIZE;
    while (u4Len < Sa.u2PayLoadLen)
    {
        pu1Ptr = pu1Packet + u4Len;
        IKE_MEMCPY ((UINT1 *) &Prop, pu1Ptr, IKE_PROP_PAYLOAD_SIZE);
        Prop.u2PayLoadLen = IKE_NTOHS (Prop.u2PayLoadLen);
        if ((Prop.u2PayLoadLen == IKE_ZERO) || (Prop.u1NumTran == IKE_ZERO))
        {
            IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeValidSa: Proposal payload zero or zero trans\n");
            return IKE_FAILURE;
        }

        if ((Prop.u1SpiSize != sizeof (UINT4)) &&
            (Prop.u1SpiSize != IKE_ZERO) && (Prop.u1SpiSize != IKE_EIGHT) &&
            (Prop.u1SpiSize != IKE_TWO))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeValidSa: SPI Size is greater than 4!\n");
            return IKE_FAILURE;
        }

        pu1Ptr += (IKE_PROP_PAYLOAD_SIZE + Prop.u1SpiSize);
        for (u4Count = IKE_ZERO; u4Count < Prop.u1NumTran; u4Count++)
        {
            if (IkeValidTransform (pSesInfo, Prop.u2PayLoadLen, &pu1Ptr) ==
                IKE_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        u4Len += Prop.u2PayLoadLen;
    }
    if (u4Len != Sa.u2PayLoadLen)
    {
        IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidSa:Sa pay load mismatch\n");
        return IKE_FAILURE;
    }
    *pu4CurLen += Sa.u2PayLoadLen;
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeValidTransform 
 *  Description   : Check the Transform pay load validity.
 *  Input(s)      : u2Len - Propsal payload len.
 *                  pu1Packet - packet array pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS
 * ---------------------------------------------------------------
 */
INT1
IkeValidTransform (tIkeSessionInfo * pSesInfo, UINT2 u2Len,
                   UINT1 **pu1IncommingPkt)
{
    tTransPayLoad       Trans;
    tSaVpiAttribute     SaVpiAttrib;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT2               u2Flag = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO;

    if (u2Len < IKE_TRAN_PAYLOAD_SIZE)
    {
        IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidTransform:unequal payload lengths!\n");
        return IKE_FAILURE;
    }

    IKE_MEMCPY ((UINT1 *) &Trans, (UINT1 *) *pu1IncommingPkt,
                IKE_TRAN_PAYLOAD_SIZE);
    Trans.u2PayLoadLen = IKE_NTOHS (Trans.u2PayLoadLen);
    if ((Trans.u2PayLoadLen > u2Len) || (Trans.u2PayLoadLen == IKE_ZERO))
    {
        IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidTransform:Unequal payload lengths!\n");
        return IKE_FAILURE;
    }
    u4Len = IKE_TRAN_PAYLOAD_SIZE;
    while (u4Len < Trans.u2PayLoadLen)
    {
        pu1Ptr = *pu1IncommingPkt + u4Len;
        IKE_MEMCPY ((UINT1 *) &u2Attr, pu1Ptr, sizeof (UINT2));
        u2Attr = IKE_NTOHS (u2Attr);

        u2Flag = (u2Attr & IKE_BASIC_ATTR);
        if (u2Flag == IKE_ZERO)
        {
            IKE_MEMCPY ((UINT1 *) &SaVpiAttrib, pu1Ptr,
                        sizeof (tSaVpiAttribute));
            SaVpiAttrib.u2VpiLength = IKE_NTOHS (SaVpiAttrib.u2VpiLength);
            if (SaVpiAttrib.u2VpiLength == IKE_ZERO)
            {
                IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeValidTransform:Variable attrib length zero\n");
                return IKE_FAILURE;
            }
            u4Len += SaVpiAttrib.u2VpiLength;
        }
        u4Len += sizeof (tSaVpiAttribute);
    }
    if (u4Len != Trans.u2PayLoadLen)
    {
        IKE_SET_ERROR (pSesInfo, IKE_UNEQUAL_PAYLOAD_LEN);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidTransform:Trans pay load mismatch\n");
        return IKE_FAILURE;
    }
    *pu1IncommingPkt += Trans.u2PayLoadLen;
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeProcessKE                                       */
/*  Description     :This function is used to process the key exchange  */
/*                  :payload in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :                                                   */
/*  Output(s)       :Process the key exchange payload                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessKE (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{
    tKeyExcPayLoad      KE;
    UINT4               u4Len = IKE_ZERO;
    tIkePayload        *pKEPayload = NULL;
    UINT1              *pu1PayLoad = NULL;
    CHR1               *pu1PeerAddrStr = NULL;

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    IKE_UNUSED (pu1Pd);
    if (pSesInfo->u1ExchangeType != IKE_QUICK_MODE)
    {
        if (pSesInfo->u1Role == IKE_INITIATOR)
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_KEY_EXCH) ||
                  (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessKE: \
                             Not Expecting this Packet from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_SA_SETUP) ||
                  (pSesInfo->u1FsmState == IKE_AG_NO_STATE)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessKE: \
                             Not Expecting this Packet from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }

    /* There will be only one KE payload */
    pKEPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_KEY_PAYLOAD]);
    if (pKEPayload == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessKE: KE Payload not received from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    pu1PayLoad = pKEPayload->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &KE, pu1PayLoad, IKE_KEY_PAYLOAD_SIZE);

    KE.u2PayLoadLen = IKE_NTOHS (KE.u2PayLoadLen);
    u4Len = (UINT4) (KE.u2PayLoadLen - IKE_KEY_PAYLOAD_SIZE);

    IKE_ASSERT (u4Len <= pSesInfo->pDhInfo->u4Length)
        pSesInfo->pHisDhPub = HexNumNew (u4Len);
    if (pSesInfo->pHisDhPub == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessKE:Unable to allocate for pHisDhPub\n");
        return IKE_FAILURE;
    }

    IKE_MEMCPY (pSesInfo->pHisDhPub->pu1Value, (UINT1 *)
                (pu1PayLoad + IKE_KEY_PAYLOAD_SIZE), u4Len);

    if (pSesInfo->pMyDhPub == NULL)
    {
        pSesInfo->pMyDhPub = HexNumNew (u4Len);
        if (pSesInfo->pMyDhPub == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessKE:Unable to allocate for pMyDhPub\n");
            return IKE_FAILURE;
        }

        IKE_ASSERT (pSesInfo->pMyDhPrv == NULL);

        pSesInfo->pMyDhPrv = HexNumNew (u4Len);
        if (pSesInfo->pMyDhPrv == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessKE:Unable to allocate for pMyDhPrv\n");
            return IKE_FAILURE;
        }

        if (FSDhGenerateKeyPair (pSesInfo->pDhInfo, pSesInfo->pMyDhPub,
                                 pSesInfo->pMyDhPrv) < IKE_ZERO)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessKE:\
                         Failed to Generate DH Key pair for peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }
    }

    IKE_ASSERT (pSesInfo->pDhSharedSecret == NULL);
    pSesInfo->pDhSharedSecret = HexNumNew (u4Len);
    if (pSesInfo->pDhSharedSecret == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessKE: \
                     Unable to allocate for Shared Secret for peer: %s\n", pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    if (FSDhCompute (pSesInfo->pDhInfo, pSesInfo->pMyDhPub, pSesInfo->pMyDhPrv,
                     pSesInfo->pHisDhPub, pSesInfo->pDhSharedSecret) < IKE_ZERO)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessKE:Unable to compute secret for peer: %s\n",
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessId                                       */
/*  Description     :This function is used to process the ID PayLoad of */
/*                  :the Isakmp Mesg                                    */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :                                                   */
/*  Output(s)       :Process the Id PayLoad                             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessId (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tIdPayLoad          Id;
    UINT4               u4IdLen = IKE_ZERO;
    tIkeKey            *pIkeKey = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIp4Addr            u4Ip4Addr;
    tIkePhase1ID       *pPeerId = NULL;
    tIkeRemoteAccessInfo *pRAInfo = NULL;
    CHR1               *pu1PeerAddrStr = NULL;
    INT1                i1Buf[MAX_NAME_LENGTH];

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if ((pSesInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if (pSesInfo->u1Role == IKE_INITIATOR)
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_AUTH_EXCH) ||
                  (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessID: \
                             Not Expecting this Packet for peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_KEY_EXCH) ||
                  (pSesInfo->u1FsmState == IKE_AG_NO_STATE)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessID: \
                             Not Expecting this Packet from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }

    IKE_BZERO (&Id, sizeof (tIdPayLoad));
    IKE_MEMCPY ((UINT1 *) &Id, pu1PayLoad, IKE_ID_PAYLOAD_SIZE);

    Id.u2PayLoadLen = IKE_NTOHS (Id.u2PayLoadLen);
    Id.u2Port = IKE_NTOHS (Id.u2Port);

    /* Check the received port and protocol in case of Phase1 negotiation *
     * It should be UDP protocol, 500 port else they should be set 0 */
    if ((pSesInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if ((Id.u1Protocol != IKE_UDP) && (Id.u1Protocol != IKE_NONE))
        {
            IKE_SET_ERROR (pSesInfo, IKE_INVALID_PROTOCOL_ID);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessID: Invalid Protocol information in "
                          "Phase1 from peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }

        if ((Id.u2Port != IKE_UDP_PORT) && (Id.u2Port != IKE_NONE))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessID: \
                         Invalid Port information in Phase1 from peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }
    }

    u4IdLen = (UINT4) (Id.u2PayLoadLen - IKE_ID_PAYLOAD_SIZE);

    /* Copy (Id Payload - Generic header) for Hash computation purposes */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1IdPayloadRecd) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessID: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pSesInfo->pu1IdPayloadRecd == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessID:Couldn't allocate for storing"
                     "Recd Id Payload\n");
        return IKE_FAILURE;
    }

    IKE_MEMCPY (pSesInfo->pu1IdPayloadRecd,
                (pu1PayLoad + sizeof (tGenericPayLoad)),
                (Id.u2PayLoadLen - sizeof (tGenericPayLoad)));
    pSesInfo->u4IdPayloadRecdLen = Id.u2PayLoadLen - sizeof (tGenericPayLoad);

    if (Id.u1IdType == IPV4ADDR)
    {
        IKE_MEMCPY (&u4Ip4Addr, 
                    &(pSesInfo->RemoteTunnelTermAddr.uIpAddr),sizeof(UINT4));
    }

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE_PRESHARED_KEY)
    {
        if (pSesInfo->pIkeSA->ResponderID.i4IdType == IKE_NONE)
        {
            pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessID:Engine not found\n");
                return (IKE_FAILURE);
            }

            /* If Responder ID is not set, do a look-up in the key table, */
            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
            {
                if (pIkeKey->u1Status == IKE_ACTIVE)
                {
                    if (Id.u1IdType == pIkeKey->KeyID.i4IdType)
                    {
                        if (Id.u1IdType == IPV4ADDR)
                        {
                            if (IKE_MEMCMP (&pIkeKey->KeyID.uID,
                                            &u4Ip4Addr,
                                            pIkeKey->KeyID.u4Length)
                                == IKE_ZERO)
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (IKE_MEMCMP (&pIkeKey->KeyID.uID,
                                            (pu1PayLoad + IKE_ID_PAYLOAD_SIZE),
                                            pIkeKey->KeyID.u4Length)
                                == IKE_ZERO)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            GIVE_IKE_SEM ();

            if (pIkeKey != NULL)
            {
                /* Get the Pre-shared Key */

                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pSesInfo->IkePhase1Info.
                     pu1PreSharedKey) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessID: unable to allocate "
                                 "buffer\n");
                    return IKE_FAILURE;
                }

                if (pSesInfo->IkePhase1Info.pu1PreSharedKey == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessID:Malloc fails\n");
                    return (IKE_FAILURE);

                }

                /* XXX PreShared Key Configuration will not
                 * be NULL terminated
                 */
                STRCPY (pSesInfo->IkePhase1Info.pu1PreSharedKey,
                        pIkeKey->au1KeyString);
                pSesInfo->IkePhase1Info.u2PreSharedKeyLen =
                    (UINT2) STRLEN (pIkeKey->au1KeyString);

                /* Fill in the Responder ID */
                pSesInfo->pIkeSA->ResponderID.i4IdType =
                    pIkeKey->KeyID.i4IdType;
                pSesInfo->pIkeSA->ResponderID.u4Length =
                    pIkeKey->KeyID.u4Length;
                IKE_MEMCPY (&pSesInfo->pIkeSA->ResponderID.uID,
                            &pIkeKey->KeyID.uID, pIkeKey->KeyID.u4Length);

            }
            else
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessID:Invalid ID recd from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            /* Check the responder Id against the received Id */
            if (Id.u1IdType == pSesInfo->pIkeSA->ResponderID.i4IdType)
            {
                if (Id.u1IdType == IPV4ADDR)
                {
                    if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID,
                                    &u4Ip4Addr,
                                    pSesInfo->pIkeSA->ResponderID.u4Length) !=
                        IKE_ZERO)
                    {
                        IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessID:ID's do not match" " the ID\
                                     used for lookup\
                                     of preshared key for peer: %s\n", pu1PeerAddrStr);
                        return (IKE_FAILURE);
                    }

                }
                else if (IKE_IPSEC_ID_DER_ASN1_DN == Id.u1IdType)
                {
                    if (IkeConvertDERToStr (pu1PayLoad + IKE_ID_PAYLOAD_SIZE,
                                            Id.u2PayLoadLen -
                                            IKE_ID_PAYLOAD_SIZE,
                                            i1Buf) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessID: Failed converting DER to STR "
                                     "for ID match\n");
                        return (IKE_FAILURE);
                    }
                    if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID, i1Buf,
                                    pSesInfo->pIkeSA->ResponderID.u4Length) !=
                        0)
                    {
                        IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessID: DN ID's do not match with "
                                      " the existing IKE SA for peer: %s\n",
                                      pu1PeerAddrStr);
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID,
                                    (pu1PayLoad + IKE_ID_PAYLOAD_SIZE),
                                    pSesInfo->pIkeSA->ResponderID.u4Length) !=
                        IKE_ZERO)
                    {
                        IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessID:ID's do not match"
                                      " the ID used for \
                                     lookup of preshared key for peer: %s\n", pu1PeerAddrStr);
                        return (IKE_FAILURE);
                    }
                }
            }
            else
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessID:ID's do not match"
                              " the ID used for lookup\
                             of preshared key for peer: %s\n", pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }
    else
    {
        /* Store the peer id */
        pSesInfo->pIkeSA->ResponderID.i4IdType = Id.u1IdType;
        pSesInfo->pIkeSA->ResponderID.u4Length = u4IdLen;

        /* 
         * for IKE_IPSEC_ID_DER_ASN1_DN type the Id will be of ASN.1 DER format 
         * so the length will be more than the actual string. 
         * so don't check for this type 
         */
        if (IKE_IPSEC_ID_DER_ASN1_DN != Id.u1IdType &&
            u4IdLen >= MAX_NAME_LENGTH)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessID: Invalid Peer ID Length for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }

        switch (Id.u1IdType)
        {
            case IPV4ADDR:
            {
                pSesInfo->pIkeSA->ResponderID.uID.Ip4Addr = u4Ip4Addr;
                break;
            }

            case IPV6ADDR:
            {
                if (u4IdLen >= IP6_ADDR_SIZE)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkeProcessID: \
                                 Invalid Peer ID Length for peer: %s\n", pu1PeerAddrStr);
                    return (IKE_FAILURE);
                }
                IKE_MEMCPY (pSesInfo->pIkeSA->ResponderID.uID.Ip6Addr.u1_addr,
                            (pu1PayLoad + IKE_ID_PAYLOAD_SIZE), u4IdLen);
                break;
            }

            case IKE_IPSEC_ID_USER_FQDN:
            {
                IKE_MEMCPY (pSesInfo->pIkeSA->ResponderID.uID.au1Email,
                            (pu1PayLoad + IKE_ID_PAYLOAD_SIZE), u4IdLen);
                break;
            }

            case IKE_IPSEC_ID_DER_ASN1_DN:
            {
                /* convert the DER to string before storing in session Id */
                if (IkeConvertDERToStr (pu1PayLoad + IKE_ID_PAYLOAD_SIZE,
                                        Id.u2PayLoadLen - IKE_ID_PAYLOAD_SIZE,
                                        i1Buf) == IKE_FAILURE)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkeProcessID: Failed to convert DER to ASN.1 DN: %s\n",
                                  pu1PeerAddrStr);
                }
                IKE_MEMCPY (pSesInfo->pIkeSA->ResponderID.uID.au1Dn,
                            i1Buf, IKE_STRLEN (i1Buf));
                pSesInfo->pIkeSA->ResponderID.u4Length = IKE_STRLEN (i1Buf);
                break;
            }

            case IKE_IPSEC_ID_FQDN:
            {
                IKE_MEMCPY (pSesInfo->pIkeSA->ResponderID.uID.au1Fqdn,
                            (pu1PayLoad + IKE_ID_PAYLOAD_SIZE), u4IdLen);
                break;
            }

            default:
                break;
        }
    }

    pPeerId = &(pSesInfo->pIkeSA->ResponderID);
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessID:Engine not found\n");
        return (IKE_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pRAInfo, tIkeRemoteAccessInfo *)
    {
        if (IkeComparePhase1IDs (&pRAInfo->Phase1ID, pPeerId) == IKE_ZERO)
        {
            IKE_MEMCPY (&(pSesInfo->IkeTransInfo.IkeRAInfo), pRAInfo,
                        sizeof (tIkeRemoteAccessInfo));
            break;
        }
    }

    if (pRAInfo != NULL)
    {
        pSesInfo->pIkeSA->bXAuthEnabled = pRAInfo->bXAuthEnabled;
        pSesInfo->pIkeSA->bCMEnabled = pRAInfo->bCMEnabled;
        pSesInfo->pIkeSA->bXAuthDone = FALSE;
        pSesInfo->pIkeSA->bCMDone = FALSE;
    }
    else
    {
        pSesInfo->pIkeSA->bXAuthEnabled = FALSE;
        pSesInfo->pIkeSA->bCMEnabled = FALSE;
        pSesInfo->pIkeSA->bXAuthDone = FALSE;
        pSesInfo->pIkeSA->bCMDone = FALSE;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessPhase2Id                                 */
/*  Description     :This function is used to process the ID PayLoads   */
/*                  :of the Phase2 Isakmp Mesg                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessPhase2Id (tIkeSessionInfo * pSesInfo)
{
    tIkePayload        *pMyIdPayload = NULL;
    tIkePayload        *pHisIdPayload = NULL;
    tIdPayLoad          Id;
    tIkeNetwork         MyId;
    tIkeNetwork         HisId;
    UINT4               u4Count = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;

    u4Count = SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ID_PAYLOAD]);
    if ((u4Count != IKE_TWO) && (u4Count != IKE_ZERO))
    {
        /* Either we should get both ID payloads or none */
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessPhase2Id:Error in extracting Id Payloads\n");
        return (IKE_FAILURE);
    }

    /* Initialize MyId and HisId */
    IKE_BZERO (&MyId, sizeof (tIkeNetwork));
    IKE_BZERO (&HisId, sizeof (tIkeNetwork));

    IKE_BZERO (&Id, sizeof (tIdPayLoad));

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessPhase2Id:IkeGetIkeEngineFromIndex(%d) failed\n",
                      pSesInfo->u4IkeEngineIndex);
        return (IKE_FAILURE);
    }

    if (u4Count == IKE_TWO)
    {
        /* Received both Local and remote ID payloads */
        /* We will receive Initiator Id first and then responder Id 
         * in the packet, and since we push into the linked list, 
         * responder IdPayload will be first, and then initiator will be 
         * extracted */
        if (pSesInfo->u1Role == IKE_RESPONDER)
        {
            pMyIdPayload =
                (tIkePayload *) SLL_FIRST
                (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ID_PAYLOAD]);
            pHisIdPayload =
                (tIkePayload *) SLL_NEXT
                (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ID_PAYLOAD],
                 (t_SLL_NODE *) pMyIdPayload);
        }
        else
        {
            pHisIdPayload =
                (tIkePayload *) SLL_FIRST
                (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ID_PAYLOAD]);
            pMyIdPayload =
                (tIkePayload *) SLL_NEXT
                (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ID_PAYLOAD],
                 (t_SLL_NODE *) pHisIdPayload);
        }

        if ((pMyIdPayload == NULL) || (pHisIdPayload == NULL))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessPhase2Id:Error in extracting Id "
                         "Payloads\n");
            return (IKE_FAILURE);
        }

        /* Process MyId first */
        IKE_MEMCPY ((UINT1 *) &Id, pMyIdPayload->pRawPayLoad,
                    IKE_ID_PAYLOAD_SIZE);
        Id.u2PayLoadLen = IKE_NTOHS (Id.u2PayLoadLen);
        Id.u2Port = IKE_NTOHS (Id.u2Port);
        IkeGetId (&Id, (pMyIdPayload->pRawPayLoad + IKE_ID_PAYLOAD_SIZE),
                  &MyId);

        /* Now HisId */
        IKE_MEMCPY ((UINT1 *) &Id, pHisIdPayload->pRawPayLoad,
                    IKE_ID_PAYLOAD_SIZE);
        Id.u2PayLoadLen = IKE_NTOHS (Id.u2PayLoadLen);
        Id.u2Port = IKE_NTOHS (Id.u2Port);
        IkeGetId (&Id, (pHisIdPayload->pRawPayLoad + IKE_ID_PAYLOAD_SIZE),
                  &HisId);
        /* 
           For initiator, already being done in IkePhase2SessionInit
         */
        if (pSesInfo->u1Role == IKE_RESPONDER)
        {
            TAKE_IKE_SEM ();
            if (pSesInfo->pIkeSA == NULL)
            {
                GIVE_IKE_SEM ();
                return (IKE_FAILURE);
            }
            if (pSesInfo->pIkeSA->bCMEnabled == IKE_TRUE)
            {
                if (pSesInfo->pIkeSA->bCMDone == IKE_FALSE)
                {
                    GIVE_IKE_SEM ();
                    IKE_SET_ERROR (pSesInfo, IKE_INVALID_EXCHANGE_TYPE);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessPhase2Id:\
                                 Quick Mode packet received " "before CM Enabled is Successful \n");
                    return (IKE_FAILURE);
                }
            }

            pCryptoMap =
                IkeGetCryptoMapFromIDs (pIkeEngine, &MyId, &HisId,
                                        IKE_ISAKMP_VERSION);
            if (pCryptoMap == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id: Unable to get Crypto Map\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pSesInfo->IkePhase2Policy, pCryptoMap,
                        sizeof (tIkeCryptoMap));

            /* Save the Inbound SPI's in the crypto map structure,
             * so that we can these can be used as index to scan and delete the 
             * required crypto map when processing the received
             * delete message */

            /* As only one transform is supported for each cryptomap, we 
             * the index is set to 0, if multiple transform for a crypto 
             * is supported then a loop should be used */
            if (pSesInfo->IkePhase2Policy.aTransformSetBundle[IKE_INDEX_0]->
                u1EspEncryptionAlgo != IKE_ZERO)
            {
                pCryptoMap->u4CurrSpi = pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
            }
            else
            {
                pCryptoMap->u4CurrSpi = pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4Spi;
            }

            /* Copy the Local and Remote Networks to IPSecBundle */
            IKE_MEMCPY (&pSesInfo->IkePhase2Info.IPSecBundle.LocalProxy,
                        &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
            IKE_MEMCPY (&pSesInfo->IkePhase2Info.IPSecBundle.RemoteProxy,
                        &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));
            GIVE_IKE_SEM ();

            if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_1)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo768;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_2)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo1024;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_5)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo1536;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == SEC_DH_GROUP_14)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo2048;
            }
        }
        else
        {
            /* Compare the received ID's with the stored ones */
            if ((IKE_FALSE ==
                 IkeCompareNetworks (&pSesInfo->IkePhase2Info.IPSecBundle.
                                     LocalProxy, &MyId, IKE_ISAKMP_VERSION))
                || (IKE_FALSE ==
                    IkeCompareNetworks (&pSesInfo->IkePhase2Info.IPSecBundle.
                                        RemoteProxy, &HisId,
                                        IKE_ISAKMP_VERSION)))
            {
                /* Received IDs different from configured ones */
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id: ID Mismatch!\n");
                return (IKE_FAILURE);
            }
        }
        return (IKE_SUCCESS);
    }
    else
    {
        /* We did not receive ID payloads, use the local and remote
         * tunnel term addresses to get crypto map
         */
        if (pSesInfo->u1Role == IKE_RESPONDER)
        {
            TAKE_IKE_SEM ();
            if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
            {
                pCryptoMap = IkeGetCryptoMap (pIkeEngine,
                                              &pIkeEngine->Ip4TunnelTermAddr,
                                              &pSesInfo->RemoteTunnelTermAddr);
            }
            else
            {
                pCryptoMap = IkeGetCryptoMap (pIkeEngine,
                                              &pIkeEngine->Ip6TunnelTermAddr,
                                              &pSesInfo->RemoteTunnelTermAddr);
            }
            if (pCryptoMap == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id: Unable to get Crypto Map\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pSesInfo->IkePhase2Policy, pCryptoMap,
                        sizeof (tIkeCryptoMap));
            /* Copy the Local and Remote Networks to IPSecBundle */
            IKE_MEMCPY (&pSesInfo->IkePhase2Info.IPSecBundle.LocalProxy,
                        &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
            IKE_MEMCPY (&pSesInfo->IkePhase2Info.IPSecBundle.RemoteProxy,
                        &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));
            GIVE_IKE_SEM ();

            if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_1)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo768;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_2)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo1024;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_5)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo1536;
            }
            else if (pSesInfo->IkePhase2Policy.u1Pfs == SEC_DH_GROUP_14)
            {
                pSesInfo->pDhInfo = &gDhGroupInfo2048;
            }
        }
        else
        {
            /* Initiator case */
            return (IKE_SUCCESS);
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessNonce                                    */
/*  Description     :This function is used to process the Nonce PayLoad */
/*                  :of the Isakmp Mesg                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :                                                   */
/*  Output(s)       :Process the Nonce PayLoad                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessNonce (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{
    tNoncePayLoad       Nonce;
    INT4                i4NonceLen = IKE_ZERO;
    tIkePayload        *pNoncePayload = NULL;
    UINT1              *pu1PayLoad = NULL;
    UINT1               u1NonceType = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_UNUSED (pu1Pd);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSesInfo->u1ExchangeType != IKE_QUICK_MODE)
    {
        if (pSesInfo->u1Role == IKE_INITIATOR)
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_KEY_EXCH) ||
                  (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessNonce: \
                             Not Expecting this Packet for peer: %s\n", pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_SA_SETUP) ||
                  (pSesInfo->u1FsmState == IKE_AG_NO_STATE)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessNonce: \
                             Not Expecting this Packet for peer: %s\n", pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }

    /* There will be only one NONCE payload */
    pNoncePayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_NONCE_PAYLOAD]);
    if (pNoncePayload == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessNonce: \
                     Nonce Payload not received from peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    pu1PayLoad = pNoncePayload->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &Nonce, pu1PayLoad, IKE_NONCE_PAYLOAD_SIZE);
    Nonce.u2PayLoadLen = IKE_NTOHS (Nonce.u2PayLoadLen);

    /* Received nonce payload length should be between 8 and 256 */
    if ((Nonce.u2PayLoadLen < IKE_MIN_NONCE_LENGTH) ||
        (Nonce.u2PayLoadLen > IKE_MAX_NONCE_LENGTH))
    {
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessNonce: Incorrect Nonce Payload length "
                      "received %d from peer: %s\n", Nonce.u2PayLoadLen,
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    i4NonceLen = Nonce.u2PayLoadLen - IKE_NONCE_PAYLOAD_SIZE;

    /* Copy the received nonce to the session info structure */

    /* Since we are in ProcessNonce function, if we are Initiator,
     * then we should
     set the responder nonce, and vice-versa
     */
    u1NonceType =
        (pSesInfo->u1Role == IKE_INITIATOR) ? IKE_RESPONDER : IKE_INITIATOR;

    /* The fourth param is to tell the function whether to set the Initiator or
       Responder Nonce
     */
    if (IkeSetNonceInSessionInfo (pSesInfo,
                                  (pu1PayLoad + IKE_NONCE_PAYLOAD_SIZE),
                                  (UINT2) i4NonceLen,
                                  u1NonceType) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessNonce:Cannot store Nonce\n");
        return (IKE_FAILURE);
    }

    /* Unable to move out IkeGenSkeyId for Aggressive mode 
       Initiator case. It is necessary to have the
       KeyId to verify the Hash payload sent by the peer, and the Hash
       Payload is right after the Nonce Payload
     */
    if ((pSesInfo->u1Role == IKE_INITIATOR) &&
        (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if (IkeGenSkeyId (pSesInfo) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeVerifyHash                                      */
/*  Description     :This function is used to compute hash for phase-1  */
/*                  :negotiation                                        */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :pu1Hash  :Pointer to the Computed Digest Value     */
/*                  :                                                   */
/*  Output(s)       :The Computed Digest Value                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeVerifyHash (tIkeSessionInfo * pSesInfo, UINT1 *pu1Hash)
{
    UINT4               u4Len = IKE_ZERO, u4HashLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL, *pu1Temp = NULL;

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u4Len =
        (UINT4) ((UINT4)
                 (pSesInfo->pMyDhPub->i4Length +
                  pSesInfo->pHisDhPub->i4Length) +
                 (UINT4) (IKE_TWO * IKE_COOKIE_LEN) +
                 (UINT4) pSesInfo->u2SALength + pSesInfo->u4IdPayloadRecdLen);

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVerifyHash: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeComputeHash:malloc fails\n");
        return IKE_FAILURE;
    }

    pu1Temp = pu1Ptr;

    /* If we are Initiator, generate HASH_R to check 
     * RFC 2409: g^xr | g ^xi | CKY-R | CKY-I | SAi_b | IDir_b 
     * We are Responder, generate HASH_I to check 
     * RFC 2409: g^xi | g ^xr | CKY-I | CKY-R | SAi_b | IDii_b */

    /* Regardless of Initiator or Responder, for verification, pHisDhPub
     * will come first
     */
    IKE_MEMCPY (pu1Temp, pSesInfo->pHisDhPub->pu1Value,
                pSesInfo->pHisDhPub->i4Length);
    pu1Temp += pSesInfo->pHisDhPub->i4Length;

    IKE_MEMCPY (pu1Temp, pSesInfo->pMyDhPub->pu1Value,
                pSesInfo->pMyDhPub->i4Length);
    pu1Temp += pSesInfo->pMyDhPub->i4Length;

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1ResponderCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1InitiatorCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;
    }
    else
    {

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1InitiatorCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;

        IKE_MEMCPY (pu1Temp, pSesInfo->pIkeSA->au1ResponderCookie,
                    IKE_COOKIE_LEN);
        pu1Temp += IKE_COOKIE_LEN;
    }

    /* Even in case of responder, we copy the Selected Payload onto
     * pSAOffered, so this can be common
     */
    IKE_MEMCPY (pu1Temp, pSesInfo->pSAOffered, pSesInfo->u2SALength);
    pu1Temp += pSesInfo->u2SALength;

    /* Copy Recd. ID Payload - Generic Header */
    /* The ID payload is always the Recd. ID Payload, regardless of
     * whether we are Initiator or Responder
     */
    IKE_MEMCPY (pu1Temp, pSesInfo->pu1IdPayloadRecd,
                (INT4) pSesInfo->u4IdPayloadRecdLen);

    (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyId,
         (INT4) pSesInfo->pIkeSA->u2SKeyIdLen, pu1Ptr, (INT4) u4Len, pu1Hash);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
    UNUSED_PARAM (u4HashLen);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessHash                                     */
/*  Description     :This function is used to process the Hash PayLoad  */
/*                  :of the Isakmp Mesg                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :                                                   */
/*  Output(s)       :Process the Hash PayLoad                           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeProcessHash (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{

    tHashPayLoad        Hash;
    UINT1               au1HashData[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if (!((pSesInfo->u1FsmState == IKE_MM_AUTH_EXCH) ||
              (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessHash:Not Expecting this Packet\n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (!((pSesInfo->u1FsmState == IKE_MM_KEY_EXCH) ||
              (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessHash:Not Expecting this Packet\n");
            return (IKE_FAILURE);
        }
    }

    IKE_MEMCPY ((UINT1 *) &Hash, pu1PayLoad, IKE_HASH_PAYLOAD_SIZE);
    Hash.u2PayLoadLen = IKE_NTOHS (Hash.u2PayLoadLen);
    Hash.u2PayLoadLen =
        (UINT2) (Hash.u2PayLoadLen - ((UINT2) IKE_HASH_PAYLOAD_SIZE));

    if (((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_MD5)
         && (Hash.u2PayLoadLen != IKE_MD5_LENGTH))
        || ((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_SHA1)
            && (Hash.u2PayLoadLen != IKE_SHA_LENGTH))
        || ((pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_256)
            && (Hash.u2PayLoadLen != IKE_SHA256_LENGTH))
        || ((pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_384)
            && (Hash.u2PayLoadLen != IKE_SHA384_LENGTH))
        || ((pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_512)
            && (Hash.u2PayLoadLen != IKE_SHA512_LENGTH)))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessHash:Hash Algo data mismatch\n");
        IKE_SET_ERROR (pSesInfo, IKE_INVALID_HASH_INFO);
        return (IKE_FAILURE);
    }

    /* Compute the digest value */
    if (IkeVerifyHash (pSesInfo, au1HashData) == IKE_FAILURE)
    {
        IKE_SET_ERROR (pSesInfo, IKE_INVALID_HASH_INFO);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessHash:Unsupported hash algorithm\n");
        return (IKE_FAILURE);
    }

    /* Compare the received digest value and the computed one */
    if (IKE_MEMCMP ((pu1PayLoad + IKE_HASH_PAYLOAD_SIZE), au1HashData,
                    gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
                    u4KeyLen) != IKE_ZERO)
    {
        IKE_SET_ERROR (pSesInfo, IKE_AUTH_FAILED);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessHash:Hash Check Fails");
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeSendInvalidSpiToIPSec                           */
/*  Description     :This function is used to send an Invalid SPI mesg  */
/*                  :to IPSec, it is called from IkeProcessNotify       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :pNotifyPayLoad : The Notify payload                */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSendInvalidSpiToIPSec (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                          tNotifyPayLoad * pNotifyPayLoad)
{
    UINT1              *pu1Ptr = NULL;
    tIkeDelSpi          DeleteSpi;
    tIkeIPSecIfParam    IkeIPSecIfParam;
    UINT2               u2InvalidSpiSize = IKE_ZERO;

    /* The payload length should be sizeof notify payload(12) + 
     * spi size (4) in case of invalid spi notify type */

    u2InvalidSpiSize =
        (UINT2) (pNotifyPayLoad->u2PayLoadLen - sizeof (tNotifyPayLoad) -
                 pNotifyPayLoad->u1SpiSize);

    if ((pNotifyPayLoad->u1ProtocolId == IKE_IPSEC_ESP) ||
        (pNotifyPayLoad->u1ProtocolId == IKE_IPSEC_AH))
    {
        if (u2InvalidSpiSize != IKE_IPSEC_SPI_SIZE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeSendInvalidSpiToIPSec: Invalid SPI length: %d \n",
                          u2InvalidSpiSize);
            return IKE_FAILURE;
        }
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSendInvalidSpiToIPSec: Received Invalid SPI for "
                     "ISAKMP Protocol\n");
        return IKE_SUCCESS;
    }

    IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
    IKE_BZERO (&DeleteSpi, sizeof (tIkeDelSpi));

    DeleteSpi.u2NumSPI = IKE_ONE;
    DeleteSpi.u1Protocol = pNotifyPayLoad->u1ProtocolId;
    pu1Ptr = pu1PayLoad + sizeof (tNotifyPayLoad) + pNotifyPayLoad->u1SpiSize;
    IKE_MEMCPY (DeleteSpi.au4SPI, pu1Ptr, sizeof (UINT4));
    /* Convert to host order, only one SPI in IkeInvalidSPI */
    DeleteSpi.au4SPI[IKE_INDEX_0] = IKE_NTOHL (DeleteSpi.au4SPI[IKE_INDEX_0]);

    IKE_MEMCPY (&DeleteSpi.RemoteAddr, &pSesInfo->pIkeSA->IpPeerAddr,
                sizeof (tIkeIpAddr));

    IKE_MEMCPY (&DeleteSpi.LocalAddr, &pSesInfo->pIkeSA->IpLocalAddr,
                sizeof (tIkeIpAddr));

    IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DELETE_SA;
    DeleteSpi.u1IkeVersion = IKE_ISAKMP_VERSION;

    IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSpiInfo, &DeleteSpi,
                sizeof (tIkeDelSpi));
    if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSendInvalidSpiToIPSec: Unable to send to Invalid "
                     "spi information to IPSec\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*
 * -----------------------------------------------------------------
 *  Function Name : IkeProcessNotify 
 *  Description   : This function is used to process the 
 *                  Process PayLoad. 
 *  Input(s)      : pSesInfo   - Pointer to session info data base.
 *                  pu1PayLoad - The current payload to be processed.
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS
 * -----------------------------------------------------------------
 */

INT1
IkeProcessNotify (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    UINT1              *pu1Ptr = NULL;
    UINT1               u1EncrMask = IKE_ZERO;
    UINT1               u1AuthMask = IKE_ZERO;
    tNotifyPayLoad      Notify;
    tIkeDelSAByAddr     InitialContact;
    tIkeIPSecIfParam    IkeIPSecIfParam;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (pu1Ptr);

    IKE_MEMCPY (&Notify, pu1PayLoad, sizeof (tNotifyPayLoad));
    Notify.u4Doi = IKE_NTOHL (Notify.u4Doi);
    Notify.u2NotifyMsg = IKE_NTOHS (Notify.u2NotifyMsg);
    Notify.u2PayLoadLen = IKE_NTOHS (Notify.u2PayLoadLen);

    IncIkeNoOfNotifyRecv (&pSesInfo->pIkeSA->IpLocalAddr);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (Notify.u4Doi != IKE_IPSEC_DOI)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessNotify: Invalid DOI value from peer: %s\n",
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    if ((Notify.u1ProtocolId != IKE_ISAKMP) &&
        (Notify.u1ProtocolId != IKE_IPSEC_AH) &&
        (Notify.u1ProtocolId != IKE_IPSEC_ESP) &&
        (Notify.u1ProtocolId != IKE_NONE))
    {
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessNotify: Invalid Protocol value %d: "
                      "from peer: %s\n", Notify.u1ProtocolId, pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    if (Notify.u1SpiSize > (Notify.u2PayLoadLen - sizeof (tNotifyPayLoad)))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessNotify: Invalid SPI Length\n");
        return IKE_FAILURE;
    }

    if (((Notify.u2NotifyMsg > IKE_MAX_NOTIFY_TYPE) &&
         ((Notify.u2NotifyMsg != IKE_INITIAL_CONTACT) &&
          (Notify.u2NotifyMsg != IKE_RESPONDER_LIFETIME) &&
          (Notify.u2NotifyMsg != IKE_REPLAY_STATUS))) ||
        (Notify.u2NotifyMsg < IKE_MIN_NOTIFY_TYPE))
    {
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessNotify: Invalid Notify Type value %d: "
                      "from peer: %s\n", Notify.u2NotifyMsg, pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    /* Log the notify type */
    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                  "IkeProcessNotify: Notify Type received is: %d\n",
                  Notify.u2NotifyMsg);

    /* Point to the payload of Notify */
    /* If SPI is present it would point to the spi else it would point to the *
     * notification information */
    pu1Ptr = pu1PayLoad + sizeof (tNotifyPayLoad);

    if ((Notify.u2NotifyMsg == IKE_INVALID_SPI) ||
        (Notify.u2NotifyMsg == IKE_INITIAL_CONTACT))
    {
        /* For the following notification type processing the message 
         * should come
         * protected */
        u1EncrMask = IKE_TEST_ENCR_MASK (pSesInfo->IkeRxPktInfo.pu1RawPkt);
        u1AuthMask = IKE_TEST_AUTH_MASK (pSesInfo->IkeRxPktInfo.pu1RawPkt);

        if ((u1EncrMask == IKE_ZERO) && (u1AuthMask == IKE_ZERO))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessNotify: Packet is not under Isakmp "
                          "protection from peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }
    }

    switch (Notify.u2NotifyMsg)
    {
        case IKE_INVALID_SPI:
        {
            /* XXX Call IkeSendInvalidSpiToIPSec () here
             */
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessNotify: Invalid SPI recd! from peer: %s\n",
                          pu1PeerAddrStr);
            break;
        }

        case IKE_INITIAL_CONTACT:
        {

            /* In the case of isakmp protocol the spi field should be
             * initiator and responder cookies and the spi length should
             * be 16 */
            if (Notify.u1ProtocolId == IKE_ISAKMP)
            {
                if ((Notify.u1SpiSize != IKE_ZERO) &&
                    ((Notify.u1SpiSize != IKE_TWO * IKE_COOKIE_LEN) &&
                     (Notify.u2PayLoadLen - sizeof (tNotifyPayLoad)
                      != IKE_TWO * IKE_COOKIE_LEN)))
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkeProcessNotify: Invalid SPI length: %s\n",
                                  pu1PeerAddrStr);
                    return IKE_FAILURE;
                }
            }
            else                /* For AH | ESP protocol spi size should be 4 */
            {
                if ((Notify.u1SpiSize != sizeof (UINT4)) &&
                    (Notify.u2PayLoadLen - sizeof (tNotifyPayLoad)
                     != sizeof (UINT4)))
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkeProcessNotify: \
                                 Invalid payload length from peer: %s\n", pu1PeerAddrStr);
                    return IKE_FAILURE;
                }
            }

            IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
            IKE_BZERO (&InitialContact, sizeof (tIkeDelSAByAddr));

            IKE_MEMCPY (&InitialContact.RemoteAddr,
                        &pSesInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

            IKE_MEMCPY (&InitialContact.LocalAddr,
                        &pSesInfo->pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));

            IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;

            IkeIPSecIfParam.IPSecReq.DelSAInfo.u1IkeVersion =
                IKE_ISAKMP_VERSION;

            IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSAInfo,
                        &InitialContact, sizeof (tIkeDelSAByAddr));

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeProcessNotify:INITIAL CONTACT received for peer: %s\n",
                          pu1PeerAddrStr));
            if (IkeProcessInitialContact (pSesInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessNotify: IkeProcessInitialContact - "
                              "Failed for peer: %s\n", pu1PeerAddrStr);
            }

            if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessNotify: Unable to send to Initial "
                              "contact information to IPSec for peer: %s\n",
                              pu1PeerAddrStr);
                return IKE_FAILURE;
            }
            break;
        }

        default:
        {
            return IKE_SUCCESS;
        }
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeProcessDelete                                   */
/*  Description     :This function is used to process the Delete PayLoad */
/*                  :of the Isakmp Mesg                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : The current payload to be processed   */
/*                  :u4MesgId : Identifies the phase to which the       */
/*                  :payload belongs                                    */
/*                  :                                                   */
/*  Output(s)       :Process the Delete PayLoad                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessDelete (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tDeletePayLoad      DelPayload;
    INT4                i4DelLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL;
    tIsakmpHdr         *pIsakmpHdr = NULL;
    UINT4               u4SpiCount = IKE_ZERO;
    UINT1               u1EncrMask = IKE_ZERO, u1AuthMask = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    u1EncrMask = (IKE_TEST_ENCR_MASK (pSesInfo->IkeRxPktInfo.pu1RawPkt));
    u1AuthMask = (IKE_TEST_AUTH_MASK (pSesInfo->IkeRxPktInfo.pu1RawPkt));

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if ((u1EncrMask == IKE_ZERO) && (u1AuthMask == IKE_ZERO))
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessDelete: Packet is not sent under "
                      "Isakmp protection from peer: %s\n", pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    IKE_BZERO (&DelPayload, sizeof (tDeletePayLoad));
    IKE_MEMCPY (&DelPayload, pu1PayLoad, sizeof (tDeletePayLoad));
    DelPayload.u2PayLoadLen = IKE_NTOHS (DelPayload.u2PayLoadLen);
    DelPayload.u4Doi = IKE_NTOHL (DelPayload.u4Doi);
    DelPayload.u2NSpis = IKE_NTOHS (DelPayload.u2NSpis);

    if (DelPayload.u4Doi != IKE_IPSEC_DOI)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessDelete: Invalid DOI value from peer: %s\n",
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    if (DelPayload.u2NSpis > IKE_MAX_NUM_SPI)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessDelete: Exceeded the Max capacity of "
                      "SPI information for peer: %s\n", pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    i4DelLen =
        (INT4) ((UINT2) DelPayload.u2PayLoadLen - sizeof (tDeletePayLoad));

    /* Ignore Delete Message in Phase-2 */
    if (pSesInfo->u1ExchangeType != IKE_ISAKMP_INFO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDelete: Delete information in "
                     "unsupported exchange\n");
        return (IKE_FAILURE);
    }

    pu1Ptr = pu1PayLoad + sizeof (tDeletePayLoad);
    /* In Phase process the delete message */
    switch (DelPayload.u1ProtocolId)
    {
        case IKE_ISAKMP:
        {
            tIkeIPSecIfParam   *pIkeIPSecIfParam = NULL;
            if (MemAllocateMemBlock
                (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeIPSecIfParam) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcess: unable to allocate " "buffer\n");
                return IKE_FAILURE;
            }

            IKE_BZERO (pIkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
            pIkeIPSecIfParam->u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
            pIkeIPSecIfParam->IPSecReq.DelSAInfo.u1IkeVersion =
                IKE_ISAKMP_VERSION;

            IKE_MEMCPY (&(pIkeIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr),
                        &pSesInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));
            IKE_MEMCPY (&(pIkeIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr),
                        &pSesInfo->pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));

            /* As specified in RFC 5996, section 1.4.1, Deleting an IKE SA
             * should implicitly delete, 
             * remaining Child SAs negotiated under it. */
            if (IkeSendIPSecIfParam (pIkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike1ProcessDelete: Unable to send to "
                             "delete SA information to IPSec\n");
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return (IKE_FAILURE);
            }
            pIsakmpHdr =
                (tIsakmpHdr *) (void *) (pSesInfo->IkeRxPktInfo.pu1RawPkt);

            if ((DelPayload.u1SpiSize != IKE_TWO * IKE_COOKIE_LEN)
                || (DelPayload.u2NSpis != IKE_ONE))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessDelete:invalid message from peer: %s\n",
                              pu1PeerAddrStr);
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return (IKE_FAILURE);
            }

            /* Cookies in the isakmp header should not be equal to the cookies
             * in the spi field */
            if ((IKE_MEMCMP (pu1Ptr, pIsakmpHdr->au1InitCookie,
                             IKE_COOKIE_LEN) == IKE_ZERO) &&
                (IKE_MEMCMP (pu1Ptr + IKE_COOKIE_LEN, pIsakmpHdr->au1RespCookie,
                             IKE_COOKIE_LEN) == IKE_ZERO))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessDelete:Attempting to deleting IKE SA"
                             "which is still in use\n");
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return (IKE_FAILURE);
            }

            if (IkeDeleteIkeSAWithCookies
                (pSesInfo, pu1Ptr, pu1Ptr + IKE_COOKIE_LEN) == IKE_FAILURE)
            {

                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessDelete: \
                             IkeDeleteIkeSA - Failed for peer: %s\n", pu1PeerAddrStr);
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return (IKE_FAILURE);
            }
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessDelete:  \
                         Successfully deleted Phase1 SA for peer: %s\n", pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeProcessDelete: Successfully deleted Phase1 SA for peer: %s\n",
                          pu1PeerAddrStr));
            MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                (UINT1 *) pIkeIPSecIfParam);
            break;
        }
        case IKE_IPSEC_AH:
        case IKE_IPSEC_ESP:
        {
            tIkeDelSpi          DeleteSpi;
            tIkeIPSecIfParam   *pIkeIPSecIfParam = NULL;
            if (MemAllocateMemBlock
                (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeIPSecIfParam) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcess: unable to allocate " "buffer\n");
                return IKE_FAILURE;
            }

            IKE_BZERO (pIkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
            IKE_BZERO (&DeleteSpi, sizeof (tIkeDelSpi));

            if (DelPayload.u1ProtocolId == IKE_IPSEC_ESP)
            {
                DeleteSpi.u1Protocol = IKE_IP_PROTO_ESP;
            }
            else
            {
                DeleteSpi.u1Protocol = IKE_IP_PROTO_AH;
            }

            DeleteSpi.u2NumSPI = DelPayload.u2NSpis;
            IKE_MEMCPY (DeleteSpi.au4SPI, pu1Ptr,
                        IKE_IPSEC_SPI_SIZE * DelPayload.u2NSpis);
            /* Convert to host order */
            for (u4SpiCount = IKE_ZERO;
                 u4SpiCount < DelPayload.u2NSpis; u4SpiCount++)
            {
                DeleteSpi.au4SPI[u4SpiCount] =
                    IKE_NTOHL (DeleteSpi.au4SPI[u4SpiCount]);

                /* Get the Corresponding Cryptomap and Delete it in case of 
                 * RA-VPN */
                if (pSesInfo->pIkeSA->bCMDone == TRUE)
                {
                    if (gbCMServer == TRUE)
                    {
                        if (IkeUtilDelDynCryptoMapForServer
                            (pSesInfo->pIkeSA->u4IkeEngineIndex, DeleteSpi.
                             au4SPI[u4SpiCount]) == IKE_FAILURE)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "IkeProcessDelete::"
                                         "IkeDeleteDynamicCryptoMapForServer "
                                         "Failed\n");
                        }
                    }
                }

            }

            IKE_MEMCPY (&DeleteSpi.RemoteAddr, &pSesInfo->pIkeSA->IpPeerAddr,
                        sizeof (tIkeIpAddr));

            IKE_MEMCPY (&DeleteSpi.LocalAddr, &pSesInfo->pIkeSA->IpLocalAddr,
                        sizeof (tIkeIpAddr));

            pIkeIPSecIfParam->u4MsgType = IKE_IPSEC_PROCESS_DELETE_SA;
            /* Intimate IPSec whether to install only the SAs or
             * all (SA, Policy, ACL and Selectors */
            if (pSesInfo->pIkeSA->bTransDone == TRUE)
            {
                DeleteSpi.bDelDynIPSecDB = TRUE;
            }
            else
            {
                DeleteSpi.bDelDynIPSecDB = FALSE;
            }
            /* Set the Ike Version to IKEv1 */
            DeleteSpi.u1IkeVersion = IKE_ISAKMP_VERSION;
            IKE_MEMCPY (&pIkeIPSecIfParam->IPSecReq.DelSpiInfo, &DeleteSpi,
                        sizeof (tIkeDelSpi));

            if (IkeSendIPSecIfParam (pIkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessDelete: Unable to send to "
                             "Invalid spi information to IPSec\n");
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return IKE_FAILURE;
            }
            MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                (UINT1 *) pIkeIPSecIfParam);
        }
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessDelete: \
                         Successfully deleted Phase2 SA for peer: %s\n",
                          pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeProcessDelete: Successfully deleted Phase2 SA for peer: %s\n",
                          pu1PeerAddrStr));
            break;
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessDelete: Invalid Protocol value\n");
            return (IKE_FAILURE);
        }
    }
    UNUSED_PARAM (i4DelLen);
    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessSa 
 *  Description   : Function Process the incomming either Phase 1 
 *                  or Phase 2 SA.
 *  Input(s)      : pSessionInfo - session data base pointer.
 *                  pu1PayLoad - Pay Load to be processed
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS
 * ---------------------------------------------------------------
 */

/* Keeping the payload param temporarily so that Phase1 is not affected */
INT1
IkeProcessSa (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{
    tSaPayLoad          Sa;
    tProposalPayLoad    Prop;
    tSaOffer            Offer;
    UINT1              *pu1Ptr = NULL;
    INT1                i1Return = IKE_FAILURE;
    tIkePayload        *pSaPayload = NULL;
    UINT1              *pu1PayLoad = NULL;
    UINT2               u2SaPayLoadLen = IKE_ZERO, u2PropLen = IKE_ZERO;
    tTransPayLoad      *pTransform = NULL;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_UNUSED (pu1Pd);

    IKE_BZERO (&Offer, sizeof (tSaOffer));
    IKE_BZERO (&Prop, sizeof (tProposalPayLoad));

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if ((pSesInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if (pSesInfo->u1Role == IKE_INITIATOR)
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_SA_SETUP) ||
                  (pSesInfo->u1FsmState == IKE_AG_KEY_EXCH)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessSa: \
                             Not Expecting this Packet from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            if (!((pSesInfo->u1FsmState == IKE_MM_NO_STATE) ||
                  (pSesInfo->u1FsmState == IKE_AG_NO_STATE)))
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessSa: \
                             Not Expecting this Packet from peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }

    /* XXX For now, we assume that we get only one SA payload */
    pSaPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_SA_PAYLOAD]);
    if (pSaPayload == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSa: SA Payload not received: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    pu1PayLoad = pSaPayload->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &Sa, pu1PayLoad, IKE_SA_PAYLOAD_SIZE);
    Sa.u4Doi = IKE_NTOHL (Sa.u4Doi);
    Sa.u4Situation = IKE_NTOHL (Sa.u4Situation);
    u2SaPayLoadLen = (UINT2) (IKE_NTOHS (Sa.u2PayLoadLen) -
                              IKE_SA_PAYLOAD_SIZE);

    /* Check the DOI */
    if (Sa.u4Doi != IKE_IPSEC_DOI)
    {
        IKE_SET_ERROR (pSesInfo, IKE_DOI_NOT_SUPPORTED);
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSa: DOI(%d) not supported, for peer: %s\n",
                      Sa.u4Doi, pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    /* Check the situation */
    if (Sa.u4Situation != SIT_IDENTITY_ONLY)
    {
        IKE_SET_ERROR (pSesInfo, IKE_SITUATION_NOT_SUPPORTED);
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSa: Situation(%d) not supported for peer: %s\n",
                      Sa.u4Situation, pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    pu1Ptr = pu1PayLoad + IKE_SA_PAYLOAD_SIZE;

    if (pSesInfo->u1ExchangeType == IKE_QUICK_MODE)
    {
        if (IkeParsePhase2Proposal (pu1Ptr, pSesInfo, u2SaPayLoadLen) ==
            IKE_SUCCESS)
        {
            i1Return = IkePhase2SelectProposal (pSesInfo);
        }
        else
        {
            IKE_SET_ERROR (pSesInfo, IKE_PROPOSAL_BAD_SYNTAX);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessSa:\
                         Failed to parse Sa payload for peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }
    }
    else if ((pSesInfo->u1ExchangeType == IKE_MAIN_MODE) ||
             (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if (SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_SA_PAYLOAD])
            > IKE_ONE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessSa: \
                         Multiple SA payloads in phase1 from peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }

        IKE_MEMCPY (&Prop, pu1Ptr, IKE_PROP_PAYLOAD_SIZE);
        pu1Ptr += IKE_PROP_PAYLOAD_SIZE + Prop.u1SpiSize;
        u2PropLen = (UINT2) (IKE_NTOHS (Prop.u2PayLoadLen) -
                             (IKE_PROP_PAYLOAD_SIZE + Prop.u1SpiSize));

        if (Prop.u1ProtocolId != IKE_ISAKMP)
        {
            IKE_SET_ERROR (pSesInfo, IKE_INVALID_PROTOCOL_ID);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessSa:\
                         Not a Proposal Pay Load from peer: %s\n", pu1PeerAddrStr);
            return IKE_FAILURE;
        }

        i1Return = IkePhase1CheckProposal (pSesInfo, pu1Ptr, &Offer.pu1Atts,
                                           &Offer.u2Natts, u2PropLen);
    }
    else
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSa: Invalid phase from peer: %s\n",
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }

    if (i1Return == IKE_SUCCESS)
    {
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
        if ((pSesInfo->u1ExchangeType == IKE_MAIN_MODE) ||
            (pSesInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
        {
            Sa.u2PayLoadLen = (UINT2) (sizeof (tProposalPayLoad) +
                                       Offer.u2Natts + sizeof (tSaPayLoad));
            Sa.u2PayLoadLen = IKE_HTONS (Sa.u2PayLoadLen);

            if (pSesInfo->u1Role == IKE_RESPONDER)
            {
                pSesInfo->u2SALength = (UINT2) (u2SaPayLoadLen +
                                                IKE_SA_PAYLOAD_SIZE -
                                                (UINT2)
                                                sizeof (tGenericPayLoad));

                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pSesInfo->pSAOffered) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessSa: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) Offer.pu1Atts);

                    return IKE_FAILURE;
                }
                if (pSesInfo->pSAOffered == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessSa: resource fails for "
                                 "sa offer \n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) Offer.pu1Atts);
                    return IKE_FAILURE;
                }
                IKE_MEMCPY (pSesInfo->pSAOffered, pu1PayLoad +
                            sizeof (tGenericPayLoad), pSesInfo->u2SALength);
                pSesInfo->u2SaSelectedLen = (UINT2)
                    (sizeof (tProposalPayLoad) + Offer.u2Natts);
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pSesInfo->pu1SaSelected) ==
                    MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessSa: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) Offer.pu1Atts);
                    return IKE_FAILURE;
                }
                if (pSesInfo->pu1SaSelected == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessSa: resource fails  \n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) Offer.pu1Atts);
                    return IKE_FAILURE;
                }

                Prop.u2PayLoadLen = (UINT2) (sizeof (tProposalPayLoad) +
                                             Offer.u2Natts);
                Prop.u2PayLoadLen = IKE_HTONS (Prop.u2PayLoadLen);
                Prop.u1NumTran = IKE_ONE;
                Prop.u1SpiSize = IKE_ZERO;
                IKE_MEMCPY (pSesInfo->pu1SaSelected, (UINT1 *) &Prop,
                            sizeof (tProposalPayLoad));

                /* The Accepted Transform with be the only and 
                 * last payload in the 
                 SA, so set the Next payload in the trasform header as 
                 IKE_NONE_PAYLOAD (0), & the Transform Number as 1 */
                pTransform = (tTransPayLoad *) (void *) Offer.pu1Atts;
                if (pTransform == NULL)
                {
                    return IKE_FAILURE;
                }

                pTransform->u1NextPayLoad = IKE_NONE_PAYLOAD;
                pTransform->u1TransNum = IKE_ONE;

                IKE_MEMCPY ((pSesInfo->pu1SaSelected +
                             sizeof (tProposalPayLoad)),
                            Offer.pu1Atts, Offer.u2Natts);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) Offer.pu1Atts);
        }
    }
    else
    {
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
        IKE_SET_ERROR (pSesInfo, IKE_NO_PROPOSAL_CHOSEN);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSa: no proposal chosen for peer: %s\n",
                      pu1PeerAddrStr);
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/* --------------------------------------------------------------*/
/*  Function Name : IkeParsePhase2Proposal                       */
/*  Description   : Function to parse the Proposal and transform */
/*                : payloads present in the SA payload.          */
/*  Input(s)      : pu1Ptr - Pointer to the proposal data.       */
/*                : pSesInfo - Pointer to Session Info structure.*/
/*                : u2SaPayLoadLen - Length of SA payload        */
/*                : minus the header length.                     */
/*  Output(s)     : Constructs list of proposal and transforms   */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                   */
/* --------------------------------------------------------------*/

INT1
IkeParsePhase2Proposal (UINT1 *pu1Ptr, tIkeSessionInfo * pSesInfo,
                        UINT2 u2SaPayLoadLen)
{
    tProposalPayLoad   *pProp = NULL;
    tTransPayLoad      *pTrans = NULL;
    tIkeProposal       *pIkeProposal = NULL, *pTempProposal = NULL;
    tIkeTrans          *pIkeTrans = NULL;
    UINT1              *pu1Temp = pu1Ptr,
        *pu1TempTrans = NULL, u1Count = IKE_ZERO;
    UINT2               u2TranLen = IKE_ZERO;
    UINT2               u2ParseLen = IKE_ZERO;
    UINT2               u2PropLen = IKE_ZERO;
    UINT1               u1LastPropNum = IKE_INVALID_PROPOSAL_NUMBER;
    UINT4               u4Spi = IKE_ZERO;
    UINT1               u1Flag = IKE_FALSE;

    do
    {
        u1Flag = IKE_FALSE;
        pProp = (tProposalPayLoad *) (void *) pu1Temp;
        u2PropLen = IKE_NTOHS (pProp->u2PayLoadLen);
        IKE_MEMCPY (&u4Spi, (pu1Temp + IKE_PROP_PAYLOAD_SIZE),
                    pProp->u1SpiSize);
        if (MemAllocateMemBlock
            (IKE_PROPOSAL_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pIkeProposal) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeParsePhase2Proposal: unable to allocate "
                         "buffer\n");
            return (IKE_FAILURE);
        }
        if (pIkeProposal == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeP2ParseProposal:No Memory allocated\n");
            SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
            return (IKE_FAILURE);
        }
        pIkeProposal->pu1Proposal = pu1Temp;
        pIkeProposal->u1PropNum = pProp->u1PropNum;
        pIkeProposal->u1ProtocolId = pProp->u1ProtocolId;
        pIkeProposal->u4Spi = u4Spi;
        pIkeProposal->u2PayLoadLen = IKE_NTOHS (pProp->u2PayLoadLen);
        pIkeProposal->pNextProposal = NULL;
        SLL_INIT (&(pIkeProposal->IkeTransPayLoadList));
        if (pProp->u1PropNum == u1LastPropNum)
        {
            SLL_SCAN (&pSesInfo->IkeProposalList, pTempProposal, tIkeProposal *)
            {
                if (pTempProposal->u1PropNum == pProp->u1PropNum)
                {
                    if (pTempProposal->u1ProtocolId != pProp->u1ProtocolId)
                    {
                        if (pTempProposal->pNextProposal == NULL)
                        {
                            /*We are supporting only two protocols- 
                             * AH and ESP */
                            pTempProposal->pNextProposal = pIkeProposal;
                            pIkeProposal->pNextProposal = NULL;
                            u1Flag = IKE_TRUE;
                        }
                        else
                        {
                            MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                                (UINT1 *) pIkeProposal);
                            SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                             IkeFreeProposal);
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "IkeP2ParseProposal:Proposal"
                                         "Malformed\n");
                            return (IKE_FAILURE);
                        }
                    }
                    else
                    {
                        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                            (UINT1 *) pIkeProposal);
                        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                         IkeFreeProposal);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeP2ParseProposal:Proposals with same"
                                     "protocol Id having the same proposal"
                                     "number\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                    SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                     IkeFreeProposal);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeP2ParseProposal:Proposals with same"
                                 "protocol Id having the same proposal"
                                 "number\n");
                    return (IKE_FAILURE);
                }
            }
        }
        else
        {
            SLL_ADD (&(pSesInfo->IkeProposalList), (t_SLL_NODE *) pIkeProposal);
            u1Flag = IKE_TRUE;
        }
        pu1TempTrans = pu1Temp + IKE_PROP_PAYLOAD_SIZE + pProp->u1SpiSize;
        for (u1Count = IKE_ZERO; u1Count < pProp->u1NumTran; u1Count++)
        {
            pTrans = (tTransPayLoad *) (void *) pu1TempTrans;
            u2TranLen = IKE_NTOHS (pTrans->u2PayLoadLen);
            if (MemAllocateMemBlock
                (IKE_TRANSFORM_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeTrans) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeParsePhase2Proposal: unable to allocate "
                             "buffer\n");
                if (u1Flag == IKE_FALSE)
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                }
                SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeP2ParseProposal:No Memory allocated\n");
                return (IKE_FAILURE);
            }
            if (pIkeTrans == NULL)
            {
                /* Check wthether the proposal node has been added to 
                 * list or not. If not added free the node here */
                if (u1Flag == IKE_FALSE)
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                }
                SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeP2ParseProposal:No Memory allocated\n");
                return (IKE_FAILURE);
            }
            pIkeTrans->pu1Transform = pu1TempTrans;
            pIkeTrans->u1ProtocolId = pProp->u1ProtocolId;
            pIkeTrans->u1TransId = pTrans->u1TransId;
            pIkeTrans->u2PayLoadLen = IKE_NTOHS (pTrans->u2PayLoadLen);
            SLL_ADD (&(pIkeProposal->IkeTransPayLoadList),
                     (t_SLL_NODE *) pIkeTrans);
            pu1TempTrans += u2TranLen;
        }
        u2ParseLen = (UINT2) (u2ParseLen + u2PropLen);
        u1LastPropNum = pProp->u1PropNum;
        if (u2ParseLen < u2SaPayLoadLen)
        {
            pu1Temp += u2PropLen;
        }
    }
    while (u2ParseLen < u2SaPayLoadLen);
    if (u1Flag == IKE_FALSE)
    {
        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal);
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), IkeFreeProposal);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeP2ParseProposal:No Memory allocated\n");
        return (IKE_FAILURE);

    }
    return IKE_SUCCESS;
}

/* ---------------------------------------------------------------*/
/*  Function Name : IkePhase2SelectProposal                       */
/*  Description   : Function to select each proposal and          */
/*                : corresponding transform from the list         */
/*                : of proposals and transforms and checking      */
/*                : for the set of proposals and transforms       */
/*                : matching the configured transformset          */
/*                : configuration.                                */
/*  Input(s)      :                                               */
/*                  pSesInfo - Pointer to session info structure  */
/*  Output(s)     : None.                                         */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                    */
/* ---------------------------------------------------------------*/

INT1
IkePhase2SelectProposal (tIkeSessionInfo * pSesInfo)
{
    tIkeProposal       *pIkeProposal = NULL, *pIkeNextProposal = NULL;
    tIkeTrans          *pIkeTrans = NULL, *pIkeNextTrans = NULL;
    SLL_SCAN (&pSesInfo->IkeProposalList, pIkeProposal, tIkeProposal *)
    {
        SLL_SCAN (&pIkeProposal->IkeTransPayLoadList, pIkeTrans, tIkeTrans *)
        {
            if (pIkeProposal->pNextProposal != NULL)
            {
                pIkeNextProposal = pIkeProposal->pNextProposal;
                SLL_SCAN (&(pIkeNextProposal->IkeTransPayLoadList),
                          pIkeNextTrans, tIkeTrans *)
                {
                    if (IkePhase2CheckProposal
                        (pSesInfo, pIkeTrans, pIkeNextTrans, pIkeProposal,
                         pIkeNextProposal) == IKE_SUCCESS)
                    {
                        return IKE_SUCCESS;
                    }

                }
            }
            else
            {
                if (IkePhase2CheckProposal
                    (pSesInfo, pIkeTrans, NULL, pIkeProposal,
                     NULL) == IKE_SUCCESS)
                {
                    return IKE_SUCCESS;
                }
            }
        }
    }
    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkePhase2SelectProposal:Failed\n");
    return IKE_FAILURE;
}

/* ------------------------------------------------------------------------*/
/*  Function Name : IkePhase2CheckProposal                                 */
/*  Description   : Function to check the Phase2 configuration for         */
/*                  for a pair of proposals and transforms .               */
/*                                                                         */
/*  Input(s)      :                                                        */
/*                : pSesInfo - phase 2 info pointer.                       */
/*                : pIkeProposal1-Pointer to structure containing          */
/*                : pointer to proposal1                                   */
/*                : pIkeTrans1- pointer to structure containing pointer    */
/*                : to transform1 present in proposal1 to be checked.      */
/*                : pIkeProposal2-Pointer to structure containing pointer  */
/*                : to proposal2.                                          */
/*                : pIkeTrans1- pointer to structure containing pointer    */
/*                : to transform1 present in proposal2 to be checked.      */
/*                :u2SaPayLoadLen - Length of SA payload subtracting      */
/*                :          the header length.                            */
/*  Output(s)     : None.                                                  */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                             */
/* ------------------------------------------------------------------------*/

INT1
IkePhase2CheckProposal (tIkeSessionInfo * pSesInfo, tIkeTrans * pIkeTrans1,
                        tIkeTrans * pIkeTrans2, tIkeProposal * pIkeProposal1,
                        tIkeProposal * pIkeProposal2)
{
    tTransPayLoad      *pTrans1 = NULL, *pTrans2 = NULL;
    UINT1               u1TransNum = IKE_ZERO;
    INT4                i4TransFlag = IKE_FAILURE;
    BOOLEAN             bMultiProtocolFlag = IKE_FALSE;
    BOOLEAN             bLastPropSelected = IKE_FALSE;

    if (pIkeTrans1 != NULL)
    {
        pTrans1 = (tTransPayLoad *) (void *) pIkeTrans1->pu1Transform;
    }

    if ((pIkeTrans1 == NULL) || (pTrans1 == NULL))
    {
        return IKE_FAILURE;
    }

    if (pIkeTrans2 != NULL)
    {
        pTrans2 = (tTransPayLoad *) (void *) pIkeTrans2->pu1Transform;
    }
    for (u1TransNum = IKE_ZERO; u1TransNum < MAX_TRANSFORMS; u1TransNum++)
    {
        i4TransFlag = IKE_FAILURE;
        bMultiProtocolFlag = IKE_FALSE;
        if ((pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum] != NULL)
            && (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                u1Status == IKE_ACTIVE))
        {
            if ((pTrans1 != NULL) && (pTrans2 != NULL))
            {
                bMultiProtocolFlag = IKE_TRUE;
            }

            switch (pIkeTrans1->u1ProtocolId)
            {
                case IKE_IPSEC_AH:
                    if (pSesInfo->IkePhase2Policy.
                        aTransformSetBundle[u1TransNum]->
                        u1AhHashAlgo == pTrans1->u1TransId)
                    {
                        if (bMultiProtocolFlag == IKE_TRUE)
                        {
                            if (pSesInfo->IkePhase2Policy.
                                aTransformSetBundle[u1TransNum]->
                                u1EspEncryptionAlgo == pTrans2->u1TransId)
                            {
                                pSesInfo->IkePhase2Info.u2NegKeyLen = IKE_NONE;
                                IkeSetIPsecBundle (pSesInfo, pIkeTrans1,
                                                   pIkeTrans2,
                                                   pIkeProposal1->u4Spi,
                                                   pIkeProposal2->u4Spi,
                                                   IKE_TRUE);
                                i4TransFlag = IKE_SUCCESS;
                            }
                        }
                        else
                        {
                            if (pSesInfo->IkePhase2Policy.
                                aTransformSetBundle[u1TransNum]->
                                u1EspEncryptionAlgo == IKE_ZERO)
                            {
                                IkeSetIPsecBundle (pSesInfo, pIkeTrans1,
                                                   pIkeTrans2,
                                                   pIkeProposal1->u4Spi,
                                                   IKE_ZERO, IKE_FALSE);
                                i4TransFlag = IKE_SUCCESS;
                            }
                        }
                    }
                    break;

                case IKE_IPSEC_ESP:
                    if (pSesInfo->IkePhase2Policy.
                        aTransformSetBundle[u1TransNum]->
                        u1EspEncryptionAlgo == pTrans1->u1TransId)
                    {
                        pSesInfo->IkePhase2Info.u2NegKeyLen = IKE_NONE;
                        if (bMultiProtocolFlag == IKE_TRUE)
                        {
                            if (pSesInfo->IkePhase2Policy.
                                aTransformSetBundle[u1TransNum]->
                                u1AhHashAlgo == pTrans2->u1TransId)
                            {
                                IkeSetIPsecBundle (pSesInfo, pIkeTrans1,
                                                   pIkeTrans2,
                                                   pIkeProposal1->u4Spi,
                                                   pIkeProposal2->u4Spi,
                                                   IKE_TRUE);
                                i4TransFlag = IKE_SUCCESS;
                            }
                        }
                        else
                        {
                            if (pSesInfo->IkePhase2Policy.
                                aTransformSetBundle[u1TransNum]->
                                u1AhHashAlgo == IKE_ZERO)
                            {
                                IkeSetIPsecBundle (pSesInfo, pIkeTrans1,
                                                   pIkeTrans2,
                                                   pIkeProposal1->u4Spi,
                                                   IKE_ZERO, IKE_FALSE);
                                i4TransFlag = IKE_SUCCESS;
                            }
                        }
                    }
                    break;

                default:
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase2CheckProposal:Protocol not "
                                 "supported\n");
                    return (IKE_FAILURE);
            }
        }
        if (i4TransFlag == IKE_SUCCESS)
        {
            /*This will check the attributes of the first proposal and 
               stores the attributes in the au4LastPropAttrib in session info
               structure */

            if (IkePhase2CheckTransAtts (pSesInfo->u1Role, pSesInfo,
                                         pIkeTrans1->pu1Transform +
                                         sizeof (tTransPayLoad), (UINT2)
                                         (pIkeTrans1->u2PayLoadLen -
                                          sizeof (tTransPayLoad)),
                                         pIkeTrans1->u1ProtocolId,
                                         u1TransNum) == IKE_SUCCESS)
            {
                if (bMultiProtocolFlag == IKE_TRUE)
                {
                    pSesInfo->bLastPropNumSame = IKE_TRUE;

                    /*This will check the attributes and compare the attributes
                       with the attributes of last proposal stored in 
                       au4LastPropAttrib */

                    if (IkePhase2CheckTransAtts (pSesInfo->u1Role, pSesInfo,
                                                 pIkeTrans2->pu1Transform +
                                                 sizeof (tTransPayLoad), (UINT2)
                                                 (pIkeTrans2->u2PayLoadLen -
                                                  sizeof (tTransPayLoad)),
                                                 pIkeTrans2->u1ProtocolId,
                                                 u1TransNum) != IKE_SUCCESS)
                    {
                        pSesInfo->bLastPropNumSame = IKE_FALSE;
                    }
                    else
                    {
                        bLastPropSelected = IKE_TRUE;
                    }
                }
                else
                {
                    bLastPropSelected = IKE_TRUE;
                }
            }

            if (bLastPropSelected == IKE_TRUE)
            {
                if (pSesInfo->u1Role == IKE_RESPONDER)
                {
                    if (IkeSetPhase2ResponseProposal (pSesInfo, pIkeTrans1,
                                                      pIkeTrans2,
                                                      pIkeProposal1,
                                                      pIkeProposal2,
                                                      bMultiProtocolFlag) ==
                        IKE_SUCCESS)
                    {
                        return IKE_SUCCESS;
                    }
                }
                else
                {
                    return IKE_SUCCESS;
                }
            }
        }
    }
    return IKE_FAILURE;
}

/* -----------------------------------------------------------------------  */
/*  Function Name : IkeSetPhase2ResponseProposal                            */
/*  Description   : Function to set the response proposal in case           */
/*                  of role as a responder.                                 */
/*                                                                          */
/*  Input(s)      :                                                         */
/*                  pSesInfo - phase 2 info pointer.                        */
/*                  pIkeProposal1-Pointer to structure containing           */
/*                  pointer to proposal1                                    */
/*                  pIkeTrans1- pointer to structure containing pointer     */
/*                  to transform1 present in proposal1 to be checked.       */
/*                  pIkeProposal2-Pointer to structure containing pointer   */
/*                  to proposal2.                                           */
/*                  pIkeTrans1- pointer to structure containing pointer     */
/*                  to transform1 present in proposal2 to be checked.       */
/*                  bMultiPropFlag - Flag indicating whether proposal       */
/*                  or proposals are selected.                              */
/*                                                                          */
/*  Output(s)     : None.                                                   */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                              */
/* -------------------------------------------------------------------------*/

INT1
IkeSetPhase2ResponseProposal (tIkeSessionInfo * pSesInfo,
                              tIkeTrans * pIkeTrans1, tIkeTrans * pIkeTrans2,
                              tIkeProposal * pIkeProposal1,
                              tIkeProposal * pIkeProposal2,
                              BOOLEAN bMultiPropFlag)
{
    tProposalPayLoad    Prop1, Prop2;
    tTransPayLoad       Trans1, Trans2;
    UINT1              *pu1TempPtr = NULL;
    UINT2               u2PropLen = IKE_ZERO;
    UINT4               u4InboundSpi1 = IKE_ZERO, u4InboundSpi2 = IKE_ZERO;

    IKE_MEMCPY ((UINT1 *) &Prop1, pIkeProposal1->pu1Proposal,
                sizeof (tProposalPayLoad));
    IKE_MEMCPY ((UINT1 *) &Trans1, pIkeTrans1->pu1Transform,
                sizeof (tTransPayLoad));
    Prop1.u1NumTran = IKE_ONE;
    Trans1.u1NextPayLoad = IKE_NONE_PAYLOAD;

    u4InboundSpi1 = IkeGetInSpi (Prop1.u1ProtocolId, pSesInfo);
    if (u4InboundSpi1 == IKE_FALSE)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeP2ChPr:Protocol not supported: " "No Spi found\n");
        return (IKE_FAILURE);
    }

    u2PropLen = (UINT2) (pIkeTrans1->u2PayLoadLen + IKE_PROP_PAYLOAD_SIZE +
                         Prop1.u1SpiSize);
    Prop1.u2PayLoadLen = IKE_HTONS (u2PropLen);
    Prop1.u1NextPayLoad = IKE_NONE_PAYLOAD;

    if (bMultiPropFlag == IKE_TRUE)
    {
        IKE_MEMCPY ((UINT1 *) &Prop2, pIkeProposal2->pu1Proposal,
                    sizeof (tProposalPayLoad));
        IKE_MEMCPY ((UINT1 *) &Trans2, pIkeTrans2->pu1Transform,
                    sizeof (tTransPayLoad));
        u2PropLen = (UINT2) (pIkeTrans1->u2PayLoadLen +
                             pIkeTrans2->u2PayLoadLen +
                             IKE_TWO * IKE_PROP_PAYLOAD_SIZE + Prop1.u1SpiSize +
                             Prop2.u1SpiSize);
        Prop2.u1NumTran = IKE_ONE;
        Prop2.u2PayLoadLen = IKE_HTONS ((pIkeTrans2->u2PayLoadLen +
                                         IKE_PROP_PAYLOAD_SIZE +
                                         Prop2.u1SpiSize));
        u4InboundSpi2 = IkeGetInSpi (Prop2.u1ProtocolId, pSesInfo);
        if (u4InboundSpi2 == IKE_FALSE)
        {

            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeP2ChPr:Protocol Not supported: " "No Spi found\n");
            return (IKE_FAILURE);
        }
        Prop1.u1NextPayLoad = IKE_PROPOSAL_PAYLOAD;
        Prop2.u1NextPayLoad = IKE_NONE_PAYLOAD;
        Trans2.u1NextPayLoad = IKE_NONE_PAYLOAD;
    }
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1SaSelected) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeP2ChPr: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pSesInfo->pu1SaSelected == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeP2ChPr:unable to allocate " "resource\n");
        return (IKE_FAILURE);
    }
    pSesInfo->u2SaSelectedLen = u2PropLen;
    pu1TempPtr = pSesInfo->pu1SaSelected;
    pu1TempPtr = IkeCopyProposal (&Prop1, &Trans1, u4InboundSpi1, pIkeTrans1,
                                  pu1TempPtr);
    if (bMultiPropFlag == IKE_TRUE)
    {
        pu1TempPtr += (pIkeTrans1->u2PayLoadLen - sizeof (tTransPayLoad));
        pu1TempPtr =
            IkeCopyProposal (&Prop2, &Trans2, u4InboundSpi2, pIkeTrans2,
                             pu1TempPtr);
    }
    UNUSED_PARAM (pu1TempPtr);
    return IKE_SUCCESS;

}

/* ---------------------------------------------------------------*/
/*  Function Name : IkeGetInSpi                                   */
/*  Description   : Function to retrieve inbound spi from Session */
/*                  Info structure.                               */
/*                                                                */
/*  Input(s)      : u1ProtocolId - Protocol Id of the proposal.   */
/*                  pSesInfo - phase 2 info pointer.              */
/*  Output(s)     : None.                                         */
/*  Return Values : u4Spi or IKE_FALSE                            */
/* ---------------------------------------------------------------*/

UINT4
IkeGetInSpi (UINT1 u1ProtocolId, tIkeSessionInfo * pSesInfo)
{
    UINT4               u4Spi = IKE_ZERO;

    switch (u1ProtocolId)
    {
        case IKE_IPSEC_AH:

            u4Spi = pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4Spi;
            break;

        case IKE_IPSEC_ESP:

            u4Spi = pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeP2ChPr:Invalid Protocol - No Spi " "present\n");
            return (IKE_FALSE);

    }
    return u4Spi;
}

/* -----------------------------------------------------------------------  */
/*  Function Name : IkeCopyProposal                                         */
/*  Description   : Function to copy the response proposal to session info  */
/*                  structure.                                              */
/*                :                                                         */
/*  Input(s)      :                                                         */
/*                : pProp    - Proposal payload header to be copied         */
/*                : pTrans   - Transform payload header to be copied        */
/*                : u4Spi    - SPI to be copied to the response proposal    */
/*                : payload                                                 */
/*                : pIkeTrans - pointer to structure containing pointer     */
/*                : to transform  present in proposal  to be copied .       */
/*                : pu1TempPtr- Pointer to destination location             */
/*                :                                                         */
/*  Output(s)     : None.                                                   */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                              */
/* -------------------------------------------------------------------------*/

UINT1              *
IkeCopyProposal (tProposalPayLoad * pProp, tTransPayLoad * pTrans, UINT4
                 u4Spi, tIkeTrans * pIkeTrans, UINT1 *pu1TempPtr)
{

    IKE_MEMCPY (pu1TempPtr, (UINT1 *) pProp, sizeof (tProposalPayLoad));
    pu1TempPtr += sizeof (tProposalPayLoad);
    u4Spi = IKE_HTONL (u4Spi);
    IKE_MEMCPY (pu1TempPtr, (UINT1 *) &u4Spi, pProp->u1SpiSize);
    pu1TempPtr += pProp->u1SpiSize;
    IKE_MEMCPY (pu1TempPtr, (UINT1 *) pTrans, sizeof (tTransPayLoad));
    pu1TempPtr += sizeof (tTransPayLoad);
    IKE_MEMCPY (pu1TempPtr,
                (pIkeTrans->pu1Transform + sizeof (tTransPayLoad)),
                (pIkeTrans->u2PayLoadLen - sizeof (tTransPayLoad)));
    return pu1TempPtr;
}

/* ---------------------------------------------------------------------*/
/*  Function Name : IkeSetIPSecBundle                                   */
/*  Description   : Function is used to set IPSEC interface data        */
/*                : structures with protocolId,spi and other value      */
/*                :                                                     */
/*  Input(s)      : pSesInfo - Pointer to Session Info structure        */
/*                : pIkeTrans1- pointer to structure containing pointer */
/*                : to transform present in selected proposal1          */
/*                : pIkeTrans2- pointer to structure containing pointer */
/*                : to transform  present in selected proposal2         */
/*                : u4Spi1    - SPI for protocolId of the first proposal*/
/*                : u4Spi2    - SPI for protocolId of the sec. proposal */
/*                : bMultiProtocolFlag - Flag to indicate multiple proto*/
/*                : cols.                                               */
/*  Output(s)     : None.                                               */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                          */
/* ---------------------------------------------------------------------*/

VOID
IkeSetIPsecBundle (tIkeSessionInfo * pSesInfo, tIkeTrans * pIkeTrans1,
                   tIkeTrans * pIkeTrans2, UINT4 u4Spi1,
                   UINT4 u4Spi2, BOOLEAN bMultiProtocolFlag)
{
    UINT1               u1ProtocolId = IKE_ZERO;

    u1ProtocolId = pIkeTrans1->u1ProtocolId;
    switch (u1ProtocolId)
    {
        case IKE_IPSEC_ESP:
            pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_ESP;
            pSesInfo->IkePhase2Info.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_ESP;
            /* Store the OutBound SPI */
            pSesInfo->IkePhase2Info.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi1);
            pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u1EncrAlgo =
                pIkeTrans1->u1TransId;
            pSesInfo->IkePhase2Info.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u1EncrAlgo =
                pIkeTrans1->u1TransId;
            if (bMultiProtocolFlag == IKE_TRUE)
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_AH;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol =
                    IKE_IPSEC_AH;
                /* Store the OutBound SPI */
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi2);

            }
            else
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_ZERO;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_ZERO;
            }
            break;

        case IKE_IPSEC_AH:
            pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_AH;
            pSesInfo->IkePhase2Info.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_AH;
            /* Store the OutBound SPI */
            pSesInfo->IkePhase2Info.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi1);
            if (bMultiProtocolFlag == IKE_TRUE)
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol =
                    IKE_IPSEC_ESP;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol =
                    IKE_IPSEC_ESP;
                /* Store the OutBound SPI */
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi2);
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u1EncrAlgo =
                    pIkeTrans2->u1TransId;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u1EncrAlgo =
                    pIkeTrans2->u1TransId;

            }
            else
            {

                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_ZERO;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_ZERO;
            }
            break;

        default:

            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetIPSecBundle:Invalid Protocol"
                         " - No Spi present\n ");
            break;

    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkePhase1CheckProposal 
 *  Description   : Function to check the phase 1 Proposal.
 *  Input(s)      : pu1Ptr - pointer where the data is available.
 *                  pSessionInfo - Session info pointer.
 *                  ppu1Atts - Accepted Attributes.
 *                  pu2Natts - Lenth of the accepted attributes.
 *  Output(s)     : None.
 *  Return Values : IKE_FAILURE or IKE_SUCCESS
 * ---------------------------------------------------------------
 */

INT1
IkePhase1CheckProposal (tIkeSessionInfo * pSessionInfo, UINT1 *pu1Ptr,
                        UINT1 **ppu1Atts, UINT2 *pu2Natts, UINT2 u2PropLen)
{
    tTransPayLoad       Trans;
    INT1                i1Return = IKE_FAILURE;
    UINT1              *pu1TempPtr = NULL;
    UINT2               u2ParseLen = IKE_ZERO;

    do
    {
        IKE_MEMCPY ((UINT1 *) &Trans, pu1Ptr, IKE_TRAN_PAYLOAD_SIZE);
        pu1TempPtr = pu1Ptr;
        pu1Ptr += IKE_TRAN_PAYLOAD_SIZE;
        Trans.u2PayLoadLen = IKE_NTOHS (Trans.u2PayLoadLen);
        Trans.u2PayLoadLen =
            (UINT2) (Trans.u2PayLoadLen - IKE_TRAN_PAYLOAD_SIZE);

        if (Trans.u1TransId == IKE_TRANS_ID_KEY_OAKLEY)
        {
            i1Return =
                IkePhase1CheckTransAtts (pSessionInfo, pu1Ptr,
                                         Trans.u2PayLoadLen);
            if (i1Return == IKE_SUCCESS)
            {
                *pu2Natts = (UINT2) (Trans.u2PayLoadLen +
                                     IKE_TRAN_PAYLOAD_SIZE);
                if (*pu2Natts != IKE_NONE)
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &(*ppu1Atts)) == MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkePhase1CheckProposal: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (*ppu1Atts == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeP1CkP:unable to allocate resource\n");
                        return IKE_FAILURE;
                    }
                    IKE_MEMCPY (*ppu1Atts, pu1TempPtr, *pu2Natts);
                }
                return (IKE_SUCCESS);
            }
        }
        pu1Ptr += Trans.u2PayLoadLen;
        u2ParseLen =
            (UINT2) (u2ParseLen + Trans.u2PayLoadLen + IKE_TRAN_PAYLOAD_SIZE);
    }
    while (u2ParseLen < u2PropLen);
    return (i1Return);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkePhase1CheckTransAtts 
 *  Description   : Function to check the phase 1 Transform Atts.
 *  Input(s)      : pu1Ptr - pointer where the data is available.
 *                  pSessionInfo - Session info pointer.
 *                  u2PayLoadLen - Lenth of the transform payload.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkePhase1CheckTransAtts (tIkeSessionInfo * pSessionInfo, UINT1 *pu1Ptr,
                         UINT2 u2PayLoadLen)
{
    INT4                i4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT4               au4TransAttributes[IKE_MAX_TRANS_ATTRIBUTES]
        = { IKE_ZERO };
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4SoftLifetimeFactor = IKE_ZERO;

    pSessionInfo->IkePhase1Info.u2NegKeyLen = IKE_NONE;
    while (i4Count < u2PayLoadLen)
    {
        i4Len = IkeGetAtts (pu1Ptr, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase1CheckTransAtts: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if ((u4AttrType != 0) && (u2Attr < IKE_MAX_TRANS_ATTRIBUTES))
        {
            au4TransAttributes[u2Attr] = u2BasicValue;
        }

        if (IkePhase1CheckAtts (pSessionInfo, u4AttrType,
                                u2Attr, u2BasicValue,
                                au1VarAtts, (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase1CheckTransAtts: IkePhase1CheckTransAtts "
                         "failed!\n");
            return IKE_FAILURE;
        }
        i4Count =
            (INT4) ((UINT2) i4Count + (sizeof (tSaBasicAttribute)) +
                    (UINT2) i4Len);
        pu1Ptr = pu1Ptr + (sizeof (tSaBasicAttribute)) + i4Len;
    }

    if ((au4TransAttributes[IKE_ENCR_ALGO_ATTRIB] == IKE_NONE) ||
        (au4TransAttributes[IKE_HASH_ALGO_ATTRIB] == IKE_NONE) ||
        (au4TransAttributes[IKE_GROUP_DESC_ATTRIB] == IKE_NONE) ||
        (au4TransAttributes[IKE_AUTH_METHOD_ATTRIB] == IKE_NONE))

    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase1CheckTransAtts: Mandatory attributes "
                     "missing\n");
        return IKE_FAILURE;
    }

    if (pSessionInfo->IkePhase1Policy.u1EncryptionAlgo == IKE_AES)
    {
        if (pSessionInfo->IkePhase1Info.u2NegKeyLen == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase1CheckTransAtts: Key length attribute "
                         "for AES missing\n");
            return IKE_FAILURE;
        }
    }
    /* If we have configured KB and recd secs or vice-versa,
     * copy our configured values into IkeSA
     */
    /* XXX Send a notification since we are using our local
       values, in this case, as a responder, we should not send back
       the attributes that he sent as lifetime as we are not accepting them
     */
    if (pSessionInfo->pIkeSA->u4LifeTime == IKE_NONE)
    {
        if (pSessionInfo->IkePhase1Policy.u4LifeTime != IKE_NONE)
        {
            pSessionInfo->pIkeSA->u4LifeTime =
                IkeGetPhase1LifeTime (&pSessionInfo->IkePhase1Policy);
        }
    }

    if (pSessionInfo->pIkeSA->u4LifeTimeKB == IKE_NONE)
    {
        if (pSessionInfo->IkePhase1Policy.u4LifeTimeKB != IKE_NONE)
        {
            pSessionInfo->pIkeSA->u4LifeTimeKB =
                pSessionInfo->IkePhase1Policy.u4LifeTimeKB;

            /* Set the soft lifetime in bytes */
            /* SoftLifeTime for KB is being calculated here itself
             * as the SA will be used to Encrypt/Decrypt some messages
             * of Phase1 exchange, even before Storing IkeSA 
             */
            u4SoftLifetimeFactor =
                (UINT4) (OSIX_RAND
                         (IKE_MIN_SOFTLIFETIME_FACTOR,
                          IKE_MAX_SOFTLIFETIME_FACTOR));
            pSessionInfo->pIkeSA->u4SoftLifeTimeBytes =
                (pSessionInfo->pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB *
                 u4SoftLifetimeFactor) / IKE_DIVISOR_100;
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkePhase2CheckTransAtts 
 *  Description   : Function to check the phase 2 Transform Atts.
 *  Input(s)      : pu1Ptr - pointer where the data is available.
 *                  pSessionInfo - pointer to session data structure
 *                  u2PayLoadLen - Lenth of the transform payload.
 *                  u1Role - Initator/Responder of the session.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkePhase2CheckTransAtts (UINT1 u1Role, tIkeSessionInfo * pSesInfo,
                         UINT1 *pu1Ptr, UINT2 u2PayLoadLen, UINT1 u1ProtocolId,
                         UINT1 u1TransNum)
{
    INT4                i4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    UINT4               au4TransAttributes[IKE_MAX_TRANS_ATTRIBUTES]
        = { IKE_ZERO };
    INT4                i4Len = IKE_ZERO;

    while (i4Count < u2PayLoadLen)
    {
        i4Len = IkeGetAtts (pu1Ptr, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase2CheckTransAtts: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if ((u4AttrType != 0) && (u2Attr < IKE_MAX_TRANS_ATTRIBUTES))
        {
            au4TransAttributes[u2Attr] = u2BasicValue;
        }

        if (IkePhase2CheckAtts (u1Role, pSesInfo, u4AttrType,
                                u2Attr, u2BasicValue,
                                au1VarAtts, (UINT2) i4Len,
                                u1ProtocolId, u1TransNum) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase2CheckTransAtts: IkePhase2CheckAtts "
                         "failed!\n");
            return IKE_FAILURE;
        }
        i4Count = (INT4) (i4Count + (INT4) sizeof (tSaBasicAttribute) + i4Len);
        pu1Ptr = (pu1Ptr + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
    }

    if (au4TransAttributes[IKE_IPSEC_ENCAPSULATION_MODE_ATTRIB] == IKE_NONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase2CheckTransAtts: Mandatory attributes "
                     "missing\n");
        return IKE_FAILURE;
    }

    if ((pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
         u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES) ||
        (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
         u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES_CTR))
    {
        if (pSesInfo->IkePhase2Info.u2NegKeyLen == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase2CheckTransAtts: Key length attribute "
                         "for AES missing\n");
            return IKE_FAILURE;
        }
    }

    /* If lifetime is configured and we don't receive any lifetime... */
    /* XXX Send Notify */
    if (pSesInfo->IkePhase2Policy.u4LifeTime != IKE_NONE)
    {
        if (u1ProtocolId == IKE_IPSEC_ESP)
        {
            if (pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4LifeTime == IKE_NONE)
            {

                IKE_CONVERT_LIFETIME_TO_SECS (pSesInfo->IkePhase2Policy.
                                              u1LifeTimeType,
                                              pSesInfo->IkePhase2Policy.
                                              u4LifeTime);

                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                    pSesInfo->IkePhase2Policy.u4LifeTime;

                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                    pSesInfo->IkePhase2Policy.u4LifeTime;
            }
        }
        else if (u1ProtocolId == IKE_IPSEC_AH)
        {
            if (pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4LifeTime == IKE_NONE)
            {

                IKE_CONVERT_LIFETIME_TO_SECS (pSesInfo->IkePhase2Policy.
                                              u1LifeTimeType,
                                              pSesInfo->IkePhase2Policy.
                                              u4LifeTime);

                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                    pSesInfo->IkePhase2Policy.u4LifeTime;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                    pSesInfo->IkePhase2Policy.u4LifeTime;
            }
        }
    }

    /* Same for LifeTime in KB */
    if (pSesInfo->IkePhase2Policy.u4LifeTimeKB != IKE_NONE)
    {
        if (u1ProtocolId == IKE_IPSEC_ESP)
        {
            if (pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB == IKE_NONE)
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->IkePhase2Policy.u4LifeTimeKB;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->IkePhase2Policy.u4LifeTimeKB;
            }
        }
        else if (u1ProtocolId == IKE_IPSEC_AH)
        {
            if (pSesInfo->IkePhase2Info.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB == IKE_NONE)
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->IkePhase2Policy.u4LifeTimeKB;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->IkePhase2Policy.u4LifeTimeKB;
            }
        }
    }

    if (pSesInfo->IkePhase2Info.u1NegPfs != pSesInfo->IkePhase2Policy.u1Pfs)
    {
        /* PFS configured but not received */
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase2CheckTransAtts: PFS configured but not "
                     "received\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGetAtts
 *  Description   : Function to read class and value from the atts.
 *  Input(s)      : pu1Ptr - pointer where the data is available.
 *  Output(s)     : pu2Attr - pointer to store class info.
 *                  pu2Value - pointer to sotre attribute value.
 *  Return Values : For Var attribute, returns length of the attribute
 *                  For Basic Attribute, returns 0
 *                  FAILURE, returns IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT4
IkeGetAtts (UINT1 *pu1Ptr, UINT4 *pu4AttrType, UINT2 *pu2Attr,
            UINT2 *pu2Value, UINT1 *pu1VarPtr)
{
    tSaBasicAttribute   SaBasicAttrib;
    tSaVpiAttribute     SaVpiAttrib;
    UINT2               u2Attr = IKE_ZERO;

    IKE_MEMCPY (&u2Attr, pu1Ptr, sizeof (UINT2));
    u2Attr = IKE_NTOHS (u2Attr);
    /* Get the AF bit */
    *pu4AttrType = u2Attr & IKE_BASIC_ATTR;
    /* Get the Attribute value */
    *pu2Attr = u2Attr & IKE_VAR_ATTR;
    if (*pu4AttrType != IKE_ZERO)
    {
        /* Basic Type */
        IKE_MEMCPY (&SaBasicAttrib, pu1Ptr, sizeof (tSaBasicAttribute));
        SaBasicAttrib.u2Attr = IKE_NTOHS (SaBasicAttrib.u2Attr);
        SaBasicAttrib.u2BasicValue = IKE_NTOHS (SaBasicAttrib.u2BasicValue);
        *pu2Value = SaBasicAttrib.u2BasicValue;
        return (IKE_ZERO);
    }
    /* Variable attr */
    IKE_MEMCPY (&SaVpiAttrib, pu1Ptr, sizeof (tSaVpiAttribute));
    SaVpiAttrib.u2Attr = IKE_NTOHS (SaVpiAttrib.u2Attr);
    SaVpiAttrib.u2VpiLength = IKE_NTOHS (SaVpiAttrib.u2VpiLength);
    if (SaVpiAttrib.u2VpiLength > IKE_MAX_ATTR_SIZE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGetAtts: Not enought memory to copy attr value\n");
        return IKE_FAILURE;
    }
    IKE_MEMCPY (pu1VarPtr, pu1Ptr + sizeof (tSaVpiAttribute),
                SaVpiAttrib.u2VpiLength);
    return (SaVpiAttrib.u2VpiLength);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkePhase1CheckAtts
 *  Description   : Function to check phase 1 attributes. 
 *  Input(s)      : pSessionInfo -Session data base pointer. 
 *                : u2class - class info 
 *                  u2Value - attribute value.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkePhase1CheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                    UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr, UINT2 u2Len)
{
    UINT2               u2ConfValue = IKE_ZERO;
    /* XXX In case of multiple policies configured, have to check thru
       all of them
     */

    IKE_UNUSED (u2Len);
    switch (u2Attr)
    {
        case IKE_ENCR_ALGO_ATTRIB:
        {
            if (u2Value != pSessionInfo->IkePhase1Policy.u1EncryptionAlgo)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: Esp Algo not supported\n");
                return IKE_FAILURE;
            }
            else
            {
                pSessionInfo->pIkeSA->u1Phase1EncrAlgo = (UINT1) u2Value;
            }
            break;
        }

        case IKE_HASH_ALGO_ATTRIB:
        {
            if (gau4TransP1HashAlgo[pSessionInfo->IkePhase1Policy.u1HashAlgo] !=
                IKE_ZERO)
            {
                u2ConfValue = (UINT2) (gau4TransP1HashAlgo
                                       [pSessionInfo->IkePhase1Policy.
                                        u1HashAlgo]);
            }
            else
            {
                u2ConfValue = pSessionInfo->IkePhase1Policy.u1HashAlgo;
            }

            if (u2Value != u2ConfValue)
            {
                IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkePhase1Check: Hash Algo not supported"
                              "Conf : %d, Recd : %d\n",
                              pSessionInfo->IkePhase1Policy.u1HashAlgo,
                              u2Value);
                return IKE_FAILURE;
            }
            else
            {
                pSessionInfo->pIkeSA->u1Phase1HashAlgo =
                    pSessionInfo->IkePhase1Policy.u1HashAlgo;
            }
            break;
        }

        case IKE_SA_LIFE_TYPE_ATTRIB:
        {
            if ((u2Value == IKE_LIFE_SECONDS) ||
                (u2Value == IKE_LIFE_KILOBYTES))
            {
                pSessionInfo->u1LifeTimeType = (UINT1) u2Value;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: Life Type not supported\n");
                return IKE_FAILURE;
            }
            break;
        }

        case IKE_SA_LIFE_DUR_ATTRIB:
        {
            UINT4               u4ConfLifeTime = IKE_ZERO;
            UINT4               u4RecdLifeTime = IKE_ZERO;

            if (u4AttrType != IKE_ZERO)
            {
                /* Basic Type */
                u4RecdLifeTime = u2Value;
            }
            else
            {
                /* Variable Type */
                u4RecdLifeTime = IKE_NTOHL (*(UINT4 *) (void *) pu1VarPtr);
            }

            if (pSessionInfo->u1LifeTimeType == IKE_LIFE_SECONDS)
            {
                /* secs */
                /* Set the lifetime */
                u4ConfLifeTime = IkeGetPhase1LifeTime
                    (&pSessionInfo->IkePhase1Policy);

                if (u4ConfLifeTime == IKE_ZERO)
                {
                    /* Lifetime in seconds not configured */
                    if (u4RecdLifeTime < FSIKE_MIN_LIFETIME_SECS)
                    {
                        pSessionInfo->pIkeSA->u4LifeTime =
                            FSIKE_MIN_LIFETIME_SECS;
                        /* Send a notification */
                    }
                    else
                    {
                        pSessionInfo->pIkeSA->u4LifeTime = u4RecdLifeTime;
                    }
                    break;
                }

                /* Whichever is less between u4ConfLifeTime
                 * and recd value is used
                 */
                if (u4ConfLifeTime > u4RecdLifeTime)
                {
                    /* Configured lifetime is greater, use the
                     * peer's value
                     */
                    pSessionInfo->pIkeSA->u4LifeTime = u4RecdLifeTime;
                }
                else
                {
                    /* Peer's value is greater, use the configured
                     * value
                     * XXX We may have to send a notification of type
                     * RESPONDER-LIFETIME to the peer, as defined by
                     * RFC 2407, Sec-4.5.4 and Sec-4.6.3.1
                     * This is only for IPSec, put this comment 
                     * there when we change CheckPhase2Attributes
                     */
                    pSessionInfo->pIkeSA->u4LifeTime = u4ConfLifeTime;
                }
            }
            else
            {
                UINT4               u4SoftLifetimeFactor = IKE_ZERO;

                /* KB */
                u4ConfLifeTime = pSessionInfo->IkePhase1Policy.u4LifeTimeKB;
                /* Set the KB lifetime */
                if (u4ConfLifeTime == IKE_ZERO)
                {
                    /* LifeTimeKB not configured, make sure that
                     * the recd value is not too low to guard against
                     * Denial-of-Service attacks
                     */
                    if (u4RecdLifeTime < FSIKE_MIN_LIFETIME_KB)
                    {
                        pSessionInfo->pIkeSA->u4LifeTimeKB =
                            FSIKE_MIN_LIFETIME_KB;
                        /* XXX Send Notification */
                    }
                    else
                    {
                        /* Set the LifeTimeKB value */
                        pSessionInfo->pIkeSA->u4LifeTimeKB = u4RecdLifeTime;
                    }

                    /* Set the soft lifetime in bytes */
                    /* SoftLifeTime for KB is being calculated here itself
                     * as the SA will be used to Encrypt/Decrypt some messages
                     * of Phase1 exchange, even before Storing IkeSA 
                     */
                    u4SoftLifetimeFactor =
                        (UINT4) (OSIX_RAND (IKE_MIN_SOFTLIFETIME_FACTOR,
                                            IKE_MAX_SOFTLIFETIME_FACTOR));
                    pSessionInfo->pIkeSA->u4SoftLifeTimeBytes =
                        (pSessionInfo->pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB *
                         u4SoftLifetimeFactor) / IKE_DIVISOR_100;
                    break;
                }

                /* Whichever is less between u4ConfLifeTime
                 * and recd value is used
                 */
                if (u4ConfLifeTime > u4RecdLifeTime)
                {
                    /* Configured lifetime is greater, use the
                     * peer's value
                     */
                    pSessionInfo->pIkeSA->u4LifeTimeKB = u4RecdLifeTime;
                }
                else
                {
                    /* Peer's value is greater, use the configured
                     * value
                     * XXX We may have to send a notification of type
                     * RESPONDER-LIFETIME to the peer, as defined by
                     * RFC 2407, Sec-4.5.4 and Sec-4.6.3.1
                     */
                    pSessionInfo->pIkeSA->u4LifeTimeKB = u4ConfLifeTime;
                }

                /* Set the soft lifetime in bytes */
                /* SoftLifeTime for KB is being calculated here itself
                 * as the SA will be used to Encrypt/Decrypt some messages
                 * of Phase1 exchange, even before Storing IkeSA 
                 */
                u4SoftLifetimeFactor =
                    (UINT4) (OSIX_RAND
                             (IKE_MIN_SOFTLIFETIME_FACTOR,
                              IKE_MAX_SOFTLIFETIME_FACTOR));
                pSessionInfo->pIkeSA->u4SoftLifeTimeBytes =
                    (pSessionInfo->pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB *
                     u4SoftLifetimeFactor) / IKE_DIVISOR_100;
            }
            break;
        }

        case IKE_GROUP_DESC_ATTRIB:
        {
            if (u2Value != pSessionInfo->IkePhase1Policy.u1DHGroup)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: Dh Group does not match\n");
                return IKE_FAILURE;
            }
            break;
        }

        case IKE_AUTH_METHOD_ATTRIB:
        {
            /* XXX msb Added the below switch-case 
             * to support XAuth and CM */
            switch (u2Value)
            {
                case IKE_XAUTH_INIT_PRESHARED_KEY:
                {
                    if ((gbXAuthServer == TRUE) &&
                        (pSessionInfo->u1Role == IKE_INITIATOR))
                    {
                        return IKE_FAILURE;
                    }

                    if ((gbXAuthServer == FALSE) &&
                        (pSessionInfo->u1Role == IKE_RESPONDER))
                    {
                        return IKE_FAILURE;
                    }
                    u2Value = IKE_PRESHARED_KEY;
                }
                    break;

                case IKE_XAUTH_RESP_PRESHARED_KEY:
                {
                    /* Not supported */
                    return IKE_FAILURE;
                }
                default:
                {
                    break;
                }
            }                    /* switch */

            if (u2Value != pSessionInfo->pIkeSA->u2AuthMethod)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: Auth Type not Supported\n");
                return IKE_FAILURE;
            }
            else
            {
                pSessionInfo->pIkeSA->u2AuthMethod = u2Value;
            }
            /* XXX msb done */
            break;
        }

        case IKE_SA_KEY_LEN_ATTRIB:
        {
            if (pSessionInfo->IkePhase1Policy.u1EncryptionAlgo == IKE_AES)
            {
                if (u2Value != pSessionInfo->IkePhase1Policy.u2KeyLen)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase1Check: AES key len not supported\n");
                    return IKE_FAILURE;
                }
                pSessionInfo->IkePhase1Info.u2NegKeyLen = u2Value;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: Key Len attribute not"
                             "supported for this encryption algo\n");
                return IKE_FAILURE;
            }
        }
            break;

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase1Check: Attribute not Supported\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkePhase2CheckAtts
 *  Description   : Function to check phase 2 attributes. 
 *  Input(s)      : pSesInfo - pointer to phase 2 data structure 
 *                : u2class - class info 
 *                  u2Value - attribute value.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkePhase2CheckAtts (UINT1 u1Role, tIkeSessionInfo * pSesInfo, UINT4 u4AttrType,
                    UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr, UINT2 u2Len,
                    UINT1 u1ProtocolId, UINT1 u1TransNum)
{
    UINT2               u2ConfValue = IKE_ZERO;

    IKE_UNUSED (u1Role);
    IKE_UNUSED (u2Len);

    if (u1TransNum >= MAX_TRANSFORMS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase2CheckAtts: Invalid TransformSetBundle Index\n");
        return IKE_FAILURE;
    }

    switch (u2Attr)
    {
        case IKE_IPSEC_AUTH_ALGO_ATTRIB:
        {
            if (u1ProtocolId == IKE_IPSEC_AH)
            {
                if (((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      IKE_IPSEC_AH_MD5) && (u2Value != IKE_IPSEC_HMAC_MD5))
                    ||
                    ((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      IKE_IPSEC_AH_SHA1) && (u2Value != IKE_IPSEC_HMAC_SHA1))
                    ||
                    ((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      SEC_XCBCMAC) && (u2Value != SEC_XCBCMAC))
                    ||
                    ((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      HMAC_SHA_256) && (u2Value != HMAC_SHA_256))
                    ||
                    ((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      HMAC_SHA_384) && (u2Value != HMAC_SHA_384))
                    ||
                    ((pSesInfo->IkePhase2Policy.
                      aTransformSetBundle[u1TransNum]->u1AhHashAlgo ==
                      HMAC_SHA_512) && (u2Value != HMAC_SHA_512)))
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase2Check: Auth Algo not supported\n");
                    return IKE_FAILURE;
                }
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u1HashAlgo = (UINT1) u2Value;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u1HashAlgo = (UINT1) u2Value;
            }
            else if (u1ProtocolId == IKE_IPSEC_ESP)
            {
                u2ConfValue =
                    pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                    u1EspHashAlgo;

                if (gau4TransP2HashAlgo[u2ConfValue] != IKE_ZERO)
                {
                    u2ConfValue = (UINT2) gau4TransP2HashAlgo[u2ConfValue];
                }
                else
                {
                    u2ConfValue =
                        pSesInfo->IkePhase2Policy.
                        aTransformSetBundle[u1TransNum]->u1EspHashAlgo;
                }
                if (u2ConfValue != u2Value)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase2Check: Auth Algo not supported\n");
                    return IKE_FAILURE;
                }
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u1HashAlgo =
                    pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                    u1EspHashAlgo;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u1HashAlgo =
                    pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                    u1EspHashAlgo;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase2CheckAtts: Protocol not supported\n");
                return IKE_FAILURE;
            }
            break;
        }

        case IKE_IPSEC_SA_LIFE_TYPE_ATTRIB:
        {
            if ((u2Value == IKE_LIFE_SECONDS) ||
                (u2Value == IKE_LIFE_KILOBYTES))
            {
                pSesInfo->u1LifeTimeType = (UINT1) u2Value;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase2CheckAtts: Life Type not supported\n");
                return IKE_FAILURE;
            }
            break;
        }

        case IKE_IPSEC_SA_LIFE_DUR_ATTRIB:
        {
            UINT4               u4ConfLifeTime = IKE_ZERO;
            UINT4               u4RecdLifeTime = IKE_ZERO;
            UINT4               u4FinalLifeTime = IKE_ZERO;

            if (u4AttrType != IKE_ZERO)
            {
                /* Basic Type */
                u4RecdLifeTime = u2Value;
            }
            else
            {
                /* Variable Type */
                u4RecdLifeTime = IKE_NTOHL (*(UINT4 *) (void *) pu1VarPtr);
            }

            if (pSesInfo->u1LifeTimeType == IKE_LIFE_SECONDS)
            {
                /* secs */
                /* Set the lifetime */
                u4ConfLifeTime = IkeGetPhase2LifeTime
                    (&pSesInfo->IkePhase2Policy);

                /* Whichever is less between u4ConfLifeTime
                 * and recd value is used
                 */
                if ((u4ConfLifeTime > u4RecdLifeTime)
                    || (u4ConfLifeTime == IKE_ZERO))
                {
                    /* Configured lifetime is greater, use the
                     * peer's value if it is above a certain
                     * threshold
                     */
                    if (u4RecdLifeTime < FSIKE_MIN_LIFETIME_SECS)
                    {
                        u4FinalLifeTime = FSIKE_MIN_LIFETIME_SECS;
                        /* Send a notification */
                    }
                    else
                    {
                        u4FinalLifeTime = u4RecdLifeTime;
                    }
                }
                else
                {
                    /* Peer's value is greater, use the configured
                     * value
                     * XXX We may have to send a notification of type
                     * RESPONDER-LIFETIME to the peer, as defined by
                     * RFC 2407, Sec-4.5.4 and Sec-4.6.3.1
                     * This is only for IPSec, put this comment 
                     * there when we change CheckPhase2Attributes
                     */
                    u4FinalLifeTime = u4ConfLifeTime;
                }

                if (u1ProtocolId == IKE_IPSEC_ESP)
                {
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aInSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                        u4FinalLifeTime;
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                        u4FinalLifeTime;
                }
                else if (u1ProtocolId == IKE_IPSEC_AH)
                {
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aInSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                        u4FinalLifeTime;
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aOutSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                        u4FinalLifeTime;
                }
            }
            else
            {
                /* KB */
                u4ConfLifeTime = pSesInfo->IkePhase2Policy.u4LifeTimeKB;
                /* Set the KB lifetime to whichever is less between 
                 * u4ConfLifeTime and recd value
                 */
                if ((u4ConfLifeTime > u4RecdLifeTime)
                    || (u4ConfLifeTime == IKE_ZERO))
                {
                    /* LifeTimeKB not configured or peer value is lesser, 
                     * make sure that the recd value is not too low to 
                     * guard against Denial-of-Service attacks
                     */
                    if (u4RecdLifeTime < FSIKE_MIN_LIFETIME_KB)
                    {
                        u4FinalLifeTime = FSIKE_MIN_LIFETIME_KB;
                        /* XXX Send Notification */
                    }
                    else
                    {
                        /* Set the LifeTimeKB value */
                        u4FinalLifeTime = u4RecdLifeTime;
                    }
                }
                else
                {
                    /* Peer's value is greater, use the configured
                     * value
                     * XXX We may have to send a notification of type
                     * RESPONDER-LIFETIME to the peer, as defined by
                     * RFC 2407, Sec-4.5.4 and Sec-4.6.3.1
                     */
                    u4FinalLifeTime = u4ConfLifeTime;
                }

                if (u1ProtocolId == IKE_IPSEC_ESP)
                {
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aInSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                        u4FinalLifeTime;
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                        u4FinalLifeTime;
                }
                else if (u1ProtocolId == IKE_IPSEC_AH)
                {
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aInSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                        u4FinalLifeTime;
                    pSesInfo->IkePhase2Info.IPSecBundle.
                        aOutSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                        u4FinalLifeTime;
                }
            }
            break;
        }

        case IKE_IPSEC_ENCAPSULATION_MODE_ATTRIB:
        {
            if (u2Value == IKE_IPSEC_UDP_TUNNEL_MODE)
            {
                u2Value = IKE_IPSEC_TUNNEL_MODE;
            }
            if ((u1ProtocolId == IKE_IPSEC_AH) && (pSesInfo->IkePhase2Policy.
                                                   bModeForEspOnly == IKE_TRUE)
                && (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                    u1EspEncryptionAlgo != IKE_ZERO))
            {
                if (u2Value != IKE_IPSEC_TRANSPORT_MODE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase2CheckAtts: Mode mis-match\n");
                    return IKE_FAILURE;
                }
            }
            else
            {
                if (u2Value != pSesInfo->IkePhase2Policy.u1Mode)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase2CheckAtts: Mode mis-match\n");
                    return IKE_FAILURE;
                }
            }
            if (u1ProtocolId == IKE_IPSEC_ESP)
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u1Mode = (UINT1) u2Value;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_ESP_SA_INDEX].u1Mode = (UINT1) u2Value;
            }
            else
            {
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u1Mode = (UINT1) u2Value;
                pSesInfo->IkePhase2Info.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u1Mode = (UINT1) u2Value;
            }
            break;
        }

        case IKE_IPSEC_GROUP_DESC_ATTRIB:
        {
            if (pSesInfo->IkePhase2Policy.u1Pfs == IKE_NONE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase2Check: PFS not configured\n");
                return IKE_FAILURE;
            }

            if (pSesInfo->bLastPropNumSame != IKE_TRUE)
            {
                pSesInfo->au4LastPropAttrib[u2Attr] = u2Value;
            }
            else
            {
                if (u2Value != pSesInfo->au4LastPropAttrib[u2Attr])
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase1Check: PFS group does not match"
                                 " the pfs group of previous"
                                 "proposal with same number\n");
                    return IKE_FAILURE;
                }
            }

            if (u2Value != pSesInfo->IkePhase2Policy.u1Pfs)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase1Check: PFS group does not match\n");
                return IKE_FAILURE;
            }
            else
            {
                pSesInfo->IkePhase2Info.u1NegPfs = (UINT1) u2Value;
            }
            break;
        }

        case IKE_IPSEC_KEY_LEN_ATTRIB:
        {
            if ((pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                 u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES) ||
                (pSesInfo->IkePhase2Policy.aTransformSetBundle[u1TransNum]->
                 u1EspEncryptionAlgo == IKE_IPSEC_ESP_AES_CTR))

            {
                if (u2Value != pSesInfo->IkePhase2Policy.
                    aTransformSetBundle[u1TransNum]->u2KeyLen)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkePhase1Check:%d  Key length not supported"
                                  "\n", u2Value);
                    return IKE_FAILURE;
                }
                pSesInfo->IkePhase2Info.u2NegKeyLen = u2Value;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkePhase2CheckAtts: Atribute Not Supported\n");
                return IKE_FAILURE;
            }
        }
            break;

        default:
        {
            IKE_SET_ERROR (pSesInfo, IKE_ATTRIBUTES_NOT_SUPPORTED);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkePhase2CheckAtts: Atribute Not Supported\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessQmHash1
 *  Description   : Function to process QM Hash 1
 *  Input(s)      : pSesInfo - pointer to phase 2 data structure 
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT1
IkeProcessQmHash1 (tIkeSessionInfo * pSesInfo)
{
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL, *pu1PayLoad = NULL;
    UINT1               au1Digest[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    INT4                i4HashStrt = { IKE_ZERO };
    tIkePayload        *pHashPayload = NULL;
    UINT4               u4MesgId = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    if (SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD])
        != IKE_ONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash1: No or More than one Hash payload "
                     "recd\n");
        return (IKE_FAILURE);
    }

    pHashPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD]);
    if (pHashPayload == NULL)
    {
        return (IKE_FAILURE);
    }
    pu1PayLoad = pHashPayload->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &HashPayLoad, pu1PayLoad, IKE_HASH_PAYLOAD_SIZE);

    if (pSesInfo->u1ExchangeType == IKE_QUICK_MODE)
    {
        if (HashPayLoad.u1NextPayLoad != IKE_SA_PAYLOAD)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessQmHash1:\
                         Hash payload is not followed by SA " "Payload\n");
            return (IKE_FAILURE);
        }
    }

    HashPayLoad.u2PayLoadLen = IKE_NTOHS (HashPayLoad.u2PayLoadLen);

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    i4HashStrt = sizeof (tIsakmpHdr) + sizeof (tHashPayLoad);

    if ((HashPayLoad.u2PayLoadLen - IKE_HASH_PAYLOAD_SIZE) != (UINT2) u4HashLen)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash1: Digest Length Mismatch\n");
        return (IKE_FAILURE);
    }

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeRxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash1: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessQmHash1: Memory Not Available\n");
        return IKE_FAILURE;
    }
    /* Calculation of Hash 1 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | SA | Ni [ | KE ] [ | IDci | IDcr ] */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4),
                pSesInfo->IkeRxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         (INT4) (u2PayLoadLen + sizeof (UINT4)), au1Digest);

    /* Return  the resultant after Comparing the computed hash and the 
     * received hash */

    u4HashLen = (u4HashLen <= IKE_MAX_DIGEST_SIZE) ?
        u4HashLen : IKE_MAX_DIGEST_SIZE;

    if (IKE_MEMCMP
        (au1Digest, (UINT1 *) (pSesInfo->IkeRxPktInfo.pu1RawPkt + i4HashStrt),
         u4HashLen) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash1:Digest Mismatch!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessQmHash2
 *  Description   : Function to process QM Hash 2
 *  Input(s)      : pSesInfo - pointer to phase 2 data structure 
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT1
IkeProcessQmHash2 (tIkeSessionInfo * pSesInfo)
{
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL, *pu1PayLoad = NULL;
    UINT1               au1Digest[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    INT4                i4HashStrt = IKE_ZERO;
    tIkePayload        *pHashPayload = NULL;
    UINT4               u4MesgId = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    if (SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD])
        != IKE_ONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash2: No or More than one Hash payload "
                     "recd\n");
        return (IKE_FAILURE);
    }

    pHashPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD]);
    if (pHashPayload == NULL)
    {
        return (IKE_FAILURE);
    }
    pu1PayLoad = pHashPayload->pRawPayLoad;

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    IKE_MEMCPY ((UINT1 *) &HashPayLoad, pu1PayLoad, IKE_HASH_PAYLOAD_SIZE);
    if (HashPayLoad.u1NextPayLoad != IKE_SA_PAYLOAD)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash2: Hash payload is not followed by SA "
                     "Payload\n");
        return (IKE_FAILURE);
    }

    HashPayLoad.u2PayLoadLen = IKE_NTOHS (HashPayLoad.u2PayLoadLen);

    i4HashStrt = sizeof (tIsakmpHdr) + sizeof (tHashPayLoad);

    if ((UINT2) (HashPayLoad.u2PayLoadLen - IKE_HASH_PAYLOAD_SIZE) !=
        (UINT2) u4HashLen)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash2: Digest Length Mismatch\n");
        return (IKE_FAILURE);
    }

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeRxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash2: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessQmHash2: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 2 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | Ni_b | SA | Ni [ | KE ] [ | IDci | IDcr ] */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4), pSesInfo->pu1InitiatorNonce,
                pSesInfo->u2InitiatorNonceLen);

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4) + pSesInfo->u2InitiatorNonceLen,
                pSesInfo->IkeRxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         (INT4) (u2PayLoadLen + sizeof (UINT4) + pSesInfo->u2InitiatorNonceLen),
         au1Digest);

    /* Return  the resultant after Comparing the computed hash and the 
     * received hash */

    u4HashLen = (u4HashLen <= IKE_MAX_DIGEST_SIZE) ?
        u4HashLen : IKE_MAX_DIGEST_SIZE;

    if (IKE_MEMCMP
        (au1Digest, (UINT1 *) (pSesInfo->IkeRxPktInfo.pu1RawPkt + i4HashStrt),
         u4HashLen) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash2:Digest Mismatch!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessQmHash3
 *  Description   : Function to process QM Hash 3
 *  Input(s)      : pSesInfo - pointer to phase 2 data structure 
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT1
IkeProcessQmHash3 (tIkeSessionInfo * pSesInfo)
{
    UINT2               u2MesStrt = IKE_ZERO, u2BufLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL, *pu1PayLoad = NULL;
    UINT1               au1Digest[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    INT4                i4HashStrt = IKE_ZERO;
    tIkePayload        *pHashPayload = NULL;
    UINT4               u4MesgId = IKE_ZERO, u4HashLen = IKE_ZERO;

    UNUSED_PARAM (u2MesStrt);

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);
    if (SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD])
        != IKE_ONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash3: No or More than one Hash payload "
                     "recd\n");
        return (IKE_FAILURE);
    }

    pHashPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD]);
    if (pHashPayload == NULL)
    {
        return (IKE_FAILURE);
    }
    pu1PayLoad = pHashPayload->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &HashPayLoad, pu1PayLoad, IKE_HASH_PAYLOAD_SIZE);

    HashPayLoad.u2PayLoadLen = IKE_NTOHS (HashPayLoad.u2PayLoadLen);

    i4HashStrt = sizeof (tIsakmpHdr) + sizeof (tHashPayLoad);

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    if ((HashPayLoad.u2PayLoadLen - IKE_HASH_PAYLOAD_SIZE) != (UINT2) u4HashLen)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash3: Digest Length Mismatch\n");
        return (IKE_FAILURE);
    }

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2BufLen =
        (UINT2) (IKE_ONE + sizeof (u4MesgId) + pSesInfo->u2ResponderNonceLen +
                 pSesInfo->u2InitiatorNonceLen);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash3: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessQmHash3: Memory Not Available\n");
        return IKE_FAILURE;
    }

    /* Calculation of Hash 3 in Quick Mode */
    /* Key is SKEYID_a, and the buffer is *
     * 0 | M-ID | Ni_b | Nr_b */

    pu1BufPtr[IKE_INDEX_0] = IKE_ZERO;

    IKE_MEMCPY ((pu1BufPtr + IKE_ONE), &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY ((pu1BufPtr + IKE_ONE
                 + sizeof (UINT4)), pSesInfo->pu1InitiatorNonce,
                pSesInfo->u2InitiatorNonceLen);

    IKE_MEMCPY ((pu1BufPtr + IKE_ONE + sizeof (UINT4) +
                 pSesInfo->u2InitiatorNonceLen), pSesInfo->pu1ResponderNonce,
                pSesInfo->u2ResponderNonceLen);

    (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr, u2BufLen, au1Digest);

    /* Return  the resultant after Comparing the computed hash and the 
     * received hash */

    u4HashLen = (u4HashLen <= IKE_MAX_DIGEST_SIZE) ?
        u4HashLen : IKE_MAX_DIGEST_SIZE;

    if (IKE_MEMCMP
        (au1Digest, (UINT1 *) (pSesInfo->IkeRxPktInfo.pu1RawPkt + i4HashStrt),
         u4HashLen) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessQmHash3:Digest Mismatch!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeProcessCertRequest                              */
/*  Description     :This functions is used to process the certificate  */
/*                  :request payload                                    */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the cert req   */
/*                  :information                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeProcessCertRequest (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tCertReqPayLoad     CertReq;
    tIkeCertReqInfo    *pCertReq = NULL;
    UINT2               u2CANameLen = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_BZERO (&CertReq, sizeof (tCertReqPayLoad));
    IKE_MEMCPY ((UINT1 *) &CertReq, pu1PayLoad, sizeof (tCertReqPayLoad));

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    CertReq.u2PayLoadLen = IKE_NTOHS (CertReq.u2PayLoadLen);

    if (CertReq.u1EncodeType != IKE_X509_SIGNATURE)
    {
        IKE_SET_ERROR (pSesInfo, IKE_CERT_TYPE_UNSUPPORTED);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessCertRequest:\
                     Unsupported Encode type for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    if (MemAllocateMemBlock
        (IKE_CERT_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pCertReq) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertRequest: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pCertReq == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertRequest: Failed to allocate memory for "
                     "certreq\n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pCertReq, sizeof (tIkeCertReqInfo));
    pCertReq->u1EncodeType = CertReq.u1EncodeType;
    pCertReq->pu1HashList = NULL;

    if (CertReq.u2PayLoadLen > IKE_CERT_PAYLOAD_SIZE)
    {
        /* Validate CA name and convert it into X509Name format */
        u2CANameLen = (UINT2) (CertReq.u2PayLoadLen - IKE_CERT_PAYLOAD_SIZE);

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pCertReq->pu1HashList) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessCertRequest: unable to allocate "
                         "buffer\n");
            MemReleaseMemBlock (IKE_CERT_INFO_MEMPOOL_ID, (UINT1 *) pCertReq);
            return IKE_FAILURE;
        }

        if (pCertReq->pu1HashList == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessCertRequest: MemAlloc for storing the "
                         "HashList failed\n");
            MemReleaseMemBlock (IKE_CERT_INFO_MEMPOOL_ID, (UINT1 *) pCertReq);
            return (IKE_FAILURE);
        }
        IKE_BZERO (pCertReq->pu1HashList, (u2CANameLen + IKE_ONE));
        IKE_MEMCPY (pCertReq->pu1HashList, (pu1PayLoad + CertReq.u2PayLoadLen),
                    u2CANameLen);
        pCertReq->u4HashListLen = u2CANameLen;
    }

    /* Store certificate request information to session */
    SLL_ADD (&(pSesInfo->PeerCertReq), (t_SLL_NODE *) pCertReq);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessCertficate                               */
/*  Description     :This functions is used to process the certificate  */
/*                  :payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the certficate */
/*                  :information                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeProcessCertificate (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tCertPayLoad        Cert;
    UINT2               u2CertLen = IKE_ZERO;
    tIkeX509           *pX509Cert = NULL;
    tIkeX509Name       *pCAName = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertDB         *pPeerCert = NULL;
    CHR1               *pu1PeerAddrStr = NULL;
    UINT1               au1PeerAddrStr[40];

    IKE_BZERO (au1PeerAddrStr, sizeof (au1PeerAddrStr));

    IKE_BZERO (&Cert, sizeof (tCertPayLoad));

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }
    /* Copying to array to avoid the use of address of static variable */

    IKE_MEMCPY (au1PeerAddrStr, pu1PeerAddrStr, STRLEN (pu1PeerAddrStr));

    IKE_MEMCPY ((UINT1 *) &Cert, pu1PayLoad, IKE_CERT_PAYLOAD_SIZE);

    Cert.u2PayLoadLen = IKE_NTOHS (Cert.u2PayLoadLen);

    if (Cert.u1EncodeType != IKE_X509_SIGNATURE)
    {
        IKE_SET_ERROR (pSesInfo, IKE_CERT_TYPE_UNSUPPORTED);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertificate: Unsupported Encode type\n");
        return (IKE_FAILURE);
    }

    u2CertLen = (UINT2) (Cert.u2PayLoadLen - IKE_CERT_PAYLOAD_SIZE);

    if (u2CertLen == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertificate: No certificate present\n");
        return (IKE_FAILURE);
    }

    pX509Cert =
        IkeGetX509FromDER ((UINT1 *) (pu1PayLoad + IKE_CERT_PAYLOAD_SIZE),
                           u2CertLen);

    if (pX509Cert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertificate: Failed to cert to X509 format\n");
        return (IKE_FAILURE);
    }

    pCAName = IkeGetX509IssuerName (pX509Cert);

    pCASerialNum = IkeGetX509SerialNum (pX509Cert);

    if ((pCAName == NULL) || (pCASerialNum == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeProcessCertificate:\
                     Failed to get CA info from cert\n");
        return (IKE_FAILURE);
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessCertificate: Engine not active\n");
        return (IKE_FAILURE);
    }

    pPeerCert =
        IkeGetCertFromPeerCerts (pIkeEngine->au1EngineName, pCAName,
                                 pCASerialNum);

    if (pPeerCert == NULL)
    {
        /* Validate the certificate params */
        if (IkeValidatePeerCert (pSesInfo, pX509Cert) == IKE_FAILURE)
        {
            IKE_SET_ERROR (pSesInfo, IKE_INVALID_CERTIFICATE);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessCertificate: Validation of the cert "
                         "failed\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeProcessCertificate: Validation of the cert failed for peer %s\n",
                          au1PeerAddrStr));
            return (IKE_FAILURE);
        }
        else
        {
            /* Verify the certificate */
            if (IkeVerifyCert (pIkeEngine->au1EngineName, pX509Cert) ==
                IKE_FAILURE)
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_CERTIFICATE);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessCertificate: Verification of the "
                             "cert failed\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                              "IkeProcessCertificate: Verification of the cert failed for peer %s\n",
                              au1PeerAddrStr));
                return (IKE_FAILURE);
            }
            else
            {
                /* Add this certificate to certificate database */
                if (IkeAddCertToPeerCerts (pIkeEngine->au1EngineName, NULL,
                                           pX509Cert,
                                           IKE_DYNAMIC_PEER_CERT) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessCertificate: Unable to "
                                 "allocate memory for peer cert\n");
                    return (IKE_FAILURE);
                }
            }
        }
    }

    /* Store the cert information to the session data structure */
    pSesInfo->PeerCertInfo.pCAName = IkeX509NameDup (pCAName);
    pSesInfo->PeerCertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);

    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                  "IkeProcessCert: "
                  "Verification of the certificate successful for peer: %s\n",
                  au1PeerAddrStr);
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId, "IkeProcessCert: "
                  "Verification of the certificate successful for peer: %s\n",
                  au1PeerAddrStr));

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessSignature                                */
/*  Description     :This functions is used to process the certificate  */
/*                  :payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeProcessSignature (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tSigPayLoad         Sig;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertDB         *pPeerCert = NULL;
    tIkePKey           *pPubKey = NULL;
    UINT2               u2BufLen = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO, u4ErrorCode = IKE_ZERO;
    INT4                i4DecrLen = IKE_ZERO;
    UINT1              *pu1DecrBuf = NULL;
    UINT1               au1HashData[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    CHR1               *pu1PeerAddrStr = NULL;

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    IKE_BZERO ((UINT1 *) au1HashData, IKE_MAX_DIGEST_SIZE);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if ((pSesInfo->PeerCertInfo.pCAName == NULL) ||
        (pSesInfo->PeerCertInfo.pCASerialNum == NULL))
    {
        IKE_SET_ERROR (pSesInfo, IKE_INVALID_SIGNATURE);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSignature: No cert info is available for "
                      "verification for peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    IKE_BZERO (&Sig, sizeof (tSigPayLoad));
    IKE_MEMCPY ((UINT1 *) &Sig, pu1PayLoad, IKE_SIG_PAYLOAD_SIZE);

    Sig.u2PayLoadLen = IKE_NTOHS (Sig.u2PayLoadLen);

    u2BufLen = (UINT2) (Sig.u2PayLoadLen - IKE_SIG_PAYLOAD_SIZE);

    if (u2BufLen == IKE_ZERO)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSignature: No signature data for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessSignature: Engine not active\n");
        return (IKE_FAILURE);
    }

    pPeerCert =
        IkeGetCertFromPeerCerts (pIkeEngine->au1EngineName,
                                 pSesInfo->PeerCertInfo.pCAName,
                                 pSesInfo->PeerCertInfo.pCASerialNum);

    if (pPeerCert == NULL)
    {
        IKE_SET_ERROR (pSesInfo, IKE_INVALID_SIGNATURE);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSignature: No cert is available for "
                      "verification for peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    pPubKey = IkeGetX509PubKey (pPeerCert->pCert);

    if (pPubKey == NULL)
    {
        IKE_SET_ERROR (pSesInfo, IKE_INVALID_SIGNATURE);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSignature: Failed to get public key from "
                      "cert for peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    if (IkeVerifyHash (pSesInfo, au1HashData) == IKE_FAILURE)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeProcessSignature : Compute Hash fails for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    switch (pSesInfo->pIkeSA->u2AuthMethod)
    {
        case IKE_RSA_SIGNATURE:
        {
            if (IkeRsaPublicDecrypt
                ((UINT1 *) (pu1PayLoad + IKE_SIG_PAYLOAD_SIZE), u2BufLen,
                 &pu1DecrBuf, &i4DecrLen, pPubKey, &u4ErrorCode) == IKE_FAILURE)
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_SIGNATURE);
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_RSA_FAILED:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : Unable to get"
                                      " rsa key for peer: %s\n",
                                      pu1PeerAddrStr);
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessSignature : Memory allocation"
                                     " failed\n");
                        break;
                    }
                    case IKE_ERR_RSA_PUB_DECRYPT_FAILED:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : Rsa public decrypt"
                                      " failed for peer: %s\n", pu1PeerAddrStr);
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : Rsa decryption"
                                      " failed for peer: %s\n", pu1PeerAddrStr);
                        break;
                    }
                }
                if (pu1DecrBuf != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1DecrBuf);
                }
                return (IKE_FAILURE);
            }
            /* Compare the decrypted signature with the receiver hash */
            if (u4HashLen != (UINT4) i4DecrLen)
            {
                IKE_SET_ERROR (pSesInfo, IKE_AUTH_FAILED);
                IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessSignature : Signature length (%d)"
                              "mismatch for peer: %s\n", i4DecrLen,
                              pu1PeerAddrStr);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1DecrBuf);
                return (IKE_FAILURE);
            }

            if (IKE_MEMCMP (au1HashData, pu1DecrBuf, u4HashLen) != IKE_ZERO)
            {
                IKE_SET_ERROR (pSesInfo, IKE_AUTH_FAILED);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeProcessSignature : Signature verification "
                              "fails for peer: %s\n", pu1PeerAddrStr);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1DecrBuf);
                return (IKE_FAILURE);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1DecrBuf);
            break;
        }
        case IKE_DSS_SIGNATURE:
        {
            /*Verify the Signature which is received in Auth Payload */
            if (IkeDsaVerify (au1HashData, (pu1PayLoad + IKE_SIG_PAYLOAD_SIZE),
                              pPubKey, &u4ErrorCode) == IKE_FAILURE)
            {
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_DSA_FAILED:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : "
                                      "Unable to get Dsa key for peer: %s\n",
                                      pu1PeerAddrStr);
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : Memory "
                                      "allocation failed for peer: %s\n",
                                      pu1PeerAddrStr);
                        break;
                    }
                    case IKE_ERR_DSA_PUB_DECRYPT_FAILED:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : Dsa public "
                                      "decrpt failed for peer: %s\n",
                                      pu1PeerAddrStr);
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                      "IkeProcessSignature : "
                                      "Dsa decryption failed for peer: %s\n",
                                      pu1PeerAddrStr);
                        break;
                    }
                }
                return (IKE_FAILURE);
            }
            break;
        }
        default:
        {
            IKE_SET_ERROR (pSesInfo, IKE_AUTH_FAILED);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeProcessSignature : Auth Method not supported for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessVendorId                                 */
/*  Description     :This functions is used to process the Vendor ID    */
/*                  :payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Vendor ID  */
/*                  :information                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessVendorId (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tVendorIdPayLoad    VendorId;
    UINT2               u2VendorIdSize = IKE_ZERO;

    IKE_BZERO (&VendorId, sizeof (tVendorIdPayLoad));
    IKE_MEMCPY ((UINT1 *) &VendorId, pu1PayLoad, IKE_VID_PAYLOAD_SIZE);

    VendorId.u2PayLoadLen = IKE_NTOHS (VendorId.u2PayLoadLen);
    if (VendorId.u2PayLoadLen == IKE_VID_PAYLOAD_SIZE)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeProcessVendorId: No VendorID field!\n");
        /* We will not fail the negotiation because of this */
        return IKE_SUCCESS;
    }

    /* Store the recd. Vendor ID */
    pSesInfo->u4NoofInVendorIDs++;

    u2VendorIdSize = (UINT2) (VendorId.u2PayLoadLen - IKE_VID_PAYLOAD_SIZE);

    u2VendorIdSize = (UINT2) ((u2VendorIdSize <= IKE_MAX_VENDOR_ID_SIZE) ?
                              u2VendorIdSize : IKE_MAX_VENDOR_ID_SIZE);

    if (gaIkeVendorIdPayload[IKE_NATT_SUPPORT_VENDOR_ID].u4Length ==
        (UINT4) (VendorId.u2PayLoadLen - IKE_VID_PAYLOAD_SIZE))
    {
        if (IKE_MEMCMP
            (gaIkeVendorIdPayload[IKE_NATT_SUPPORT_VENDOR_ID].au1Payload,
             (pu1PayLoad + IKE_VID_PAYLOAD_SIZE), u2VendorIdSize) == IKE_ZERO)
        {
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSesInfo->pIkeSA->IkeNattInfo.
                u1NattFlags | IKE_PEER_SUPPORT_NATT;
        }
    }
    if (pSesInfo->u4NoofInVendorIDs >= IKE_MAX_VENDOR_IDS)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeProcessVendorId: Vendor ID array is full!\n");
        /* We will not fail the negotiation because of this */
        return IKE_SUCCESS;
    }

    IKE_MEMCPY (pSesInfo->aInVendorIDs[pSesInfo->u4NoofInVendorIDs].
                au1VendorID, (pu1PayLoad + IKE_VID_PAYLOAD_SIZE),
                u2VendorIdSize);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpCMCfgReqPkt                        */
/*  Description     :This functions is used to process the CM Cfg Req   */
/*                  :Pkt                                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpCMCfgReqPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);

    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_REQUEST)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt:\
                     Received pkt not an request pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckCMCfgReqOrAckAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt:"
                     "IkeCheckCMCfgReqOrAckAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;

    /* Updating the Attribute ID for Interop with the Greenbow VPN client */
    pSesInfo->IkeTransInfo.u2RequestId = IKE_HTONS (AttrPayLoad.u2Id);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpCMCfgRepPkt                        */
/*  Description     :This functions is used to process the CM Cfg Rep   */
/*                  :Pkt                                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpCMCfgRepPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgRepPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_REPLY)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgRep:\
                     Received pkt not an reply pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckCMCfgRepOrSetAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgRepPkt:"
                     "IkeCheckCMCfgRepAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;
    pSesInfo->IkeTransInfo.bCMSuccess = IKE_TRUE;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpCMCfgSetPkt                        */
/*  Description     :This functions is used to process the CM Cfg Set   */
/*                  :Pkt                                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeProcessIsakmpCMCfgSetPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{
    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgSetPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_SET)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgRep:\
                     Received pkt not an reply pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckCMCfgRepOrSetAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgRepPkt:"
                     "IkeCheckCMCfgRepAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;

    return (IKE_SUCCESS);

}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpCMCfgAcktPkt                       */
/*  Description     :This functions is used to process the CM Cfg Ack   */
/*                  :Pkt                                                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpCMCfgAckPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgSetPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_ACK)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgAck: Received pkt not an Ack pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckCMCfgReqOrAckAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgAckPkt:"
                     "IkeCheckCMCfgReqOrAckAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;
    pSesInfo->IkeTransInfo.bCMSuccess = IKE_TRUE;
    return (IKE_SUCCESS);

}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpXAuthCfgReqPkt                     */
/*  Description     :This functions is used to process the XAUTH Cfg    */
/*                  :Req Pkt                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpXAuthCfgReqPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_REQUEST)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgReqPkt: "
                     "Received pkt not an request pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckXAuthCfgReqAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt:"
                     "IkeCheckCMCfgReqAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpXAuthCfgRepPkt                     */
/*  Description     :This functions is used to process the XAUTH Cfg    */
/*                  :Rep Pkt                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpXAuthCfgRepPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgRepPkt: \
                     AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_REPLY)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgRepPkt: "
                     "Received pkt not an reply pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckXAuthCfgRepAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgRepPkt:"
                     "IkeCheckXAuthCfgRepAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;

    if (AuthDbValidateUser
        ((UINT1 *) pSesInfo->IkeTransInfo.au1XAuthRecvUsername,
         (UINT1 *) pSesInfo->IkeTransInfo.au1XAuthRecvPassword,
         (UINT1 *) "IKE") == USER_AUTH_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgRepPkt:"
                     "User Authentication Failed \n");
        return IKE_SUCCESS;
    }
    pSesInfo->IkeTransInfo.bUserAuthenticated = TRUE;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpXAuthCfgSetPkt                     */
/*  Description     :This functions is used to process the XAUTH Cfg    */
/*                  :Set Pkt                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpXAuthCfgSetPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_SET)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgSetPkt: "
                     "Received pkt not an Set pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckXAuthCfgSetAttributes (pSesInfo, pu1PayLoad, u4Len) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgSetPkt:"
                     "IkeCheckXAuthCfgSetAttributes Failed \n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pu1TmpPtr;

    return (IKE_SUCCESS);

}

/************************************************************************/
/*  Function Name   :IkeProcessIsakmpXAuthCfgAckPkt                     */
/*  Description     :This functions is used to process the XAUTH Cfg    */
/*                  :Set Pkt                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeProcessIsakmpXAuthCfgAckPkt (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd)
{
    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;

    IKE_UNUSED (pu1Pd);
    UNUSED_PARAM (u4Len);
    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpCMCfgReqPkt: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    if (AttrPayLoad.u1Type != IKE_ISAKMP_CFG_ACK)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIsakmpXAuthCfgAckPkt: "
                     "Received pkt not an Ack pkt\n");
        return (IKE_FAILURE);
    }

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    pu1PayLoad = pu1TmpPtr;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeCheckCMCfgReqOrAckAttributes                    */
/*  Description     :This functions is used to check the attributes of  */
/*                  :CM Req or Ack                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckCMCfgReqOrAckAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                 UINT4 u4Len)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    while ((u4Count < u4Len) && (u1Index < IKE_MAX_SUPPORTED_ATTR))
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckCMCfgReqOrAckAttributes:\
                         IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }
        switch (u2Attr)
        {

            case IKE_SUPPORTED_ATTRIBUTES:
            {
                if ((UINT2) i4Len != IKE_ZERO)
                {

                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCMCfgReqCheckAtts: Length of the "
                                 "supported attribute must be zero\n");
                    return IKE_FAILURE;
                }
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_SUPPORTED_ATTRIBUTES;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_ADDRESS:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_ADDRESS;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_NETMASK:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_NETMASK;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_SUBNET:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_SUBNET;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP6_ADDRESS:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP6_ADDRESS;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP6_NETMASK:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP6_NETMASK;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP4_DNS:
            case IKE_INTERNAL_ADDRESS_EXPIRY:
            case IKE_INTERNAL_IP4_DHCP:
            case IKE_APPLICATION_VERSION:
            case IKE_INTERNAL_IP6_DNS:
            case IKE_INTERNAL_IP6_DHCP:
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCMCfgReqCheckAtts:\
                             Unsupported Attributes Discarded \n");
                return IKE_SUCCESS;
            }
        }
        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCheckCMCfgRepOrSetAttributes                    */
/*  Description     :This functions is used to check the attributes of  */
/*                  :CM Rep or Set                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckCMCfgRepOrSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                                 UINT4 u4Len)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    BOOLEAN             bRecdInternalIpAddress = FALSE;
    BOOLEAN             bRecdInternalIpNetMask = FALSE;
    BOOLEAN             bRecdInternalIpSubnet = FALSE;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckCMCfgRepOrSetAttributes:\
                         IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if (IkeCMCfgRepOrSetCheckAtts (pSesInfo, u4AttrType,
                                       u2Attr, u2BasicValue,
                                       au1VarAtts,
                                       (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckCMCfgRepOrSetAttributes: \
                         IkeCMCfgRepOrSetCheckAtts" "failed!\n");
            return IKE_FAILURE;
        }
        switch (u2Attr)
        {

            case IKE_INTERNAL_IP4_ADDRESS:
            case IKE_INTERNAL_IP6_ADDRESS:
                bRecdInternalIpAddress = TRUE;
                break;

            case IKE_INTERNAL_IP4_SUBNET:
                bRecdInternalIpSubnet = TRUE;
                break;

            case IKE_INTERNAL_IP4_NETMASK:
            case IKE_INTERNAL_IP6_NETMASK:
                bRecdInternalIpNetMask = TRUE;
                break;

            default:
                break;
        }

        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    /* IpAddress and Subnet Mask are expected as mandatory parameters from the
     * server 
     * IpSubnet is not treated as a mandatory parameter because certain servers
     * like Safenet donot send these values*/
    if ((bRecdInternalIpAddress != TRUE) || (bRecdInternalIpNetMask != TRUE))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCheckCMCfgRepOrSetAttributes: "
                     "Mandatory CM Attributes not received!\n");
        return IKE_FAILURE;
    }

    UNUSED_PARAM (bRecdInternalIpSubnet);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCheckXAuthCfgReqAttributes                      */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Req                                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckXAuthCfgReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                               UINT4 u4Len)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckCMCfgReqAttributes: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if (IkeXAuthCfgReqCheckAtts (pSesInfo, u4AttrType,
                                     u2Attr, u2BasicValue,
                                     au1VarAtts, (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckXAuthCfgReqAttributes:IkeXAuthCfgReqCheckAtts"
                         "failed!\n");
            return IKE_FAILURE;
        }
        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCheckXAuthCfgRepAttributes                      */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Rep                                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckXAuthCfgRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                               UINT4 u4Len)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckCMCfgReqAttributes: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if (IkeXAuthCfgRepCheckAtts (pSesInfo, u4AttrType,
                                     u2Attr, u2BasicValue,
                                     au1VarAtts, (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckXAuthCfgRepAttributes:IkeXAuthCfgRepCheckAtts"
                         "failed!\n");
            return IKE_FAILURE;
        }
        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCheckXAuthCfgSetAttributes                      */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Set                                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1Pd - The current payload to be processed.       */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckXAuthCfgSetAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                               UINT4 u4Len)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckXAuthCfgSetAttributes: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if (IkeXAuthCfgSetCheckAtts (pSesInfo, u4AttrType,
                                     u2Attr, u2BasicValue,
                                     au1VarAtts, (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckXAuthCfgSetAttributes:IkeXAuthCfgSetCheckAtts"
                         "failed!\n");
            return IKE_FAILURE;
        }
        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCMCfgRepOrSetCheckAtts                          */
/*  Description     :This functions is used to check the attributes of  */
/*                  :CM Cfg Rep or Set                                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u4AttrType : Indicates attribute type(Basic or VPI */
/*                  :u2Attr : Specifies the Attribute                   */
/*                  :u2Value : Value of the Attribute                   */
/*                  :pu1VarPtr: Stores the Variable attributes          */
/*                  : u2Len : Length of the attribute                   */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCMCfgRepOrSetCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                           UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                           UINT2 u2Len)
{
    UINT1               u1Count = IKE_ZERO;
    UINT1               u1Count1 = IKE_ZERO;

    IKE_UNUSED (u2Len);
    IKE_UNUSED (u4AttrType);
    IKE_UNUSED (u2Value);

    switch (u2Attr)
    {
        case IKE_INTERNAL_IP4_ADDRESS:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_ADDRESS;
            pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv4Addr =
                IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType = IPV4ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_NETMASK:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_NETMASK;
            pSessionInfo->IkeTransInfo.InternalMask.Ipv4Addr =
                IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->IkeTransInfo.InternalMask.u4AddrType = IPV4ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_SUBNET:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_SUBNET;
            pSessionInfo->IkeTransInfo.ProtectedNetwork[u1Count1].uNetwork.
                Ip4Subnet.Ip4Addr = IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->IkeTransInfo.ProtectedNetwork[u1Count1].uNetwork.
                Ip4Subnet.Ip4SubnetMask =
                IKE_NTOHL ((*(UINT4 *) (void *) (pu1VarPtr + IKE_FOUR)));
            pSessionInfo->IkeTransInfo.ProtectedNetwork[u1Count1].u4Type =
                IKE_IPSEC_ID_IPV4_SUBNET;

            u1Count++;
            u1Count1++;
            break;
        }
        case IKE_INTERNAL_IP6_ADDRESS:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP6_ADDRESS;
            IKE_MEMCPY (&(pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv6Addr),
                        pu1VarPtr, sizeof (tIp6Addr));
            pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType = IPV6ADDR;
            u1Count++;
            break;
        }
        case IKE_INTERNAL_IP6_NETMASK:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP6_NETMASK;
            IKE_MEMCPY (&(pSessionInfo->IkeTransInfo.InternalMask.Ipv6Addr),
                        pu1VarPtr, sizeof (tIp6Addr));
            pSessionInfo->IkeTransInfo.InternalMask.u4AddrType = IPV6ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_DNS:
        case IKE_INTERNAL_ADDRESS_EXPIRY:
        case IKE_INTERNAL_IP4_DHCP:
        case IKE_APPLICATION_VERSION:
        case IKE_INTERNAL_IP6_DNS:
        case IKE_INTERNAL_IP6_DHCP:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCMCfgReOrSetpCheckAtts: \
                         Unsupported Attributes Discarded \n");
            return IKE_SUCCESS;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCMCfgRepOrSetCheckAtts: \
                         Attribute not Supported\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeXAuthCfgReqCheckAtts                            */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Cfg Req                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u4AttrType : Indicates attribute type(Basic or VPI */
/*                  :u2Attr : Specifies the Attribute                   */
/*                  :u2Value : Value of the Attribute                   */
/*                  :pu1VarPtr: Stores the Variable attributes          */
/*                  : u2Len : Length of the attribute                   */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeXAuthCfgReqCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                         UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                         UINT2 u2Len)
{

    UINT1               u1Count = IKE_ZERO;

    IKE_UNUSED (u2Len);
    IKE_UNUSED (u4AttrType);
    IKE_UNUSED (pu1VarPtr);

    switch (u2Attr)
    {

        case IKE_XAUTH_TYPE:
        {
            if (u2Value != IKE_XAUTH_GENERIC)
            {

                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeXAuthCfgReqCheckAtts: Unsupported "
                             "Authentication Type\n");
                pSessionInfo->IkeTransInfo.bXAuthReqSuccess = FALSE;
                return IKE_SUCCESS;
            }
            pSessionInfo->IkeTransInfo.au2XAuthRecvAttr[u1Count] =
                IKE_XAUTH_TYPE;
            u1Count++;
            break;
        }

        case IKE_XAUTH_USER_NAME:
        {

            if (u2Len != IKE_ZERO)
            {

                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeXAuthCfgReqCheckAtts: Length of the "
                             "received attribute must be zero\n");
                return IKE_FAILURE;
            }

            pSessionInfo->IkeTransInfo.au2XAuthRecvAttr[u1Count] =
                IKE_XAUTH_USER_NAME;
            u1Count++;
            break;
        }

        case IKE_XAUTH_USER_PASSWORD:
        {

            if (u2Len != IKE_ZERO)
            {

                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeXAuthCfgReqCheckAtts: Length of the "
                             "received attribute must be zero\n");
                return IKE_FAILURE;
            }
            pSessionInfo->IkeTransInfo.au2XAuthRecvAttr[u1Count] =
                IKE_XAUTH_USER_PASSWORD;
            u1Count++;
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeXAuthCfgReqCheckAtts: Attribute not Supported\n");
            return IKE_FAILURE;
        }
    }
    pSessionInfo->IkeTransInfo.bXAuthReqSuccess = TRUE;
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeXAuthCfgRepCheckAtts                            */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Cfg Rep                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u4AttrType : Indicates attribute type(Basic or VPI */
/*                  :u2Attr : Specifies the Attribute                   */
/*                  :u2Value : Value of the Attribute                   */
/*                  :pu1VarPtr: Stores the Variable attributes          */
/*                  : u2Len : Length of the attribute                   */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeXAuthCfgRepCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                         UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                         UINT2 u2Len)
{

    IKE_UNUSED (u4AttrType);

    u2Len = (UINT2) ((u2Len >= MAX_NAME_LENGTH) ? MAX_NAME_LENGTH : u2Len);

    switch (u2Attr)
    {
        case IKE_XAUTH_TYPE:
        {
            break;
        }

        case IKE_XAUTH_STATUS:
        {
            if (u2Value == IKE_XAUTH_FAIL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeXAuthCfgRepCheckAtts: Received"
                             "XAUTH_STATUS attribute with value FAIL\n");
                pSessionInfo->IkeTransInfo.bUserAuthenticated = FALSE;
                return IKE_FAILURE;
            }
            break;
        }

        case IKE_XAUTH_USER_NAME:
        {
            MEMCPY (pSessionInfo->IkeTransInfo.au1XAuthRecvUsername, pu1VarPtr,
                    u2Len);
            break;
        }

        case IKE_XAUTH_USER_PASSWORD:
        {
            MEMCPY (pSessionInfo->IkeTransInfo.au1XAuthRecvPassword, pu1VarPtr,
                    u2Len);
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeXAuthCfgRepCheckAtts: Attribute not Supported\n");
            return IKE_FAILURE;
        }
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeXAuthCfgSetCheckAtts                            */
/*  Description     :This functions is used to check the attributes of  */
/*                  :XAuth Cfg Set                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u4AttrType : Indicates attribute type(Basic or VPI */
/*                  :u2Attr : Specifies the Attribute                   */
/*                  :u2Value : Value of the Attribute                   */
/*                  :pu1VarPtr: Stores the Variable attributes          */
/*                  : u2Len : Length of the attribute                   */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeXAuthCfgSetCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                         UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                         UINT2 u2Len)
{

    IKE_UNUSED (u4AttrType);
    IKE_UNUSED (pu1VarPtr);
    IKE_UNUSED (u2Len);

    switch (u2Attr)
    {

        case IKE_XAUTH_STATUS:
        {
            if (u2Value == IKE_XAUTH_FAIL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeXAuthCfgSetCheckAtts: Received"
                             "XAUTH_STATUS attribute with value FAIL\n");
                pSessionInfo->IkeTransInfo.bUserAuthenticated = FALSE;
            }
            else
            {
                pSessionInfo->IkeTransInfo.bUserAuthenticated = TRUE;
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeXAuthCfgSetCheckAtts: Attribute not Supported\n");
            return IKE_FAILURE;
        }
    }

    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessTransactionHash
 *  Description   : Function to process Transaction Hash 
 *  Input(s)      : pSesInfo - pointer to Trans Exch  data structure 
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT1
IkeProcessTransactionHash (tIkeSessionInfo * pSesInfo)
{
    UINT2               u2MesStrt = IKE_ZERO, u2PayLoadLen = IKE_ZERO;
    UINT1              *pu1BufPtr = NULL, *pu1PayLoad = NULL;
    UINT1               au1Digest[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tHashPayLoad        HashPayLoad;
    INT4                i4HashStrt = IKE_ZERO;
    tIkePayload        *pHashPayload = NULL;
    UINT4               u4MesgId = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4MesgId = IKE_HTONL (pSesInfo->u4MsgId);

    if (SLL_COUNT (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD])
        != IKE_ONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessTransactionHash:\
                     No or More than one Hash payload " "recd\n");
        return (IKE_FAILURE);
    }

    pHashPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_HASH_PAYLOAD]);
    if (pHashPayload != NULL)
    {
        pu1PayLoad = pHashPayload->pRawPayLoad;
        IKE_MEMCPY ((UINT1 *) &HashPayLoad, pu1PayLoad, IKE_HASH_PAYLOAD_SIZE);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessTransactionHash: No Hash payload recd\n");
        return (IKE_FAILURE);
    }

    if (pSesInfo->u1ExchangeType == IKE_TRANSACTION_MODE)
    {
        if (HashPayLoad.u1NextPayLoad != IKE_ATTRIBUTE_PAYLOAD)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessTransactionHash: Hash payload is not"
                         "followed by Attribute Payload\n");
            return (IKE_FAILURE);
        }
    }

    HashPayLoad.u2PayLoadLen = IKE_NTOHS (HashPayLoad.u2PayLoadLen);

    u4HashLen = gaIkeHashAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    i4HashStrt = sizeof (tIsakmpHdr) + sizeof (tHashPayLoad);

    if ((HashPayLoad.u2PayLoadLen - IKE_HASH_PAYLOAD_SIZE) != (UINT2) u4HashLen)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessTransactionHash: Digest Length Mismatch\n");
        return (IKE_FAILURE);
    }

    u2MesStrt = (UINT2) (sizeof (tIsakmpHdr) + sizeof (tHashPayLoad) +
                         u4HashLen);

    u2PayLoadLen = (UINT2) (pSesInfo->IkeRxPktInfo.u4PacketLen - u2MesStrt);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessTransactionHash: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1BufPtr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessTransactionHash: Memory Not Available\n");
        return IKE_FAILURE;
    }
    /* Calculation of Transaction Hash  in TM Mode */
    /* Key is SKEYID_a, and the buffer is *
     * M-ID | SA | ATTR */

    IKE_MEMCPY (pu1BufPtr, &u4MesgId, sizeof (UINT4));

    IKE_MEMCPY (pu1BufPtr + sizeof (UINT4),
                pSesInfo->IkeRxPktInfo.pu1RawPkt + u2MesStrt, u2PayLoadLen);

    (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSesInfo->pIkeSA->pSKeyIdA,
         (INT4) pSesInfo->pIkeSA->u2SKeyIdLen, pu1BufPtr,
         (INT4) (u2PayLoadLen + sizeof (UINT4)), au1Digest);

    /* Return  the resultant after Comparing the computed hash and the 
     * received hash */

    u4HashLen = (u4HashLen <= IKE_MAX_DIGEST_SIZE) ?
        u4HashLen : IKE_MAX_DIGEST_SIZE;

    if (IKE_MEMCMP
        (au1Digest, (UINT1 *) (pSesInfo->IkeRxPktInfo.pu1RawPkt + i4HashStrt),
         u4HashLen) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessTransactionHash: Digest Mismatch!\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkePreProcessTransPkt                              */
/*  Description     :This functions is used to identify whether the     */
/*                  :Received Pkt is XAuth or Config Mode Pkt           */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :                                                   */
/*  Output(s)       :Updates IsXAuth Field of  session data structure   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkePreProcessTransPkt (tIkeSessionInfo * pSesInfo)
{

    tAttributePayLoad   AttrPayLoad;
    tIkePayload        *pAttrPayLoad = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1              *pu1PayLoad = NULL;
    UINT1              *pu1TmpPtr = NULL;
    UINT1               u1PktType = IKE_ZERO;

    pAttrPayLoad = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.aPayload[IKE_ATTRIBUTE_PAYLOAD]);

    if (pAttrPayLoad == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeIsXAuth: AttrPayLoad not received\n");
        return (IKE_FAILURE);
    }

    pu1PayLoad = pAttrPayLoad->pRawPayLoad;

    IKE_MEMCPY ((UINT1 *) &AttrPayLoad, pu1PayLoad, IKE_ATTR_PAYLOAD_SIZE);

    AttrPayLoad.u2PayLoadLen = IKE_NTOHS (AttrPayLoad.u2PayLoadLen);
    u4Len = (UINT4) (AttrPayLoad.u2PayLoadLen - IKE_ATTR_PAYLOAD_SIZE);

    u1PktType = AttrPayLoad.u1Type;

    pu1TmpPtr = pu1PayLoad;
    pu1PayLoad = (pu1PayLoad + IKE_ATTR_PAYLOAD_SIZE);

    if (IkeCheckTransExchAttributes (pSesInfo, pu1PayLoad, u4Len, u1PktType) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeIsXAuth:" "IkeCheckTransExchAttributes Failed \n");
        return (IKE_FAILURE);
    }
    pu1PayLoad = pu1TmpPtr;

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCheckTransExchAttributes                        */
/*  Description     :This functions is used to check the Trans Exch     */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pu1PayLoad : Pointer to the PayLoad                */
/*                  :u4Len : The Length of the attribute data to be     */
/*                  :processed                                          */
/*                  :                                                   */
/*  Output(s)       :Updates IsXAuth Field of  session data structure   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeCheckTransExchAttributes (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                             UINT4 u4Len, UINT1 u1PktType)
{

    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO, u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckTransExchAttributes: IkeGetAtts failed!\n");
            return IKE_FAILURE;
        }

        if (IkeTransExchCheckAtts (pSesInfo, u2Attr, u1PktType) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckTransExchAttributes:IkeTransExchCheckAtts"
                         "failed!\n");
            return IKE_FAILURE;
        }
        u4Count =
            (UINT4) (u4Count + sizeof (tSaBasicAttribute) + (UINT2) i4Len);
        pu1PayLoad = pu1PayLoad + sizeof (tSaBasicAttribute) + i4Len;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeTransExchCheckAttributes                        */
/*  Description     :This functions is used to check the Trans Exch     */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :u4AttrType : Indicates attribute type(Basic or VPI */
/*                  :u2Attr : Specifies the Attribute                   */
/*                  :u2Value : Value of the Attribute                   */
/*                  :pu1VarPtr: Stores the Variable attributes          */
/*                  : u2Len : Length of the attribute                   */
/*                  :                                                   */
/*  Output(s)       :Updates IsXAuth Field of  session data structure   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeTransExchCheckAtts (tIkeSessionInfo * pSessionInfo, UINT2 u2Attr,
                       UINT1 u1PktType)
{

    BOOLEAN             IsXAuthPkt = FALSE;

    switch (u2Attr)
    {

        case IKE_XAUTH_STATUS:
        case IKE_XAUTH_TYPE:
        case IKE_XAUTH_USER_PASSWORD:
        case IKE_XAUTH_USER_NAME:
        case IKE_XAUTH_PASSCODE:
        case IKE_XAUTH_MESSAGE:
        case IKE_XAUTH_CHALLENGE:
        case IKE_XAUTH_DOMAIN:
            IsXAuthPkt = TRUE;
            break;
        default:
            break;
    }

    switch (u1PktType)
    {
        case IKE_ISAKMP_CFG_REQUEST:
        {
            if (!(FALSE == IsXAuthPkt))
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_XAUTH_REQ_PKT;
            }
            else
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_CM_REQ_PKT;

            }
            break;
        }
        case IKE_ISAKMP_CFG_REPLY:
        {
            if (!(FALSE == IsXAuthPkt))
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_XAUTH_REP_PKT;

            }
            else
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_CM_REP_PKT;

            }
            break;
        }
        case IKE_ISAKMP_CFG_SET:
        {
            if (!(FALSE == IsXAuthPkt))
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_XAUTH_SET_PKT;

            }
            else
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_CM_SET_PKT;

            }
            break;
        }
        case IKE_ISAKMP_CFG_ACK:
        {
            if (!(FALSE == IsXAuthPkt))
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_XAUTH_ACK_PKT;

            }
            else
            {
                pSessionInfo->IkeTransInfo.u1TransPktType = IKE_CM_ACK_PKT;

            }
            break;
        }
        default:
        {
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSetId                                           */
/*  Description     :This function is used to set the Key ID in IKE     */
/*                  : Phase1 Session info database                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1ID : The current ID to be processed        */
/*                  :                                                   */
/*  Output(s)       :Set the Id                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeSetId (tIkeSessionInfo * pSesInfo, UINT1 *pu1ID, UINT1 *pu1IdType)
{
    tIkeKey            *pIkeKey = NULL;
    tIkePolicy         *pIkePolicy = NULL;
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetId:Engine not found\n");
        return (IKE_FAILURE);
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (*pu1IdType == (UINT1) pIkePolicy->PolicyPeerID.i4IdType)
        {
            if (IKE_MEMCMP (pu1ID,
                            &pIkePolicy->PolicyPeerID.uID,
                            pIkePolicy->PolicyPeerID.u4Length) == IKE_ZERO)
            {
                break;
            }
        }
    }

    if (pIkePolicy == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetId:Policy not found\n");
        return (IKE_FAILURE);
    }

    IKE_MEMCPY (&pSesInfo->IkePhase1Policy, pIkePolicy, sizeof (tIkePolicy));

    GIVE_IKE_SEM ();

    pSesInfo->pIkeSA->u2AuthMethod = pSesInfo->IkePhase1Policy.u2AuthMethod;

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE_PRESHARED_KEY)
    {
        if (pSesInfo->pIkeSA->ResponderID.i4IdType == IKE_NONE)
        {

            /* If Responder ID is not set, do a look-up in the key table, */
            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
            {
                if (pIkeKey->u1Status == IKE_ACTIVE)
                {
                    if (*pu1IdType == (UINT1) pIkeKey->KeyID.i4IdType)
                    {
                        if (IKE_MEMCMP (&pIkeKey->KeyID.uID,
                                        pu1ID, pIkeKey->KeyID.u4Length)
                            == IKE_ZERO)
                        {
                            break;
                        }
                    }
                }
            }

            GIVE_IKE_SEM ();

            if (pIkeKey != NULL)
            {
                /* Get the Pre-shared Key */
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pSesInfo->IkePhase1Info.
                     pu1PreSharedKey) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetId: unable to allocate " "buffer\n");
                    return IKE_FAILURE;
                }
                if (pSesInfo->IkePhase1Info.pu1PreSharedKey == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSetId:Malloc fails\n");
                    return (IKE_FAILURE);
                }

                STRCPY (pSesInfo->IkePhase1Info.pu1PreSharedKey,
                        pIkeKey->au1KeyString);
                pSesInfo->IkePhase1Info.u2PreSharedKeyLen =
                    (UINT2) STRLEN (pIkeKey->au1KeyString);

                /* Fill in the Responder ID */
                pSesInfo->pIkeSA->ResponderID.i4IdType =
                    pIkeKey->KeyID.i4IdType;
                pSesInfo->pIkeSA->ResponderID.u4Length =
                    pIkeKey->KeyID.u4Length;
                IKE_MEMCPY (&pSesInfo->pIkeSA->ResponderID.uID,
                            &pIkeKey->KeyID.uID, pIkeKey->KeyID.u4Length);
            }
            else
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_ID_INFO);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetId:Invalid ID recd\n");
                return (IKE_FAILURE);
            }
        }
    }

    if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_1)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo768;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_2)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo1024;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_5)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo1536;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == SEC_DH_GROUP_14)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo2048;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeProcessNatDPayLoad                              */
/*  Description     :This function is used to Process Nat Detect Payload*/
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to session info data base        */
/*                  :pu1PayLoad : Pointer to current payload            */
/*                  :u1NumOfNatdPayLoad : Position of NatD Payload, if  */
/*                  :                     This value is zero, then it is*/
/*                  :                     NatD Dest or it is NATD Source*/
/*                  :                     Payload                       */
/*                  :                                                   */
/*  Output(s)       :Nonw                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS                                        */
/************************************************************************/

INT1
IkeProcessNatDPayLoad (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                       UINT1 u1NumOfNatdPayLoad)
{
    tVendorIdPayLoad    IkeNatD;
    UINT1               au1NatDHash[IKE_SHA512_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN]
        = { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Len = IKE_ZERO;

    IKE_BZERO (au1NatdData, (IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                             IKE_IP6_ADDR_LEN + IKE_PORT_LEN));
    IKE_BZERO (au1NatDHash, IKE_SHA512_LENGTH);
    IKE_BZERO (&IkeNatD, sizeof (tNattDetectPayLoad));
    IKE_MEMCPY ((UINT1 *) &IkeNatD, pu1PayLoad, IKE_NATD_PAYLOAD_SIZE);
    IkeNatD.u2PayLoadLen = IKE_NTOHS (IkeNatD.u2PayLoadLen);
    if (IkeNatD.u2PayLoadLen == IKE_NATD_PAYLOAD_SIZE)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeProcessNatDPayLoad: Invalid Len Ignoring!\n");
        /* We will not fail the negotiation because of this */
        return IKE_SUCCESS;
    }
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0], pSesInfo->pIkeSA->au1InitiatorCookie,
                IKE_COOKIE_LEN);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN,
                pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    u2Len = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);

    if (u1NumOfNatdPayLoad != IKE_ZERO)
    {
        if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
        {
            u4IpAddr = pSesInfo->pIkeSA->IpPeerAddr.Ipv4Addr;
            u4IpAddr = IKE_HTONL (u4IpAddr);
            IKE_MEMCPY (&au1NatdData[IKE_INDEX_0]
                        + u2Len, &u4IpAddr, IKE_IP4_ADDR_LEN);
            u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
        }
        else
        {
            IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len,
                        &(pSesInfo->pIkeSA->IpPeerAddr.Ipv6Addr),
                        IKE_IP6_ADDR_LEN);
            u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
        }
        u2IkePort = IKE_HTONS (pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort);
    }
    else
    {
        if (pSesInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV4ADDR)
        {
            u4IpAddr = pSesInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
            u4IpAddr = IKE_HTONL (u4IpAddr);
            IKE_MEMCPY (&au1NatdData[IKE_INDEX_0]
                        + u2Len, &u4IpAddr, IKE_IP4_ADDR_LEN);
            u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
        }
        else
        {
            IKE_MEMCPY (&au1NatdData[IKE_INDEX_0]
                        + u2Len,
                        &(pSesInfo->pIkeSA->IpLocalAddr.Ipv6Addr),
                        IKE_IP6_ADDR_LEN);
            u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
        }
        u2IkePort = IKE_HTONS (u2IkePort);
    }
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len, &u2IkePort, IKE_PORT_LEN);
    u2Len = (UINT2) (u2Len + IKE_PORT_LEN);

    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_MD5)
    {
        IkeMd5HashAlgo (au1NatdData, u2Len, au1NatDHash);
        if (IKE_MEMCMP (au1NatDHash, pu1PayLoad + IKE_NATD_PAYLOAD_SIZE,
                        IKE_MD5_LENGTH) != IKE_ZERO)
        {
            if (u1NumOfNatdPayLoad != IKE_ZERO)
            {
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_PEER_BEHIND_NAT;
            /* Use NATT port as IKE_PEER_BEHIND_NAT flag is set*/
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
            else
            {
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
        }
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE_SHA1)
    {
        IkeSha1HashAlgo (au1NatdData, u2Len, au1NatDHash);
        if (IKE_MEMCMP (au1NatDHash, pu1PayLoad + IKE_NATD_PAYLOAD_SIZE,
                        IKE_SHA_LENGTH) != IKE_ZERO)
        {
            if (u1NumOfNatdPayLoad != IKE_ZERO)
            {
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_PEER_BEHIND_NAT;
            /* Use NATT port as IKE_PEER_BEHIND_NAT flag is set*/
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
            else
            {
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
        }
    }

    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_256)
    {
        IkeSha256HashAlgo (au1NatdData, u2Len, au1NatDHash);
        if (IKE_MEMCMP (au1NatDHash, pu1PayLoad + IKE_NATD_PAYLOAD_SIZE,
                        IKE_SHA256_LENGTH) != IKE_ZERO)
        {

            if (u1NumOfNatdPayLoad != IKE_ZERO)
            {
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_PEER_BEHIND_NAT;
            /* Use NATT port as IKE_PEER_BEHIND_NAT flag is set*/
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
            else
            {
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
        }
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_384)
    {
        IkeSha384HashAlgo (au1NatdData, u2Len, au1NatDHash);
        if (IKE_MEMCMP (au1NatDHash, pu1PayLoad + IKE_NATD_PAYLOAD_SIZE,
                        IKE_SHA384_LENGTH) != IKE_ZERO)
        {
            if (u1NumOfNatdPayLoad != IKE_ZERO)
            {
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_PEER_BEHIND_NAT;
            /* Use NATT port as IKE_PEER_BEHIND_NAT flag is set*/
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
            else
            {
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
        }
    }
    if (pSesInfo->pIkeSA->u1Phase1HashAlgo == HMAC_SHA_512)
    {
        IkeSha512HashAlgo (au1NatdData, u2Len, au1NatDHash);
        if (IKE_MEMCMP (au1NatDHash, pu1PayLoad + IKE_NATD_PAYLOAD_SIZE,
                        IKE_SHA512_LENGTH) != IKE_ZERO)
        {
            if (u1NumOfNatdPayLoad != IKE_ZERO)
            {
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_PEER_BEHIND_NAT;
            /* Use NATT port as IKE_PEER_BEHIND_NAT flag is set*/
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
            else
            {
                if (!
                    (pSesInfo->pIkeSA->IkeNattInfo.
                     u1NattFlags & IKE_USE_NATT_PORT))
                {
                    pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort =
                        IKE_NATT_UDP_PORT;
                }
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                    pSesInfo->pIkeSA->IkeNattInfo.
                    u1NattFlags | IKE_USE_NATT_PORT;
            }
        }
    }
    return IKE_SUCCESS;
}
