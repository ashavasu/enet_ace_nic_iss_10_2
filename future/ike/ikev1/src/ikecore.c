/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ikecore.c,v 1.11 2014/04/23 12:21:38 siva Exp $
 *
 * Description: This has functions for IKE core SubModule
 *
 ***********************************************************************/

#include "ikegen.h"
#include "iketdfs.h"
#include "fsike.h"
#include "fssocket.h"
#include "utilalgo.h"
#include "ikememmac.h"

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDecryptPayLoad 
 *  Description   : Function to decrypt the ISAKMP payload.
 *  Input(s)      : pSessionInf- Session Info Pointer.
 *                  u4Len - Length of the data to be decrypted.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeDecryptPayLoad (tIkeSessionInfo * pSessionInfo)
{
    UINT1              *pLocalPtr =
        pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr);
    UINT4               u4Len = IKE_ZERO;
    unUtilAlgo          UtilAlgo;
    unArCryptoKey       ArCryptoKey;

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    u4Len = IKE_NTOHL
        (((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt))->
         u4TotalLen);
    if ((pSessionInfo->pIkeSA->pSKeyIdE == NULL)
        || (pSessionInfo->pu1CryptIV == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDecryptPayLoad: Key Not Available\n");
        return IKE_FAILURE;
    }

    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pLocalPtr;
    UtilAlgo.UtilDesAlgo.u4DesInBufSize = (u4Len - sizeof (tIsakmpHdr));
    UtilAlgo.UtilDesAlgo.pu1DesInitVect = pSessionInfo->pu1CryptIV;
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pSessionInfo->pIkeSA->pSKeyIdE;
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
        (pSessionInfo->pIkeSA->pSKeyIdE + IKE_DES_KEY_LEN);
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
        (pSessionInfo->pIkeSA->pSKeyIdE + IKE_TWO * IKE_DES_KEY_LEN);
    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
        (UINT4) pSessionInfo->pIkeSA->u2EncrKLen;
    UtilAlgo.UtilDesAlgo.u4DesInitVectLen =
        gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
    switch (pSessionInfo->pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE_DES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }

            IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));

            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
            UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            break;
        }

        case IKE_3DES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }

            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));
            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey2, sizeof (tDesKey));
            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey3, sizeof (tDesKey));

            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

            UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            break;
        }
        case IKE_AES:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                pSessionInfo->pIkeSA->pSKeyIdE;
            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pLocalPtr;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = (u4Len - sizeof (tIsakmpHdr));
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                         IKE_EIGHT);
            UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                pSessionInfo->pIkeSA->au1AesDecrKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector = pSessionInfo->pu1CryptIV;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
                u4BlockLen;
            UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;

            /* Decryption Key */
            UtilDecrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo);
            break;

        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeDecryptPayLoad:Encryption Algo unknown/not "
                         "supported\n");
            return IKE_FAILURE;
        }
    }

    if (pSessionInfo->pIkeSA->u4LifeTimeKB != IKE_ZERO)
    {
        pSessionInfo->pIkeSA->u4ByteCount += (u4Len - sizeof (tIsakmpHdr));

        /* Check if soft lifetime expired */
        if (pSessionInfo->pIkeSA->u4ByteCount >
            pSessionInfo->pIkeSA->u4SoftLifeTimeBytes)
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to rekey */
            if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_REKEY_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pSessionInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDecryptPayLoad: IkeStartTimer failed!\n");
                return (IKE_FAILURE);
            }
        }

        /* If Hard Lifetime expires... */
        if (pSessionInfo->pIkeSA->u4ByteCount >
            (pSessionInfo->pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB))
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to delete SA */
            if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pSessionInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDecryptPayLoad: IkeStartTimer failed!\n");
                return (IKE_FAILURE);
            }
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeEncryptPayLoad
 *  Description   : Fucntion the encrypt the ISAKMP payload 
 *  Input(s)      : pSessionInfo - Session Info Pointer.
 *                  u4Len - length of the data to be cncrypted.
 *  Output(s)     : None. 
 *  Return Values : IKE_SUCCESS or IKE_FAILURE.
 * ---------------------------------------------------------------
 */

INT1
IkeEncryptPayLoad (tIkeSessionInfo * pSessionInfo)
{
    UINT1              *pLocalPtr = NULL;
    UINT4               u4PadLen = IKE_ZERO, u4Temp = IKE_ZERO,
        u4Len = IKE_ZERO;
    unUtilAlgo          UtilAlgo;
    unArCryptoKey       ArCryptoKey;

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    if (pSessionInfo->pu1CryptIV == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeEncryptPayLoad:IV is NULL!\n");
        return IKE_FAILURE;
    }

    u4Len = IKE_NTOHL
        (((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
         u4TotalLen);
    u4Temp =
        (u4Len -
         sizeof (tIsakmpHdr)) %
        gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    if (u4Temp != IKE_ZERO)
    {
        u4PadLen = gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
            u4BlockLen - u4Temp;
        if (ConstructIsakmpMesg (pSessionInfo, (INT4) u4Len, u4PadLen) ==
            IKE_FAILURE)
        {
            return IKE_FAILURE;
        }

        u4Len += u4PadLen;
        ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
            u4TotalLen = IKE_HTONL (u4Len);
    }

    pLocalPtr = pSessionInfo->IkeTxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr);
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pSessionInfo->pIkeSA->pSKeyIdE;
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
        (pSessionInfo->pIkeSA->pSKeyIdE + IKE_DES_KEY_LEN);
    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
        (pSessionInfo->pIkeSA->pSKeyIdE + IKE_TWO * IKE_DES_KEY_LEN);
    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
        (UINT4) pSessionInfo->pIkeSA->u2EncrKLen;
    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pLocalPtr;
    UtilAlgo.UtilDesAlgo.u4DesInBufSize = (u4Len - sizeof (tIsakmpHdr));
    UtilAlgo.UtilDesAlgo.pu1DesInitVect = pSessionInfo->pu1CryptIV;
    UtilAlgo.UtilDesAlgo.u4DesInitVectLen =
        gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
    switch (pSessionInfo->pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE_DES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }
            IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));

            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

            UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            break;
        }

        case IKE_3DES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }
            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));
            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey2, sizeof (tDesKey));
            IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                        pSessionInfo->pIkeSA->DesKey3, sizeof (tDesKey));

            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

            UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            break;
        }

        case IKE_AES:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                pSessionInfo->pIkeSA->pSKeyIdE;
            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pLocalPtr;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = (u4Len - sizeof (tIsakmpHdr));
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                         IKE_EIGHT);
            UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                pSessionInfo->pIkeSA->au1AesEncrKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector = pSessionInfo->pu1CryptIV;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
                u4BlockLen;
            UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;

            UtilEncrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo);
            break;

        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeEncryptPayLoad:Encryption Algo unknown/not "
                         "supported\n");
            return IKE_FAILURE;
        }
    }

    if (pSessionInfo->pIkeSA->u4LifeTimeKB != IKE_ZERO)
    {
        pSessionInfo->pIkeSA->u4ByteCount += (u4Len - sizeof (tIsakmpHdr));

        /* Check if soft lifetime expired */
        if (pSessionInfo->pIkeSA->u4ByteCount >
            pSessionInfo->pIkeSA->u4SoftLifeTimeBytes)
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to rekey */
            if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_REKEY_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pSessionInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeEncryptPayLoad: IkeStartTimer failed!\n");
                return (IKE_FAILURE);
            }
        }

        /* If Hard Lifetime expires... */
        if (pSessionInfo->pIkeSA->u4ByteCount >
            (pSessionInfo->pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB))
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to delete SA */
            if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pSessionInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeEncryptPayLoad: IkeStartTimer failed!\n");
                return (IKE_FAILURE);
            }
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeCoreProcess 
 *  Description   : This will be called from ike main mode to proces
 *                  the incomming request and construct response.
 *  Input(s)      : pSessionInfo - session info pointer. 
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT1
IkeCoreProcess (tIkeSessionInfo * pSessionInfo)
{
    tIsakmpHdr         *pIsakmpHdr = NULL;
    INT1                i1Return = IKE_SUCCESS;
    UINT4               u4TotalLen = IKE_ZERO;
    UINT4               u4IVLen = IKE_ZERO;
    UINT1              *pu1IV = NULL;
    BOOLEAN             bFlag, bFlag1;
    CHR1               *pu1PeerAddrStr = NULL;

    /* Set Error Status as No Error */
    IKE_SET_ERROR (pSessionInfo, IKE_NONE);

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        /* Read Isakmp Header from Packet */
        pIsakmpHdr =
            (tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        u4TotalLen = IKE_NTOHL (pIsakmpHdr->u4TotalLen);
        bFlag1 = pIsakmpHdr->u1Flags & IKE_ENCRYPT_MASK;
        if (bFlag1 != IKE_ZERO)
        {
            /* Make a copy of the IV before decrypting in case
               the received packet is invalid
             */
            if (pSessionInfo->pu1CryptIV == NULL)
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeCoreProcess: IV not present! for peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }

            u4IVLen = gaIkeCipherAlgoList
                [pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pu1IV) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCoreProcess: unable to allocate " "buffer\n");
                return (IKE_FAILURE);
            }
            if (pu1IV == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCoreProcess: memory allocation failed!\n");
                return IKE_FAILURE;
            }
            IKE_MEMCPY (pu1IV, pSessionInfo->pu1CryptIV, u4IVLen);

            if (IkeDecryptPayLoad (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeCoreProcess: \
                             IkeDecryptPayLoad failed! for peer: %s\n", pu1PeerAddrStr);
                /* Restore the IV */
                IKE_MEMCPY (pSessionInfo->pu1CryptIV, pu1IV, u4IVLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IV);
                pu1IV = NULL;
                return IKE_FAILURE;
            }
        }

        /* validate isakmp packet */
        if (IkeValidatePacket (pSessionInfo, u4TotalLen,
                               pIsakmpHdr->u1NextPayload,
                               (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                sizeof (tIsakmpHdr))) == IKE_FAILURE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeCoreProcess: \
                         IkeValidatePacket failed for peer: %s\n", pu1PeerAddrStr);
            if (pu1IV != NULL)
            {
                /* Restore the IV */
                IKE_MEMCPY (pSessionInfo->pu1CryptIV, pu1IV, u4IVLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IV);
                pu1IV = NULL;
            }

            /* Send Notify Message */
            if (pSessionInfo->u2NotifyType != IKE_NONE)
            {
                if (IkeSendNotifyInfo (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "IkeCoreProcess:\
                                 IkeSendNotifyInfo failed for peer: %s\n", pu1PeerAddrStr);
                    return IKE_FAILURE;
                }
            }
            return IKE_FAILURE;
        }

        /* Stop the timer and decrement the ref count */
        IkeStopTimer (&pSessionInfo->IkeRetransTimer);

        /* Packet valid, no need to retain previous IV */
        if (pu1IV != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IV);
            pu1IV = NULL;
        }
    }

    /* Validation success, Release the previous transmitted packet */
    if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->IkeTxPktInfo.pu1RawPkt);
        pSessionInfo->IkeTxPktInfo.pu1RawPkt = NULL;
    }

    IKE_BZERO (&pSessionInfo->IkeTxPktInfo, sizeof (tIkePacket));

    switch (pSessionInfo->u1ExchangeType)
    {
        case IKE_MAIN_MODE:
            if (NULL != pIsakmpHdr)
            {
                i1Return = IkeMainMode (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1Return = IkeMainMode (pSessionInfo, NULL);
            }
            if (i1Return == IKE_FAILURE)
            {
                /*Increment statistics for phase1 session failed */
                IncIkeNoOfPhase1SessionsFailed ();
            }
            break;

        case IKE_AGGRESSIVE_MODE:
            if (NULL != pIsakmpHdr)
            {
                i1Return = IkeAggressiveMode (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1Return = IkeAggressiveMode (pSessionInfo, NULL);
            }

            if (i1Return == IKE_FAILURE)
            {
                /*Increment statistics for phase1 session failed */
                IncIkeNoOfPhase1SessionsFailed ();
            }
            break;

        case IKE_QUICK_MODE:
            i1Return = IkeQuickMode (pSessionInfo);
            if (i1Return == IKE_FAILURE)
            {
                /*Increment statistics for phase2 session failed */
                IncIkeNoOfSessionsFail (&pSessionInfo->RemoteTunnelTermAddr);
            }
            break;

        case IKE_ISAKMP_INFO:
            i1Return = IkeInformationExchMode (pSessionInfo);
            break;

        case IKE_TRANSACTION_MODE:
            i1Return = IkeTransactionExchMode (pSessionInfo);
            break;

        default:
            IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeCoreProcess: \
                         Invalid Exchange type: %d for peer: %s\n", pSessionInfo->u1ExchangeType, pu1PeerAddrStr);
            return IKE_FAILURE;
    }

    if (i1Return == IKE_FAILURE)
    {
        /* If the Send Notify Flag is set - Send the
         * NOTIFY message */
        if (pSessionInfo->u2NotifyType != IKE_NONE)
        {
            IkeSendNotifyInfo (pSessionInfo);
        }

        /* Mark the session to be deleted */
        if (pIsakmpHdr != NULL)
        {
            bFlag = IKE_TEST_ENCR_MASK (pIsakmpHdr);
            if (bFlag == IKE_TRUE)
            {
                pSessionInfo->bDelSession = IKE_TRUE;
                DecIkeActiveSessions (&pSessionInfo->pIkeSA->IpPeerAddr);
            }
        }
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeMainMode 
 *  Description   : Called from Ike main to handle main mode 
 *                  packets.
 *  Input(s)      : pSessionInfo - Session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeMainMode (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT4               u4Len = sizeof (tIsakmpHdr);
    BOOLEAN             bSendCR = IKE_FALSE;
    UINT1               u1NxtPayload = IKE_NONE_PAYLOAD;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE_MM_NO_STATE:
            u1NxtPayload = IKE_VENDORID_PAYLOAD;
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                 sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: MM_NO_STATE "
                                 "IkeProcess Failed\n");
                    return IKE_FAILURE;
                }
                if ((pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                     IKE_PEER_SUPPORT_NATT) == IKE_ZERO)
                {
                    u1NxtPayload = IKE_NONE_PAYLOAD;
                }
            }
            /* Constuct ISAKMP Header */
            if (IkeConstructHeader (pSessionInfo, IKE_MAIN_MODE,
                                    IKE_SA_PAYLOAD) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: MM_NO_STATE "
                             "IkeConstructHeader Failed\n");
                return IKE_FAILURE;
            }

            if (IkeConstructIsakmpSA (pSessionInfo, u1NxtPayload,
                                      &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: MM_NO_STATE "
                             "IkeConstructIsakmpSA Failed\n");
                return IKE_FAILURE;
            }
            if (u1NxtPayload == IKE_VENDORID_PAYLOAD)
            {
                if (IkeConstructVendorIdPayload (pSessionInfo, IKE_NONE_PAYLOAD,
                                                 &u4Len,
                                                 IKE_NATT_SUPPORT_VENDOR_ID) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: MM_NO_STATE "
                                 "IkeConstructVendorIdPayload Failed\n");
                    return IKE_FAILURE;
                }
            }

            pSessionInfo->u1FsmState = IKE_MM_SA_SETUP;

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
            if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                               IKE_RETRANS_TIMER_ID, IKE_RETRANSMIT_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure
                 */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: MM_NO_STATE "
                             "IkeStartTimer Failed\n");
            }
            break;

        case IKE_MM_SA_SETUP:
            if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                            (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                             sizeof (tIsakmpHdr))) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_SA_SETUP "
                             "IkeProcess Failed\n");
                return IKE_FAILURE;
            }

            /* Constuct ISAKMP Header */

            if (IkeConstructHeader (pSessionInfo, IKE_MAIN_MODE,
                                    IKE_KEY_PAYLOAD) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_SA_SETUP "
                             "IkeConstructHeader Failed\n");
                return IKE_FAILURE;
            }

            /* Construct Key PayLoad */

            if (IkeConstructKE (pSessionInfo, IKE_NONCE_PAYLOAD,
                                &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_SA_SETUP "
                             "IkeConstructKE Failed\n");
                return IKE_FAILURE;
            }

            if ((pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_RSA_SIGNATURE) ||
                (pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_DSS_SIGNATURE))
            {
                /* check whether cert request is required to send */
                if (IkeCheckPeerCertDB (pSessionInfo) == IKE_FALSE)
                {
                    bSendCR = IKE_TRUE;
                }
            }

            if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                IKE_PEER_SUPPORT_NATT)
            {
                u1NxtPayload = IKE_NAT_DETECT_PAYLOADS;
            }
            else
            {
                u1NxtPayload =
                    ((bSendCR == IKE_TRUE) ? IKE_CR_PAYLOAD : IKE_NONE_PAYLOAD);
            }

            /* Construct Nonce PayLoad */
            if (IkeConstructNonce (pSessionInfo, u1NxtPayload, &u4Len)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_SA_SETUP "
                             "IkeConstructNonce Failed\n");
                return IKE_FAILURE;
            }

            if (u1NxtPayload == IKE_NAT_DETECT_PAYLOADS)
            {
                u1NxtPayload =
                    ((bSendCR == IKE_TRUE) ? IKE_CR_PAYLOAD : IKE_NONE_PAYLOAD);
                if (IkeConstructNatdPayLoad (pSessionInfo,
                                             u1NxtPayload,
                                             &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_SA_SETUP "
                                 "IkeConstructNatDPayload Failed\n");
                    return IKE_FAILURE;
                }
            }

            /* Construct Certificate request PayLoad */
            if (bSendCR == IKE_TRUE)
            {
                if (IkeConstructCertRequest
                    (pSessionInfo, IKE_NONE_PAYLOAD, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_SA_SETUP "
                                 "IkeConstructCertRequest Failed\n");
                    return IKE_FAILURE;
                }
            }

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (IkeGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_SA_SETUP "
                                 "IkeGenSkeyId Failed\n");
                    return IKE_FAILURE;
                }
            }

            pSessionInfo->u1FsmState = IKE_MM_KEY_EXCH;

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
            if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                               IKE_RETRANS_TIMER_ID, IKE_RETRANSMIT_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure
                 */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: MM_SA_SETUP "
                             "IkeStartTimer Failed\n");
            }

            break;

        case IKE_MM_KEY_EXCH:
            if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                            (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                             sizeof (tIsakmpHdr))) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_KEY_EXCH "
                             "IkeProcess Failed\n");
                return IKE_FAILURE;
            }

            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                if (IkeGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_KEY_EXCH "
                                 "IkeGenSkeyId Failed\n");
                    return IKE_FAILURE;
                }
            }

            /* Construct ISAKMP Header */
            if (IkeConstructHeader (pSessionInfo, IKE_MAIN_MODE,
                                    IKE_ID_PAYLOAD) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_KEY_EXCH "
                             "IkeConstructHeader Failed\n");
                return IKE_FAILURE;
            }

            u1NxtPayload = IKE_HASH_PAYLOAD;

            if ((pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_RSA_SIGNATURE) ||
                (pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_DSS_SIGNATURE))
            {
                if (IkeCheckSendingCertPayload (pSessionInfo) == IKE_TRUE)
                {
                    u1NxtPayload = IKE_CERT_PAYLOAD;
                }
                else
                {
                    u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
                }
            }

            /* Construct ID PayLoad */
            if (IkeConstructID (pSessionInfo, u1NxtPayload,
                                &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeMainMode: State MM_KEY_EXCH "
                             "IkeConstructID Failed\n");
                return IKE_FAILURE;
            }

            if (u1NxtPayload == IKE_CERT_PAYLOAD)
            {
                /* Construct certificate PayLoad */
                if (IkeConstructCertificate
                    (pSessionInfo, IKE_SIGNATURE_PAYLOAD,
                     &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_KEY_EXCH "
                                 "IkeConstructCertificate Failed\n");
                    return IKE_FAILURE;
                }
                u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
            }

            if (u1NxtPayload == IKE_SIGNATURE_PAYLOAD)
            {
                /* Construct signature PayLoad */
                if (IkeConstructSignature
                    (pSessionInfo, IKE_NONE_PAYLOAD, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_KEY_EXCH "
                                 "IkeConstructSignature Failed\n");
                    return IKE_FAILURE;
                }
            }
            else
            {
                /* Construct Hash PayLoad */
                if (IkeConstructHash
                    (pSessionInfo, IKE_NONE_PAYLOAD, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_KEY_EXCH "
                                 "IkeConstructHash Failed\n");
                    return IKE_FAILURE;
                }
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                pSessionInfo->u1FsmState = IKE_MM_AUTH_EXCH;
                pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
                if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                   IKE_RETRANS_TIMER_ID,
                                   IKE_RETRANSMIT_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* This is not critical error, so just log and don't  
                       return failure */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_KEY_EXCH "
                                 "IkeStartTimer Failed\n");
                }
            }
            else
            {
                pSessionInfo->bPhase1Done = IKE_TRUE;
            }

            IKE_SET_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            break;

        case IKE_MM_AUTH_EXCH:
            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                 sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeMainMode: State MM_AUTH_EXCH "
                                 "IkeProcess Failed\n");
                    return IKE_FAILURE;
                }

                /* Mark Phase1 as done */
                pSessionInfo->bPhase1Done = IKE_TRUE;
            }
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeMainMode:Unknown MM state\n");
            return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeAggressiveMode 
 *  Description   : Called from Ike main to handle aggressive
 *                  mode packets.
 *  Input(s)      : pSessionInfo - Session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeAggressiveMode (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT1               u1NxtPayload = IKE_ZERO;
    BOOLEAN             bSendCR = IKE_FALSE;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE_AG_NO_STATE:
        {
            UINT1               au1ID[MAX_NAME_LENGTH] = { IKE_ZERO };
            UINT1               u1IdType = IKE_ZERO;

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                IKE_BZERO (au1ID, sizeof (au1ID));

                if (IkeGetIDFromPacket (au1ID, &u1IdType,
                                        (UINT1 *) pIsakmpHdr) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "Unable to Get peer ID from packet\n");
                    return IKE_FAILURE;
                }

                if ((u1IdType != IPV4ADDR) && (u1IdType != IPV6ADDR))
                {
                    if (IkeSetId (pSessionInfo, au1ID, &u1IdType) ==
                        IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeAggressiveMode: AG_NO_STATE "
                                     "IkeSetId Failed\n");
                        return IKE_FAILURE;
                    }
                }
                if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                 sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeProcess Failed\n");
                    return IKE_FAILURE;
                }
            }

            /* Construct ISAKMP Header */
            if (IkeConstructHeader (pSessionInfo, IKE_AGGRESSIVE_MODE,
                                    IKE_SA_PAYLOAD) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_NO_STATE "
                             "IkeConstructHeader Failed\n");
                return IKE_FAILURE;
            }

            /* Construct ISAKMP SA */
            if (IkeConstructIsakmpSA (pSessionInfo, IKE_KEY_PAYLOAD,
                                      &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_NO_STATE "
                             "IkeConstructIsakmpSA Failed\n");
                return IKE_FAILURE;
            }

            /* Construct Key PayLoad */
            if (IkeConstructKE (pSessionInfo, IKE_NONCE_PAYLOAD,
                                &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_NO_STATE "
                             "IkeConstructKE Failed\n");
                return IKE_FAILURE;
            }

            if ((pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_RSA_SIGNATURE) ||
                (pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                 IKE_DSS_SIGNATURE))
            {
                /* check whether cert request is required to send */
                if (IkeCheckPeerCertDB (pSessionInfo) == IKE_FALSE)
                {
                    bSendCR = IKE_TRUE;
                }
            }

            u1NxtPayload =
                ((bSendCR == IKE_TRUE) ? IKE_CR_PAYLOAD : IKE_ID_PAYLOAD);

            /* Construct Nonce PayLoad */
            if (IkeConstructNonce (pSessionInfo, u1NxtPayload,
                                   &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_NO_STATE "
                             "IkeConstructNonce Failed\n");
                return IKE_FAILURE;
            }

            /* Construct Certificate request PayLoad */
            if (bSendCR == IKE_TRUE)
            {
                if (IkeConstructCertRequest
                    (pSessionInfo, IKE_ID_PAYLOAD, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeConstructCertRequest Failed\n");
                    return IKE_FAILURE;
                }
            }

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                u1NxtPayload = IKE_HASH_PAYLOAD;

                if ((pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                     IKE_RSA_SIGNATURE) ||
                    (pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                     IKE_DSS_SIGNATURE))
                {
                    if (IkeCheckSendingCertPayload (pSessionInfo) == IKE_TRUE)
                    {
                        u1NxtPayload = IKE_CERT_PAYLOAD;
                    }
                    else
                    {
                        u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
                    }
                }

                /* Constuct ID PayLoad */
                if (IkeConstructID (pSessionInfo, u1NxtPayload,
                                    &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeConstructID Failed\n");
                    return IKE_FAILURE;
                }

                /* Generate the Phase1 Keys */
                if (IkeGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeGenSkeyId Failed\n");
                    return IKE_FAILURE;
                }

                if (u1NxtPayload == IKE_CERT_PAYLOAD)
                {
                    /* Construct certificate payloads */
                    if (IkeConstructCertificate
                        (pSessionInfo, IKE_SIGNATURE_PAYLOAD,
                         &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeMainMode: State MM_KEY_EXCH "
                                     "IkeConstructCertificate Failed\n");
                        return IKE_FAILURE;
                    }
                    u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
                }

                if (u1NxtPayload == IKE_SIGNATURE_PAYLOAD)
                {
                    if (IkeConstructSignature
                        (pSessionInfo, IKE_NONE_PAYLOAD, &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeMainMode: State MM_KEY_EXCH "
                                     "IkeConstructSignature Failed\n");
                        return IKE_FAILURE;
                    }
                }
                else
                {
                    /* Construct Hash PayLoad */
                    if (IkeConstructHash (pSessionInfo, IKE_VENDORID_PAYLOAD,
                                          &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeAggressiveMode: AG_NO_STATE "
                                     "IkeConstructHash Failed\n");
                        return IKE_FAILURE;
                    }
                    u1NxtPayload = IKE_NONE_PAYLOAD;
                    /* Fill The Payload for Xauth Vendor ID */
                    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                        IKE_PEER_SUPPORT_NATT)
                    {
                        u1NxtPayload = IKE_NAT_DETECT_PAYLOADS;
                    }
                    if (IkeConstructVendorId (pSessionInfo, u1NxtPayload,
                                              &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeAggressiveMode: AG_NO_STATE "
                                     "IkeConstructHash Failed\n");
                        return IKE_FAILURE;
                    }
                    if (u1NxtPayload == IKE_NAT_DETECT_PAYLOADS)
                    {
                        if (IkeConstructNatdPayLoad (pSessionInfo,
                                                     IKE_NONE_PAYLOAD,
                                                     &u4Len) == IKE_FAILURE)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "IkeAggressiveMode: AG_NO_STATE "
                                         "IkeConstructNatDPayload Failed\n");
                            return IKE_FAILURE;
                        }
                    }
                }
            }
            else
            {
                /* Constuct ID PayLoad  here next pay load is zero */
                if (IkeConstructID (pSessionInfo, IKE_VENDORID_PAYLOAD,
                                    &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeConstructID Failed\n");
                    return IKE_FAILURE;
                }
                if (IkeConstructVendorIdPayload (pSessionInfo, IKE_NONE_PAYLOAD,
                                                 &u4Len,
                                                 IKE_NATT_SUPPORT_VENDOR_ID) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_NO_STATE "
                                 "IkeConstructVendorIdPayload Failed\n");
                    return IKE_FAILURE;
                }
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            pSessionInfo->u1FsmState = IKE_AG_KEY_EXCH;
            pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
            if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                               IKE_RETRANS_TIMER_ID, IKE_RETRANSMIT_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_NO_STATE "
                             "IkeStartTimer Failed\n");
            }
            break;
        }

        case IKE_AG_KEY_EXCH:
        {
            if (IkeProcess (pIsakmpHdr->u1NextPayload, pSessionInfo,
                            (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                             sizeof (tIsakmpHdr))) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: AG_KEY_EXCH "
                             "IkeProcess Failed\n");
                return IKE_FAILURE;
            }

            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                u1NxtPayload = IKE_HASH_PAYLOAD;

                if ((pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                     IKE_RSA_SIGNATURE) ||
                    (pSessionInfo->IkePhase1Policy.u2AuthMethod ==
                     IKE_DSS_SIGNATURE))
                {
                    if (IkeCheckSendingCertPayload (pSessionInfo) == IKE_TRUE)
                    {
                        u1NxtPayload = IKE_CERT_PAYLOAD;
                    }
                    else
                    {
                        u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
                    }
                }

                /* Construct ISAKMP Header */
                if (IkeConstructHeader
                    (pSessionInfo, IKE_AGGRESSIVE_MODE,
                     u1NxtPayload) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeAggressiveMode: AG_KEY_EXCH "
                                 "IkeConstructHeader Failed\n");
                    return IKE_FAILURE;
                }

                if (u1NxtPayload == IKE_CERT_PAYLOAD)
                {
                    /* Construct certificate and signature payloads */
                    if (IkeConstructCertificate
                        (pSessionInfo, IKE_SIGNATURE_PAYLOAD,
                         &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeMainMode: State MM_KEY_EXCH "
                                     "IkeConstructCertificate Failed\n");
                        return IKE_FAILURE;
                    }
                    u1NxtPayload = IKE_SIGNATURE_PAYLOAD;
                }

                if (u1NxtPayload == IKE_SIGNATURE_PAYLOAD)
                {
                    u1NxtPayload = IKE_NONE_PAYLOAD;
                    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                        IKE_PEER_SUPPORT_NATT)
                    {
                        u1NxtPayload = IKE_NAT_DETECT_PAYLOADS;
                    }
                    if (IkeConstructSignature
                        (pSessionInfo, u1NxtPayload, &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeMainMode: State MM_KEY_EXCH "
                                     "IkeConstructSignature Failed\n");
                        return IKE_FAILURE;
                    }
                }
                else
                {
                    u1NxtPayload = IKE_NONE_PAYLOAD;
                    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                        IKE_PEER_SUPPORT_NATT)
                    {
                        u1NxtPayload = IKE_NAT_DETECT_PAYLOADS;
                    }
                    /* Construct Hash PayLoad */
                    if (IkeConstructHash (pSessionInfo, u1NxtPayload,
                                          &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeAggressiveMode: AG_KEY_EXCH "
                                     "IkeConstructHash Failed\n");
                        return IKE_FAILURE;
                    }
                }
                if (u1NxtPayload == IKE_NAT_DETECT_PAYLOADS)
                {
                    if (IkeConstructNatdPayLoad (pSessionInfo,
                                                 IKE_NONE_PAYLOAD,
                                                 &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeAggressiveMode: AG_NO_STATE "
                                     "IkeConstructNatDPayload Failed\n");
                        return IKE_FAILURE;
                    }
                }

                /* Update Packet total length in the Isakmp hdr */
                IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
                u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
                ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.
                                          pu1RawPkt))->u4TotalLen = u4Len;
                IKE_SET_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            }

            /* Mark Phase1 as done */
            pSessionInfo->bPhase1Done = IKE_TRUE;
            break;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeAggressiveMode:Unknown AG state\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeQuickMode 
 *  Description   : Called from Ike main to handle quick
 *                  mode packets.
 *  Input(s)      : pSessionInfo - Session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeQuickMode (tIkeSessionInfo * pSessionInfo)
{
    tIkeNetwork         InitId, RespId;
    UINT4               u4Len = IKE_ZERO, u4HashLen = IKE_ZERO;
    UINT1               u1NextPayload = IKE_ZERO;
    tIkeIPSecIfParam    IPSecIfParam;
    BOOLEAN             bFlag;
    CHR1               *pu1PeerAddrStr = NULL;

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        bFlag = IKE_TEST_ENCR_MASK (pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        if (bFlag == IKE_ZERO)
        {
            /* In Quick Mode and not an encrypted packet, reject */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeQuickMode: Not "
                         "an encrypted packet!\n");
            return (IKE_FAILURE);
        }

        /* Check if the first payload is Hash Payload */
        u1NextPayload =
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt))->
            u1NextPayload;
        if (u1NextPayload != IKE_HASH_PAYLOAD)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeQuickMode: First payload is not HASH Payload!\n");
            return (IKE_FAILURE);
        }
    }

    u4HashLen =
        gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE_QM_IDLE:
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (IkeProcessQmHash1 (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: ProcessQmHash1 Failed\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessPhase2Id (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "ProcessPhase2Id Failed\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessSa (pSessionInfo, NULL) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE " "ProcessSa Failed\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessNonce (pSessionInfo, NULL) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "ProcessNonce Failed\n");
                    return (IKE_FAILURE);
                }

                if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
                {
                    if (IkeProcessKE (pSessionInfo, NULL) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeQuickMode: QM_IDLE "
                                     "ProcessKE Failed\n");
                        return (IKE_FAILURE);
                    }
                }

            }

            u4Len = sizeof (tIsakmpHdr);
            /* Construct ISAKMP Header */
            if (IkeConstructHeader (pSessionInfo, IKE_QUICK_MODE,
                                    IKE_HASH_PAYLOAD) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: QM_IDLE " "Construct Hdr Failed\n");
                return IKE_FAILURE;
            }

            u4Len += u4HashLen + sizeof (tHashPayLoad);
            if (ConstructIsakmpMesg (pSessionInfo,
                                     sizeof (tIsakmpHdr),
                                     (u4HashLen + sizeof (tHashPayLoad))) ==
                IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                             "IkeQuickMode: QM_IDLE " "Memory Not Available\n");
                return IKE_FAILURE;
            }

            /* Construct Ipsec SA */
            if (IkeConstructIpsecSA (pSessionInfo, IKE_NONCE_PAYLOAD,
                                     &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: QM_IDLE " "Construct SA Failed\n");
                return IKE_FAILURE;
            }

            u1NextPayload = (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO) ?
                IKE_KEY_PAYLOAD : IKE_ID_PAYLOAD;
            /* Construct Nonce PayLoad */
            if (IkeConstructNonce (pSessionInfo,
                                   u1NextPayload, &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: QM_IDLE "
                             "Construct Nonce Failed\n");
                return IKE_FAILURE;
            }

            if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
            {
                if (IkeConstructKE (pSessionInfo, IKE_ID_PAYLOAD, &u4Len)
                    == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "Cannot construct KE Payload\n");
                    return IKE_FAILURE;
                }
            }

            /* Construct Proxy ID PayLoad */
            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                InitId = pSessionInfo->IkePhase2Policy.LocalNetwork;
                RespId = pSessionInfo->IkePhase2Policy.RemoteNetwork;
            }
            else
            {
                InitId = pSessionInfo->IkePhase2Policy.RemoteNetwork;
                RespId = pSessionInfo->IkePhase2Policy.LocalNetwork;
            }

            /* We either send both ID Payloads or neither */
            if (InitId.u4Type != IKE_ZERO)
            {
                if (RespId.u4Type == IKE_ZERO)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "DestId not present\n");
                    return IKE_FAILURE;
                }

                if (IkeConstructProxyID (pSessionInfo, &InitId,
                                         IKE_ID_PAYLOAD, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "Cannot construct InitId\n");
                    return IKE_FAILURE;
                }

                if (IkeConstructProxyID (pSessionInfo, &RespId,
                                         IKE_ZERO, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "Cannot construct RespId\n");
                    return IKE_FAILURE;
                }
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                if (IkeConstructQmHash1 (pSessionInfo, IKE_SA_PAYLOAD) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "Cannot construct Hash1\n");
                    return IKE_FAILURE;
                }
            }
            else
            {
                if (IkeConstructQmHash2 (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_IDLE "
                                 "Cannot construct Hash2\n");
                    return IKE_FAILURE;
                }
            }

            pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
            if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                               IKE_RETRANS_TIMER_ID, IKE_RETRANSMIT_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeAggressiveMode: QM_IDLE "
                             "IkeStartTimer Failed\n");
            }
            pSessionInfo->u1FsmState = IKE_QM_WAIT;
            IKE_SET_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            break;

        case IKE_QM_WAIT:
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (IkeProcessQmHash3 (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "Cannot process Hash3\n");
                    return IKE_FAILURE;
                }
                pSessionInfo->bPhase2Done = IKE_TRUE;

            }
            else
            {
                /* Process the received Payloads */
                if (IkeProcessQmHash2 (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "IkeProcessQmHash2 failed\n");
                    return IKE_FAILURE;
                }

                if (IkeProcessPhase2Id (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "IkeProcessPhase2Id Failed\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessSa (pSessionInfo, NULL) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "IkeProcessSa Failed\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessNonce (pSessionInfo, NULL) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "IkeProcessNonce Failed\n");
                    return (IKE_FAILURE);
                }

                if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
                {
                    if (IkeProcessKE (pSessionInfo, NULL) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeQuickMode: QM_WAIT "
                                     "ProcessKE Failed\n");
                        return (IKE_FAILURE);
                    }
                }

                u4Len = sizeof (tIsakmpHdr);
                /* Construct ISAKMP Header */
                if (IkeConstructHeader (pSessionInfo, IKE_QUICK_MODE,
                                        IKE_HASH_PAYLOAD) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "Cannot construct ISAKMP Hdr\n");
                    return IKE_FAILURE;
                }

                u4Len += u4HashLen + sizeof (tHashPayLoad);
                if (ConstructIsakmpMesg (pSessionInfo,
                                         sizeof (tIsakmpHdr),
                                         (u4HashLen + sizeof (tHashPayLoad))) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "Memory Not Available\n");
                    return IKE_FAILURE;
                }

                /* Construct Hash PayLoad */
                if (IkeConstructQmHash3 (pSessionInfo, IKE_ZERO) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode: QM_WAIT "
                                 "Cannot construct Hash3\n");
                    return IKE_FAILURE;
                }

                /* Update Packet total length in the Isakmp hdr */
                IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
                u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
                ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.
                                          pu1RawPkt))->u4TotalLen = u4Len;
                IKE_SET_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            }

            /* Generate the IPSec Keys */
            if (IkeGenIPSecKeys (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: IkeGenIPSecKeys failed\n");
                return IKE_FAILURE;
            }

            /* Copy the Phase2 SA Bundle and send to IPSec */
            IKE_BZERO (&IPSecIfParam, sizeof (tIkeIPSecIfParam));

            /* Intimate IPSec whether to install only the SAs or
             * all (SA, Policy, ACL and Selectors */
            if (pSessionInfo->pIkeSA->bTransDone == TRUE)
            {
                pSessionInfo->IkePhase2Info.IPSecBundle.bInstallDynamicIPSecDB
                    = TRUE;
                /* Create the VPNC Interface in case of RAVPN Client */
                if (gbCMServer == IKE_FALSE)
                {
                    if (IkeCreateVpncInterface (pSessionInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeQuickMode:\
                                     IkeCreateVpncInterface failed\n");
                        return IKE_FAILURE;
                    }
                }
            }
            else
            {
                pSessionInfo->IkePhase2Info.IPSecBundle.bInstallDynamicIPSecDB
                    = FALSE;
            }

            /* Intimate IPSec about the VPN mode */
            pSessionInfo->IkePhase2Info.IPSecBundle.bVpnServer = gbCMServer;

            IPSecIfParam.u4MsgType = IKE_IPSEC_INSTALL_SA;

            if (((pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                  IKE_USE_NATT_PORT) == IKE_USE_NATT_PORT) &&
                ((pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
                  IKE_PEER_BEHIND_NAT) == IKE_PEER_BEHIND_NAT))
            {
                if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeNatKeepAliveTimer,
                                   IKE_NAT_KEEP_ALIVE_TIMER_ID,
                                   IKE_NAT_KEEP_ALIVE_TIMER_INTERVAL,
                                   pSessionInfo->pIkeSA) != IKE_SUCCESS)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeQuickMode:\
                                 NatKeepAliveTmr: IkeStartTimer failed!\n");
                }

            }
            IKE_MEMCPY (&pSessionInfo->IkePhase2Info.IPSecBundle.
                        aOutSABundle[IKE_INDEX_0].IkeNattInfo,
                        &pSessionInfo->pIkeSA->IkeNattInfo, sizeof (tIkeNatT));
            IKE_MEMCPY (&IPSecIfParam.IPSecReq.InstallSA,
                        &pSessionInfo->IkePhase2Info.IPSecBundle,
                        sizeof (tIPSecBundle));
            IPSecIfParam.IPSecReq.InstallSA.u1IkeVersion = IKE_ISAKMP_VERSION;

            if (IkeSendIPSecIfParam (&IPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeQuickMode: IkeSendIPSecIfParam Failed\n");
                return IKE_FAILURE;
            }
            IncIkeNoOfSessionsSucc (&pSessionInfo->RemoteTunnelTermAddr);

            if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
            {
                pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
                    (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr);
            }
            else
            {
                UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                        pSessionInfo->RemoteTunnelTermAddr.
                                        Ipv4Addr);
            }

            IKE_MOD_TRC1 (gu4IkeTrace, CNTRL_PLANE_TRC, "IKE",
                          "IkeQuickMode: "
                          "Successfuly established IPsec SA with peer: %s\n",
                          pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeQuickMode: Successfuly established IPsec SA with peer: %s\n",
                          pu1PeerAddrStr));
            pSessionInfo->bPhase2Done = IKE_TRUE;
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeQuickMode:Unknown QM state\n");
            return IKE_FAILURE;

    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeInformationExchMode 
 *  Description   : Called from Ike main to handle information exchange 
 *                  packets.
 *  Input(s)      : pSessionInfo - Session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeInformationExchMode (tIkeSessionInfo * pSessionInfo)
{
    tIsakmpHdr         *pIsakmpHdr = NULL;
    UINT1              *pu1PayLoad = NULL;
    UINT4               u4HashLen = IKE_ZERO;
    UINT1               u1NextPayLoad = IKE_ZERO;
    BOOLEAN             bFlag, bFlag1;

    pIsakmpHdr = (tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt);
    pu1PayLoad = pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr);

    bFlag = IKE_TEST_ENCR_MASK (pIsakmpHdr);
    bFlag1 = IKE_TEST_AUTH_MASK (pIsakmpHdr);

    if ((bFlag != IKE_ZERO) || (bFlag1 != IKE_ZERO))
    {
        if (pIsakmpHdr->u1NextPayload == IKE_HASH_PAYLOAD)
        {
            u4HashLen =
                gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
                u4KeyLen;
            if (IkeProcessQmHash1 (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeInformationExchMode: ProcessQmHash1 Failed\n");
                return (IKE_FAILURE);
            }

            u1NextPayLoad =
                ((tHashPayLoad *) (void *) pu1PayLoad)->u1NextPayLoad;

            /* Point the buffer to the next payload */
            pu1PayLoad = pu1PayLoad + sizeof (tHashPayLoad) + u4HashLen;
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeInformationExchMode: Hash Payload not present\n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        u1NextPayLoad = pIsakmpHdr->u1NextPayload;
    }

    switch (u1NextPayLoad)
    {
        case IKE_NOTIFY_PAYLOAD:
            if (IkeProcessNotify (pSessionInfo, pu1PayLoad) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeInformationExchMode: Unable to process "
                             "notify payload\n");
            }
            break;

        case IKE_DELETE_PAYLOAD:
            if (IkeProcessDelete (pSessionInfo, pu1PayLoad) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeInformationExchMode: Unable to process "
                             "Delete payload\n");
            }
            break;

        default:
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeInformationExchMode: Unsupported payload "
                          "type %d:\n", pIsakmpHdr->u1NextPayload);
            break;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeTransactionExchMode 
 *  Description   : Handles Transaction Exchange packets
 * 
 *  Input(s)      : pSessionInfo - Session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT1
IkeTransactionExchMode (tIkeSessionInfo * pSessionInfo)
{
    UINT1               u1NextPayload = IKE_ZERO;
    BOOLEAN             bFlag;
    UINT4               u4HashLen = IKE_ZERO;

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        bFlag = IKE_TEST_ENCR_MASK (pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        if (bFlag == IKE_ZERO)
        {
            /* Reject unencrypted packets */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeTransactionExchMode: Not an encrypted packet!\n");
            return (IKE_FAILURE);
        }
        /* Check if the first payload is Hash Payload */
        u1NextPayload =
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt))->
            u1NextPayload;
        if (u1NextPayload != IKE_HASH_PAYLOAD)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeTransactionExchMode: First payload is not HASH "
                         "Payload!\n");
            return (IKE_FAILURE);
        }
        u4HashLen =
            gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

        UNUSED_PARAM (u4HashLen);
        if (IkeProcessTransactionHash (pSessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeTransactionExchMode:IkeProcessTransactionHash"
                         "Failed!\n");
            return (IKE_FAILURE);
        }

        if (IkePreProcessTransPkt (pSessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeTransactionExchMode: Failed to process "
                         "CM/XAuth packet!\n");
            return (IKE_FAILURE);
        }
    }

    switch (pSessionInfo->u1FsmState)
    {
        case IKE_IDLE_TRANS_PKT_RECV:
        {
            if (pSessionInfo->IkeTransInfo.u1TransPktType == IKE_XAUTH_REQ_PKT)
            {
                if (pSessionInfo->IkeTransInfo.bXAuthServer == TRUE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth server "
                                 "configured but XAuth Request packet "
                                 "received!\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessIsakmpXAuthCfgReqPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpXAuthCfgRepPkt (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth construct "
                                 "reply payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_XAUTH_REPLY_SENT;
                if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                                   IKE_DELETE_SESSION_TIMER_ID,
                                   IKE_DELAYED_SESSION_DELETE_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* Not critical error, just log */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: IkeStartTimer "
                                 "failed for Delete Session!\n");
                }
            }
            else if (pSessionInfo->IkeTransInfo.u1TransPktType ==
                     IKE_CM_REQ_PKT)
            {
                if ((pSessionInfo->pIkeSA->bXAuthEnabled == TRUE) &&
                    (pSessionInfo->pIkeSA->bXAuthDone == FALSE))
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth is not done "
                                 "but CM started\n");
                    return (IKE_FAILURE);
                }

                if (pSessionInfo->IkeTransInfo.bCMServer == FALSE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM Client configured"
                                 " but CM Req packet received!\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessIsakmpCMCfgReqPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpCMCfgRepPkt (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM process config "
                                 "reply payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_CM_REPLY_SENT;
                pSessionInfo->pIkeSA->bCMDone = TRUE;
                pSessionInfo->pIkeSA->bTransDone = TRUE;

                if (IkeCreateDynCryptoMapForServer (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode:"
                                 "IkeCreateDynCryptoMapForServer " "Failed\n");
                    return IKE_FAILURE;
                }
            }
            else if (pSessionInfo->IkeTransInfo.u1TransPktType ==
                     IKE_CM_SET_PKT)
            {
                if (gbCMServer == FALSE)
                {
                    /* RAVPN Client must not process Config SET request.
                     * So set the bDelSession to TRUE and return failure */
                    pSessionInfo->bDelSession = IKE_TRUE;
                    return (IKE_FAILURE);
                }
            }
            else if (pSessionInfo->IkeTransInfo.u1TransPktType ==
                     IKE_XAUTH_SET_PKT)
            {
                IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer != TRUE);

                if (IkeProcessIsakmpXAuthCfgSetPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpXAuthCfgAckPkt (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_XAUTH_ACK_SENT;

                if (pSessionInfo->IkeTransInfo.bUserAuthenticated == IKE_TRUE)
                {
                    pSessionInfo->pIkeSA->bXAuthDone = TRUE;
                    if (pSessionInfo->IkeTransInfo.IkeRAInfo.bCMEnabled ==
                        FALSE)
                    {
                        pSessionInfo->pIkeSA->bTransDone = TRUE;

                    }
                }
                pSessionInfo->bDelSession = IKE_TRUE;
            }
            else
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeTransactionExchMode: Invalid first "
                              "transaction payload %d!!\n",
                              pSessionInfo->IkeTransInfo.u1TransPktType);
                return (IKE_FAILURE);
            }

            return IKE_SUCCESS;
        }

        case IKE_XAUTH_REPLY_SENT:
        {
            if (pSessionInfo->IkeTransInfo.u1TransPktType == IKE_XAUTH_REQ_PKT)
            {
                IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer != TRUE);

                if (IkeProcessIsakmpXAuthCfgReqPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpXAuthCfgRepPkt (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

            }
            else if (pSessionInfo->IkeTransInfo.u1TransPktType ==
                     IKE_XAUTH_SET_PKT)
            {
                IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer != TRUE);

                if (IkeProcessIsakmpXAuthCfgSetPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpXAuthCfgAckPkt (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_XAUTH_ACK_SENT;
                if (pSessionInfo->IkeTransInfo.bUserAuthenticated == IKE_TRUE)
                {
                    pSessionInfo->pIkeSA->bXAuthDone = TRUE;
                    if (pSessionInfo->IkeTransInfo.IkeRAInfo.bCMEnabled ==
                        FALSE)
                    {
                        pSessionInfo->pIkeSA->bTransDone = TRUE;

                    }
                }
                pSessionInfo->bDelSession = IKE_TRUE;
            }
            else
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "IkeTransactionExchMode: Invalid transaction "
                              "payload received: state: IKE_XAUTH_REPLY_SENT"
                              " payload:%d!!\n",
                              pSessionInfo->IkeTransInfo.u1TransPktType);
                return (IKE_FAILURE);
            }

            return IKE_SUCCESS;
        }

        case IKE_XAUTH_ACK_SENT:
        {

            IKE_ASSERT (IKE_ZERO);
#if 0
            if (pSessionInfo->IkeTransInfo.u1TransPktType == IKE_CM_REQ_PKT)
            {
                if ((pSessionInfo->pIkeSA->bXAuthEnabled == TRUE) &&
                    (pSessionInfo->pIkeSA->bXAuthDone == FALSE))
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth is not done "
                                 "but CM started\n");
                    return (IKE_FAILURE);
                }

                if (pSessionInfo->IkeTransInfo.bCMServer == FALSE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM Client configured"
                                 " but XAuth Ack packet received!\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessIsakmpCMCfgReqPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode:CM process config "
                                 "request payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpCMCfgRepPkt (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM process config "
                                 "reply payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_CM_REPLY_SENT;
                pSessionInfo->pIkeSA->bCMDone = TRUE;
                pSessionInfo->pIkeSA->bTransDone = TRUE;
                if (IkeCreateDynCryptoMapForServer (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode:"
                                 "IkeCreateDynCryptoMapForServer " "Failed\n");
                    return IKE_FAILURE;
                }
            }
            else if (pSessionInfo->IkeTransInfo.u1TransPktType ==
                     IKE_CM_SET_PKT)
            {
                if ((pSessionInfo->pIkeSA->bXAuthEnabled == TRUE) &&
                    (pSessionInfo->pIkeSA->bXAuthDone == FALSE))
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: XAuth is not done "
                                 "but CM started\n");
                    return (IKE_FAILURE);
                }

                if (pSessionInfo->IkeTransInfo.bCMServer == TRUE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM server configured"
                                 " but CM Set packet received!\n");
                    return (IKE_FAILURE);
                }

                if (IkeProcessIsakmpCMCfgSetPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM process config "
                                 "Set payload failed!!\n");
                    return (IKE_FAILURE);
                }

                if (IkeConstructIsakmpCMCfgAckPkt (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: CM process config "
                                 "ack payload failed!!\n");
                    return (IKE_FAILURE);
                }

                pSessionInfo->u1FsmState = IKE_CM_ACK_SENT;
                pSessionInfo->pIkeSA->bCMDone = TRUE;
                pSessionInfo->pIkeSA->bTransDone = TRUE;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: Invalid transaction "
                             "payload received: state: IKE_XAUTH_ACK_SENT"
                             " payload:%d!!\n",
                             pSessionInfo->IkeTransInfo.u1TransPktType);
                return (IKE_FAILURE);
            }

            return IKE_SUCCESS;
#endif
            break;

        }

        case IKE_XAUTH_IDLE_INIT_REQ:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer == TRUE)
                if (IkeConstructIsakmpXAuthCfgReqPkt (pSessionInfo) ==
                    IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: XAuth process config "
                             "request payload failed!!\n");
                return (IKE_FAILURE);
            }

            pSessionInfo->u1FsmState = IKE_XAUTH_REQ_SENT;

            return IKE_SUCCESS;
        }

        case IKE_XAUTH_REQ_SENT:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer == TRUE)
                if (IkeProcessIsakmpXAuthCfgRepPkt (pSessionInfo, NULL) ==
                    IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: XAuth process config "
                             "reply payload failed!!\n");
                return (IKE_FAILURE);
            }

            /* Start a new message ID */
            IkeGetRandom ((UINT1 *) &pSessionInfo->u4MsgId, IKE_MSGID_LEN);
            if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: IkeGenIV failed\n");
                return (IKE_FAILURE);
            }
            if (IkeConstructIsakmpXAuthCfgSetPkt (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: XAuth construct config "
                             "set payload failed!!\n");
                return (IKE_FAILURE);
            }

            pSessionInfo->u1FsmState = IKE_XAUTH_SET_SENT;

            return IKE_SUCCESS;
        }

        case IKE_XAUTH_SET_SENT:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bXAuthServer == TRUE);

            if (IkeProcessIsakmpXAuthCfgAckPkt (pSessionInfo, NULL) ==
                IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: XAuth process config "
                             "ack payload failed!!\n");
                return (IKE_FAILURE);
            }

            if (pSessionInfo->IkeTransInfo.bUserAuthenticated == IKE_TRUE)
            {
                pSessionInfo->pIkeSA->bXAuthDone = TRUE;
            }

            if ((pSessionInfo->pIkeSA->bCMEnabled == FALSE) ||
                (pSessionInfo->pIkeSA->bCMDone == TRUE))
            {
                pSessionInfo->pIkeSA->bTransDone = TRUE;

                if (IkeCreateDynCryptoMapForServer (pSessionInfo) ==
                    IKE_FAILURE)
                {

                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode: Create Dynamic"
                                 "Crypto Map For Server failed!!\n");

                }
                return IKE_SUCCESS;
            }

            if (pSessionInfo->IkeTransInfo.bCMServer == FALSE)
            {
                /* Wait for the other end to start config mode */
                /* Rare case: typically XAuthServer will also be CMServer */

                /* Set bTransDone to TRUE. A new session will be created
                 * when the peer starts CM mode.
                 */
                pSessionInfo->pIkeSA->bTransDone = TRUE;
                return IKE_SUCCESS;
            }

            /* Start CM session */

            /* Change Msg ID as this is new session */
            IkeGetRandom ((UINT1 *) &pSessionInfo->u4MsgId, IKE_MSGID_LEN);

            if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: IkeGenIV failed\n");
                return (IKE_FAILURE);
            }
            if (IkeConstructIsakmpCMCfgSetPkt (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "Set payload failed!!\n");
                return (IKE_FAILURE);
            }

            pSessionInfo->u1FsmState = IKE_CM_SET_SENT;

            /* The client may/may not send a CM Request packet 
             * so , we are pro-actively sending the CM SET (PUSH) packet.
             * If the client ignores the CM SET and sends a CM REQ pkt 
             * then the timer will delete this session as a new session
             * will be created for the CM Request.*/
            if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               IKE_DELAYED_SESSION_DELETE_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* Not critical error, just log */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeStartTimer "
                             "failed for Delete Session!\n");
            }
            return IKE_SUCCESS;
        }

        case IKE_CM_SET_SENT:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bCMServer == TRUE);
            /* The CLIENT has accepted the CM SET packet and sent a CM ACK.
             * so stop the timer and proceed with the processing */
            IkeStopTimer (&pSessionInfo->IkeSessionDeleteTimer);
            if (IkeProcessIsakmpCMCfgAckPkt (pSessionInfo, NULL) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "Ack payload failed!!\n");
                return (IKE_FAILURE);
            }

            if (pSessionInfo->IkeTransInfo.bCMSuccess == IKE_TRUE)
            {
                pSessionInfo->pIkeSA->bCMDone = TRUE;
                pSessionInfo->pIkeSA->bTransDone = TRUE;
                if (IkeCreateDynCryptoMapForServer (pSessionInfo) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeTransactionExchMode:"
                                 "IkeCreateDynCryptoMapForServer " "Failed\n");
                    return IKE_FAILURE;
                }
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: XAuth process config "
                             "request payload failed!!\n");
                return (IKE_FAILURE);
            }

            return IKE_SUCCESS;
        }

        case IKE_CM_IDLE_INIT_REQ:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bCMServer == FALSE);

            if (IkeConstructIsakmpCMCfgReqPkt (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "request payload failed!!\n");
                return (IKE_FAILURE);
            }

            pSessionInfo->u1FsmState = IKE_CM_REQ_SENT;

            return IKE_SUCCESS;
        }

        case IKE_CM_REQ_SENT:
        {
            IKE_ASSERT (pSessionInfo->IkeTransInfo.bCMServer == FALSE);

            if (IkeProcessIsakmpCMCfgRepPkt (pSessionInfo, NULL) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "reply payload failed!!\n");
                return (IKE_FAILURE);
            }

            if (pSessionInfo->IkeTransInfo.bCMSuccess == IKE_TRUE)
            {
                pSessionInfo->pIkeSA->bCMDone = TRUE;
                pSessionInfo->pIkeSA->bTransDone = TRUE;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "reply payload failed!!\n");
                return (IKE_FAILURE);
            }
            return IKE_SUCCESS;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeTransactionExchMode: Invalid Packet Type!!\n");
        }
    }                            /* switch */
    return (IKE_FAILURE);
}
