/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ikesession.c,v 1.19 2015/10/07 11:11:27 siva Exp $
 *
 * Description: This has functions for IKE session management 
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikeconstruct.h"
#include "ikedsa.h"
#include "ikememmac.h"

BOOLEAN             gbXAuthServer = FALSE;    /* VR support is not there in IPSec.
                                               If one engine is XAuthServer, then
                                               all the engines must be 
                                               XAuthServer. So bXAuthServer and 
                                               bCMServer are global */

/************************************************************************/
/*  Function Name   :IkePhase1SessionInit                               */
/*  Description     :This function is used to initialize the Phase1     */
/*                  :session structure                                  */
/*  Input(s)        :u1Role - Initiator or Responder                    */
/*                  :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :u1Exch - Exchange mode (main/aggressive)           */
/*                  :pPeerTEAddr - The Peer Tunnel Endpoint Address     */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the SessionInfo structure               */
/************************************************************************/

tIkeSessionInfo    *
IkePhase1SessionInit (UINT1 u1Role, tIkeEngine * pIkeEngine, UINT1 u1Exch,
                      tIkeIpAddr * pPeerTEAddr)
{
    tIkeSessionInfo    *pSesInfo = NULL;
    INT4                i4Pos = IKE_ZERO;
    tIkeCertMapToPeer  *pCertMap = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeSA             *pIkeSA = NULL;

/*Incase of Responder if alredy one IKE Active session is exist stop the Re-key of that IKE session and start Delete Timer
 * since one new IKE session is initiated from other side, having the OLD IKE SAs will result in multiple Re-Keying*/
    if( u1Role == IKE_RESPONDER)
    {
     SLL_SCAN (&pIkeEngine->IkeSAList, pIkeSA, tIkeSA *)
      {
         if(pPeerTEAddr->u4AddrType == IPV4ADDR)
         {
             if (pIkeSA->IpPeerAddr.Ipv4Addr == pPeerTEAddr->Ipv4Addr)
             {
                 IkeStopTimer(&pIkeSA->IkeSALifeTimeTimer);
                 if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                                     IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                                     pIkeSA) != IKE_SUCCESS)
                 {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkePhase1SessionInit: IkeStartTimer failed!\n");
                     return NULL;
                 }                                     
             }
         }
      }
    }
    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase1SessionInit: unable to allocate " "buffer\n");
        return NULL;
    }
    if (pSesInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkePhase1SessionInit: Session memory Allocation Fail\n");
        return NULL;
    }
    IKE_BZERO (pSesInfo, sizeof (tIkeSessionInfo));
    if (MemAllocateMemBlock
        (IKE_SA_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pIkeSA) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase1SessionInit: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return NULL;
    }
    if (pSesInfo->pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkePhase1SessionInit:Not enough memory\n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return (NULL);
    }
    IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
    pSesInfo->pIkeSA->u4RefCount++;

    IKE_STRCPY (pSesInfo->au1EngineName, pIkeEngine->au1EngineName);

    pSesInfo->u1SessionStatus = IKE_ACTIVE;
    pSesInfo->u1IkeVer = IKE_ISAKMP_VERSION;
    pSesInfo->pIkeSA->u1IkeVer = IKE_ISAKMP_VERSION;

    OsixGetSysTime (&(pSesInfo->u4TimeStamp));
    pSesInfo->pIkeSA->u1SAStatus = IKE_ACTIVE;
    pSesInfo->u1Role = u1Role;
    pSesInfo->u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
    pSesInfo->pIkeSA->u4IkeEngineIndex = pIkeEngine->u4IkeIndex;

    /* Copy the Tunnel Term addr of the peer */
    IKE_MEMCPY (&pSesInfo->RemoteTunnelTermAddr, pPeerTEAddr,
                sizeof (tIkeIpAddr));
    IKE_MEMCPY (&pSesInfo->pIkeSA->IpPeerAddr, pPeerTEAddr,
                sizeof (tIkeIpAddr));

    /* Copy the local Tunnel Termination address */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&pSesInfo->pIkeSA->IpLocalAddr,
                    &pIkeEngine->Ip6TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    else
    {
        IKE_MEMCPY (&pSesInfo->pIkeSA->IpLocalAddr,
                    &pIkeEngine->Ip4TunnelTermAddr, sizeof (tIkeIpAddr));
    }

    if (IkeCopyPhase1SessionInfo (pIkeEngine, pSesInfo, u1Exch) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkePhase1SessionInit:Not able to copy "
                     "Phase1SessionInfo\n");
        /* FIPS compliance - Cryptographic keys zeroed at the
         * end of their lifecycle */
        IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
        MemReleaseMemBlock (IKE_SA_MEMPOOL_ID, (UINT1 *) pSesInfo->pIkeSA);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return (NULL);
    }

    if ((pSesInfo->pIkeSA->u2AuthMethod == IKE_RSA_SIGNATURE) ||
        (pSesInfo->pIkeSA->u2AuthMethod == IKE_DSS_SIGNATURE))
    {
        /* check if a specific certificate is configured for this peer */
        if ((pSesInfo->IkePhase1Policy.PolicyPeerID.i4IdType != IKE_KEY_IPV4)
            &&
            (pSesInfo->IkePhase1Policy.PolicyPeerID.i4IdType != IKE_KEY_IPV6))
        {
            pCertMap =
                IkeGetCertMapFromRemoteID (pIkeEngine,
                                           pSesInfo->IkePhase1Policy.
                                           PolicyPeerID.uID.au1Dn,
                                           (UINT1) pSesInfo->IkePhase1Policy.
                                           PolicyPeerID.i4IdType);
        }
        else
        {
            pCertMap =
                IkeGetCertMapFromPeerAddr (pIkeEngine,
                                           &pSesInfo->pIkeSA->IpPeerAddr);
        }
        if (pCertMap == NULL)
        {
            /* Check if default certificate is present */
            if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
                (pIkeEngine->DefaultCert.pCASerialNum != NULL))
            {
                /* Copy the default cert information to session 
                 * data structure */
                TAKE_IKE_SEM ();
                pSesInfo->MyCert.pCAName =
                    IkeX509NameDup (pIkeEngine->DefaultCert.pCAName);
                pSesInfo->MyCert.pCASerialNum =
                    IkeASN1IntegerDup (pIkeEngine->DefaultCert.pCASerialNum);
                GIVE_IKE_SEM ();
                pSesInfo->bDefaultMyCert = IKE_TRUE;
            }
            else
            {
                /* if Default Cert is not there and the peer configuration 
                 * not present then don't start negotiation */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo: Certificate "
                             "not configured for the peer!\n");
                /* FIPS compliance - Cryptographic keys zeroed at the
                 * end of their lifecycle */
                IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
                MemReleaseMemBlock (IKE_SA_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo->pIkeSA);
                MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo);
                return (NULL);
            }
        }
        else
        {
            pSesInfo->MyCert.pCAName =
                IkeX509NameDup (pCertMap->MyCert.pCAName);
            pSesInfo->MyCert.pCASerialNum =
                IkeASN1IntegerDup (pCertMap->MyCert.pCASerialNum);
            pSesInfo->bDefaultMyCert = IKE_FALSE;

            pSesInfo->pIkeSA->ResponderID.i4IdType = pCertMap->PeerID.i4IdType;
            pSesInfo->pIkeSA->ResponderID.u4Length = pCertMap->PeerID.u4Length;
            IKE_MEMCPY (&pSesInfo->pIkeSA->ResponderID.uID,
                        &pCertMap->PeerID.uID, pCertMap->PeerID.u4Length);

        }
        if (pSesInfo->pIkeSA->u2AuthMethod == IKE_RSA_SIGNATURE)
        {
            /* Store the rsa key associated with this certificate */
            pIkeRsa = IkeGetRSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);

            if (pIkeRsa != NULL)
            {
                IKE_MEMCPY (&pSesInfo->MyCertKey, pIkeRsa,
                            sizeof (tIkeRsaKeyInfo));
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo: Unable to get"
                             "RSA key using certificate information !\n");
                /* FIPS compliance - Cryptographic keys zeroed at the
                 * end of their lifecycle */
                IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
                MemReleaseMemBlock (IKE_SA_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo->pIkeSA);
                MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo);
                return (NULL);
            }
        }
        else if (pSesInfo->pIkeSA->u2AuthMethod == IKE_DSS_SIGNATURE)
        {
            /* Store the dsa key associated with this certificate */
            pIkeDsa = IkeGetDSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);

            if (pIkeDsa != NULL)
            {
                IKE_MEMCPY (&pSesInfo->MyCertKey, pIkeDsa,
                            sizeof (tIkeDsaKeyInfo));
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo: Unable to get"
                             "DSA key using certificate information !\n");
                /* FIPS compliance - Cryptographic keys zeroed at the
                 * end of their lifecycle */
                IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
                MemReleaseMemBlock (IKE_SA_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo->pIkeSA);
                MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                    (UINT1 *) pSesInfo);
                return (NULL);
            }
        }
        SLL_INIT (&(pSesInfo->PeerCertReq));
    }

    if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_1)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo768;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_2)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo1024;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == IKE_DH_GROUP_5)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo1536;
    }
    else if (pSesInfo->IkePhase1Policy.u1DHGroup == SEC_DH_GROUP_14)
    {
        pSesInfo->pDhInfo = &gDhGroupInfo2048;
    }

    if (u1Role == IKE_RESPONDER)
    {
        if (pSesInfo->IkePhase1Policy.u1Mode != u1Exch)
        {
            IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkePhase1SessionInit: Responder case, Configured"
                          " mode(%d) does not match received mode(%d)\n",
                          pSesInfo->IkePhase1Policy.u1Mode, u1Exch);
            IKE_SET_ERROR (pSesInfo, IKE_INVALID_EXCHANGE_TYPE);
            pSesInfo->u1ExchangeType = pSesInfo->IkePhase1Policy.u1Mode;
            IkeSendNotifyInfo (pSesInfo);
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
            MemReleaseMemBlock (IKE_SA_MEMPOOL_ID, (UINT1 *) pSesInfo->pIkeSA);
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSesInfo);
            return (NULL);
        }
        pSesInfo->u1ExchangeType = u1Exch;
    }
    else
    {
        pSesInfo->u1ExchangeType = pSesInfo->IkePhase1Policy.u1Mode;
        pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort = IKE_UDP_PORT;
    }

    if (pSesInfo->IkePhase1Policy.u1Mode == IKE_MAIN_MODE)
    {
        pSesInfo->u1FsmState = IKE_MM_NO_STATE;
    }
    else if (pSesInfo->IkePhase1Policy.u1Mode == IKE_AGGRESSIVE_MODE)
    {
        pSesInfo->u1FsmState = IKE_AG_NO_STATE;
    }
    pSesInfo->bPhase1Done = IKE_FALSE;

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSesInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }
    SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSesInfo);
    return (pSesInfo);
}

/************************************************************************/
/*  Function Name   :IkePhase2SessionInit                               */
/*  Description     :This function is used to initialize the Phase2     */
/*                  :session structure                                  */
/*  Input(s)        :pIkeSA - Pointer to the IkeSA structure            */
/*                  :pIPSecReq - Pointer to the IPSecReq structure      */
/*                  :u4MesgId - The Message Id                          */
/*                  :u1Role - Initiator or Responder                    */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the SessionInfo structure               */
/************************************************************************/
tIkeSessionInfo    *
IkePhase2SessionInit (tIkeSA * pIkeSA, tIkeNewSA * pIPSecReq, UINT4 u4MesgId,
                      UINT1 u1Role)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    INT4                i4Pos = IKE_ZERO;
    UINT4               u4Spi = IKE_ZERO;
    tIkeIPSecIfParam    IkeIfParam;

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase2SessionInit: unable to allocate " "buffer\n");
        return NULL;
    }

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkePhase2SessionInit: Session memory Allocation Fail\n");
        return NULL;
    }

    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;
    OsixGetSysTime (&(pSessionInfo->u4TimeStamp));

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    /* Store the message Id */
    if (u1Role == IKE_INITIATOR)
    {
        IkeGetRandom ((UINT1 *) &pSessionInfo->u4MsgId, IKE_MSGID_LEN);
    }
    else
    {
        pSessionInfo->u4MsgId = u4MesgId;
    }

    /* Store the initial vector */
    if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
    {
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkePhase2SessionInit: IkeGenIV failed\n",
                      pIkeSA->u4IkeEngineIndex);
        return NULL;
    }

    pSessionInfo->u1FsmState = IKE_QM_IDLE;
    pSessionInfo->u1IkeVer = IKE_ISAKMP_VERSION;

    pSessionInfo->u1Role = u1Role;
    pSessionInfo->u1ExchangeType = IKE_QUICK_MODE;
    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;
    SLL_INIT (&(pSessionInfo->IkeProposalList));

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkePhase2SessionInit: No IkeEngine Available with "
                      "Index %d\n", pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1CryptIV);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    pSessionInfo->i2SocketId =
        (INT2) ((pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR) ?
                (pIkeEngine->i2V6SocketFd) : (pIkeEngine->i2SocketId));

    if (pIPSecReq != NULL)
    {
        IKE_MEMCPY (&pSessionInfo->PostPhase1Info.IPSecRequest, pIPSecReq,
                    sizeof (tIkeNewSA));

        TAKE_IKE_SEM ();
        pCryptoMap = IkeGetCryptoMapFromIDs (pIkeEngine, &pIPSecReq->SrcNet,
                                             &pIPSecReq->DestNet,
                                             IKE_ISAKMP_VERSION);
        if (pCryptoMap == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "IkePhase2SessionInit: No Crypto Map Available \n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1CryptIV);
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return NULL;
        }

        IKE_MEMCPY (&pSessionInfo->IkePhase2Policy, pCryptoMap,
                    sizeof (tIkeCryptoMap));
        /* Copy local and remote network addresses to IPSecBundle */
        IKE_MEMCPY (&pSessionInfo->IkePhase2Info.IPSecBundle.LocalProxy,
                    &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
        IKE_MEMCPY (&pSessionInfo->IkePhase2Info.IPSecBundle.RemoteProxy,
                    &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));
        GIVE_IKE_SEM ();

        if (pSessionInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_1)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo768;
        }
        else if (pSessionInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_2)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo1024;
        }
        else if (pSessionInfo->IkePhase2Policy.u1Pfs == IKE_DH_GROUP_5)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo1536;
        }
        else if (pSessionInfo->IkePhase2Policy.u1Pfs == SEC_DH_GROUP_14)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo2048;
        }
    }
    pSessionInfo->bPhase2Done = IKE_FALSE;

    SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSessionInfo);

    IKE_MEMCPY (&pSessionInfo->IkePhase2Info.IPSecBundle.LocTunnTermAddr,
                &pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));
    IKE_MEMCPY (&pSessionInfo->IkePhase2Info.IPSecBundle.RemTunnTermAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));
    /* Generate the SPIs and send to IPSec to check for
       duplicate
     */
    IKE_BZERO (&IkeIfParam, sizeof (tIkeIPSecIfParam));
    IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
    IkeIfParam.IPSecReq.DupSpiInfo.u4AhSpi = u4Spi;
    IkeIfParam.IPSecReq.DupSpiInfo.u1IkeVersion = IKE_ISAKMP_VERSION;
    pSessionInfo->IkePhase2Info.IPSecBundle.
        aInSABundle[IKE_AH_SA_INDEX].u4Spi = u4Spi;

    IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
    IkeIfParam.IPSecReq.DupSpiInfo.u4EspSpi = u4Spi;

    IkeIfParam.IPSecReq.DupSpiInfo.bDupSpi = IKE_FALSE;
    pSessionInfo->IkePhase2Info.IPSecBundle.
        aInSABundle[IKE_ESP_SA_INDEX].u4Spi = u4Spi;

    IkeIfParam.u4MsgType = IKE_IPSEC_DUPLICATE_SPI_REQ;
    pSessionInfo->u4RefCount++;
    IkeIfParam.IPSecReq.DupSpiInfo.u4Context = (FS_ULONG) pSessionInfo;
    IKE_MEMCPY (&IkeIfParam.IPSecReq.DupSpiInfo.LocalAddr,
                &pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));
    IKE_MEMCPY (&IkeIfParam.IPSecReq.DupSpiInfo.RemoteAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    if (IkeSendIPSecIfParam (&IkeIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePhase2SessionInit: IkeSendIPSecIfParam " "Failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1CryptIV);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    pSessionInfo->u1FsmState = IKE_QM_WAIT_ON_IPSEC;
    return pSessionInfo;
}

/************************************************************************/
/*  Function Name   :IkeInfoExchInit                                    */
/*  Description     :This function is used to initialize the Info       */
/*                  :exchange with the peer                             */
/*  Input(s)        :pIkeSA - Pointer to the IkeSA structure            */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the SessionInfo structure               */
/************************************************************************/

tIkeSessionInfo    *
IkeInfoExchInit (tIkeSA * pIkeSA, UINT4 u4MsgId, UINT1 bEncrFlag)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInfoExchInit: unable to allocate " "buffer\n");
        return NULL;
    }

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeInfoExchInit: Session memory Allocation Fail\n");
        return NULL;
    }
    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr, &pIkeSA->IpPeerAddr,
                sizeof (tIkeIpAddr));

    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;
    pSessionInfo->u4MsgId = u4MsgId;

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeInfoExchInit: No IkeEngine Available with Index %d\n",
                      pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    pSessionInfo->i2SocketId = (INT2) ((pSessionInfo->RemoteTunnelTermAddr.
                                        u4AddrType ==
                                        IPV6ADDR) ? (pIkeEngine->
                                                     i2V6SocketFd)
                                       : (pIkeEngine->i2SocketId));
    pSessionInfo->u1ExchangeType = IKE_ISAKMP_INFO;
    pSessionInfo->u1IkeVer = IKE_ISAKMP_VERSION;

    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;
    if (bEncrFlag == IKE_TRUE)
    {
        /* Store the initial vector */
        if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
        {
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeInfoExchInit: IkeGenIV failed\n",
                          pIkeSA->u4IkeEngineIndex);
            return NULL;
        }
    }

    SLL_ADD (&pIkeEngine->IkeSessionList, (t_SLL_NODE *) pSessionInfo);
    return pSessionInfo;
}

/************************************************************************/
/*  Function Name   :IkeTransSessInit                                   */
/*  Description     :This function is used to initialize the Tarnsaction*/
/*                  :exchange with the peer                             */
/*  Input(s)        :pIkeSA - Pointer to the IkeSA structure            */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the SessionInfo structure               */
/************************************************************************/

tIkeSessionInfo    *
IkeTransSessInit (tIkeSA * pIkeSA, UINT4 u4MsgId, UINT1 bEncrFlag, UINT1 u1Role)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pRAPolicy = NULL;
    INT4                i4Pos = IKE_ZERO;

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTransSessInit: unable to allocate " "buffer\n");
        return NULL;
    }

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeTransSessInit: Session memory Allocation Fail\n");
        return NULL;
    }

    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr, &pIkeSA->IpPeerAddr,
                sizeof (tIkeIpAddr));

    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    if (u4MsgId == IKE_ZERO)
    {
        /* We are the initiator of this exchange */
        IkeGetRandom ((UINT1 *) &pSessionInfo->u4MsgId, IKE_MSGID_LEN);
    }
    else
    {
        pSessionInfo->u4MsgId = u4MsgId;
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeTransSessInit: No IkeEngine Available with Index %d\n",
                      pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    pSessionInfo->i2SocketId = (INT2) ((pSessionInfo->RemoteTunnelTermAddr.
                                        u4AddrType ==
                                        IPV6ADDR) ? (pIkeEngine->
                                                     i2V6SocketFd)
                                       : (pIkeEngine->i2SocketId));
    pSessionInfo->u1IkeVer = IKE_ISAKMP_VERSION;
    pSessionInfo->u1FsmState = IKE_IDLE;
    pSessionInfo->u1Role = u1Role;
    pSessionInfo->u1ExchangeType = IKE_TRANSACTION_MODE;

    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;

    if (bEncrFlag == IKE_TRUE)
    {
        /* Store the initial vector */
        if (IkeGenIV (pSessionInfo, IKE_PHASE2) != IKE_SUCCESS)
        {
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeTransSessInit: IkeGenIV failed\n",
                          pIkeSA->u4IkeEngineIndex);
            return NULL;
        }
    }

    /* Read Transaction Exchange Info */
    pSessionInfo->IkeTransInfo.bXAuthServer = gbXAuthServer;
    pSessionInfo->IkeTransInfo.bCMServer = gbCMServer;

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pRAPolicy, tIkeRemoteAccessInfo *)
    {
        if (IkeComparePhase1IDs (&pRAPolicy->Phase1ID,
                                 &pIkeSA->ResponderID) == IKE_ZERO)
        {
            IKE_MEMCPY (&pSessionInfo->IkeTransInfo.IkeRAInfo, pRAPolicy,
                        sizeof (tIkeRemoteAccessInfo));
            break;
        }
    }

    if (pRAPolicy == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1CryptIV);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTransSessInit: RAPolicy NULL\n");
        return NULL;
    }

    SLL_ADD (&pIkeEngine->IkeSessionList, (t_SLL_NODE *) pSessionInfo);
    return pSessionInfo;
}

/************************************************************************/
/*  Function Name   :StartTransactionExchange                           */
/*  Description     :This function is called when Phase1 is done.       */
/*                  :This function starts Transaction Exch if requried  */
/*  Input(s)        :pIkeSA - Pointer to the Phase1 SA                  */
/*  Output(s)       :                                                   */
/*  Returns         :Pointer to the Transaction SessionInfo structure   */
/************************************************************************/
tIkeSessionInfo    *
StartTransactionExchange (tIkeSA * pIkeSA, UINT1 u1Role)
{
    tIkeSessionInfo    *pTransSessInfo = NULL;

    if (pIkeSA->bXAuthEnabled == TRUE)
    {
        if (gbXAuthServer)
        {
            IKE_ASSERT (u1Role == IKE_RESPONDER);

            pTransSessInfo = IkeTransSessInit
                (pIkeSA, IKE_ZERO, TRUE, IKE_INITIATOR);

            if (pTransSessInfo == NULL)
            {
                /* The previous function would have logged */
                return NULL;
            }

            pTransSessInfo->u1FsmState = IKE_XAUTH_IDLE_INIT_REQ;
            goto StartTransactionSession;
        }

        /* Wait for other party to do XAuth */
        return NULL;
    }
    else if (pIkeSA->bCMEnabled == TRUE)
    {
        if (gbCMServer == TRUE)
        {
            /* Client has to send a CFG Request, But if it does not send,
             * we have 
             * to pro-actively send the CM_SET message with the supported 
             * attributes. */
            pTransSessInfo = IkeTransSessInit
                (pIkeSA, IKE_ZERO, TRUE, IKE_INITIATOR);

            if (pTransSessInfo == NULL)
            {
                /* The previous function would have logged */
                return NULL;
            }

            /* Change Msg ID as this is new session */
            IkeGetRandom ((UINT1 *) &pTransSessInfo->u4MsgId, IKE_MSGID_LEN);

            if (IkeGenIV (pTransSessInfo, IKE_PHASE2) != IKE_SUCCESS)
            {
                MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                    (UINT1 *) pTransSessInfo);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: IkeGenIV failed\n");
                return NULL;
            }
            if (IkeConstructIsakmpCMCfgSetPkt (pTransSessInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: CM process config "
                             "Set payload failed!!\n");
                return NULL;
            }
            /* Start timer to delete the session created to sent the 
               CFG SET Packet */
            if (IkeStartTimer (&pTransSessInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               IKE_DELAYED_SESSION_DELETE_INTERVAL,
                               pTransSessInfo) != IKE_SUCCESS)
            {
                /* Not critical error, just log */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeTransactionExchMode: IkeStartTimer "
                             "failed for Delete Session!\n");
            }

            pTransSessInfo->u1FsmState = IKE_CM_SET_SENT;
            return (pTransSessInfo);
        }

        pTransSessInfo = IkeTransSessInit
            (pIkeSA, IKE_ZERO, TRUE, IKE_INITIATOR);

        if (pTransSessInfo == NULL)
        {
            /* The previous function would have logged */
            return NULL;
        }

        pTransSessInfo->u1FsmState = IKE_CM_IDLE_INIT_REQ;
        goto StartTransactionSession;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: IkeCoreProcess " "failed!\n");
        return NULL;
    }

  StartTransactionSession:
    if (IkeCoreProcess (pTransSessInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSocketHandler: IkeCoreProcess " "failed!\n");

        if (pTransSessInfo->bDelSession == IKE_TRUE)
        {
            if (IkeDeleteSession (pTransSessInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeDeleteSession " "Failed\n");
                return NULL;
            }
        }
        else
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pTransSessInfo->pu1LastRcvdPkt);
            pTransSessInfo->pu1LastRcvdPkt = pTransSessInfo->pu1LastRcvdGoodPkt;
            pTransSessInfo->u2LastRcvdPktLen =
                pTransSessInfo->u2LastRcvdGoodPktLen;
            pTransSessInfo->pu1LastRcvdGoodPkt = NULL;
            pTransSessInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }
        return NULL;
    }

    /* Delete Phase1 Session structure */
    return (pTransSessInfo);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeSetNonceInSessionInfo
 *  Description   : Used copy Nonce to Session structure
 *  Input(s)      : pSesInfo - The Session Info structure
 *                  pu1Ptr - Pointer has nonce value.
 *                  u2NonceLen - Nonce Length.
 *                  u1NonceType - Whether Initiator/Responder Nonce
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT4
IkeSetNonceInSessionInfo (tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr,
                          UINT2 u2NonceLen, UINT1 u1NonceType)
{
    if (u1NonceType == IKE_RESPONDER)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->pu1ResponderNonce) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetNonceInSessionInfo: unable to allocate "
                         "buffer\n");
            return IKE_FAILURE;
        }
        if (pSesInfo->pu1ResponderNonce == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "IkeSetNonceInSessionInfo: Out Of Memory\n");
            return IKE_FAILURE;
        }

        IKE_MEMCPY (pSesInfo->pu1ResponderNonce, pu1Ptr, u2NonceLen);
        pSesInfo->u2ResponderNonceLen = u2NonceLen;
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->pu1InitiatorNonce) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetNonceInSessionInfo: unable to allocate "
                         "buffer\n");
            return IKE_FAILURE;
        }
        if (pSesInfo->pu1InitiatorNonce == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "IkeSetNonceInSessionInfo: Out Of Memory\n");
            return IKE_FAILURE;
        }

        IKE_MEMCPY (pSesInfo->pu1InitiatorNonce, pu1Ptr, u2NonceLen);
        pSesInfo->u2InitiatorNonceLen = u2NonceLen;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetPhase2SessionFromIkeSA                       */
/*  Description     :This function is used to get the ongoing Phase2    */
/*                  :sessions using this Ike SA                         */
/*  Input(s)        :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :pIkeSA     - Pointer to the IkeSA structure        */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the SessionInfo structure               */
/************************************************************************/

tIkeSessionInfo    *
IkeGetPhase2SessionFromIkeSA (tIkeEngine * pIkeEngine, tIkeSA * pIkeSA)
{
    tIkeSessionInfo    *pScanNode = NULL;

    SLL_SCAN (&pIkeEngine->IkeSessionList, pScanNode, tIkeSessionInfo *)
    {
        if ((IKE_MEMCMP
             (pScanNode->pIkeSA->au1InitiatorCookie,
              pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN) == IKE_ZERO)
            &&
            (IKE_MEMCMP
             (pScanNode->pIkeSA->au1ResponderCookie,
              pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN) == IKE_ZERO))
        {
            if (pScanNode->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
            {
                if (pScanNode->RemoteTunnelTermAddr.Ipv4Addr ==
                    pIkeSA->IpPeerAddr.Ipv4Addr)
                {
                    return pScanNode;
                }
            }
            else
            {
                if (IKE_MEMCMP (&pScanNode->RemoteTunnelTermAddr.Ipv6Addr,
                                &pIkeSA->IpPeerAddr.Ipv6Addr,
                                sizeof (tIp6Addr)) == IKE_ZERO)
                {
                    return pScanNode;
                }
            }
        }
    }
    return NULL;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGetSessionFromCookie 
 *  Description   : Get session info from session data base based
 *                  on peer address, Initiator cookie and 
 *                  responder cookie.
 *  Input(s)      : IkeSessionInfo - session data base.
 *                  pu1InitCookie - Initiator cookie.
 *                  pu1RespCookiew - Responder cookie.
 *  Output(s)     : tIkeSessionInfo -Pointer to the session info.
 *  Return Values : session info pointer or NULL. 
 * ---------------------------------------------------------------
 */

tIkeSessionInfo    *
IkeGetSessionFromCookie (tIkeEngine * pIkeEngine, tIkeIpAddr * pPeerTEAddr,
                         UINT2 u2RemotePort, UINT2 u2LocalPort,
                         tIsakmpHdr * pIsakmpHdr)
{
    tIkeSessionInfo    *pSesInfo = NULL, *pInfoSession = NULL;
    tIkeSA             *pIkeSA = NULL;
    UINT1               au1TempCookie[IKE_COOKIE_LEN] = { IKE_ZERO };
    UINT4               u4MsgId = IKE_ZERO;
    UINT4               u4Flag = IKE_ZERO;

    u4MsgId = IKE_NTOHL (pIsakmpHdr->u4MessId);
    if (pPeerTEAddr->u4AddrType == IKE_IPSEC_ID_IPV4_ADDR)
    {
        pPeerTEAddr->Ipv4Addr = IKE_NTOHL (pPeerTEAddr->Ipv4Addr);
    }
    SLL_SCAN (&pIkeEngine->IkeSessionList, pSesInfo, tIkeSessionInfo *)
    {
        if ((IKE_MEMCMP
             (pSesInfo->pIkeSA->au1InitiatorCookie, pIsakmpHdr->au1InitCookie,
              IKE_COOKIE_LEN) == IKE_ZERO)
            &&
            ((IKE_MEMCMP
              (pSesInfo->pIkeSA->au1ResponderCookie, pIsakmpHdr->au1RespCookie,
               IKE_COOKIE_LEN) == IKE_ZERO) ||
             (IKE_MEMCMP (pSesInfo->pIkeSA->au1ResponderCookie,
                          au1TempCookie, IKE_COOKIE_LEN) == IKE_ZERO))
            && (pSesInfo->u4MsgId == u4MsgId))
        {
            if (IKE_MEMCMP (pSesInfo->pIkeSA->au1ResponderCookie, au1TempCookie,
                            IKE_COOKIE_LEN) == IKE_ZERO)
            {
                /* Copy the Responder cookie */
                IKE_MEMCPY (pSesInfo->pIkeSA->au1ResponderCookie,
                            pIsakmpHdr->au1RespCookie, IKE_COOKIE_LEN);
            }

            if (pIsakmpHdr->u1Exch == IKE_ISAKMP_INFO)
            {
                pInfoSession =
                    IkeInfoExchInit (pSesInfo->pIkeSA, u4MsgId, IKE_FALSE);
                if (pInfoSession == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeGetSessionFromCookie:IkeInfoExchInit "
                                 "failed\n");
                    return NULL;
                }
                return pInfoSession;
            }
            else
            {
                if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
                {
                    if (pSesInfo->RemoteTunnelTermAddr.Ipv4Addr ==
                        pPeerTEAddr->Ipv4Addr)
                    {
                        return pSesInfo;
                    }
                }
                else
                {
                    if (IKE_MEMCMP
                        (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr,
                         &pPeerTEAddr->Ipv6Addr, sizeof (tIp6Addr)) == IKE_ZERO)
                    {
                        return pSesInfo;
                    }
                }
            }
        }
    }

    /* For the case when peer initiates and we are getting for the
     * first time
     */
    if (IKE_MEMCMP (pIsakmpHdr->au1RespCookie, au1TempCookie,
                    IKE_COOKIE_LEN) == IKE_ZERO)
    {
        if (pIsakmpHdr->u1Exch == IKE_ISAKMP_INFO)
        {
            /* Received an info exchange message, ignore */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGetSessionFromCookie:Received info " "message\n");
            return (NULL);
        }

        SLL_SCAN (&pIkeEngine->IkeSessionList, pSesInfo, tIkeSessionInfo *)
        {
            if ((IKE_MEMCMP
                 (pSesInfo->pIkeSA->au1InitiatorCookie,
                  pIsakmpHdr->au1InitCookie, IKE_COOKIE_LEN) == IKE_ZERO)
                && (pSesInfo->u4MsgId == u4MsgId))
            {
                if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
                {
                    if (pSesInfo->RemoteTunnelTermAddr.Ipv4Addr ==
                        pPeerTEAddr->Ipv4Addr)
                    {
                        return pSesInfo;
                    }
                }
                else
                {
                    if (IKE_MEMCMP
                        (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr,
                         &pPeerTEAddr->Ipv6Addr, sizeof (tIp6Addr)) == IKE_ZERO)
                    {
                        return pSesInfo;
                    }
                }
            }
        }
        /* RespCookie is zero, first message from peer, Construct a 
           SessionInfo structure */
        pSesInfo = IkePhase1SessionInit (IKE_RESPONDER, pIkeEngine,
                                         pIsakmpHdr->u1Exch, pPeerTEAddr);
        if (pSesInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGetSessionFromCookie:Unable to create "
                         "SessionInfo\n");
            return (NULL);
        }
        pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort = u2RemotePort;
        if (u2LocalPort == IKE_NATT_UDP_PORT)
        {
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_USE_NATT_PORT;
        }

        /* Copy the initiator cookie */
        IKE_MEMCPY (pSesInfo->pIkeSA->au1InitiatorCookie,
                    pIsakmpHdr->au1InitCookie, IKE_COOKIE_LEN);
        return (pSesInfo);
    }

    /* If we get a request from phase2, we have to start phase2 initiation, * 
     * in which case the role would be responder */
    if (pIsakmpHdr->u4MessId != IKE_ZERO)
    {
        pIkeSA =
            IkeGetIkeSAFromCookies (pIkeEngine, pIsakmpHdr->au1InitCookie,
                                    pIsakmpHdr->au1RespCookie, pPeerTEAddr);

        if (pIkeSA == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGetSessionFromCookie:IkeGetIkeSAFromCookies "
                         "failed\n");
            return NULL;
        }

        /* If RESPONDER, pass the u4MesgId */
        if (pIsakmpHdr->u1Exch == IKE_QUICK_MODE)
        {
            pSesInfo =
                IkePhase2SessionInit (pIkeSA, NULL, u4MsgId, IKE_RESPONDER);
        }
        else if (pIsakmpHdr->u1Exch == IKE_ISAKMP_INFO)
        {
            u4Flag = IKE_TEST_ENCR_MASK (pIsakmpHdr);

            if (u4Flag != IKE_ZERO)
            {
                pSesInfo = IkeInfoExchInit (pIkeSA, u4MsgId, IKE_TRUE);
            }
            else
            {
                pSesInfo = IkeInfoExchInit (pIkeSA, u4MsgId, IKE_FALSE);
            }
        }
        else if (pIsakmpHdr->u1Exch == IKE_TRANSACTION_MODE)
        {
            u4Flag = IKE_TEST_ENCR_MASK (pIsakmpHdr);

            if (u4Flag == IKE_ZERO)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeGetSessionFromCookie:Unencrypted Transaction "
                             "exchange received, ignoring\n");
                return NULL;
            }
            pSesInfo = IkeTransSessInit (pIkeSA, u4MsgId, IKE_TRUE,
                                         IKE_RESPONDER);
            if (pSesInfo == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeGetSessionFromCookie:IkeTransSessInit "
                             "failed\n");
                return NULL;
            }
            pSesInfo->u1FsmState = IKE_IDLE_TRANS_PKT_RECV;
        }

        if (pSesInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGetSessionFromCookie:IkePhase2SessionInit "
                         "failed\n");
            return NULL;
        }

        /* Phase2SessionInit would have sent a message to
           IPSec to check for duplicate SPI, will have to
           wait for the reply before starting the Phase2
           negotiation */

        return pSesInfo;
    }

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeGetSessionFromCookie:Could not get/init session\n");
    return NULL;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeCopyPhase1SessionInfo 
 *  Description   : Create a new session and add it to session 
 *                  database.  
 *  Input(s)      : IkeSessionInfo - session data base.
 *  Output(s)     : tIkeSessionInfo - pointer to the newly 
 *                                    created session.
 *  Return Values : newly created session pointer or NULL. 
 * ---------------------------------------------------------------
 */

INT4
IkeCopyPhase1SessionInfo (tIkeEngine * pIkeEngine,
                          tIkeSessionInfo * pSessionInfo, UINT1 u1Exch)
{
    tIkePolicy         *pIkePolicy = NULL;
    tIkePolicy         *pIkeDefaultPolicy = NULL;
    tIkeKey            *pIkeKey = NULL;

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType ==
            (UINT4) pIkePolicy->PeerAddr.u4AddrType)
        {
            if (pIkePolicy->PeerAddr.u4AddrType == IPV6ADDR)
            {
                if (IKE_MEMCMP (&pSessionInfo->RemoteTunnelTermAddr.uIpAddr,
                                &pIkePolicy->PeerAddr.uIpAddr,
                                sizeof (pIkePolicy->PeerAddr.uIpAddr))
                    == IKE_ZERO)
                {
                    break;
                }
            }
            else if (pIkePolicy->PeerAddr.u4AddrType == IPV4ADDR)
            {
                if (pSessionInfo->RemoteTunnelTermAddr.uIpAddr.Ip4Addr ==
                    pIkePolicy->PeerAddr.uIpAddr.Ip4Addr)
                {
                    break;
                }
            }
        }
        if ((pIkePolicy->PolicyPeerID.uID.Ip4Addr == IKE_DEFAULT_IP) &&
            (pIkePolicy->PolicyPeerID.i4IdType == IPV4ADDR))
        {
            pIkeDefaultPolicy = pIkePolicy;
        }
    }

    if (pIkePolicy != NULL)
    {
        IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkePolicy,
                    sizeof (tIkePolicy));
        GIVE_IKE_SEM ();
    }
    else if (pIkeDefaultPolicy != NULL)
    {
        IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkeDefaultPolicy,
                    sizeof (tIkePolicy));
        GIVE_IKE_SEM ();
    }
    else
    {
        if ((pSessionInfo->u1Role == IKE_RESPONDER) &&
            (u1Exch == IKE_AGGRESSIVE_MODE))
        {
            pIkePolicy = (tIkePolicy *) SLL_FIRST (&pIkeEngine->IkePolicyList);

            if (pIkePolicy == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo:Policy not found!\n");
                return (IKE_FAILURE);
            }

            IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkePolicy,
                        sizeof (tIkePolicy));
            GIVE_IKE_SEM ();
            pSessionInfo->IkePhase1Policy.u1Mode = IKE_AGGRESSIVE_MODE;

            if (pIkePolicy->u2AuthMethod == IKE_RSA_SIGNATURE)
            {
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_RSA_SIGNATURE;
            }
            else if (pIkePolicy->u2AuthMethod == IKE_DSS_SIGNATURE)
            {
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_DSS_SIGNATURE;
            }
            else
            {
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_PRESHARED_KEY;
            }
        }
        else if ((pSessionInfo->u1Role == IKE_RESPONDER) &&
                 (u1Exch == IKE_MAIN_MODE))
        {
            pIkePolicy = (tIkePolicy *) SLL_FIRST (&pIkeEngine->IkePolicyList);

            if (pIkePolicy == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo:Policy not found!\n");
                return (IKE_FAILURE);
            }
            if (pIkePolicy->u2AuthMethod == IKE_RSA_SIGNATURE)
            {
                IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkePolicy,
                            sizeof (tIkePolicy));
                GIVE_IKE_SEM ();
                pSessionInfo->IkePhase1Policy.u1Mode = IKE_MAIN_MODE;
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_RSA_SIGNATURE;
            }
            else if (pIkePolicy->u2AuthMethod == IKE_DSS_SIGNATURE)
            {
                IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkePolicy,
                            sizeof (tIkePolicy));
                GIVE_IKE_SEM ();
                pSessionInfo->IkePhase1Policy.u1Mode = IKE_MAIN_MODE;
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_DSS_SIGNATURE;
            }
            else if (pIkePolicy->u2AuthMethod == IKE_PRESHARED_KEY)
            {
                IKE_MEMCPY (&pSessionInfo->IkePhase1Policy, pIkePolicy,
                            sizeof (tIkePolicy));
                GIVE_IKE_SEM ();
                pSessionInfo->IkePhase1Policy.u1Mode = IKE_MAIN_MODE;
                pSessionInfo->pIkeSA->u2AuthMethod = IKE_PRESHARED_KEY;
            }
            else
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo: Policy not found!\n");
                return (IKE_FAILURE);
            }
        }
        else
        {
            /* MAIN MODE-Preshared key or INITIATOR */
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCopyPhase1SessionInfo: ID Mismatch"
                         "Policy not found!\n");
            return (IKE_FAILURE);
        }
    }

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pSessionInfo->i2SocketId = pIkeEngine->i2V6SocketFd;
        pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd =
            pIkeEngine->i2V6NattSocketFd;
    }
    else
    {

        pSessionInfo->i2SocketId = pIkeEngine->i2SocketId;
        pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd =
            pIkeEngine->i2NattSocketFd;
    }
    pSessionInfo->pIkeSA->u2AuthMethod =
        pSessionInfo->IkePhase1Policy.u2AuthMethod;

    if (pIkePolicy == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCopyPhase1SessionInfo: pIkePolicy is NULL " "\n");
        return IKE_FAILURE;
    }

    /* Store the Initiator ID */
    IKE_MEMCPY (&pSessionInfo->pIkeSA->InitiatorID, &pIkePolicy->PolicyLocalID,
                sizeof (tIkePhase1ID));

    IKE_MEMCPY (&pSessionInfo->pIkeSA->ResponderID, &pIkePolicy->PolicyPeerID,
                sizeof (tIkePhase1ID));

    if (pSessionInfo->u1Role == IKE_INITIATOR)
    {
        IkeGetRandom (pSessionInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    }
    else
    {
        IkeGetRandom (pSessionInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    }

    if (pSessionInfo->pIkeSA->u2AuthMethod == IKE_PRESHARED_KEY)
    {
        /* Search for the preshared key using the Peer Id */
        TAKE_IKE_SEM ();
        SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
        {
            if (pIkeKey->u1Status == IKE_ACTIVE)
            {
                if (pSessionInfo->IkeExchInfo.Phase1Info.IkePolicy.
                    PolicyPeerID.i4IdType == pIkeKey->KeyID.i4IdType)
                {
                    if (IKE_MEMCMP (&pSessionInfo->IkeExchInfo.Phase1Info.
                                    IkePolicy.PolicyPeerID,
                                    &pIkeKey->KeyID,
                                    sizeof (tIkePhase1ID)) == IKE_ZERO)
                    {
                        break;
                    }
                }
            }
        }

        if (pIkeKey != NULL)
        {
            /* Get the Pre-shared Key */
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pSessionInfo->IkePhase1Info.
                 pu1PreSharedKey) == MEM_FAILURE)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo: unable to allocate "
                             "buffer\n");
                return IKE_FAILURE;
            }

            if (pSessionInfo->IkePhase1Info.pu1PreSharedKey == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo:malloc failed!\n");
                return IKE_FAILURE;
            }

            STRCPY (pSessionInfo->IkePhase1Info.pu1PreSharedKey,
                    pIkeKey->au1KeyString);
            pSessionInfo->IkePhase1Info.u2PreSharedKeyLen =
                (UINT2) STRLEN (pIkeKey->au1KeyString);

            /* Fill in the Responder ID */
            pSessionInfo->pIkeSA->ResponderID.i4IdType =
                pIkeKey->KeyID.i4IdType;
            pSessionInfo->pIkeSA->ResponderID.u4Length =
                pIkeKey->KeyID.u4Length;
            IKE_MEMCPY (&pSessionInfo->pIkeSA->ResponderID.uID,
                        &pIkeKey->KeyID.uID, pIkeKey->KeyID.u4Length);

            /* Lookup RA Node */
            if (IkeCopyRAPolicy (pSessionInfo) == IKE_FAILURE)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo:IkeCopyRAPolicy "
                             "Failed!\n");
                return IKE_FAILURE;

            }
        }
        else
        {
            /* If Auth Method is preshared key and exchange mode is
             * Main Mode, fail the session if preshared key is not found. 
             */
            if (pSessionInfo->IkePhase1Policy.u1Mode == IKE_MAIN_MODE)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCopyPhase1SessionInfo:PreShared Key not found "
                             "in Main Mode!\n");
                return IKE_FAILURE;
            }
        }
        GIVE_IKE_SEM ();
    }                            /*  if (pSessionInfo->pIkeSA->u2AuthMethod == IKE_PRESHARED_KEY) */
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelPhase2SessSAForPeer  
 *  Description   : Function to delete all Active Phase2 Sessions 
 *                : and Phase2 SAs for a specified peer   
 *  Input(s)      : pIkeEngine - Pointer to IkeEngine          
 *                : pPeerIpAddr - The Peer IP Address          
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelPhase2SessSAForPeer (tIkeEngine * pIkeEngine, tIkeIpAddr * pPeerIpAddr)
{
    tIkeSessionInfo    *pSessionInfo = NULL, *pPrev = NULL;

    SLL_SCAN (&(pIkeEngine->IkeSessionList), pSessionInfo, tIkeSessionInfo *)
    {
        if ((pSessionInfo->u1ExchangeType == IKE_QUICK_MODE) &&
            (IKE_MEMCMP (&pSessionInfo->RemoteTunnelTermAddr, pPeerIpAddr,
                         sizeof (tIkeIpAddr)) == IKE_ZERO))
        {
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDelPhase2SessSAForPeer: Unable to "
                             "delete Session!\n");
                return (IKE_FAILURE);
            }
            pSessionInfo = pPrev;
        }
        pPrev = pSessionInfo;
    }

    if (pPeerIpAddr->u4AddrType == IPV6ADDR)
    {
        IkeDelPhase2Sa (&pIkeEngine->Ip6TunnelTermAddr, pPeerIpAddr,
                        IKE_ISAKMP_VERSION);
    }
    else
    {
        IkeDelPhase2Sa (&pIkeEngine->Ip4TunnelTermAddr, pPeerIpAddr,
                        IKE_ISAKMP_VERSION);
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelAllPhase2SessSA      
 *  Description   : Function to delete all Active Phase2 Sessions 
 *                : and Phase2 SAs                        
 *  Input(s)      : pIkeEngine - Pointer to IkeEngine          
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelAllPhase2SessSA (tIkeEngine * pIkeEngine)
{
    tIkeSessionInfo    *pSessionInfo = NULL, *pPrev = NULL;

    SLL_SCAN (&(pIkeEngine->IkeSessionList), pSessionInfo, tIkeSessionInfo *)
    {
        if (pSessionInfo->u1ExchangeType == IKE_QUICK_MODE)
        {
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDelAllPhase2SessSA: Unable to "
                             "delete Session!\n");
                return (IKE_FAILURE);
            }
            pSessionInfo = pPrev;
        }
        pPrev = pSessionInfo;
    }
    if (pIkeEngine->Ip6TunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        IkeDelPhase2SaForLocal (&pIkeEngine->Ip6TunnelTermAddr);
    }
    if (pIkeEngine->Ip4TunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        IkeDelPhase2SaForLocal (&pIkeEngine->Ip4TunnelTermAddr);
    }
    return IKE_SUCCESS;
}

INT1
IkeDelPhase1Session (tIkeSessionInfo * pSessionInfo)
{

    if (pSessionInfo->u1Role == IKE_INITIATOR)
    {
        if (pSessionInfo->u1ExchangeType == IKE_MAIN_MODE)
        {
            IkeDeleteSession (pSessionInfo);
            pSessionInfo = NULL;
        }
        else
        {
            /* For Aggressive mode, Initiator sends the
               last packet, so have to wait for any
               retransmissions from peer
             */
            if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               IKE_DELAYED_SESSION_DELETE_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* Not critical error, just log */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeStartTimer "
                             "failed for Delete Session!\n");
            }
        }
    }
    else
    {
        if (pSessionInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE)
        {
            IkeDeleteSession (pSessionInfo);
            pSessionInfo = NULL;
        }
        else if (pSessionInfo->u1ExchangeType == IKE_MAIN_MODE)
        {
            /* For Main mode, Responder sends the
               last packet, so have to wait for any
               retransmissions from peer
             */
            if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               IKE_DELAYED_SESSION_DELETE_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* Not critical error, just log */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSocketHandler: IkeStartTimer "
                             "failed for Delete Session!\n");
            }
        }
    }

    return IKE_SUCCESS;
}

/* This function is called only when Preshared key lookup is successful */
INT1
IkeCopyRAPolicy (tIkeSessionInfo * pSessionInfo)
{
    tIkePhase1ID       *pPeerId = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pRAPolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pSessionInfo->au1EngineName,
                                   (INT4) STRLEN (pSessionInfo->au1EngineName));

    if (pIkeEngine == NULL)
    {
        return (IKE_FAILURE);
    }

    /* Irrespective of the Role the Responder Peer ID will be
     * the Remote Tunnel termination address */

    pPeerId = &(pSessionInfo->pIkeSA->ResponderID);

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pRAPolicy, tIkeRemoteAccessInfo *)
    {
        if (IkeComparePhase1IDs (&pRAPolicy->Phase1ID, pPeerId) == IKE_ZERO)
        {
            if (gbCMServer == TRUE)
            {
                IKE_MEMCPY (&(pRAPolicy->PeerAddr),
                            &(pSessionInfo->RemoteTunnelTermAddr),
                            sizeof (tIkeIpAddr));
            }
            IKE_MEMCPY (&(pSessionInfo->IkeTransInfo.IkeRAInfo), pRAPolicy,
                        sizeof (tIkeRemoteAccessInfo));
            break;
        }
    }

    if (pRAPolicy != NULL)
    {
        pSessionInfo->pIkeSA->bXAuthEnabled = pRAPolicy->bXAuthEnabled;
        pSessionInfo->pIkeSA->bCMEnabled = pRAPolicy->bCMEnabled;
        pSessionInfo->pIkeSA->bXAuthDone = FALSE;
        pSessionInfo->pIkeSA->bCMDone = FALSE;
    }
    else
    {
        pSessionInfo->pIkeSA->bXAuthEnabled = FALSE;
        pSessionInfo->pIkeSA->bCMEnabled = FALSE;
        pSessionInfo->pIkeSA->bXAuthDone = FALSE;
        pSessionInfo->pIkeSA->bCMDone = FALSE;
    }

    return IKE_SUCCESS;
}

/* Get ID from packet */
INT4
IkeGetIDFromPacket (UINT1 *pu1ID, UINT1 *pu1IdType, UINT1 *pIsakmpHdr)
{
    tIdPayLoad          Id;
    tGenericPayLoad     Dummy;
    UINT1              *pu1PayLoad = pIsakmpHdr + sizeof (tIsakmpHdr);
    UINT1               u1PayloadType =
        ((tIsakmpHdr *) (void *) pIsakmpHdr)->u1NextPayload;

    IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, IKE_GEN_PAYLOAD_SIZE);
    Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);

    while (u1PayloadType != IKE_ZERO)
    {
        if (u1PayloadType > IKE_MAX_PAYLOADS)
        {
            return (IKE_FAILURE);
        }

        if (u1PayloadType == IKE_ID_PAYLOAD)
        {
            break;
        }

        u1PayloadType = Dummy.u1NextPayLoad;
        pu1PayLoad += Dummy.u2PayLoadLen;
        IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, IKE_GEN_PAYLOAD_SIZE);
        Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);
    }

    if (u1PayloadType == IKE_ID_PAYLOAD)
    {
        IKE_BZERO (&Id, sizeof (tIdPayLoad));
        IKE_MEMCPY ((UINT1 *) &Id, pu1PayLoad, IKE_ID_PAYLOAD_SIZE);

        IKE_MEMCPY (pu1ID, pu1PayLoad + IKE_ID_PAYLOAD_SIZE,
                    Dummy.u2PayLoadLen - IKE_ID_PAYLOAD_SIZE);

        Id.u2PayLoadLen = IKE_NTOHS (Id.u2PayLoadLen);
        Id.u2Port = IKE_NTOHS (Id.u2Port);
        *pu1IdType = Id.u1IdType;
    }
    else
    {
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGenSkeyId 
 *  Description   : Function to generate key for authentication
 *                  and encryption.
 *  Input(s)      : pSessionInfo - Pointer to session info. 
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT1
IkeGenSkeyId (tIkeSessionInfo * pSessionInfo)
{
    UINT2               u2NonceDataLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4HashLen = IKE_ZERO;

    u2NonceDataLen = (UINT2) (pSessionInfo->u2InitiatorNonceLen +
                              pSessionInfo->u2ResponderNonceLen);

    /* Get the Hash Algorithm length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* This buffer is used for calculating SKEYID */
    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId:Malloc Fails \n");
        return IKE_FAILURE;
    }

    /* Allocate buffer for storing the keys */
    pSessionInfo->pIkeSA->u2SKeyIdLen = (UINT2) u4HashLen;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeSA->pSKeyId) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return IKE_FAILURE;
    }

    if (pSessionInfo->pIkeSA->pSKeyId == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId:Malloc Fails \n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return IKE_FAILURE;
    }

    IKE_MEMCPY (pu1Ptr, pSessionInfo->pu1InitiatorNonce,
                pSessionInfo->u2InitiatorNonceLen);
    IKE_MEMCPY ((pu1Ptr + pSessionInfo->u2InitiatorNonceLen),
                pSessionInfo->pu1ResponderNonce,
                pSessionInfo->u2ResponderNonceLen);

    switch (pSessionInfo->pIkeSA->u2AuthMethod)
    {
        case IKE_PRESHARED_KEY:
            /* Generation of SKEYID */
            /* key is the preshared key and message is
             * the concatenated value of initiator and responder Nonces */
            (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pSessionInfo->IkePhase1Info.pu1PreSharedKey,
                        pSessionInfo->IkePhase1Info.u2PreSharedKeyLen, pu1Ptr,
                        u2NonceDataLen, pSessionInfo->pIkeSA->pSKeyId);

            if (IkeDeriveKeysFromSkeyId (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeGenSkeyId: IkeDeriveKeysFromSkeyId failed \n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
                return IKE_FAILURE;
            }
            break;

        case IKE_RSA_SIGNATURE:
        case IKE_DSS_SIGNATURE:
            (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pu1Ptr, u2NonceDataLen, pSessionInfo->pDhSharedSecret->
                        pu1Value, pSessionInfo->pDhSharedSecret->i4Length,
                        pSessionInfo->pIkeSA->pSKeyId);

            if (IkeDeriveKeysFromSkeyId (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeGenSkeyId: IkeDeriveKeysFromSkeyId failed \n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
                return IKE_FAILURE;
            }
            break;

        case IKE_RSA_ENCRYPTION:
        case IKE_REVISED_RSA_ENCRYPTION:
        default:
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenSkeyId: Unsupported authentication method \n");
            return IKE_FAILURE;
        }
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeriveKeysFromSkeyId                            */
/*  Description     :This function derives skeyid_d, skeyid_a, skeyid_e */
/*                   from skeyid                                        */
/*  Input(s)        :pSessionInfo - Pointer to the session structure    */
/*  Output(s)       :stores skeid_d, skeyid_a, skeyid_e in the session  */
/*                   structure                                          */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeDeriveKeysFromSkeyId (tIkeSessionInfo * pSessionInfo)
{
    UINT1              *pu1TempA = NULL;
    UINT1              *pu1TempE = NULL, *pu1TempD = NULL, *pu1Skeyd = NULL;
    UINT1              *pu1BufPtr = NULL;
    UINT4               u4KeyMatLen = IKE_ZERO,
        u4HashLen = IKE_ZERO, u4EncryKLen = IKE_ZERO;
    UINT4               u4TotalKeyLen = IKE_ZERO, u4DataLen = IKE_ZERO;
    UINT1              *pu1KeyMat = NULL, *pu1Temp = NULL, *pu1KeyData = NULL;
    UINT1               u1Data = IKE_ZERO;

    /* Get the Encryption Algorithm Key length */

    if (pSessionInfo->pIkeSA->u1Phase1EncrAlgo == IKE_AES)
    {
        u4TotalKeyLen =
            IKE_BITS2BYTES ((UINT4) pSessionInfo->IkePhase1Policy.u2KeyLen);
    }
    else
    {
        u4TotalKeyLen = gaIkeCipherAlgoList[pSessionInfo->pIkeSA->
                                            u1Phase1EncrAlgo].u4KeyLen;
    }

    u4EncryKLen = u4TotalKeyLen;

    /* Get the Hash Algorithm length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* For deriving the keys the maximum length required is *
     * SKEYID length + Shared secret length + Initiator cookie len +
     * responder cookie length + 1 : RFC 2409*/

    u4DataLen = (IKE_COOKIE_LEN * IKE_TWO) + IKE_ONE +
        pSessionInfo->pDhInfo->u4Length + u4HashLen;

    /* This buffer is used for calculating the skeyid_d, skeyId_a, skeyid_e */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeriveKeysFromSkeyId: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeSA->pSKeyIdD) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeriveKeysFromSkeyId: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeSA->pSKeyIdA) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeriveKeysFromSkeyId: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeSA->pSKeyIdE) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeriveKeysFromSkeyId: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyId);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyIdE);
        return IKE_FAILURE;
    }

    if ((pSessionInfo->pIkeSA->pSKeyIdD == NULL)
        || (pSessionInfo->pIkeSA->pSKeyIdE == NULL)
        || (pSessionInfo->pIkeSA->pSKeyIdA == NULL) || (pu1BufPtr == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId:Malloc Fails \n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyId);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyIdD);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyIdE);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeSA->pSKeyIdA);
        return IKE_FAILURE;
    }

    pu1TempA = pu1TempE = pu1BufPtr;

    /* Point to the Buffer without auth digest size as it is not required for
     * calculating SKEYID_d*/
    pu1TempD = pu1BufPtr + u4HashLen;
    pu1Skeyd = pu1BufPtr + u4HashLen;

    /* Copy the shared secret */
    IKE_MEMCPY (pu1TempD, pSessionInfo->pDhSharedSecret->pu1Value,
                pSessionInfo->pDhSharedSecret->i4Length);
    pu1TempD += pSessionInfo->pDhSharedSecret->i4Length;

    /* Copy the initiator and responder cookie */
    IKE_MEMCPY (pu1TempD, pSessionInfo->pIkeSA->au1InitiatorCookie,
                IKE_COOKIE_LEN);
    IKE_MEMCPY (pu1TempD + IKE_COOKIE_LEN,
                pSessionInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    pu1TempD += IKE_TWO * IKE_COOKIE_LEN;

    /* For SKEYID_d 0 has to be appended at the end */
    pu1TempD[IKE_INDEX_0] = IKE_ZERO;

    /* Generation of SKEYID_d */
    /* Key is SKEYID and message is the concatenation of *
     * shared secret key + Initiator cookie + responder cookie + 0*/

    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
     HmacAlgo) (pSessionInfo->pIkeSA->pSKeyId,
                pSessionInfo->pIkeSA->u2SKeyIdLen, pu1Skeyd,
                (INT4) (u4DataLen - u4HashLen), pSessionInfo->pIkeSA->pSKeyIdD);

    IKE_MEMCPY (pu1BufPtr, pSessionInfo->pIkeSA->pSKeyIdD, u4HashLen);
    pu1TempD[IKE_INDEX_0] = IKE_ONE;

    /* Generation of SKEYID_a */
    /* Key is SKEYID and message is the concatenation of *
     * SKEID_d + shared secret key + Initiator cookie + responder cookie
     * + 1 */

    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
     HmacAlgo) (pSessionInfo->pIkeSA->pSKeyId,
                pSessionInfo->pIkeSA->u2SKeyIdLen, pu1TempA, (INT4) u4DataLen,
                pSessionInfo->pIkeSA->pSKeyIdA);

    IKE_MEMCPY (pu1BufPtr, pSessionInfo->pIkeSA->pSKeyIdA, u4HashLen);
    pu1TempD[IKE_INDEX_0] = IKE_TWO;

    /* Generation of SKEYID_e */
    /* Key is SKEYID and message is the concatenation of *
     * SKEID_a + shared secret key + Initiator cookie + responder cookie
     * + 1 */

    (*gaIkeHmacAlgoList
     [pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
(pSessionInfo->pIkeSA->pSKeyId,
pSessionInfo->pIkeSA->u2SKeyIdLen, pu1TempE, (INT4) u4DataLen,
pSessionInfo->pIkeSA->pSKeyIdE);

    /* Check if length of key generated is sufficient */

    if (u4TotalKeyLen <= u4HashLen)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pu1KeyMat) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeDeriveKeysFromSkeyId: unable to allocate "
                         "buffer\n");
            return IKE_FAILURE;
        }

        if (pu1KeyMat == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenSkeyId: malloc failed!\n");

            /* For FIPS compliance, we will bzero any secret data before
               releasing the pointer */
            if (pu1BufPtr != NULL)
            {
                IKE_BZERO (pu1BufPtr, u4DataLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
            }
            return (IKE_FAILURE);
        }
        u4KeyMatLen = u4TotalKeyLen;
        IKE_MEMCPY (pu1KeyMat, pSessionInfo->pIkeSA->pSKeyIdE, u4TotalKeyLen);
    }
    else
    {
        /* We need to generate more key material */

        /* Make Total Key Length a multiple of Hash length */
        u4TotalKeyLen = (((u4TotalKeyLen - IKE_ONE)
                          / u4HashLen) + IKE_ONE) * u4HashLen;
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pu1KeyMat) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeDeriveKeysFromSkeyId: unable to allocate "
                         "buffer\n");
            if (pu1BufPtr != NULL)
            {
                IKE_BZERO (pu1BufPtr, u4DataLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
            }
            return IKE_FAILURE;
        }
        if (pu1KeyMat == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenSkeyId: malloc failed!\n");

            if (pu1BufPtr != NULL)
            {
                IKE_BZERO (pu1BufPtr, u4DataLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
            }
            return (IKE_FAILURE);
        }
        u4KeyMatLen = u4TotalKeyLen;
        pu1Temp = pu1KeyMat;
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pu1KeyData) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeDeriveKeysFromSkeyId: unable to allocate "
                         "buffer\n");
            if (pu1KeyMat != NULL)
            {
                IKE_BZERO (pu1KeyMat, u4TotalKeyLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyMat);
            }

            if (pu1BufPtr != NULL)
            {
                IKE_BZERO (pu1BufPtr, u4DataLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
            }
            return IKE_FAILURE;
        }
        if (pu1KeyData == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenSkeyId: malloc failed!\n");

            if (pu1BufPtr != NULL)
            {
                IKE_BZERO (pu1BufPtr, u4DataLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
            }

            if (pu1KeyMat != NULL)
            {
                IKE_BZERO (pu1KeyMat, u4TotalKeyLen);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyMat);
            }
            return (IKE_FAILURE);
        }

        /* Generate the first part of the key */
        (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
         HmacAlgo) (pSessionInfo->pIkeSA->pSKeyIdE,
                    (INT4) u4HashLen, &u1Data, sizeof (UINT1), pu1Temp);
        u4TotalKeyLen -= u4HashLen;
        while (u4TotalKeyLen != IKE_ZERO)
        {
            IKE_MEMCPY (pu1KeyData, pu1Temp, u4HashLen);
            pu1Temp += u4HashLen;
            (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pSessionInfo->pIkeSA->pSKeyIdE,
                        (INT4) u4HashLen, pu1KeyData, (INT4) u4HashLen,
                        pu1Temp);
            u4TotalKeyLen -= u4HashLen;
        }

        if (pu1KeyData != NULL)
        {
            IKE_BZERO (pu1KeyData, u4HashLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyData);
        }
    }

    /* Incase the Encrypt Key Len is greater than Hash len, then
     * more key material would have got generated in the KeyMat but 
     * not updated in the pSkeyIdE, update the same. */

    IKE_MEMCPY (pSessionInfo->pIkeSA->pSKeyIdE, pu1KeyMat, u4EncryKLen);

    pSessionInfo->pIkeSA->u2EncrKLen = (UINT2) u4EncryKLen;

    if (IkeSetPhase1Key (pSessionInfo, pu1KeyMat) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId: IkeSetPhase1Key failed! \n");

        if (pu1BufPtr != NULL)
        {
            IKE_BZERO (pu1BufPtr, u4DataLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        }

        if (pu1KeyMat != NULL)
        {
            IKE_BZERO (pu1KeyMat, u4KeyMatLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyMat);
        }
        return (IKE_FAILURE);
    }

    if (IkeGenIV (pSessionInfo, IKE_PHASE1) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenSkeyId: IkeGenIV failed! \n");

        if (pu1BufPtr != NULL)
        {
            IKE_BZERO (pu1BufPtr, u4DataLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
        }

        if (pu1KeyMat != NULL)
        {
            IKE_BZERO (pu1KeyMat, u4KeyMatLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyMat);
            /*     MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Temp); */
        }
        return (IKE_FAILURE);
    }

    if (pu1KeyMat != NULL)
    {
        IKE_BZERO (pu1KeyMat, u4KeyMatLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1KeyMat);
        /*   MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Temp); */
    }

    if (pu1BufPtr != NULL)
    {
        IKE_BZERO (pu1BufPtr, u4DataLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1BufPtr);
    }
    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGenIPSecKeyMat
 *  Description   : Function to generate key Material for IPSec
 *  Input(s)      : pSessionInfo - Pointer to session info. 
 *                : pIPSecSA - This is the IPSecSA from which we
 *                : get the info for Key gen and where we store the key
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT1
IkeGenIPSecKeyMat (tIkeSessionInfo * pSessionInfo, tIPSecSA * pIPSecSA)
{
    UINT2               u2NonceDataLen = IKE_ZERO, u2DataLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL, *pu1Temp = NULL;
    UINT4               u4Spi = IKE_ZERO;
    UINT4               u4KeyLen = IKE_ZERO,
        u4TotalKeyLen = IKE_ZERO, u4HashLen = IKE_ZERO;
    UINT1              *pu1Key = NULL, u1HashAlgo = IKE_ZERO,
        u1EncrAlgo = IKE_ZERO;

    if (pIPSecSA->u1IPSecProtocol == IKE_IPSEC_AH)
    {
        /*
           u1HashAlgo = gau1AHToIKEHashAlgo[pIPSecSA->u1HashAlgo];
         */
        u1HashAlgo = pIPSecSA->u1HashAlgo;
        pIPSecSA->u2AuthKeyLen = (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
        pIPSecSA->u2EncrKeyLen = IKE_ZERO;
    }
    else if (pIPSecSA->u1IPSecProtocol == IKE_IPSEC_ESP)
    {
        if (pIPSecSA->u1HashAlgo != IKE_ZERO)
        {
            u1HashAlgo = gau1ESPToIKEHashAlgo[pIPSecSA->u1HashAlgo];
            pIPSecSA->u2AuthKeyLen =
                (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
        }
        else
        {
            /* Hash algo is not configured and ESP is configured NULL
             * no need to generate keys */
            if (pIPSecSA->u1EncrAlgo == IKE_IPSEC_ESP_NULL)
            {
                return (IKE_SUCCESS);
            }
        }

        u1EncrAlgo = gau1IPSecToIKEEncrAlgo[pIPSecSA->u1EncrAlgo];
        if (pIPSecSA->u1EncrAlgo == IKE_IPSEC_ESP_AES)
        {
            if (pSessionInfo->IkePhase2Policy.aTransformSetBundle[IKE_INDEX_0]
                != NULL)
            {
                pIPSecSA->u2EncrKeyLen = (UINT2)
                    ((pSessionInfo->IkePhase2Policy.
                      aTransformSetBundle[IKE_INDEX_0]->u2KeyLen) / IKE_EIGHT);
            }
        }
        else if (pIPSecSA->u1EncrAlgo == IKE_IPSEC_ESP_AES_CTR)
        {
            if (pSessionInfo->IkePhase2Policy.aTransformSetBundle[IKE_INDEX_0]
                != NULL)
            {
                pIPSecSA->u2EncrKeyLen = (UINT2)
                    ((pSessionInfo->IkePhase2Policy.
                      aTransformSetBundle[IKE_INDEX_0]->u2KeyLen) / IKE_EIGHT);
            }
        }

        else
        {
            pIPSecSA->u2EncrKeyLen =
                (UINT2) gaIkeCipherAlgoList[u1EncrAlgo].u4KeyLen;
        }
    }
    else
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeGenIPSecKeyMat: %d Protocol not supported \n",
                      pIPSecSA->u1IPSecProtocol);
        return (IKE_FAILURE);
    }

    if (pIPSecSA->u1EncrAlgo != IKE_IPSEC_ESP_AES_CTR)
    {

        u4TotalKeyLen =
            (UINT4) (pIPSecSA->u2AuthKeyLen + pIPSecSA->u2EncrKeyLen);
    }
    else
    {
        u4TotalKeyLen =
            (UINT4) (pIPSecSA->u2AuthKeyLen + pIPSecSA->u2EncrKeyLen +
                     IKE_NONCE_LEN);

    }

    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Make Total Key Len a multiple of Hash Length */
    u4TotalKeyLen =
        (((u4TotalKeyLen - IKE_ONE) / u4HashLen) + IKE_ONE) * u4HashLen;

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Key)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenIPSecKeyMat: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Key == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenIPSecKeyMat:Malloc Failed \n");
        pIPSecSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecSA->u2EncrKeyLen = IKE_ZERO;
        return (IKE_FAILURE);
    }
    u4KeyLen = u4TotalKeyLen;

    u4Spi = IKE_HTONL (pIPSecSA->u4Spi);
    u2NonceDataLen = (UINT2) (pSessionInfo->u2InitiatorNonceLen +
                              pSessionInfo->u2ResponderNonceLen);
    u2DataLen = (UINT2)
        (IKE_PROTOCOL_ID_SIZE + sizeof (pIPSecSA->u4Spi) + u2NonceDataLen);

    /* If PFS Enabled, allocate for Shared Secret also */
    if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
    {
        if (pSessionInfo->pDhSharedSecret == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenIPSecKeyMat:PFS Enabled and Shared Secret is "
                         "NULL!\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
            return (IKE_FAILURE);
        }
        u2DataLen =
            (UINT2) (u2DataLen + pSessionInfo->pDhSharedSecret->i4Length);
    }

    /* Adding u2HashLen to allow concatenation */

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenIPSecKeyMat: unable to allocate " "buffer\n");
        pIPSecSA->u2AuthKeyLen = 0;
        pIPSecSA->u2EncrKeyLen = 0;
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
        return IKE_FAILURE;
    }

    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenIPSecKeyMat:Malloc Failed \n");
        pIPSecSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecSA->u2EncrKeyLen = IKE_ZERO;
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
        return (IKE_FAILURE);
    }
    /* Give offset for previous key material */
    pu1Temp = pu1Ptr + u4HashLen;
    if (pSessionInfo->IkePhase2Policy.u1Pfs != IKE_ZERO)
    {
        IKE_MEMCPY (pu1Temp, pSessionInfo->pDhSharedSecret->pu1Value,
                    pSessionInfo->pDhSharedSecret->i4Length);
        pu1Temp += pSessionInfo->pDhSharedSecret->i4Length;
    }
    IKE_MEMCPY (pu1Temp, &pIPSecSA->u1IPSecProtocol, IKE_PROTOCOL_ID_SIZE);
    pu1Temp += IKE_PROTOCOL_ID_SIZE;
    IKE_MEMCPY (pu1Temp, &u4Spi, sizeof (u4Spi));
    pu1Temp += sizeof (u4Spi);
    IKE_MEMCPY (pu1Temp, pSessionInfo->pu1InitiatorNonce,
                pSessionInfo->u2InitiatorNonceLen);
    pu1Temp += pSessionInfo->u2InitiatorNonceLen;
    IKE_MEMCPY (pu1Temp, pSessionInfo->pu1ResponderNonce,
                pSessionInfo->u2ResponderNonceLen);
    pu1Temp += pSessionInfo->u2ResponderNonceLen;

    /* Generating key material */
    pu1Temp = pu1Key;
    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSessionInfo->pIkeSA->pSKeyIdD, (INT4) u4HashLen, (pu1Ptr + u4HashLen),
         (INT4) u2DataLen, pu1Temp);
    u4TotalKeyLen -= u4HashLen;
    while (u4TotalKeyLen != IKE_ZERO)
    {
        IKE_MEMCPY (pu1Ptr, pu1Temp, u4HashLen);
        pu1Temp += u4HashLen;
        (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
            (pSessionInfo->pIkeSA->pSKeyIdD, (INT4) u4HashLen, pu1Ptr,
             (INT4) (u2DataLen + u4HashLen), pu1Temp);
        u4TotalKeyLen -= u4HashLen;
    }

    if ((pIPSecSA->u2AuthKeyLen > IKE_MAX_KEY_LEN) ||
        (pIPSecSA->u2EncrKeyLen > IKE_MAX_KEY_LEN))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenIPSecKeyMat:Invalid Key Length \n");
        pIPSecSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecSA->u2EncrKeyLen = IKE_ZERO;
        IKE_BZERO (pu1Ptr, (u2DataLen + u4HashLen));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return (IKE_FAILURE);
    }

    if (pIPSecSA->u1IPSecProtocol == IKE_IPSEC_AH)
    {
        IKE_MEMCPY (pIPSecSA->au1AuthKey, pu1Key, pIPSecSA->u2AuthKeyLen);
    }
    else if (pIPSecSA->u1IPSecProtocol == IKE_IPSEC_ESP)
    {
        if (pIPSecSA->u1EncrAlgo != IKE_IPSEC_ESP_AES_CTR)
        {
            pu1Temp = pu1Key;
            IKE_MEMCPY (pIPSecSA->au1EncrKey, pu1Temp, pIPSecSA->u2EncrKeyLen);
            pu1Temp += pIPSecSA->u2EncrKeyLen;
            IKE_MEMCPY (pIPSecSA->au1AuthKey, pu1Temp, pIPSecSA->u2AuthKeyLen);
        }
        else
        {
            pu1Temp = pu1Key;
            IKE_MEMCPY (pIPSecSA->au1EncrKey, pu1Temp,
                        (pIPSecSA->u2EncrKeyLen));
            pu1Temp += (pIPSecSA->u2EncrKeyLen);
            IKE_MEMCPY (pIPSecSA->au1Nonce, pu1Temp, IKE_NONCE_LEN);
            pu1Temp += IKE_NONCE_LEN;
            IKE_MEMCPY (pIPSecSA->au1AuthKey, pu1Temp, pIPSecSA->u2AuthKeyLen);
        }
    }

    /* We memset to 0 for FIPS compliance */
    IKE_BZERO (pu1Ptr, (u2DataLen + u4HashLen));
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
    IKE_BZERO (pu1Key, u4KeyLen);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
    return (IKE_SUCCESS);
}
