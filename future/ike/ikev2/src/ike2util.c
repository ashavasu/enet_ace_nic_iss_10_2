
/**********************************************************************/
/*  Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/*  $Id: ike2util.c,v 1.8 2014/05/02 10:49:39 siva Exp $            */
/*                                                                    */
/*  Description:This file contains utility functions for ikev2        */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_UTIL_C_
#define _IKE2_UTIL_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :Ike2UtilCheckKBTrasmitted                          */
/*  Description     :This function is used to start rekey for IKE SA    */
/*                  :based upon the no.of bytes transmitted             */
/*  Input(s)        :pSessionInfo - Session Ifo structure               */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/
INT1
Ike2UtilCheckKBTrasmitted (tIkeSessionInfo * pSessionInfo, UINT1 u1Flag)
{
    UINT4               u4Len = IKE_ZERO;
    tIkeSA             *pIkeSA = NULL;

    pIkeSA = pSessionInfo->pIkeSA;

    if (u1Flag == IKE_ENCRYPTION)
    {
        u4Len = IKE_NTOHL
            (((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.
                                       pu1RawPkt))->u4TotalLen);
    }
    else
    {
        u4Len = IKE_NTOHL
            (((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.
                                       pu1RawPkt))->u4TotalLen);
    }
    if (u4Len == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2UtilCheckKBTrasmitted: Isakmp Header is NULL\n");
        return (IKE_FAILURE);
    }
    if (pIkeSA->u4LifeTimeKB != IKE_ZERO)
    {
        pSessionInfo->pIkeSA->u4ByteCount += (u4Len - sizeof (tIsakmpHdr));

        /* Check if soft lifetime expired */
        if (pIkeSA->u4ByteCount > pIkeSA->u4SoftLifeTimeBytes)
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to rekey */
            if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                               IKE_REKEY_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2UtilCheckKBTrasmitted: IkeStartTimer "
                             "failed!\n");
                return (IKE_FAILURE);
            }
        }

        /* If Hard Lifetime expires... */
        if (pIkeSA->u4ByteCount > (pIkeSA->u4LifeTimeKB * IKE_BYTES_PER_KB))
        {
            /* Stop the Lifetime timer if already running */
            IkeStopTimer (&pIkeSA->IkeSALifeTimeTimer);
            /* Start a 1 second timer to post the event to delete SA */
            if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2UtilCheckKBTrasmitted: IkeStartTimer "
                             "failed!\n");
                return (IKE_FAILURE);
            }
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2UtilFreeProposal                               */
/*  Description     :This function is used to free the proposal info    */
/*                  :structure                                          */
/*  Input(s)        :pNode - The proposal(tIkeProposal) Node            */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
Ike2UtilFreeProposal (t_SLL_NODE * pNode)
{
    tIkeProposal       *pIkeProposal = NULL;
    tIkeProposal       *pTempIkeProposal = NULL;

    pIkeProposal = (tIkeProposal *) pNode;
    SLL_DELETE_LIST (&(pIkeProposal->IkeTransPayLoadList),
                     Ike2UtilFreeTransform);

    while (pIkeProposal->pNextProposal != NULL)
    {
        pTempIkeProposal = pIkeProposal->pNextProposal;
        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal);
        pIkeProposal = NULL;
        pIkeProposal = pTempIkeProposal;
    }
    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal);
    return;

}

/************************************************************************/
/*  Function Name   :Ike2UtilFreeTransform                              */
/*  Description     :This function is used to free the transform info   */
/*                  :structure                                          */
/*  Input(s)        :pNode - The transform(tIke2Trans) Node             */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
Ike2UtilFreeTransform (t_SLL_NODE * pNode)
{
    tIke2Trans         *pIkeTrans = NULL;

    pIkeTrans = (tIke2Trans *) pNode;
    MemReleaseMemBlock (gIke2TransMemPoolId, (UINT1 *) pIkeTrans);
    return;
}

/************************************************************************/
/*   Function Name : Ike2UtilGetInSpi                                   */
/*   Description   : Function to retrieve inbound spi from Session      */
/*                 : Info structure.                                    */
/*                 :                                                    */
/*   Input(s)      : u1ProtocolId - Protocol Id of the proposal.        */
/*                 : pSesInfo     - Session Info pointer.               */
/*   Output(s)     : None.                                              */
/*   Return Values : u4Spi or IKE_FALSE                                 */
/************************************************************************/

UINT4
Ike2UtilGetInSpi (UINT1 u1ProtocolId, tIkeSessionInfo * pSesInfo)
{
    UINT4               u4Spi = IKE_ZERO;

    switch (u1ProtocolId)
    {
        case IKE_IPSEC_AH:
        {
            u4Spi = pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4Spi;
            break;
        }
        case IKE_IPSEC_ESP:
        {
            u4Spi = pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2UtilGetInSpi:Invalid Protocol - No Spi "
                         "present\n");
            return (IKE_FALSE);
        }
    }
    return (u4Spi);
}

/************************************************************************/
/*  Function Name   :Ike2UtilIkeSADeleteRequest                         */
/*  Description     :This function initializes and sends out delete     */
/*                  :info message                                       */
/*  Input(s)        :pIkeSA - pointer to tIkeSA structure               */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/
INT1
Ike2UtilIkeSADeleteRequest (tIkeSA * pIkeSA)
{
    tIkeNotifyInfo     *pNotifyInfo = NULL;
    tIkeSessionInfo    *pSessionInfo = NULL;

    /* Allocate memory for holding Notification information */
    if (MemAllocateMemBlock
        (IKE_NOTIFY_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pNotifyInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2UtilIkeSADeleteRequest: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (pNotifyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2UtilIkeSADeleteRequest: Malloc failed for "
                     "Notify Info\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (pNotifyInfo, sizeof (tIkeNotifyInfo));

    pNotifyInfo->u1Protocol = IKE_ISAKMP;
    pNotifyInfo->u2NotifyType = IKE_POST_P1_DELETE;
    pNotifyInfo->u1SpiSize = IKE_ZERO;
    pNotifyInfo->u2NotifyDataLen = IKE_ZERO;

    pSessionInfo = Ike2SessInfoExchSessInit (pIkeSA, IKE_ZERO, IKE_INITIATOR);
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2UtilIkeSADeleteRequest: Info Session "
                     "initialization failed\n");
        MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
        return IKE_FAILURE;
    }
    MEMCPY (&(pSessionInfo->PostPhase1Info.IkeNotifyInfo),
            pNotifyInfo, sizeof (tIkeNotifyInfo));

    if ((Ike2CoreProcess (pSessionInfo)) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2UtilIkeSADeleteRequest: Send Delete SA "
                     "Informational Msg Failed!\n");
        MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
        IkeHandleCoreFailure (pSessionInfo);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_NOTIFY_INFO_MEMPOOL_ID, (UINT1 *) pNotifyInfo);
    IkeHandleCoreSuccess (pSessionInfo);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :Ike2UtilReleaseBuf                                 */
/*  Description     :This function releases the buffer                  */
/*                  :                                                   */
/*  Input(s)        :pu1Buf - Buffer                                    */
/*                  :u4Len  - Buffer length                             */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/
VOID
Ike2UtilReleaseBuf (UINT1 **pu1Buf, UINT4 u4Len)
{
    if (*pu1Buf != NULL)
    {
        IKE_BZERO (*pu1Buf, u4Len);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) (*pu1Buf));
        *pu1Buf = NULL;
    }
    return;
}

/************************************************************************/
/*  Function Name   :Ike2UtilHexDump                                    */
/*  Description     :This function dumps the buffer                     */
/*                  :                                                   */
/*  Input(s)        :pu1Buf - Buffer                                    */
/*                  :u4Le n - Buffer length                             */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/
VOID
Ike2UtilHexDump (UINT1 *pu1Buf, UINT4 u4Len)
{
    UINT4               u4Count = IKE_ZERO;
    for (u4Count = IKE_ZERO; u4Count < (u4Len); u4Count++)
    {
        if (u4Count % IKE2_DIVSOR_16 == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "\n");
        }
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      " %02x ", *(pu1Buf + u4Count));
    }
    UNUSED_PARAM (pu1Buf);
    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "\n");
}

/******************************************************************************/
/*  Function Name : Ike2UtilGetTsInfo                                         */
/*  Description   : This function Gets the Access netowrk information         */
/*                : from the traffic selector in the traffic selector payload */
/*                :                                                           */
/*  Input(s)      : pu1TsPayload - Pointer to the Traffic Selector Payload    */
/*                :                Pointing to the First Traffic Selector     */
/*                : pIkeNetwork  - Gets Network from TS payload               */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo is updated with the new fsm state.           */
/*  Return        : NONE                                                      */
/******************************************************************************/
VOID
Ike2UtilGetTsInfo (UINT1 *pu1TsPayload, tIkeNetwork * pIkeNetwork)
{
    UINT1              *pu1TempPtr = pu1TsPayload;
    tIke2TsInfo         TsInfo;

    IKE_BZERO ((UINT1 *) &TsInfo, sizeof (tIke2TsInfo));

    IKE_MEMCPY ((UINT1 *) &TsInfo, pu1TempPtr, IKE2_TS_SEL_LEN);
    pu1TempPtr += IKE2_TS_SEL_LEN;

    TsInfo.u2SelectorLen = IKE_NTOHS (TsInfo.u2SelectorLen);
    TsInfo.u2StartPort = IKE_NTOHS (TsInfo.u2StartPort);
    TsInfo.u2EndPort = IKE_NTOHS (TsInfo.u2EndPort);

    if (TsInfo.u1TSType == IKE2_TS_IPV4_ADDR_RANGE)
    {
        IKE_MEMCPY (&(TsInfo.StartAddr.Ipv4Addr), pu1TempPtr,
                    sizeof (tIp4Addr));
        pu1TempPtr += sizeof (tIp4Addr);
        IKE_MEMCPY (&(TsInfo.EndAddr.Ipv4Addr), pu1TempPtr, sizeof (tIp4Addr));
        pu1TempPtr += sizeof (tIp4Addr);
        TsInfo.StartAddr.Ipv4Addr = IKE_HTONL (TsInfo.StartAddr.Ipv4Addr);
        TsInfo.EndAddr.Ipv4Addr = IKE_HTONL (TsInfo.EndAddr.Ipv4Addr);
        /* Fill the IkeNetwork */
        pIkeNetwork->u4Type = IKE2_TS_IPV4_ADDR_RANGE;
        pIkeNetwork->u1HLProtocol = TsInfo.u1ProtocolId;
        pIkeNetwork->u2StartPort = TsInfo.u2StartPort;
        pIkeNetwork->u2EndPort = TsInfo.u2EndPort;

        pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr = TsInfo.StartAddr.Ipv4Addr;
        pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr = TsInfo.EndAddr.Ipv4Addr;
    }
    else
    {
        IKE_MEMCPY (&(TsInfo.StartAddr.Ipv6Addr), pu1TempPtr,
                    sizeof (tIp6Addr));
        pu1TempPtr += sizeof (tIp6Addr);
        IKE_MEMCPY (&(TsInfo.EndAddr.Ipv6Addr), pu1TempPtr, sizeof (tIp6Addr));
        pu1TempPtr += sizeof (tIp6Addr);
        /* Fill the IkeNetwork */
        pIkeNetwork->u4Type = IKE2_TS_IPV6_ADDR_RANGE;
        pIkeNetwork->u1HLProtocol = TsInfo.u1ProtocolId;
        pIkeNetwork->u2StartPort = TsInfo.u2StartPort;
        pIkeNetwork->u2EndPort = TsInfo.u2EndPort;

        IKE_MEMCPY (&(pIkeNetwork->uNetwork.Ip6Range.Ip6StartAddr),
                    &(TsInfo.StartAddr.Ipv6Addr), sizeof (tIp6Addr));
        IKE_MEMCPY (&(pIkeNetwork->uNetwork.Ip6Range.Ip6EndAddr),
                    &(TsInfo.EndAddr.Ipv6Addr), sizeof (tIp6Addr));
    }
    return;
}

/*************************************************************************/
/*  Function Name : Ike2UtilSetNonceInSessionInfo                        */
/*  Description   : Used copy Nonce to Session structure                 */
/*  Input(s)      : pSesInfo - The Session Info structure                */
/*                  pu1Ptr - Pointer has nonce value.                    */
/*                  u2NonceLen - Nonce Length.                           */
/*                  u1NonceType - Whether Initiator/Responder Nonce      */
/*  Output(s)     : None.                                                */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                           */
/*************************************************************************/
INT4
Ike2UtilSetNonceInSessionInfo (tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr,
                               UINT2 u2NonceLen, UINT1 u1NonceType)
{
    tIkeSA             *pIkeSA = NULL;

    /* Select the IKESA based upon the Exchange type */
    if ((pSesInfo->u1ExchangeType == IKE2_CHILD_EXCH) &&
        (pSesInfo->u1ChildType == IKE2_IPSEC_REKEY_IKESA))
    {
        pIkeSA = pSesInfo->pIkeReKeySA;
    }
    else
    {
        pIkeSA = pSesInfo->pIkeSA;
    }

    if (u1NonceType == IKE_RESPONDER)
    {
        /* If creating the first child failed during the auth exchange,
         * then use the memory already allocated */
        if (pIkeSA->Ike2SA.pu1RespNonce == NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeSA->Ike2SA.pu1RespNonce) ==
                MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2UtilSetNonceInSessionInfo: not able to allocate "
                             " the buffer\n");
                return IKE_FAILURE;
            }
            if (pIkeSA->Ike2SA.pu1RespNonce == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                             "Ike2UtilSetNonceInSessionInfo: Memory allocation"
                             " failed for Responder Nonce\n");
                return (IKE_FAILURE);
            }
        }
        IKE_BZERO (pIkeSA->Ike2SA.pu1RespNonce,
                   (size_t) (u2NonceLen + IKE_ONE));
        IKE_MEMCPY (pIkeSA->Ike2SA.pu1RespNonce, pu1Ptr, u2NonceLen);
        pIkeSA->Ike2SA.u2RespNonceLen = u2NonceLen;

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->pu1ResponderNonce) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2UtilSetNonceInSessionInfo: unable to allocate "
                         "buffer\n");

            if (pIkeSA->Ike2SA.pu1RespNonce != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeSA->Ike2SA.pu1RespNonce);
                pIkeSA->Ike2SA.pu1RespNonce = NULL;
            }

            return IKE_FAILURE;
        }

        if (pSesInfo->pu1ResponderNonce == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2UtilSetNonceInSessionInfo: Out Of Memory\n");
            if (pIkeSA->Ike2SA.pu1RespNonce != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeSA->Ike2SA.pu1RespNonce);
                pIkeSA->Ike2SA.pu1RespNonce = NULL;
            }

            return IKE_FAILURE;
        }

        IKE_BZERO (pSesInfo->pu1ResponderNonce, u2NonceLen);
        IKE_MEMCPY (pSesInfo->pu1ResponderNonce, pu1Ptr, u2NonceLen);
        pSesInfo->u2ResponderNonceLen = u2NonceLen;
    }
    else
    {
        /* If creating the first child failed during the auth exchange,
         * then use the memory already allocated */
        if (pIkeSA->Ike2SA.pu1InitNonce == NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeSA->Ike2SA.pu1InitNonce) ==
                MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2UtilSetNonceInSessionInfo: unable to allocate "
                             "buffer\n");
                return IKE_FAILURE;
            }
            if (pIkeSA->Ike2SA.pu1InitNonce == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                             "Ike2UtilSetNonceInSessionInfo: Memory allocation"
                             " failed for Initiator Nonce \n");
                return (IKE_FAILURE);
            }
        }
        IKE_BZERO (pIkeSA->Ike2SA.pu1InitNonce,
                   (size_t) (u2NonceLen + IKE_ONE));
        IKE_MEMCPY (pIkeSA->Ike2SA.pu1InitNonce, pu1Ptr, u2NonceLen);
        pIkeSA->Ike2SA.u2InitNonceLen = u2NonceLen;

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->pu1InitiatorNonce) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSetNonceInSessionInfo: unable to allocate "
                         "buffer\n");
            if (pIkeSA->Ike2SA.pu1InitNonce != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeSA->Ike2SA.pu1InitNonce);
                pIkeSA->Ike2SA.pu1InitNonce = NULL;
            }
            return IKE_FAILURE;
        }

        if (pSesInfo->pu1InitiatorNonce == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2UtilSetNonceInSessionInfo: Out Of Memory\n");
            if (pIkeSA->Ike2SA.pu1InitNonce != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeSA->Ike2SA.pu1InitNonce);
                pIkeSA->Ike2SA.pu1InitNonce = NULL;
            }
            return IKE_FAILURE;
        }

        IKE_BZERO (pSesInfo->pu1InitiatorNonce, u2NonceLen);
        IKE_MEMCPY (pSesInfo->pu1InitiatorNonce, pu1Ptr, u2NonceLen);
        pSesInfo->u2InitiatorNonceLen = u2NonceLen;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name : Ike2UtilGenIV                                       */
/*  Description   : Function to generate initial vector                 */
/*  Input(s)      : pSesInfo - The Session Info structure               */
/*                  u1Phase  - Specifies the negotiation                */
/*                :                                                     */
/*  Output(s)     : Updated pointer for pSessionInfo IV                 */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                          */
/************************************************************************/
INT4
Ike2UtilGenIV (tIkeSessionInfo * pSessionInfo)
{
    tIkeSA             *pIkeSA = NULL;
    UINT1               au1IV[IKE_MAX_DIGEST_SIZE + IKE_INDEX_1] = { IKE_ZERO };
    UINT4               u4IVLen = IKE_ZERO;

    IKE_BZERO (au1IV, IKE_MAX_DIGEST_SIZE + IKE_ONE);

    /* Select the IKESA based upon the Exchange type */
    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else
    {
        pIkeSA = pSessionInfo->pIkeReKeySA;
    }

    u4IVLen = gaIke2CipherAlgoList[pIkeSA->u1Phase1EncrAlgo].u4BlockLen;

    if (u4IVLen == IKE_ZERO)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2UtilGenIV: Unknown Encr Algorithm %d\n",
                      pIkeSA->u1Phase1EncrAlgo);
        return (IKE_FAILURE);
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeSA->pu1FinalPhase1IV) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2UtilGenIV: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }

    if (pIkeSA->pu1FinalPhase1IV == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2UtilGenIV: Malloc failed for Initialization "
                     "vector \n");
        return (IKE_FAILURE);
    }

    IKE_BZERO (pIkeSA->pu1FinalPhase1IV, u4IVLen);
    IkeGetRandom (au1IV, (INT4) u4IVLen);

    IKE_MEMCPY (pIkeSA->pu1FinalPhase1IV, au1IV, u4IVLen);

    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2UtilGetNextPayloadAndType                             */
/*  Description   : This function returns the Next payload in the packet      */
/*                :                                                           */
/*  Input(s)      : pu1Payload      - Pointer to the Payload                  */
/*                : pu1NextPayload  - Pointer to the Next Payload type to be  */
/*                :                   returned.                               */
/*                : pu2CurPaylodLen - Pointer to fill the current payload len */
/*                :                                                           */
/*  Output(s)     : pu1NextPayload is filled with the next payload type       */
/*                : and the payload pointer is moved to point to the next.    */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
VOID
Ike2UtilGetNextPayloadAndType (UINT1 *pu1Payload, UINT1 *pu1NextPayloadType,
                               UINT2 *pu2CurPaylodLen)
{
    /* The payloads are already validated for length etc, so no need 
     * of further checks */
    tGenericPayLoad    *pIkeGenPayload = NULL;
    pIkeGenPayload = (tGenericPayLoad *) (void *) pu1Payload;
    *pu1NextPayloadType = pIkeGenPayload->u1NextPayLoad;
    *pu2CurPaylodLen = IKE_NTOHS (pIkeGenPayload->u2PayLoadLen);
    return;
}

/******************************************************************************/
/*  Function Name : Ike2UtilUpdateCryptoForRA                                 */
/*  Description   : This function is used to copy cryptomap to the sessioninfo*/
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo is updated with the new fsm state.           */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2UtilUpdateCryptoForRA (tIkeSessionInfo * pSesInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkeNetwork         DstNet;
    tIkeNetwork         SrcNet;

    IKE_BZERO (&DstNet, sizeof (tIkeNetwork));
    IKE_BZERO (&SrcNet, sizeof (tIkeNetwork));

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2UtilUpdateCryptoForRA: IkeGetIkeEngineFromIndex(%d)"
                      " failed\n", pSesInfo->u4IkeEngineIndex);
        return (IKE_FAILURE);
    }
    if (gbCMServer == IKE_TRUE)
    {
        if (pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
            u4AddrType == IPV4ADDR)
        {
            DstNet.u4Type = IKE_IPSEC_ID_IPV4_SUBNET;
            DstNet.uNetwork.Ip4Subnet.Ip4Addr =
                pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.InternalIpAddr.
                Ipv4Addr;

            DstNet.uNetwork.Ip4Subnet.Ip4SubnetMask = IP4_SUB_MASK_FFFFFFFF;
            DstNet.u2StartPort = IKE_TS_MIN_PORT;
            DstNet.u2EndPort = IKE_TS_MAX_PORT;
            IKE_MEMCPY (&SrcNet,
                        &(pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                          ProtectedNetwork[IKE_INDEX_0]), sizeof (tIkeNetwork));
        }
        else
        {
            DstNet.u4Type = IKE_IPSEC_ID_IPV6_SUBNET;
            IKE_MEMCPY (&(DstNet.uNetwork.Ip6Subnet.Ip6Addr),
                        &(pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                          InternalIpAddr.Ipv6Addr), sizeof (tIp6Addr));

            IKE_MEMCPY (&(DstNet.uNetwork.Ip6Subnet.Ip6SubnetMask),
                        &gaIp6PrefixToMask[IKE_INDEX_127], sizeof (tIp6Addr));
            DstNet.u2StartPort = IKE_TS_MIN_PORT;
            DstNet.u2EndPort = IKE_TS_MAX_PORT;
            IKE_MEMCPY (&SrcNet,
                        &(pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                          ProtectedNetwork[IKE_INDEX_0]), sizeof (tIkeNetwork));
        }
    }
    else
    {
        if (pSesInfo->Ike2AuthInfo.InternalIpAddr.u4AddrType == IPV4ADDR)
        {
            SrcNet.u4Type = IKE_IPSEC_ID_IPV4_SUBNET;
            SrcNet.uNetwork.Ip4Subnet.Ip4Addr =
                pSesInfo->Ike2AuthInfo.InternalIpAddr.Ipv4Addr;

            SrcNet.uNetwork.Ip4Subnet.Ip4SubnetMask = IP4_SUB_MASK_FFFFFFFF;
            SrcNet.u2StartPort = IKE_TS_MIN_PORT;
            SrcNet.u2EndPort = IKE_TS_MAX_PORT;
            IKE_MEMCPY (&DstNet, &(pSesInfo->Ike2AuthInfo.
                                   ProtectedNetwork[IKE_INDEX_0]),
                        sizeof (tIkeNetwork));
            DstNet.u2StartPort = IKE_TS_MIN_PORT;
            DstNet.u2EndPort = IKE_TS_MAX_PORT;
        }
        else
        {
            SrcNet.u4Type = IKE_IPSEC_ID_IPV6_SUBNET;
            IKE_MEMCPY (&(SrcNet.uNetwork.Ip6Subnet.Ip6Addr),
                        &(pSesInfo->Ike2AuthInfo.InternalIpAddr.Ipv6Addr),
                        sizeof (tIp6Addr));
            IKE_MEMCPY (&(SrcNet.uNetwork.Ip6Subnet.Ip6SubnetMask),
                        gaIp6PrefixToMask[IKE_INDEX_127], sizeof (tIp6Addr));
            SrcNet.u2StartPort = IKE_TS_MIN_PORT;
            SrcNet.u2EndPort = IKE_TS_MAX_PORT;
            IKE_MEMCPY (&DstNet,
                        &(pSesInfo->Ike2AuthInfo.ProtectedNetwork[IKE_INDEX_0]),
                        sizeof (tIkeNetwork));
            DstNet.u2StartPort = IKE_TS_MIN_PORT;
            DstNet.u2EndPort = IKE_TS_MAX_PORT;
        }
    }
    TAKE_IKE_SEM ();

    pCryptoMap =
        IkeGetCryptoMapFromIDs (pIkeEngine, &SrcNet,
                                &DstNet, IKE2_MAJOR_VERSION);
    if (pCryptoMap == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2UtilUpdateCryptoForRA: No Crypto Map Available "
                     "for Peer \n");
        return (IKE_FAILURE);
    }
    GIVE_IKE_SEM ();

    IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IkeCryptoMap, pCryptoMap,
                sizeof (tIkeCryptoMap));

    /* Copy local and remote network addresses to IPSecBundle */
    IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IPSecBundle.LocalProxy,
                &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
    IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IPSecBundle.RemoteProxy,
                &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));

    return (IKE_SUCCESS);
}
#endif /* _IKE2_UTIL_C_ */
