
/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/* $Id: ike2natt.c,v 1.4 2013/11/12 11:18:41 siva Exp $             */
/*                                                                    */
/* Description:This file contains functions for related to NAT-T     */
/*              for IKEv2                                             */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_NATT_C_
#define _IKE2_NATT_C_

#include "ikegen.h"
#include "ike2inc.h"

/************************************************************************/
/*  Function Name   :Ike2NattConstructDetectSrcIP                       */
/*  Description     :This function is used to construct NAT Detect SRC  */
/*                  :IP Payload                                         */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2NattConstructDetectSrcIP (tIkeSessionInfo * pSesInfo,
                              UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIkeNotifyInfo      IkeNatDInfo;
    UINT1               au1NatDHash[IKE_SHA512_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN] =
        { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Len = IKE_ZERO;

    IKE_BZERO (&IkeNatDInfo, sizeof (tIkeNotifyInfo));

    IkeNatDInfo.u2NotifyType = IKE2_NAT_DETECTION_SOURCE_IP;
    IkeNatDInfo.u2NotifyDataLen = IKE_SHA_LENGTH;
    IkeNatDInfo.u1Protocol = IKE_ZERO;
    IkeNatDInfo.u1SpiSize = IKE_ZERO;

    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0], pSesInfo->pIkeSA->au1InitiatorCookie,
                IKE_COOKIE_LEN);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN,
                pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    u2Len = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);

    if (pSesInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV4ADDR)
    {
        u4IpAddr = pSesInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len, &u4IpAddr,
                    IKE_IP4_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
    }
    else
    {
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len,
                    &pSesInfo->pIkeSA->IpLocalAddr.Ipv6Addr, IKE_IP6_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
    }
    u2IkePort = IKE_HTONS (IKE_UDP_PORT);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len, &u2IkePort, IKE_PORT_LEN);

    IkeSha1HashAlgo (au1NatdData, (u2Len + IKE_PORT_LEN), au1NatDHash);
    IkeNatDInfo.pu1NotifyData = au1NatDHash;
    if (Ike2ConstructNotify (pSesInfo, u1NextPayLoad, pu4Pos, &IkeNatDInfo) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2NattConstructDetectSrcIP: Ike2ConstructNotify "
                     "failed\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2NattConstructDetectDstIP                       */
/*  Description     :This function is used to construct NAT Detect Dest */
/*                  :IP Paylod                                          */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2NattConstructDetectDstIP (tIkeSessionInfo * pSesInfo,
                              UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIkeNotifyInfo      IkeNatDInfo;
    UINT1               au1NatDHash[IKE_SHA_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN] =
        { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Len = IKE_ZERO;

    IKE_BZERO (&IkeNatDInfo, sizeof (tIkeNotifyInfo));

    IkeNatDInfo.u2NotifyType = IKE2_NAT_DETECTION_DESTINATION_IP;
    IkeNatDInfo.u2NotifyDataLen = IKE_SHA_LENGTH;
    IkeNatDInfo.u1Protocol = IKE_ZERO;
    IkeNatDInfo.u1SpiSize = IKE_ZERO;

    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0], pSesInfo->pIkeSA->au1InitiatorCookie,
                IKE_COOKIE_LEN);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN,
                pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    u2Len = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);

    if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
    {
        u4IpAddr = pSesInfo->pIkeSA->IpPeerAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + IKE_COOKIE_LEN + IKE_COOKIE_LEN,
                    &u4IpAddr, IKE_IP4_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
    }
    else
    {
        IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len,
                    &pSesInfo->pIkeSA->IpPeerAddr.Ipv6Addr, IKE_IP6_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
    }
    u2IkePort = IKE_HTONS (pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort);
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0] + u2Len, &u2IkePort, IKE_PORT_LEN);

    IkeSha1HashAlgo (au1NatdData, (u2Len + IKE_PORT_LEN), au1NatDHash);
    IkeNatDInfo.pu1NotifyData = au1NatDHash;

    if (Ike2ConstructNotify (pSesInfo, u1NextPayLoad, pu4Pos, &IkeNatDInfo) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2NattConstructDetectDstIP: Ike2ConstructNotify "
                     "failed\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/**************************************************************************/
/*  Function Name : Ike2NattProcessDetectDstIP                            */
/*  Description   : This function is used to process Nat Detect           */
/*                : Destination IP                                        */
/*  Input(s)      : pSesInfo - Pointer to the Session Structure           */
/*                : pu1Ptr   - Pointer to the Notify Payload              */
/*                :                                                       */
/*  Output(s)     : pSesInfo is updated with the new NATT Flags           */
/*  Return        : None                                                  */
/**************************************************************************/

VOID
Ike2NattProcessDetectDstIP (tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr)
{
    UINT1               au1NatDHash[IKE_SHA_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN] =
        { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Len = IKE_ZERO;

    /*
     * NatD Hash Value is Calculated using below logic.
     * all below values (Cookie's, IP Addr and Port) are Concatenated.
     * NatDHash = SHA1_SUM(InitCookie | RespCookie | LocIpAddr | LocPort)
     * Calculated Hash Value is compared with Received Hash Value, to detect
     * NAT Device Between Peers.
     */

    IKE_BZERO (au1NatDHash, IKE_SHA_LENGTH);

    IKE_BZERO (&au1NatdData[IKE_INDEX_0], (IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                           IKE_IP4_ADDR_LEN + IKE_PORT_LEN));
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0], pSesInfo->pIkeSA->au1InitiatorCookie,
                IKE_COOKIE_LEN);
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + IKE_COOKIE_LEN),
                    pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    }
    u2Len = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);
    if (pSesInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV4ADDR)
    {
        u4IpAddr = pSesInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len), &u4IpAddr,
                    IKE_IP4_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
    }
    else
    {
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len),
                    &pSesInfo->pIkeSA->IpLocalAddr.Ipv6Addr, IKE_IP6_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
    }
    u2IkePort = IKE_HTONS (u2IkePort);
    IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len), &u2IkePort,
                IKE_PORT_LEN);

    IkeSha1HashAlgo (au1NatdData, (u2Len + IKE_PORT_LEN), au1NatDHash);
    if (IKE_MEMCMP (au1NatDHash, pu1Ptr, IKE_SHA_LENGTH) != IKE_ZERO)
    {
        if (!(pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT))
        {
            pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort = IKE_NATT_UDP_PORT;
        }
        pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_BEHIND_NAT;
        pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_USE_NATT_PORT;
    }
    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
        pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_PEER_SUPPORT_NATT;
    return;
}

/**************************************************************************/
/*  Function Name : Ike2NattProcessDetectSrcIP                            */
/*  Description   : This function is used to process Nat Detect Source IP */
/*  Input(s)      : pSesInfo - Pointer to the Session Structure           */
/*                : pu1Ptr   - Pointer to the Notify Payload              */
/*                :                                                       */
/*  Output(s)     : pSesInfo is updated with the new NATT Flags           */
/*  Return        : None                                                  */
/**************************************************************************/

VOID
Ike2NattProcessDetectSrcIP (tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr)
{
    UINT1               au1NatDHash[IKE_SHA_LENGTH] = { IKE_ZERO };
    UINT1               au1NatdData[IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                    IKE_IP6_ADDR_LEN + IKE_PORT_LEN] =
        { IKE_ZERO };
    UINT2               u2IkePort = IKE_UDP_PORT;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT2               u2Len = IKE_ZERO;

    /*
     * NatD Hash Value is Calculated using below logic.
     * all below values (Cookie's, IP Addr and Port) are Concatenated.
     * NatDHash = SHA1_SUM(InitCookie | RespCookie | PeerIpAddr | PeerPort)
     * Calculated Hash Value is compared with Received Hash Value, to detect
     * NAT Device Between Peers.
     */

    IKE_BZERO (au1NatDHash, IKE_SHA_LENGTH);

    IKE_BZERO (&au1NatdData[IKE_INDEX_0], (IKE_COOKIE_LEN + IKE_COOKIE_LEN +
                                           IKE_IP4_ADDR_LEN + IKE_PORT_LEN));
    IKE_MEMCPY (&au1NatdData[IKE_INDEX_0],
                pSesInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + IKE_COOKIE_LEN),
                    pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    }
    u2Len = (IKE_COOKIE_LEN + IKE_COOKIE_LEN);
    if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
    {
        u4IpAddr = pSesInfo->pIkeSA->IpPeerAddr.Ipv4Addr;
        u4IpAddr = IKE_HTONL (u4IpAddr);
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len), &u4IpAddr,
                    IKE_IP4_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP4_ADDR_LEN);
    }
    else
    {
        IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len),
                    &pSesInfo->pIkeSA->IpPeerAddr.Ipv6Addr, IKE_IP6_ADDR_LEN);
        u2Len = (UINT2) (u2Len + IKE_IP6_ADDR_LEN);
    }
    u2IkePort = pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort;
    u2IkePort = IKE_HTONS (u2IkePort);
    IKE_MEMCPY (((&au1NatdData[IKE_INDEX_0]) + u2Len), &u2IkePort,
                IKE_PORT_LEN);

    IkeSha1HashAlgo (au1NatdData, (u2Len + IKE_PORT_LEN), au1NatDHash);
    if (IKE_MEMCMP (au1NatDHash, pu1Ptr, IKE_SHA_LENGTH) != IKE_ZERO)
    {
        pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_PEER_BEHIND_NAT;
    }
    pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
        pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_PEER_SUPPORT_NATT;
    return;
}
#endif /* _IKE2_NATT_C_ */
