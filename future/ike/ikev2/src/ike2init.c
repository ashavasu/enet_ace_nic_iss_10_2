
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2init.c,v 1.4 2013/11/12 11:18:40 siva Exp $
 *
 * Description: This has functions for IKEv2 Initializations
 *
 ***********************************************************************/

#ifndef _IKE2_INIT_C_
#define _IKE2_INIT_C_

#include "ikegen.h"
#include "ike2inc.h"

/******************************************************************************/
/*  Function Name :  Ike2InitMemPoolInit                                      */
/*  Description   :  Initialize the buddy memory pools for ikev2              */
/*                :                                                           */
/*  Input(s)      :  NONE                                                     */
/*                :                                                           */
/*  Output(s)     :  NONE                                                     */
/*  Return        :  IKE_SUCCESS/IKE_FAULURE                                  */
/******************************************************************************/
INT1
Ike2InitMemPoolInit (VOID)
{
    if ((gi4Ike2PktPoolId =
         MemBuddyCreate (IKE_MAX_DATA_SIZE, IKE_MIN_DATA_SIZE,
                         IKE2_MAX_NUM_OF_PKT_BUFFS,
                         BUDDY_NON_CONT_BUF)) == BUDDY_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Unable to allocate memory for new session");
        return IKE_FAILURE;
    }

    if ((MemCreateMemPool (sizeof (tIkeDsaKeyInfo),
                           MAX_IKE_DSA_KEY_INFO_BLOCKS,
                           MEM_DEFAULT_MEMORY_TYPE,
                           &gu4IkeDsaMemPoolId)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Unable to Memory Pool for DSA Key Info");
        return IKE_FAILURE;
    }
    if ((MemCreateMemPool (sizeof (tIke2Trans),
                           MAX_IKE_V2_TRANSFORM_BLOCKS,
                           MEM_DEFAULT_MEMORY_TYPE,
                           &gIke2TransMemPoolId)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Unable to Memory Pool for tIke2Trans Info");
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/******************************************************************************/
/*  Function Name :  Ike2InitMemPoolDeInit                                    */
/*  Description   :  Delete the  buddy memory pools created for Ikev2         */
/*                :                                                           */
/*  Input(s)      :  NONE                                                     */
/*                :                                                           */
/*  Output(s)     :  NONE                                                     */
/*  Return        :  NONE                                                     */
/******************************************************************************/
VOID
Ike2InitMemPoolDeInit (VOID)
{
    MemBuddyDestroy ((UINT1) gi4Ike2PktPoolId);
    gi4Ike2PktPoolId = IKE_ZERO;

    if (gu4IkeDsaMemPoolId != IKE_ZERO)
    {
        MemDeleteMemPool (gu4IkeDsaMemPoolId);
        gu4IkeDsaMemPoolId = IKE_ZERO;
    }
    if (gIke2TransMemPoolId != IKE_ZERO)
    {
        MemDeleteMemPool (gIke2TransMemPoolId);
        gIke2TransMemPoolId = IKE_ZERO;
    }
    return;
}

#endif /* _IKE2_INIT_C_ */
