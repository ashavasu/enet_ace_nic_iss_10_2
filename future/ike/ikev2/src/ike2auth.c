
/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/* $Id: ike2auth.c,v 1.8 2014/03/16 11:40:27 siva Exp $ */
/*                                                                    */
/*  Description:This file contains functions related to auth payload  */
/*              construction and processing                           */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_AUTH_C_
#define _IKE2_AUTH_C_

#include "ikegen.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ike2inc.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :Ike2AuthConstructhMsg                              */
/*  Description     :This function is used to compute auth message      */
/*                  :to be hased                                        */
/*                  :                                                   */
/*  Input(s)        :pSesInfo       - Pointer to the Session Info       */
/*                  :pu1AuthData    - Pointer to the Computed auth msg  */
/*                  :pu4AuthDataLen - Computed auth message length      */
/*                  :u1Flag         - Indicates process or construct    */
/*                  :                                                   */
/*  Output(s)       :Computes Auth message                              */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2AuthConstructhMsg (tIkeSessionInfo * pSesInfo, UINT1 **pu1AuthData,
                       UINT4 *pu4AuthDataLen, UINT1 u1Flag)
{
    UINT1              *pu1SKeyIdP = NULL;
    UINT1              *pu1IdPayload = NULL;
    UINT1              *pu1InitPkt = NULL;
    UINT1              *pu1Nonce = NULL;
    UINT1               au1Buf[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };

    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4IdLen = IKE_ZERO;
    UINT4               u4InitPktLen = IKE_ZERO;
    UINT2               u2NonceLen = IKE_ZERO;

    IKE_BZERO (au1Buf, IKE_MAX_DIGEST_SIZE);

    /* If we are Initiator, generate AUTH data as 
     * AUTH_DATA_i = ikesa_init_1 message | Nr | prf(SK_pi, IDi') 
     * We are Responder, generate AUTH data as 
     * AUTH_DATA_r = ikesa_init_2 message | Ni | prf(SK_pr, IDr') 
     */

    /* First calculate prf(SK_pi, IDi') or prf(SK_pr, IDr')
       based upon the u1flag 
     */

    if (pSesInfo->pIkeSA != NULL)
    {
        u4HashLen =
            gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructhMsg: pSesInof->pIkeSA is NULL\n");
        return (IKE_FAILURE);
    }

    /* Initiator Auth construct - ikesa_init_1 message | Nr | prf(SK_pi, IDsent)
     * Responder Auth construct - ikesa_init_2 message | Ni | prf(SK_pr, IDsent)
     * Initiator Auth process - ikesa_init_2 message | Ni | prf(SK_pr, IDRcvd)
     * Responder Auth process - ikesa_init_1 message | Nr | prf(SK_pi,IDRcvd) */

    if (((u1Flag == IKE2_CONSTRUCT_AUTH) &&
         (pSesInfo->u1Role == IKE_INITIATOR)) ||
        ((u1Flag == IKE2_PROCESS_AUTH) && (pSesInfo->u1Role == IKE_RESPONDER)))
    {
        pu1SKeyIdP = pSesInfo->pIkeSA->Ike2SA.au1SKeyIdPi;
        pu1InitPkt = pSesInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt;
        u4InitPktLen = pSesInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen;
        pu1Nonce = pSesInfo->pIkeSA->Ike2SA.pu1RespNonce;
        u2NonceLen = pSesInfo->pIkeSA->Ike2SA.u2RespNonceLen;
    }
    else
    {
        pu1SKeyIdP = pSesInfo->pIkeSA->Ike2SA.au1SKeyIdPr;
        pu1InitPkt = pSesInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt;
        u4InitPktLen = pSesInfo->pIkeSA->Ike2SA.u4SaInitRspPktLen;
        pu1Nonce = pSesInfo->pIkeSA->Ike2SA.pu1InitNonce;
        u2NonceLen = pSesInfo->pIkeSA->Ike2SA.u2InitNonceLen;
    }

    if (u1Flag == IKE2_CONSTRUCT_AUTH)
    {
        pu1IdPayload = pSesInfo->pu1IdPayloadSent;
        u4IdLen = pSesInfo->u4IdPayloadSentLen;
    }
    else
    {
        pu1IdPayload = pSesInfo->pu1IdPayloadRecd;
        u4IdLen = pSesInfo->u4IdPayloadRecdLen;
    }

    if ((pu1SKeyIdP == NULL) || (pu1IdPayload == NULL) ||
        (pu1InitPkt == NULL) || (pu1Nonce == NULL))
    {
        /* Memory clean up will be handled in IKESA & session deletion */
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructhMsg: One of the inputs for calculating "
                     " Auth message is null. \n");
        return (IKE_FAILURE);
    }

    /* Calculate AUTH_DATA_i/AUTH_DATA_r length  */
    *pu4AuthDataLen = (u4InitPktLen + (UINT4) u2NonceLen + u4HashLen);

    /* prf(SK_pi, IDi') or prf(SK_pr, IDr') */
    if ((*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pu1SKeyIdP, (INT4) u4HashLen, pu1IdPayload, (INT4) u4IdLen,
         au1Buf) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructhMsg: Unsupported Hash algorithm\n");
        return (IKE_FAILURE);
    }

    /* Allocate memory for AUTH message */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*pu1AuthData)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructhMsg: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*pu1AuthData == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructhMsg: Malloc failed to initialize"
                     "memory for Auth message\n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (*pu1AuthData, *pu4AuthDataLen);

    /* Copy ike_sa_init pkt and Nonce   */
    IKE_MEMCPY (*pu1AuthData, pu1InitPkt, u4InitPktLen);
    IKE_MEMCPY ((*pu1AuthData + u4InitPktLen), pu1Nonce, u2NonceLen);

    /* Copy prf(Sk_p ,ID') */
    IKE_MEMCPY ((*pu1AuthData + u4InitPktLen + u2NonceLen), au1Buf, u4HashLen);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2AuthConstructDigest                            */
/*  Description     :This function is used to compute digest for auth   */
/*                  :message                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :pu1AuthData   - Pointer to the Computed auth data  */
/*                  :u4AuthDataLen - Auth data length                   */
/*                  :pu1AuthDigest - Pointer to the digest value        */
/*                  :                                                   */
/*  Output(s)       :Computes Auth Digest                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2AuthConstructDigest (tIkeSessionInfo * pSesInfo, UINT1 *pu1AuthData,
                         UINT4 u4AuthDataLen, UINT1 *pu1AuthDigest)
{

    UINT1              *pu1Buf = NULL;
    UINT1               au1HashData[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    UINT1              *pu1Sigret = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeCertDB         *pIkeCert = NULL;

    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4ErrorCode = IKE_ZERO;
    UINT4               u4Encrlen = IKE_ZERO;
    UINT4               u4DigestLen = IKE_ZERO;
    UINT4               u4SignAlgo = IKE_ZERO;
    INT4                i4RetVal = IKE_FAILURE;

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Buf) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructDigest: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (pu1Buf, IKE_MAX_DIGEST_SIZE);

    TAKE_IKE_SEM ();
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthConstructDigest: Engine not found\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
        return (IKE_FAILURE);
    }
    GIVE_IKE_SEM ();

    switch (pSesInfo->pIkeSA->u2AuthMethod)
    {
        case IKE2_AUTH_PSK:
        {
            /* AUTH_Digest = prf(prf(Shared Secret,"Key Pad for IKEv2"),
               <msg octets>) */

            /* Search the preshared key using the peer identity */
            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
            {
                if (pIkeKey->u1Status == IKE_ACTIVE)
                {
                    if (pSesInfo->pIkeSA->ResponderID.i4IdType ==
                        pIkeKey->KeyID.i4IdType)
                    {
                        if (IKE_MEMCMP (&(pIkeKey->KeyID.uID),
                                        &(pSesInfo->pIkeSA->ResponderID.uID),
                                        pIkeKey->KeyID.u4Length) == IKE_ZERO)
                        {
                            break;
                        }
                    }
                }
            }
            if (pIkeKey == NULL)
            {
                GIVE_IKE_SEM ();
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: Preshared key doesn't "
                             "exist for the Responder id\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }
            GIVE_IKE_SEM ();

            /* prf(Shared Secret,"Key Pad for IKEv2") */
            (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pIkeKey->au1KeyString,
                        (INT4) (IKE_STRLEN (pIkeKey->au1KeyString)),
                        (UINT1 *) "Key Pad for IKEv2", IKE_SEVENTEEN, pu1Buf);

            u4HashLen =
                gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

            /* Calculate AUTH Digest */
            (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pu1Buf, (INT4) u4HashLen, pu1AuthData,
                        (INT4) u4AuthDataLen, pu1AuthDigest);
            break;
        }
        case IKE2_AUTH_RSA:
        {
            pIkeRsa = IkeGetRSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);
            if (pIkeRsa == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: IkeGetRSAKeyWithCertInfo"
                             " fails\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }

            pIkeCert =
                IkeGetCertFromMyCerts (pIkeEngine->au1EngineName,
                                       pIkeRsa->CertInfo.pCAName,
                                       pIkeRsa->CertInfo.pCASerialNum);
            if (pIkeCert == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: IkeGetCertFromMyCerts"
                             " fails\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }
            IKE_BZERO ((UINT1 *) au1HashData, IKE_MAX_DIGEST_SIZE);
            u4SignAlgo = IkeGetSignatureAlgo (pIkeCert->pCert);
            if (u4SignAlgo == IKE_SHA1)
            {
                /* Calculate the SHA1 hash of the Authentication Data */
                IkeSha1HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                 au1HashData);
                u4DigestLen = IKE_SHA_LENGTH;
                u4SignAlgo = NID_sha1;
            }
            else if (u4SignAlgo == IKE_SHA224)
            {
                /* Calculate the SHA224 hash of the Authentication Data */
                IkeSha224HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   au1HashData);
                u4DigestLen = IKE_SHA224_LENGTH;
                u4SignAlgo = NID_sha224;
            }
            else if (u4SignAlgo == IKE_SHA256)
            {
                /* Calculate the SHA256 hash of the Authentication Data */
                IkeSha256HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   au1HashData);
                u4DigestLen = IKE_SHA256_LENGTH;
                u4SignAlgo = NID_sha256;
            }
            else if (u4SignAlgo == IKE_SHA384)
            {
                /* Calculate the SHA384 hash of the Authentication Data */
                IkeSha384HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   au1HashData);
                u4DigestLen = IKE_SHA384_LENGTH;
                u4SignAlgo = NID_sha384;
            }
            else if (u4SignAlgo == IKE_SHA512)
            {
                /* Calculate the SHA512 hash of the Authentication Data */
                IkeSha512HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   au1HashData);
                u4DigestLen = IKE_SHA512_LENGTH;
                u4SignAlgo = NID_sha512;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: Invalid Hashing"
                             " Algorithm\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }

            if (FipsGetFipsCurrOperMode () == FIPS_MODE)
            {
                /* This function will calculate the Signature over the Hashdata 
                 * provided using the RSA key inputed */
                i4RetVal = IkeRsaSign
                    (pu1AuthData, (UINT2) u4AuthDataLen, u4SignAlgo, &pu1Sigret,
                     (UINT4 *) &u4Encrlen, &pIkeRsa->RsaKey, &u4ErrorCode);
            }
            else
            {
                i4RetVal = IkeRsaSign
                    (au1HashData, (UINT2) u4DigestLen, u4SignAlgo, &pu1Sigret,
                     (UINT4 *) &u4Encrlen, &pIkeRsa->RsaKey, &u4ErrorCode);
            }

            if (i4RetVal == IKE_FAILURE)
            {
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_RSA_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Unable to get rsa key\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Memory allocation failed\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                    case IKE_ERR_RSA_SIGN_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Rsa signing failed\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Rsa Signing failed\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                }
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (pu1AuthDigest, pu1Sigret, u4Encrlen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Sigret);
            break;
        }
        case IKE2_AUTH_DSA:
        {
            pIkeDsa = IkeGetDSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);
            if (pIkeDsa == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: IkeGetDSAKeyWithCertInfo"
                             " fails\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }

            IKE_BZERO ((UINT1 *) au1HashData, IKE_MAX_DIGEST_SIZE);

            /* Calculate the SHA1 hash of the Authentication Data */
            IkeSha1HashAlgo (pu1AuthData, (INT4) u4AuthDataLen, au1HashData);

            /* This function will calculate the Signature over the Hashdata 
             * provided using the DSA key inputed */
            if (IkeDsaSign (au1HashData, IKE_SHA_LENGTH, &pu1Sigret,
                            (UINT4 *) &u4Encrlen, &pIkeDsa->DsaKey,
                            &u4ErrorCode) == IKE_FAILURE)
            {
                switch (u4ErrorCode)
                {
                    case IKE_ERR_GET_DSA_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Unable to get Dsa key\n");
                        break;
                    }
                    case IKE_ERR_MEM_ALLOC:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Memory allocation failed\n");
                        break;
                    }
                    case IKE_ERR_DSA_SIGN_FAILED:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Dsa signing failed\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                    default:
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2AuthConstructDigest : "
                                     "Dsa Signing failed\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1Sigret);
                        break;
                    }
                }
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (pu1AuthDigest, pu1Sigret, u4Encrlen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Sigret);
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2AuthConstructDigest : Unsupported "
                         "authentication method\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
            return (IKE_FAILURE);
        }
    }
    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Buf);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2AuthVerifyDigest                               */
/*  Description     :This function is used to compute hash for the auth */
/*                  :message                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :pu1AuthData   - Pointer to the Computed auth data  */
/*                  :u4AuthDataLen - Auth data length                   */
/*                  :u1AuthMethod  - Authentication method              */
/*                  :pu1PayLoad    - Current payload                    */
/*  Output(s)       :The Computed Auth Digest                           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2AuthVerifyDigest (tIkeSessionInfo * pSesInfo, UINT1 *pu1AuthData,
                      UINT4 u4AuthDataLen, UINT1 u1AuthMethod,
                      UINT1 *pu1PayLoad)
{

    UINT1               au1Buf[IKE_MAX_DIGEST_SIZE + IKE_INDEX_1] =
        { IKE_ZERO };
    UINT1              *pu1AuthDigest = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeCertDB         *pIkeCert = NULL;

    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4SignAlgo = IKE_ZERO;
    UINT4               u4DigestLen = IKE_ZERO;

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pu1AuthDigest) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifyDigest: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (au1Buf, (IKE_MAX_DIGEST_SIZE + IKE_INDEX_1));
    IKE_BZERO (pu1AuthDigest, (IKE_MAX_DIGEST_SIZE + IKE_INDEX_1));

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifyDigest: Engine not found\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1AuthDigest);
        return (IKE_FAILURE);
    }

    switch (u1AuthMethod)
    {
        case IKE2_AUTH_PSK:
        {
            /* AUTH_Digest = prf(prf(Shared Secret,"Key Pad for IKEv2"),
               <msg octets>) */

            /* Search the preshared key using the peer identity */
            SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
            {
                if (pIkeKey->u1Status == IKE_ACTIVE)
                {
                    if (pSesInfo->pIkeSA->ResponderID.i4IdType ==
                        pIkeKey->KeyID.i4IdType)
                    {
                        if (IKE_MEMCMP (&(pIkeKey->KeyID.uID),
                                        &(pSesInfo->pIkeSA->ResponderID.uID),
                                        pIkeKey->KeyID.u4Length) == IKE_ZERO)
                        {
                            break;
                        }
                    }
                }
            }
            if (pIkeKey == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthVerifyDigest: Preshared key "
                             "doesn't exist for the Responder id\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }

            (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (pIkeKey->au1KeyString,
                        (INT4) (IKE_STRLEN (pIkeKey->au1KeyString) +
                                IKE_INDEX_1), (UINT1 *) "Key Pad for IKEv2",
                        IKE_SEVENTEEN, au1Buf);

            u4HashLen =
                gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

            (*gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].
             HmacAlgo) (au1Buf, (INT4) u4HashLen, pu1AuthData,
                        (INT4) u4AuthDataLen, pu1AuthDigest);

            /* Compare the recieved digest value and the computed one */
            if (IKE_MEMCMP (pu1PayLoad, pu1AuthDigest, u4HashLen) != IKE_ZERO)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthVerifyDigest :Auth Digest "
                             "Verfication failed");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }
            break;
        }
        case IKE2_AUTH_RSA:
        {
            pIkeRsa = IkeGetRSAKeyWithCertInfo (pIkeEngine, &pSesInfo->MyCert);
            if (pIkeRsa == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: IkeGetRSAKeyWithCertInfo"
                             " fails\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }

            pIkeCert =
                IkeGetCertFromMyCerts (pIkeEngine->au1EngineName,
                                       pIkeRsa->CertInfo.pCAName,
                                       pIkeRsa->CertInfo.pCASerialNum);
            if (pIkeCert == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthConstructDigest: IkeGetCertFromMyCerts"
                             " fails\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }
            u4SignAlgo = IkeGetSignatureAlgo (pIkeCert->pCert);
            if (u4SignAlgo == IKE_SHA1)
            {
                /* Calculate the SHA1 hash of the Authentication Data */
                IkeSha1HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                 pu1AuthDigest);
                u4DigestLen = IKE_SHA_LENGTH;
                u4SignAlgo = NID_sha1;
            }
            else if (u4SignAlgo == IKE_SHA224)
            {
                /* Calculate the SHA224 hash of the Authentication Data */
                IkeSha224HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   pu1AuthDigest);
                u4DigestLen = IKE_SHA224_LENGTH;
                u4SignAlgo = NID_sha224;
            }
            else if (u4SignAlgo == IKE_SHA256)
            {
                /* Calculate the SHA256 hash of the Authentication Data */
                IkeSha256HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   pu1AuthDigest);
                u4DigestLen = IKE_SHA256_LENGTH;
                u4SignAlgo = NID_sha256;
            }
            else if (u4SignAlgo == IKE_SHA384)
            {
                /* Calculate the SHA384 hash of the Authentication Data */
                IkeSha384HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   pu1AuthDigest);
                u4DigestLen = IKE_SHA384_LENGTH;
                u4SignAlgo = NID_sha384;
            }
            else if (u4SignAlgo == IKE_SHA512)
            {
                /* Calculate the SHA512 hash of the Authentication Data */
                IkeSha512HashAlgo (pu1AuthData, (INT4) u4AuthDataLen,
                                   pu1AuthDigest);
                u4DigestLen = IKE_SHA512_LENGTH;
                u4SignAlgo = NID_sha512;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthVerifyDigest: Invalid Hashing"
                             " Algorithm\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }

            if (FipsGetFipsCurrOperMode () != FIPS_MODE)
            {
                /* Verify the sigature sent in Auth payload */
                if (Ike2AuthVerifySignature
                    (pSesInfo, pu1AuthDigest, pu1PayLoad, u4SignAlgo,
                     u4DigestLen) == IKE_FAILURE)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifyDigest : Auth Digest "
                                 "Verfication failed");
                    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                        (UINT1 *) pu1AuthDigest);
                    return (IKE_FAILURE);
                }
            }
            else
            {
                /* Verify the sigature sent in Auth payload */
                /* In case of FIPS mode original data needs to be 
                 * passed to verify the signature */
                if (Ike2AuthVerifySignature (pSesInfo, pu1AuthData, pu1PayLoad,
                                             u4SignAlgo, u4AuthDataLen)
                    == IKE_FAILURE)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifyDigest : Auth Digest "
                                 "Verfication failed");
                    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                        (UINT1 *) pu1AuthDigest);
                    return (IKE_FAILURE);
                }
            }
            break;
        }
        case IKE2_AUTH_DSA:
        {
            /* Calculate the SHA1 hash of the Authentication Data */
            IkeSha1HashAlgo (pu1AuthData, (INT4) u4AuthDataLen, pu1AuthDigest);

            u4DigestLen = IKE_SHA_LENGTH;
            u4SignAlgo = NID_sha1;
            /* Verify the sigature sent in Auth payload */
            if (Ike2AuthVerifySignature (pSesInfo, pu1AuthDigest, pu1PayLoad,
                                         u4SignAlgo, u4DigestLen)
                == IKE_FAILURE)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2AuthVerifyDigest : Auth Digest "
                             "Verfication failed");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1AuthDigest);
                return (IKE_FAILURE);
            }
            break;
        }
        default:
        {
            IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2AuthVerifyDigest : Auth Method " "not supported");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                (UINT1 *) pu1AuthDigest);
            return (IKE_FAILURE);
        }
    }
    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1AuthDigest);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2AuthVerifySignature                            */
/*  Description     :This functions is used to process the signature    */
/*                  :in the Authentication payload                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info DataBase  */
/*                  :pu1HashData- Pointer to the Digest value           */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :u4SignAlgo - Hashing algorithm type                */
/*                  :                                                   */
/*  Output(s)       :Verifies the signature                             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2AuthVerifySignature (tIkeSessionInfo * pSesInfo, UINT1 *pu1HashData,
                         UINT1 *pu1PayLoad, UINT4 u4SignAlgo, UINT4 u4DigestLen)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertDB         *pPeerCert = NULL;
    tIkePKey           *pPubKey = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;

    /*Check whether Engine is Active */
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifySignature: Engine not found\n");
        return (IKE_FAILURE);
    }

    /*Check whether we have Peer Certificate information */
    if ((pSesInfo->PeerCertInfo.pCAName == NULL) ||
        (pSesInfo->PeerCertInfo.pCASerialNum == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifySignature: No cert info is available\n");
        return (IKE_FAILURE);
    }

    /*Get the peer certificate from the Certificate Database */
    pPeerCert =
        IkeGetCertFromPeerCerts (pIkeEngine->au1EngineName,
                                 pSesInfo->PeerCertInfo.pCAName,
                                 pSesInfo->PeerCertInfo.pCASerialNum);
    if (pPeerCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifySignature: No cert is available "
                     "for verification\n");
        return (IKE_FAILURE);
    }

    /*Extract the public key from the peer certificate */
    pPubKey = IkeGetX509PubKey (pPeerCert->pCert);
    if (pPubKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2AuthVerifySignature: Failed to get public key "
                     "from cert\n");
        return (IKE_FAILURE);
    }

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA)
    {
        /*Verify the Signature which is received in Auth Payload */
        if (IkeRsaVerify (pu1HashData, pu1PayLoad,
                          pPubKey, u4SignAlgo, u4DigestLen,
                          &u4ErrorCode) == IKE_FAILURE)
        {
            switch (u4ErrorCode)
            {
                case IKE_ERR_GET_RSA_FAILED:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : "
                                 "Unable to get rsa key\n");
                    break;
                }
                case IKE_ERR_MEM_ALLOC:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : Memory "
                                 "allocation failed\n");
                    break;
                }
                case IKE_ERR_RSA_PUB_DECRYPT_FAILED:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : Rsa public "
                                 "decrpt failed\n");
                    break;
                }
                default:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : "
                                 "Rsa decryption failed\n");
                    break;
                }
            }
            return (IKE_FAILURE);
        }
    }
    else if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA)
    {
        /*Verify the Signature which is received in Auth Payload */
        if (IkeDsaVerify (pu1HashData, pu1PayLoad,
                          pPubKey, &u4ErrorCode) == IKE_FAILURE)
        {
            switch (u4ErrorCode)
            {
                case IKE_ERR_GET_DSA_FAILED:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : "
                                 "Unable to get Dsa key\n");
                    break;
                }
                case IKE_ERR_MEM_ALLOC:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : Memory "
                                 "allocation failed\n");
                    break;
                }
                case IKE_ERR_DSA_PUB_DECRYPT_FAILED:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : Dsa public "
                                 "decrpt failed\n");
                    break;
                }
                default:
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2AuthVerifySignature : "
                                 "Dsa decryption failed\n");
                    break;
                }
            }
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

#endif /* _IKE2_AUTH_C_ */
