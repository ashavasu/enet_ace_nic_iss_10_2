/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/*  $Id: ike2proc.c,v 1.21 2014/08/22 11:03:15 siva Exp $                                   */
/*                                                                    */
/*  Description:This file contains functions for processing the       */
/*              payloads in the ikev2 packet                          */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_PROC_C_
#define _IKE2_PROC_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikememmac.h"

PRIVATE INT1        (*gaIke2ProcessFunc[]) (tIkeSessionInfo *, UINT1 *, UINT1) =
{
    NULL, Ike2ProcessSa, Ike2ProcessKE, Ike2ProcessId, Ike2ProcessId, Ike2ProcessCert, Ike2ProcessCertReq, Ike2ProcessAuth, Ike2ProcessNonce, Ike2ProcessNotify, Ike2ProcessDelete, Ike2ProcessVendorId, Ike2ProcessTS, Ike2ProcessTS, Ike2ProcessEncrypt, Ike2ProcessCfg, NULL    /* IKE2_PAYLOAD_EAP */
};

/************************************************************************/
/*  Function Name   :Ike2Proces                                         */
/*                  :                                                   */
/*  Description     :This function is used to process all the payloads  */
/*                  :in an Isakmp Message                               */
/*                  :                                                   */
/*  Input(s)        :u1PayLoadType - Identifies the first payload in an */
/*                  :Isakmp Message                                     */
/*                  :pSesInfo      - Pointer to the Session Info        */
/*                  :pu1PayLoad    - The recieved Isakmp message to be  */
/*                  :                processed                          */
/*                  :                                                   */
/*  Output(s)       :Processed Status                                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2Process (UINT1 u1PayLoadType, tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad)
{
    tGenericPayLoad     Dummy;
    UINT2               u2Len = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_ASSERT (pSesInfo->IkeRxPktInfo.pu1RawPkt != NULL);

    /* Process the ISAKMP Message until the Last payload is 
     * encountered */

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    while (u1PayLoadType != IKE_ZERO)
    {
        /* If PayLoadType equal to IKE2_MAX_PAYLOAD_TYPE , means 
         * EAP payload is receieved. Since EAP is not supported 
         * return failure */
        if (u1PayLoadType == IKE2_MAX_PAYLOAD_TYPE)
        {
            IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2Process: Payload Type %d not supported from peer: %s\n",
                          u1PayLoadType, pu1PeerAddrStr);
            /* Increament drop count statistics */
            IncIkeDropPkts (&pSesInfo->pIkeSA->IpLocalAddr);
            return (IKE_FAILURE);
        }

        IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, IKE2_GEN_PAYLOAD_SIZE);

        Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);

        if ((u1PayLoadType > IKE2_MAX_PAYLOAD_TYPE) ||
            (u1PayLoadType <= IKE2_MINIMUM_PAYLOAD_ID))
        {
            /* Check if the critical bit is set. If set , it means any
             * unimplemented payload is recieved .So return it with notify
             * IKE2_UNSUPPORTED_CRITICAL_PAYLOAD */
            if (Dummy.u1Reserved & IKE2_CRITICAL_BIT)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_UNSUPPORTED_CRITICAL_PAYLOAD);
                /* Increament drop count statistics */
                IncIkeDropPkts (&pSesInfo->pIkeSA->IpLocalAddr);
            }
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2Process: Critical bit set in payload\n");
            return (IKE_FAILURE);
        }

        if (((gaIke2ProcessFunc[u1PayLoadType - IKE2_MINIMUM_PAYLOAD_ID]) !=
             NULL) &&
            ((*gaIke2ProcessFunc[u1PayLoadType - IKE2_MINIMUM_PAYLOAD_ID])
             (pSesInfo, pu1PayLoad, u1PayLoadType) == IKE_FAILURE))
        {
            /* Increament drop count statistics */
            IncIkeDropPkts (&pSesInfo->pIkeSA->IpLocalAddr);
            return (IKE_FAILURE);
        }

        u2Len = Dummy.u2PayLoadLen;
        u1PayLoadType = Dummy.u1NextPayLoad;
        pu1PayLoad += u2Len;
    }
    return (IKE_SUCCESS);
}

/***************************************************************************/
/*   Function Name : Ike2ProcessSa                                         */
/*   Description   : Function Process the incoming SA payload              */
/*                 :                                                       */
/*   Input(s)      : pSessionInfo - Session Info Pointer.                  */
/*                 : pu1PayLoad   - The current payload to be processed    */
/*                 : u1Flag       - Current payload type(unused)           */
/*                 :                                                       */
/*   Output(s)     : None.                                                 */
/*   Return Values : IKE_SUCCESS or IKE_FAILURE                            */
/***************************************************************************/
INT1
Ike2ProcessSa (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2SaPayLoad      Sa;
    tIke2PropPayLoad    Prop;
    UINT1              *pu1Ptr = NULL;
    INT1                i1Return = IKE_FAILURE;
    UINT2               u2SaPayLoadLen = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (u1Flag);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    /* Sa payload is valid only in the INIT ,AUTH and CHILD Exchange mode */
    if (pSesInfo->u1ExchangeType == IKE2_INFO_EXCH)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessSa: Not Expecting SA payload from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_WAIT) &&
            (pSesInfo->u1FsmState != IKE2_AUTH_WAIT) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_WAIT))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessSa: Not Expecting SA payload from peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    else
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_IDLE) &&
            (pSesInfo->u1FsmState != IKE2_AUTH_IDLE) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_IDLE))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessSa: Not Expecting SA payload from peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }

    /* Copy the SA payload and calculate the SA payload data length */
    IKE_MEMCPY ((UINT1 *) &Sa, pu1PayLoad, IKE2_SA_PAYLOAD_SIZE);
    u2SaPayLoadLen = (UINT2) (IKE_NTOHS (Sa.u2PayLoadLen) -
                              IKE2_SA_PAYLOAD_SIZE);

    /* pu1Ptr points to the first proposal in the SA payload */
    pu1Ptr = pu1PayLoad + IKE2_SA_PAYLOAD_SIZE;

    /* Copy the first proposal */
    IKE_MEMCPY (&Prop, pu1Ptr, IKE2_PROP_PAYLOAD_SIZE);

    /* Check whether multiple SA payloads are present. If present log error 
       and return failue */
    if (SLL_COUNT
        (&pSesInfo->IkeRxPktInfo.
         aPayload[IKE2_PAYLOAD_SA - IKE2_MINIMUM_PAYLOAD_ID]) > IKE_ONE)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessSa: Multiple SA payloads present in "
                      "ISAKMP msg from peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    /* If role is responder,parse the SA payload and store it in the
     * session structure. On success of parsing select the matching 
     * proposal,on failure send notify IKE2_INVALID_SYNTAX */
    if (Ike2SaParseProposal (pu1Ptr, pSesInfo, u2SaPayLoadLen) == IKE_SUCCESS)
    {
        i1Return = Ike2SaSelectProposal (pSesInfo);
    }
    else
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessSa: Failed to parse SA payload from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    /* If the matching Proposal not found send notify payload 
     * IKE2_NO_PROPOSAL_CHOSEN.Delete the proposal list from the 
     * session structure. */
    if (i1Return == IKE_SUCCESS)
    {
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), Ike2UtilFreeProposal);
    }
    else
    {
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), Ike2UtilFreeProposal);
        IKE_SET_ERROR (pSesInfo, IKE2_NO_PROPOSAL_CHOSEN);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessSa: No proposal chosen for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessKE                                      */
/*  Description     :This function is used to process the key exchange  */
/*                  :payload in the ISAKMP Message                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo- Session Info Pointer.                    */
/*                  :pu1Pd   - Key Exchange payload to be processed     */
/*                  :u1Flag  - Current payload type(unused)             */
/*                  :                                                   */
/*  Output(s)       :Process the key exchange payload                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessKE (tIkeSessionInfo * pSesInfo, UINT1 *pu1Pd, UINT1 u1Flag)
{
    tIke2KePayLoad      KE;
    UINT4               u4Len = IKE_ZERO;
    UINT2               u2DHGroup = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_BZERO (&KE, sizeof (tIke2KePayLoad));

    UNUSED_PARAM (u1Flag);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_WAIT) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_WAIT))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessKE: Not Expecting KE Packet from peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    else
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_IDLE) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_IDLE))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessKE: Not Expecting KE Packet from peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }

    if (pSesInfo->pDhInfo == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessKE: PFS is not configured for peer: %s\n",
                      pu1PeerAddrStr);
        IKE_SET_ERROR (pSesInfo, IKE2_NO_PROPOSAL_CHOSEN);
        return (IKE_FAILURE);
    }

    /* Copy the KE payload */
    IKE_MEMCPY ((UINT1 *) &KE, pu1Pd, IKE2_KE_PAYLOAD_SIZE);
    KE.u2PayLoadLen = IKE_NTOHS (KE.u2PayLoadLen);
    KE.u2DhGroup = IKE_NTOHS (KE.u2DhGroup);

    /* Check the KE Payload length */
    if (KE.u2PayLoadLen <= sizeof (tIke2KePayLoad))
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessKE: KE payload length is invalid from peer: %s\n",
                      pu1PeerAddrStr);
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        return (IKE_FAILURE);
    }
    u4Len = (UINT4) (KE.u2PayLoadLen - IKE2_KE_PAYLOAD_SIZE);

    /* Check whether the DH Group in the KE payload is matching 
     * with the received the proposal.If it is not macthing sent 
     * INVALID_KE_PAYLOAD notify with the configured DH group. */
    if (KE.u2DhGroup != pSesInfo->pDhInfo->u1Group)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessKE: DH Group KE Payload is not matching"
                      " the DH Group in the selected Proposal Payload/Configured for peer: %s\n",
                      pu1PeerAddrStr);
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_KE_PAYLOAD);
        pSesInfo->NotifyInfo.u2NotifyDataLen = sizeof (UINT2);

        /* Notify Data memory will be freed after constructing 
         * Notify payload */
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->NotifyInfo.pu1NotifyData) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessKE: unable to allocate " "buffer\n");
            return IKE_FAILURE;
        }

        if (pSesInfo->NotifyInfo.pu1NotifyData == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessKE: Mem alloc failed for Nofify data\n");
            return (IKE_FAILURE);
        }

        u2DHGroup = IKE_HTONS ((UINT2) pSesInfo->pDhInfo->u1Group);
        IKE_MEMCPY (pSesInfo->NotifyInfo.pu1NotifyData,
                    &u2DHGroup, sizeof (UINT2));
        pSesInfo->NotifyInfo.u1Protocol = IKE_ISAKMP;
        return (IKE_FAILURE);
    }

    pSesInfo->pHisDhPub = HexNumNew (u4Len);
    if (pSesInfo->pHisDhPub == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessKE: Unable to allocate for pHisDhPub\n");
        return (IKE_FAILURE);
    }

    /* Copy Peer DH value (KE Payload - Generic header)  */
    IKE_MEMCPY (pSesInfo->pHisDhPub->pu1Value, (UINT1 *)
                (pu1Pd + IKE2_KE_PAYLOAD_SIZE), u4Len);

    /* Generate My DH public & private value if not exist */
    if (pSesInfo->pMyDhPub == NULL)
    {
        pSesInfo->pMyDhPub = HexNumNew (u4Len);
        if (pSesInfo->pMyDhPub == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessKE: Unable to allocate for pMyDhPub\n");
            return (IKE_FAILURE);
        }

        pSesInfo->pMyDhPrv = HexNumNew (u4Len);
        if (pSesInfo->pMyDhPrv == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessKE: Unable to allocate for pMyDhPrv\n");
            return (IKE_FAILURE);
        }

        if (FSDhGenerateKeyPair (pSesInfo->pDhInfo, pSesInfo->pMyDhPub,
                                 pSesInfo->pMyDhPrv) < IKE_ZERO)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessKE: Failed to Generate DH Key pair for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    /* Comput the DH secret value */
    pSesInfo->pDhSharedSecret = HexNumNew (u4Len);
    if (pSesInfo->pDhSharedSecret == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessKE: Unable to allocate for Shared Secret\n");
        return (IKE_FAILURE);
    }

    if (FSDhCompute (pSesInfo->pDhInfo, pSesInfo->pMyDhPub, pSesInfo->pMyDhPrv,
                     pSesInfo->pHisDhPub, pSesInfo->pDhSharedSecret) < IKE_ZERO)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessKE: Unable to compute secret for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessId                                      */
/*  Description     :This function is used to process the ID PayLoad of */
/*                  :the ISAKMP Message                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to session info data base     */
/*                  :pu1PayLoad - The ID payload to be processed        */
/*                  :u1Flag     - Current payload type                  */
/*                  :                                                   */
/*  Output(s)       :Process the Id PayLoad                             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessId (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2IdPayLoad      Id;
    tIp4Addr            u4Ip4Addr = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;
    INT1                i1Buf[MAX_NAME_LENGTH];

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessID: Not Expecting ID Payload from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_WAIT)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessID: Not Expecting ID Payload from peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_IDLE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessID: Not Expecting ID Payload for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }

    /* Copy ID payload header */
    IKE_BZERO (&Id, sizeof (tIke2IdPayLoad));
    IKE_MEMCPY ((UINT1 *) &Id, pu1PayLoad, IKE2_ID_PAYLOAD_SIZE);

    Id.u2PayLoadLen = IKE_NTOHS (Id.u2PayLoadLen);

    if (Id.u1IdType == IPV4ADDR)
    {
        IKE_BZERO (&u4Ip4Addr, sizeof (tIp4Addr));
        IKE_MEMCPY (&u4Ip4Addr, (pu1PayLoad + IKE2_ID_PAYLOAD_SIZE),
                    (Id.u2PayLoadLen - IKE2_ID_PAYLOAD_SIZE));
        u4Ip4Addr = IKE_NTOHL (u4Ip4Addr);
    }

    /* If IDi - cmp it with the peer Id  in the policy DB, 
     * If Initiator (IDr) - Cmp it with Peer Id in Policy DB,   
     * If Respoder (IDr) - Cmp it with Local ID in Policy DB */

    if ((u1Flag == IKE2_PAYLOAD_IDR) && (pSesInfo->u1Role == IKE_RESPONDER))
    {
        /* Check the local Id against the received Id */
        if (Id.u1IdType == pSesInfo->pIkeSA->InitiatorID.i4IdType)
        {
            if (Id.u1IdType == IPV4ADDR)
            {
                if (IKE_MEMCMP (&pSesInfo->pIkeSA->InitiatorID.uID,
                                &u4Ip4Addr,
                                pSesInfo->pIkeSA->InitiatorID.u4Length) !=
                    IKE_ZERO)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2ProcessID: IDr do not match with "
                                  "the existing local Id in IKE SA for peer: %s\n",
                                  pu1PeerAddrStr);
                    return (IKE_FAILURE);
                }
            }
            else if (IKE_IPSEC_ID_DER_ASN1_DN == Id.u1IdType)
            {
                if (IkeConvertDERToStr (pu1PayLoad + IKE2_ID_PAYLOAD_SIZE,
                                        Id.u2PayLoadLen - IKE2_ID_PAYLOAD_SIZE,
                                        i1Buf) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ProcessID: Failed converting DER to STR "
                                 "for ID match\n");
                    return (IKE_FAILURE);
                }
                if (IKE_MEMCMP (&pSesInfo->pIkeSA->InitiatorID.uID, i1Buf,
                                pSesInfo->pIkeSA->InitiatorID.u4Length) != 0)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2ProcessID: DN ID's do not match with "
                                  " the existing IKE SA for peer: %s\n",
                                  pu1PeerAddrStr);
                    return (IKE_FAILURE);
                }
            }
            else
            {
                if (IKE_MEMCMP (&pSesInfo->pIkeSA->InitiatorID.uID,
                                (pu1PayLoad + IKE2_ID_PAYLOAD_SIZE),
                                pSesInfo->pIkeSA->InitiatorID.u4Length) !=
                    IKE_ZERO)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2ProcessID: IDr do not match with "
                                  " the Local Id IKE SA for peer: %s\n",
                                  pu1PeerAddrStr);
                    return (IKE_FAILURE);
                }
            }
        }
        else
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessID: IDr do not match with "
                          " the Local Id IKE SA for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
        return (IKE_SUCCESS);
    }

    /* Copy (Id Payload - Generic header) for Hash computation purposes */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1IdPayloadRecd) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessID: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }

    if (pSesInfo->pu1IdPayloadRecd == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2ProcessID: Couldn't allocate memory for storing"
                     "Recd Id Payload\n");
        return (IKE_FAILURE);
    }

    IKE_MEMCPY (pSesInfo->pu1IdPayloadRecd,
                (pu1PayLoad + sizeof (tGenericPayLoad)),
                (Id.u2PayLoadLen - sizeof (tGenericPayLoad)));
    pSesInfo->u4IdPayloadRecdLen = Id.u2PayLoadLen - sizeof (tGenericPayLoad);

    /* Check the responder Id against the received Id */
    if (Id.u1IdType == pSesInfo->pIkeSA->ResponderID.i4IdType)
    {
        if (Id.u1IdType == IPV4ADDR)
        {
            if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID,
                            &u4Ip4Addr,
                            pSesInfo->pIkeSA->ResponderID.u4Length) != IKE_ZERO)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2ProcessID: ID's do not match with "
                              "the existing IKE SA for peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else if (IKE_IPSEC_ID_DER_ASN1_DN == Id.u1IdType)
        {
            if (IkeConvertDERToStr (pu1PayLoad + IKE2_ID_PAYLOAD_SIZE,
                                    Id.u2PayLoadLen - IKE2_ID_PAYLOAD_SIZE,
                                    i1Buf) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessID: Failed converting DER to STR "
                             "for ID match\n");
                return (IKE_FAILURE);
            }
            if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID, i1Buf,
                            pSesInfo->pIkeSA->ResponderID.u4Length) != 0)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2ProcessID: DN ID's do not match with "
                              " the existing IKE SA for peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            if (IKE_MEMCMP (&pSesInfo->pIkeSA->ResponderID.uID,
                            (pu1PayLoad + IKE2_ID_PAYLOAD_SIZE),
                            pSesInfo->pIkeSA->ResponderID.u4Length) != IKE_ZERO)
            {
                IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2ProcessID: ID's do not match with "
                              " the existing IKE SA for peer: %s\n",
                              pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
        }
    }
    else
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessID: Invalid ID recvd from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    /* Lookup RA Node using IDi in case of RAVPN Server. */
    if ((u1Flag == IKE2_PAYLOAD_IDR) && (gbCMServer == TRUE))
    {
        TAKE_IKE_SEM ();
        if (Ike2SessCopyRAPolicy (pSesInfo) == IKE_FAILURE)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessID: Ike2SessCopyRAPolicy Failed!\n");
            return (IKE_FAILURE);
        }
        GIVE_IKE_SEM ();
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessCertficate                              */
/*  Description     :This functions is used to process the certificate  */
/*                  :payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info DataBase  */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :u1Flag     - Current payload type(unused param)    */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the certficate */
/*                  :information                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessCert (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tCertPayLoad        Cert;
    UINT2               u2CertLen = IKE_ZERO;
    tIkeX509           *pX509Cert = NULL;
    tIkeX509Name       *pCAName = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertDB         *pPeerCert = NULL;
    CHR1               *pu1PeerAddrStr = NULL;
    CHR1                au1PeerAddrStr[40];

    UNUSED_PARAM (u1Flag);
    MEMSET (au1PeerAddrStr, 0, sizeof (au1PeerAddrStr));
    IKE_BZERO (&Cert, sizeof (tCertPayLoad));
    IKE_MEMCPY ((UINT1 *) &Cert, pu1PayLoad, IKE_CERT_PAYLOAD_SIZE);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    IKE_MEMCPY (au1PeerAddrStr, pu1PeerAddrStr, STRLEN (pu1PeerAddrStr));

    Cert.u2PayLoadLen = IKE_NTOHS (Cert.u2PayLoadLen);

    if (Cert.u1EncodeType != IKE_X509_SIGNATURE)
    {
        IKE_SET_ERROR (pSesInfo, IKE_CERT_TYPE_UNSUPPORTED);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCert: Unsupported Encode type\n");
        return (IKE_FAILURE);
    }

    u2CertLen = (UINT2) (Cert.u2PayLoadLen - IKE_CERT_PAYLOAD_SIZE);

    if (u2CertLen == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCert: No certificate present\n");
        return (IKE_FAILURE);
    }

    pX509Cert =
        IkeGetX509FromDER ((UINT1 *) (pu1PayLoad + IKE_CERT_PAYLOAD_SIZE),
                           u2CertLen);

    if (pX509Cert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCert: Failed to cert to X509 format\n");
        return (IKE_FAILURE);
    }

    pCAName = IkeGetX509IssuerName (pX509Cert);

    pCASerialNum = IkeGetX509SerialNum (pX509Cert);

    if ((pCAName == NULL) || (pCASerialNum == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCert: Failed to get CA info from cert\n");
        return (IKE_FAILURE);
    }
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCert: Engine not active\n");
        return (IKE_FAILURE);
    }

    pPeerCert =
        IkeGetCertFromPeerCerts (pIkeEngine->au1EngineName, pCAName,
                                 pCASerialNum);

    if (pPeerCert == NULL)
    {
        /* Validate the certificate params */
        if (IkeValidatePeerCert (pSesInfo, pX509Cert) == IKE_FAILURE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessCert: "
                          "Validation of the cert failed for peer: %s\n",
                          pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "Ike2ProcessCert: Validation of the cert failed for peer: %s\n",
                          pu1PeerAddrStr));
            return (IKE_FAILURE);
        }
        else
        {
            /* Verify the certificate */
            if (IkeVerifyCert (pIkeEngine->au1EngineName, pX509Cert) ==
                IKE_FAILURE)
            {
                IKE_SET_ERROR (pSesInfo, IKE_INVALID_CERTIFICATE);
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2ProcessCert: "
                              "Verification of the cert failed for peer: %s\n",
                              au1PeerAddrStr);
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                              "Ike2ProcessCert: Verification of the cert failed for peer: %s\n",
                              au1PeerAddrStr));
                return (IKE_FAILURE);
            }
            else
            {
                /* Add this certificate to certificate database */
                if (IkeAddCertToPeerCerts (pIkeEngine->au1EngineName, NULL,
                                           pX509Cert,
                                           IKE_DYNAMIC_PEER_CERT) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ProcessCert: Unable to "
                                 "allocate memory for peer cert\n");
                    return (IKE_FAILURE);
                }
            }
        }
    }
    /* Store the cert information to the session data structure */
    pSesInfo->PeerCertInfo.pCAName = IkeX509NameDup (pCAName);
    pSesInfo->PeerCertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);
    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                  "Ike2ProcessCert: "
                  "Verification of the certificate successful for peer: %s\n",
                  pu1PeerAddrStr);
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId, "Ike2ProcessCert: "
                  "Verification of the certificate successful for peer: %s\n",
                  pu1PeerAddrStr));
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessCertReq                                 */
/*  Description     :This functions is used to process the certificate  */
/*                  :request payload                                    */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info DataBase  */
/*                  :pu1PayLoad - The current payload to be processed.  */
/*                  :u1Flag     - Current payload type(unused param)    */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the cert req   */
/*                  :information                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessCertReq (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tCertReqPayLoad     CertReq;
    tIkeCertReqInfo    *pCertReq = NULL;
    UINT2               u2HashLen = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (u1Flag);

    /* If the peer authentication method is other than RSA_CERTIFICATE 
     * processing of Cert Request is not required.*/
    if ((pSesInfo->pIkeSA->u2AuthMethod != IKE2_AUTH_RSA) &&
        (pSesInfo->pIkeSA->u2AuthMethod != IKE2_AUTH_DSA))
    {
        return (IKE_SUCCESS);
    }

    IKE_BZERO (&CertReq, sizeof (tCertReqPayLoad));
    IKE_MEMCPY ((UINT1 *) &CertReq, pu1PayLoad, sizeof (tCertReqPayLoad));

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    CertReq.u2PayLoadLen = IKE_NTOHS (CertReq.u2PayLoadLen);

    if (CertReq.u1EncodeType != IKE_X509_SIGNATURE)
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessCertReq: Unsupported Encode type for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    if (MemAllocateMemBlock
        (IKE_CERT_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pCertReq) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCertReq: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pCertReq == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCertReq: Failed to allocate memory "
                     "for certreq\n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pCertReq, sizeof (tIkeCertReqInfo));
    pCertReq->u1EncodeType = CertReq.u1EncodeType;
    pCertReq->pu1HashList = NULL;

    if (CertReq.u2PayLoadLen > IKE_CERT_PAYLOAD_SIZE)
    {

        u2HashLen = (UINT2) (CertReq.u2PayLoadLen - IKE_CERT_PAYLOAD_SIZE);
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pCertReq->pu1HashList) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessCertReq: unable to allocate " "buffer\n");
            MemReleaseMemBlock (IKE_CERT_INFO_MEMPOOL_ID, (UINT1 *) pCertReq);
            return IKE_FAILURE;
        }

        if (pCertReq->pu1HashList == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessCertReq: MemAlloc for storing the "
                         "HashList failed\n");
            MemReleaseMemBlock (IKE_CERT_INFO_MEMPOOL_ID, (UINT1 *) pCertReq);
            return (IKE_FAILURE);
        }
        IKE_BZERO (pCertReq->pu1HashList, ((size_t) u2HashLen + IKE_ONE));
        IKE_MEMCPY (pCertReq->pu1HashList, (pu1PayLoad + CertReq.u2PayLoadLen),
                    u2HashLen);
        pCertReq->u4HashListLen = u2HashLen;

    }

    /* Store certificate request information to session */
    SLL_ADD (&(pSesInfo->PeerCertReq), (t_SLL_NODE *) pCertReq);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessNonce                                   */
/*  Description     :This function is used to process the Nonce PayLoad */
/*                  :of the Isakmp Mesg                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to session info data base     */
/*                  :pu1PayLoad - The Nonce payload to be processed     */
/*                  :u1Flag     - Current payload type(unused param)    */
/*                  :                                                   */
/*  Output(s)       :Process the Nonce PayLoad                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessNonce (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2NoncePayLoad   Nonce;
    INT4                i4NonceLen = IKE_ZERO;
    UINT1               u1NonceType = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (u1Flag);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_WAIT) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_WAIT))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessNonce: Not Expecting Nonce Packet for peer: %s\n",
                          pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }
    else
    {
        if ((pSesInfo->u1FsmState != IKE2_INIT_IDLE) &&
            (pSesInfo->u1FsmState != IKE2_CHILD_IDLE))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessNonce: Not Expecting Nonce Packet from "
                          "peer: %s\n", pu1PeerAddrStr);
            return (IKE_FAILURE);
        }
    }

    IKE_BZERO (&Nonce, sizeof (tIke2NoncePayLoad));

    /* There will be only one NONCE payload.Copy the Nonce payload header */
    IKE_MEMCPY ((UINT1 *) &Nonce, pu1PayLoad, IKE2_NONCE_PAYLOAD_SIZE);
    Nonce.u2PayLoadLen = IKE_NTOHS (Nonce.u2PayLoadLen);

    /* Received nonce data length should be between 16 and 256 */
    i4NonceLen = Nonce.u2PayLoadLen - IKE2_NONCE_PAYLOAD_SIZE;

    if ((i4NonceLen < IKE2_MIN_NONCE_LEN) || (i4NonceLen > IKE2_MAX_NONCE_LEN))
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessNonce: Incorrect Nonce Payload length "
                      "received %d\n", Nonce.u2PayLoadLen);
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        return (IKE_FAILURE);
    }

    /* Copy the recieved nonce to the session info structure.
     * Since we are in ProcessNonce function, if we are Initiator,
     * then we should set the responder nonce, and vice-versa. */
    u1NonceType =
        (pSesInfo->u1Role == IKE_INITIATOR) ? IKE_RESPONDER : IKE_INITIATOR;

    /* The fourth param is to tell the function whether to set the Initiator or
     * Responder Nonce */
    if (Ike2UtilSetNonceInSessionInfo (pSesInfo,
                                       (pu1PayLoad + IKE2_NONCE_PAYLOAD_SIZE),
                                       (UINT2) i4NonceLen,
                                       u1NonceType) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessNonce: Cannot store Nonce in session info\n");
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/**********************************************************************/
/*   Function Name : Ike2ProcessNotify                                */
/*   Description   : This function is used to process the             */
/*                 : Notify PayLoad.                                  */
/*   Input(s)      : pSesInfo   - Pointer to session info data base.  */
/*                 : pu1PayLoad - The current payload to be processed.*/
/*                 : u1Flag     - Current payload type(unused param)  */
/*                 :                                                  */
/*   Output(s)     : None.                                            */
/*   Return Values : IKE_FAILURE or IKE_SUCCESS                       */
/**********************************************************************/
INT1
Ike2ProcessNotify (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2NotifyPayLoad  Notify;
    tIkeIPSecIfParam    IkeIPSecIfParam;
    UINT1              *pu1Ptr = NULL;
    INT1                i1RetVal = IKE_SUCCESS;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (u1Flag);

    IKE_MEMCPY (&Notify, pu1PayLoad, sizeof (tIke2NotifyPayLoad));
    Notify.u2NotifyMsgType = IKE_NTOHS (Notify.u2NotifyMsgType);
    Notify.u2PayLoadLen = IKE_NTOHS (Notify.u2PayLoadLen);

    IncIkeNoOfNotifyRecv (&pSesInfo->pIkeSA->IpLocalAddr);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if ((Notify.u1ProtocolId != IKE_ISAKMP) &&
        (Notify.u1ProtocolId != IKE_IPSEC_AH) &&
        (Notify.u1ProtocolId != IKE_IPSEC_ESP) &&
        (Notify.u1ProtocolId != IKE_NONE))
    {
        IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessNotify: Invalid Protocol value %d: from "
                      "peer: %s\n", Notify.u1ProtocolId, pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    if (Notify.u1SpiSize > (Notify.u2PayLoadLen - sizeof (tIke2NotifyPayLoad)))
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessNotify: Invalid SPI Length from peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    /* Point to the payload of Notify */
    /* If SPI is present it would point to the spi else it would point to the *
     * notification information */
    pu1Ptr = pu1PayLoad + sizeof (tIke2NotifyPayLoad);

    switch (Notify.u2NotifyMsgType)
    {
        case IKE2_NAT_DETECTION_SOURCE_IP:
        {
            Ike2NattProcessDetectSrcIP (pSesInfo, pu1Ptr);
            break;
        }

        case IKE2_NAT_DETECTION_DESTINATION_IP:
        {
            Ike2NattProcessDetectDstIP (pSesInfo, pu1Ptr);
            break;
        }

        case IKE2_INVALID_SYNTAX:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID SYNTAX recd\n");
            break;
        }

        case IKE2_INVALID_MESSAGE_ID:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID MSG ID recd\n");
            break;
        }

        case IKE2_INITIAL_CONTACT:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: "
                         "INITIAL CONTACT received for peer: %s\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "Ike2ProcessNotify:INITIAL CONTACT received for peer: %s\n",
                          pu1PeerAddrStr));
            IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));

            IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
            IkeIPSecIfParam.IPSecReq.DelSAInfo.u1IkeVersion =
                IKE2_MAJOR_VERSION;

            IKE_MEMCPY (&(IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr),
                        &pSesInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));
            IKE_MEMCPY (&(IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr),
                        &pSesInfo->pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));

            if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessNotify: Unable to send to "
                             "delete SA information to IPSec\n");
                return (IKE_FAILURE);
            }
            break;
        }

        case IKE2_SINGLE_PAIR_REQUIRED:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: SINGLE_PAIR_REQUIRED recd\n");
            break;
        }

        case IKE2_NO_ADDITIONAL_SAS:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: NO_ADDITIONAL_SAS recd\n");
            break;
        }

        case IKE2_SET_WINDOW_SIZE:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: SET_WINDOW_SIZE recd\n");
            break;
        }

        case IKE2_ADDITIONAL_TS_POSSIBLE:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: ADDITIONAL_TS_POSSIBLE recd\n");
            break;
        }

        case IKE2_IPCOMP_SUPPORTED:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: IPCOMP_SUPPORTED recd\n");
            break;
        }

        case IKE2_COOKIE:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: COOKIE recd\n");
            break;
        }

        case IKE2_USE_TRANSPORT_MODE:
        {
            pSesInfo->Ike2AuthInfo.bTransportReqd = IKE_TRUE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: USE_TRANSPORT_MODE recd\n");
            break;
        }

        case IKE2_HTTP_CERT_LOOKUP_SUPPORTED:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: HTTP_CERT_LOOKUP_SUPP recd\n");
            break;
        }

        case IKE2_REKEY_SA:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: REKEY_SA recd\n");
            break;
        }

        case IKE2_ESP_TFC_PADDING_NOT_SUPPORTED:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: ESP_TFC_PAD_NOT_SUPP recd\n");
            break;
        }

        case IKE2_NON_FIRST_FRAGMENTS_ALSO:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: NON_FIRST_FRAG_ALSO recd\n");
            break;
        }

        case IKE2_UNSUPPORTED_CRITICAL_PAYLOAD:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: UNSUPP_CRIT_PAYLOAD recd\n");
            break;
        }

        case IKE2_INVALID_IKE_SPI:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID_IKE_SPI recd\n");
            break;
        }

        case IKE2_INVALID_MAJOR_VERSION:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID_MAJ_VER recd\n");
            break;
        }

        case IKE2_INVALID_SPI:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID_SPI recd\n");
            break;
        }

        case IKE2_NO_PROPOSAL_CHOSEN:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: NO_PROPOSAL_CHOOSEN recd\n");
            break;
        }

        case IKE2_INVALID_KE_PAYLOAD:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVLD_KE_PAYLOAD recd\n");
            break;
        }

        case IKE2_AUTHENTICATION_FAILED:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: AUTH_FAILED recd\n");
            break;
        }

        case IKE2_TS_UNACCEPTABLE:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: TS_UNACCEPTABLE recd\n");
            break;
        }

        case IKE2_INTERNAL_ADDRESS_FAILURE:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INTERNAL_ADDR_FAIL recd\n");
            break;
        }

        case IKE2_FAILED_CP_REQUIRED:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: CP_REQUIRED recd\n");
            break;
        }

        case IKE2_INVALID_SELECTORS:
        {
            i1RetVal = IKE_FAILURE;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessNotify: INVALID_SELECTORS recd\n");
            break;
        }

        default:
        {
            i1RetVal = IKE_SUCCESS;
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessNotify: Notify Msg Type  - %d recd!\n",
                          Notify.u2NotifyMsgType);
            break;
        }
    }
    return (i1RetVal);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessAuth                                    */
/*  Description     :This function is used to procees the Auth payload  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info DataBase  */
/*                  :pu1PayLoad - Pointer to the Auth payload           */
/*                  :u1Flag     - Current payload type(unused param)    */
/*                  :                                                   */
/*  Output(s)       :Process the Auth payload                           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessAuth (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2AuthPayLoad    Auth;
    UINT1              *pu1AuthData = NULL;
    UINT4               u4AuthDataLen = IKE_ZERO;

    UNUSED_PARAM (u1Flag);

    if (pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessAuth: Not Expecting AUTH Payload\n");
        return (IKE_FAILURE);

    }
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_WAIT)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessAuth: Not Expecting AUTH Payload\n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_IDLE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessAuth: Not Expecting AUTH Payload\n");
            return (IKE_FAILURE);
        }
    }

    /* Fill the auth payload header */
    IKE_BZERO (&Auth, sizeof (tIke2AuthPayLoad));
    IKE_MEMCPY ((UINT1 *) &Auth, pu1PayLoad, IKE2_AUTH_PAYLOAD_SIZE);
    Auth.u2PayLoadLen = IKE_NTOHS (Auth.u2PayLoadLen);
    Auth.u2PayLoadLen =
        (UINT2) (Auth.u2PayLoadLen - (UINT2) sizeof (tIke2AuthPayLoad));

    /* Check the auth data length */
    if (((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE2_PRF_HMAC_MD5) &&
         ((Auth.u2PayLoadLen != IKE_MD5_LENGTH) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_64) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_128) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_256) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_512) &&
          (Auth.u2PayLoadLen != IKE2_DSA_HASH_LEN))) ||
        ((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE2_PRF_HMAC_SHA1) &&
         ((Auth.u2PayLoadLen != IKE_SHA_LENGTH) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_64) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_128) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_256) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_512) &&
          (Auth.u2PayLoadLen != IKE2_DSA_HASH_LEN))) ||
        ((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE2_PRF_HMAC_SHA256) &&
         ((Auth.u2PayLoadLen != IKE_SHA256_LENGTH) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_64) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_128) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_256) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_512) &&
          (Auth.u2PayLoadLen != IKE2_DSA_HASH_LEN))) ||
        ((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE2_PRF_HMAC_SHA384) &&
         ((Auth.u2PayLoadLen != IKE_SHA384_LENGTH) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_64) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_128) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_256) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_512) &&
          (Auth.u2PayLoadLen != IKE2_DSA_HASH_LEN))) ||
        ((pSesInfo->pIkeSA->u1Phase1HashAlgo == IKE2_PRF_HMAC_SHA512) &&
         ((Auth.u2PayLoadLen != IKE_SHA512_LENGTH) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_64) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_128) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_256) &&
          (Auth.u2PayLoadLen != IKE2_CERT_SIG_LEN_512) &&
          (Auth.u2PayLoadLen != IKE2_DSA_HASH_LEN))))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessAuth: Hash Algo data length mismatch\n");
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        return (IKE_FAILURE);
    }

    /* Construct the auth data to calculate the digest */
    if (Ike2AuthConstructhMsg (pSesInfo, &pu1AuthData, &u4AuthDataLen,
                               IKE2_PROCESS_AUTH) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessAuth:Compute Auth message fails\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);
        return (IKE_FAILURE);
    }

    /* Compute and compare the digest value */
    if (Ike2AuthVerifyDigest (pSesInfo, pu1AuthData, u4AuthDataLen,
                              Auth.u1AuthMethod,
                              (pu1PayLoad + IKE2_AUTH_PAYLOAD_SIZE)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessAuth: Auth diget comparison failed\n");
        IKE_SET_ERROR (pSesInfo, IKE2_AUTHENTICATION_FAILED);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);
        return (IKE_FAILURE);
    }
    /* Set the Auth Done flag to IKE_TRUE */
    pSesInfo->pIkeSA->Ike2SA.bAuthDone = IKE_TRUE;
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);

    return (IKE_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Ike2ProcessEncryptPayload                          */
/*  Description   : Function to decrypt the ISAKMP payload.            */
/*  Input(s)      : pSessionInfo - Session Info Pointer.               */
/*                : pu1PayLoad   - The current payload to be processed */
/*                : u1Flag       - Current payload type(unused param)  */
/*  Output(s)     : None.                                              */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                         */
/***********************************************************************/
INT1
Ike2ProcessEncryptPayload (tIkeSessionInfo * pSessionInfo, UINT1 *pu1PayLoad,
                           UINT1 u1Flag)
{
    UINT1              *pu1LocalPtr = pSessionInfo->IkeRxPktInfo.pu1RawPkt;
    UINT1              *pu1SKeyIdA = NULL;
    UINT1              *pu1Hash;
    UINT1               u1PadLen = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT4               u4IVLen = IKE_ZERO;
    UINT4               u4HashMsgLen = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4DigestLen = IKE_ZERO;
    tIke2EncrPayload   *pEncryptHdr = NULL;
    UINT1              *pu1IV = NULL;
    tIsakmpHdr         *pIsakmpHdr =
        ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt));
    unUtilAlgo          UtilAlgo;
    unArCryptoKey       ArCryptoKey;

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (pu1PayLoad);

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Hash) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessEncryptPayload: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (pu1Hash, IKE_MAX_DIGEST_SIZE);

    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessEncryptPayload: Not Expecting an "
                     "Encrypted Packet\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }
    /* Copy Encrypt payload header */
    pEncryptHdr = (tIke2EncrPayload *)
        (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr));

    /* Total Length of the packet */
    u4Len = IKE_NTOHL (pIsakmpHdr->u4TotalLen);

    /* Key Length of Integrity Algorithm */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Digest length of Integrity Algorithm */
    u4DigestLen = (UINT4)
        Ike2GetDigestLenFromAlgo (pSessionInfo->pIkeSA->u1Phase1HashAlgo);

    /* IV Length is equal to the block length of the encryption algorithm */
    u4IVLen = gaIke2CipherAlgoList
        [pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;

    /* IV  */
    pu1IV = (pu1LocalPtr + sizeof (tIsakmpHdr) + sizeof (tIke2EncrPayload));

    /* Key to calculate the Intergrity checksum */
    if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
    {
        pu1SKeyIdA = pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdAr;
    }
    else
    {
        pu1SKeyIdA = pSessionInfo->pIkeSA->pSKeyIdA;
    }

    /* Hash Message Length = Total length -  Check sum length(12) */
    u4HashMsgLen = u4Len - u4DigestLen;

    /* Calculate the Integrity Checksum */
    if ((*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pu1SKeyIdA, (INT4) u4HashLen, pu1LocalPtr, (INT4) u4HashMsgLen,
         pu1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessEncryptPayload: Unsupported Hash "
                     "algorithm\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }

    /* Compare the integrity checksum with the received with checksum */
    if (IKE_MEMCMP ((pu1LocalPtr + u4HashMsgLen), pu1Hash, u4DigestLen) !=
        IKE_ZERO)
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessEncryptPayload:Integrity checksum failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }

    /* Reset the Digest in RawPacket Buffer */
    IKE_BZERO ((pSessionInfo->IkeRxPktInfo.pu1RawPkt + u4HashMsgLen),
               u4DigestLen);

    /* Update the total length of ISAKMP header as 
     * (Total length - Digest length) */
    pIsakmpHdr->u4TotalLen = IKE_HTONL ((u4Len - u4DigestLen));

    /* Update the EncrPayload Header as (sizeof(tIke2EncrPayload) + IV len */
    pEncryptHdr->u2PayLoadLen = IKE_HTONS ((sizeof (tIke2EncrPayload) +
                                            u4IVLen));

    /* Mark the pointer to encrypted data by Skipping the ISAKMP 
     * header ,EncrPayload and IV */
    pu1LocalPtr += (sizeof (tIsakmpHdr) + sizeof (tIke2EncrPayload) + u4IVLen);

    /* Calculate the length of Encrypted Buffer */
    u4Len = u4Len - (sizeof (tIsakmpHdr) + sizeof (tIke2EncrPayload) +
                     u4IVLen + u4DigestLen);
    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1LocalPtr;
    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Len;
    UtilAlgo.UtilDesAlgo.pu1DesInitVect = pu1IV;
    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
        gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4KeyLen;
    UtilAlgo.UtilDesAlgo.u4DesInitVectLen =
        gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
    switch (pSessionInfo->pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE2_ENCR_DES:
        {
            /* Decryption Key */
            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes1,
                            sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
                UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            }
            else
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
                UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            }
            break;
        }

        case IKE2_ENCR_3DES:
        {
            /* Decryption Key */
            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes1,
                            sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes2,
                            sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes3,
                            sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                    (pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr +
                     IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                    (pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr +
                     IKE_TWO * IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            }
            else
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey2, sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey3, sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                    (pSessionInfo->pIkeSA->pSKeyIdE + IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                    (pSessionInfo->pIkeSA->pSKeyIdE + IKE_TWO *
                     IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            }
            break;
        }
        case IKE2_ENCR_AES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }

            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                             IKE_EIGHT);
            }
            else
            {
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                             IKE_EIGHT);
            }

            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1LocalPtr;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Len;
            UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                pSessionInfo->pIkeSA->au1AesDecrKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector = pu1IV;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
                u4BlockLen;
            UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;

            /* Decryption Key */
            UtilDecrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo);
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessEncryptPayload: Encryption Algo"
                         " unknown/not supported\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
            return (IKE_FAILURE);
        }
    }

    /* Calculate the padding length , Digest length and subtract it 
     * from the total length */
    u4Len = IKE_NTOHL (pIsakmpHdr->u4TotalLen);

    MEMCPY (&u1PadLen, (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                        (u4Len - IKE_ONE)), sizeof (UINT1));

    /* Update the total length of ISAKMP header by removing 
     * padding length + pad length */
    pIsakmpHdr->u4TotalLen =
        (UINT4) IKE_HTONL ((UINT4) (u4Len - (UINT4) (u1PadLen + IKE_ONE)));

    if (Ike2UtilCheckKBTrasmitted (pSessionInfo, IKE_DECRYPTION) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessEncryptPayload: "
                     "Ike2UtilCheckKBTransmitted failed!\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }
    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
    return (IKE_SUCCESS);
}

/*************************************************************************/
/*  Function Name   :Ike2ProcessDelete                                   */
/*  Description     :This function is used to process the Delete PayLoad */
/*                  :of the Isakmp Mesg                                  */
/*                  :                                                    */
/*  Input(s)        :pSesInfo   - Pointer to session info data base      */
/*                  :pu1PayLoad - The current payload to be processed    */
/*                  :u1Flag     - Current payload type (unused param)    */
/*                  :                                                    */
/*  Output(s)       :Process the Delete PayLoad                          */
/*                  :                                                    */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                          */
/*                  :                                                    */
/*************************************************************************/
INT1
Ike2ProcessDelete (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2DeletePayLoad  DelPayload;
    INT4                i4DelLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL;
    tIsakmpHdr         *pIsakmpHdr = NULL;
    UINT4               u4SpiCount = IKE_ZERO;
    tIkeDelSpi          DeleteSpi;
    tIkeIPSecIfParam    IkeIPSecIfParam;
    CHR1               *pu1PeerAddrStr = NULL;

    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (i4DelLen);
    UNUSED_PARAM (pIsakmpHdr);

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    /* Delete Message will be recieved only in Informational
     * exchange */
    if (pSesInfo->u1ExchangeType != IKE2_INFO_EXCH)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessDelete: Delete information in "
                      "unsupported exchange for peer: %s\n", pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    IKE_BZERO (&DelPayload, sizeof (tIke2DeletePayLoad));
    IKE_MEMCPY (&DelPayload, pu1PayLoad, sizeof (tIke2DeletePayLoad));
    DelPayload.u2PayLoadLen = IKE_NTOHS (DelPayload.u2PayLoadLen);
    DelPayload.u2SpiCount = IKE_NTOHS (DelPayload.u2SpiCount);

    /* Check for the received payload size */
    if ((DelPayload.u1SpiSize * DelPayload.u2SpiCount) !=
        (DelPayload.u2PayLoadLen - sizeof (tIke2DeletePayLoad)))
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessDelete: Invalid delete payload size for peer: %s\n",
                      pu1PeerAddrStr);
        return (IKE_FAILURE);
    }

    i4DelLen = (INT4) (DelPayload.u2PayLoadLen - sizeof (tIke2DeletePayLoad));

    pu1Ptr = pu1PayLoad + sizeof (tIke2DeletePayLoad);

    /* In Phase process the delete message */
    switch (DelPayload.u1ProtocolId)
    {
        case IKE_ISAKMP:
        {
            IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
            IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_ADD_TIME_DEL_SA;

            IkeIPSecIfParam.IPSecReq.DelSAInfo.u1IkeVersion =
                IKE2_MAJOR_VERSION;
            IKE_MEMCPY (&(IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr),
                        &pSesInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));
            IKE_MEMCPY (&(IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr),
                        &pSesInfo->pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));

            /* As specified in RFC 5996, section 1.4.1, Deleting an IKE SA  
             * should implicitly delete, remaining Child SAs negotiated 
             * under it. */
            if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessDelete: Unable to send to "
                             "delete SA information to IPSec\n");
                return (IKE_FAILURE);
            }
            pIsakmpHdr =
                (tIsakmpHdr *) (void *) (pSesInfo->IkeRxPktInfo.pu1RawPkt);

            if ((DelPayload.u1SpiSize != IKE_ZERO) &&
                (DelPayload.u2SpiCount != IKE_ONE))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessDelete:Invalid SPI for IKE in delete "
                             " payload\n");
                return (IKE_FAILURE);
            }

            /* Stop the Lifetime timer if already running and 
             * start a 1 minute timer to delete IKE SA only 
             * 1) we are the responder and we have not replied to the delete
             *  request before. 
             * 2) In case of Initiator, when we receive a Info Delete message
             * reply for the request that we have sent.   */

            IkeStopTimer (&pSesInfo->pIkeSA->IkeSALifeTimeTimer);
            if (IkeStartTimer (&pSesInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID,
                               (IKE_TIMER_COUNT * IKE_MIN_TIMER_INTERVAL),
                               pSesInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessDelete: IkeStartTimer failed!\n");
                return (IKE_FAILURE);
            }
            /* Set the SA status to IKE_DESTROY, as it should not be 
             * used any more. */
            pSesInfo->pIkeSA->u1SAStatus = IKE_DESTROY;
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessDelete: Successfully deleted Phase1 SA for peer: %s\n",
                          pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "Ike2ProcessDelete: Successfully deleted Phase1 SA for peer: %s\n",
                          pu1PeerAddrStr));
            break;
        }
        case IKE_IPSEC_AH:
        case IKE_IPSEC_ESP:
        {
            IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));
            IKE_BZERO (&DeleteSpi, sizeof (tIkeDelSpi));

            if (DelPayload.u1ProtocolId == IKE_IPSEC_ESP)
            {
                DeleteSpi.u1Protocol = IKE_IP_PROTO_ESP;
            }
            else
            {
                DeleteSpi.u1Protocol = IKE_IP_PROTO_AH;
            }

            if (DelPayload.u2SpiCount <= IKE_MAX_NUM_SPI)
            {
                DeleteSpi.u2NumSPI = DelPayload.u2SpiCount;
                IKE_MEMCPY (DeleteSpi.au4SPI, pu1Ptr,
                            IKE_IPSEC_SPI_SIZE * DelPayload.u2SpiCount);
            }
            else
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2ProcessDelete: "
                              "No.of SPI count received exceeds the "
                              "limit %d\n", IKE_MAX_NUM_SPI);
                return (IKE_FAILURE);
            }
            for (u4SpiCount = IKE_ZERO; u4SpiCount < DelPayload.u2SpiCount;
                 u4SpiCount++)
            {
                DeleteSpi.au4SPI[u4SpiCount] =
                    IKE_NTOHL (DeleteSpi.au4SPI[u4SpiCount]);
            }

            IKE_MEMCPY (&DeleteSpi.RemoteAddr, &pSesInfo->pIkeSA->IpPeerAddr,
                        sizeof (tIkeIpAddr));
            IKE_MEMCPY (&DeleteSpi.LocalAddr, &pSesInfo->pIkeSA->IpLocalAddr,
                        sizeof (tIkeIpAddr));

            IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DELETE_SA;

            DeleteSpi.u1IkeVersion = IKE2_MAJOR_VERSION;
            /* Intimate IPSec whether to delete only the SAs or
               all (SA, Policy, ACL and Selectors ) */
            if (pSesInfo->pIkeSA->bCMDone == TRUE)
            {
                DeleteSpi.bDelDynIPSecDB = TRUE;
            }
            else
            {
                DeleteSpi.bDelDynIPSecDB = FALSE;
            }

            IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSpiInfo, &DeleteSpi,
                        sizeof (tIkeDelSpi));

            if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessDelete: Unable to send to "
                             "delete SA information to IPSec\n");
                return (IKE_FAILURE);
            }
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2ProcessDelete: Successfully deleted Phase2 SA for peer: %s\n",
                          pu1PeerAddrStr);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "Ike2ProcessDelete: Successfully deleted Phase2 SA for peer: %s\n",
                          pu1PeerAddrStr));

            break;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessDelete: Invalid Protocol value\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/*************************************************************************/
/*  Function Name   :Ike2ProcessVendorId                                 */
/*  Description     :This function is used to process the vendorId       */
/*                  :PayLoad of the Isakmp Mesg                          */
/*                  :                                                    */
/*  Input(s)        :pSesInfo   - Pointer to session info data base      */
/*                  :pu1PayLoad - The current payload to be processed    */
/*                  :u1Flag     - Current payload type(unused param)     */
/*                  :                                                    */
/*  Output(s)       :Process the VendorId PayLoad                        */
/*                  :                                                    */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                          */
/*                  :                                                    */
/*************************************************************************/
INT1
Ike2ProcessVendorId (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                     UINT1 u1Flag)
{
    tVendorIdPayLoad    VendorId;
    UINT2               u2VendorIDLen = IKE_ZERO;

    UNUSED_PARAM (u1Flag);

    IKE_BZERO (&VendorId, sizeof (tVendorIdPayLoad));
    IKE_MEMCPY ((UINT1 *) &VendorId, pu1PayLoad, IKE_VID_PAYLOAD_SIZE);
    VendorId.u2PayLoadLen = IKE_NTOHS (VendorId.u2PayLoadLen);

    if (VendorId.u2PayLoadLen == IKE_VID_PAYLOAD_SIZE)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "Ike2ProcessVendorId: No VendorID field!\n");
        /* We will not fail the negotiation because of this */
        return (IKE_SUCCESS);
    }

    if (pSesInfo->u4NoofInVendorIDs >= IKE_MAX_VENDOR_IDS)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "Ike2ProcessVendorId: Vendor ID array is full!\n");
        /* We will not fail the negotiation because of this */
        return (IKE_SUCCESS);
    }

    u2VendorIDLen =
        (UINT2) (VendorId.u2PayLoadLen - (UINT2) sizeof (tVendorIdPayLoad));

    if (u2VendorIDLen > IKE_MAX_VENDOR_ID_SIZE)
    {
        u2VendorIDLen = IKE_MAX_VENDOR_ID_SIZE;
    }

    IKE_MEMCPY (pSesInfo->aInVendorIDs[pSesInfo->u4NoofInVendorIDs].
                au1VendorID, (pu1PayLoad + IKE_VID_PAYLOAD_SIZE),
                u2VendorIDLen);
    pSesInfo->u4NoofInVendorIDs++;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessTS                                      */
/*  Description     :This function is used to process the TSi           */
/*                  :payload in an IKE Message                          */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to session info data base     */
/*                  :pu1PayLoad - The current payload to be processed   */
/*                  :u1Flag     - Current payload type                  */
/*                  :                                                   */
/*  Output(s)       :Processes the TS  payload                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessTS (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    UINT1              *pu1TempPtr = pu1PayLoad;

    tIke2TsPayLoad      IkeTsPayload;
    tIke2TsInfo         TsInfo;
    tIkeNetwork        *pIkeNetwork = NULL;
    tIkeIpv4Range       Ipv4Range;
    tIkeIpv6Range       Ipv6Range;
    UINT1               u1MatchFound = IKE_ZERO;
    UINT1               u1TsNum = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &IkeTsPayload, sizeof (tIke2TsPayLoad));
    IKE_BZERO ((UINT1 *) &TsInfo, sizeof (tIke2TsInfo));
    IKE_BZERO ((UINT1 *) &Ipv4Range, sizeof (tIkeIpv4Range));
    IKE_BZERO ((UINT1 *) &Ipv6Range, sizeof (tIkeIpv6Range));

    /* In case of Ravpn Server --> TSi(0.0.0.0/32), return SUCCESS 
     *                             TSr ,compare with RaPolicy ProtectedNetwork 
     * Other cases(Initiator)  --> TSi ,compare with IkeCryptoMap.LocalNetwork
     *                             TSr ,compare with IkeCryptoMap.RemoteNetwork
     *            (Responder)  --> TSi ,compare with IkeCryptoMap.RemoteNetwork
     *                             TSr ,compare with IkeCryptoMap.LocalNetwork 
     */
    if ((pSesInfo->pIkeSA->bCMEnabled == TRUE) &&
        (pSesInfo->Ike2AuthInfo.bCMServer == IKE_TRUE) &&
        (pSesInfo->pIkeSA->bCMDone != IKE_TRUE))
    {
        if (u1Flag == IKE2_PAYLOAD_TSR)
        {
            pIkeNetwork = &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                            CMServerInfo.ProtectedNetwork[IKE_INDEX_0]);
        }
        else
        {
            return IKE_SUCCESS;
        }
    }
    else
    {
        if (u1Flag == IKE2_PAYLOAD_TSI)
        {
            if (pSesInfo->u1Role == IKE_INITIATOR)
            {
                pIkeNetwork =
                    &(pSesInfo->Ike2AuthInfo.IkeCryptoMap.LocalNetwork);
            }
            else
            {
                pIkeNetwork =
                    &(pSesInfo->Ike2AuthInfo.IkeCryptoMap.RemoteNetwork);
            }
        }
        else
        {
            if (pSesInfo->u1Role == IKE_INITIATOR)
            {
                pIkeNetwork =
                    &(pSesInfo->Ike2AuthInfo.IkeCryptoMap.RemoteNetwork);
            }
            else
            {
                pIkeNetwork =
                    &(pSesInfo->Ike2AuthInfo.IkeCryptoMap.LocalNetwork);
            }
        }
    }
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        /* Convert the IPV4 subnet into IPv4 Range */
        IkeConvertToRangeForIpv4 (pIkeNetwork, &Ipv4Range);
    }
    else
    {
        /* Convert the IPV6 subnet into IPv6 Range */
        IkeConvertToRangeForIpv6 (pIkeNetwork, &Ipv6Range);
    }

    /* Copy the TS payload header */
    IKE_MEMCPY ((UINT1 *) &IkeTsPayload, pu1TempPtr, sizeof (tIke2TsPayLoad));
    pu1TempPtr += sizeof (tIke2TsPayLoad);
    IkeTsPayload.u2PayLoadLen = IKE_NTOHS (IkeTsPayload.u2PayLoadLen);

    u1MatchFound = IKE_FALSE;
    for (u1TsNum = IKE_ONE; u1TsNum <= IkeTsPayload.u1NumTS; u1TsNum++)
    {
        /* Copy the TSi payload */
        IKE_MEMCPY ((UINT1 *) &TsInfo, pu1TempPtr, IKE2_TS_SEL_LEN);
        pu1TempPtr += IKE2_TS_SEL_LEN;

        TsInfo.u2SelectorLen = IKE_NTOHS (TsInfo.u2SelectorLen);
        TsInfo.u2StartPort = IKE_NTOHS (TsInfo.u2StartPort);
        TsInfo.u2EndPort = IKE_NTOHS (TsInfo.u2EndPort);

        if (TsInfo.u1TSType == IKE2_TS_IPV4_ADDR_RANGE)
        {
            IKE_MEMCPY (&(TsInfo.StartAddr.Ipv4Addr), pu1TempPtr,
                        sizeof (tIp4Addr));
            pu1TempPtr += sizeof (tIp4Addr);
            IKE_MEMCPY (&(TsInfo.EndAddr.Ipv4Addr), pu1TempPtr,
                        sizeof (tIp4Addr));
            pu1TempPtr += sizeof (tIp4Addr);
            TsInfo.StartAddr.Ipv4Addr = IKE_HTONL (TsInfo.StartAddr.Ipv4Addr);
            TsInfo.EndAddr.Ipv4Addr = IKE_HTONL (TsInfo.EndAddr.Ipv4Addr);
        }
        else
        {
            IKE_MEMCPY (&(TsInfo.StartAddr.Ipv6Addr), pu1TempPtr,
                        sizeof (tIp6Addr));
            pu1TempPtr += sizeof (tIp6Addr);
            IKE_MEMCPY (&(TsInfo.EndAddr.Ipv6Addr), pu1TempPtr,
                        sizeof (tIp6Addr));
            pu1TempPtr += sizeof (tIp6Addr);
        }

        /* Compare the Protocol received in the payload with the configured 
         * protocol.
         * If no match, set error and return failure */
        if (TsInfo.u1ProtocolId != pIkeNetwork->u1HLProtocol)
        {
            continue;
        }

        /* Compare the start & end ports with our configured ones.
         * If no match, set error and return failure */
        if (TsInfo.u2StartPort != pIkeNetwork->u2StartPort)
        {
            continue;
        }

        if (TsInfo.u2EndPort != pIkeNetwork->u2EndPort)
        {
            continue;
        }

        /* Compare the address received with the configured ones. */
        if (TsInfo.u1TSType == IKE2_TS_IPV4_ADDR_RANGE)
        {
            /* In case of Ravpn Client ,the configured Cryptomap Localnetwork 
             * will be of 32 bit mask. So if the Ravpn Server sents a TSi of 
             * type range we should not fail to process.So checking for range */
            if ((u1Flag == IKE2_PAYLOAD_TSI) &&
                (pSesInfo->pIkeSA->bCMEnabled == TRUE) &&
                (pSesInfo->Ike2AuthInfo.bCMServer == IKE_FALSE) &&
                (pSesInfo->pIkeSA->bCMDone == IKE_TRUE))
            {
                if ((Ipv4Range.Ip4StartAddr < TsInfo.StartAddr.Ipv4Addr) ||
                    (Ipv4Range.Ip4StartAddr > TsInfo.EndAddr.Ipv4Addr))
                {
                    continue;
                }
            }
            else
            {
                if (TsInfo.StartAddr.Ipv4Addr != Ipv4Range.Ip4StartAddr)
                {
                    continue;
                }

                if (TsInfo.EndAddr.Ipv4Addr != Ipv4Range.Ip4EndAddr)
                {
                    continue;
                }
            }
        }
        else
        {
            if (IKE_MEMCMP
                (&(TsInfo.StartAddr.Ipv6Addr), &(Ipv6Range.Ip6StartAddr),
                 sizeof (tIp6Addr)) != IKE_ZERO)
            {
                continue;
            }

            if (IKE_MEMCMP (&(TsInfo.EndAddr.Ipv6Addr), &(Ipv6Range.Ip6EndAddr),
                            sizeof (tIp6Addr)) != IKE_ZERO)
            {
                continue;
            }
        }
        u1MatchFound = IKE_TRUE;
    }                            /* End of the For Loop for Multiple TS payloads */

    if (u1MatchFound != IKE_TRUE)
    {
        IKE_SET_ERROR (pSesInfo, IKE2_TS_UNACCEPTABLE);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessTS: Address Not Supported!\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessCfg                                     */
/*  Description     :This function is used to procees the config payload*/
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info DataBase  */
/*                  :pu1PayLoad - The current payload to be processed   */
/*                  :u1Flag     - Current payload type(unused param)    */
/*                  :                                                   */
/*  Output(s)       :Process the config payload                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessCfg (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    tIke2ConfigPayLoad  Cfg;
    UINT1              *pu1TempPtr = NULL;
    UINT4               u4Len = IKE_ZERO;

    UNUSED_PARAM (u1Flag);

    if (pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCfg: Not Expecting Cfg Payload\n");
        return (IKE_FAILURE);

    }
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_WAIT)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessCfg: Not Expecting Cfg Payload\n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (pSesInfo->u1FsmState != IKE2_AUTH_IDLE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ProcessCfg:Not Expecting Cfg Packet\n");
            return (IKE_FAILURE);
        }
    }

    IKE_BZERO (&Cfg, sizeof (tIke2ConfigPayLoad));

    /* Copy the Cfg payload header */
    IKE_MEMCPY ((UINT1 *) &Cfg, pu1PayLoad, sizeof (tIke2ConfigPayLoad));
    Cfg.u2PayLoadLen = IKE_NTOHS (Cfg.u2PayLoadLen);

    /*Check the Cfg Payload length */
    if (Cfg.u2PayLoadLen <= sizeof (tIke2ConfigPayLoad))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessCfg: Config payload length is invalid\n");
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        return (IKE_FAILURE);
    }
    u4Len = Cfg.u2PayLoadLen - sizeof (tIke2ConfigPayLoad);

    /* Based upon Cfg type call appropriate function */
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        switch (Cfg.u1CfgType)
        {
            case IKE2_CFG_REPLY:
            {
                pu1TempPtr = pu1PayLoad + sizeof (tIke2ConfigPayLoad);
                if (Ike2CfgCheckCMCfgRepAttr (pSesInfo, pu1TempPtr,
                                              u4Len) == IKE_FAILURE)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ProcessCfg:"
                                 "Ike2CheckCMCfgRepAttr Failed \n");
                    return (IKE_FAILURE);
                }

                break;
            }
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessCfg: CFG Type not supported!!\n");
                return (IKE_FAILURE);
            }
        }
    }
    else
    {
        switch (Cfg.u1CfgType)
        {
            case IKE2_CFG_REQUEST:
            {
                pu1TempPtr = pu1PayLoad + sizeof (tIke2ConfigPayLoad);
                if (Ike2CfgCheckCMReqAttr (pSesInfo, pu1TempPtr,
                                           u4Len) == IKE_FAILURE)
                {
                    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ProcessCfg:"
                                 "Ike2CfgCheckCMReqAttr Failed \n");
                    return (IKE_FAILURE);
                }
                /* If processing of CP(CFG_REQ) successfull, then set the flag
                   pSessionInfo->Ike2AuthInfo.bCMRequested to IKE_TRUE */
                pSesInfo->Ike2AuthInfo.bCMRequested = IKE_TRUE;

                break;
            }
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ProcessCfg: CFG Type not supported!!\n");
                return (IKE_FAILURE);
            }
        }
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ProcessEncrypt                                 */
/*  Description     :This function is used to procees the encrypted     */
/*                   payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo : Pointer to the Session Info DataBase    */
/*                  :                                                   */
/*  Output(s)       :Process the encrypted payload                      */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ProcessEncrypt (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad, UINT1 u1Flag)
{
    /* Dummy function will bypass the EncrPayload header + IV */
    UNUSED_PARAM (pSesInfo);
    UNUSED_PARAM (pu1PayLoad);
    UNUSED_PARAM (u1Flag);

    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2ProcessTsId                                           */
/*  Description   : This function is used to process the ID PayLoads (Traffic */
/*                : Selector ) and select the cryptomap for the peer. It      */
/*                : selects the matching Traffic Selector for the Peer.       */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo is updated with the new fsm state.           */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2ProcessTsId (tIkeSessionInfo * pSesInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkePayload        *pu1TsiPayload = NULL;
    tIkePayload        *pu1TsrPayload = NULL;
    tIke2TsPayLoad      InitTsPayload;
    tIke2TsPayLoad      RespTsPayload;
    tIkeNetwork         MyTs;
    tIkeNetwork         HisTs;
    UINT1               u1InitTsNum = IKE_ZERO;
    UINT1               u1RspTsNum = IKE_ZERO;

    /* In case if RA_VPN Server during auth Exchange, Process the
     * IDi payload first instead of TS payloads to select the Crypto map 
     * (Incase of RA-VPN Server select the RAInfo and 
     * fill the Cryptomap structure in AuthSessionInfo)*/
    if ((pSesInfo->u1ExchangeType == IKE2_AUTH_EXCH) &&
        (pSesInfo->pIkeSA->bCMEnabled == TRUE) &&
        (pSesInfo->Ike2AuthInfo.bCMServer == IKE_TRUE) &&
        (pSesInfo->pIkeSA->bCMDone != IKE_TRUE))
    {
        return (IKE_SUCCESS);
    }

    pu1TsiPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.
         aPayload[IKE2_PAYLOAD_TSI - IKE2_MINIMUM_PAYLOAD_ID]);

    pu1TsrPayload = (tIkePayload *) SLL_FIRST
        (&pSesInfo->IkeRxPktInfo.
         aPayload[IKE2_PAYLOAD_TSR - IKE2_MINIMUM_PAYLOAD_ID]);

    if ((pu1TsiPayload == NULL) || (pu1TsrPayload == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ProcessTsId: TSi or TSr payloads not found in the "
                     "list \n");
        return (IKE_FAILURE);
    }

    /* Initialize MyId and HisId */
    IKE_BZERO (&InitTsPayload, sizeof (tIke2TsPayLoad));
    IKE_BZERO (&RespTsPayload, sizeof (tIke2TsPayLoad));

    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ProcessTsId: IkeGetIkeEngineFromIndex(%d) failed\n",
                      pSesInfo->u4IkeEngineIndex);
        return (IKE_FAILURE);
    }

    /* Copy the TSi Payload  */
    IKE_MEMCPY ((UINT1 *) &InitTsPayload, pu1TsiPayload->pRawPayLoad,
                IKE2_TS_SEL_LEN);
    InitTsPayload.u2PayLoadLen = IKE_NTOHS (InitTsPayload.u2PayLoadLen);

    /* Copy the TSr Payload  */
    IKE_MEMCPY ((UINT1 *) &RespTsPayload, pu1TsrPayload->pRawPayLoad,
                IKE2_TS_SEL_LEN);
    RespTsPayload.u2PayLoadLen = IKE_NTOHS (RespTsPayload.u2PayLoadLen);

    /* For all the  Traffic Selectors combinations available, see if it matches
     * with the available configured CryptoMap */
    for (u1InitTsNum = IKE_ONE; u1InitTsNum <= InitTsPayload.u1NumTS;
         u1InitTsNum++)
    {
        /* TSi - will contain the remote protected network */
        /* TSr - will contain local protected network */
        IKE_BZERO (&HisTs, sizeof (tIkeNetwork));
        Ike2UtilGetTsInfo ((pu1TsiPayload->pRawPayLoad + IKE2_TS_SEL_LEN),
                           &HisTs);
        for (u1RspTsNum = IKE_ONE; u1RspTsNum <= RespTsPayload.u1NumTS;
             u1RspTsNum++)
        {
            IKE_BZERO (&MyTs, sizeof (tIkeNetwork));
            Ike2UtilGetTsInfo ((pu1TsrPayload->pRawPayLoad + IKE2_TS_SEL_LEN),
                               &MyTs);
            /* For initiator, already being done in Ike2AuthSessionInit */
            if (pSesInfo->u1Role == IKE_RESPONDER)
            {
                TAKE_IKE_SEM ();
                if (pSesInfo->pIkeSA == NULL)
                {
                    GIVE_IKE_SEM ();
                    continue;
                }
                pCryptoMap = IkeGetCryptoMapFromIDs (pIkeEngine, &MyTs, &HisTs,
                                                     IKE2_MAJOR_VERSION);
                if (pCryptoMap == NULL)
                {
                    GIVE_IKE_SEM ();
                    IKE_SET_ERROR (pSesInfo, IKE2_TS_UNACCEPTABLE);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ProcessTsId: Unable to get Crypto Map\n");
                    continue;
                }
                IKE_MEMCPY (&pSesInfo->Ike2IPSecPolicy, pCryptoMap,
                            sizeof (tIkeCryptoMap));

                /* Save the Inbound SPI's in the crypto map structure,
                 * so that these can be used as index to scan and delete the 
                 * required crypto map when processing the received 
                 * delete message */

                /* As only one transform is supported for each cryptomap,  
                 * the index is set to 0, if multiple transform for a crypto 
                 * is supported then a loop should be used */
                if (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
                    u1EspEncryptionAlgo != IKE_ZERO)
                {
                    pCryptoMap->u4CurrSpi = pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
                }
                else
                {
                    pCryptoMap->u4CurrSpi = pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aInSABundle[IKE_AH_SA_INDEX].u4Spi;
                }

                /* Copy the Local and Remote Networks to IPSecBundle */
                IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IPSecBundle.LocalProxy,
                            &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
                IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IPSecBundle.RemoteProxy,
                            &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));
                GIVE_IKE_SEM ();

                if (gbCMServer == TRUE)
                {
                    pSesInfo->Ike2AuthInfo.bCMServer = IKE_TRUE;
                }
                if (pSesInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_1)
                {
                    pSesInfo->pDhInfo = &gDhGroupInfo768;
                }
                else if (pSesInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_2)
                {
                    pSesInfo->pDhInfo = &gDhGroupInfo1024;
                }
                else if (pSesInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_5)
                {
                    pSesInfo->pDhInfo = &gDhGroupInfo1536;
                }
                else if (pSesInfo->Ike2IPSecPolicy.u1Pfs == SEC_DH_GROUP_14)
                {
                    pSesInfo->pDhInfo = &gDhGroupInfo2048;
                }
                return (IKE_SUCCESS);
            }
        }                        /* Tsr Loop Ends */
    }                            /* Tsi Loop Ends */

    /* The received trafffic selector doesn't match with any of the 
     * configured traffic selector.
     * Sent the IKE2_TS_UNACCEPTABLE notify and return failure */
    IKE_SET_ERROR (pSesInfo, IKE2_TS_UNACCEPTABLE);
    return (IKE_FAILURE);
}

/******************************************************************************/
/*  Function Name : Ike2GetDigestLenFromAlgo                                  */
/*  Description   : This function will return the authentication digest size  */
/*                : for the given Hash Algo                                   */
/*                : selects the matching Traffic Selector for the Peer.       */
/*  Input(s)      : u1Phase1HashAlgo - which SHA algorithm to query           */
/*                :                                                           */
/*  Output(s)     : None                                                      */
/*  Return        : Authentication digest size                                */
/******************************************************************************/

INT4
Ike2GetDigestLenFromAlgo (UINT1 u1Phase1HashAlgo)
{
    switch (u1Phase1HashAlgo)
    {
        case HMAC_SHA_256:
            return IKE2_SHA_256_DIGEST_LEN;
        case HMAC_SHA_384:
            return IKE2_SHA_384_DIGEST_LEN;
        case HMAC_SHA_512:
            return IKE2_SHA_512_DIGEST_LEN;

        default:
            return IKE2_DIGEST_LEN;
    }

}

#endif /* _IKE2_PROC_C_ */
