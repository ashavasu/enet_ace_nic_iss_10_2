/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2main.c,v 1.6 2014/04/23 12:21:39 siva Exp $
 *
 * Description: This has functions for IKEv2 Main Module
 *
 ***********************************************************************/

#ifndef _IKE2_MAIN_C_
#define _IKE2_MAIN_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikememmac.h"

PRIVATE tIkeSessionInfo *Ike2MainPostInitExchAction PROTO ((tIkeSessionInfo
                                                            * pSessionInfo));

/******************************************************************************/
/*  Function Name : Ike2MainProcessIkePkt                                     */
/*  Description   : This API is called from IkeProcessPkt to process IKEv2    */
/*                : packet that is received.                                  */
/*                :                                                           */
/*  Input(s)      : pIkeEngien - Pointer to the Ike Engine.                   */
/*                : pu1Packet - Pointer to the Recieved pkt                   */
/*                : u2PacketLen - received pkt len                            */
/*                : pPeerIpAddr - Pointer to the Peer Address                 */
/*                : u2RemotePort- Peer Port                                   */
/*                : u2LocalPort - Local Port (4500/500)                       */
/*  Output(s)     : NONE                                                      */
/*  Return        : NONE                                                      */
/******************************************************************************/
VOID
Ike2MainProcessIkePkt (tIkeEngine * pIkeEngine, UINT1 *pu1Packet,
                       UINT2 u2PacketLen, tIkeIpAddr * pPeerIpAddr,
                       UINT2 u2RemotePort, UINT2 u2LocalPort)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIsakmpHdr         *pIsakmpHdr = NULL;
    tIsakmpHdr          IkeIsakmpHdr;
    UINT4               u4DeleteTime = IKE_ZERO;
    INT4                i4Pos = IKE_ZERO;
    CHR1               *pu1PeerAddrStr = NULL;

    if (pPeerIpAddr->u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop (&pPeerIpAddr->Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr, pPeerIpAddr->Ipv4Addr);
    }

    /* Copy Isakmp Header to Isakmp Structure from packet */
    IKE_MEMCPY (&IkeIsakmpHdr, pu1Packet, sizeof (tIsakmpHdr));
    IkeIsakmpHdr.u4TotalLen = IKE_NTOHL (IkeIsakmpHdr.u4TotalLen);

    /* Get Session Info from Session Data base based on cookie */
    pSessionInfo = Ike2SessGetSessionFromCookie (pIkeEngine, pPeerIpAddr,
                                                 u2RemotePort, u2LocalPort,
                                                 &IkeIsakmpHdr);
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2MainProcessIkePkt: Ike2SessGetSessionFromCookie "
                     "failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
        return;
    }
    if (u2LocalPort == IKE_UDP_PORT)
    {
        if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2MainProcessIkePkt:"
                          " Natt is enabled received pkt on 500 from: %s\n",
                          pu1PeerAddrStr);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
            return;
        }
    }
    else
    {
        if (!
            (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT))
        {
            pSessionInfo->pIkeSA->IkeNattInfo.u2RemotePort = u2RemotePort;
            pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSessionInfo->pIkeSA->IkeNattInfo.
                u1NattFlags | IKE_USE_NATT_PORT;
            pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSessionInfo->pIkeSA->IkeNattInfo.
                u1NattFlags | IKE_PEER_SUPPORT_NATT;
        }
    }

    /* Check whether the packet recd is a retransmitted one */
    if (pSessionInfo->pu1LastRcvdPkt != NULL)
    {
        if ((u2PacketLen == pSessionInfo->u2LastRcvdPktLen) &&
            (IKE_MEMCMP (pu1Packet, pSessionInfo->pu1LastRcvdPkt,
                         u2PacketLen) == IKE_FALSE))
        {
            /* Retransmitted pkt, send the reply pkt if there is one */
            if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
            {
                IkeSendToSocketLayer (pSessionInfo);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
            return;
        }

        /* Store the last rcvd good packet in case the one
         * received now is junk or replay
         */
        pSessionInfo->pu1LastRcvdGoodPkt = pSessionInfo->pu1LastRcvdPkt;
        pSessionInfo->u2LastRcvdGoodPktLen = pSessionInfo->u2LastRcvdPktLen;
    }

    /* Copy the recd pkt into pu1LastRcvdPkt */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pu1LastRcvdPkt) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2MainProcessIkePkt: unable to allocate " "buffer\n");
        return;
    }
    if (pSessionInfo->pu1LastRcvdPkt == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2MainProcessIkePkt: Unable to allocate Memory for "
                     "Packet\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Packet);
        return;
    }

    IKE_MEMCPY (pSessionInfo->pu1LastRcvdPkt, pu1Packet, u2PacketLen);
    pSessionInfo->u2LastRcvdPktLen = u2PacketLen;

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        pSessionInfo->IkeRxPktInfo.pu1RawPkt = NULL;
    }

    /* Free the payloads of the last received pkt */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_DELETE_LIST (&(pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]),
                         IkeFreePayload);
    }

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    pSessionInfo->IkeRxPktInfo.pu1RawPkt = pu1Packet;
    pSessionInfo->IkeRxPktInfo.u4PacketLen = u2PacketLen;

    if (pSessionInfo->u1FsmState == IKE2_WAIT_ON_IPSEC)
    {
        /* We are waiting for IPSec to reply on Duplicate SPI
           request, Free the received packet and return  */
        return;
    }

    if (Ike2CoreProcess (pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2MainProcessIkePkt: Ike2CoreProcess Failed\n");
        /* Processing resulted in some error condition,
         * Need to send the response with the notify payload and then
         * Delete the IKE Session (If Established) & the session. */
        IkeHandleCoreFailure (pSessionInfo);
        return;
    }
    else
    {
        /* Ike2CoreProcess() returned IKE_SUCCESS */

        /* Processing of packet succeeded, free the last rcvd
         * good packet
         */
        if (pSessionInfo->pu1LastRcvdGoodPkt != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1LastRcvdGoodPkt);
            pSessionInfo->pu1LastRcvdGoodPkt = NULL;
            pSessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }

        if (pSessionInfo->bPhase1Done == IKE_TRUE)
        {
            if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
            {
                pIsakmpHdr =
                    ((tIsakmpHdr *) (void *) pSessionInfo->IkeTxPktInfo.
                     pu1RawPkt);
                if (pIsakmpHdr->u1Exch != IKE2_INIT_EXCH)
                {
                    Ike2ConstructEncryptPayLoad (pSessionInfo);
                }
                IkeSendToSocketLayer (pSessionInfo);
            }

            if (IkeStoreIkeSA (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2MainProcessIkePkt: IkeStoreIkeSA Failed\n");
                return;
            }

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                /* Responder, nothing to do. Delete session and return */
                Ike2SessDelInitExchSession (pSessionInfo);
                return;
            }

            pSessionInfo = Ike2MainPostInitExchAction (pSessionInfo);
            if (pSessionInfo == NULL)
            {
                /* Post Init Action was NOTIFY MSG or failure */
                return;
            }
        }                        /* Phase1Done == TRUE */

        if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
        {
            pIsakmpHdr =
                ((tIsakmpHdr *) (void *) pSessionInfo->IkeTxPktInfo.pu1RawPkt);
        }

        if ((pSessionInfo->bPhase2Done == IKE_TRUE) ||
            ((pSessionInfo->u1ExchangeType == IKE2_INFO_EXCH) &&
             (pSessionInfo->bInfoDone == IKE_TRUE)))
        {
            /* Start Timer to delete the session */
            if (pSessionInfo->u1ExchangeType == IKE2_INFO_EXCH)
            {
                u4DeleteTime = IKE_MIN_TIMER_INTERVAL;
            }
            else
            {
                u4DeleteTime = IKE_DELAYED_SESSION_DELETE_INTERVAL;
            }
            if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                               IKE_DELETE_SESSION_TIMER_ID,
                               u4DeleteTime, pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't  
                   return failure */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2MainProcessIkePkt: IkeStartTimer for Delete "
                             " session Failed\n");
            }
        }

        if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
        {
            pIsakmpHdr =
                ((tIsakmpHdr *) (void *) pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            if (pIsakmpHdr->u1Exch != IKE2_INIT_EXCH)
            {
                Ike2ConstructEncryptPayLoad (pSessionInfo);
            }
            IkeSendToSocketLayer (pSessionInfo);
        }
    }                            /* Ike2CoreProcess == IKE_SUCCESS */
    return;
}

/******************************************************************************/
/*  Function Name : Ike2MainPostInitExchAction                                */
/*  Description   : This API is called from Ike2CoreProcessIkePkt to Perform  */
/*                : Post Initial Exchange Operation.                          */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo - Pointer to the Session Structure or NULL   */
/*  Return        : Pointer to the Session Structure or NULL on error         */
/******************************************************************************/
tIkeSessionInfo    *
Ike2MainPostInitExchAction (tIkeSessionInfo * pSessionInfo)
{
    tIkeSessionInfo    *pAuthSessInfo = NULL;
    tIkeSessionInfo    *pInfoSessInfo = NULL;

    /* As Init Session is completed, set the Message Id to 1 */
    if ((pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH) &&
        (pSessionInfo->u1Role == IKE_INITIATOR))
    {
        pSessionInfo->pIkeSA->Ike2SA.u4TxMsgId = IKE_ONE;
    }

    if ((pSessionInfo->PostPhase1Info.u4ActionType ==
         IKE_POST_P1_NOTIFY)
        || (pSessionInfo->PostPhase1Info.u4ActionType == IKE_POST_P1_DELETE))
    {
        pInfoSessInfo =
            Ike2SessInfoExchSessInit (pSessionInfo->pIkeSA, IKE_ZERO,
                                      IKE_INITIATOR);
        if (pInfoSessInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2MainPostInitExchAction: Info Session "
                         "Initialisation Failed\n");
            IkeDeleteSession (pSessionInfo);
            pSessionInfo = NULL;
            return NULL;
        }

        /* Copy the Notify Information related to the SPI, that should 
         * be delted etc.*/
        MEMCPY (&(pInfoSessInfo->PostPhase1Info.IkeNotifyInfo),
                &(pSessionInfo->PostPhase1Info.IkeNotifyInfo),
                sizeof (tIkeNotifyInfo));

        /* delete the current session */
        Ike2SessDelInitExchSession (pSessionInfo);
        pSessionInfo = NULL;

        if (Ike2CoreProcess (pInfoSessInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2MainPostInitExchAction: Ike2CoreProcess "
                         " failed!\n");
            if (pInfoSessInfo != NULL)
            {
                if (IkeDeleteSession (pInfoSessInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2MainPostInitExchAction: IkeDeleteSession "
                                 "Failed\n");
                }
            }
            return NULL;
        }
        return (pInfoSessInfo);
    }
    else if (pSessionInfo->PostPhase1Info.u4ActionType == IKE_POST_P1_NEW_SA)
    {
        pAuthSessInfo =
            Ike2SessAuthExchSessInit (pSessionInfo->pIkeSA,
                                      &pSessionInfo->PostPhase1Info.
                                      IPSecRequest, IKE2_AUTH_MSG_ID,
                                      IKE_INITIATOR);
        if (pAuthSessInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2MainPostInitExchAction: AUTH Session "
                         "Initialisation Failed\n");
            IkeDeleteSession (pSessionInfo);
            pSessionInfo = NULL;
            return NULL;
        }

        /*This is used to populate the required certificate info
           in INITIATOR Case i.e., Required info is copied from 
           INIT Session to AUTH Session */

        if (pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA)
        {
            SLL_INIT (&(pAuthSessInfo->PeerCertReq));
            IKE_MEMCPY (&(pAuthSessInfo->PeerCertReq),
                        &(pSessionInfo->PeerCertReq), sizeof (t_SLL));
            IKE_MEMCPY (&(pAuthSessInfo->MyCert), &(pSessionInfo->MyCert),
                        sizeof (tIkeCertInfo));
            pAuthSessInfo->bDefaultMyCert = pSessionInfo->bDefaultMyCert;
            IKE_MEMCPY (&(pAuthSessInfo->MyCertKey), &(pSessionInfo->MyCertKey),
                        sizeof (tIkeRsaKeyInfo));
        }
        else if (pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA)
        {
            SLL_INIT (&(pAuthSessInfo->PeerCertReq));
            IKE_MEMCPY (&(pAuthSessInfo->PeerCertReq),
                        &(pSessionInfo->PeerCertReq), sizeof (t_SLL));
            IKE_MEMCPY (&(pAuthSessInfo->MyCert), &(pSessionInfo->MyCert),
                        sizeof (tIkeCertInfo));
            pAuthSessInfo->bDefaultMyCert = pSessionInfo->bDefaultMyCert;
            IKE_MEMCPY (&(pAuthSessInfo->MyCertKey), &(pSessionInfo->MyCertKey),
                        sizeof (tIkeDsaKeyInfo));
        }

        /* delete the current session */
        Ike2SessDelInitExchSession (pSessionInfo);
        pSessionInfo = NULL;

        if (pAuthSessInfo->u1FsmState == IKE2_WAIT_ON_IPSEC)
        {
            /* Ike2AuthExchSessInit would have sent a message to
               IPSec to check for duplicate SPI, will have to
               wait for the reply before starting the AUTH
               negotiation */
            return NULL;
        }

        if (Ike2CoreProcess (pAuthSessInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2MainPostInitExchAction: Ike2CoreProcess "
                         " failed!\n");
            if (pAuthSessInfo != NULL)
            {
                if (IkeDeleteSession (pAuthSessInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2MainPostInitExchAction: IkeDeleteSession "
                                 "Failed\n");
                }
            }
            return NULL;
        }

        /* Packet will be sent below, no need to duplicate code */
        return (pAuthSessInfo);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2MainPostInitExchAction: Deleting IKE session as "
                     "no post Init exchange action is specified \n");
        Ike2SessDelInitExchSession (pSessionInfo);
        pSessionInfo = NULL;
        return NULL;
    }
}
#endif
