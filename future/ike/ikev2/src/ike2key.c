/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2key.c,v 1.8 2014/12/15 12:14:16 siva Exp $
 *
 * Description: This has functions for IKEv2 session management
 *
 ***********************************************************************/
#ifndef _IKE2_KEY_C_
#define _IKE2_KEY_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikememmac.h"

PRIVATE INT1 Ike2KeySetIkeSAEncrKey PROTO ((tIkeSessionInfo * pSessionInfo,
                                            UINT1 *pu1KeyMat, UINT1 u1Role));

PRIVATE INT4        Ike2KeyDeriveKeysFromSkeyId
PROTO ((tIkeSessionInfo * pSessionInfo));

PRIVATE INT1 Ike2KeyGenIPSecKeyMat PROTO ((tIkeSessionInfo * pSessionInfo,
                                           tIPSecSA * pIPSecInSA,
                                           tIPSecSA * pIPSecOutSA));

PRIVATE INT1 Ike2KeyGenSkeyIdNew PROTO ((tIkeSessionInfo * pSessionInfo));

PRIVATE INT1 Ike2KeyGenSkeyIdReKey PROTO ((tIkeSessionInfo * pSessionInfo));

/**************************************************************************/
/*  Function Name : Ike2KeyGenSkeyId                                      */
/*  Description   : Function to generate key for authentication,          */
/*                  encryption and integrity                              */
/*  Input(s)      : pSessionInfo - Pointer to session info.               */
/*  Output(s)     : Update the session structure with new keys            */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                            */
/**************************************************************************/
INT1
Ike2KeyGenSkeyId (tIkeSessionInfo * pSessionInfo)
{
    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        if (Ike2KeyGenSkeyIdNew (pSessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenSkeyId: Ike2KeyGenSkeyIdNew failed \n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (Ike2KeyGenSkeyIdReKey (pSessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenSkeyId: Ike2KeyGenSkeyIdReKey failed \n");
            return (IKE_FAILURE);
        }
    }

    if (Ike2KeyDeriveKeysFromSkeyId (pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyId: Ike2KeyDeriveKeysFromSkeyId "
                     "failed \n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Ike2KeyGenSkeyIdNew                                */
/*  Description   : Function to generate key for authentication,       */
/*                  encryption and integrity (New IKESA)               */
/*  Input(s)      : pSessionInfo - Pointer to session info.            */
/*  Output(s)     : Update the session structure with new keys         */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                         */
/***********************************************************************/
INT1
Ike2KeyGenSkeyIdNew (tIkeSessionInfo * pSessionInfo)
{
    UINT4               u4KeyLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4HashLen = IKE_ZERO;

    /* Generation of SKEYID */
    /* SKEYSEED = prf (Ni | Nr, g^ir) 
       key is the concatenated value of initiator and responder Nonces 
       and message is the diffe-helman shared key */

    /* Get the Hash Algorithm key length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Total Key Length */
    u4KeyLen = (UINT2) (pSessionInfo->pIkeSA->Ike2SA.u2InitNonceLen +
                        pSessionInfo->pIkeSA->Ike2SA.u2RespNonceLen);

    if ((pSessionInfo->pIkeSA->Ike2SA.u2InitNonceLen == IKE_ZERO) ||
        (pSessionInfo->pIkeSA->Ike2SA.u2RespNonceLen == IKE_ZERO))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdNew: Nonce Length is wrong \n");
        return (IKE_FAILURE);
    }

    /* This buffer is used for calculating SKEYID */
    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdNew: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdNew: Malloc Failed for key material \n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pu1Ptr, u4KeyLen);

    /* Allocate buffer for storing SKEYID */
    pSessionInfo->pIkeSA->u2SKeyIdLen = (UINT2) u4HashLen;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeSA->pSKeyId) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdNew: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return IKE_FAILURE;
    }
    if (pSessionInfo->pIkeSA->pSKeyId == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdNew: Malloc Failed for SkeyId \n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return (IKE_FAILURE);
    }

    IKE_MEMCPY (pu1Ptr, pSessionInfo->pIkeSA->Ike2SA.pu1InitNonce,
                pSessionInfo->pIkeSA->Ike2SA.u2InitNonceLen);
    IKE_MEMCPY ((pu1Ptr + pSessionInfo->pIkeSA->Ike2SA.u2InitNonceLen),
                pSessionInfo->pIkeSA->Ike2SA.pu1RespNonce,
                pSessionInfo->pIkeSA->Ike2SA.u2RespNonceLen);

    /* Generation of SKEYID Rekey case */
    /* SKEYSEED = prf(SK_d (old), [g^ir (new)] | Ni | Nr)
       Use OLD SAs PRF functions for both prf_plus and prf */

    /* SKEYSEED = prf (Ni | Nr, g^ir) */
    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
     HmacAlgo) (pu1Ptr, (INT4) u4KeyLen,
                pSessionInfo->pDhSharedSecret->pu1Value,
                pSessionInfo->pDhSharedSecret->i4Length,
                pSessionInfo->pIkeSA->pSKeyId);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
    pu1Ptr = NULL;

    return (IKE_SUCCESS);
}

/**************************************************************************/
/*  Function Name : Ike2KeyGenSkeyIdReKey                                 */
/*  Description   : Function to generate key for authentication,          */
/*                  encryption and integrity (Rekey IKESA)                */
/*  Input(s)      : pSessionInfo - Pointer to session info.               */
/*  Output(s)     : Update the session structure with new keys            */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                            */
/**************************************************************************/
INT1
Ike2KeyGenSkeyIdReKey (tIkeSessionInfo * pSessionInfo)
{
    UINT4               u4MsgLen = IKE_ZERO;
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1TempPtr = NULL;
    UINT4               u4HashLen = IKE_ZERO;

    /* Generation of SKEYID Rekey case */
    /* SKEYSEED = prf(SK_d (old), [g^ir (new)] | Ni(new) | Nr (new))
       Use OLD SAs PRF functions for both prf_plus and prf */
    /* Key is SK_d (old) and message is the concatenated value of new
       diffe-helman shared key,initiator and responder Nonces */

    /* Get the Hash Algorithm key length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    if ((pSessionInfo->pIkeReKeySA->Ike2SA.u2InitNonceLen == IKE_ZERO) ||
        (pSessionInfo->pIkeReKeySA->Ike2SA.u2RespNonceLen == IKE_ZERO))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdReKey: Invalid Nonce Length \n");
        return (IKE_FAILURE);
    }

    /* Total Key Length */
    u4MsgLen = (UINT2) (pSessionInfo->pDhSharedSecret->i4Length +
                        pSessionInfo->pIkeReKeySA->Ike2SA.u2InitNonceLen +
                        pSessionInfo->pIkeReKeySA->Ike2SA.u2RespNonceLen);

    /* This buffer is used for calculating SKEYID */
    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Ptr)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdReKey: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Ptr == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdReKey: Malloc Failed for key "
                     " material \n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pu1Ptr, u4MsgLen);

    /* Allocate buffer for storing SKEYID */
    pSessionInfo->pIkeReKeySA->u2SKeyIdLen = (UINT2) u4HashLen;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeReKeySA->pSKeyId) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdReKey: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return IKE_FAILURE;
    }
    if (pSessionInfo->pIkeReKeySA->pSKeyId == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenSkeyIdReKey: Malloc failed for SkeyId \n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
        return (IKE_FAILURE);
    }
    pu1TempPtr = pu1Ptr;

    if (pSessionInfo->pDhSharedSecret->i4Length != IKE_ZERO)
    {
        IKE_MEMCPY (pu1TempPtr, pSessionInfo->pDhSharedSecret->pu1Value,
                    pSessionInfo->pDhSharedSecret->i4Length);
        pu1TempPtr = pu1TempPtr + pSessionInfo->pDhSharedSecret->i4Length;
    }
    IKE_MEMCPY (pu1TempPtr, pSessionInfo->pIkeReKeySA->Ike2SA.pu1InitNonce,
                pSessionInfo->pIkeReKeySA->Ike2SA.u2InitNonceLen);
    pu1TempPtr = pu1TempPtr + pSessionInfo->pIkeReKeySA->Ike2SA.u2InitNonceLen;

    IKE_MEMCPY (pu1TempPtr, pSessionInfo->pIkeReKeySA->Ike2SA.pu1RespNonce,
                pSessionInfo->pIkeReKeySA->Ike2SA.u2RespNonceLen);

    /* Generation of SKEYID Rekey case */
    /* SKEYSEED = prf(SK_d (old), [g^ir (new)] | Ni | Nr) */
    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
     HmacAlgo) (pSessionInfo->pIkeSA->pSKeyIdD, (INT4) u4HashLen,
                pu1Ptr, (INT4) u4MsgLen, pSessionInfo->pIkeReKeySA->pSKeyId);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Ptr);
    pu1Ptr = NULL;

    return (IKE_SUCCESS);
}

/***********************************************************************/
/*  Function Name   :Ike2KeyDeriveKeysFromSkeyId                       */
/*  Description     :This function derives skeyid_d, skeyid_ai,        */
/*                   skeyid_ar,skeyid_ei, skeyid_er, skeyid_pi,        */
/*                   skeyid_pr                                         */
/*                   from skeyid                                       */
/*  Input(s)        :pSessionInfo - Pointer to the session structure   */
/*  Output(s)       :Stores skeyid_d, skeyid_ai,skeyid_ar,skeyid_ei,   */
/*                   skeyid_er, skeyid_pi, skeyid_pr in the session    */
/*                   structure                                         */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                        */
/***********************************************************************/
INT4
Ike2KeyDeriveKeysFromSkeyId (tIkeSessionInfo * pSessionInfo)
{
    UINT1              *pu1TempD = NULL;
    UINT1              *pu1Skeyd = NULL;
    UINT1              *pu1BufPtr = NULL;
    UINT4               u4KeyMatTotLen = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4DataLen = IKE_ZERO;
    UINT4               u4EncrLen = IKE_ZERO;
    UINT4               u4Count = IKE_ZERO;
    UINT1              *pu1KeyMat = NULL;
    UINT1              *pu1TempKeyMat = NULL;
    tIkeSA             *pIkeSA = NULL;

    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else
    {
        pIkeSA = pSessionInfo->pIkeReKeySA;
    }

    /* Get the Hash Algorithm key length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Get the Encryption Algorithm key length */
    if (pSessionInfo->pIkeSA->u1Phase1EncrAlgo != IKE2_ENCR_AES_CBC)
    {
        u4EncrLen =
            gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
            u4KeyLen;
    }
    else
    {
        u4EncrLen = IKE_BITS2BYTES (pSessionInfo->Ike2InitInfo.u2NegKeyLen);
    }

    /* Derive 7 keys as follows from SKEYSEED.
       {SK_d | SK_ai | SK_ar | SK_ei | SK_er | SK_pi | SK_pr} = 
       prf+ (SKEYSEED, Ni | Nr | SPIi | SPIr ) */

    /* For deriving the keys the maximum length required is 
       u4HashLen + Initiator Nonce length + Responder Nonce Len + 
       Initiator SPI length + Responder SPI length + 1 */

    u4DataLen = (u4HashLen + pIkeSA->Ike2SA.u2InitNonceLen +
                 pIkeSA->Ike2SA.u2RespNonceLen +
                 (IKE_TWO * IKE_COOKIE_LEN) + IKE_ONE);

    /* This buffer is used for calculating the skeyid_d, skeyid_ai,
       skeyid_ar,skeyid_ei, skeyid_er, skeyid_pi, skeyid_pr */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1BufPtr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    /* Allocate memory for keys */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeSA->pSKeyIdD) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: unable to allocate "
                     "buffer\n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeSA->pSKeyIdA) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: unable to allocate "
                     "buffer\n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyId, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdD, u4HashLen);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeSA->pSKeyIdE) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: unable to allocate "
                     "buffer\n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyId, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdD, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdA, u4HashLen);
        return IKE_FAILURE;
    }

    /* Calculate the total key length.Since each prf iteration will 
     * generate a digest of 16, make it the multiple of Hash length  */
    /* 5 keys of size u4HashLen (pSkeyID, pSkeyIdAi, au1SkeyIdAr, 
     *                           au1SkeyIdPi, au1SkeyIdPr)
     * 2 encr keys of size u4EncrLen (pSkeyidEi, au1SkeyIdEr) */
    u4KeyMatTotLen = ((IKE2_NUM_OF_HASH_KEYS * u4HashLen) +
                      (IKE2_NUM_OF_ENCR_KEYS * u4EncrLen));

    /* Make u4KeyMatTotLen multiple of u4HashLen */
    u4KeyMatTotLen = (((u4KeyMatTotLen - IKE_ONE) / u4HashLen) + IKE_ONE) *
        u4HashLen;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1KeyMat) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: unable to allocate "
                     "buffer\n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyId, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdD, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdA, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdE, u4EncrLen);
        return IKE_FAILURE;
    }

    if ((pIkeSA->pSKeyIdD == NULL) || (pIkeSA->pSKeyIdA == NULL) ||
        (pIkeSA->pSKeyIdE == NULL) || (pu1BufPtr == NULL) ||
        (pu1KeyMat == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: Malloc Fails for keys\n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pu1KeyMat, u4KeyMatTotLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyId, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdD, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdA, u4HashLen);
        Ike2UtilReleaseBuf (&pIkeSA->pSKeyIdE, u4EncrLen);
        return (IKE_FAILURE);
    }

    IKE_BZERO (pu1BufPtr, u4DataLen);
    IKE_BZERO (pu1KeyMat, u4KeyMatTotLen);
    pu1TempKeyMat = pu1KeyMat;

    /* Point to the Buffer without auth digest size as it is not required for
     * calculating SKEYID_d*/
    pu1TempD = pu1BufPtr + u4HashLen;
    pu1Skeyd = pu1BufPtr + u4HashLen;

    /* {SK_d | SK_ai | SK_ar | SK_ei | SK_er | SK_pi | SK_pr} = 
       prf+ (SKEYSEED, Ni | Nr | SPIi | SPIr )
       Key is SKEYID and message is the concatenation of 
       Initiator Nonce + Responder Nonce + Initiator SPI + Responder SPI + 0
       Copy the initiator and responder Nonce */

    IKE_MEMCPY (pu1TempD, pIkeSA->Ike2SA.pu1InitNonce,
                pIkeSA->Ike2SA.u2InitNonceLen);
    IKE_MEMCPY ((pu1TempD + pIkeSA->Ike2SA.u2InitNonceLen),
                pIkeSA->Ike2SA.pu1RespNonce, pIkeSA->Ike2SA.u2RespNonceLen);
    pu1TempD = pu1TempD + (pIkeSA->Ike2SA.u2InitNonceLen +
                           pIkeSA->Ike2SA.u2RespNonceLen);

    /* Copy the Initiator and Responder cookie */
    IKE_MEMCPY (pu1TempD, pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    IKE_MEMCPY (pu1TempD + IKE_COOKIE_LEN,
                pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    pu1TempD += (IKE_TWO * IKE_COOKIE_LEN);

    /* For first iteration 0 has to be appended at the end */
    pu1TempD[IKE_INDEX_0] = IKE_ONE;

    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
     HmacAlgo) (pIkeSA->pSKeyId, (INT4) pIkeSA->u2SKeyIdLen, pu1Skeyd,
                (INT4) (u4DataLen - u4HashLen), pu1TempKeyMat);

    for (u4Count = IKE_ONE; u4Count < (u4KeyMatTotLen / u4HashLen); u4Count++)
    {
        /* Key is SKEYID and message is the concatenation of 
           pu1BufPtr +  Initiator Nonce + Responder Nonce + Initiator SPI + 
           Responder SPI + (u4Count + 1) */

        pu1TempD[IKE_INDEX_0] = ((UINT1) (u4Count + IKE_ONE));
        IKE_MEMCPY (pu1BufPtr, pu1KeyMat, u4HashLen);
        pu1KeyMat += u4HashLen;

        (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].
         HmacAlgo) (pIkeSA->pSKeyId, (INT4) pIkeSA->u2SKeyIdLen, pu1BufPtr,
                    (INT4) u4DataLen, pu1KeyMat);
    }

    pu1Skeyd = pu1TempKeyMat;
    /* Generation of SKeyIdD */
    IKE_MEMCPY (pIkeSA->pSKeyIdD, pu1Skeyd, u4HashLen);
    pu1Skeyd += u4HashLen;

    /* Generation of SKeyIdAi */
    IKE_MEMCPY (pIkeSA->pSKeyIdA, pu1Skeyd, u4HashLen);
    pu1Skeyd += u4HashLen;

    /* Generation of SKeyIdAr */
    IKE_MEMCPY (pIkeSA->Ike2SA.au1SKeyIdAr, pu1Skeyd, u4HashLen);
    pu1Skeyd += u4HashLen;

    /* Generation of SKeyIdEi */
    IKE_MEMCPY (pIkeSA->pSKeyIdE, pu1Skeyd, u4EncrLen);
    pu1Skeyd += u4EncrLen;

    /* Generation of SKeyIdEr */
    IKE_MEMCPY (pIkeSA->Ike2SA.au1SKeyIdEr, pu1Skeyd, u4EncrLen);
    pu1Skeyd += u4EncrLen;

    /* Generation of SKeyIdPi */
    IKE_MEMCPY (pIkeSA->Ike2SA.au1SKeyIdPi, pu1Skeyd, u4HashLen);
    pu1Skeyd += u4HashLen;

    /* Generation of SKeyIdPr */
    IKE_MEMCPY (pIkeSA->Ike2SA.au1SKeyIdPr, pu1Skeyd, u4HashLen);

    if (Ike2KeySetIkeSAEncrKey (pSessionInfo, pIkeSA->pSKeyIdE,
                                IKE_INITIATOR) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: Ike2KeySetIkeSAEncrKey for"
                     " Initiator failed! \n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pu1TempKeyMat, u4KeyMatTotLen);
        return (IKE_FAILURE);
    }

    if (Ike2KeySetIkeSAEncrKey (pSessionInfo, pIkeSA->Ike2SA.au1SKeyIdEr,
                                IKE_RESPONDER) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: Ike2KeySetIkeSAEncrKey "
                     "for Responder failed! \n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pu1TempKeyMat, u4KeyMatTotLen);
        return (IKE_FAILURE);
    }

    if (Ike2UtilGenIV (pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyDeriveKeysFromSkeyId: Ike2UtilGenIV failed! \n");
        Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
        Ike2UtilReleaseBuf (&pu1TempKeyMat, u4KeyMatTotLen);
        return (IKE_FAILURE);
    }

    Ike2UtilReleaseBuf (&pu1BufPtr, u4DataLen);
    Ike2UtilReleaseBuf (&pu1TempKeyMat, u4KeyMatTotLen);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2KeySetIkeSAEncrKey                             */
/*  Description     :This function is used to set the DES/3DES keys     */
/*                  :using the Key Material                             */
/*  Input(s)        :pSessionInfo - Pointer to the SessionInfo          */
/*                  :structure                                          */
/*                  :pu1KeyMat - The Key Material                       */
/*                  :u1Role    - INITIATOR /RESPONDER                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/
INT1
Ike2KeySetIkeSAEncrKey (tIkeSessionInfo * pSessionInfo, UINT1 *pu1KeyMat,
                        UINT1 u1Role)
{
    tIkeSA             *pIkeSA = NULL;

    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else
    {
        pIkeSA = pSessionInfo->pIkeReKeySA;
    }

    switch (pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE2_ENCR_DES:
        {
            pIkeSA->u2EncrKLen =
                (UINT2) IKE_BITS2BYTES (pSessionInfo->Ike2IkePolicy.u2KeyLen);
            if (IkeDesIsWeakKey (pu1KeyMat) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2KeySetIkeSAEncrKey: Weak key generated\n");
                return (IKE_FAILURE);
            }
            if (u1Role == IKE_INITIATOR)
            {
                if (IkeDesSetKey (pu1KeyMat, &pIkeSA->DesKey1) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2KeySetIkeSAEncrKey: "
                                 "IkeDesSetKey failed\n");
                    return (IKE_FAILURE);
                }
            }
            else if (u1Role == IKE_RESPONDER)
            {
                if (IkeDesSetKey (pu1KeyMat, &pIkeSA->Ike2SA.DesKeyRes1) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2KeySetIkeSAEncrKey: "
                                 "IkeDesSetKey failed\n");
                    return (IKE_FAILURE);
                }
            }
            break;
        }

        case IKE2_ENCR_3DES:
        {
            pIkeSA->u2EncrKLen =
                (UINT2) IKE_BITS2BYTES (pSessionInfo->Ike2IkePolicy.u2KeyLen);

            if (IkeDesIsWeakKey (pu1KeyMat) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2KeySetIkeSAEncrKey: Weak key generated\n");
                return (IKE_FAILURE);
            }

            if (IkeDesIsWeakKey (pu1KeyMat + IKE_DES_KEY_LEN) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2KeySetIkeSAEncrKey: Weak key generated\n");
                return (IKE_FAILURE);
            }
            if (IkeDesIsWeakKey (pu1KeyMat + (IKE_TWO * IKE_DES_KEY_LEN)) ==
                IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2KeySetIkeSAEncrKey: Weak key generated\n");
                return (IKE_FAILURE);
            }
            if (u1Role == IKE_INITIATOR)
            {
                if (IkeTripleDesSetKey (pu1KeyMat, &pIkeSA->DesKey1,
                                        &pIkeSA->DesKey2, &pIkeSA->DesKey3) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2KeySetIkeSAEncrKey: "
                                 "IkeTripleDesSetKey failed\n");
                    return (IKE_FAILURE);
                }
            }
            else if (u1Role == IKE_RESPONDER)
            {
                if (IkeTripleDesSetKey (pu1KeyMat,
                                        &pIkeSA->Ike2SA.DesKeyRes1,
                                        &pIkeSA->Ike2SA.DesKeyRes2,
                                        &pIkeSA->Ike2SA.DesKeyRes3) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2KeySetIkeSAEncrKey: "
                                 "IkeTripleDesSetKey failed\n");
                    return (IKE_FAILURE);
                }
            }
            break;
        }
        case IKE2_ENCR_AES_CBC:
        {
            /* AES KEY LENGTH */
            pIkeSA->u2EncrKLen =
                (UINT2) IKE_BITS2BYTES (pSessionInfo->Ike2IkePolicy.u2KeyLen);

            /* For Initiator -> Generate EncrCtx from SKeyIdEi 
             *                  and Decr DecrCtx from SKeyIdEr 
             * For Responder -> Generate EncrCtx from SKeyIdEr 
             *                  and Decr DecrCtx from SKeyIdEi */
            if (u1Role == IKE_INITIATOR)
            {
                if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
                {
                    if (IkeAesSetEncrKey (pu1KeyMat, (UINT1) pIkeSA->u2EncrKLen,
                                          pIkeSA->au1AesEncrKey) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2KeySetIkeSAEncrKey: "
                                     "IkeAesSetEncrKey failed\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    if (IkeAesSetDecrKey (pu1KeyMat, (UINT1) pIkeSA->u2EncrKLen,
                                          pIkeSA->au1AesDecrKey) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2KeySetIkeSAEncrKey: "
                                     "IkeAesSetDecrKey failed\n");
                        return (IKE_FAILURE);
                    }
                }
            }
            else if (u1Role == IKE_RESPONDER)
            {
                if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
                {
                    if (IkeAesSetDecrKey (pu1KeyMat, (UINT1) pIkeSA->u2EncrKLen,
                                          pIkeSA->au1AesDecrKey) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2KeySetIkeSAEncrKey: "
                                     "IkeAesSetDecrKey failed\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    if (IkeAesSetEncrKey (pu1KeyMat, (UINT1) pIkeSA->u2EncrKLen,
                                          pIkeSA->au1AesEncrKey) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2KeySetIkeSAEncrKey: "
                                     "IkeAesSetEncrKey failed\n");
                        return (IKE_FAILURE);
                    }
                }
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeySetIkeSAEncrKey: "
                         "IkeAesSetEncrKey failed\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/********************************************************************/
/*   Function Name : Ike2KeyGenIPSecKeys                            */
/*   Description   : Function to generate key Material for IPSec    */
/*   Input(s)      : pSessionInfo - Pointer to session info.        */
/*   Output(s)     : None.                                          */
/*   Return Values : IKE_SUCCESS or IKE_FAILURE                     */
/********************************************************************/
INT1
Ike2KeyGenIPSecKeys (tIkeSessionInfo * pSessionInfo)
{
    tIPSecSA           *pIPSecInSA = NULL;
    tIPSecSA           *pIPSecOutSA = NULL;

    /*Generate Key for Inbound and OutBound SA for ESP */
    pIPSecInSA = &pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aInSABundle[IKE_ESP_SA_INDEX];
    pIPSecOutSA = &pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aOutSABundle[IKE_ESP_SA_INDEX];

    if ((pIPSecInSA->u1IPSecProtocol != IKE_ZERO) &&
        (pIPSecOutSA->u1IPSecProtocol != IKE_ZERO))
    {
        if (Ike2KeyGenIPSecKeyMat (pSessionInfo, pIPSecInSA, pIPSecOutSA) ==
            IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenIPSecKeys: Generate Key for Inbound and OutBound SA"
                         "for ESP protocol failed\n");
            return (IKE_FAILURE);
        }
    }

    /*Generate Key for Inbound and OutBound SA for AH */
    pIPSecInSA = &pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aInSABundle[IKE_AH_SA_INDEX];
    pIPSecOutSA = &pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aOutSABundle[IKE_AH_SA_INDEX];

    if ((pIPSecInSA->u1IPSecProtocol != IKE_ZERO) &&
        (pIPSecOutSA->u1IPSecProtocol != IKE_ZERO))
    {
        if (Ike2KeyGenIPSecKeyMat (pSessionInfo, pIPSecInSA, pIPSecOutSA) ==
            IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenIPSecKeys: Generate Key for Inbound and OutBound SA"
                         "for AH protocol failed\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/********************************************************************/
/*  Function Name : Ike2KeyGenIPSecKeyMat                           */
/*  Description   : Function to generate key Material for IPSec     */
/*  Input(s)      : pSessionInfo - Pointer to session info.         */
/*                : pIPSecInSA - Inbound IPSecSA from which we      */
/*                : get the info for Key gen and where we store the */
/*                  key                                             */
/*                : pIPSecOutSA - Outbound IPSecSA from which we    */
/*                : get the info for Key gen and where we store the */
/*                  key                                             */
/*  Output(s)     : None.                                           */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                      */
/********************************************************************/
INT1
Ike2KeyGenIPSecKeyMat (tIkeSessionInfo * pSessionInfo, tIPSecSA * pIPSecInSA,
                       tIPSecSA * pIPSecOutSA)
{
    UINT2               u2DataLen = IKE_ZERO;
    UINT1              *pu1Seed = NULL;
    UINT1              *pu1TempBuf = NULL;
    UINT1              *pu1TempKey = NULL;
    UINT1              *pu1TempSeed = NULL;
    UINT4               u4KeyLen = IKE_ZERO;
    UINT4               u4TotalKeyLen = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT1              *pu1Key = NULL;
    UINT1               u1HashAlgo = IKE_ZERO;

    if (pIPSecInSA->u1IPSecProtocol == IKE_IPSEC_AH)
    {
        u1HashAlgo = pIPSecInSA->u1HashAlgo;
        pIPSecInSA->u2AuthKeyLen =
            (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
        pIPSecOutSA->u2AuthKeyLen =
            (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
        pIPSecInSA->u2EncrKeyLen = IKE_ZERO;
        pIPSecOutSA->u2EncrKeyLen = IKE_ZERO;
    }
    else if (pIPSecInSA->u1IPSecProtocol == IKE_IPSEC_ESP)
    {
        if (pIPSecInSA->u1HashAlgo != IKE_ZERO)
        {
            u1HashAlgo = gau1ESPToIKEHashAlgo[pIPSecInSA->u1HashAlgo];
            pIPSecInSA->u2AuthKeyLen =
                (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
            pIPSecOutSA->u2AuthKeyLen =
                (UINT2) gaIkeHmacAlgoList[u1HashAlgo].u4KeyLen;
        }
        else
        {
            /* Hash algo is not configured and ESP is configured NULL
             * no need to generate keys */
            if (pIPSecInSA->u1EncrAlgo == IKE_IPSEC_ESP_NULL)
            {
                return (IKE_SUCCESS);
            }
        }

        if ((pIPSecInSA->u1EncrAlgo == IKE_IPSEC_ESP_AES)
            || (pIPSecInSA->u1EncrAlgo == IKE_IPSEC_ESP_AES_CTR))
        {
            if (pSessionInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]
                != NULL)
            {
                pIPSecInSA->u2EncrKeyLen =
                    (UINT2) IKE_BITS2BYTES (pSessionInfo->Ike2IPSecPolicy.
                                            aTransformSetBundle[IKE_INDEX_0]->
                                            u2KeyLen);
                pIPSecOutSA->u2EncrKeyLen =
                    (UINT2) IKE_BITS2BYTES (pSessionInfo->Ike2IPSecPolicy.
                                            aTransformSetBundle[IKE_INDEX_0]->
                                            u2KeyLen);
            }
        }
        else
        {
            pIPSecInSA->u2EncrKeyLen =
                (UINT2) gaIke2CipherAlgoList[pIPSecInSA->u1EncrAlgo].u4KeyLen;
            pIPSecOutSA->u2EncrKeyLen =
                (UINT2) gaIke2CipherAlgoList[pIPSecInSA->u1EncrAlgo].u4KeyLen;
        }
    }
    else
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2KeyGenIPSecKeyMat: %d Protocol not supported \n",
                      pIPSecInSA->u1IPSecProtocol);
        return (IKE_FAILURE);
    }
    if ((pIPSecInSA->u1EncrAlgo != IKE_IPSEC_ESP_AES_CTR))
    {
        /* Total keylength = Encryption key length + Auth Key length */

        /* Total keylength = Encryption key length + Auth Key length */
        u4TotalKeyLen = (UINT4) (IKE_TWO * (pIPSecInSA->u2AuthKeyLen +
                                            pIPSecInSA->u2EncrKeyLen));
    }
    else
    {
        u4TotalKeyLen = (UINT4) (IKE_TWO * (pIPSecInSA->u2AuthKeyLen +
                                            pIPSecInSA->u2EncrKeyLen +
                                            IKE_NONCE_LEN));
    }
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Make Total Key Len a multiple of Hash Length */
    u4TotalKeyLen = (((u4TotalKeyLen - IKE_ONE) / u4HashLen) + IKE_ONE) *
        u4HashLen;

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Key)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenIPSecKeyMat: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pu1Key == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenIPSecKeyMat: "
                     "Malloc Failed for Key Material\n");
        pIPSecInSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecInSA->u2EncrKeyLen = IKE_ZERO;
        pIPSecOutSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecOutSA->u2EncrKeyLen = IKE_ZERO;
        return (IKE_FAILURE);
    }
    IKE_BZERO (pu1Key, u4TotalKeyLen);

    u4KeyLen = u4TotalKeyLen;

    /* Keying material for child SA is generated as follows:
       KEYMAT = prf+(SK_d, Ni | Nr) */

    /* For CREATE_CHILD_SA exchanges including an optional Diffie-Hellman
       exchange, the keying material is defined as:
       KEYMAT = prf+(SK_d, g^ir (new) | Ni | Nr ) */

    u2DataLen = (UINT2) (pSessionInfo->u2InitiatorNonceLen +
                         pSessionInfo->u2ResponderNonceLen);
    /* If PFS Enabled, allocate for Shared Secret also */
    if ((pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH) &&
        (pSessionInfo->Ike2IPSecPolicy.u1Pfs != IKE_ZERO))
    {
        if (pSessionInfo->pDhSharedSecret == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenIPSecKeyMat: PFS Enabled and Shared "
                         "Secret is NULL!\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
            return (IKE_FAILURE);
        }
        u2DataLen =
            (UINT2) (u2DataLen +
                     (UINT2) pSessionInfo->pDhSharedSecret->i4Length);
    }
    u2DataLen = (UINT2) (u2DataLen + IKE_ONE);

    /* Adding u4HashLen to allow concatenation */
    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Seed)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenIPSecKeyMat: unable to allocate " "buffer\n");
        pIPSecInSA->u2AuthKeyLen = 0;
        pIPSecInSA->u2EncrKeyLen = 0;
        pIPSecOutSA->u2AuthKeyLen = 0;
        pIPSecOutSA->u2EncrKeyLen = 0;
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
        return IKE_FAILURE;
    }
    if (pu1Seed == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2KeyGenIPSecKeyMat: Malloc Failed for SEED \n");
        pIPSecInSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecInSA->u2EncrKeyLen = IKE_ZERO;
        pIPSecOutSA->u2AuthKeyLen = IKE_ZERO;
        pIPSecOutSA->u2EncrKeyLen = IKE_ZERO;
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
        return (IKE_FAILURE);
    }
    pu1TempSeed = (pu1Seed + u4HashLen);

    /* Copy g^ir if pfs is enabled */
    if ((pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH) &&
        (pSessionInfo->Ike2IPSecPolicy.u1Pfs != IKE_ZERO) &&
        (pSessionInfo->pDhSharedSecret->i4Length != IKE_ZERO))
    {
        IKE_MEMCPY (pu1TempSeed, pSessionInfo->pDhSharedSecret->pu1Value,
                    pSessionInfo->pDhSharedSecret->i4Length);
        pu1TempSeed = pu1TempSeed + pSessionInfo->pDhSharedSecret->i4Length;
    }

    /* Copy the initiator and responder nonce */
    IKE_MEMCPY (pu1TempSeed, pSessionInfo->pu1InitiatorNonce,
                pSessionInfo->u2InitiatorNonceLen);
    IKE_MEMCPY ((pu1TempSeed + pSessionInfo->u2InitiatorNonceLen),
                pSessionInfo->pu1ResponderNonce,
                pSessionInfo->u2ResponderNonceLen);
    /* Generating key material */
    pu1TempKey = pu1Key;
    /* Input Data = Shared Sec(If DH) + Ni + Nr + 1  */
    pu1TempBuf = pu1Seed + (u2DataLen + u4HashLen - IKE_ONE);
    pu1TempBuf[IKE_INDEX_0] = IKE_ONE;

    (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSessionInfo->pIkeSA->pSKeyIdD, (INT4) u4HashLen,
         (pu1Seed + u4HashLen), (INT4) u2DataLen, pu1TempKey);
    u4TotalKeyLen = (u4TotalKeyLen - u4HashLen);

    while (u4TotalKeyLen != IKE_ZERO)
    {
        pu1TempBuf[IKE_INDEX_0]++;
        IKE_MEMCPY (pu1Seed, pu1TempKey, u4HashLen);
        pu1TempKey += u4HashLen;
        (*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
            (pSessionInfo->pIkeSA->pSKeyIdD, (INT4) u4HashLen, pu1Seed,
             (INT4) ((UINT4) u2DataLen + u4HashLen), pu1TempKey);
        u4TotalKeyLen = (u4TotalKeyLen - u4HashLen);
    }

    pu1TempKey = pu1Key;

    if (pIPSecInSA->u1IPSecProtocol == IKE_IPSEC_AH)
    {
        if ((pIPSecInSA->u2AuthKeyLen <= IKE_MAX_KEY_LEN) &&
            (pIPSecOutSA->u2AuthKeyLen <= IKE_MAX_KEY_LEN))
        {
            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                            pIPSecOutSA->u2AuthKeyLen);
                pu1TempKey += pIPSecOutSA->u2AuthKeyLen;
                
                IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                            pIPSecInSA->u2AuthKeyLen);
            }
            else
            {
                IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                            pIPSecInSA->u2AuthKeyLen);
                pu1TempKey += pIPSecInSA->u2AuthKeyLen;
               
                IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                            pIPSecOutSA->u2AuthKeyLen);
            }
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenIPSecKeyMat: Invalid Key Length.\n");

            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Seed);
            return (IKE_FAILURE);
        }
    }
    else if (pIPSecInSA->u1IPSecProtocol == IKE_IPSEC_ESP)
    {
        if (pIPSecInSA->u1EncrAlgo != IKE_IPSEC_ESP_AES_CTR)
        {
            if ((pIPSecOutSA->u2EncrKeyLen <= IKE_MAX_KEY_LEN) &&
                (pIPSecOutSA->u2AuthKeyLen <= IKE_MAX_KEY_LEN) &&
                (pIPSecInSA->u2EncrKeyLen <= IKE_MAX_KEY_LEN) &&
                (pIPSecInSA->u2AuthKeyLen <= IKE_MAX_KEY_LEN))
            {
                if (pSessionInfo->u1Role == IKE_INITIATOR)
                {
                    IKE_MEMCPY (pIPSecOutSA->au1EncrKey, pu1TempKey,
                                pIPSecOutSA->u2EncrKeyLen);
                    pu1TempKey += pIPSecOutSA->u2EncrKeyLen;

                    IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                                pIPSecOutSA->u2AuthKeyLen);
                    pu1TempKey += pIPSecOutSA->u2AuthKeyLen;

                    IKE_MEMCPY (pIPSecInSA->au1EncrKey, pu1TempKey,
                                pIPSecInSA->u2EncrKeyLen);
                    pu1TempKey += pIPSecInSA->u2EncrKeyLen;

                    IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                                pIPSecInSA->u2AuthKeyLen);
                }
                else
                {
                    IKE_MEMCPY (pIPSecInSA->au1EncrKey, pu1TempKey,
                                pIPSecInSA->u2EncrKeyLen);
                    pu1TempKey += pIPSecInSA->u2EncrKeyLen;

                    IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                                pIPSecInSA->u2AuthKeyLen);
                    pu1TempKey += pIPSecInSA->u2AuthKeyLen;

                    IKE_MEMCPY (pIPSecOutSA->au1EncrKey, pu1TempKey,
                                pIPSecOutSA->u2EncrKeyLen);
                    pu1TempKey += pIPSecOutSA->u2EncrKeyLen;

                    IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                                pIPSecOutSA->u2AuthKeyLen);

                }
            }
        }
        else if (pIPSecInSA->u1EncrAlgo == IKE_IPSEC_ESP_AES_CTR)
        {
            if ((pIPSecOutSA->u2EncrKeyLen <= (IKE_MAX_KEY_LEN)) &&
                (pIPSecOutSA->u2AuthKeyLen <= (IKE_MAX_KEY_LEN)) &&
                (pIPSecInSA->u2EncrKeyLen <= (IKE_MAX_KEY_LEN)) &&
                (pIPSecInSA->u2AuthKeyLen <= (IKE_MAX_KEY_LEN)))
            {
                if (pSessionInfo->u1Role == IKE_INITIATOR)
                {
                    IKE_MEMCPY (pIPSecOutSA->au1EncrKey, pu1TempKey,
                                (pIPSecOutSA->u2EncrKeyLen));
                    pu1TempKey += ((pIPSecOutSA->u2EncrKeyLen));
                    IKE_MEMCPY (pIPSecOutSA->au1Nonce, pu1TempKey,
                                IKE_NONCE_LEN);
                    pu1TempKey += IKE_NONCE_LEN;

                    IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                                pIPSecOutSA->u2AuthKeyLen);
                    pu1TempKey += pIPSecOutSA->u2AuthKeyLen;

                    IKE_MEMCPY (pIPSecInSA->au1EncrKey, pu1TempKey,
                                (pIPSecInSA->u2EncrKeyLen));
                    pu1TempKey += ((pIPSecInSA->u2EncrKeyLen));
                    IKE_MEMCPY (pIPSecInSA->au1Nonce, pu1TempKey,
                                IKE_NONCE_LEN);
                    pu1TempKey += IKE_NONCE_LEN;

                    IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                                pIPSecInSA->u2AuthKeyLen);
                }
                else
                {
                    IKE_MEMCPY (pIPSecInSA->au1EncrKey, pu1TempKey,
                                pIPSecInSA->u2EncrKeyLen);
                    pu1TempKey += ((pIPSecInSA->u2EncrKeyLen));
                    IKE_MEMCPY (pIPSecInSA->au1Nonce, pu1TempKey,
                                IKE_NONCE_LEN);
                    pu1TempKey += IKE_NONCE_LEN;

                    IKE_MEMCPY (pIPSecInSA->au1AuthKey, pu1TempKey,
                                pIPSecInSA->u2AuthKeyLen);
                    pu1TempKey += pIPSecInSA->u2AuthKeyLen;

                    IKE_MEMCPY (pIPSecOutSA->au1EncrKey, pu1TempKey,
                                pIPSecOutSA->u2EncrKeyLen);
                    pu1TempKey += ((pIPSecOutSA->u2EncrKeyLen));
                    IKE_MEMCPY (pIPSecOutSA->au1Nonce, pu1TempKey,
                                IKE_NONCE_LEN);
                    pu1TempKey += IKE_NONCE_LEN;

                    IKE_MEMCPY (pIPSecOutSA->au1AuthKey, pu1TempKey,
                                pIPSecOutSA->u2AuthKeyLen);

                }
            }
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2KeyGenIPSecKeyMat: Invalid Key Length.\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Seed);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
            return (IKE_FAILURE);
        }
    }

    /* We memset to 0 for FIPS compliance */
    IKE_BZERO (pu1Seed, (u2DataLen + u4HashLen));
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Seed);
    IKE_BZERO (pu1Key, u4KeyLen);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Key);
    return (IKE_SUCCESS);
}
#endif /* End of the File _IKE2_KEY_C_ */
