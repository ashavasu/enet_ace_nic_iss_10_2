/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2core.c,v 1.12 2015/10/07 11:11:27 siva Exp $
 *
 * Description: This has functions for IKEv2 core Module (FSM)
 *
 ***********************************************************************/

#ifndef _IKE2_CORE_C_
#define _IKE2_CORE_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ike2core.h"
#include "ikememmac.h"

/******************************************************************************/
/*  Function Name : Ike2CoreProcess                                           */
/*  Description   : This function handles the Finite state machine for IKEv2  */
/*                : negotiations.                                             */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreProcess (tIkeSessionInfo * pSessionInfo)
{
    tIsakmpHdr         *pIsakmpHdr = NULL;
    UINT4               u4TotalLen = IKE_ZERO;
    INT1                i1RetVal = IKE_SUCCESS;
    CHR1               *pu1PeerAddrStr = NULL;

    /* Set Error Status as No Error */
    IKE_SET_ERROR (pSessionInfo, IKE_NONE);

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        /* Read Isakmp Header from Packet */
        pIsakmpHdr =
            (tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt);

        if ((pIsakmpHdr->u1Exch != IKE2_INIT_EXCH) &&
            (pIsakmpHdr->u1NextPayload == IKE2_PAYLOAD_ENCRYPTED))
        {
            if (Ike2ProcessEncryptPayload (pSessionInfo,
                                           pSessionInfo->IkeRxPktInfo.pu1RawPkt,
                                           IKE_NONE) == IKE_FAILURE)
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2CoreProcess: Ike2ProcessEncryptPayload "
                              "failed! for peer: %s\n", pu1PeerAddrStr);
                return (IKE_FAILURE);
            }
            /* Read Isakmp Header from Packet */
            pIsakmpHdr =
                (tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        }

        if ((pIsakmpHdr->u1Exch != IKE2_INIT_EXCH) &&
            (pIsakmpHdr->u1NextPayload != IKE2_PAYLOAD_ENCRYPTED))
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2CoreProcess: Discarding Unencrypted "
                          "PayLoad from peer: %s\n", pu1PeerAddrStr);
            return (IKE_FAILURE);
        }

        u4TotalLen = IKE_NTOHL (pIsakmpHdr->u4TotalLen);

        /* validate isakmp packet */
        if (Ike2ValidatePacket (pSessionInfo, u4TotalLen,
                                pIsakmpHdr->u1NextPayload,
                                (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                 sizeof (tIsakmpHdr))) == IKE_FAILURE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2CoreProcess: Ike2ValidatePacket failed for peer: %s\n",
                          pu1PeerAddrStr);
            /* Send Notify Message */
            if (pSessionInfo->u2NotifyType != IKE_NONE)
            {
                if (Ike2ConstructRespWithNotify
                    (pSessionInfo, pSessionInfo->u1ExchangeType) == IKE_FAILURE)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2CoreProcess: "
                                  "Ike2ConstructRespWithNotify failed for peer: %s\n",
                                  pu1PeerAddrStr);
                    return (IKE_FAILURE);
                }
            }
            return (IKE_FAILURE);
        }

        /* Stop the timer and decrement the ref count */
        IkeStopTimer (&pSessionInfo->IkeRetransTimer);

        /* Validation success, Release the previous transmitted packet */
        if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->IkeTxPktInfo.pu1RawPkt);
            pSessionInfo->IkeTxPktInfo.pu1RawPkt = NULL;
        }
    }

    IKE_BZERO (&pSessionInfo->IkeTxPktInfo, sizeof (tIkePacket));

    switch (pSessionInfo->u1ExchangeType)
    {
        case IKE2_INIT_EXCH:
            if (NULL != pIsakmpHdr)
            {
                i1RetVal = Ike2CoreInitExch (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1RetVal = Ike2CoreInitExch (pSessionInfo, NULL);
            }

            if (i1RetVal == IKE_FAILURE)
            {
                /* If notify flag is set, then send Init Response with 
                 * Notify Message of the specified type */
                if (pSessionInfo->u2NotifyType != IKE_NONE)
                {
                    /* Any memory allocated for the Notify Data in the 
                     * session structure during processing will be released
                     * in the API Ike2ConstructRespWithNotify */
                    Ike2ConstructRespWithNotify (pSessionInfo, IKE2_INIT_EXCH);
                }
                /*Increment statistics for phase1 session failed */
                IncIkeNoOfPhase1SessionsFailed ();
                pSessionInfo->bDelSession = IKE_TRUE;
                return (IKE_FAILURE);
            }
            break;

        case IKE2_AUTH_EXCH:
            if (NULL != pIsakmpHdr)
            {
                i1RetVal = Ike2CoreAuthExch (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1RetVal = Ike2CoreAuthExch (pSessionInfo, NULL);
            }

            if (i1RetVal == IKE_FAILURE)
            {
                /* If notify flag is set, then send Init Response with 
                 * Notify Message of the specified type */
                if (pSessionInfo->u2NotifyType != IKE_NONE)
                {
                    Ike2ConstructRespWithNotify (pSessionInfo, IKE2_AUTH_EXCH);
                }
                /*Increment statistics for phase1 session failed */
                pSessionInfo->bDelSession = IKE_TRUE;
                IncIkeNoOfSessionsFail (&pSessionInfo->RemoteTunnelTermAddr);
                return (IKE_FAILURE);
            }
            break;

        case IKE2_CHILD_EXCH:
            if (NULL != pIsakmpHdr)
            {

                i1RetVal = Ike2CoreChildExch (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1RetVal = Ike2CoreChildExch (pSessionInfo, NULL);
            }

            if (i1RetVal == IKE_FAILURE)
            {
                /* If notify flag is set, then send Init Response with 
                 * Notify Message of the specified type */
                if (pSessionInfo->u2NotifyType != IKE_NONE)
                {
                    Ike2ConstructRespWithNotify (pSessionInfo, IKE2_CHILD_EXCH);
                }
                /*Increment statistics for phase2 session failed */
                pSessionInfo->bDelSession = IKE_TRUE;
                IncIkeNoOfSessionsFail (&pSessionInfo->RemoteTunnelTermAddr);
                return (IKE_FAILURE);
            }
            break;

        case IKE2_INFO_EXCH:
            if (NULL != pIsakmpHdr)
            {

                i1RetVal = Ike2CoreInfoExch (pSessionInfo, pIsakmpHdr);
            }
            else
            {
                i1RetVal = Ike2CoreInfoExch (pSessionInfo, NULL);
            }
            if (i1RetVal == IKE_FAILURE)
            {
                return (IKE_FAILURE);
            }

            break;

        default:
            IKE_MOD_TRC2 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2CoreProcess: Invalid Exchange Type: %d for peer: %s\n",
                          pSessionInfo->u1ExchangeType, pu1PeerAddrStr);
            return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreInitExch                                          */
/*  Description   : This function handles the State machine for handling      */
/*                : IKE_SA_INIT messages.                                     */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreInitExch (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    tIke2NotifyPayLoad *pNotifyPayload = NULL;
    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT4               u4TotalLen = IKE_ZERO;
    UINT2               u2NotifyType = IKE_ZERO;
    UINT2               u2Offset = IKE_ZERO;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE2_INIT_IDLE:
        {
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                 (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                  sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInitExch: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }
                /* Make a copy of the new IKE_SA_INIT Request that is sent out 
                 * again for generating the auth payload for AUTH Request  */
                u4TotalLen = IKE_NTOHL (pIsakmpHdr->u4TotalLen);

                if ((pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt =
                     IKE_BUDDY_ALLOC (u4TotalLen)) == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInitExch: State IKE2_INIT_IDLE "
                                 "Memory allocation failed for "
                                 "pu1SaInitReqPkt\n");
                    return (IKE_FAILURE);
                }
                pSessionInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen = u4TotalLen;
                IKE_MEMCPY (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt,
                            pSessionInfo->IkeRxPktInfo.pu1RawPkt, u4TotalLen);
            }
            /* Construct the basic IKE INIT Message */
            if (Ike2CoreConstructInitMsg (pSessionInfo, IKE2_PAYLOAD_SA,
                                          &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInitExch: State IKE2_INIT_IDLE"
                             "Ike2CoreConstructInitMsg Failed\n");
                if (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt != NULL)
                {
                    IKE_BUDDY_FREE
                        (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt);
                    pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt = NULL;
                }
                return (IKE_FAILURE);
            }
            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            /* If responder, generate the Skey ID for IKE */
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (Ike2KeyGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInitExch: State IKE2_INIT_IDLE "
                                 "Ike2KeyGenSkeyId Failed\n");
                    if (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt != NULL)
                    {
                        IKE_BUDDY_FREE
                            (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt);
                    }
                    return (IKE_FAILURE);
                }

                /* Make a copy of the new IKE_SA_INIT Request that is sent out 
                 * again for generating the auth payload for AUTH Request  */
                if ((pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt =
                     IKE_BUDDY_ALLOC
                     (pSessionInfo->IkeTxPktInfo.u4PacketLen)) == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInitExch: State IKE2_INIT_IDLE "
                                 "Malloc failed for pu1SaInitReqPkt  Failed\n");
                    if (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt != NULL)
                    {
                        IKE_BUDDY_FREE
                            (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt);
                    }
                    return (IKE_FAILURE);
                }
                pSessionInfo->pIkeSA->Ike2SA.u4SaInitRspPktLen =
                    pSessionInfo->IkeTxPktInfo.u4PacketLen;
                IKE_MEMCPY (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt,
                            pSessionInfo->IkeTxPktInfo.pu1RawPkt,
                            pSessionInfo->IkeTxPktInfo.u4PacketLen);
                pSessionInfo->bPhase1Done = IKE_TRUE;
                pSessionInfo->u1FsmState = IKE2_INIT_DONE;
            }
            else
            {
                pSessionInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen =
                    pSessionInfo->IkeTxPktInfo.u4PacketLen;
                /* Make a copy of the new IKE_SA_INIT Request that is sent out 
                 * again for generating the auth payload for AUTH Request  */
                if ((pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt =
                     IKE_BUDDY_ALLOC
                     (pSessionInfo->IkeTxPktInfo.u4PacketLen)) == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInitExch: State IKE2_INIT_IDLE "
                                 "Malloc failed for pu1SaInitReqPkt  Failed\n");
                    return (IKE_FAILURE);
                }
                IKE_MEMCPY (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt,
                            pSessionInfo->IkeTxPktInfo.pu1RawPkt,
                            pSessionInfo->IkeTxPktInfo.u4PacketLen);
                pSessionInfo->u1FsmState = IKE2_INIT_WAIT;
            }

            pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
            if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                               IKE_RETRANS_TIMER_ID, IKE_RETRANSMIT_INTERVAL,
                               pSessionInfo) != IKE_SUCCESS)
            {
                /* This is not critical error, so just log and don't
                   return failure
                 */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInitExch: IKE2_INIT_IDLE "
                             "IkeStartTimer Failed\n");
            }
            break;
        }
        case IKE2_INIT_WAIT:
        {
            /* This state is only for Inititator */
            if (pIsakmpHdr->u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                pNotifyPayload =
                    (tIke2NotifyPayLoad *) (void *) (pSessionInfo->IkeRxPktInfo.
                                                     pu1RawPkt +
                                                     sizeof (tIsakmpHdr));

                u2NotifyType = IKE_NTOHS (pNotifyPayload->u2NotifyMsgType);

                if ((u2NotifyType != IKE2_COOKIE) &&
                    (pNotifyPayload->u1NextPayLoad == IKE2_PAYLOAD_NONE))
                {
                    /*We have received a Notify payload for an error condition */
                    if (Ike2ProcessNotify (pSessionInfo,
                                           (UINT1 *) pNotifyPayload,
                                           IKE_ZERO) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                                     "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                                     "Received Notify Error as response.\n");
                        /* Session Will be deleted in 
                         * Ike2ProcessIkePkt on failure */
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    /* The Peer has sent N(COOKIE), insert the N(COOKIE) in the 
                     * INIT_REQUEST and resend */
                    pSessionInfo->NotifyInfo.u2NotifyType = IKE2_COOKIE;
                    pSessionInfo->NotifyInfo.u1Protocol = IKE2_PROTOCOL_IKE;
                    pSessionInfo->NotifyInfo.u1SpiSize = IKE_NONE;

                    pSessionInfo->NotifyInfo.u2NotifyDataLen =
                        IKE_NTOHS ((pNotifyPayload->u2PayLoadLen -
                                    sizeof (tIke2NotifyPayLoad)));

                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pSessionInfo->NotifyInfo.
                         pu1NotifyData) == MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInitExch: unable to allocate "
                                     "buffer\n");
                        return (IKE_FAILURE);
                    }
                    if (pSessionInfo->NotifyInfo.pu1NotifyData == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                                     "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                                     " Session memory Allocation Fail\n");
                        return (IKE_FAILURE);
                    }
                    IKE_BZERO (pSessionInfo->NotifyInfo.pu1NotifyData,
                               (size_t) (pSessionInfo->NotifyInfo.
                                         u2NotifyDataLen + IKE_ONE));

                    u2Offset = sizeof (tIke2NotifyPayLoad);
                    IKE_MEMCPY (pSessionInfo->NotifyInfo.pu1NotifyData,
                                (pNotifyPayload + u2Offset),
                                pSessionInfo->NotifyInfo.u2NotifyDataLen);

                    /* Memory allocated for the NotifyData will be released in 
                     * Ike2CoreConstructInitMsg.*/
                    if (Ike2CoreConstructInitMsg (pSessionInfo,
                                                  IKE2_PAYLOAD_NOTIFY,
                                                  &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                                     "Ike2CoreConstructInitMsg Failed\n");
                        return (IKE_FAILURE);
                    }

                    /* Update Packet total length in the Isakmp hdr */
                    IKE_ASSERT (u4Len ==
                                pSessionInfo->IkeTxPktInfo.u4PacketLen);
                    u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
                    ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.
                                              pu1RawPkt))->u4TotalLen = u4Len;

                    /* Make a copy of the new IKE_SA_INIT Request 
                     * that is sent out again for generating the 
                     * auth payload for AUTH Request  */
                    if (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt != NULL)
                    {
                        IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.
                                        pu1SaInitReqPkt);
                    }

                    if ((pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt =
                         IKE_BUDDY_ALLOC (pSessionInfo->IkeTxPktInfo.
                                          u4PacketLen)) == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                                     "Malloc failed for pu1SaInitReqPkt  Failed\n");
                        return (IKE_FAILURE);
                    }
                    pSessionInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen =
                        pSessionInfo->IkeTxPktInfo.u4PacketLen;
                    IKE_MEMCPY (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt,
                                pSessionInfo->IkeTxPktInfo.pu1RawPkt,
                                pSessionInfo->IkeTxPktInfo.u4PacketLen);

                    pSessionInfo->u1FsmState = IKE2_INIT_WAIT;
                    pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
                    if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                       IKE_RETRANS_TIMER_ID,
                                       IKE_RETRANSMIT_INTERVAL,
                                       pSessionInfo) != IKE_SUCCESS)
                    {
                        /* This is not critical error, so just log and don't
                           return failure
                         */
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInitExch: IKE2_INIT_IDLE "
                                     "IkeStartTimer Failed\n");
                    }
                    return (IKE_SUCCESS);
                }
            }

            if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                             (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                              sizeof (tIsakmpHdr))) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInitExch: State IKE2_INIT_WAIT"
                             " Ike2Process Failed \n");
                return (IKE_FAILURE);
            }

            /* If responder, generate the Skey ID for IKE */
            if (Ike2KeyGenSkeyId (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                             "Ike2KeyGenSkeyId Failed\n");
                return (IKE_FAILURE);
            }

            /* Make a copy of the new IKE_SA_INIT Response that is 
             * received for generating the auth payload for AUTH Response */
            u4TotalLen = IKE_NTOHL (pIsakmpHdr->u4TotalLen);

            if ((pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt =
                 IKE_BUDDY_ALLOC (u4TotalLen)) == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInitExch: State IKE2_INIT_WAIT "
                             "Malloc failed for pu1SaInitReqPkt  Failed\n");
                return (IKE_FAILURE);
            }

            pSessionInfo->pIkeSA->Ike2SA.u4SaInitRspPktLen = u4TotalLen;
            IKE_MEMCPY (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt,
                        pSessionInfo->IkeRxPktInfo.pu1RawPkt, u4TotalLen);
            /* Set Phase1 Done == True */
            pSessionInfo->bPhase1Done = IKE_TRUE;
            pSessionInfo->u1FsmState = IKE2_INIT_DONE;
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreInitExch: State Invalid "
                         "Session in Invalid State \n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreConstructInitMsg                                  */
/*  Description   : This function handles the Construction of Init Req/Resp   */
/*                : for IKE_SA_INIT messages.This API will release any memory */
/*                : allocated for the pSessionInfo->pu1NotifyData.            */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : u1NextPayload - Next Payload type to be filled in         */
/*                :                 the isakmp header (SA/Notify)             */
/*                : pu4Len        - Pointer to the Length of the packet to    */
/*                :                 be updated                                */
/*  Output(s)     : NONE                                                      */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreConstructInitMsg (tIkeSessionInfo * pSessionInfo, UINT1 u1NextPayload,
                          UINT4 *pu4Len)
{
    BOOLEAN             bSendCertReq = IKE_FALSE;
    UINT1               u1NxtPayload = u1NextPayload;
    tIkeNotifyInfo     *pNotifyInfo = NULL;

    pNotifyInfo = &pSessionInfo->NotifyInfo;

    /* Construct IKEv2 (ISAKMP) Header */
    if (Ike2ConstructHeader (pSessionInfo, IKE2_INIT_EXCH,
                             u1NextPayload) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreConstructInitMsg: Ike2ConstructHeader Failed\n");
        if (pNotifyInfo->pu1NotifyData != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo->pu1NotifyData);
            pNotifyInfo->pu1NotifyData = NULL;
        }
        return (IKE_FAILURE);
    }

    /* If a N(COOKIE) payload is received in the INIT response,
     * Insert it in the last send INIT Request as the first payload type */
    if (u1NxtPayload == IKE2_PAYLOAD_NOTIFY)
    {
        pNotifyInfo->u1Protocol = IKE2_PROTOCOL_IKE;
        pNotifyInfo->u1SpiSize = IKE_NONE;

        /* Copy the Responder Cookie into the NotifyData for NotifyInfo 
         * structure */
        if (Ike2ConstructNotify (pSessionInfo, IKE2_PAYLOAD_SA,
                                 pu4Len, pNotifyInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreConstructInitMsg:  "
                         "Ike2ConstructNotify Failed\n");
            if (pNotifyInfo->pu1NotifyData != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pNotifyInfo->pu1NotifyData);
                pNotifyInfo->pu1NotifyData = NULL;
            }
            return (IKE_FAILURE);
        }

        /* Release the memory allocated for the Notify Data in the session 
         * structure,  as it is not required any more */
        if (pNotifyInfo->pu1NotifyData != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo->pu1NotifyData);
            pNotifyInfo->pu1NotifyData = NULL;
        }
    }

    /* Construct IKE SA Payload */
    if (Ike2ConstructSA (pSessionInfo, IKE2_PAYLOAD_KE, pu4Len) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreConstructInitMsg:  Ike2ConstructSA Failed\n");
        return (IKE_FAILURE);
    }

    /* Construct Key Exchange PayLoad */
    if (Ike2ConstructKE (pSessionInfo, IKE2_PAYLOAD_NONCE,
                         pu4Len) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreConstructInitMsg:  Ike2ConstructKE Failed\n");
        return (IKE_FAILURE);
    }

    /* In case of Responder, check if Peer Supports NAT Traversal,
     * Update Next Payload Type */
    u1NxtPayload = IKE2_PAYLOAD_NONE;
    if (pSessionInfo->u1Role == IKE_RESPONDER)
    {
        if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
            IKE_PEER_SUPPORT_NATT)
        {
            u1NxtPayload = IKE2_PAYLOAD_NOTIFY;
        }
        if ((pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA) ||
            (pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA))
        {
            /* check whether cert request is required to be sent */
            if (IkeCheckPeerCertDB (pSessionInfo) == IKE_FALSE)
            {
                bSendCertReq = IKE_TRUE;
                u1NxtPayload = IKE2_PAYLOAD_CERTREQ;
            }
        }
    }
    else
    {
        u1NxtPayload = IKE2_PAYLOAD_NOTIFY;
    }

    /* Construct Nonce PayLoad */
    if (Ike2ConstructNonce (pSessionInfo, u1NxtPayload, pu4Len) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreConstructInitMsg: Ike2ConstructNonce Failed\n");
        return (IKE_FAILURE);
    }

    if (u1NxtPayload == IKE2_PAYLOAD_NOTIFY)
    {
        /* In case of Responder, check if we need to send the CERT REQ 
         * payload*/
        if (pSessionInfo->u1Role == IKE_RESPONDER)
        {
            u1NxtPayload = IKE2_PAYLOAD_NONE;
        }
        else
        {
            u1NxtPayload = IKE2_PAYLOAD_NONE;
        }
        if (Ike2NattConstructDetectSrcIP (pSessionInfo, IKE2_PAYLOAD_NOTIFY,
                                          pu4Len) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreConstructInitMsg: "
                         "Ike2NattConstructDetectSrcIP Failed\n");
            return (IKE_FAILURE);
        }
        if (Ike2NattConstructDetectDstIP (pSessionInfo,
                                          u1NxtPayload, pu4Len) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreConstructInitMsg: "
                         "Ike2NattConstructDetectDstIP Failed\n");
            return (IKE_FAILURE);
        }
    }

    if ((pSessionInfo->u1Role == IKE_RESPONDER) && (bSendCertReq == IKE_TRUE))
    {
        /* Construct Certificate request PayLoad */
        if (Ike2ConstructCertReq (pSessionInfo,
                                  IKE_NONE_PAYLOAD, pu4Len) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreConstructInitMsg: Ike2ConstructCertReq "
                         "Failed\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreAuthExch                                          */
/*  Description   : This function handles the State machine for handling      */
/*                : IKE2_AUTH  messages.                                      */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreAuthExch (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT1              *pu1Payload = NULL;
    tIke2NotifyPayLoad *pu1NotifyPayload = NULL;
    tIke2ConfigPayLoad *pu1CfgPayload = NULL;
    tIkeNotifyInfo      NotifyInfo;

    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT1               u1CfgType = IKE_NONE;
    UINT1               u1NextPayload = IKE_NONE;
    BOOLEAN             bSendCertReq = IKE_FALSE;
    UINT2               u2CurPayloadLen = IKE_ZERO;
    UINT2               u2NotifyType = IKE_ZERO;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE2_AUTH_IDLE:
        {
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                /* If we are the responder, The Cryptomap policy to 
                 * be used is to be selected by processing the TS /IDi payload
                 * and the sessionInfo structure is to be updated with the 
                 * Cryptomap details */
                if (Ike2ProcessTsId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }

                if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                 (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                  sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }

                /* AUTH must be successfull, bAuthDone flag to IKE_TRUE */
                pSessionInfo->pIkeSA->Ike2SA.bAuthDone = IKE_TRUE;

                /* Check if the required Payloads are present  */
                if ((pSessionInfo->pIkeSA->bCMEnabled == TRUE) &&
                    (pSessionInfo->Ike2AuthInfo.bCMServer == IKE_TRUE))
                {
                    /* If we are RA-VPN Server and CP(CFG_REQUEST) message 
                     * was not received and processed, 
                     * Return N(CP_FAILED_REQUIRED)*/
                    if (pSessionInfo->Ike2AuthInfo.bCMRequested != IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_FAILED_CP_REQUIRED);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: IKE2_AUTH_IDLE"
                                     "CP(CFG_REQUEST) not found in AUTH "
                                     "Request \n");
                        return (IKE_FAILURE);
                    }
                }
                /* If we are configured to use Transport Mode and Peer has
                 * not sent N(USE_TRANSPORT_MODE), then Send N(INVALID_SYNTAX)*/
                if ((pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                     IKE_IPSEC_TRANSPORT_MODE) &&
                    (pSessionInfo->Ike2AuthInfo.bTransportReqd != IKE_TRUE))
                {
                    IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: N(USE_TRANSPORT_MODE) "
                                 "payload, not found in packet, when local "
                                 "configured is to use transport mode\n");
                    return (IKE_FAILURE);
                }

                /* We are configured to use TUNNEL mode and peer has sent a 
                 * N(USE_TRANSPORT_MODE), send */
                if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                    IKE_IPSEC_TUNNEL_MODE)
                {
                    if (pSessionInfo->Ike2AuthInfo.bTransportReqd == IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: N(USE_TRANSPORT_MODE) "
                                     "payload,  found in packet, when local "
                                     "configuration is to use tunnel mode\n");
                        return (IKE_FAILURE);
                    }
                }
                /* Processiong Successful, Construct the Auth Response 
                 * payload with first payload as IKE2_PAYLOAD_IDR*/
                u1NextPayload = IKE2_PAYLOAD_IDR;
            }
            else
            {
                /* For Initiator Construct the Auth Response payload with 
                 * first payload as IKE2_PAYLOAD_IDI*/
                u1NextPayload = IKE2_PAYLOAD_IDI;
            }

            /* Construct IKEv2 (ISAKMP) Header */
            if (Ike2ConstructHeader (pSessionInfo, IKE2_AUTH_EXCH,
                                     u1NextPayload) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch: Ike2ConstructHeader Failed\n");
                return (IKE_FAILURE);
            }

            /* Check if we need to send CERT Payload and CERTREQ payload
             * to the Peer */
            u1NextPayload = IKE2_PAYLOAD_AUTH;
            if ((pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA) ||
                (pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA))
            {
                if (IkeCheckSendingCertPayload (pSessionInfo) == IKE_TRUE)
                {
                    u1NextPayload = IKE2_PAYLOAD_CERT;
                }
            }

            /* Construct the AUTH Request/Response */
            if (Ike2ConstructID (pSessionInfo, u1NextPayload, &u4Len)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch: IKE2_AUTH_IDLE "
                             "Ike2ConstructID Failed\n");
                return (IKE_FAILURE);
            }

            /* If Auth Menthod is certificates, Send CERT and CERTREQ 
             * Payloads too */
            if (u1NextPayload == IKE2_PAYLOAD_CERT)
            {
                /* In case of Initiator , see if we need to send the CERTREQ 
                 * Payload */
                u1NextPayload = IKE2_PAYLOAD_AUTH;
                if ((pSessionInfo->u1Role == IKE_INITIATOR) &&
                    (IkeCheckPeerCertDB (pSessionInfo) == IKE_FALSE))
                {
                    bSendCertReq = IKE_TRUE;
                    u1NextPayload = IKE2_PAYLOAD_CERTREQ;
                }
                /* Construct certificate payloads */
                if (Ike2ConstructCert
                    (pSessionInfo, u1NextPayload, &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: State IKE2_AUTH_IDLE "
                                 "Ike2ConstructCert Failed\n");
                    return (IKE_FAILURE);
                }

                /* check whether cert request is required to send */
                /* Construct Certificate request PayLoad */
                if ((pSessionInfo->u1Role == IKE_INITIATOR) &&
                    (bSendCertReq == IKE_TRUE))
                {
                    if (Ike2ConstructCertReq (pSessionInfo,
                                              IKE2_PAYLOAD_AUTH,
                                              &u4Len) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: "
                                     "State IKE2_AUTH_IDLE "
                                     "Ike2ConstructCertReq Failed\n");
                        return (IKE_FAILURE);
                    }
                }
            }

            /* If the Policy for the peer is RA-VPN (Client/Server),
             * then Send the CP Paylod (CFG_REQ/CFG_REPLY) */

            u1NextPayload = IKE2_PAYLOAD_SA;

            if ((pSessionInfo->pIkeSA->bCMEnabled == TRUE) &&
                (pSessionInfo->pIkeSA->bCMDone == FALSE))
            {
                /* In case of RA-VPN, Next payload is CONFIG either 
                 * (CFG_REQ/CFG_REP) */
                u1NextPayload = IKE2_PAYLOAD_CONFIG;
            }
            else if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                     IKE_IPSEC_TRANSPORT_MODE)
            {
                /* Site-Site case, transport case - next payload is 
                 * N(USE_TRANSPORT_MODE) */
                u1NextPayload = IKE2_PAYLOAD_NOTIFY;
            }

            if (Ike2ConstructAuth (pSessionInfo, u1NextPayload,
                                   &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch: State IKE2_AUTH_IDLE "
                             "Ike2ConstructAuth Failed\n");
                return (IKE_FAILURE);
            }

            if (u1NextPayload == IKE2_PAYLOAD_CONFIG)
            {
                /* If we are the CM server, Construct Config payload of 
                 * type CFG_REPLY, else CFG_REQUEST */
                u1CfgType = IKE2_CONFIG_REQUEST;
                if (pSessionInfo->Ike2AuthInfo.bCMServer == IKE_TRUE)
                {
                    u1CfgType = IKE2_CONFIG_REPLY;
                }

                u1NextPayload = IKE2_PAYLOAD_SA;
                if (Ike2ConstructCfg (pSessionInfo, u1NextPayload,
                                      &u4Len, u1CfgType) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: State IKE2_AUTH_IDLE "
                                 "Ike2Constructcfg Failed\n");
                    return (IKE_FAILURE);
                }

                /* Install the Dynamic crypto map for server... */
                if (u1CfgType == IKE2_CONFIG_REPLY)
                {
                    if (IkeCreateDynCryptoMapForServer (pSessionInfo) ==
                        IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: state IKE2_AUTH_IDLE"
                                     "IkeCreateDynCryptoMapForServer "
                                     "Failed\n");
                        return (IKE_FAILURE);
                    }
                    if (Ike2UtilUpdateCryptoForRA (pSessionInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: state IKE2_AUTH_IDLE"
                                     "Ike2UtilUpdateCryptoForRA Failed\n");
                        return (IKE_FAILURE);
                    }
                    /* Set the CMDone to True */
                    pSessionInfo->pIkeSA->bCMDone = IKE_TRUE;
                }
            }

            /* Need to send N(USE_TRANSPORT_MODE) if mode is configured
             * as transport/and we receive a msg with N(USE_TRANSPORT_MODE)
             * */
            if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                IKE_BZERO (&NotifyInfo, sizeof (tIkeNotifyInfo));
                NotifyInfo.u1Protocol = IKE2_PROTOCOL_IKE;
                NotifyInfo.u2NotifyType = IKE2_USE_TRANSPORT_MODE;
                NotifyInfo.u1SpiSize = IKE_NONE;
                NotifyInfo.u2NotifyDataLen = IKE_ZERO;

                if (Ike2ConstructNotify (pSessionInfo, IKE2_PAYLOAD_SA,
                                         &u4Len, &NotifyInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                                 "Ike2ConstructNotify \n");
                    return (IKE_FAILURE);
                }
            }

            /* Construct IPSec SA Payload */
            if (Ike2ConstructIpsecSA (pSessionInfo, IKE2_PAYLOAD_TSI,
                                      &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                             "Ike2ConstructIpsecSA Failed\n");
                return (IKE_FAILURE);
            }
            /* Construct the Traffic Selector at the initiators end (TSi) */
            if (Ike2ConstructTSi (pSessionInfo, IKE2_PAYLOAD_TSR,
                                  &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                             "Ike2ConstructTSi Failed\n");
                return (IKE_FAILURE);
            }
            /* Construct the Traffic Selector at the responders end (TSr) */
            if (Ike2ConstructTSr (pSessionInfo, IKE2_PAYLOAD_NONE,
                                  &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                             "Ike2ConstructTSr Failed\n");
                return (IKE_FAILURE);
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                /* Generate the IPSec keys and intimiate to IPsec to Store
                   the SA etc. */
                if (Ike2CoreDoPostChildExch (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                                 "Ike2CoreDoPostChildExch Failed\n");
                    return (IKE_FAILURE);
                }
                pSessionInfo->u1FsmState = IKE2_AUTH_DONE;

                /* Release memory of all the transients required 
                 * for authentication */
                /* Release the Init Req packet Buffer and
                   ID payload from the session structure */
                IKE_BZERO (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt,
                           pSessionInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen);

                IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt);
                pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt = NULL;

                IKE_BZERO (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt,
                           pSessionInfo->pIkeSA->Ike2SA.u4SaInitRspPktLen);
                IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt);
                pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt = NULL;

                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo->pu1IdPayloadSent);
                pSessionInfo->pu1IdPayloadSent = NULL;
                pSessionInfo->u4IdPayloadSentLen = IKE_ZERO;

                /* Release the Init Response packet Buffer and
                   ID payload from the session structure */
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo->pu1IdPayloadRecd);
                pSessionInfo->pu1IdPayloadRecd = NULL;
                pSessionInfo->u4IdPayloadRecdLen = IKE_ZERO;

            }
            else
            {
                pSessionInfo->u1FsmState = IKE2_AUTH_WAIT;
                pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
                if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                   IKE_RETRANS_TIMER_ID,
                                   IKE_RETRANSMIT_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* This is not critical error, so just log and don't
                     *                    return failure */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: IKE2_AUTH_IDLE "
                                 "IkeStartTimer Failed\n");
                }
            }
            break;
        }
        case IKE2_AUTH_WAIT:
        {
            /* This state is only for the INITIATOR. */
            /* Check if the first payload is Hash Payload */
            u1NextPayload =
                ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.
                                          pu1RawPkt))->u1NextPayload;

            pu1Payload =
                (pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr));

            /* If the Next payload is encrypted, The payload is already
             * decrypted, so now just skip the encrypted payload and move
             * to the next one */
            if (u1NextPayload == IKE2_PAYLOAD_ENCRYPTED)
            {
                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;
            }

            if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                /* Auth Req Failed , peer has sent Notify to intimate 
                 * the failure,  No Need to send a notify for a notify , 
                 * so no error checking. */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreAuthExch: IKE2_AUTH_WAIT "
                             "Received Notify Payload as AUTH Response \n");
                Ike2ProcessNotify (pSessionInfo, pu1Payload, IKE2_NONE);
                /* Session Will be deleted in Ike2ProcessIkePkt on failure */
                return (IKE_FAILURE);
            }
            else if (u1NextPayload == IKE2_PAYLOAD_IDR)
            {
                if (Ike2ProcessId (pSessionInfo, pu1Payload,
                                   IKE_RESPONDER) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: IKE2_AUTH_WAIT "
                                 "Ike2ProcessId Payload  failed \n");
                    return (IKE_FAILURE);
                }
                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;

                if (u1NextPayload == IKE2_PAYLOAD_CERT)
                {
                    if (Ike2ProcessCert (pSessionInfo, pu1Payload, IKE_ZERO) ==
                        IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: IKE2_AUTH_WAIT "
                                     "Ike2ProcessCert Payload failed \n");
                        return (IKE_FAILURE);
                    }
                    Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                                   &u2CurPayloadLen);
                    pu1Payload = pu1Payload + u2CurPayloadLen;
                }
                /* As of now we do not support, Certificate, chaining, so skip 
                 * the rest of the certificates other than the first one */
                while (u1NextPayload == IKE2_PAYLOAD_CERT)
                {
                    Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                                   &u2CurPayloadLen);
                    pu1Payload = pu1Payload + u2CurPayloadLen;
                }
                if (u1NextPayload == IKE2_PAYLOAD_AUTH)
                {
                    if (Ike2ProcessAuth (pSessionInfo, pu1Payload, IKE_ZERO) ==
                        IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: IKE2_AUTH_WAIT "
                                     "Ike2ProcessAuth Payload failed \n");
                        return (IKE_FAILURE);
                    }
                    if (pSessionInfo->pIkeSA->Ike2SA.bAuthDone != IKE_TRUE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: Ike2Process Auth "
                                     "status is not done after processing of "
                                     "IKE2_AUTH Request, Auth Payload might be "
                                     " missing in the request\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: IKE2_AUTH_WAIT "
                                 "Auth Payload expected but not found \n");
                    return (IKE_FAILURE);
                }
                /* Authentication is successful, Set the bAuthDone flag in
                 * IKE_SA to true */
                pSessionInfo->pIkeSA->Ike2SA.bAuthDone = IKE_TRUE;

                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;

                if ((pSessionInfo->pIkeSA->bCMEnabled == TRUE) &&
                    (pSessionInfo->Ike2AuthInfo.bCMServer == IKE_FALSE) &&
                    (pSessionInfo->pIkeSA->bCMDone != TRUE))
                {
                    if (u1NextPayload == IKE2_PAYLOAD_CONFIG)
                    {
                        pu1CfgPayload =
                            (tIke2ConfigPayLoad *) (void *) pu1Payload;
                        if (pu1CfgPayload->u1CfgType != IKE2_CONFIG_REPLY)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "Ike2CoreAuthExch: IKE2_AUTH_WAIT -"
                                         " CM is enabled but no CFG_REPLY "
                                         "received in packet \n");
                            return (IKE_FAILURE);
                        }
                        if (Ike2ProcessCfg (pSessionInfo, pu1Payload,
                                            IKE_ZERO) == IKE_FAILURE)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "Ike2CoreAuthExch: IKE2_AUTH_WAIT -"
                                         " Processing of CP(CFG_REPLY) "
                                         "failed \n");
                            return (IKE_FAILURE);
                        }
                        /* Install the Dynamic crypto map for client */
                        if (IkeCreateDynCryptoMapForClient (pSessionInfo) ==
                            IKE_FAILURE)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "Ike2CoreAuthExch: state IKE2_AUTH_IDLE"
                                         "IkeCreateDynCryptoMapForClient "
                                         "Failed\n");
                            return (IKE_FAILURE);
                        }
                        else
                        {
                            if (Ike2UtilUpdateCryptoForRA (pSessionInfo) ==
                                IKE_FAILURE)
                            {
                                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                             "Ike2CoreAuthExch: state IKE2_AUTH_IDLE"
                                             "Ike2UtilUpdateCryptoForRA Failed\n");
                                return (IKE_FAILURE);
                            }
                            /* Set the CMDone to True */
                            pSessionInfo->pIkeSA->bCMDone = IKE_TRUE;
                        }

                        Ike2UtilGetNextPayloadAndType (pu1Payload,
                                                       &u1NextPayload,
                                                       &u2CurPayloadLen);
                        pu1Payload = pu1Payload + u2CurPayloadLen;
                    }
                    else if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
                    {
                        pu1NotifyPayload =
                            (tIke2NotifyPayLoad *) (void *) pu1Payload;
                        u2NotifyType =
                            IKE_NTOHS (pu1NotifyPayload->u2NotifyMsgType);

                        if (u2NotifyType == IKE2_INTERNAL_ADDRESS_FAILURE)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "Ike2CoreAuthExch: IKE2_AUTH_WAIT - "
                                         "Received N(INTERNAL_ADDRESS_FAILURE)"
                                         " ending negotiation \n");
                            return (IKE_FAILURE);
                        }
                    }
                    else
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: IKE2_AUTH_WAIT -"
                                     " CP(CFG_REPLY) expected "
                                     "but not received \n");
                        return (IKE_FAILURE);
                    }
                }
                /* Check all the available Notify Payloads */
                while (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
                {
                    pu1NotifyPayload =
                        (tIke2NotifyPayLoad *) (void *) pu1Payload;
                    u2NotifyType =
                        IKE_NTOHS (pu1NotifyPayload->u2NotifyMsgType);

                    if ((u2NotifyType == IKE2_NO_PROPOSAL_CHOSEN) ||
                        (u2NotifyType == IKE2_TS_UNACCEPTABLE) ||
                        (u2NotifyType == IKE2_SINGLE_PAIR_REQUIRED))
                    {
                        /* In this case, the negotiation cannot 
                         * proceed, unless, the configuration is 
                         * changed for the mismatching payload */
                        Ike2ProcessNotify (pSessionInfo, pu1Payload, IKE2_NONE);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: IKE2_AUTH_WAIT -"
                                     " Received Notify Payload with type  "
                                     "(NO_PROPOSAL_CHOOSEN or TS_UNACCEPTABLE)"
                                     "received in packet \n");
                        /* Session Will be deleted in 
                         * Ike2ProcessIkePkt on failure */
                        return (IKE_FAILURE);
                    }
                    /* If Ra-VPN Client, then see if we got a CP_ */
                    /* Rest of the Notify Payloads like IPCOMP_SUPPORTED 
                     * etc can be ignored */
                    Ike2ProcessNotify (pSessionInfo, pu1Payload, IKE2_NONE);
                    Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                                   &u2CurPayloadLen);
                    pu1Payload = pu1Payload + u2CurPayloadLen;
                }
                /* Process rest of the payloads */
                if (u1NextPayload != IKE2_PAYLOAD_SA)
                {
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2CoreAuthExch: State IKE2_AUTH_WAIT - "
                                  "Expecting a SA Payload, but received %d "
                                  "payload\n", u1NextPayload);
                    return (IKE_FAILURE);
                }

                if (Ike2Process (u1NextPayload, pSessionInfo,
                                 pu1Payload) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }
                /* If we are configured to use Transport Mode and Peer has not
                 * sent N(USE_TRANSPORT_MODE), then Send N(INVALID_SYNTAX) */
                if ((pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                     IKE_IPSEC_TRANSPORT_MODE))
                {
                    if (pSessionInfo->Ike2AuthInfo.bTransportReqd != IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: N(USE_TRANSPORT_MODE) "
                                     "payload, not found in packet, when local "
                                     "configured is to use transport mode\n");
                        return (IKE_FAILURE);
                    }
                }

                /* We are configured to use TUNNEL mode and peer has sent a 
                 * N(USE_TRANSPORT_MODE), send N (INVALID_SYNTAX)*/
                if ((pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                     IKE_IPSEC_TUNNEL_MODE))
                {
                    if (pSessionInfo->Ike2AuthInfo.bTransportReqd == IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreAuthExch: N(USE_TRANSPORT_MODE) "
                                     "payload,  found in packet, when local "
                                     "configuration is to use tunnel mode\n");
                        return (IKE_FAILURE);
                    }
                }
                /* Store Child SA and intimate IPsec to store the IPSec SA */
                if (Ike2CoreDoPostChildExch (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreAuthExch:  IKE2_AUTH_IDLE "
                                 "Ike2CoreDoPostChildExch Failed\n");
                    return (IKE_FAILURE);
                }
                pSessionInfo->u1FsmState = IKE2_AUTH_DONE;

                /* Release memory of all the transients required 
                 * for authentication. Release the Init Req/Rsp packet 
                 * Buffer and ID payloads(Sent/Rcvd) from the session 
                 * structure */
                IKE_BZERO (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt,
                           pSessionInfo->pIkeSA->Ike2SA.u4SaInitReqPktLen);

                IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt);
                pSessionInfo->pIkeSA->Ike2SA.pu1SaInitReqPkt = NULL;

                IKE_BZERO (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt,
                           pSessionInfo->pIkeSA->Ike2SA.u4SaInitRspPktLen);
                IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt);
                pSessionInfo->pIkeSA->Ike2SA.pu1SaInitRspPkt = NULL;

                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo->pu1IdPayloadSent);
                pSessionInfo->pu1IdPayloadSent = NULL;
                pSessionInfo->u4IdPayloadSentLen = IKE_ZERO;

                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pSessionInfo->pu1IdPayloadRecd);
                pSessionInfo->pu1IdPayloadRecd = NULL;
                pSessionInfo->u4IdPayloadRecdLen = IKE_ZERO;

            }
            else
            {
                IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                              "Ike2CoreAuthExch: IKE2_AUTH_WAIT -"
                              "Expecting IDr Payload but received "
                              " payload of type %d \n", u1NextPayload);
                return (IKE_FAILURE);
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreAuthExch:  Invalid State \n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreDoPostChildExch                                   */
/*  Description   : This function Does the post Child Exch activities like    */
/*                : generating IPSec keys, Intimating IPSec to Install the SA */
/*                : etc.                                                      */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                :                                                           */
/*  Output(s)     :  NONE                                                     */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/

INT1
Ike2CoreDoPostChildExch (tIkeSessionInfo * pSessionInfo)
{
    tIkeIPSecIfParam    IPSecIfParam;
    CHR1               *pu1PeerAddrStr = NULL;

    /* Generate the IPSec Keys */
    if (Ike2KeyGenIPSecKeys (pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreDoPostChildExch: Ike2KeyGenIPSecKeys failed\n");
        return (IKE_FAILURE);
    }

    /* Copy the Phase2 SA Bundle and send to IPSec */
    IKE_BZERO (&IPSecIfParam, sizeof (tIkeIPSecIfParam));

    /* Intimate IPSec whether to install only the SAs or
     * all (SA, Policy, ACL and Selectors */
    if (pSessionInfo->pIkeSA->bCMDone == TRUE)
    {
        pSessionInfo->Ike2AuthInfo.IPSecBundle.bInstallDynamicIPSecDB = TRUE;
        /* Create the VPNC Interface in case of RAVPN Client in AUTH 
           exchange */
        if (pSessionInfo->Ike2AuthInfo.bCMServer == IKE_FALSE)
        {
            if (IkeCreateVpncInterface (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreDoPostChildExch: IkeCreateVpncInterface failed\n");
                return IKE_FAILURE;
            }
        }
    }
    else
    {
        pSessionInfo->Ike2AuthInfo.IPSecBundle.bInstallDynamicIPSecDB = FALSE;
    }
    /* Intimate IPSec about the VPN mode */
    pSessionInfo->Ike2AuthInfo.IPSecBundle.bVpnServer = gbCMServer;

    IPSecIfParam.u4MsgType = IKE_IPSEC_INSTALL_SA;

    if (((pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
          IKE_USE_NATT_PORT) == IKE_USE_NATT_PORT) &&
        ((pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags &
          IKE_PEER_BEHIND_NAT) == IKE_PEER_BEHIND_NAT))
    {
        if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeNatKeepAliveTimer,
                           IKE_NAT_KEEP_ALIVE_TIMER_ID,
                           IKE_NAT_KEEP_ALIVE_TIMER_INTERVAL,
                           pSessionInfo->pIkeSA) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreDoPostChildExch:NatKeepAliveTmr:IkeStartTimer failed!\n");
        }
    }
    IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_INDEX_0].IkeNattInfo,
                &pSessionInfo->pIkeSA->IkeNattInfo, sizeof (tIkeNatT));

    IKE_MEMCPY (&IPSecIfParam.IPSecReq.InstallSA,
                &pSessionInfo->Ike2AuthInfo.IPSecBundle, sizeof (tIPSecBundle));

    IPSecIfParam.IPSecReq.InstallSA.u1IkeVersion = IKE2_MAJOR_VERSION;

    if (IkeSendIPSecIfParam (&IPSecIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreDoPostChildExch: IkeSendIPSecIfParam Failed\n");
        return (IKE_FAILURE);
    }

    IncIkeNoOfSessionsSucc (&pSessionInfo->RemoteTunnelTermAddr);

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    IKE_MOD_TRC1 (gu4IkeTrace, CNTRL_PLANE_TRC, "IKE",
                  "IkeQuickMode: "
                  "Successfuly established IPsec SA with peer: %s\n",
                  pu1PeerAddrStr);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                  "IkeQuickMode: Successfuly established IPsec SA with peer: %s\n",
                  pu1PeerAddrStr));

    pSessionInfo->bPhase2Done = IKE_TRUE;

    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreChildExch                                         */
/*  Description   : This function handles the State machine for handling      */
/*                : CREATE_CHILD_SA Exchange                                  */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreChildExch (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    tIkePayload        *pu1NotifyPayload = NULL;
    tIkePayload        *pu1TsPayload = NULL;
    tIke2NotifyPayLoad  NotifyPayload;

    if (pSessionInfo->u1Role == IKE_RESPONDER)
    {
        pu1TsPayload = (tIkePayload *) SLL_FIRST (&pSessionInfo->IkeRxPktInfo.
                                                  aPayload[IKE2_PAYLOAD_TSI -
                                                           IKE2_MINIMUM_PAYLOAD_ID]);
        if (pu1TsPayload != NULL)
        {
            pu1TsPayload =
                (tIkePayload *) SLL_FIRST (&pSessionInfo->IkeRxPktInfo.
                                           aPayload[IKE2_PAYLOAD_TSR -
                                                    IKE2_MINIMUM_PAYLOAD_ID]);
            /*
             * pu1TsPayload will be Valid ( Not NULL ), if both TSI
             * and TSR are present in the Payload.
             * either of TSR or TSI is missing, then pu1TsPayload
             * will be NULL.
             */
        }

        pu1NotifyPayload = (tIkePayload *) SLL_FIRST
            (&pSessionInfo->IkeRxPktInfo.
             aPayload[IKE2_PAYLOAD_NOTIFY - IKE2_MINIMUM_PAYLOAD_ID]);

        if (pu1NotifyPayload != NULL)
        {
            /* There might be more than one notify payload which are stored 
             * last to first. so check all the notify payloads present 
             * in the packet for the N(REKEY_SA) */
            do
            {
                IKE_BZERO (&NotifyPayload, sizeof (tIke2NotifyPayLoad));
                /* Copy the received notify  Payload  */
                IKE_MEMCPY ((UINT1 *) &NotifyPayload,
                            pu1NotifyPayload->pRawPayLoad,
                            sizeof (tIke2NotifyPayLoad));

                if ((IKE_NTOHS (NotifyPayload.u2NotifyMsgType)) ==
                    IKE2_REKEY_SA)
                {
                    /* If a N(REKEY_SA) is found break out of the loop */
                    break;
                }
                pu1NotifyPayload = (tIkePayload *) SLL_NEXT
                    (&pSessionInfo->IkeRxPktInfo.
                     aPayload[IKE2_PAYLOAD_NOTIFY - IKE2_MINIMUM_PAYLOAD_ID],
                     (t_SLL_NODE *) pu1NotifyPayload);
            }
            while (pu1NotifyPayload != NULL);
        }

        /* If there is no N(REKEY_SA) and TS payload, then it is a child exch 
           to rekey an existing IKE SA */
        if ((pu1NotifyPayload == NULL) && (pu1TsPayload == NULL))
        {
            /* Increment the Rekey Stats for the Ike SA */
            IncIkePhase1ReKey ();
            /* * RE KEY an existing IKE SA   */
            pSessionInfo->u1ChildType = IKE2_IPSEC_REKEY_IKESA;
        }
        else if ((pu1NotifyPayload != NULL) && (pu1TsPayload != NULL))
        {
            /* If we received N(REKEY_SA), Tsi & TSr, then the child exchange is
             * for rekeying an existing Child SA */
            /* Increment Phase2 rekey statistics */
            IncIkePhase2ReKey ();
            pSessionInfo->u1ChildType = IKE2_IPSEC_REKEY_CHILD;
        }
        else
        {
            /* Only Tsi and Tsr are present - the child exchange is for 
             * creating a new child SA */
            pSessionInfo->u1ChildType = IKE2_IPSEC_NEW_CHILD;
        }
    }

    if (pSessionInfo->u1ChildType == IKE2_IPSEC_REKEY_IKESA)
    {
        if (Ike2CoreIkeChildFsm (pSessionInfo, pIsakmpHdr) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreChildExch:  Ike2CoreIkeChildFsm Failed \n");
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (Ike2CoreChildFsm (pSessionInfo, pIsakmpHdr) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         " Ike2CoreChildExch:  Ike2CoreChildFsm Failed \n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreIkeChildFsm                                       */
/*  Description   : This function handles the State machine for handling      */
/*                : Child Exchanges for IKE SA Rekey                          */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreIkeChildFsm (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT1              *pu1Payload = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT1               u1NextPayload = IKE2_PAYLOAD_NONE;
    UINT2               u2CurPayloadLen = IKE_ZERO;
    tIkeSA              *pOldIkeSA = NULL;

    /* In case of Responder fill the Session Info and pSessionInfo->bIkeReKeySA 
     * with the IKE Policy Information */
    if (pSessionInfo->u1Role == IKE_RESPONDER)
    {
        TAKE_IKE_SEM ();
        pIkeEngine =
            IkeGetIkeEngineFromIndex (pSessionInfo->pIkeSA->u4IkeEngineIndex);
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "Ike2CoreIkeChildFsm: No IkeEngine Available with "
                          "Index %d\n", pSessionInfo->pIkeSA->u4IkeEngineIndex);
            return (IKE_FAILURE);
        }
        GIVE_IKE_SEM ();

        /* Allocate memory for the New IKE SA and copy the 
         * required information */
        if (Ike2SessCopyIkeSAInfo (pSessionInfo, pIkeEngine) == IKE_FAILURE)
        {
            pSessionInfo->pIkeSA->u4RefCount--;
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2CoreIkeChildFsm: Ike2SessCopyIkeSAInfo"
                         "failed \n");
            return (IKE_FAILURE);
        }

        /* Copy the initiator cookie */
        IKE_MEMCPY (pSessionInfo->pIkeReKeySA->au1InitiatorCookie,
                    pIsakmpHdr->au1InitCookie, IKE_COOKIE_LEN);

        /* We are the responder in this rekey, so set the original
         * initiator flag to FALSE */
        pSessionInfo->pIkeReKeySA->Ike2SA.bIsOrgInitiator = IKE_FALSE;
    }

    switch (pSessionInfo->u1FsmState)
    {
        case IKE2_CHILD_IDLE:
        {
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
    /*Incase of Responder if alredy one IKE Active session is exist stop the Re-key of that IKE session and start Delete Timer
     * since one new IKE session is initiated from other side, having the OLD IKE SAs will result in multiple Re-Keying*/
     SLL_SCAN (&pIkeEngine->IkeSAList, pOldIkeSA, tIkeSA *)
      {
         if(pSessionInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
         {
             if (pOldIkeSA->IpPeerAddr.Ipv4Addr == pSessionInfo->pIkeSA->IpPeerAddr.Ipv4Addr)
             {
                 IkeStopTimer(&pOldIkeSA->IkeSALifeTimeTimer);
                 if (IkeStartTimer (&pOldIkeSA->IkeSALifeTimeTimer,
                                     IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                                     pOldIkeSA) != IKE_SUCCESS)
                 {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SessChildExchSessInit: IkeStartTimer failed!\n");
                    MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                                (UINT1 *) pSessionInfo);
                     return (IKE_FAILURE);
                 }                                     
             }
         }
        }                 
                if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                 (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                  sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                                 " Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }
            }

            /* Construct IKEv2 (ISAKMP) Header */
            if (Ike2ConstructHeader (pSessionInfo, IKE2_CHILD_EXCH,
                                     IKE2_PAYLOAD_SA) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                             "Ike2ConstructHeader Failed\n");
                return (IKE_FAILURE);
            }
            /* Construct IKE SA Payload */
            if (Ike2ConstructSA (pSessionInfo, IKE2_PAYLOAD_NONCE, &u4Len)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                             "Ike2ConstructSA Failed\n");
                return (IKE_FAILURE);
            }

            /* Construct Nonce PayLoad */
            if (Ike2ConstructNonce (pSessionInfo, IKE2_PAYLOAD_KE,
                                    &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                             "Ike2ConstructNonce Failed\n");
                return (IKE_FAILURE);
            }

            /* Construct Key Exchange PayLoad */
            if (Ike2ConstructKE (pSessionInfo, IKE2_PAYLOAD_NONE,
                                 &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                             " Ike2ConstructKE Failed\n");
                return (IKE_FAILURE);
            }
            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            /* If responder, generate the Skey ID for IKE */
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (Ike2KeyGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE "
                                 "Ike2KeyGenSkeyId Failed\n");
                    IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.
                                    pu1SaInitReqPkt);
                    return (IKE_FAILURE);
                }
                pSessionInfo->bPhase1Done = IKE_TRUE;
                pSessionInfo->pIkeReKeySA->Ike2SA.bAuthDone = IKE_TRUE;
                pSessionInfo->u1FsmState = IKE2_CHILD_DONE;
            }
            else
            {
                pSessionInfo->u1FsmState = IKE2_CHILD_WAIT;
                pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;

                if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                   IKE_RETRANS_TIMER_ID,
                                   IKE_RETRANSMIT_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* This is not critical error, so just log and don't
                     *                        return failure
                     *                                             */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreIkeChildFsm: State IKE2_CHILD_IDLE"
                                 "IkeStartTimer Failed\n");
                }
            }
            break;
        }
        case IKE2_CHILD_WAIT:
        {
            /* This state is only for the INITIATOR. */
            u1NextPayload =
                ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.
                                          pu1RawPkt))->u1NextPayload;
            pu1Payload =
                (pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr));

            /* If the Next payload is encrypted, The payload is already 
             * decrypted, so now just skip the encrypted payload and move
             * to the next one */
            if (u1NextPayload == IKE2_PAYLOAD_ENCRYPTED)
            {
                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;
            }

            if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                /* Auth Req Failed , peer has sent Notify to intimate 
                 * the failure,  No Need to send a notify for a notify , 
                 * so no error checking. */
                Ike2ProcessNotify (pSessionInfo, pu1Payload, IKE2_NONE);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: IKE2_CHILD_WAIT "
                             "Received Notify Payload as CHILD Response \n");
                return (IKE_FAILURE);
            }
            else if (u1NextPayload == IKE2_PAYLOAD_SA)
            {
                if (Ike2Process (IKE2_PAYLOAD_SA, pSessionInfo,
                                 (pu1Payload)) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreIkeChildFsm: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }
                if (Ike2KeyGenSkeyId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreIkeChildFsm: State IKE2_CHILD_WAIT "
                                 "Ike2KeyGenSkeyId Failed\n");
                    IKE_BUDDY_FREE (pSessionInfo->pIkeSA->Ike2SA.
                                    pu1SaInitReqPkt);
                    return (IKE_FAILURE);
                }
                pSessionInfo->bPhase1Done = IKE_TRUE;
                pSessionInfo->pIkeReKeySA->Ike2SA.bAuthDone = IKE_TRUE;
                pSessionInfo->u1FsmState = IKE2_CHILD_DONE;
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreIkeChildFsm: Invalid Payload received\n");
                return (IKE_FAILURE);
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreIkeChildFsm:  Invalid State \n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreChildFsm                                          */
/*  Description   : This function handles the State machine for handling      */
/*                : Child Exchanges for REKEY_CHILD/NEW_CHILD SA              */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreChildFsm (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT1              *pu1Payload = NULL;
    tIke2NotifyPayLoad *pu1NotifyPayload = NULL;
    tIkeNotifyInfo      NotifyInfo;
    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT4               u4Spi = IKE_ZERO;
    UINT2               u2CurPayloadLen = IKE_ZERO;
    UINT2               u2NotifyMsgType = IKE_ZERO;
    UINT1               u1NextPayload = IKE2_PAYLOAD_NONE;
    BOOLEAN             bTransModeReqd = IKE_ZERO;

    switch (pSessionInfo->u1FsmState)
    {
        case IKE2_CHILD_IDLE:
        {
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {

                /*  In the case of responder, we have not yet generated
                 *  the SPI (new spi for inbound traffic) information, generate 
                 *  and copy it into the  session structure. */

                IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
                pSessionInfo->Ike2AuthInfo.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4Spi = u4Spi;

                IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
                pSessionInfo->Ike2AuthInfo.IPSecBundle.
                    aInSABundle[IKE_ESP_SA_INDEX].u4Spi = u4Spi;

                /* Copy the local and remote tunnel termination address */
                IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.
                            LocTunnTermAddr, &pSessionInfo->pIkeSA->IpLocalAddr,
                            sizeof (tIkeIpAddr));
                IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.
                            RemTunnTermAddr, &pSessionInfo->pIkeSA->IpPeerAddr,
                            sizeof (tIkeIpAddr));

                /* If we are the responder, The Cryptomap policy to
                 * be used is to be selected by processing the TS /IDi payload
                 * and the sessionInfo structure is to be updated with the
                 * Cryptomap details */
                if (Ike2ProcessTsId (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm: state IKE2_CHILD_IDLE "
                                 "Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }

                if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                 (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                  sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm: state IKE2_CHILD_IDLE "
                                 "Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }

                /* If we are configured to use Transport Mode and Peer has
                 * sent N(USE_TRANSPORT_MODE), then Send N(INVALID_SYNTAX) */

                if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                    IKE_IPSEC_TRANSPORT_MODE)
                {
                    if (pSessionInfo->Ike2AuthInfo.bTransportReqd != IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm: N(USE_TRANSPORT_MODE) "
                                     "payload, not found in packet, when local "
                                     "configured is to use transport mode\n");
                        return (IKE_FAILURE);
                    }
                }
                /* We are configured to use TUNNEL mode and peer has sent a
                 * N(USE_TRANSPORT_MODE), send */
                if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                    IKE_IPSEC_TUNNEL_MODE)
                {
                    if (pSessionInfo->Ike2AuthInfo.bTransportReqd == IKE_TRUE)
                    {
                        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm: N(USE_TRANSPORT_MODE) "
                                     "payload,  found in packet, when local "
                                     "configuration is to use tunnel mode\n");
                        return (IKE_FAILURE);
                    }
                }
            }
            /* Construct the payloads for Child SA */
            u1NextPayload = IKE2_PAYLOAD_SA;

            if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                IKE_IPSEC_TRANSPORT_MODE)
            {
                bTransModeReqd = IKE_TRUE;
            }

            if ((pSessionInfo->u1ChildType == IKE2_IPSEC_REKEY_CHILD) ||
                (bTransModeReqd == IKE_TRUE))
            {
                /* transport case-next payload is N(USE_TRANSPORT_MODE) */
                u1NextPayload = IKE2_PAYLOAD_NOTIFY;
            }

            /* Construct IKEv2 (ISAKMP) Header */
            if (Ike2ConstructHeader (pSessionInfo, IKE2_CHILD_EXCH,
                                     u1NextPayload) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  Ike2ConstructHeader Failed\n");
                return (IKE_FAILURE);
            }

            if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                IKE_BZERO (&NotifyInfo, sizeof (tIkeNotifyInfo));
                if (pSessionInfo->u1ChildType == IKE2_IPSEC_REKEY_CHILD)
                {
                    NotifyInfo.u1Protocol =
                        pSessionInfo->Ike2AuthInfo.u1Protocol;
                    NotifyInfo.u2NotifyType = IKE2_REKEY_SA;
                    NotifyInfo.u1SpiSize = IKE_IPSEC_SPI_SIZE;
                    NotifyInfo.u2NotifyDataLen = IKE_IPSEC_SPI_SIZE;

                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &NotifyInfo.pu1NotifyData) ==
                        MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (NotifyInfo.pu1NotifyData == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                                     "Malloc failed for Notify Data (SPI)  \n");
                        return (IKE_FAILURE);
                    }
                    IKE_MEMCPY (NotifyInfo.pu1NotifyData,
                                &(pSessionInfo->Ike2AuthInfo.u4ReKeyingSpi),
                                IKE_IPSEC_SPI_SIZE);
                    /* Check what should be the next payload */
                    u1NextPayload = IKE2_PAYLOAD_SA;
                    if (bTransModeReqd == IKE_TRUE)
                    {
                        u1NextPayload = IKE2_PAYLOAD_NOTIFY;
                    }
                    if (Ike2ConstructNotify (pSessionInfo, u1NextPayload,
                                             &u4Len,
                                             &NotifyInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                                     "Ike2ConstructNotify \n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) NotifyInfo.pu1NotifyData);
                        return (IKE_FAILURE);
                    }
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) NotifyInfo.pu1NotifyData);
                }
                if (bTransModeReqd == IKE_TRUE)
                {
                    IKE_BZERO (&NotifyInfo, sizeof (tIkeNotifyInfo));
                    NotifyInfo.u1Protocol =
                        pSessionInfo->Ike2AuthInfo.u1Protocol;
                    NotifyInfo.u2NotifyType = IKE2_USE_TRANSPORT_MODE;
                    NotifyInfo.u1SpiSize = IKE_ZERO;
                    NotifyInfo.u2NotifyDataLen = IKE_ZERO;

                    if (Ike2ConstructNotify (pSessionInfo, IKE2_PAYLOAD_SA,
                                             &u4Len,
                                             &NotifyInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                                     "Ike2ConstructNotify \n");
                        return (IKE_FAILURE);
                    }
                }
            }

            /* Construct IPSec SA Payload */
            if (Ike2ConstructIpsecSA (pSessionInfo, IKE2_PAYLOAD_NONCE,
                                      &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                             "Ike2ConstructIpsecSA Failed\n");
                return (IKE_FAILURE);
            }

            u1NextPayload = IKE2_PAYLOAD_TSI;
            if (pSessionInfo->Ike2IPSecPolicy.u1Pfs != IKE_ZERO)
            {
                u1NextPayload = IKE2_PAYLOAD_KE;
            }

            if (Ike2ConstructNonce (pSessionInfo, u1NextPayload,
                                    &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                             "Ike2ConstructNonce Failed\n");
                return (IKE_FAILURE);

            }
            if (u1NextPayload == IKE2_PAYLOAD_KE)
            {
                if (Ike2ConstructKE (pSessionInfo, IKE2_PAYLOAD_TSI,
                                     &u4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                                 "Ike2ConstructKE Failed\n");
                    return (IKE_FAILURE);
                }
            }

            /* Construct the Traffic Selector at the initiators end (TSi) */
            if (Ike2ConstructTSi (pSessionInfo, IKE2_PAYLOAD_TSR,
                                  &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                             "Ike2ConstructTSi Failed\n");
                return (IKE_FAILURE);
            }
            /* Construct the Traffic Selector at the responders end (TSr) */
            if (Ike2ConstructTSr (pSessionInfo, IKE2_PAYLOAD_NONE,
                                  &u4Len) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                             "Ike2ConstructTSr Failed\n");
                return (IKE_FAILURE);
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                /* Generate the IPSec keys and intimiate to IPsec to Store
                   the SA etc. */
                if (Ike2CoreDoPostChildExch (pSessionInfo) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm:  IKE2_CHILD_IDLE "
                                 "Ike2CoreDoPostChildExch Failed\n");
                    return (IKE_FAILURE);
                }
                pSessionInfo->u1FsmState = IKE2_CHILD_DONE;
                pSessionInfo->bPhase2Done = IKE_TRUE;
            }
            else
            {
                pSessionInfo->u1FsmState = IKE2_CHILD_WAIT;
                pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
                if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                   IKE_RETRANS_TIMER_ID,
                                   IKE_RETRANSMIT_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* This is not critical error, so just log and don't
                     *                    return failure */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm: IKE2_CHILD_IDLE "
                                 "IkeStartTimer Failed\n");
                }
            }
            break;
        }
        case IKE2_CHILD_WAIT:
        {
            /* This state is only for the INITIATOR. */
            /* Check if the first payload is Hash Payload */
            u1NextPayload =
                ((tIsakmpHdr *) (void *) (pSessionInfo->IkeRxPktInfo.
                                          pu1RawPkt))->u1NextPayload;
            pu1Payload =
                (pSessionInfo->IkeRxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr));

            /* If the Next payload is encrypted, The payload is already
             * decrypted, so now just skip the encrypted payload and move
             * to the next one */
            if (u1NextPayload == IKE2_PAYLOAD_ENCRYPTED)
            {
                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;
            }

            while (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
            {
                pu1NotifyPayload = (tIke2NotifyPayLoad *) (void *) pu1Payload;
                u2NotifyMsgType = IKE_NTOHS (pu1NotifyPayload->u2NotifyMsgType);

                if ((u2NotifyMsgType == IKE2_NO_PROPOSAL_CHOSEN) ||
                    (u2NotifyMsgType == IKE2_TS_UNACCEPTABLE) ||
                    (u2NotifyMsgType == IKE2_SINGLE_PAIR_REQUIRED))
                {
                    /* In this case, the negotiation cannot proceed, unless, the
                     * configuration is changed for the mismatching payload */
                    Ike2ProcessNotify (pSessionInfo, pu1Payload, IKE2_NONE);
                    IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                  "Ike2CoreChildFsm: IKE2_CHILD_WAIT - "
                                  "Received Notify Payload with type (%d) "
                                  "(NO_PROPOSAL_CHOOSEN or TS_UNACCEPTABLE) "
                                  "received in packet \n", u2NotifyMsgType);
                    return (IKE_FAILURE);
                }
                /* If Ra-VPN Client, then see if we got a CP_ */
                /* Rest of the Notify Payloads like IPCOMP_SUPPORTED 
                 * etc can be ignored */
                if (Ike2ProcessNotify (pSessionInfo, pu1Payload,
                                       IKE_ZERO) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm: Ike2Process Failed \n");
                    return (IKE_FAILURE);
                }
                Ike2UtilGetNextPayloadAndType (pu1Payload, &u1NextPayload,
                                               &u2CurPayloadLen);
                pu1Payload = pu1Payload + u2CurPayloadLen;
            }

            /* Process rest of the payloads */
            if (Ike2Process (u1NextPayload, pSessionInfo,
                             pu1Payload) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm: Ike2Process Failed \n");
                return (IKE_FAILURE);
            }
            /* If we are configured to use Transport Mode and Peer has
             * not sent N(USE_TRANSPORT_MODE), then Send N(INVALID_SYNTAX) */
            if ((pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                 IKE_IPSEC_TRANSPORT_MODE) &&
                (pSessionInfo->Ike2AuthInfo.bTransportReqd != IKE_TRUE))
            {
                IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm: N(USE_TRANSPORT_MODE) "
                             "payload, not found in packet, when local "
                             "configured is to use transport mode\n");
                return (IKE_FAILURE);
            }

            /* We are configured to use TUNNEL mode and peer has sent a
             * N(USE_TRANSPORT_MODE), send */
            if (pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode ==
                IKE_IPSEC_TUNNEL_MODE)
            {
                if (pSessionInfo->Ike2AuthInfo.bTransportReqd == IKE_TRUE)
                {
                    IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreChildFsm: State IKE2_CHILD_WAIT "
                                 "N(USE_TRANSPORT_MODE) payload,  found in "
                                 "packet, when local configuration is to use "
                                 "tunnel mode\n");
                    return (IKE_FAILURE);
                }
            }
            /* Store Child SA and intimate IPsec to store the IPSec SA */
            if (Ike2CoreDoPostChildExch (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreChildFsm:  State IKE2_CHILD_WAIT "
                             "Ike2CoreDoPostChildExch Failed\n");
                return (IKE_FAILURE);
            }
            pSessionInfo->u1FsmState = IKE2_CHILD_DONE;
            pSessionInfo->bPhase2Done = IKE_TRUE;
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreChildFsm:  Invalid State \n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2CoreInfoExch                                          */
/*  Description   : This function handles the State machine for handling      */
/*                : IKE_INFORMATIONAL Exchanges                               */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : pIsakmpHdr   - Pointer to the IKE Header                  */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     :  pSessionInfo is updated with the new fsm state.          */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2CoreInfoExch (tIkeSessionInfo * pSessionInfo, tIsakmpHdr * pIsakmpHdr)
{
    UINT4               u4Len = sizeof (tIsakmpHdr);
    UINT1               u1NextPayload = IKE2_PAYLOAD_NONE;
    tIkeNotifyInfo      NotifyInfo;
    tIkeNotifyInfo     *pNotifyInfo = NULL;

    IKE_BZERO (&NotifyInfo, sizeof (tIkeNotifyInfo));

    switch (pSessionInfo->u1FsmState)
    {
        case IKE2_INFO_IDLE:
        {
            if (pSessionInfo->u1Role == IKE_RESPONDER)
            {
                if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                                 (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                                  sizeof (tIsakmpHdr))) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInfoExch: Ike2Process Failed \n");
                    pSessionInfo->bDelSession = IKE_TRUE;
                    return (IKE_FAILURE);
                }
            }
            else
            {
                if (pSessionInfo->PostPhase1Info.IkeNotifyInfo.u2NotifyType ==
                    IKE_POST_P1_DELETE)
                {
                    u1NextPayload = IKE2_PAYLOAD_DELETE;
                }
                else
                {
                    u1NextPayload = IKE2_PAYLOAD_NOTIFY;
                }
            }

            if (Ike2ConstructHeader
                (pSessionInfo, IKE2_INFO_EXCH, u1NextPayload) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInfoExch: Ike2ConstructHeader Failed \n");
                pSessionInfo->bDelSession = IKE_TRUE;
                return (IKE_FAILURE);
            }
            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                if (u1NextPayload == IKE2_PAYLOAD_DELETE)
                {
                    IKE_MEMCPY (&NotifyInfo,
                                &(pSessionInfo->PostPhase1Info.IkeNotifyInfo),
                                sizeof (tIkeNotifyInfo));

                    /* Construct the Delete SA Payload */
                    if (Ike2ConstructDelete (pSessionInfo, IKE2_PAYLOAD_NONE,
                                             &u4Len,
                                             &NotifyInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInfoExch:  IKE2_INFO_IDLE "
                                     "Ike2ConstructDelete failed\n");
                        pSessionInfo->bDelSession = IKE_TRUE;
                        return (IKE_FAILURE);
                    }
                    if (pSessionInfo->PostPhase1Info.IkeNotifyInfo.u1Protocol ==
                        IKE_ISAKMP)
                    {
                        /* Stop the Lifetime timer if already running and 
                         * start a 1 second timer to delete IKE SA.
                         * This is done to sent reply to the peer */
                        IkeStopTimer (&pSessionInfo->pIkeSA->
                                      IkeSALifeTimeTimer);
                        if (IkeStartTimer
                            (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                             IKE_DELETESA_TIMER_ID,
                             (IKE_TIMER_COUNT * IKE_RETRANSMIT_INTERVAL),
                             pSessionInfo->pIkeSA) != IKE_SUCCESS)
                        {
                            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                         "Ike2CoreInfoExch: "
                                         "IkeStartTimer failed\n");
                            return (IKE_FAILURE);
                        }
                        pSessionInfo->pIkeSA->bInfoDelSent = IKE_TRUE;
                    }
                    /* Release the notify Data */
                    if (pSessionInfo->PostPhase1Info.IkeNotifyInfo.
                        pu1NotifyData != NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pSessionInfo->
                                            PostPhase1Info.IkeNotifyInfo.
                                            pu1NotifyData);
                        pSessionInfo->PostPhase1Info.IkeNotifyInfo.
                            pu1NotifyData = NULL;
                    }
                }
                else if (u1NextPayload == IKE2_PAYLOAD_NOTIFY)
                {
                    pNotifyInfo = &pSessionInfo->PostPhase1Info.IkeNotifyInfo;
                    if (Ike2ConstructNotify (pSessionInfo, IKE2_PAYLOAD_NONE,
                                             &u4Len,
                                             pNotifyInfo) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CoreInfoExch:  IKE2_INFO_IDLE "
                                     "Ike2ConstructNotify failed\n");
                        pSessionInfo->bDelSession = IKE_TRUE;
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInfoExch:  IKE2_INFO_IDLE "
                                 "Invalid Info Payload type \n");
                    pSessionInfo->bDelSession = IKE_TRUE;
                    return (IKE_FAILURE);
                }
            }

            /* Update Packet total length in the Isakmp hdr */
            IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);

            u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);

            ((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
                u4TotalLen = u4Len;

            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                pSessionInfo->u1FsmState = IKE2_INFO_WAIT;

                pSessionInfo->u1ReTransCount = IKE_RETRANSMIT_COUNT;
                if (IkeStartTimer (&pSessionInfo->IkeRetransTimer,
                                   IKE_RETRANS_TIMER_ID,
                                   IKE_RETRANSMIT_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CoreInfoExch: IKE2_INFO_IDLE "
                                 "IkeStartTimer Failed\n");
                    return (IKE_FAILURE);
                }
            }
            else
            {
                pSessionInfo->pIkeSA->bInfoDelSent = IKE_TRUE;
                pSessionInfo->bInfoDone = IKE_TRUE;
            }
            break;
        }
        case IKE2_INFO_WAIT:
        {
            /* See if this is the response message, received for 
             * the IKE SA delete request sent by us, If yes, just return.*/
            if (Ike2Process (pIsakmpHdr->u1NextPayload, pSessionInfo,
                             (pSessionInfo->IkeRxPktInfo.pu1RawPkt +
                              sizeof (tIsakmpHdr))) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CoreInfoExch: Ike2Process Failed \n");
                pSessionInfo->bDelSession = IKE_TRUE;
                return (IKE_FAILURE);
            }

            pSessionInfo->bInfoDone = IKE_TRUE;
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CoreInfoExch: State invalid "
                         "session in invalid State \n");
            pSessionInfo->bDelSession = IKE_TRUE;
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

#endif
