
/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/* $Id: ike2cfg.c,v 1.4 2013/12/16 15:27:57 siva Exp $             */
/*                                                                    */
/*  Description:This file contains functions for ravpn config mode    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_CFG_C_
#define _IKE2_CFG_C_

#include "ikegen.h"
#include "ike2inc.h"

PRIVATE INT1
    Ike2CfgSetCMRepCheckAtts PROTO ((tIkeSessionInfo *, UINT4, UINT2, UINT2,
                                     UINT1 *, UINT2));

/************************************************************************/
/*  Function Name   :Ike2CfgSetCMReqAttributes                          */
/*  Description     :This function is used to set CM Req parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the session info data base */
/*                  :ppu1Atts   - Pointer refering to the pointer       */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen - Pointer to the payload len            */
/*                  :pi4Len     - Pointer to the bytes added            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Req attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2CfgSetCMReqAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                           INT4 *pi4AttsLen, INT4 *pi4Len)
{
    UINT1               u1Value = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->Ike2AuthInfo.IkeRAInfo.CMClientInfo.
            au2ConfiguredAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->Ike2AuthInfo.IkeRAInfo.CMClientInfo.
                au2ConfiguredAttr[u1Index])
        {
            case IKE_INTERNAL_IP4_ADDRESS:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP4_ADDRESS,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMReqAttributes: Unable to set"
                                 "Var Att IP address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP4_NETMASK:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP4_NETMASK,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMReqAttributes: Unable to set"
                                 "Var Att IP address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP6_ADDRESS:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMReqAttributes: Unable to set"
                                 "Var Att IP address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP6_NETMASK:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMReqAttributes: Unable to set"
                                 "Var Att IP address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_SUPPORTED_ATTRIBUTES:
            {
                if (VpiAtt
                    (IKE_SUPPORTED_ATTRIBUTES,
                     (UINT1 *) &u1Value, IKE_ZERO,
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMReqAttributes: Unable to set"
                                 "Var Att Supported attr\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            default:
                break;
        }
        u1Index++;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2CfgSetCMRepAttributes                          */
/*  Description     :This function is used to set CM Rep parameters     */
/*                  :in the form of AVP or AVPL based on their type     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the session info data base */
/*                  :ppu1Atts   - Pointer refering to the pointer       */
/*                  :pointing to the payload                            */
/*                  :pi4AttsLen - Pointer to the payload len            */
/*                  :pi4Len     - Pointer to the bytes added            */
/*                  :                                                   */
/*  Output(s)       :Constructs the CM Rep attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2CfgSetCMRepAttributes (tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                           INT4 *pi4AttsLen, INT4 *pi4Len)
{
    UINT1               au1Network[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1               u1Index = IKE_ZERO;
    UINT4               u4Address = IKE_ZERO;

    IKE_BZERO (au1Network, IKE_MAX_ADDR_STR_LEN);

    while ((u1Index < IKE_MAX_SUPPORTED_ATTR) &&
           (pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index] != IKE_ZERO))
    {
        switch (pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index])
        {

            case IKE_SUPPORTED_ATTRIBUTES:
            {
                if (pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr.u4AddrType == IPV4ADDR)
                {
                    u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        InternalIpAddr.Ipv4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    if (VpiAtt
                        (IKE_INTERNAL_IP4_ADDRESS,
                         (UINT1 *) &(u4Address), sizeof (UINT4),
                         ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CfgSetCMRepAttributes: Unable to set"
                                     "Var Att\n");
                        return (IKE_FAILURE);
                    }
                    u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        InternalMask.Ipv4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    if (VpiAtt (IKE_INTERNAL_IP4_NETMASK,
                                (UINT1 *) &(u4Address), sizeof (UINT4),
                                ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CfgSetCMRepAttributes: Unable to "
                                     "set Var Att\n");
                        return (IKE_FAILURE);
                    }
                    u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.
                        Ip4Addr;
                    u4Address = IKE_HTONL (u4Address);
                    IKE_MEMCPY (au1Network, &(u4Address), sizeof (UINT4));
                    u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.
                        Ip4SubnetMask;
                    u4Address = IKE_HTONL (u4Address);
                    IKE_MEMCPY ((au1Network + sizeof (UINT4)), &(u4Address),
                                sizeof (UINT4));
                    if (VpiAtt
                        (IKE_INTERNAL_IP4_SUBNET,
                         (UINT1 *) au1Network, IKE2_VPI_ATTR_LEN, ppu1Atts,
                         pi4Len, pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CfgSetCMRepAttributes: Unable to set "
                                     "Var Att\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    if (VpiAtt
                        (IKE_INTERNAL_IP6_ADDRESS,
                         (UINT1 *) &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                     CMServerInfo.InternalIpAddr.Ipv6Addr),
                         sizeof (tIp6Addr), ppu1Atts, pi4Len,
                         pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CfgSetCMRepAttributes: Unable to "
                                     "set Var Att\n");
                        return (IKE_FAILURE);
                    }
                    if (VpiAtt
                        (IKE_INTERNAL_IP6_NETMASK,
                         (UINT1 *) &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                     CMServerInfo.InternalMask.Ipv6Addr),
                         sizeof (tIp6Addr), ppu1Atts, pi4Len,
                         pi4AttsLen) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2CfgSetCMRepAttributes: Unable to "
                                     "set Var Att\n");
                        return (IKE_FAILURE);
                    }
                }
                return (IKE_SUCCESS);
            }
            case IKE_INTERNAL_IP4_ADDRESS:
            {
                u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                    InternalIpAddr.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_ADDRESS,
                     (UINT1 *) &(u4Address), sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMRepAttributes: Unable to "
                                 "set Var Att IPv4 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP4_NETMASK:
            {
                u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                    InternalMask.Ipv4Addr;
                u4Address = IKE_HTONL (u4Address);
                if (VpiAtt
                    (IKE_INTERNAL_IP4_NETMASK,
                     (UINT1 *) &(u4Address), sizeof (UINT4),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMRepAttributes: Unable to "
                                 "set Var Att IPv4 Netmask\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP4_SUBNET:
            {
                u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.Ip4Addr;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY (au1Network, &(u4Address), sizeof (UINT4));
                u4Address = pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                    ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.
                    Ip4SubnetMask;
                u4Address = IKE_HTONL (u4Address);
                IKE_MEMCPY ((au1Network + sizeof (UINT4)), &(u4Address),
                            sizeof (UINT4));
                if (VpiAtt
                    (IKE_INTERNAL_IP4_SUBNET,
                     (UINT1 *) au1Network, IKE2_VPI_ATTR_LEN, ppu1Atts, pi4Len,
                     pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMRepAttributes: Unable to set "
                                 "Var Att IPv4 Subnet\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP6_ADDRESS:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_ADDRESS,
                     (UINT1 *) &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                 CMServerInfo.InternalIpAddr.Ipv6Addr),
                     sizeof (tIp6Addr), ppu1Atts, pi4Len,
                     pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMRepAttributes: Unable to "
                                 "set Var Att IPv6 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            case IKE_INTERNAL_IP6_NETMASK:
            {
                if (VpiAtt
                    (IKE_INTERNAL_IP6_NETMASK,
                     (UINT1 *) &(pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                                 InternalMask.Ipv6Addr), sizeof (tIp6Addr),
                     ppu1Atts, pi4Len, pi4AttsLen) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgSetCMRepAttributes: Unable to "
                                 "set Var Att IPv6 Netmask\n");
                    return (IKE_FAILURE);
                }
                break;
            }
            default:
                break;
        }
        u1Index++;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2CfgCheckCMReqAttr                              */
/*  Description     :This functions is used to check the attributes of  */
/*                  :CM Req or Ack                                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to the Session Info           */
/*                  :pu1PayLoad- The current payload to be processed.   */
/*                  :u4Len - Length of the payload                      */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2CfgCheckCMReqAttr (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                       UINT4 u4Len)
{
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO;
    UINT2               u2BasicValue = IKE_ZERO;
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    while ((u4Count < u4Len) && (u1Index < IKE_MAX_SUPPORTED_ATTR))
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CfgCheckCMReqAttr: IkeGetAtts failed!\n");
            return (IKE_FAILURE);
        }
        switch (u2Attr)
        {
            case IKE_SUPPORTED_ATTRIBUTES:
            {
                if ((UINT2) i4Len != IKE_ZERO)
                {

                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2CfgCheckCMReqAttr: Length of the "
                                 "supported attribute must be zero\n");
                    return (IKE_FAILURE);
                }
                pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index] =
                    IKE_SUPPORTED_ATTRIBUTES;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_ADDRESS:
            {
                pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_ADDRESS;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_NETMASK:
            {
                pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_NETMASK;
                u1Index++;
                break;
            }

            case IKE_INTERNAL_IP4_SUBNET:
            {
                pSesInfo->Ike2AuthInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP4_SUBNET;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP6_ADDRESS:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP6_ADDRESS;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP6_NETMASK:
            {
                pSesInfo->IkeTransInfo.au2CMRecvAttr[u1Index] =
                    IKE_INTERNAL_IP6_NETMASK;
                u1Index++;
                break;
            }
            case IKE_INTERNAL_IP4_DNS:
            case IKE_INTERNAL_ADDRESS_EXPIRY:
            case IKE_INTERNAL_IP4_DHCP:
            case IKE_APPLICATION_VERSION:
            case IKE_INTERNAL_IP6_DNS:
            case IKE_INTERNAL_IP6_DHCP:
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2CfgCheckCMReqAttr: Unsupported "
                             "Attributes Discarded \n");
                break;
            }
        }

        u4Count =
            (UINT4) (u4Count + (UINT4) sizeof (tSaBasicAttribute) +
                     (UINT4) i4Len);
        pu1PayLoad =
            (UINT1 *) (pu1PayLoad + (UINT1) sizeof (tSaBasicAttribute) +
                       (UINT1) i4Len);
    }
    if (u4Count != u4Len)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CfgCheckCMReqAttr: Config payload " "is invalid!\n");
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2CfgCheckCMCfgRepAttr                           */
/*  Description     :This functions is used to check the attributes of  */
/*                  :CM                                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  - Pointer to the Session Info DataBase   */
/*                  :pu1PayLoad- The current payload to be processed.   */
/*                  :u4Len - Length of the payload                      */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2CfgCheckCMCfgRepAttr (tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                          UINT4 u4Len)
{
    UINT4               u4Count = IKE_ZERO;
    UINT2               u2Attr = IKE_ZERO;
    UINT2               u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT4               u4AttrType = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    BOOLEAN             bRecdInternalIpAddress = FALSE;

    while (u4Count < u4Len)
    {
        i4Len = IkeGetAtts (pu1PayLoad, &u4AttrType, &u2Attr,
                            &u2BasicValue, au1VarAtts);
        if (i4Len == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CfgCheckCMCfgRepAttr: IkeGetAtts failed!\n");
            return (IKE_FAILURE);
        }

        if (Ike2CfgSetCMRepCheckAtts (pSesInfo, u4AttrType,
                                      u2Attr, u2BasicValue,
                                      au1VarAtts, (UINT2) i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CfgCheckCMCfgRepAttr: IkeCMCfgRepOrSetCheckAtts"
                         "failed!\n");
            return (IKE_FAILURE);
        }
        if (u2Attr == IKE_INTERNAL_IP4_ADDRESS)
        {
            bRecdInternalIpAddress = TRUE;
        }
        u4Count =
            (UINT4) (u4Count + (UINT4) sizeof (tSaBasicAttribute) +
                     (UINT4) i4Len);
        pu1PayLoad =
            (UINT1 *) (pu1PayLoad + (UINT1) sizeof (tSaBasicAttribute) +
                       (UINT1) i4Len);
    }

    if (u4Count != u4Len)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CfgCheckCMCfgRepAttr: "
                     "Config payload is invalid!\n");
        return (IKE_FAILURE);
    }

    if (bRecdInternalIpAddress != TRUE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CfgCheckCMCfgRepAttr: "
                     "Mandatory CM Attributes not received!\n");
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2CfgSetCMRepCheckAtts                           */
/*  Description     :This functions is used to set the attributes of    */
/*                  :CM Cfg Reply                                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  - Pointer to the Session Info DataBase   */
/*                  :u4AttrType- Indicates attribute type(Basic or VPI  */
/*                  :u2Attr    - Specifies the Attribute                */
/*                  :u2Value   - Value of the Attribute                 */
/*                  :pu1VarPtr - Stores the Variable attributes         */
/*                  :u2Len     - Length of the attribute                */
/*                  :                                                   */
/*  Output(s)       :Updates session data structure with the Received   */
/*                  :Attributes                                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2CfgSetCMRepCheckAtts (tIkeSessionInfo * pSessionInfo, UINT4 u4AttrType,
                          UINT2 u2Attr, UINT2 u2Value, UINT1 *pu1VarPtr,
                          UINT2 u2Len)
{
    UINT1               u1Count = IKE_ZERO;
    UINT1               u1Count1 = IKE_ZERO;

    IKE_UNUSED (u2Len);
    IKE_UNUSED (u4AttrType);
    IKE_UNUSED (u2Value);

    switch (u2Attr)
    {
        case IKE_INTERNAL_IP4_ADDRESS:
        {
            pSessionInfo->Ike2AuthInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_ADDRESS;
            pSessionInfo->Ike2AuthInfo.InternalIpAddr.Ipv4Addr =
                IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->Ike2AuthInfo.InternalIpAddr.u4AddrType = IPV4ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_NETMASK:
        {
            pSessionInfo->Ike2AuthInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_NETMASK;
            pSessionInfo->Ike2AuthInfo.InternalMask.Ipv4Addr =
                IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->Ike2AuthInfo.InternalMask.u4AddrType = IPV4ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_SUBNET:
        {
            pSessionInfo->Ike2AuthInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP4_SUBNET;
            pSessionInfo->Ike2AuthInfo.ProtectedNetwork[u1Count1].uNetwork.
                Ip4Subnet.Ip4Addr = IKE_NTOHL ((*(UINT4 *) (void *) pu1VarPtr));
            pSessionInfo->Ike2AuthInfo.ProtectedNetwork[u1Count1].uNetwork.
                Ip4Subnet.Ip4SubnetMask =
                IKE_NTOHL ((*(UINT4 *) (void *) (pu1VarPtr + sizeof (UINT4))));
            pSessionInfo->Ike2AuthInfo.ProtectedNetwork[u1Count1].u4Type =
                IKE_IPSEC_ID_IPV4_SUBNET;

            u1Count++;
            u1Count1++;
            break;
        }
        case IKE_INTERNAL_IP6_ADDRESS:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP6_ADDRESS;
            IKE_MEMCPY (&(pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv6Addr),
                        pu1VarPtr, sizeof (tIp6Addr));
            pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType = IPV6ADDR;
            u1Count++;
            break;
        }
        case IKE_INTERNAL_IP6_NETMASK:
        {
            pSessionInfo->IkeTransInfo.au2CMAcptAttr[u1Count] =
                IKE_INTERNAL_IP6_NETMASK;
            IKE_MEMCPY (&(pSessionInfo->IkeTransInfo.InternalMask.Ipv6Addr),
                        pu1VarPtr, sizeof (tIp6Addr));
            pSessionInfo->IkeTransInfo.InternalMask.u4AddrType = IPV6ADDR;
            u1Count++;
            break;
        }

        case IKE_INTERNAL_IP4_DNS:
        case IKE_INTERNAL_ADDRESS_EXPIRY:
        case IKE_INTERNAL_IP4_DHCP:
        case IKE_APPLICATION_VERSION:
        case IKE_INTERNAL_IP6_DNS:
        case IKE_INTERNAL_IP6_DHCP:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CfgSetCMRepCheckAtts: Unsupported "
                         "Attributes Discarded \n");
            return (IKE_SUCCESS);
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2CfgSetCMRepCheckAtts: Attribute not "
                         "Supported\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}
#endif /* _IKE2_CFG_C_ */
