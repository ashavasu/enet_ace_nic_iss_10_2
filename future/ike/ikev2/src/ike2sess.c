
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2sess.c,v 1.14 2014/08/22 11:03:15 siva Exp $
 *
 * Description: This has functions for IKEv2 session management
 *
 ***********************************************************************/

#ifndef _IKE2_SESS_C_
#define _IKE2_SESS_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikedsa.h"
#include "ikememmac.h"

PRIVATE INT1 Ike2SessCopyRaInfoToCrypto PROTO ((tIkeEngine * pIkeEngine,
                                                tIkeSessionInfo * pSessionInfo,
                                                tIkeRemoteAccessInfo *
                                                pRAPolicy));

PRIVATE INT1 Ike2SessCopyIPSecInfo PROTO ((tIkeEngine *, tIkeSessionInfo *,
                                           tIkeNewSA * pIPSecReq));

/******************************************************************************/
/*  Function Name :  Ike2SessGetSessionFromCookie                             */
/*  Description   :  Get session info from session data base based            */
/*                :  on peer address, Initiator cookie and                    */
/*                :  responder cookie                                         */
/*  Input(s)      :  pIkeEngine  - Pointer to the IKE Engine                  */
/*                :  pPeerTEAddr - Pointer to the Peer Address Struct         */
/*                :  pIsakmpHdr  - Pointer to the IKE Header                  */
/*                :                                                           */
/*  Output(s)     :  pSesInfo    - Pointer to the Session or NULL             */
/*  Return        :  Session Info or NULL                                     */
/******************************************************************************/
tIkeSessionInfo    *
Ike2SessGetSessionFromCookie (tIkeEngine * pIkeEngine, tIkeIpAddr * pPeerTEAddr,
                              UINT2 u2RemotePort, UINT2 u2LocalPort,
                              tIsakmpHdr * pIsakmpHdr)
{

    tIkeSessionInfo    *pSesInfo = NULL;
    tIkeSA             *pIkeSA = NULL;
    UINT1               au1TempCookie[IKE_COOKIE_LEN] = { IKE_ZERO };
    UINT4               u4MsgId = IKE_ZERO;
    UINT1               u1ChildType = IKE_ZERO;

    u4MsgId = IKE_NTOHL (pIsakmpHdr->u4MessId);
    if (pPeerTEAddr->u4AddrType == IKE_IPSEC_ID_IPV4_ADDR)
    {
        pPeerTEAddr->Ipv4Addr = IKE_NTOHL (pPeerTEAddr->Ipv4Addr);
    }

    SLL_SCAN (&pIkeEngine->IkeSessionList, pSesInfo, tIkeSessionInfo *)
    {
        if ((IKE_MEMCMP
             (pSesInfo->pIkeSA->au1InitiatorCookie, pIsakmpHdr->au1InitCookie,
              IKE_COOKIE_LEN) == IKE_ZERO)
            &&
            ((IKE_MEMCMP
              (pSesInfo->pIkeSA->au1ResponderCookie, pIsakmpHdr->au1RespCookie,
               IKE_COOKIE_LEN) == IKE_ZERO) ||
             (IKE_MEMCMP
              (pSesInfo->pIkeSA->au1ResponderCookie, au1TempCookie,
               IKE_COOKIE_LEN) == IKE_ZERO))
            &&
            (pSesInfo->u4MsgId == u4MsgId) &&
            (pSesInfo->u1ExchangeType == pIsakmpHdr->u1Exch))
        {
            if (IKE_MEMCMP (pSesInfo->pIkeSA->au1ResponderCookie, au1TempCookie,
                            IKE_COOKIE_LEN) == IKE_ZERO)
            {
                /* Copy the Responder cookie */
                IKE_MEMCPY (pSesInfo->pIkeSA->au1ResponderCookie,
                            pIsakmpHdr->au1RespCookie, IKE_COOKIE_LEN);
            }

            if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
            {
                if (pSesInfo->RemoteTunnelTermAddr.Ipv4Addr ==
                    pPeerTEAddr->Ipv4Addr)
                {
                    return pSesInfo;
                }
            }
            else
            {
                if (IKE_MEMCMP
                    (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr,
                     &pPeerTEAddr->Ipv6Addr, sizeof (tIp6Addr)) == IKE_ZERO)
                {
                    return pSesInfo;
                }
            }
        }
    }

    /* For the case when peer initiates and we are getting for the
     * first time
     */
    if (IKE_MEMCMP (pIsakmpHdr->au1RespCookie, au1TempCookie,
                    IKE_COOKIE_LEN) == IKE_ZERO)
    {
        if (pIsakmpHdr->u1Exch == IKE2_INIT_EXCH)
        {
            SLL_SCAN (&pIkeEngine->IkeSessionList, pSesInfo, tIkeSessionInfo *)
            {
                if ((IKE_MEMCMP
                     (pSesInfo->pIkeSA->au1InitiatorCookie,
                      pIsakmpHdr->au1InitCookie, IKE_COOKIE_LEN) == IKE_ZERO)
                    && (pSesInfo->u4MsgId == u4MsgId))
                {
                    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
                    {
                        if (pSesInfo->RemoteTunnelTermAddr.Ipv4Addr ==
                            pPeerTEAddr->Ipv4Addr)
                        {
                            return pSesInfo;
                        }
                    }
                    else
                    {
                        if (IKE_MEMCMP
                            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr,
                             &pPeerTEAddr->Ipv6Addr, sizeof (tIp6Addr)) ==
                            IKE_ZERO)
                        {
                            return pSesInfo;
                        }
                    }
                }
            }                    /* SLL_SCAN Ends */
        }
        else
        {
            /* Received an info exchange message, ignore */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessGetSessionFromCookie:Received non IKE_SA_INIT"
                         " exchangemessage with responder cookie as 0\n");
            return (NULL);
        }
        /* RespCookie is zero, first message from peer, Construct a 
           SessionInfo structure */
        pSesInfo =
            Ike2SessInitExchSessInit (pIkeEngine, IKE_RESPONDER, pPeerTEAddr);
        if (pSesInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessGetSessionFromCookie:Unable to create "
                         "Init SessionInfo\n");
            return (NULL);
        }
        pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort = u2RemotePort;
        if (u2LocalPort == IKE_NATT_UDP_PORT)
        {
            pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags =
                pSesInfo->pIkeSA->IkeNattInfo.u1NattFlags | IKE_USE_NATT_PORT;
        }

        /* Copy the initiator cookie */
        IKE_MEMCPY (pSesInfo->pIkeSA->au1InitiatorCookie,
                    pIsakmpHdr->au1InitCookie, IKE_COOKIE_LEN);
        return (pSesInfo);
    }

    /* If we get a request from AUTH or Child or Info, we have to start 
     * cooresponding initiation,  in which case the role would be responder */
    if (pIsakmpHdr->u1Exch != IKE2_INIT_EXCH)
    {
        pIkeSA =
            IkeGetIkeSAFromCookies (pIkeEngine, pIsakmpHdr->au1InitCookie,
                                    pIsakmpHdr->au1RespCookie, pPeerTEAddr);

        if (pIkeSA == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessGetSessionFromCookie:IkeGetIkeSAFromCookies "
                         "failed\n");
            return NULL;
        }
        /* If RESPONDER, pass the u4MsgId */
        if (pIsakmpHdr->u1Exch == IKE2_AUTH_EXCH)
        {
            pSesInfo =
                Ike2SessAuthExchSessInit (pIkeSA, NULL, u4MsgId, IKE_RESPONDER);
        }
        if (pIsakmpHdr->u1Exch == IKE2_CHILD_EXCH)
        {
            pSesInfo =
                Ike2SessChildExchSessInit (pIkeSA, NULL, u4MsgId, IKE_RESPONDER,
                                           u1ChildType);
        }
        else if (pIsakmpHdr->u1Exch == IKE2_INFO_EXCH)
        {
            pSesInfo =
                Ike2SessInfoExchSessInit (pIkeSA, u4MsgId, IKE_RESPONDER);
        }

        if (pSesInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessGetSessionFromCookie: Auth/Child session "
                         "initialization failed\n");
            return NULL;
        }

        /* Ike2SessAuthExchSessInit would have sent a message to
           IPSec to check for duplicate SPI, will have to
           wait for the reply before starting the Phase2
           negotiation */

        return pSesInfo;
    }

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "Ike2SessGetSessionFromCookie: Could not get/initialize "
                 "session\n");
    return NULL;
}

/******************************************************************************/
/*  Function Name : Ike2SessInitExchSessInit                                  */
/*  Description   : This function Initializes the Initial Exchanges           */
/*                : of the IKEv2 Session Structure                            */
/*                :                                                           */
/*  Input(s)      : pIkeEngine   - Pointer to the IKE Engine                  */
/*                : u1Role       - Role (Initiator or Responder)              */
/*                : pPeerTEAddr  - Pointer to the Peer Address Info           */
/*                :                                                           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
tIkeSessionInfo    *
Ike2SessInitExchSessInit (tIkeEngine * pIkeEngine, UINT1 u1Role,
                          tIkeIpAddr * pPeerTEAddr)
{
    tIkeSessionInfo    *pSesInfo = NULL;
    UINT4               u4Pos = IKE_ZERO;

    /* Allocate memory for the session structure */
    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessInitExchSessInit: unable to allocate "
                     "buffer\n");
        return NULL;
    }

    if (pSesInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessInitExchSessInit: Session memory Allocation "
                     "Fail\n");
        return NULL;
    }
    IKE_BZERO (pSesInfo, sizeof (tIkeSessionInfo));

    /* Allocate memory for the IKE SA */

    if (MemAllocateMemBlock
        (IKE_SA_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pIkeSA) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessInitExchSessInit: unable to allocate "
                     "buffer\n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return NULL;
    }
    if (pSesInfo->pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessInitExchSessInit:Not enough memory\n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return (NULL);
    }
    IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));

    pSesInfo->pIkeSA->u4RefCount++;

    IKE_STRCPY (pSesInfo->au1EngineName, pIkeEngine->au1EngineName);

    /* Set the Version info in session and IKE SA */
    pSesInfo->u1IkeVer = IKE2_MAJOR_VERSION;
    pSesInfo->pIkeSA->u1IkeVer = IKE2_MAJOR_VERSION;
    pSesInfo->u1SessionStatus = IKE_ACTIVE;
    pSesInfo->u1ExchangeType = IKE2_INIT_EXCH;
    OsixGetSysTime (&(pSesInfo->u4TimeStamp));
    pSesInfo->pIkeSA->u1SAStatus = IKE_ACTIVE;
    pSesInfo->u1Role = u1Role;
    pSesInfo->u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
    pSesInfo->pIkeSA->u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
    /* Initialize the proposal list to store the 
     * received Proposals */
    SLL_INIT (&(pSesInfo->IkeProposalList));
    /* Copy the Tunnel Term addr of the peer */
    IKE_MEMCPY (&pSesInfo->RemoteTunnelTermAddr, pPeerTEAddr,
                sizeof (tIkeIpAddr));
    IKE_MEMCPY (&pSesInfo->pIkeSA->IpPeerAddr, pPeerTEAddr,
                sizeof (tIkeIpAddr));

    /* Copy the local Tunnel Termination address */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&pSesInfo->pIkeSA->IpLocalAddr,
                    &pIkeEngine->Ip6TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    else
    {
        IKE_MEMCPY (&pSesInfo->pIkeSA->IpLocalAddr,
                    &pIkeEngine->Ip4TunnelTermAddr, sizeof (tIkeIpAddr));
    }

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        /* In case of We are the initiator for the Init session, 
         * then set the original initiator flag in IKESA*/
        pSesInfo->pIkeSA->Ike2SA.bIsOrgInitiator = IKE_TRUE;
    }

    /* Initialize the Recd Packet structure */
    for (u4Pos = IKE_ZERO; u4Pos <= IKE_MAX_PAYLOADS; u4Pos++)
    {
        SLL_INIT (&pSesInfo->IkeRxPktInfo.aPayload[u4Pos]);
    }

    if (Ike2SessCopyInitSessionInfo (pIkeEngine, pSesInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessInitExchSessInit:Not able to copy "
                     "Init exchange sessionInfo\n");
        /* FIPS compliance - Cryptographic keys zeroed at the
         * end of their lifecycle */
        IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
        MemReleaseMemBlock (IKE_SA_MEMPOOL_ID, (UINT1 *) pSesInfo->pIkeSA);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSesInfo);
        return (NULL);
    }

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA ||
        pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA)
    {
        if (Ike2SessCopyCertInfo (pIkeEngine, pSesInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessInitExchSessInit:Not able to copy "
                         "certificate related information\n");
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IKE_BZERO (pSesInfo->pIkeSA, sizeof (tIkeSA));
            MemReleaseMemBlock (IKE_SA_MEMPOOL_ID, (UINT1 *) pSesInfo->pIkeSA);
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSesInfo);
            return (NULL);
        }
    }

    pSesInfo->u1FsmState = IKE2_INIT_IDLE;
    pSesInfo->bPhase1Done = IKE_FALSE;

    if (u1Role == IKE_INITIATOR)
    {
        pSesInfo->pIkeSA->IkeNattInfo.u2RemotePort = IKE_UDP_PORT;
    }
    SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSesInfo);
    return (pSesInfo);
}

/******************************************************************************/
/*  Function Name : Ike2SessCopyCertInfo                                      */
/*  Description   : This function Copies  the required Certificates           */
/*                  related information used while Authentication             */
/*                :                                                           */
/*  Input(s)      : pIkeEngine   - Pointer to the IKE Engine                  */
/*                : pSessionInfo - Pointer to the Session Structure Info      */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure with Certificate Information      */
/*  Return        : Pointer to the updated   session Structure                */
/******************************************************************************/
INT4
Ike2SessCopyCertInfo (tIkeEngine * pIkeEngine, tIkeSessionInfo * pSesInfo)
{
    tIkeCertMapToPeer  *pCertMap = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;
    tIkeIpAddr          PeerIpAddr;

    UNUSED_PARAM (pIkePolicy);

    MEMSET (&PeerIpAddr, IKE_ZERO, sizeof (tIkeIpAddr));

    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));

    if ((pSesInfo->pIkeSA->ResponderID.i4IdType != IKE_KEY_IPV4) &&
        (pSesInfo->pIkeSA->ResponderID.i4IdType != IKE_KEY_IPV6))
    {
        pCertMap = IkeGetCertMapFromRemoteID (pIkeDummyEngine,
                                              pSesInfo->pIkeSA->ResponderID.uID.
                                              au1Dn,
                                              (UINT1) pSesInfo->pIkeSA->
                                              ResponderID.i4IdType);
    }
    else
    {
        PeerIpAddr.u4AddrType = (UINT4) pSesInfo->pIkeSA->ResponderID.i4IdType;
        PeerIpAddr.Ipv4Addr = pSesInfo->pIkeSA->ResponderID.uID.Ip4Addr;
        pCertMap = IkeGetCertMapFromPeerAddr (pIkeDummyEngine, &PeerIpAddr);
    }

    if (pCertMap == NULL)
    {
        if (pIkeDummyEngine != NULL)
        {
            /* Check if default certificate is present */
            if ((pIkeDummyEngine->DefaultCert.pCAName != NULL) &&
                (pIkeDummyEngine->DefaultCert.pCASerialNum != NULL))
            {
                /* Copy the default cert information to session 
                 * data structure */
                TAKE_IKE_SEM ();
                pSesInfo->MyCert.pCAName =
                    IkeX509NameDup (pIkeDummyEngine->DefaultCert.pCAName);
                pSesInfo->MyCert.pCASerialNum =
                    IkeASN1IntegerDup (pIkeDummyEngine->DefaultCert.
                                       pCASerialNum);
                GIVE_IKE_SEM ();
                pSesInfo->bDefaultMyCert = IKE_TRUE;
            }
            else
            {
                /* if Default Cert is not there and the peer configuration 
                 * not present then don't start negotiation */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SessCopyCertInfo: Certificate "
                             "not configured for the peer!\n");
                return (IKE_FAILURE);
            }
        }
    }
    else
    {
        pSesInfo->MyCert.pCAName = IkeX509NameDup (pCertMap->MyCert.pCAName);
        pSesInfo->MyCert.pCASerialNum =
            IkeASN1IntegerDup (pCertMap->MyCert.pCASerialNum);
        pSesInfo->bDefaultMyCert = IKE_FALSE;
    }

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA)
    {
        /* Store the rsa key associated with this certificate */
        pIkeRsa = IkeGetRSAKeyWithCertInfo (pIkeDummyEngine, &pSesInfo->MyCert);
        if (pIkeRsa != NULL)
        {
            IKE_MEMCPY (&pSesInfo->MyCertKey, pIkeRsa, sizeof (tIkeRsaKeyInfo));
            IKE_MEMCPY (&pSesInfo->MyCertKey.au1EngineName,
                        &pIkeEngine->au1EngineName,
                        STRLEN (pIkeEngine->au1EngineName));
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessCopyCertInfo: Unable to get "
                         "RSA key using certificate information !\n");
            return (IKE_FAILURE);
        }
    }
    else if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA)
    {
        /* Store the dsa key associated with this certificate */
        pIkeDsa = IkeGetDSAKeyWithCertInfo (pIkeDummyEngine, &pSesInfo->MyCert);
        if (pIkeDsa != NULL)
        {
            IKE_MEMCPY (&pSesInfo->MyCertKey, pIkeDsa, sizeof (tIkeDsaKeyInfo));
            IKE_MEMCPY (&pSesInfo->MyCertKey.au1EngineName,
                        &pIkeEngine->au1EngineName,
                        STRLEN (pIkeEngine->au1EngineName));
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessCopyCertInfo: Unable to get "
                         "DSA key using certificate information !\n");
            return (IKE_FAILURE);
        }
    }
    SLL_INIT (&(pSesInfo->PeerCertReq));
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2SessCopyInitSessionInfo                               */
/*  Description   : This function Copies  the Initial session info            */
/*                : structure with the Configured SA Details.                 */
/*                :                                                           */
/*  Input(s)      : pIkeEngine   - Pointer to the IKE Engine                  */
/*                : pSessionInfo - Pointer to the Session Structure Info      */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the updated   session Structure                */
/******************************************************************************/
INT4
Ike2SessCopyInitSessionInfo (tIkeEngine * pIkeEngine,
                             tIkeSessionInfo * pSessionInfo)
{
    tIkePolicy         *pIkePolicy = NULL;
    tIkeSA             *pIkeSA = NULL;
    tIkeRemoteAccessInfo *pRAPolicy = NULL;
    BOOLEAN             bPolicyFound = IKE_FALSE;

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType ==
            (UINT4) pIkePolicy->PeerAddr.u4AddrType)
        {
            if (pIkePolicy->PeerAddr.u4AddrType == IPV6ADDR)
            {
                if (IKE_MEMCMP (&pSessionInfo->RemoteTunnelTermAddr.uIpAddr,
                                &pIkePolicy->PeerAddr.uIpAddr,
                                sizeof (pIkePolicy->PeerAddr.uIpAddr)) ==
                    IKE_ZERO)
                {
                    break;
                }
            }
            else if (pIkePolicy->PeerAddr.u4AddrType == IPV4ADDR)
            {
                if (pSessionInfo->RemoteTunnelTermAddr.uIpAddr.Ip4Addr ==
                    pIkePolicy->PeerAddr.uIpAddr.Ip4Addr)
                {
                    break;
                }
            }
        }
    }

    if (pIkePolicy != NULL)
    {
        IKE_MEMCPY (&pSessionInfo->Ike2IkePolicy, pIkePolicy,
                    sizeof (tIkePolicy));
        bPolicyFound = IKE_TRUE;
    }
    else
    {
        /* Select the policy for RA-VPN Server  */
        SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
        {
            SLL_SCAN (&(pIkeEngine->IkeRAPolicyList), pRAPolicy,
                      tIkeRemoteAccessInfo *)
            {
                if ((IkeComparePhase1IDs
                     (&pRAPolicy->Phase1ID, &pIkePolicy->PolicyPeerID) ==
                     IKE_ZERO) && (gbCMServer == TRUE))
                {
                    IKE_MEMCPY (&pSessionInfo->Ike2IkePolicy, pIkePolicy,
                                sizeof (tIkePolicy));
                    bPolicyFound = IKE_TRUE;
                    break;
                }
            }
            if (bPolicyFound == IKE_TRUE)
            {
                break;
            }
        }
    }
    if (bPolicyFound != IKE_TRUE)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessCopyInitSessionInfo: ID Mismatch"
                     "Policy not found!\n");
        return (IKE_FAILURE);
    }

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pSessionInfo->i2SocketId = pIkeEngine->i2V6SocketFd;
        pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd =
            pIkeEngine->i2V6NattSocketFd;
    }
    else
    {

        pSessionInfo->i2SocketId = pIkeEngine->i2SocketId;
        pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd =
            pIkeEngine->i2NattSocketFd;
    }
    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else
    {                            /* In case of Child Exchange for IKE SA Rekey */
        pIkeSA = pSessionInfo->pIkeReKeySA;
    }

    pIkeSA->u2AuthMethod = pSessionInfo->Ike2IkePolicy.u2AuthMethod;

    /* Store the Initiator ID */
    IKE_MEMCPY (&pIkeSA->InitiatorID, &pIkePolicy->PolicyLocalID,
                sizeof (tIkePhase1ID));
    /* Store the Responder ID */
    IKE_MEMCPY (&pIkeSA->ResponderID, &pIkePolicy->PolicyPeerID,
                sizeof (tIkePhase1ID));

    if (pSessionInfo->u1Role == IKE_INITIATOR)
    {
        IkeGetRandom (pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    }
    else
    {
        IkeGetRandom (pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);
    }

    /* Update Life time attributes into IKE SA */
    if (pSessionInfo->Ike2IkePolicy.u4LifeTimeKB != IKE_ZERO)
    {
        pIkeSA->u4LifeTimeKB = pSessionInfo->Ike2IkePolicy.u4LifeTimeKB;
    }
    else
    {
        pIkeSA->u4LifeTime =
            IkeGetPhase1LifeTime (&pSessionInfo->Ike2IkePolicy);
    }

    /* Set the DH group in the session Info  */
    if (pSessionInfo->Ike2IkePolicy.u1DHGroup == IKE_DH_GROUP_1)
    {
        pSessionInfo->pDhInfo = &gDhGroupInfo768;
    }
    else if (pSessionInfo->Ike2IkePolicy.u1DHGroup == IKE_DH_GROUP_2)
    {
        pSessionInfo->pDhInfo = &gDhGroupInfo1024;
    }
    else if (pSessionInfo->Ike2IkePolicy.u1DHGroup == IKE_DH_GROUP_5)
    {
        pSessionInfo->pDhInfo = &gDhGroupInfo1536;
    }
    else if (pSessionInfo->Ike2IkePolicy.u1DHGroup == SEC_DH_GROUP_14)
    {
        pSessionInfo->pDhInfo = &gDhGroupInfo2048;
    }
    GIVE_IKE_SEM ();
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2SessCopyRAPolicy                                      */
/*  Description   : This function Copies the RA INfo Into the Session         */
/*                : Structure                                                 */
/*                  This function is called only when Preshared key lookup    */
/*                : is Successfull                                            */
/*  Input(s)      : pSessionInfo - Pointer to the IKE Session Strucutre       */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
INT1
Ike2SessCopyRAPolicy (tIkeSessionInfo * pSessionInfo)
{
    tIkePhase1ID       *pPeerId = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pRAPolicy = NULL;

    pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessCopyRAPolicy: No Engine found !\n");
        return (IKE_FAILURE);
    }

    /* Irrespective of the Role the Responder Peer ID will be
     * the Remote Tunnel termination address */
    pPeerId = &(pSessionInfo->pIkeSA->ResponderID);

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pRAPolicy, tIkeRemoteAccessInfo *)
    {
        if (IkeComparePhase1IDs (&pRAPolicy->Phase1ID, pPeerId) == IKE_ZERO)
        {
            if (gbCMServer == TRUE)
            {
                pSessionInfo->Ike2AuthInfo.bCMServer = IKE_TRUE;
                IKE_MEMCPY (&(pRAPolicy->PeerAddr),
                            &(pSessionInfo->RemoteTunnelTermAddr),
                            sizeof (tIkeIpAddr));
            }
            IKE_MEMCPY (&(pSessionInfo->Ike2AuthInfo.IkeRAInfo), pRAPolicy,
                        sizeof (tIkeRemoteAccessInfo));
            GIVE_IKE_SEM ();
            Ike2SessCopyRaInfoToCrypto (pIkeEngine, pSessionInfo, pRAPolicy);
            TAKE_IKE_SEM ();
            break;
        }
    }

    if (pRAPolicy != NULL)
    {
        /* No Xauth is IKEv2 */
        pSessionInfo->pIkeSA->bXAuthEnabled = pRAPolicy->bXAuthEnabled;
        pSessionInfo->pIkeSA->bCMEnabled = pRAPolicy->bCMEnabled;
        pSessionInfo->pIkeSA->bXAuthDone = FALSE;
        pSessionInfo->pIkeSA->bCMDone = FALSE;
        if (gbCMServer != IKE_TRUE)
        {
            IKE_MEMCPY (&(pSessionInfo->Ike2AuthInfo.
                          ProtectedNetwork[IKE_INDEX_0]),
                        &(pSessionInfo->Ike2AuthInfo.IkeRAInfo.
                          aProtectNetBundle[IKE_INDEX_0]->ProtectedNetwork),
                        sizeof (tIkeNetwork));
        }
    }
    else
    {
        pSessionInfo->pIkeSA->bXAuthEnabled = FALSE;
        pSessionInfo->pIkeSA->bCMEnabled = FALSE;
        pSessionInfo->pIkeSA->bXAuthDone = FALSE;
        pSessionInfo->pIkeSA->bCMDone = FALSE;
    }

    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2SessAuthExchSessInit                                  */
/*  Description   : This function Initializes the Auth Exchanges              */
/*                : of the IKEv2 Session Structure                            */
/*                :                                                           */
/*  Input(s)      : pIkeSA     - Pointer to the IkeSA structure               */
/*                : pIPSecReq  - Pointer to the IPSecReq structure            */
/*                : u4MsgId   - The Message Id                               */
/*                : u1Role     - Initiator or Responder                       */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
tIkeSessionInfo    *
Ike2SessAuthExchSessInit (tIkeSA * pIkeSA, tIkeNewSA * pIPSecReq, UINT4 u4MsgId,
                          UINT1 u1Role)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    INT4                i4Pos = IKE_ZERO;

    /* For an AUTH Pkt, the Msg Id is always 1 */
    if (u4MsgId != IKE2_AUTH_MSG_ID)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessAuthExchSessInit: Received Msg Id, Not 1 for "
                     " an AUTH Packet \n");
        return NULL;
    }

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessAuthExchSessInit: unable to allocate "
                     "buffer\n");
        return NULL;
    }
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessAuthExchSessInit: Session "
                     "memory Allocation Fail\n");
        return NULL;
    }

    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;
    OsixGetSysTime (&(pSessionInfo->u4TimeStamp));

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    /* Store the message Id */

    pSessionInfo->u4MsgId = IKE2_AUTH_MSG_ID;

    pSessionInfo->u1IkeVer = IKE2_MAJOR_VERSION;
    pSessionInfo->u1FsmState = IKE2_AUTH_IDLE;
    pSessionInfo->u1Role = u1Role;
    pSessionInfo->u1ExchangeType = IKE2_AUTH_EXCH;
    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;
    pSessionInfo->bPhase2Done = IKE_FALSE;
    pSessionInfo->pIkeSA->Ike2SA.bAuthDone = IKE_FALSE;
    /* Initialize the proposal list to store the 
     * received Proposals */
    SLL_INIT (&(pSessionInfo->IkeProposalList));

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        pSessionInfo->pIkeSA->u4RefCount--;
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2SessAuthExchSessInit: No IkeEngine Available with "
                      "Index %d\n", pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }
    pSessionInfo->i2SocketId =
        (INT2) ((pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR) ?
                pIkeEngine->i2V6SocketFd : pIkeEngine->i2SocketId);

    /* Lookup RA Node */
    TAKE_IKE_SEM ();
    if (Ike2SessCopyRAPolicy (pSessionInfo) == IKE_FAILURE)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessAuthExchSessInit:Ike2SessCopyRAPolicy Failed!\n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }
    GIVE_IKE_SEM ();

    if (Ike2SessCopyIPSecInfo (pIkeEngine, pSessionInfo, pIPSecReq) ==
        IKE_FAILURE)
    {
        pSessionInfo->pIkeSA->u4RefCount--;
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessAuthExchSessInit: Error Copying IPSec info to "
                     "AuthSession \n");
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    if ((pSessionInfo->u1Role == IKE_RESPONDER) &&
        (pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA ||
         pSessionInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA))
    {
        if (Ike2SessCopyCertInfo (pIkeEngine, pSessionInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessAuthExchSessInit: Not able to copy "
                         "certificate related information\n");
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return (NULL);

        }
    }

    if (pSessionInfo->pu1ResponderNonce == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSessionInfo->pu1ResponderNonce) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessAuthExchSessInit: Unable to Allocate Buffer\n");
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return (NULL);
        }
    }
    IKE_BZERO (pSessionInfo->pu1ResponderNonce, pIkeSA->Ike2SA.u2RespNonceLen);
    IKE_MEMCPY (pSessionInfo->pu1ResponderNonce, pIkeSA->Ike2SA.pu1RespNonce,
                pIkeSA->Ike2SA.u2RespNonceLen);
    pSessionInfo->u2ResponderNonceLen = pIkeSA->Ike2SA.u2RespNonceLen;

    if (pSessionInfo->pu1InitiatorNonce == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSessionInfo->pu1InitiatorNonce) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessAuthExchSessInit: Unable to Allocate Buffer\n");
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return (NULL);
        }
    }
    IKE_BZERO (pSessionInfo->pu1InitiatorNonce, pIkeSA->Ike2SA.u2InitNonceLen);
    IKE_MEMCPY (pSessionInfo->pu1InitiatorNonce, pIkeSA->Ike2SA.pu1InitNonce,
                pIkeSA->Ike2SA.u2InitNonceLen);
    pSessionInfo->u2InitiatorNonceLen = pIkeSA->Ike2SA.u2InitNonceLen;

    SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSessionInfo);

    IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.LocTunnTermAddr,
                &pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));
    IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.RemTunnTermAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    pSessionInfo->u1FsmState = IKE2_WAIT_ON_IPSEC;
    return pSessionInfo;
}

/******************************************************************************/
/*  Function Name : Ike2SessCopyIPSecInfo                                     */
/*  Description   : This function Initializes the Create Child SA Exchanges   */
/*                : of the IKEv2 Session Structure                            */
/*                :                                                           */
/*  Input(s)      : pIkeEngine     - Pointer to the Ike  Engine               */
/*                : pSessionInfo   - Pointer to the Session  structure        */
/*                : pIPSecReq   - Pointer to the IPSecReq structure           */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
INT1
Ike2SessCopyIPSecInfo (tIkeEngine * pIkeEngine, tIkeSessionInfo * pSessionInfo,
                       tIkeNewSA * pIPSecReq)
{
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkeIPSecIfParam    IkeIfParam;
    UINT4               u4Spi = IKE_ZERO;

    if ((pIPSecReq != NULL) && ((pSessionInfo->pIkeSA->bCMEnabled != TRUE) ||
                                (pSessionInfo->pIkeSA->bCMDone == IKE_TRUE)))
    {
        IKE_MEMCPY (&pSessionInfo->PostPhase1Info.IPSecRequest, pIPSecReq,
                    sizeof (tIkeNewSA));

        TAKE_IKE_SEM ();
        pCryptoMap = IkeGetCryptoMapFromIDs (pIkeEngine, &pIPSecReq->SrcNet,
                                             &pIPSecReq->DestNet,
                                             IKE2_MAJOR_VERSION);
        if (pCryptoMap == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessCopyIPSecInfo: No Crypto Map Available "
                         "for Peer \n");
            return (IKE_FAILURE);
        }

        IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IkeCryptoMap, pCryptoMap,
                    sizeof (tIkeCryptoMap));

        /* Copy local and remote network addresses to IPSecBundle */
        IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.LocalProxy,
                    &pCryptoMap->LocalNetwork, sizeof (tIkeNetwork));
        IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.RemoteProxy,
                    &pCryptoMap->RemoteNetwork, sizeof (tIkeNetwork));
        GIVE_IKE_SEM ();

        if (pSessionInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_1)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo768;
        }
        else if (pSessionInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_2)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo1024;
        }
        else if (pSessionInfo->Ike2IPSecPolicy.u1Pfs == IKE_DH_GROUP_5)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo1536;
        }
        else if (pSessionInfo->Ike2IPSecPolicy.u1Pfs == SEC_DH_GROUP_14)
        {
            pSessionInfo->pDhInfo = &gDhGroupInfo2048;
        }
    }

    /* Generate the SPIs and send to IPSec to check for
       duplicate */
    IKE_BZERO (&IkeIfParam, sizeof (tIkeIPSecIfParam));

    IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
    IkeIfParam.IPSecReq.DupSpiInfo.u4AhSpi = u4Spi;
    pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aInSABundle[IKE_AH_SA_INDEX].u4Spi = u4Spi;

    IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
    IkeIfParam.IPSecReq.DupSpiInfo.u4EspSpi = u4Spi;

    /* Set the Ikev2 Major Version in the Ipsec Request */
    IkeIfParam.IPSecReq.DupSpiInfo.u1IkeVersion = IKE2_MAJOR_VERSION;

    IkeIfParam.IPSecReq.DupSpiInfo.bDupSpi = IKE_FALSE;
    pSessionInfo->Ike2AuthInfo.IPSecBundle.
        aInSABundle[IKE_ESP_SA_INDEX].u4Spi = u4Spi;

    IkeIfParam.u4MsgType = IKE_IPSEC_DUPLICATE_SPI_REQ;
    pSessionInfo->u4RefCount++;
    IkeIfParam.IPSecReq.DupSpiInfo.u4Context = (FS_ULONG) pSessionInfo;
    IKE_MEMCPY (&IkeIfParam.IPSecReq.DupSpiInfo.LocalAddr,
                &pSessionInfo->pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));
    IKE_MEMCPY (&IkeIfParam.IPSecReq.DupSpiInfo.RemoteAddr,
                &pSessionInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    if (IkeSendIPSecIfParam (&IkeIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessCopyIPSecInfo: IkeSendIPSecIfParam Failed\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2SessChildExchSessInit                                 */
/*  Description   : This function Initializes the Create Child SA Exchanges   */
/*                : of the IKEv2 Session Structure                            */
/*                :                                                           */
/*  Input(s)      : pIkeSA      - Pointer to the IkeSA structure              */
/*                : pIPSecReq   - Pointer to the IPSecReq structure           */
/*                : u4MsgId    - The Message Id                              */
/*                : u1Role      - Initiator or Responder                      */
/*                : u1ChildType - Rekey Type (IKE/CHILD SA)                   */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
tIkeSessionInfo    *
Ike2SessChildExchSessInit (tIkeSA * pIkeSA, tIkeNewSA * pIPSecReq,
                           UINT4 u4MsgId, UINT1 u1Role, UINT1 u1ChildType)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    INT4                i4Pos = IKE_ZERO;

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessChildExchSessInit: unable to allocate "
                     "buffer\n");
        return NULL;
    }

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessChildExchSessInit: Session memory Allocation "
                     "Fail\n");
        return NULL;
    }

    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));

    /* Set the Version info in session and IKE SA */
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;
    /* Fill the IKe SA */
    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;
    pSessionInfo->u1IkeVer = IKE2_MAJOR_VERSION;

    /* Initialize the proposal list to store the 
     * received Proposals */
    OsixGetSysTime (&(pSessionInfo->u4TimeStamp));

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr,
                &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    /* Store the message Id */
    if (u1Role == IKE_INITIATOR)
    {
        pSessionInfo->u4MsgId = pIkeSA->Ike2SA.u4TxMsgId;
    }
    else
    {
        pSessionInfo->u4MsgId = u4MsgId;
    }

    pSessionInfo->u1FsmState = IKE2_CHILD_IDLE;
    pSessionInfo->u1IkeVer = IKE2_MAJOR_VERSION;
    pSessionInfo->u1Role = u1Role;
    pSessionInfo->u1ExchangeType = IKE2_CHILD_EXCH;
    /* Set the Child type(either for REKEY IKESA/REKEy_CHIL/CREATE_NEW_CHILD) */
    pSessionInfo->u1ChildType = u1ChildType;
    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;

    SLL_INIT (&(pSessionInfo->IkeProposalList));

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        pSessionInfo->pIkeSA->u4RefCount--;
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2SessChildExchSessInit: No IkeEngine Available with "
                      "Index %d\n", pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }
    IKE_STRCPY (pSessionInfo->au1EngineName, pIkeEngine->au1EngineName);
    pSessionInfo->i2SocketId =
        (INT2) ((pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR) ?
                pIkeEngine->i2V6SocketFd : pIkeEngine->i2SocketId);

    if (u1Role == IKE_RESPONDER)
    {
        /* In case of Responder, It is not sure what the Rekey Type is, 
         * It will be decided after the packet is decrypted and then the 
         * Ike or IPSec info will be copied to the Session/IkeReKeySA */
        pSessionInfo->u1FsmState = IKE2_CHILD_IDLE;
        SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSessionInfo);
        return pSessionInfo;
    }

    if (u1ChildType == IKE2_IPSEC_REKEY_IKESA)
    {
        if (Ike2SessCopyIkeSAInfo (pSessionInfo, pIkeEngine) == IKE_FAILURE)
        {
            pSessionInfo->pIkeSA->u4RefCount--;
            IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                         "Ike2SessChildExchSessInit: Not enough memory for "
                         "Rekey IkeSA\n");
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return (NULL);
        }
        /* We are the initiator for this IKE SA, so set the original 
         * Initiator flag to TRUE */
        pSessionInfo->pIkeReKeySA->Ike2SA.bIsOrgInitiator = IKE_TRUE;
    }
    else
    {
        pSessionInfo->pIkeReKeySA = NULL;
        /* In case of REKEY_CHILD/NEW_CHILD */
        if (Ike2SessCopyIPSecInfo (pIkeEngine, pSessionInfo,
                                   pIPSecReq) == IKE_FAILURE)
        {
            pSessionInfo->pIkeSA->u4RefCount--;
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessChildExchSessInit: Error Copying IPSec info "
                         "to Auth Session \n");
            MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo);
            return NULL;
        }
        if (gbCMServer == TRUE)
        {
            pSessionInfo->Ike2AuthInfo.bCMServer = IKE_TRUE;
        }
        IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.LocTunnTermAddr,
                    &pIkeSA->IpLocalAddr, sizeof (tIkeIpAddr));
        IKE_MEMCPY (&pSessionInfo->Ike2AuthInfo.IPSecBundle.RemTunnTermAddr,
                    &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));
        pSessionInfo->u1FsmState = IKE2_WAIT_ON_IPSEC;
    }
    SLL_ADD (&pIkeEngine->IkeSessionList, (tTMO_SLL_NODE *) pSessionInfo);
    return pSessionInfo;
}

/******************************************************************************/
/*  Function Name : Ike2SessCopyIkeSAInfo                                     */
/*  Description   : This function Copies the IKESA related info into the      */
/*                : of the Rekeying IKESA and the Child Session Structure     */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Strucuture          */
/*                : pIkeEngin    - Pointer to the Ike Engine.                 */
/*                :                                                           */
/*  Output(s)     : NONE                                                      */
/*  Return        : IKE_SUCCESS/IKE_FAILURE                                   */
/******************************************************************************/
INT1
Ike2SessCopyIkeSAInfo (tIkeSessionInfo * pSessionInfo, tIkeEngine * pIkeEngine)
{
    /* Allocate memory for the New IKE SA */
    if (MemAllocateMemBlock
        (IKE_SA_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pIkeReKeySA) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessCopyIkeSAInfo: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pSessionInfo->pIkeReKeySA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessCopyIkeSAInfo: Not enough memory for "
                     "Rekey IkeSA\n");
        return (IKE_FAILURE);
    }

    IKE_BZERO (pSessionInfo->pIkeReKeySA, sizeof (tIkeSA));

    pSessionInfo->pIkeReKeySA->Ike2SA.bAuthDone =
        pSessionInfo->pIkeSA->Ike2SA.bAuthDone;

    pSessionInfo->pIkeReKeySA->u1IkeVer = IKE2_MAJOR_VERSION;
    pSessionInfo->pIkeReKeySA->u1SAStatus = IKE_ACTIVE;
    pSessionInfo->pIkeReKeySA->u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
    /* Update the RA VPN related info int he IkeSA */
    pSessionInfo->pIkeReKeySA->bXAuthEnabled =
        pSessionInfo->pIkeSA->bXAuthEnabled;
    pSessionInfo->pIkeReKeySA->bCMEnabled = pSessionInfo->pIkeSA->bCMEnabled;
    pSessionInfo->pIkeReKeySA->bXAuthDone = pSessionInfo->pIkeSA->bXAuthDone;
    pSessionInfo->pIkeReKeySA->bCMDone = pSessionInfo->pIkeSA->bCMDone;

    /* Copy the NatT related information into the new IKE SA */
    IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IkeNattInfo,
                &pSessionInfo->pIkeSA->IkeNattInfo, sizeof (tIkeNatT));

    /* Copy the Tunnel Term addr of the peer */
    IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IpPeerAddr,
                &pSessionInfo->pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr));

    /* Copy the local Tunnel Termination address */
    if (pSessionInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IpLocalAddr,
                    &pIkeEngine->Ip6TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    else
    {
        IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IpLocalAddr,
                    &pIkeEngine->Ip4TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IpLocalAddr,
                    &pIkeEngine->Ip6TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    else
    {
        IKE_MEMCPY (&pSessionInfo->pIkeReKeySA->IpLocalAddr,
                    &pIkeEngine->Ip4TunnelTermAddr, sizeof (tIkeIpAddr));
    }
    /* Copy the Ike Policy Info to the New SA */
    if (Ike2SessCopyInitSessionInfo (pIkeEngine, pSessionInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessCopyIkeSAInfo: Not able to copy "
                     "Ike2SessCopyInitSessionInfo\n");
        MemReleaseMemBlock (IKE_SA_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pIkeReKeySA);
        return (IKE_FAILURE);
    }

    pSessionInfo->bPhase1Done = IKE_FALSE;
    pSessionInfo->u1FsmState = IKE2_CHILD_IDLE;
    pSessionInfo->bPhase1Done = IKE_FALSE;
    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2SessInfoExchSessInit                                  */
/*  Description   : This function Initializes the Info Exchanges              */
/*                : of the IKEv2 Session Structure                            */
/*                :                                                           */
/*  Input(s)      : pIkeSA      - Pointer to the IkeSA structure              */
/*                : u4MsgId    - The Message Id                               */
/*                : u1Role      - Initiator or Responder                      */
/*                :                                                           */
/*  Output(s)     : pSessionInfo- Pointer to the Initialized session          */
/*                :               Structure                                   */
/*  Return        : Pointer to the Initialized session Structure              */
/******************************************************************************/
tIkeSessionInfo    *
Ike2SessInfoExchSessInit (tIkeSA * pIkeSA, UINT4 u4MsgId, UINT1 u1Role)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    INT4                i4Pos = IKE_ZERO;

    if (MemAllocateMemBlock
        (IKE_SESSION_INFO_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessInfoExchSessInit: unable to allocate "
                     "buffer\n");
        return NULL;
    }
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2SessInfoExchSessInit: Session memory Allocation "
                     "Fail\n");
        return NULL;
    }
    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    pSessionInfo->u1SessionStatus = IKE_ACTIVE;

    IKE_MEMCPY (&pSessionInfo->RemoteTunnelTermAddr, &pIkeSA->IpPeerAddr,
                sizeof (tIkeIpAddr));

    pSessionInfo->u4IkeEngineIndex = pIkeSA->u4IkeEngineIndex;

    /* Store the message Id */
    if (u1Role == IKE_INITIATOR)
    {
        /* For Auth Exchange, Msg Id is always 1 */
        pSessionInfo->u4MsgId = pIkeSA->Ike2SA.u4TxMsgId;
    }
    else
    {
        pSessionInfo->u4MsgId = u4MsgId;
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2SessInfoExchSessInit: No IkeEngine Available with "
                      "Index %d\n", pIkeSA->u4IkeEngineIndex);
        MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo);
        return NULL;
    }

    pSessionInfo->pIkeSA = pIkeSA;
    pSessionInfo->pIkeSA->u4RefCount++;
    pSessionInfo->i2SocketId =
        (INT2) ((pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR) ?
                pIkeEngine->i2V6SocketFd : pIkeEngine->i2SocketId);

    pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd =
        pIkeEngine->i2NattSocketFd;

    pSessionInfo->u1IkeVer = IKE2_MAJOR_VERSION;
    pSessionInfo->u1FsmState = IKE2_INFO_IDLE;
    pSessionInfo->u1Role = u1Role;
    pSessionInfo->u1ExchangeType = IKE2_INFO_EXCH;

    /* Initialize the Recd Packet structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_INIT (&pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]);
    }

    SLL_ADD (&pIkeEngine->IkeSessionList, (t_SLL_NODE *) pSessionInfo);
    return pSessionInfo;
}

/******************************************************************************/
/*  Function Name : Ike2SessDelInitExchSession                                */
/*  Description   : This function deletes the Initial Exchange session and the*/
/*                : related information and frees the allocated memory        */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Ike Session Structure       */
/*  Output(s)     : NONE                                                      */
/*  Return        : IKE_SUCCESS/IKE_FAILURE                                   */
/******************************************************************************/
INT1
Ike2SessDelInitExchSession (tIkeSessionInfo * pSessionInfo)
{
    if (pSessionInfo->u1Role == IKE_INITIATOR)
    {
        IkeDeleteSession (pSessionInfo);
        pSessionInfo = NULL;
    }
    else
    {
        /* As a responder, wait for any re-transmissions from the peer
         * and then delete the session */
        IkeStopTimer (&pSessionInfo->IkeSessionDeleteTimer);
        if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                           IKE_DELETE_SESSION_TIMER_ID,
                           IKE_DELAYED_SESSION_DELETE_INTERVAL,
                           pSessionInfo) != IKE_SUCCESS)
        {
            /* Not critical error, just log */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SessDelInitSession: IkeStartTimer "
                         "failed for Delete Session!\n");
        }
    }
    return (IKE_SUCCESS);
}

/********************************************************************/
/*  Function Name : Ike2SessCopyRaInfoToCrypto                      */
/*  Description   : Function to Copy the RaInfo information into    */
/*                : the Crypto map in session info.                 */
/*                                                                  */
/*  Input(s)      : pIkeEngine   - Pointer to IkeEngine             */
/*                : pSessionInfo - Pointer to session info.         */
/*                : pRAPolicy  - Pointer to the RaPolicy            */
/*  Output(s)     : None.                                           */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                      */
/********************************************************************/
INT1
Ike2SessCopyRaInfoToCrypto (tIkeEngine * pIkeEngine,
                            tIkeSessionInfo * pSessionInfo,
                            tIkeRemoteAccessInfo * pRAPolicy)
{
    tSNMP_OCTET_STRING_TYPE TransformsOctetStr;

    if (gbCMServer == TRUE)
    {
        pSessionInfo->Ike2AuthInfo.bCMServer = IKE_TRUE;
        IKE_MEMCPY (&(pRAPolicy->PeerAddr),
                    &(pSessionInfo->RemoteTunnelTermAddr), sizeof (tIkeIpAddr));
    }
    IKE_MEMCPY (&(pSessionInfo->Ike2IPSecPolicy.PeerAddr),
                &(pSessionInfo->RemoteTunnelTermAddr), sizeof (tIkeIpAddr));

    /* Copy the Transform set info into the Cryptomap */
    TransformsOctetStr.pu1_OctetList =
        pSessionInfo->Ike2AuthInfo.IkeRAInfo.au1TransformSetBundleDisplay;

    TransformsOctetStr.i4_Length =
        (INT4) STRLEN (pSessionInfo->Ike2AuthInfo.IkeRAInfo.
                       au1TransformSetBundleDisplay);

    if (IkeCryptoSetTransformSetBundle
        (pIkeEngine, &(pSessionInfo->Ike2IPSecPolicy),
         TransformsOctetStr.pu1_OctetList,
         TransformsOctetStr.i4_Length) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SessCopyRaInfoToCrypto: Failed to copy the TS bundle "
                     "to Crypto map\n");
        return (IKE_FAILURE);
    }
    /* Decrement the Refcount of the TSBundle[0] as we have not yet really mapped
     * it but just taken a copy of the TS parameters, so reference count is to be
     * re-set. If Bundle is supported, then We need to loop through the available
     * TS Bunldes instead of 0(au1TS1) alone  */
    pSessionInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
        u4RefCount--;
    /* Copy onto string for show purposes */
    MEMCPY (&(pSessionInfo->Ike2AuthInfo.IkeCryptoMap.au1TSBundleDisplay),
            TransformsOctetStr.pu1_OctetList, TransformsOctetStr.i4_Length);

    pSessionInfo->Ike2AuthInfo.IkeCryptoMap.au1TSBundleDisplay
        [TransformsOctetStr.i4_Length] = '\0';

    /* Set the Mode (Tunnel/Transport) */
    pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode =
        pSessionInfo->Ike2AuthInfo.IkeRAInfo.u1Mode;
    /* Set Pfs if configured */
    if (pSessionInfo->Ike2AuthInfo.IkeRAInfo.u1Pfs != IKE_ZERO)
    {
        pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1Pfs =
            pSessionInfo->Ike2AuthInfo.IkeRAInfo.u1Pfs;
    }
    /* Set the Life time type and value */
    if (pSessionInfo->Ike2AuthInfo.IkeRAInfo.u1LifeTimeType != IKE_ZERO)
    {
        pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u1LifeTimeType =
            pSessionInfo->Ike2AuthInfo.IkeRAInfo.u1LifeTimeType;

        pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u4LifeTime =
            pSessionInfo->Ike2AuthInfo.IkeRAInfo.u4LifeTime;
    }
    /* Set the Life time in KB if configured */
    if (pSessionInfo->Ike2AuthInfo.IkeRAInfo.u4LifeTimeKB != IKE_ZERO)
    {
        pSessionInfo->Ike2AuthInfo.IkeCryptoMap.u4LifeTimeKB =
            pSessionInfo->Ike2AuthInfo.IkeRAInfo.u4LifeTimeKB;
    }
    /* Set the CryptoMap configuration as Dynamic */
    pSessionInfo->Ike2AuthInfo.IkeCryptoMap.bIsCryptoDynamic = IKE_TRUE;

    return (IKE_SUCCESS);
}
#endif /* End of the File */
