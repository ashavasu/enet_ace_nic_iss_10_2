
/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/* $Id: ike2val.c,v 1.4 2014/04/23 12:21:39 siva Exp $                                                   */
/*                                                                    */
/* Description:This file contains functions for validating           */
/*              packet and payloads                                   */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_VAL_C_
#define _IKE2_VAL_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "ikememmac.h"

PRIVATE INT1
    Ike2ValidTransform PROTO ((tIkeSessionInfo * pSesInfo, UINT2 u2PropLen,
                               UINT1 **pu1IncommingPkt, UINT1 u1ProtocolId));

PRIVATE INT1
    Ike2ValidSa PROTO ((tIkeSessionInfo * pSesInfo, UINT4 *pu4CurLen,
                        UINT4 u4TotalLen, UINT1 *pu1Packet));

PRIVATE INT1
    Ike2ValidPayLoad PROTO ((tIkeSessionInfo * pSessionInfo, UINT4 *pu4CurLen,
                             UINT4 u4TotLen, UINT1 *pu1PayloadType,
                             UINT1 *pu1PayLoad));

/************************************************************************/
/*  Function Name   :Ike2ValidatePacket                                 */
/*  Description     :This function is used to validate payloads in an   */
/*                  :Isakmp Message                                     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u4TotLen      - Total length of the Isakmp Message */
/*                  :u1PayLoadType - Identifies the type of payload     */
/*                  :pu1PayLoad    - Pointer to the payload to be       */
/*                  :                validated                          */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ValidatePacket (tIkeSessionInfo * pSessionInfo, UINT4 u4TotLen,
                    UINT1 u1PayLoadType, UINT1 *pu1PayLoad)
{
    UINT1              *pu1TempPayLoad = pu1PayLoad;
    UINT4               u4Len = IKE_ZERO;

    if (pu1PayLoad == NULL)
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidatePacket: Malformed Pkt\n");
        IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
        return (IKE_FAILURE);
    }
    /* Verify the packet length */
    if ((u4TotLen <= sizeof (tIsakmpHdr)) || (u4TotLen > IKE_MAX_PACKET_SIZE))
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidatePacket: Malformed Pkt\n");
        IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
        return (IKE_FAILURE);
    }

    /* Validate Payloads to ensure that the length of the individual
     * payloads do not exceed the size of the ISAKMP Message */
    while (u1PayLoadType != IKE2_PAYLOAD_NONE)
    {
        if (Ike2ValidPayLoad (pSessionInfo, &u4Len, u4TotLen,
                              &u1PayLoadType, pu1TempPayLoad) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ValidatePacket: Malformed Pkt\n");
            IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
            return (IKE_FAILURE);
        }
        pu1TempPayLoad = pu1PayLoad + u4Len;
    }
    /* Check the parsed length is equal to the tIsakmpHdr total length 
     * or not.*/
    if ((u4Len + sizeof (tIsakmpHdr)) == u4TotLen)
    {
        return (IKE_SUCCESS);
    }
    else
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IncIkeDropPkts (&pSessionInfo->pIkeSA->IpLocalAddr);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidatePacket: Malformed Pkt\n");
        return (IKE_FAILURE);
    }
}

/************************************************************************/
/*  Function Name   :Ike2ValidPayLoad                                   */
/*  Description     :This function validates the payload                */
/*                  :                                                   */
/*  Input(s)        :pSesInfo       - Pointer to the Session Info       */
/*                  :pi4CurLen      - Length of the Current payload     */
/*                  :i4TotLen       - Total length of the Isakmp Msg    */
/*                  :pu1PayloadType - The payload type                  */
/*                  :pu1PayLoad     - Pointer to the payload to be      */
/*                  :                 validated                         */
/*                  :                                                   */
/*  Output(s)       :Validates the payload and returns their status     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ValidPayLoad (tIkeSessionInfo * pSessionInfo, UINT4 *pu4CurLen,
                  UINT4 u4TotLen, UINT1 *pu1PayloadType, UINT1 *pu1PayLoad)
{
    tGenericPayLoad     Dummy;
    tIkePayload        *pIkePayload = NULL;
    UINT4               u4PayloadType = IKE_ZERO;

    if (*pu1PayloadType > IKE2_MAX_PAYLOAD_TYPE)
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidPayLoad: Invalid Payload Type\n");
        return (IKE_FAILURE);
    }

    /* Copy the first payload to Dummy payload for validation */
    IKE_MEMCPY ((UINT1 *) &Dummy, pu1PayLoad, sizeof (tGenericPayLoad));

    Dummy.u2PayLoadLen = IKE_NTOHS (Dummy.u2PayLoadLen);

    if ((Dummy.u2PayLoadLen > u4TotLen) || (Dummy.u2PayLoadLen == IKE_ZERO))
    {
        IKE_SET_ERROR (pSessionInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidPayLoad: Pakcet Length mismatch\n");
        return (IKE_FAILURE);
    }

    /* Store the payloads in the Payload array */

    if (MemAllocateMemBlock
        (IKE_PAYLOAD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkePayload) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidPayLoad: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pIkePayload == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidPayLoad: Malloc failed!\n");
        return (IKE_FAILURE);
    }
    IKE_BZERO (pIkePayload, sizeof (tIkePayload));
    pIkePayload->u2PayLoadType = *pu1PayloadType;
    pIkePayload->u2NxtPayLoadType = Dummy.u1NextPayLoad;
    pIkePayload->pRawPayLoad = pu1PayLoad;
    pIkePayload->u4PayLoadLen = Dummy.u2PayLoadLen;

    u4PayloadType = (UINT4) (*pu1PayloadType - IKE2_MINIMUM_PAYLOAD_ID);
    if ((u4PayloadType > IKE_ZERO) && (u4PayloadType <= IKE_MAX_PAYLOADS))
    {
        SLL_INSERT_IN_FRONT (&pSessionInfo->IkeRxPktInfo.
                             aPayload[u4PayloadType],
                             (t_SLL_NODE *) pIkePayload);
    }
    else
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ValidPayLoad: No. of Payloads Exceeds the"
                      "Max payload limit %d.\n", IKE_MAX_PAYLOADS);
        MemReleaseMemBlock (IKE_PAYLOAD_MEMPOOL_ID, (UINT1 *) pIkePayload);
        return (IKE_FAILURE);
    }

    /* If the payload is SA,  validate proposal and transform payloads too */
    if (*pu1PayloadType == IKE2_PAYLOAD_SA)
    {
        if (Ike2ValidSa (pSessionInfo, pu4CurLen, u4TotLen,
                         pu1PayLoad) == IKE_FAILURE)
        {
            return (IKE_FAILURE);
        }
    }
    else
    {
        *pu4CurLen += Dummy.u2PayLoadLen;
    }
    *pu1PayloadType = Dummy.u1NextPayLoad;
    return (IKE_SUCCESS);
}

/***********************************************************************/
/*  Function Name   : Ike2ValidSa                                      */
/*  Description     : Validates the SA payload                         */
/*                  :                                                  */
/*  Input(s)        : pSesInfo   - Pointer to the Session Info         */
/*                  : pu4CurLen  - processed length of the packet.     */
/*                  : u4TotalLen - Total length of the packet.         */
/*                  : pu1Packet  - packet array pointer.               */
/*                  :                                                  */
/*  Output(s)       : None.                                            */
/*  Return Values   : IKE_FAILURE or IKE_SUCCESS                       */
/***********************************************************************/
INT1
Ike2ValidSa (tIkeSessionInfo * pSesInfo, UINT4 *pu4CurLen,
             UINT4 u4TotalLen, UINT1 *pu1Packet)
{
    tIke2SaPayLoad      Sa;
    tIke2PropPayLoad    Prop;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4Len = IKE_ZERO;

    u4Len = (*pu4CurLen);
    IKE_MEMCPY ((UINT1 *) &Sa, pu1Packet, IKE2_SA_PAYLOAD_SIZE);

    /* Sa payload length */
    Sa.u2PayLoadLen = IKE_NTOHS (Sa.u2PayLoadLen);

    /* Compare the total length of the received packet and SA payload length. */
    if ((u4Len + Sa.u2PayLoadLen > u4TotalLen) || (Sa.u2PayLoadLen == IKE_ZERO))
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidSa: Packet Length mismatch\n");
        return (IKE_FAILURE);
    }

    /* If SA payload length is equal to the SA payload header length , 
     * SA payload is empty. So return failure*/
    if (Sa.u2PayLoadLen == IKE2_SA_PAYLOAD_SIZE)
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidSa: Empty SA payload\n");
        return (IKE_FAILURE);
    }

    /* Check that the total Payload Length is consistent
     * with the payload's internal lengths and counts */
    u4Len = IKE2_SA_PAYLOAD_SIZE;
    while (u4Len < Sa.u2PayLoadLen)
    {
        pu1Ptr = pu1Packet + u4Len;
        IKE_MEMCPY ((UINT1 *) &Prop, pu1Ptr, IKE2_PROP_PAYLOAD_SIZE);
        Prop.u2PayLoadLen = IKE_NTOHS (Prop.u2PayLoadLen);

        if ((Prop.u2PayLoadLen == IKE_ZERO) || (Prop.u1NumTran == IKE_ZERO))
        {
            IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ValidSa: Proposal payload zero or "
                         "zero trans\n");
            return (IKE_FAILURE);
        }

        /* For IKE_SA negotiation, Spi size field MUST be zero;
         * During subsequent negotiations, it is equal to the size, in
         * octets, of the SPI of the corresponding protocol (8 for IKE, 4
         * for ESP and AH).*/
        if ((Prop.u1SpiSize != IKE_IPSEC_SPI_SIZE) &&
            (Prop.u1SpiSize != 0) && (Prop.u1SpiSize != IKE_COOKIE_LEN))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ValidSa: Invalid SPI Size !\n");
            return (IKE_FAILURE);
        }

        pu1Ptr += (IKE2_PROP_PAYLOAD_SIZE + Prop.u1SpiSize);

        /* Check the length of a Proposal is consistent with the lengths
         * of all Transforms and Attributes it contains. */
        if (Ike2ValidTransform (pSesInfo,
                                (UINT2) (Prop.u2PayLoadLen -
                                         ((UINT2) sizeof (tIke2PropPayLoad) +
                                          (UINT2) Prop.u1SpiSize)), &pu1Ptr,
                                Prop.u1ProtocolId) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ValidSa: Ike2ValidTransform " "failed !\n");
            return (IKE_FAILURE);
        }
        u4Len += Prop.u2PayLoadLen;
    }
    if (u4Len != Sa.u2PayLoadLen)
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidSa: Invalid Sa payload length\n");
        return (IKE_FAILURE);
    }
    *pu4CurLen += Sa.u2PayLoadLen;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*   Function Name : Ike2ValidTransform                                 */
/*   Description   : Check the Transform payload validity.              */
/*                 :                                                    */
/*   Input(s)      : pSesInfo        - Pointer to the Session Info      */
/*                 : u2PropLen       - Propsal payload len.             */
/*                 : pu1IncommingPkt - Pointer to packet array          */
/*                 :                   pointer                          */
/*                 : u1ProtocolId    - ISAKMP/AH/ESP                    */
/*                 :                                                    */
/*   Output(s)     : None.                                              */
/*   Return Values : IKE_FAILURE or IKE_SUCCESS                         */
/************************************************************************/
INT1
Ike2ValidTransform (tIkeSessionInfo * pSesInfo, UINT2 u2PropLen,
                    UINT1 **pu1IncommingPkt, UINT1 u1ProtocolId)
{
    tIke2TransPayLoad   Trans;
    UINT1              *pu1Ptr = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT1               au1Trans[IKE2_MAX_TRANSFORM] = { IKE_ZERO };

    IKE_BZERO (au1Trans, IKE2_MAX_TRANSFORM);

    while (u4Len < u2PropLen)
    {
        /* Check the length of a Proposal is consistent with the lengths
         * of all Transforms and Attributes it contains. */
        pu1Ptr = *pu1IncommingPkt + u4Len;
        IKE_MEMCPY ((UINT1 *) &Trans, pu1Ptr, sizeof (tIke2TransPayLoad));
        Trans.u2TransformLen = IKE_NTOHS (Trans.u2TransformLen);
        Trans.u2TransformId = IKE_NTOHS (Trans.u2TransformId);
        if ((Trans.u2TransformLen > u2PropLen) ||
            (Trans.u2TransformLen == IKE_ZERO))
        {
            IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ValidTransform: Invalid transform "
                         "payload length!\n");
            return (IKE_FAILURE);
        }
        if (Trans.u1TransType < IKE2_MAX_TRANSFORM)
        {
            au1Trans[Trans.u1TransType]++;
        }
        u4Len = (u4Len + Trans.u2TransformLen);
    }
    if (u4Len != (UINT4) u2PropLen)
    {
        IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ValidTransform: Trans payload " "length mismatch\n");
        return (IKE_FAILURE);
    }
    /* Make the raw pkt point to the Next Proposal/Next 
     * Payload(In case only one proposal) */
    *pu1IncommingPkt = *pu1IncommingPkt + u4Len;

    /* Check all the mandatory parameters are present or not */
    switch (u1ProtocolId)
    {
        case IKE_ISAKMP:
        {
            /* For ISAKMP, IKE2_TRANSFORM_ENCR , IKE2_TRANSFORM_PRF, 
             * IKE2_TRANSFORM_INTEG and IKE2_TRANSFORM_DH are mandatory */
            if ((au1Trans[IKE2_TRANSFORM_ENCR] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_PRF] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_INTEG] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_DH] > IKE_ZERO))
            {
                return (IKE_SUCCESS);
            }
            break;
        }
        case IKE_IPSEC_ESP:
        {
            /* For ESP, IKE2_TRANSFORM_ENCR and IKE2_TRANSFORM_ESN 
             * are mandatory */
            if ((au1Trans[IKE2_TRANSFORM_ENCR] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_ESN] > IKE_ZERO))
            {
                return (IKE_SUCCESS);
            }
            break;
        }
        case IKE_IPSEC_AH:
        {
            /* For AH, IKE2_TRANSFORM_INTEG and IKE2_TRANSFORM_ESN are 
             * mandatory */
            if ((au1Trans[IKE2_TRANSFORM_INTEG] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_ESN] > IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_ENCR] == IKE_ZERO) &&
                (au1Trans[IKE2_TRANSFORM_PRF] == IKE_ZERO))
            {
                return (IKE_SUCCESS);
            }
            break;
        }
        default:
            break;
    }
    IKE_SET_ERROR (pSesInfo, IKE2_INVALID_SYNTAX);
    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "Ike2ValidTransform: Unsupported protocol "
                 "or Missing mandatory fields!\n");
    return (IKE_FAILURE);
}
#endif /* _IKE2_VAL_C_ */
