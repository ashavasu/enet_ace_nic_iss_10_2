/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ike2cons.c,v 1.17 2014/08/22 11:03:15 siva Exp $
 *
 * Description: This has functions for IKEv2 paylod construction Module
 *
 ***********************************************************************/

#ifndef _IKE2_CONS_C_
#define _IKE2_CONS_C_

#include "ikegen.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ike2inc.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :Ike2ConstructHeader                                */
/*  Description     :This function is used to construct Isakmp Header   */
/*                  :in an Isakmp Message                               */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1Exch        - Identifies the mode of Exchange    */
/*                  :u1NextPayLoad - Identifies the Successive payload  */
/*                  :in an Isakmp Message                               */
/*  Output(s)       :Appends the Isakmp Header in the Packet            */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructHeader (tIkeSessionInfo * pSesInfo,
                     UINT1 u1Exch, UINT1 u1NextPayLoad)
{
    tIsakmpHdr          Hdr;

    /* Initialize the Isakmp Header */
    IKE_BZERO ((UINT1 *) &Hdr, sizeof (tIsakmpHdr));

    /* Fill the Isakmp Header with the cookie */
    IKE_MEMCPY ((UINT1 *) &Hdr.au1InitCookie,
                (UINT1 *) pSesInfo->pIkeSA->au1InitiatorCookie, IKE_COOKIE_LEN);
    IKE_MEMCPY ((UINT1 *) &Hdr.au1RespCookie,
                (UINT1 *) pSesInfo->pIkeSA->au1ResponderCookie, IKE_COOKIE_LEN);

    /* Update Isakmp Header Major Version as 2 and Minor version as 0 */
    Hdr.u1Version |= IKE2_MINOR_VERSION;
    Hdr.u1Version |= IKE2_MAJOR_VERSION;

    /* Update Exchange type ,next payload type and message id */
    Hdr.u1Exch = u1Exch;
    Hdr.u1NextPayload = u1NextPayLoad;
    Hdr.u4MessId = IKE_HTONL (pSesInfo->u4MsgId);

    /* Update Initiator flag based on Original role (during Initial IKESA SA 
     * establishment)*/
    if (pSesInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
    {
        Hdr.u1Flags |= IKE2_FLAG_INITIATOR;
    }

    /* Update the Response flag based on the current Role 
     * (If Initiator - Request(Reset bit), else Response (Set bit))*/
    if (pSesInfo->u1Role == IKE_RESPONDER)
    {
        Hdr.u1Flags |= IKE2_FLAG_RESPONSE;
    }

    /* Allocate Space for the Isakmp Header in the Outgoing Isakmp Mesg */
    if (Ike2ConstructIsakmpMesg (pSesInfo, IKE_ZERO,
                                 sizeof (tIsakmpHdr)) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructHeader: Ike2ConstructIsakmpMesg failed to "
                     "allocate memory for Isakmp Header\n");
        return (IKE_FAILURE);
    }

    /* Copy the Isakmp Header to the PayLoad */
    IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt, (UINT1 *) &Hdr,
                sizeof (tIsakmpHdr));
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructIsakmpMesg                            */
/*                  :                                                   */
/*  Description     :This function is invoked to allocate the required  */
/*                  :memory  for the Isakmp Packet                      */
/*                  :                                                   */
/*  Input(s)        :pSesInfo   - Pointer to session info data base     */
/*                  :             which contains the outgoining packet  */
/*                  :i4Pos      - Specifies the position in the packet  */
/*                  :             where the payload is to be appended   */
/*                  :u4NumBytes - Specifies the Number of Bytes to be   */
/*                  :             in an Isakmp Message                  */
/*                  :                                                   */
/*  Output(s)       :Allocates memory for specified no of bytes         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructIsakmpMesg (tIkeSessionInfo * pSesInfo, INT4 i4Pos,
                         UINT4 u4NumBytes)
{

    if (pSesInfo->IkeTxPktInfo.u4BufLen > (UINT4) ((UINT4) i4Pos + u4NumBytes))
    {
        pSesInfo->IkeTxPktInfo.u4PacketLen += u4NumBytes;
        return (IKE_SUCCESS);
    }

    /* Reallocate Memory to the Payload with the specified
     * no of bytes at the said position */
    while (pSesInfo->IkeTxPktInfo.u4BufLen < ((UINT4) i4Pos + u4NumBytes))
    {
        pSesInfo->IkeTxPktInfo.u4BufLen += IKE_CHUNK_SIZE;
    }

    pSesInfo->IkeTxPktInfo.u4PacketLen += u4NumBytes;
    IKE_ASSERT (pSesInfo->IkeTxPktInfo.u4BufLen != IKE_ZERO);

    if (pSesInfo->IkeTxPktInfo.pu1RawPkt == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pSesInfo->IkeTxPktInfo.pu1RawPkt) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructIsakmpMesg: unable to allocate "
                         "buffer\n");
            return (IKE_FAILURE);
        }
        if (pSesInfo->IkeTxPktInfo.pu1RawPkt == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructIsakmpMesg: Realloc failed for "
                         "IkeTxPktInfo buffer\n");
            return (IKE_FAILURE);
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructSA                                    */
/*  Description     :This function is used to construct IKE SA Payload  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the session info        */
/*                  :                database which contains the        */
/*                  :                outgoing packet                    */
/*                  :u1NextPayLoad - Identifies the next paylaod in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Specifies the position in Isakmp   */
/*                  :                Mesg where the payload is to be    */
/*                  :                appended                           */
/*                  :                                                   */
/*  Output(s)       :Appends the SA PayLoad in the Isakmp Mesg          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructSA (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIke2SaPayLoad      Sa;

    INT4                i4SavePos = IKE_ZERO;
    INT4                i4FixPos = IKE_ZERO;
    INT4                i4NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &Sa, sizeof (tIke2SaPayLoad));

    /* Points to the SA Payload header which is used to update 
     * the SA payload header in the msg after adding proposal 
     * payload */
    i4SavePos = (INT4) *pu4Pos;

    /* Allocate Space for the Sa payload header in the Isakmp 
     * Mesg */
    if (Ike2ConstructIsakmpMesg (pSesInfo, i4SavePos,
                                 sizeof (tIke2SaPayLoad)) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructSA: Ike2ConstructIsakmpMesg failed to"
                     "allocate memory for SA payload header\n");
        return (IKE_FAILURE);
    }
    *pu4Pos += sizeof (tIke2SaPayLoad);

    /* Points to the Proposal payload header used to update
     * the proposal payload header after adding transforms */
    i4FixPos = (INT4) *pu4Pos;

    /* Fill next payload type and total length in the SA payload header */
    Sa.u1NextPayLoad = u1NextPayLoad;
    Sa.u2PayLoadLen = IKE_HTONS (sizeof (tIke2SaPayLoad));

    /* Copy SA payload header in the Isakmp message */
    IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos),
                (UINT1 *) &Sa, sizeof (tIke2SaPayLoad));

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        /* Construct the IKE proposal payload from the configured 
         * parameters */
        i4NBytes = Ike2SaConstructProposal (pSesInfo, i4FixPos, pu4Pos,
                                            IKE_ISAKMP, IKE_ONE,
                                            IKE_NONE_PAYLOAD, IKE_NONE);
        if (i4NBytes == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructSA: Ike2SaConstructProposal returned "
                         "zero bytes copied\n");
            return (IKE_FAILURE);
        }

        /* Update the new length in the SA payload header */
        Sa.u2PayLoadLen =
            (UINT2) IKE_HTONS (((UINT2) i4NBytes + sizeof (tIke2SaPayLoad)));
        IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                    IKE_SA_LEN_OFFSET, &Sa.u2PayLoadLen,
                    sizeof (Sa.u2PayLoadLen));
    }
    else
    {
        /* In case of Responder , SA payload is constructed at the time 
         * of processing of Offered SA.So just copy the constructed 
         * SA payload into the buffer update the SA payload length */
        if (Ike2ConstructIsakmpMesg (pSesInfo,
                                     sizeof (tIsakmpHdr) +
                                     sizeof (tIke2SaPayLoad),
                                     pSesInfo->u2SaSelectedLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructSA: Ike2ConstructIsakmpMesg failed"
                         "  to construct SA payload\n");
            return (IKE_FAILURE);
        }
        IKE_MEMCPY (pSesInfo->IkeTxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr)
                    + sizeof (tIke2SaPayLoad),
                    pSesInfo->pu1SaSelected, pSesInfo->u2SaSelectedLen);
        Sa.u2PayLoadLen = IKE_HTONS ((sizeof (tIke2SaPayLoad) +
                                      pSesInfo->u2SaSelectedLen));
        IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                     IKE_SA_LEN_OFFSET),
                    &Sa.u2PayLoadLen, sizeof (Sa.u2PayLoadLen));
        *pu4Pos += pSesInfo->u2SaSelectedLen;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructIpsecSA                               */
/*  Description     :This function is used to construct Ipsec SA        */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the session info        */
/*                  :u1NextPayLoad - Identifies the next paylaod in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Specifies the position in Isakmp   */
/*                                   Mesg where the payload is to be    */
/*                                   appended                           */
/*                  :                                                   */
/*  Output(s)       :Construct the Ipsec SA PayLoad                     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructIpsecSA (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                      UINT4 *pu4Pos)
{
    tIke2SaPayLoad      SaPayLoad;
    INT4                i4SavePos = IKE_ZERO;
    INT4                i4FixPos = IKE_ZERO;
    INT4                i4NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &SaPayLoad, sizeof (tIke2SaPayLoad));

    /* Points to the SA Payload header */
    i4SavePos = (INT4) *pu4Pos;

    /* Alllocate memory for SA payload header */
    if (Ike2ConstructIsakmpMesg (pSesInfo, i4SavePos, sizeof (tIke2SaPayLoad))
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructIpsecSA: Ike2ConstructIsakmpMesg failed for"
                     "SA payload header\n");
        return (IKE_FAILURE);
    }

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        *pu4Pos += sizeof (tIke2SaPayLoad);

        /* Points to the Proposal payload header */
        i4FixPos = (INT4) *pu4Pos;

        /* Construct the IKE proposal payload from the configured parameters */
        i4NBytes = Ike2SaConstructIpsecProposal (pSesInfo, i4FixPos, pu4Pos);
        if (i4NBytes == IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIpsecSA: Ike2SaConstructIpsecProposal"
                         " failed\n");
            return (IKE_FAILURE);
        }

    }
    else
    {
        /* In case of Responder , SA payload is constructed from the
         * SA payload constructed at the time of Offered SA processing */
        *pu4Pos += sizeof (tIke2SaPayLoad);
        if (Ike2ConstructIsakmpMesg
            (pSesInfo, (INT4) *pu4Pos,
             pSesInfo->u2SaSelectedLen) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeConstructIpsecSA: Ike2ConstructIsakmpMesg failed "
                         "to construct IPSEC SA payload\n");
            return (IKE_FAILURE);
        }

        IKE_MEMCPY ((pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos +
                     sizeof (tIke2SaPayLoad)), pSesInfo->pu1SaSelected,
                    pSesInfo->u2SaSelectedLen);
        i4NBytes = pSesInfo->u2SaSelectedLen;
        *pu4Pos = (UINT4) (*pu4Pos + (UINT4) i4NBytes);
    }

    /* Update the new length and next payload type in the SA payload header */
    SaPayLoad.u1NextPayLoad = u1NextPayLoad;
    SaPayLoad.u2PayLoadLen =
        (UINT2) ((UINT2) i4NBytes + sizeof (tIke2SaPayLoad));
    SaPayLoad.u2PayLoadLen = IKE_HTONS (SaPayLoad.u2PayLoadLen);

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + i4SavePos),
                (UINT1 *) &SaPayLoad, sizeof (tIke2SaPayLoad));

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructKE                                    */
/*  Description     :This function is used to construct the KeyExchange */
/*                  :payload in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :                DataBase                           */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Message                            */
/*                  :pu4Pos        - Specifies the position for the     */
/*                                   payload to be in the Packet        */
/*                  :                                                   */
/*  Output(s)       :Appends the KeyExchange Payload in the Message     */
/*                  :at the specified position                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructKE (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIke2KePayLoad      KE;
    UINT4               u4NBytes = IKE_ZERO;

    /* Check DH public and private keys are present in the session info.
     * If not create a DH key pair */
    if (pSesInfo->pMyDhPub == NULL)
    {
        pSesInfo->pMyDhPub = HexNumNew (pSesInfo->pDhInfo->u4Length);
        pSesInfo->pMyDhPrv = HexNumNew (pSesInfo->pDhInfo->u4Length);

        if ((pSesInfo->pMyDhPub == NULL) || (pSesInfo->pMyDhPrv == NULL))
        {
            IKE_ASSERT (pSesInfo->pMyDhPub == NULL);
            IKE_ASSERT (pSesInfo->pMyDhPrv == NULL);
            return (IKE_FAILURE);
        }

        if (FSDhGenerateKeyPair (pSesInfo->pDhInfo, pSesInfo->pMyDhPub,
                                 pSesInfo->pMyDhPrv) < IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructKE: Failed to Generate DH Key pair\n");
            return (IKE_FAILURE);
        }
    }

    /* Calculate the KE payload length  */
    u4NBytes =
        (UINT4) (sizeof (tIke2KePayLoad) +
                 (UINT2) pSesInfo->pMyDhPub->i4Length);

    /* Initialize the Key Exchange PayLoad */
    IKE_BZERO ((UINT1 *) &KE, sizeof (tIke2KePayLoad));

    /* Update the KE payload with Next payload,and DH Gorup and Total 
     * KE payload length */
    KE.u1NextPayLoad = u1NextPayLoad;
    KE.u2PayLoadLen = (UINT2) (u4NBytes);
    KE.u2PayLoadLen = IKE_HTONS (KE.u2PayLoadLen);
    KE.u2DhGroup = (UINT2) pSesInfo->pDhInfo->u1Group;
    KE.u2DhGroup = IKE_HTONS (KE.u2DhGroup);

    /* Allocate Space for the KE payload Isakmp Message */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructKE: Ike2ConstructIsakmpMesg failed "
                     "to allocate space for the KE payload\n");
        return (IKE_FAILURE);
    }

    /* Copy the KE PayLoad header to the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &KE, sizeof (tIke2KePayLoad));
    *pu4Pos += sizeof (tIke2KePayLoad);

    /* Copy Diffie-Hellman public value into the "Key Exchange Data" 
     * portion of the payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) pSesInfo->pMyDhPub->pu1Value,
                pSesInfo->pMyDhPub->i4Length);
    *pu4Pos = (UINT4) (*pu4Pos + (UINT4) pSesInfo->pMyDhPub->i4Length);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructNonce                                 */
/*  Description     :This functions is used to construct the Nonce      */
/*                  :PayLoad in Message                                 */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextpayLoad - Identifies the next PayLoad in     */
/*                  :                the Message                        */
/*                  :pu4Pos        - Specifies the position in the Mesg */
/*                  :                where the payload can be appended  */
/*                  :                                                   */
/*  Output(s)       :Appends the Nonce PayLoad in the Message at the    */
/*                  :specified position                                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructNonce (tIkeSessionInfo * pSesInfo,
                    UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    UINT1               au1MyNonce[IKE2_NONCE_LEN] = { IKE_ZERO };
    tIke2NoncePayLoad   Nonce;

    /* Initialize the Nonce payload and au1MyNonce */
    IKE_BZERO ((UINT1 *) &Nonce, sizeof (tIke2NoncePayLoad));
    IKE_BZERO (au1MyNonce, IKE2_NONCE_LEN);

    /* Generate the Nonce Randomly */
    /* Nonce Length must be atleast 16 bytes or half the length of 
     * negotiated PRF.Here Nonce Length is taken as IKE2_NONCE_LEN(32) 
     * which is long enough for all the PRF's */
    IkeGetRandom (au1MyNonce, IKE2_NONCE_LEN);

    /* The fourth param is to tell the function whether to set the 
     * Initiator or Responder Nonce */
    if (Ike2UtilSetNonceInSessionInfo (pSesInfo, au1MyNonce, IKE2_NONCE_LEN,
                                       pSesInfo->u1Role) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructNonce: Nonce updation to the session "
                     "structure failed\n");
        return (IKE_FAILURE);
    }

    /* Update the Nonce payload with Next payload  and Total 
     * Nonce payload length */
    Nonce.u1NextPayLoad = u1NextPayLoad;
    Nonce.u2PayLoadLen = (UINT2) (IKE2_NONCE_LEN + sizeof (tIke2NoncePayLoad));
    Nonce.u2PayLoadLen = IKE_HTONS (Nonce.u2PayLoadLen);

    /* Allocate Space for the Nonce payload in Isakmp Message */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (IKE2_NONCE_LEN +
                                                            sizeof
                                                            (tIke2NoncePayLoad)))
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructNonce: Ike2ConstructIsakmpMesg failed "
                     "for Nonce payload\n");
        return (IKE_FAILURE);
    }

    /* Copy the Nonce PayLoad header in the Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Nonce, sizeof (tIke2NoncePayLoad));
    *pu4Pos += sizeof (tIke2NoncePayLoad);

    /* Copy the generated nonce to the Nonce payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) au1MyNonce, IKE2_NONCE_LEN);
    *pu4Pos += IKE2_NONCE_LEN;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructID                                    */
/*  Description     :This function is used to construct ID in the       */
/*                  :message                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in     */
/*                  :the Message                                        */
/*                  :pu4Pos        - Pointer to the Position in         */
/*                  :                the message where this payload is  */
/*                  :                to be appended                     */
/*                  :                                                   */
/*  Output(s)       :Appends the ID paylaod at the specified position in*/
/*                  :the Message                                        */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructID (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tIke2IdPayLoad      ID;
    tIkePhase1ID        IdValue;
    INT1                i1DerId[IKE_MAX_DER_ID_LEN];
    INT4                i4NBytes = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &ID, sizeof (tIke2IdPayLoad));
    IKE_BZERO (i1DerId, IKE_MAX_DER_ID_LEN);

    if (pSesInfo->pIkeSA->InitiatorID.i4IdType == IKE_IPSEC_ID_DER_ASN1_DN)
    {
        if (IkeConvertStrToDER
            ((INT1 *) &pSesInfo->pIkeSA->InitiatorID.uID.au1Dn, i1DerId,
             &i4Len) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructID: Converting string to DN format Failed\n");
            return (IKE_FAILURE);
        }

        IdValue.i4IdType = IKE_IPSEC_ID_DER_ASN1_DN;
        IdValue.u4Length = (UINT4) i4Len;
    }
    else
    {
        /* Always our Id will be present in pIkeSA->InitiatorID */
        IKE_MEMCPY (&IdValue, &pSesInfo->pIkeSA->InitiatorID,
                    sizeof (tIkePhase1ID));
    }
    i4NBytes = (INT4) (sizeof (tIke2IdPayLoad) + (UINT2) IdValue.u4Length);

    if (IdValue.i4IdType == IKE_IPSEC_ID_IPV4_ADDR)
    {
        IdValue.uID.Ip4Addr = IKE_HTONL (IdValue.uID.Ip4Addr);
    }

    /* Fill the ID payload with Next payload, Total ID payload 
     * length and id type */
    ID.u1NextPayLoad = u1NextPayLoad;
    ID.u2PayLoadLen = (UINT2) IKE_HTONS (i4NBytes);
    ID.u1IdType = (UINT1) IdValue.i4IdType;

    /* Allocate Space for the ID payload Isakmp Message */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) i4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructID: Ike2ConstructIsakmpMesg failed "
                     "ID payload\n");
        return (IKE_FAILURE);
    }

    /* pu1IdPayloadSent to be used in construction of Auth Payload */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1IdPayloadSent) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructID: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pSesInfo->pu1IdPayloadSent == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "Ike2ConstructID: Memory allocation failed to "
                     " initialize ID payload sent\n");
        return (IKE_FAILURE);
    }

    /* Copy the ID payload to the Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &ID, sizeof (tIke2IdPayLoad));

    /* Copy ID Payload header pu1IdPayloadSent */
    IKE_MEMCPY (pSesInfo->pu1IdPayloadSent,
                (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos +
                 sizeof (tGenericPayLoad)),
                (sizeof (tIke2IdPayLoad) - sizeof (tGenericPayLoad)));

    /* Advance the pos */
    *pu4Pos += sizeof (tIke2IdPayLoad);

    if (pSesInfo->pIkeSA->InitiatorID.i4IdType == IKE_IPSEC_ID_DER_ASN1_DN)
    {
        /* Copy the ID Value to Message */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) (i1DerId), IdValue.u4Length);

        /* Copy ID Value to pu1IdPayloadSent and set the length */
        IKE_MEMCPY ((pSesInfo->pu1IdPayloadSent + sizeof (tIke2IdPayLoad) -
                     sizeof (tGenericPayLoad)),
                    (UINT1 *) (i1DerId), IdValue.u4Length);

    }
    else
    {
        /* Copy the ID Value to Message */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) (&IdValue.uID), IdValue.u4Length);

        /* Copy ID Value to pu1IdPayloadSent and set the length */
        IKE_MEMCPY ((pSesInfo->pu1IdPayloadSent + sizeof (tIke2IdPayLoad) -
                     sizeof (tGenericPayLoad)),
                    (UINT1 *) (&IdValue.uID), IdValue.u4Length);
    }
    pSesInfo->u4IdPayloadSentLen =
        ((UINT2) i4NBytes - sizeof (tGenericPayLoad));

    *pu4Pos += IdValue.u4Length;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructCertReq                               */
/*  Description     :This functions is used to construct the CertReq    */
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextpayLoad - Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos        - Specifies the position in an Isakmp*/
/*                  :                Mesg where the payload can be      */
/*                  :                appended                           */
/*                  :                                                   */
/*  Output(s)       :Appends the cert request PayLoad in an Isakmp Mesg */
/*                  : at the specified position                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructCertReq (tIkeSessionInfo * pSesInfo,
                      UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tCertReqPayLoad     CertReq;
    tIkeCertDB         *pCertNode = NULL;
    tIkePKey           *pPubKey = NULL;
    UINT1              *pu1PublicKey = NULL;
    INT4                i4Len = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;
    UINT1               au1PubKeyHash[IKE_SHA_LENGTH + IKE_ONE] = { IKE_ZERO };

    IKE_BZERO (au1PubKeyHash, (IKE_SHA_LENGTH + IKE_ONE));

    TAKE_IKE_SEM ();
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ConstructCertReq: No IkeEngine Available with "
                      "Index %d\n", pSesInfo->pIkeSA->u4IkeEngineIndex);
        return (IKE_FAILURE);
    }
    GIVE_IKE_SEM ();

    /* Retrive the CA Certificate from the information available
     * in the Session info */
    pCertNode = IkeGetCertFromCaCerts (pIkeEngine->au1EngineName,
                                       pSesInfo->MyCert.pCAName,
                                       pSesInfo->MyCert.pCASerialNum);
    if (pCertNode == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCertReq: No CA certificate present\n");
        return (IKE_FAILURE);
    }

    /*Retrive the Public key from the CA Certificate */
    pPubKey = IkeGetX509PubKey (pCertNode->pCert);
    if (pPubKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCertReq: Failed to get public key "
                     "from cert\n");
        return (IKE_FAILURE);
    }

    /*Convert the Public key to DER format */
    i4Len = IkeConvertPublicKeyToDER (pPubKey, &pu1PublicKey);
    if (i4Len <= IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCertReq: Failed to convert  public key "
                     "from PEM to DER format\n");
        if (pu1PublicKey != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1PublicKey);
        }
        return (IKE_FAILURE);
    }
    IKE_BZERO (au1PubKeyHash, (IKE_SHA_LENGTH + IKE_ONE));

    /*Generate the Hash of the CA Public Key   */
    IkeSha1HashAlgo (pu1PublicKey, i4Len, au1PubKeyHash);

    IKE_BZERO (&CertReq, sizeof (tCertReqPayLoad));

    /* Fill the Certificate Request Payload */
    CertReq.u1NextPayLoad = u1NextPayLoad;
    CertReq.u2PayLoadLen = IKE2_CERT_PAYLOAD_SIZE + IKE_SHA_LENGTH;
    CertReq.u2PayLoadLen = IKE_HTONS (CertReq.u2PayLoadLen);
    CertReq.u1EncodeType = IKE2_X509_SIGNATURE;

    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos,
                                 IKE2_CERT_PAYLOAD_SIZE + IKE_SHA_LENGTH)
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertReq: Ike2ConstructIsakmpMesg failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1PublicKey);
        return (IKE_FAILURE);
    }

    /* Copy the CertReq PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &CertReq, IKE2_CERT_PAYLOAD_SIZE);
    *pu4Pos += IKE_CERT_PAYLOAD_SIZE;

    /*Copy the PubKey Hash into the TxPkt Buffer */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                au1PubKeyHash, IKE_SHA_LENGTH);
    *pu4Pos += IKE_SHA_LENGTH;

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1PublicKey);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructCert                                  */
/*  Description     :This functions is used to construct the Certificate*/
/*                  :PayLoad in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextpayLoad - Identifies the next PayLoad in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos        - Specifies the position in an Isakmp*/
/*                  :                Mesg where the payload can be      */
/*                  :                appended                           */
/*                  :                                                   */
/*  Output(s)       :Appends the certificate PayLoad in an Isakmp Mesg  */
/*                  :at the specified position                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructCert (tIkeSessionInfo * pSesInfo,
                   UINT1 u1NextPayLoad, UINT4 *pu4Pos)
{
    tCertPayLoad        Cert;
    tIkeCertDB         *pCertNode = NULL;
    UINT1              *pu1Cert = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4CertLen = IKE_ZERO;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "Ike2ConstructCert: No IkeEngine Available with "
                      "Index %d\n", pSesInfo->pIkeSA->u4IkeEngineIndex);
        return (IKE_FAILURE);
    }
    GIVE_IKE_SEM ();

    /* Get the certifiaate which needs to be sent to peer */
    pCertNode = IkeGetCertFromMyCerts (pIkeEngine->au1EngineName,
                                       pSesInfo->MyCert.pCAName,
                                       pSesInfo->MyCert.pCASerialNum);

    if (pCertNode == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCert: "
                     "No certificate with default cert information\n");
        return (IKE_FAILURE);
    }

    /* Convert the certificate to DER Format */
    if (IkeConvertX509ToDER (pCertNode->pCert, &pu1Cert, &u4CertLen) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCert: Unable to convert to DER format\n");
        if (pu1Cert != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Cert);
        }
        return (IKE_FAILURE);
    }

    IKE_BZERO (&Cert, sizeof (tCertPayLoad));

    /* Update the Certificate payload */
    Cert.u1NextPayLoad = u1NextPayLoad;

    /*Length to be updated with Payload Length + Certificate Length */
    Cert.u2PayLoadLen = (UINT2) (IKE2_CERT_PAYLOAD_SIZE + u4CertLen);
    Cert.u2PayLoadLen = IKE_HTONS (Cert.u2PayLoadLen);
    Cert.u1EncodeType = IKE2_X509_SIGNATURE;

    /*Allocate memory to accomodate the Certificate payload and Certificate */
    if (Ike2ConstructIsakmpMesg
        (pSesInfo, (INT4) *pu4Pos,
         IKE2_CERT_PAYLOAD_SIZE + u4CertLen) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConstructCertificate: Ike2ConstructIsakmpMesg "
                     "failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Cert);
        return (IKE_FAILURE);
    }

    /* Copy the Certificate PayLoad in the Isakmp Message */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Cert, IKE2_CERT_PAYLOAD_SIZE);
    *pu4Pos += IKE_CERT_PAYLOAD_SIZE;

    /* Copy the DER formatted certificate to the payload */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                pu1Cert, u4CertLen);
    *pu4Pos += u4CertLen;

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Cert);
    return (IKE_SUCCESS);
}

/*************************************************************************/
/*  Function Name   :Ike2ConstructAuth                                   */
/*  Description     :This function is used to construct the              */
/*                  :authentication payload in an Isakmp Mesg            */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info         */
/*                  :u1NextpayLoad - Identifies the next PayLoad in an   */
/*                  :                Isakmp Message                      */
/*                  :pu4Pos        - Specifies the position in an Isakmp */
/*                  :                Mesg where the payload can be       */
/*                  :                appended                            */
/*                  :                                                    */
/*  Output(s)       :Appends the Auth PayLoad in the Message             */
/*                  :                                                    */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                          */
/*                  :                                                    */
/*************************************************************************/

INT1
Ike2ConstructAuth (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                   UINT4 *pu4Pos)
{
    tIke2AuthPayLoad    Auth;
    UINT1              *pu1AuthData = NULL;
    UINT4               u4NBytes = IKE_ZERO;
    UINT4               u4HashLen = IKE_MAX_DIGEST_SIZE;
    UINT4               u4AuthDataLen = IKE_ZERO;
    UINT1               au1AuthDigest[IKE_MAX_DIGEST_SIZE] = { IKE_ZERO };
    tIkeEngine         *pIkeEngine = NULL;

    IKE_BZERO ((UINT1 *) &Auth, sizeof (tIke2AuthPayLoad));

    u4NBytes = sizeof (tIke2AuthPayLoad);

    if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_PSK)
    {
        u4HashLen =
            gaIkeHmacAlgoList[pSesInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;
    }
    else if ((pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA) ||
             (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA))
    {
        TAKE_IKE_SEM ();
        pIkeEngine = IkeGetIkeEngineFromIndex (pSesInfo->u4IkeEngineIndex);
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2AuthConstructDigest: Engine not found\n");
            return (IKE_FAILURE);
        }
        GIVE_IKE_SEM ();

        if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_RSA)
        {
            if (IkeRsaGetKeyLength (pIkeEngine, &pSesInfo->MyCert, &u4HashLen)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ConstructAuth: IkeGetRSAKeyWithCertInfo"
                             " fails\n");
                return (IKE_FAILURE);
            }
        }
        else if (pSesInfo->pIkeSA->u2AuthMethod == IKE2_AUTH_DSA)
        {
            if (IkeDsaGetKeyLength (pIkeEngine, &pSesInfo->MyCert, &u4HashLen)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ConstructAuth: IkeGetDSAKeyWithCertInfo"
                             " fails\n");
                return (IKE_FAILURE);
            }
        }
    }
    u4NBytes += u4HashLen;

    /* Compute the Auth message. The fourth param is to tell the function 
     * whether to compute the Initiator or Responder Auth message*/
    if (Ike2AuthConstructhMsg (pSesInfo, &pu1AuthData,
                               &u4AuthDataLen,
                               IKE2_CONSTRUCT_AUTH) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructAuth: Compute Auth message failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);
        return (IKE_FAILURE);
    }
    if (pu1AuthData == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructAuth: Auth message is NULL\n");
        return (IKE_FAILURE);
    }

    /* Compute Auth digest value */
    if (Ike2AuthConstructDigest (pSesInfo, pu1AuthData, u4AuthDataLen,
                                 au1AuthDigest) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructAuth: Compute Auth digest failed\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);
        return (IKE_FAILURE);
    }

    /* This memory allocated inside Ike2AuthConstructhMsg function */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1AuthData);

    /* Update total payload length ,next payload type and authentication 
     * method */
    Auth.u1NextPayLoad = u1NextPayLoad;
    Auth.u2PayLoadLen = (UINT2) u4NBytes;
    Auth.u2PayLoadLen = IKE_HTONS (Auth.u2PayLoadLen);
    Auth.u1AuthMethod = (UINT1) pSesInfo->pIkeSA->u2AuthMethod;

    /* Allocate Space for the Auth payload in Isakmp Message */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, u4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructAuth: Ike2ConstructIsakmpMesg failed "
                     "for Auth payload\n");
        return (IKE_FAILURE);
    }
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Auth, sizeof (tIke2AuthPayLoad));
    *pu4Pos += sizeof (tIke2AuthPayLoad);

    /* Copy the computed digest value to the Isakmp after Auth Payload 
     * header */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                au1AuthDigest, u4HashLen);
    *pu4Pos += u4HashLen;

    return (IKE_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Ike2ConstructEncryptPayLoad                        */
/*  Description   : Fucntion the encrypt the ISAKMP payload            */
/*                                                                     */
/*  Input(s)      : pSessionInfo - Session Info Pointer.               */
/*                : u1NextPayLoad - Identifies the Next PayLoad in the */
/*                : the Message                                        */
/*                : pu4Pos        - Pointer to the position in the     */
/*                :                 msg where the paylaod is to        */
/*                :                 be appended                        */
/*                :                                                    */
/*  Output(s)     : Constructs the Encrypted payload                   */
/*                :                                                    */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE.                        */
/***********************************************************************/
INT1
Ike2ConstructEncryptPayLoad (tIkeSessionInfo * pSessionInfo)
{
    UINT1              *pu1LocalPtr = NULL;
    UINT1              *pu1TempRawPkt = NULL;
    UINT1              *pSKeyIdA = NULL;
    tIsakmpHdr         *pHdr = NULL;
    tIke2EncrPayload    EncrPayload;
    UINT1              *pu1Hash = NULL;
    UINT1               u1PadLen = IKE_ZERO;
    UINT4               u4HashLen = IKE_ZERO;
    UINT4               u4DigestLen = IKE_ZERO;
    UINT4               u4IVLen = IKE_ZERO;
    UINT4               u4Temp = IKE_ZERO;
    UINT4               u4Len = IKE_ZERO;
    UINT4               u4TotLen = IKE_ZERO;
    unUtilAlgo          UtilAlgo;
    unArCryptoKey       ArCryptoKey;

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    IKE_BZERO ((UINT1 *) &EncrPayload, sizeof (tIke2EncrPayload));

    if (pSessionInfo->pIkeSA->pu1FinalPhase1IV == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: IV is null\n");
        return (IKE_FAILURE);
    }

    /*Total packet length */
    u4Len = IKE_NTOHL
        (((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
         u4TotalLen);

    /* Allocate a new buffer and copy the full packet */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pu1TempRawPkt) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: unable to allocate "
                     "buffer\n");
        return (IKE_FAILURE);
    }
    if (pu1TempRawPkt == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: Malloc Failed to "
                     "initialize memory\n");
        return (IKE_FAILURE);
    }
    IKE_MEMCPY (pu1TempRawPkt, pSessionInfo->IkeTxPktInfo.pu1RawPkt, u4Len);
    pHdr = (tIsakmpHdr *) (void *) pu1TempRawPkt;

    /*Calculate IV length */
    u4IVLen = gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
        u4BlockLen;

    /*Calculate Hash length */
    u4HashLen =
        gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    u4DigestLen = (UINT4)
        Ike2GetDigestLenFromAlgo (pSessionInfo->pIkeSA->u1Phase1HashAlgo);

    /* Calculate padding length .Padding MUST have a length that makes the 
     * combination of the Payloads, the Padding, and the Pad Length (1 Byte) 
     * to be  a multiple of the encryption block size */

    u4Temp = (u4Len - sizeof (tIsakmpHdr) + IKE_ONE) %
        gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    if (u4Temp != IKE_ZERO)
    {
        u1PadLen =
            (UINT1) (gaIke2CipherAlgoList
                     [pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen -
                     u4Temp);
    }

/* Total length of the Encrypted pkt = 
 * Received pkt len(embedded Payloads) + Encryption payload header  len +
 * IV len + padding bytes + pad length (1 Byte) + u4DigestLen*/

    u4TotLen =
        u4Len + sizeof (tIke2EncrPayload) + u4IVLen + u1PadLen + IKE_ONE
        + u4DigestLen;
    /* Allocate memory for new length and reset the Raw pkt Buffer */
    if (Ike2ConstructIsakmpMesg (pSessionInfo, (INT4) u4Len, (u4TotLen - u4Len))
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: Ike2ConstructIsakmpMesg "
                     "failed\n");
        IKE_BZERO (pu1TempRawPkt, u4Len);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TempRawPkt);
        return (IKE_FAILURE);
    }
    IKE_BZERO (pSessionInfo->IkeTxPktInfo.pu1RawPkt, u4TotLen);

/* Modify the total length and u1NextPayLoad in the ISAKMP Header */
    EncrPayload.u1NextPayLoad = pHdr->u1NextPayload;
    pHdr->u1NextPayload = IKE2_PAYLOAD_ENCRYPTED;
    pHdr->u4TotalLen = IKE_HTONL (u4TotLen);

/* Copy ISAKMP Header to the Raw packet */
    IKE_MEMCPY (pSessionInfo->IkeTxPktInfo.pu1RawPkt, pu1TempRawPkt,
                sizeof (tIsakmpHdr));
    u4TotLen = sizeof (tIsakmpHdr);

/* Update the Encrypted payload header and copy to the Raw packet 
 * (Encr Hdr + IVLen + Encr Data (Embedded Payloads + PaddingBytes + 
 * PadLen(1 Bytes)) + DigestLen */
    EncrPayload.u2PayLoadLen = (UINT2) (sizeof (tIke2EncrPayload) + u4IVLen +
                                        (u4Len - sizeof (tIsakmpHdr)) +
                                        u1PadLen + IKE_ONE + u4DigestLen);
    EncrPayload.u2PayLoadLen = IKE_HTONS (EncrPayload.u2PayLoadLen);

    IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + u4TotLen),
                &EncrPayload, sizeof (tIke2EncrPayload));
    u4TotLen += sizeof (tIke2EncrPayload);

/* Copy the IV to the Raw packet */
    IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + u4TotLen),
                pSessionInfo->pIkeSA->pu1FinalPhase1IV, u4IVLen);
    u4TotLen += u4IVLen;

/* Copy the data from the temp buffer to Raw pkt and release 
 * the temp buffer */
    IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + u4TotLen),
                (pu1TempRawPkt + sizeof (tIsakmpHdr)),
                (u4Len - sizeof (tIsakmpHdr)));
    u4TotLen += (u4Len - sizeof (tIsakmpHdr));

    /* Release the temporory buffer */
    IKE_BZERO (pu1TempRawPkt, u4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TempRawPkt);

/* Copy padding length to the Buffer */
    if (u1PadLen != IKE_ZERO)
    {
        IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + u4TotLen +
                     u1PadLen), &u1PadLen, sizeof (UINT1));
    }

/* Update the total length with the number of padding bytes + padding 
 * lenth (1 Byte) */
    u4TotLen = u4TotLen + u1PadLen + IKE_ONE;

/* Calculate the length of data to be encrypted.
 * Encrypted data length = encryption header + combination of the Payloads + 
 * Padding + Pad Length */
    u4Len = u4TotLen - (sizeof (tIsakmpHdr) + sizeof (tIke2EncrPayload) +
                        u4IVLen);

    pu1LocalPtr =
        pSessionInfo->IkeTxPktInfo.pu1RawPkt + sizeof (tIsakmpHdr) +
        sizeof (tIke2EncrPayload) + u4IVLen;

/*Call appropriate encryption algorithm to encrypt the packet */
    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1LocalPtr;
    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Len;
    UtilAlgo.UtilDesAlgo.pu1DesInitVect =
        pSessionInfo->pIkeSA->pu1FinalPhase1IV;
    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
        gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4KeyLen;
    UtilAlgo.UtilDesAlgo.u4DesInitVectLen =
        gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
    switch (pSessionInfo->pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE2_ENCR_DES:
        {

            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
                UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            }
            else
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes1,
                            sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
                UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);
            }
            break;
        }

        case IKE2_ENCR_3DES:
        {
            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey1, sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey2, sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                            pSessionInfo->pIkeSA->DesKey3, sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                    (pSessionInfo->pIkeSA->pSKeyIdE + IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                    (pSessionInfo->pIkeSA->pSKeyIdE +
                     IKE_TWO * IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            }
            else
            {
                if (gu1IkeBypassCrypto == BYPASS_ENABLED)
                {
                    break;
                }

                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes1,
                            sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes2,
                            sizeof (tDesKey));
                IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                            pSessionInfo->pIkeSA->Ike2SA.DesKeyRes3,
                            sizeof (tDesKey));

                UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                    (pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr +
                     IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                    (pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr +
                     IKE_TWO * IKE_DES_KEY_LEN);
                UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);
            }
            break;
        }

        case IKE2_ENCR_AES_CBC:
        {
            if (gu1IkeBypassCrypto == BYPASS_ENABLED)
            {
                break;
            }

            if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
            {
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSessionInfo->pIkeSA->pSKeyIdE;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                             IKE_EIGHT);
            }
            else
            {
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdEr;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (((UINT1) (pSessionInfo->pIkeSA->u2EncrKLen)) *
                             IKE_EIGHT);
            }

            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1LocalPtr;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Len;
            UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                pSessionInfo->pIkeSA->au1AesEncrKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector =
                pSessionInfo->pIkeSA->pu1FinalPhase1IV;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                gaIke2CipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
                u4BlockLen;
            UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;

            UtilEncrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo);
            break;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2ConstructEncryptPayLoad: Encryption Algo "
                         "unknown/not supported\n");
            return (IKE_FAILURE);
        }
    }

/* Modify the u4len to calculate the digest */
/* Integrity data length = ISAKMP Header + Encryption paayload header + 
 * IV + combination of the Payloads + Padding + the Pad Length */
    u4Len += (sizeof (tIsakmpHdr) + sizeof (tIke2EncrPayload) + u4IVLen);
    pu1LocalPtr = pSessionInfo->IkeTxPktInfo.pu1RawPkt;

    if (pSessionInfo->pIkeSA->Ike2SA.bIsOrgInitiator == IKE_TRUE)
    {
        pSKeyIdA = pSessionInfo->pIkeSA->pSKeyIdA;
    }
    else
    {
        pSKeyIdA = pSessionInfo->pIkeSA->Ike2SA.au1SKeyIdAr;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Hash) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if ((*gaIkeHmacAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HmacAlgo)
        (pSKeyIdA, (INT4) u4HashLen, pu1LocalPtr, (INT4) u4Len,
         pu1Hash) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: Unsupported Hash "
                     "algorithm\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }

    /* First 12 byte of the Hash output will be the checksum data */
    IKE_MEMCPY ((pu1LocalPtr + u4Len), pu1Hash, u4DigestLen);

    if (Ike2UtilCheckKBTrasmitted (pSessionInfo, IKE_ENCRYPTION) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructEncryptPayLoad: Ike2UtilCheckKBTrasmitted "
                     "failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
        return (IKE_FAILURE);
    }
    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1Hash);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructNotify                                */
/*  Description     :This function is used to construct notify paylaod  */
/*                  :in an Isakmp Mesg                                  */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :pNotifyInfo   - pointer to notification info       */
/*                  :                                                   */
/*  Output(s)       :Appends the Notify payload in an Isakmp Message    */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructNotify (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                     UINT4 *pu4Pos, tIkeNotifyInfo * pNotifyInfo)
{
    tIke2NotifyPayLoad  Notify;
    INT4                i4NBytes = IKE_ZERO;

    i4NBytes =
        (INT4) (sizeof (tIke2NotifyPayLoad) + pNotifyInfo->u2NotifyDataLen);
    IKE_BZERO ((UINT1 *) &Notify, sizeof (tIke2NotifyPayLoad));

    /*  Fill the Notify PayLoad with the Recieved values */
    Notify.u1NextPayLoad = u1NextPayLoad;
    Notify.u1SpiSize = pNotifyInfo->u1SpiSize;
    Notify.u1ProtocolId = pNotifyInfo->u1Protocol;
    Notify.u2NotifyMsgType = IKE_HTONS (pNotifyInfo->u2NotifyType);
    Notify.u2PayLoadLen = (UINT2) IKE_HTONS (i4NBytes);

    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) i4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructNotify: Ike2ConstructNotify failed\n");
        return (IKE_FAILURE);
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Notify, sizeof (tIke2NotifyPayLoad));
    *pu4Pos += sizeof (tIke2NotifyPayLoad);

    /* Copy the Error or Informational Mesg Recieved in addition to
     * Notify Mesg */

    if ((pNotifyInfo->pu1NotifyData != NULL) &&
        (pNotifyInfo->u2NotifyDataLen != IKE_ZERO))
    {
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) pNotifyInfo->pu1NotifyData,
                    pNotifyInfo->u2NotifyDataLen);

        *pu4Pos += pNotifyInfo->u2NotifyDataLen;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructDelete                                */
/*  Description     :This function is used to construct the Delete      */
/*                  :payload in an Isakmp Message                       */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :                                                   */
/*  Output(s)       :Appends the Delete Payload in the Message          */
/*                  :at the specified position                          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructDelete (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                     UINT4 *pu4Pos, tIkeNotifyInfo * pDeleteInfo)
{
    tIke2DeletePayLoad  DelPayload;

    IKE_BZERO ((UINT1 *) &DelPayload, sizeof (tIke2DeletePayLoad));

    /* Fill the Delete PayLoad with the Recieved values */
    DelPayload.u1NextPayLoad = u1NextPayLoad;
    DelPayload.u2PayLoadLen = (UINT2)
        (sizeof (tIke2DeletePayLoad) + pDeleteInfo->u2NotifyDataLen);
    DelPayload.u1ProtocolId = pDeleteInfo->u1Protocol;
    DelPayload.u1SpiSize = pDeleteInfo->u1SpiSize;
    DelPayload.u2SpiCount = IKE_HTONS (pDeleteInfo->u2SpiNum);

    DelPayload.u2PayLoadLen = IKE_HTONS (DelPayload.u2PayLoadLen);

    if (Ike2ConstructIsakmpMesg
        (pSesInfo, (INT4) *pu4Pos,
         (sizeof (tIke2DeletePayLoad) + (pDeleteInfo->u2NotifyDataLen))) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructDelete: Memory allocation failed\n");
        return (IKE_FAILURE);
    }

    /* Copy the delete payload to the Isakmp mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &DelPayload, sizeof (tIke2DeletePayLoad));
    *pu4Pos += sizeof (tIke2DeletePayLoad);

    if (pDeleteInfo->pu1NotifyData != NULL)
    {
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) pDeleteInfo->pu1NotifyData,
                    pDeleteInfo->u2NotifyDataLen);
        *pu4Pos += pDeleteInfo->u2NotifyDataLen;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructCfg                                   */
/*  Description     :This function is used to copnstruct the config     */
/*                   payload                                            */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :u1CfgType - REQ/RPLY/ACK/SET                       */
/*  Output(s)       :constructs the config payload                      */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2ConstructCfg (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                  UINT4 *pu4Pos, UINT1 u1CfgType)
{
    tIke2ConfigPayLoad  Cfg;
    INT4                i4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    INT4                i4AttsLen = IKE_ZERO;
    tIkeIpAddr          IntAddress;
    UINT4               u4IntNetmask = IKE_ZERO;

    IKE_BZERO (&Cfg, sizeof (tIke2ConfigPayLoad));

    /* Fill next payload type and Config type */
    Cfg.u1NextPayLoad = u1NextPayLoad;
    Cfg.u1CfgType = u1CfgType;

    /*  Allocate  Memory for Config PayLoad */
    if (Ike2ConstructIsakmpMesg
        (pSesInfo, (INT4) *pu4Pos,
         (sizeof (tIke2ConfigPayLoad))) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCfg: Memory allocation failed to "
                     "allocate memory for config PayLoad\n");
        return (IKE_FAILURE);
    }

    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCfg: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (pu1Atts == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCfg: Memory allocation for "
                     "attributes failed\n");
        return (IKE_FAILURE);
    }
    i4AttsLen = IKE_MAX_ATTR_SIZE;

    /* RAVPN client will be always initiator and RAVPN server will
       be always responder */
    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        switch (u1CfgType)
        {
            case IKE2_CFG_REQUEST:
            {
                /* CFG Request */
                if (Ike2CfgSetCMReqAttributes
                    (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeConstructIsakmpCMCfgReq: "
                                 "Ike2CfgSetCMReqAttributes failed\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return (IKE_FAILURE);
                }
                break;
            }
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ConstructCfg: CFG Type not supported\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                return (IKE_FAILURE);
            }
        }
    }
    else
    {
        switch (u1CfgType)
        {
                /* CFG Reply */
            case IKE2_CFG_REPLY:
            {
                /* Get a free IP address from the address pool and 
                 * assign it to the Remote CLIENT */
                if (IkeUtilGetFreeAddrFromPool
                    (&pSesInfo->RemoteTunnelTermAddr, &IntAddress,
                     &u4IntNetmask) == VPN_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ConstructCfg: Cannot Get Free IP Address"
                                 " attribute from the pool\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return (IKE_FAILURE);
                }

                if (IntAddress.u4AddrType == IPV4ADDR)
                {
                    /* Set the IP address attribute */
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        au2ConfiguredAttr[pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                          CMServerInfo.u1ConfiAttrIndex] =
                        IKE_INTERNAL_IP4_ADDRESS;
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        InternalIpAddr.Ipv4Addr = IntAddress.Ipv4Addr;
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        InternalIpAddr.u4AddrType = IPV4ADDR;
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        u1ConfiAttrIndex++;

                    /* Set the Netmask Attribute */
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        au2ConfiguredAttr[pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                          CMServerInfo.u1ConfiAttrIndex] =
                        IKE_INTERNAL_IP4_NETMASK;
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.InternalMask.
                        Ipv4Addr = gaIp4PrefixToMask[u4IntNetmask];
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        u1ConfiAttrIndex++;
                }
                else
                {
                    /* Set the IP address attribute */
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        au2ConfiguredAttr[pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                          CMServerInfo.u1ConfiAttrIndex] =
                        IKE_INTERNAL_IP6_ADDRESS;
                    IKE_MEMCPY (&(pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                                  InternalIpAddr.Ipv6Addr),
                                &(IntAddress.Ipv6Addr), sizeof (tIp6Addr));
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        InternalIpAddr.u4AddrType = IPV6ADDR;
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        u1ConfiAttrIndex++;

                    /* Set the Netmask Attribute */
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        au2ConfiguredAttr[pSesInfo->Ike2AuthInfo.IkeRAInfo.
                                          CMServerInfo.u1ConfiAttrIndex] =
                        IKE_INTERNAL_IP6_NETMASK;
                    IKE_MEMCPY (&pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                                InternalMask.Ipv6Addr,
                                gaIp6PrefixToMask[u4IntNetmask],
                                sizeof (tIp6Addr));
                    pSesInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
                        u1ConfiAttrIndex++;
                }
                if (Ike2CfgSetCMRepAttributes
                    (pSesInfo, &pu1Atts, &i4AttsLen, &i4Len) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2ConstructCfg :"
                                 "Ike2CfgSetCMRepAttributes failed\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                    return (IKE_FAILURE);
                }
                break;
            }
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2ConstructCfg: CFG Type not supported\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
                return (IKE_FAILURE);
            }
        }
    }
    /*  Allocate memory for the attributes */
    if (Ike2ConstructIsakmpMesg (pSesInfo,
                                 (INT4) ((UINT2) *pu4Pos +
                                         sizeof (tIke2ConfigPayLoad)),
                                 (UINT4) i4Len) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructCfg: Ike2ConstructIsakmpMesg failed "
                     " to allocate memory for cfg attributes\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        return (IKE_FAILURE);
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos +
                           sizeof (tIke2ConfigPayLoad)), pu1Atts, i4Len);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);

    /* Calculate the total length of payload */
    Cfg.u2PayLoadLen =
        (UINT2) IKE_HTONS ((sizeof (tIke2ConfigPayLoad) + (UINT2) i4Len));

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Cfg, sizeof (tIke2ConfigPayLoad));

    *pu4Pos =
        (UINT4) (*pu4Pos +
                 (UINT4) ((UINT2) i4Len + sizeof (tIke2ConfigPayLoad)));
    return (IKE_SUCCESS);

}

/************************************************************************/
/*  Function Name   :Ike2ConstructTSi                                   */
/*  Description     :This function is used to construct TSi Payload     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :                                                   */
/*  Output(s)       :Appends the TSi PayLoad in the IKE AUTH Mesg       */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructTSi (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                  UINT4 *pu4Pos)
{
    tIke2TsPayLoad      TsPayload;
    tIke2TsInfo         TsInfo;
    tIkeNetwork        *pIkeNetwork = NULL;
    tIkeIpv4Range       Ipv4Range;
    tIkeIpv6Range       Ipv6Range;

    UINT2               u2NBytes = IKE_ZERO;
    UINT4               u4TsInfoBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &TsPayload, sizeof (tIke2TsPayLoad));
    IKE_BZERO ((UINT1 *) &TsInfo, sizeof (tIke2TsInfo));
    IKE_BZERO ((UINT1 *) &Ipv4Range, sizeof (tIkeIpv4Range));
    IKE_BZERO ((UINT1 *) &Ipv6Range, sizeof (tIkeIpv6Range));

    u2NBytes = sizeof (tIke2TsPayLoad);
    u4TsInfoBytes = (sizeof (tIke2TsInfo) - (IKE_TWO * (sizeof (tIkeIpAddr))));
    u2NBytes = (UINT2) (u2NBytes + u4TsInfoBytes);

    if (pSesInfo->u1Role == IKE_INITIATOR)
    {
        pIkeNetwork =
            &(pSesInfo->IkeExchInfo.AuthExchInfo.IkeCryptoMap.LocalNetwork);
    }
    else
    {
        pIkeNetwork =
            &(pSesInfo->IkeExchInfo.AuthExchInfo.IkeCryptoMap.RemoteNetwork);
    }

    /* There will be only one TSi payload */
    TsPayload.u1NumTS = IKE_ONE;

    /* Fill Next payload in the TSi payload */
    TsPayload.u1NextPayLoad = u1NextPayLoad;

    /* Fill Protocol in the TSi Payload */
    TsInfo.u1ProtocolId = pIkeNetwork->u1HLProtocol;

    /* Fill Start & End Address in the TSi Payload */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        /* V4 address */
        TsPayload.u2PayLoadLen = IKE_HTONS (IKE2_TS_PAYLOAD_LEN_IPV4);
        TsInfo.u1TSType = IKE2_TS_IPV4_ADDR_RANGE;
        TsInfo.u2SelectorLen = IKE_HTONS (IKE2_TS_LEN_IPV4);
        /* In case of RAVPN client, Cryptomap Localnetwork will be 
         * 0.0.0.0/0.0.0.0 and type is 0. But TSi should be 0.0.0.0/32.
         * To generate 0.0.0.0/32,set the network type to SUBNET */
        if (pIkeNetwork->u4Type == IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_SUBNET;
        }
        /* Convert the IPV4 subnet into IPv4 Range */
        IkeConvertToRangeForIpv4 (pIkeNetwork, &Ipv4Range);
        TsInfo.StartAddr.Ipv4Addr = IKE_HTONL (Ipv4Range.Ip4StartAddr);
        TsInfo.EndAddr.Ipv4Addr = IKE_HTONL (Ipv4Range.Ip4EndAddr);
        u2NBytes = (UINT2) (u2NBytes + (IKE_TWO * sizeof (tIp4Addr)));
    }
    else
    {
        TsPayload.u2PayLoadLen = IKE_HTONS (IKE2_TS_PAYLOAD_LEN_IPV6);
        TsInfo.u1TSType = IKE2_TS_IPV6_ADDR_RANGE;
        TsInfo.u2SelectorLen = IKE_HTONS (IKE2_TS_LEN_IPV6);
        /* In case of RAVPN client, Cryptomap Localnetwork will be 
         * 0.0.0.0/0.0.0.0 and type is 0. But TSi should be 0.0.0.0/32.
         * To generate 0.0.0.0/32,set the network type to SUBNET */
        if (pIkeNetwork->u4Type == IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_SUBNET;
        }
        /* Convert the IPV6 subnet into IPv6 Range */
        IkeConvertToRangeForIpv6 (pIkeNetwork, &Ipv6Range);

        IKE_MEMCPY (&(TsInfo.StartAddr.Ipv6Addr),
                    &(Ipv6Range.Ip6StartAddr), sizeof (tIp6Addr));
        IKE_MEMCPY (&(TsInfo.EndAddr.Ipv6Addr), &(Ipv6Range.Ip6EndAddr),
                    sizeof (tIp6Addr));
        u2NBytes = (UINT2) (u2NBytes + (IKE_TWO * sizeof (tIp6Addr)));
    }

    /* Fill Start Port & End Port in the TSi Payload */
    TsInfo.u2StartPort = IKE_HTONS (pIkeNetwork->u2StartPort);
    TsInfo.u2EndPort = (UINT2) ((pIkeNetwork->u2EndPort == IKE_ZERO) ?
                                IKE_HTONS (IKE_TS_MAX_PORT) :
                                IKE_HTONS (pIkeNetwork->u2EndPort));

    /* Allocate space for the TSi payload in the IKE mesg */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) u2NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKEv2",
                     "Ike2ConstructTS: Ike2ConstructIsakmpMesg failed "
                     "for TSi payload\n");
        return (IKE_FAILURE);
    }

    /* Copy TS payload Header in the IKE mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &TsPayload, sizeof (tIke2TsPayLoad));
    *pu4Pos += sizeof (tIke2TsPayLoad);

    /* Copy TSi payload in the IKE Mesg, excluding the tIkeIpAddr */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &TsInfo, u4TsInfoBytes);
    *pu4Pos += u4TsInfoBytes;

    /* Copy Address part of the TSi payload in the IKE mesg */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        /* Copy the IPv4 Start Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.StartAddr.Ipv4Addr, (sizeof (tIp4Addr)));
        *pu4Pos += (sizeof (tIp4Addr));

        /* Copy the IPv4 End Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.EndAddr.Ipv4Addr, (sizeof (tIp4Addr)));
        *pu4Pos += (sizeof (tIp4Addr));
    }
    else
    {
        /* Copy the IPv6 Start Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.StartAddr.Ipv6Addr, (sizeof (tIp6Addr)));
        *pu4Pos += (sizeof (tIp6Addr));

        /* Copy the IPv6 End Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.EndAddr.Ipv6Addr, (sizeof (tIp6Addr)));
        *pu4Pos += (sizeof (tIp6Addr));
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2ConstructTSr                                   */
/*  Description     :This function is used to construct TSr Payload     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the Session Info        */
/*                  :u1NextPayLoad - Identifies the next payload in an  */
/*                  :                Isakmp Message                     */
/*                  :pu4Pos        - Pointer to the Position in an      */
/*                  :                Isakmp Mesg where the payload is   */
/*                  :                to be appended                     */
/*                  :                                                   */
/*  Output(s)       :Appends the TSr PayLoad in the IKE AUTH Mesg       */
/*                  :                                                   */
/*  Returns         :IKE_SUCCES or IKE_FAILURE                          */
/*                  :                                                   */
/************************************************************************/

INT1
Ike2ConstructTSr (tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                  UINT4 *pu4Pos)
{
    tIkeNetwork        *pIkeNetwork = NULL;
    tIke2TsPayLoad      TsPayload;
    tIke2TsInfo         TsInfo;
    tIkeIpv4Range       Ipv4Range;
    tIkeIpv6Range       Ipv6Range;
    UINT4               u4TsInfoBytes = IKE_ZERO;
    UINT2               u2NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &TsPayload, sizeof (tIke2TsPayLoad));
    IKE_BZERO ((UINT1 *) &TsInfo, sizeof (tIke2TsInfo));
    IKE_BZERO ((UINT1 *) &Ipv4Range, sizeof (tIkeIpv4Range));
    IKE_BZERO ((UINT1 *) &Ipv6Range, sizeof (tIkeIpv6Range));

    u2NBytes = sizeof (tIke2TsPayLoad);
    u4TsInfoBytes = (sizeof (tIke2TsInfo) - (IKE_TWO * (sizeof (tIkeIpAddr))));
    u2NBytes = (UINT2) (u2NBytes + u4TsInfoBytes);

    /* In case of Ravpn Server --> TSr , IkeRAInfo.CMServerInfo.ProtectedNetwork[0]
     * In case of Ravpn Client --> TSr , IkeRAInfo.aProtectNetBundle[0]->ProtectedNetwork
     * Other cases(Initiator)  --> TSr , IkeCryptoMap.RemoteNetwork
     *            (Responder)  --> TSr , IkeCryptoMap.Localnetwork
     */
    if ((pSesInfo->pIkeSA->bCMEnabled == TRUE) &&
        (pSesInfo->pIkeSA->bCMDone != IKE_TRUE))
    {
        if (pSesInfo->Ike2AuthInfo.bCMServer == IKE_TRUE)
        {
            pIkeNetwork = &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                            CMServerInfo.ProtectedNetwork[IKE_INDEX_0]);
        }
        else
        {
            pIkeNetwork =
                &(pSesInfo->Ike2AuthInfo.IkeRAInfo.
                  aProtectNetBundle[IKE_INDEX_0]->ProtectedNetwork);
        }
    }
    else
    {
        if (pSesInfo->u1Role == IKE_INITIATOR)
        {
            pIkeNetwork =
                &(pSesInfo->IkeExchInfo.AuthExchInfo.IkeCryptoMap.
                  RemoteNetwork);
        }
        else
        {
            pIkeNetwork =
                &(pSesInfo->IkeExchInfo.AuthExchInfo.IkeCryptoMap.LocalNetwork);
        }
    }
    /* There will be only one TSr payload */
    TsPayload.u1NumTS = IKE_ONE;

    /* Fill the next payload in the TSr payload */
    TsPayload.u1NextPayLoad = u1NextPayLoad;

    /* Fill Protocol in the TSr Payload */
    TsInfo.u1ProtocolId = pIkeNetwork->u1HLProtocol;

    /* Fill Start & End Address in the TSr Payload */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        TsPayload.u2PayLoadLen = IKE_HTONS (IKE2_TS_PAYLOAD_LEN_IPV4);
        TsInfo.u1TSType = IKE2_TS_IPV4_ADDR_RANGE;
        TsInfo.u2SelectorLen = IKE_HTONS (IKE2_TS_LEN_IPV4);

        /* Convert the IPV4 subnet into IPv4 Range */
        IkeConvertToRangeForIpv4 (pIkeNetwork, &Ipv4Range);
        TsInfo.StartAddr.Ipv4Addr = IKE_HTONL (Ipv4Range.Ip4StartAddr);
        TsInfo.EndAddr.Ipv4Addr = IKE_HTONL (Ipv4Range.Ip4EndAddr);
        u2NBytes = (UINT2) (u2NBytes + (IKE_TWO * sizeof (tIp4Addr)));
    }
    else
    {
        TsPayload.u2PayLoadLen = IKE_HTONS (IKE2_TS_PAYLOAD_LEN_IPV6);
        TsInfo.u1TSType = IKE2_TS_IPV6_ADDR_RANGE;
        TsInfo.u2SelectorLen = IKE_HTONS (IKE2_TS_LEN_IPV6);
        /* Convert the IPV6 subnet into IPv6 Range */
        IkeConvertToRangeForIpv6 (pIkeNetwork, &Ipv6Range);

        IKE_MEMCPY (&(TsInfo.StartAddr.Ipv6Addr),
                    &(Ipv6Range.Ip6StartAddr), sizeof (tIp6Addr));
        IKE_MEMCPY (&(TsInfo.EndAddr.Ipv6Addr), &(Ipv6Range.Ip6EndAddr),
                    sizeof (tIp6Addr));
        u2NBytes = (UINT2) (u2NBytes + (IKE_TWO * sizeof (tIp6Addr)));
    }

    /* Fill Start & End Port in the TSr Payload */
    TsInfo.u2StartPort = IKE_HTONS (pIkeNetwork->u2StartPort);
    TsInfo.u2EndPort = IKE_HTONS (pIkeNetwork->u2EndPort);

    /* Allocate Space for the TSr payload in the IKE mesg */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) u2NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKEv2",
                     "Ike2ConstructTS: Ike2ConstructIsakmpMesg failed for "
                     "TSr payload\n");
        return (IKE_FAILURE);
    }

    /* Copy the TS payload Header in the IKE mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &TsPayload, sizeof (tIke2TsPayLoad));
    *pu4Pos += sizeof (tIke2TsPayLoad);

    /* Copy the TSr payload in the IKE Mesg */
    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &TsInfo, u4TsInfoBytes);
    *pu4Pos += u4TsInfoBytes;

    /* Copy the Addres part of the TSr payload in the IKE mesg */
    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
    {
        /* Copy the IPv4 start Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.StartAddr.Ipv4Addr, (sizeof (tIp4Addr)));
        *pu4Pos += sizeof (tIp4Addr);
        /* Copy the IPv4 End Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.EndAddr.Ipv4Addr, (sizeof (tIp4Addr)));
        *pu4Pos += sizeof (tIp4Addr);
    }
    else
    {
        /* Copy the IPv6 start Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.StartAddr.Ipv6Addr, (sizeof (tIp6Addr)));
        *pu4Pos += (sizeof (tIp6Addr));

        /* Copy the IPv6 End Addr into the Pkt */
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) &TsInfo.EndAddr.Ipv6Addr, (sizeof (tIp6Addr)));
        *pu4Pos += (sizeof (tIp6Addr));
    }

    return (IKE_SUCCESS);
}

/******************************************************************************/
/*  Function Name : Ike2ConstructRespWithNotify                               */
/*  Description   : This function Sends the Response packet for all exchanges */
/*                : with notify payloads. This API is called in the case of   */
/*                : processing Errors to notify the peer.                     */
/*                :                                                           */
/*  Input(s)      : pSessionInfo - Pointer to the Session Structure           */
/*                : u1Exch       - Exchange for which the notify response     */
/*                :                has to be sent                             */
/*                :                                                           */
/*  Output(s)     :  NONE                                                     */
/*  Return        : IKE_SUCCESS or IKE_FAILURE                                */
/******************************************************************************/
INT1
Ike2ConstructRespWithNotify (tIkeSessionInfo * pSessionInfo, UINT1 u1Exch)
{
    UINT4               u4Len = sizeof (tIsakmpHdr);
    tIkeNotifyInfo     *pNotifyInfo = NULL;

    pNotifyInfo = &pSessionInfo->NotifyInfo;

    /* Construct IKEv2 (ISAKMP) Header */
    if (Ike2ConstructHeader (pSessionInfo, u1Exch,
                             IKE2_PAYLOAD_NOTIFY) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructRespWithNotify:  "
                     "Ike2ConstructHeader Failed\n");
        if (pNotifyInfo->pu1NotifyData != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo->pu1NotifyData);
        }
        return (IKE_FAILURE);
    }

    /* Update the Notify type from the session structure */
    pNotifyInfo->u2NotifyType = pSessionInfo->u2NotifyType;
    pNotifyInfo->u2SpiNum = IKE_ZERO;
    pNotifyInfo->u1SpiSize = IKE_ZERO;

    if (u1Exch == IKE2_INIT_EXCH)
    {
        pNotifyInfo->u1Protocol = IKE_ISAKMP;
    }
    else
    {
        pNotifyInfo->u1Protocol = pSessionInfo->Ike2AuthInfo.u1Protocol;
    }

    /* Construct the Notify Payload in the response packet */
    if (Ike2ConstructNotify (pSessionInfo, IKE2_PAYLOAD_NONE,
                             &u4Len, pNotifyInfo) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2ConstructRespWithNotify:  "
                     "Ike2ConstructNotify Failed\n");
        if (pNotifyInfo->pu1NotifyData != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pNotifyInfo->pu1NotifyData);
        }
        return (IKE_FAILURE);
    }
    /* Update Packet total length in the Isakmp hdr */
    IKE_ASSERT (u4Len == pSessionInfo->IkeTxPktInfo.u4PacketLen);
    u4Len = IKE_HTONL (pSessionInfo->IkeTxPktInfo.u4PacketLen);
    ((tIsakmpHdr *) (VOID *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
        u4TotalLen = u4Len;

    /* Release the Data */
    if (pNotifyInfo->pu1NotifyData != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pNotifyInfo->pu1NotifyData);
    }
    return (IKE_SUCCESS);
}
#endif /* _IKE2_CONS_C_ */
