/**********************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved               */
/*                                                                    */
/*  $Id: ike2sa.c,v 1.11 2014/04/10 12:02:04 siva Exp $              */
/*                                                                    */
/*  Description:This file contains functions related to SA payload    */
/*              construction and processing                           */
/*                                                                    */
/**********************************************************************/

#ifndef _IKE2_SA_C_
#define _IKE2_SA_C_

#include "ikegen.h"
#include "ike2inc.h"
#include "utilrand.h"
#include "ikememmac.h"

PRIVATE INT1
    Ike2SaConstructIsakmpAtts PROTO ((tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                                      INT4 *pi4Offset, INT4 *pi4NTans));

PRIVATE INT1
    Ike2SaConstructTransform PROTO ((tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                                     UINT1 u1NextPayLoad, UINT1 u1TransType,
                                     UINT1 u1TransId, UINT1 *pu1Atts,
                                     INT4 i4AttsLen));
PRIVATE VOID        Ike2SaConstructBasicAtt
PROTO ((UINT2 u2Class, INT4 i4Type, UINT1 *pu1P, INT4 *pi4Len));

PRIVATE INT1
    Ike2SaConstructIpsecAtts PROTO ((tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                                     UINT1 u1Algo, INT4 *pi4Offset,
                                     INT4 *pi4Num));

PRIVATE INT1 Ike2SaCheckProposal PROTO ((tIkeSessionInfo * pSesInfo,
                                         tIkeProposal * pIkeProposal));

PRIVATE INT1 Ike2SaCheckIkeProposal PROTO ((tIkeSessionInfo * pSessionInfo,
                                            tIkeProposal * pIkeProposal));

PRIVATE INT1 Ike2SaCheckIpsecProposal PROTO ((tIkeSessionInfo * pSesInfo,
                                              tIkeProposal * pIkeProposal));

PRIVATE INT1 Ike2SaCheckAtts PROTO ((tIkeSessionInfo * pSessionInfo,
                                     UINT1 u1ProtocolId, tIke2Trans * pTrans));

PRIVATE VOID Ike2SaSetIPsecBundle PROTO ((tIkeSessionInfo * pSesInfo,
                                          UINT1 u1ProtocolId, UINT4 u4Spi1));

PRIVATE INT1 Ike2SaSetResponseProposal PROTO ((tIkeSessionInfo * pSesInfo,
                                               tIkeProposal * pIkeProposal));

/*************************************************************************/
/*  Function Name   :Ike2SaConstructProposal                             */
/*  Description     :This function sets the proposal payload based upon  */
/*                  :the confiured parameters                            */
/*                  :                                                    */
/*  Input(s)        :pSesInfo      - Pointer to the session info database*/
/*                  :i4FixPos      - The position of SA the Isakmp Msg   */
/*                  :pu4Pos        - The position of the last payload    */
/*                  :                in the Isakmp Message               */
/*                  :u1Num         - Refres to ISAKMP or IPSEC Protocol  */
/*                  :u2PropNum     - Identifies the Proposal             */
/*                  :u1NextPayLoad - Identifies the next paylaod in an   */
/*                  :                Isakmp Message                      */
/*                  :u1Protocol    - Identifies ESP or AH(IPSEC)         */
/*                  :                                                    */
/*  Output(s)       :Construct Proposal PayLoad                          */
/*                  :                                                    */
/*  Returns         :IKE_ZERO or total no.of bytes appended              */
/*                  :                                                    */
/*************************************************************************/
INT1
Ike2SaConstructProposal (tIkeSessionInfo * pSessionInfo, INT4 i4FixPos,
                         UINT4 *pu4Pos, UINT1 u1Num, UINT2 u2PropNum,
                         UINT1 u1NextPayLoad, UINT1 u1Protocol)
{
    tIke2PropPayLoad    Prop;
    UINT4               u4Spi = IKE_ZERO;
    INT4                i4NBytes = IKE_ZERO;
    INT4                i4NTrans = IKE_ZERO;
    INT4                i4Offset = IKE_ZERO;
    UINT1               u1Algo = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &Prop, sizeof (tIke2PropPayLoad));

    /* Update the next payload type and proposal number */
    Prop.u1NextPayLoad = u1NextPayLoad;
    Prop.u1PropNum = (UINT1) u2PropNum;

    /* Update the protocol */
    if (u1Num == IKE_ISAKMP)
    {
        Prop.u1ProtocolId = IKE_ISAKMP;
    }
    else
    {
        Prop.u1ProtocolId = u1Protocol;
    }

    /* Update the SPI and SPI length */
    if (Prop.u1ProtocolId == IKE_ISAKMP)
    {
        /* In case of IKE Rekey add the New SPI */
        if (pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH)
        {
            /* For rekey IKE SA SPI length is IKE_COOKIE_LEN(8) */
            Prop.u1SpiSize = IKE_COOKIE_LEN;
        }
        else
        {
            /* For initial exchange ISAKMP SPI length is ZERO */
            Prop.u1SpiSize = IKE_ZERO;
        }
    }
    else
    {
        /* For ESP amd AH protocol  SPI length is 4 */
        Prop.u1SpiSize = sizeof (UINT4);
        if (u1Protocol == IKE_IPSEC_AH)
        {
            u4Spi = pSessionInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4Spi;
        }
        else if (u1Protocol == IKE_IPSEC_ESP)
        {
            u4Spi = pSessionInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4Spi;
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaConstructProposal: Bad Protocol ID\n");
            return (IKE_ZERO);
        }
        u4Spi = IKE_HTONL (u4Spi);
    }
    i4NBytes =
        (INT4) ((INT4) sizeof (tIke2PropPayLoad) + (INT4) Prop.u1SpiSize);

    /* Allocate memory for Proposal paylaod header */
    if (Ike2ConstructIsakmpMesg (pSessionInfo, (INT4) *pu4Pos, (UINT4) i4NBytes)
        == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaConstructProposal: Ike2ConstructIsakmpMesg failed "
                     "allocate memory for Proposal paylaod header\n");
        return (IKE_ZERO);
    }

    *pu4Pos = (UINT4) (*pu4Pos + (UINT4) i4NBytes);

    /* u1Num indicates the protocol is ISAKMP/ESP/AH */
    /* In case of ISAKMP call Ike2SaConstructIsakmpAtts function to
     * construct proposal payload */
    if (u1Num == IKE_ISAKMP)
    {
        /* For IKE */
        /* i4Offset is the total no.of bytes copied and 
         * i4NTrans is the total no.of transforms created*/
        if (Ike2SaConstructIsakmpAtts (pSessionInfo, pu4Pos, &i4Offset,
                                       &i4NTrans) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaConstructProposal: Ike2SaConstructIsakmpAtts "
                         "failed\n");
            return (IKE_ZERO);
        }
    }
    else
    {
        /* AH or ESP */
        /* Call Ike2SaConstructIpsecAtts function to construct 
         * proposal payload */
        pSessionInfo->Ike2AuthInfo.u1Protocol = u1Protocol;

        switch (u1Protocol)
        {
            case IKE_IPSEC_AH:
            {
                if (pSessionInfo->Ike2IPSecPolicy.
                    aTransformSetBundle[IKE_INDEX_0]->u1AhHashAlgo ==
                    IKE2_HMAC_SHA1_96)
                {
                    u1Algo = IKE2_HMAC_SHA1_96;
                }
                else if (pSessionInfo->Ike2IPSecPolicy.
                         aTransformSetBundle[IKE_INDEX_0]->
                         u1AhHashAlgo == IKE2_HMAC_MD5_96)
                {
                    u1Algo = IKE2_HMAC_MD5_96;
                }
                else if (pSessionInfo->Ike2IPSecPolicy.
                         aTransformSetBundle[IKE_INDEX_0]->
                         u1AhHashAlgo == IKE2_AES_XCBC_96)
                {
                    u1Algo = IKE2_AES_XCBC_96;
                }
                else if (pSessionInfo->Ike2IPSecPolicy.
                         aTransformSetBundle[IKE_INDEX_0]->u1AhHashAlgo ==
                         HMAC_SHA_256)
                {
                    u1Algo = HMAC_SHA_256;
                }
                else if (pSessionInfo->Ike2IPSecPolicy.
                         aTransformSetBundle[IKE_INDEX_0]->u1AhHashAlgo ==
                         HMAC_SHA_384)
                {
                    u1Algo = HMAC_SHA_384;
                }
                else if (pSessionInfo->Ike2IPSecPolicy.
                         aTransformSetBundle[IKE_INDEX_0]->u1AhHashAlgo ==
                         HMAC_SHA_512)
                {
                    u1Algo = HMAC_SHA_512;
                }

                /* For AH */
                /* i4Offset is the total no.of bytes copied and 
                 * i4NTrans is the total no.of transforms created*/
                if (Ike2SaConstructIpsecAtts (pSessionInfo, pu4Pos, u1Algo,
                                              &i4Offset,
                                              &i4NTrans) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SaConstructProposal: "
                                 "Ike2SaConstructIpsecAtts failed\n");
                    return (IKE_ZERO);
                }
                if (i4Offset == IKE_ZERO)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SaConstructProposal: No of Bytes "
                                 "returned is ZERO\n");
                    return (IKE_ZERO);
                }
                break;
            }
            case IKE_IPSEC_ESP:
            {
                u1Algo = pSessionInfo->Ike2IPSecPolicy.
                    aTransformSetBundle[IKE_INDEX_0]->u1EspHashAlgo;

                /* For ESP */
                /* i4Offset is the total no.of bytes copied and 
                 * i4NTrans is the total no.of transforms created*/
                if (Ike2SaConstructIpsecAtts (pSessionInfo, pu4Pos, u1Algo,
                                              &i4Offset,
                                              &i4NTrans) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SaConstructProposal: "
                                 "Ike2SaConstructIpsecAtts failed\n");
                    return (IKE_ZERO);
                }
                if (i4Offset == IKE_ZERO)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SaConstructProposal: No of Bytes "
                                 "copied is ZERO \n");
                    return (IKE_ZERO);
                }
                break;
            }
                /* Invalid protocol received */
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaConstructProposal: Bad Protocol ID\n");
                return (IKE_ZERO);
            }
        }
    }
    /* Update the total bytes added into the payload */
    i4NBytes += i4Offset;

    /* Update the proposal payload with the total length of transform 
     * payloads */
    Prop.u2PayLoadLen = (UINT2) i4NBytes;
    Prop.u2PayLoadLen = IKE_HTONS (Prop.u2PayLoadLen);

    /* Update the no.of transform payloads */
    Prop.u1NumTran = (UINT1) i4NTrans;

    IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos),
                (UINT1 *) &Prop, sizeof (tIke2PropPayLoad));

    if (Prop.u1ProtocolId != IKE_ISAKMP)
    {
        /* Copy the SPI after proposal payload header */
        IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos +
                     sizeof (tIke2PropPayLoad)), (UINT1 *) &u4Spi,
                    sizeof (UINT4));
    }
    else
    {
        /* In case of IKE rekey the new cookie should be copied 
         * in the Proposal payload SPI field */
        if (pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH)
        {
            if (pSessionInfo->u1Role == IKE_INITIATOR)
            {
                /* For Initiator - copy initiator cookie */
                IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos +
                             sizeof (tIke2PropPayLoad)),
                            pSessionInfo->pIkeReKeySA->au1InitiatorCookie,
                            IKE_COOKIE_LEN);
            }
            else
            {
                /* For Responder - copy responder cookie */
                IKE_MEMCPY ((pSessionInfo->IkeTxPktInfo.pu1RawPkt + i4FixPos +
                             sizeof (tIke2PropPayLoad)),
                            pSessionInfo->pIkeReKeySA->au1ResponderCookie,
                            IKE_COOKIE_LEN);
            }
        }
    }
    /* Returns proposal payload length */
    return (INT1) (i4NBytes);
}

/************************************************************************/
/*  Function Name   :Ike2SaConstructIsakmpAtts                          */
/*  Description     :This function is used to set Ike SA attributes     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo  - Pointer to the session info data base  */
/*                  :pu4Pos    - The position of the last payload       */
/*                               in the Isakmp Message                  */
/*                  :pi4Offset - Total no of bytes added                */
/*                  :pi4NTans  - Total no .of transforms                */
/*                  :                                                   */
/*  Output(s)       :Constructs the Ike SA attributes                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2SaConstructIsakmpAtts (tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                           INT4 *pi4Offset, INT4 *pi4NTans)
{
    INT4                i4Len = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    UINT4               u4AttsLen = IKE_ZERO;

    /* pi4Offset contains the total no of bytes which includes the total 
       length of all transform payloads added in the proposal.
       pi4NTans indicates the total no .of transforms in the proposal 
       payload */

    /* Encryption Algorithm.In case of AES , key length attribute
       should be added */
    if (pSesInfo->Ike2IkePolicy.u1EncryptionAlgo == IKE2_ENCR_AES_CBC)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Atts) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaConstructIsakmpAtts: unable to allocate "
                         "buffer\n");
            return (IKE_FAILURE);
        }
        if (pu1Atts == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaConstructIsakmpAtts: Malloc failed for "
                         "AES key length Attribute\n");
            return (IKE_FAILURE);
        }

        IKE_BZERO (pu1Atts, sizeof (tSaBasicAttribute));
        u4AttsLen = sizeof (tSaBasicAttribute);

        Ike2SaConstructBasicAtt
            (IKE_SA_KEY_LEN_ATTRIB, pSesInfo->Ike2IkePolicy.u2KeyLen,
             pu1Atts, &i4Len);

        *pi4Offset =
            (INT4) (*pi4Offset +
                    (INT4) Ike2SaConstructTransform (pSesInfo, pu4Pos,
                                                     IKE_TRANSFORM_PAYLOAD,
                                                     IKE2_TRANSFORM_ENCR,
                                                     pSesInfo->Ike2IkePolicy.
                                                     u1EncryptionAlgo, pu1Atts,
                                                     (INT4) u4AttsLen));

        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
        u4AttsLen = IKE_ZERO;
    }
    else
    {
        *pi4Offset +=
            Ike2SaConstructTransform (pSesInfo, pu4Pos, IKE_TRANSFORM_PAYLOAD,
                                      IKE2_TRANSFORM_ENCR,
                                      pSesInfo->Ike2IkePolicy.u1EncryptionAlgo,
                                      NULL, IKE_ZERO);
    }
    *pi4NTans += IKE_ONE;

    /* Integrity Algorithm */
    *pi4Offset += Ike2SaConstructTransform (pSesInfo, pu4Pos,
                                            IKE_TRANSFORM_PAYLOAD,
                                            IKE2_TRANSFORM_INTEG,
                                            pSesInfo->Ike2IkePolicy.u1HashAlgo,
                                            NULL, IKE_ZERO);
    *pi4NTans += IKE_ONE;

    /* PRF Algorithm */
    *pi4Offset += Ike2SaConstructTransform (pSesInfo, pu4Pos,
                                            IKE_TRANSFORM_PAYLOAD,
                                            IKE2_TRANSFORM_PRF,
                                            pSesInfo->Ike2IkePolicy.u1HashAlgo,
                                            NULL, IKE_ZERO);
    *pi4NTans += IKE_ONE;

    /* Diffie Hellman Group */
    *pi4Offset += Ike2SaConstructTransform (pSesInfo, pu4Pos,
                                            IKE2_PAYLOAD_NONE,
                                            IKE2_TRANSFORM_DH,
                                            pSesInfo->Ike2IkePolicy.u1DHGroup,
                                            NULL, IKE_ZERO);
    *pi4NTans += IKE_ONE;

    /* If the No.of Bytes copied is zero  or No.of transforms added is ZERO
     * return failure*/
    if ((*pi4NTans == IKE_ZERO) || (*pi4Offset == IKE_ZERO))
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaConstructIsakmpAtts: No.of Bytes copied is zero "
                     "or No.of transforms is zero\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :Ike2SaConstructTransform                           */
/*  Description     :This function constructs the transform payload     */
/*                  :                                                   */
/*  Input(s)        :pSesInfo      - Pointer to the session info data   */
/*                  :                base                               */
/*                  :pu4Pos        - Refers to the position of the last */
/*                  :u1NextPayLoad - Next payload type payload in the   */
/*                  :                isakmp message                     */
/*                  :u1TransType   - Identifies the Transform  type     */
/*                  :u1TransId     - Transform Id                       */
/*                  :pu1Atts       - Pointer to the Attributes          */
/*                  :i4AttsLen     - Length of the Attributes           */
/*                  :                                                   */
/*  Output(s)       :Appends the transform payload into proposal        */
/*                  :                                                   */
/*  Returns         :IKE_ZERO or total no.of bytes appended             */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2SaConstructTransform (tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                          UINT1 u1NextPayLoad, UINT1 u1TransType,
                          UINT1 u1TransId, UINT1 *pu1Atts, INT4 i4AttsLen)
{
    tIke2TransPayLoad   Trans;
    INT4                i4NBytes = IKE_ZERO;

    IKE_BZERO ((UINT1 *) &Trans, sizeof (tIke2TransPayLoad));

    /* Constructs the transform payload */
    i4NBytes = (INT4) ((INT4) sizeof (tIke2TransPayLoad) + i4AttsLen);
    Trans.u1NextPayLoad = u1NextPayLoad;
    Trans.u2TransformLen = (UINT2) i4NBytes;
    Trans.u1TransType = u1TransType;
    if (u1TransType == IKE_TWO)
    {
        if (pSesInfo->Ike2IkePolicy.u1HashAlgo == HMAC_SHA_256)
        {
            Trans.u2TransformId = IKE2_PRF_HMAC_SHA256;
        }
        else if (pSesInfo->Ike2IkePolicy.u1HashAlgo == HMAC_SHA_384)
        {
            Trans.u2TransformId = IKE2_PRF_HMAC_SHA384;
        }
        else if (pSesInfo->Ike2IkePolicy.u1HashAlgo == HMAC_SHA_512)
        {
            Trans.u2TransformId = IKE2_PRF_HMAC_SHA512;
        }
        else
        {
            Trans.u2TransformId = (UINT2) u1TransId;
        }
    }
    else
    {
        Trans.u2TransformId = (UINT2) u1TransId;
    }

    Trans.u2TransformId = IKE_HTONS (Trans.u2TransformId);
    Trans.u2TransformLen = IKE_HTONS (Trans.u2TransformLen);

    /* Allocate memory for transfrom and copy it to the Proposal payload */
    if (Ike2ConstructIsakmpMesg (pSesInfo, (INT4) *pu4Pos, (UINT4) i4NBytes) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaConstructTransform: Memory allocation failed"
                     " for transfrom\n");
        return (IKE_ZERO);
    }

    IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                (UINT1 *) &Trans, sizeof (tIke2TransPayLoad));
    *pu4Pos += sizeof (tIke2TransPayLoad);

    /* Copy the attribute if present */
    if (i4AttsLen != IKE_ZERO)
    {
        IKE_MEMCPY ((UINT1 *) (pSesInfo->IkeTxPktInfo.pu1RawPkt + *pu4Pos),
                    (UINT1 *) pu1Atts, i4AttsLen);
        *pu4Pos = (UINT4) (*pu4Pos + (UINT4) i4AttsLen);
    }
    /* Returns total length of transform payload */
    return (INT1) (i4NBytes);
}

/************************************************************************/
/*  Function Name   :Ike2SaConstructIpsecProposal                       */
/*  Description     :This function is used to construct Ipsec Proposals */
/*                  :                                                   */
/*  Input(s)        :pSesInfo - Pointer to the session info data base   */
/*                  :i4FixPos - Refers to the fixed position in the     */
/*                  :Isakmp Message                                     */
/*                  :pu4Pos   - Pointer to the position in the Isakmp   */
/*                              Mesg                                    */
/*                  :                                                   */
/*  Output(s)       :Constructs Ipsec Proposal payload                  */
/*                  :                                                   */
/*  Returns         :IKE_ZERO or the no of Bytes added                  */
/*                  :                                                   */
/************************************************************************/
INT4
Ike2SaConstructIpsecProposal (tIkeSessionInfo * pSesInfo, INT4 i4FixPos,
                              UINT4 *pu4Pos)
{
    INT4                i4TotBytes = IKE_ZERO;
    INT4                i4NBytes = IKE_ZERO;

    /* Only one transform set is supported per policy. 
     * So aTransformSetBundle is accesed by index 0 */
    if (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0] != NULL)
    {
        if (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
            u1EspEncryptionAlgo != IKE_ZERO)
        {
            /* Construct the IPSEC proposal payload from the configured 
             * parameters*/
            i4NBytes =
                Ike2SaConstructProposal (pSesInfo, (i4FixPos + i4TotBytes),
                                         pu4Pos, IKE_IPSEC, IKE_ONE,
                                         IKE_NONE_PAYLOAD, IKE_IPSEC_ESP);
            if (i4NBytes == IKE_ZERO)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaConstructIpsecProposal: "
                             "Ike2SaConstructProposal returned zero\n");
                return (IKE_ZERO);
            }
            i4TotBytes += i4NBytes;
        }
        if (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
            u1AhHashAlgo != IKE_ZERO)
        {
            /* Construct the IPSEC proposal payload from the configured 
             * parameters*/
            i4NBytes =
                Ike2SaConstructProposal (pSesInfo, (i4FixPos + i4TotBytes),
                                         pu4Pos, IKE_IPSEC, IKE_ONE,
                                         IKE_NONE_PAYLOAD, IKE_IPSEC_AH);
            if (i4NBytes == IKE_ZERO)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaConstructIpsecProposal: "
                             "Ike2SaConstructProposal returned zero\n");
                return (IKE_ZERO);
            }
            i4TotBytes += i4NBytes;
        }
    }
    return (i4TotBytes);
}

/************************************************************************/
/*  Function Name   :Ike2SaConstructBasicAtt                            */
/*  Description     :This function is used to construct basic attributes*/
/*                  :                                                   */
/*  Input(s)        :u2Class - Refers to the attribute class            */
/*                  :i4Type  - Refers to the attribute type             */
/*                  :pu1P    - Pointer to the PayLoad                   */
/*                  :pi4Len  - Refers to the length of attributes       */
/*                  :                                                   */
/*  Output(s)       :Constructs the basic attributes                    */
/*  Returns         :NONE                                               */
/*                  :                                                   */
/************************************************************************/
VOID
Ike2SaConstructBasicAtt (UINT2 u2Class, INT4 i4Type, UINT1 *pu1P, INT4 *pi4Len)
{
    tSaBasicAttribute   SaBasicAtt;

    IKE_BZERO ((UINT1 *) &SaBasicAtt, sizeof (tSaBasicAttribute));

    /* Parse the attribute , get the attribute type and value */
    SaBasicAtt.u2Attr = u2Class;
    SaBasicAtt.u2Attr = SaBasicAtt.u2Attr | IKE_BASIC_ATTR;
    SaBasicAtt.u2Attr = IKE_HTONS (SaBasicAtt.u2Attr);

    SaBasicAtt.u2BasicValue = (UINT2) i4Type;

    SaBasicAtt.u2BasicValue = IKE_HTONS (SaBasicAtt.u2BasicValue);

    IKE_MEMCPY ((pu1P + *pi4Len), (UINT1 *) &SaBasicAtt,
                sizeof (tSaBasicAttribute));
    *pi4Len = (INT4) (*pi4Len + (INT4) sizeof (tSaBasicAttribute));
    return;
}

/************************************************************************/
/*  Function Name   :Ike2SaConstructIpsecAtts                           */
/*  Description     :This function is used to construct IPSEC           */
/*                  :attributes                                         */
/*                  :                                                   */
/*  Input(s)        :pSessionInfo - Pointer to Session data structure   */
/*                  :pu4Pos       - The position of the last payload    */
/*                  :               in the Isakmp Message               */
/*                  :u1Algo       - Authentication algorithm            */
/*                  :pi4Offset    - Total no of bytes added             */
/*                  :pi4Num       - Total no .of transforms             */
/*                  :                                                   */
/*  Output(s)       :Constructs IPSEC Attributes                        */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
Ike2SaConstructIpsecAtts (tIkeSessionInfo * pSesInfo, UINT4 *pu4Pos,
                          UINT1 u1Algo, INT4 *pi4Offset, INT4 *pi4Num)
{

    INT4                i4Len = IKE_ZERO;
    UINT4               u4AttsLen = IKE_ZERO;
    UINT1              *pu1Atts = NULL;
    UINT1               u1NextPayload = IKE2_PAYLOAD_NONE;

    /* pi4Offset contains the total no of bytes which includes the total 
     * length of all transform payloads added in the proposal.
     * pi4NTans indicates the total no .of transforms in the proposal 
     * payload */

    /* Encryption Algorithm */
    /* In case of AES , key length attribute should be added */
    if (pSesInfo->Ike2AuthInfo.u1Protocol == IKE_IPSEC_ESP)
    {
        /* Always there is ESN Transform that will be sent, so
         * next payload is again transform */
        u1NextPayload = IKE_TRANSFORM_PAYLOAD;
        if ((pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_ZERO]->
             u1EspEncryptionAlgo == IKE2_ENCR_AES_CBC)
            || (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_ZERO]->
                u1EspEncryptionAlgo == IKE2_ENCR_AES_CTR))
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pu1Atts) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaConstructIpsecAtts: unable to allocate "
                             "buffer\n");
                return (IKE_FAILURE);
            }
            if (pu1Atts == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaConstructIpsecAtts: Memory allocation for "
                             "Attributes failed\n");
                return (IKE_FAILURE);
            }
            u4AttsLen = sizeof (tSaBasicAttribute);
            IKE_BZERO (pu1Atts, u4AttsLen);

            Ike2SaConstructBasicAtt
                (IKE_SA_KEY_LEN_ATTRIB, pSesInfo->Ike2IPSecPolicy.
                 aTransformSetBundle[IKE_INDEX_0]->u2KeyLen, pu1Atts, &i4Len);
        }
        *pi4Offset =
            (INT4) (*pi4Offset +
                    (INT4) Ike2SaConstructTransform (pSesInfo, pu4Pos,
                                                     u1NextPayload,
                                                     IKE2_TRANSFORM_ENCR,
                                                     pSesInfo->Ike2IPSecPolicy.
                                                     aTransformSetBundle
                                                     [IKE_INDEX_0]->
                                                     u1EspEncryptionAlgo,
                                                     pu1Atts,
                                                     (INT4) u4AttsLen));

        if (u4AttsLen != IKE_ZERO)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Atts);
            u4AttsLen = IKE_ZERO;
        }
        *pi4Num += IKE_ONE;
    }

    /* Authentication Algorithm */
    if (u1Algo != IKE_ZERO)
    {
        /* Always there is ESN Transform that will be sent, so
         * next payload is again transform */
        u1NextPayload = IKE_TRANSFORM_PAYLOAD;
        *pi4Offset +=
            Ike2SaConstructTransform (pSesInfo, pu4Pos, u1NextPayload,
                                      IKE2_TRANSFORM_INTEG, u1Algo, NULL,
                                      IKE_ZERO);
        *pi4Num += IKE_ONE;
    }

    /* Extended sequence number */
    if ((pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
        && (pSesInfo->Ike2IPSecPolicy.u1Pfs != IKE_ZERO))
    {
        /* Next is ESN Payload followed by PFS  */
        u1NextPayload = IKE_TRANSFORM_PAYLOAD;
    }
    else
    {
        /* Next is ESN , which will be the last */
        u1NextPayload = IKE2_PAYLOAD_NONE;
    }
    /* ESN Value will be 0 */
    *pi4Offset += Ike2SaConstructTransform (pSesInfo, pu4Pos, u1NextPayload,
                                            IKE2_TRANSFORM_ESN, IKE_ZERO,
                                            NULL, IKE_ZERO);
    *pi4Num += IKE_ONE;

    /* pfs */
    if ((pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
        && (pSesInfo->Ike2IPSecPolicy.u1Pfs != IKE_ZERO))
    {
        *pi4Offset +=
            Ike2SaConstructTransform (pSesInfo, pu4Pos, IKE2_PAYLOAD_NONE,
                                      IKE2_TRANSFORM_DH,
                                      pSesInfo->Ike2IPSecPolicy.u1Pfs, NULL,
                                      IKE_ZERO);
        *pi4Num += IKE_ONE;
    }
    return (IKE_SUCCESS);
}

/***************************************************************************/
/*   Function Name : Ike2SaParseProposal                                   */
/*   Description   : Function to parse the Proposal and transform          */
/*                 : payloads present in the SA payload.                   */
/*                 :                                                       */
/*   Input(s)      : pu1Ptr - Pointer to the proposal data.                */
/*                 : pSesInfo - Pointer to Session Info structure.         */
/*                 : u2SaPayLoadLen - Length of SA payload                 */
/*                 : minus the header length.                              */
/*                 :                                                       */
/*   Output(s)     : Constructs list of proposal and transforms            */
/*   Return Values : IKE_FAILURE or IKE_SUCCESS                            */
/***************************************************************************/
INT1
Ike2SaParseProposal (UINT1 *pu1Ptr, tIkeSessionInfo * pSesInfo,
                     UINT2 u2SaPayLoadLen)
{
    tIke2PropPayLoad   *pProp = NULL;
    tIke2TransPayLoad  *pTrans = NULL;
    tIkeProposal       *pIkeProposal = NULL;
    tIkeProposal       *pTempProposal = NULL;
    tIkeProposal       *pLocalProp = NULL;
    tIke2Trans         *pIkeTrans = NULL;
    UINT1              *pu1Temp = pu1Ptr;
    UINT1              *pu1TempTrans = NULL;
    UINT1               u1Count = IKE_ZERO;
    UINT2               u2TranLen = IKE_ZERO;
    UINT2               u2ParseLen = IKE_ZERO;
    UINT2               u2PropLen = IKE_ZERO;
    UINT1               u1LastPropNum = IKE2_INVALID_PROPOSAL_NUMBER;
    UINT1               u1Flag = IKE_FALSE;

    /* Traverse the SA payload and store the Proposal and Transform payloads 
     * in the session structure */
    do
    {
        u1Flag = IKE_FALSE;
        pProp = (tIke2PropPayLoad *) (void *) pu1Temp;
        u2PropLen = IKE_NTOHS (pProp->u2PayLoadLen);

        /* Allocate memory for proposal structure in the session structure 
         * and store the proposal payload info in the proposal structure*/

        if (MemAllocateMemBlock
            (IKE_PROPOSAL_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pIkeProposal) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaParseProposal: unable to allocate " "buffer\n");
            return (IKE_FAILURE);
        }
        if (pIkeProposal == NULL)
        {
            SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                             Ike2UtilFreeProposal);
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaParseProposal: No Memory allocated\n");
            return (IKE_FAILURE);
        }
        pIkeProposal->pu1Proposal = pu1Temp;
        pIkeProposal->u1PropNum = pProp->u1PropNum;
        pIkeProposal->u1ProtocolId = pProp->u1ProtocolId;

        /* In case of IKE_ISAKMP , SPI size will be 0 or 8 (8 in case of Rekey) 
         * and for IKE_IPSEC_AH and IKE_IPSEC_ESP SPI size will be 4 */
        if (pProp->u1SpiSize == IKE_IPSEC_SPI_SIZE)
        {
            IKE_MEMCPY (&(pIkeProposal->u4Spi),
                        (pu1Temp + IKE2_PROP_PAYLOAD_SIZE), pProp->u1SpiSize);

        }
        else if (pProp->u1SpiSize == IKE_COOKIE_LEN)
        {
            IKE_MEMCPY (pIkeProposal->au1IkeCookie,
                        (pu1Temp + IKE2_PROP_PAYLOAD_SIZE), pProp->u1SpiSize);
        }
        else
        {
            pIkeProposal->u4Spi = IKE_ZERO;
        }
        pIkeProposal->u2PayLoadLen = IKE_NTOHS (pProp->u2PayLoadLen);
        pIkeProposal->pNextProposal = NULL;
        SLL_INIT (&(pIkeProposal->IkeTransPayLoadList));

        if (pProp->u1PropNum == u1LastPropNum)
        {
            /* Scan Proposal List */
            SLL_SCAN (&pSesInfo->IkeProposalList, pTempProposal, tIkeProposal *)
            {
                /* If two successive structures have the same Proposal number,
                 * it means that the proposal consists of the first structure 
                 * AND the second.Currently bundling is not supported .
                 * Even if the bundle is not supported, store 
                 * it properly for processing */
                if (pTempProposal->u1PropNum == pProp->u1PropNum)
                {
                    if (pTempProposal->u1ProtocolId != pProp->u1ProtocolId)
                    {
                        pLocalProp = pTempProposal;
                        while (pLocalProp->pNextProposal != NULL)
                        {
                            pLocalProp = pLocalProp->pNextProposal;
                        }
                        pLocalProp->pNextProposal = pIkeProposal;
                        pIkeProposal->pNextProposal = NULL;
                        u1Flag = IKE_TRUE;
                    }
                    else
                    {
                        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                            (UINT1 *) pIkeProposal);
                        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                         Ike2UtilFreeProposal);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2SaParseProposal: Proposals "
                                     "with same protocol Id having the "
                                     "same proposal number\n");
                        return (IKE_FAILURE);
                    }
                }
                else
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                    SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                     Ike2UtilFreeProposal);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "Ike2SaParseProposal: Proposals "
                                 "with same protocol Id having the "
                                 "same proposal number\n");
                    return (IKE_FAILURE);
                }
            }
        }
        else
        {
            /* Add the prposal payload to the proposal list in the 
             * Session structure */
            SLL_ADD (&(pSesInfo->IkeProposalList), (t_SLL_NODE *) pIkeProposal);
            u1Flag = IKE_TRUE;
        }

        /* Add the transform payload to the Tranform list in the proposal List */
        pu1TempTrans = pu1Temp + IKE2_PROP_PAYLOAD_SIZE + pProp->u1SpiSize;
        for (u1Count = IKE_ZERO; u1Count < pProp->u1NumTran; u1Count++)
        {
            pTrans = (tIke2TransPayLoad *) (void *) pu1TempTrans;
            u2TranLen = IKE_NTOHS (pTrans->u2TransformLen);

            if (MemAllocateMemBlock
                (gIke2TransMemPoolId,
                 (UINT1 **) (VOID *) &pIkeTrans) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaParseProposal: unable to allocate "
                             "buffer\n");
                if (u1Flag == IKE_FALSE)
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                }
                SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                 Ike2UtilFreeProposal);
                return IKE_FAILURE;
            }
            if (pIkeTrans == NULL)
            {
                /* Check wthether the proposal node has been added to 
                 * list or not. If not added free the node here */
                if (u1Flag == IKE_FALSE)
                {
                    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                        (UINT1 *) pIkeProposal);
                }
                SLL_DELETE_LIST (&(pSesInfo->IkeProposalList),
                                 Ike2UtilFreeProposal);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaParseProposal: No Memory allocated\n");
                MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                                    (UINT1 *) pIkeProposal);
                return (IKE_FAILURE);
            }
            pIkeTrans->pu1Transform = pu1TempTrans;
            pIkeTrans->u1ProtocolId = pProp->u1ProtocolId;
            pIkeTrans->u2TransId = IKE_NTOHS (pTrans->u2TransformId);
            pIkeTrans->u1TransType = pTrans->u1TransType;
            pIkeTrans->u2PayLoadLen = IKE_NTOHS (pTrans->u2TransformLen);
            SLL_ADD (&(pIkeProposal->IkeTransPayLoadList),
                     (t_SLL_NODE *) pIkeTrans);
            pu1TempTrans += u2TranLen;
        }
        u2ParseLen = (UINT2) (u2ParseLen + u2PropLen);
        u1LastPropNum = pProp->u1PropNum;
        if (u2ParseLen < u2SaPayLoadLen)
        {
            pu1Temp += u2PropLen;
        }
    }
    while (u2ParseLen < u2SaPayLoadLen);
    if (u1Flag == IKE_FALSE)
    {
        SLL_DELETE_LIST (&(pSesInfo->IkeProposalList), Ike2UtilFreeProposal);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaParseProposal: No Memory allocated\n");
        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal);
        return (IKE_FAILURE);
    }

    /*MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal); */
    return (IKE_SUCCESS);
}

/**************************************************************************/
/*   Function Name : Ike2SaSelectProposal                                 */
/*   Description   : Function to select each proposal and corresponding   */
/*                 : transform from the list of proposals and transforms. */
/*                 : And checking for the set of proposals and transforms */
/*                 : matching the configured transformset configuration.  */
/*                 :                                                      */
/*   Input(s)      : pSesInfo - Pointer to session info structure         */
/*                 :                                                      */
/*   Output(s)     : None.                                                */
/*   Return Values : IKE_FAILURE or IKE_SUCCESS                           */
/**************************************************************************/
INT1
Ike2SaSelectProposal (tIkeSessionInfo * pSesInfo)
{
    tIkeProposal       *pIkeProposal = NULL;
    INT4                i4TransFlag = IKE_FALSE;

    SLL_SCAN (&pSesInfo->IkeProposalList, pIkeProposal, tIkeProposal *)
    {
        if (pIkeProposal->pNextProposal != NULL)
        {
            /* Since bundle is not supported ,processing of that payload is 
             * not required.So skip that proposal payload */
            continue;
        }
        else
        {
            IKE_BZERO (pSesInfo->au4LastPropAttrib, IKE_MAX_ATTRIBUTES);
            if (Ike2SaCheckProposal (pSesInfo, pIkeProposal) == IKE_SUCCESS)
            {
                i4TransFlag = IKE_TRUE;
                break;
            }
        }
    }
    /* If a matching proposal is found return success else return failure.
     * On sucess if role is Responder construct the Response SA payload ,
     * store it in the session structure */
    if (i4TransFlag == IKE_TRUE)
    {
        if (pSesInfo->u1Role == IKE_RESPONDER)
        {
            if (Ike2SaSetResponseProposal (pSesInfo, pIkeProposal) ==
                IKE_SUCCESS)
            {
                return (IKE_SUCCESS);
            }
        }
        else
        {
            return (IKE_SUCCESS);
        }
    }
    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "Ike2SaSelectProposal: Failed\n");
    return (IKE_FAILURE);
}

/***************************************************************************/
/*  Function Name : Ike2SaCheckProposal                                    */
/*  Description   : Function to call curresponding functions to process    */
/*                  IKE and IPSEC proposal payloads                        */
/*                                                                         */
/*  Input(s)      : pSesInfo - Pointer to session info structure           */
/*                : pIkeProposal-Pointer to structure containing           */
/*                :                proposal                                */
/*  Output(s)     : None.                                                  */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                             */
/***************************************************************************/
INT1
Ike2SaCheckProposal (tIkeSessionInfo * pSesInfo, tIkeProposal * pIkeProposal)
{
    /* Proposal ID is IKE_IPSEC_ESP or IKE_IPSEC_AH indicates the IPSEC 
       proposals and Proposal ID IKE_ISAKMP indicates IKE prpoposal  */
    if (pIkeProposal->u1ProtocolId == IKE_ISAKMP)
    {
        /* For IKE_ISAKMP proposal */
        if (Ike2SaCheckIkeProposal (pSesInfo, pIkeProposal) == IKE_SUCCESS)
        {
            return (IKE_SUCCESS);
        }
    }
    else if ((pIkeProposal->u1ProtocolId == IKE_IPSEC_AH) ||
             (pIkeProposal->u1ProtocolId == IKE_IPSEC_ESP))
    {
        /* For IKE_IPSEC_ESP /IKE_IPSEC_ESP proposal */
        if (Ike2SaCheckIpsecProposal (pSesInfo, pIkeProposal) == IKE_SUCCESS)
        {
            return (IKE_SUCCESS);
        }
    }
    return (IKE_FAILURE);
}

/**************************************************************************/
/*  Function Name : Ike2SaCheckIkeProposal                                */
/*  Description   : Function to check for the set of proposals and        */
/*                : transforms matching the configured IKE transformset   */
/*                : configuration.                                        */
/*                :                                                       */
/*  Input(s)      : pSessionInfo - Session info pointer.                  */
/*                : pIkeProposal-Pointer to structure containing          */
/*                :                proposal                               */
/*  Output(s)     : None.                                                 */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                            */
/**************************************************************************/
INT1
Ike2SaCheckIkeProposal (tIkeSessionInfo * pSessionInfo,
                        tIkeProposal * pIkeProposal)
{
    tIke2Trans         *pIkeTrans = NULL;
    UINT4              *pTransAttributes = NULL;
    UINT4               u4SoftLifetimeFactor = IKE_ZERO;
    tIkeSA             *pIkeSA = NULL;

    pSessionInfo->Ike2InitInfo.u2NegKeyLen = IKE_NONE;

    /* Scan all the transform payloads */
    SLL_SCAN (&pIkeProposal->IkeTransPayLoadList, pIkeTrans, tIke2Trans *)
    {
        /* Check for the matching transform attributes and store it the 
         * au4LastPropAttrib array of ession structure */
        if (Ike2SaCheckAtts (pSessionInfo, pIkeProposal->u1ProtocolId,
                             pIkeTrans) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIkeProposal: Check Attibutes failed \n");
            return (IKE_FAILURE);
        }
    }
    pTransAttributes = pSessionInfo->au4LastPropAttrib;

    /* In case of IKE macthing Encryption ,Integrity ,PRF and DH transform 
     * are mandatory. */
    if ((*(pTransAttributes + IKE2_TRANSFORM_ENCR) == IKE_NONE) ||
        (*(pTransAttributes + IKE2_TRANSFORM_INTEG) == IKE_NONE) ||
        (*(pTransAttributes + IKE2_TRANSFORM_DH) == IKE_NONE) ||
        (*(pTransAttributes + IKE2_TRANSFORM_PRF) == IKE_NONE))
    {
        /* Since this can be positive cases when multiple prposals are
         * present.So traces are not required. */
        return (IKE_FAILURE);
    }

    /* Select the IKESA based upon the Exchange type */
    if (pSessionInfo->u1ExchangeType == IKE2_INIT_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }
    else
    {
        pIkeSA = pSessionInfo->pIkeReKeySA;
        /* Set the new cookie in case of rekey */
        if (pSessionInfo->u1Role == IKE_INITIATOR)
        {
            IKE_MEMCPY (pIkeSA->au1ResponderCookie, pIkeProposal->au1IkeCookie,
                        IKE_COOKIE_LEN);
        }
        else
        {
            IKE_MEMCPY (pIkeSA->au1InitiatorCookie, pIkeProposal->au1IkeCookie,
                        IKE_COOKIE_LEN);
        }
    }

    /* Configure Encryption and Authentication Algorithm */
    pIkeSA->u1Phase1EncrAlgo =
        (UINT1) *(pTransAttributes + IKE2_TRANSFORM_ENCR);

    pIkeSA->u1Phase1HashAlgo =
        (UINT1) *(pTransAttributes + IKE2_TRANSFORM_INTEG);

    /* Configure the Negotiated key length if the Encryption Algorithm is AES */
    if ((*(pTransAttributes + IKE2_TRANSFORM_ENCR) == IKE2_ENCR_AES_CBC)
        || (*(pTransAttributes + IKE2_TRANSFORM_ENCR) == IKE2_ENCR_AES_CTR))
    {
        pSessionInfo->Ike2InitInfo.u2NegKeyLen =
            (UINT2) *(pTransAttributes + IKE2_KEY_LENGTH_ATTR);
    }

    /* Configure the lifetime parameters */
    if (pSessionInfo->Ike2IkePolicy.u4LifeTime != IKE_NONE)
    {
        pIkeSA->u4LifeTime =
            IkeGetPhase1LifeTime (&pSessionInfo->Ike2IkePolicy);
    }

    if (pSessionInfo->Ike2IkePolicy.u4LifeTimeKB != IKE_NONE)
    {
        pIkeSA->u4LifeTimeKB = pSessionInfo->Ike2IkePolicy.u4LifeTimeKB;

        /* Set the soft lifetime in bytes */
        u4SoftLifetimeFactor = (UINT4) OSIX_RAND (IKE_MIN_SOFTLIFETIME_FACTOR,
                                                  IKE_MAX_SOFTLIFETIME_FACTOR);
        pIkeSA->u4SoftLifeTimeBytes = (pIkeSA->u4LifeTimeKB *
                                       IKE_BYTES_PER_KB *
                                       u4SoftLifetimeFactor) / IKE2_DIVSOR_100;
    }
    return (IKE_SUCCESS);
}

/***************************************************************************/
/*  Function Name : Ike2SaCheckIpsecProposal                               */
/*  Description   : Function to check the IPSEC configuration for          */
/*                : for a pair of proposals and transforms .               */
/*                :                                                        */
/*  Input(s)      : pSesInfo     - Session info pointer.                   */
/*                : pIkeProposal-Pointer to structure containing           */
/*                :                proposal                                */
/*                :                                                        */
/*  Output(s)     : None.                                                  */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                             */
/***************************************************************************/
INT1
Ike2SaCheckIpsecProposal (tIkeSessionInfo * pSesInfo, tIkeProposal *
                          pIkeProposal)
{
    tIke2Trans         *pIkeTrans = NULL;
    UINT4              *pTransAttributes = NULL;

    /* Scan all the transform payloads */
    SLL_SCAN (&pIkeProposal->IkeTransPayLoadList, pIkeTrans, tIke2Trans *)
    {
        /* If multiple transform set are configured,we have to go thru
         * all of them.Currently multiple transform set are not supported */
        if ((pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0] !=
             NULL)
            && (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
                u1Status == IKE_ACTIVE))
        {
            /* Check for the matching transform attributes and store it the 
             * au4LastPropAttrib array of session structure */
            if (Ike2SaCheckAtts (pSesInfo, pIkeProposal->u1ProtocolId,
                                 pIkeTrans) == IKE_FAILURE)
            {
                return (IKE_FAILURE);
            }
        }
    }
    pTransAttributes = pSesInfo->au4LastPropAttrib;

    /* If protocol is IKE_IPSEC_AH , check whether the matching 
     * Authentication algorithm is present or not.If algorithm is not 
     * present log error and return failure, 
     * else store it in the IPSec Bundle*/
    if (pIkeProposal->u1ProtocolId == IKE_IPSEC_AH)
    {
        if (*(pTransAttributes + IKE2_TRANSFORM_INTEG) == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: Authentication "
                         "algorithm mismatch\n");
            return (IKE_FAILURE);
        }
        pSesInfo->Ike2AuthInfo.IPSecBundle.
            aInSABundle[IKE_AH_SA_INDEX].u1HashAlgo =
            (UINT1) *(pTransAttributes + IKE2_TRANSFORM_INTEG);
        pSesInfo->Ike2AuthInfo.IPSecBundle.
            aOutSABundle[IKE_AH_SA_INDEX].u1HashAlgo =
            (UINT1) *(pTransAttributes + IKE2_TRANSFORM_INTEG);
    }
    /* If protocol is IKE_IPSEC_ESP , check whether the matching encryption 
     * and is present or not.If not present log error,else store it in 
     * the IPSec Bundle */
    /* If Hash algorithm is configured , check for matching entry. If not 
     * found, return failure */
    else if (pIkeProposal->u1ProtocolId == IKE_IPSEC_ESP)
    {
        if (*(pTransAttributes + IKE2_TRANSFORM_ENCR) == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: Encryption "
                         "algorithm mismatch\n");
            return (IKE_FAILURE);
        }

        if ((pSesInfo->Ike2IPSecPolicy.
             aTransformSetBundle[IKE_INDEX_0]->u1EspHashAlgo != IKE_NONE) &&
            (*(pTransAttributes + IKE2_TRANSFORM_INTEG) == IKE_NONE))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: Integrity "
                         "algorithm mismatch\n");
            return (IKE_FAILURE);
        }
        if ((pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
             u1EspEncryptionAlgo == IKE2_ENCR_AES_CBC) &&
            (*(pTransAttributes + IKE2_KEY_LENGTH_ATTR)) == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: Encryption "
                         "algorithm key length not found\n");
            return (IKE_FAILURE);
        }
        if ((pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
             u1EspEncryptionAlgo == IKE2_ENCR_AES_CTR) &&
            (*(pTransAttributes + IKE2_KEY_LENGTH_ATTR)) == IKE_NONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: Encryption "
                         "algorithm key length not found\n");
            return (IKE_FAILURE);
        }

        /* Encryption Algorithm */
        pSesInfo->Ike2AuthInfo.IPSecBundle.aInSABundle[IKE_ESP_SA_INDEX].
            u1EncrAlgo = (UINT1) *(pTransAttributes + IKE2_TRANSFORM_ENCR);
        pSesInfo->Ike2AuthInfo.IPSecBundle.aOutSABundle[IKE_ESP_SA_INDEX].
            u1EncrAlgo = (UINT1) *(pTransAttributes + IKE2_TRANSFORM_ENCR);

        /* Authentication Algorithm */
        pSesInfo->Ike2AuthInfo.IPSecBundle.aInSABundle[IKE_ESP_SA_INDEX].
            u1HashAlgo = (UINT1) *(pTransAttributes + IKE2_TRANSFORM_INTEG);
        pSesInfo->Ike2AuthInfo.IPSecBundle.aOutSABundle[IKE_ESP_SA_INDEX].
            u1HashAlgo = (UINT1) *(pTransAttributes + IKE2_TRANSFORM_INTEG);

        /* Configure the Negotiated key length if the Encryption Algorithm 
           is AES */
        if ((pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
             u1EspEncryptionAlgo == IKE2_ENCR_AES_CBC) ||
            (pSesInfo->Ike2IPSecPolicy.aTransformSetBundle[IKE_INDEX_0]->
             u1EspEncryptionAlgo == IKE2_ENCR_AES_CTR))
        {
            pSesInfo->Ike2AuthInfo.u2NegKeyLen =
                (UINT2) *(pTransAttributes + IKE2_KEY_LENGTH_ATTR);
        }
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaCheckIpsecProposal: Protocol not " "supported\n");
        return (IKE_FAILURE);
    }
    if (pSesInfo->u1ExchangeType != IKE2_AUTH_EXCH)
    {

        /* If Pfs is configured and matching entry not found return failure */
        if ((pSesInfo->Ike2IPSecPolicy.u1Pfs != IKE_NONE) &&
            (*(pTransAttributes + IKE2_TRANSFORM_DH) == IKE_NONE))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckIpsecProposal: PFS group "
                         "doesn't match\n");
            return (IKE_FAILURE);
        }
        if (pSesInfo->Ike2IPSecPolicy.u1Pfs != IKE_NONE)
        {
            pSesInfo->Ike2AuthInfo.u1NegPfs =
                (UINT1) *(pTransAttributes + IKE2_TRANSFORM_DH);
        }
    }
    /* Check if selected ESN is received 0 or not */
    if (*(pTransAttributes + IKE2_TRANSFORM_ESN) == IKE_NONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaCheckIpsecProposal: ESN not found\n");
        return (IKE_FAILURE);
    }

    /* Set the neccessary parameters in the IPSEC SA */
    Ike2SaSetIPsecBundle (pSesInfo, pIkeProposal->u1ProtocolId,
                          pIkeProposal->u4Spi);

    return (IKE_SUCCESS);
}

/***************************************************************************/
/*  Function Name : Ike2SaSetIPSecBundle                                   */
/*  Description   : Function is used to set IPSEC interface data           */
/*                : structures with protocolId,spi and other values        */
/*                :                                                        */
/*  Input(s)      : pSesInfo - Pointer to Session Info structure           */
/*                : u1ProtocolId - Protocol (ESP/AH)                       */
/*                : u4Spi1       - SPI for protocolId of the proposal      */
/*  Output(s)     : None.                                                  */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                             */
/***************************************************************************/
VOID
Ike2SaSetIPsecBundle (tIkeSessionInfo * pSesInfo, UINT1 u1ProtocolId,
                      UINT4 u4Spi1)
{
    switch (u1ProtocolId)
    {
        case IKE_IPSEC_ESP:
        {
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_ESP;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_ESP;

            /* Store the OutBound SPI */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi1);

            /* Set the bundle SPI to 0 */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_ZERO;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_ZERO;

            /* Set Ike Version */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u1IkeVer = IKE2_MAJOR_VERSION;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u1IkeVer = IKE2_MAJOR_VERSION;

            /* Set TUNNEL/TRANSPORT mode */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u1Mode =
                pSesInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u1Mode =
                pSesInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode;

            /*Configure the lifetime parameters */
            if (pSesInfo->Ike2IPSecPolicy.u1LifeTimeType != FSIKE_LIFETIME_KB)
            {
                if (pSesInfo->Ike2IPSecPolicy.u4LifeTime != IKE_NONE)
                {
                    IKE_CONVERT_LIFETIME_TO_SECS (pSesInfo->Ike2IPSecPolicy.
                                                  u1LifeTimeType,
                                                  pSesInfo->Ike2IPSecPolicy.
                                                  u4LifeTime);
                    pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aInSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                        pSesInfo->Ike2IPSecPolicy.u4LifeTime;
                    pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTime =
                        pSesInfo->Ike2IPSecPolicy.u4LifeTime;
                }
            }
            else
            {
                if (pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB != IKE_NONE)
                {
                    pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aInSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                        pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB;
                    pSesInfo->Ike2AuthInfo.IPSecBundle.
                        aOutSABundle[IKE_ESP_SA_INDEX].u4LifeTimeKB =
                        pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB;
                }
            }
            break;
        }
        case IKE_IPSEC_AH:
        {
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_AH;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u1IPSecProtocol = IKE_IPSEC_AH;

            /* Store the OutBound SPI */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u4Spi = IKE_NTOHL (u4Spi1);

            /* Set the bundle SPI to 0 */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_ZERO;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_ESP_SA_INDEX].u4Spi = IKE_ZERO;

            /* Set Ike Version */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u1IkeVer = IKE2_MAJOR_VERSION;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u1IkeVer = IKE2_MAJOR_VERSION;

            /* Set TUNNEL/TRANSPORT mode */
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aInSABundle[IKE_AH_SA_INDEX].u1Mode =
                pSesInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode;
            pSesInfo->Ike2AuthInfo.IPSecBundle.
                aOutSABundle[IKE_AH_SA_INDEX].u1Mode =
                pSesInfo->Ike2AuthInfo.IkeCryptoMap.u1Mode;

            /*Configure the lifetime parameters */
            if (pSesInfo->Ike2IPSecPolicy.u4LifeTime != IKE_NONE)
            {
                IKE_CONVERT_LIFETIME_TO_SECS (pSesInfo->Ike2IPSecPolicy.
                                              u1LifeTimeType,
                                              pSesInfo->Ike2IPSecPolicy.
                                              u4LifeTime);
                pSesInfo->Ike2AuthInfo.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                    pSesInfo->Ike2IPSecPolicy.u4LifeTime;
                pSesInfo->Ike2AuthInfo.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4LifeTime =
                    pSesInfo->Ike2IPSecPolicy.u4LifeTime;
            }
            if (pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB != IKE_NONE)
            {
                pSesInfo->Ike2AuthInfo.IPSecBundle.
                    aInSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB;
                pSesInfo->Ike2AuthInfo.IPSecBundle.
                    aOutSABundle[IKE_AH_SA_INDEX].u4LifeTimeKB =
                    pSesInfo->Ike2IPSecPolicy.u4LifeTimeKB;
            }
            break;
        }
        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessSetIPSecBundle: Invalid Protocol\n ");
            break;
        }
    }
    return;
}

/***********************************************************************/
/*  Function Name : Ike2SaCheckAtts                                    */
/*  Description   : Function to check matching transform attributes    */
/*  Input(s)      : pSessionInfo -Session data base pointer.           */
/*                : u1ProtocolId - IKE/ESP/AH                          */
/*                : pTrans       - Pointer to transform present        */
/*                : in selected proposal                               */
/*  Return Values : IKE_SUCCESS or IKE_FAILURE                         */
/***********************************************************************/
INT1
Ike2SaCheckAtts (tIkeSessionInfo * pSessionInfo, UINT1 u1ProtocolId,
                 tIke2Trans * pTrans)
{
    UINT2               u2Attr = IKE_ZERO;
    UINT2               u2KeyLen = IKE_ZERO;
    UINT2               u2BasicValue = IKE_ZERO;
    UINT1               au1VarAtts[IKE_MAX_ATTR_SIZE] = { IKE_ZERO };
    UINT1               u1EncryptionAlgo = IKE_ZERO;
    UINT1               u1HashAlgo = IKE_ZERO;
    UINT1               u1Prf = IKE_ZERO;
    INT4                i4Len = IKE_ZERO;
    UINT4               u4AttrType = IKE_ZERO;
    UINT4              *pTransAttributes = NULL;

    pTransAttributes = pSessionInfo->au4LastPropAttrib;

    switch (pTrans->u1TransType)
    {
        case IKE2_TRANSFORM_ENCR:
        {
            /* Based upon the protocolId ,select the configured encryption algo,
             * key length and compare it with the transform payload attributes.
             * If matching found store it the au4LastPropAttrib in the session
             * data structure */
            u1EncryptionAlgo = (UINT1) ((u1ProtocolId == IKE_ISAKMP) ?
                                        pSessionInfo->Ike2IkePolicy.
                                        u1EncryptionAlgo : pSessionInfo->
                                        Ike2IPSecPolicy.
                                        aTransformSetBundle[IKE_INDEX_0]->
                                        u1EspEncryptionAlgo);
            if (pTrans->u2TransId == u1EncryptionAlgo)
            {
                if ((pTrans->u2TransId == IKE2_ENCR_AES_CBC)
                    || (pTrans->u2TransId == IKE2_ENCR_AES_CTR))
                {
                    u2KeyLen = (UINT2) ((u1ProtocolId == IKE_ISAKMP) ?
                                        pSessionInfo->Ike2IkePolicy.u2KeyLen :
                                        pSessionInfo->Ike2IPSecPolicy.
                                        aTransformSetBundle[IKE_INDEX_0]->
                                        u2KeyLen);

                    i4Len = IkeGetAtts ((pTrans->pu1Transform +
                                         sizeof (tIke2TransPayLoad)),
                                        &u4AttrType, &u2Attr, &u2BasicValue,
                                        au1VarAtts);
                    if (i4Len == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2SaCheckAtts: IkeGetAtts failed "
                                     "to get the Aes key length!\n");
                        return (IKE_FAILURE);
                    }

                    if ((!(u4AttrType & IKE_BASIC_ATTR)) &&
                        (u2Attr != IKE_SA_KEY_LEN_ATTRIB))
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "Ike2SaCheckAtts: Key length "
                                     "attribute not present!\n");
                        return (IKE_FAILURE);
                    }

                    if (u2BasicValue == u2KeyLen)
                    {
                        pTrans->u1Match = IKE_TRUE;
                        *(pTransAttributes + u2Attr) = (UINT4) u2BasicValue;
                    }
                }
                *(pTransAttributes + (pTrans->u1TransType)) =
                    (UINT4) pTrans->u2TransId;

            }
            break;
        }

        case IKE2_TRANSFORM_INTEG:
        {
            /* Based upon the protocolId ,select the configured authentication
             * algo, key length and compare it with the transform payload 
             * attributes. If matching found store it the au4LastPropAttrib in 
             * the session data structure */
            if (u1ProtocolId == IKE_ISAKMP)
            {
                u1HashAlgo = pSessionInfo->Ike2IkePolicy.u1HashAlgo;
            }
            else if (u1ProtocolId == IKE_IPSEC_ESP)
            {
                u1HashAlgo = pSessionInfo->Ike2IPSecPolicy.
                    aTransformSetBundle[IKE_INDEX_0]->u1EspHashAlgo;
            }
            else if (u1ProtocolId == IKE_IPSEC_AH)
            {
                u1HashAlgo = pSessionInfo->Ike2IPSecPolicy.
                    aTransformSetBundle[IKE_INDEX_0]->u1AhHashAlgo;
            }
            if (pTrans->u2TransId == (UINT2) u1HashAlgo)
            {
                *(pTransAttributes + (pTrans->u1TransType)) =
                    (UINT4) pTrans->u2TransId;
            }
            break;
        }
        case IKE2_TRANSFORM_PRF:
        {
            /* PRF will be negotiated only in case of IKE policy */
            switch (pSessionInfo->Ike2IkePolicy.u1HashAlgo)
            {
                case HMAC_SHA_256:
                    *(pTransAttributes + (pTrans->u1TransType)) =
                        IKE2_PRF_HMAC_SHA256;
                    break;
                case HMAC_SHA_384:
                    *(pTransAttributes + (pTrans->u1TransType)) =
                        IKE2_PRF_HMAC_SHA384;
                    break;
                case HMAC_SHA_512:
                    *(pTransAttributes + (pTrans->u1TransType)) =
                        IKE2_PRF_HMAC_SHA512;
                    break;
                default:
                    *(pTransAttributes + (pTrans->u1TransType)) =
                        pSessionInfo->Ike2IkePolicy.u1HashAlgo;
            }

            break;
        }
        case IKE2_TRANSFORM_DH:
        {
            /* Based upon the protocolId ,select the configured DH group,
             * key length and compare it with the transform payload attributes.
             * If matching found store it the au4LastPropAttrib in the session
             * data structure */
            u1Prf = (UINT1) ((u1ProtocolId == IKE_ISAKMP) ?
                             pSessionInfo->Ike2IkePolicy.u1DHGroup :
                             pSessionInfo->Ike2IPSecPolicy.u1Pfs);
            if (pTrans->u2TransId == u1Prf)
            {
                *(pTransAttributes + (pTrans->u1TransType)) =
                    (UINT4) pTrans->u2TransId;
            }
            break;
        }
        case IKE2_TRANSFORM_ESN:
        {
            /* Accept ESN if the value is zero (NO EXTENDED SEQUENCE NUMBERING) */
            if (pTrans->u2TransId == IKE_ZERO)
            {
                /* The pTransAttributes[ESN] is set to 1 which 
                 * is to check the ESN(0) is negotiated or not */
                *(pTransAttributes + (pTrans->u1TransType)) =
                    (UINT4) (pTrans->u2TransId + IKE_ONE);
            }
            break;
        }

        default:
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "Ike2SaCheckAtts: Transform " "Type not Supported\n");
            break;
            /* Return failure is not required , just 
             * ignore if the transform type is not supported */
        }
    }
    return (IKE_SUCCESS);
}

/****************************************************************************/
/*  Function Name : Ike2SaSetResponseProposal                               */
/*  Description   : Function to set the response proposal when role         */
/*                : is responder.                                           */
/*                :                                                         */
/*  Input(s)      : pSesInfo - SessionInfo pointer.                         */
/*                : pIkeProposal  - Pointer to structure containing         */
/*                :                 selected proposal                       */
/*  Output(s)     : None.                                                   */
/*  Return Values : IKE_FAILURE or IKE_SUCCESS                              */
/****************************************************************************/
INT1
Ike2SaSetResponseProposal (tIkeSessionInfo * pSesInfo,
                           tIkeProposal * pIkeProposal)
{
    tIke2PropPayLoad    Prop;
    tIke2Trans         *pIkeTrans = NULL;
    tIke2Trans         *pIkeTransSel = NULL;
    UINT1              *pu1Ptr = NULL;
    UINT1               u1Match = IKE_FALSE;
    UINT2               u2PropLen = IKE_ZERO;
    UINT4               u4InboundSpi = IKE_ZERO;
    t_SLL               Ike2SelTranList;
    tIke2TransPayLoad  *pTrans1 = NULL;

    /* Initialize the list to store the selected transforms */
    TMO_SLL_Init (&Ike2SelTranList);
    /* Copy the proposal payload */
    IKE_MEMCPY ((UINT1 *) &Prop, pIkeProposal->pu1Proposal,
                sizeof (tIke2PropPayLoad));

    u2PropLen = (UINT2) (IKE2_PROP_PAYLOAD_SIZE + Prop.u1SpiSize);
    Prop.u1NextPayLoad = IKE2_PAYLOAD_NONE;

    /* Reset the no.of transforms in the proposal payload.
       This will be filled while creating the trnsforms */
    Prop.u1NumTran = IKE_ZERO;

    /* Scan the transform payloads in the selected proposal and 
     * find the curresponding payload for matching entries.
     * Create a new transform list using the matching transform payloads */

    SLL_SCAN (&pIkeProposal->IkeTransPayLoadList, pIkeTrans, tIke2Trans *)
    {
        u1Match = IKE_FALSE;
        if ((pIkeTrans->u1TransType == IKE2_TRANSFORM_ENCR) &&
            (pSesInfo->au4LastPropAttrib[IKE2_TRANSFORM_ENCR] ==
             pIkeTrans->u2TransId))
        {
            /* In case of AES check for key length also */
            if ((pIkeTrans->u2TransId == IKE2_ENCR_AES_CBC)
                || (pIkeTrans->u2TransId == IKE2_ENCR_AES_CTR))
            {
                if (pIkeTrans->u1Match == IKE_TRUE)
                {
                    u1Match = IKE_TRUE;
                    u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
                    Prop.u1NumTran++;
                }
            }
            else
            {
                u1Match = IKE_TRUE;
                u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
                Prop.u1NumTran++;
            }
        }
        else if ((pIkeTrans->u1TransType == IKE2_TRANSFORM_INTEG) &&
                 (pSesInfo->au4LastPropAttrib[IKE2_TRANSFORM_INTEG] ==
                  pIkeTrans->u2TransId))
        {
            u1Match = IKE_TRUE;
            u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
            Prop.u1NumTran++;
        }
        else if ((pIkeTrans->u1TransType == IKE2_TRANSFORM_PRF) &&
                 (pSesInfo->au4LastPropAttrib[IKE2_TRANSFORM_PRF] ==
                  pIkeTrans->u2TransId))
        {
            u1Match = IKE_TRUE;
            u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
            Prop.u1NumTran++;
        }
        else if ((pIkeTrans->u1TransType == IKE2_TRANSFORM_DH) &&
                 (pSesInfo->au4LastPropAttrib[IKE2_TRANSFORM_DH] ==
                  pIkeTrans->u2TransId))
        {
            u1Match = IKE_TRUE;
            u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
            Prop.u1NumTran++;
        }
        else if ((pIkeTrans->u1TransType == IKE2_TRANSFORM_ESN) &&
                 (pIkeTrans->u2TransId == IKE_ZERO))
        {
            u1Match = IKE_TRUE;
            u2PropLen = (UINT2) (u2PropLen + pIkeTrans->u2PayLoadLen);
            Prop.u1NumTran++;
        }
        if (u1Match == IKE_TRUE)
        {

            if (MemAllocateMemBlock
                (gIke2TransMemPoolId,
                 (UINT1 **) (VOID *) &pIkeTransSel) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaSetResponseProposal: unable to allocate "
                             "buffer\n");
                return (IKE_FAILURE);
            }
            if (pIkeTransSel == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaSetResponseProposal: "
                             "Unable to allocate resource\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (pIkeTransSel, pIkeTrans, sizeof (tIke2Trans));
            SLL_ADD (&Ike2SelTranList, (t_SLL_NODE *) pIkeTransSel);
        }
    }

    /* Set the Proposal payload length */
    Prop.u2PayLoadLen = IKE_HTONS (u2PropLen);

    /* Allocate memory for the SA Response packet */

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSesInfo->pu1SaSelected) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaSetResponseProposal: unable to allocate "
                     "buffer\n");
        return (IKE_FAILURE);
    }
    if (pSesInfo->pu1SaSelected == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2SaSetResponseProposal: Unable to allocate "
                     "resource\n");
        return (IKE_FAILURE);
    }
    pSesInfo->u2SaSelectedLen = u2PropLen;

    pu1Ptr = pSesInfo->pu1SaSelected;

    IKE_BZERO (pu1Ptr, (size_t) (u2PropLen + IKE_ONE));
    /* Copy Proposal Payload */
    IKE_MEMCPY (pu1Ptr, (UINT1 *) &Prop, u2PropLen);
    pu1Ptr += sizeof (tIke2PropPayLoad);

    /* Copy SPI for AH or ESP  protocol  */
    if (Prop.u1SpiSize != IKE_ZERO)
    {
        if (pSesInfo->u1ChildType != IKE2_IPSEC_REKEY_IKESA)
        {
            u4InboundSpi = Ike2UtilGetInSpi (Prop.u1ProtocolId, pSesInfo);
            if (u4InboundSpi == IKE_FALSE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "Ike2SaSetResponseProposal: No Spi found\n");
                return (IKE_FAILURE);
            }
            u4InboundSpi = IKE_HTONL (u4InboundSpi);
            IKE_MEMCPY (pu1Ptr, (UINT1 *) &u4InboundSpi, Prop.u1SpiSize);
        }
        else
        {
            IKE_MEMCPY (pu1Ptr, pSesInfo->pIkeReKeySA->au1ResponderCookie,
                        ((Prop.u1SpiSize <=
                          IKE_COOKIE_LEN) ? Prop.u1SpiSize : IKE_COOKIE_LEN));
        }
        pu1Ptr += Prop.u1SpiSize;
    }

    /* Copy the transform payloads from the newly created transform list */
    SLL_SCAN (&Ike2SelTranList, pIkeTrans, tIke2Trans *)
    {
        pTrans1 = (tIke2TransPayLoad *) (void *) pIkeTrans->pu1Transform;
        if (((tIke2Trans *) TMO_SLL_Next ((tTMO_SLL *) & Ike2SelTranList,
                                          (tTMO_SLL_NODE *) pIkeTrans)) == NULL)
        {
            /* This transform will be the last payload in the proposal,
             * so set the Next payload in the trasform header as 
             * IKE_NONE_PAYLOAD (0) */
            pTrans1->u1NextPayLoad = IKE_NONE_PAYLOAD;
        }
        else
        {
            pTrans1->u1NextPayLoad = IKE_TRANSFORM_PAYLOAD;
        }
        IKE_MEMCPY (pu1Ptr, pIkeTrans->pu1Transform, pIkeTrans->u2PayLoadLen);
        pu1Ptr += pIkeTrans->u2PayLoadLen;
    }
    /* Release the Ike2SelTranList */
    SLL_DELETE_LIST (&Ike2SelTranList, Ike2UtilFreeTransform);

    return (IKE_SUCCESS);
}
#endif /* _IKE2_SA_C_ */
