/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved 
*
* $Id: ike2defs.h,v 1.3 2013/11/12 11:16:56 siva Exp $
*
* Description:This file contains all the macros required for IKEv2.
*
*******************************************************************/

#ifndef __IKE2DEFS_H__
#define __IKE2DEFS_H__

#define IKE2_MAX_NUM_OF_PKT_BUFFS              10

/* IKEv2 Payload Types */
#define IKE2_PAYLOAD_NONE               0
#define IKE2_MINIMUM_PAYLOAD_ID                32
#define IKE2_PAYLOAD_SA                  33
#define IKE2_PAYLOAD_KE                        34
#define IKE2_PAYLOAD_IDI                  35
#define IKE2_PAYLOAD_IDR                  36
#define IKE2_PAYLOAD_CERT                  37
#define IKE2_PAYLOAD_CERTREQ                 38
#define IKE2_PAYLOAD_AUTH                  39
#define IKE2_PAYLOAD_NONCE                  40
#define IKE2_PAYLOAD_NOTIFY                 41
#define IKE2_PAYLOAD_DELETE                 42
#define IKE2_PAYLOAD_VENDORID                 43
#define IKE2_PAYLOAD_TSI                  44
#define IKE2_PAYLOAD_TSR                  45
#define IKE2_PAYLOAD_ENCRYPTED                 46
#define IKE2_PAYLOAD_CONFIG                 47
#define IKE2_PAYLOAD_EAP                  48
#define IKE2_MAX_PAYLOAD_TYPE                  48

/* Transform Type Values */
#define IKE2_TRANSFORM_ENCR              1
#define IKE2_TRANSFORM_PRF               2
#define IKE2_TRANSFORM_INTEG            3
#define IKE2_TRANSFORM_DH             4
#define IKE2_TRANSFORM_ESN             5
#define IKE2_MAX_TRANSFORM             6
                                       
/* - Macros related to Transform Attributes ---*/
#define IKE2_KEY_LENGTH_ATTR                    14

/* Transform types - Encryption */ 
#define IKE2_ENCR_NONE                0
#define IKE2_ENCR_DES_IV64             1
#define IKE2_ENCR_DES                   2
#define IKE2_ENCR_3DES               3
#define IKE2_ENCR_RC5               4
#define IKE2_ENCR_IDEA               5
#define IKE2_ENCR_CAST             6
#define IKE2_ENCR_BLOWFISH              7
#define IKE2_ENCR_3IDEA               8
#define IKE2_ENCR_DES_IV32             9
#define IKE2_ENCR_RC4                  10
#define IKE2_ENCR_NULL                 11
#define IKE2_ENCR_AES_CBC             12
#define IKE2_ENCR_AES_CTR             13
                               
/* Transform types - PRF */    
#define IKE2_PRF_HMAC_MD5              1 
#define IKE2_PRF_HMAC_SHA1            2
#define IKE2_PRF_HMAC_SHA256                            5 
#define IKE2_PRF_HMAC_SHA384                            6
#define IKE2_PRF_HMAC_SHA512                            7             
#define IKE2_PRF_HMAC_TIGER           3             
#define IKE2_PRF_AES128_XCBC           4             


/* Transform types - INTEGRITY */
#define IKE2_NONE                        0
#define IKE2_HMAC_MD5_96                  1
#define IKE2_HMAC_SHA1_96                2
#define IKE2_DES_MAC                     3
#define IKE2_KPDK_MD5                     4
#define IKE2_AES_XCBC_96                 5

/* Transform types - DH Group */
#define IKE2_DH_NONE                        0
#define IKE2_DH1_MODP768               1
#define IKE2_DH2_MODP1024                2
#define IKE2_DH5_MODP1536                5

/* Identification types */
#define IKE2_ID_RESERVED                  0
#define IKE2_ID_IPV4_ADDR                1
#define IKE2_ID_FQDN                     2
#define IKE2_ID_RFC822_ADDR              3
#define IKE2_ID_IPV6_ADDR                5
#define IKE2_ID_DER_ASN1_DN              9
#define IKE2_ID_DER_ASN1_GN               10
#define IKE2_ID_KEY_ID                  11

/* Certificate Encoding Types */
#define IKE2_PKCS_WRAP_X509_CERT             1
#define IKE2_PGP_CERT                 2
#define IKE2_DNS_SIGNED_KEY               3
#define IKE2_X509_SIGNATURE               4
#define IKE2_KERBEROS_TOKENS           6
#define IKE2_CERT_REVOCATION_LIST          7
#define IKE2_AUTHORITY_REVOCATION_LIST      8
#define IKE2_SPKI_CERT                9
#define IKE2_X509_CERT_ATTR               10
#define IKE2_RAW_RSAKEY                 11 
#define IKE2_HASHURL_X509_CERT            12
#define IKE2_HASHURL_X509_BUNDLE           13



/* IKEv2 Notify message types */
#define IKE2_UNSUPPORTED_CRITICAL_PAYLOAD       1
#define IKE2_INVALID_IKE_SPI                 4
#define IKE2_INVALID_MAJOR_VERSION           5
#define IKE2_INVALID_SYNTAX             7
#define IKE2_INVALID_MESSAGE_ID                 9
#define IKE2_INVALID_SPI                      11
#define IKE2_NO_PROPOSAL_CHOSEN                 14
#define IKE2_INVALID_KE_PAYLOAD                 17
#define IKE2_AUTHENTICATION_FAILED              24
#define IKE2_SINGLE_PAIR_REQUIRED               34
#define IKE2_NO_ADDITIONAL_SAS                  35
#define IKE2_INTERNAL_ADDRESS_FAILURE      36
#define IKE2_FAILED_CP_REQUIRED           37
#define IKE2_TS_UNACCEPTABLE           38
#define IKE2_INVALID_SELECTORS           39
#define IKE2_INITIAL_CONTACT           16384
#define IKE2_SET_WINDOW_SIZE           16385
#define IKE2_ADDITIONAL_TS_POSSIBLE          16386
#define IKE2_IPCOMP_SUPPORTED           16387
#define IKE2_NAT_DETECTION_SOURCE_IP      16388
#define IKE2_NAT_DETECTION_DESTINATION_IP     16389
#define IKE2_COOKIE                    16390
#define IKE2_USE_TRANSPORT_MODE           16391
#define IKE2_HTTP_CERT_LOOKUP_SUPPORTED      16392
#define IKE2_REKEY_SA                16393
#define IKE2_ESP_TFC_PADDING_NOT_SUPPORTED     16394
#define IKE2_NON_FIRST_FRAGMENTS_ALSO      16395


/* IkEv2 TS Types */
#define IKE2_TS_IPV4_ADDR_RANGE           7 
#define IKE2_TS_IPV6_ADDR_RANGE            8

/* Ikev2 Config Types */
#define IKE2_CFG_REQUEST               1
#define IKE2_CFG_REPLY                  2
#define IKE2_CFG_SET                    3
#define IKE2_CFG_ACK                    4

/* Payload sizes */
#define IKE2_GEN_PAYLOAD_SIZE                   4
#define IKE2_SA_PAYLOAD_SIZE           4 
#define IKE2_KE_PAYLOAD_SIZE                 8 
#define IKE2_ID_PAYLOAD_SIZE                    8 
#define IKE2_CERT_PAYLOAD_SIZE                  5 
#define IKE2_AUTH_PAYLOAD_SIZE                  8
#define IKE2_NONCE_PAYLOAD_SIZE                 4
#define IKE2_PROP_PAYLOAD_SIZE                  8
/* Traffic selector payload excluding TS header and IP */
#define IKE2_TS_SEL_LEN                         8
/* Nonce Length */
#define IKE2_NONCE_LEN                 32
#define IKE2_MIN_NONCE_LEN                16
#define IKE2_MAX_NONCE_LEN               256

/* PROTOCOL */
#define IKE2_PROTOCOL_IKE                       1
#define IKE2_PROTOCOL_AH                        2
#define IKE2_PROTOCOL_ESP                       3


/* IKE2 Config Payload types */
#define IKE2_CONFIG_NONE                        0
#define IKE2_CONFIG_REQUEST                     1
#define IKE2_CONFIG_REPLY                       2
#define IKE2_CONFIG_SET                         3
#define IKE2_CONFIG_ACL                         4

/*Ikev2 Cookie length */
#define IKE2_COOKIE_LEN                      8

#define IKE_IPSEC                           2
#define IKE2_INVALID_PROPOSAL_NUMBER            0xff

/*Ikev2 Digest length */
#define IKE2_DIGEST_LEN                     12   
#define IKE2_SHA_256_DIGEST_LEN                     16    
#define IKE2_SHA_384_DIGEST_LEN                     24    
#define IKE2_SHA_512_DIGEST_LEN                     32    
#define IKE2_VPI_ATTR_LEN                     8

/*Ikev2 Critical bit */
#define IKE2_CRITICAL_BIT                       0x80

/* IKEv2 TS header Length */
#define  IKE2_TS_PAYLOAD_LEN_IPV4               24
#define  IKE2_TS_PAYLOAD_LEN_IPV6               48

/* IKEv2 TS selector Length */
#define  IKE2_TS_LEN_IPV4                       16
#define  IKE2_TS_LEN_IPV6                       40


#define  IKE2_CONSTRUCT_AUTH                    1
#define  IKE2_PROCESS_AUTH                      2

#define  IKE2_NUM_OF_HASH_KEYS                  5
#define  IKE2_NUM_OF_ENCR_KEYS                  2

#endif

/* Macros for Harcoded values */

#define IKE2_DIVSOR_100         100
#define IKE2_DIVSOR_16          16
#define IP4_SUB_MASK_FFFFFFFF  0xFFFFFFFF


