
/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved 
*
* $Id: ike2extn.h,v 1.3 2011/10/28 10:42:16 siva Exp $
*
* Description:This file contains all the extern declarations for   
              IKEv2
*******************************************************************/

#ifndef __IKE2EXTN_H__
#define __IKE2EXTN_H___

/* extern declarations */
extern INT4 IkeGetAtts PROTO ((UINT1 *pu1Ptr, UINT4 *pu4AttrType, 
                               UINT2 *pu2Attr, UINT2 *pu2Value, 
                               UINT1 *pu1VarPtr));


extern INT1 VpiAtt PROTO ((UINT2 u2Class, UINT1 *pu1Val, UINT2 u2Size, 
                           UINT1 **ppu1P, INT4 *pi4Len, 
                           INT4 *pi4PayLoadLen));


#endif
