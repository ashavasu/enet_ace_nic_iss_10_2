/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ike2prot.h,v 1.5 2014/01/25 13:54:10 siva Exp $
 *
 * Description: This has functions for IKEv2 Prototypes
 *
 ***********************************************************************/

#ifndef __IKE2PROT_H__
#define __IKE2PROT_H__

#include "ikegen.h"
/********** Prototypes related to Ike2Sess.c *********************/
tIkeSessionInfo  *
Ike2SessGetSessionFromCookie PROTO ((tIkeEngine *pIkeEngine, tIkeIpAddr *pIpAddr,
                                     UINT2 u2LocalPort, UINT2 u2RemotePort, 
                                     tIsakmpHdr *pIsakmpHdr));
INT4 
Ike2SessCopyInitSessionInfo PROTO ((tIkeEngine *pIkeEngine ,
                                   tIkeSessionInfo * pSesInfo));

INT1
Ike2SessCopyRAPolicy PROTO ((tIkeSessionInfo * pSesInfo));

INT4 
Ike2SessCopyCertInfo PROTO ((tIkeEngine *pIkeEngine, tIkeSessionInfo *pSesInfo));

INT1
Ike2SessDelInitExchSession PROTO ((tIkeSessionInfo *pSesInfo));

INT1
Ike2SessCopyIkeSAInfo PROTO ((tIkeSessionInfo *pSessionInfo,
                              tIkeEngine *pIkeEngine));

/********** Prototypes related to Ike2key.c *********************/
INT1
Ike2KeyGenSkeyId PROTO((tIkeSessionInfo * pSesInfo));

INT1
Ike2KeyGenIPSecKeys PROTO((tIkeSessionInfo *pSesInfo));

/********** Prototypes related to Ike2core.c *********************/
INT1 
Ike2ConstructRespWithNotify PROTO ((tIkeSessionInfo *pSesInfo, UINT1 u1Exch));

/********** Prototypes related to Ike2proc.c *********************/
INT1
Ike2Process PROTO ((UINT1 u1PayLoadType, tIkeSessionInfo * pSesInfo, 
                    UINT1 *pu1PayLoad));

INT1
Ike2ProcessKE PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                      UINT1 u1Flag));

INT1
Ike2ProcessId PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                      UINT1 u1Flag));

INT1
Ike2ProcessNonce PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                         UINT1 u1Flag));

INT1
Ike2ProcessAuth PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload,
                        UINT1 u1Flag));

INT1
Ike2ProcessEncryptPayload PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload,
                                  UINT1 u1Flag));

INT1
Ike2ProcessEncrypt PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload,
                           UINT1 u1Flag));

INT1
Ike2ProcessCfg PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                       UINT1 u1Flag));

INT1
Ike2ProcessSa PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload,
                      UINT1 u1Flag));

INT1
Ike2ProcessTS PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                      UINT1 u1Flag));
INT1
Ike2ProcessCert PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                        UINT1 u1Flag));

INT1
Ike2ProcessCertReq PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                           UINT1 u1Flag));

INT1
Ike2ProcessDelete PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                          UINT1 u1Flag));

INT1
Ike2ProcessNotify PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                          UINT1 u1Flag));

INT1
Ike2ProcessVendorId PROTO ((tIkeSessionInfo *pSesInfo, UINT1 *pu1Payload, 
                            UINT1 u1Flag));

INT1
Ike2ProcessTsId PROTO ((tIkeSessionInfo * pSesInfo));

INT4
Ike2GetDigestLenFromAlgo PROTO ((UINT1 u1Phase1HashAlgo));

/********** Prototypes related to Ike2cons.c *********************/
INT1
Ike2ConstructHeader PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1Exch, 
                            UINT1 u1NextPayLoad));

INT1
Ike2ConstructIsakmpMesg PROTO((tIkeSessionInfo * , INT4 i4Pos, 
                               UINT4 u4NumBytes));

INT1
Ike2ConstructKE PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, 
                        UINT4 *pu4Pos));

INT1
Ike2ConstructNonce PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, 
                           UINT4 *pu4Pos));

INT1
Ike2ConstructID PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, 
                        UINT4 *pu4Pos));

INT1
Ike2ConstructAuth PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                          UINT4 *pu4Pos));


INT1
Ike2ConstructSA PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad, 
                        UINT4 *pu4Pos));

INT1
Ike2ConstructIpsecSA PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                             UINT4 *pu4Pos));

INT1
Ike2ConstructTSi PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                         UINT4 *pu4Pos));

INT1
Ike2ConstructTSr PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                         UINT4 *pu4Pos));

INT1
Ike2ConstructDelete PROTO ((tIkeSessionInfo * pSesInfo, UINT1 u1NextPayLoad,
                     UINT4 *pu4Pos, tIkeNotifyInfo * pDeleteInfo));

INT1
Ike2ConstructCfg PROTO((tIkeSessionInfo *, UINT1 ,UINT4 *,UINT1 ));

INT1
Ike2ConstructCertReq PROTO ((tIkeSessionInfo *pSesInfo, UINT1 u1NextPayLoad, 
                            UINT4 *pu4Pos));

INT1
Ike2ConstructCert PROTO ((tIkeSessionInfo *pSesInfo, UINT1 u1NextPayLoad,
                            UINT4 *pu4Pos));

INT1
Ike2ConstructNotify PROTO((tIkeSessionInfo *pSesInfo, UINT1 u1NextPayLoad,
                          UINT4 *pu4Pos, tIkeNotifyInfo * pNotifyInfo));

/********** Prototypes related to Ike2util.c *********************/
INT1
Ike2UtilCheckKBTrasmitted PROTO((tIkeSessionInfo *pSesInfo, UINT1 u1Flag));

VOID
Ike2UtilFreeProposal PROTO((t_SLL_NODE * pNode));

VOID
Ike2UtilFreeTransform PROTO((t_SLL_NODE *pNode));

UINT4 
Ike2UtilGetInSpi PROTO ((UINT1 u1ProtocolId, tIkeSessionInfo *pSesInfo));


VOID
Ike2UtilHexDump PROTO((UINT1 *pu1Payload, UINT4 u4Bytes));

VOID
Ike2UtilReleaseBuf PROTO ((UINT1 **pu1Buf, UINT4 u4Len));

VOID
Ike2UtilGetTsInfo PROTO ((UINT1 *pu1TsPayload, tIkeNetwork * pIkeNetwork));

INT4
Ike2UtilSetNonceInSessionInfo PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr,
                               UINT2 u2NonceLen, UINT1 u1NonceType));

INT4 
Ike2UtilGenIV PROTO ((tIkeSessionInfo *pSesInfo));

VOID 
Ike2UtilGetNextPayloadAndType PROTO ((UINT1 *pu1Payload, 
                                      UINT1 *pu1NextPayloadType,
                                      UINT2 *pu2CurPaylodLen));

INT1 Ike2UtilUpdateCryptoForRA PROTO((tIkeSessionInfo * pSesInfo));

/********** Prototypes related to Ike2natt.c *********************/
INT1
Ike2NattConstructDetectSrcIP PROTO ((tIkeSessionInfo * pSesInfo,
                             UINT1 u1NextPayLoad, UINT4 *pu4Pos));

INT1
Ike2NattConstructDetectDstIP PROTO ((tIkeSessionInfo * pSesInfo,
                             UINT1 u1NextPayLoad, UINT4 *pu4Pos));

VOID
Ike2NattProcessDetectDstIP PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr));

VOID 
Ike2NattProcessDetectSrcIP PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1Ptr));

/********** Prototypes related to Ike2auth.c *********************/
INT1
Ike2AuthConstructhMsg PROTO ((tIkeSessionInfo * pSesInfo, UINT1 **pu1AuthData,
                       UINT4 *pu4AuthDataLen, UINT1 u1Flag));

INT1
Ike2AuthConstructDigest PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1AuthData,
                         UINT4 u4AuthDataLen, UINT1 *pu1AuthDigest));

INT1
Ike2AuthVerifyDigest PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1AuthData,
                      UINT4 u4AuthDataLen, UINT1 u1AuthMethod,
                      UINT1 *pu1PayLoad));

INT1
Ike2AuthVerifySignature PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1HashData,
                         UINT1 *pu1PayLoad, UINT4 u4SignAlgo, UINT4));


/********** Prototypes related to Ike2val.c *********************/
INT1
Ike2ValidatePacket PROTO ((tIkeSessionInfo * pSessionInfo, UINT4 u4TotLen,
                    UINT1 u1PayLoadType, UINT1 *pu1PayLoad));

/********** Prototypes related to Ike2cfg.c *********************/
INT1 
Ike2CfgSetCMRepAttributes PROTO ((tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                              INT4 *pi4AttsLen, INT4 *pi4Len));
INT1 
Ike2CfgSetCMReqAttributes PROTO ((tIkeSessionInfo * pSesInfo, UINT1 **ppu1Atts,
                           INT4 *pi4AttsLen, INT4 *pi4Len));

INT1 
Ike2CfgCheckCMReqAttr PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                       UINT4 u4Len));

INT1 
Ike2CfgCheckCMCfgRepAttr PROTO ((tIkeSessionInfo * pSesInfo, UINT1 *pu1PayLoad,
                            UINT4 u4Len));


/********** Prototypes related to Ike2sa.c *********************/

INT1
Ike2SaSelectProposal PROTO ((tIkeSessionInfo *pSesInfo));

INT1
Ike2SaConstructProposal PROTO ((tIkeSessionInfo * pSessionInfo, INT4 i4FixPos,
                         UINT4 *pu4Pos, UINT1 u1Num, UINT2 u2PropNum,
                        UINT1 u1NextPayLoad, UINT1 u1Protocol));

INT4
Ike2SaConstructIpsecProposal PROTO ((tIkeSessionInfo * pSesInfo, INT4 i4FixPos,
                              UINT4 *pu4Pos));

INT1
Ike2SaParseProposal PROTO ((UINT1 *pu1Ptr, tIkeSessionInfo * pSesInfo,
                     UINT2 u2SaPayLoadLen));

#endif /* __IKE2PROT_H__*/
