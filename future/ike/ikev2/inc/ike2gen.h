/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved 
*
* $Id: ike2gen.h,v 1.3 2014/01/25 13:54:10 siva Exp $
*
* Description:This file contains the datastructures and
*             common prototypes required for IKEv2.
*
*******************************************************************/

#ifndef __IKE2GEN_H__
#define __IKE2GEN_H__

#include "ikegen.h"


typedef struct IKE2TRANS
{
    t_SLL_NODE Next;
    UINT1 *pu1Transform;
    UINT2 u2PayLoadLen;
    UINT1 u1ProtocolId;
    UINT1 u1TransType;
    UINT2 u2TransId;
    UINT1 u1Match;
    UINT1 au1Padding;
}tIke2Trans;

#endif /* _IKE2GEN_H_ */
