/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: ike2core.h,v 1.1 2010/11/03 08:48:45 prabuc Exp $
 *
 * Description: This has functions for IKEv2 Prototypes for core module
 *
 ***********************************************************************/

#ifndef __IKE2CORE_H__
#define __IKE2CORE_H__

#include "ikegen.h"
#include "ike2inc.h"

/********** Prototypes related to Ike2core.c *********************/

PRIVATE INT1 Ike2CoreInitExch PROTO ((tIkeSessionInfo * pSessionInfo,
                                      tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreAuthExch PROTO ((tIkeSessionInfo * pSessionInfo,
                                      tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreChildExch PROTO ((tIkeSessionInfo * pSessionInfo,
                                       tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreInfoExch PROTO ((tIkeSessionInfo * pSessionInfo,
                                      tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreChildFsm PROTO ((tIkeSessionInfo * pSessionInfo,
                                      tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreIkeChildFsm PROTO ((tIkeSessionInfo * pSessionInfo,
                                         tIsakmpHdr * pIsakmpHdr));

PRIVATE INT1 Ike2CoreDoPostChildExch PROTO ((tIkeSessionInfo * pSessionInfo));


PRIVATE INT1 Ike2CoreConstructInitMsg PROTO ((tIkeSessionInfo * pSessionInfo, 
                                              UINT1 u1NextPayload,
                                              UINT4 *pu4Len));
#endif


