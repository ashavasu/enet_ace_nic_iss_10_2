/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved 
*
* $Id: ike2inc.h,v 1.2 2011/06/07 10:16:11 siva Exp $
*
* Description:This file contains the list of header files for IKEv2.
*
*******************************************************************/
#ifndef __IKE2_INC__
#define __IKE2_INC__

#include "fsike.h"
#include "ike2gen.h"
#include "ike2defs.h"
#include "ike2gen.h"
#include "ike2pyld.h"
#include "ike2prot.h"
#include "ike2extn.h"
#include "utilalgo.h"
#endif
