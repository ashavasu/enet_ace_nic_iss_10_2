/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikestat.c,v 1.6 2013/11/12 11:17:41 siva Exp $ 
 *
 * Description: This has abstraction functions for SNMP to access and 
 *               configure IKE mib elements. 
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikestat.h"
#include "ikememmac.h"

tIkeStatisticsInfo *gpIkeStatisticsInfo = NULL;

/**************************************************************************/
/*  Function Name   : IkeTableInitialise                                  */
/*  Description     : This function allocates the memory for the arrays   */
/*                    used by IKE.                                        */
/*  Input(s)        : None.                                               */
/*  Output(s)       : None.                                               */
/*  Returns         : Success or Failure                                  */
/**************************************************************************/
INT1
IkeTableInitialise (VOID)
{
    if (MemAllocateMemBlock
        (IKE_STATISTICS_MEMPOOL_ID,
         (UINT1 **) (VOID *) &gpIkeStatisticsInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTableInitialise: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (gpIkeStatisticsInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTableInitialise:Malloc  Fails\n");
        return IKE_FAILURE;

    }

    IKE_BZERO (gpIkeStatisticsInfo, IKE_MAX_STATISTICS_ENTRY *
               sizeof (tIkeStatisticsInfo));
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIkeInPkts 
 *  Description   : Function to increment number of packets received.
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeInPkts (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeInPkts++;
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIkeOutPkts 
 *  Description   : Function to increment number of packets sent.
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeOutPkts (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeOutPkts++;
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIkeDropPkts 
 *  Description   : Increment number of packets dropped.
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeDropPkts (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeDropPkts++;
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIkeoOfNotifySent 
 *  Description   : Increment number of notify message sent
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeNoOfNotifySent (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfNotifySent++;
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIke NoOfNotifyRecv
 *  Description   : Function to increment number of notify message 
 *                  received.
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeNoOfNotifyRecv (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfNotifyReceive++;
    }
}

/* ---------------------------------------------------------------
 * Function Name : IncIkeNoOfSessionsSucc
 * Description   : Function to increment number of Sessions successful
 * Input(s)      : IpAddr - Peer Ip Addr
 * Output(s)     : None.
 * Return Values : None. 
 -----------------------------------------------------------------*/

VOID
IncIkeNoOfSessionsSucc (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    /* The Index can be used per peer address also.
     * As of now the index is assigned to zero ,for making it as
     * global statistics */
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    i4Index = IKE_ZERO;
    gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsSucceed++;
}

/* ---------------------------------------------------------------
 * Function Name : IncIkeNoOfSessionsFail
 * Description   : Function to increment number of Sessions successful
 * Input(s)      : IpAddr - Peer Ip Addr
 * Output(s)     : None.
 * Return Values : None. 
 -----------------------------------------------------------------*/

VOID
IncIkeNoOfSessionsFail (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;

    UNUSED_PARAM (pIpAddr);

    /* The Index can be used per peer address also.
     * As of now the index is assigned to zero, for making it as
     * global statistics */
    gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsFailed++;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IncIkeActiveSessions 
 *  Description   : Function to decrement active sessions
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
IncIkeActiveSessions (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    gpIkeStatisticsInfo[i4Index].u4IkeActiveSessions++;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : DecIkeActiveSessions 
 *  Description   : Function to decrement active sessions
 *  Input(s)      : IpAddr - Peer Ip Addr
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */

VOID
DecIkeActiveSessions (tIkeIpAddr * pIpAddr)
{
    INT4                i4Index = IKE_ZERO;
    i4Index = IkeGetIndexFromStatisticsTable (pIpAddr,
                                              IKE_CREATE_ENTRY_IF_NOT_EXISTING);
    if (i4Index >= IKE_ZERO)
    {
        gpIkeStatisticsInfo[i4Index].u4IkeActiveSessions--;
    }
}

/**************************************************************************/
/*  Function Name   : IkeGetIndexFromStatisticsTable                      */
/*  Description     : This function get the array index of the Statistics */
/*                    table.                                              */
/*  Input(s)        : Peer IP Address.                                    */
/*  Output(s)       : None.                                               */
/*  Returns         : Array Index - If entry is found for that Peer Addr. */
/*                    IKE_ERR_NO_MACTCH_FOUND - If entry does not exist   */
/*                                              for that Peer Addr.       */
/**************************************************************************/
INT4
IkeGetIndexFromStatisticsTable (tIkeIpAddr * pIpPeerAddr, UINT1 u1Flag)
{
    INT4                i4Index = IKE_ZERO;

    for (i4Index = IKE_ZERO; i4Index < IKE_MAX_STATISTICS_ENTRY; i4Index++)
    {
        if (pIpPeerAddr->u4AddrType == IPV4ADDR)
        {
            if (IKE_MEMCMP
                (&pIpPeerAddr->Ipv4Addr, &(GetIkeIpPeerAddr (i4Index)).Ipv4Addr,
                 sizeof (tIp4Addr)) == IKE_ZERO)
            {
                return i4Index;
            }
        }
        else
        {
            if (IKE_MEMCMP
                (&pIpPeerAddr->Ipv6Addr, &(GetIkeIpPeerAddr (i4Index)).Ipv6Addr,
                 sizeof (tIp6Addr)) == IKE_ZERO)
            {
                return i4Index;
            }
        }
    }

    if (u1Flag == IKE_CREATE_ENTRY_IF_NOT_EXISTING)
    {
        for (i4Index = IKE_ZERO; i4Index < IKE_MAX_STATISTICS_ENTRY; i4Index++)
        {
            if (GetIkeIpPeerAddr (i4Index).u4AddrType == IKE_ZERO)
            {
                GetIkeIpPeerAddr (i4Index).u4AddrType = pIpPeerAddr->u4AddrType;
                if (pIpPeerAddr->u4AddrType == IPV4ADDR)
                {
                    GetIkeIpPeerAddr (i4Index).Ipv4Addr = pIpPeerAddr->Ipv4Addr;
                }
                else
                {
                    GetIkeIpPeerAddr (i4Index).Ipv6Addr = pIpPeerAddr->Ipv6Addr;
                }
                return i4Index;
            }
        }
    }
    return IKE_FAILURE;
}

/* ---------------------------------------------------------------
 *Function Name : IncIkePhase1ReKey
 *Description   : Function to increment phase1 rekey
 *Input(s)      : None
 *Output(s)     : None.
 *Return Values : None.
 *-----------------------------------------------------------------*/

VOID
IncIkePhase1ReKey (VOID)
{
    gpIkeStatisticsInfo[IKE_INDEX_0].u4IkePhase1Rekey++;
}

/* ---------------------------------------------------------------
 *Function Name : IncIkePhase2ReKey
 *Description   : Function to increment phase1 rekey
 *Input(s)      : None
 *Output(s)     : None.
 *Return Values : None.
 *-----------------------------------------------------------------*/
VOID
IncIkePhase2ReKey (VOID)
{
    gpIkeStatisticsInfo[IKE_INDEX_0].u4IkePhase2Rekey++;
}

/* ---------------------------------------------------------------
 * Function Name : IncIkeNoOfPhase1SessionsSucc
 * Description   : Function to increment number of phase1 
 *               : Sessions successful
 * Input(s)      : None
 * Output(s)     : None.
 * Return Values : None. 
 -----------------------------------------------------------------*/

VOID
IncIkeNoOfPhase1SessionsSucc (VOID)
{
    gpIkeStatisticsInfo[IKE_INDEX_0].u4IkeNoOfPhase1SessionsSucceed++;
}

/* ---------------------------------------------------------------
 * Function Name : IncIkeNoOfPhase1SessionsFailed
 * Description   : Function to increment number of phase1 
 *               : Sessions failed
 * Input(s)      : None
 * Output(s)     : None.
 * Return Values : None. 
 -----------------------------------------------------------------*/

VOID
IncIkeNoOfPhase1SessionsFailed (VOID)
{
    gpIkeStatisticsInfo[IKE_INDEX_0].u4IkeNoOfPhase1SessionsFailed++;
}
