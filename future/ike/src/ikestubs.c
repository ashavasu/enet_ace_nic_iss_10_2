/*******************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ikestubs.c,v 1.3 2014/05/02 10:50:40 siva Exp $
*
* Description: IPSec related API stubs required for IKE stack compilation 
*******************************************************************************/

#ifndef _IKE_STUBS_C_
#define _IKE_STUBS_C_

#include "ikegen.h"

INT1 nmhGetFsipv4SecGlobalStatus PROTO ((INT4 *pi4RetValFsipv4SecGlobalStatus));

INT1 nmhGetFsipv4SecIfInPkts PROTO ((INT4 i4Fsipv4SecIfIndex,
                                     UINT4 *pu4RetValFsipv4SecIfInPkts));

INT1 nmhGetFsipv4SecIfOutPkts PROTO ((INT4 i4Fsipv4SecIfIndex,
                                      UINT4 *pu4RetValFsipv4SecIfOutPkts));

INT1 nmhGetFsipv4SecIfPktsApply PROTO ((INT4 i4Fsipv4SecIfIndex,
                                        UINT4 *pu4RetValFsipv4SecIfPktsApply));

INT1 nmhGetFsipv4SecIfPktsDiscard PROTO ((INT4 i4Fsipv4SecIfIndex,
                                          UINT4
                                          *pu4RetValFsipv4SecIfPktsDiscard));

INT1 nmhTestv2Fsipv4SecGlobalStatus PROTO ((UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsipv4SecGlobalStatus));

INT1 nmhSetFsipv4SecGlobalStatus PROTO ((INT4 i4SetValFsipv4SecGlobalStatus));

VOID Secv4GetAssocEntryCount PROTO ((UINT4 *u4IPSecSAsActive));
INT1 VpnFillIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));
INT1 VpnDeleteIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));
PUBLIC INT1
    nmhGetFsipv4SecDummyPktGen PROTO ((INT4 *pi4RetValFsipv4SecDummyPktGen));
PUBLIC INT1
    nmhGetFsipv4SecDummyPktParam
PROTO ((INT4 *pi4RetValFsipv4SecDummyPktParam));
PUBLIC INT1 nmhTestv2Fsipv4SecDummyPktGen
PROTO ((UINT4 *pu4ErrorCode, INT4 i4TestValFsipv4SecDummyPktGen));
PUBLIC INT1 nmhTestv2Fsipv4SecDummyPktParam
PROTO ((UINT4 *pu4ErrorCode, INT4 i4TestValFsipv4SecDummyPktParam));
PUBLIC INT1         nmhSetFsipv4SecDummyPktGen
PROTO ((INT4 i4SetValFsipv4SecDummyPktGen));
PUBLIC INT1         nmhSetFsipv4SecDummyPktParam
PROTO ((INT4 i4SetValFsipv4SecDummyPktParam));

/****************************************************************************
 Function    :  nmhGetFsipv4SecGlobalStatus
 Input       :  The Indices

                The Object
                retValFsipv4SecGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecGlobalStatus (INT4 *pi4RetValFsipv4SecGlobalStatus)
{
    UNUSED_PARAM (*pi4RetValFsipv4SecGlobalStatus);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                retValFsVpnDummyPktGen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDummyPktGen (INT4 *pi4RetValFsipv4SecDummyPktGen)
{
    UNUSED_PARAM (pi4RetValFsipv4SecDummyPktGen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVpnDummyPktParam
 Input       :  The Indices

                The Object
                retValFsVpnDummyPktParam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef VPN_WANTED
INT1
nmhGetFsVpnDummyPktParam (INT4 *pi4RetValFsVpnDummyPktParam)
{
    UNUSED_PARAM (pi4RetValFsVpnDummyPktParam);
    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  nmhGetFsipv4SecIfInPkts
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfInPkts (INT4 i4Fsipv4SecIfIndex,
                         UINT4 *pu4RetValFsipv4SecIfInPkts)
{
    UNUSED_PARAM (i4Fsipv4SecIfIndex);
    UNUSED_PARAM (*pu4RetValFsipv4SecIfInPkts);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfOutPkts
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfOutPkts (INT4 i4Fsipv4SecIfIndex,
                          UINT4 *pu4RetValFsipv4SecIfOutPkts)
{
    UNUSED_PARAM (i4Fsipv4SecIfIndex);
    UNUSED_PARAM (*pu4RetValFsipv4SecIfOutPkts);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfPktsApply
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfPktsApply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfPktsApply (INT4 i4Fsipv4SecIfIndex,
                            UINT4 *pu4RetValFsipv4SecIfPktsApply)
{
    UNUSED_PARAM (i4Fsipv4SecIfIndex);
    UNUSED_PARAM (*pu4RetValFsipv4SecIfPktsApply);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfPktsDiscard
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfPktsDiscard (INT4 i4Fsipv4SecIfIndex,
                              UINT4 *pu4RetValFsipv4SecIfPktsDiscard)
{

    UNUSED_PARAM (i4Fsipv4SecIfIndex);
    UNUSED_PARAM (*pu4RetValFsipv4SecIfPktsDiscard);
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecGlobalStatus
 Input       :  The Indices

                The Object
                testValFsipv4SecGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecGlobalStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsipv4SecGlobalStatus)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv4SecGlobalStatus);
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsipv4SecGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsipv4SecGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecGlobalStatus (INT4 i4SetValFsipv4SecGlobalStatus)
{
    UNUSED_PARAM (i4SetValFsipv4SecGlobalStatus);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                setValFsVpnDummyPktGen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDummyPktGen (INT4 i4SetValFsipv4SecDummyPktGen)
{
    UNUSED_PARAM (i4SetValFsipv4SecDummyPktGen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDummyPktParam
 Input       :  The Indices

                The Object
                setValFsVpnDummyPktParam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDummyPktParam (INT4 i4SetValFsipv4SecDummyPktParam)
{
    UNUSED_PARAM (i4SetValFsipv4SecDummyPktParam);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                testValFsVpnDummyPktGen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDummyPktGen (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv4SecDummyPktGen)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv4SecDummyPktGen);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDummyPktParam
 Input       :  The Indices

                The Object
                testValFsVpnDummyPktParam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDummyPktParam (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsipv4SecDummyPktParam)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsipv4SecDummyPktParam);
    return (SNMP_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv4GetAssocEntryCount                              */
/*  Description     : This function returns the total active SAs           */
/*  Input(s)        : u4IPSecSAsActive :Refers to the SA count             */
/*  Output(s)       : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/
VOID
Secv4GetAssocEntryCount (UINT4 *u4IPSecSAsActive)
{
    UNUSED_PARAM (*u4IPSecSAsActive);
    return;
}

/***************************************************************************/
/*  Function Name   : VpnFillIpsecParams                                   */
/*  Description     : This function configures the VPN Policy in the IPSec */
/*                  : module                                               */
/*  Input(s)        : pVpnPolicy - Pointer to the Vpn Policy               */
/*  Output(s)       : None                                                 */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                            */
/***************************************************************************/

INT1
VpnFillIpsecParams (tVpnPolicy * pVpnPolicy)
{
    UNUSED_PARAM (pVpnPolicy);
    return (SNMP_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : VpnDeleteIpsecParams                                 */
/*  Description     : This function deletes    the VPN Policy in the IPSec */
/*                  : module                                               */
/*  Input(s)        : pVpnPolicy - Pointer to the Vpn Policy               */
/*  Output(s)       : None                                                 */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                            */
/***************************************************************************/

INT1
VpnDeleteIpsecParams (tVpnPolicy * pVpnPolicy)
{
    UNUSED_PARAM (pVpnPolicy);
    return (SNMP_SUCCESS);
}

#endif
