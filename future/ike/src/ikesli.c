/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikesli.c,v 1.17 2014/08/25 11:56:10 siva Exp $
 *
 * Description: This has interface APIs to the socket layer
 *
 ***********************************************************************/

#include "ikegen.h"
#include "fssocket.h"
#include "ikememmac.h"

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeReadFromSocketLayer
 *  Description   :  This will read data from the socket layer and posts
 *                   the packet to the main task.
 *  Input(s)      :  *i1pParam - UNUSED
 *  Output(s)     :  NONE
 *  Return Values :  NONE
 * ---------------------------------------------------------------
 */

VOID
IkeSockSelectTaskMain (INT1 *i1pParam)
{
    fd_set              readFds;
    INT4                i4AddrLen = sizeof (struct sockaddr_in);
    INT4                i4RetVal = IKE_ZERO;
    INT4                i4DataLen = IKE_ZERO;
    UINT1              *pu1RecvBuff = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pTempIkeEngine = NULL;
    tIkeQMsg           *pIkeQueue = NULL;
    tIkePktBuffer       IkePktBuffer;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    INT2                i2SelectFd = IKE_INVALID;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;
    tIkeIpAddr          sPeerAddr;
    struct sockaddr_in  SocketAddr;
#ifdef IP6_WANTED
    struct sockaddr_in6 SocketAddr_i6;
#endif

    MEMSET (au1EngineName, IKE_ZERO, MAX_NAME_LENGTH);
    MEMSET (&readFds, IKE_ZERO, sizeof (fd_set));
    UNUSED_PARAM (i1pParam);

    while (IKE_TRUE)
    {
        MEMSET (&sPeerAddr, IKE_ZERO, sizeof (sPeerAddr));

        /* Fill the required IKE related parameters from the VPN Policy */
        STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, MAX_NAME_LENGTH);
        EngineNameOctetStr.pu1_OctetList = au1EngineName;
        EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

        pIkeEngine = IkeSnmpGetEngine (EngineNameOctetStr.pu1_OctetList,
                                       EngineNameOctetStr.i4_Length);
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "VpnIkeCreateAndUpdatePolicyDB:"
                         "Engine does not exist\r\n");
            return;
        }
        FD_ZERO (&readFds);

        /* If the socket descriptor is not set, do not do a select 
         */
        if (pIkeEngine->i2SocketId == IKE_INVALID)
        {
            continue;
        }
        if (pIkeEngine->i2NattSocketFd == IKE_INVALID)
        {
            continue;
        }
#ifdef IP6_WANTED
        if (pIkeEngine->i2V6SocketFd == IKE_INVALID)
        {
            continue;
        }
        if (pIkeEngine->i2V6NattSocketFd == IKE_INVALID)
        {
            continue;
        }
#endif

        FD_ZERO (&readFds);

        /* Set the list of  socket descriptors which needs
         * to be verified for  reading
         */
        FD_SET (pIkeEngine->i2SocketId, &readFds);
        FD_SET (pIkeEngine->i2NattSocketFd, &readFds);
#ifdef IP6_WANTED
        FD_SET (pIkeEngine->i2V6SocketFd, &readFds);
        FD_SET (pIkeEngine->i2V6NattSocketFd, &readFds);
#endif
        i2SelectFd = pIkeEngine->i2SocketId;
        if (i2SelectFd < pIkeEngine->i2NattSocketFd)
        {
            i2SelectFd = pIkeEngine->i2NattSocketFd;
        }
#ifdef IP6_WANTED
        if (i2SelectFd < pIkeEngine->i2V6SocketFd)
        {
            i2SelectFd = pIkeEngine->i2V6SocketFd;
        }
        if (i2SelectFd < pIkeEngine->i2V6NattSocketFd)
        {
            i2SelectFd = pIkeEngine->i2V6NattSocketFd;
        }
#endif
        if ((i4RetVal = select (i2SelectFd + IKE_ONE, &readFds,
                                NULL, NULL, NULL)) > IKE_ZERO)
        {
            if (FD_ISSET (pIkeEngine->i2SocketId, &readFds))
            {
                i4AddrLen = sizeof (struct sockaddr_in);

                /* Allocate memory for incoming packet */

                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pu1RecvBuff) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: unable to allocate "
                                 "buffer\n");
                    return;
                }

                if (pu1RecvBuff == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Unable allocate Memory "
                                 "for Packet\n");
                    return;
                }

                SocketAddr.sin_family = AF_INET;
                SocketAddr.sin_addr.s_addr = IKE_ZERO;
                SocketAddr.sin_port = IKE_HTONS (IKE_ZERO);
                if ((i4DataLen = recvfrom (pIkeEngine->i2SocketId, pu1RecvBuff,
                                           (INT4) IKE_MAX_DATA_SIZE, IKE_ZERO,
                                           (struct sockaddr *) &SocketAddr,
                                           &i4AddrLen)) <= IKE_ZERO)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    perror ("recvfrom failed\r\n");
                }

                /* Get the Engine index from the packet that is
                 * received */
                sPeerAddr.u4AddrType = IPV4ADDR;
                sPeerAddr.Ipv4Addr = IKE_NTOHL (SocketAddr.sin_addr.s_addr);

                pIkeEngine = NULL;

                SLL_SCAN (&gIkeEngineList, pTempIkeEngine, tIkeEngine *)
                {
                    SLL_SCAN (&(pTempIkeEngine->IkeCryptoMapList),
                              pIkeCryptoMap, tIkeCryptoMap *)
                    {
                        if (IKE_ZERO == IKE_MEMCMP
                            (&(pIkeCryptoMap->PeerAddr),
                             &sPeerAddr, sizeof (sPeerAddr)))
                        {
                            /* Engine found */
                            pIkeEngine = pTempIkeEngine;
                            break;
                        }
                    }
                    if (NULL != pIkeEngine)
                    {
                        break;
                    }
                }

                if (NULL == pIkeEngine)
                {
                    /* Engine not found so map the first policy from the list 
                     * This is a temporary fix added to
                     * make it work for RAVPN server
                     * case where peer IP of RAVPN client is not known */
                    SLL_SCAN (&gIkeEngineList, pTempIkeEngine, tIkeEngine *)
                    {
                        if (STRCMP
                            (pTempIkeEngine->au1EngineName,
                             IKE_DUMMY_ENGINE) != IKE_ZERO)
                        {
                            pIkeEngine = pTempIkeEngine;
                        }
                    }
                }
                if (NULL == pIkeEngine)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Failed to determine"
                                 "the engine from the packet received\n");

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    continue;

                }

                IkePktBuffer.PeerAddr.u4AddrType = IPV4ADDR;
                IKE_MEMCPY (&IkePktBuffer.PeerAddr.Ipv4Addr,
                            (UINT1 *) &SocketAddr.sin_addr.s_addr,
                            sizeof (tIp4Addr));
                IkePktBuffer.u2RemotePort = IKE_HTONS (SocketAddr.sin_port);
                IkePktBuffer.u2LocalPort = IKE_UDP_PORT;
                if (i4DataLen > IKE_ZERO)
                {
                    IkePktBuffer.pu1PktBuffer = pu1RecvBuff;
                    IkePktBuffer.u4PktLen = (UINT4) i4DataLen;
                    IkePktBuffer.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;

                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pIkeQueue) == MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeSocketHandler: Failed to determine"
                                     "the engine from the packet received\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                    if (pIkeQueue == NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    /* Fill the Message details  required for processing the 
                     * event */
                    pIkeQueue->u4MsgType = IKE_PKT_ARRIVAL;
                    IKE_MEMCPY (&pIkeQueue->IkeIfParam.IkePktBuffer,
                                &IkePktBuffer, sizeof (tIkePktBuffer));
                    if (OsixSendToQ
                        (SELF, IKE_QUEUE_NAME, (tOsixMsg *) pIkeQueue,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pIkeQueue);
                        continue;
                    }

                    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT)
                        != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                }
                else
                {
                    if (pu1RecvBuff != NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                    }
                }                /* RecvMsg Succ Ends */
            }                    /* FD_ISSET Ends for port 500 */

            if (FD_ISSET (pIkeEngine->i2NattSocketFd, &readFds))
            {
                i4AddrLen = sizeof (struct sockaddr_in);

                /* Allocate memory for incoming packet */
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pu1RecvBuff) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: unable to allocate"
                                 "buffer\n");
                    return;
                }

                if (pu1RecvBuff == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Unable allocate Memory "
                                 "for Packet from Port 4500\n");
                    return;
                }

                SocketAddr.sin_family = AF_INET;
                SocketAddr.sin_addr.s_addr = IKE_ZERO;
                SocketAddr.sin_port = IKE_HTONS (IKE_ZERO);
                if ((i4DataLen = recvfrom (pIkeEngine->i2NattSocketFd,
                                           pu1RecvBuff,
                                           (INT4) IKE_MAX_DATA_SIZE, IKE_ZERO,
                                           (struct sockaddr *) &SocketAddr,
                                           &i4AddrLen)) <= IKE_ZERO)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                }

                /* Get the Engine index from the packet that is
                 * received */
                sPeerAddr.u4AddrType = IPV4ADDR;
                sPeerAddr.Ipv4Addr = IKE_NTOHL (SocketAddr.sin_addr.s_addr);

                pIkeEngine = NULL;

                SLL_SCAN (&gIkeEngineList, pTempIkeEngine, tIkeEngine *)
                {
                    SLL_SCAN (&(pTempIkeEngine->IkeCryptoMapList),
                              pIkeCryptoMap, tIkeCryptoMap *)
                    {
                        if (IKE_ZERO == IKE_MEMCMP
                            (&(pIkeCryptoMap->PeerAddr),
                             &sPeerAddr, sizeof (sPeerAddr)))
                        {
                            /* Engine found */
                            pIkeEngine = pTempIkeEngine;
                            break;
                        }
                    }
                    if (NULL != pIkeEngine)
                    {
                        break;
                    }
                }
                if (NULL == pIkeEngine)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Failed to determine"
                                 "the engine from the packet received\n");

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    continue;

                }

                IkePktBuffer.PeerAddr.u4AddrType = IPV4ADDR;
                IKE_MEMCPY (&IkePktBuffer.PeerAddr.Ipv4Addr,
                            (UINT1 *) &SocketAddr.sin_addr.s_addr,
                            sizeof (tIp4Addr));
                IkePktBuffer.u2RemotePort = IKE_HTONS (SocketAddr.sin_port);
                IkePktBuffer.u2LocalPort = IKE_NATT_UDP_PORT;
                if (i4DataLen > IKE_ZERO)
                {
                    IkePktBuffer.pu1PktBuffer = pu1RecvBuff;
                    IkePktBuffer.u4PktLen = (UINT4) i4DataLen;
                    IkePktBuffer.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pIkeQueue) == MEM_FAILURE)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                    if (pIkeQueue == NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    /* Fill the Message details  required for processing the 
                     * event */
                    pIkeQueue->u4MsgType = IKE_PKT_ARRIVAL;
                    IKE_MEMCPY (&pIkeQueue->IkeIfParam.IkePktBuffer,
                                &IkePktBuffer, sizeof (tIkePktBuffer));

                    if (OsixSendToQ
                        (SELF, IKE_QUEUE_NAME, (tOsixMsg *) pIkeQueue,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pIkeQueue);
                        continue;
                    }

                    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT)
                        != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                }
                else
                {
                    if (pu1RecvBuff != NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                    }
                }                /* RecvMsg Succ Ends */
            }                    /* FD_ISSET Ends for Port 4500 */
#ifdef IP6_WANTED
            if (FD_ISSET (pIkeEngine->i2V6SocketFd, &readFds))
            {
                /* Allocate memory for incoming packet */
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pu1RecvBuff) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: unable to allocate"
                                 "buffer\n");
                    return;
                }

                if (pu1RecvBuff == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Unable allocate Memory "
                                 "for Packet\n");
                    return;
                }

                SocketAddr_i6.sin6_family = AF_INET6;
                IKE_BZERO (SocketAddr_i6.sin6_addr.s6_addr, IKE_IP6_ADDR_LEN);
                SocketAddr_i6.sin6_port = IKE_HTONS (IKE_ZERO);

                if ((i4DataLen = recvfrom (pIkeEngine->i2V6SocketFd,
                                           pu1RecvBuff,
                                           (INT4) IKE_MAX_DATA_SIZE, IKE_ZERO,
                                           (struct sockaddr *) &SocketAddr_i6,
                                           &i4AddrLen)) <= IKE_ZERO)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    perror ("recvfrom failed\r\n");
                    continue;
                }

                /* Get the Engine index from the packet that is
                 * received */
                sPeerAddr.u4AddrType = IPV6ADDR;
                IKE_MEMCPY (sPeerAddr.Ipv6Addr.u1_addr,
                            (UINT1 *) (&SocketAddr_i6.sin6_addr.s6_addr),
                            sizeof (sPeerAddr.Ipv6Addr.u1_addr));

                pIkeEngine = NULL;

                SLL_SCAN (&gIkeEngineList, pTempIkeEngine, tIkeEngine *)
                {
                    SLL_SCAN (&(pTempIkeEngine->IkeCryptoMapList),
                              pIkeCryptoMap, tIkeCryptoMap *)
                    {
                        if (IKE_ZERO == IKE_MEMCMP
                            (&(pIkeCryptoMap->PeerAddr),
                             &sPeerAddr, sizeof (sPeerAddr)))
                        {
                            /* Engine found */
                            pIkeEngine = pTempIkeEngine;
                            break;
                        }
                    }
                    if (NULL != pIkeEngine)
                    {
                        break;
                    }
                }
                if (NULL == pIkeEngine)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Failed to determine"
                                 "the engine from the packet received\n");

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    continue;

                }

                IkePktBuffer.PeerAddr.u4AddrType = IPV6ADDR;
                IKE_MEMCPY (&IkePktBuffer.PeerAddr.Ipv6Addr,
                            (UINT1 *) &SocketAddr_i6.sin6_addr.s6_addr,
                            sizeof (tIp6Addr));
                IkePktBuffer.u2RemotePort = IKE_HTONS (SocketAddr_i6.sin6_port);
                IkePktBuffer.u2LocalPort = IKE_UDP_PORT;
                if (i4DataLen > IKE_ZERO)
                {
                    IkePktBuffer.pu1PktBuffer = pu1RecvBuff;
                    IkePktBuffer.u4PktLen = (UINT4) i4DataLen;
                    IkePktBuffer.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;

                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pIkeQueue) == MEM_FAILURE)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    if (pIkeQueue == NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    /* Fill the Message details  required for processing the 
                     * event */
                    pIkeQueue->u4MsgType = IKE_PKT_ARRIVAL;
                    IKE_MEMCPY (&pIkeQueue->IkeIfParam.IkePktBuffer,
                                &IkePktBuffer, sizeof (tIkePktBuffer));

                    if (OsixSendToQ
                        (SELF, IKE_QUEUE_NAME, (tOsixMsg *) pIkeQueue,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pIkeQueue);
                        continue;
                    }

                    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT)
                        != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                }
            }                    /* Select Ends */
            if (FD_ISSET (pIkeEngine->i2V6NattSocketFd, &readFds))
            {
                /* Allocate memory for incoming packet */
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pu1RecvBuff) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: unable to allocate"
                                 "buffer\n");
                    return;
                }

                if (pu1RecvBuff == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Unable allocate Memory "
                                 "for Packet\n");
                    return;
                }

                SocketAddr_i6.sin6_family = AF_INET6;
                IKE_BZERO (SocketAddr_i6.sin6_addr.s6_addr, IKE_IP6_ADDR_LEN);
                SocketAddr_i6.sin6_port = IKE_HTONS (IKE_ZERO);

                if ((i4DataLen = recvfrom (pIkeEngine->i2V6NattSocketFd,
                                           pu1RecvBuff,
                                           (INT4) IKE_MAX_DATA_SIZE, IKE_ZERO,
                                           (struct sockaddr *) &SocketAddr_i6,
                                           &i4AddrLen)) <= IKE_ZERO)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    perror ("recvfrom failed\r\n");
                    continue;
                }

                /* Get the Engine index from the packet that is
                 * received */
                sPeerAddr.u4AddrType = IPV6ADDR;
                IKE_MEMCPY (sPeerAddr.Ipv6Addr.u1_addr,
                            (UINT1 *) (&SocketAddr_i6.sin6_addr.s6_addr),
                            sizeof (tIp6Addr));

                pIkeEngine = NULL;

                SLL_SCAN (&gIkeEngineList, pTempIkeEngine, tIkeEngine *)
                {
                    SLL_SCAN (&(pTempIkeEngine->IkeCryptoMapList),
                              pIkeCryptoMap, tIkeCryptoMap *)
                    {
                        if (IKE_ZERO == IKE_MEMCMP
                            (&(pIkeCryptoMap->PeerAddr),
                             &sPeerAddr, sizeof (sPeerAddr)))
                        {
                            /* Engine found */
                            pIkeEngine = pTempIkeEngine;
                            break;
                        }
                    }
                    if (NULL != pIkeEngine)
                    {
                        break;
                    }
                }
                if (NULL == pIkeEngine)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeSocketHandler: Failed to determine"
                                 "the engine from the packet received\n");

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1RecvBuff);
                    pu1RecvBuff = NULL;
                    continue;

                }

                IkePktBuffer.PeerAddr.u4AddrType = IPV6ADDR;
                IKE_MEMCPY (&IkePktBuffer.PeerAddr.Ipv6Addr,
                            (UINT1 *) &SocketAddr_i6.sin6_addr.s6_addr,
                            sizeof (tIp6Addr));
                IkePktBuffer.u2RemotePort = IKE_HTONS (SocketAddr_i6.sin6_port);
                IkePktBuffer.u2LocalPort = IKE_NATT_UDP_PORT;
                if (i4DataLen > IKE_ZERO)
                {
                    IkePktBuffer.pu1PktBuffer = pu1RecvBuff;
                    IkePktBuffer.u4PktLen = (UINT4) i4DataLen;
                    IkePktBuffer.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;

                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pIkeQueue) == MEM_FAILURE)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    if (pIkeQueue == NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }

                    /* Fill the Message details  required for processing the 
                     * event */
                    pIkeQueue->u4MsgType = IKE_PKT_ARRIVAL;
                    IKE_MEMCPY (&pIkeQueue->IkeIfParam.IkePktBuffer,
                                &IkePktBuffer, sizeof (tIkePktBuffer));

                    if (OsixSendToQ
                        (SELF, IKE_QUEUE_NAME, (tOsixMsg *) pIkeQueue,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pIkeQueue);
                        continue;
                    }

                    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT)
                        != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1RecvBuff);
                        pu1RecvBuff = NULL;
                        continue;
                    }
                }
            }                    /* FD_ISSET ends */
#endif
        }                        /* Select Ends */
    }                            /* while Ends */
}                                /* Func Ends */

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeSendToSocketLayer 
 *  Description   : This will the data to the socket layer for 
 *                  peer communication.
 *  Input(s)      : pSessionInfo - SessionInfo from data base.
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */

VOID
IkeSendToSocketLayer (tIkeSessionInfo * pSessionInfo)
{
    INT2                i2SocketId = pSessionInfo->i2SocketId;
    UINT2               u2Len = sizeof (struct sockaddr_in);
    UINT4               u4PktLen = IKE_ZERO;
    tIsakmpHdr         *pIsakmpHdr = NULL;
    UINT1              *pu1TempBuf = NULL;
    struct sockaddr_in  SocketAddr;
#ifdef IP6_WANTED
    struct sockaddr_in6 SocketAddr_i6;
#endif

    if ((pSessionInfo->u1IkeVer == IKE2_MAJOR_VERSION) &&
        (pSessionInfo->u1ReTransCount == IKE_RETRANSMIT_COUNT))
    {
        TAKE_IKE_SEM ();
        pIsakmpHdr =
            (tIsakmpHdr *) (void *) pSessionInfo->IkeTxPktInfo.pu1RawPkt;

        if (!(pIsakmpHdr->u1Flags & IKE2_FLAG_RESPONSE))
        {
            /* If you are an sending out a request message, Increment the 
             * TxMsgId, so for the next time you are the sending a request
             * you will have the new message Id */
            pSessionInfo->pIkeSA->Ike2SA.u4TxMsgId++;
        }
        else
        {
            /* If you are the responder for the Particular session, 
             * Next packet that you get from the Peer will have the 
             * next message Id. */
            pSessionInfo->pIkeSA->Ike2SA.u4RxMsgId++;
            pSessionInfo->u4MsgId = pSessionInfo->pIkeSA->Ike2SA.u4RxMsgId;
        }
        GIVE_IKE_SEM ();
    }

    u4PktLen = IKE_NTOHL
        (((tIsakmpHdr *) (void *) (pSessionInfo->IkeTxPktInfo.pu1RawPkt))->
         u4TotalLen);
    pu1TempBuf = pSessionInfo->IkeTxPktInfo.pu1RawPkt;

    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT)
    {
        i2SocketId = pSessionInfo->pIkeSA->IkeNattInfo.i2NattSocketFd;
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pu1TempBuf) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSendToSocketLayer: unable to allocate "
                         "buffer\n");
            return;
        }

        if (pu1TempBuf == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSendToSocketLayer: Malloc Failed\n");
            return;
        }

        pu1TempBuf[IKE_INDEX_0] = IKE_ZERO;
        pu1TempBuf[IKE_INDEX_1] = IKE_ZERO;
        pu1TempBuf[IKE_INDEX_2] = IKE_ZERO;
        pu1TempBuf[IKE_INDEX_3] = IKE_ZERO;

        IKE_MEMCPY ((pu1TempBuf + IKE_IPSEC_NON_ESP_MARKER_SIZE),
                    pSessionInfo->IkeTxPktInfo.pu1RawPkt, u4PktLen);
        u4PktLen = u4PktLen + IKE_IPSEC_NON_ESP_MARKER_SIZE;
    }

    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
#ifdef IP6_WANTED
        u2Len = sizeof (struct sockaddr_in6);
        SocketAddr_i6.sin6_family = AF_INET6;

        IKE_MEMCPY (SocketAddr_i6.sin6_addr.s6_addr,
                    &pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr,
                    IKE_IP6_ADDR_LEN);

        SocketAddr_i6.sin6_port = IKE_HTONS (pSessionInfo->pIkeSA->
                                             IkeNattInfo.u2RemotePort);

        if (sendto ((INT4) i2SocketId, pu1TempBuf, (INT4) u4PktLen,
                    IKE_ZERO, (struct sockaddr *) &SocketAddr_i6,
                    (INT4) u2Len) != (INT4) u4PktLen)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSendToSocketLayer: Sending Error\n");
        }
#endif
    }
    else
    {

        SocketAddr.sin_family = AF_INET;
        SocketAddr.sin_addr.s_addr =
            IKE_HTONL (pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr);
        SocketAddr.sin_port = IKE_HTONS (pSessionInfo->pIkeSA->
                                         IkeNattInfo.u2RemotePort);
        if (sendto ((INT4) i2SocketId, pu1TempBuf, (INT4) u4PktLen,
                    IKE_ZERO, (struct sockaddr *) &SocketAddr,
                    (INT4) u2Len) != (INT4) u4PktLen)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSendToSocketLayer: Sending Error\n");
        }

    }

    if (pSessionInfo->pIkeSA->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TempBuf);
    }

    IncIkeOutPkts (&pSessionInfo->pIkeSA->IpLocalAddr);

    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeStartListener           
 *  Description   : Function to start listener                  
 *  Input(s)      : pIkeEngine - Pointer to IkeEngine          
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeStartListener (tIkeEngine * pIkeEngine)
{
    struct sockaddr_in  clientAddr, tIkeNattAddr;

#ifdef IP6_WANTED
    struct sockaddr_in6 IkeAddress6;
    INT4                i4OptVal = IKE_ZERO;
    INT4                i4ErrVal = IKE_ZERO;
#endif

    if (pIkeEngine->i2SocketId != IKE_INVALID)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Socket Already listening"
                     "on port 500\n");
        return IKE_SUCCESS;
    }

    if (pIkeEngine->i2NattSocketFd != IKE_INVALID)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Socket Already listening"
                     "on port 4500\n");
        return IKE_SUCCESS;
    }

#ifdef IP6_WANTED
    if (pIkeEngine->i2V6SocketFd != IKE_INVALID)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Socket Already listening"
                     "on port IPv6\n");
        return IKE_SUCCESS;
    }
    if (pIkeEngine->i2V6NattSocketFd != IKE_INVALID)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Socket Already listening"
                     "on port IPv6\n");
        return IKE_SUCCESS;
    }
#endif
    pIkeEngine->i2SocketId = (INT2) socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (pIkeEngine->i2SocketId < IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to create Socket for peer "
                     "communction\n");
        return IKE_FAILURE;
    }

    pIkeEngine->i2NattSocketFd =
        (INT2) socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (pIkeEngine->i2NattSocketFd < IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to create Socket for peer "
                     "communction for NATT\n");
        return IKE_FAILURE;
    }

    if (fcntl (pIkeEngine->i2SocketId, F_SETFL, O_NONBLOCK) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to make ike socket as "
                     "non blocking\n");
        close (pIkeEngine->i2SocketId);
        return IKE_FAILURE;
    }

    if (fcntl (pIkeEngine->i2NattSocketFd, F_SETFL, O_NONBLOCK) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to make ike NATT socket as "
                     "non blocking\n");
        close (pIkeEngine->i2NattSocketFd);
        return IKE_FAILURE;
    }

    IKE_BZERO (&clientAddr, sizeof (clientAddr));
    clientAddr.sin_family = AF_INET;

    /* Ike Will be listening on any (0.0.0.0) IP address */
    clientAddr.sin_addr.s_addr = IKE_ZERO;
    clientAddr.sin_port = IKE_HTONS (IKE_UDP_PORT);

    if (bind (pIkeEngine->i2SocketId, (struct sockaddr *) &clientAddr,
              sizeof (clientAddr)) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to bind port 500 for peer "
                     "commn\n");
        close (pIkeEngine->i2SocketId);
        return IKE_FAILURE;
    }

    IKE_BZERO (&tIkeNattAddr, sizeof (tIkeNattAddr));
    tIkeNattAddr.sin_family = AF_INET;

    /* Ike Will be listening on any (0.0.0.0) IP address */
    tIkeNattAddr.sin_addr.s_addr = IKE_ZERO;
    tIkeNattAddr.sin_port = IKE_HTONS (IKE_NATT_UDP_PORT);

    if (bind (pIkeEngine->i2NattSocketFd, (struct sockaddr *) &tIkeNattAddr,
              sizeof (tIkeNattAddr)) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to bind port 4500 for peer "
                     "communction for IPv6\n");
        close (pIkeEngine->i2NattSocketFd);
        return IKE_FAILURE;
    }

#ifdef IP6_WANTED
    /* Fill in local address details for Ipv6 */
    IKE_BZERO (&IkeAddress6, sizeof (IkeAddress6));

    IkeAddress6.sin6_family = AF_INET6;
    IkeAddress6.sin6_port = OSIX_HTONS (IKE_UDP_PORT);

    pIkeEngine->i2V6SocketFd = (INT2) socket (AF_INET6, SOCK_DGRAM, IKE_ZERO);
    if (pIkeEngine->i2V6SocketFd < IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to create Socket for peer "
                     "communction for IPv6\n");
        return IKE_FAILURE;
    }

    i4OptVal = OSIX_TRUE;
    i4ErrVal = setsockopt (pIkeEngine->i2V6SocketFd, IPPROTO_IPV6,
                           IPV6_V6ONLY, &i4OptVal, sizeof (i4OptVal));
    if (i4ErrVal < IKE_ZERO)
    {
        return (IKE_FAILURE);

    }

    if (fcntl (pIkeEngine->i2V6SocketFd, F_SETFL, O_NONBLOCK) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to make ike NATT socket as "
                     "non blocking\n");
        close (pIkeEngine->i2V6SocketFd);
        return IKE_FAILURE;
    }
    if ((i4ErrVal = bind (pIkeEngine->i2V6SocketFd,
                          (struct sockaddr *) &IkeAddress6,
                          sizeof (IkeAddress6))) < IKE_ZERO)
    {
        close (pIkeEngine->i2V6SocketFd);
        return (IKE_FAILURE);
    }

    /* Fill in local address details for Ipv6 */
    IKE_BZERO (&IkeAddress6, sizeof (IkeAddress6));

    IkeAddress6.sin6_family = AF_INET6;
    IkeAddress6.sin6_port = OSIX_HTONS (IKE_NATT_UDP_PORT);

    pIkeEngine->i2V6NattSocketFd =
        (INT2) socket (AF_INET6, SOCK_DGRAM, IKE_ZERO);
    if (pIkeEngine->i2V6NattSocketFd < IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to create Socket for peer "
                     "communction for IPv6\n");
        return IKE_FAILURE;
    }

    i4OptVal = OSIX_TRUE;
    i4ErrVal = setsockopt (pIkeEngine->i2V6NattSocketFd, IPPROTO_IPV6,
                           IPV6_V6ONLY, &i4OptVal, sizeof (i4OptVal));
    if (i4ErrVal < IKE_ZERO)
    {
        return (IKE_FAILURE);

    }
    if (fcntl (pIkeEngine->i2V6NattSocketFd, F_SETFL, O_NONBLOCK) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartListener:Unable to make ike NATT socket as "
                     "non blocking\n");
        close (pIkeEngine->i2V6NattSocketFd);
        return IKE_FAILURE;
    }
    if ((i4ErrVal = bind (pIkeEngine->i2V6NattSocketFd,
                          (struct sockaddr *) &IkeAddress6,
                          sizeof (IkeAddress6))) < IKE_ZERO)
    {
        close (pIkeEngine->i2V6NattSocketFd);
        return (IKE_FAILURE);
    }
#endif
    return (IKE_SUCCESS);
}
