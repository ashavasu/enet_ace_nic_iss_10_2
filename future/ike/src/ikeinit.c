/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikeinit.c,v 1.13 2015/04/09 06:02:02 siva Exp $
 *
 * Description: This file contains the IKE initialization routines    
 *
 ***********************************************************************/
#ifndef __IKE_INIT_C__
#define __IKE_INIT_C__

#include "ikegen.h"
#include "ikecertsave.h"
#include "ikersa.h"
#include "ikestat.h"
#include "fssocket.h"
#include "fsopenssl.h"
#include "ikememmac.h"
#ifdef NPAPI_WANTED
#include "ikenp.h"
#endif
#include "fssyslog.h"

INT4                gi4IkeSysLogId;

extern t_SLL        gIkeVpnPolicyList;

UINT4               gu4IkeTrace = IKE_ZERO;
tOsixTaskId         gu4IkeSockSelTaskId;
tOsixSemId          gIkeSemId;
UINT4               gu4NumIkeEngines = IKE_ZERO;
tIkeEngToIntMap     gaIkeEngToIntMap[IKE_MAX_ENGINE];

static VOID         IkeProcessQMsg (VOID);
static VOID         IkeTaskShutDown (VOID);
static INT1         IkeInit (VOID);
static VOID         IkeProcessPkt (tIkePktBuffer * pIkePktBuffer);

/* ---------------------------------------------------------------
 *  Function Name : IkeInit 
 *  Description   : Function to initialise the session database,
 *                  create queue for ipsec communication,
 *                  create timer for retransmission and create
 *                  socket for ike peer communication.
 *  Input(s)      : None. 
 *  Output(s)     : pSessionInfo - Pointer to the session
 *                                 linked list.
 *                  pSocketId    - Pointer to the ike socket id.
 *                  pTimerId     - Pointer to the ike timer id.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeInit (VOID)
{
    tOsixQId            QId;
    UINT4               u4Seed = IKE_ZERO;

    IkeEngineInitialise ();
    IkeInitVpnPolicy ();        /* Initialize VPN Policy table */

    OSIX_SEED (u4Seed);
    OSIX_SRAND (u4Seed);

#ifdef SYSLOG_WANTED
    gi4IkeSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "IKE", SYSLOG_INFO_LEVEL);
#endif
    /* Create Queue for IPSec to post message to IKE */
    if (OsixCreateQ (IKE_QUEUE_NAME, IKE_INTERFACE_Q_DEPTH,
                     (UINT4) ZERO, &QId) == OSIX_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit:Unable to create queue for IPSec\n");
        return IKE_FAILURE;
    }

    if (OsixCreateSem (IKE_GLOBAL_SEM_NAME, IKE_ONE, OSIX_LOCAL, &gIkeSemId)
        == OSIX_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit:Unable to Create Global semaphore\n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        return IKE_FAILURE;
    }

    /* Create Timer */
    if (TmrCreateTimerList (IKE_TASK_NAME, IKE_TIMER0_EVENT,
                            NULL,
                            (tTimerListId *) (VOID *) &gu4TimerId) !=
        TMR_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit:Unable to create Timer for retrnasmision\n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        return IKE_FAILURE;
    }

    /*Create memory pools required for IKEv2 */
    if (Ike2InitMemPoolInit () == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit: Memory pool creation failed for ikev2\n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        return IKE_FAILURE;
    }
    if (IkeSizingMemCreateMemPools () == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit: Memory pool creation failed for ikev2\n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        return IKE_FAILURE;
    }
    if (MemCreateMemPool (sizeof (tIkeQMsg), MAX_IKE_QUEUE_MESSAGE,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &IKE_MSG_MEMPOOL_ID) != MEM_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit: Memory pool creation failed for ikev2\n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        return IKE_FAILURE;
    }

    /* Initialize the Authentication Data base for RA VPN */
    AuthInit ();

    IkeTableInitialise ();

    /* Initialize the OpenSSL Library */
    FSOpenSslInit ();

    /* Create and Initialize the default dummy ENGINE That is used 
     * for the socket communication and certificate storing */

    /* Actual engines for key negitiation will be created at
     * the time of security policy association with a particular interface */

    IkeEngineInit ((UINT1 *) IKE_DUMMY_ENGINE);

    /* Crate and bind the socket to any IP address */
    IkeSocketInit ();

    /* Create and start a new task to listen and interrupt main
     * task when a IKE packet is recieved */
    if (OsixCreateTask (IKE_SOCKET_SELECT_TASK_NAME,
                        IKE_SELECT_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        IkeSockSelectTaskMain, NULL,
                        IKE_SELECT_TASK_MODE,
                        &gu4IkeSockSelTaskId) == OSIX_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeInit:Unable to create task for reading the packets"
                     " from socket \n");
        OsixDeleteQ (SELF, IKE_QUEUE_NAME);
        Ike2InitMemPoolDeInit ();
        return IKE_FAILURE;
    }
#ifdef NPAPI_WANTED
    /* Calling HW function to initialise the IKE */
    /* For CW, user defined filter to trap IKE packets is not 
       required as the IKE packets has to be processed in kernel */
#endif
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeTaskShutDown            
 *  Description   : Function to Shut down the IKE Task          
 *  Input(s)      : None                                       
 *  Output(s)     : None.
 *  Return Values : None                            
 * ---------------------------------------------------------------
 */

VOID
IkeTaskShutDown (VOID)
{
    /* Delete the Memory pools created for ikev2 */
    Ike2InitMemPoolDeInit ();
    IkeSizingMemDeleteMemPools ();
    IkeEngineDeInit ();
    /* Release the IKE Msg Pool Memory */
    MemDeleteMemPool (IKE_MSG_MEMPOOL_ID);
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeTaskMain 
 *  Description   : This is task entry point function. This function
 *                  polls queue,socket and timer for an event. Based 
 *                  on the event it will call diferent functions.
 *  Input(s)      : None. 
 *  Output(s)     : None.
 *  Return Values : None. 
 * ---------------------------------------------------------------
 */
PUBLIC UINT4
IkeTaskMain ()
{
    UINT4               u4Event = IKE_ZERO;
    UINT4               u4EventTimer = IKE_ZERO;
    UINT4               u4EventQ = IKE_ZERO;
    UINT4               u4OsixRecv = IKE_ZERO;

    /* Call IkeMainInitialise functions for necessary Initialisation */
    if (IkeInit () == IKE_FAILURE)
    {
        IkeTaskShutDown ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    lrInitComplete (OSIX_SUCCESS);

    /* Polling Socket, Queue and Timer for event */
    /* If retransmission Timer expires */
    u4OsixRecv = OsixReceiveEvent
        (IKE_TIMER0_EVENT | IKE_INPUT_Q_EVENT,
         OSIX_WAIT | OSIX_EV_ANY, IKE_ZERO, &u4Event);
    while (u4OsixRecv == OSIX_SUCCESS)
    {
        /* Timer Event */
        u4EventTimer = (u4Event & IKE_TIMER0_EVENT);
        if (u4EventTimer != IKE_ZERO)
        {
            IkeProcessTimer ();
        }

        /* If any data in the ipsec ike interface queue */
        u4EventQ = (u4Event & IKE_INPUT_Q_EVENT);
        if (u4EventQ != IKE_ZERO)
        {
            IkeProcessQMsg ();
        }

        u4OsixRecv = OsixReceiveEvent (IKE_TIMER0_EVENT |
                                       IKE_INPUT_Q_EVENT, OSIX_WAIT |
                                       OSIX_EV_ANY, IKE_ZERO, &u4Event);
    }
    return OSIX_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessPkt    
 *  Description   : Called from ike main to process data from socket 
 *                  layer for key negotiation.
 *  Input(s)      : pIkePktBuffer - Pointer to the packet buffer 
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */
VOID
IkeProcessPkt (tIkePktBuffer * pIkePktBuffer)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIsakmpHdr         *pIkeIsakmpHdr = NULL;

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkePktBuffer->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessPkt: Unable to " "find Engine\n");
        return;
    }

    if (pIkePktBuffer->PeerAddr.u4AddrType == IPV6ADDR)
    {
        IncIkeInPkts (&pIkeEngine->Ip6TunnelTermAddr);
    }
    else
    {
        IncIkeInPkts (&pIkeEngine->Ip4TunnelTermAddr);
    }
    pIkeIsakmpHdr = (tIsakmpHdr *) (void *) pIkePktBuffer->pu1PktBuffer;
    if (pIkeIsakmpHdr->u1Version == IKE_ISAKMP_VERSION)
    {
        IkeProcessIkePkt (pIkeEngine, pIkePktBuffer->pu1PktBuffer,
                          (UINT2) pIkePktBuffer->u4PktLen,
                          &pIkePktBuffer->PeerAddr,
                          pIkePktBuffer->u2RemotePort,
                          pIkePktBuffer->u2LocalPort);
    }
    if (pIkeIsakmpHdr->u1Version == IKE2_MAJOR_VERSION)
    {
        Ike2MainProcessIkePkt (pIkeEngine, pIkePktBuffer->pu1PktBuffer,
                               (UINT2) pIkePktBuffer->u4PktLen,
                               &pIkePktBuffer->PeerAddr,
                               pIkePktBuffer->u2RemotePort,
                               pIkePktBuffer->u2LocalPort);
    }
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessConfigRequest    
 *  Description   : Function to Process Config request from SNMP/CLI      
 *  Input(s)      : pIkeConfigIfParam - Message structure which contains
 *                : a union structure for the various configuration params
 *  Output(s)     : None.
 *  Return Values : None                            
 * ---------------------------------------------------------------
 */

VOID
IkeProcessConfigRequest (tIkeConfigIfParam * pIkeConfigIfParam)
{

    switch (pIkeConfigIfParam->u4MsgType)
    {
        case IKE_CONFIG_P1_POLICY_CHG:
        case IKE_CONFIG_P1_DEL_POLICY:
        {
            tIkeEngine         *pIkeEngine = NULL;
            UINT4               u4IkeEngineIndex = IKE_ZERO;
            tIkePhase1ID       *pPeerId = NULL;

            u4IkeEngineIndex =
                pIkeConfigIfParam->ConfigReq.IkeConfPolicy.u4IkeEngineIndex;
            pPeerId = &(pIkeConfigIfParam->ConfigReq.IkeConfPolicy.PeerId);

            pIkeEngine = IkeGetIkeEngineFromIndex (u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            /* The Phase1 Policy has changed/deleted
               Delete all on-going Phase1 and Phase2 Negotiations
               and all existing SAs with the peer
             */
            if (IkeDelAllSessSAInEngineWithPeer (pIkeEngine, pPeerId)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete all "
                             "Sessions/SAs\n");
                return;
            }
            break;
        }

        case IKE_CONFIG_P1_ALL_POLICY_CHG:
        case IKE_CONFIG_P1_DEL_ALL_POLICY:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            /* The Phase1 Policy has changed/deleted
               delete all on-going Phase1 and Phase2 Negotiations
               and all existing SAs
             */
            IkeDelAllSessSAInEngine (pIkeEngine);
            break;
        }

        case IKE_CONFIG_CHG_TUNNEL_TERM_ADDR:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                                   IkeConfEngineTunnTermAddr.
                                                   u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            /* The Engine Tunnel TermAddr has changed has changed
               delete all on-going Phase1 and Phase2 Negotiations
               and all existing SAs
             */
            IkeDelAllSessSAInEngine (pIkeEngine);
            /* Since the Socket is bound to 0.0.0.0 IP address, no need to 
             * close and restart the socket */
            if (pIkeConfigIfParam->ConfigReq.IkeConfEngineTunnTermAddr.
                LocTunnTermAddr.u4AddrType == IPV6ADDR)
            {
                IKE_MEMCPY
                    (&pIkeEngine->Ip6TunnelTermAddr,
                     &pIkeConfigIfParam->ConfigReq.IkeConfEngineTunnTermAddr.
                     LocTunnTermAddr, sizeof (tIkeIpAddr));
            }
            else
            {
                IKE_MEMCPY
                    (&pIkeEngine->Ip4TunnelTermAddr,
                     &pIkeConfigIfParam->ConfigReq.IkeConfEngineTunnTermAddr.
                     LocTunnTermAddr, sizeof (tIkeIpAddr));
            }
            break;
        }

        case IKE_CONFIG_CHG_PRESHARED_KEY:
        case IKE_CONFIG_DEL_PRESHARED_KEY:
        {
            tIkeEngine         *pIkeEngine = NULL;
            tIkeIpAddr          PeerIpAddr;

            pIkeEngine = IkeGetIkeEngineFromIndex
                (pIkeConfigIfParam->ConfigReq.IkeConfPreSharedkey.
                 u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            /* This is assuming that the KeyID is always IP Address.
               XXX When we support Non-IP KeyID, we will have to change 
               the processing to be based on the RemoteId instead of 
               the remote address
             */
            PeerIpAddr.u4AddrType = (UINT4) pIkeConfigIfParam->ConfigReq.
                IkeConfPreSharedkey.KeyID.i4IdType;
            if ((PeerIpAddr.u4AddrType != IPV4ADDR) &&
                (PeerIpAddr.u4AddrType != IPV6ADDR))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Only V4 or V6 "
                             "supported in Key for now\n");
                return;
            }

            IKE_MEMCPY (&PeerIpAddr.uIpAddr,
                        &pIkeConfigIfParam->ConfigReq.IkeConfPreSharedkey.
                        KeyID.uID,
                        ((PeerIpAddr.u4AddrType == IPV4ADDR) ?
                         sizeof (tIp4Addr) : sizeof (tIp6Addr)));

            if (IkeDelSessSAForPeer (pIkeEngine, &PeerIpAddr) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete "
                             "Sessions/SAs for peer\n");
                return;
            }
            break;
        }

        case IKE_CONFIG_CHG_CRYPTO_MAP:
        case IKE_CONFIG_DEL_CRYPTO_MAP:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeGetIkeEngineFromIndex
                (pIkeConfigIfParam->ConfigReq.IkeConfCryptoMap.
                 u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            if (IkeDelPhase2SessSAForPeer (pIkeEngine,
                                           &pIkeConfigIfParam->ConfigReq.
                                           IkeConfCryptoMap.PeerIpAddr) ==
                IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete "
                             "Phase2 Sessions/SAs for peer\n");
                return;
            }
	    /* Delete the local tunnel termination address of the engine */ 
	    MEMSET (&((pIkeEngine->Ip4TunnelTermAddr).uIpAddr.Ip4Addr), IKE_ZERO,
			sizeof((pIkeEngine->Ip4TunnelTermAddr).uIpAddr.Ip4Addr));
            break;
        }

        case IKE_CONFIG_CHG_TRANSFORM_SET:
        case IKE_CONFIG_DEL_TRANSFORM_SET:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeGetIkeEngineFromIndex
                (pIkeConfigIfParam->ConfigReq.IkeConfCryptoMap.
                 u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            if (IkeDelAllPhase2SessSA (pIkeEngine) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete "
                             "Phase2 Sessions/SAs\n");
                return;
            }
            break;
        }

        case IKE_CONFIG_DEL_TRANSFORM_SET_SPECIFIC:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeGetIkeEngineFromIndex
                (pIkeConfigIfParam->ConfigReq.IkeConfCryptoMap.
                 u4IkeEngineIndex);

            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }
            if (IkeDelPhase2SessSAForPeer (pIkeEngine,
                                           &pIkeConfigIfParam->ConfigReq.
                                           IkeConfCryptoMap.PeerIpAddr) ==
                IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete "
                             "Phase2 Sessions/SAs for peer\n");
                return;
            }
            break;
        }

        case IKE_CONFIG_NEW_ENGINE:
        {
            tIkeEngine         *pIkeEngine = NULL;

            if (MemAllocateMemBlock
                (IKE_ENGINE_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeEngine) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: unable to allocate "
                             "buffer\n");
                return;
            }

            if (pIkeEngine == NULL)
            {
                return;
            }
            IKE_BZERO (pIkeEngine, sizeof (tIkeEngine));
            IKE_STRCPY (pIkeEngine->au1EngineName,
                        pIkeConfigIfParam->ConfigReq.IkeConfCreateEngine.
                        au1EngineName);
            pIkeEngine->u1Status = IKE_NOTREADY;
            IkeInitEngine (pIkeEngine);
            SLL_ADD (&gIkeEngineList, (t_SLL_NODE *) pIkeEngine);
            break;
        }

        case IKE_CONFIG_START_ENGINE_LISTENER:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            /* Read certificates if any */
            IkeReadCertConfig (pIkeEngine->au1EngineName);

            /* Start the listener */
            IkeStartListener (pIkeEngine);
            break;
        }

        case IKE_CONFIG_DEL_ENGINE:
        {
            tIkeEngine         *pIkeEngine = NULL;

            SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
            {
                if ((pIkeEngine->u1Status == IKE_DESTROY) &&
                    (pIkeEngine->u4IkeIndex ==
                     pIkeConfigIfParam->ConfigReq.u4IkeEngineIndex))
                {
                    break;
                }
            }

            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find Engine\n");
                return;
            }

            IkeDelAllSessSAInEngine (pIkeEngine);

            SLL_DELETE (&gIkeEngineList, (t_SLL_NODE *) pIkeEngine);
            IkeDeleteEngine ((t_SLL_NODE *) pIkeEngine);
            break;
        }

        case IKE_CONFIG_NEW_VPN_POLICY:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

            if (MemAllocateMemBlock
                (IKE_VPN_POLICY_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeVpnPolicy) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: unable to allocate "
                             "buffer\n");
                return;
            }
            if (pIkeVpnPolicy == NULL)
            {
                return;
            }
            IKE_BZERO (pIkeVpnPolicy, sizeof (tIkeVpnPolicy));
            IKE_STRCPY (pIkeVpnPolicy->au1VpnPolicyName,
                        pIkeConfigIfParam->ConfigReq.IkeConfCreateVpnPolicy.
                        au1VpnPolicyName);
            pIkeVpnPolicy->u1Status = IKE_NOTREADY;

            /* Initialize vpn policy node with default attributes */
            IkeInitDefaultVpnPolicy (pIkeVpnPolicy);

            /* Add the new vpn policy node to the vpn policy list */
            SLL_ADD (&gIkeVpnPolicyList, (t_SLL_NODE *) pIkeVpnPolicy);

            break;
        }                        /* case IKE_CONFIG_NEW_VPN_POLICY */

        case IKE_CONFIG_DEL_VPN_POLICY:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

            SLL_SCAN (&gIkeVpnPolicyList, pIkeVpnPolicy, tIkeVpnPolicy *)
            {
                if ((pIkeVpnPolicy->u1Status == IKE_DESTROY) &&
                    (pIkeVpnPolicy->u4VpnPolicyIndex ==
                     pIkeConfigIfParam->ConfigReq.u4VpnPolicyIndex))
                {
                    break;
                }
            }

            if (pIkeVpnPolicy == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to "
                             "find VPN Policy\n");
                return;
            }

            SLL_DELETE (&gIkeVpnPolicyList, (t_SLL_NODE *) pIkeVpnPolicy);
            IkeDeleteVpnPolicy ((t_SLL_NODE *) pIkeVpnPolicy);
            break;
        }                        /* case IKE_CONFIG_DEL_VPN_POLICY */

        case IKE_CONFIG_DELETE_MYCERT:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          IkeDelCertInfo.u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            /* Check the certificate deleted is the default certificate,
             * if so delete all the ikesa's whose auth method is rsasig */

            if (pIkeConfigIfParam->ConfigReq.IkeDelCertInfo.PeerIpAddr.
                u4AddrType == IKE_NONE)
            {
                /* Delete all the IKE SAs whose auth method is rsasig */
                /* Currently all the SA's associated with the peer gets
                 * deleted */
                IkeDelAllSessSAInEngine (pIkeEngine);
            }
            else
            {
                if (IkeDelSessSAForPeer
                    (pIkeEngine,
                     &pIkeConfigIfParam->ConfigReq.IkeDelCertInfo.PeerIpAddr) ==
                    IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessConfigRequest: Unable to delete "
                                 "all Sessions/SAs\n");
                    return;
                }
            }
            break;
        }

        case IKE_CONFIG_DEL_PEER_CERTIFICATE:
        {
            tIkeEngine         *pIkeEngine = NULL;
            tIkeIpAddr          PeerIpAddr;
            tIkePhase1ID       *pPhase1ID = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          IkeDelPeerCertInfo.u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            pPhase1ID =
                &pIkeConfigIfParam->ConfigReq.IkeDelPeerCertInfo.Phase1ID;

            if (pPhase1ID->i4IdType != IKE_NONE)
            {
                if ((pPhase1ID->i4IdType == IPV4ADDR) ||
                    (pPhase1ID->i4IdType == IPV6ADDR))
                {
                    IKE_BZERO (&PeerIpAddr, sizeof (tIkeIpAddr));

                    PeerIpAddr.u4AddrType = (UINT4) pPhase1ID->i4IdType;

                    if (PeerIpAddr.u4AddrType == IPV4ADDR)
                    {
                        PeerIpAddr.Ipv4Addr = pPhase1ID->uID.Ip4Addr;
                    }
                    else
                    {
                        IKE_MEMCPY (&PeerIpAddr.Ipv6Addr,
                                    &pPhase1ID->uID.Ip6Addr, sizeof (tIp6Addr));
                    }

                    if (IkeDelSessSAForPeer (pIkeEngine, &PeerIpAddr)
                        == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessConfigRequest:\
                                     Unable to delete " "all Sessions/SAs\n");
                        return;
                    }
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessConfigRequest: Currently non IP"
                                 "Identifiers are not supported\n");
                    return;
                }
            }
            else
            {
                /* Delete All ike SAs in the engine whose auth method is
                 * rsasig */
                /* Currently all the SA's associated with the peer gets
                 * deleted */
                IkeDelAllSessSAInEngine (pIkeEngine);
            }

            break;
        }

        case IKE_CONFIG_DEL_CACERT:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          IkeDelCaCertInfo.u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            /* Delete All ike SAs in the engine whose auth method is rsasig */
            /* Currently all the SA's associated with the peer gets
             * deleted */
            IkeDelAllSessSAInEngine (pIkeEngine);
            break;
        }

        case IKE_CONFIG_DEL_CERT_MAP:
        {
            tIkeEngine         *pIkeEngine = NULL;
            tIkeIpAddr          PeerIpAddr;
            tIkePhase1ID       *pPeerID = NULL;

            pIkeEngine =
                IkeGetIkeEngineFromIndex (pIkeConfigIfParam->ConfigReq.
                                          IkeMapCertInfo.u4IkeEngineIndex);
            if (pIkeEngine == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to find "
                             "Engine\n");
                return;
            }

            pPeerID = &pIkeConfigIfParam->ConfigReq.IkeMapCertInfo.Phase1ID;

            if (pPeerID->i4IdType != IKE_MAP_DEFAULT)
            {
                if ((pPeerID->i4IdType == IPV4ADDR)
                    || (pPeerID->i4IdType == IPV6ADDR))
                {
                    IKE_BZERO (&PeerIpAddr, sizeof (tIkeIpAddr));

                    PeerIpAddr.u4AddrType = (UINT4) pPeerID->i4IdType;

                    if (PeerIpAddr.u4AddrType == IPV4ADDR)
                    {
                        IKE_MEMCPY (&PeerIpAddr.Ipv4Addr, &pPeerID->uID.Ip4Addr,
                                    IKE_IPV4_LENGTH);
                    }
                    else
                    {
                        IKE_MEMCPY (&PeerIpAddr.Ipv6Addr, &pPeerID->uID.Ip6Addr,
                                    IKE_IPV6_LENGTH);
                    }

                    if (IkeDelSessSAForPeer (pIkeEngine, &PeerIpAddr)
                        == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessConfigRequest:\
                                     Unable to delete " "all Sessions/SAs\n");
                        return;
                    }
                }
                else
                {

                    if (IkeDelAllSessSAInEngineWithPeer (pIkeEngine, pPeerID)
                        == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessConfigRequest: Unable to delete all "
                                     "Sessions/SAs\n");
                        return;
                    }

                }
            }
            else
            {
                /* Delete All ike SAs in the engine whose auth
                 * method is rsasig */
                /* Currently all the SA's associated with the peer gets
                 * deleted */
                IkeDelAllSessSAInEngine (pIkeEngine);
            }
            break;
        }

        case IKE_CONFIG_DELETE_SA:
        {
            tIkeIPSecIfParam   *pIkeIPSecIfParam;
            if (MemAllocateMemBlock
                (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeIPSecIfParam) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: unable to allocate "
                             "buffer\n");
                return;
            }

            IKE_BZERO (pIkeIPSecIfParam, sizeof (tIkeIPSecIfParam));

            pIkeIPSecIfParam->u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
            IKE_MEMCPY (&pIkeIPSecIfParam->IPSecReq.DelSAInfo,
                        &pIkeConfigIfParam->ConfigReq.DelSAByAddr,
                        sizeof (tIkeDelSAByAddr));

            if (IkeSendIPSecIfParam (pIkeIPSecIfParam) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessConfigRequest: Unable to delete "
                             " IPSec SAs\n");
                MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                    (UINT1 *) pIkeIPSecIfParam);
                return;
            }
            MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                (UINT1 *) pIkeIPSecIfParam);
            break;
        }
        default:
            break;

    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessQMsg             
 *  Description   : Function to Process the IKE Input Queue               
 *  Input(s)      : None                                                  
 *  Output(s)     : None.
 *  Return Values : None                            
 * ---------------------------------------------------------------
 */

VOID
IkeProcessQMsg (VOID)
{

    tIkeQMsg           *pIkeQMsg = NULL;
    tBufChainHeader    *pInBufChain = NULL;

    while (OsixReceiveFromQ ((UINT4) ZERO, IKE_QUEUE_NAME,
                             OSIX_NO_WAIT, IKE_ZERO, &pInBufChain) ==
           OSIX_SUCCESS)
    {
        if (pInBufChain == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "Ike",
                         "IkeProcessQMsg:Rcv returns null pointer\n");
            return;
        }
        pIkeQMsg = (tIkeQMsg *) pInBufChain;

        switch (pIkeQMsg->u4MsgType)
        {
            case IPSEC_IF_MSG:
                IkeProcessIpsecRequest (&(pIkeQMsg->IkeIfParam.IkeIPSecParam));
                break;

            case CONFIG_IF_MSG:
                IkeProcessConfigRequest (&
                                         (pIkeQMsg->IkeIfParam.IkeConfigParam));
                if (OsixSendEvent (SELF, OsixExGetTaskName (pIkeQMsg->u4TaskId),
                                   IKE_CFG_WAIT_EVENT) != OSIX_SUCCESS)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "Ike",
                                 "IkeProcessQMsg:OsixSendEvent fails\n");
                    return;
                }
                break;

            case L2TP_IF_MSG:
                IkeProcessL2tpRequest (&(pIkeQMsg->IkeIfParam.IkeL2TPIfParam));
                break;

            case IKE_PKT_ARRIVAL:
                IkeProcessPkt (&(pIkeQMsg->IkeIfParam.IkePktBuffer));
                break;
            default:
                break;
        }
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQMsg);
        pIkeQMsg = NULL;
        pInBufChain = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IkeLock                                              */
/*                                                                           */
/* Description        : This function is used to take the IKE  Semaphore     */
/*                      to avoid simultaneous access to  IKE database        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
IkeLock (VOID)
{
    OsixSemTake (gIkeSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IkeUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the IKE Semaphore      */
/*                      to avoid simultaneous access to IKE database         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IkeUnLock (VOID)
{
    OsixSemGive (gIkeSemId);
    return SNMP_SUCCESS;
}
#endif
