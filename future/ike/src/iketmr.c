/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iketmr.c,v 1.8 2013/12/18 12:50:09 siva Exp $
 *
 * Description: This file contains the IKE Timer processing routines     
 *
 ***********************************************************************/

#include "ikegen.h"

tTimerListId        gu4TimerId;
static VOID         (*IkeTimerRoutines[]) (VOID *) =
{
NULL,
        IkeProcessRetransmissionTimer,
        IkeProcessReKeyTimer,
        IkeProcessDeleteSATimer,
        IkeProcessDeleteSessionTimer, IkeProcessNatKeepAliveTimer};

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessTimer 
 *  Description   : Ike Timer Processing routine
 *  Input(s)      : None
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessTimer (VOID)
{
    tIkeTimer          *pIkeTimer = NULL;
    tTmrAppTimer       *pExpTimer = NULL;

    pExpTimer = TmrGetNextExpiredTimer ((tTimerListId) gu4TimerId);
    while (pExpTimer != NULL)
    {
        pIkeTimer = (tIkeTimer *) pExpTimer->u4Data;

        IKE_INIT_TIMER (pIkeTimer);
        if ((pIkeTimer->u1TimerId > IKE_MIN_TIMER_ID) &&
            (pIkeTimer->u1TimerId < IKE_MAX_TIMER_ID))
        {
            (*IkeTimerRoutines[pIkeTimer->u1TimerId])
                ((VOID *) (pIkeTimer->u4Context));
        }
        pExpTimer = TmrGetNextExpiredTimer ((tTimerListId) gu4TimerId);
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeStartTimer 
 *  Description   : start specified ike timer
 *  Input(s)      : pIkeTimer - Ike timer structure
 *                  TimerId - Type of timer
 *                  u4TimeOut - timeout in secs
 *  Output(s)     : None
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT4
IkeStartTimer (tIkeTimer * pIkeTimer, tIkeTimerId TimerId,
               UINT4 u4TimeOut, VOID *pTmrContext)
{
    if (IKE_IS_TIMER_ACTIVE (pIkeTimer) == TRUE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStartTimer:Attempt to start already running timer\n");
        return IKE_FAILURE;
    }

    IKE_ACTIVATE_TIMER (pIkeTimer);
    switch (TimerId)
    {
        case IKE_RETRANS_TIMER_ID:
        case IKE_DELETE_SESSION_TIMER_ID:
        {
            tIkeSessionInfo    *pSessionInfo = (tIkeSessionInfo *) pTmrContext;
            pIkeTimer->u1IkeVer = pSessionInfo->u1IkeVer;
            pSessionInfo->u4RefCount++;
            break;
        }

        case IKE_REKEY_TIMER_ID:
        case IKE_DELETESA_TIMER_ID:
        {
            tIkeSA             *pIkeSA = (tIkeSA *) pTmrContext;
            pIkeTimer->u1IkeVer = pIkeSA->u1IkeVer;
            pIkeSA->u4RefCount++;
            break;
        }

        case IKE_MIN_TIMER_ID:
        case IKE_NAT_KEEP_ALIVE_TIMER_ID:
        case IKE_MAX_TIMER_ID:
            break;

        default:
            break;
    }

    pIkeTimer->u1TimerId = (UINT1) TimerId;
    pIkeTimer->u4Context = (FS_ULONG) pTmrContext;
    pIkeTimer->Node.u4Data = (FS_ULONG) pIkeTimer;
    if (TmrStartTimer ((tTimerListId) gu4TimerId, &pIkeTimer->Node,
                       (UINT4) (u4TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
        TMR_SUCCESS)
    {
        return (IKE_SUCCESS);
    }
    else
    {
        return (IKE_FAILURE);
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeStopTimer 
 *  Description   : stop specified ike timer
 *  Input(s)      : pIkeTimer - Ike timer structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

INT4
IkeStopTimer (tIkeTimer * pIkeTimer)
{
    if (IKE_IS_TIMER_ACTIVE (pIkeTimer) == TRUE)
    {
        switch (pIkeTimer->u1TimerId)
        {
            case IKE_RETRANS_TIMER_ID:
            case IKE_DELETE_SESSION_TIMER_ID:
            {
                tIkeSessionInfo    *pSessionInfo =
                    (tIkeSessionInfo *) pIkeTimer->u4Context;

                pSessionInfo->u4RefCount--;
                break;
            }

            case IKE_REKEY_TIMER_ID:
            case IKE_DELETESA_TIMER_ID:
            {
                tIkeSA             *pIkeSA = (tIkeSA *) pIkeTimer->u4Context;

                pIkeSA->u4RefCount--;
            }
                break;

            default:
                break;
        }

        if (TmrStopTimer ((tTimerListId) gu4TimerId, &pIkeTimer->Node) ==
            TMR_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeStopTimer:TmrStopTimer failed!\n");
            return IKE_FAILURE;
        }
        IKE_INIT_TIMER (pIkeTimer);
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessRetransmissionTimer 
 *  Description   : Process the retransmission timer
 *  Input(s)      : pTmrContext - This is the Ike Session structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessRetransmissionTimer (VOID *pTmrContext)
{
    tIkeSessionInfo    *pSessionInfo = (tIkeSessionInfo *) pTmrContext;
    BOOLEAN             bReTransmit = IKE_TRUE;
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessRetransmissionTimer:Tmr Context (Session) "
                     "is NULL!\n");
        return;
    }

    if (pSessionInfo->u1SessionStatus != IKE_ACTIVE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessRetransmissionTimer:Session structure is "
                     "not active!!\n");
        return;
    }

    pSessionInfo->u4RefCount--;

    if ((pSessionInfo->u1Role == IKE_RESPONDER) &&
        (pSessionInfo->u1IkeVer == IKE2_MAJOR_VERSION))
    {
        /* In Ikev2- Responder should not re-transmit the responses */
        bReTransmit = IKE_FALSE;
    }

    if (bReTransmit == IKE_TRUE)
    {
        IkeSendToSocketLayer (pSessionInfo);
    }

    pSessionInfo->u1ReTransCount--;

    if (pSessionInfo->u1ReTransCount == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessRetransmissionTimer:Max Retransmissions "
                     "occurred\n");
        /* Start timer to delete session */
        if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                           IKE_DELETE_SESSION_TIMER_ID,
                           IKE_DELAYED_SESSION_DELETE_INTERVAL, pSessionInfo) !=
            IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessRetransmissionTimer: IkeStartTimer failed "
                         "for Delete Session!\n");
        }
        IncIkeNoOfPhase1SessionsFailed ();
        return;
    }

    /* Start Retransmission timer for packet retransmision */
    if (IkeStartTimer (&pSessionInfo->IkeRetransTimer, IKE_RETRANS_TIMER_ID,
                       IKE_RETRANSMIT_INTERVAL, pSessionInfo) != IKE_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessRetransmissionTimer: IkeStartTimer failed "
                     "for retransmission!\n");
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessDeleteSessionTimer 
 *  Description   : This function is called to delete the session
 *                  after Phase1 is done
 *  Input(s)      : pTmrContext - This is the Ike Session structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessDeleteSessionTimer (VOID *pTmrContext)
{
    tVpnIpAddr          VpnIpAddr;
    tIkeSessionInfo    *pSessionInfo = (tIkeSessionInfo *) pTmrContext;
    UINT4               u4Index = IKE_ZERO;

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDeleteSessionTimer:Tmr Context (Session) "
                     "is NULL!\n");
        return;
    }

    if (pSessionInfo->u1SessionStatus != IKE_ACTIVE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDeleteSessionTimer:Session structure is "
                     "not active!!\n");
        return;
    }
    pSessionInfo->u1SessionStatus = IKE_NOTINSERVICE;
    pSessionInfo->u4RefCount--;
    /* In case of RAVPN if IP address is assigned to the client
     * then release the ip address to the free pool */

    while ((u4Index < pSessionInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
            u1ConfiAttrIndex) && (pSessionInfo->IkeTransInfo.IkeRAInfo.
                                  CMServerInfo.au2ConfiguredAttr[u4Index] !=
                                  IKE_ZERO))
    {

        if ((pSessionInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
             au2ConfiguredAttr[u4Index] == IKE_INTERNAL_IP4_ADDRESS) ||
            (pSessionInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
             au2ConfiguredAttr[u4Index] == IKE_INTERNAL_IP6_ADDRESS))
        {

            if (pSessionInfo->IkeTransInfo.IkeRAInfo.CMServerInfo.
                u1AddrType == IPV4ADDR)
            {
                VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
                IKE_MEMCPY (&(VpnIpAddr.uIpAddr.Ip4Addr),
                            &pSessionInfo->IkeTransInfo.IkeRAInfo.
                            CMServerInfo.InternalIpAddr.Ipv4Addr,
                            sizeof (UINT4));
            }
            else
            {
                VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
                IKE_MEMCPY (&(VpnIpAddr.uIpAddr.Ip6Addr),
                            &pSessionInfo->IkeTransInfo.IkeRAInfo.
                            CMServerInfo.InternalIpAddr.Ipv6Addr,
                            sizeof (tIp6Addr));
            }

            VpnUtilDelAddrFromAllocList (&(VpnIpAddr));
        }
        u4Index++;

        if (u4Index >= IKE_MAX_SUPPORTED_ATTR)
        {
            break;
        }
    }

    IkeDeleteSession (pSessionInfo);
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessReKeyTimer 
 *  Description   : This function is called for Re-keying of IKE SA
 *                  when soft lifetime expires
 *  Input(s)      : pTmrContext - This is the Ike SA structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessReKeyTimer (VOID *pTmrContext)
{
    tIkeSA             *pIkeSA = (tIkeSA *) pTmrContext;
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4CurrTime = IKE_ZERO;
    INT1                i1RetVal = IKE_ZERO;

    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessReKeyTimer: Tmr Context (pIkeSA) NULL\n");
        return;
    }

    if (pIkeSA->u1SAStatus != IKE_ACTIVE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessReKeyTimer: pIkeSA not ACTIVE!\n");
        return;
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessReKeyTimer: Unable to get Engine\n");
        return;
    }

    pIkeSA->u4RefCount--;
    if (pIkeSA->u4LifeTime != IKE_ZERO)
    {
        /* Get Current Sys time */
        OsixGetSysTime (&u4CurrTime);
        /* Convert into seconds */
        u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

        if (pIkeSA->u4AbsHardLifeTime >= u4CurrTime)
        {
            /* Start the hard lifetime timer */
            if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID,
                               (pIkeSA->u4AbsHardLifeTime - u4CurrTime),
                               pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessReKeyTimer: IkeStartTimer failed "
                             "for HardLifetime!\n");
                pIkeSA->u1SAStatus = IKE_NOTINSERVICE;
            }
        }
        else
        {
            /* The Hard lifetime has also expired, delete the SA */
            if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID,
                               IKE_MIN_TIMER_INTERVAL, pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessReKeyTimer: IkeStartTimer failed "
                             "for Delete SA!\n");
                pIkeSA->u1SAStatus = IKE_NOTINSERVICE;
            }
        }
    }

    /* Block phase1 Rekey from the RAVPN Server */
    if ((pIkeSA->u1IkeVer == IKE_ISAKMP_VERSION) &&
        (gbCMServer == TRUE) && (pIkeSA->bCMEnabled == IKE_TRUE))
    {
        return;
    }
    /* If an ongoing phase1 negotiation exists with the peer, then don't
     * start one more rekey */

    SLL_SCAN (&pIkeEngine->IkeSessionList, pSessionInfo, tIkeSessionInfo *)
    {
        if (IKE_MEMCMP (&pSessionInfo->RemoteTunnelTermAddr,
                        &pIkeSA->IpPeerAddr, sizeof (tIkeIpAddr)) == IKE_ZERO)
        {
            return;
        }
    }

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeProcessReKeyTimer: Rekeying ...\n");

    if (pIkeSA->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        pSessionInfo = IkePhase1SessionInit (IKE_INITIATOR,
                                             pIkeEngine, IKE_ZERO,
                                             &pIkeSA->IpPeerAddr);
        if (pSessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessReKeyTimer: Unable to create Phase1/Init "
                         "Session\n");
            return;
        }
        /* Indicates RAVPN client phase1 Rekey */
        if ((gbCMServer == FALSE) && (pIkeSA->bCMEnabled == TRUE))
        {
            pSessionInfo->bRekey = IKE_POST_RA_REKEY;
        }
    }
    else
    {
        pSessionInfo = Ike2SessChildExchSessInit (pIkeSA, NULL,
                                                  IKE_ZERO, IKE_INITIATOR,
                                                  IKE2_IPSEC_REKEY_IKESA);
    }
    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessReKeyTimer: Unable to create Phase1/Init "
                     "Session\n");
        return;
    }

    /*Increment the phase1 rekey statistics */
    IncIkePhase1ReKey ();
    if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        i1RetVal = IkeCoreProcess (pSessionInfo);
    }
    else
    {
        i1RetVal = Ike2CoreProcess (pSessionInfo);
    }
    if (i1RetVal == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessReKeyTimer: Core Process failed for "
                     "Phase1/Init Session \n");
        IkeHandleCoreFailure (pSessionInfo);
        return;
    }
    else
    {
        /* Core Process is Success, Send the Packet Out on the 
         * wire */
        IkeHandleCoreSuccess (pSessionInfo);
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessNatKeepAliveTimer
 *  Description   : This function is called for Sending NAT
 *                  Keep-Alive Message.
 *  Input(s)      : pTmrContext - This is the Ike SA structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessNatKeepAliveTimer (VOID *pTmrContext)
{
    tIkeSA             *pIkeSA = (tIkeSA *) pTmrContext;
    UINT2               u2Len = sizeof (struct sockaddr_in);
    struct sockaddr_in  SocketAddr;
    UINT1               u1NatKeepAliveMsg = IKE_NAT_KEEP_ALIVE_MSG;
    struct sockaddr_in6 SocketAddr6;

    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessNatKeepAliveTimer:\
                     Tmr Context (pIkeSA) NULL\n");
        return;
    }

    if (pIkeSA->IpPeerAddr.u4AddrType == IPV6ADDR)
    {
        SocketAddr6.sin6_family = AF_INET6;
        IKE_BZERO (SocketAddr6.sin6_addr.s6_addr, IKE_IP6_ADDR_LEN);
        SocketAddr6.sin6_port = IKE_HTONS (pIkeSA->IkeNattInfo.u2RemotePort);
        IKE_MEMCPY ((UINT1 *) &SocketAddr6.sin6_addr.s6_addr,
                    &pIkeSA->IpPeerAddr.Ipv6Addr, IKE_IP6_ADDR_LEN);
        if (sendto (pIkeSA->IkeNattInfo.i2NattSocketFd, &u1NatKeepAliveMsg,
                    IKE_ONE, IKE_ZERO, (struct sockaddr *) &SocketAddr6,
                    u2Len) != (INT4) IKE_ONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessNatKeepAliveTimer: Sending Error\n");
            return;
        }
    }
    else
    {
        SocketAddr.sin_family = AF_INET;
        SocketAddr.sin_addr.s_addr = IKE_HTONL (pIkeSA->IpPeerAddr.Ipv4Addr);
        SocketAddr.sin_port = IKE_HTONS (pIkeSA->IkeNattInfo.u2RemotePort);
        if (sendto
            (pIkeSA->IkeNattInfo.i2NattSocketFd, &u1NatKeepAliveMsg, IKE_ONE,
             IKE_ZERO, (struct sockaddr *) &SocketAddr,
             u2Len) != (INT4) IKE_ONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessNatKeepAliveTimer: Sending Error\n");
            return;
        }
    }
    if (IkeStartTimer (&pIkeSA->IkeNatKeepAliveTimer,
                       IKE_NAT_KEEP_ALIVE_TIMER_ID,
                       IKE_NAT_KEEP_ALIVE_TIMER_INTERVAL,
                       pIkeSA) != IKE_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessNatKeepAliveTimer: IkeStartTimer failed!\n");
    }
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessDeleteSATimer 
 *  Description   : This function is called for Deleting of IKE SA
 *                  when hard lifetime expires
 *  Input(s)      : pTmrContext - This is the Ike SA structure
 *  Output(s)     : None
 *  Return Values : None
 * ---------------------------------------------------------------
 */

VOID
IkeProcessDeleteSATimer (VOID *pTmrContext)
{
    tIkeSA             *pIkeSA = (tIkeSA *) pTmrContext;

    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDeleteSATimer: Tmr Context (pIkeSA) NULL\n");
        return;
    }

    if ((pIkeSA->u1SAStatus != IKE_ACTIVE) &&
        (pIkeSA->u1SAStatus != IKE_DESTROY))

    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDeleteSATimer: pIkeSA not ACTIVE"
                     " Or Destroy!\n");
        return;
    }

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeProcessDeleteSATimer: Deleting SA\n");
    pIkeSA->u1SAStatus = IKE_NOTINSERVICE;
    pIkeSA->u4RefCount--;

    if (pIkeSA->u4RefCount > IKE_ZERO)
    {
        pIkeSA->u1SAStatus = IKE_DESTROY;
        /* SA in use. Retry again */
        if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer,
                           IKE_DELETESA_TIMER_ID,
                           IKE_DEFAULT_SA_LIFETIME, pIkeSA) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessReKeyTimer: IkeStartTimer failed "
                         "for HardLifetime!\n");
            pIkeSA->u1SAStatus = IKE_NOTINSERVICE;
        }
        return;
    }

    IkeDeleteIkeSA (pIkeSA);
    return;
}
