/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecli.c,v 1.15 2014/10/29 12:52:52 siva Exp $
 *
 * Description: This has functions for IKE CLI SubModule
 *
 ***********************************************************************/
#include "ikegen.h"
#include "fsike.h"
#include "cli.h"
#include "ikecli.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikex509.h"
#include "ikecertsave.h"
#include "ikememmac.h"

#ifdef IKE_WANTED

/**************************************************************************/
/*  Function Name   : IkeCliGenKeyPair                                    */
/*  Description     : This function generates rsa/dsa key pair            */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliGenKeyPair (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4ErrorCode = IKE_ZERO;
    INT4                i4RetVal = IKE_FAILURE;

    u4Count = SLL_COUNT (&(gIkeEngineList));

    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1KeyName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "Table has no entries : \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1KeyName);
        return;
    }

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeGenKey.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeGenKey.pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1KeyName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeGenKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1KeyName);
        return;
    }

    if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_RSA_SIGNATURE)
    {
        i4RetVal = IkeRsaGenKeyPair (pIkeCliConfigParams->
                                     CliIkeGenKey.pu1EngineName,
                                     pIkeCliConfigParams->
                                     CliIkeGenKey.pu1KeyName,
                                     (UINT2) pIkeCliConfigParams->CliIkeGenKey.
                                     i4Bits, &u4ErrorCode);
    }
    else if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_DSS_SIGNATURE)
    {
        i4RetVal = IkeDsaGenKeyPair (pIkeCliConfigParams->
                                     CliIkeGenKey.pu1EngineName,
                                     pIkeCliConfigParams->
                                     CliIkeGenKey.pu1KeyName,
                                     (UINT2) pIkeCliConfigParams->CliIkeGenKey.
                                     i4Bits, &u4ErrorCode);
    }
    if (i4RetVal == IKE_FAILURE)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenKeyPair: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1KeyName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_GEN_ERR:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Error while operating on files \r\n");
                break;
            }
            case IKE_ERR_DIRECTORY_CREATION_FAILED:
            {
                SPRINTF ((char *) pu1Temp, "%% Directory creation failed \r\n");
                break;
            }
            case IKE_ERR_KEY_ID_ALREADY_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% Duplicate keyid \r\n");
                break;
            }
            case IKE_ERR_KEY_GEN_FAILED:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Error while generating key \r\n");
                break;
            }
            case IKE_ERR_FILE_NAME_TOO_LONG:
            {
                SPRINTF ((char *) pu1Temp, "%% KeyId name too long \r\n");
                break;
            }
            default:
            {
                SPRINTF ((char *) pu1Temp, "%% Failed to generate key\r\n");
                break;
            }
        }
    }
    else
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.pu1KeyName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliDelKeyPair                                    */
/*  Description     : This function deletes the rsa/dsa key               */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliDelKeyPair (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4ErrorCode = IKE_ZERO;
    UINT4               u4Type = IKE_ZERO;
    tIkeASN1Integer    *pCASerialNum = NULL;
    tIkeX509Name       *pCAName = NULL;
    UINT1               u1RetVal = IKE_ZERO;
    tIkeIpAddr          PeerIpAddr;
    tIkeCertMapToPeer  *pCertMap = NULL, *pPrev = NULL;
    BOOLEAN             bErrValue = IKE_FALSE;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeGenKey.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeGenKey.pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelKeyPair: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelKeyPair: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                    pu1KeyName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeGenKey.pu1EngineName);
        if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1KeyName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }

    if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_RSA_SIGNATURE)
    {
        u4Count = SLL_COUNT (&(pIkeEngine->IkeRsaKeyList));
        u4Type = pIkeCliConfigParams->CliIkeGenKey.u4Type;
    }
    else if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_DSS_SIGNATURE)
    {
        u4Count = SLL_COUNT (&(pIkeEngine->IkeDsaKeyList));
        u4Type = pIkeCliConfigParams->CliIkeGenKey.u4Type;
    }
    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeVpnShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelKeyPair: Failed to allocate memory"
                         "using IKE_ALLOC_INTF_MESSAGE \r\n");
            if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                    pu1KeyName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries to delete\r\n");
        if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1KeyName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }
    else
    {
        if (IkeDeleteKeyPair
            (pIkeCliConfigParams->CliIkeGenKey.pu1EngineName,
             pIkeCliConfigParams->CliIkeGenKey.pu1KeyName, &pCAName,
             &pCASerialNum, &u1RetVal, u4Type, &u4ErrorCode) == IKE_SUCCESS)
        {
            if (u1RetVal == DEL_ALL_CERT_SA)
            {
                if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
                    (pIkeEngine->DefaultCert.pCASerialNum != NULL))
                {
                    TAKE_IKE_SEM ();
                    IkeX509NameFree (pIkeEngine->DefaultCert.pCAName);
                    IkeASN1IntegerFree (pIkeEngine->DefaultCert.pCASerialNum);

                    pIkeEngine->DefaultCert.pCAName = NULL;
                    pIkeEngine->DefaultCert.pCASerialNum = NULL;
                    GIVE_IKE_SEM ();
                }

                TAKE_IKE_SEM ();
                SLL_DELETE_LIST (&(pIkeEngine->IkeCertMapList),
                                 IkeFreeCertMapInfo);
                GIVE_IKE_SEM ();

                /* Send event to IKE task for deletion of IkeSAs */

                if (IkeNmSendCertDeleteReqToIke
                    (pIkeEngine->u4IkeIndex, NULL,
                     IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                {
                    IKE_FORMAT_ERROR_MESSAGE
                        ("Event posting to Ike for deletion failed\r\n",
                         ppu1Output);
                    if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeGenKey.pu1KeyName);
                    }
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeGenKey.pu1EngineName);
                    return;
                }
            }
            else if (u1RetVal == CERT_EXIST)
            {
                /* For each node in the certificate map list, send event to
                 * ike for deleting the node */
                if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
                    (pIkeEngine->DefaultCert.pCASerialNum != NULL))
                {
                    if ((IkeX509NameCmp
                         (pIkeEngine->DefaultCert.pCAName,
                          pCAName) == IKE_ZERO)
                        &&
                        (IkeASN1IntegerCmp
                         (pIkeEngine->DefaultCert.pCASerialNum,
                          pCASerialNum) == IKE_ZERO))
                    {
                        TAKE_IKE_SEM ();
                        IkeX509NameFree (pIkeEngine->DefaultCert.pCAName);
                        IkeASN1IntegerFree (pIkeEngine->DefaultCert.
                                            pCASerialNum);

                        pIkeEngine->DefaultCert.pCAName = NULL;
                        pIkeEngine->DefaultCert.pCASerialNum = NULL;

                        GIVE_IKE_SEM ();
                        /* The certificate deleted is a default cert
                         * hence delete all the ikeSA's in ike */
                        if (IkeNmSendCertDeleteReqToIke
                            (pIkeEngine->u4IkeIndex, NULL,
                             IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                        {
                            bErrValue = IKE_TRUE;
                            IKE_FORMAT_ERROR_MESSAGE
                                ("Event posting to Ike for deletion failed\r\n",
                                 ppu1Output);
                        }
                    }
                }

                /* The certificate deleted is not a default certificate,
                 * hence search for peer ID using this certificate and
                 * delete the IKE SAs associated with that peer */
                SLL_SCAN (&(pIkeEngine->IkeCertMapList), pCertMap,
                          tIkeCertMapToPeer *)
                {
                    if ((IkeX509NameCmp
                         (pCAName, pCertMap->MyCert.pCAName) == IKE_ZERO)
                        &&
                        (IkeASN1IntegerCmp
                         (pCASerialNum, pCertMap->MyCert.pCASerialNum) ==
                         IKE_ZERO))
                    {
                        if ((pCertMap->PeerID.i4IdType == IPV4ADDR) ||
                            (pCertMap->PeerID.i4IdType == IPV6ADDR))
                        {
                            IKE_BZERO (&PeerIpAddr, sizeof (tIkeIpAddr));

                            PeerIpAddr.u4AddrType =
                                (UINT4) pCertMap->PeerID.i4IdType;

                            if (PeerIpAddr.u4AddrType == IPV4ADDR)
                            {
                                PeerIpAddr.Ipv4Addr =
                                    pCertMap->PeerID.uID.Ip4Addr;
                            }
                            else
                            {
                                IKE_MEMCPY (&PeerIpAddr.Ipv6Addr,
                                            &pCertMap->PeerID.uID.Ip6Addr,
                                            sizeof (tIp6Addr));
                            }

                            if (IkeNmSendCertDeleteReqToIke
                                (pIkeEngine->u4IkeIndex, &PeerIpAddr,
                                 IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                            {
                                bErrValue = IKE_TRUE;
                                if (*ppu1Output != NULL)
                                {
                                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                        (UINT1 *) *ppu1Output);
                                }
                                IKE_FORMAT_ERROR_MESSAGE ("Event posting to\
                                     Ike for deletion failed\r\n", ppu1Output);

                                /* Added to address klocwork warning, which seemed logical */
                                break;
                            }
                        }
                        else
                        {
                            bErrValue = IKE_TRUE;
                            if (*ppu1Output != NULL)
                            {
                                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                    (UINT1 *) *ppu1Output);
                            }

                            IKE_FORMAT_ERROR_MESSAGE ("Currently no IP\
                                 identifiers are not supported\r\n", ppu1Output);

                            /* Added to address klocwork warning, which seemed logical */
                            break;
                        }

                        TAKE_IKE_SEM ();
                        SLL_DELETE (&(pIkeEngine->IkeCertMapList),
                                    (t_SLL_NODE *) pCertMap);
                        IkeFreeCertMapInfo ((t_SLL_NODE *) pCertMap);
                        GIVE_IKE_SEM ();
                        pCertMap = pPrev;
                    }
                    pPrev = pCertMap;
                }

                IkeX509NameFree (pCAName);
                pCAName = NULL;
                IkeASN1IntegerFree (pCASerialNum);
                pCASerialNum = NULL;

                if (bErrValue == IKE_FALSE)
                {
                    IKE_NO_RESPONSE_MESSAGE (ppu1Output);
                }
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                    pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                    pu1KeyName);
                return;
            }
            IKE_NO_RESPONSE_MESSAGE (ppu1Output);
        }
        /* Replaced 'if (u4ErrorCode != 0)' with else, 
         * Since u4ErrorCode is filled only in case of
         * IKE_FAILURE */
        else
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliDelKeyPair: unable to allocate "
                             "buffer\n");
                return;
            }

            if (*ppu1Output == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliDelKeyPair: Failed to allocate memory"
                             "using MemAllocateMemBlock \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                    pu1EngineName);
                if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeGenKey.pu1KeyName);
                }
                return;
            }

            CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
            pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

            switch (u4ErrorCode)
            {
                case IKE_ERR_CHANGE_DIR_FAILED:
                {
                    SPRINTF ((char *) pu1Temp,
                             "%% Failed to change directory  \r\n");
                    break;
                }

                case IKE_KEY_ID_DOES_NOT_EXIST:
                {
                    SPRINTF ((char *) pu1Temp,
                             "%% Specified key ID doesn't exist \r\n");
                    break;
                }

                case IKE_ERR_DEL_CERT_FAILED:
                {
                    SPRINTF ((char *) pu1Temp,
                             "%% Failed to delete certificate \r\n");
                    break;
                }

                case IKE_ERR_CERTDB_UNUPDATED:
                {
                    SPRINTF ((char *) pu1Temp,
                             "%% Unable to update certificate update \r\n");
                    break;
                }

                default:
                {
                    SPRINTF ((char *) pu1Temp, "%% Failed to delete key \r\n");
                    break;
                }
            }
        }
    }
    if (pIkeCliConfigParams->CliIkeGenKey.pu1KeyName != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1KeyName);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliGenCertReq                                    */
/*  Description     : This function generates certificate request         */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliGenCertReq (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1              *pu1CertReq = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeGenCertReq.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeGenCertReq.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenCertReq: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenCertReq: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                                pu1SubjectName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                                pu1SubjectAltName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeGenCertReq.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectAltName);
        return;
    }

    if (IkeGenerateCertificateRequest (pIkeCliConfigParams->CliIkeGenCertReq.
                                       pu1EngineName,
                                       pIkeCliConfigParams->CliIkeGenCertReq.
                                       pu1KeyName,
                                       pIkeCliConfigParams->CliIkeGenCertReq.
                                       pu1SubjectAltName,
                                       pIkeCliConfigParams->CliIkeGenCertReq.
                                       pu1SubjectName, &pu1CertReq,
                                       pIkeCliConfigParams->CliIkeGenCertReq.
                                       u4Type, &u4ErrorCode) == IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectAltName);

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenCertReq: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliGenCertReq: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_GEN_ERR:
            {
                SPRINTF ((char *) pu1Temp, "%% Failed in file operation \r\n");
                break;
            }

            case IKE_ERR_FILE_NAME_TOO_LONG:
            {
                SPRINTF ((char *) pu1Temp, "%% File name too long \r\n");
                break;
            }

            case IKE_ERR_KEY_ID_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% KeyID doesn't exist \r\n");
                break;
            }

            case IKE_ERR_MEM_ALLOC:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed while allocating memory \r\n");
                break;
            }

            case IKE_ERR_CONSTRUCT_PKCS10_CERT_REQ:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to construct certificate request \r\n");
                break;
            }

            case IKE_ERR_ADD_X509V3_EXT:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to add extensions to cert request \r\n");
                break;
            }

            case IKE_ERR_KEY_READ_FAILED:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to read key from file \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to generate cert request \r\n");
                break;
            }
        }
        return;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelCerts: unable to allocate " "buffer\n");
        return;
    }
    if (*ppu1Output == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelCerts: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1CertReq);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1SubjectAltName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                            pu1EngineName);
        return;
    }
    CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
    pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
    SPRINTF ((char *) pu1Temp, "%s ", pu1CertReq);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1CertReq);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                        pu1KeyName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                        pu1SubjectName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                        pu1SubjectAltName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenCertReq.
                        pu1EngineName);

    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliImportCert                                    */
/*  Description     : This function imports certificate                   */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliImportCert (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1FileName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1FileName);
        return;
    }

    if (IkeImportMyCertInCLI
        (pIkeCliConfigParams->CliIkeImportCert.pu1EngineName,
         pIkeCliConfigParams->CliIkeImportCert.pu1KeyName,
         pIkeCliConfigParams->CliIkeImportCert.pu1FileName,
         pIkeCliConfigParams->CliIkeImportCert.u1EncodeType,
         &u4ErrorCode) == IKE_SUCCESS)
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCert: unable to allocate " "buffer\n");
            return;
        }

        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1FileName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }

            case IKE_ERR_INVALID_FORMAT:
            {
                SPRINTF ((char *) pu1Temp, "%% Improper file format \r\n");
                break;
            }

            case IKE_ERR_CERT_KEY_MISMATCH:
            {
                SPRINTF ((char *) pu1Temp, "%% Certificate key mismatch \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to import certificate \r\n");
                break;
            }
        }

    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1KeyName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1FileName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliImportPeerCert                                */
/*  Description     : This function imports peer certificate              */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliImportPeerCert (tIkeCliConfigParams * pIkeCliConfigParams,
                      UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportPeerCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportPeerCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportPeerCert: unable to allocate "
                         "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1FileName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1CertName);
        return;
    }

    if (IkeImportPeerCertInCLI
        (pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName,
         pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName,
         pIkeCliConfigParams->CliIkeImportPeerCert.pu1FileName,
         pIkeCliConfigParams->CliIkeImportPeerCert.u1Flag,
         pIkeCliConfigParams->CliIkeImportPeerCert.u1EncodeType,
         &u4ErrorCode) == IKE_SUCCESS)
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportPeerCert: unable to allocate "
                         "buffer\n");
            return;
        }

        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1FileName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }

            case IKE_ERR_INVALID_FORMAT:
            {
                SPRINTF ((char *) pu1Temp, "%% Improper file format \r\n");
                break;
            }

            case IKE_ERR_CA_NOT_TRUSTED:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Verification failed due to untrusted CA \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to import peer cert \r\n");
                break;
            }
        }

    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                        pu1CertName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                        pu1FileName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliDelPeerCert                                   */
/*  Description     : This function deletes peer certificate              */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliDelPeerCert (tIkeCliConfigParams * pIkeCliConfigParams,
                   UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;
    UINT4               u4Count2 = IKE_ZERO;
    UINT4               u4Count1 = IKE_ZERO;
    tIkePhase1ID        Phase1ID;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportPeerCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportPeerCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
        }
        return;
    }

    u4Count1 = SLL_COUNT (&(pIkeEngine->TrustedPeerCerts));
    u4Count2 = SLL_COUNT (&(pIkeEngine->UntrustedPeerCerts));

    if ((u4Count1 == IKE_ZERO) && (u4Count2 == IKE_ZERO))
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries in table\r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
        }
        return;
    }
    else
    {
        if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
        {
            IKE_BZERO (&Phase1ID, sizeof (tIkePhase1ID));

            if (IkeDelPeerCert
                (pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName,
                 pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName,
                 pIkeCliConfigParams->CliIkeImportPeerCert.u1DelFlag,
                 &Phase1ID, &u4ErrorCode) == IKE_SUCCESS)
            {
                /* Send event to ike with phase1 Id information */
                if (IkeNmSendDelPeerCertReqToIke
                    (pIkeEngine->u4IkeIndex, &Phase1ID,
                     pIkeCliConfigParams->CliIkeImportPeerCert.u1DelFlag,
                     IKE_CONFIG_DEL_PEER_CERTIFICATE) == IKE_FAILURE)
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCliDelPeerCert: unable to allocate "
                                     "buffer\n");
                        return;
                    }

                    if (*ppu1Output == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCliDelPeerCert: Failed to allocate memory"
                                     "using MemAllocateMemBlock \r\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeImportPeerCert.pu1EngineName);
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeImportPeerCert.pu1CertName);
                        return;
                    }

                    CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                    pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
                    SPRINTF ((char *) pu1Temp,
                             "%% Failed to send event to IKE task for deletion\r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1CertName);
                    return;
                }
            }
        }
        else
        {
            if (IkeDelPeerCert
                (pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName, NULL,
                 pIkeCliConfigParams->CliIkeImportPeerCert.u1DelFlag,
                 &Phase1ID, &u4ErrorCode) == IKE_SUCCESS)
            {

                /* Send event to ike with phase1 Id information */
                if (IkeNmSendDelPeerCertReqToIke
                    (pIkeEngine->u4IkeIndex, NULL,
                     pIkeCliConfigParams->CliIkeImportPeerCert.u1DelFlag,
                     IKE_CONFIG_DEL_PEER_CERTIFICATE) == IKE_FAILURE)
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCliDelPeerCert: unable to allocate "
                                     "buffer\n");
                        return;
                    }
                    if (*ppu1Output == NULL)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCliDelPeerCert: Failed to allocate memory"
                                     "using MemAllocateMemBlock \r\n");
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeImportPeerCert.pu1EngineName);
                        return;
                    }

                    CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                    pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
                    SPRINTF ((char *) pu1Temp,
                             "%% Failed to send event to IKE task for deletion\r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1EngineName);
                    return;
                }
            }
        }
    }

    if (u4ErrorCode != IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }
            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to delete peer certificate \r\n");
                break;
            }
        }
    }
    else
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                        pu1EngineName);
    if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1CertName);
    }
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliImportCaCert                                  */
/*  Description     : This function imports ca certificate                */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliImportCaCert (tIkeCliConfigParams * pIkeCliConfigParams,
                    UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCaCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCaCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1FileName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1CertName);
        return;
    }

    if (IkeImportCaCertInCLI
        (pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName,
         pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName,
         pIkeCliConfigParams->CliIkeImportCaCert.pu1FileName,
         pIkeCliConfigParams->CliIkeImportCaCert.u1EncodeType,
         &u4ErrorCode) == IKE_SUCCESS)
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1FileName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }

            case IKE_ERR_INVALID_FORMAT:
            {
                SPRINTF ((char *) pu1Temp, "%% Improper file format \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp, "%% Failed to delete CA cert \r\n");
                break;
            }
        }

    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                        pu1CertName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                        pu1FileName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliDelCaCert                                     */
/*  Description     : This function deletes ca certificate                */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliDelCaCert (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;
    UINT4               u4Count = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCaCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCaCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1EngineName);
        return;
    }

    u4Count = SLL_COUNT (&(pIkeEngine->CACerts));

    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries to delete \r\n");
        if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1EngineName);
        return;
    }

    if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
    {
        if (IkeDelCaCert (pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName,
                          pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName,
                          &u4ErrorCode) == IKE_SUCCESS)
        {
            /* Send event to ike with IkeEngine name */
            if (IkeNmSendDelCaCertInfoToIke
                (pIkeEngine->u4IkeIndex,
                 pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName,
                 IKE_CONFIG_DEL_CACERT) == IKE_FAILURE)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliDelCaCert: unable to allocate "
                                 "buffer\n");
                    return;
                }
                if (*ppu1Output == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliDelCaCert: Failed to allocate memory"
                                 "using MemAllocateMemBlock \r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportCaCert.pu1CertName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportCaCert.pu1EngineName);
                    return;
                }

                CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to send event to IKE task for deletion : \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1EngineName);
                return;
            }
            else
            {
                IKE_NO_RESPONSE_MESSAGE (ppu1Output);
            }
        }
    }
    else
    {
        if (IkeDelCaCert
            (pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName, NULL,
             &u4ErrorCode) == IKE_SUCCESS)
        {
            /* Send event to ike with IkeEngine name and CA certificate name */
            if (IkeNmSendDelCaCertInfoToIke
                (pIkeEngine->u4IkeIndex,
                 pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName,
                 IKE_CONFIG_DEL_CACERT) == IKE_FAILURE)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliDelCaCert: unable to allocate "
                                 "buffer\n");
                    return;
                }
                if (*ppu1Output == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliDelCaCert: Failed to allocate memory"
                                 "using MemAllocateMemBlock \r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportCaCert.pu1EngineName);
                    return;
                }

                CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to send event to IKE task for deletion : \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1EngineName);
                return;
            }
            IKE_NO_RESPONSE_MESSAGE (ppu1Output);
        }
    }

    if (u4ErrorCode != IKE_ZERO)
    {
        if (*ppu1Output != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) (*ppu1Output));
        }
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to delete CA certificate \r\n");
                break;
            }
        }
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                        pu1EngineName);
    if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1CertName);
    }

    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliCertMapPeer                                   */
/*  Description     : This function maps certificate to a peer or makes as*/
/*                    default                                             */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliCertMapPeer (tIkeCliConfigParams * pIkeCliConfigParams,
                   UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    tIkeCertMapToPeer  *pCertMap = NULL;
    INT4                i4RetVal = IKE_ONE;
    INT4                i4Val = IKE_FAILURE;
    tIkePhase1ID        Phase1Id;
    tIkeRsaKeyInfo     *pIkeRsaInfo = NULL;
    tIkeDsaKeyInfo     *pIkeDsaInfo = NULL;
    tIkeX509Name       *pIkeCaName = NULL;
    tIkeASN1Integer    *pIkeCaSN = NULL;

    IKE_BZERO (&Phase1Id, sizeof (tIkePhase1ID));

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeMapCertToPeer.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeMapCertToPeer.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliCertMapPeer: Engine does not exist! \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);

        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }

    pIkeRsaInfo =
        IkeGetRsaKeyEntry (pIkeEngine->au1EngineName,
                           pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyName);
    if (pIkeRsaInfo != NULL)
    {
        if ((pIkeRsaInfo->CertInfo.pCAName != NULL) &&
            (pIkeRsaInfo->CertInfo.pCASerialNum != NULL))
        {
            pIkeCaName = pIkeRsaInfo->CertInfo.pCAName;
            pIkeCaSN = pIkeRsaInfo->CertInfo.pCASerialNum;
            i4Val = IKE_SUCCESS;
        }
    }
    else
    {
        pIkeDsaInfo = IkeGetDsaKeyEntry (pIkeEngine->au1EngineName,
                                         pIkeCliConfigParams->
                                         CliIkeMapCertToPeer.pu1KeyName);
        if ((pIkeDsaInfo != NULL) && (pIkeDsaInfo->CertInfo.pCAName != NULL) &&
            (pIkeDsaInfo->CertInfo.pCASerialNum != NULL))
        {
            pIkeCaName = pIkeDsaInfo->CertInfo.pCAName;
            pIkeCaSN = pIkeDsaInfo->CertInfo.pCASerialNum;
            i4Val = IKE_SUCCESS;
        }
    }

    /* If key entry cert information is not present then discard 
     * this configuration */
    if (i4Val == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliCertMapPeer: Key entry doesn't exist \r\n");

        IKE_FORMAT_ERROR_MESSAGE ("Key Entry doesn't exist (or) "
                                  "Import the Certificate before mapping\r\n",
                                  ppu1Output);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }

    Phase1Id.i4IdType = pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType;
    if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType != IKE_MAP_DEFAULT)
    {
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType == IPV4ADDR)
        {
            tIp4Addr            u4TempIp4Addr = IKE_ZERO;

            i4RetVal =
                INET_PTON4 (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                            &u4TempIp4Addr);

            if (i4RetVal > IKE_ZERO)
            {
                u4TempIp4Addr = IKE_NTOHL (u4TempIp4Addr);
                Phase1Id.uID.Ip4Addr = u4TempIp4Addr;
                Phase1Id.u4Length = IKE_IPV4_LENGTH;
            }
        }
        else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                 IPV6ADDR)
        {
            tIp6Addr            TempIp6Addr;

            i4RetVal =
                INET_PTON6 (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                            &(TempIp6Addr.u1_addr));

            if (i4RetVal > IKE_ZERO)
            {
                IKE_MEMCPY (&Phase1Id.uID.Ip6Addr, &(TempIp6Addr.u1_addr),
                            IKE_IPV6_LENGTH);
                Phase1Id.u4Length = IKE_IPV6_LENGTH;
            }
        }
        else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                 CLI_IKE_KEY_FQDN)
        {
            MEMSET (Phase1Id.uID.au1Fqdn, IKE_ZERO, MAX_NAME_LENGTH);
            IKE_MEMCPY (Phase1Id.uID.au1Fqdn,
                        pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                        MAX_NAME_LENGTH);
            Phase1Id.u4Length = MAX_NAME_LENGTH;
        }
        else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                 CLI_IKE_KEY_EMAIL)
        {
            MEMSET (Phase1Id.uID.au1Email, IKE_ZERO, MAX_NAME_LENGTH);
            IKE_MEMCPY (Phase1Id.uID.au1Email,
                        pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                        MAX_NAME_LENGTH);
            Phase1Id.u4Length = MAX_NAME_LENGTH;
        }
        else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                 CLI_IKE_KEY_DN)
        {
            MEMSET (Phase1Id.uID.au1Dn, IKE_ZERO, MAX_NAME_LENGTH);
            IKE_MEMCPY (Phase1Id.uID.au1Dn,
                        pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                        MAX_NAME_LENGTH);
            Phase1Id.u4Length = MAX_NAME_LENGTH;
        }
        else
        {
            IKE_FORMAT_ERROR_MESSAGE
                (" Invalid identifier. IP/FQDN/EMAIL/DN identifiers are "
                 "supported\r\n", ppu1Output);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
            return;
        }
    }

    if (i4RetVal <= IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliCertMapPeer: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyId);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp,
                 "%% Unable to convert the address to pointer format \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }

    /* Check if the configuration for peer is already existing */
    if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType != IKE_MAP_DEFAULT)
    {
        TAKE_IKE_SEM ();
        SLL_SCAN (&(pIkeEngine->IkeCertMapList), pCertMap, tIkeCertMapToPeer *)
        {
            if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                IPV4ADDR)
            {
                if (Phase1Id.uID.Ip4Addr == pCertMap->PeerID.uID.Ip4Addr)
                {
                    break;
                }
            }
            else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                     IPV6ADDR)
            {
                if (IKE_MEMCMP
                    (&Phase1Id.uID.Ip6Addr,
                     &pCertMap->PeerID.uID.Ip6Addr, IKE_IPV6_LENGTH)
                    == IKE_ZERO)
                {
                    break;
                }
            }
        }
        GIVE_IKE_SEM ();

        if (pCertMap != NULL)
        {
            /* updating the certificate identity */
            if (STRLEN (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyName) >
                MAX_NAME_LENGTH)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliCertMapPeer: Certificate Key name size is greater than 64\r\n");
                return;
            }
            IKE_STRNCPY (pCertMap->au1KeyName, pIkeCliConfigParams->
                         CliIkeMapCertToPeer.pu1KeyName,
                         MAX_NAME_LENGTH - IKE_ONE);
            /* Already an entry is existing with the same entry 
             * hence reject this configuration */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliCertMapPeer: Duplicate configuration\r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
            return;
        }
        else
        {

            if (MemAllocateMemBlock
                (IKE_CERTMAP_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pCertMap) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessKernelIpsecRequest: unable to allocate "
                             "buffer\n");
                return;
            }
            if (pCertMap == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliCertMapPeer: Memory allocation "
                             "failed! \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyId);
                return;
            }

            IKE_STRCPY (pCertMap->au1EngineName, pIkeEngine->au1EngineName);
            IKE_STRCPY (pCertMap->au1KeyName, pIkeCliConfigParams->
                        CliIkeMapCertToPeer.pu1KeyName);

            pCertMap->MyCert.pCAName = IkeX509NameDup (pIkeCaName);
            pCertMap->MyCert.pCASerialNum = IkeASN1IntegerDup (pIkeCaSN);

            IKE_MEMCPY (&pCertMap->PeerID, &Phase1Id, sizeof (tIkePhase1ID));

            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeCertMapList), (t_SLL_NODE *) pCertMap);
            GIVE_IKE_SEM ();
        }
    }
    else
    {
        if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
            (pIkeEngine->DefaultCert.pCASerialNum != NULL))
        {
            if ((IkeX509NameCmp (pIkeEngine->DefaultCert.pCAName,
                                 pIkeCaName) == IKE_ZERO) &&
                (IkeASN1IntegerCmp (pIkeEngine->DefaultCert.pCASerialNum,
                                    pIkeCaSN) == IKE_ZERO))
            {
                /* Already an entry is existing with the same entry 
                 * hence reject this configuration */
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliCertMapPeer: Duplicate configuration\r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyName);
                if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyId);
                }
                return;
            }
            else
            {
                IKE_FORMAT_ERROR_MESSAGE
                    (" Delete current default  configuration\r\n", ppu1Output);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyName);
                if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyId);
                }

                return;
            }
        }
        else
        {
            pIkeEngine->DefaultCert.pCAName = IkeX509NameDup (pIkeCaName);
            pIkeEngine->DefaultCert.pCASerialNum = IkeASN1IntegerDup (pIkeCaSN);
        }
    }

    IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                        pu1KeyName);
    if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyId);
    }
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliDelCertMap                                    */
/*  Description     : This function maps certificate to a peer or makes as*/
/*                    default                                             */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliDelCertMap (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkePhase1ID        Phase1Id;
    tIkeIpAddr          PeerIpAddr;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRsaKeyInfo     *pIkeRsaInfo = NULL;
    tIkeDsaKeyInfo     *pIkeDsaInfo = NULL;
    tIkeCertMapToPeer  *pIkeCertMap = NULL;
    tIkeX509Name       *pIkeCaName = NULL;
    tIkeASN1Integer    *pIkeCaSN = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4Count = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    UINT4               u4TempLen = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeMapCertToPeer.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeMapCertToPeer.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCertMap: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCertMap: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyId);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeGenKey.pu1EngineName);

        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }

    pIkeRsaInfo =
        IkeGetRsaKeyEntry (pIkeEngine->au1EngineName,
                           pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyName);
    if (pIkeRsaInfo != NULL)
    {
        if ((pIkeRsaInfo->CertInfo.pCAName != NULL) &&
            (pIkeRsaInfo->CertInfo.pCASerialNum != NULL))
        {
            pIkeCaName = pIkeRsaInfo->CertInfo.pCAName;
            pIkeCaSN = pIkeRsaInfo->CertInfo.pCASerialNum;
            i4RetVal = IKE_SUCCESS;
        }
    }
    else
    {
        pIkeDsaInfo = IkeGetDsaKeyEntry (pIkeEngine->au1EngineName,
                                         pIkeCliConfigParams->
                                         CliIkeMapCertToPeer.pu1KeyName);
        if ((pIkeDsaInfo != NULL) && (pIkeDsaInfo->CertInfo.pCAName != NULL) &&
            (pIkeDsaInfo->CertInfo.pCASerialNum != NULL))
        {
            pIkeCaName = pIkeDsaInfo->CertInfo.pCAName;
            pIkeCaSN = pIkeDsaInfo->CertInfo.pCASerialNum;
            i4RetVal = IKE_SUCCESS;
        }
    }

    /* If key entry cert information is not present then discard 
     * this configuration */
    if (i4RetVal == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliDelCertMap: Key entry doesn't exist \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }
    u4Count = SLL_COUNT (&(pIkeEngine->IkeCertMapList));

    IKE_BZERO (&Phase1Id, sizeof (tIkePhase1ID));
    Phase1Id.i4IdType = pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType;

    if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
    {
        if (u4Count == IKE_ZERO)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
            return;
        }
        else
        {
            IKE_BZERO (&PeerIpAddr, sizeof (tIkeIpAddr));

            if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                IPV4ADDR)
            {
                tIp4Addr            u4TempIp4Addr = IKE_ZERO;

                i4RetVal =
                    INET_PTON4 (pIkeCliConfigParams->CliIkeMapCertToPeer.
                                pu1KeyId, &u4TempIp4Addr);

                if (i4RetVal > IKE_ZERO)
                {
                    u4TempIp4Addr = IKE_NTOHL (u4TempIp4Addr);
                    PeerIpAddr.u4AddrType = IPV4ADDR;
                    PeerIpAddr.Ipv4Addr = u4TempIp4Addr;
                    Phase1Id.uID.Ip4Addr = u4TempIp4Addr;
                    Phase1Id.u4Length = IKE_IPV4_LENGTH;
                }
                else
                {
                    IKE_FORMAT_ERROR_MESSAGE
                        ("Unable to convert the address to pointer format \r\n",
                         ppu1Output);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyId);
                    return;
                }
            }
            else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                     IPV6ADDR)
            {
                tIp6Addr            TempIp6Addr;

                i4RetVal =
                    INET_PTON6 (pIkeCliConfigParams->CliIkeMapCertToPeer.
                                pu1KeyId, &(TempIp6Addr.u1_addr));

                if (i4RetVal > IKE_ZERO)
                {
                    PeerIpAddr.u4AddrType = IPV6ADDR;
                    IKE_MEMCPY (&PeerIpAddr.Ipv6Addr, &(TempIp6Addr.u1_addr),
                                sizeof (tIp6Addr));
                    IKE_MEMCPY (&Phase1Id.uID.Ip6Addr, &(TempIp6Addr.u1_addr),
                                sizeof (tIp6Addr));
                    Phase1Id.u4Length = IKE_IPV6_LENGTH;
                }
                else
                {
                    IKE_FORMAT_ERROR_MESSAGE
                        ("Unable to convert the address to pointer format \r\n",
                         ppu1Output);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1EngineName);

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyId);
                    return;
                }
            }
            else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                     CLI_IKE_KEY_FQDN)
            {
                MEMSET (Phase1Id.uID.au1Fqdn, IKE_ZERO, MAX_NAME_LENGTH);
                IKE_MEMCPY (Phase1Id.uID.au1Fqdn,
                            pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                            MAX_NAME_LENGTH);
                Phase1Id.u4Length = MAX_NAME_LENGTH;
            }
            else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                     CLI_IKE_KEY_EMAIL)
            {
                MEMSET (Phase1Id.uID.au1Email, IKE_ZERO, MAX_NAME_LENGTH);
                IKE_MEMCPY (Phase1Id.uID.au1Email,
                            pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                            MAX_NAME_LENGTH);
                Phase1Id.u4Length = MAX_NAME_LENGTH;
            }
            else if (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                     CLI_IKE_KEY_DN)
            {
                MEMSET (Phase1Id.uID.au1Dn, IKE_ZERO, MAX_NAME_LENGTH);
                u4TempLen = MEM_MAX_BYTES (STRLEN (pIkeCliConfigParams->
                                                   CliIkeMapCertToPeer.
                                                   pu1KeyId),
                                           (UINT4) MAX_NAME_LENGTH - IKE_ONE);
                IKE_MEMCPY (Phase1Id.uID.au1Dn,
                            pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId,
                            u4TempLen);
                Phase1Id.u4Length = MAX_NAME_LENGTH;
                Phase1Id.uID.au1Dn[u4TempLen] = '\0';
            }

            else
            {
                IKE_FORMAT_ERROR_MESSAGE
                    ("Currently ipv4 and ipv6 id types are supported \r\n",
                     ppu1Output);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1EngineName);

                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyId);
                return;
            }

            if ((pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                 IPV4ADDR)
                || (pIkeCliConfigParams->CliIkeMapCertToPeer.u1KeyIdType ==
                    IPV6ADDR))
            {
                pIkeCertMap =
                    IkeGetCertMapFromPeerAddr (pIkeEngine, &PeerIpAddr);
            }
            else
            {
                pIkeCertMap =
                    IkeGetCertMapFromRemoteID (pIkeEngine, Phase1Id.uID.au1Dn,
                                               pIkeCliConfigParams->
                                               CliIkeMapCertToPeer.u1KeyIdType);
            }
            if (pIkeCertMap != NULL)
            {

                if ((pIkeCaName != NULL) && (pIkeCaSN != NULL))
                {
                    if ((IkeX509NameCmp
                         (pIkeCertMap->MyCert.pCAName, pIkeCaName) != IKE_ZERO)
                        ||
                        (IkeASN1IntegerCmp
                         (pIkeCertMap->MyCert.pCASerialNum,
                          pIkeCaSN) != IKE_ZERO))
                    {
                        IKE_FORMAT_ERROR_MESSAGE
                            ("Wrong key used to delete cert-map "
                             "information \r\n", ppu1Output);
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeMapCertToPeer.pu1EngineName);

                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeMapCertToPeer.pu1KeyName);
                        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId !=
                            NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pIkeCliConfigParams->
                                                CliIkeMapCertToPeer.pu1KeyId);
                        }

                        return;
                    }

                }
                TAKE_IKE_SEM ();
                SLL_DELETE (&(pIkeEngine->IkeCertMapList),
                            (t_SLL_NODE *) pIkeCertMap);
                GIVE_IKE_SEM ();
            }
            else
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliDelCertMap: No entry with this peer ID \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeMapCertToPeer.pu1KeyId);
                return;
            }
        }
    }
    else
    {
        /* Check if default cert information is existing in the router */
        if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
            (pIkeEngine->DefaultCert.pCASerialNum != NULL))
        {
            if ((pIkeCaName != NULL) && (pIkeCaSN != NULL))
            {
                if ((IkeX509NameCmp
                     (pIkeEngine->DefaultCert.pCAName, pIkeCaName) != IKE_ZERO)
                    ||
                    (IkeASN1IntegerCmp
                     (pIkeEngine->DefaultCert.pCASerialNum,
                      pIkeCaSN) != IKE_ZERO))
                {
                    IKE_FORMAT_ERROR_MESSAGE
                        ("Wrong key used to delete default cert "
                         "information \r\n", ppu1Output);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1EngineName);

                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeMapCertToPeer.pu1KeyName);
                    return;
                }
            }
            /* Remove the information ike default cert information */
            IkeX509NameFree (pIkeEngine->DefaultCert.pCAName);
            pIkeEngine->DefaultCert.pCAName = NULL;
            IkeASN1IntegerFree (pIkeEngine->DefaultCert.pCASerialNum);
            pIkeEngine->DefaultCert.pCASerialNum = NULL;
        }
        else
        {
            IKE_FORMAT_ERROR_MESSAGE
                ("No default cert information in the engine to delete \r\n",
                 ppu1Output);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);

            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyName);
            return;
        }
    }

    if (IkeNmSendMapCertInfoToIke
        (pIkeEngine->u4IkeIndex,
         pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyName,
         &Phase1Id, IKE_CONFIG_DEL_CERT_MAP) == IKE_FAILURE)
    {
        /* CliPrintf (CliHandle, " Unable to send information to IKE\r\n",
           ppu1Output); */
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyName);
        if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1KeyId);
        }
        return;
    }

    IKE_NO_RESPONSE_MESSAGE (ppu1Output);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                        pu1KeyName);
    if (pIkeCliConfigParams->CliIkeMapCertToPeer.pu1KeyId != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1KeyId);
    }
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliSaveCert                                      */
/*  Description     : This function saves the certificates to filesystem  */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliSaveCert (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    UINT1              *pu1Temp = NULL;

    if (pIkeCliConfigParams->CliIkeSaveCert.pu1EngineName == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliSaveCert: unable to allocate " "buffer\n");
            return;
        }

        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliSaveCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeSaveCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeGenKey.pu1EngineName);

        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeSaveCert.
                            pu1EngineName);
        return;
    }

    if (IkeSaveCertConfig (pIkeCliConfigParams->CliIkeSaveCert.pu1EngineName)
        == IKE_FAILURE)
    {
        IKE_FORMAT_ERROR_MESSAGE
            ("Failed to save the certficates \r\n", ppu1Output);
    }
    else
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeSaveCert.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliDelCert                                       */
/*  Description     : This function deletes certificate                   */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliDelCert (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;
    tIkeX509Name       *pCAName = NULL;
    UINT4               u4ErrorCode = IKE_ZERO;
    UINT4               u4Count = IKE_ZERO;
    tIkeCertMapToPeer  *pCertMap = NULL, *pPrev = NULL;
    tIkeIpAddr          PeerIpAddr;
    BOOLEAN             bErrValue = IKE_FALSE;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
        }
        return;
    }

    u4Count = SLL_COUNT (&(pIkeEngine->MyCerts));
    if (u4Count == IKE_ZERO)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
        }
        return;
    }
    else
    {
        if (IkeDelMyCert
            (pIkeCliConfigParams->CliIkeImportCert.pu1EngineName,
             pIkeCliConfigParams->CliIkeImportCert.pu1KeyName, &pCAName,
             &pCASerialNum, &u4ErrorCode) == IKE_SUCCESS)
        {
            if ((pCAName == NULL) || (pCASerialNum == NULL))
            {
                TAKE_IKE_SEM ();
                if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
                    (pIkeEngine->DefaultCert.pCASerialNum != NULL))
                {
                    IkeX509NameFree (pIkeEngine->DefaultCert.pCAName);
                    IkeASN1IntegerFree (pIkeEngine->DefaultCert.pCASerialNum);

                    pIkeEngine->DefaultCert.pCAName = NULL;
                    pIkeEngine->DefaultCert.pCASerialNum = NULL;
                }

                SLL_DELETE_LIST (&(pIkeEngine->IkeCertMapList),
                                 IkeFreeCertMapInfo);
                GIVE_IKE_SEM ();

                if (IkeNmSendCertDeleteReqToIke
                    (pIkeEngine->u4IkeIndex, NULL,
                     IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                {
                    IKE_FORMAT_ERROR_MESSAGE
                        ("Event posting to Ike for deletion failed\r\n",
                         ppu1Output);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportCert.pu1EngineName);
                    if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName !=
                        NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pIkeCliConfigParams->
                                            CliIkeImportCert.pu1KeyName);
                    }
                    return;
                }
            }
            else
            {
                /* Check if the deleted certificate is a default 
                 * certificate, if so remove the mapping */
                if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
                    (pIkeEngine->DefaultCert.pCASerialNum != NULL) &&
                    (IkeX509NameCmp
                     (pCAName, pIkeEngine->DefaultCert.pCAName) == IKE_ZERO)
                    &&
                    (IkeASN1IntegerCmp
                     (pCASerialNum, pIkeEngine->DefaultCert.pCASerialNum) ==
                     IKE_ZERO))
                {
                    TAKE_IKE_SEM ();
                    IkeX509NameFree (pIkeEngine->DefaultCert.pCAName);
                    IkeASN1IntegerFree (pIkeEngine->DefaultCert.pCASerialNum);

                    pIkeEngine->DefaultCert.pCAName = NULL;
                    pIkeEngine->DefaultCert.pCASerialNum = NULL;
                    GIVE_IKE_SEM ();

                    if (IkeNmSendCertDeleteReqToIke
                        (pIkeEngine->u4IkeIndex, NULL,
                         IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                    {
                        bErrValue = IKE_TRUE;
                        IKE_FORMAT_ERROR_MESSAGE
                            ("Event posting to Ike for deletion failed\r\n",
                             ppu1Output);
                    }
                }

                SLL_SCAN (&(pIkeEngine->IkeCertMapList), pCertMap,
                          tIkeCertMapToPeer *)
                {
                    if ((IkeX509NameCmp
                         (pCAName, pCertMap->MyCert.pCAName) == IKE_ZERO)
                        &&
                        (IkeASN1IntegerCmp
                         (pCASerialNum, pCertMap->MyCert.pCASerialNum) ==
                         IKE_ZERO))
                    {
                        IKE_BZERO (&PeerIpAddr, sizeof (tIkeIpAddr));

                        PeerIpAddr.u4AddrType =
                            (UINT4) pCertMap->PeerID.i4IdType;

                        if (PeerIpAddr.u4AddrType == IPV4ADDR)
                        {
                            PeerIpAddr.Ipv4Addr = pCertMap->PeerID.uID.Ip4Addr;
                        }
                        else
                        {
                            IKE_MEMCPY (&PeerIpAddr.Ipv6Addr,
                                        &pCertMap->PeerID.uID.Ip6Addr,
                                        sizeof (tIp6Addr));
                        }

                        if (IkeNmSendCertDeleteReqToIke
                            (pIkeEngine->u4IkeIndex, &PeerIpAddr,
                             IKE_CONFIG_DELETE_MYCERT) == IKE_FAILURE)
                        {
                            bErrValue = IKE_TRUE;
                            if (*ppu1Output != NULL)
                            {
                                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                    (UINT1 *) *ppu1Output);
                            }

                            IKE_FORMAT_ERROR_MESSAGE
                                ("Event posting to Ike for deletion failed\r\n",
                                 ppu1Output);

                            /* Added to address klocwork warning, which seemed logical */
                            break;
                        }
                        TAKE_IKE_SEM ();
                        SLL_DELETE (&(pIkeEngine->IkeCertMapList),
                                    (t_SLL_NODE *) pCertMap);
                        IkeFreeCertMapInfo ((t_SLL_NODE *) pCertMap);
                        GIVE_IKE_SEM ();
                        pCertMap = pPrev;
                    }
                    pPrev = pCertMap;
                }

                IkeX509NameFree (pCAName);
                pCAName = NULL;
                IkeASN1IntegerFree (pCASerialNum);
                pCASerialNum = NULL;
            }

            if (bErrValue == IKE_FALSE)
            {
                IKE_NO_RESPONSE_MESSAGE (ppu1Output);
            }
        }
    }

    if (u4ErrorCode != IKE_ZERO)
    {
        if (*ppu1Output != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) (*ppu1Output));
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliDelCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCert.pu1KeyName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Certificate does not exist \r\n");
                break;
            }

            default:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Failed to delete certificate \r\n");
                break;
            }
        }
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1EngineName);
    if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1KeyName);
    }
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliShowKeys                                      */
/*  Description     : This function displays the RSA/DSA Keys             */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliShowKeys (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4Count = IKE_ZERO;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeGenKey.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeGenKey.pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessKernelIpsecRequest: unable to allocate "
                         "buffer\n");
            return;
        }

        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowKeys: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeGenKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }

    if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_RSA_SIGNATURE)
    {
        u4Count = SLL_COUNT (&(pIkeEngine->IkeRsaKeyList));
    }
    else if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_DSS_SIGNATURE)
    {
        u4Count = SLL_COUNT (&(pIkeEngine->IkeDsaKeyList));
    }
    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowKeys: unable to allocate " "buffer\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            return;
        }

        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowKeys: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries in table\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliShowKeys: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }

    if (*ppu1Output == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliShowKeys: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                            pu1EngineName);
        return;
    }

    CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
    pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

    if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_RSA_SIGNATURE)
    {
        SLL_SCAN (&(pIkeEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
        {
            SPRINTF ((char *) pu1Temp, "\t KeyID %-36s", pIkeRsa->au1RsaKeyName);
            pu1Temp += STRLEN (pu1Temp);
            SPRINTF ((char *) pu1Temp, "\tRsaKey : ********\n");
            pu1Temp += STRLEN (pu1Temp);
        }
    }
    else if (pIkeCliConfigParams->CliIkeGenKey.u4Type == IKE_DSS_SIGNATURE)
    {
        SLL_SCAN (&(pIkeEngine->IkeDsaKeyList), pIkeDsa, tIkeDsaKeyInfo *)
        {
            SPRINTF ((char *) pu1Temp, "\t KeyID %-36s", pIkeDsa->au1DsaKeyName);
            pu1Temp += STRLEN (pu1Temp);
            SPRINTF ((char *) pu1Temp, "\tDsaKey : ********\n");
            pu1Temp += STRLEN (pu1Temp);
        }
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeGenKey.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliShowCerts                                     */
/*  Description     : This function displays the certificates             */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliShowCerts (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL, *pu1ASCII = NULL;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4ASCIILen = IKE_ZERO;
    tIkeCertDB         *pIkeCert = NULL;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCert.pu1KeyName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeImportCert.pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        return;
    }

    u4Count = SLL_COUNT (&(pIkeEngine->MyCerts));
    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCert.pu1KeyName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries in table\n");
        if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        return;
    }
    else if (pIkeCliConfigParams->CliIkeImportCert.pu1KeyName == NULL)
    {
        /* display all the certificates */
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        TAKE_IKE_SEM ();
        SLL_SCAN (&(pIkeEngine->MyCerts), pIkeCert, tIkeCertDB *)
        {

            if (IkeConvertX509ToASCII (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen)
                == IKE_SUCCESS)
            {
                SPRINTF ((char *) pu1Temp, "\t Cert ID: %s \n",
                         pIkeCert->au1CertId);
                pu1Temp += STRLEN (pu1Temp);

                /* The Certificate details are not supposed to be 
                 * displayed in FIPS mode */
                if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                {
                    SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                    pu1Temp += STRLEN (pu1Temp);
                }

                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
                pu1ASCII = NULL;
            }
        }
        GIVE_IKE_SEM ();
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);

        if (pu1ASCII != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
            pu1ASCII = NULL;
        }
        return;
    }

    pIkeRsa =
        IkeGetRSAKeyEntry (pIkeCliConfigParams->CliIkeImportCert.
                           pu1EngineName,
                           pIkeCliConfigParams->CliIkeImportCert.pu1KeyName);

    if (pIkeRsa != NULL)
    {
        if ((pIkeRsa->CertInfo.pCAName != NULL) &&
            (pIkeRsa->CertInfo.pCASerialNum != NULL))
        {
            pIkeCert =
                IkeGetCertFromMyCerts (pIkeEngine->au1EngineName,
                                       pIkeRsa->CertInfo.pCAName,
                                       pIkeRsa->CertInfo.pCASerialNum);
        }
    }
    else if ((pIkeDsa = IkeGetDSAKeyEntry (pIkeCliConfigParams->
                                           CliIkeImportCert.pu1EngineName,
                                           pIkeCliConfigParams->
                                           CliIkeImportCert.pu1KeyName))
             != NULL)
    {
        if ((pIkeDsa->CertInfo.pCAName != NULL) &&
            (pIkeDsa->CertInfo.pCASerialNum != NULL))
        {
            pIkeCert =
                IkeGetCertFromMyCerts (pIkeEngine->au1EngineName,
                                       pIkeDsa->CertInfo.pCAName,
                                       pIkeDsa->CertInfo.pCASerialNum);
        }
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% No key entry with this keyID\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        return;
    }
    if (pIkeCert == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% No certificate exits with this keyID\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                            pu1EngineName);
        return;
    }
    if (IkeConvertX509ToASCII
        (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCerts: Failed to allocate "
                         "memory MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                                pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        SPRINTF ((char *) pu1Temp, "\tCert ID: %s \n", pIkeCert->au1CertId);
        pu1Temp += STRLEN (pu1Temp);

        /* The Certificate details are not supposed to be 
         * displayed in FIPS mode */
        if (FipsGetFipsCurrOperMode () != FIPS_MODE)
        {
            SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
            pu1Temp += STRLEN (pu1Temp);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
        pu1ASCII = NULL;
    }
    else
    {
        IKE_FORMAT_ERROR_MESSAGE (" Failed to convert the cert to ASCII "
                                  "format", ppu1Output);
        if (pu1ASCII != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
            pu1ASCII = NULL;
        }
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1KeyName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCert.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliShowPeerCert                                  */
/*  Description     : This function displays peer certificates            */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/
VOID
IkeCliShowPeerCert (tIkeCliConfigParams * pIkeCliConfigParams,
                    UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1              *pu1ASCII = NULL;
    UINT4               u4Count1 = IKE_ZERO;
    UINT4               u4Count2 = IKE_ZERO;
    UINT4               u4ASCIILen = IKE_ZERO;
    tIkeCertDB         *pIkeCert = NULL;
    UINT4               u4Dynamic = IKE_ZERO;
    UINT4               u4Mask = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportPeerCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportPeerCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeImportPeerCert.pu1EngineName);
        if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1EngineName);
        return;
    }

    u4Count1 = SLL_COUNT (&(pIkeEngine->TrustedPeerCerts));
    u4Count2 = SLL_COUNT (&(pIkeEngine->UntrustedPeerCerts));

    if ((u4Count1 == IKE_ZERO) && (u4Count2 == IKE_ZERO))
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);

            if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries in table\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1EngineName);

        if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
        }
        return;
    }
    else if (pIkeCliConfigParams->CliIkeImportPeerCert.pu1CertName == NULL)
    {
        if (pIkeCliConfigParams->CliIkeImportPeerCert.u1Flag ==
            IKE_DYNAMIC_PEERCERT)
        {
            /* Count the number of dynamic peer certs */
            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->TrustedPeerCerts), pIkeCert, tIkeCertDB *)
            {
                u4Mask = pIkeCert->u4Flag & IKE_DYNAMIC_PEER_CERT;
                if (u4Mask != IKE_ZERO)
                {
                    u4Dynamic++;
                }
            }
            GIVE_IKE_SEM ();

            if (u4Dynamic == IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: unable to allocate"
                                 "buffer\n");
                    return;
                }
                if (*ppu1Output == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: Failed to allocate memory"
                                 "using MemAllocateMemBlock \r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1EngineName);
                    return;
                }

                CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
                SPRINTF ((char *) pu1Temp,
                         "%% No dynamic peer certs in table\n");
            }
            else
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: unable to allocate "
                                 "buffer\n");
                    return;
                }
                if (*ppu1Output == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: Failed to allocate memory"
                                 "using MemAllocateMemBlock \r\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1EngineName);
                    return;
                }

                CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

                u4Mask = IKE_ZERO;
                TAKE_IKE_SEM ();
                SLL_SCAN (&(pIkeEngine->TrustedPeerCerts), pIkeCert,
                          tIkeCertDB *)
                {
                    SPRINTF ((char *) pu1Temp, "%s ",
                             "\t Dynamically learned peer certificates\n");
                    pu1Temp += STRLEN (pu1Temp);

                    u4Mask = pIkeCert->u4Flag & IKE_DYNAMIC_PEER_CERT;

                    if (u4Mask != IKE_ZERO)
                    {
                        if (IkeConvertX509ToASCII
                            (pIkeCert->pCert, &pu1ASCII,
                             &u4ASCIILen) == IKE_SUCCESS)
                        {
                            /* The Certificate details are not supposed to be 
                             * displayed in FIPS mode */
                            if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                            {
                                SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                                pu1Temp += STRLEN (pu1Temp);
                            }

                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1ASCII);
                            pu1ASCII = NULL;
                        }
                    }
                }

                GIVE_IKE_SEM ();
            }
            if (pu1ASCII != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
                pu1ASCII = NULL;
            }
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            return;
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: unable to allocate" "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        if (u4Count1 != IKE_ZERO)
        {
            SPRINTF ((char *) pu1Temp, "%s ", "\t Trusted peer certificates\n");
            pu1Temp += STRLEN (pu1Temp);

            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->TrustedPeerCerts), pIkeCert, tIkeCertDB *)
            {
                if (IkeConvertX509ToASCII
                    (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
                {
                    /* The Certificate details are not supposed to be 
                     * displayed in FIPS mode */
                    if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                    {
                        SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                        pu1Temp += STRLEN (pu1Temp);
                    }
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1ASCII);
                    pu1ASCII = NULL;
                }
            }
            GIVE_IKE_SEM ();
        }
        if (pu1ASCII != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
            pu1ASCII = NULL;
        }

        if (u4Count2 != IKE_ZERO)
        {
            SPRINTF ((char *) pu1Temp, "%s ",
                     "\t UnTrusted peer certificates\n");
            pu1Temp += STRLEN (pu1Temp);
            TAKE_IKE_SEM ();
            SLL_SCAN (&(pIkeEngine->UntrustedPeerCerts), pIkeCert, tIkeCertDB *)
            {
                if (IkeConvertX509ToASCII
                    (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
                {
                    /* The Certificate details are not supposed to be 
                     * displayed in FIPS mode */
                    if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                    {
                        SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                        pu1Temp += STRLEN (pu1Temp);
                    }
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1ASCII);
                    pu1ASCII = NULL;
                }
            }
            GIVE_IKE_SEM ();
        }
        if (pu1ASCII != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
            pu1ASCII = NULL;
        }
    }
    else
    {
        /* Search trusted and untrusted database */
        pIkeCert =
            IkeGetPeerCertById (pIkeEngine->au1EngineName,
                                pIkeCliConfigParams->
                                CliIkeImportPeerCert.pu1CertName);

        if (pIkeCert != NULL)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliShowPeerCert: unable to allocate"
                             "buffer\n");
                return;
            }
            if (*ppu1Output == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliShowPeerCert: Failed to allocate memory"
                             "using MemAllocateMemBlock \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1EngineName);
                return;
            }

            CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
            pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

            if (IkeConvertX509ToASCII
                (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
            {
                /* The Certificate details are not supposed to be 
                 * displayed in FIPS mode */
                if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                {
                    SNPRINTF ((char *) pu1Temp, STRLEN (pu1ASCII), "%s \n",
                              pu1ASCII);
                    pu1Temp += STRLEN (pu1Temp);
                }
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
                pu1ASCII = NULL;
            }
            else
            {
#ifdef UNWANTED_CODE

                /* Already allocation and moving to data ptr are done */

                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: unable to allocate"
                                 "buffer\n");
                    return OSIX_FAILURE;
                }
                if (*ppu1Output == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCliShowPeerCert: Failed to allocate memory"
                                 "using MemAllocateMemBlock \r\n");
                    if (pu1ASCII != NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1ASCII);
                        pu1ASCII = NULL;
                    }
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1CertName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pIkeCliConfigParams->
                                        CliIkeImportPeerCert.pu1EngineName);
                    return;
                }
                CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
                pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
#endif

                SPRINTF ((char *) pu1Temp,
                         "Failed in converting cert to ASCII\n");
                if (pu1ASCII != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1ASCII);
                    pu1ASCII = NULL;
                }

            }
        }
        else
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliShowPeerCert: unable to allocate"
                             "buffer\n");
                return;
            }
            if (*ppu1Output == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCliShowPeerCert: Failed to allocate memory"
                             "using MemAllocateMemBlock \r\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1CertName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportPeerCert.pu1EngineName);
                return;
            }

            CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
            pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
            SPRINTF ((char *) pu1Temp,
                     "%% No entry with this certificate name\n");
        }
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                            pu1CertName);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportPeerCert.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliShowCaCert                                    */
/*  Description     : This function displays ca certificates              */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliShowCaCert (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL, *pu1ASCII = NULL;
    tIkeCertDB         *pIkeCert = NULL;
    UINT4               u4ASCIILen = IKE_ZERO;
    UINT4               u4Count = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportCaCert.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportCaCert.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowPeerCert: unable to allocate" "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);

            if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\n",
                 pIkeCliConfigParams->CliIkeImportCaCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1EngineName);

        if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
        }
        return;
    }

    u4Count = SLL_COUNT (&(pIkeEngine->CACerts));
    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);

            if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pIkeCliConfigParams->
                                    CliIkeImportCaCert.pu1CertName);
            }
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "No entries in table\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1EngineName);

        if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
        }
        return;
    }

    if (pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName != NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1CertName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        TAKE_IKE_SEM ();
        SLL_SCAN (&(pIkeEngine->CACerts), pIkeCert, tIkeCertDB *)
        {
            if (IKE_MEMCMP (pIkeCert->au1CertId,
                            pIkeCliConfigParams->CliIkeImportCaCert.pu1CertName,
                            IKE_STRLEN (pIkeCliConfigParams->CliIkeImportCaCert.
                                        pu1CertName)) == IKE_ZERO)
            {
                if (IkeConvertX509ToASCII
                    (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
                {
                    SPRINTF ((char *) pu1Temp, "\tCert ID:  %s \n",
                             pIkeCert->au1CertId);
                    pu1Temp += STRLEN (pu1Temp);

                    /* The Certificate details are not supposed to be 
                     * displayed in FIPS mode */
                    if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                    {
                        SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1ASCII);
                    }
                    pu1ASCII = NULL;
                    break;
                }
                if (pu1ASCII != NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pu1ASCII);
                    pu1ASCII = NULL;
                }
            }
        }
        GIVE_IKE_SEM ();
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                            pu1CertName);
    }
    else
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliShowCaCert: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeImportCaCert.pu1EngineName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        TAKE_IKE_SEM ();
        SLL_SCAN (&(pIkeEngine->CACerts), pIkeCert, tIkeCertDB *)
        {
            if (IkeConvertX509ToASCII
                (pIkeCert->pCert, &pu1ASCII, &u4ASCIILen) == IKE_SUCCESS)
            {
                SPRINTF ((char *) pu1Temp, "\tCert ID:  %s \n",
                         pIkeCert->au1CertId);
                pu1Temp += STRLEN (pu1Temp);
                /* The Certificate details are not supposed to be 
                 * displayed in FIPS mode */
                if (FipsGetFipsCurrOperMode () != FIPS_MODE)
                {
                    SPRINTF ((char *) pu1Temp, "%s \n", pu1ASCII);
                    pu1Temp += STRLEN (pu1Temp);
                }
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
                pu1ASCII = NULL;
            }
            if (pu1ASCII != NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1ASCII);
                pu1ASCII = NULL;
            }

        }
        GIVE_IKE_SEM ();
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportCaCert.
                        pu1EngineName);

    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliShowCertMap                                   */
/*  Description     : This function maps certificate to a peer or makes as*/
/*                    default                                             */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliShowCertMap (tIkeCliConfigParams * pIkeCliConfigParams,
                   UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    tIkeCertMapToPeer  *pIkeCertMap = NULL;
    UINT4               u4Count = IKE_ZERO;
    const INT1         *pi1Temp = NULL;
    BOOLEAN             i1Default = FALSE;
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeDsaKeyInfo     *pIkeDsa = NULL;

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeMapCertToPeer.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeMapCertToPeer.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        IKE_FORMAT_ERROR_MESSAGE ("Engine does not exist \n", ppu1Output);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        return;
    }
    u4Count = SLL_COUNT (&(pIkeEngine->IkeCertMapList));

    if ((pIkeEngine->DefaultCert.pCAName != NULL) &&
        (pIkeEngine->DefaultCert.pCASerialNum != NULL))
    {
        i1Default = TRUE;
    }

    if (u4Count == IKE_ZERO)
    {
        if (i1Default == FALSE)
        {
            IKE_FORMAT_ERROR_MESSAGE ("No entries in table\n", ppu1Output);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->
                                CliIkeMapCertToPeer.pu1EngineName);
            return;
        }
    }

    if (i1Default == TRUE)
    {
        u4Count += IKE_ONE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowCerts: unable to allocate " "buffer\n");
        return;
    }
    if (*ppu1Output == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCliShowCaCert: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                            pu1EngineName);
        return;
    }

    CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
    pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

    TMO_SLL_Scan (&(pIkeEngine->IkeCertMapList), pIkeCertMap,
                  tIkeCertMapToPeer *)
    {
        TMO_SLL_Scan (&(pIkeEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
        {
            if ((pIkeCertMap->MyCert.pCAName != NULL) &&
                (pIkeRsa->CertInfo.pCAName != NULL) &&
                (IkeX509NameCmp
                 (pIkeCertMap->MyCert.pCAName, pIkeRsa->CertInfo.pCAName)
                 == IKE_ZERO)
                &&
                (pIkeCertMap->MyCert.pCASerialNum != NULL) &&
                (pIkeRsa->CertInfo.pCASerialNum != NULL) &&
                (IkeASN1IntegerCmp
                 (pIkeCertMap->MyCert.pCASerialNum,
                  pIkeRsa->CertInfo.pCASerialNum) == IKE_ZERO))
            {
                break;
            }
        }

        if (pIkeRsa != NULL)
        {
            SPRINTF ((char *) pu1Temp, "\t RSA KeyID %s ",
                     pIkeCertMap->au1KeyName);
            pu1Temp += STRLEN (pu1Temp);
        }
        else
        {
            TMO_SLL_Scan (&(pIkeEngine->IkeDsaKeyList), pIkeDsa,
                          tIkeDsaKeyInfo *)
            {
                if ((pIkeCertMap->MyCert.pCAName != NULL) &&
                    (pIkeDsa->CertInfo.pCAName != NULL) &&
                    (IkeX509NameCmp
                     (pIkeCertMap->MyCert.pCAName, pIkeDsa->CertInfo.pCAName)
                     == IKE_ZERO)
                    &&
                    (pIkeCertMap->MyCert.pCASerialNum != NULL) &&
                    (pIkeDsa->CertInfo.pCASerialNum != NULL) &&
                    (IkeASN1IntegerCmp
                     (pIkeCertMap->MyCert.pCASerialNum,
                      pIkeDsa->CertInfo.pCASerialNum) == IKE_ZERO))
                {
                    break;
                }
            }

            if (pIkeDsa != NULL)
            {
                SPRINTF ((char *) pu1Temp, "\t DSA KeyID %s ",
                         pIkeCertMap->au1KeyName);
                pu1Temp += STRLEN (pu1Temp);
            }
        }

        switch (pIkeCertMap->PeerID.i4IdType)
        {
            case IKE_KEY_IPV4:
            {
                UINT1               au1KeyId[INET_ADDRSTRLEN] = { IKE_ZERO };
                tIp4Addr            u4Ip4Addr = IKE_ZERO;

                SPRINTF ((char *) pu1Temp, "\t PeerID type: (ipv4)");
                pu1Temp += STRLEN (pu1Temp);

                u4Ip4Addr = IKE_HTONL (pIkeCertMap->PeerID.uID.Ip4Addr);

                pi1Temp = (const INT1 *) INET_NTOP4 (&u4Ip4Addr, au1KeyId);

                if (pi1Temp != NULL)
                {
                    SPRINTF ((char *) pu1Temp, "\t PeerID: %s \n", au1KeyId);
                    pu1Temp += STRLEN (pu1Temp);
                }
                break;
            }

            case IKE_KEY_IPV6:
            {
                UINT1               au1KeyId[INET6_ADDRSTRLEN] = { IKE_ZERO };
                SPRINTF ((char *) pu1Temp, "\t PeerID type: (ipv6)");
                pu1Temp += STRLEN (pu1Temp);

                pi1Temp =
                    (const INT1 *) INET_NTOP6 (&pIkeCertMap->PeerID.uID.Ip6Addr.
                                               u1_addr, au1KeyId);

                if (pi1Temp != NULL)
                {
                    SPRINTF ((char *) pu1Temp, "\t PeerID: %s \n", au1KeyId);
                    pu1Temp += STRLEN (pu1Temp);
                }
                break;
            }

            case IKE_KEY_EMAIL:
            {
                SPRINTF ((char *) pu1Temp, "\t PeerID type: (email)");
                pu1Temp += STRLEN (pu1Temp);
                SPRINTF ((char *) pu1Temp, "\t PeerID: %s \n",
                         pIkeCertMap->PeerID.uID.au1Email);
                pu1Temp += STRLEN (pu1Temp);
                break;
            }

            case IKE_KEY_FQDN:
            {
                SPRINTF ((char *) pu1Temp, "\t PeerID type: (fqdn)");
                pu1Temp += STRLEN (pu1Temp);
                SPRINTF ((char *) pu1Temp, "\t PeerID: %s \n",
                         pIkeCertMap->PeerID.uID.au1Fqdn);
                pu1Temp += STRLEN (pu1Temp);
                break;
            }

            case IKE_KEY_DN:
            {
                SPRINTF ((char *) pu1Temp, "\t PeerID type: (dn)");
                pu1Temp += STRLEN (pu1Temp);
                SPRINTF ((char *) pu1Temp, "\t PeerID: %s \n",
                         pIkeCertMap->PeerID.uID.au1Dn);
                pu1Temp += STRLEN (pu1Temp);
                break;
            }
            default:
                break;
        }
    }
    if (i1Default == TRUE)
    {
        TMO_SLL_Scan (&(pIkeEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
        {
            if ((IkeX509NameCmp
                 (pIkeEngine->DefaultCert.pCAName, pIkeRsa->CertInfo.pCAName)
                 == IKE_ZERO)
                &&
                (IkeASN1IntegerCmp
                 (pIkeEngine->DefaultCert.pCASerialNum,
                  pIkeRsa->CertInfo.pCASerialNum) == IKE_ZERO))
            {
                break;
            }
        }
        if (pIkeRsa != NULL)
        {
            SPRINTF ((char *) pu1Temp, "Default Certificate \n");
            pu1Temp += STRLEN (pu1Temp);

            SPRINTF ((char *) pu1Temp, "\t RSA KeyID %s ",
                     pIkeRsa->au1RsaKeyName);
            pu1Temp += STRLEN (pu1Temp);

            SPRINTF ((char *) pu1Temp, "\t PeerID : Default \n");
            pu1Temp += STRLEN (pu1Temp);
        }
        else
        {
            TMO_SLL_Scan (&(pIkeEngine->IkeDsaKeyList), pIkeDsa,
                          tIkeDsaKeyInfo *)
            {
                if ((IkeX509NameCmp
                     (pIkeEngine->DefaultCert.pCAName,
                      pIkeDsa->CertInfo.pCAName) == IKE_ZERO) &&
                    (IkeASN1IntegerCmp
                     (pIkeEngine->DefaultCert.pCASerialNum,
                      pIkeDsa->CertInfo.pCASerialNum) == IKE_ZERO))
                {
                    break;
                }
            }
            if (pIkeDsa != NULL)
            {
                SPRINTF ((char *) pu1Temp, "Default Certificate \n");
                pu1Temp += STRLEN (pu1Temp);

                SPRINTF ((char *) pu1Temp, "\t DSA KeyID %s ",
                         pIkeDsa->au1DsaKeyName);
                pu1Temp += STRLEN (pu1Temp);

                SPRINTF ((char *) pu1Temp, "\t PeerID : Default \n");
                pu1Temp += STRLEN (pu1Temp);
            }
        }
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeMapCertToPeer.
                        pu1EngineName);
    return;
}

/**************************************************************************/
/*  Function Name   : IkeCliImportKey                                     */
/*  Description     : This function Imports an rsa/dsa key pair           */
/*  Input(s)        : pIkeCliConfigParams - A union of structures which   */
/*                  : contains the parameters required for the CLI cmd    */
/*  Output(s)       : ppu1Output - This contains any reply message to the */
/*                  : user                                                */
/*  Returns         : None                                                */
/**************************************************************************/

VOID
IkeCliImportKey (tIkeCliConfigParams * pIkeCliConfigParams, UINT1 **ppu1Output)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4ErrorCode = IKE_ZERO;
    INT4                i4RetVal = IKE_FAILURE;

    u4Count = SLL_COUNT (&(gIkeEngineList));

    if (u4Count == IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1FileName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "Table has no entries : \r\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1FileName);
        return;
    }

    pIkeEngine = IkeSnmpGetEngine (pIkeCliConfigParams->CliIkeImportKey.
                                   pu1EngineName,
                                   (INT4) STRLEN (pIkeCliConfigParams->
                                                  CliIkeImportKey.
                                                  pu1EngineName));
    if (pIkeEngine == NULL)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1FileName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);
        SPRINTF ((char *) pu1Temp, "%% Engine does not exist : %s\r\n",
                 pIkeCliConfigParams->CliIkeImportKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                            pu1FileName);
        return;
    }

    if (pIkeCliConfigParams->CliIkeImportKey.u4Type == IKE_RSA_SIGNATURE)
    {
        i4RetVal = IkeImportRsaKey (pIkeCliConfigParams->
                                    CliIkeImportKey.pu1EngineName,
                                    pIkeCliConfigParams->
                                    CliIkeImportKey.pu1KeyName,
                                    pIkeCliConfigParams->
                                    CliIkeImportKey.pu1FileName, &u4ErrorCode);
    }
    else if (pIkeCliConfigParams->CliIkeImportKey.u4Type == IKE_DSS_SIGNATURE)
    {
        i4RetVal = IkeImportDsaKey (pIkeCliConfigParams->
                                    CliIkeImportKey.pu1EngineName,
                                    pIkeCliConfigParams->
                                    CliIkeImportKey.pu1KeyName,
                                    pIkeCliConfigParams->
                                    CliIkeImportKey.pu1FileName, &u4ErrorCode);
    }
    if (i4RetVal == IKE_FAILURE)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppu1Output)) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: unable to allocate " "buffer\n");
            return;
        }
        if (*ppu1Output == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCliImportKey: Failed to allocate memory"
                         "using MemAllocateMemBlock \r\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                                pu1FileName);
            return;
        }

        CLI_SET_RESP_STATUS (*ppu1Output, TRUE);
        pu1Temp = CLI_GET_DATA_PTR (*ppu1Output);

        switch (u4ErrorCode)
        {
            case IKE_ERR_FILE_NOT_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% File does not exist \r\n");
                break;
            }
            case IKE_ERR_GEN_ERR:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Error while operating on files \r\n");
                break;
            }
            case IKE_ERR_KEY_ID_ALREADY_EXIST:
            {
                SPRINTF ((char *) pu1Temp, "%% Duplicate keyid \r\n");
                break;
            }
            case IKE_ERR_INVALID_FORMAT:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Error which retrieving the key \r\n");
                break;
            }
            case IKE_ERR_ADD_TO_DB_FAILED:
            {
                SPRINTF ((char *) pu1Temp,
                         "%% Adding to Key Database Failed \r\n");
                break;
            }
            default:
            {
                SPRINTF ((char *) pu1Temp, "%% Importing Key Failed \r\n");
                break;
            }
        }
    }
    else
    {
        IKE_NO_RESPONSE_MESSAGE (ppu1Output);
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                        pu1EngineName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                        pu1KeyName);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) pIkeCliConfigParams->CliIkeImportKey.
                        pu1FileName);
    return;
}

#endif /*IKE_WANTED */
