/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsikelow.c,v 1.18 2014/01/25 13:54:11 siva Exp $
 *
 * Description: This has the SNMP Low level functions                 
 *
 ***********************************************************************/

# include  "include.h"
# include  "fsikelow.h"
# include  "fsikecon.h"
# include  "fsikeogi.h"
# include  "midconst.h"

#include "fsike.h"
#include "ikegen.h"
#include "ikestat.h"
#include "sec.h"
#include "ikememmac.h"

/* Global VPN Policy Table */
extern t_SLL        gIkeVpnPolicyList;

/* Global Engine Table */
extern UINT4        gu4VpnRestoredConfig;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsikeGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsikeGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsikeGlobalDebug (INT4 *pi4RetValFsikeGlobalDebug)
{
    *pi4RetValFsikeGlobalDebug = (INT4) gu4IkeTrace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsikeRAXAuthMode
 Input       :  The Indices

                The Object 
                retValFsikeRAXAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsikeRAXAuthMode (INT4 *pi4RetValFsikeRAXAuthMode)
{
    *pi4RetValFsikeRAXAuthMode = gbXAuthServer;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsikeRACMMode
 Input       :  The Indices

                The Object 
                retValFsikeRACMMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsikeRACMMode (INT4 *pi4RetValFsikeRACMMode)
{
    *pi4RetValFsikeRACMMode = gbCMServer;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsikeGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsikeGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsikeGlobalDebug (INT4 i4SetValFsikeGlobalDebug)
{
    gu4IkeTrace = (UINT4) i4SetValFsikeGlobalDebug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsikeRAXAuthMode
 Input       :  The Indices

                The Object 
                setValFsikeRAXAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsikeRAXAuthMode (INT4 i4SetValFsikeRAXAuthMode)
{
    gbXAuthServer = (BOOLEAN) i4SetValFsikeRAXAuthMode;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsikeRACMMode
 Input       :  The Indices

                The Object 
                setValFsikeRACMMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsikeRACMMode (INT4 i4SetValFsikeRACMMode)
{
    gbCMServer = (BOOLEAN) i4SetValFsikeRACMMode;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsikeGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsikeGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsikeGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsikeGlobalDebug)
{
    if ((i4TestValFsikeGlobalDebug < IKE_DISABLE)
        || (i4TestValFsikeGlobalDebug > IKE_BUFFER))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsikeRAXAuthMode
 Input       :  The Indices

                The Object 
                testValFsikeRAXAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsikeRAXAuthMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsikeRAXAuthMode)
{

    if ((i4TestValFsikeRAXAuthMode != IKE_ZERO) &&
        (i4TestValFsikeRAXAuthMode != IKE_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsikeRACMMode
 Input       :  The Indices

                The Object 
                testValFsikeRACMMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsikeRACMMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsikeRACMMode)
{
    if ((i4TestValFsikeRACMMode != IKE_ZERO) &&
        (i4TestValFsikeRACMMode != IKE_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsIkeEngineTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeEngineTable
 Input       :  The Indices
                FsIkeEngineName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkeEngineTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeEngineTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsIkeEngineName)
#else
INT1
nmhValidateIndexInstanceFsIkeEngineTable (pFsIkeEngineName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeEngineTable
 Input       :  The Indices
                FsIkeEngineName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkeEngineTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeEngineTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName)
#else
INT1
nmhGetFirstIndexFsIkeEngineTable (pFsIkeEngineName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
#endif
{
    tIkeEngine         *pEngine = NULL;

    pEngine = (tIkeEngine *) SLL_FIRST (&gIkeEngineList);

    if (pEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pFsIkeEngineName->i4_Length = (INT4) STRLEN (pEngine->au1EngineName);
    MEMCPY (pFsIkeEngineName->pu1_OctetList, pEngine->au1EngineName,
            pFsIkeEngineName->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeEngineTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkeEngineTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeEngineTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsIkeEngineName)
#else
INT1
nmhGetNextIndexFsIkeEngineTable (pFsIkeEngineName, pNextFsIkeEngineName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeEngineName;
#endif
{
    tIkeEngine         *pEngine = NULL;
    tIkeEngine         *pNextEngine = NULL;

    SLL_SCAN (&gIkeEngineList, pEngine, tIkeEngine *)
    {
        if (MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            pNextEngine =
                (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                         (t_SLL_NODE *) pEngine);
            if (pNextEngine == NULL)
            {
                return (SNMP_FAILURE);
            }

            pNextFsIkeEngineName->i4_Length =
                (INT4) STRLEN (pNextEngine->au1EngineName);
            MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                    pNextEngine->au1EngineName,
                    pNextFsIkeEngineName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeEngineTunnelTermAddrType
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                retValFsIkeEngineTunnelTermAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeEngineTunnelTermAddrType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeEngineTunnelTermAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                     INT4
                                     *pi4RetValFsIkeEngineTunnelTermAddrType)
#else
INT1
nmhGetFsIkeEngineTunnelTermAddrType (pFsIkeEngineName,
                                     pi4RetValFsIkeEngineTunnelTermAddrType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     INT4               *pi4RetValFsIkeEngineTunnelTermAddrType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        if (*pi4RetValFsIkeEngineTunnelTermAddrType == IPV6ADDR)
        {
            *pi4RetValFsIkeEngineTunnelTermAddrType =
                (INT4) pIkeEngine->Ip6TunnelTermAddr.u4AddrType;
        }
        else
        {
            *pi4RetValFsIkeEngineTunnelTermAddrType =
                (INT4) pIkeEngine->Ip4TunnelTermAddr.u4AddrType;
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeEngineTunnelTermAddr
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                retValFsIkeEngineTunnelTermAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeEngineTunnelTermAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeEngineTunnelTermAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsIkeEngineTunnelTermAddr)
#else
INT1
nmhGetFsIkeEngineTunnelTermAddr (pFsIkeEngineName,
                                 pRetValFsIkeEngineTunnelTermAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeEngineTunnelTermAddr;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    const INT1         *pTemp = NULL;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        if (IPV4ADDR == pIkeEngine->Ip4TunnelTermAddr.u4AddrType)
        {
            pRetValFsIkeEngineTunnelTermAddr->i4_Length = INET_ADDRSTRLEN;
            /* Make sure that the converted address is stored into 
               pRetValFsIkeEngineTunnelTermAddr */
            /* pRetValFsIkeEngineTunnelTermAddr->pu1_OctetList =  */
            u4TempIp4Addr =
                IKE_HTONL (pIkeEngine->Ip4TunnelTermAddr.uIpAddr.Ip4Addr);
            pTemp = (const INT1 *) INET_NTOP4 (&u4TempIp4Addr,
                                               pRetValFsIkeEngineTunnelTermAddr->
                                               pu1_OctetList);
            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }

            return (SNMP_SUCCESS);
        }

        if (IPV6ADDR == pIkeEngine->Ip6TunnelTermAddr.u4AddrType)
        {
            pRetValFsIkeEngineTunnelTermAddr->i4_Length = INET6_ADDRSTRLEN;
            /* Make sure that the converted address is stored into 
               pRetValFsIkeEngineTunnelTermAddr */
            /* pRetValFsIkeEngineTunnelTermAddr->pu1_OctetList =  */
            /* Since we are storing and using in u1[16] format, no
             * problem of Network or Host Byte order
             */
            pTemp = (const INT1 *)
                INET_NTOP6 (&pIkeEngine->Ip6TunnelTermAddr.uIpAddr.Ip6Addr.
                            u1_addr,
                            pRetValFsIkeEngineTunnelTermAddr->pu1_OctetList);

            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }

            return (SNMP_SUCCESS);
        }

        /* Something other that IPv4 or IPv6 Address, return failure */
        return (SNMP_FAILURE);
    }
    else
    {
        /* Entry not found */
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeEngineRowStatus
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                retValFsIkeEngineRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeEngineRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeEngineRowStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            INT4 *pi4RetValFsIkeEngineRowStatus)
#else
INT1
nmhGetFsIkeEngineRowStatus (pFsIkeEngineName, pi4RetValFsIkeEngineRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     INT4               *pi4RetValFsIkeEngineRowStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        *pi4RetValFsIkeEngineRowStatus = (INT4) pIkeEngine->u1Status;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeEngineTunnelTermAddr
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                setValFsIkeEngineTunnelTermAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeEngineTunnelTermAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeEngineTunnelTermAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsIkeEngineTunnelTermAddr)
#else
INT1
nmhSetFsIkeEngineTunnelTermAddr (pFsIkeEngineName,
                                 pSetValFsIkeEngineTunnelTermAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeEngineTunnelTermAddr;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;
    tIkeConfigIfParam   IkeConfigIfParam;
    tIkeIpAddr         *pTunnelTermAddr = NULL;

    pSetValFsIkeEngineTunnelTermAddr->
        pu1_OctetList[pSetValFsIkeEngineTunnelTermAddr->i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pSetValFsIkeEngineTunnelTermAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return (SNMP_FAILURE);
    }

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        IkeConfigIfParam.u4MsgType = IKE_CONFIG_CHG_TUNNEL_TERM_ADDR;
        IkeConfigIfParam.ConfigReq.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;

        if (pIkeEngine->u1Status != IKE_ACTIVE)
        {
            /* Engine not in use, or Tunnel Term Addr being set for the 
               first time, no need to send to IKE task
             */
            if (u1AddrType == IPV6ADDR)
            {
                pTunnelTermAddr = &pIkeEngine->Ip6TunnelTermAddr;
            }
            else
            {
                pTunnelTermAddr = &pIkeEngine->Ip4TunnelTermAddr;
            }
        }
        else
        {
            pTunnelTermAddr =
                &IkeConfigIfParam.ConfigReq.IkeConfEngineTunnTermAddr.
                LocTunnTermAddr;
        }

        pTunnelTermAddr->u4AddrType = u1AddrType;
        if (IPV4ADDR == u1AddrType)
        {
            i4RetVal =
                INET_PTON4 (pSetValFsIkeEngineTunnelTermAddr->pu1_OctetList,
                            &u4TempIp4Addr);
            pTunnelTermAddr->uIpAddr.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
            if (i4RetVal > IKE_ZERO)
            {
                if (pIkeEngine->u1Status != IKE_ACTIVE)
                {
                    /* 
                     * Since the Tunnel Termination address has been set, the
                     * engine is now ready to be activated
                     */
                    pIkeEngine->u1Status = IKE_NOTINSERVICE;
                }
                else
                {
                    /* If the Tunnel Termination address is changed, then post
                     * an event to IKE task to update this change */
                    if (IKE_MEMCMP (&pTunnelTermAddr->uIpAddr.Ip4Addr,
                                    &pIkeEngine->Ip4TunnelTermAddr.uIpAddr.
                                    Ip4Addr, sizeof (tIp4Addr)) != IKE_ZERO)
                    {
                        /* Send Message to IKE Task */
                        IkeSendConfMsgToIke (&IkeConfigIfParam);
                    }
                }
                return (SNMP_SUCCESS);
            }
        }

        if (IPV6ADDR == u1AddrType)
        {
            i4RetVal =
                INET_PTON6 (pSetValFsIkeEngineTunnelTermAddr->pu1_OctetList,
                            &pTunnelTermAddr->uIpAddr.Ip6Addr.u1_addr);
            if (i4RetVal > IKE_ZERO)
            {
                if (pIkeEngine->u1Status != IKE_ACTIVE)
                {
                    /* 
                     * Since the Tunnel Termination address has been set, the
                     * engine is now ready to be activated
                     */
                    pIkeEngine->u1Status = IKE_NOTINSERVICE;
                }
                else
                {
                    /* If the Tunnel Termination address is changed, then post
                     * an event to IKE task to update this change */
                    if (IKE_MEMCMP (&pTunnelTermAddr->uIpAddr.Ip6Addr,
                                    &pIkeEngine->Ip6TunnelTermAddr.uIpAddr.
                                    Ip6Addr, sizeof (tIp6Addr)) != IKE_ZERO)
                    {
                        /* Send Message to IKE Task */
                        IkeSendConfMsgToIke (&IkeConfigIfParam);
                    }
                }
                return (SNMP_SUCCESS);
            }
        }
        /* Not v4 or v6 address */
        return (SNMP_FAILURE);
    }
    else
    {
        /* Entry not found */
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeEngineRowStatus
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                setValFsIkeEngineRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeEngineRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeEngineRowStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            INT4 i4SetValFsIkeEngineRowStatus)
#else
INT1
nmhSetFsIkeEngineRowStatus (pFsIkeEngineName, i4SetValFsIkeEngineRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     INT4                i4SetValFsIkeEngineRowStatus;

#endif
{
    tIkeConfigIfParam   IkeConfigIfParam;

    switch (i4SetValFsIkeEngineRowStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeEngine         *pIkeEngine = NULL;
#if 0
            INT1                i1Count;
#endif

            pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                           pFsIkeEngineName->i4_Length);
            if (pIkeEngine != NULL)
            {
                return (SNMP_FAILURE);
            }
#if IKE_ZERO
            /* Send Msg to IKE to create the engine */
            IkeConfigIfParam.u4MsgType = IKE_CONFIG_NEW_ENGINE;
            IKE_MEMCPY
                (IkeConfigIfParam.ConfigReq.IkeConfCreateEngine.au1EngineName,
                 pFsIkeEngineName->pu1_OctetList, pFsIkeEngineName->i4_Length);
            IkeConfigIfParam.ConfigReq.IkeConfCreateEngine.
                au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            IkeSendConfMsgToIke (&IkeConfigIfParam);

            /* Delay the task so that IKE task can create the engine */
            for (i1Count = IKE_ZERO; i1Count < IKE_THREE; i1Count++)
            {
                OsixDelayTask (IKE_ONE);
                if (IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                      pFsIkeEngineName->i4_Length) != NULL)
                {
                    return (SNMP_SUCCESS);
                }
            }
            return (SNMP_FAILURE);
#endif

            if (MemAllocateMemBlock
                (IKE_ENGINE_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeEngine) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeEngineRowStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }
            if (!pIkeEngine)
                return (SNMP_FAILURE);
            MEMSET (pIkeEngine, IKE_ZERO, sizeof (tIkeEngine));
            MEMCPY (pIkeEngine->au1EngineName, pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkeEngine->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkeEngine->u1Status = IKE_NOTREADY;
            IkeInitEngine (pIkeEngine);
            SLL_ADD (&gIkeEngineList, (t_SLL_NODE *) pIkeEngine);
            return (SNMP_SUCCESS);

        }

        case IKE_ACTIVE:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                           pFsIkeEngineName->i4_Length);
            if (pIkeEngine != NULL)
            {
                pIkeEngine->u1Status = IKE_ACTIVE;
                return (SNMP_SUCCESS);
            }
            else
                return (SNMP_FAILURE);
        }

        case IKE_NOTINSERVICE:
        {
            tIkeEngine         *pIkeEngine = NULL;

            pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                           pFsIkeEngineName->i4_Length);

            if (pIkeEngine == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeEngine->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeEngine->u1Status)
            {
                pIkeEngine->u1Status = IKE_NOTINSERVICE;
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_ENGINE;
                IkeConfigIfParam.ConfigReq.u4IkeEngineIndex =
                    pIkeEngine->u4IkeIndex;
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                return (SNMP_SUCCESS);
            }
            else
                return (SNMP_FAILURE);
        }

        case IKE_DESTROY:
        {
            tIkeEngine         *pIkeEngine = NULL;
            INT1                i1Count = IKE_ZERO;

            pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                           pFsIkeEngineName->i4_Length);
            if (pIkeEngine != NULL)
            {
                /* We will only mark this engine to be destroyed, 
                   and send an event to IKE to actually delete the structure
                 */
                pIkeEngine->u1Status = IKE_DESTROY;
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_ENGINE;
                IkeConfigIfParam.ConfigReq.u4IkeEngineIndex =
                    pIkeEngine->u4IkeIndex;
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                for (i1Count = IKE_ZERO; i1Count < IKE_THREE; i1Count++)
                {
                    OsixDelayTask (IKE_ONE);
                    if (IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                          pFsIkeEngineName->i4_Length) == NULL)
                    {
                        return (SNMP_SUCCESS);
                    }
                }

                /*
                   SLL_DELETE (&gIkeEngineList, (t_SLL_NODE *) pIkeEngine);
                   IkeDeleteEngine ((t_SLL_NODE *) pIkeEngine);
                 */
                return (SNMP_FAILURE);
            }
            else
                return (SNMP_FAILURE);
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeEngineTunnelTermAddr
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                testValFsIkeEngineTunnelTermAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeEngineTunnelTermAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeEngineTunnelTermAddr (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsIkeEngineTunnelTermAddr)
#else
INT1
nmhTestv2FsIkeEngineTunnelTermAddr (pu4ErrorCode, pFsIkeEngineName,
                                    pTestValFsIkeEngineTunnelTermAddr)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeEngineTunnelTermAddr;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    UINT4               u4Ip4Addr;
    /* Not checking Addr validity here as it is being checked in Set routine 
     * anyways 
     */

    pTestValFsIkeEngineTunnelTermAddr->
        pu1_OctetList[pTestValFsIkeEngineTunnelTermAddr->i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pTestValFsIkeEngineTunnelTermAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (u1AddrType == IPV4ADDR)
    {
        INET_PTON4 (pTestValFsIkeEngineTunnelTermAddr->pu1_OctetList,
                    &u4Ip4Addr);
        if (NetIpv4IfIsOurAddress (OSIX_NTOHL (u4Ip4Addr)) == NETIPV4_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    pIkeEngine =
        IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                          pFsIkeEngineName->i4_Length);
    if (pIkeEngine != NULL)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeEngineRowStatus
 Input       :  The Indices
                FsIkeEngineName

                The Object 
                testValFsIkeEngineRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeEngineRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeEngineRowStatus (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                               INT4 i4TestValFsIkeEngineRowStatus)
#else
INT1
nmhTestv2FsIkeEngineRowStatus (pu4ErrorCode, pFsIkeEngineName,
                               i4TestValFsIkeEngineRowStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     INT4                i4TestValFsIkeEngineRowStatus;
#endif
{

    if (i4TestValFsIkeEngineRowStatus == IKE_NOTREADY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsIkeEngineRowStatus == IKE_CREATEANDGO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIkeEngineRowStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeEngineRowStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeEngineTable (pFsIkeEngineName) ==
        SNMP_SUCCESS)
    {
        if (i4TestValFsIkeEngineRowStatus == IKE_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        else
            return (SNMP_SUCCESS);
    }
    else
    {
        if (i4TestValFsIkeEngineRowStatus != IKE_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        else
            return (SNMP_SUCCESS);
    }
}

/* LOW LEVEL Routines for Table : FsIkePolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkePolicyTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkePolicyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkePolicyTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsIkeEngineName,
                                          UINT4 u4FsIkePolicyPriority)
#else
INT1
nmhValidateIndexInstanceFsIkePolicyTable (pFsIkeEngineName,
                                          u4FsIkePolicyPriority)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
        if (pIkePolicy != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkePolicyTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkePolicyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkePolicyTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                  UINT4 *pu4FsIkePolicyPriority)
#else
INT1
nmhGetFirstIndexFsIkePolicyTable (pFsIkeEngineName, pu4FsIkePolicyPriority)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4              *pu4FsIkePolicyPriority;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkePolicy = (tIkePolicy *) SLL_FIRST (&pIkeEngine->IkePolicyList);
    if (pIkePolicy == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length = (INT4) STRLEN (pIkePolicy->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList, pIkePolicy->au1EngineName,
                pFsIkeEngineName->i4_Length);
    *pu4FsIkePolicyPriority = pIkePolicy->u4Priority;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkePolicyTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkePolicyPriority
                nextFsIkePolicyPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkePolicyTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkePolicyTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsIkeEngineName,
                                 UINT4 u4FsIkePolicyPriority,
                                 UINT4 *pu4NextFsIkePolicyPriority)
#else
INT1
nmhGetNextIndexFsIkePolicyTable (pFsIkeEngineName, pNextFsIkeEngineName,
                                 u4FsIkePolicyPriority,
                                 pu4NextFsIkePolicyPriority)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4              *pu4NextFsIkePolicyPriority;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;
    tIkePolicy         *pNextIkePolicy = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkePolicyList, pIkePolicy, tIkePolicy *)
    {
        if (pIkePolicy->u4Priority == u4FsIkePolicyPriority)
        {
            pNextIkePolicy =
                (tIkePolicy *) SLL_NEXT (&pIkeEngine->IkePolicyList,
                                         (t_SLL_NODE *) pIkePolicy);
            if (pNextIkePolicy == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Policy of this new engine */
                pNextIkePolicy =
                    (tIkePolicy *) SLL_FIRST (&pNextIkeEngine->IkePolicyList);
                if (pNextIkePolicy == NULL)
                {
                    return (SNMP_FAILURE);
                }

            }

            pNextFsIkeEngineName->i4_Length = (INT4) STRLEN (pNextIkePolicy->
                                                             au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkePolicy->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            *pu4NextFsIkePolicyPriority = pNextIkePolicy->u4Priority;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkePolicyAuthMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyAuthMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyAuthMethod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyAuthMethod (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             UINT4 u4FsIkePolicyPriority,
                             INT4 *pi4RetValFsIkePolicyAuthMethod)
#else
INT1
nmhGetFsIkePolicyAuthMethod (pFsIkeEngineName, u4FsIkePolicyPriority,
                             pi4RetValFsIkePolicyAuthMethod)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyAuthMethod;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyAuthMethod = (INT4) pIkePolicy->u2AuthMethod;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyHashAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           UINT4 u4FsIkePolicyPriority,
                           INT4 *pi4RetValFsIkePolicyHashAlgo)
#else
INT1
nmhGetFsIkePolicyHashAlgo (pFsIkeEngineName, u4FsIkePolicyPriority,
                           pi4RetValFsIkePolicyHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyHashAlgo;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyHashAlgo = (INT4) pIkePolicy->u1HashAlgo;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyEncryptionAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyEncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 UINT4 u4FsIkePolicyPriority,
                                 INT4 *pi4RetValFsIkePolicyEncryptionAlgo)
#else
INT1
nmhGetFsIkePolicyEncryptionAlgo (pFsIkeEngineName, u4FsIkePolicyPriority,
                                 pi4RetValFsIkePolicyEncryptionAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyEncryptionAlgo;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyEncryptionAlgo =
            (INT4) pIkePolicy->u1EncryptionAlgo;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyDHGroup
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyDHGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyDHGroup ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyDHGroup (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          UINT4 u4FsIkePolicyPriority,
                          INT4 *pi4RetValFsIkePolicyDHGroup)
#else
INT1
nmhGetFsIkePolicyDHGroup (pFsIkeEngineName, u4FsIkePolicyPriority,
                          pi4RetValFsIkePolicyDHGroup)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyDHGroup;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyDHGroup = (INT4) pIkePolicy->u1DHGroup;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyLifeTimeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyLifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                               UINT4 u4FsIkePolicyPriority,
                               INT4 *pi4RetValFsIkePolicyLifeTimeType)
#else
INT1
nmhGetFsIkePolicyLifeTimeType (pFsIkeEngineName, u4FsIkePolicyPriority,
                               pi4RetValFsIkePolicyLifeTimeType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyLifeTimeType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyLifeTimeType = (INT4) pIkePolicy->u1LifeTimeType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyLifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           UINT4 u4FsIkePolicyPriority,
                           UINT4 *pu4RetValFsIkePolicyLifeTime)
#else
INT1
nmhGetFsIkePolicyLifeTime (pFsIkeEngineName, u4FsIkePolicyPriority,
                           pu4RetValFsIkePolicyLifeTime)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4              *pu4RetValFsIkePolicyLifeTime;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pu4RetValFsIkePolicyLifeTime = pIkePolicy->u4LifeTime;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyLifeTimeKB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyLifeTimeKB (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             UINT4 u4FsIkePolicyPriority,
                             UINT4 *pu4RetValFsIkePolicyLifeTimeKB)
#else
INT1
nmhGetFsIkePolicyLifeTimeKB (pFsIkeEngineName, u4FsIkePolicyPriority,
                             pu4RetValFsIkePolicyLifeTimeKB)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4              *pu4RetValFsIkePolicyLifeTimeKB;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pu4RetValFsIkePolicyLifeTimeKB = pIkePolicy->u4LifeTimeKB;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       UINT4 u4FsIkePolicyPriority,
                       INT4 *pi4RetValFsIkePolicyMode)
#else
INT1
nmhGetFsIkePolicyMode (pFsIkeEngineName, u4FsIkePolicyPriority,
                       pi4RetValFsIkePolicyMode)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyMode;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyMode = (INT4) pIkePolicy->u1Mode;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkePolicyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                retValFsIkePolicyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkePolicyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkePolicyStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         UINT4 u4FsIkePolicyPriority,
                         INT4 *pi4RetValFsIkePolicyStatus)
#else
INT1
nmhGetFsIkePolicyStatus (pFsIkeEngineName, u4FsIkePolicyPriority,
                         pi4RetValFsIkePolicyStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4               *pi4RetValFsIkePolicyStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        *pi4RetValFsIkePolicyStatus = (INT4) pIkePolicy->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkePolicyAuthMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyAuthMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyAuthMethod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyAuthMethod (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             UINT4 u4FsIkePolicyPriority,
                             INT4 i4SetValFsIkePolicyAuthMethod)
#else
INT1
nmhSetFsIkePolicyAuthMethod (pFsIkeEngineName, u4FsIkePolicyPriority,
                             i4SetValFsIkePolicyAuthMethod)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyAuthMethod;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u2AuthMethod = (UINT2) i4SetValFsIkePolicyAuthMethod;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyHashAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           UINT4 u4FsIkePolicyPriority,
                           INT4 i4SetValFsIkePolicyHashAlgo)
#else
INT1
nmhSetFsIkePolicyHashAlgo (pFsIkeEngineName, u4FsIkePolicyPriority,
                           i4SetValFsIkePolicyHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyHashAlgo;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u1HashAlgo = (UINT1) i4SetValFsIkePolicyHashAlgo;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyEncryptionAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyEncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 UINT4 u4FsIkePolicyPriority,
                                 INT4 i4SetValFsIkePolicyEncryptionAlgo)
#else
INT1
nmhSetFsIkePolicyEncryptionAlgo (pFsIkeEngineName, u4FsIkePolicyPriority,
                                 i4SetValFsIkePolicyEncryptionAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyEncryptionAlgo;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {

        if ((i4SetValFsIkePolicyEncryptionAlgo == IKE_DES_CBC) ||
            (i4SetValFsIkePolicyEncryptionAlgo == IKE_3DES_CBC) ||
            (i4SetValFsIkePolicyEncryptionAlgo == IKE_IPSEC_ESP_3DES_CBC) ||
            (i4SetValFsIkePolicyEncryptionAlgo == IKE_IPSEC_ESP_DES_CBC))
        {
            pIkePolicy->u1EncryptionAlgo =
                (UINT1) i4SetValFsIkePolicyEncryptionAlgo;
            if (pIkePolicy->u1IkeVersion == IKE2_MAJOR_VERSION)
            {
                pIkePolicy->u2KeyLen =
                    (UINT2)
                    IKE_BYTES2BITS (gaIke2CipherAlgoList
                                    [pIkePolicy->u1EncryptionAlgo].u4KeyLen);
            }
            else
            {
                pIkePolicy->u2KeyLen =
                    (UINT2)
                    IKE_BYTES2BITS (gaIkeCipherAlgoList
                                    [pIkePolicy->u1EncryptionAlgo].u4KeyLen);
            }
        }
        else
        {
            if (pIkePolicy->u1IkeVersion == IKE2_MAJOR_VERSION)
            {
                pIkePolicy->u1EncryptionAlgo = IKE_IPSEC_ESP_AES;
            }
            else
            {
                pIkePolicy->u1EncryptionAlgo = IKE_AES;
            }

            if (i4SetValFsIkePolicyEncryptionAlgo == IKE_AES_DEF_KEY_LEN_VAL)
            {
                pIkePolicy->u2KeyLen = IKE_AES_DEF_KEY_LEN_VAL;
            }
            else if (i4SetValFsIkePolicyEncryptionAlgo == IKE_AES_KEY_LEN_128)
            {
                pIkePolicy->u2KeyLen = IKE_AES_KEY_LEN_128;
            }
            else
            {
                pIkePolicy->u2KeyLen = IKE_AES_KEY_LEN_256;
            }
        }

        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyDHGroup
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyDHGroup
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyDHGroup ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyDHGroup (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          UINT4 u4FsIkePolicyPriority,
                          INT4 i4SetValFsIkePolicyDHGroup)
#else
INT1
nmhSetFsIkePolicyDHGroup (pFsIkeEngineName, u4FsIkePolicyPriority,
                          i4SetValFsIkePolicyDHGroup)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyDHGroup;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u1DHGroup = (UINT1) i4SetValFsIkePolicyDHGroup;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyLifeTimeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyLifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                               UINT4 u4FsIkePolicyPriority,
                               INT4 i4SetValFsIkePolicyLifeTimeType)
#else
INT1
nmhSetFsIkePolicyLifeTimeType (pFsIkeEngineName, u4FsIkePolicyPriority,
                               i4SetValFsIkePolicyLifeTimeType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyLifeTimeType;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u1LifeTimeType = (UINT1) i4SetValFsIkePolicyLifeTimeType;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyLifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           UINT4 u4FsIkePolicyPriority,
                           UINT4 u4SetValFsIkePolicyLifeTime)
#else
INT1
nmhSetFsIkePolicyLifeTime (pFsIkeEngineName, u4FsIkePolicyPriority,
                           u4SetValFsIkePolicyLifeTime)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4               u4SetValFsIkePolicyLifeTime;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        /* 
         * NOTE : If the lifetimeType is not set before setting this
         * then the default unit will be in hours
         */
        pIkePolicy->u4LifeTime = u4SetValFsIkePolicyLifeTime;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyLifeTimeKB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyLifeTimeKB (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             UINT4 u4FsIkePolicyPriority,
                             UINT4 u4SetValFsIkePolicyLifeTimeKB)
#else
INT1
nmhSetFsIkePolicyLifeTimeKB (pFsIkeEngineName, u4FsIkePolicyPriority,
                             u4SetValFsIkePolicyLifeTimeKB)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4               u4SetValFsIkePolicyLifeTimeKB;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u4LifeTimeKB = u4SetValFsIkePolicyLifeTimeKB;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       UINT4 u4FsIkePolicyPriority,
                       INT4 i4SetValFsIkePolicyMode)
#else
INT1
nmhSetFsIkePolicyMode (pFsIkeEngineName, u4FsIkePolicyPriority,
                       i4SetValFsIkePolicyMode)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyMode;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->u1Mode = (UINT1) i4SetValFsIkePolicyMode;
        if (pIkePolicy->u1Status == IKE_ACTIVE)
        {
            /* IkePolicy has changed, send info to IKE Task */
            IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                        IKE_CONFIG_CHG_POLICY);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhSetFsIkePolicyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                setValFsIkePolicyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkePolicyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkePolicyStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         UINT4 u4FsIkePolicyPriority,
                         INT4 i4SetValFsIkePolicyStatus)
#else
INT1
nmhSetFsIkePolicyStatus (pFsIkeEngineName, u4FsIkePolicyPriority,
                         i4SetValFsIkePolicyStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4SetValFsIkePolicyStatus;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    /* Test routine will already have checked whether this entry exists, 
       so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkePolicyStatus)
    {
        case IKE_CREATEANDGO:
        {
            tIkePolicy         *pIkePolicy = NULL;
            UINT4               u4PolicyCount = IKE_ZERO;

            TAKE_IKE_SEM ();
            u4PolicyCount = TMO_SLL_Count (&(pIkeEngine->IkePolicyList));
            if (u4PolicyCount >= (MAX_NUMBER_OF_SA / IKE_TWO))
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "nmhSetFsIkePolicyStatus: Unable to"
                             " create ike policy reached maximum limit\n");
                GIVE_IKE_SEM ();
                return (SNMP_FAILURE);
            }
            GIVE_IKE_SEM ();

            pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
            if (pIkePolicy != NULL)
            {
                return (SNMP_FAILURE);
            }

            if (MemAllocateMemBlock
                (IKE_POLICY_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkePolicy) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkePolicyStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }

            if (pIkePolicy == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkePolicy, IKE_ZERO, sizeof (tIkePolicy));
            pIkePolicy->u4Priority = u4FsIkePolicyPriority;
            MEMCPY (pIkePolicy->au1EngineName, pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkePolicy->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkePolicy->u1Status = IKE_ACTIVE;
            IkeInitPolicy (pIkePolicy);
            IkeAddToPolicyList (pIkeEngine, pIkePolicy);
            return (SNMP_SUCCESS);
        }

        case IKE_CREATEANDWAIT:
        {
            tIkePolicy         *pIkePolicy = NULL;
            UINT4               u4PolicyCount = IKE_ZERO;

            TAKE_IKE_SEM ();
            u4PolicyCount = TMO_SLL_Count (&(pIkeEngine->IkePolicyList));
            if (u4PolicyCount >= (MAX_NUMBER_OF_SA / IKE_TWO))
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "nmhSetFsIkePolicyStatus: Unable to"
                             " create ike policy reached maximum limit\n");
                GIVE_IKE_SEM ();
                return (SNMP_FAILURE);
            }
            GIVE_IKE_SEM ();

            pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
            if (pIkePolicy != NULL)
            {
                return (SNMP_FAILURE);
            }
            if (MemAllocateMemBlock
                (IKE_POLICY_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkePolicy) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "nmhSetFsIkePolicyStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }

            if (pIkePolicy == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkePolicy, IKE_ZERO, sizeof (tIkePolicy));
            pIkePolicy->u4Priority = u4FsIkePolicyPriority;
            MEMCPY (pIkePolicy->au1EngineName, pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkePolicy->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkePolicy->u1Status = IKE_NOTINSERVICE;
            IkeInitPolicy (pIkePolicy);
            IkeAddToPolicyList (pIkeEngine, pIkePolicy);
            return (SNMP_SUCCESS);
        }

        case IKE_ACTIVE:
        {
            tIkePolicy         *pIkePolicy = NULL;

            pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
            if (pIkePolicy == NULL)
            {
                return (SNMP_FAILURE);
            }
            /* Since all attributes have default values, don't have to check */
            pIkePolicy->u1Status = IKE_ACTIVE;
            return (SNMP_SUCCESS);
        }

        case IKE_NOTINSERVICE:
        {
            tIkePolicy         *pIkePolicy = NULL;

            pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
            if (pIkePolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (IKE_NOTINSERVICE == pIkePolicy->u1Status)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkePolicy->u1Status)
            {
                pIkePolicy->u1Status = IKE_NOTINSERVICE;
                /* IkePolicy has been deactivated, send info to IKE Task */
                IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                            IKE_CONFIG_DEL_POLICY);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkePolicy         *pIkePolicy = NULL;

            pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
            if (pIkePolicy != NULL)
            {
                /* IkePolicy has been deleted, send info to IKE Task */
                IkeSnmpSendPolicyInfoToIke (pIkeEngine->u4IkeIndex, pIkePolicy,
                                            IKE_CONFIG_DEL_POLICY);
                IkeDeletePolicy (pIkeEngine, pIkePolicy);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyAuthMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyAuthMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyAuthMethod ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyAuthMethod (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                UINT4 u4FsIkePolicyPriority,
                                INT4 i4TestValFsIkePolicyAuthMethod)
#else
INT1
nmhTestv2FsIkePolicyAuthMethod (pu4ErrorCode, pFsIkeEngineName,
                                u4FsIkePolicyPriority,
                                i4TestValFsIkePolicyAuthMethod)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyAuthMethod;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Only PreShared key and RSA signatures are Supported for now */
    if ((i4TestValFsIkePolicyAuthMethod != IKE_PRESHARED_KEY) &&
        (i4TestValFsIkePolicyAuthMethod != IKE_RSA_SIGNATURE) &&
        (i4TestValFsIkePolicyAuthMethod != IKE2_AUTH_PSK) &&
        (i4TestValFsIkePolicyAuthMethod != IKE2_AUTH_RSA))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyHashAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyHashAlgo (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              UINT4 u4FsIkePolicyPriority,
                              INT4 i4TestValFsIkePolicyHashAlgo)
#else
INT1
nmhTestv2FsIkePolicyHashAlgo (pu4ErrorCode, pFsIkeEngineName,
                              u4FsIkePolicyPriority,
                              i4TestValFsIkePolicyHashAlgo)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyHashAlgo;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkePolicyHashAlgo != IKE_MD5) &&
        (i4TestValFsIkePolicyHashAlgo != IKE_SHA1) &&
        (i4TestValFsIkePolicyHashAlgo != SEC_XCBCMAC) &&
        (i4TestValFsIkePolicyHashAlgo != HMAC_SHA_256) &&
        (i4TestValFsIkePolicyHashAlgo != HMAC_SHA_384) &&
        (i4TestValFsIkePolicyHashAlgo != HMAC_SHA_512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyEncryptionAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyEncryptionAlgo (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    UINT4 u4FsIkePolicyPriority,
                                    INT4 i4TestValFsIkePolicyEncryptionAlgo)
#else
INT1
nmhTestv2FsIkePolicyEncryptionAlgo (pu4ErrorCode, pFsIkeEngineName,
                                    u4FsIkePolicyPriority,
                                    i4TestValFsIkePolicyEncryptionAlgo)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyEncryptionAlgo;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* For Ikev2, the Algorithm values are same as that of IPSec Encryption
     * algorithms for IKEv1 */
    if ((i4TestValFsIkePolicyEncryptionAlgo != IKE_DES_CBC) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_3DES_CBC) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_AES_DEF_KEY_LEN_VAL) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_AES_KEY_LEN_128) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_AES_KEY_LEN_256) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_IPSEC_ESP_AES) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_IPSEC_ESP_3DES_CBC) &&
        (i4TestValFsIkePolicyEncryptionAlgo != IKE_IPSEC_ESP_DES_CBC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyDHGroup
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyDHGroup
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyDHGroup ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyDHGroup (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             UINT4 u4FsIkePolicyPriority,
                             INT4 i4TestValFsIkePolicyDHGroup)
#else
INT1
nmhTestv2FsIkePolicyDHGroup (pu4ErrorCode, pFsIkeEngineName,
                             u4FsIkePolicyPriority, i4TestValFsIkePolicyDHGroup)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyDHGroup;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkePolicyDHGroup != IKE_DH_GROUP_1) &&
        (i4TestValFsIkePolicyDHGroup != IKE_DH_GROUP_2) &&
        (i4TestValFsIkePolicyDHGroup != IKE_DH_GROUP_5) &&
        (i4TestValFsIkePolicyDHGroup != SEC_DH_GROUP_14))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyLifeTimeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyLifeTimeType (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                  UINT4 u4FsIkePolicyPriority,
                                  INT4 i4TestValFsIkePolicyLifeTimeType)
#else
INT1
nmhTestv2FsIkePolicyLifeTimeType (pu4ErrorCode, pFsIkeEngineName,
                                  u4FsIkePolicyPriority,
                                  i4TestValFsIkePolicyLifeTimeType)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyLifeTimeType;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkePolicyLifeTimeType < FSIKE_LIFETIME_SECS) ||
        (i4TestValFsIkePolicyLifeTimeType > FSIKE_LIFETIME_DAYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyLifeTime (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              UINT4 u4FsIkePolicyPriority,
                              UINT4 u4TestValFsIkePolicyLifeTime)
#else
INT1
nmhTestv2FsIkePolicyLifeTime (pu4ErrorCode, pFsIkeEngineName,
                              u4FsIkePolicyPriority,
                              u4TestValFsIkePolicyLifeTime)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4               u4TestValFsIkePolicyLifeTime;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Not using the nmhValidate routine here as we want to check
       the Lifetime value using pIkePolicy
     */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);
        if (pIkePolicy != NULL)
        {
            IKE_CONVERT_LIFETIME_TO_SECS (pIkePolicy->u1LifeTimeType,
                                          u4TestValFsIkePolicyLifeTime);
            if ((u4TestValFsIkePolicyLifeTime != IKE_ZERO) &&
                (u4TestValFsIkePolicyLifeTime < FSIKE_MIN_LIFETIME_SECS))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            return (SNMP_SUCCESS);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyLifeTimeKB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyLifeTimeKB (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                UINT4 u4FsIkePolicyPriority,
                                UINT4 u4TestValFsIkePolicyLifeTimeKB)
#else
INT1
nmhTestv2FsIkePolicyLifeTimeKB (pu4ErrorCode, pFsIkeEngineName,
                                u4FsIkePolicyPriority,
                                u4TestValFsIkePolicyLifeTimeKB)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     UINT4               u4TestValFsIkePolicyLifeTimeKB;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        if ((u4TestValFsIkePolicyLifeTimeKB != IKE_ZERO) &&
            (u4TestValFsIkePolicyLifeTimeKB < FSIKE_MIN_LIFETIME_KB))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyMode (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          UINT4 u4FsIkePolicyPriority,
                          INT4 i4TestValFsIkePolicyMode)
#else
INT1
nmhTestv2FsIkePolicyMode (pu4ErrorCode, pFsIkeEngineName, u4FsIkePolicyPriority,
                          i4TestValFsIkePolicyMode)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyMode;
#endif
{

    if (IkeGetPolicyPeerIdStatus (pFsIkeEngineName->pu1_OctetList,
                                  pFsIkeEngineName->i4_Length,
                                  u4FsIkePolicyPriority) != IKE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkePolicyMode != IKE_MAIN_MODE) &&
        (i4TestValFsIkePolicyMode != IKE_AGGRESSIVE_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pFsIkeEngineName, u4FsIkePolicyPriority) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkePolicyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkePolicyPriority

                The Object 
                testValFsIkePolicyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkePolicyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkePolicyStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            UINT4 u4FsIkePolicyPriority,
                            INT4 i4TestValFsIkePolicyStatus)
#else
INT1
nmhTestv2FsIkePolicyStatus (pu4ErrorCode, pFsIkeEngineName,
                            u4FsIkePolicyPriority, i4TestValFsIkePolicyStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     UINT4               u4FsIkePolicyPriority;
     INT4                i4TestValFsIkePolicyStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    if (u4FsIkePolicyPriority == IKE_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsIkePolicyStatus == IKE_NOTREADY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkePolicyStatus < IKE_ACTIVE)
        || (i4TestValFsIkePolicyStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkePolicyTable (pFsIkeEngineName,
                                                      u4FsIkePolicyPriority) ==
            SNMP_SUCCESS)
        {
            if ((i4TestValFsIkePolicyStatus == IKE_CREATEANDWAIT) ||
                (i4TestValFsIkePolicyStatus == IKE_CREATEANDGO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
        else
        {
            if ((i4TestValFsIkePolicyStatus != IKE_CREATEANDWAIT) &&
                (i4TestValFsIkePolicyStatus != IKE_CREATEANDGO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeRAInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeRAInfoTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIkeRAInfoTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsIkeEngineName,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsIkeRAInfoName)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                       pFsIkeRAInfoName->pu1_OctetList,
                                       pFsIkeRAInfoName->i4_Length);
        if (pIkeRAInfo != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeRAInfoTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIkeRAInfoTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                  tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo =
        (tIkeRemoteAccessInfo *) SLL_FIRST (&pIkeEngine->IkeRAPolicyList);
    if (pIkeRAInfo == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length = (INT4) STRLEN (pIkeRAInfo->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList,
                pIkeRAInfo->au1EngineName, pFsIkeEngineName->i4_Length);
    pFsIkeRAInfoName->i4_Length = (INT4) STRLEN (pIkeRAInfo->au1RAInfoName);
    IKE_MEMCPY (pFsIkeRAInfoName->pu1_OctetList,
                pIkeRAInfo->au1RAInfoName, pFsIkeRAInfoName->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeRAInfoTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkeRAInfoName
                nextFsIkeRAInfoName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIkeRAInfoTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsIkeRAInfoName)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    tIkeRemoteAccessInfo *pNextIkeRAInfo = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (IKE_MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pIkeRAInfo, tIkeRemoteAccessInfo *)
    {
        if (IKE_MEMCMP (pIkeRAInfo->au1RAInfoName,
                        pFsIkeRAInfoName->pu1_OctetList,
                        pFsIkeRAInfoName->i4_Length) == IKE_ZERO)
        {
            pNextIkeRAInfo =
                (tIkeRemoteAccessInfo *) SLL_NEXT (&pIkeEngine->IkeRAPolicyList,
                                                   (t_SLL_NODE *) pIkeRAInfo);
            if (pNextIkeRAInfo == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Key of this new engine */
                pNextIkeRAInfo =
                    (tIkeRemoteAccessInfo *) SLL_FIRST (&pNextIkeEngine->
                                                        IkeRAPolicyList);
                if (pNextIkeRAInfo == NULL)
                {
                    return (SNMP_FAILURE);
                }
            }

            pNextFsIkeEngineName->i4_Length = (INT4) STRLEN (pNextIkeRAInfo->
                                                             au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkeRAInfo->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            pNextFsIkeRAInfoName->i4_Length =
                (INT4) STRLEN (pNextIkeRAInfo->au1RAInfoName);
            IKE_MEMCPY (pNextFsIkeRAInfoName->pu1_OctetList,
                        pNextIkeRAInfo->au1RAInfoName,
                        pNextFsIkeRAInfoName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIkeRAInfoPeerIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInfoPeerIdType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInfoPeerIdType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                             INT4 *pi4RetValFsIkeRAInfoPeerIdType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRAInfoPeerIdType = pIkeRAInfo->Phase1ID.i4IdType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInfoPeerId
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInfoPeerId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInfoPeerId (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsIkeRAInfoPeerId)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {

        pRetValFsIkeRAInfoPeerId->i4_Length =
            (INT4) STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId);
        IKE_MEMCPY (pRetValFsIkeRAInfoPeerId->pu1_OctetList,
                    pIkeRAInfo->au1DisplayRAInfoPeerId,
                    pRetValFsIkeRAInfoPeerId->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAXAuthEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAXAuthEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAXAuthEnable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                          INT4 *pi4RetValFsIkeRAXAuthEnable)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRAXAuthEnable = pIkeRAInfo->bXAuthEnabled;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRACMEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRACMEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRACMEnable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                       INT4 *pi4RetValFsIkeRACMEnable)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRACMEnable = pIkeRAInfo->bCMEnabled;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetFsIkeRAXAuthType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAXAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAXAuthType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                        INT4 *pi4RetValFsIkeRAXAuthType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            *pi4RetValFsIkeRAXAuthType = pIkeRAInfo->XAuthClient.u1XAuthType;
        }
        else
        {
            *pi4RetValFsIkeRAXAuthType = pIkeRAInfo->XAuthServer.u1XAuthType;
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAXAuthUserName
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAXAuthUserName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAXAuthUserName (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIkeRAXAuthUserName)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == TRUE)
        {
            pRetValFsIkeRAXAuthUserName->i4_Length = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        pRetValFsIkeRAXAuthUserName->i4_Length =
            (INT4) IKE_STRLEN (pIkeRAInfo->XAuthClient.au1UserName);
        IKE_MEMCPY (pRetValFsIkeRAXAuthUserName->pu1_OctetList,
                    pIkeRAInfo->XAuthClient.au1UserName,
                    pRetValFsIkeRAXAuthUserName->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAXAuthUserPasswd
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAXAuthUserPasswd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAXAuthUserPasswd (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsIkeRAXAuthUserPasswd)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == TRUE)
        {
            pRetValFsIkeRAXAuthUserPasswd->i4_Length = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        if (IKE_STRLEN (pIkeRAInfo->XAuthClient.au1UserPasswd) > IKE_ZERO)
        {
            pRetValFsIkeRAXAuthUserPasswd->i4_Length = IKE_THREE;
            pRetValFsIkeRAXAuthUserPasswd->pu1_OctetList[IKE_INDEX_0] = '*';
            pRetValFsIkeRAXAuthUserPasswd->pu1_OctetList[IKE_INDEX_1] = '*';
            pRetValFsIkeRAXAuthUserPasswd->pu1_OctetList[IKE_INDEX_2] = '*';
        }
        else
        {
            pRetValFsIkeRAXAuthUserPasswd->i4_Length = IKE_ZERO;
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAIPAddressAllocMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAIPAddressAllocMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAIPAddressAllocMethod (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                   tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                   INT4 *pi4RetValFsIkeRAIPAddressAllocMethod)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            *pi4RetValFsIkeRAIPAddressAllocMethod = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        *pi4RetValFsIkeRAIPAddressAllocMethod =
            pIkeRAInfo->CMServerInfo.u1AddrType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInternalIPAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInternalIPAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInternalIPAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 INT4 *pi4RetValFsIkeRAInternalIPAddrType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            *pi4RetValFsIkeRAInternalIPAddrType = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        *pi4RetValFsIkeRAInternalIPAddrType =
            (INT4) pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInternalIPAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInternalIPAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInternalIPAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsIkeRAInternalIPAddr)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    const INT1         *pTemp = NULL;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            pRetValFsIkeRAInternalIPAddr->i4_Length = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        if (IPV4ADDR == pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType)
        {
            pRetValFsIkeRAInternalIPAddr->i4_Length = INET_ADDRSTRLEN;
            u4TempIp4Addr = IKE_HTONL (pIkeRAInfo->CMServerInfo.
                                       InternalIpAddr.uIpAddr.Ip4Addr);
            pTemp = (const INT1 *) INET_NTOP4 (&u4TempIp4Addr,
                                               pRetValFsIkeRAInternalIPAddr->
                                               pu1_OctetList);
            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }
        }
        else if (IPV6ADDR == pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType)
        {
            pRetValFsIkeRAInternalIPAddr->i4_Length = INET6_ADDRSTRLEN;
            pTemp = (const INT1 *)
                INET_NTOP6 (&pIkeRAInfo->CMServerInfo.InternalIpAddr.uIpAddr.
                            Ip6Addr.u1_addr,
                            pRetValFsIkeRAInternalIPAddr->pu1_OctetList);
            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }
        }
        else
        {
            /* Something other that IPv4 or IPv6 Address */
            pRetValFsIkeRAInternalIPAddr->i4_Length = IKE_ZERO;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInternalIPNetMaskType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInternalIPNetMaskType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInternalIPNetMaskType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                    INT4 *pi4RetValFsIkeRAInternalIPNetMaskType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            *pi4RetValFsIkeRAInternalIPNetMaskType = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        *pi4RetValFsIkeRAInternalIPNetMaskType =
            (INT4) pIkeRAInfo->CMServerInfo.InternalMask.u4AddrType;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInternalIPNetMask
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInternalIPNetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInternalIPNetMask (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsIkeRAInternalIPNetMask)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;
    const INT1         *pTemp = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == FALSE)
        {
            pRetValFsIkeRAInternalIPNetMask->i4_Length = IKE_ZERO;
            return (SNMP_SUCCESS);
        }
        if (IPV4ADDR == pIkeRAInfo->CMServerInfo.InternalMask.u4AddrType)
        {
            pRetValFsIkeRAInternalIPNetMask->i4_Length = INET_ADDRSTRLEN;
            u4TempIp4Addr = IKE_HTONL (pIkeRAInfo->CMServerInfo.
                                       InternalMask.uIpAddr.Ip4Addr);
            pTemp = (const INT1 *) INET_NTOP4 (&u4TempIp4Addr,
                                               pRetValFsIkeRAInternalIPNetMask->
                                               pu1_OctetList);

            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }

        }
        else if (IPV6ADDR == pIkeRAInfo->CMServerInfo.InternalMask.u4AddrType)
        {
            pRetValFsIkeRAInternalIPNetMask->i4_Length = INET6_ADDRSTRLEN;
            pTemp = (const INT1 *)
                INET_NTOP6 (&pIkeRAInfo->CMServerInfo.InternalMask.uIpAddr.
                            Ip6Addr.u1_addr,
                            pRetValFsIkeRAInternalIPNetMask->pu1_OctetList);
            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }
        }
        else
        {
            /* Something other that IPv4 or IPv6 Address */
            pRetValFsIkeRAInternalIPNetMask->i4_Length = IKE_ZERO;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInfoProtectedNetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInfoProtectedNetBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInfoProtectedNetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                     tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsIkeRAInfoProtectedNetBundle)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pRetValFsIkeRAInfoProtectedNetBundle->i4_Length =
            (INT4) STRLEN (pIkeRAInfo->au1ProtectNetBundleDisplay);
        MEMCPY (pRetValFsIkeRAInfoProtectedNetBundle->pu1_OctetList,
                pIkeRAInfo->au1ProtectNetBundleDisplay,
                pRetValFsIkeRAInfoProtectedNetBundle->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeRATransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRATransformSetBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRATransformSetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsIkeRATransformSetBundle)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pRetValFsIkeRATransformSetBundle->i4_Length =
            (INT4) STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay);
        MEMCPY (pRetValFsIkeRATransformSetBundle->pu1_OctetList,
                pIkeRAInfo->au1TransformSetBundleDisplay,
                pRetValFsIkeRATransformSetBundle->i4_Length);
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIkeRAMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                   tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                   INT4 *pi4RetValFsIkeRAMode)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRAMode = pIkeRAInfo->u1Mode;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsIkeRAPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAPfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAPfs (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                  tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                  INT4 *pi4RetValFsIkeRAPfs)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRAPfs = pIkeRAInfo->u1Pfs;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsIkeRALifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRALifeTimeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRALifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                           INT4 *pi4RetValFsIkeRALifeTimeType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRALifeTimeType = pIkeRAInfo->u1LifeTimeType;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIkeRALifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRALifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRALifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                       UINT4 *pu4RetValFsIkeRALifeTime)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pu4RetValFsIkeRALifeTime = pIkeRAInfo->u4LifeTime;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsIkeRAInfoStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                retValFsIkeRAInfoStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeRAInfoStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                         INT4 *pi4RetValFsIkeRAInfoStatus)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        *pi4RetValFsIkeRAInfoStatus = (INT4) pIkeRAInfo->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsIkeRAInfoPeerIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInfoPeerIdType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInfoPeerIdType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                             INT4 i4SetValFsIkeRAInfoPeerIdType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);

    if (pIkeRAInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeRAInfoPeerIdType)
    {
        case IKE_KEY_IPV4:
        {
            tIp4Addr            u4TempIp4Addr = IKE_ZERO;

            i4RetVal =
                INET_PTON4 (pIkeRAInfo->au1DisplayRAInfoPeerId, &u4TempIp4Addr);
            if (i4RetVal > IKE_ZERO)
            {
                pIkeRAInfo->Phase1ID.uID.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
                pIkeRAInfo->Phase1ID.u4Length = sizeof (tIp4Addr);
            }
            else
            {
                return (SNMP_FAILURE);
            }
            break;
        }

        case IKE_KEY_IPV6:
        {
            tIp6Addr            TempIp6Addr;

            i4RetVal = INET_PTON6 (pIkeRAInfo->au1DisplayRAInfoPeerId,
                                   &(TempIp6Addr.u1_addr));
            if (i4RetVal > IKE_ZERO)
            {
                MEMCPY (&pIkeRAInfo->Phase1ID.uID.Ip6Addr,
                        &TempIp6Addr, sizeof (tIp6Addr));
                pIkeRAInfo->Phase1ID.u4Length = sizeof (tIp6Addr);
            }
            else
            {
                return (SNMP_FAILURE);
            }
            break;
        }

        case IKE_KEY_EMAIL:
        {
            STRNCPY (pIkeRAInfo->Phase1ID.uID.au1Email,
                     pIkeRAInfo->au1DisplayRAInfoPeerId,
                     STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId));
            pIkeRAInfo->Phase1ID.u4Length =
                STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId);
            break;
        }

        case IKE_KEY_FQDN:
        {
            STRNCPY (pIkeRAInfo->Phase1ID.uID.au1Fqdn,
                     pIkeRAInfo->au1DisplayRAInfoPeerId,
                     STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId));
            pIkeRAInfo->Phase1ID.u4Length =
                STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId);
            break;
        }

        case IKE_KEY_DN:
        {
            STRNCPY (pIkeRAInfo->Phase1ID.uID.au1Dn,
                     pIkeRAInfo->au1DisplayRAInfoPeerId,
                     STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId));
            pIkeRAInfo->Phase1ID.u4Length =
                STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId);
            break;
        }
        case IKE_KEY_KEYID:
        {
            STRNCPY (pIkeRAInfo->Phase1ID.uID.au1KeyId,
                     pIkeRAInfo->au1DisplayRAInfoPeerId,
                     STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId));
            pIkeRAInfo->Phase1ID.u4Length =
                STRLEN (pIkeRAInfo->au1DisplayRAInfoPeerId);
            break;
        }
        default:
            break;
    }
    pIkeRAInfo->Phase1ID.i4IdType = i4SetValFsIkeRAInfoPeerIdType;

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInfoPeerId
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInfoPeerId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInfoPeerId (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsIkeRAInfoPeerId)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo =
        IkeSnmpGetRAInfo (pIkeEngine, pFsIkeRAInfoName->pu1_OctetList,
                          pFsIkeRAInfoName->i4_Length);

    if (pIkeRAInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    IKE_MEMCPY (pIkeRAInfo->au1DisplayRAInfoPeerId,
                pSetValFsIkeRAInfoPeerId->pu1_OctetList,
                pSetValFsIkeRAInfoPeerId->i4_Length);
    pIkeRAInfo->au1DisplayRAInfoPeerId[pSetValFsIkeRAInfoPeerId->i4_Length] =
        '\0';

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAXAuthEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAXAuthEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAXAuthEnable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                          INT4 i4SetValFsIkeRAXAuthEnable)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->bXAuthEnabled = (BOOLEAN) i4SetValFsIkeRAXAuthEnable;
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRACMEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRACMEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRACMEnable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                       INT4 i4SetValFsIkeRACMEnable)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->bCMEnabled = (BOOLEAN) i4SetValFsIkeRACMEnable;
    }
    else
    {
        return (SNMP_FAILURE);
    }

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAXAuthType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAXAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAXAuthType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                        INT4 i4SetValFsIkeRAXAuthType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (gbXAuthServer == TRUE)
        {
            pIkeRAInfo->XAuthServer.u1XAuthType =
                (UINT1) i4SetValFsIkeRAXAuthType;
        }
        else
        {
            pIkeRAInfo->XAuthClient.u1XAuthType =
                (UINT1) i4SetValFsIkeRAXAuthType;
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAXAuthUserName
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAXAuthUserName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAXAuthUserName (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsIkeRAXAuthUserName)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        IKE_MEMCPY (pIkeRAInfo->XAuthClient.au1UserName,
                    pSetValFsIkeRAXAuthUserName->pu1_OctetList,
                    pSetValFsIkeRAXAuthUserName->i4_Length);
        pIkeRAInfo->XAuthClient.au1UserName
            [pSetValFsIkeRAXAuthUserName->i4_Length] = '\0';
    }
    else
    {
        return (SNMP_FAILURE);
    }

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsIkeRAXAuthUserPasswd
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAXAuthUserPasswd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAXAuthUserPasswd (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsIkeRAXAuthUserPasswd)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        IKE_MEMCPY (pIkeRAInfo->XAuthClient.au1UserPasswd,
                    pSetValFsIkeRAXAuthUserPasswd->pu1_OctetList,
                    pSetValFsIkeRAXAuthUserPasswd->i4_Length);
        pIkeRAInfo->XAuthClient.au1UserPasswd
            [pSetValFsIkeRAXAuthUserPasswd->i4_Length] = '\0';
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAIPAddressAllocMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAIPAddressAllocMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAIPAddressAllocMethod (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                   tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                   INT4 i4SetValFsIkeRAIPAddressAllocMethod)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->CMServerInfo.u1AddrType =
            (UINT1) i4SetValFsIkeRAIPAddressAllocMethod;
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInternalIPAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInternalIPAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInternalIPAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 INT4 i4SetValFsIkeRAInternalIPAddrType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType =
        (UINT4) i4SetValFsIkeRAInternalIPAddrType;

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInternalIPAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInternalIPAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInternalIPAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsIkeRAInternalIPAddr)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pSetValFsIkeRAInternalIPAddr->pu1_OctetList[pSetValFsIkeRAInternalIPAddr->
                                                i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pSetValFsIkeRAInternalIPAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (IPV4ADDR == u1AddrType)
        {
            if (pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex >=
                IKE_MAX_SUPPORTED_ATTR)
            {
                return (SNMP_FAILURE);
            }
            pIkeRAInfo->CMServerInfo.au2ConfiguredAttr
                [pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex] =
                IKE_INTERNAL_IP4_ADDRESS;
            pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex++;
            pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType = u1AddrType;
            INET_PTON4 (pSetValFsIkeRAInternalIPAddr->pu1_OctetList,
                        &u4TempIp4Addr);
            pIkeRAInfo->CMServerInfo.InternalIpAddr.uIpAddr.Ip4Addr =
                IKE_NTOHL (u4TempIp4Addr);
        }

        else if (IPV6ADDR == u1AddrType)
        {
            INET_PTON6 (pSetValFsIkeRAInternalIPAddr->pu1_OctetList,
                        &pIkeRAInfo->CMServerInfo.InternalIpAddr.
                        uIpAddr.Ip6Addr.u1_addr);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInternalIPNetMaskType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInternalIPNetMaskType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInternalIPNetMaskType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                    INT4 i4SetValFsIkeRAInternalIPNetMaskType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAInfo->CMServerInfo.InternalMask.u4AddrType =
        (UINT4) i4SetValFsIkeRAInternalIPNetMaskType;

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInternalIPNetMask
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInternalIPNetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInternalIPNetMask (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsIkeRAInternalIPNetMask)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pSetValFsIkeRAInternalIPNetMask->pu1_OctetList
        [pSetValFsIkeRAInternalIPNetMask->i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pSetValFsIkeRAInternalIPNetMask->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {

        if (IPV4ADDR == u1AddrType)
        {
            if (pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex >=
                IKE_MAX_SUPPORTED_ATTR)
            {
                return (SNMP_FAILURE);
            }
            pIkeRAInfo->CMServerInfo.au2ConfiguredAttr
                [pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex] =
                IKE_INTERNAL_IP4_NETMASK;
            pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex++;
            pIkeRAInfo->CMServerInfo.InternalMask.u4AddrType = u1AddrType;
            INET_PTON4 (pSetValFsIkeRAInternalIPNetMask->pu1_OctetList,
                        &u4TempIp4Addr);
            pIkeRAInfo->CMServerInfo.InternalMask.uIpAddr.Ip4Addr =
                IKE_NTOHL (u4TempIp4Addr);
        }

        if (IPV6ADDR == u1AddrType)
        {
            INET_PTON6 (pSetValFsIkeRAInternalIPNetMask->pu1_OctetList,
                        &pIkeRAInfo->CMServerInfo.InternalMask.uIpAddr.
                        Ip6Addr.u1_addr);
        }
        else
        {
            /* neither v4 nor v6 address */
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInfoProtectedNetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInfoProtectedNetBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInfoProtectedNetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                     tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsIkeRAInfoProtectedNetBundle)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (IkeRAInfoSetProtectNetBundle (pIkeEngine, pIkeRAInfo,
                                          pSetValFsIkeRAInfoProtectedNetBundle->
                                          pu1_OctetList,
                                          pSetValFsIkeRAInfoProtectedNetBundle->
                                          i4_Length) == IKE_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if (pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex >= IKE_MAX_SUPPORTED_ATTR)
        {
            return (SNMP_FAILURE);
        }

        pIkeRAInfo->CMServerInfo.au2ConfiguredAttr
            [pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex] =
            IKE_INTERNAL_IP4_SUBNET;
        pIkeRAInfo->CMServerInfo.u1ConfiAttrIndex++;
        /* Copy onto string for show purposes */
        MEMCPY (pIkeRAInfo->au1ProtectNetBundleDisplay,
                pSetValFsIkeRAInfoProtectedNetBundle->pu1_OctetList,
                pSetValFsIkeRAInfoProtectedNetBundle->i4_Length);
        pIkeRAInfo->au1ProtectNetBundleDisplay
            [pSetValFsIkeRAInfoProtectedNetBundle->i4_Length] = '\0';
    }
    else
    {
        return (SNMP_FAILURE);
    }

    if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
    {
        if (pIkeRAInfo->u1Status != IKE_ACTIVE)
        {
            /* Entry ready to go Active */
            pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
        }
        else
        {
            if (pIkeRAInfo->Phase1ID.i4IdType != IKE_ZERO)
            {
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_CHG_RA_INFO);
            }
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRATransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRATransformSetBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRATransformSetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsIkeRATransformSetBundle)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (IkeRAInfoSetTransformSetBundle (pIkeEngine, pIkeRAInfo,
                                            pSetValFsIkeRATransformSetBundle->
                                            pu1_OctetList,
                                            pSetValFsIkeRATransformSetBundle->
                                            i4_Length) == IKE_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        /* Copy onto string for show purposes */
        MEMCPY (pIkeRAInfo->au1TransformSetBundleDisplay,
                pSetValFsIkeRATransformSetBundle->pu1_OctetList,
                pSetValFsIkeRATransformSetBundle->i4_Length);
        pIkeRAInfo->au1TransformSetBundleDisplay
            [pSetValFsIkeRATransformSetBundle->i4_Length] = '\0';
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                   tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                   INT4 i4SetValFsIkeRAMode)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->u1Mode = (UINT1) i4SetValFsIkeRAMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIkeRAPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAPfs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAPfs (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                  tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                  INT4 i4SetValFsIkeRAPfs)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->u1Pfs = (UINT1) i4SetValFsIkeRAPfs;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsIkeRALifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRALifeTimeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRALifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                           INT4 i4SetValFsIkeRALifeTimeType)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        pIkeRAInfo->u1LifeTimeType = (UINT1) i4SetValFsIkeRALifeTimeType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIkeRALifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRALifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRALifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                       UINT4 u4SetValFsIkeRALifeTime)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                   pFsIkeRAInfoName->pu1_OctetList,
                                   pFsIkeRAInfoName->i4_Length);
    if (pIkeRAInfo != NULL)
    {
        if (pIkeRAInfo->u1LifeTimeType == FSIKE_LIFETIME_KB)
        {
            pIkeRAInfo->u4LifeTimeKB = u4SetValFsIkeRALifeTime;
        }
        else
        {
            pIkeRAInfo->u4LifeTime = u4SetValFsIkeRALifeTime;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsIkeRAInfoStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                setValFsIkeRAInfoStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeRAInfoStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                         INT4 i4SetValFsIkeRAInfoStatus)
{

    tIkeEngine         *pIkeEngine = NULL;

    /* Test routine will already have checked whether this entry exists, 
       so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeRAInfoStatus)
    {
        case IKE_CREATEANDWAIT:
        {
            tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

            pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                           pFsIkeRAInfoName->
                                           pu1_OctetList,
                                           pFsIkeRAInfoName->i4_Length);
            if (pIkeRAInfo != NULL)
            {
                return (SNMP_FAILURE);
            }
            if (MemAllocateMemBlock
                (IKE_RAINFO_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeRAInfo) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeRAInfoStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }
            if (pIkeRAInfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkeRAInfo, IKE_ZERO, sizeof (tIkeRemoteAccessInfo));

            /* Set the Index in the entry */
            MEMCPY (pIkeRAInfo->au1RAInfoName,
                    pFsIkeRAInfoName->pu1_OctetList,
                    pFsIkeRAInfoName->i4_Length);
            pIkeRAInfo->au1RAInfoName[pFsIkeRAInfoName->i4_Length] = '\0';

            /* Set the Engine Name in the entry */
            MEMCPY (pIkeRAInfo->au1EngineName,
                    pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkeRAInfo->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkeRAInfo->u1Status = IKE_NOTREADY;
            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeRAPolicyList), (t_SLL_NODE *) pIkeRAInfo);
            GIVE_IKE_SEM ();
            return (SNMP_SUCCESS);
        }
        case IKE_ACTIVE:
        {
            tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
            pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                           pFsIkeRAInfoName->
                                           pu1_OctetList,
                                           pFsIkeRAInfoName->i4_Length);
            if (pIkeRAInfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            if (IkeCheckRAInfoAttributes (pIkeRAInfo) == IKE_SUCCESS)
            {
                if (gbCMServer == FALSE)
                {
                    pIkeRAInfo->CMClientInfo.au2ConfiguredAttr[IKE_INDEX_0] =
                        IKE_SUPPORTED_ATTRIBUTES;
                    pIkeRAInfo->CMClientInfo.au2ConfiguredAttr[IKE_INDEX_1] =
                        IKE_INTERNAL_IP4_ADDRESS;
                }
                pIkeRAInfo->u1Status = IKE_ACTIVE;
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
        case IKE_NOTINSERVICE:
        {
            tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
            pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                           pFsIkeRAInfoName->
                                           pu1_OctetList,
                                           pFsIkeRAInfoName->i4_Length);
            if (pIkeRAInfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            if (pIkeRAInfo->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }
            if (IKE_ACTIVE == pIkeRAInfo->u1Status)
            {
                pIkeRAInfo->u1Status = IKE_NOTINSERVICE;
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_DEL_RA_INFO);
                return (SNMP_SUCCESS);
            }
            break;
        }
        case IKE_DESTROY:
        {
            tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
            pIkeRAInfo = IkeSnmpGetRAInfo (pIkeEngine,
                                           pFsIkeRAInfoName->
                                           pu1_OctetList,
                                           pFsIkeRAInfoName->i4_Length);
            if (pIkeRAInfo != NULL)
            {
                /* This will not free the crypto map memory, it willl
                   only unlink it from the Engine */
                IkeDeleteRAInfo (pIkeEngine, pIkeRAInfo);
                /* Crypto Map has been deleted, send message to
                   IKE */
                IkeSnmpSendRAInfoToIke (pIkeEngine->u4IkeIndex,
                                        pIkeRAInfo, IKE_CONFIG_DEL_RA_INFO);
                /* Now free the memory */
                MemReleaseMemBlock (IKE_RAINFO_MEMPOOL_ID,
                                    (UINT1 *) pIkeRAInfo);
                return (SNMP_SUCCESS);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInfoPeerIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInfoPeerIdType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInfoPeerIdType (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                INT4 i4TestValFsIkeRAInfoPeerIdType)
{
    if (!((i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_IPV4_ADDR) ||
          (i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_IPV6_ADDR) ||
          (i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_USER_FQDN) ||
          (i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_FQDN) ||
          (i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_DER_ASN1_DN) ||
          (i4TestValFsIkeRAInfoPeerIdType == IKE_IPSEC_ID_KEY_ID)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInfoPeerId
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInfoPeerId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInfoPeerId (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsIkeRAInfoPeerId)
{

    IKE_UNUSED (pTestValFsIkeRAInfoPeerId);

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAXAuthEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAXAuthEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAXAuthEnable (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                             INT4 i4TestValFsIkeRAXAuthEnable)
{

    if ((i4TestValFsIkeRAXAuthEnable != IKE_XAUTH_ENABLE) &&
        (i4TestValFsIkeRAXAuthEnable != IKE_XAUTH_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRACMEnable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRACMEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRACMEnable (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                          INT4 i4TestValFsIkeRACMEnable)
{

    if ((i4TestValFsIkeRACMEnable != IKE_CM_ENABLE) &&
        (i4TestValFsIkeRACMEnable != IKE_CM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAXAuthType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAXAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAXAuthType (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                           INT4 i4TestValFsIkeRAXAuthType)
{

    if (i4TestValFsIkeRAXAuthType != IKE_XAUTH_GENERIC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAXAuthUserName
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAXAuthUserName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAXAuthUserName (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                               tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsIkeRAXAuthUserName)
{

    if (gbXAuthServer == IKE_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if (pTestValFsIkeRAXAuthUserName->i4_Length > MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAXAuthUserPasswd
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAXAuthUserPasswd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAXAuthUserPasswd (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsIkeRAXAuthUserPasswd)
{

    if (gbXAuthServer == IKE_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if (pTestValFsIkeRAXAuthUserPasswd->i4_Length > MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAIPAddressAllocMethod
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAIPAddressAllocMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAIPAddressAllocMethod (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsIkeEngineName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsIkeRAInfoName,
                                      INT4 i4TestValFsIkeRAIPAddressAllocMethod)
{

    if (gbXAuthServer == IKE_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeRAIPAddressAllocMethod != IKE_IPADDR_ALLOC_POOL) &&
        (i4TestValFsIkeRAIPAddressAllocMethod != IKE_IPADDR_ALLOC_STATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInternalIPAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInternalIPAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInternalIPAddrType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                    INT4 i4TestValFsIkeRAInternalIPAddrType)
{

    if (gbXAuthServer == IKE_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeRAInternalIPAddrType != IKE_KEY_IPV4) &&
        (i4TestValFsIkeRAInternalIPAddrType != IKE_KEY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInternalIPAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInternalIPAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInternalIPAddr (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsIkeRAInternalIPAddr)
{

    UINT1               u1AddrType = IKE_ZERO;

    if (gbXAuthServer == IKE_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    pTestValFsIkeRAInternalIPAddr->
        pu1_OctetList[pTestValFsIkeRAInternalIPAddr->i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pTestValFsIkeRAInternalIPAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInternalIPNetMaskType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInternalIPNetMaskType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInternalIPNetMaskType (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIkeEngineName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIkeRAInfoName,
                                       INT4
                                       i4TestValFsIkeRAInternalIPNetMaskType)
{

    if (gbXAuthServer == IKE_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeRAInternalIPNetMaskType != IKE_KEY_IPV4) &&
        (i4TestValFsIkeRAInternalIPNetMaskType != IKE_KEY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInternalIPNetMask
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInternalIPNetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInternalIPNetMask (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                   tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsIkeRAInternalIPNetMask)
{

    UINT1               u1AddrType = IKE_ZERO;

    if (gbXAuthServer == IKE_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    pTestValFsIkeRAInternalIPNetMask->
        pu1_OctetList[pTestValFsIkeRAInternalIPNetMask->i4_Length] = '\0';

    u1AddrType =
        IkeParseAddrString (pTestValFsIkeRAInternalIPNetMask->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pFsIkeEngineName, pFsIkeRAInfoName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInfoProtectedNetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInfoProtectedNetBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInfoProtectedNetBundle (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIkeEngineName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIkeRAInfoName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsIkeRAInfoProtectedNetBundle)
{

    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (pTestValFsIkeRAInfoProtectedNetBundle->i4_Length > MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRATransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRATransformSetBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRATransformSetBundle (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsIkeRATransformSetBundle)
{
    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (pTestValFsIkeRATransformSetBundle->i4_Length > MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAMode (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                      INT4 i4TestValFsIkeRAMode)
{
    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (!((i4TestValFsIkeRAMode == IKE_ONE) ||
          (i4TestValFsIkeRAMode == IKE_TWO)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAPfs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAPfs (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                     INT4 i4TestValFsIkeRAPfs)
{

    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (!((i4TestValFsIkeRAPfs == IKE_ONE) ||
          (i4TestValFsIkeRAPfs == IKE_TWO) ||
          (i4TestValFsIkeRAPfs == IKE_FIVE) ||
          (i4TestValFsIkeRAPfs == IKE_FOURTEEN)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRALifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRALifeTimeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRALifeTimeType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                              INT4 i4TestValFsIkeRALifeTimeType)
{

    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (!((i4TestValFsIkeRALifeTimeType == IKE_ONE) ||
          (i4TestValFsIkeRALifeTimeType == IKE_TWO) ||
          (i4TestValFsIkeRALifeTimeType == IKE_THREE) ||
          (i4TestValFsIkeRALifeTimeType == IKE_FOUR) ||
          (i4TestValFsIkeRALifeTimeType == IKE_FIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRALifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRALifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRALifeTime (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                          UINT4 u4TestValFsIkeRALifeTime)
{

    IKE_UNUSED (pFsIkeEngineName);
    IKE_UNUSED (pFsIkeRAInfoName);

    if (u4TestValFsIkeRALifeTime == IKE_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeRAInfoStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeRAInfoName

                The Object 
                testValFsIkeRAInfoStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeRAInfoStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeRAInfoName,
                            INT4 i4TestValFsIkeRAInfoStatus)
{

    tIkeEngine         *pIkeEngine = NULL;

    /* 
     * Nothing to check in Engine Name or Transform-set Name
     */

    if ((i4TestValFsIkeRAInfoStatus == IKE_CREATEANDGO) ||
        (i4TestValFsIkeRAInfoStatus == IKE_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeRAInfoStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeRAInfoStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkeRAInfoTable (pFsIkeEngineName,
                                                      pFsIkeRAInfoName) ==
            SNMP_SUCCESS)
        {
            if (i4TestValFsIkeRAInfoStatus == IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
        else
        {
            if (i4TestValFsIkeRAInfoStatus != IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeProtectNetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeProtectNetTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIkeProtectNetTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsIkeEngineName,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsIkeProtectNetName)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                                 pFsIkeProtectNetName->
                                                 pu1_OctetList,
                                                 pFsIkeProtectNetName->
                                                 i4_Length);
        if (pIkeRAProtectNet != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeProtectNetTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIkeProtectNetTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsIkeEngineName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsIkeProtectNetName)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeRAProtectNet =
        (tIkeRAProtectNet *) SLL_FIRST (&pIkeEngine->IkeProtectNetList);
    if (pIkeRAProtectNet == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length =
        (INT4) STRLEN (pIkeRAProtectNet->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList,
                pIkeRAProtectNet->au1EngineName, pFsIkeEngineName->i4_Length);
    pFsIkeProtectNetName->i4_Length =
        (INT4) STRLEN (pIkeRAProtectNet->au1ProtectNetName);
    IKE_MEMCPY (pFsIkeProtectNetName->pu1_OctetList,
                pIkeRAProtectNet->au1ProtectNetName,
                pFsIkeProtectNetName->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeProtectNetTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkeProtectNetName
                nextFsIkeProtectNetName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIkeProtectNetTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsIkeEngineName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsIkeProtectNetName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsIkeProtectNetName)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;
    tIkeRAProtectNet   *pNextIkeRAProtectNet = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (IKE_MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeProtectNetList, pIkeRAProtectNet,
              tIkeRAProtectNet *)
    {
        if (IKE_MEMCMP (pIkeRAProtectNet->au1ProtectNetName,
                        pFsIkeProtectNetName->pu1_OctetList,
                        (size_t) pFsIkeProtectNetName->i4_Length) == IKE_ZERO)
        {
            pNextIkeRAProtectNet =
                (tIkeRAProtectNet *) SLL_NEXT (&pIkeEngine->IkeProtectNetList,
                                               (t_SLL_NODE *) pIkeRAProtectNet);
            if (pNextIkeRAProtectNet == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Key of this new engine */
                pNextIkeRAProtectNet =
                    (tIkeRAProtectNet *) SLL_FIRST (&pNextIkeEngine->
                                                    IkeProtectNetList);
                if (pNextIkeRAProtectNet == NULL)
                {
                    return (SNMP_FAILURE);
                }
            }

            pNextFsIkeEngineName->i4_Length =
                (INT4) STRLEN (pNextIkeRAProtectNet->au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkeRAProtectNet->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            pNextFsIkeProtectNetName->i4_Length =
                (INT4) STRLEN (pNextIkeRAProtectNet->au1ProtectNetName);
            IKE_MEMCPY (pNextFsIkeProtectNetName->pu1_OctetList,
                        pNextIkeRAProtectNet->au1ProtectNetName,
                        pNextFsIkeProtectNetName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeProtectNetType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                retValFsIkeProtectNetType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeProtectNetType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                           INT4 *pi4RetValFsIkeProtectNetType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pFsIkeProtectNetName->
                                             pu1_OctetList,
                                             pFsIkeProtectNetName->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        *pi4RetValFsIkeProtectNetType =
            (INT4) pIkeRAProtectNet->ProtectedNetwork.u4Type;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetFsIkeProtectNetAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                retValFsIkeProtectNetAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeProtectNetAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsIkeProtectNetAddr)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pFsIkeProtectNetName->
                                             pu1_OctetList,
                                             pFsIkeProtectNetName->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        pRetValFsIkeProtectNetAddr->i4_Length =
            (INT4) STRLEN (pIkeRAProtectNet->au1LocalNetworkDisplay);
        MEMCPY (pRetValFsIkeProtectNetAddr->pu1_OctetList,
                pIkeRAProtectNet->au1LocalNetworkDisplay,
                pRetValFsIkeProtectNetAddr->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetFsIkeProtectNetStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                retValFsIkeProtectNetStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeProtectNetStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                             INT4 *pi4RetValFsIkeProtectNetStatus)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pFsIkeProtectNetName->
                                             pu1_OctetList,
                                             pFsIkeProtectNetName->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        *pi4RetValFsIkeProtectNetStatus = (INT4) pIkeRAProtectNet->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeProtectNetType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                setValFsIkeProtectNetType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeProtectNetType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                           INT4 i4SetValFsIkeProtectNetType)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pFsIkeProtectNetName->
                                             pu1_OctetList,
                                             pFsIkeProtectNetName->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        pIkeRAProtectNet->ProtectedNetwork.u4Type =
            (UINT4) i4SetValFsIkeProtectNetType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeProtectNetAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                setValFsIkeProtectNetAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeProtectNetAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsIkeProtectNetAddr)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pFsIkeProtectNetName->
                                             pu1_OctetList,
                                             pFsIkeProtectNetName->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        if (IkeProtectNetSetNetworkAddress
            (&(pIkeRAProtectNet->ProtectedNetwork),
             pSetValFsIkeProtectNetAddr->pu1_OctetList,
             pSetValFsIkeProtectNetAddr->i4_Length) == IKE_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Copy onto string for show purposes */
        if (pSetValFsIkeProtectNetAddr->i4_Length >= MAX_NAME_LENGTH)
        {
            return (SNMP_FAILURE);
        }
        u4TempLen =
            MEM_MAX_BYTES ((UINT4) pSetValFsIkeProtectNetAddr->i4_Length,
                           STRLEN (pSetValFsIkeProtectNetAddr->pu1_OctetList));
        MEMCPY (pIkeRAProtectNet->au1LocalNetworkDisplay,
                pSetValFsIkeProtectNetAddr->pu1_OctetList, u4TempLen);
        pIkeRAProtectNet->au1LocalNetworkDisplay[u4TempLen] = '\0';

        if (IkeCheckProtectNetAttributes (pIkeRAProtectNet) == IKE_SUCCESS)
        {
            if (pIkeRAProtectNet->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeRAProtectNet->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeRAProtectNet->ProtectedNetwork.u4Type != IKE_ZERO)
                {
                    IkeSnmpSendProtectNetInfoToIke (pIkeEngine->u4IkeIndex,
                                                    pIkeRAProtectNet,
                                                    IKE_CONFIG_CHG_PROTECT_NET);

                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeProtectNetStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                setValFsIkeProtectNetStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeProtectNetStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                             INT4 i4SetValFsIkeProtectNetStatus)
{

    tIkeEngine         *pIkeEngine = NULL;

    /* Test routine will already have checked whether this entry exists, 
       so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeProtectNetStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

            pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                                     pFsIkeProtectNetName->
                                                     pu1_OctetList,
                                                     pFsIkeProtectNetName->
                                                     i4_Length);
            if (pIkeRAProtectNet != NULL)
            {
                return (SNMP_FAILURE);
            }

            if (MemAllocateMemBlock
                (IKE_PROTECTNET_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeRAProtectNet) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeProtectNetStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }
            if (pIkeRAProtectNet == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkeRAProtectNet, IKE_ZERO, sizeof (tIkeRAProtectNet));

            /* Set the Index in the entry */
            MEMCPY (pIkeRAProtectNet->au1ProtectNetName,
                    pFsIkeProtectNetName->pu1_OctetList,
                    pFsIkeProtectNetName->i4_Length);
            pIkeRAProtectNet->au1ProtectNetName[pFsIkeProtectNetName->
                                                i4_Length] = '\0';

            /* Set the Engine Name in the entry */
            MEMCPY (pIkeRAProtectNet->au1EngineName,
                    pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkeRAProtectNet->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkeRAProtectNet->u1Status = IKE_NOTREADY;
            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeProtectNetList),
                     (t_SLL_NODE *) pIkeRAProtectNet);
            GIVE_IKE_SEM ();
            return (SNMP_SUCCESS);
        }

        case IKE_ACTIVE:
        {
            tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

            pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                                     pFsIkeProtectNetName->
                                                     pu1_OctetList,
                                                     pFsIkeProtectNetName->
                                                     i4_Length);
            if (pIkeRAProtectNet == NULL)
            {
                return (SNMP_FAILURE);
            }

            /* Check if required attributes are configured, other than 
               the default */
            if (IkeCheckProtectNetAttributes (pIkeRAProtectNet) == IKE_SUCCESS)
            {
                pIkeRAProtectNet->u1Status = IKE_ACTIVE;
                return (SNMP_SUCCESS);
            }
            else
            {
                /* All attributes have not been set, return FAILURE */
                return (SNMP_FAILURE);
            }
        }

        case IKE_NOTINSERVICE:
        {
            tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

            pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                                     pFsIkeProtectNetName->
                                                     pu1_OctetList,
                                                     pFsIkeProtectNetName->
                                                     i4_Length);
            if (pIkeRAProtectNet == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeRAProtectNet->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeRAProtectNet->u1Status)
            {
                pIkeRAProtectNet->u1Status = IKE_NOTINSERVICE;
                IkeSnmpSendProtectNetInfoToIke (pIkeEngine->u4IkeIndex,
                                                pIkeRAProtectNet,
                                                IKE_CONFIG_DEL_PROTECT_NET);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

            pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                                     pFsIkeProtectNetName->
                                                     pu1_OctetList,
                                                     pFsIkeProtectNetName->
                                                     i4_Length);
            if (pIkeRAProtectNet != NULL)
            {
                /* This will not free the crypto map memory, it willl
                   only unlink it from the Engine */
                IkeDeleteProtectNet (pIkeEngine, pIkeRAProtectNet);
                /* Crypto Map has been deleted, send message to
                   IKE */
                IkeSnmpSendProtectNetInfoToIke (pIkeEngine->u4IkeIndex,
                                                pIkeRAProtectNet,
                                                IKE_CONFIG_DEL_PROTECT_NET);
                /* Now free the memory */
                MemReleaseMemBlock (IKE_PROTECTNET_MEMPOOL_ID,
                                    (UINT1 *) pIkeRAProtectNet);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeProtectNetType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                testValFsIkeProtectNetType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeProtectNetType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                              INT4 i4TestValFsIkeProtectNetType)
{

    if ((i4TestValFsIkeProtectNetType != IPV4ADDR) &&
        (i4TestValFsIkeProtectNetType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeProtectNetTable
        (pFsIkeEngineName, pFsIkeProtectNetName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeProtectNetAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                testValFsIkeProtectNetAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeProtectNetAddr (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsIkeProtectNetAddr)
{

    UINT1               u1AddrType = IKE_ZERO;

    u1AddrType =
        IkeParseNetworkAddr (pTestValFsIkeProtectNetAddr->pu1_OctetList,
                             pTestValFsIkeProtectNetAddr->i4_Length);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsIkeProtectNetTable
        (pFsIkeEngineName, pFsIkeProtectNetName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeProtectNetStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeProtectNetName

                The Object 
                testValFsIkeProtectNetStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeProtectNetStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE * pFsIkeProtectNetName,
                                INT4 i4TestValFsIkeProtectNetStatus)
{

    tIkeEngine         *pIkeEngine = NULL;

    if ((i4TestValFsIkeProtectNetStatus == IKE_CREATEANDGO) ||
        (i4TestValFsIkeProtectNetStatus == IKE_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeProtectNetStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeProtectNetStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkeProtectNetTable (pFsIkeEngineName,
                                                          pFsIkeProtectNetName)
            == SNMP_SUCCESS)
        {
            if (i4TestValFsIkeProtectNetStatus == IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
        else
        {
            if (i4TestValFsIkeProtectNetStatus != IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeKeyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeKeyTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkeKeyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeKeyTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsIkeEngineName,
                                       tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId)
#else
INT1
nmhValidateIndexInstanceFsIkeKeyTable (pFsIkeEngineName, pFsIkeKeyId)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeKey =
            IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                           pFsIkeKeyId->i4_Length);
        if (pIkeKey != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeKeyTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkeKeyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeKeyTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                               tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId)
#else
INT1
nmhGetFirstIndexFsIkeKeyTable (pFsIkeEngineName, pFsIkeKeyId)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeKey = (tIkeKey *) SLL_FIRST (&pIkeEngine->IkeKeyList);
    if (pIkeKey == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length = (INT4) STRLEN (pIkeKey->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList, pIkeKey->au1EngineName,
                pFsIkeEngineName->i4_Length);
    pFsIkeKeyId->i4_Length = (INT4) STRLEN (pIkeKey->au1DisplayKeyId);
    IKE_MEMCPY (pFsIkeKeyId->pu1_OctetList, pIkeKey->au1DisplayKeyId,
                pFsIkeKeyId->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeKeyTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkeKeyId
                nextFsIkeKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkeKeyTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeKeyTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pNextFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                              tSNMP_OCTET_STRING_TYPE * pNextFsIkeKeyId)
#else
INT1
nmhGetNextIndexFsIkeKeyTable (pFsIkeEngineName, pNextFsIkeEngineName,
                              pFsIkeKeyId, pNextFsIkeKeyId)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeKeyId;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    tIkeKey            *pNextIkeKey = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (IKE_MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeKeyList, pIkeKey, tIkeKey *)
    {
        if (IKE_MEMCMP (pIkeKey->au1DisplayKeyId, pFsIkeKeyId->pu1_OctetList,
                        (size_t) pFsIkeKeyId->i4_Length) == IKE_ZERO)
        {
            pNextIkeKey =
                (tIkeKey *) SLL_NEXT (&pIkeEngine->IkeKeyList,
                                      (t_SLL_NODE *) pIkeKey);
            if (pNextIkeKey == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Key of this new engine */
                pNextIkeKey =
                    (tIkeKey *) SLL_FIRST (&pNextIkeEngine->IkeKeyList);
                if (pNextIkeKey == NULL)
                {
                    return (SNMP_FAILURE);
                }

            }

            pNextFsIkeEngineName->i4_Length = (INT4) STRLEN (pNextIkeKey->
                                                             au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkeKey->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            pNextFsIkeKeyId->i4_Length =
                (INT4) STRLEN (pNextIkeKey->au1DisplayKeyId);
            IKE_MEMCPY (pNextFsIkeKeyId->pu1_OctetList,
                        pNextIkeKey->au1DisplayKeyId,
                        pNextFsIkeKeyId->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeKeyIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                retValFsIkeKeyIdType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeKeyIdType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeKeyIdType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      INT4 *pi4RetValFsIkeKeyIdType)
#else
INT1
nmhGetFsIkeKeyIdType (pFsIkeEngineName, pFsIkeKeyId, pi4RetValFsIkeKeyIdType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4               *pi4RetValFsIkeKeyIdType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeKey =
        IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                       pFsIkeKeyId->i4_Length);
    if (pIkeKey != NULL)
    {
        *pi4RetValFsIkeKeyIdType = pIkeKey->KeyID.i4IdType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeKeyString
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                retValFsIkeKeyString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeKeyString ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeKeyString (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsIkeKeyString)
#else
INT1
nmhGetFsIkeKeyString (pFsIkeEngineName, pFsIkeKeyId, pRetValFsIkeKeyString)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeKeyString;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeKey =
        IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                       pFsIkeKeyId->i4_Length);
    if (pIkeKey != NULL)
    {
        /* We don't want to reveal the key, just send back a dummy string */
        STRCPY (pRetValFsIkeKeyString->pu1_OctetList, IKE_KEY_DUMMY_STRING);
        pRetValFsIkeKeyString->i4_Length = (INT4) STRLEN (IKE_KEY_DUMMY_STRING);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeKeyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                retValFsIkeKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeKeyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeKeyStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      INT4 *pi4RetValFsIkeKeyStatus)
#else
INT1
nmhGetFsIkeKeyStatus (pFsIkeEngineName, pFsIkeKeyId, pi4RetValFsIkeKeyStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4               *pi4RetValFsIkeKeyStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeKey =
        IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                       pFsIkeKeyId->i4_Length);
    if (pIkeKey != NULL)
    {
        *pi4RetValFsIkeKeyStatus = (INT4) pIkeKey->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeKeyIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                setValFsIkeKeyIdType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeKeyIdType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeKeyIdType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      INT4 i4SetValFsIkeKeyIdType)
#else
INT1
nmhSetFsIkeKeyIdType (pFsIkeEngineName, pFsIkeKeyId, i4SetValFsIkeKeyIdType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4                i4SetValFsIkeKeyIdType;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeKey =
        IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                       pFsIkeKeyId->i4_Length);
    if (pIkeKey != NULL)
    {
        switch (i4SetValFsIkeKeyIdType)
        {
            case IKE_KEY_IPV4:
            {
                tIp4Addr            u4TempIp4Addr = IKE_ZERO;

                i4RetVal =
                    INET_PTON4 (pIkeKey->au1DisplayKeyId, &u4TempIp4Addr);
                if (i4RetVal > IKE_ZERO)
                {
                    pIkeKey->KeyID.uID.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
                    pIkeKey->KeyID.u4Length = sizeof (tIp4Addr);
                }
                else
                {
                    return (SNMP_FAILURE);
                }
                break;
            }

            case IKE_KEY_IPV6:
            {
                tIp6Addr            TempIp6Addr;

                i4RetVal = INET_PTON6 (pIkeKey->au1DisplayKeyId,
                                       &(TempIp6Addr.u1_addr));
                if (i4RetVal > IKE_ZERO)
                {
                    MEMCPY (&pIkeKey->KeyID.uID.Ip6Addr,
                            &TempIp6Addr, sizeof (tIp6Addr));
                    pIkeKey->KeyID.u4Length = sizeof (tIp6Addr);
                }
                else
                {
                    return (SNMP_FAILURE);
                }
                break;
            }

            case IKE_KEY_EMAIL:
            {
                STRCPY (pIkeKey->KeyID.uID.au1Email, pIkeKey->au1DisplayKeyId);
                pIkeKey->KeyID.u4Length = STRLEN (pIkeKey->au1DisplayKeyId);
                break;
            }

            case IKE_KEY_FQDN:
            {
                STRCPY (pIkeKey->KeyID.uID.au1Fqdn, pIkeKey->au1DisplayKeyId);
                pIkeKey->KeyID.u4Length = STRLEN (pIkeKey->au1DisplayKeyId);
                break;
            }

            case IKE_KEY_DN:
            {
                STRCPY (pIkeKey->KeyID.uID.au1Dn, pIkeKey->au1DisplayKeyId);
                pIkeKey->KeyID.u4Length = STRLEN (pIkeKey->au1DisplayKeyId);
                break;
            }
            case IKE_KEY_KEYID:
            {
                STRCPY (pIkeKey->KeyID.uID.au1KeyId, pIkeKey->au1DisplayKeyId);
                pIkeKey->KeyID.u4Length = STRLEN (pIkeKey->au1DisplayKeyId);
                break;
            }
            default:
                break;
        }
        pIkeKey->KeyID.i4IdType = i4SetValFsIkeKeyIdType;

        if (IkeCheckKeyAttributes (pIkeKey) == IKE_SUCCESS)
        {
            /* All required attributes set, ready to go ACTIVE */
            pIkeKey->u1Status = IKE_NOTINSERVICE;
        }
        return (SNMP_SUCCESS);
    }
    else
        return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsIkeKeyString
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                setValFsIkeKeyString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeKeyString ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeKeyString (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsIkeKeyString)
#else
INT1
nmhSetFsIkeKeyString (pFsIkeEngineName, pFsIkeKeyId, pSetValFsIkeKeyString)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeKeyString;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    tIkeConfigIfParam   IkeConfigIfParam;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeKey =
        IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                       pFsIkeKeyId->i4_Length);
    if (pIkeKey != NULL)
    {
        MEMCPY (pIkeKey->au1KeyString, pSetValFsIkeKeyString->pu1_OctetList,
                pSetValFsIkeKeyString->i4_Length);
        pIkeKey->au1KeyString[pSetValFsIkeKeyString->i4_Length] = '\0';
        pIkeKey->bKeyStringSet = IKE_TRUE;
        if (IkeCheckKeyAttributes (pIkeKey) == IKE_SUCCESS)
        {
            if (pIkeKey->u1Status != IKE_ACTIVE)
            {
                /* All required attributes set, ready to go ACTIVE */
                pIkeKey->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                /* Key has changed, send mesg to IKE Task */
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_CHG_PRESHARED_KEY;
                IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeConfPreSharedkey.
                            KeyID, &pIkeKey->KeyID, sizeof (tIkePhase1ID));
                IkeSendConfMsgToIke (&IkeConfigIfParam);
            }
        }
        return (SNMP_SUCCESS);
    }
    else
        return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsIkeKeyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                setValFsIkeKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeKeyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeKeyStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                      INT4 i4SetValFsIkeKeyStatus)
#else
INT1
nmhSetFsIkeKeyStatus (pFsIkeEngineName, pFsIkeKeyId, i4SetValFsIkeKeyStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4                i4SetValFsIkeKeyStatus;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeConfigIfParam   IkeConfigIfParam;
    UINT4               u4TempLen = IKE_ZERO;

    /* Test routine will already have checked whether this entry exists, 
       so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeKeyStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeKey            *pIkeKey = NULL;

            pIkeKey =
                IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                               pFsIkeKeyId->i4_Length);
            if (pIkeKey != NULL)
            {
                return (SNMP_FAILURE);
            }

            if (MemAllocateMemBlock
                (IKE_KEY_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeKey) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeKeyStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }

            if (pIkeKey == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkeKey, IKE_ZERO, sizeof (tIkeKey));

            /* Set the Index in the entry */
            u4TempLen = MEM_MAX_BYTES ((UINT4) pFsIkeKeyId->i4_Length,
                                       STRLEN (pFsIkeKeyId->pu1_OctetList));
            MEMCPY (pIkeKey->au1DisplayKeyId,
                    pFsIkeKeyId->pu1_OctetList, u4TempLen);
            pIkeKey->au1DisplayKeyId[u4TempLen] = '\0';

            /* Set the Engine Name in the entry */
            if (STRLEN (pIkeKey->au1EngineName) > MAX_NAME_LENGTH)
            {

                return (SNMP_FAILURE);
            }
            u4TempLen =
                MEM_MAX_BYTES ((UINT4) pFsIkeEngineName->i4_Length,
                               STRLEN (pFsIkeEngineName->pu1_OctetList));
            MEMCPY (pIkeKey->au1EngineName, pFsIkeEngineName->pu1_OctetList,
                    u4TempLen);
            pIkeKey->au1EngineName[u4TempLen] = '\0';
            pIkeKey->u1Status = IKE_NOTREADY;
            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeKeyList), (t_SLL_NODE *) pIkeKey);
            GIVE_IKE_SEM ();
            return (SNMP_SUCCESS);
        }

        case IKE_ACTIVE:
        {
            tIkeKey            *pIkeKey = NULL;

            pIkeKey =
                IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                               pFsIkeKeyId->i4_Length);
            if (pIkeKey == NULL)
            {
                return (SNMP_FAILURE);
            }

            /* 
             * Check if required attributes are configured, other than 
             * the default 
             */
            if (IkeCheckKeyAttributes (pIkeKey) == IKE_SUCCESS)
            {
                if (gu4VpnRestoredConfig == OSIX_TRUE)
                {
                    /*Write preshared key into file if 
                     * callback registration is done*/

                    if (IkeUtilCallBack
                        (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM) != IKE_SUCCESS)
                    {
                        return (SNMP_FAILURE);
                    }
                }
                pIkeKey->u1Status = IKE_ACTIVE;
                return (SNMP_SUCCESS);
            }
            else
            {
                /* All attributes have not been set, return FAILURE */
                return (SNMP_FAILURE);
            }
        }

        case IKE_NOTINSERVICE:
        {
            tIkeKey            *pIkeKey = NULL;

            pIkeKey =
                IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                               pFsIkeKeyId->i4_Length);
            if (pIkeKey == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeKey->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeKey->u1Status)
            {
                pIkeKey->u1Status = IKE_NOTINSERVICE;
                /* Key has been deactivated, send mesg to IKE Task */
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_PRESHARED_KEY;
                IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeConfPreSharedkey.
                            KeyID, &pIkeKey->KeyID, sizeof (tIkePhase1ID));
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkeKey            *pIkeKey = NULL;

            pIkeKey =
                IkeSnmpGetKey (pIkeEngine, pFsIkeKeyId->pu1_OctetList,
                               pFsIkeKeyId->i4_Length);
            if (pIkeKey != NULL)
            {
                /* Key has been destroyed, send mesg to IKE Task */
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_PRESHARED_KEY;
                IkeConfigIfParam.ConfigReq.IkeConfPreSharedkey.
                    u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
                IkeConfigIfParam.ConfigReq.IkeConfPreSharedkey.KeyID.i4IdType =
                    pIkeKey->KeyID.i4IdType;
                IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeConfPreSharedkey.
                            KeyID, &pIkeKey->KeyID, sizeof (tIkePhase1ID));
                IkeDeleteIkeKeyMsr (pIkeKey);
                IkeDeleteKey (pIkeEngine, pIkeKey);
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeKeyIdType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                testValFsIkeKeyIdType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeKeyIdType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeKeyIdType (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                         INT4 i4TestValFsIkeKeyIdType)
#else
INT1
nmhTestv2FsIkeKeyIdType (pu4ErrorCode, pFsIkeEngineName, pFsIkeKeyId,
                         i4TestValFsIkeKeyIdType)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4                i4TestValFsIkeKeyIdType;
#endif
{
    if ((i4TestValFsIkeKeyIdType < IKE_KEY_IPV4)
        || (i4TestValFsIkeKeyIdType > IKE_KEY_KEYID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeKeyTable (pFsIkeEngineName, pFsIkeKeyId) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeKeyString
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                testValFsIkeKeyString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeKeyString ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeKeyString (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsIkeKeyString)
#else
INT1
nmhTestv2FsIkeKeyString (pu4ErrorCode, pFsIkeEngineName, pFsIkeKeyId,
                         pTestValFsIkeKeyString)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeKeyString;
#endif
{
    if (pTestValFsIkeKeyString->i4_Length < IKE_MIN_KEYLEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIkeKeyTable (pFsIkeEngineName, pFsIkeKeyId) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeKeyStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeKeyId

                The Object 
                testValFsIkeKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeKeyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeKeyStatus (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeKeyId,
                         INT4 i4TestValFsIkeKeyStatus)
#else
INT1
nmhTestv2FsIkeKeyStatus (pu4ErrorCode, pFsIkeEngineName, pFsIkeKeyId,
                         i4TestValFsIkeKeyStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeKeyId;
     INT4                i4TestValFsIkeKeyStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    /* 
     * Nothing to check in Engine Name or Key ID
     */
    if ((i4TestValFsIkeKeyStatus == IKE_CREATEANDGO) ||
        (i4TestValFsIkeKeyStatus == IKE_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeKeyStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeKeyStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkeKeyTable (pFsIkeEngineName,
                                                   pFsIkeKeyId) == SNMP_SUCCESS)
        {
            if (i4TestValFsIkeKeyStatus == IKE_CREATEANDWAIT)
            {
                nmhSetFsIkeKeyStatus (pFsIkeEngineName, pFsIkeKeyId,
                                      IKE_DESTROY);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
        else
        {
            if (i4TestValFsIkeKeyStatus != IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeTransformSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeTransformSetTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/* $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkeTransformSetTable */
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeTransformSetTable (tSNMP_OCTET_STRING_TYPE *
                                                pFsIkeEngineName,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsIkeTransformSetName)
#else
INT1
nmhValidateIndexInstanceFsIkeTransformSetTable (pFsIkeEngineName,
                                                pFsIkeTransformSetName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                                   pFsIkeTransformSetName->
                                                   pu1_OctetList,
                                                   pFsIkeTransformSetName->
                                                   i4_Length);
        if (pIkeTransformSet != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeTransformSetTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkeTransformSetTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeTransformSetTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsIkeEngineName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIkeTransformSetName)
#else
INT1
nmhGetFirstIndexFsIkeTransformSetTable (pFsIkeEngineName,
                                        pFsIkeTransformSetName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeTransformSet =
        (tIkeTransformSet *) SLL_FIRST (&pIkeEngine->IkeTransformSetList);
    if (pIkeTransformSet == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length =
        (INT4) STRLEN (pIkeTransformSet->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList,
                pIkeTransformSet->au1EngineName, pFsIkeEngineName->i4_Length);
    pFsIkeTransformSetName->i4_Length =
        (INT4) STRLEN (pIkeTransformSet->au1TransformSetName);
    IKE_MEMCPY (pFsIkeTransformSetName->pu1_OctetList,
                pIkeTransformSet->au1TransformSetName,
                pFsIkeTransformSetName->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeTransformSetTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkeTransformSetName
                nextFsIkeTransformSetName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkeTransformSetTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeTransformSetTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsIkeEngineName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsIkeEngineName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIkeTransformSetName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsIkeTransformSetName)
#else
INT1
nmhGetNextIndexFsIkeTransformSetTable (pFsIkeEngineName, pNextFsIkeEngineName,
                                       pFsIkeTransformSetName,
                                       pNextFsIkeTransformSetName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeTransformSetName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;
    tIkeTransformSet   *pNextIkeTransformSet = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (IKE_MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeTransformSetList, pIkeTransformSet,
              tIkeTransformSet *)
    {
        if (IKE_MEMCMP (pIkeTransformSet->au1TransformSetName,
                        pFsIkeTransformSetName->pu1_OctetList,
                        (size_t) pFsIkeTransformSetName->i4_Length) == IKE_ZERO)
        {
            pNextIkeTransformSet =
                (tIkeTransformSet *) SLL_NEXT (&pIkeEngine->IkeTransformSetList,
                                               (t_SLL_NODE *) pIkeTransformSet);
            if (pNextIkeTransformSet == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Key of this new engine */
                pNextIkeTransformSet =
                    (tIkeTransformSet *) SLL_FIRST (&pNextIkeEngine->
                                                    IkeTransformSetList);
                if (pNextIkeTransformSet == NULL)
                {
                    return (SNMP_FAILURE);
                }
            }

            pNextFsIkeEngineName->i4_Length =
                (INT4) STRLEN (pNextIkeTransformSet->au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkeTransformSet->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            pNextFsIkeTransformSetName->i4_Length =
                (INT4) STRLEN (pNextIkeTransformSet->au1TransformSetName);
            IKE_MEMCPY (pNextFsIkeTransformSetName->pu1_OctetList,
                        pNextIkeTransformSet->au1TransformSetName,
                        pNextFsIkeTransformSetName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeTSEspHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                retValFsIkeTSEspHashAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeTSEspHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeTSEspHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                          INT4 *pi4RetValFsIkeTSEspHashAlgo)
#else
INT1
nmhGetFsIkeTSEspHashAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                          pi4RetValFsIkeTSEspHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4               *pi4RetValFsIkeTSEspHashAlgo;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        *pi4RetValFsIkeTSEspHashAlgo = (INT4) pIkeTransformSet->u1EspHashAlgo;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeTSEspEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                retValFsIkeTSEspEncryptionAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeTSEspEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeTSEspEncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsIkeTransformSetName,
                                INT4 *pi4RetValFsIkeTSEspEncryptionAlgo)
#else
INT1
nmhGetFsIkeTSEspEncryptionAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                                pi4RetValFsIkeTSEspEncryptionAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4               *pi4RetValFsIkeTSEspEncryptionAlgo;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        *pi4RetValFsIkeTSEspEncryptionAlgo =
            (INT4) pIkeTransformSet->u1EspEncryptionAlgo;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeTSAhHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                retValFsIkeTSAhHashAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeTSAhHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeTSAhHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                         INT4 *pi4RetValFsIkeTSAhHashAlgo)
#else
INT1
nmhGetFsIkeTSAhHashAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                         pi4RetValFsIkeTSAhHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4               *pi4RetValFsIkeTSAhHashAlgo;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        *pi4RetValFsIkeTSAhHashAlgo = (INT4) pIkeTransformSet->u1AhHashAlgo;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeTSStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                retValFsIkeTSStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeTSStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeTSStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                     INT4 *pi4RetValFsIkeTSStatus)
#else
INT1
nmhGetFsIkeTSStatus (pFsIkeEngineName, pFsIkeTransformSetName,
                     pi4RetValFsIkeTSStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4               *pi4RetValFsIkeTSStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        *pi4RetValFsIkeTSStatus = (INT4) pIkeTransformSet->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeTSEspHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                setValFsIkeTSEspHashAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeTSEspHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeTSEspHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                          INT4 i4SetValFsIkeTSEspHashAlgo)
#else
INT1
nmhSetFsIkeTSEspHashAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                          i4SetValFsIkeTSEspHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4SetValFsIkeTSEspHashAlgo;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        pIkeTransformSet->u1EspHashAlgo = (UINT1) i4SetValFsIkeTSEspHashAlgo;
        if (IkeCheckTransformSetAttributes (pIkeTransformSet) == IKE_SUCCESS)
        {
            if (pIkeTransformSet->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeTransformSet->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                /* TS conf has changed, send mesg to IKE */
                IkeSnmpSendTransformInfoToIke (pIkeEngine->u4IkeIndex,
                                               IKE_CONFIG_CHG_TRANSFORM_SET);
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeTSEspEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                setValFsIkeTSEspEncryptionAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeTSEspEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeTSEspEncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsIkeTransformSetName,
                                INT4 i4SetValFsIkeTSEspEncryptionAlgo)
#else
INT1
nmhSetFsIkeTSEspEncryptionAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                                i4SetValFsIkeTSEspEncryptionAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4SetValFsIkeTSEspEncryptionAlgo;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTS = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTS = IkeSnmpGetTransformSet (pIkeEngine,
                                     pFsIkeTransformSetName->
                                     pu1_OctetList,
                                     pFsIkeTransformSetName->i4_Length);
    if (pIkeTS != NULL)
    {
        if (i4SetValFsIkeTSEspEncryptionAlgo == IKE_AES_KEY_LEN_128)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES;
            pIkeTS->u2KeyLen = IKE_AES_KEY_LEN_128;
        }
        else if (i4SetValFsIkeTSEspEncryptionAlgo == IKE_AES_DEF_KEY_LEN_VAL)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES;
            pIkeTS->u2KeyLen = IKE_AES_DEF_KEY_LEN_VAL;
        }
        else if (i4SetValFsIkeTSEspEncryptionAlgo == IKE_AES_KEY_LEN_256)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES;
            pIkeTS->u2KeyLen = IKE_AES_KEY_LEN_256;
        }
        else if (i4SetValFsIkeTSEspEncryptionAlgo == SEC_AESCTR)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES_CTR;
            pIkeTS->u2KeyLen = IKE_AES_KEY_LEN_128;
        }
        else if (i4SetValFsIkeTSEspEncryptionAlgo == SEC_AESCTR192)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES_CTR;
            pIkeTS->u2KeyLen = IKE_AES_DEF_KEY_LEN_VAL;
        }
        else if (i4SetValFsIkeTSEspEncryptionAlgo == SEC_AESCTR256)
        {
            pIkeTS->u1EspEncryptionAlgo = IKE_IPSEC_ESP_AES_CTR;
            pIkeTS->u2KeyLen = IKE_AES_KEY_LEN_256;
        }
        else
        {
            pIkeTS->u1EspEncryptionAlgo =
                (UINT1) i4SetValFsIkeTSEspEncryptionAlgo;
        }

        if (IkeCheckTransformSetAttributes (pIkeTS) == IKE_SUCCESS)
        {
            if (pIkeTS->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeTS->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                /* TS conf has changed, send mesg to IKE */
                IkeSnmpSendTransformInfoToIke (pIkeEngine->u4IkeIndex,
                                               IKE_CONFIG_CHG_TRANSFORM_SET);
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeTSAhHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                setValFsIkeTSAhHashAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeTSAhHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeTSAhHashAlgo (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                         INT4 i4SetValFsIkeTSAhHashAlgo)
#else
INT1
nmhSetFsIkeTSAhHashAlgo (pFsIkeEngineName, pFsIkeTransformSetName,
                         i4SetValFsIkeTSAhHashAlgo)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4SetValFsIkeTSAhHashAlgo;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                               pFsIkeTransformSetName->
                                               pu1_OctetList,
                                               pFsIkeTransformSetName->
                                               i4_Length);
    if (pIkeTransformSet != NULL)
    {
        pIkeTransformSet->u1AhHashAlgo = (UINT1) i4SetValFsIkeTSAhHashAlgo;
        if (IkeCheckTransformSetAttributes (pIkeTransformSet) == IKE_SUCCESS)
        {
            if (pIkeTransformSet->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeTransformSet->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                /* TS conf has changed, send mesg to IKE */
                IkeSnmpSendTransformInfoToIke (pIkeEngine->u4IkeIndex,
                                               IKE_CONFIG_CHG_TRANSFORM_SET);
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeTSStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                setValFsIkeTSStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeTSStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeTSStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                     INT4 i4SetValFsIkeTSStatus)
#else
INT1
nmhSetFsIkeTSStatus (pFsIkeEngineName, pFsIkeTransformSetName,
                     i4SetValFsIkeTSStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4SetValFsIkeTSStatus;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    /* Test routine will already have checked whether this entry exists, 
     * so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeTSStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeTransformSet   *pIkeTransformSet = NULL;

            pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                                       pFsIkeTransformSetName->
                                                       pu1_OctetList,
                                                       pFsIkeTransformSetName->
                                                       i4_Length);
            if (pIkeTransformSet != NULL)
            {
                return (SNMP_FAILURE);
            }
            if (MemAllocateMemBlock
                (IKE_TRANSFORMSET_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeTransformSet) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeTSStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }
            if (pIkeTransformSet == NULL)
            {
                return (SNMP_FAILURE);
            }

            MEMSET (pIkeTransformSet, IKE_ZERO, sizeof (tIkeTransformSet));

            /* Set the Index in the entry */
            MEMCPY (pIkeTransformSet->au1TransformSetName,
                    pFsIkeTransformSetName->pu1_OctetList,
                    pFsIkeTransformSetName->i4_Length);
            pIkeTransformSet->au1TransformSetName[pFsIkeTransformSetName->
                                                  i4_Length] = '\0';

            /* Set the Engine Name in the entry */
            MEMCPY (pIkeTransformSet->au1EngineName,
                    pFsIkeEngineName->pu1_OctetList,
                    pFsIkeEngineName->i4_Length);
            pIkeTransformSet->au1EngineName[pFsIkeEngineName->i4_Length] = '\0';
            pIkeTransformSet->u1Status = IKE_NOTREADY;
            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeTransformSetList),
                     (t_SLL_NODE *) pIkeTransformSet);
            GIVE_IKE_SEM ();
            return (SNMP_SUCCESS);
        }

        case IKE_ACTIVE:
        {
            tIkeTransformSet   *pIkeTransformSet = NULL;

            pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                                       pFsIkeTransformSetName->
                                                       pu1_OctetList,
                                                       pFsIkeTransformSetName->
                                                       i4_Length);
            if (pIkeTransformSet == NULL)
            {
                return (SNMP_FAILURE);
            }

            /* Check if required attributes are configured, 
             * other than the default */
            if (IkeCheckTransformSetAttributes (pIkeTransformSet) ==
                IKE_SUCCESS)
            {
                pIkeTransformSet->u1Status = IKE_ACTIVE;
                return (SNMP_SUCCESS);
            }
            else
            {
                /* All attributes have not been set, return FAILURE */
                return (SNMP_FAILURE);
            }
        }

        case IKE_NOTINSERVICE:
        {
            tIkeTransformSet   *pIkeTransformSet = NULL;
            tIkePolicy         *pIkePolicy = NULL;
            tIkeConfCryptoMap   IkeConfCryptoMap;

            MEMSET (&IkeConfCryptoMap, IKE_ZERO, sizeof (tIkeConfCryptoMap));

            pIkePolicy = IkeSnmpGetPolicyFromName (pIkeEngine,
                                                   pFsIkeTransformSetName->
                                                   pu1_OctetList,
                                                   pFsIkeTransformSetName->
                                                   i4_Length);

            if (pIkePolicy == NULL)
            {
                return SNMP_FAILURE;
            }

            IkeConfCryptoMap.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
            IkeConfCryptoMap.PeerIpAddr = pIkePolicy->PeerAddr;

            pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                                       pFsIkeTransformSetName->
                                                       pu1_OctetList,
                                                       pFsIkeTransformSetName->
                                                       i4_Length);
            if (pIkeTransformSet == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeTransformSet->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeTransformSet->u1Status)
            {
                pIkeTransformSet->u1Status = IKE_NOTINSERVICE;
                /* TS conf has been deactivated, send mesg to IKE */
                IkeSnmpSendTransformInfoToIke (pIkeEngine->u4IkeIndex,
                                               IKE_CONFIG_DEL_TRANSFORM_SET);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkeTransformSet   *pIkeTransformSet = NULL;
            tIkePolicy         *pIkePolicy = NULL;
            tIkeConfCryptoMap   IkeConfCryptoMap;

            MEMSET (&IkeConfCryptoMap, IKE_ZERO, sizeof (tIkeConfCryptoMap));

            pIkePolicy = IkeSnmpGetPolicyFromName (pIkeEngine,
                                                   pFsIkeTransformSetName->
                                                   pu1_OctetList,
                                                   pFsIkeTransformSetName->
                                                   i4_Length);

            if (pIkePolicy == NULL)
            {
                return SNMP_FAILURE;
            }

            IkeConfCryptoMap.u4IkeEngineIndex = pIkeEngine->u4IkeIndex;
            IkeConfCryptoMap.PeerIpAddr = pIkePolicy->PeerAddr;

            pIkeTransformSet = IkeSnmpGetTransformSet (pIkeEngine,
                                                       pFsIkeTransformSetName->
                                                       pu1_OctetList,
                                                       pFsIkeTransformSetName->
                                                       i4_Length);
            if (pIkeTransformSet != NULL)
            {
                if (pIkeTransformSet->u4RefCount != IKE_ZERO)
                {
                    /* XXX Some Debug print here ??? */
                    /* Entry still being used by some Crypto Map */
                    return (SNMP_FAILURE);
                }
                /* TS conf has been destroyed, send mesg to IKE */
                IkeSnmpSendDelTransformInfoToIke (IkeConfCryptoMap);
                IkeDeleteTransformSet (pIkeEngine, pIkeTransformSet);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeTSEspHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                testValFsIkeTSEspHashAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeTSEspHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeTSEspHashAlgo (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                             tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                             INT4 i4TestValFsIkeTSEspHashAlgo)
#else
INT1
nmhTestv2FsIkeTSEspHashAlgo (pu4ErrorCode, pFsIkeEngineName,
                             pFsIkeTransformSetName,
                             i4TestValFsIkeTSEspHashAlgo)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4TestValFsIkeTSEspHashAlgo;
#endif
{
    if ((i4TestValFsIkeTSEspHashAlgo != IKE_IPSEC_HMAC_MD5) &&
        (i4TestValFsIkeTSEspHashAlgo != IKE_IPSEC_HMAC_SHA1) &&
        (i4TestValFsIkeTSEspHashAlgo != SEC_XCBCMAC) &&
        (i4TestValFsIkeTSEspHashAlgo != HMAC_SHA_256) &&
        (i4TestValFsIkeTSEspHashAlgo != HMAC_SHA_384) &&
        (i4TestValFsIkeTSEspHashAlgo != HMAC_SHA_512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pFsIkeEngineName, pFsIkeTransformSetName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeTSEspEncryptionAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                testValFsIkeTSEspEncryptionAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeTSEspEncryptionAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeTSEspEncryptionAlgo (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsIkeTransformSetName,
                                   INT4 i4TestValFsIkeTSEspEncryptionAlgo)
#else
INT1
nmhTestv2FsIkeTSEspEncryptionAlgo (pu4ErrorCode, pFsIkeEngineName,
                                   pFsIkeTransformSetName,
                                   i4TestValFsIkeTSEspEncryptionAlgo)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4TestValFsIkeTSEspEncryptionAlgo;
#endif
{
    if ((i4TestValFsIkeTSEspEncryptionAlgo != IKE_IPSEC_ESP_DES_CBC) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != IKE_IPSEC_ESP_3DES_CBC) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != IKE_AES_KEY_LEN_128) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != IKE_AES_DEF_KEY_LEN_VAL) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != IKE_AES_KEY_LEN_256) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != SEC_AESCTR192) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != SEC_AESCTR) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != SEC_AESCTR256) &&
        (i4TestValFsIkeTSEspEncryptionAlgo != IKE_IPSEC_ESP_NULL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pFsIkeEngineName, pFsIkeTransformSetName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeTSAhHashAlgo
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                testValFsIkeTSAhHashAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeTSAhHashAlgo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeTSAhHashAlgo (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                            INT4 i4TestValFsIkeTSAhHashAlgo)
#else
INT1
nmhTestv2FsIkeTSAhHashAlgo (pu4ErrorCode, pFsIkeEngineName,
                            pFsIkeTransformSetName, i4TestValFsIkeTSAhHashAlgo)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4TestValFsIkeTSAhHashAlgo;
#endif
{
    if ((i4TestValFsIkeTSAhHashAlgo != IKE_IPSEC_AH_MD5) &&
        (i4TestValFsIkeTSAhHashAlgo != IKE_IPSEC_AH_SHA1) &&
        (i4TestValFsIkeTSAhHashAlgo != IKE_IPSEC_HMAC_MD5) &&
        (i4TestValFsIkeTSAhHashAlgo != SEC_XCBCMAC) &&
        (i4TestValFsIkeTSAhHashAlgo != HMAC_SHA_256) &&
        (i4TestValFsIkeTSAhHashAlgo != HMAC_SHA_384) &&
        (i4TestValFsIkeTSAhHashAlgo != HMAC_SHA_512) &&
        (i4TestValFsIkeTSAhHashAlgo != IKE_IPSEC_HMAC_SHA1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pFsIkeEngineName, pFsIkeTransformSetName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeTSStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeTransformSetName

                The Object 
                testValFsIkeTSStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeTSStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeTSStatus (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeTransformSetName,
                        INT4 i4TestValFsIkeTSStatus)
#else
INT1
nmhTestv2FsIkeTSStatus (pu4ErrorCode, pFsIkeEngineName, pFsIkeTransformSetName,
                        i4TestValFsIkeTSStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeTransformSetName;
     INT4                i4TestValFsIkeTSStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    /* 
     * Nothing to check in Engine Name or Transform-set Name
     */

    if ((i4TestValFsIkeTSStatus == IKE_CREATEANDGO) ||
        (i4TestValFsIkeTSStatus == IKE_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);

    }
    if ((i4TestValFsIkeTSStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeTSStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkeTransformSetTable (pFsIkeEngineName,
                                                            pFsIkeTransformSetName)
            == SNMP_SUCCESS)
        {
            if (i4TestValFsIkeTSStatus == IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
        else
        {
            if (i4TestValFsIkeTSStatus != IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeCryptoMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeCryptoMapTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkeCryptoMapTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeCryptoMapTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsIkeEngineName,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsIkeCryptoMapName)
#else
INT1
nmhValidateIndexInstanceFsIkeCryptoMapTable (pFsIkeEngineName,
                                             pFsIkeCryptoMapName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                             pFsIkeCryptoMapName->pu1_OctetList,
                                             pFsIkeCryptoMapName->i4_Length);
        if (pIkeCryptoMap != NULL)
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeCryptoMapTable
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkeCryptoMapTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeCryptoMapTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsIkeCryptoMapName)
#else
INT1
nmhGetFirstIndexFsIkeCryptoMapTable (pFsIkeEngineName, pFsIkeCryptoMapName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIkeCryptoMap = (tIkeCryptoMap *) SLL_FIRST (&pIkeEngine->IkeCryptoMapList);
    if (pIkeCryptoMap == NULL)
    {
        return (IKE_FAILURE);
    }

    pFsIkeEngineName->i4_Length = (INT4) STRLEN (pIkeCryptoMap->au1EngineName);
    IKE_MEMCPY (pFsIkeEngineName->pu1_OctetList,
                pIkeCryptoMap->au1EngineName, pFsIkeEngineName->i4_Length);
    pFsIkeCryptoMapName->i4_Length =
        (INT4) STRLEN (pIkeCryptoMap->au1CryptoMapName);
    IKE_MEMCPY (pFsIkeCryptoMapName->pu1_OctetList,
                pIkeCryptoMap->au1CryptoMapName,
                pFsIkeCryptoMapName->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeCryptoMapTable
 Input       :  The Indices
                FsIkeEngineName
                nextFsIkeEngineName
                FsIkeCryptoMapName
                nextFsIkeCryptoMapName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkeCryptoMapTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeCryptoMapTable (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIkeCryptoMapName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsIkeCryptoMapName)
#else
INT1
nmhGetNextIndexFsIkeCryptoMapTable (pFsIkeEngineName, pNextFsIkeEngineName,
                                    pFsIkeCryptoMapName,
                                    pNextFsIkeCryptoMapName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeCryptoMapName;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pNextIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;
    tIkeCryptoMap      *pNextIkeCryptoMap = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (IKE_MEMCMP
            (pFsIkeEngineName->pu1_OctetList, pIkeEngine->au1EngineName,
             (size_t) pFsIkeEngineName->i4_Length) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeCryptoMapList, pIkeCryptoMap, tIkeCryptoMap *)
    {
        if (IKE_MEMCMP (pIkeCryptoMap->au1CryptoMapName,
                        pFsIkeCryptoMapName->pu1_OctetList,
                        (size_t) pFsIkeCryptoMapName->i4_Length) == IKE_ZERO)
        {
            pNextIkeCryptoMap =
                (tIkeCryptoMap *) SLL_NEXT (&pIkeEngine->IkeCryptoMapList,
                                            (t_SLL_NODE *) pIkeCryptoMap);
            if (pNextIkeCryptoMap == NULL)
            {
                /* Have to check the next engine */
                pNextIkeEngine = (tIkeEngine *) SLL_NEXT (&gIkeEngineList,
                                                          (t_SLL_NODE *)
                                                          pIkeEngine);
                if (pNextIkeEngine == NULL)
                {
                    return (SNMP_FAILURE);
                }
                /* Return the first Key of this new engine */
                pNextIkeCryptoMap =
                    (tIkeCryptoMap *) SLL_FIRST (&pNextIkeEngine->
                                                 IkeCryptoMapList);
                if (pNextIkeCryptoMap == NULL)
                {
                    return (SNMP_FAILURE);
                }
            }

            pNextFsIkeEngineName->i4_Length = (INT4) STRLEN (pNextIkeCryptoMap->
                                                             au1EngineName);
            IKE_MEMCPY (pNextFsIkeEngineName->pu1_OctetList,
                        pNextIkeCryptoMap->au1EngineName,
                        pNextFsIkeEngineName->i4_Length);
            pNextFsIkeCryptoMapName->i4_Length =
                (INT4) STRLEN (pNextIkeCryptoMap->au1CryptoMapName);
            IKE_MEMCPY (pNextFsIkeCryptoMapName->pu1_OctetList,
                        pNextIkeCryptoMap->au1CryptoMapName,
                        pNextFsIkeCryptoMapName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeCMLocalAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMLocalAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMLocalAddrType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMLocalAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                            INT4 *pi4RetValFsIkeCMLocalAddrType)
#else
INT1
nmhGetFsIkeCMLocalAddrType (pFsIkeEngineName, pFsIkeCryptoMapName,
                            pi4RetValFsIkeCMLocalAddrType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMLocalAddrType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMLocalAddrType =
            (INT4) pIkeCryptoMap->LocalNetwork.u4Type;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMLocalAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMLocalAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMLocalAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMLocalAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsIkeCMLocalAddr)
#else
INT1
nmhGetFsIkeCMLocalAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                        pRetValFsIkeCMLocalAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeCMLocalAddr;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pRetValFsIkeCMLocalAddr->i4_Length =
            (INT4) STRLEN (pIkeCryptoMap->au1LocalNetworkDisplay);
        MEMCPY (pRetValFsIkeCMLocalAddr->pu1_OctetList,
                pIkeCryptoMap->au1LocalNetworkDisplay,
                pRetValFsIkeCMLocalAddr->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMRemAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMRemAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMRemAddrType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMRemAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                          INT4 *pi4RetValFsIkeCMRemAddrType)
#else
INT1
nmhGetFsIkeCMRemAddrType (pFsIkeEngineName, pFsIkeCryptoMapName,
                          pi4RetValFsIkeCMRemAddrType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMRemAddrType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMRemAddrType =
            (INT4) pIkeCryptoMap->RemoteNetwork.u4Type;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMRemAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMRemAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMRemAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMRemAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsIkeCMRemAddr)
#else
INT1
nmhGetFsIkeCMRemAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                      pRetValFsIkeCMRemAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeCMRemAddr;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pRetValFsIkeCMRemAddr->i4_Length =
            (INT4) STRLEN (pIkeCryptoMap->au1RemoteNetworkDisplay);
        MEMCPY (pRetValFsIkeCMRemAddr->pu1_OctetList,
                pIkeCryptoMap->au1RemoteNetworkDisplay,
                pRetValFsIkeCMRemAddr->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMPeerAddrType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMPeerAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMPeerAddrType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMPeerAddrType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                           INT4 *pi4RetValFsIkeCMPeerAddrType)
#else
INT1
nmhGetFsIkeCMPeerAddrType (pFsIkeEngineName, pFsIkeCryptoMapName,
                           pi4RetValFsIkeCMPeerAddrType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMPeerAddrType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMPeerAddrType =
            (INT4) pIkeCryptoMap->PeerAddr.u4AddrType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMPeerAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMPeerAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMPeerAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMPeerAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsIkeCMPeerAddr)
#else
INT1
nmhGetFsIkeCMPeerAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                       pRetValFsIkeCMPeerAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeCMPeerAddr;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;
    const INT1         *pTemp = NULL;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        if (IPV4ADDR == pIkeCryptoMap->PeerAddr.u4AddrType)
        {
            pRetValFsIkeCMPeerAddr->i4_Length = INET_ADDRSTRLEN;
            /* XXX Have to check if pRetValFsIkeCMPeerAddr is set */
            /* pRetValFsIkeCMPeerAddr->pu1_OctetList =  */
            u4TempIp4Addr = IKE_HTONL (pIkeCryptoMap->PeerAddr.uIpAddr.Ip4Addr);
            pTemp = (const INT1 *) INET_NTOP4 (&u4TempIp4Addr,
                                               pRetValFsIkeCMPeerAddr->
                                               pu1_OctetList);

            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }

            return (SNMP_SUCCESS);
        }

        if (IPV6ADDR == pIkeCryptoMap->PeerAddr.u4AddrType)
        {
            pRetValFsIkeCMPeerAddr->i4_Length = INET6_ADDRSTRLEN;
            /* XXX Have to check if pRetValFsIkeCMPeerAddr is set */
            /* pRetValFsIkeCMPeerAddr->pu1_OctetList =  */
            pTemp = (const INT1 *)
                INET_NTOP6 (&pIkeCryptoMap->PeerAddr.uIpAddr.Ip6Addr.u1_addr,
                            pRetValFsIkeCMPeerAddr->pu1_OctetList);

            if (pTemp == NULL)
            {
                return (SNMP_FAILURE);
            }

            return (SNMP_SUCCESS);
        }

        /* Something other that IPv4 or IPv6 Address, return failure */
        return (SNMP_FAILURE);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMTransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMTransformSetBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMTransformSetBundle ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMTransformSetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsIkeCMTransformSetBundle)
#else
INT1
nmhGetFsIkeCMTransformSetBundle (pFsIkeEngineName, pFsIkeCryptoMapName,
                                 pRetValFsIkeCMTransformSetBundle)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pRetValFsIkeCMTransformSetBundle;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pRetValFsIkeCMTransformSetBundle->i4_Length =
            (INT4) STRLEN (pIkeCryptoMap->au1TSBundleDisplay);
        MEMCPY (pRetValFsIkeCMTransformSetBundle->pu1_OctetList,
                pIkeCryptoMap->au1TSBundleDisplay,
                pRetValFsIkeCMTransformSetBundle->i4_Length);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                   tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                   INT4 *pi4RetValFsIkeCMMode)
#else
INT1
nmhGetFsIkeCMMode (pFsIkeEngineName, pFsIkeCryptoMapName, pi4RetValFsIkeCMMode)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMMode;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMMode = (INT4) pIkeCryptoMap->u1Mode;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMPfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMPfs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMPfs (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                  tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                  INT4 *pi4RetValFsIkeCMPfs)
#else
INT1
nmhGetFsIkeCMPfs (pFsIkeEngineName, pFsIkeCryptoMapName, pi4RetValFsIkeCMPfs)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMPfs;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMPfs = (INT4) pIkeCryptoMap->u1Pfs;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMLifeTimeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMLifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                           INT4 *pi4RetValFsIkeCMLifeTimeType)
#else
INT1
nmhGetFsIkeCMLifeTimeType (pFsIkeEngineName, pFsIkeCryptoMapName,
                           pi4RetValFsIkeCMLifeTimeType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMLifeTimeType;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMLifeTimeType = (INT4) pIkeCryptoMap->u1LifeTimeType;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMLifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                       UINT4 *pu4RetValFsIkeCMLifeTime)
#else
INT1
nmhGetFsIkeCMLifeTime (pFsIkeEngineName, pFsIkeCryptoMapName,
                       pu4RetValFsIkeCMLifeTime)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4              *pu4RetValFsIkeCMLifeTime;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pu4RetValFsIkeCMLifeTime = pIkeCryptoMap->u4LifeTime;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMLifeTimeKB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMLifeTimeKB (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                         UINT4 *pu4RetValFsIkeCMLifeTimeKB)
#else
INT1
nmhGetFsIkeCMLifeTimeKB (pFsIkeEngineName, pFsIkeCryptoMapName,
                         pu4RetValFsIkeCMLifeTimeKB)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4              *pu4RetValFsIkeCMLifeTimeKB;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pu4RetValFsIkeCMLifeTimeKB = pIkeCryptoMap->u4LifeTimeKB;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFsIkeCMStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                retValFsIkeCMStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeCMStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeCMStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                     INT4 *pi4RetValFsIkeCMStatus)
#else
INT1
nmhGetFsIkeCMStatus (pFsIkeEngineName, pFsIkeCryptoMapName,
                     pi4RetValFsIkeCMStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4               *pi4RetValFsIkeCMStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        *pi4RetValFsIkeCMStatus = (INT4) pIkeCryptoMap->u1Status;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeCMLocalAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMLocalAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMLocalAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMLocalAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsIkeCMLocalAddr)
#else
INT1
nmhSetFsIkeCMLocalAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                        pSetValFsIkeCMLocalAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeCMLocalAddr;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        if (IkeCryptoSetNetworkAddress (&(pIkeCryptoMap->LocalNetwork),
                                        pSetValFsIkeCMLocalAddr->pu1_OctetList,
                                        pSetValFsIkeCMLocalAddr->i4_Length)
            == IKE_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Copy onto string for show purposes */
        if (pSetValFsIkeCMLocalAddr->i4_Length >= MAX_NAME_LENGTH)
        {
            return (SNMP_FAILURE);
        }
        MEMCPY (pIkeCryptoMap->au1LocalNetworkDisplay,
                pSetValFsIkeCMLocalAddr->pu1_OctetList,
                pSetValFsIkeCMLocalAddr->i4_Length);
        pIkeCryptoMap->au1LocalNetworkDisplay
            [pSetValFsIkeCMLocalAddr->i4_Length] = '\0';

        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMRemAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMRemAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMRemAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMRemAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsIkeCMRemAddr)
#else
INT1
nmhSetFsIkeCMRemAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                      pSetValFsIkeCMRemAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeCMRemAddr;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        if (IkeCryptoSetNetworkAddress (&(pIkeCryptoMap->RemoteNetwork),
                                        pSetValFsIkeCMRemAddr->pu1_OctetList,
                                        pSetValFsIkeCMRemAddr->i4_Length)
            == IKE_FAILURE)
        {

            return SNMP_FAILURE;
        }

        /* Copy onto string for show purposes */
        if (pSetValFsIkeCMRemAddr->i4_Length >= MAX_NAME_LENGTH)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (pIkeCryptoMap->au1RemoteNetworkDisplay,
                pSetValFsIkeCMRemAddr->pu1_OctetList,
                pSetValFsIkeCMRemAddr->i4_Length);
        pIkeCryptoMap->au1RemoteNetworkDisplay
            [pSetValFsIkeCMRemAddr->i4_Length] = '\0';

        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
        return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMPeerAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMPeerAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMPeerAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMPeerAddr (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsIkeCMPeerAddr)
#else
INT1
nmhSetFsIkeCMPeerAddr (pFsIkeEngineName, pFsIkeCryptoMapName,
                       pSetValFsIkeCMPeerAddr)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeCMPeerAddr;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pSetValFsIkeCMPeerAddr->pu1_OctetList[pSetValFsIkeCMPeerAddr->i4_Length] =
        '\0';

    u1AddrType = IkeParseAddrString (pSetValFsIkeCMPeerAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->PeerAddr.u4AddrType = u1AddrType;
        if (IPV4ADDR == u1AddrType)
        {
            i4RetVal = INET_PTON4 (pSetValFsIkeCMPeerAddr->pu1_OctetList,
                                   &u4TempIp4Addr);
            pIkeCryptoMap->PeerAddr.uIpAddr.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
            if (i4RetVal > IKE_ZERO)
            {
                if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
                {
                    if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
                    {
                        /* Entry ready to go Active */
                        pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
                    }
                    else
                    {
                        if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                        {
                            IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->
                                                           u4IkeIndex,
                                                           pIkeCryptoMap,
                                                           IKE_CONFIG_CHG_CRYPTO_MAP);
                        }
                    }
                }
                return (SNMP_SUCCESS);
            }
        }

        if (IPV6ADDR == u1AddrType)
        {
            i4RetVal = INET_PTON6 (pSetValFsIkeCMPeerAddr->pu1_OctetList,
                                   &pIkeCryptoMap->PeerAddr.uIpAddr.Ip6Addr.
                                   u1_addr);
            if (i4RetVal > IKE_ZERO)
            {
                if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
                {
                    if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
                    {
                        /* Entry ready to go Active */
                        pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
                    }
                    else
                    {
                        if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                        {
                            IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->
                                                           u4IkeIndex,
                                                           pIkeCryptoMap,
                                                           IKE_CONFIG_CHG_CRYPTO_MAP);
                        }
                    }
                }
                return (SNMP_SUCCESS);
            }
        }
        /* neither v4 nor v6 address */
        return (SNMP_FAILURE);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMTransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMTransformSetBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMTransformSetBundle ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMTransformSetBundle (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsIkeCMTransformSetBundle)
#else
INT1
nmhSetFsIkeCMTransformSetBundle (pFsIkeEngineName, pFsIkeCryptoMapName,
                                 pSetValFsIkeCMTransformSetBundle)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pSetValFsIkeCMTransformSetBundle;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        if (IkeCryptoSetTransformSetBundle (pIkeEngine, pIkeCryptoMap,
                                            pSetValFsIkeCMTransformSetBundle->
                                            pu1_OctetList,
                                            pSetValFsIkeCMTransformSetBundle->
                                            i4_Length) == IKE_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        /* Copy onto string for show purposes */
        MEMCPY (pIkeCryptoMap->au1TSBundleDisplay,
                pSetValFsIkeCMTransformSetBundle->pu1_OctetList,
                pSetValFsIkeCMTransformSetBundle->i4_Length);
        pIkeCryptoMap->au1TSBundleDisplay
            [pSetValFsIkeCMTransformSetBundle->i4_Length] = '\0';

        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMMode (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                   tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                   INT4 i4SetValFsIkeCMMode)
#else
INT1
nmhSetFsIkeCMMode (pFsIkeEngineName, pFsIkeCryptoMapName, i4SetValFsIkeCMMode)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4SetValFsIkeCMMode;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->u1Mode = (UINT1) i4SetValFsIkeCMMode;
        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMPfs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMPfs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMPfs (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                  tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                  INT4 i4SetValFsIkeCMPfs)
#else
INT1
nmhSetFsIkeCMPfs (pFsIkeEngineName, pFsIkeCryptoMapName, i4SetValFsIkeCMPfs)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4SetValFsIkeCMPfs;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->u1Pfs = (UINT1) i4SetValFsIkeCMPfs;
        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMLifeTimeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMLifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                           INT4 i4SetValFsIkeCMLifeTimeType)
#else
INT1
nmhSetFsIkeCMLifeTimeType (pFsIkeEngineName, pFsIkeCryptoMapName,
                           i4SetValFsIkeCMLifeTimeType)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4SetValFsIkeCMLifeTimeType;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->u1LifeTimeType = (UINT1) i4SetValFsIkeCMLifeTimeType;
        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMLifeTime (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                       tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                       UINT4 u4SetValFsIkeCMLifeTime)
#else
INT1
nmhSetFsIkeCMLifeTime (pFsIkeEngineName, pFsIkeCryptoMapName,
                       u4SetValFsIkeCMLifeTime)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4               u4SetValFsIkeCMLifeTime;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->u4LifeTime = u4SetValFsIkeCMLifeTime;
        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMLifeTimeKB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMLifeTimeKB (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                         UINT4 u4SetValFsIkeCMLifeTimeKB)
#else
INT1
nmhSetFsIkeCMLifeTimeKB (pFsIkeEngineName, pFsIkeCryptoMapName,
                         u4SetValFsIkeCMLifeTimeKB)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4               u4SetValFsIkeCMLifeTimeKB;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pFsIkeCryptoMapName->pu1_OctetList,
                                         pFsIkeCryptoMapName->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        pIkeCryptoMap->u4LifeTimeKB = u4SetValFsIkeCMLifeTimeKB;
        if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
        {
            if (pIkeCryptoMap->u1Status != IKE_ACTIVE)
            {
                /* Entry ready to go Active */
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
            }
            else
            {
                if (pIkeCryptoMap->PeerAddr.u4AddrType != IKE_ZERO)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_CHG_CRYPTO_MAP);
                }
            }
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhSetFsIkeCMStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                setValFsIkeCMStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeCMStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeCMStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                     INT4 i4SetValFsIkeCMStatus)
#else
INT1
nmhSetFsIkeCMStatus (pFsIkeEngineName, pFsIkeCryptoMapName,
                     i4SetValFsIkeCMStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4SetValFsIkeCMStatus;

#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    /* Test routine will already have checked whether this entry exists, 
       so no need to check again */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsIkeCMStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeCryptoMap      *pIkeCryptoMap = NULL;

            pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                                 pFsIkeCryptoMapName->
                                                 pu1_OctetList,
                                                 pFsIkeCryptoMapName->
                                                 i4_Length);
            if (pIkeCryptoMap != NULL)
            {
                return (SNMP_FAILURE);
            }

            if (MemAllocateMemBlock
                (IKE_CRYPTOMAP_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pIkeCryptoMap) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "nmhSetFsIkeCMStatus: unable to allocate "
                             "buffer\n");
                return (SNMP_FAILURE);
            }

            if (pIkeCryptoMap == NULL)
            {
                return (SNMP_FAILURE);
            }
            MEMSET (pIkeCryptoMap, IKE_ZERO, sizeof (tIkeCryptoMap));

            /* Set the Index in the entry */
            u4TempLen = MEM_MAX_BYTES ((UINT4) pFsIkeCryptoMapName->i4_Length,
                                       STRLEN (pFsIkeCryptoMapName->
                                               pu1_OctetList));
            MEMCPY (pIkeCryptoMap->au1CryptoMapName,
                    pFsIkeCryptoMapName->pu1_OctetList, u4TempLen);
            pIkeCryptoMap->au1CryptoMapName[u4TempLen] = '\0';
            if (STRLEN (pFsIkeEngineName->pu1_OctetList) >= MAX_NAME_LENGTH)
            {
                MemReleaseMemBlock (IKE_CRYPTOMAP_MEMPOOL_ID,
                                    (UINT1 *) pIkeCryptoMap);
                return (SNMP_FAILURE);
            }
            /* Set the Engine Name in the entry */
            u4TempLen = MEM_MAX_BYTES ((UINT4) pFsIkeEngineName->i4_Length,
                                       STRLEN (pFsIkeEngineName->
                                               pu1_OctetList));
            MEMCPY (pIkeCryptoMap->au1EngineName,
                    pFsIkeEngineName->pu1_OctetList, u4TempLen);
            pIkeCryptoMap->au1EngineName[u4TempLen] = '\0';
            IkeInitCryptoMap (pIkeCryptoMap);
            pIkeCryptoMap->u1Status = IKE_NOTREADY;
            TAKE_IKE_SEM ();
            SLL_ADD (&(pIkeEngine->IkeCryptoMapList),
                     (t_SLL_NODE *) pIkeCryptoMap);
            GIVE_IKE_SEM ();
            return (SNMP_SUCCESS);
        }

        case IKE_ACTIVE:
        {
            tIkeCryptoMap      *pIkeCryptoMap = NULL;

            pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                                 pFsIkeCryptoMapName->
                                                 pu1_OctetList,
                                                 pFsIkeCryptoMapName->
                                                 i4_Length);
            if (pIkeCryptoMap == NULL)
            {
                return (SNMP_FAILURE);
            }

            /* Check if required attributes are configured, other than 
               the default */
            if (IkeCheckCryptoMapAttributes (pIkeCryptoMap) == IKE_SUCCESS)
            {
                pIkeCryptoMap->u1Status = IKE_ACTIVE;
                return (SNMP_SUCCESS);
            }
            else
            {
                /* All attributes have not been set, return FAILURE */
                return (SNMP_FAILURE);
            }
        }

        case IKE_NOTINSERVICE:
        {
            tIkeCryptoMap      *pIkeCryptoMap = NULL;

            pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                                 pFsIkeCryptoMapName->
                                                 pu1_OctetList,
                                                 pFsIkeCryptoMapName->
                                                 i4_Length);
            if (pIkeCryptoMap == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeCryptoMap->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeCryptoMap->u1Status)
            {
                pIkeCryptoMap->u1Status = IKE_NOTINSERVICE;
                IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                               pIkeCryptoMap,
                                               IKE_CONFIG_DEL_CRYPTO_MAP);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkeCryptoMap      *pIkeCryptoMap = NULL;

            pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                                 pFsIkeCryptoMapName->
                                                 pu1_OctetList,
                                                 pFsIkeCryptoMapName->
                                                 i4_Length);
            if (pIkeCryptoMap != NULL)
            {
                /* This will not free the crypto map memory, it will
                   only unlink it from the Engine */
                IkeDeleteCryptoMap (pIkeEngine, pIkeCryptoMap);
                /* Crypto Map has been deleted, send message to
                   IKE */
                if (pIkeCryptoMap->bIsCryptoDynamic != IKE_TRUE)
                {
                    IkeSnmpSendCryptoMapInfoToIke (pIkeEngine->u4IkeIndex,
                                                   pIkeCryptoMap,
                                                   IKE_CONFIG_DEL_CRYPTO_MAP);
                }
                /* Now free the memory */
                IKE_BZERO (pIkeCryptoMap, sizeof (tIkeCryptoMap));
                MemReleaseMemBlock (IKE_CRYPTOMAP_MEMPOOL_ID,
                                    (UINT1 *) pIkeCryptoMap);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMLocalAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMLocalAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMLocalAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMLocalAddr (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                           tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsIkeCMLocalAddr)
#else
INT1
nmhTestv2FsIkeCMLocalAddr (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                           pTestValFsIkeCMLocalAddr)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeCMLocalAddr;
#endif
{

    UINT1               u1AddrType = IKE_ZERO;

    u1AddrType = IkeParseNetworkAddr (pTestValFsIkeCMLocalAddr->pu1_OctetList,
                                      pTestValFsIkeCMLocalAddr->i4_Length);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMRemAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMRemAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMRemAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMRemAddr (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                         tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsIkeCMRemAddr)
#else
INT1
nmhTestv2FsIkeCMRemAddr (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                         pTestValFsIkeCMRemAddr)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeCMRemAddr;
#endif
{
    UINT1               u1AddrType = IKE_ZERO;

    u1AddrType = IkeParseNetworkAddr (pTestValFsIkeCMRemAddr->pu1_OctetList,
                                      pTestValFsIkeCMRemAddr->i4_Length);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* XXX Should we check for validity of RemAddr ??? */
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMPeerAddr
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMPeerAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMPeerAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMPeerAddr (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsIkeCMPeerAddr)
#else
INT1
nmhTestv2FsIkeCMPeerAddr (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                          pTestValFsIkeCMPeerAddr)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeCMPeerAddr;
#endif
{
    /* 
     * Not checking the PeerAddr here as it is being checked in Set 
     * Routine anyways 
     */
    UINT1               u1AddrType = IKE_ZERO;

    pTestValFsIkeCMPeerAddr->
        pu1_OctetList[pTestValFsIkeCMPeerAddr->i4_Length] = '\0';

    u1AddrType = IkeParseAddrString (pTestValFsIkeCMPeerAddr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMTransformSetBundle
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMTransformSetBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMTransformSetBundle ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMTransformSetBundle (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIkeCryptoMapName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsIkeCMTransformSetBundle)
#else
INT1
nmhTestv2FsIkeCMTransformSetBundle (pu4ErrorCode, pFsIkeEngineName,
                                    pFsIkeCryptoMapName,
                                    pTestValFsIkeCMTransformSetBundle)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     tSNMP_OCTET_STRING_TYPE *pTestValFsIkeCMTransformSetBundle;
#endif
{
    if (pTestValFsIkeCMTransformSetBundle->i4_Length > MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* XXX Should we check the validity of the Bundle right here ??? */
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMMode
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMMode (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                      tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                      INT4 i4TestValFsIkeCMMode)
#else
INT1
nmhTestv2FsIkeCMMode (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                      i4TestValFsIkeCMMode)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4TestValFsIkeCMMode;
#endif
{
    if ((i4TestValFsIkeCMMode != IKE_IPSEC_TUNNEL_MODE) &&
        (i4TestValFsIkeCMMode != IKE_IPSEC_TRANSPORT_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMPfs
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMPfs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMPfs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMPfs (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                     tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                     INT4 i4TestValFsIkeCMPfs)
#else
INT1
nmhTestv2FsIkeCMPfs (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                     i4TestValFsIkeCMPfs)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4TestValFsIkeCMPfs;
#endif
{
    if ((i4TestValFsIkeCMPfs != IKE_DH_GROUP_1) &&
        (i4TestValFsIkeCMPfs != IKE_DH_GROUP_2) &&
        (i4TestValFsIkeCMPfs != IKE_DH_GROUP_5) &&
        (i4TestValFsIkeCMPfs != SEC_DH_GROUP_14))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMLifeTimeType
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMLifeTimeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMLifeTimeType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMLifeTimeType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                              tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                              INT4 i4TestValFsIkeCMLifeTimeType)
#else
INT1
nmhTestv2FsIkeCMLifeTimeType (pu4ErrorCode, pFsIkeEngineName,
                              pFsIkeCryptoMapName, i4TestValFsIkeCMLifeTimeType)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4TestValFsIkeCMLifeTimeType;
#endif
{

    if ((i4TestValFsIkeCMLifeTimeType < FSIKE_LIFETIME_SECS) ||
        (i4TestValFsIkeCMLifeTimeType > FSIKE_LIFETIME_DAYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMLifeTime
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMLifeTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMLifeTime (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                          tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                          UINT4 u4TestValFsIkeCMLifeTime)
#else
INT1
nmhTestv2FsIkeCMLifeTime (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                          u4TestValFsIkeCMLifeTime)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4               u4TestValFsIkeCMLifeTime;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    /* Not using the nmhValidate routine here as we want to check
       the Lifetime value using pIkeCryptoMap
     */
    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);
    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                             pFsIkeCryptoMapName->pu1_OctetList,
                                             pFsIkeCryptoMapName->i4_Length);
        if (pIkeCryptoMap != NULL)
        {
            if (pIkeCryptoMap->u1LifeTimeType == FSIKE_LIFETIME_KB)
            {
                if ((u4TestValFsIkeCMLifeTime != IKE_ZERO) &&
                    (u4TestValFsIkeCMLifeTime < FSIKE_MIN_LIFETIME_KB))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return (SNMP_FAILURE);
                }
            }
            else
            {
                IKE_CONVERT_LIFETIME_TO_SECS (pIkeCryptoMap->u1LifeTimeType,
                                              u4TestValFsIkeCMLifeTime);
                if ((u4TestValFsIkeCMLifeTime != IKE_ZERO) &&
                    (u4TestValFsIkeCMLifeTime < FSIKE_MIN_LIFETIME_SECS))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return (SNMP_FAILURE);
                }
            }
            return (SNMP_SUCCESS);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMLifeTimeKB
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMLifeTimeKB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMLifeTimeKB ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMLifeTimeKB (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                            tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                            UINT4 u4TestValFsIkeCMLifeTimeKB)
#else
INT1
nmhTestv2FsIkeCMLifeTimeKB (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                            u4TestValFsIkeCMLifeTimeKB)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     UINT4               u4TestValFsIkeCMLifeTimeKB;
#endif
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pFsIkeEngineName, pFsIkeCryptoMapName) == SNMP_SUCCESS)
    {
        if ((u4TestValFsIkeCMLifeTimeKB != IKE_ZERO) &&
            (u4TestValFsIkeCMLifeTimeKB < FSIKE_MIN_LIFETIME_KB))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeCMStatus
 Input       :  The Indices
                FsIkeEngineName
                FsIkeCryptoMapName

                The Object 
                testValFsIkeCMStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeCMStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeCMStatus (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                        tSNMP_OCTET_STRING_TYPE * pFsIkeCryptoMapName,
                        INT4 i4TestValFsIkeCMStatus)
#else
INT1
nmhTestv2FsIkeCMStatus (pu4ErrorCode, pFsIkeEngineName, pFsIkeCryptoMapName,
                        i4TestValFsIkeCMStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeEngineName;
     tSNMP_OCTET_STRING_TYPE *pFsIkeCryptoMapName;
     INT4                i4TestValFsIkeCMStatus;
#endif
{
    tIkeEngine         *pIkeEngine = NULL;

    /* 
     * Nothing to check in Engine Name or Transform-set Name
     */

    if ((i4TestValFsIkeCMStatus == IKE_CREATEANDGO) ||
        (i4TestValFsIkeCMStatus == IKE_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsIkeCMStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeCMStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pFsIkeEngineName->pu1_OctetList,
                                   pFsIkeEngineName->i4_Length);

    if ((pIkeEngine != NULL) && (IKE_ACTIVE == pIkeEngine->u1Status))
    {
        if (nmhValidateIndexInstanceFsIkeCryptoMapTable (pFsIkeEngineName,
                                                         pFsIkeCryptoMapName) ==
            SNMP_SUCCESS)
        {
            if (i4TestValFsIkeCMStatus == IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
                return (SNMP_SUCCESS);
        }
        else
        {
            if (i4TestValFsIkeCMStatus != IKE_CREATEANDWAIT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            else
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : FsIkeStatTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeStatTable
 Input       :  The Indices
                FsTunnelTerminationAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeStatTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsTunnelTerminationAddr)
#else
INT1
nmhValidateIndexInstanceFsIkeStatTable (pFsTunnelTerminationAddr)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->
        pu1_OctetList[pFsTunnelTerminationAddr->i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);

    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);

    if (i4Index >= IKE_ZERO)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeStatTable
 Input       :  The Indices
                FsTunnelTerminationAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeStatTable (tSNMP_OCTET_STRING_TYPE *
                                pFsTunnelTerminationAddr)
#else
INT1
nmhGetFirstIndexFsIkeStatTable (pFsTunnelTerminationAddr)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
#endif
{
    UINT4               u4Index = IKE_ZERO;
    const INT1         *pTemp = NULL;
    tIp4Addr            u4TempIpAddr = IKE_ZERO;

    for (u4Index = IKE_ZERO; u4Index < IKE_MAX_STATISTICS_ENTRY; u4Index++)
    {
        if (GetIkeIpPeerAddr (u4Index).u4AddrType != IKE_ZERO)
        {
            if (IPV4ADDR == GetIkeIpPeerAddr (u4Index).u4AddrType)
            {
                u4TempIpAddr = OSIX_HTONL (GetIkeIpPeerAddr (u4Index).Ipv4Addr);
                pTemp =
                    (const INT1 *) INET_NTOP4 (&u4TempIpAddr,
                                               pFsTunnelTerminationAddr->
                                               pu1_OctetList);
                if (pTemp == NULL)
                {
                    return (SNMP_FAILURE);
                }
                pFsTunnelTerminationAddr->i4_Length =
                    (INT4) IKE_STRLEN (pFsTunnelTerminationAddr->pu1_OctetList);
                return (SNMP_SUCCESS);
            }
            else
            {
                pTemp =
                    (const INT1 *) INET_NTOP6 (&GetIkeIpPeerAddr (u4Index).
                                               uIpAddr.Ip6Addr.u1_addr,
                                               pFsTunnelTerminationAddr->
                                               pu1_OctetList);

                if (pTemp == NULL)
                {
                    return (SNMP_FAILURE);
                }
                pFsTunnelTerminationAddr->i4_Length =
                    (INT4) IKE_STRLEN (pFsTunnelTerminationAddr->pu1_OctetList);
                return (SNMP_SUCCESS);
            }

        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeStatTable
 Input       :  The Indices
                FsTunnelTerminationAddr
                nextFsTunnelTerminationAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeStatTable (tSNMP_OCTET_STRING_TYPE *
                               pFsTunnelTerminationAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pNextFsTunnelTerminationAddr)
#else
INT1
nmhGetNextIndexFsIkeStatTable (pFsTunnelTerminationAddr,
                               pNextFsTunnelTerminationAddr)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     tSNMP_OCTET_STRING_TYPE *pNextFsTunnelTerminationAddr;
#endif
{
    INT4                i4Index = IKE_ZERO;
    const INT1         *pTemp = NULL;
    tIkeIpAddr          IpAddr;
    tIp4Addr            u4TempIpAddr = IKE_ZERO;

    IKE_BZERO (&IpAddr, sizeof (tIkeIpAddr));
    pFsTunnelTerminationAddr->pu1_OctetList
        [pFsTunnelTerminationAddr->i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }

    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index++;

    for (; i4Index < IKE_MAX_STATISTICS_ENTRY; i4Index++)
    {
        if (GetIkeIpPeerAddr (i4Index).u4AddrType != IKE_ZERO)
        {
            if (IPV4ADDR == GetIkeIpPeerAddr (i4Index).u4AddrType)
            {
                u4TempIpAddr = OSIX_HTONL (GetIkeIpPeerAddr (i4Index).Ipv4Addr);
                pTemp =
                    (const INT1 *) INET_NTOP4 (&u4TempIpAddr,
                                               pNextFsTunnelTerminationAddr->
                                               pu1_OctetList);
                if (pTemp == NULL)
                {
                    return (SNMP_FAILURE);
                }
                pNextFsTunnelTerminationAddr->i4_Length =
                    (INT4) IKE_STRLEN (pNextFsTunnelTerminationAddr->
                                       pu1_OctetList);
                return (SNMP_SUCCESS);
            }
            else
            {
                pTemp =
                    (const INT1 *) INET_NTOP6 (&GetIkeIpPeerAddr (i4Index).
                                               uIpAddr.Ip6Addr.u1_addr,
                                               pNextFsTunnelTerminationAddr->
                                               pu1_OctetList);
                if (pTemp == NULL)
                {
                    return (SNMP_FAILURE);
                }
                pNextFsTunnelTerminationAddr->i4_Length =
                    (INT4) IKE_STRLEN (pNextFsTunnelTerminationAddr->
                                       pu1_OctetList);
                return (SNMP_SUCCESS);
            }
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeSAs
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeSAs (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                UINT4 *pu4RetValFsIkeSAs)
#else
INT1
nmhGetFsIkeSAs (pFsTunnelTerminationAddr, pu4RetValFsIkeSAs)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeSAs;
#endif
{
    tIkeIpAddr          IpAddr;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSA             *pIkeSA = NULL;
    UINT4               u4SACount = IKE_ZERO;

    IKE_BZERO (&IpAddr, sizeof (tIkeIpAddr));
    pFsTunnelTerminationAddr->pu1_OctetList
        [pFsTunnelTerminationAddr->i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);

    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "nmhGetFsIkeSAs: Unable to get address type\n");
        return (SNMP_FAILURE);
    }

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        SLL_SCAN (&(pIkeEngine->IkeSAList), pIkeSA, tIkeSA *)
        {
            if (pIkeSA->IpPeerAddr.u4AddrType == IpAddr.u4AddrType)
            {
                if (pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
                {
                    if (pIkeSA->IpPeerAddr.Ipv4Addr == IpAddr.Ipv4Addr)
                    {
                        u4SACount++;
                    }
                }
                else
                {
                    if (IKE_MEMCMP (&(pIkeSA->IpPeerAddr.Ipv6Addr),
                                    &IpAddr.Ipv6Addr, sizeof (tIp6Addr)) ==
                        IKE_ZERO)
                    {
                        u4SACount++;
                    }
                }
            }
        }
    }

    *pu4RetValFsIkeSAs = u4SACount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIkeInitSessions
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeInitSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeInitSessions (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                         UINT4 *pu4RetValFsIkeInitSessions)
#else
INT1
nmhGetFsIkeInitSessions (pFsTunnelTerminationAddr, pu4RetValFsIkeInitSessions)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeInitSessions;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    IKE_BZERO (&IpAddr, sizeof (tIkeIpAddr));
    pFsTunnelTerminationAddr->pu1_OctetList
        [pFsTunnelTerminationAddr->i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeInitSessions =
        gpIkeStatisticsInfo[i4Index].u4IkeInitSessions;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsIkeAcceptSessions
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeAcceptSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeAcceptSessions (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                           UINT4 *pu4RetValFsIkeAcceptSessions)
#else
INT1
nmhGetFsIkeAcceptSessions (pFsTunnelTerminationAddr,
                           pu4RetValFsIkeAcceptSessions)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeAcceptSessions;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeAcceptSessions =
        gpIkeStatisticsInfo[i4Index].u4IkeAcceptSessions;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeActiveSessions
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeActiveSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeActiveSessions (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                           UINT4 *pu4RetValFsIkeActiveSessions)
#else
INT1
nmhGetFsIkeActiveSessions (pFsTunnelTerminationAddr,
                           pu4RetValFsIkeActiveSessions)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeActiveSessions;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeActiveSessions =
        gpIkeStatisticsInfo[i4Index].u4IkeActiveSessions;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeInPackets
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeInPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeInPackets (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                      UINT4 *pu4RetValFsIkeInPackets)
#else
INT1
nmhGetFsIkeInPackets (pFsTunnelTerminationAddr, pu4RetValFsIkeInPackets)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeInPackets;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeInPackets = gpIkeStatisticsInfo[i4Index].u4IkeInPkts;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeOutPackets
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeOutPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeOutPackets (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                       UINT4 *pu4RetValFsIkeOutPackets)
#else
INT1
nmhGetFsIkeOutPackets (pFsTunnelTerminationAddr, pu4RetValFsIkeOutPackets)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeOutPackets;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';

    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeOutPackets = gpIkeStatisticsInfo[i4Index].u4IkeOutPkts;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeDropPackets
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeDropPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeDropPackets (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                        UINT4 *pu4RetValFsIkeDropPackets)
#else
INT1
nmhGetFsIkeDropPackets (pFsTunnelTerminationAddr, pu4RetValFsIkeDropPackets)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeDropPackets;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeDropPackets = gpIkeStatisticsInfo[i4Index].u4IkeDropPkts;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeNoOfNotifySent
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeNoOfNotifySent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeNoOfNotifySent (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                           UINT4 *pu4RetValFsIkeNoOfNotifySent)
#else
INT1
nmhGetFsIkeNoOfNotifySent (pFsTunnelTerminationAddr,
                           pu4RetValFsIkeNoOfNotifySent)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeNoOfNotifySent;
#endif
{

    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeNoOfNotifySent =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfNotifySent;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeNoOfNotifyRecv
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeNoOfNotifyRecv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeNoOfNotifyRecv (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                           UINT4 *pu4RetValFsIkeNoOfNotifyRecv)
#else
INT1
nmhGetFsIkeNoOfNotifyRecv (pFsTunnelTerminationAddr,
                           pu4RetValFsIkeNoOfNotifyRecv)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeNoOfNotifyRecv;
#endif
{

    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeNoOfNotifyRecv =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfNotifyReceive;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIkeNoOfSessionsSucc
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeNoOfSessionsSucc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeNoOfSessionsSucc (tSNMP_OCTET_STRING_TYPE * pFsTunnelTerminationAddr,
                             UINT4 *pu4RetValFsIkeNoOfSessionsSucc)
#else
INT1
nmhGetFsIkeNoOfSessionsSucc (pFsTunnelTerminationAddr,
                             pu4RetValFsIkeNoOfSessionsSucc)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeNoOfSessionsSucc;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeNoOfSessionsSucc =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsSucceed;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsIkeNoOfSessionsFailure
 Input       :  The Indices
                FsTunnelTerminationAddr

                The Object 
                retValFsIkeNoOfSessionsFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIkeNoOfSessionsFailure (tSNMP_OCTET_STRING_TYPE *
                                pFsTunnelTerminationAddr,
                                UINT4 *pu4RetValFsIkeNoOfSessionsFailure)
#else
INT1
nmhGetFsIkeNoOfSessionsFailure (pFsTunnelTerminationAddr,
                                pu4RetValFsIkeNoOfSessionsFailure)
     tSNMP_OCTET_STRING_TYPE *pFsTunnelTerminationAddr;
     UINT4              *pu4RetValFsIkeNoOfSessionsFailure;
#endif
{
    INT4                i4Index = IKE_ZERO;
    tIkeIpAddr          IpAddr;

    pFsTunnelTerminationAddr->pu1_OctetList[pFsTunnelTerminationAddr->
                                            i4_Length] = '\0';
    IkeGetIpAddrFromString (pFsTunnelTerminationAddr->pu1_OctetList, &IpAddr);
    if (IpAddr.u4AddrType == IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    i4Index = IkeGetIndexFromStatisticsTable (&IpAddr, IKE_NONE);
    if (i4Index < IKE_ZERO)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValFsIkeNoOfSessionsFailure =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsFailed;
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsIkeVpnPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIkeVpnPolicyTable
 Input       :  The Indices
                FsIkeVpnPolicyName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIkeVpnPolicyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIkeVpnPolicyTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsIkeVpnPolicyName)
#else
INT1
nmhValidateIndexInstanceFsIkeVpnPolicyTable (pFsIkeVpnPolicyName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
#endif
{
    tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

    pIkeVpnPolicy =
        IkeSnmpGetVpnPolicy (pFsIkeVpnPolicyName->pu1_OctetList,
                             pFsIkeVpnPolicyName->i4_Length);
    if (pIkeVpnPolicy != NULL)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIkeVpnPolicyTable
 Input       :  The Indices
                FsIkeVpnPolicyName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIkeVpnPolicyTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIkeVpnPolicyTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsIkeVpnPolicyName)
#else
INT1
nmhGetFirstIndexFsIkeVpnPolicyTable (pFsIkeVpnPolicyName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
#endif
{
    tIkeVpnPolicy      *pVpnPolicy = NULL;

    pVpnPolicy = (tIkeVpnPolicy *) SLL_FIRST (&gIkeVpnPolicyList);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pFsIkeVpnPolicyName->i4_Length =
        (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);
    MEMCPY (pFsIkeVpnPolicyName->pu1_OctetList, pVpnPolicy->au1VpnPolicyName,
            pFsIkeVpnPolicyName->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIkeVpnPolicyTable
 Input       :  The Indices
                FsIkeVpnPolicyName
                nextFsIkeVpnPolicyName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIkeVpnPolicyTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIkeVpnPolicyTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsIkeVpnPolicyName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsIkeVpnPolicyName)
#else
INT1
nmhGetNextIndexFsIkeVpnPolicyTable (pFsIkeVpnPolicyName,
                                    pNextFsIkeVpnPolicyName)
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
     tSNMP_OCTET_STRING_TYPE *pNextFsIkeVpnPolicyName;
#endif
{
    tIkeVpnPolicy      *pVpnPolicy = NULL;
    tIkeVpnPolicy      *pNextVpnPolicy = NULL;

    SLL_SCAN (&gIkeVpnPolicyList, pVpnPolicy, tIkeVpnPolicy *)
    {
        if (MEMCMP
            (pFsIkeVpnPolicyName->pu1_OctetList, pVpnPolicy->au1VpnPolicyName,
             pFsIkeVpnPolicyName->i4_Length) == IKE_ZERO)
        {
            pNextVpnPolicy =
                (tIkeVpnPolicy *) SLL_NEXT (&gIkeVpnPolicyList,
                                            (t_SLL_NODE *) pVpnPolicy);
            if (pNextVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            pNextFsIkeVpnPolicyName->i4_Length =
                (INT4) STRLEN (pNextVpnPolicy->au1VpnPolicyName);
            MEMCPY (pNextFsIkeVpnPolicyName->pu1_OctetList,
                    pNextVpnPolicy->au1VpnPolicyName,
                    pNextFsIkeVpnPolicyName->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIkeVpnPolicyRowStatus
 Input       :  The Indices
                FsIkeVpnPolicyName

                The Object 
                retValFsIkeVpnPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIkeVpnPolicyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIkeVpnPolicyRowStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeVpnPolicyName,
                               INT4 *pi4RetValFsIkeVpnPolicyRowStatus)
#else
INT1
nmhGetFsIkeVpnPolicyRowStatus (pFsIkeVpnPolicyName,
                               pi4RetValFsIkeVpnPolicyRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
     INT4               *pi4RetValFsIkeVpnPolicyRowStatus;
#endif
{
    tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

    pIkeVpnPolicy =
        IkeSnmpGetVpnPolicy (pFsIkeVpnPolicyName->pu1_OctetList,
                             pFsIkeVpnPolicyName->i4_Length);
    if (pIkeVpnPolicy != NULL)
    {
        *pi4RetValFsIkeVpnPolicyRowStatus = (INT4) pIkeVpnPolicy->u1Status;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIkeVpnPolicyRowStatus
 Input       :  The Indices
                FsIkeVpnPolicyName

                The Object 
                setValFsIkeVpnPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIkeVpnPolicyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIkeVpnPolicyRowStatus (tSNMP_OCTET_STRING_TYPE * pFsIkeVpnPolicyName,
                               INT4 i4SetValFsIkeVpnPolicyRowStatus)
#else
INT1
nmhSetFsIkeVpnPolicyRowStatus (pFsIkeVpnPolicyName,
                               i4SetValFsIkeVpnPolicyRowStatus)
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
     INT4                i4SetValFsIkeVpnPolicyRowStatus;

#endif
{
    tIkeConfigIfParam   IkeConfigIfParam;

    switch (i4SetValFsIkeVpnPolicyRowStatus)
    {

        case IKE_CREATEANDWAIT:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;
            INT1                i1Count = IKE_ZERO;
            UINT4               u4VpnPolicyCount = IKE_ZERO;

            pIkeVpnPolicy = IkeSnmpGetVpnPolicy (pFsIkeVpnPolicyName->
                                                 pu1_OctetList,
                                                 pFsIkeVpnPolicyName->
                                                 i4_Length);
            if (pIkeVpnPolicy != NULL)
            {
                return (SNMP_FAILURE);
            }

            TAKE_IKE_SEM ();
            u4VpnPolicyCount = TMO_SLL_Count (&(gIkeVpnPolicyList));
            if (u4VpnPolicyCount >= (MAX_NUMBER_OF_SA / IKE_TWO))
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "VPN POLICY",
                             "nmhSetFsIkeVpnPolicyRowStatus: Unable "
                             " to create Vpn Policy reached maximum limit\n");
                GIVE_IKE_SEM ();
                return (SNMP_FAILURE);
            }
            GIVE_IKE_SEM ();

            /* Send Msg to IKE to create the VPN Policy */
            IkeConfigIfParam.u4MsgType = IKE_CONFIG_NEW_VPN_POLICY;

            IKE_MEMCPY
                (IkeConfigIfParam.ConfigReq.IkeConfCreateVpnPolicy.
                 au1VpnPolicyName,
                 pFsIkeVpnPolicyName->pu1_OctetList,
                 pFsIkeVpnPolicyName->i4_Length);

            IkeConfigIfParam.ConfigReq.IkeConfCreateVpnPolicy.
                au1VpnPolicyName[pFsIkeVpnPolicyName->i4_Length] = '\0';
            IkeSendConfMsgToIke (&IkeConfigIfParam);

            /* Delay the task so that IKE task can create the engine */
            for (i1Count = IKE_ZERO; i1Count < IKE_THREE; i1Count++)
            {
                OsixDelayTask (IKE_ONE);
                if (IkeSnmpGetVpnPolicy (pFsIkeVpnPolicyName->pu1_OctetList,
                                         pFsIkeVpnPolicyName->i4_Length) !=
                    NULL)
                {
                    return (SNMP_SUCCESS);
                }
            }
            return (SNMP_FAILURE);
        }

        case IKE_ACTIVE:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

            pIkeVpnPolicy = IkeSnmpGetVpnPolicy
                (pFsIkeVpnPolicyName->pu1_OctetList,
                 pFsIkeVpnPolicyName->i4_Length);

            if (pIkeVpnPolicy != NULL)
            {
                if (IkeCheckVpnPolicyAttributes (pIkeVpnPolicy) == IKE_SUCCESS)
                {
                    pIkeVpnPolicy->u1Status = IKE_ACTIVE;
                    return (SNMP_SUCCESS);
                }
                else
                {
                    /* All attributes have not been set, return FAILURE */
                    return (SNMP_FAILURE);
                }
            }
            else
                return (SNMP_FAILURE);
        }

        case IKE_NOTINSERVICE:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;

            pIkeVpnPolicy = IkeSnmpGetVpnPolicy
                (pFsIkeVpnPolicyName->pu1_OctetList,
                 pFsIkeVpnPolicyName->i4_Length);

            if (pIkeVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pIkeVpnPolicy->u1Status == IKE_NOTINSERVICE)
            {
                return (SNMP_SUCCESS);
            }

            if (IKE_ACTIVE == pIkeVpnPolicy->u1Status)
            {
                pIkeVpnPolicy->u1Status = IKE_NOTINSERVICE;
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_VPN_POLICY;
                IkeConfigIfParam.ConfigReq.u4VpnPolicyIndex =
                    pIkeVpnPolicy->u4VpnPolicyIndex;
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                return (SNMP_SUCCESS);
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }

        case IKE_DESTROY:
        {
            tIkeVpnPolicy      *pIkeVpnPolicy = NULL;
            INT1                i1Count = IKE_ZERO;

            pIkeVpnPolicy = IkeSnmpGetVpnPolicy
                (pFsIkeVpnPolicyName->pu1_OctetList,
                 pFsIkeVpnPolicyName->i4_Length);

            if (pIkeVpnPolicy != NULL)
            {
                /* We will only mark this vpn policy to be destroyed, 
                   and send an event to IKE to actually delete the structure
                 */
                pIkeVpnPolicy->u1Status = IKE_DESTROY;
                IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_VPN_POLICY;
                IkeConfigIfParam.ConfigReq.u4VpnPolicyIndex =
                    pIkeVpnPolicy->u4VpnPolicyIndex;
                IkeSendConfMsgToIke (&IkeConfigIfParam);
                for (i1Count = IKE_ZERO; i1Count < IKE_THREE; i1Count++)
                {
                    OsixDelayTask (IKE_ONE);
                    if (IkeSnmpGetVpnPolicy (pFsIkeVpnPolicyName->pu1_OctetList,
                                             pFsIkeVpnPolicyName->i4_Length)
                        == NULL)
                    {
                        return (SNMP_SUCCESS);
                    }
                }

                return (SNMP_FAILURE);
            }
            else
                return (SNMP_FAILURE);
        }
        default:
            break;
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIkeVpnPolicyRowStatus
 Input       :  The Indices
                FsIkeVpnPolicyName

                The Object 
                testValFsIkeVpnPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIkeVpnPolicyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIkeVpnPolicyRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsIkeVpnPolicyName,
                                  INT4 i4TestValFsIkeVpnPolicyRowStatus)
#else
INT1
nmhTestv2FsIkeVpnPolicyRowStatus (pu4ErrorCode, pFsIkeVpnPolicyName,
                                  i4TestValFsIkeVpnPolicyRowStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pFsIkeVpnPolicyName;
     INT4                i4TestValFsIkeVpnPolicyRowStatus;
#endif
{

    if (i4TestValFsIkeVpnPolicyRowStatus == IKE_NOTREADY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsIkeVpnPolicyRowStatus == IKE_CREATEANDGO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIkeVpnPolicyRowStatus < IKE_ACTIVE) ||
        (i4TestValFsIkeVpnPolicyRowStatus > IKE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (nmhValidateIndexInstanceFsIkeVpnPolicyTable (pFsIkeVpnPolicyName) ==
        SNMP_SUCCESS)
    {
        if (i4TestValFsIkeVpnPolicyRowStatus == IKE_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        else
            return (SNMP_SUCCESS);
    }
    else
    {
        if (i4TestValFsIkeVpnPolicyRowStatus != IKE_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        else
            return (SNMP_SUCCESS);
    }
}
