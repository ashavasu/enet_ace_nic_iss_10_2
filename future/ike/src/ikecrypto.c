/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecrypto.c,v 1.11 2013/11/12 11:17:00 siva Exp $
 *
 * Description: This has functions for IKE used algorithms 
 *
 ***********************************************************************/
#include "ikegen.h"
#include "utilalgo.h"

tIkeHmacAlgo        gaIkeHmacAlgoList[] = {
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_MD5_LENGTH, IkeHmacMd5Algo}
    ,
    {IKE_SHA_LENGTH, IkeHmacSha1Algo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_XCBC_LENGTH, IkeXcbcMacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_ZERO, IkeInvalidHmacAlgo}
    ,
    {IKE_SHA256_LENGTH, IkeHmacSha256Algo}
    ,
    {IKE_SHA384_LENGTH, IkeHmacSha384Algo}
    ,
    {IKE_SHA512_LENGTH, IkeHmacSha512Algo}
};

tIkeHashAlgo        gaIkeHashAlgoList[] = {
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_MD5_LENGTH, IkeMd5HashAlgo}
    ,
    {IKE_SHA_LENGTH, IkeSha1HashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_ZERO, IkeInvalidHashAlgo}
    ,
    {IKE_SHA256_LENGTH, IkeSha256HashAlgo}
    ,
    {IKE_SHA384_LENGTH, IkeSha384HashAlgo}
    ,
    {IKE_SHA512_LENGTH, IkeSha512HashAlgo}
};

tIkeCipherAlgo      gaIkeCipherAlgoList[] = {
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_EIGHT, IKE_EIGHT}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_EIGHT, IKE_TWENTYFOUR}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_SIXTEEN, IKE_TWENTYFOUR}
    ,
    {IKE_SIXTEEN, IKE_SIXTEEN}
};

tIkeCipherAlgo      gaIke2CipherAlgoList[] = {
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_EIGHT, IKE_EIGHT}
    ,
    {IKE_EIGHT, IKE_TWENTYFOUR}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_ZERO, IKE_ZERO}
    ,
    {IKE_SIXTEEN, IKE_TWENTYFOUR}
    ,
    {IKE_SIXTEEN, IKE_SIXTEEN}
};
UINT1               gau1IKEToIPSecEncrAlgo[] = {
    IKE_ZERO,                    /* Not defined */
    IKE_TWO,                    /* DES IKE (1) --> IPSec (2) */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_THREE,                    /* 3DES IKE (5) --> IPSec (3) */
    IKE_ZERO,                    /* Not supported */
    IKE_TWELVE,                    /*AES IKE (7) ---> IPSec (12) */
    IKE_THIRTEEN                /* AES IKE CTR (8) ---> IPSec (13) */
};

UINT1               gau1IPSecToIKEEncrAlgo[] = {
    IKE_ZERO,                    /* Not defined */
    IKE_ZERO,                    /* Not supported */
    IKE_ONE,                    /* DES IPSec (2) --> IKE (1) */
    IKE_FIVE,                    /* 3DES IPSec (3) --> IKE (5) */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* Not supported */
    IKE_ZERO,                    /* NULL IPSec (11) --> IKE (Not supported) */
    IKE_SEVEN,                    /* AES IPSec (12) --> IKE (7) */
    IKE_EIGHT                    /* AES IPSec (13) --> IKE (8) */
};

UINT1               gau1ESPToIKEHashAlgo[] = {
    IKE_ZERO,                    /* Not defined */
    IKE_ONE,                    /* MD5 ESP (1) --> IKE (1) */
    IKE_TWO,                    /* SHA1 ESP (2) --> IKE (2) */
    IKE_ZERO,
    IKE_ZERO,
    IKE_FIVE,                    /*AES XCBC MAC (5) */
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_TWELVE,                    /*HMAC SHA 256 (12) */
    IKE_THIRTEEN,                /*HMAC SHA 384 (13) */
    IKE_FOURTEEN                /*HMAC SHA 512 (14) */
};

UINT1               gau1AHToIKEHashAlgo[] = {
    IKE_ZERO,                    /* Not defined */
    IKE_ONE,                    /* MD5 AH (1) --> IKE (1) */
    IKE_TWO,                    /* SHA1 AH (2) --> IKE (2) */
    IKE_ZERO,
    IKE_ZERO,
    IKE_FIVE,                    /*AES XCBC MAC (5) */
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_ZERO,
    IKE_TWELVE,                    /*HMAC SHA 256 (12) */
    IKE_THIRTEEN,                /*HMAC SHA 384 (13) */
    IKE_FOURTEEN                /*HMAC SHA 512 (14) */
};

/* --------------------------------------------------------------------- */
/************************************************************************/
/*  Function Name   : IkeInvalidHmacAlgo                                */
/*  Description     : Dummy function                                    */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/

INT4
IkeInvalidHmacAlgo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                    INT4 i4BufLen, UINT1 *pu1Digest)
{
    UNUSED_PARAM (pu1BufPtr);
    UNUSED_PARAM (i4BufLen);
    UNUSED_PARAM (pu1Key);
    UNUSED_PARAM (i4KeyLen);
    UNUSED_PARAM (pu1Digest);

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeInvalidHmacAlgo: Unknown Hmac Algorithm \n");
    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   : IkeHmacMd5Algo                                    */
/*  Description     : The interface function to HMAC MD5                */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeHmacMd5Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeXcbcMacAlgo                                    */
/*  Description     : The interface function to AES XCBC MAC            */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeXcbcMacAlgo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pu1Key;
    UtilAlgo.UtilAesAlgo.u4AesInKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1BufPtr;
    UtilAlgo.UtilAesAlgo.u4AesInBufSize = (UINT4) i4BufLen;
    UtilAlgo.UtilAesAlgo.pu1AesMac = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   : IkeHmacSha256Algo                                 */
/*  Description     : The interface function to HMAC SHA 256            */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeHmacSha256Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                   INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.HmacShaVersion = IKE_TWO;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeHmacSha384Algo                                 */
/*  Description     : The interface function to HMAC SHA 384            */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeHmacSha384Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                   INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.HmacShaVersion = IKE_THREE;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeHmacSha512Algo                                 */
/*  Description     : The interface function to HMAC SHA 512           */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeHmacSha512Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                   INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.HmacShaVersion = IKE_FOUR;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeHmacSha1Algo                                   */
/*  Description     : The interface function to HMAC SHA1               */
/*  Input(s)        : pu1Key - The input key                            */
/*                  : i4KeyLen - The length of the input key            */
/*                  : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeHmacSha1Algo (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                 INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_SHA1, &UtilAlgo);
    return IKE_SUCCESS;
}

/*------------------------------- */
/************************************************************************/
/*  Function Name   : IkeInvalidHashAlgo                                */
/*  Description     : Dummy function                                    */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/

VOID
IkeInvalidHashAlgo (UINT1 *pu1BufPtr, INT4 i4BufLen, UINT1 *pu1Digest)
{
    UNUSED_PARAM (pu1BufPtr);
    UNUSED_PARAM (i4BufLen);
    UNUSED_PARAM (pu1Digest);

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeInvalidHashAlgo: Unknown Hash Algorithm \n");
    return;
}

/************************************************************************/
/*  Function Name   : IkeMd5HashAlgoo                                   */
/*  Description     : The interface function to MD5                     */
/*  Input(s)        : pu1BufPtr - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The MD5 digest value                  */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeMd5HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1Buf;
    UtilAlgo.UtilMd5Algo.u4Md5InBufLen = (UINT4) i4BufLen;
    UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSha1HashAlgo                                   */
/*  Description     : The interface function to SHA1                    */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA1 digest value                 */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeSha1HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilSha1Algo.pu1Sha1Buf = pu1Buf;
    UtilAlgo.UtilSha1Algo.u4Sha1BufLen = (UINT4) i4BufLen;
    UtilAlgo.UtilSha1Algo.pu1Sha1Digest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_SHA1_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSha224HashAlgo                                 */
/*  Description     : The interface function to SHA224                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA224 digest value               */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeSha224HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA224_ALGO;
    UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = pu1Buf;
    UtilAlgo.UtilSha2Algo.i4Sha2BufLen = i4BufLen;
    UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_SHA224_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSha256HashAlgo                                 */
/*  Description     : The interface function to SHA256                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA256 digest value               */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeSha256HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA256_ALGO;
    UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = pu1Buf;
    UtilAlgo.UtilSha2Algo.i4Sha2BufLen = i4BufLen;
    UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_SHA256_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSha384HashAlgo                                 */
/*  Description     : The interface function to SHA384                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA384 digest value               */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeSha384HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA384_ALGO;
    UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = pu1Buf;
    UtilAlgo.UtilSha2Algo.i4Sha2BufLen = i4BufLen;
    UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_SHA384_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSha512HashAlgo                                 */
/*  Description     : The interface function to SHA512                  */
/*  Input(s)        : pu1Buf    - The input buffer                      */
/*                  : i4BufLen - The input buffer length                */
/*  Output(s)       : pu1Digest - The SHA512 digest value               */
/*  Returns         : NONE                                              */
/************************************************************************/

VOID
IkeSha512HashAlgo (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Digest)
{
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, IKE_ZERO, sizeof (unUtilAlgo));

    UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA512_ALGO;
    UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = pu1Buf;
    UtilAlgo.UtilSha2Algo.i4Sha2BufLen = i4BufLen;
    UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Digest;

    UtilMac (ISS_UTIL_ALGO_SHA512_ALGO, &UtilAlgo);
    return;
}

/************************************************************************/
/*  Function Name   : IkeDesSetKey                                      */
/*  Description     : This function allocates space and Initialize DES  */
/*                  : key schedule arrays                               */
/*  Input(s)        :pu1KeyMat-Refers to the  keying material           */
/*                  :DesKey-Refers to the Input key with which data is  */
/*                  :to be encrypted                                    */
/*  Output(s)       : DesKey-Key schedule array is initialized          */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/************************************************************************/

INT1
IkeDesSetKey (UINT1 *pu1KeyMat, tDesKey * pDesKey)
{
    unArCryptoKey       ArCryptoKey;

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    if (pu1KeyMat == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDesSetKey: IkeDesSetKey Failure \n");
        return IKE_FAILURE;
    }
    DesArKeyScheduler (pu1KeyMat, &ArCryptoKey);
    IKE_MEMCPY (pDesKey, ArCryptoKey.tArDes.au8ArSubkey,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeDesCbcEncrypt                                  */
/*  Description     : The function for DES CBC Encryption               */
/*  Input(s)        :pu1Buf:Refers to the Incoming Data to be           */
/*                  :Encrypted                                          */
/*                  :u2BufSize - Buffer Size                            */
/*                  :Key:Refers to the key for encryption               */
/*                  :pu1InitVector:The Initialization Vector            */
/*                  :                                                   */
/*  Output(s)       : pu1OutBuf - The Encrypted buffer                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

VOID
IkeDesCbcEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
                  tDesKey Key, UINT1 *pu1InitVector)
{
    unArCryptoKey       ArCryptoKey;

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey, Key, sizeof (tDesKey));

    DesArCbcEncrypt (pu1Buf, u2BufSize, &ArCryptoKey, pu1InitVector);
    return;
}

/************************************************************************/
/*  Function Name   : IkeDesCbcDecrypt                                  */
/*  Description     : The function for DES CBC Decryption               */
/*  Input(s)        :pu1Buf:Refers to the Incoming Data to be           */
/*                  :decrypted                                          */
/*                  :u2BufSize - Buffer Size                            */
/*                  :Key:Refers to the key for  decryption              */
/*                  :pu1InitVector:The Initialization Vector            */
/*                  :                                                   */
/*  Output(s)       : pu1OutBuf - The Decrypted buffer                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

VOID
IkeDesCbcDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
                  tDesKey Key, UINT1 *pu1InitVector)
{
    unArCryptoKey       ArCryptoKey;

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    IKE_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey, Key, sizeof (tDesKey));

    DesArCbcDecrypt (pu1Buf, u2BufSize, &ArCryptoKey, pu1InitVector);
    return;
}

/************************************************************************/
/*  Function Name   : IkeTripleDesSetKey                                 */
/*  Description     : This function allocates space and Initialize 3DES */
/*                  : key schedule arrays                               */
/*  Input(s)        :pu1KeyMat-Refers to the  keying material           */
/*                  :DesKey-Refers to the Input key with which data is  */
/*                  :to be encrypted                                    */
/*  Output(s)       : DesKey-Key schedule array is initialized          */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/************************************************************************/

INT1
IkeTripleDesSetKey (UINT1 *pu1KeyMat, tDesKey * pDesKey1, tDesKey * pDesKey2,
                    tDesKey * pDesKey3)
{

    if (pu1KeyMat == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeTripleDesSetKey: IkeTripleDesSetKey Failure \n");
        return IKE_FAILURE;

    }

    IkeDesSetKey (pu1KeyMat, pDesKey1);
    IkeDesSetKey ((pu1KeyMat + IKE_DES_KEY_LEN), pDesKey2);
    IkeDesSetKey ((pu1KeyMat + IKE_TWO * IKE_DES_KEY_LEN), pDesKey3);

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeTripleDesCbcEncrypt                            */
/*  Description     : The function for 3DES CBC Encryption              */
/*  Input(s)        :pu1Buf - Refers to the Incoming Data to be         */
/*                  :         Encrypted                                 */
/*                  :u2BufSize - Buffer Size                            */
/*                  :pu1Key1 - The Key for encryption                   */
/*                  :pu1Key2 - The Key for encryption                   */
/*                  :pu1Key3 - The Key for encryption                   */
/*                  :pu1InitVector:The Initialization Vector            */
/*                  :                                                   */
/*  Output(s)       : pu1OutBuf - The Encrypted buffer                  */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

VOID
IkeTripleDesCbcEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
                        tDesKey Key1, tDesKey Key2, tDesKey Key3,
                        UINT1 *pu1InitVector)
{
    unArCryptoKey       ArCryptoKey;

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey, Key1, sizeof (tDesKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey, Key2, sizeof (tDesKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey, Key3, sizeof (tDesKey));

    TDesArCbcEncrypt (pu1Buf, u2BufSize, &ArCryptoKey, pu1InitVector);
    return;
}

/************************************************************************/
/*  Function Name   : IkeTripleDesCbcDecrypt                            */
/*  Description     : The function for 3DES CBC Decryption              */
/*   Input(s)       : pu1InBuf - The input buffer                       */
/*                  : u2BufSize - The input buffer size                 */
/*                  : pu1Key1 - The Key for decryption                  */
/*                  : pu1Key2 - The Key for decryption                  */
/*                  : pu1Key3 - The Key for decryption                  */
/*                  : pu1InitVector - The Initialization vector         */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

VOID
IkeTripleDesCbcDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize,
                        tDesKey Key1, tDesKey Key2, tDesKey Key3,
                        UINT1 *pu1InitVector)
{
    unArCryptoKey       ArCryptoKey;

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey, Key1, sizeof (tDesKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey, Key2, sizeof (tDesKey));
    IKE_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey, Key3, sizeof (tDesKey));

    TDesArCbcDecrypt (pu1Buf, u2BufSize, &ArCryptoKey, pu1InitVector);
    return;
}

UINT1               gau1DesWeakKeys[IKE_DES_MAX_WEAK_KEYS][IKE_DES_KEY_LEN] = {
    /* Weak keys */
    {IKE_DES_KEY_0x01, IKE_DES_KEY_0x01, IKE_DES_KEY_0x01, IKE_DES_KEY_0x01,
     IKE_DES_KEY_0x01, IKE_DES_KEY_0x01, IKE_DES_KEY_0x01, IKE_DES_KEY_0x01},
    {IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F,
     IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0},
    {IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xE0,
     IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x1F},
    {IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE,
     IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xFE},
    /* Semi weak keys */
    {IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE,
     IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE},
    {IKE_DES_KEY_0x1F, IKE_DES_KEY_0xE0, IKE_DES_KEY_0x1F, IKE_DES_KEY_0xE0,
     IKE_DES_KEY_0x0E, IKE_DES_KEY_0xF1, IKE_DES_KEY_0x0E, IKE_DES_KEY_0xF1},
    {IKE_DES_KEY_0x01, IKE_DES_KEY_0xE0, IKE_DES_KEY_0x01, IKE_DES_KEY_0xE0,
     IKE_DES_KEY_0x01, IKE_DES_KEY_0xF1, IKE_DES_KEY_0x01, IKE_DES_KEY_0xF1},
    {IKE_DES_KEY_0x1F, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x1F, IKE_DES_KEY_0xFE,
     IKE_DES_KEY_0x0E, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x0E, IKE_DES_KEY_0xFE},
    {IKE_DES_KEY_0x01, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x01, IKE_DES_KEY_0x1F,
     IKE_DES_KEY_0x01, IKE_DES_KEY_0x0E, IKE_DES_KEY_0x01, IKE_DES_KEY_0x0E},
    {IKE_DES_KEY_0xE0, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xFE,
     IKE_DES_KEY_0xF1, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xF1, IKE_DES_KEY_0xFE},
    {IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01,
     IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x01},
    {IKE_DES_KEY_0xE0, IKE_DES_KEY_0x1F, IKE_DES_KEY_0xE0, IKE_DES_KEY_0x1F,
     IKE_DES_KEY_0xF1, IKE_DES_KEY_0x0E, IKE_DES_KEY_0xF1, IKE_DES_KEY_0x0E},
    {IKE_DES_KEY_0xE0, IKE_DES_KEY_0x01, IKE_DES_KEY_0xE0, IKE_DES_KEY_0x01,
     IKE_DES_KEY_0xF1, IKE_DES_KEY_0x01, IKE_DES_KEY_0xF1, IKE_DES_KEY_0x01},
    {IKE_DES_KEY_0xFE, IKE_DES_KEY_0x1F, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x1F,
     IKE_DES_KEY_0xFE, IKE_DES_KEY_0x0E, IKE_DES_KEY_0xFE, IKE_DES_KEY_0x0E},
    {IKE_DES_KEY_0x1F, IKE_DES_KEY_0x01, IKE_DES_KEY_0x1F, IKE_DES_KEY_0x01,
     IKE_DES_KEY_0x0E, IKE_DES_KEY_0x01, IKE_DES_KEY_0x0E, IKE_DES_KEY_0x01},
    {IKE_DES_KEY_0xFE, IKE_DES_KEY_0xE0, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xE0,
     IKE_DES_KEY_0xFE, IKE_DES_KEY_0xF1, IKE_DES_KEY_0xFE, IKE_DES_KEY_0xF1}
};

/************************************************************************/
/*  Function Name   : IkeDesIsWeakKey                                   */
/*  Description     : This function checks whether the given key is a   */
/*                  : weak or semi-weak key                             */
/*  Input(s)        : pu1Key                                            */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_TRUE or IKE_FALSE                             */
/************************************************************************/

BOOLEAN
IkeDesIsWeakKey (UINT1 *pu1Key)
{
    INT4                i4Count = IKE_ZERO;
    if (pu1Key == NULL)
    {
        return IKE_FALSE;
    }

    for (i4Count = IKE_ZERO; i4Count < IKE_DES_MAX_WEAK_KEYS; i4Count++)
    {
        if (IKE_MEMCMP (pu1Key, gau1DesWeakKeys[i4Count], IKE_DES_KEY_LEN)
            == IKE_ZERO)
        {
            return IKE_TRUE;
        }
    }
    return IKE_FALSE;
}

/************************************************************************/
/*  Function Name   : IkeAesSetKey                                      */
/*  Description     : This function allocates space and Initialize AES  */
/*                  : key schedule arrays                               */
/*  Input(s)        : pu4InKey-Refers to the Input key with             */
/*                  : which encryption and decryption keys are derived  */
/*                  : u1InKeyLen - The Key  Length                      */
/*  Output(s)       :                                                   */
/*                  : pu4EncrKey - The Key for Encryption               */
/*                  : pu4DecrKey - The Key for Decryption               */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/************************************************************************/

INT1
IkeAesSetKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesEncrKey,
              UINT1 *au1AesDecrKey)
{
    unArCryptoKey       ArCryptoKey;
    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    if (u1InKeyLen == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAesSetKey: IkeAesSetKey Failure \n");
        return IKE_FAILURE;

    }
    /* Do Key Schedule for Encryption and Decryption */

    AesArSetEncryptKey (pu1InKey, (UINT2) (u1InKeyLen * IKE_EIGHT),
                        &ArCryptoKey);
    IKE_MEMCPY (au1AesEncrKey, ArCryptoKey.tArAes.au1ArSubkey,
                sizeof (ArCryptoKey.tArAes.au1ArSubkey));

    AesArSetDecryptKey (pu1InKey, (UINT2) (u1InKeyLen * IKE_EIGHT),
                        &ArCryptoKey);
    IKE_MEMCPY (au1AesDecrKey, ArCryptoKey.tArAes.au1ArSubkey,
                sizeof (ArCryptoKey.tArAes.au1ArSubkey));

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeAesSetEncrKey                                  */
/*  Description     : This function allocates space and Initialize AES  */
/*                  : encrypt key schedule arrays for IKEv2             */
/*  Input(s)        : pu4InKey-Refers to the Input key with             */
/*                  : which encryption key is derived                   */
/*                  : u1InKeyLen - The Key  Length                      */
/*  Output(s)       :                                                   */
/*                  : pAesCtxEncr - The Key for Encryption              */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/************************************************************************/

INT1
IkeAesSetEncrKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesEncrKey)
{
    unArCryptoKey       ArCryptoKey;
    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));

    if (u1InKeyLen == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAesSetEncrKey: key length is zero \n");
        return IKE_FAILURE;

    }
    /* Do Key Schedule for Encryption and Decryption */
    AesArSetEncryptKey (pu1InKey, (UINT2) (u1InKeyLen * IKE_EIGHT),
                        &ArCryptoKey);
    IKE_MEMCPY (au1AesEncrKey, ArCryptoKey.tArAes.au1ArSubkey,
                sizeof (ArCryptoKey.tArAes.au1ArSubkey));
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeAesSetDecrKey                                  */
/*  Description     : This function allocates space and Initialize AES  */
/*                  : decrypt key schedule arrays for Ikev2             */
/*  Input(s)        : pu4InKey-Refers to the Input key with             */
/*                  : which decryption key is derived                   */
/*                  : u1InKeyLen - The Key  Length                      */
/*  Output(s)       :                                                   */
/*                  : pAesCtxDecr - The Key for Decryption              */
/*  Returns         : IKE_FAILURE or IKE_SUCCESS                        */
/************************************************************************/

INT1
IkeAesSetDecrKey (UINT1 *pu1InKey, UINT1 u1InKeyLen, UINT1 *au1AesDecrKey)
{
    unArCryptoKey       ArCryptoKey;
    MEMSET (&ArCryptoKey, IKE_ZERO, sizeof (unArCryptoKey));
    if (u1InKeyLen == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAesSetDecrKey: Key length is zero \n");
        return IKE_FAILURE;

    }
    AesArSetDecryptKey (pu1InKey, (UINT2) (u1InKeyLen * IKE_EIGHT),
                        &ArCryptoKey);
    IKE_MEMCPY (au1AesDecrKey, ArCryptoKey.tArAes.au1ArSubkey,
                sizeof (ArCryptoKey.tArAes.au1ArSubkey));
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeAesEncrypt                                     */
/*  Description     : The function for AES  Encryption                  */
/*   Input(s)       : pu1Buf - The input buffer                         */
/*                  : u2BufSize - The input buffer length               */
/*                  : u2KeyLen - The input Key length                   */
/*                  : pu4EncrKey - The Key for encryption               */
/*  Output(s)       :  The Encrypted Data                               */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeAesEncrypt (UINT1 *pu1Buf, UINT2 u2BufSize, UINT1 u1InKeyLen,
               UINT1 *au1AesEncrKey, UINT1 *pu1InitVector)
{

    if (pu1Buf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAesEncrypt: IkeAesEncrypt Failure \n");
        return;

    }

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    AesArCbcEncrypt (pu1Buf, u2BufSize, (UINT2) (u1InKeyLen * IKE_EIGHT),
                     au1AesEncrKey, pu1InitVector);
    return;
}

/************************************************************************/
/*  Function Name   : IkeAesDecrypt                                     */
/*  Description     : The function for AES  Decryption                  */
/*   Input(s)       : pu1Buf - The input buffer                         */
/*                  : u2BufSize - The input buffer length               */
/*                  : u2KeyLen - The input key length                   */
/*                  : pu4EncrKey - The Key for decryption               */
/*                  : pu4DecrKey - The Key for decryption               */
/*  Output(s)       :  The Decrypted Data                               */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeAesDecrypt (UINT1 *pu1Buf, UINT2 u2BufSize, UINT1 u1InKeyLen,
               UINT1 *au1AesDecrKey, UINT1 *pu1InitVector)
{

    if (pu1Buf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAesDecrypt: IkeAesDecrypt Failure \n");
        return;

    }

    if (gu1IkeBypassCrypto == BYPASS_ENABLED)
    {
        return;
    }

    AesArCbcDecrypt (pu1Buf, u2BufSize, (UINT2) (u1InKeyLen * IKE_EIGHT),
                     au1AesDecrKey, pu1InitVector);
    return;
}
