/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikenm.c,v 1.23 2015/10/07 11:11:27 siva Exp $
 *
 * Description: This has support functions for Network management 
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikecert.h"
#include "iketdfs.h"
#include "utilrand.h"
#include "ikememmac.h"

/* Global VPN Policy Table */
t_SLL               gIkeVpnPolicyList;
/* Used to assign the engine index */
UINT4               gu4VpnPolicyIndex = IKE_ONE;
/* Global Engine Table */
t_SLL               gIkeEngineList;
/* Used to assign the engine index */
UINT4               gu4EngineIndex = IKE_ONE;
VOID                IkeFreeRemoteAccessInfo (t_SLL_NODE * pNode);

/* Engine related functions */
/************************************************************************/
/*  Function Name   : IkeInitVpnPolicy                                  */
/*  Description     : This function Initializes the VPN Policy Manager  */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeInitVpnPolicy (VOID)
{
    SLL_INIT (&(gIkeVpnPolicyList));
    return;
}

/************************************************************************/
/*  Function Name   : IkeDeInitVpnPolicy                                */
/*  Description     : This function De-initializes the VPN Policies     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeInitVpnPolicy (VOID)
{
    SLL_DELETE_LIST (&(gIkeVpnPolicyList), IkeFreeVpnPolicyInfo);
}

/************************************************************************/
/*  Function Name   : IkeInitDefaultVpnPolicy                           */
/*  Description     : This function Initializes the vpn policy node     */
/*  Input(s)        : pIkeVpnPolicy - Pointer to the vpn policy node    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeInitDefaultVpnPolicy (tIkeVpnPolicy * pIkeVpnPolicy)
{
    pIkeVpnPolicy->u4VpnPolicyIndex = gu4VpnPolicyIndex++;
    pIkeVpnPolicy->u1VpnPolicyAttrMask = IKE_VPN_POLICY_NO_ATTR_SET;
}

/************************************************************************/
/*  Function Name   : IkeDeleteVpnPolicy                                */
/*  Description     : This function Deletes the vpn policy specified    */
/*  Input(s)        : pNode - The vpn policy node                       */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeleteVpnPolicy (t_SLL_NODE * pNode)
{
    IkeFreeVpnPolicyInfo (pNode);
}

/************************************************************************/
/*  Function Name   : IkeFreeVpnPolicyInfo                              */
/*  Description     : This function Frees the VPN Policy Database       */
/*  Input(s)        : pNode - The Key node                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreeVpnPolicyInfo (t_SLL_NODE * pNode)
{
    tIkeVpnPolicy      *pIkeVpnPolicyNode = NULL;

    pIkeVpnPolicyNode = (tIkeVpnPolicy *) pNode;
    MemReleaseMemBlock (IKE_VPN_POLICY_MEMPOOL_ID, (UINT1 *) pIkeVpnPolicyNode);
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetVpnPolicy                               */
/*  Description     : This function is used to get the VPN Policy node  */
/*                  : from the policy Name                              */
/*  Input(s)        : pVpnPolicyName - The policy name                  */
/*                  : NameLen - The Name Length                         */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the vpn policy node                    */
/************************************************************************/

tIkeVpnPolicy      *
IkeSnmpGetVpnPolicy (UINT1 *pVpnPolicyName, INT4 NameLen)
{
    tIkeVpnPolicy      *pVpnPolicy = NULL;

    if (pVpnPolicyName == NULL)
    {
        return (tIkeVpnPolicy *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&gIkeVpnPolicyList, pVpnPolicy, tIkeVpnPolicy *)
    {
        if ((UINT4) NameLen == STRLEN (pVpnPolicy->au1VpnPolicyName))
        {
            if (MEMCMP
                (pVpnPolicyName, pVpnPolicy->au1VpnPolicyName,
                 (size_t) NameLen) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return (pVpnPolicy);
            }
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeCheckVpnPolicyAttributes                       */
/*  Description     : This function is used to check if the params      */
/*                  : required to make the VpnPolicy ACTIVE are set     */
/*  Input(s)        : pIkeVpnPolicy - Pointer to the Vpn Policy         */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckVpnPolicyAttributes (tIkeVpnPolicy * pIkeVpnPolicy)
{
    /* 
     * Check if all the attributes of VPN Policy datbase is set
     */
    if ((pIkeVpnPolicy->u1VpnPolicyAttrMask & IKE_VPN_POLICY_ALL_ATTR_SET)
        == IKE_VPN_POLICY_ALL_ATTR_SET)
    {
        return (IKE_SUCCESS);
    }
    return (IKE_FAILURE);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSetVpnPolicyKeyMode                        */
/*  Description     : This function is used to set the IPsec keying     */
/*                  : mode. If the key mode is preshared, then key      */
/*                    string is also set                                */
/*  Input(s)        : pIkeVpnPolicy  - Pointer to the vpn policy        */
/*                  : Key mode - IPSec keying mode                      */
/*                  : pu1KeyString - Key value (null for manual keying) */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSetVpnPolicyKeyMode (tIkeVpnPolicy * pIkeVpnPolicy,
                            UINT4 u4IpsecKeyMode, UINT1 *pu1KeyString)
{
    switch (u4IpsecKeyMode)
    {
        case IKE_PRESHARED_KEY:
            if (pu1KeyString == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeSnmpSetVpnPolicyKeyMode:\
                             preshread key null\n");
                return (IKE_FAILURE);
            }
            else
            {
                if (STRLEN (pu1KeyString) <
                    STRLEN (IKE_VP_KEY_PRESHARED (pIkeVpnPolicy)))
                {
                    STRCPY (IKE_VP_KEY_PRESHARED (pIkeVpnPolicy), pu1KeyString);
                }
                else
                {
                    return (IKE_FAILURE);
                }

            }
            break;

        case IKE_RSA_SIGNATURE:
            if (pu1KeyString == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeSnmpSetVpnPolicyKeyMode: Cert ID null\n");
                return (IKE_FAILURE);
            }
            else
            {
                STRCPY (IKE_VP_ID_CERT (pIkeVpnPolicy), pu1KeyString);
            }
            break;

        case IPSEC_MANUAL_KEY:
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeSnmpSetVpnPolicyKeyMode: Unsupported key mode\n");
            return (IKE_FAILURE);
    }

    pIkeVpnPolicy->u4IpsecKeyType = u4IpsecKeyMode;
    pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_KEY_MODE_SET;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSetVpnPolicyRemotePeer                     */
/*  Description     : This function is used to set the remote peer      */
/*                  : gateway address, peer id type and peer id value   */
/*  Input(s)        : pIkeVpnPolicy  - Pointer to the vpn policy        */
/*                  : u4PeerIdType - Peer ID type                       */
/*                  : pu1PeerIdVal - Peer ID Value                      */
/*                  : pu1RemTunnTermAddr - Remote tunnel term address   */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSetVpnPolicyRemotePeer (tIkeVpnPolicy * pIkeVpnPolicy,
                               UINT4 u4PeerIdType, UINT1 *pu1PeerIdVal,
                               UINT1 *pu1RemTunnTermAddr)
{
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;
    tIp6Addr            TempIp6Addr;
    UINT4               u4AddrType = IKE_ZERO;

    /* No need set the Phase I peer id for ipsec manual key */
    if (IKE_VP_KEY_TYPE (pIkeVpnPolicy) != IPSEC_MANUAL_KEY)
    {
        tIpsecAutoDB       *pIpsecAutoDB = NULL;
        IKE_VP_GET_AUTODB (pIkeVpnPolicy, pIpsecAutoDB);

        if (NULL == pIpsecAutoDB)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeSnmpSetVpnPolicyRemotePeer: Invalid key mode\n");
            return IKE_FAILURE;
        }

        switch (u4PeerIdType)
        {
            case IKE_KEY_IPV4:
            {
                if (INET_PTON4 (pu1PeerIdVal, &u4TempIp4Addr) > IKE_ZERO)
                {
                    IKE_VP_PEERID (pIpsecAutoDB).uID.Ip4Addr =
                        IKE_NTOHL (u4TempIp4Addr);
                    IKE_VP_PEERID (pIpsecAutoDB).u4Length = sizeof (tIp4Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeSnmpSetVpnPolicyRemotePeer: "
                                 "Invalid IPv4 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }                    /* case ipv4 */

            case IKE_KEY_IPV6:
            {
                if (INET_PTON6 (pu1PeerIdVal, &(TempIp6Addr.u1_addr))
                    > IKE_ZERO)
                {
                    MEMCPY (&IKE_VP_PEERID (pIpsecAutoDB).uID.Ip6Addr,
                            &TempIp6Addr, sizeof (tIp6Addr));
                    IKE_VP_PEERID (pIpsecAutoDB).u4Length = sizeof (tIp6Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeSnmpSetVpnPolicyRemotePeer: "
                                 "Invalid IPv6 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }                    /* case ipv6 */

            case IKE_KEY_EMAIL:
            {
                if (STRLEN (pu1PeerIdVal) >= MAX_NAME_LENGTH)
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeSnmpSetVpnPolicyRemotePeer: "
                                 "Invalid Email Address\n");
                    return (IKE_FAILURE);
                }

                STRCPY (IKE_VP_PEERID (pIpsecAutoDB).uID.au1Email,
                        pu1PeerIdVal);
                IKE_VP_PEERID (pIpsecAutoDB).u4Length = STRLEN (pu1PeerIdVal);
                break;
            }                    /* case email */

            case IKE_KEY_FQDN:
            {
                STRCPY (IKE_VP_PEERID (pIpsecAutoDB).uID.au1Fqdn, pu1PeerIdVal);
                IKE_VP_PEERID (pIpsecAutoDB).u4Length = STRLEN (pu1PeerIdVal);
                break;
            }                    /* case fqdn */

            case IKE_KEY_DN:
            {
                STRCPY (IKE_VP_PEERID (pIpsecAutoDB).uID.au1Dn, pu1PeerIdVal);
                IKE_VP_PEERID (pIpsecAutoDB).u4Length = STRLEN (pu1PeerIdVal);
                break;
            }                    /* case dn */

            default:
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeSnmpSetVpnPolicyRemotePeer:\
                             Unsupported Peer ID" "\r\n");
                return (IKE_FAILURE);
        }                        /* switch-case u4PeerIdType */

        IKE_VP_PEERID (pIpsecAutoDB).i4IdType = (INT4) u4PeerIdType;

    }                            /* Key Mode != IPSEC_MANUAL_KEY */

    /* Now set the Remote gateway address. */

    if (STRCMP (pu1RemTunnTermAddr, CLI_IKE_VP_GW_ANY) == IKE_ZERO)
    {
        pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_REMOTE_PEER_SET;

        return (IKE_SUCCESS);
    }
    u4AddrType = IkeParseAddrString (pu1RemTunnTermAddr);

    if ((u4AddrType != IPV4ADDR) && (u4AddrType != IPV6ADDR))
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeSnmpSetVpnPolicyRemotePeer: Invalid Remote address\n");
        return (IKE_FAILURE);
    }

    pIkeVpnPolicy->RemTunnTermAddr.u4AddrType = u4AddrType;
    if (IPV4ADDR == u4AddrType)
    {
        INET_PTON4 (pu1RemTunnTermAddr, &u4TempIp4Addr);
        pIkeVpnPolicy->RemTunnTermAddr.uIpAddr.Ip4Addr =
            IKE_NTOHL (u4TempIp4Addr);
    }
    else if (IPV6ADDR == u4AddrType)
    {
        INET_PTON6 (pu1RemTunnTermAddr,
                    &pIkeVpnPolicy->RemTunnTermAddr.uIpAddr.Ip6Addr.u1_addr);
    }

    /* Update the vpn policy attribute mask */
    pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_REMOTE_PEER_SET;

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSetVpnPolicyIkeProposal                    */
/*  Description     : This function is used to set the IKE Phase I      */
/*                    paramaters in the vpn policy database             */
/*  Input(s)        : pIkeVpnPolicy  - Pointer to the vpn policy        */
/*                  : pIkeProposal - IKE Phase I proposal paramaters    */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSetVpnPolicyIkeProposal (tIkeVpnPolicy * pIkeVpnPolicy,
                                tIkePolicy * pIkeProposal)
{

    /* No need set the IKE Phase I params for ipsec manual key */
    if (IKE_VP_KEY_TYPE (pIkeVpnPolicy) != IPSEC_MANUAL_KEY)
    {
        tIkePolicy         *pVpnPolicyIkeProposal = NULL;
        tIpsecAutoDB       *pIpsecAutoDB = NULL;
        IKE_VP_GET_AUTODB (pIkeVpnPolicy, pIpsecAutoDB);

        if (NULL == pIpsecAutoDB)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeSnmpSetVpnPolicyIkeProposal: Invalid key mode\n");
            return IKE_FAILURE;
        }

        /* Set the following IKE Phase I paramaters in the vpn policy database:
         * Mode - main/aggressive
         * Hash Algo - md5/sha1
         * Encryption Algo - des/triple-des/aes
         * DH Group - Deffie-Hellman Group 1/2/5/14
         * Liftime Type - secs/minutes/hours
         * Lifetime value
         */

        pVpnPolicyIkeProposal = &(IKE_VP_IKE_PROPOSAL (pIpsecAutoDB));

        pVpnPolicyIkeProposal->u1Mode = pIkeProposal->u1Mode;
        pVpnPolicyIkeProposal->u1HashAlgo = pIkeProposal->u1HashAlgo;
        pVpnPolicyIkeProposal->u1EncryptionAlgo =
            pIkeProposal->u1EncryptionAlgo;
        pVpnPolicyIkeProposal->u1DHGroup = pIkeProposal->u1DHGroup;
        pVpnPolicyIkeProposal->u1LifeTimeType = pIkeProposal->u1LifeTimeType;
        pVpnPolicyIkeProposal->u4LifeTime = pIkeProposal->u4LifeTime;

    }                            /* Key Mode != IPSEC_MANUAL_KEY */

    /* Update the vpn policy attribute mask */
    pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_IKE_PROPOSAL_SET;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSetVpnPolicyIpsecProposal                  */
/*  Description     : This function is used to set the IPSec Phase II   */
/*                    paramaters in the vpn policy database             */
/*  Input(s)        : pIkeVpnPolicy  - Pointer to the vpn policy        */
/*                  : pIpsecProposal - IPSec Phase II paramaters        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSetVpnPolicyIpsecProposal (tIkeVpnPolicy * pIkeVpnPolicy,
                                  tIkePolicy * pIpsecProposal)
{
    /* No need set the IPSec Phase II params for ipsec manual key */
    if (IKE_VP_KEY_TYPE (pIkeVpnPolicy) != IPSEC_MANUAL_KEY)
    {
        tIkePolicy         *pVpnPolicyIpsecProposal = NULL;
        tIpsecAutoDB       *pIpsecAutoDB = NULL;
        IKE_VP_GET_AUTODB (pIkeVpnPolicy, pIpsecAutoDB);

        if (NULL == pIpsecAutoDB)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeSnmpSetVpnPolicyIpsecProposal:\
                         Invalid key mode\n");
            return IKE_FAILURE;
        }

        /* Set the following IPSec Phase II paramaters in the
         * vpn policy database:
         * Mode - tunnel/transport
         * Hash Algo - md5/sha1
         * Encryption Algo - des/triple-des/aes
         * PFS DH Group - Deffie-Hellman Group 1/2/5/14
         * Liftime Type - secs/minutes/hours
         * Lifetime value
         */

        pVpnPolicyIpsecProposal = &(IKE_VP_IPSEC_PROPOSAL (pIpsecAutoDB));

        pVpnPolicyIpsecProposal->u1Mode = pIpsecProposal->u1Mode;
        pVpnPolicyIpsecProposal->u1HashAlgo = pIpsecProposal->u1HashAlgo;
        pVpnPolicyIpsecProposal->u1EncryptionAlgo =
            pIpsecProposal->u1EncryptionAlgo;
        pVpnPolicyIpsecProposal->u1DHGroup = pIpsecProposal->u1DHGroup;
        pVpnPolicyIpsecProposal->u1LifeTimeType =
            pIpsecProposal->u1LifeTimeType;
        pVpnPolicyIpsecProposal->u4LifeTime = pIpsecProposal->u4LifeTime;

        SNPRINTF (((CHR1 *) IKE_VP_CRYPTO_MAP (pIpsecAutoDB)),
                  MAX_NAME_LENGTH, "%sC%ld", IKE_VP_NAME (pIkeVpnPolicy),
                  /* retrieves the Vpn Policy name */
                  IKE_VP_INDEX (pIkeVpnPolicy)
                  /* retrieves the Vpn Policy index */
            );

        SNPRINTF (((CHR1 *) IKE_VP_TRANSFORM_SET (pIpsecAutoDB)),
                  MAX_NAME_LENGTH, "%sT%ld", IKE_VP_NAME (pIkeVpnPolicy),
                  /* retrieves the Vpn Policy name */
                  IKE_VP_INDEX (pIkeVpnPolicy)
                  /* retrieves the Vpn Policy index */
            );

    }                            /* Key Mode != IPSEC_MANUAL_KEY */

    /* Update the vpn policy attribute mask */
    pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_IPSEC_PROPOSAL_SET;
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSetVpnPolicyNetworks                       */
/*  Description     : This function is used to set the local and remote */
/*                    networks which will be used by crypto map         */
/*  Input(s)        : pIkeVpnPolicy  - Pointer to the vpn policy        */
/*                  : pLocalNetwork - Pointer to local network          */
/*                  : pRemoteNetwork - Pointer to remote network        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSetVpnPolicyNetworks (tIkeVpnPolicy * pIkeVpnPolicy,
                             UINT1 *pLocalNetwork, UINT1 *pRemoteNetwork)
{
    if (IkeCryptoSetNetworkAddress (&(pIkeVpnPolicy->LocalNetwork),
                                    pLocalNetwork,
                                    (INT4) STRLEN (pLocalNetwork)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeSnmpSetVpnPolicyNetworks: Invalid Local network\n");
        return IKE_FAILURE;
    }

    if (IkeCryptoSetNetworkAddress (&(pIkeVpnPolicy->RemoteNetwork),
                                    pRemoteNetwork,
                                    (INT4) STRLEN (pRemoteNetwork)) ==
        IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeSnmpSetVpnPolicyNetworks: Invalid remote network\n");
        return IKE_FAILURE;
    }

    /* Update the vpn policy attribute mask */
    pIkeVpnPolicy->u1VpnPolicyAttrMask |= IKE_VPN_POLICY_LOCAL_REMOTE_NW_SET;

    return IKE_SUCCESS;
}

/* Engine related functions */
/************************************************************************/
/*  Function Name   : IkeEngineInitialise                               */
/*  Description     : This function Initializes the Engine structure    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeEngineInitialise (VOID)
{
    SLL_INIT (&(gIkeEngineList));
    return;
}

/************************************************************************/
/*  Function Name   : IkeSocketInit                                     */
/*  Description     : This function Creates and Initializes the Socket  */
/*                  : to listen on for IKE Packets                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeSocketInit (VOID)
{
    tIkeEngine         *pIkeEngine = NULL;

    /* As per the current implementation, only the dummy engine
     * has the valid sockets since same socket is used for listening 
     * in all the engines. */
    pIkeEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                   STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeEngine != NULL)
    {
        if (IkeStartListener (pIkeEngine) == IKE_FAILURE)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeEngineInitialise: Unable to start listner! for  - %s\n",
                          pIkeEngine->au1EngineName);
        }
    }
    return;
}

/************************************************************************/
/*  Function Name   : IkeEngineDeInit                                   */
/*  Description     : This function De-initializes the Engine structure */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeEngineDeInit (VOID)
{
    SLL_DELETE_LIST (&(gIkeEngineList), IkeDeleteEngine);
}

/************************************************************************/
/*  Function Name   : IkeFreePolicyInfo                                 */
/*  Description     : This function Frees the Policy Info structure     */
/*  Input(s)        : pNode - The Policy node                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreePolicyInfo (t_SLL_NODE * pNode)
{
    tIkePolicy         *pIkePolicy = NULL;

    pIkePolicy = (tIkePolicy *) pNode;
    MemReleaseMemBlock (IKE_POLICY_MEMPOOL_ID, (UINT1 *) pIkePolicy);
}

/************************************************************************/
/*  Function Name   : IkeFreeKeyInfo                                    */
/*  Description     : This function Frees the Key Info structure        */
/*  Input(s)        : pNode - The Key node                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreeKeyInfo (t_SLL_NODE * pNode)
{
    tIkeKey            *pIkeKey = NULL;

    pIkeKey = (tIkeKey *) pNode;
    IKE_BZERO (pIkeKey, sizeof (tIkeKey));
    MemReleaseMemBlock (IKE_KEY_MEMPOOL_ID, (UINT1 *) pIkeKey);
}

/************************************************************************/
/*  Function Name   : IkeFreeTransformSetInfo                           */
/*  Description     : This function Frees the Transform-set Info        */
/*  Input(s)        : pNode - The Transform-set node                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreeTransformSetInfo (t_SLL_NODE * pNode)
{
    tIkeTransformSet   *pIkeTransformSet = NULL;

    pIkeTransformSet = (tIkeTransformSet *) pNode;
    MemReleaseMemBlock (IKE_TRANSFORMSET_MEMPOOL_ID,
                        (UINT1 *) pIkeTransformSet);
}

/************************************************************************/
/*  Function Name   : IkeFreeCryptoMapInfo                              */
/*  Description     : This function Frees the Crypto-map info           */
/*  Input(s)        : pNode - The Crypto-map node                       */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreeCryptoMapInfo (t_SLL_NODE * pNode)
{
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    pIkeCryptoMap = (tIkeCryptoMap *) pNode;
    MemReleaseMemBlock (IKE_CRYPTOMAP_MEMPOOL_ID, (UINT1 *) pIkeCryptoMap);
}

/************************************************************************/
/*  Function Name   : IkeFreeSAInfo                                     */
/*  Description     : This function Frees the SA structure              */
/*  Input(s)        : pNode - The SA node                               */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeFreeSAInfo (t_SLL_NODE * pNode)
{
    tIkeSA             *pIkeSA = (tIkeSA *) (void *) pNode;

    IkeStopTimer (&pIkeSA->IkeSALifeTimeTimer);
    IkeStopTimer (&pIkeSA->IkeNatKeepAliveTimer);
    if (pIkeSA->u4RefCount != IKE_ZERO)
    {
        IKE_ASSERT (pIkeSA->u4RefCount == IKE_ZERO);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeFreeSAInfo:RefCount not zero in SA!\n");
        return (IKE_FAILURE);
    }

    /* We memset the keys to 0 to ensure FIPS compliance */
    if (pIkeSA->pSKeyId != NULL)
    {
        IKE_BZERO (pIkeSA->pSKeyId, pIkeSA->u2SKeyIdLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pIkeSA->pSKeyId);
    }

    if (pIkeSA->pSKeyIdD != NULL)
    {
        IKE_BZERO (pIkeSA->pSKeyIdD, pIkeSA->u2SKeyIdLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pIkeSA->pSKeyIdD);
    }

    if (pIkeSA->pSKeyIdA != NULL)
    {
        IKE_BZERO (pIkeSA->pSKeyIdA, pIkeSA->u2SKeyIdLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pIkeSA->pSKeyIdA);
    }

    if (pIkeSA->pSKeyIdE != NULL)
    {
        IKE_BZERO (pIkeSA->pSKeyIdE, pIkeSA->u2EncrKLen);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pIkeSA->pSKeyIdE);
    }

    if (pIkeSA->u1IkeVer == IKE2_MAJOR_VERSION)
    {
        /* Release memory of all the transients required 
         * for authentication. Release the Init Req/Rsp packet 
         * Buffer and ID payloads(Sent/Rcvd) from the session 
         * structure */
        if (pIkeSA->Ike2SA.pu1SaInitReqPkt != NULL)
        {
            IKE_BZERO (pIkeSA->Ike2SA.pu1SaInitReqPkt,
                       pIkeSA->Ike2SA.u4SaInitReqPktLen);

            IKE_BUDDY_FREE (pIkeSA->Ike2SA.pu1SaInitReqPkt);
            pIkeSA->Ike2SA.pu1SaInitReqPkt = NULL;
        }
        if (pIkeSA->Ike2SA.pu1SaInitRspPkt != NULL)
        {
            IKE_BZERO (pIkeSA->Ike2SA.pu1SaInitRspPkt,
                       pIkeSA->Ike2SA.u4SaInitRspPktLen);
            IKE_BUDDY_FREE (pIkeSA->Ike2SA.pu1SaInitRspPkt);
            pIkeSA->Ike2SA.pu1SaInitRspPkt = NULL;
        }
        if (pIkeSA->Ike2SA.pu1InitNonce != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeSA->Ike2SA.pu1InitNonce);
            pIkeSA->Ike2SA.pu1InitNonce = NULL;
        }
        if (pIkeSA->Ike2SA.pu1RespNonce != NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pIkeSA->Ike2SA.pu1RespNonce);
            pIkeSA->Ike2SA.pu1RespNonce = NULL;
        }
    }

    if (pIkeSA->pu1FinalPhase1IV != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pIkeSA->pu1FinalPhase1IV);
    }

    IKE_BZERO (pIkeSA, sizeof (tIkeSA));
    MemReleaseMemBlock (IKE_SA_MEMPOOL_ID, (UINT1 *) pIkeSA);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeDeleteEngine                                   */
/*  Description     : This function Deletes the Engine specified        */
/*  Input(s)        : pNode - The Engine node                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeleteEngine (t_SLL_NODE * pNode)
{
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine = (tIkeEngine *) pNode;
    SLL_DELETE_LIST (&(pIkeEngine->IkePolicyList), IkeFreePolicyInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeTransformSetList),
                     IkeFreeTransformSetInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeCryptoMapList), IkeFreeCryptoMapInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeKeyList), IkeFreeKeyInfo);
    IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
    SLL_DELETE_LIST (&(pIkeEngine->IkeSessionList), IkeFreeSessionInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeSAList), IkeFreeSAInfo);

    SLL_DELETE_LIST (&(pIkeEngine->TrustedPeerCerts), IkeFreeCertDBEntry);
    SLL_DELETE_LIST (&(pIkeEngine->UntrustedPeerCerts), IkeFreeCertDBEntry);
    SLL_DELETE_LIST (&(pIkeEngine->MyCerts), IkeFreeCertDBEntry);
    SLL_DELETE_LIST (&(pIkeEngine->CACerts), IkeFreeCertDBEntry);
    SLL_DELETE_LIST (&(pIkeEngine->IkeCertMapList), IkeFreeCertMapInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeRsaKeyList), IkeFreeRsaKeyInfo);
    SLL_DELETE_LIST (&(pIkeEngine->IkeDsaKeyList), IkeFreeDsaKeyInfo);
    /* XXX msb added below statement */
    SLL_DELETE_LIST (&(pIkeEngine->IkeRAPolicyList), IkeFreeRemoteAccessInfo);
    /* XXX msb done */

    /* The implementation is changed so that to have only one socket opened
     * for all the engines. That socket descriptor will be present in the dummy
     * engine. So piece of code that does closing of the socket descriptors 
     * have been removed from here. */

    /* Do the rest of the deletions */
    MemReleaseMemBlock (IKE_ENGINE_MEMPOOL_ID, (UINT1 *) pIkeEngine);
}

/************************************************************************/
/*  Function Name   : IkeInitEngine                                     */
/*  Description     : This function Initializes the Engine structure    */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeInitEngine (tIkeEngine * pIkeEngine)
{
    pIkeEngine->u4IkeIndex = gu4EngineIndex++;
    pIkeEngine->i2SocketId = IKE_INVALID;
    pIkeEngine->i2NattSocketFd = IKE_INVALID;
    pIkeEngine->i2V6SocketFd = IKE_INVALID;
    pIkeEngine->i2V6NattSocketFd = IKE_INVALID;
    SLL_INIT (&(pIkeEngine->IkePolicyList));
    SLL_INIT (&(pIkeEngine->IkeTransformSetList));
    SLL_INIT (&(pIkeEngine->IkeCryptoMapList));
    SLL_INIT (&(pIkeEngine->IkeProtectNetList));    /* XXX msb */
    SLL_INIT (&(pIkeEngine->IkeKeyList));
    SLL_INIT (&(pIkeEngine->IkeSessionList));
    SLL_INIT (&(pIkeEngine->IkeSAList));
    SLL_INIT (&(pIkeEngine->IkeRsaKeyList));
    SLL_INIT (&(pIkeEngine->IkeDsaKeyList));
    SLL_INIT (&(pIkeEngine->IkeCertMapList));
    SLL_INIT (&(pIkeEngine->CACerts));
    SLL_INIT (&(pIkeEngine->TrustedPeerCerts));
    SLL_INIT (&(pIkeEngine->UntrustedPeerCerts));
    SLL_INIT (&(pIkeEngine->MyCerts));
    SLL_INIT (&(pIkeEngine->IkeRAPolicyList));    /* XXX msb */
    /* Do the rest of the initialisations */

}

/************************************************************************/
/*  Function Name   : IkeSnmpGetEngine                                  */
/*  Description     : This function is used to get the Engine structure */
/*                  : from the Engine Name                              */
/*  Input(s)        : pEngineName - The Engine Name                     */
/*                  : NameLen - The Name Length                         */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Engine                             */
/************************************************************************/

tIkeEngine         *
IkeSnmpGetEngine (CONST UINT1 *pEngineName, INT4 NameLen)
{
    tIkeEngine         *pEngine = NULL;

    if (pEngineName == NULL)
    {
        return (tIkeEngine *) NULL;
    }

    SLL_SCAN (&gIkeEngineList, pEngine, tIkeEngine *)
    {
        if ((UINT4) NameLen == STRLEN (pEngine->au1EngineName))
        {
            if (MEMCMP
                (pEngineName, pEngine->au1EngineName,
                 STRLEN (pEngine->au1EngineName)) == IKE_ZERO)
            {
                return (pEngine);
            }
        }
    }
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetRAInfo                                  */
/*  Description     : This function is used to get the RA Info          */
/*                  : using the RA-Info  name (index)                   */
/*  Input(s)        : pIkeEngine    - Pointer to the Engine             */
/*                  : pIkeRAInfoName - The RA Info name                 */
/*                  : NameLen - The Name Length                         */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the RA Info                            */
/************************************************************************/

tIkeRemoteAccessInfo *
IkeSnmpGetRAInfo (tIkeEngine * pIkeEngine, UINT1 *pRAInfoName, INT4 NameLen)
{
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    if (pRAInfoName == NULL)
    {
        return (tIkeRemoteAccessInfo *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkeRAPolicyList), pIkeRAInfo,
              tIkeRemoteAccessInfo *)
    {
        if ((UINT4) NameLen == STRLEN (pIkeRAInfo->au1RAInfoName))
        {
            u4TempLen = MEM_MAX_BYTES ((UINT4) NameLen, STRLEN (pRAInfoName));

            if (MEMCMP (pRAInfoName, pIkeRAInfo->au1RAInfoName, u4TempLen)
                == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return (pIkeRAInfo);
            }
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetProtectNet                            */
/*  Description     : This function is used to get the Transform-set    */
/*                  : using the Transform-set name (index)              */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pProtectNetName - The Transform-set 
 *                  Name                  */
/*                  : NameLen - The Transform-set Name Length           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

tIkeRAProtectNet   *
IkeSnmpGetProtectNet (tIkeEngine * pIkeEngine, UINT1 *pProtectNetName,
                      INT4 NameLen)
{
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    if (pProtectNetName == NULL)
    {
        return (tIkeRAProtectNet *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkeProtectNetList), pIkeRAProtectNet,
              tIkeRAProtectNet *)
    {
        if ((UINT4) NameLen == STRLEN (pIkeRAProtectNet->au1ProtectNetName))
        {
            u4TempLen =
                MEM_MAX_BYTES ((UINT4) NameLen, STRLEN (pProtectNetName));
            if (MEMCMP
                (pProtectNetName, pIkeRAProtectNet->au1ProtectNetName,
                 u4TempLen) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return (pIkeRAProtectNet);
            }
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeCheckEngineAttributes                          */
/*  Description     : This function is used to check if the params      */
/*                  : required to make the Engine ACTIVE are set        */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckEngineAttributes (tIkeEngine * pIkeEngine)
{
    /* 
     * Currently, only Tunnel Term Address needs to be set for Engine to
     * become active
     */
    if ((pIkeEngine->Ip4TunnelTermAddr.u4AddrType == IKE_NONE) &&
        (pIkeEngine->Ip6TunnelTermAddr.u4AddrType == IKE_NONE))
    {
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/* Policy related functions */
/************************************************************************/
/*  Function Name   : IkeDeletePolicy                                   */
/*  Description     : This function is used to delete the policy        */
/*                  : using the specified params                        */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pIkePolicy - Pointer to the Policy structure      */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeletePolicy (tIkeEngine * pIkeEngine, tIkePolicy * pIkePolicy)
{
    if ((pIkeEngine == NULL) || (pIkePolicy == NULL))
    {
        return;
    }
    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkePolicyList), (t_SLL_NODE *) pIkePolicy);
    GIVE_IKE_SEM ();
    MemReleaseMemBlock (IKE_POLICY_MEMPOOL_ID, (UINT1 *) pIkePolicy);
}

/************************************************************************/
/*  Function Name   : IkeInitPolicy                                     */
/*  Description     : This function is used to initialize the policy    */
/*  Input(s)        : pIkePolicy - Pointer to the Policy structure      */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeInitPolicy (tIkePolicy * pIkePolicy)
{
    pIkePolicy->u1HashAlgo = IKE_SHA1;
    pIkePolicy->u1EncryptionAlgo = IKE_3DES_CBC;
    pIkePolicy->u1DHGroup = IKE_DH_GROUP_2;
    pIkePolicy->u1LifeTimeType = IKE_DEFAULT_LIFETIME_TYPE;
    pIkePolicy->u4LifeTime = IKE_DEFAULT_LIFETIME;
    pIkePolicy->u2AuthMethod = IKE_PRESHARED_KEY;
    pIkePolicy->u1Mode = IKE_MAIN_MODE;
    pIkePolicy->u2KeyLen = IKE_AES_DEF_KEY_LEN_VAL;
}

/************************************************************************/
/*  Function Name   : IkeGetPhase2LifeTime                              */
/*  Description     : This function is used to get the Phase2 lifetime  */
/*                  : in seconds                                        */
/*  Input(s)        : pIkeCryptoMap - Pointer to the Crypto-map         */
/*  Output(s)       : None                                              */
/*  Returns         : The lifetime in seconds                           */
/************************************************************************/

UINT4
IkeGetPhase2LifeTime (tIkeCryptoMap * pIkeCryptoMap)
{
    switch (pIkeCryptoMap->u1LifeTimeType)
    {
        case (FSIKE_LIFETIME_SECS):
            return (pIkeCryptoMap->u4LifeTime);
        case (FSIKE_LIFETIME_MINS):
            return (pIkeCryptoMap->u4LifeTime * FSIKE_SECS_PER_MIN);
        case (FSIKE_LIFETIME_HRS):
            return (pIkeCryptoMap->u4LifeTime * FSIKE_SECS_PER_HOUR);
        case (FSIKE_LIFETIME_DAYS):
            return (pIkeCryptoMap->u4LifeTime * FSIKE_SECS_PER_DAY);
        default:
            return IKE_SUCCESS;
    }
}

/************************************************************************/
/*  Function Name   : IkeGetPhase1LifeTime                              */
/*  Description     : This function is used to get the Phase1 lifetime  */
/*                  : in seconds                                        */
/*  Input(s)        : pIkePolicy - Pointer to the Policy                */
/*  Output(s)       : None                                              */
/*  Returns         : The lifetime in seconds                           */
/************************************************************************/

UINT4
IkeGetPhase1LifeTime (tIkePolicy * pIkePolicy)
{
    switch (pIkePolicy->u1LifeTimeType)
    {
        case (FSIKE_LIFETIME_SECS):
            return (pIkePolicy->u4LifeTime);
        case (FSIKE_LIFETIME_MINS):
            return (pIkePolicy->u4LifeTime * FSIKE_SECS_PER_MIN);
        case (FSIKE_LIFETIME_HRS):
            return (pIkePolicy->u4LifeTime * FSIKE_SECS_PER_HOUR);
        case (FSIKE_LIFETIME_DAYS):
            return (pIkePolicy->u4LifeTime * FSIKE_SECS_PER_DAY);
        default:
            return IKE_SUCCESS;
    }
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetPolicy                                  */
/*  Description     : This function is used to get the Phase1 Policy    */
/*                  : given the Engine and Policy priority (index)      */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : u4FsIkePolicyPriority - The policy priority       */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Policy                             */
/************************************************************************/

tIkePolicy         *
IkeSnmpGetPolicy (tIkeEngine * pIkeEngine, UINT4 u4FsIkePolicyPriority)
{
    tIkePolicy         *pIkePolicy = NULL;

    if (u4FsIkePolicyPriority == IKE_NONE)
    {
        return (tIkePolicy *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (pIkePolicy->u4Priority == u4FsIkePolicyPriority)
        {
            GIVE_IKE_SEM ();
            return (pIkePolicy);
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetPolicyFromName                          */
/*  Description     : This function is used to get the Phase1 Policy    */
/*                  : given the Engine and Policy Name with the len     */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pIkePolicyName - The policy Name                  */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Policy                             */
/************************************************************************/

tIkePolicy         *
IkeSnmpGetPolicyFromName (tIkeEngine * pIkeEngine, UINT1 *pIkePolicyName,
                          INT4 NameLen)
{
    tIkePolicy         *pIkePolicy = NULL;

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (MEMCMP
            (pIkePolicyName, pIkePolicy->au1PolicyName,
             (size_t) (NameLen)) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return (pIkePolicy);
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeAddToPolicyList                                */
/*  Description     : This function is used to add the Phase1 Policy    */
/*                  : given the Engine and the Policy                   */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pIkePolicy - Pointer to the Policy structure      */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeAddToPolicyList (tIkeEngine * pIkeEngine, tIkePolicy * pIkePolicy)
{
    tIkePolicy         *pPrevPolicy = NULL;
    tIkePolicy         *pNextPolicy = NULL;

    if (SLL_COUNT (&(pIkeEngine->IkePolicyList)) == IKE_ZERO)
    {
        /* First Member, just add the entry into the list */
        TAKE_IKE_SEM ();
        SLL_ADD (&(pIkeEngine->IkePolicyList), (t_SLL_NODE *) pIkePolicy);
        GIVE_IKE_SEM ();
        return;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pNextPolicy, tIkePolicy *)
    {
        if ((pNextPolicy != NULL) && (pNextPolicy->u1Status == IKE_ACTIVE))
        {
            if (pNextPolicy->u4Priority > pIkePolicy->u4Priority)
            {
                SLL_INSERT (&(pIkeEngine->IkePolicyList),
                            (t_SLL_NODE *) pPrevPolicy,
                            (t_SLL_NODE *) pIkePolicy);
                GIVE_IKE_SEM ();
                return;
            }
            pPrevPolicy = pNextPolicy;
        }
    }
    /* Have come out of SCAN without adding, so just add 
     * to the end of the list */
    SLL_ADD (&(pIkeEngine->IkePolicyList), (t_SLL_NODE *) pIkePolicy);
    GIVE_IKE_SEM ();
    return;
}

/* Key-ID table functions */
/************************************************************************/
/*  Function Name   : IkeDeleteKey                                      */
/*  Description     : This function is used to Delete a key entry       */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pIkeKey - Pointer to the key structure            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeleteKey (tIkeEngine * pIkeEngine, tIkeKey * pIkeKey)
{
    if ((pIkeEngine == NULL) || (pIkeKey == NULL))
    {
        return;
    }
    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkeKeyList), (t_SLL_NODE *) pIkeKey);
    GIVE_IKE_SEM ();
    MemReleaseMemBlock (IKE_KEY_MEMPOOL_ID, (UINT1 *) pIkeKey);
    IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetKey                                     */
/*  Description     : This function is used to get a key entry given    */
/*                  : the Engine and the KeyID (index)                  */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pKeyID     - Pointer to the keyID                 */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Key structure                      */
/************************************************************************/

tIkeKey            *
IkeSnmpGetKey (tIkeEngine * pIkeEngine, UINT1 *pKeyID, INT4 KeyLen)
{
    tIkeKey            *pIkeKey = NULL;
    UINT4               u4TempLen = IKE_ZERO;
    if (pKeyID == NULL)
    {
        return (tIkeKey *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
    {
        u4TempLen =
            MEM_MAX_BYTES ((UINT4) KeyLen, STRLEN (pIkeKey->au1DisplayKeyId));
        if (MEMCMP (pKeyID, pIkeKey->au1DisplayKeyId, u4TempLen) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return (pIkeKey);
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeDeleteKeyBasedOnPeerID                         */
/*  Description     : This function is used to delete the preshared key */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pPeerID     - Pointer to the key Peer ID          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeDeleteKeyBasedOnPeerID (tIkeEngine * pIkeEngine, tIkePhase1ID * pPeerID)
{
    tIkeKey            *pIkeKey = NULL;
    tIkeKey            *pTemp = NULL;

    if (pPeerID == NULL)
    {
        return;
    }
    TAKE_IKE_SEM ();
    SLL_DYN_OFFSET_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, pTemp, tIkeKey *)
    {
        if (pIkeKey->KeyID.i4IdType == pPeerID->i4IdType)
        {
            switch (pIkeKey->KeyID.i4IdType)
            {
                case IKE_KEY_IPV4:
                    if (pIkeKey->KeyID.uID.Ip4Addr == pPeerID->uID.Ip4Addr)
                    {
                        SLL_DELETE (&(pIkeEngine->IkeKeyList),
                                    (t_SLL_NODE *) pIkeKey);
                        GIVE_IKE_SEM ();
                        IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
                        return;
                    }
                    break;
                case IKE_KEY_IPV6:
                    if (MEMCMP (&(pIkeKey->KeyID.uID.Ip6Addr),
                                &(pPeerID->uID.Ip6Addr), IKE_IPV6_LENGTH)
                        == IKE_ZERO)
                    {
                        SLL_DELETE (&(pIkeEngine->IkeKeyList),
                                    (t_SLL_NODE *) pIkeKey);
                        GIVE_IKE_SEM ();
                        IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
                        return;
                    }
                    break;
                case IKE_KEY_EMAIL:
                    if (IKE_TRUE == MEMCMP (pIkeKey->KeyID.uID.au1Email,
                                            pPeerID->uID.au1Email,
                                            pPeerID->u4Length))
                    {
                        SLL_DELETE (&(pIkeEngine->IkeKeyList),
                                    (t_SLL_NODE *) pIkeKey);
                        GIVE_IKE_SEM ();
                        IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
                        return;
                    }
                    break;
                case IKE_KEY_FQDN:
                    if (IKE_TRUE == MEMCMP (pIkeKey->KeyID.uID.au1Fqdn,
                                            pPeerID->uID.au1Fqdn,
                                            pPeerID->u4Length))
                    {
                        SLL_DELETE (&(pIkeEngine->IkeKeyList),
                                    (t_SLL_NODE *) pIkeKey);
                        GIVE_IKE_SEM ();
                        IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
                        return;
                    }
                    break;
                case IKE_KEY_DN:
                    if (IKE_TRUE == MEMCMP (pIkeKey->KeyID.uID.au1Dn,
                                            pPeerID->uID.au1Dn,
                                            pPeerID->u4Length))
                    {
                        SLL_DELETE (&(pIkeEngine->IkeKeyList),
                                    (t_SLL_NODE *) pIkeKey);
                        GIVE_IKE_SEM ();
                        IkeUtilCallBack (IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM);
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    GIVE_IKE_SEM ();
}

/************************************************************************/
/*  Function Name   : IkeCheckKeyAttributes                             */
/*  Description     : This function is used to check if all the params  */
/*                  : required to make a key ACTIVE are set             */
/*  Input(s)        : pIkeKey - Pointer to the Key structure            */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Key structure                      */
/************************************************************************/

INT1
IkeCheckKeyAttributes (tIkeKey * pIkeKey)
{
    /* Even if one attribute not set, return IKE_FAILURE immediately */
    if (pIkeKey->bKeyStringSet == IKE_FALSE)
    {
        return (IKE_FAILURE);
    }

    if (pIkeKey->KeyID.i4IdType == IKE_NONE)
    {
        return (IKE_FAILURE);
    }

    /* All attributes are set, return IKE_SUCCESS */
    return (IKE_SUCCESS);
}

/* Functions for Transform-Set Table */
/************************************************************************/
/*  Function Name   : IkeDeleteTransformSet                             */
/*  Description     : This function is used to delete the Transform-set */
/*                  : specified                                         */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pIkeTransformSet - Pointer to the Transform-set   */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeleteTransformSet (tIkeEngine * pIkeEngine,
                       tIkeTransformSet * pIkeTransformSet)
{
    if ((pIkeEngine == NULL) || (pIkeTransformSet == NULL))
    {
        return;
    }
    if (pIkeTransformSet->u4RefCount != IKE_ZERO)
    {
        IKE_ASSERT (pIkeTransformSet->u4RefCount == IKE_ZERO);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteTransformSet:Transform Set Refcount not 0!\n");
        return;
    }

    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkeTransformSetList),
                (t_SLL_NODE *) pIkeTransformSet);
    GIVE_IKE_SEM ();
    MemReleaseMemBlock (IKE_TRANSFORMSET_MEMPOOL_ID,
                        (UINT1 *) pIkeTransformSet);
    return;
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetTransformSet                            */
/*  Description     : This function is used to get the Transform-set    */
/*                  : using the Transform-set name (index)              */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pTSName - The Transform-set Name                  */
/*                  : NameLen - The Transform-set Name Length           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

tIkeTransformSet   *
IkeSnmpGetTransformSet (tIkeEngine * pIkeEngine, UINT1 *pTSName, INT4 NameLen)
{
    tIkeTransformSet   *pIkeTransformSet = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    if (pTSName == NULL)
    {
        return (tIkeTransformSet *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkeTransformSetList), pIkeTransformSet,
              tIkeTransformSet *)
    {
        if ((UINT4) NameLen == STRLEN (pIkeTransformSet->au1TransformSetName))
        {
            u4TempLen = MEM_MAX_BYTES ((UINT4) NameLen, STRLEN (pTSName));

            if (MEMCMP
                (pTSName, pIkeTransformSet->au1TransformSetName,
                 u4TempLen) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return (pIkeTransformSet);
            }
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeCheckTransformSetAttributes                    */
/*  Description     : This function is used to check if all the params  */
/*                  : required for making the entry ACTIVE are set      */
/*  Input(s)        : pIkeTransformSet - Pointer to the Transform-set   */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckTransformSetAttributes (tIkeTransformSet * pIkeTransformSet)
{

    /* When both ESP Encr and ESP Hash are reqd., will have to
       add u1EspHashAlgo too
     */
    if ((pIkeTransformSet->u1EspEncryptionAlgo != IKE_NONE)
        || (pIkeTransformSet->u1AhHashAlgo != IKE_NONE))
    {
        return (IKE_SUCCESS);
    }
    else
    {
        return (IKE_FAILURE);
    }
}

/* Routines for Crypto Map Table */
/************************************************************************/
/*  Function Name   : IkeInitCryptoMap                                  */
/*  Description     : This function is used to Initialize the           */
/*                  : Crypto-map                                        */
/*  Input(s)        : pIkeCryptoMap - Pointer to the Crypto-map         */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeInitCryptoMap (tIkeCryptoMap * pIkeCryptoMap)
{
    pIkeCryptoMap->u1Mode = IKE_IPSEC_TUNNEL_MODE;
    pIkeCryptoMap->u1LifeTimeType = IKE_DEFAULT_LIFETIME_TYPE;
    pIkeCryptoMap->u4LifeTime = IKE_DEFAULT_LIFETIME;
    SLL_INIT_NODE (&pIkeCryptoMap->Next);
    return;
}

/************************************************************************/
/*  Function Name   : IkeDeleteCryptoMap                                */
/*  Description     : This function is used to delete the Crypto-map    */
/*                  : specified                                         */
/*  Input(s)        : pIkeEngine    - Pointer to the Engine             */
/*                  : pIkeCryptoMap - Pointer to the Crypto-map         */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeDeleteCryptoMap (tIkeEngine * pIkeEngine, tIkeCryptoMap * pIkeCryptoMap)
{
    INT4                i4Count = IKE_ZERO;

    /* Reduce the Reference counts of the Transform-sets associated 
       with this Crypto Map */
    for (i4Count = IKE_ZERO; i4Count < MAX_TRANSFORMS; i4Count++)
    {
        if (pIkeCryptoMap->aTransformSetBundle[i4Count] != NULL)
        {
            pIkeCryptoMap->aTransformSetBundle[i4Count]->u4RefCount--;
            pIkeCryptoMap->aTransformSetBundle[i4Count] = NULL;
        }
    }
    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkeCryptoMapList), (t_SLL_NODE *) pIkeCryptoMap);
    GIVE_IKE_SEM ();
    /* MemReleaseMemBlock (IKE_CRYPTOMAP_MEMPOOL_ID, (UINT1*)pIkeCryptoMap); */
}

/************************************************************************/
/*  Function Name   : IkeSnmpGetCryptoMap                               */
/*  Description     : This function is used to get the Crypto-map       */
/*                  : using the Crypto-map name (index)                 */
/*  Input(s)        : pIkeEngine    - Pointer to the Engine             */
/*                  : pCryptoMapName - The Crypto-map name              */
/*                  : NameLen - The Name Length                         */
/*  Output(s)       : None                                              */
/*  Returns         : Pointer to the Crypto-map                         */
/************************************************************************/

tIkeCryptoMap      *
IkeSnmpGetCryptoMap (tIkeEngine * pIkeEngine, UINT1 *pCryptoMapName,
                     INT4 NameLen)
{
    tIkeCryptoMap      *pIkeCryptoMap = NULL;
    UINT4               u4TempLen = IKE_ZERO;

    if (pCryptoMapName == NULL)
    {
        return (tIkeCryptoMap *) NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkeCryptoMapList), pIkeCryptoMap, tIkeCryptoMap *)
    {
        if ((UINT4) NameLen == STRLEN (pIkeCryptoMap->au1CryptoMapName))
        {
            u4TempLen =
                MEM_MAX_BYTES ((UINT4) NameLen, STRLEN (pCryptoMapName));
            if (MEMCMP
                (pCryptoMapName, pIkeCryptoMap->au1CryptoMapName,
                 u4TempLen) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return (pIkeCryptoMap);
            }
        }
    }
    GIVE_IKE_SEM ();
    return (NULL);
}

/************************************************************************/
/*  Function Name   : IkeCheckCryptoMapAttributes                       */
/*  Description     : This function is used to check if all the params  */
/*                  : required to make the entry active are set         */
/*  Input(s)        : pIkeCryptoMap - Pointer to the Crypto-map         */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckCryptoMapAttributes (tIkeCryptoMap * pIkeCryptoMap)
{
    /* Even if one of the required attributes (other than default) are 
       not set, return IKE_FAILURE */
    if (pIkeCryptoMap->LocalNetwork.u4Type == IKE_NONE)
    {
        return (IKE_FAILURE);
    }
    if (pIkeCryptoMap->RemoteNetwork.u4Type == IKE_NONE)
    {
        return (IKE_FAILURE);
    }
    if (pIkeCryptoMap->PeerAddr.u4AddrType == IKE_NONE)
    {
        return (IKE_FAILURE);
    }
    /* At least one Transform-set should be set */
    if (pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_0] == NULL)
    {
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeCheckRAInfoAttributes                          */
/*  Description     : This function is used to check if all the params  */
/*                  : required to make the entry active are set         */
/*  Input(s)        : pIkeRAInfo - Pointer to the Remote Access Info    */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckRAInfoAttributes (tIkeRemoteAccessInfo * pIkeRAInfo)
{
    /* Even if one of the required attributes (other than default) are 
       not set, return IKE_FAILURE */

    if (pIkeRAInfo->Phase1ID.i4IdType == IKE_NONE)
    {
        return (IKE_FAILURE);
    }

    if ((gbXAuthServer == TRUE) && (pIkeRAInfo->bCMEnabled == TRUE))
    {
        /* At least one Protected should be set */
        if (pIkeRAInfo->aProtectNetBundle[IKE_INDEX_0] == NULL)
        {
            return (IKE_FAILURE);
        }
    }
    if (pIkeRAInfo->aTransformSetBundle[IKE_INDEX_0] == NULL)
    {
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeCheckProtectNetAttributes                          */
/*  Description     : This function is used to check if all the params  */
/*                  : required to make the entry active are set         */
/*  Input(s)        : pIkeRAInfo - Pointer to the Remote Access Info    */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCheckProtectNetAttributes (tIkeRAProtectNet * pIkeRAInfo)
{
    /* Even if one of the required attributes (other than default) are 
       not set, return IKE_FAILURE */
    if (pIkeRAInfo->ProtectedNetwork.u4Type == IKE_NONE)
    {
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}

UINT4               gaIp4PrefixToMask[] = {
    IKE_IPV4_PREFIX_TO_MASK_00000000,
    IKE_IPV4_PREFIX_TO_MASK_80000000,
    IKE_IPV4_PREFIX_TO_MASK_c0000000,
    IKE_IPV4_PREFIX_TO_MASK_e0000000,
    IKE_IPV4_PREFIX_TO_MASK_f0000000,
    IKE_IPV4_PREFIX_TO_MASK_f8000000,
    IKE_IPV4_PREFIX_TO_MASK_fc000000,
    IKE_IPV4_PREFIX_TO_MASK_fe000000,
    IKE_IPV4_PREFIX_TO_MASK_ff000000,
    IKE_IPV4_PREFIX_TO_MASK_ff800000,
    IKE_IPV4_PREFIX_TO_MASK_ffc00000,
    IKE_IPV4_PREFIX_TO_MASK_ffe00000,
    IKE_IPV4_PREFIX_TO_MASK_fff00000,
    IKE_IPV4_PREFIX_TO_MASK_fff80000,
    IKE_IPV4_PREFIX_TO_MASK_fffc0000,
    IKE_IPV4_PREFIX_TO_MASK_fffe0000,
    IKE_IPV4_PREFIX_TO_MASK_ffff0000,
    IKE_IPV4_PREFIX_TO_MASK_ffff8000,
    IKE_IPV4_PREFIX_TO_MASK_ffffc000,
    IKE_IPV4_PREFIX_TO_MASK_ffffe000,
    IKE_IPV4_PREFIX_TO_MASK_fffff000,
    IKE_IPV4_PREFIX_TO_MASK_fffff800,
    IKE_IPV4_PREFIX_TO_MASK_fffffc00,
    IKE_IPV4_PREFIX_TO_MASK_fffffe00,
    IKE_IPV4_PREFIX_TO_MASK_ffffff00,
    IKE_IPV4_PREFIX_TO_MASK_ffffff80,
    IKE_IPV4_PREFIX_TO_MASK_ffffffc0,
    IKE_IPV4_PREFIX_TO_MASK_ffffffe0,
    IKE_IPV4_PREFIX_TO_MASK_fffffff0,
    IKE_IPV4_PREFIX_TO_MASK_fffffff8,
    IKE_IPV4_PREFIX_TO_MASK_fffffffc,
    IKE_IPV4_PREFIX_TO_MASK_fffffffe,
    IKE_IPV4_PREFIX_TO_MASK_ffffffff
};

UINT1               gaIp6PrefixToMask[][IKE_IP6_ADDR_LEN] = {
    {
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_00, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_80, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_c0, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_e0, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f0, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_f8, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fc, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_fe, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_00},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_80},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_c0},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_e0},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f0},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_f8},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fc},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_fe},
    {
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff,
     IKE_IPV6_PREFIX_TO_MASK_ff, IKE_IPV6_PREFIX_TO_MASK_ff}

};

/************************************************************************/
/*  Function Name   : IkeCryptoSetNetworkAddress                        */
/*  Description     : This function is used to set the configured       */
/*                  : address in the tIkeNetwork format                 */
/*  Input(s)        : Addr          - The Address string                */
/*                  : AddrLen       - The Address string Length         */
/*  Output(s)       : pIkeNetwork   - Pointer to IkeNetwork             */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT4
IkeCryptoSetNetworkAddress (tIkeNetwork * pIkeNetwork, UINT1 *pu1Addr,
                            INT4 i4AddrLen)
{
    UINT1              *pu1LocalCopyAddr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    tIp4Addr            u4Ip4Addr = IKE_ZERO;
    tIp6Addr            Ip6Addr;
    UINT4               u4Prefix = IKE_ZERO;

    if (i4AddrLen > IKE_MAX_ADDR_STR_LEN)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCryptoSetNetworkAddress:Invalid address length\n");
        return (IKE_FAILURE);
    }

    if (i4AddrLen == IKE_MAX_ADDR_STR_LEN)
    {
        i4AddrLen = IKE_MAX_ADDR_STR_LEN - IKE_ONE;
    }
    IKE_MEMCPY (au1LocalAddr, pu1Addr, i4AddrLen);
    au1LocalAddr[i4AddrLen] = '\0';
    pu1LocalCopyAddr = au1LocalAddr;

    /* Test for Subnet */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_SUBNET_SEPARATOR);

    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';

        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_SUBNET;
            pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr = IKE_NTOHL (u4Ip4Addr);
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV4_PREFIX)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCryptoSetNetworkAddress:Invalid IPV4 address"
                             "prefix \n ");
                return (IKE_FAILURE);
            }

            pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask =
                gaIp4PrefixToMask[u4Prefix];
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_SUBNET;
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Subnet.Ip6Addr,
                        &Ip6Addr, sizeof (Ip6Addr));
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV6_PREFIX)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCryptoSetNetworkAddress:Invalid IPV6 address"
                             "prefix \n ");
                return (IKE_FAILURE);
            }

            IKE_MEMCPY (pIkeNetwork->uNetwork.
                        Ip6Subnet.Ip6SubnetMask.
                        u1_addr, gaIp6PrefixToMask[u4Prefix],
                        sizeof (tIp6Addr));
        }
        else
        {
            return IKE_FAILURE;    /* Not a valid string */
        }

        return IKE_SUCCESS;
    }

    /* Test for Range */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_RANGE_SEPARATOR);
    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';
        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_RANGE;
            pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr = IKE_NTOHL (u4Ip4Addr);

            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
            {
                pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr
                    = IKE_NTOHL (u4Ip4Addr);

            }
            else
            {
                return IKE_FAILURE;    /* Invalid string */
            }
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_RANGE;
            IKE_MEMCPY (&pIkeNetwork->uNetwork.
                        Ip6Range.Ip6StartAddr, &Ip6Addr, sizeof (Ip6Addr));
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
            {
                IKE_MEMCPY (&pIkeNetwork->uNetwork.
                            Ip6Range.Ip6EndAddr, &Ip6Addr, sizeof (Ip6Addr));
            }
            else
            {
                return IKE_FAILURE;
            }
        }

        return IKE_SUCCESS;
    }

    /* Test for Host address */

    if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
    {
        pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_ADDR;
        pIkeNetwork->uNetwork.Ip4Addr = IKE_NTOHL (u4Ip4Addr);
    }
    else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
    {
        pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_ADDR;
        IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Addr, &Ip6Addr, sizeof (Ip6Addr));
    }
    else
    {
        return IKE_FAILURE;        /* Invalid String */
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeCryptoSetTransformSetBundle                    */
/*  Description     : This function is used to set the configured       */
/*                  : Transform-sets into the Crypto-map                */
/*  Input(s)        : pIkeEngine    - Pointer to the IkeEngine          */
/*                  : pIkeCryptoMap - Pointer to the IkeCryptoMap       */
/*                  : pu1TSBundle   - Pointer to the Transform-set's    */
/*                  : i4BundleLen   - The Length of the Bundle          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeCryptoSetTransformSetBundle (tIkeEngine * pIkeEngine,
                                tIkeCryptoMap * pIkeCryptoMap,
                                UINT1 *pu1TSBundle, INT4 i4BundleLen)
{
    UINT1               au1TS1[MAX_NAME_LENGTH + IKE_ONE] = { IKE_ZERO };
    UINT1              *pu1TS2 = NULL;
    UINT1              *pu1TS3 = NULL;
    UINT1              *pu1TS4 = NULL;
    UINT1              *pu1CopyStr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               u1CopyNum = IKE_ONE;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4Count1 = IKE_ZERO;

    pu1Temp = pu1TSBundle;
    pu1CopyStr = au1TS1;

    for (u4Count = IKE_ZERO; u4Count < (UINT4) i4BundleLen; u4Count++)
    {
        if (pu1Temp[u4Count] == '.')
        {
            pu1CopyStr[u4Count1] = '\0';
            u4Count1 = IKE_ZERO;
            u1CopyNum++;
            switch (u1CopyNum)
            {
                case IKE_TWO:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS2) == MEM_FAILURE)
                    {
                        if (pu1TS3 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        }
                        if (pu1TS4 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        }
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }

                    if (pu1TS2 == NULL)
                    {
                        if (pu1TS3 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        }
                        if (pu1TS4 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        }
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                                     "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS2, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS2;
                    break;
                }
                case IKE_THREE:
                {

                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS3) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        }
                        if (pu1TS4 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        }
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS3 == NULL)
                    {
                        if (pu1TS2 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        }
                        if (pu1TS4 != NULL)
                        {
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        }
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                                     "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS3, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS3;
                    break;
                }
                case IKE_FOUR:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS4) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS4 == NULL)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS4, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS4;
                    break;
                }
                default:
                    break;
            }
            continue;
        }
        else
        {
            pu1CopyStr[u4Count1] = pu1Temp[u4Count];
            u4Count1++;
        }
    }
    pu1CopyStr[u4Count1] = '\0';

    pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_0] =
        IkeSnmpGetTransformSet (pIkeEngine, au1TS1, (INT4) STRLEN (au1TS1));

    if (pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_0] == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        return (IKE_FAILURE);
    }
    else
    {
        pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_0]->u4RefCount++;
    }

    if (pu1TS2 != NULL)
    {
        pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_1] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS2, (INT4) STRLEN (pu1TS2));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        if (pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_1] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        else
        {
            pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_1]->u4RefCount++;
        }
    }

    if (pu1TS3 != NULL)
    {
        pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_2] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS3, (INT4) STRLEN (pu1TS3));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        if (pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_2] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        else
        {
            pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_2]->u4RefCount++;
        }
    }

    if (pu1TS4 != NULL)
    {
        pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_3] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS4, (INT4) STRLEN (pu1TS4));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        if (pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_3] == NULL)
        {
            return (IKE_FAILURE);
        }
        else
        {
            pIkeCryptoMap->aTransformSetBundle[IKE_INDEX_3]->u4RefCount++;
        }
    }
    return (IKE_SUCCESS);
}

/* General Utility functions */
/************************************************************************/
/*  Function Name   : IkeParseAddrString                                */
/*  Description     : This function is used to parset an address string */
/*                  : and return V4 or V6 address                       */
/*  Input(s)        : pAddrString   - Pointer to the Address string     */
/*  Output(s)       : None                                              */
/*  Returns         : IPV4ADDR or IPV6ADDR                              */
/************************************************************************/

UINT1
IkeParseAddrString (UINT1 *pAddrString)
{
    INT4                i4RetVal = IKE_ZERO;
    tIp4Addr            u4Ip4Addr = IKE_ZERO;
    tIp6Addr            Ip6Addr;
    i4RetVal = INET_PTON4 (pAddrString, &u4Ip4Addr);
    if (i4RetVal > IKE_ZERO)
    {
        return (IPV4ADDR);
    }
    else
    {
        i4RetVal = INET_PTON6 (pAddrString, &Ip6Addr);
        if (i4RetVal > IKE_ZERO)
        {
            return (IPV6ADDR);
        }
        else
        {
            /* Neither V4 nor V6 Address */
            return (IKE_NONE);
        }
    }
}

/************************************************************************/
/*  Function Name   : IkeParseNetworkAddr                               */
/*  Description     : This function is used to parse the network address*/
/*                  : string and return the address type v4 or v6.      */
/*  Input(s)        : pu1AddrString - Pointer to the Address string     */
/*  Output(s)       : None                                              */
/*  Returns         : IPV4ADDR or IPV6ADDR or IKE_NONE                  */
/************************************************************************/

UINT1
IkeParseNetworkAddr (UINT1 *pu1AddrString, INT4 i4Len)
{
    tIp4Addr            u4Ip4Addr = IKE_ZERO;
    tIp6Addr            Ip6Addr;
    UINT4               u4Prefix = IKE_ZERO;
    UINT1               au1LocalAddrString[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1              *pu1LocalCopyAddr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4TempLen = IKE_ZERO;
    if (i4Len > IKE_MAX_ADDR_STR_LEN)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE", "IkeParseNetworkAddr:Invalid address string\n"
                     "length\n");
        return IKE_NONE;
    }

    if (i4Len == IKE_MAX_ADDR_STR_LEN)
    {
        i4Len = IKE_MAX_ADDR_STR_LEN - IKE_ONE;
    }
    u4TempLen = MEM_MAX_BYTES ((UINT4) i4Len, STRLEN (pu1AddrString));
    IKE_MEMCPY (au1LocalAddrString, pu1AddrString, u4TempLen);
    au1LocalAddrString[u4TempLen] = '\0';
    pu1LocalCopyAddr = au1LocalAddrString;

    /* Test for Subnet */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_SUBNET_SEPARATOR);

    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';

        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV4_PREFIX)
            {
                return (IKE_NONE);
            }
            return IPV4ADDR;
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV6_PREFIX)
            {
                return (IKE_NONE);
            }
            return IPV6ADDR;

        }
        else
        {
            return IKE_NONE;    /* Not a valid string */
        }

    }

    /* Test for Range */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_RANGE_SEPARATOR);
    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';
        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
            {
                return IPV4ADDR;    /* Invalid string */

            }
            else
            {
                return IKE_NONE;    /* Invalid string */
            }
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
            {
                return IPV6ADDR;
            }
            else
            {
                return IKE_NONE;
            }
        }
        return IKE_NONE;
    }

    /* Test for Host address */

    if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
    {
        return IPV4ADDR;
    }
    else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
    {
        return IPV6ADDR;
    }
    else
    {
        return IKE_NONE;        /* Invalid String */
    };
}

/************************************************************************/
/*  Function Name   : IkeGetIpAddrFromString                            */
/*  Description     : This function is used to parse a string and       */
/*                  : fill the IkeIpAddr structure                      */
/*  Input(s)        : pPassAddr - The Address string                    */
/*  Output(s)       : pIpAddr   - The output address                    */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeGetIpAddrFromString (UINT1 *pPassAddr, tIkeIpAddr * pIpAddr)
{
    UINT1               u1AddrType = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;
    tIp6Addr            TempIp6Addr;
    u1AddrType = IkeParseAddrString (pPassAddr);
    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return;
    }

    if (IPV4ADDR == u1AddrType)
    {
        /* INET_PTON4() cannot fail as IkeParseAddrString() above uses this */
        INET_PTON4 (pPassAddr, &u4TempIp4Addr);
        pIpAddr->Ipv4Addr = OSIX_NTOHL (u4TempIp4Addr);
        pIpAddr->u4AddrType = IPV4ADDR;
    }

    if (IPV6ADDR == u1AddrType)
    {
        /* INET_PTON6() cannot fail as IkeParseAddrString() above uses this */
        INET_PTON6 (pPassAddr, &TempIp6Addr.u1_addr);
        MEMCPY (pIpAddr->Ipv6Addr.u1_addr,
                TempIp6Addr.u1_addr, IKE_IP6_ADDR_LEN);
        pIpAddr->u4AddrType = IPV6ADDR;
    }

}

/************************************************************************/
/*  Function Name   : IkeSendConfMsgToIke                               */
/*  Description     : This function is used to Send a configuration     */
/*                  : message from SNMP/CLI to IKE                      */
/*  Input(s)        : pIkeConfigIfParam - The params required for the   */
/*                  : varoius configurations                            */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSendConfMsgToIke (tIkeConfigIfParam * pIkeConfigIfParam)
{

    tIkeQMsg           *pIkeQueue = NULL;
    UINT4               u4Event = IKE_ZERO;

    if (MemAllocateMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 **) (VOID *) &pIkeQueue)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE", "IkeSendConfMsgToIke:unable to allocate memory\n");
        return IKE_FAILURE;
    }

    if (pIkeQueue == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE", "IkeSendConfMsgToIke:unable to allocate memory\n");
        return IKE_FAILURE;
    }

    pIkeQueue->u4MsgType = CONFIG_IF_MSG;
    pIkeQueue->u4TaskId = OsixGetCurTaskId ();
    IKE_MEMCPY (&pIkeQueue->IkeIfParam.
                IkeConfigParam, pIkeConfigIfParam, sizeof (tIkeConfigIfParam));

    if (OsixSendToQ
        ((UINT4) ZERO, IKE_QUEUE_NAME,
         (tOsixMsg *) pIkeQueue, OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE", "IkeSendConfMsgToIke:unable to send it to IKE Q\n");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQueue);
        return IKE_FAILURE;
    }

    if (OsixSendEvent (SELF, IKE_TASK_NAME, IKE_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeSendConfMsgToIke:unable to send Event to IKE\n");
        return IKE_FAILURE;
    }
    OsixReceiveEvent (IKE_CFG_WAIT_EVENT, OSIX_WAIT, IKE_ZERO, &u4Event);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeSendSecv4SADeleteMsgToIke                      */
/*  Description     : This function is used to Send a configuration     */
/*                  : message from SNMP/CLI to IKE                      */
/*  Input(s)        : pIkeConfigIfParam - The params required for the   */
/*                  : varoius configurations                            */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSendSecv4SADeleteMsgToIke (tIkeVpnPolicy * pIkeVpnPolicy)
{

    tIkeIPSecIfParam    IkeIPSecIfParam;
    tIkeEngine         *pIkeEngine = NULL;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    INT4                i4EngineNameLen = IKE_ZERO;

    MEMSET (au1EngineName, IKE_ZERO, MAX_NAME_LENGTH);
    IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));

    if (STRSTR (pIkeVpnPolicy->au1VpnPolicyName, IKE_WLAN_ENGINE) != NULL)
    {
        IKE_STRCPY (au1EngineName, IKE_WLAN_ENGINE);
        i4EngineNameLen = (INT4) STRLEN (au1EngineName);
    }
    else if (STRSTR (pIkeVpnPolicy->au1VpnPolicyName, IKE_BACKUP_ENGINE) !=
             NULL)
    {
        IKE_STRCPY (au1EngineName, IKE_BACKUP_ENGINE);
        i4EngineNameLen = (INT4) STRLEN (au1EngineName);
    }
    else
    {
        IKE_STRCPY (au1EngineName, IKE_DEFAULT_ENGINE);
        i4EngineNameLen = (INT4) STRLEN (au1EngineName);
    }

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (au1EngineName, i4EngineNameLen);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSendSecv4SADeleteMsgToIke:"
                     "Unable to get Engine Name \n");
        GIVE_IKE_SEM ();
        return IKE_FAILURE;

    }
    GIVE_IKE_SEM ();

    IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
    if (pIkeVpnPolicy->RemTunnTermAddr.u4AddrType == IPV4ADDR)
    {
        IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr.u4AddrType =
            IKE_IPSEC_ID_IPV4_ADDR;
        IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr.u4AddrType =
            IKE_IPSEC_ID_IPV4_ADDR;
        IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr.Ipv4Addr =
            pIkeEngine->Ip4TunnelTermAddr.uIpAddr.Ip4Addr;
        IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr.Ipv4Addr =
            pIkeVpnPolicy->RemTunnTermAddr.uIpAddr.Ip4Addr;
    }
    else
    {
        IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr.u4AddrType =
            IKE_IPSEC_ID_IPV6_ADDR;
        IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr.u4AddrType =
            IKE_IPSEC_ID_IPV6_ADDR;
        IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSAInfo.LocalAddr.Ipv6Addr,
                    &pIkeEngine->Ip6TunnelTermAddr.uIpAddr.Ip6Addr,
                    sizeof (tIp6Addr));
        IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr.Ipv6Addr,
                    &pIkeVpnPolicy->RemTunnTermAddr.uIpAddr.Ip6Addr,
                    sizeof (tIp6Addr));
    }
    if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSendSecv4SADeleteMsgToIke:"
                     "Unable to send to IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR "
                     "request to IPSec\n");
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendPolicyInfoToIke                        */
/*  Description     : This function is used to send Policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pIkePolicy - pointer to changed policy            */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendPolicyInfoToIke (UINT4 u4IkeEngineIndex, tIkePolicy * pIkePolicy,
                            UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;

    if ((pIkePolicy->PolicyPeerID.uID.Ip4Addr == IKE_DEFAULT_IP) &&
        (pIkePolicy->PolicyPeerID.i4IdType == IKE_KEY_IPV4))
    {
        /* This is not right. This will delete all policy
         * including the ones that have exclusive policy. Since
         * it is not easy to identify those cases, deleteing all */
        if (u4Msg == IKE_CONFIG_CHG_POLICY)
        {
            u4Msg = IKE_CONFIG_P1_ALL_POLICY_CHG;
        }
        else if (u4Msg == IKE_CONFIG_DEL_POLICY)
        {
            u4Msg = IKE_CONFIG_P1_DEL_ALL_POLICY;
        }
        else
        {
            return (IKE_FAILURE);
        }
    }
    else
    {
        if (u4Msg == IKE_CONFIG_CHG_POLICY)
        {
            u4Msg = IKE_CONFIG_P1_POLICY_CHG;
        }
        else if (u4Msg == IKE_CONFIG_DEL_POLICY)
        {
            u4Msg = IKE_CONFIG_P1_DEL_POLICY;
        }
        else
        {
            return (IKE_FAILURE);
        }
    }

    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeConfPolicy.u4IkeEngineIndex =
        u4IkeEngineIndex;
    IKE_MEMCPY (&(IkeConfigIfParam.ConfigReq.IkeConfPolicy.PeerId),
                &(pIkePolicy->PolicyPeerID), sizeof (tIkePhase1ID));
    IkeSendConfMsgToIke (&IkeConfigIfParam);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendDelTransformInfoToIke                  */
/*  Description     : This function is used to send Transform-set       */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendDelTransformInfoToIke (tIkeConfCryptoMap IkeConfCryptoMap)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IkeConfigIfParam.u4MsgType = IKE_CONFIG_DEL_TRANSFORM_SET_SPECIFIC;
    MEMCPY (&IkeConfigIfParam.ConfigReq.IkeConfCryptoMap, &IkeConfCryptoMap,
            sizeof (tIkeConfCryptoMap));
    IkeSendConfMsgToIke (&IkeConfigIfParam);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendTransformInfoToIke                     */
/*  Description     : This function is used to send Transform-set       */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendTransformInfoToIke (UINT4 u4IkeEngineIndex, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.u4IkeEngineIndex = u4IkeEngineIndex;
    IkeSendConfMsgToIke (&IkeConfigIfParam);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendCryptoMapInfoToIke                     */
/*  Description     : This function is used to send Crypto-map info     */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pIkeCryptoMap     - Pointer to Crypto-map         */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendCryptoMapInfoToIke (UINT4
                               u4IkeEngineIndex,
                               tIkeCryptoMap * pIkeCryptoMap, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.
        IkeConfCryptoMap.u4IkeEngineIndex = u4IkeEngineIndex;
    IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.
                IkeConfCryptoMap.PeerIpAddr,
                &pIkeCryptoMap->PeerAddr, sizeof (tIkeIpAddr));
    IkeSendConfMsgToIke (&IkeConfigIfParam);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeFreeRsaKeyInfo                                 */
/*  Description     : This function Frees the Rsa Key Info structure    */
/*  Input(s)        : pNode - The Key node                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeFreeRsaKeyInfo (t_SLL_NODE * pNode)
{
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;

    pIkeRsaKeyInfo = (tIkeRsaKeyInfo *) pNode;

    if (pIkeRsaKeyInfo->CertInfo.pCAName != NULL)
    {
        IkeX509NameFree (pIkeRsaKeyInfo->CertInfo.pCAName);
        pIkeRsaKeyInfo->CertInfo.pCAName = NULL;
    }

    if (pIkeRsaKeyInfo->CertInfo.pCASerialNum != NULL)
    {
        IkeASN1IntegerFree (pIkeRsaKeyInfo->CertInfo.pCASerialNum);
        pIkeRsaKeyInfo->CertInfo.pCASerialNum = NULL;
    }

    if (pIkeRsaKeyInfo->RsaKey.pkey.rsa != NULL)
    {
        RSA_free (pIkeRsaKeyInfo->RsaKey.pkey.rsa);
        pIkeRsaKeyInfo->RsaKey.pkey.rsa = NULL;
    }

    IKE_BZERO (pIkeRsaKeyInfo, sizeof (tIkeRsaKeyInfo));

    MemReleaseMemBlock (IKE_RSAKEY_MEMPOOL_ID, (UINT1 *) pIkeRsaKeyInfo);

    return;
}

/************************************************************************/
/*  Function Name   : IkeFreeDsaKeyInfo                                 */
/*  Description     : This function Frees the Dsa Key Info structure    */
/*  Input(s)        : pNode - The Key node                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeFreeDsaKeyInfo (t_SLL_NODE * pNode)
{
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;

    pIkeDsaKeyInfo = (tIkeDsaKeyInfo *) pNode;

    if (pIkeDsaKeyInfo->CertInfo.pCAName != NULL)
    {
        IkeX509NameFree (pIkeDsaKeyInfo->CertInfo.pCAName);
        pIkeDsaKeyInfo->CertInfo.pCAName = NULL;
    }

    if (pIkeDsaKeyInfo->CertInfo.pCASerialNum != NULL)
    {
        IkeASN1IntegerFree (pIkeDsaKeyInfo->CertInfo.pCASerialNum);
        pIkeDsaKeyInfo->CertInfo.pCASerialNum = NULL;
    }

    if (pIkeDsaKeyInfo->DsaKey.pkey.dsa != NULL)
    {
        DSA_free (pIkeDsaKeyInfo->DsaKey.pkey.dsa);
        pIkeDsaKeyInfo->DsaKey.pkey.dsa = NULL;
    }

    IKE_BZERO (pIkeDsaKeyInfo, sizeof (tIkeDsaKeyInfo));

    MemReleaseMemBlock (gu4IkeDsaMemPoolId, (UINT1 *) pIkeDsaKeyInfo);

    return;
}

/************************************************************************/
/*  Function Name   :IkeFreeCertMapInfo                                 */
/*  Description     :This function is used to free the Cert map entry   */
/*  Input(s)        :pNode - The Cert Map Node                          */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
IkeFreeCertMapInfo (t_SLL_NODE * pNode)
{
    tIkeCertMapToPeer  *pIkeCertMap = NULL;

    pIkeCertMap = (tIkeCertMapToPeer *) pNode;

    if (pIkeCertMap->MyCert.pCAName != NULL)
    {
        IkeX509NameFree (pIkeCertMap->MyCert.pCAName);
        pIkeCertMap->MyCert.pCAName = NULL;
    }

    if (pIkeCertMap->MyCert.pCASerialNum != NULL)
    {
        IkeASN1IntegerFree (pIkeCertMap->MyCert.pCASerialNum);
        pIkeCertMap->MyCert.pCASerialNum = NULL;
    }

    MemReleaseMemBlock (IKE_CERTMAP_MEMPOOL_ID, (UINT1 *) pIkeCertMap);
}

/************************************************************************/
/*  Function Name   : IkeNmSendCertInfoToIke                            */
/*  Description     : This function is used to send policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pu1CertId - Certificate identification            */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeNmSendCertInfoToIke (UINT4 u4IkeEngineIndex, UINT1 *pu1CertId, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeConfCertInfo.u4IkeEngineIndex =
        u4IkeEngineIndex;

    if (STRLEN (pu1CertId) > MAX_NAME_LENGTH)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeNmSendCertInfoToIke: Improper Cert ID length\n");
        return IKE_FAILURE;
    }
    IKE_STRNCPY (IkeConfigIfParam.ConfigReq.IkeConfCertInfo.au1CertId,
                 pu1CertId, STRLEN (pu1CertId));

    if (IkeSendConfMsgToIke (&IkeConfigIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE", "IkeNmSendCertInfoToIke: Failed to send event\n");
        return IKE_FAILURE;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeNmSendMapCertInfoToIke                         */
/*  Description     : This function is used to send policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pu1CertId - Certificate identification            */
/*                  : pPhase1Id - Phase1 identification                 */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeNmSendMapCertInfoToIke (UINT4 u4IkeEngineIndex, UINT1 *pu1CertId,
                           tIkePhase1ID * pPhase1Id, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;

    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeMapCertInfo.u4IkeEngineIndex =
        u4IkeEngineIndex;

    if ((pu1CertId != NULL) && (STRLEN (pu1CertId) < MAX_NAME_LENGTH))
    {
        IKE_STRNCPY (IkeConfigIfParam.ConfigReq.IkeMapCertInfo.
                     au1CertId, pu1CertId, STRLEN (pu1CertId));
    }

    IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeMapCertInfo.Phase1ID, pPhase1Id,
                sizeof (tIkePhase1ID));

    if (IkeSendConfMsgToIke (&IkeConfigIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeNmSendMapCertInfoToIke: Failed to send event\n");
        return IKE_FAILURE;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeNmSendCertDeleteReqToIke                       */
/*  Description     : This function is used to send policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pu1CertId - Certificate identification            */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeNmSendCertDeleteReqToIke (UINT4 u4IkeEngineIndex,
                             tIkeIpAddr * pPeerIpAddr, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;

    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeDelCertInfo.u4IkeEngineIndex =
        u4IkeEngineIndex;

    IKE_BZERO (&IkeConfigIfParam.ConfigReq.IkeDelCertInfo.PeerIpAddr,
               sizeof (tIkeIpAddr));

    if (pPeerIpAddr != NULL)
    {
        IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeDelCertInfo.PeerIpAddr,
                    pPeerIpAddr, sizeof (tIkeIpAddr));
    }

    if (IkeSendConfMsgToIke (&IkeConfigIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeNmSendCertDeleteReqToIke: Failed to send event\n");
        return IKE_FAILURE;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeNmSendDelPeerCertReqToIke                      */
/*  Description     : This function is used to send policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pu1CertId - Certificate identification            */
/*                  : u1DelFlag - Flag to indicate dynamic peer certs   */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeNmSendDelPeerCertReqToIke (UINT4 u4IkeEngineIndex,
                              tIkePhase1ID * pPhase1ID, UINT1 u1Flag,
                              UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;

    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeDelPeerCertInfo.u4IkeEngineIndex =
        u4IkeEngineIndex;

    if (pPhase1ID != NULL)
    {
        IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.IkeDelPeerCertInfo.Phase1ID,
                    pPhase1ID, sizeof (tIkePhase1ID));
    }

    if (u1Flag == IKE_DYNAMIC_PEERCERT)
    {
        IkeConfigIfParam.ConfigReq.IkeDelPeerCertInfo.bDynamic = IKE_TRUE;
    }
    else
    {
        IkeConfigIfParam.ConfigReq.IkeDelPeerCertInfo.bDynamic = IKE_FALSE;
    }

    if (IkeSendConfMsgToIke (&IkeConfigIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeNmSendDelPeerCertReqToIke: Failed to send event\n");
        return IKE_FAILURE;
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeNmSendDelCaCertInfoToIke                       */
/*  Description     : This function is used to send policy              */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pu1CertId - Certificate identification            */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeNmSendDelCaCertInfoToIke (UINT4 u4IkeEngineIndex, UINT1 *pu1CertId,
                             UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IkeConfigIfParam.u4MsgType = u4Msg;

    IkeConfigIfParam.ConfigReq.IkeDelCaCertInfo.u4IkeEngineIndex =
        u4IkeEngineIndex;

    if ((pu1CertId != NULL) && (STRLEN (pu1CertId) < MAX_NAME_LENGTH))
    {
        IKE_STRNCPY (IkeConfigIfParam.ConfigReq.IkeDelCaCertInfo.au1CertId,
                     pu1CertId, STRLEN (pu1CertId));
    }

    if (IkeSendConfMsgToIke (&IkeConfigIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC,
                     "IKE",
                     "IkeNmSendDelCaCertInfoToIke: Failed to send event\n");
        return IKE_FAILURE;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeFreeRemoteAccessInfo                           */
/*  Description     : This function Frees the Remote Access Policy Info */
/*  Input(s)        : pNode - The RA Policy node                        */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IkeFreeRemoteAccessInfo (t_SLL_NODE * pNode)
{
    tIkeRemoteAccessInfo *pRAPolicy = NULL;

    pRAPolicy = (tIkeRemoteAccessInfo *) pNode;
    MemReleaseMemBlock (IKE_RAINFO_MEMPOOL_ID, (UINT1 *) pRAPolicy);
}

/************************************************************************/
/*  Function Name   : IkeRAInfoSetTransformSetBundle                    */
/*  Description     : This function is used to Set Transform Bundles.   */
/*  Input(s)        : pIkeEngine - Pointer to Engine Structure          */
/*                  : pIkeRAInfo - Pointer to Remote Access Info Struct */
/*                  : pu1TransformSetBundle - Pointer to Transform      */
/*                  :                         Bundle                    */
/*                  : i4BundleLen - Length of Bundle.                   */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS / IKE_FAILURE                         */
/************************************************************************/

INT1
IkeRAInfoSetTransformSetBundle (tIkeEngine * pIkeEngine,
                                tIkeRemoteAccessInfo * pIkeRAInfo,
                                UINT1 *pu1TransformSetBundle, INT4 i4BundleLen)
{
    UINT1               au1TS1[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1              *pu1TS2 = NULL;
    UINT1              *pu1TS3 = NULL;
    UINT1              *pu1TS4 = NULL;
    UINT1              *pu1CopyStr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               u1CopyNum = IKE_ONE;
    UINT4               u4Count = IKE_ZERO, u4Count1 = IKE_ZERO;

    pu1Temp = pu1TransformSetBundle;
    pu1CopyStr = au1TS1;

    for (u4Count = IKE_ZERO; u4Count < (UINT4) i4BundleLen; u4Count++)
    {
        if (pu1Temp[u4Count] == '.')
        {
            pu1CopyStr[u4Count1] = '\0';
            u4Count1 = IKE_ZERO;
            u1CopyNum++;
            switch (u1CopyNum)
            {
                case IKE_TWO:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS2) == MEM_FAILURE)
                    {
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS2 == NULL)
                    {
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS2, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS2;
                    break;
                }
                case IKE_THREE:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS3) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS3 == NULL)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS3, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS3;
                    break;
                }
                case IKE_FOUR:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS4) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }

                    if (pu1TS4 == NULL)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS4, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS4;
                    break;
                }
                default:
                    break;
            }
            continue;
        }
        else
        {
            pu1CopyStr[u4Count1] = pu1Temp[u4Count];
            u4Count1++;
        }
    }
    pu1CopyStr[u4Count1] = '\0';
    pIkeRAInfo->aTransformSetBundle[IKE_INDEX_0] =
        IkeSnmpGetTransformSet (pIkeEngine, au1TS1, (INT4) STRLEN (au1TS1));
    if (pIkeRAInfo->aTransformSetBundle[IKE_INDEX_0] == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        return (IKE_FAILURE);
    }
    pIkeRAInfo->aTransformSetBundle[IKE_INDEX_0]->u4RefCount++;

    if (pu1TS2 != NULL)
    {
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_1] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS2, (INT4) STRLEN (pu1TS2));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        if (pIkeRAInfo->aTransformSetBundle[IKE_INDEX_1] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_1]->u4RefCount++;
    }

    if (pu1TS3 != NULL)
    {
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_2] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS3, (INT4) STRLEN (pu1TS3));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        if (pIkeRAInfo->aTransformSetBundle[IKE_INDEX_2] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_2]->u4RefCount++;
    }

    if (pu1TS4 != NULL)
    {
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_3] =
            IkeSnmpGetTransformSet (pIkeEngine, pu1TS4, (INT4) STRLEN (pu1TS4));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        if (pIkeRAInfo->aTransformSetBundle[IKE_INDEX_3] == NULL)
        {
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aTransformSetBundle[IKE_INDEX_3]->u4RefCount++;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeProtectNetSetNetworkAddress                    */
/*  Description     : This function is used to Set Protected Network    */
/*                  : Address.                                          */
/*  Input(s)        : pIkeNetwork - Pointer to Network Structure        */
/*                  : pu1Addr - Pointer to Local Address.               */
/*                  : i4AddrLen - Length of Address.                    */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS / IKE_FAILURE                         */
/************************************************************************/

INT4
IkeProtectNetSetNetworkAddress (tIkeNetwork * pIkeNetwork, UINT1 *pu1Addr,
                                INT4 i4AddrLen)
{
    UINT1              *pu1LocalCopyAddr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    tIp4Addr            u4Ip4Addr = IKE_ZERO;
    tIp6Addr            Ip6Addr;
    UINT4               u4Prefix = IKE_ZERO;
    UINT4               u4TempLen = IKE_ZERO;

    if (i4AddrLen > IKE_MAX_ADDR_STR_LEN)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProtectNetSetNetworkAddress:Invalid address length\n");
        return (IKE_FAILURE);
    }

    if (i4AddrLen == IKE_MAX_ADDR_STR_LEN)
    {
        i4AddrLen = IKE_MAX_ADDR_STR_LEN - IKE_ONE;
    }
    u4TempLen = MEM_MAX_BYTES ((UINT4) i4AddrLen, STRLEN (pu1Addr));
    IKE_MEMCPY (au1LocalAddr, pu1Addr, u4TempLen);
    au1LocalAddr[u4TempLen] = '\0';
    pu1LocalCopyAddr = au1LocalAddr;

    /* Test for Subnet */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_SUBNET_SEPARATOR);

    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';

        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_SUBNET;
            pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr = IKE_NTOHL (u4Ip4Addr);
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV4_PREFIX)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCryptoSetNetworkAddress:Invalid IPV4 address"
                             "prefix \n ");
                return (IKE_FAILURE);
            }

            pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask =
                gaIp4PrefixToMask[u4Prefix];
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_SUBNET;
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Subnet.Ip6Addr,
                        &Ip6Addr, sizeof (Ip6Addr));
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            u4Prefix = (UINT4) IKE_ATOI (pu1LocalCopyAddr);
            if (u4Prefix > IKE_MAX_IPV6_PREFIX)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCryptoSetNetworkAddress:Invalid IPV6 address"
                             "prefix \n ");
                return (IKE_FAILURE);
            }

            IKE_MEMCPY (pIkeNetwork->uNetwork.
                        Ip6Subnet.Ip6SubnetMask.
                        u1_addr, gaIp6PrefixToMask[u4Prefix],
                        sizeof (tIp6Addr));
        }
        else
        {
            return IKE_FAILURE;    /* Not a valid string */
        }
        return IKE_SUCCESS;
    }
    /* Test for Range */
    pu1Temp = (UINT1 *) STRCHR (pu1LocalCopyAddr, IKE_RANGE_SEPARATOR);
    if (pu1Temp != NULL)
    {
        *pu1Temp = '\0';
        if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_RANGE;
            pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr = IKE_NTOHL (u4Ip4Addr);

            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
            {
                pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr
                    = IKE_NTOHL (u4Ip4Addr);

            }
            else
            {
                return IKE_FAILURE;    /* Invalid string */
            }
        }
        else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
        {
            pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_RANGE;
            IKE_MEMCPY (&pIkeNetwork->uNetwork.
                        Ip6Range.Ip6StartAddr, &Ip6Addr, sizeof (Ip6Addr));
            pu1LocalCopyAddr = pu1Temp + IKE_ONE;
            if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
            {
                IKE_MEMCPY (&pIkeNetwork->uNetwork.
                            Ip6Range.Ip6EndAddr, &Ip6Addr, sizeof (Ip6Addr));
            }
            else
            {
                return IKE_FAILURE;
            }
        }

        return IKE_SUCCESS;
    }

    /* Test for Host address */

    if (INET_PTON4 (pu1LocalCopyAddr, &u4Ip4Addr) > IKE_ZERO)
    {
        pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV4_ADDR;
        pIkeNetwork->uNetwork.Ip4Addr = IKE_NTOHL (u4Ip4Addr);
    }
    else if (INET_PTON6 (pu1LocalCopyAddr, &Ip6Addr) > IKE_ZERO)
    {
        pIkeNetwork->u4Type = IKE_IPSEC_ID_IPV6_ADDR;
        IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Addr, &Ip6Addr, sizeof (Ip6Addr));
    }
    else
    {
        return IKE_FAILURE;        /* Invalid String */
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeRAInfoSetProtectNetBundle                      */
/*  Description     : This function is used to set the configured       */
/*                  : Transform-sets into the Crypto-map                */
/*  Input(s)        : pIkeEngine    - Pointer to the IkeEngine          */
/*                  : pIkeCryptoMap - Pointer to the IkeCryptoMap       */
/*                  : pu1TSBundle   - Pointer to the Transform-set's    */
/*                  : i4BundleLen   - The Length of the Bundle          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeRAInfoSetProtectNetBundle (tIkeEngine * pIkeEngine,
                              tIkeRemoteAccessInfo * pIkeRAInfo,
                              UINT1 *pu1ProtectNetBundle, INT4 i4BundleLen)
{
    UINT1               au1TS1[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1              *pu1TS2 = NULL;
    UINT1              *pu1TS3 = NULL;
    UINT1              *pu1TS4 = NULL;
    UINT1              *pu1CopyStr = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               u1CopyNum = IKE_ONE;
    UINT4               u4Count = IKE_ZERO, u4Count1 = IKE_ZERO;

    pu1Temp = pu1ProtectNetBundle;
    pu1CopyStr = au1TS1;

    for (u4Count = IKE_ZERO; u4Count < (UINT4) i4BundleLen; u4Count++)
    {
        if (pu1Temp[u4Count] == '.')
        {
            pu1CopyStr[u4Count1] = '\0';
            u4Count1 = IKE_ZERO;
            u1CopyNum++;
            switch (u1CopyNum)
            {
                case IKE_TWO:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS2) == MEM_FAILURE)
                    {
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS2 == NULL)
                    {
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS2, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS2;
                    break;
                }
                case IKE_THREE:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS3) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS3 == NULL)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS4 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS4);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS3, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS3;
                    break;
                }
                case IKE_FOUR:
                {
                    if (MemAllocateMemBlock
                        (IKE_UINT_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pu1TS4) == MEM_FAILURE)
                    {
                        if (pu1TS2 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS2);
                        if (pu1TS3 != NULL)
                            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                                (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle: unable to allocate "
                                     "buffer\n");
                        return IKE_FAILURE;
                    }
                    if (pu1TS4 == NULL)
                    {
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1TS2);
                        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                            (UINT1 *) pu1TS3);
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeCryptoSetTransformSetBundle:unable"
                                     " to allocate memory\n");
                        return IKE_FAILURE;
                    }
                    MEMSET (pu1TS4, IKE_ZERO, (MAX_NAME_LENGTH + IKE_ONE));
                    pu1CopyStr = pu1TS4;
                    break;
                }
                default:
                    break;
            }
            continue;
        }
        else
        {
            pu1CopyStr[u4Count1] = pu1Temp[u4Count];
            u4Count1++;
        }
    }
    pu1CopyStr[u4Count1] = '\0';
    pIkeRAInfo->aProtectNetBundle[IKE_INDEX_0] =
        IkeSnmpGetProtectNet (pIkeEngine, au1TS1, (INT4) STRLEN (au1TS1));
    if (pIkeRAInfo->aProtectNetBundle[IKE_INDEX_0] == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        return (IKE_FAILURE);
    }
    IKE_MEMCPY (&(pIkeRAInfo->CMServerInfo.ProtectedNetwork[IKE_INDEX_0]),
                &(pIkeRAInfo->aProtectNetBundle[IKE_INDEX_0]->ProtectedNetwork),
                sizeof (tIkeNetwork));
    pIkeRAInfo->aProtectNetBundle[IKE_INDEX_0]->u4RefCount++;

    if (pu1TS2 != NULL)
    {
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_1] =
            IkeSnmpGetProtectNet (pIkeEngine, pu1TS2, (INT4) STRLEN (pu1TS2));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS2);
        if (pIkeRAInfo->aProtectNetBundle[IKE_INDEX_1] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_1]->u4RefCount++;
    }

    if (pu1TS3 != NULL)
    {
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_2] =
            IkeSnmpGetProtectNet (pIkeEngine, pu1TS3, (INT4) STRLEN (pu1TS3));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS3);
        if (pIkeRAInfo->aProtectNetBundle[IKE_INDEX_2] == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_2]->u4RefCount++;
    }

    if (pu1TS4 != NULL)
    {
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_3] =
            IkeSnmpGetProtectNet (pIkeEngine, pu1TS4, (INT4) STRLEN (pu1TS4));
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1TS4);
        if (pIkeRAInfo->aProtectNetBundle[IKE_INDEX_3] == NULL)
        {
            return (IKE_FAILURE);
        }
        pIkeRAInfo->aProtectNetBundle[IKE_INDEX_3]->u4RefCount++;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendRAInfoToIke                            */
/*  Description     : This function is used to send RA info             */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pIkeCryptoMap     - Pointer to Crypto-map         */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendRAInfoToIke (UINT4 u4IkeEngineIndex,
                        tIkeRemoteAccessInfo * pIkeRAInfo, UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IKE_UNUSED (pIkeRAInfo);
    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeConfRA.u4IkeEngineIndex = u4IkeEngineIndex;
    IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.
                IkeConfRA.PeerIpAddr,
                &pIkeRAInfo->PeerAddr, sizeof (tIkeIpAddr));
    IkeSendConfMsgToIke (&IkeConfigIfParam);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeSnmpSendProtectNetInfoToIke                    */
/*  Description     : This function is used to send Protect Net info    */
/*                  : configuration message from SNMP/CLI to IKE        */
/*  Input(s)        : u4IkeEngineIndex  - The IKE Engine Index          */
/*                  : pIkeProtectNet     - Pointer to Protect Net       */
/*                  : u4Msg - The Message Type                          */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS of IKE_FAILURE                        */
/************************************************************************/

INT1
IkeSnmpSendProtectNetInfoToIke (UINT4 u4IkeEngineIndex,
                                tIkeRAProtectNet * pIkeRAProtectNetInfo,
                                UINT4 u4Msg)
{
    tIkeConfigIfParam   IkeConfigIfParam;
    IKE_UNUSED (pIkeRAProtectNetInfo);
    IkeConfigIfParam.u4MsgType = u4Msg;
    IkeConfigIfParam.ConfigReq.IkeConfRAProtectNet.u4IkeEngineIndex =
        u4IkeEngineIndex;
#if 0
    IKE_MEMCPY (&IkeConfigIfParam.ConfigReq.
                IkeConfRA.PeerIpAddr,
                &pIkeCryptoMap->PeerAddr, sizeof (tIkeIpAddr));
#endif
    IkeSendConfMsgToIke (&IkeConfigIfParam);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeDeleteRAInfo                                   */
/*  Description     : This function is used to delete the RA Info       */
/*  Input(s)        : pIkeEngine - Engine Entry Pointer                 */
/*                  : pIkeRAInfo - RA Info Pointer to be deleted        */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeDeleteRAInfo (tIkeEngine * pIkeEngine, tIkeRemoteAccessInfo * pIkeRAInfo)
{
    INT4                i4Count = IKE_ZERO;
    for (i4Count = IKE_ZERO; i4Count < IKE_MAX_PROTECTED_NET; i4Count++)
    {
        if (pIkeRAInfo->aProtectNetBundle[i4Count] != NULL)
        {
            pIkeRAInfo->aProtectNetBundle[i4Count]->u4RefCount--;
            pIkeRAInfo->aProtectNetBundle[i4Count] = NULL;
        }
    }
    for (i4Count = IKE_ZERO; i4Count < MAX_TRANSFORMS; i4Count++)
    {

        if (pIkeRAInfo->aTransformSetBundle[i4Count] != NULL)
        {
            pIkeRAInfo->aTransformSetBundle[i4Count]->u4RefCount--;
            pIkeRAInfo->aTransformSetBundle[i4Count] = NULL;
        }
    }
    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkeRAPolicyList), (t_SLL_NODE *) pIkeRAInfo);
    GIVE_IKE_SEM ();
}

/************************************************************************/
/*  Function Name   : IkeDeleteProtectNet                               */
/*  Description     : This function is used to delete the Protected net */
/*  Input(s)        : pIkeEngine - Engine Entry Pointer                 */
/*                  : pIkeRAProtectNet - Protect Net Pointer to be      */
/*                    deleted                                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeDeleteProtectNet (tIkeEngine * pIkeEngine,
                     tIkeRAProtectNet * pIkeRAProtectNet)
{
    if (pIkeRAProtectNet->u4RefCount != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteProtectNet:Protect Net Refcount not 0!\n");
        return;
    }
    TAKE_IKE_SEM ();
    SLL_DELETE (&(pIkeEngine->IkeProtectNetList),
                (t_SLL_NODE *) pIkeRAProtectNet);
    GIVE_IKE_SEM ();
}

/************************************************************************/
/*  Function Name   : IkeGetPolicyPeerIdStatus                          */
/*  Description     : This function is used to get the Phase1 Policy    */
/*                  : given the Engine and Policy priority (index)      */
/*  Input(s)        : pIkeEngineName - Pointer to the Engine Name       */
/*                  : NameLen - Engine Name Length                      */
/*                  : u4FsIkePolicyPriority - The policy priority       */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeGetPolicyPeerIdStatus (UINT1 *pEngineName, INT4 NameLen,
                          UINT4 u4FsIkePolicyPriority)
{

    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    pIkeEngine = IkeSnmpGetEngine (pEngineName, NameLen);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePeerIdSetStatus:Engine does not exist\n");
        return (IKE_FAILURE);
    }

    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4FsIkePolicyPriority);

    if (pIkePolicy == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkePeerIdSetStatus:Policy not created!\n");
        return (IKE_FAILURE);
    }

    if (pIkePolicy->u1PeerIdMask != IKE_POLICY_PEER_ID_SET)
    {
        SLL_DELETE (&(pIkeEngine->IkePolicyList), (t_SLL_NODE *) pIkePolicy);
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeStoreIkeSA                                      */
/*  Description     :This function is used to store the Ike SA in the   */
/*                  :SA List at the end of Phase1                       */
/*  Input(s)        :pSessionInfo - Pointer to the SessionInfo          */
/*                  :structure                                          */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeStoreIkeSA (tIkeSessionInfo * pSessionInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4IVLen = IKE_ZERO;
    UINT4               u4SoftLifetimeFactor = IKE_ZERO;
    tIkeSA             *pIkeSA = NULL;
    UINT4              u4Time = IKE_ZERO;

    if (pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH)
    {
        pIkeSA = pSessionInfo->pIkeReKeySA;
    }
    else
    {
        pIkeSA = pSessionInfo->pIkeSA;
    }

    if (pIkeSA->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        /* Store the Final Phase1 IV in IkeSA in case of IKEv1, 
         * In case  of Ikev2 it is already allocated during key generation */
        u4IVLen = gaIkeCipherAlgoList[pIkeSA->u1Phase1EncrAlgo].u4BlockLen;
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pIkeSA->pu1FinalPhase1IV) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeStoreIkeSA: unable to allocate " "buffer\n");
            return IKE_FAILURE;
        }
        if (pIkeSA->pu1FinalPhase1IV == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeStoreIkeSA: Memory allocation for IV failed !\n");
            return (IKE_FAILURE);

        }
        IKE_MEMCPY (pIkeSA->pu1FinalPhase1IV, pSessionInfo->pu1CryptIV,
                    u4IVLen);
    }

    if (pIkeSA->u4LifeTime != IKE_ZERO)
    {
        /* Record the absolute time when the SA is to be deleted */
        OsixGetSysTime (&u4Time);
        /* Convert to seconds */
        pIkeSA->u4AbsHardLifeTime = pIkeSA->u4LifeTime;
        pIkeSA->u4AbsHardLifeTime += (u4Time/SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

        /* Start the soft lifetime timer */
        /* for a period between 80 and 90% of lifetime */
        u4SoftLifetimeFactor = IKE_MIN_SOFTLIFETIME_FACTOR +(u4Time % 10);

        if (IkeStartTimer (&pIkeSA->IkeSALifeTimeTimer, IKE_REKEY_TIMER_ID,
                           (UINT4) ((pIkeSA->u4LifeTime *
                                     u4SoftLifetimeFactor) / IKE_DIVISOR_100),
                           pIkeSA) != IKE_SUCCESS)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeStoreIkeSA: IkeStartTimer failed!\n");
            return (IKE_FAILURE);
        }
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IkeStopTimer (&pIkeSA->IkeSALifeTimeTimer);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeStoreIkeSA: No Engine found !\n");
        return IKE_FAILURE;
    }

    /* Insert the SA in the Engine SA List */
    SLL_INSERT_IN_FRONT (&pIkeEngine->IkeSAList, (tTMO_SLL_NODE *) pIkeSA);

    /* Increment IKE phase1 negotiation succcess */
    IncIkeNoOfPhase1SessionsSucc ();

    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeFreeSessionInfo 
 *  Description   : Function to free memory allocated for a 
 *                  session.
 *  Input(s)      : pNode - session info pointer.
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT4
IkeFreeSessionInfo (t_SLL_NODE * pNode)
{
    INT4                i4Pos = IKE_ZERO;
    tIkeSessionInfo    *pSessionInfo = (tIkeSessionInfo *) pNode;

    IkeStopTimer (&pSessionInfo->IkeRetransTimer);
    IkeStopTimer (&pSessionInfo->IkeSessionDeleteTimer);
    if (pSessionInfo->u4RefCount != IKE_ZERO)
    {
        IKE_ASSERT (pSessionInfo->u4RefCount == IKE_ZERO);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeFreeSessionInfo:SessionInfo RefCount not 0!\n");
        return (IKE_FAILURE);
    }

    if (pSessionInfo->IkeRxPktInfo.pu1RawPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->IkeRxPktInfo.pu1RawPkt);
        pSessionInfo->IkeRxPktInfo.pu1RawPkt = NULL;
    }

    /* Delete the Payload structure */
    for (i4Pos = IKE_ZERO; i4Pos <= IKE_MAX_PAYLOADS; i4Pos++)
    {
        SLL_DELETE_LIST (&(pSessionInfo->IkeRxPktInfo.aPayload[i4Pos]),
                         IkeFreePayload);
    }

    if (pSessionInfo->IkeTxPktInfo.pu1RawPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->IkeTxPktInfo.pu1RawPkt);
        pSessionInfo->IkeTxPktInfo.pu1RawPkt = NULL;
    }

    if (pSessionInfo->pu1InitiatorNonce != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1InitiatorNonce);
        pSessionInfo->pu1InitiatorNonce = NULL;
    }

    if (pSessionInfo->pu1ResponderNonce != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1ResponderNonce);
        pSessionInfo->pu1ResponderNonce = NULL;
    }

    if ((pSessionInfo->u1ExchangeType == IKE_MAIN_MODE) ||
        (pSessionInfo->u1ExchangeType == IKE_AGGRESSIVE_MODE))
    {
        if (pSessionInfo->IkePhase1Info.pu1PreSharedKey != NULL)
        {
            /* For FIPS compliance */
            IKE_BZERO (pSessionInfo->IkePhase1Info.pu1PreSharedKey,
                       pSessionInfo->IkePhase1Info.u2PreSharedKeyLen);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->IkePhase1Info.
                                pu1PreSharedKey);
            pSessionInfo->IkePhase1Info.pu1PreSharedKey = NULL;
        }
    }

    if (pSessionInfo->pMyDhPub != NULL)
    {
        HexNumFree (pSessionInfo->pMyDhPub);
        pSessionInfo->pMyDhPub = NULL;
    }

    if (pSessionInfo->pMyDhPrv != NULL)
    {
        HexNumFree (pSessionInfo->pMyDhPrv);
        pSessionInfo->pMyDhPrv = NULL;
    }

    if (pSessionInfo->pHisDhPub != NULL)
    {
        HexNumFree (pSessionInfo->pHisDhPub);
        pSessionInfo->pHisDhPub = NULL;
    }

    if (pSessionInfo->pDhSharedSecret != NULL)
    {
        HexNumFree (pSessionInfo->pDhSharedSecret);
        pSessionInfo->pDhSharedSecret = NULL;
    }

    if (pSessionInfo->pSAOffered != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pSAOffered);
        pSessionInfo->pSAOffered = NULL;
    }

    if (pSessionInfo->pu1SaSelected != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1SaSelected);
        pSessionInfo->pu1SaSelected = NULL;
    }

    if (pSessionInfo->pu1IdPayloadSent != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1IdPayloadSent);
        pSessionInfo->pu1IdPayloadSent = NULL;
        pSessionInfo->u4IdPayloadSentLen = 0;
    }

    if (pSessionInfo->pu1IdPayloadRecd != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1IdPayloadRecd);
        pSessionInfo->pu1IdPayloadRecd = NULL;
        pSessionInfo->u4IdPayloadRecdLen = 0;
    }

    if (pSessionInfo->pu1CryptIV != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1CryptIV);
        pSessionInfo->pu1CryptIV = NULL;
    }

    if (pSessionInfo->pIkeSA != NULL)
    {
        pSessionInfo->pIkeSA->u4RefCount--;
        /* Don't free the SA, just set the pointer to NULL */
        pSessionInfo->pIkeSA = NULL;
    }

    if (pSessionInfo->pu1LastRcvdPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1LastRcvdPkt);
        pSessionInfo->pu1LastRcvdPkt = NULL;
    }

    if (pSessionInfo->pu1LastRcvdGoodPkt != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pSessionInfo->pu1LastRcvdGoodPkt);
        pSessionInfo->pu1LastRcvdGoodPkt = NULL;
    }

    IKE_BZERO (pSessionInfo, sizeof (tIkeSessionInfo));
    MemReleaseMemBlock (IKE_SESSION_INFO_MEMPOOL_ID, (UINT1 *) pSessionInfo);
    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDeleteSession 
 *  Description   : This function will remove a session info
 *                  from the session database and delete all 
 *                  memories allocated for that session.
 *  Input(s)      : 
 *                  pSesInfo - Pointer to the session to be
 *                                 deleted.
 *  Output(s)     :  None.
 *  Return Values :  IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT4
IkeDeleteSession (tIkeSessionInfo * pSessionInfo)
{
    tIkeEngine         *pIkeEngine = NULL;

    pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteSession:Engine not found!\n");
        return (IKE_FAILURE);
    }

    /*    SLL_DELETE_LIST (&(pSessionInfo->PeerCertReq), IkeFreeCertReq); */
    SLL_DELETE (&pIkeEngine->IkeSessionList, (t_SLL_NODE *) pSessionInfo);
    return (IkeFreeSessionInfo ((t_SLL_NODE *) pSessionInfo));
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeFreeCertReq
 *  Description   : Function to free the CertReqinfo in Session
 *  Input(s)      : pNode - The proposal(tIkeProposal) Node
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */

INT1
IkeFreeCertReq (t_SLL_NODE * pNode)
{

    tIkeCertReqInfo    *pCertReq = NULL;

    pCertReq = (tIkeCertReqInfo *) pNode;

    if (pCertReq != NULL)
    {

        if (pCertReq->u4HashListLen != IKE_ZERO)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pCertReq->pu1HashList);
        }
        pCertReq->pu1HashList = NULL;
        MemReleaseMemBlock (IKE_CERT_INFO_MEMPOOL_ID, (UINT1 *) pCertReq);
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelAllSessSAInEngine    
 *  Description   : Function to delete all Active Sessions and SAs   
 *                : in the specified engine
 *  Input(s)      : pIkeEngine                                 
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelAllSessSAInEngine (tIkeEngine * pIkeEngine)
{

    /* Delete all ongoing Phase1/Phase2 sessions */
    SLL_DELETE_LIST (&(pIkeEngine->IkeSessionList), IkeFreeSessionInfo);

    /* Delete all Phase1 SAs */
    SLL_DELETE_LIST (&(pIkeEngine->IkeSAList), IkeFreeSAInfo);

    /* Delete all Phase2 SAs */
    /* Here, we have to delete all Phase2 SAs that correspond to our Engine, 
       so we just pass the Local Tunnel Termination Address
     */
    IkeDelPhase2SaForLocal (&pIkeEngine->Ip4TunnelTermAddr);
    IkeDelPhase2SaForLocal (&pIkeEngine->Ip6TunnelTermAddr);
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelPhase2Sa             
 *  Description   : Function to delete Phase2 SAs for an address pair     
 *  Input(s)      : pLocAddr - The Local Address               
 *                : pRemAddr - The Remote Address               
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelPhase2Sa (tIkeIpAddr * pLocAddr, tIkeIpAddr * pRemAddr, UINT1 u1IkeVer)
{
    tIkeIPSecIfParam    IPSecIfParam;
    INT1                i1RetVal = IKE_ZERO;

    IKE_BZERO (&IPSecIfParam, sizeof (tIkeIPSecIfParam));
    IPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
    IKE_MEMCPY (&IPSecIfParam.IPSecReq.DelSAInfo.LocalAddr, pLocAddr,
                sizeof (tIkeIpAddr));
    if (pRemAddr != NULL)
    {
        IKE_MEMCPY (&IPSecIfParam.IPSecReq.DelSAInfo.RemoteAddr, pRemAddr,
                    sizeof (tIkeIpAddr));
    }
    IPSecIfParam.IPSecReq.DelSAInfo.u1IkeVersion = u1IkeVer;
    i1RetVal = IkeSendIPSecIfParam (&IPSecIfParam);
    if (i1RetVal == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }
    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelPhase2SaForLocal             
 *  Description   : Function to delete Phase2 SAs for an address pair     
 *  Input(s)      : pLocAddr - The Local Address               
 *                : pRemAddr - The Remote Address               
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelPhase2SaForLocal (tIkeIpAddr * pLocAddr)
{
    tIkeIPSecIfParam    IPSecIfParam;
    INT1                i1RetVal = IKE_ZERO;

    IKE_BZERO (&IPSecIfParam, sizeof (tIkeIPSecIfParam));
    IPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG;
    IKE_MEMCPY (&IPSecIfParam.IPSecReq.DelSAInfo.LocalAddr, pLocAddr,
                sizeof (tIkeIpAddr));
    i1RetVal = IkeSendIPSecIfParam (&IPSecIfParam);
    if (i1RetVal == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelSessSAForPeer        
 *  Description   : Function to delete all Active Sessions and SAs   
 *                : for a specified peer   
 *  Input(s)      : pIkeEngine - Pointer to IkeEngine          
 *                : pPeerIpAddr - The Peer IP Address          
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeDelSessSAForPeer (tIkeEngine * pIkeEngine, tIkeIpAddr * pPeerIpAddr)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeSA             *pIkeSA = NULL;
    VOID               *pPrev = NULL;
    UINT1               u1IkeVer = IKE_ZERO;

    SLL_SCAN (&(pIkeEngine->IkeSessionList), pSessionInfo, tIkeSessionInfo *)
    {
        if (IKE_MEMCMP (&pSessionInfo->RemoteTunnelTermAddr, pPeerIpAddr,
                        sizeof (tIkeIpAddr)) == IKE_ZERO)
        {
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDelSessSAForPeer: Unable to "
                             "delete Session!\n");
                return (IKE_FAILURE);
            }
            pSessionInfo = (tIkeSessionInfo *) pPrev;
        }
        pPrev = (VOID *) pSessionInfo;
    }

    pPrev = NULL;
    SLL_SCAN (&(pIkeEngine->IkeSAList), pIkeSA, tIkeSA *)
    {
        if (IKE_MEMCMP (&pIkeSA->IpPeerAddr, pPeerIpAddr,
                        sizeof (tIkeIpAddr)) == IKE_ZERO)
        {
            SLL_DELETE (&pIkeEngine->IkeSAList, (t_SLL_NODE *) pIkeSA);
            u1IkeVer = pIkeSA->u1IkeVer;
            if (IkeFreeSAInfo ((t_SLL_NODE *) pIkeSA) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeDelSessSAForPeer: Unable to delete SA!\n");
                return (IKE_FAILURE);
            }
            pIkeSA = (tIkeSA *) pPrev;
        }
        pPrev = (VOID *) pIkeSA;
    }

    if (pPeerIpAddr->u4AddrType == IPV4ADDR)
    {
        IkeDelPhase2Sa (&pIkeEngine->Ip4TunnelTermAddr, pPeerIpAddr, u1IkeVer);
    }
    else
    {
        IkeDelPhase2Sa (&pIkeEngine->Ip6TunnelTermAddr, pPeerIpAddr, u1IkeVer);
    }
    return (IKE_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeDelAllSessSAInEngineWithPeer        
 *  Description   : Function to delete all Active Sessions and SAs   
 *                : for a specified peer (given as Phase1 ID)   
 *  Input(s)      : pIkeEngine - Pointer to IkeEngine          
 *                : pPeerId - The Peer IP Address          
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */
INT1
IkeDelAllSessSAInEngineWithPeer (tIkeEngine * pIkeEngine,
                                 tIkePhase1ID * pPeerId)
{
    tIkeIpAddr          IkeAddr;
    tIkeSA             *pIkeSA = NULL;
    INT1                i1RetVal = IKE_ZERO;

    IKE_BZERO (&IkeAddr, sizeof (tIkeIpAddr));

    if (pPeerId->i4IdType == IKE_KEY_IPV4)
    {
        IkeAddr.u4AddrType = IPV4ADDR;
        IKE_MEMCPY (&(IkeAddr.uIpAddr.Ip4Addr), &(pPeerId->uID.Ip4Addr),
                    sizeof (tIp4Addr));
    }
    else if (pPeerId->i4IdType == IKE_KEY_IPV6)
    {
        IkeAddr.u4AddrType = IPV6ADDR;
        IKE_MEMCPY (&(IkeAddr.uIpAddr.Ip6Addr), &(pPeerId->uID.Ip6Addr),
                    sizeof (tIp6Addr));
    }
    else
    {
        /* For non-ip Identifiers, ongoing sessions may not get cleaned up */
        SLL_SCAN (&(pIkeEngine->IkeSAList), pIkeSA, tIkeSA *)
        {
            if ((IKE_MEMCMP (&pIkeSA->InitiatorID, pPeerId,
                             sizeof (tIkePhase1ID)) == IKE_ZERO) ||
                (IKE_MEMCMP (&pIkeSA->ResponderID, pPeerId,
                             sizeof (tIkePhase1ID)) == IKE_ZERO))
            {
                IKE_MEMCPY (&IkeAddr, &(pIkeSA->IpPeerAddr),
                            sizeof (tIkeIpAddr));
            }
        }
    }

    if ((IkeAddr.u4AddrType == IPV4ADDR) || (IkeAddr.u4AddrType == IPV6ADDR))
    {
        i1RetVal = IkeDelSessSAForPeer (pIkeEngine, &IkeAddr);
        if (i1RetVal == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    else
    {
        /* The Peer ID is non-IP identifier and there are no SAs
         * established with the peer. We will miss any ongoing
         * negotiations at this point. This window is small enough
         * to be ignored for now. */
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}
