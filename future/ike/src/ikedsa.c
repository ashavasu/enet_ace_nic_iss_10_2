/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikedsa.c,v 1.13 2014/10/29 12:52:52 siva Exp $
 *
 * Description: This file has DSA wrapper functions.  
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :IkeDsaGenKeyPair                                   */
/*  Description     :This function is used to generate DSA key pair     */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify whether the key is already existing or */
/*                  :not.If not it will generate the new key and add    */
/*                  :the key to the database.                           */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :u2NBits     : Length of the key to be generated    */
/*                  :pu4ErrNo    : Return Error Number                  */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDsaGenKeyPair (UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT2 u2NBits,
                  UINT4 *pu4ErrNo)
{
    tIkeDSA            *pIkeDSA = NULL;

    if (IkeDsaVerifyKeyId (pu1EngineId, pu1KeyId) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaGenKeyPair:Key Id  already exist in"
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_KEY_ID_ALREADY_EXIST;
        return IKE_FAILURE;
    }

    pIkeDSA = DSA_generate_parameters (u2NBits, NULL, IKE_ZERO, NULL, NULL,
                                       NULL, NULL);
    if (pIkeDSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaGenKeyPair: Key Parameter Generation failed" "\n");
        *pu4ErrNo = IKE_ERR_KEY_GEN_FAILED;
        return IKE_FAILURE;
    }

    if (IKE_TRUE == !DSA_generate_key (pIkeDSA))
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaGenKeyPair: Key Generation failed" "\n");
        *pu4ErrNo = IKE_ERR_ADD_TO_DB_FAILED;
        return IKE_FAILURE;
    }

    if (IkeAddDsaKeyEntryToDB (pu1EngineId, pu1KeyId, pIkeDSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaGenKeyPair:Failed to add entry in "
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_ADD_TO_DB_FAILED;
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeImportDsaKey                                    */
/*  Description     :This function is used to Import DSA key            */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify whether the key is already existing or */
/*                  :not.If not it will import the new key and add      */
/*                  :the key to the database.                           */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :pu4ErrNo    : Return Error Number                  */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeImportDsaKey (UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT1 *pu1FileName,
                 UINT4 *pu4ErrNo)
{
    tIkeDSA            *pIkeDSA = NULL;
    BIO                *pBp = NULL;
    INT4                i4RetVal = IKE_ZERO;
    signed char         pu1ReadFileName[IKE_MAX_PATH_SIZE] = { IKE_ZERO };
    UINT4               u4Bits = IKE_ZERO;

    if (IkeDsaVerifyKeyId (pu1EngineId, pu1KeyId) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportDsaKey:Key Id  already exist in"
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_KEY_ID_ALREADY_EXIST;
        return IKE_FAILURE;
    }

    IKE_SNPRINTF (((CHR1 *) pu1ReadFileName), IKE_MAX_PATH_SIZE, "%s/%s",
                  IKE_DEF_USER_DIR, pu1FileName);

    pBp = BIO_new (BIO_s_file ());
    if (pBp == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportDsaKey: Call to BIO_new fails\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     pu1FileName, pu1ReadFileName);
    i4RetVal = BIO_read_filename (pBp, pu1ReadFileName);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pBp);
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportDsaKey: Call to BIO_read_filename fails"
                      "for %s\n", pu1ReadFileName);
        *pu4ErrNo = IKE_ERR_FILE_NOT_EXIST;
        return IKE_FAILURE;
    }

    if ((pIkeDSA =
         (tIkeDSA *) PEM_read_bio_DSAPrivateKey (pBp, NULL, NULL, NULL))
        == NULL)
    {
        BIO_free_all (pBp);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportDsaKey: Call to PEM_read_bio_DSAPrivateKey "
                     "fails\n");
        *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
        return IKE_FAILURE;

    }

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        u4Bits = (UINT4) IKE_BYTES2BITS (DSA_size (pIkeDSA));
        if ((u4Bits == DSA_KEY_512) || (u4Bits == DSA_KEY_768) ||
            (u4Bits == DSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "DSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }
    if (IkeAddDsaKeyEntryToDB (pu1EngineId, pu1KeyId, pIkeDSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportDsaKey: Failed to add entry in "
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_ADD_TO_DB_FAILED;
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDsaVerifyKeyId                                  */
/*  Description     :This function is used to verify if the keys with   */
/*                  :the same key id already exist in the database. If  */
/*                  :yes, the function returns failure otherwise success*/
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeDsaVerifyKeyId (UINT1 *pu1EngineId, UINT1 *pu1KeyId)
{
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;

    pIkeDsaKeyInfo = IkeGetDsaKeyEntry (pu1EngineId, pu1KeyId);
    if (pIkeDsaKeyInfo != NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaVerifyKeyId:Key Id verification failed\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetDsaKeyEntry                                  */
/*  Description     :This function searches the key database for the    */
/*                  :given key id . If found it will return the key info*/
/*                  :otherwise returns NULL                             */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to tIkeDsaKeyInfo or NULL                  */
/************************************************************************/
tIkeDsaKeyInfo     *
IkeGetDsaKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyId)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeGetDsaKeyEntry:Failed to get engine with "
                      "engine Id%s\n", pu1EngineId);
        GIVE_IKE_SEM ();
        return NULL;
    }
    SLL_SCAN (&(pIkeEngine->IkeDsaKeyList), pIkeDsaKeyInfo, tIkeDsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeDsaKeyInfo->au1DsaKeyName, pu1KeyId, IKE_STRLEN (pu1KeyId) + 1)
            == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pIkeDsaKeyInfo;
        }
    }
    GIVE_IKE_SEM ();
    return pIkeDsaKeyInfo;

}

/************************************************************************/
/*  Function Name   :IkeAddDsaKeyEntryToDB                              */
/*  Description     :This function is used to add key entry for the new */
/*                  :ly generated key to the Engine structure database. */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :pIkeDSA     : Pointer to DSA key structure         */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeAddDsaKeyEntryToDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId, tIkeDSA * pIkeDSA)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;

    pIkeDsaKeyInfo = (tIkeDsaKeyInfo *) MemAllocMemBlk (gu4IkeDsaMemPoolId);
    if (pIkeDsaKeyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddDsaKeyToIkeEngine: Memory allocation failed "
                     "for DSA key info \n");
        return (IKE_FAILURE);
    }

    IKE_BZERO (pIkeDsaKeyInfo, sizeof (tIkeDsaKeyInfo));
    if (STRLEN (pu1KeyId) > MAX_NAME_LENGTH)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddDsaKeyToIkeEngine: Improper Key Id length "
                     "for DSA key info \n");
        MemReleaseMemBlock (gu4IkeDsaMemPoolId, (UINT1 *) pIkeDsaKeyInfo);
        return (IKE_FAILURE);
    }
    IKE_STRNCPY (pIkeDsaKeyInfo->au1DsaKeyName, pu1KeyId, STRLEN (pu1KeyId));
    pIkeDsaKeyInfo->DsaKey.pkey.dsa = pIkeDSA;
    pIkeDsaKeyInfo->DsaKey.type = EVP_PKEY_DSA;
    TAKE_IKE_SEM ();

    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeAddDsaKeyToKeyTable:Failed to get engine with "
                      "engine Id%s\n", pu1EngineId);
        MemReleaseMemBlock (gu4IkeDsaMemPoolId, (UINT1 *) pIkeDsaKeyInfo);
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    IKE_STRCPY (pIkeDsaKeyInfo->au1EngineName, pIkeEngine->au1EngineName);
    SLL_ADD (&(pIkeEngine->IkeDsaKeyList), (t_SLL_NODE *) pIkeDsaKeyInfo);
    GIVE_IKE_SEM ();
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeDsaGetKeyLength                                 */
/*  Description     :This function is used to calculate the key length  */
/*  Input(s)        :pIkeEngine  : Pointer to Ike Engine                */
/*                  :pCertInfo   : Pointer to the Certificate Info      */
/*  Output(s)       :u2KeySize   : Dsa Key Size                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDsaGetKeyLength (tIkeEngine * pIkeEngine, tIkeCertInfo * pCertInfo,
                    UINT4 *u4KeySize)
{
    tIkeDSA            *pIkeDSA = NULL;
    tIkeDsaKeyInfo     *pDsaKey = NULL;
    tIkePKey           *pKey = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All certificate related information stored only in 
     * the DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    pDsaKey = IkeGetDSAKeyWithCertInfo (pIkeDummyEngine, pCertInfo);
    if (pDsaKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "Ike2ConstructAuth: IkeGetDSAKeyWithCertInfo fails\n");
        return (IKE_FAILURE);
    }

    pKey = &(pDsaKey->DsaKey);
    pIkeDSA = EVP_PKEY_get1_DSA (pKey);
    if (pIkeDSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaGetKeyLength:  Error getting DSA" "\n");
        return (IKE_FAILURE);
    }
    *u4KeySize = (UINT4) DSA_size (pIkeDSA);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeDsaSign                                         */
/*  Description     :This function is used to encrypt the hash data     */
/*                  :with the private key, that is used for signing the */
/*                  :data.                                              */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :ppu1OutBuf  : Address of the pointer to output     */
/*                               : buffer                               */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing DSA key                   */
/*                  :u2InBufLen  : Length of data in the iput buffer    */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDsaSign (UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **ppu1OutBuf,
            UINT4 *pu4EncrDataLen, tIkePKey * pKey, UINT4 *pu4ErrNo)
{
    tIkeDSA            *pIkeDSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;
    INT4                i4Retval = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;

    pIkeDSA = EVP_PKEY_get1_DSA (pKey);
    if (pIkeDSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaSign:  Error getting DSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_DSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) DSA_size (pIkeDSA);
    u4Bits = (UINT4) IKE_BYTES2BITS (u2KeySize);

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((u4Bits == DSA_KEY_512) ||
            (u4Bits == DSA_KEY_768) || (u4Bits == DSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "DSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1OutBuf)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDsaSign: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1OutBuf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaSign: Failed to allocate memory " "\n");
        *pu4ErrNo = IKE_ERR_MEM_ALLOC;
        return IKE_FAILURE;
    }

    i4Retval = DSA_sign (NID_sha1, pu1InBuf, u2InBufLen,
                         *ppu1OutBuf, (unsigned int *) pu4EncrDataLen, pIkeDSA);
    if (i4Retval == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaSign: Failed to sign data " "\n");
        *pu4ErrNo = IKE_ERR_DSA_SIGN_FAILED;
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   :IkeDsaVerify                                       */
/*  Description     :This function is used to verify the Auth digest    */
/*                  :sent by the peer.                                  */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :pu1SigBuf   : Pointer to Signature buffer          */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing DSA key                   */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDsaVerify (UINT1 *pu1InBuf, UINT1 *pu1SigBuf, tIkePKey * pKey,
              UINT4 *pu4ErrNo)
{
    tIkeDSA            *pIkeDSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;
    INT4                i4Retval = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;

    pIkeDSA = EVP_PKEY_get1_DSA (pKey);
    if (pIkeDSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaVerify:  Error getting DSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_DSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) DSA_size (pIkeDSA);
    u4Bits = (UINT4) IKE_BYTES2BITS (u2KeySize);

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((u4Bits == DSA_KEY_512) ||
            (u4Bits == DSA_KEY_768) || (u4Bits == DSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "DSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    i4Retval = DSA_verify (NID_sha1, pu1InBuf, IKE_SHA_LENGTH,
                           pu1SigBuf, u2KeySize, (DSA *) pIkeDSA);
    if (i4Retval == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDsaVerify: Failed to verify the signature data" "\n");
        *pu4ErrNo = IKE_ERR_DSA_PUB_DECRYPT_FAILED;
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteDsaCertFromDB                             */
/*  Description     :This function is used to delete key and certificate*/
/*                  :from key table and cert DB respectivley            */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*                  :pu1KeyId      : Key Identifier                     */
/*                  :ppCAName      : Address of the pointer to CA name  */
/*                                 : structure                          */
/*                  :ppCASerialNum : Address of the pointer to cert     */
/*                                 : serial number structure            */
/*                  :pu1ReturnInfo : Return back info to CLI            */
/*  Output(s)       :pu4ErrNo      : Error in case of failure           */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeDeleteDsaCertFromDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                        UINT4 *pu4ErrNo, tIkeX509Name ** ppCAName,
                        tIkeASN1Integer ** ppCASerialNum, UINT1 *pu1ReturnInfo)
{
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;
    tIkeCertDB         *pIkeCertDB = NULL;
    INT1                i1RetVal = IKE_ZERO;

    pIkeDsaKeyInfo = IkeGetDsaKeyEntry (pu1EngineId, pu1KeyId);
    if (pIkeDsaKeyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteDsaCertFromDB:No Key Info Exist" "\n");
        *pu4ErrNo = IKE_ERR_NO_KEY_INFO_FOR_KEY_ID;
        return IKE_FAILURE;
    }

    if (pIkeDsaKeyInfo->CertInfo.pCAName != NULL)
    {
        *pu1ReturnInfo = CERT_EXIST;
        *ppCAName = IkeX509NameDup (pIkeDsaKeyInfo->CertInfo.pCAName);
        *ppCASerialNum =
            IkeASN1IntegerDup (pIkeDsaKeyInfo->CertInfo.pCASerialNum);
        pIkeCertDB =
            IkeGetCertFromMyCerts (pu1EngineId,
                                   pIkeDsaKeyInfo->CertInfo.pCAName,
                                   pIkeDsaKeyInfo->CertInfo.pCASerialNum);
        if (pIkeCertDB != NULL)
        {
            i1RetVal = IkeDeleteCertInMyCerts (pu1EngineId, pIkeCertDB);
            if (i1RetVal == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeDeleteDsaCertFromDB:Failed to Delete"
                             "Certificate from DB\n");
                *pu4ErrNo = IKE_ERR_DEL_CERT_FAILED;
                return IKE_FAILURE;
            }
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteDsaCertFromDB:Unable to get Certificate from"
                         "CertDB for cert info\n");
            *pu1ReturnInfo = NO_CERT_EXIST;
            *pu4ErrNo = IKE_ERR_CERTDB_UNUPDATED;
            return IKE_FAILURE;
        }

    }
    else
    {
        *pu1ReturnInfo = NO_CERT_EXIST;
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteDsaCertFromDB:Unable to get Certificate from"
                     "CertDB\n");
    }
    return IKE_SUCCESS;
}
