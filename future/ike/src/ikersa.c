/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikersa.c,v 1.17 2014/10/29 12:52:52 siva Exp $
 *
 * Description: This file has RSA wrapper functions.  
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :IkeRsaGenKeyPair                                   */
/*  Description     :This function is used to generate RSA key pair     */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify whether the key is already existing or */
/*                  :not.If not it will generate the new key and add    */
/*                  :the key to the database.                           */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :u2NBits     : Length of the key to be generated    */
/*                  :pu4ErrNo    : Return Error Number                  */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeRsaGenKeyPair (UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT2 u2NBits,
                  UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;

    if (IkeVerifyKeyId (pu1EngineId, pu1KeyId) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaGenKeyPair:Key Id  already exist in"
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_KEY_ID_ALREADY_EXIST;
        return IKE_FAILURE;
    }

    pIkeRSA = RSA_generate_key (u2NBits, RSA_F4, NULL, NULL);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaGenKeyPair: Key Generation failed" "\n");
        *pu4ErrNo = IKE_ERR_KEY_GEN_FAILED;
        return IKE_FAILURE;
    }

    if (IkeAddRsaKeyEntryToDB (pu1EngineId, pu1KeyId, pIkeRSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaGenKeyPair:Failed to add entry in "
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_ADD_TO_DB_FAILED;
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeImportRsaKey                                    */
/*  Description     :This function is used to Import RSA key            */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify whether the key is already existing or */
/*                  :not.If not it will import the new key and add      */
/*                  :the key to the database.                           */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :pu4ErrNo    : Return Error Number                  */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeImportRsaKey (UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT1 *pu1FileName,
                 UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;
    BIO                *pBp = NULL;
    INT4                i4RetVal = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;
    signed char         pu1ReadFileName[IKE_MAX_PATH_SIZE] = { IKE_ZERO };

    if (IkeVerifyKeyId (pu1EngineId, pu1KeyId) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportRsaKey:Key Id  already exist in"
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_KEY_ID_ALREADY_EXIST;
        return IKE_FAILURE;
    }

    IKE_SNPRINTF (((CHR1 *) pu1ReadFileName), IKE_MAX_PATH_SIZE, "%s/%s",
                  IKE_DEF_USER_DIR, pu1FileName);

    pBp = BIO_new (BIO_s_file ());
    if (pBp == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportRsaKey: Call to BIO_new fails\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     pu1FileName, pu1ReadFileName);
    i4RetVal = BIO_read_filename (pBp, pu1ReadFileName);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pBp);
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportRsaKey: Call to BIO_read_filename fails"
                      "for %s\n", pu1ReadFileName);
        *pu4ErrNo = IKE_ERR_FILE_NOT_EXIST;
        return IKE_FAILURE;
    }

    if ((pIkeRSA =
         (tIkeRSA *) PEM_read_bio_RSAPrivateKey (pBp, NULL, NULL, NULL))
        == NULL)
    {
        BIO_free_all (pBp);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportRsaKey: Call to\
                     PEM_read_bio_RSAPrivateKey fails\n");
        *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
        return IKE_FAILURE;

    }
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        u4Bits = (UINT4) IKE_BYTES2BITS (RSA_size (pIkeRSA));
        if ((u4Bits == RSA_KEY_512) || (u4Bits == RSA_KEY_768)
            || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    if (IkeAddRsaKeyEntryToDB (pu1EngineId, pu1KeyId, pIkeRSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportRsaKey: Failed to add entry in "
                     "the key database\n");
        *pu4ErrNo = IKE_ERR_ADD_TO_DB_FAILED;
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeVerifyKeyId                                     */
/*  Description     :This function is used to verify if the keys with   */
/*                  :the same key id already exist in the database. If  */
/*                  :yes, the function returns failure otherwise success*/
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeVerifyKeyId (UINT1 *pu1EngineId, UINT1 *pu1KeyId)
{
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;

    pIkeRsaKeyInfo = IkeGetRsaKeyEntry (pu1EngineId, pu1KeyId);
    if (pIkeRsaKeyInfo != NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeVerifyKeyId:Key Id verification failed\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetRsaKeyEntry                                  */
/*  Description     :This function searches the key database for the    */
/*                  :given key id . If found it will return the key info*/
/*                  :otherwise returns NULL                             */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to tIkeRsaKeyInfo or NULL                  */
/************************************************************************/

tIkeRsaKeyInfo     *
IkeGetRsaKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyId)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeVerifyKeyId:Failed to get engine with "
                      "engine Id%s\n", pu1EngineId);
        GIVE_IKE_SEM ();
        return NULL;
    }
    SLL_SCAN (&(pIkeEngine->IkeRsaKeyList), pIkeRsaKeyInfo, tIkeRsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeRsaKeyInfo->au1RsaKeyName, pu1KeyId, IKE_STRLEN (pu1KeyId) + 1)
            == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pIkeRsaKeyInfo;
        }
    }
    GIVE_IKE_SEM ();
    return pIkeRsaKeyInfo;

}

/************************************************************************/
/*  Function Name   :IkeAddRsaKeyEntryToDB                              */
/*  Description     :This function is used to add key entry for the new */
/*                  :ly generated key to the Engine structure database. */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :pIkeRSA     : Pointer to RSA key structure         */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeAddRsaKeyEntryToDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId, tIkeRSA * pIkeRSA)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;

    if (MemAllocateMemBlock
        (IKE_RSAKEY_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeRsaKeyInfo) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAddRsaKeyEntryToDB: unable to allocate " "buffer\n");
        return (IKE_FAILURE);
    }
    if (pIkeRsaKeyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddRsaKeyToIkeEngine: Memory allocation failed "
                     "for RSA key info \n");
        return (IKE_FAILURE);
    }

    IKE_BZERO (pIkeRsaKeyInfo, sizeof (tIkeRsaKeyInfo));
    IKE_STRCPY (pIkeRsaKeyInfo->au1RsaKeyName, pu1KeyId);
    pIkeRsaKeyInfo->RsaKey.pkey.rsa = pIkeRSA;
    pIkeRsaKeyInfo->RsaKey.type = EVP_PKEY_RSA;
    TAKE_IKE_SEM ();

    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeAddRsaKeyToKeyTable:Failed to get engine with "
                      "engine Id%s\n", pu1EngineId);
        MemReleaseMemBlock (IKE_RSAKEY_MEMPOOL_ID, (UINT1 *) pIkeRsaKeyInfo);
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    IKE_STRCPY (pIkeRsaKeyInfo->au1EngineName, pIkeEngine->au1EngineName);
    SLL_ADD (&(pIkeEngine->IkeRsaKeyList), (t_SLL_NODE *) pIkeRsaKeyInfo);
    GIVE_IKE_SEM ();
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeRsaPrivateEncrypt                               */
/*  Description     :This function is used to encrypt the hash data     */
/*                  :with the private key, that is used for signing the */
/*                  :data.                                              */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :ppu1OutBuf  : Address of the pointer to output     */
/*                               : buffer                               */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing RSA key                   */
/*                  :u2InBufLen  : Length of data in the iput buffer    */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeRsaPrivateEncrypt (UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **ppu1OutBuf,
                      INT4 *pi4EncrDataLen, tIkePKey * pKey, UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;

    pIkeRSA = EVP_PKEY_get1_RSA (pKey);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPrivateEncrypt:  Error getting RSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_RSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) RSA_size (pIkeRSA);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1OutBuf)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeRSAPrivateEncrypt: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1OutBuf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPrivateEncrypt: Failed to allocate memory " "\n");
        *pu4ErrNo = IKE_ERR_MEM_ALLOC;
        return IKE_FAILURE;
    }
    *pi4EncrDataLen = RSA_private_encrypt (u2InBufLen, pu1InBuf, *ppu1OutBuf,
                                           pIkeRSA, RSA_PKCS1_PADDING);
    if (*pi4EncrDataLen <= IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPrivateEncrypt: Failed to sign data " "\n");
        *pu4ErrNo = IKE_ERR_RSA_SIGN_FAILED;
        return IKE_FAILURE;
    }
    UNUSED_PARAM (u2KeySize);
    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   :IkeRsaPublicDecrypt                                */
/*  Description     :This function is used to decrypt the data encrypted*/
/*                  :using the private key.                            */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :ppu1OutBuf  : Address of the pointer to output     */
/*                               : buffer                               */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing RSA key                   */
/*                  :u2InBufLen  : Length of data in the iput buffer    */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeRsaPublicDecrypt (UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT1 **ppu1OutBuf,
                     INT4 *pi4DecryptDataLen, tIkePKey * pKey, UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;

    pIkeRSA = EVP_PKEY_get1_RSA (pKey);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPublicDecrypt:  Error getting RSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_RSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) RSA_size (pIkeRSA);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1OutBuf)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeRSAPublicDecrypt: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1OutBuf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPublicDecrypt: Failed to allocate memory " "\n");
        *pu4ErrNo = IKE_ERR_MEM_ALLOC;
        return IKE_FAILURE;
    }
    *pi4DecryptDataLen = RSA_public_decrypt (u2InBufLen, pu1InBuf, *ppu1OutBuf,
                                             pIkeRSA, RSA_PKCS1_PADDING);
    if (*pi4DecryptDataLen <= IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRSAPublicDecrypt: Failed to sign data " "\n");
        *pu4ErrNo = IKE_ERR_RSA_PUB_DECRYPT_FAILED;
        return IKE_FAILURE;
    }
    UNUSED_PARAM (u2KeySize);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeRsaGetKeyLength                                 */
/*  Description     :This function is used to calculate the key length  */
/*  Input(s)        :pIkeEngine  : Pointer to Ike Engine                */
/*                  :pCertInfo   : Pointer to the Certificate Info      */
/*  Output(s)       :u2KeySize   : Rsa Key Size                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeRsaGetKeyLength (tIkeEngine * pIkeEngine, tIkeCertInfo * pCertInfo,
                    UINT4 *u4KeySize)
{
    tIkeRSA            *pIkeRSA = NULL;
    tIkeRsaKeyInfo     *pRsaKey = NULL;
    tIkePKey           *pKey = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All certificate related information stored only in 
     * the DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    pRsaKey = IkeGetRSAKeyWithCertInfo (pIkeEngine, pCertInfo);
    if (pRsaKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "Ike2ConstructAuth: IkeGetRSAKeyWithCertInfo fails\n");
        return (IKE_FAILURE);
    }

    pKey = &(pRsaKey->RsaKey);
    pIkeRSA = EVP_PKEY_get1_RSA (pKey);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaGetKeyLength:  Error getting RSA" "\n");
        return (IKE_FAILURE);
    }
    *u4KeySize = (UINT4) RSA_size (pIkeRSA);
    UNUSED_PARAM (pIkeDummyEngine);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeRsaSign                                         */
/*  Description     :This function is used to encrypt the hash data     */
/*                  :with the private key, that is used for signing the */
/*                  :data.                                              */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :ppu1OutBuf  : Address of the pointer to output     */
/*                               : buffer                               */
/*                  :u4SignAlgo  : Hashing algorithm type               */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing RSA key                   */
/*                  :u2InBufLen  : Length of data in the iput buffer    */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeRsaSign (UINT1 *pu1InBuf, UINT2 u2InBufLen, UINT4 u4SignAlgo,
            UINT1 **ppu1OutBuf, UINT4 *pu4EncrDataLen,
            tIkePKey * pKey, UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;
    INT4                i4Retval = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;
    CONST EVP_MD       *pEvp_md = NULL;
    EVP_MD_CTX          mctx;

    pIkeRSA = EVP_PKEY_get1_RSA (pKey);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaSign:  Error getting RSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_RSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) RSA_size (pIkeRSA);
    u4Bits = (UINT4) IKE_BYTES2BITS (u2KeySize);

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((u4Bits == RSA_KEY_512) || (u4Bits == RSA_KEY_768)
            || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1OutBuf)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeRsaSign:: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1OutBuf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaSign: Failed to allocate memory " "\n");
        *pu4ErrNo = IKE_ERR_MEM_ALLOC;
        return IKE_FAILURE;
    }

    if (IKE_TRUE == !FIPS_mode ())
    {
        i4Retval = RSA_sign ((INT4) u4SignAlgo, pu1InBuf, u2InBufLen,
                             *ppu1OutBuf, (unsigned int *) pu4EncrDataLen,
                             pIkeRSA);
    }
    else
    {
        if ((pEvp_md = EVP_get_digestbynid ((INT4) u4SignAlgo)) == NULL)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                          "EVP_get_digestbynid %d failed", u4SignAlgo);
            return IKE_FAILURE;
        }

        EVP_MD_CTX_init (&mctx);
        i4Retval = IKE_ONE;
        if (IKE_TRUE == !EVP_SignInit_ex (&mctx, pEvp_md, NULL))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaSign: EVP_SignInit_ex failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            *pu4ErrNo = IKE_ERR_RSA_PUB_DECRYPT_FAILED;
            return IKE_FAILURE;
        }
        if (IKE_TRUE == !EVP_SignUpdate (&mctx, pu1InBuf, u2InBufLen))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaSign: EVP_SignUpdate failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            *pu4ErrNo = IKE_ERR_RSA_PUB_DECRYPT_FAILED;
            return IKE_FAILURE;
        }
        if (IKE_TRUE == !EVP_SignFinal
            (&mctx, *ppu1OutBuf, pu4EncrDataLen, pKey))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaSign: EVP_SignFinal failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            *pu4ErrNo = IKE_ERR_RSA_PUB_DECRYPT_FAILED;
            return IKE_FAILURE;
        }
        EVP_MD_CTX_cleanup (&mctx);
    }
    if (i4Retval == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaSign: Failed to sign data " "\n");
        *pu4ErrNo = IKE_ERR_RSA_SIGN_FAILED;
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   :IkeRsaVerify                                       */
/*  Description     :This function is used to verify the Auth digest    */
/*                  :sent by the peer.                                  */
/*  Input(s)        :pu1InBuf    : Pointer to Input Data buffer         */
/*                  :pu1SigBuf   : Pointer to Signature buffer          */
/*                  :tIkePKey    : Pointer to EVP_PKEY structure        */
/*                               : containing RSA key                   */
/*                  :u4SignAlgo  : Hashing algorithm type               */
/*  Output(s)       :pu4ErrNo :Error Number in case of failure          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeRsaVerify (UINT1 *pu1InBuf, UINT1 *pu1SigBuf, tIkePKey * pKey,
              UINT4 u4SignAlgo, UINT4 u4DigestLen, UINT4 *pu4ErrNo)
{
    tIkeRSA            *pIkeRSA = NULL;
    UINT2               u2KeySize = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;
    INT4                i4Retval = IKE_ZERO;
    CONST EVP_MD       *pEvp_md = NULL;
    EVP_MD_CTX          mctx;

    pIkeRSA = EVP_PKEY_get1_RSA (pKey);
    if (pIkeRSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaVerify:  Error getting RSA" "\n");
        *pu4ErrNo = IKE_ERR_GET_RSA_FAILED;
        return IKE_FAILURE;
    }
    u2KeySize = (UINT2) RSA_size (pIkeRSA);
    u4Bits = (UINT4) IKE_BYTES2BITS (u2KeySize);

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((u4Bits == RSA_KEY_512) || (u4Bits == RSA_KEY_768)
            || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    if (IKE_TRUE == !FIPS_mode ())
    {
        i4Retval = RSA_verify ((INT4) u4SignAlgo, pu1InBuf, u4DigestLen,
                               pu1SigBuf, u2KeySize, (RSA *) pIkeRSA);
    }
    else
    {
        if ((pEvp_md = EVP_get_digestbynid ((INT4) u4SignAlgo)) == NULL)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                          "IkeRsaVerify:EVP_get_digestbynid %d failed",
                          u4SignAlgo);
            return IKE_FAILURE;
        }

        EVP_MD_CTX_init (&mctx);
        i4Retval = IKE_ONE;
        if (IKE_TRUE == !EVP_VerifyInit_ex (&mctx, pEvp_md, NULL))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaVerify: EVP_SignInit_ex failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            i4Retval = IKE_ZERO;
        }
        if (IKE_TRUE == !EVP_VerifyUpdate (&mctx, pu1InBuf, u4DigestLen))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaVerify: EVP_SignUpdate failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            i4Retval = IKE_ZERO;
        }
        if (IKE_TRUE == !EVP_VerifyFinal (&mctx, pu1SigBuf, u2KeySize, pKey))
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeRsaVerify: EVP_SignFinal failed.\n");
            EVP_MD_CTX_cleanup (&mctx);
            i4Retval = IKE_ZERO;
        }
        EVP_MD_CTX_cleanup (&mctx);
        /* since all the init, update and final succeeded, setting ok as 1 */
    }
    if (i4Retval == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeRsaVerify: Failed to verify the signature data" "\n");
        *pu4ErrNo = IKE_ERR_RSA_PUB_DECRYPT_FAILED;
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteKeyPair                                   */
/*  Description     :This function is used to delete key pair based on  */
/*                  :key id .If no key id is provided it deletes all    */
/*                  :key and certificates                               */
/*  Input(s)        :pu1EngineId : Engine Identifier                    */
/*                  :pu1KeyId    : Key Identifier                       */
/*                  :tIkeX509Name: Pointer to CA Name structure         */
/*                  :tIkeASN1Integer: Pointer to structure containing   */
/*                                  : serial number info                */
/*  Output(s)       :pu4ErrNo :Error number in case of failure          */
/*                  :pu1ReturnInfo:Return info for IKE whether to delete*/
/*                  :all certificates from DB or any particular cert    */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeDeleteKeyPair (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                  tIkeX509Name ** ppCAName,
                  tIkeASN1Integer ** ppCASerialNum,
                  UINT1 *pu1ReturnInfo, UINT4 u4EncrType, UINT4 *pu4ErrNo)
{
    INT4                i4RetValue = IKE_FAILURE;

    if (pu1KeyId != NULL)
    {
        if (u4EncrType == IKE_RSA_SIGNATURE)
        {
            i4RetValue = IkeDeleteRsaCertFromDB (pu1EngineId, pu1KeyId,
                                                 pu4ErrNo, ppCAName,
                                                 ppCASerialNum, pu1ReturnInfo);
        }
        else
        {
            i4RetValue = IkeDeleteDsaCertFromDB (pu1EngineId, pu1KeyId,
                                                 pu4ErrNo, ppCAName,
                                                 ppCASerialNum, pu1ReturnInfo);
        }
        if (i4RetValue == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyPair:Failed to Delete"
                         "Certificate from Database\n");
            return IKE_FAILURE;
        }

        i4RetValue = IkeDeleteKeyEntryFromDB (pu1EngineId, pu1KeyId,
                                              u4EncrType, pu4ErrNo);
        if (i4RetValue == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyPair:*Unable to delete key entry from"
                         "database\n");
            *pu4ErrNo = IKE_ERR_DEL_KEY_INFO_FAILED;
            return IKE_FAILURE;
        }

    }
    else
    {
        *pu1ReturnInfo = DEL_ALL_CERT_SA;

        i4RetValue = IkeDeleteAllKeyInfoFromDB (pu1EngineId, u4EncrType);
        if (i4RetValue == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyPair:Failed to Delete Keys" "\n");
            *pu4ErrNo = IKE_ERR_DEL_ALL_KEY_INFO;
            return IKE_FAILURE;
        }

        i4RetValue = IkeDelAllMyCerts (pu1EngineId);
        if (i4RetValue == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyPair:Failed to Delete Certs" "\n");
            *pu4ErrNo = IKE_ERR_DEL_ALL_MY_CERTS;
            return IKE_FAILURE;
        }

    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteAllKeyInfoFromDB                          */
/*  Description     :This function is used to delete all keys from      */
/*                  :the key table.                                     */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeDeleteAllKeyInfoFromDB (UINT1 *pu1EngineId, UINT4 u4EncrType)
{
    tIkeEngine         *pIkeEngine = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDeleteAllKeyInfoFromDB:Failed to get engine with"
                      "engine Id % s \n", pu1EngineId);
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    if (u4EncrType == IKE_RSA_SIGNATURE)
    {
        SLL_DELETE_LIST (&(pIkeEngine->IkeRsaKeyList), IkeFreeRsaKeyInfo);
    }
    else
    {
        SLL_DELETE_LIST (&(pIkeEngine->IkeDsaKeyList), IkeFreeDsaKeyInfo);
    }
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteKeyEntryFromDB                            */
/*  Description     :This function is used to delete particular key     */
/*                  :entry from key table.                              */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*                  :pu1KeyId      : Key Identifier                     */
/*  Output(s)       :pu4ErrNo      : Returns error value                */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeDeleteKeyEntryFromDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                         UINT4 u4EncrType, UINT4 *pu4ErrNo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteKeyEntryFromDB:Unable to get Engine from"
                     "Engine ID\n");
        *pu4ErrNo = IKE_ERR_NO_ENGINE_WITH_ID;
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    if (u4EncrType == IKE_RSA_SIGNATURE)
    {
        pIkeRsaKeyInfo = IkeGetRsaKeyEntry (pu1EngineId, pu1KeyId);
        if (pIkeRsaKeyInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyEntryFromDB:No Key Info Exist" "\n");
            *pu4ErrNo = IKE_ERR_NO_KEY_INFO_FOR_KEY_ID;
            return IKE_FAILURE;
        }
        TAKE_IKE_SEM ();
        SLL_DELETE (&(pIkeEngine->IkeRsaKeyList),
                    (t_SLL_NODE *) pIkeRsaKeyInfo);
        IkeFreeRsaKeyInfo ((t_SLL_NODE *) pIkeRsaKeyInfo);
        GIVE_IKE_SEM ();
    }
    else
    {
        pIkeDsaKeyInfo = IkeGetDsaKeyEntry (pu1EngineId, pu1KeyId);
        if (pIkeDsaKeyInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteKeyEntryFromDB:No Key Info Exist" "\n");
            *pu4ErrNo = IKE_ERR_NO_KEY_INFO_FOR_KEY_ID;
            return IKE_FAILURE;
        }
        TAKE_IKE_SEM ();
        SLL_DELETE (&(pIkeEngine->IkeDsaKeyList),
                    (t_SLL_NODE *) pIkeDsaKeyInfo);
        IkeFreeDsaKeyInfo ((t_SLL_NODE *) pIkeDsaKeyInfo);
        GIVE_IKE_SEM ();
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteRsaCertFromDB                             */
/*  Description     :This function is used to delete key and certificate*/
/*                  :from key table and cert DB respectivley            */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*                  :pu1KeyId      : Key Identifier                     */
/*  Input(s)        :ppCAName      : Address of the pointer to CA name  */
/*                                 : structure                          */
/*  Input(s)        :ppCASerialNum : Address of the pointer to cert     */
/*                                 : serial number structure            */
/*                  :pu1ReturnInfo : Return back info to CLI            */
/*  Output(s)       :pu4ErrNo      : Error in case of failure           */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/
INT1
IkeDeleteRsaCertFromDB (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                        UINT4 *pu4ErrNo, tIkeX509Name ** ppCAName,
                        tIkeASN1Integer ** ppCASerialNum, UINT1 *pu1ReturnInfo)
{
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;
    tIkeCertDB         *pIkeCertDB = NULL;
    INT1                i1RetVal = IKE_ZERO;

    pIkeRsaKeyInfo = IkeGetRsaKeyEntry (pu1EngineId, pu1KeyId);
    if (pIkeRsaKeyInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteRsaCertFromDB:No Key Info Exist" "\n");
        *pu4ErrNo = IKE_ERR_NO_KEY_INFO_FOR_KEY_ID;
        return IKE_FAILURE;
    }

    if (pIkeRsaKeyInfo->CertInfo.pCAName != NULL)
    {
        *pu1ReturnInfo = CERT_EXIST;
        *ppCAName = IkeX509NameDup (pIkeRsaKeyInfo->CertInfo.pCAName);
        *ppCASerialNum =
            IkeASN1IntegerDup (pIkeRsaKeyInfo->CertInfo.pCASerialNum);
        pIkeCertDB =
            IkeGetCertFromMyCerts (pu1EngineId,
                                   pIkeRsaKeyInfo->CertInfo.pCAName,
                                   pIkeRsaKeyInfo->CertInfo.pCASerialNum);
        if (pIkeCertDB != NULL)
        {
            i1RetVal = IkeDeleteCertInMyCerts (pu1EngineId, pIkeCertDB);
            if (i1RetVal == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeDeleteRsaCertFromDB:Failed to Delete"
                             " Certificate from DB\n");
                *pu4ErrNo = IKE_ERR_DEL_CERT_FAILED;
                return IKE_FAILURE;
            }
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteRsaCertFromDB:Unable to get Certificate "
                         "from CertDB for cert info\n");
            *pu1ReturnInfo = NO_CERT_EXIST;
            *pu4ErrNo = IKE_ERR_CERTDB_UNUPDATED;
            return IKE_FAILURE;
        }

    }
    else
    {
        *pu1ReturnInfo = NO_CERT_EXIST;
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteRsaCertFromDB:Unable to get Certificate from"
                     " CertDB\n");
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteCertMapFromDB                             */
/*  Description     :This function is used to delete cert map structure */
/*                  :from the IkeCertMap list.                          */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*  Input(s)        :pCANameByKey  : Pointer to CAName obtained from    */
/*                                 : Cert info structure based on key id*/
/*                  :pCASerialNumBy: Pointer to CASerialNumber obtained */
/*                   Key           : from cert info structure based on  */
/*                                 : Key Id.                            */
/*  Output(s)       :Deletes cert map structure from the list           */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeDeleteCertMapFromDB (UINT1 *pu1EngineId, tIkeX509Name * pCAName,
                        tIkeASN1Integer * pCASerialNum)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertMapToPeer  *pIkeCertMap = NULL;
    UINT1               u1RetInfo = IKE_ZERO;

    pIkeCertMap = IkeGetCertMapFromDB (pu1EngineId, pCAName, pCASerialNum,
                                       &u1RetInfo);
    if (pIkeCertMap == NULL)
    {
        if (u1RetInfo == NO_CERT_MAP_INFO)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteCertMapFromDB:No cert map info"
                         "available\n");
            return IKE_SUCCESS;
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDeleteCertMapFromDB:Failed to get cert map"
                         "from database:\n");
            return IKE_FAILURE;
        }
    }
    TAKE_IKE_SEM ();

    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDeleteCertMapFromDB:Failed to get engine with"
                      "engine id:%s\n", pu1EngineId);
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    SLL_DELETE (&(pIkeEngine->IkeCertMapList), (t_SLL_NODE *) pIkeCertMap);
    IkeFreeCertMapInfo ((t_SLL_NODE *) pIkeCertMap);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetCertMapFromDB                                */
/*  Description     :This function is used to get cert map entry based  */
/*                  :on CAName and CAserial number.                     */
/*  Input(s)        :pu1EngineId   : Engine Identifier                  */
/*  Input(s)        :pCANameByKey  : Pointer to CAName obtained from    */
/*                                 : Cert info structure based on key id*/
/*                  :pCASerialNumBy: Pointer to CASerialNumber obtained */
/*                   Key           : from cert info structure based on  */
/*                                 : Key Id.                            */
/*  Output(s)       :None                                               */
/*  Returns         :Returns Cert Map entry on success or NULL otherwise*/
/************************************************************************/

tIkeCertMapToPeer  *
IkeGetCertMapFromDB (UINT1 *pu1EngineId, tIkeX509Name * pCAName,
                     tIkeASN1Integer * pCASerialNum, UINT1 *pu1RetInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertMapToPeer  *pIkeCertMap = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGetCertMapFromDB:Failed to get cert map"
                     "from database:\n");
        GIVE_IKE_SEM ();
        return pIkeCertMap;
    }

    SLL_SCAN (&(pIkeEngine->IkeCertMapList), pIkeCertMap, tIkeCertMapToPeer *)
    {
        if ((IkeX509NameCmp (pIkeCertMap->MyCert.pCAName, pCAName)
             == IKE_ZERO) &&
            (IkeASN1IntegerCmp (pIkeCertMap->MyCert.pCASerialNum,
                                pCASerialNum) == IKE_ZERO))
        {
            GIVE_IKE_SEM ();
            return pIkeCertMap;
        }
    }
    GIVE_IKE_SEM ();
    *pu1RetInfo = NO_CERT_MAP_INFO;
    return pIkeCertMap;
}
