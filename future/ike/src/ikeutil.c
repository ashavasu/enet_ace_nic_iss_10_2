/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ikeutil.c,v 1.32 2014/07/16 12:47:03 siva Exp $
 * 
 * Description:This file contains utility functions for ike
 * 
 ********************************************************************/

#include "ikegen.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "midconst.h"
#include "fsikelow.h"
#include "npapi.h"
#include "cli.h"
#include "ikecertsave.h"
#include "ikedsa.h"
#include "cust.h"
#include "msr.h"
#include "utilrand.h"
#include "ikememmac.h"

#define  IKE_FIXED_INNER_PAD  0x36
#define  IKE_FIXED_OUTER_PAD  0x5c

extern INT4         VpnIkeCheckAndDeleteEngine (UINT4 u4IfIndex);
extern INT4         IkeDeleteEngineContents (UINT1 *pu1EngineName);
extern INT4         IkeCreateEngine (UINT1 *);
extern tTMO_SLL     gVpnRemoteIdList;
unIkeCallBackEntry  gaIkeCallBack[IKE_MAX_CALLBACK_EVENTS];

UINT1               gu1IkeBypassCrypto = BYPASS_DISABLED;

/************************************************************************/
/*  Function Name   :IkeSetPhase1Key                                    */
/*  Description     :This function is used to set the DES/3DES keys     */
/*                  :using the Key Material                             */
/*  Input(s)        :pSessionInfo - Pointer to the SessionInfo          */
/*                  :structure                                          */
/*                  :pu1KeyMat - The Key Material                       */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/
INT1
IkeSetPhase1Key (tIkeSessionInfo * pSessionInfo, UINT1 *pu1KeyMat)
{

    if (pu1KeyMat == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetPhase1Key: IkeSetPhase1Key Failure \n");
        return IKE_FAILURE;

    }

    switch (pSessionInfo->pIkeSA->u1Phase1EncrAlgo)
    {
        case IKE_DES_CBC:
        {
            if (IkeDesIsWeakKey (pu1KeyMat) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase1Key: Weak key generated\n");
                return IKE_FAILURE;
            }

            IkeDesSetKey (pu1KeyMat, &pSessionInfo->pIkeSA->DesKey1);
            break;
        }

        case IKE_3DES_CBC:
        {
            if (IkeDesIsWeakKey (pu1KeyMat) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase1Key: Weak key generated\n");
                return IKE_FAILURE;
            }

            if (IkeDesIsWeakKey (pu1KeyMat + IKE_DES_KEY_LEN) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase1Key: Weak key generated\n");
                return IKE_FAILURE;
            }
            if (IkeDesIsWeakKey
                (pu1KeyMat + IKE_TWO * IKE_DES_KEY_LEN) == IKE_TRUE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSetPhase1Key: Weak key generated\n");
                return IKE_FAILURE;
            }

            IkeTripleDesSetKey (pu1KeyMat, &pSessionInfo->pIkeSA->DesKey1,
                                &pSessionInfo->pIkeSA->DesKey2,
                                &pSessionInfo->pIkeSA->DesKey3);
            break;
        }

        case IKE_AES:
        {
            IkeAesSetKey (pu1KeyMat, (UINT1) pSessionInfo->pIkeSA->u2EncrKLen,
                          (pSessionInfo->pIkeSA->au1AesEncrKey),
                          (pSessionInfo->pIkeSA->au1AesDecrKey));
            break;
        }
        default:
            break;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGenIPSecKeys 
 *  Description   : Function to generate key Material for IPSec
 *  Input(s)      : pSessionInfo - Pointer to session info. 
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE 
 * ---------------------------------------------------------------
 */

INT1
IkeGenIPSecKeys (tIkeSessionInfo * pSessionInfo)
{
    tIPSecSA           *pIPSecSA = NULL;

    /* Generate the IPSec Keys */
    pIPSecSA = &pSessionInfo->IkePhase2Info.IPSecBundle.
        aInSABundle[IKE_ESP_SA_INDEX];
    if (pIPSecSA->u1IPSecProtocol != IKE_ZERO)
    {
        if (IkeGenIPSecKeyMat (pSessionInfo, pIPSecSA) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenIPSecKeys: Generate Key for Inbound SA"
                         "for ESP protocol failed\n");
            return IKE_FAILURE;
        }
    }

    pIPSecSA = &pSessionInfo->IkePhase2Info.IPSecBundle.
        aOutSABundle[IKE_ESP_SA_INDEX];
    if (pIPSecSA->u1IPSecProtocol != IKE_ZERO)
    {
        if (IkeGenIPSecKeyMat (pSessionInfo, pIPSecSA) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenIPSecKeys: Generate Key for Outbound SA"
                         "for ESP protocol failed\n");
            return IKE_FAILURE;
        }
    }

    pIPSecSA = &pSessionInfo->IkePhase2Info.IPSecBundle.
        aInSABundle[IKE_AH_SA_INDEX];
    if (pIPSecSA->u1IPSecProtocol != IKE_ZERO)
    {
        if (IkeGenIPSecKeyMat (pSessionInfo, pIPSecSA) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenIPSecKeys: Generate Key for Inbound SA"
                         "for AH protocol failed\n");
            return IKE_FAILURE;
        }
    }

    pIPSecSA = &pSessionInfo->IkePhase2Info.IPSecBundle.
        aOutSABundle[IKE_AH_SA_INDEX];
    if (pIPSecSA->u1IPSecProtocol != IKE_ZERO)
    {
        if (IkeGenIPSecKeyMat (pSessionInfo, pIPSecSA) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGenIPSecKeys: Generate Key for Outbound SA"
                         "for AH protocol failed\n");
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGetRandom 
 *  Description   : Function to generate random number.
 *  Input(s)      : pu1Ptr - pointer where the random number 
 *                           has to be stored.
 *                  u4Len  - Required length of random number. 
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */

VOID
IkeGetRandom (UINT1 *pu1Ptr, INT4 u4Len)
{
    UINT4               u4Temp = IKE_ZERO;
    UINT1              *pu1Temp = pu1Ptr;

    while (u4Len > IKE_FOUR)
    {
        u4Temp = (UINT4) RAND ();
        IKE_MEMCPY (pu1Temp, &u4Temp, IKE_FOUR);
        pu1Temp += IKE_FOUR;
        u4Len -= IKE_FOUR;
    }

    if (u4Len > IKE_ZERO)
    {
        u4Temp = (UINT4) RAND ();
        IKE_MEMCPY (pu1Temp, &u4Temp, u4Len);
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeGenIV 
 *  Description   : Function to generate initial vector 
 *  Input(s)      : pSessioInfo - Pointer to the buffer
 *                  u1Phase  - Specifies the negotiation 
 *  Output(s)     : Updated pointer for pSessionInfo IV
 *  Return Values : IKE_SUCCESS or IKE_FAILURE
 * ---------------------------------------------------------------
 */
INT4
IkeGenIV (tIkeSessionInfo * pSessionInfo, UINT1 u1Phase)
{
    UINT1              *pu1IVBuf = NULL, *pu1CryptIV = NULL;
    UINT4               u4MsgId = IKE_ZERO,
        u4IVLen = IKE_ZERO, u4IVBufLen = IKE_ZERO, u4HashLen = IKE_ZERO;

    u4MsgId = IKE_HTONL (pSessionInfo->u4MsgId);
    u4IVLen = gaIkeCipherAlgoList[pSessionInfo->pIkeSA->u1Phase1EncrAlgo].
        u4BlockLen;

    if (u4IVLen == IKE_ZERO)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeGenPhase2IV: Unknown Encr Algorithm %d\n",
                      pSessionInfo->pIkeSA->u1Phase1EncrAlgo);
        return IKE_FAILURE;
    }

    if (u1Phase == IKE_PHASE2)
    {
        u4IVBufLen = u4IVLen + sizeof (UINT4);
    }
    else
    {
        u4IVBufLen = (UINT4) (pSessionInfo->pMyDhPub->i4Length +
                              pSessionInfo->pHisDhPub->i4Length);
    }

    /* Allocate buffer for holding the buffer for calculation of IV */
    if (MemAllocateMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1IVBuf)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenPhase2IV: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pu1IVBuf == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeGenPhase2IV: Buffer allocation for IV "
                     "generation FAIL \n");
        return IKE_FAILURE;
    }

    u4HashLen =
        gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].u4KeyLen;

    /* Allocate buffer for holding the hash generated by the hash algorithm */
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1CryptIV) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGenPhase2IV: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IVBuf);
        return IKE_FAILURE;
    }

    if (pu1CryptIV == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeGenPhase2IV: Initial Vector Allocation Fail \n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IVBuf);
        return (IKE_FAILURE);
    }

    if (u1Phase == IKE_PHASE1)
    {
        if (pSessionInfo->u1Role == IKE_INITIATOR)
        {
            IKE_MEMCPY (pu1IVBuf, pSessionInfo->pMyDhPub->pu1Value,
                        pSessionInfo->pMyDhPub->i4Length);
            IKE_MEMCPY (pu1IVBuf + pSessionInfo->pMyDhPub->i4Length,
                        pSessionInfo->pHisDhPub->pu1Value,
                        pSessionInfo->pHisDhPub->i4Length);
        }
        else
        {
            IKE_MEMCPY (pu1IVBuf, pSessionInfo->pHisDhPub->pu1Value,
                        pSessionInfo->pHisDhPub->i4Length);
            IKE_MEMCPY (pu1IVBuf + pSessionInfo->pHisDhPub->i4Length,
                        pSessionInfo->pMyDhPub->pu1Value,
                        pSessionInfo->pMyDhPub->i4Length);
        }
    }
    else if (u1Phase == IKE_PHASE2)
    {
        IKE_MEMCPY (pu1IVBuf, pSessionInfo->pIkeSA->pu1FinalPhase1IV, u4IVLen);
        IKE_MEMCPY (pu1IVBuf + u4IVLen, &u4MsgId, sizeof (UINT4));
    }

    (*gaIkeHashAlgoList[pSessionInfo->pIkeSA->u1Phase1HashAlgo].HashAlgo)
        (pu1IVBuf, (INT4) u4IVBufLen, pu1CryptIV);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pSessionInfo->pu1CryptIV) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessKernelIpsecRequest: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (pSessionInfo->pu1CryptIV == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IVBuf);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1CryptIV);
        return IKE_FAILURE;
    }
    IKE_MEMCPY (pSessionInfo->pu1CryptIV, pu1CryptIV, u4IVLen);

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1IVBuf);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1CryptIV);
    UNUSED_PARAM (u4HashLen);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeAssertFail                                      */
/*  Description     :This function is used for Assertions               */
/*  Input(s)        :pu1Expr - Expression to be asserted                */
/*                  :pu1FileName - File where assertion is present      */
/*                  :pu1LineNum - The line number where assertion is    */
/*                  :present                                            */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
IkeAssertFail (const char *pu1Expr, const char *pu1FileName, int pu1LineNum)
{
    IKE_MOD_TRC3 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                  "Assertion(%s) failed in %s:%d\n", pu1Expr, pu1FileName,
                  pu1LineNum);
    UNUSED_PARAM (pu1Expr);
    UNUSED_PARAM (pu1FileName);
    UNUSED_PARAM (pu1LineNum);

}

/************************************************************************/
/*  Function Name   :IkeGetIkeEngine                                    */
/*  Description     :This function is used to get the IkeEngine using   */
/*                  :the Local Tunnel Termination Address               */
/*  Input(s)        :pSrcTEAddr - The Local Tunnel Termination Address  */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to IkeEngine structure                     */
/************************************************************************/

tIkeEngine         *
IkeGetIkeEngine (tIkeIpAddr * pSrcTEAddr)
{
    tIkeEngine         *pIkeEngine = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (pIkeEngine->u1Status == IKE_ACTIVE)
        {
            if (pSrcTEAddr->u4AddrType == IPV4ADDR)
            {
                if (pIkeEngine->Ip4TunnelTermAddr.Ipv4Addr ==
                    pSrcTEAddr->Ipv4Addr)
                {
                    return pIkeEngine;
                }
            }
            else
            {
                if (IKE_MEMCMP (&pIkeEngine->Ip6TunnelTermAddr.Ipv6Addr,
                                &pSrcTEAddr->Ipv6Addr, sizeof (tIp6Addr))
                    == IKE_ZERO)
                {
                    return pIkeEngine;
                }
            }
        }
    }
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeConvertToRangeForIpv4                           */
/*  Description     :This function is used to convert data passed in an */
/*                  :IkeNetwork structure to a range format             */
/*  Input(s)        :pNetwork   - Pointer to the IkeNetwork structure   */
/*  Output(s)       :pIpv4Range - Pointer to the Ipv4Range structure    */
/*  Returns         :None                                               */
/************************************************************************/
VOID
IkeConvertToRangeForIpv4 (tIkeNetwork * pNetwork, tIkeIpv4Range * pIpv4Range)
{
    if (pNetwork->u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        pIpv4Range->Ip4StartAddr = pNetwork->uNetwork.Ip4Addr;
        pIpv4Range->Ip4EndAddr = pNetwork->uNetwork.Ip4Addr;
    }
    else if (pNetwork->u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        pIpv4Range->Ip4StartAddr = pNetwork->uNetwork.Ip4Subnet.Ip4Addr &
            pNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask;
        pIpv4Range->Ip4EndAddr = ((pNetwork->uNetwork.Ip4Subnet.Ip4Addr &
                                   pNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask) |
                                  ~pNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask);
    }
    else if (pNetwork->u4Type == IKE_IPSEC_ID_IPV4_RANGE)
    {
        IKE_MEMCPY (pIpv4Range, &pNetwork->uNetwork.Ip4Range,
                    sizeof (tIkeIpv4Range));
    }
    return;
}

/************************************************************************/
/*  Function Name   :IkeConvertToRangeForIpv6                           */
/*  Description     :This function is used to convert data passed in an */
/*                  :IkeNetwork structure to a range format             */
/*  Input(s)        :pNetwork   - Pointer to the IkeNetwork structure   */
/*  Output(s)       :pIpv6Range - Pointer to the Ipv4Range structure    */
/*  Returns         :None                                               */
/************************************************************************/
VOID
IkeConvertToRangeForIpv6 (tIkeNetwork * pNetwork, tIkeIpv6Range * pIpv6Range)
{
    UINT1               u1Count = IKE_ZERO;
    tIp6Addr            Ip6Addr;

    IKE_BZERO (&Ip6Addr, sizeof (tIp6Addr));

    if (pNetwork->u4Type == IKE_IPSEC_ID_IPV6_ADDR)
    {
        Ip6AddrCopy (&(pIpv6Range->Ip6StartAddr),
                     &(pNetwork->uNetwork.Ip6Addr));
        Ip6AddrCopy (&(pIpv6Range->Ip6EndAddr), &(pNetwork->uNetwork.Ip6Addr));
    }
    else if (pNetwork->u4Type == IKE_IPSEC_ID_IPV6_SUBNET)
    {

        for (u1Count = IKE_ZERO; u1Count < IKE_IP6_ADDR_LEN; u1Count++)
        {
            Ip6Addr.u1_addr[u1Count] =
                (pNetwork->uNetwork.Ip6Subnet.Ip6Addr.u1_addr[u1Count] &
                 pNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask.u1_addr[u1Count]);
        }
        Ip6AddrCopy (&(pIpv6Range->Ip6StartAddr), &Ip6Addr);

        IKE_BZERO (&Ip6Addr, sizeof (tIp6Addr));
        for (u1Count = IKE_ZERO; u1Count < IKE_IP6_ADDR_LEN; u1Count++)
        {
            Ip6Addr.u1_addr[u1Count] = (UINT1)
                ((pNetwork->uNetwork.Ip6Subnet.Ip6Addr.u1_addr[u1Count] &
                  pNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask.u1_addr[u1Count])
                 | (~pNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask.
                    u1_addr[u1Count]));
        }
        Ip6AddrCopy (&(pIpv6Range->Ip6EndAddr), &Ip6Addr);
    }
    else if (pNetwork->u4Type == IKE_IPSEC_ID_IPV6_RANGE)
    {
        IKE_MEMCPY (pIpv6Range, &pNetwork->uNetwork.Ip6Range,
                    sizeof (tIkeIpv6Range));
    }
    return;
}

/************************************************************************/
/*  Function Name   :IkeCompareNetworks                                 */
/*  Description     :This function is used to compare two structure of  */
/*                  :type tIkeNetwork                                   */
/*  Input(s)        :pNetwork1 - Pointer to the 1st IkeNetwork struct   */
/*                  :pNetwork1 - Pointer to the 2nd IkeNetwork struct   */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_TRUE if they match, else IKE_FALSE             */
/************************************************************************/
BOOLEAN
IkeCompareNetworks (tIkeNetwork * pNetwork1, tIkeNetwork * pNetwork2,
                    UINT1 u1IkeVersion)
{
    UINT4               u4Nw1Type = IKE_ZERO;
    UINT4               u4Nw2Type = IKE_ZERO;

    if ((pNetwork1->u4Type == IKE_IPSEC_ID_IPV4_ADDR) ||
        (pNetwork1->u4Type == IKE_IPSEC_ID_IPV4_SUBNET) ||
        (pNetwork1->u4Type == IKE_IPSEC_ID_IPV4_RANGE))
    {
        u4Nw1Type = IKE_IPSEC_ID_IPV4_RANGE;
    }
    else if ((pNetwork1->u4Type == IKE_IPSEC_ID_IPV6_ADDR) ||
             (pNetwork1->u4Type == IKE_IPSEC_ID_IPV6_SUBNET) ||
             (pNetwork1->u4Type == IKE_IPSEC_ID_IPV6_RANGE))
    {
        u4Nw1Type = IKE_IPSEC_ID_IPV6_RANGE;
    }

    if ((pNetwork2->u4Type == IKE_IPSEC_ID_IPV4_ADDR) ||
        (pNetwork2->u4Type == IKE_IPSEC_ID_IPV4_SUBNET) ||
        (pNetwork2->u4Type == IKE_IPSEC_ID_IPV4_RANGE))
    {
        u4Nw2Type = IKE_IPSEC_ID_IPV4_RANGE;
    }
    else if ((pNetwork2->u4Type == IKE_IPSEC_ID_IPV6_ADDR) ||
             (pNetwork2->u4Type == IKE_IPSEC_ID_IPV6_SUBNET) ||
             (pNetwork2->u4Type == IKE_IPSEC_ID_IPV6_RANGE))
    {
        u4Nw2Type = IKE_IPSEC_ID_IPV6_RANGE;
    }

    /* Make sure both are V4 or V6 */
    if ((u4Nw1Type == IKE_ZERO) || (u4Nw2Type == IKE_ZERO)
        || (u4Nw1Type != u4Nw2Type))
    {
        return IKE_FALSE;
    }

    /* Check that the IP Higher layer protocol is matching */
    if (pNetwork1->u1HLProtocol != pNetwork2->u1HLProtocol)
    {
        return IKE_FALSE;
    }

    if (u1IkeVersion == IKE2_MAJOR_VERSION)
    {
        /* In case of Ikev2, check that the protocol, start port and end port
         * match*/
        if ((pNetwork1->u1HLProtocol != pNetwork2->u1HLProtocol) ||
            (pNetwork1->u2StartPort != pNetwork2->u2StartPort) ||
            (pNetwork1->u2EndPort != pNetwork2->u2EndPort))
        {
            return IKE_FALSE;
        }
    }

    if (u4Nw1Type == IKE_IPSEC_ID_IPV4_RANGE)
    {
        /* V4 address */
        tIkeIpv4Range       Ipv4Range1;
        tIkeIpv4Range       Ipv4Range2;

        IkeConvertToRangeForIpv4 (pNetwork1, &Ipv4Range1);
        IkeConvertToRangeForIpv4 (pNetwork2, &Ipv4Range2);

        /* 
         * XXX For now, just doing a memcmp, have to support sub-divisions 
         * later on 
         */

        if ((Ipv4Range1.Ip4StartAddr == Ipv4Range2.Ip4StartAddr) &&
            (Ipv4Range1.Ip4EndAddr == Ipv4Range2.Ip4EndAddr))
        {
            return (IKE_TRUE);
        }
        else
        {
            return (IKE_FALSE);
        }
    }
    else if (u4Nw1Type == IKE_IPSEC_ID_IPV6_RANGE)
    {
        /* V6 address */
        tIkeIpv6Range       Ipv6Range1;
        tIkeIpv6Range       Ipv6Range2;

        IkeConvertToRangeForIpv6 (pNetwork1, &Ipv6Range1);
        IkeConvertToRangeForIpv6 (pNetwork2, &Ipv6Range2);

        if (IKE_MEMCMP (&Ipv6Range1, &Ipv6Range2, sizeof (tIkeIpv6Range))
            == IKE_ZERO)
        {
            /* The ranges are equal */
            return (IKE_TRUE);
        }

        /* Ipv6Range1 Refers to Configured Address Range for the given Host
           and Ipv6Range2 Refers to the Address Range of the Peer */

        if ((Ip6IsAddrInBetween
             (&(Ipv6Range2.Ip6StartAddr), &(Ipv6Range1.Ip6StartAddr),
              &(Ipv6Range1.Ip6EndAddr)) == IP6_SUCCESS)
            &&
            (Ip6IsAddrInBetween
             (&(Ipv6Range2.Ip6EndAddr), &(Ipv6Range1.Ip6StartAddr),
              &(Ipv6Range1.Ip6EndAddr)) == IP6_SUCCESS))
        {
            return (IKE_TRUE);
        }
    }
    return (IKE_FALSE);
}

/************************************************************************/
/*  Function Name   :IkeGetCryptoMapFromIDs                             */
/*  Description     :This function is used to get the Crypto-map using  */
/*                  :the local and remote networks                      */
/*  Input(s)        :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :pLocNetwork - Pointer to the Local Network         */
/*                  :pRemNetwork - Pointer to the Remote Network        */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to Crypto-map structure                    */
/************************************************************************/
tIkeCryptoMap      *
IkeGetCryptoMapFromIDs (tIkeEngine * pIkeEngine, tIkeNetwork * pLocNetwork,
                        tIkeNetwork * pRemNetwork, UINT1 u1IkeVersion)
{
    tIkeCryptoMap      *pCryptoMap = NULL;

    SLL_SCAN (&pIkeEngine->IkeCryptoMapList, pCryptoMap, tIkeCryptoMap *)
    {
        if (pCryptoMap->u1Status == IKE_ACTIVE)
        {
            if ((IkeCompareNetworks
                 (&pCryptoMap->LocalNetwork, pLocNetwork,
                  u1IkeVersion) == IKE_TRUE)
                &&
                (IkeCompareNetworks
                 (&pCryptoMap->RemoteNetwork, pRemNetwork,
                  u1IkeVersion) == IKE_TRUE))
            {
                return (pCryptoMap);
            }
        }
    }
    return (pCryptoMap);
}

/************************************************************************/
/*  Function Name   :IkeGetCryptoMap                                    */
/*  Description     :This function is used to get the Crypto-map using  */
/*                  :the local and remote tunnel termination address    */
/*  Input(s)        :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :pSrcAddr   - The Local tunnel termination address  */
/*                  :pDestAddr  - The Remote tunnel termination address */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to Crypto-map structure                    */
/************************************************************************/
tIkeCryptoMap      *
IkeGetCryptoMap (tIkeEngine * pIkeEngine, tIkeIpAddr * pSrcAddr,
                 tIkeIpAddr * pDestAddr)
{
    tIkeCryptoMap      *pCryptoMap = NULL;
    UINT4               u4AddrType = IKE_ZERO;

    /* Source and destination address types would be same */
    u4AddrType = pSrcAddr->u4AddrType;

    IKE_ASSERT (pSrcAddr->u4AddrType == pDestAddr->u4AddrType);

    SLL_SCAN (&pIkeEngine->IkeCryptoMapList, pCryptoMap, tIkeCryptoMap *)
    {
        if (pCryptoMap->u1Status == IKE_ACTIVE)
        {
            if (u4AddrType == IPV4ADDR)
            {
                if ((IkeCheckv4Network (&pCryptoMap->LocalNetwork, pSrcAddr) ==
                     IKE_SUCCESS)
                    && (IkeCheckv4Network (&pCryptoMap->RemoteNetwork,
                                           pDestAddr) == IKE_SUCCESS))
                {
                    return pCryptoMap;
                }
            }
            else
            {
                if ((IkeCheckv6Network (&pCryptoMap->LocalNetwork, pSrcAddr) ==
                     IKE_SUCCESS)
                    && (IkeCheckv6Network (&pCryptoMap->RemoteNetwork,
                                           pDestAddr) == IKE_SUCCESS))
                {
                    return pCryptoMap;
                }
            }
        }
    }
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetRAInfoFromPeerIpAddr                         */
/*  Description     :This function is used to get the RemoteAccess Info */
/*                  :using the Peer Address                             */
/*  Input(s)        :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :pDestAddr  - The Remote tunnel termination address */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to RAInfo structure                        */
/************************************************************************/
tIkeRemoteAccessInfo *
IkeGetRAInfoFromPeerIpAddr (tIkeEngine * pIkeEngine, tIkeIpAddr * pDestAddr)
{
    tIkeRemoteAccessInfo *pRAInfo = NULL;

    SLL_SCAN (&pIkeEngine->IkeRAPolicyList, pRAInfo, tIkeRemoteAccessInfo *)
    {
        if (pRAInfo->u1Status == IKE_ACTIVE)
        {
            if (pDestAddr->u4AddrType == IPV4ADDR)
            {
                if (IKE_MEMCMP (&(pRAInfo->PeerAddr.Ipv4Addr),
                                &(pDestAddr->Ipv4Addr), IKE_IP4_ADDR_LEN)
                    == IKE_ZERO)
                {
                    return (pRAInfo);
                }
            }
            else if (pDestAddr->u4AddrType == IPV6ADDR)
            {
                if (IKE_MEMCMP (&(pRAInfo->PeerAddr.Ipv6Addr),
                                &(pDestAddr->Ipv6Addr), IKE_IP6_ADDR_LEN)
                    == IKE_ZERO)
                {
                    return (pRAInfo);
                }
            }
        }
    }
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeCheckv6Network                                  */
/*  Description     :A utility function for IP6  Addresses              */
/*  Input(s)        :pIkeNetwork - Pointer to the IkeNetwork structure  */
/*                  :pIp6Addr    - The IP6 Address                      */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeCheckv6Network (tIkeNetwork * pIkeNetwork, tIkeIpAddr * pIp6Addr)
{
    tIp6Addr            Ip6SrcAddr, Ip6SrcEndAddr;
    UINT4               u4Count = IKE_ZERO,
        u4Temp = IKE_ZERO, u4Prefix = IKE_ZERO;

    /* Check the local network address */
    if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV6_SUBNET)
    {
        /* Get the Prefix from the bitmask format */
        for (u4Count = IKE_ZERO; u4Count < (IKE_IP6_ADDR_LEN / sizeof (UINT4));
             u4Count++)
        {
            u4Temp =
                pIkeNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask.u4_addr[u4Count];
            if (u4Temp == IKE_MASK)
            {
                u4Prefix += sizeof (UINT4);
            }
            else
            {
                while (u4Temp != IKE_ZERO)
                {
                    u4Temp = u4Temp << IKE_ONE;
                    u4Prefix++;
                }
                break;
            }
        }

        Ip6SrcAddr = pIkeNetwork->uNetwork.Ip6Subnet.Ip6Addr;

        if (Ip6AddrMatch (&pIp6Addr->Ipv6Addr, &Ip6SrcAddr, (INT4) u4Prefix) ==
            TRUE)
        {
            return IKE_SUCCESS;
        }
    }
    else if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV6_RANGE)
    {
        Ip6SrcAddr = pIkeNetwork->uNetwork.Ip6Range.Ip6StartAddr;
        Ip6SrcEndAddr = pIkeNetwork->uNetwork.Ip6Range.Ip6EndAddr;

        if (IkeMatchv6Range (&pIp6Addr->Ipv6Addr, &Ip6SrcAddr, &Ip6SrcEndAddr)
            == IKE_SUCCESS)
        {
            return IKE_SUCCESS;
        }
    }
    else if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV6_ADDR)
    {
        Ip6SrcAddr = pIkeNetwork->uNetwork.Ip6Addr;

        if (IKE_MEMCMP (&Ip6SrcAddr, &pIp6Addr->Ipv6Addr, sizeof (tIp6Addr)) ==
            IKE_ZERO)
        {
            return IKE_SUCCESS;
        }
    }
    return IKE_FAILURE;
}

/* V4 functions */
/************************************************************************/
/*  Function Name   :IkeCheckv4Network                                  */
/*  Description     :A utility function for IP4 Addresses               */
/*  Input(s)        :pIkeNetwork - Pointer to the IkeNetwork structure  */
/*                  :pIp4Addr    - The IP4 Address                      */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeCheckv4Network (tIkeNetwork * pIkeNetwork, tIkeIpAddr * pIp4Addr)
{
    UINT4               u4Ip4SrcSubNet = IKE_ZERO,
        u4Ip4SrcAddr = IKE_ZERO, u4Ip4SrcEndAddr = IKE_ZERO;

    /* Check the local network address */
    if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        u4Ip4SrcSubNet = pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask;
        u4Ip4SrcAddr = pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr;

        if (IkeMatchv4Network (pIp4Addr->Ipv4Addr, u4Ip4SrcAddr, u4Ip4SrcSubNet)
            == IKE_SUCCESS)
        {
            return IKE_SUCCESS;
        }
    }
    else if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV4_RANGE)
    {
        u4Ip4SrcAddr = pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr;
        u4Ip4SrcEndAddr = pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr;

        if (IkeMatchv4Range (pIp4Addr->Ipv4Addr, u4Ip4SrcAddr, u4Ip4SrcEndAddr)
            == IKE_SUCCESS)
        {
            return IKE_SUCCESS;
        }
    }
    else if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        u4Ip4SrcAddr = pIkeNetwork->uNetwork.Ip4Addr;

        if (u4Ip4SrcAddr == pIp4Addr->Ipv4Addr)
        {
            return IKE_SUCCESS;
        }
    }

    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   :IkeMatchv6Range                                    */
/*  Description     :A utility function for IP6 Addresses               */
/*  Input(s)        :pIpv6Addr     - Pointer to the IP6 Address         */
/*                  :pIp6StartAddr - Start of the Address range         */
/*                  :pIp6EndAddr   - End of the Address range           */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeMatchv6Range (tIp6Addr * pIpv6Addr, tIp6Addr * pIp6StartAddr,
                 tIp6Addr * pIp6EndAddr)
{
    if ((IKE_MEMCMP (pIpv6Addr, pIp6StartAddr,
                     sizeof (tIp6Addr)) >= IKE_ZERO) &&
        (IKE_MEMCMP (pIpv6Addr, pIp6EndAddr, sizeof (tIp6Addr)) <= IKE_ZERO))
    {
        return IKE_SUCCESS;
    }

    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   :IkeMatchv4Range                                    */
/*  Description     :A utility function for IP4 Addresses               */
/*  Input(s)        :pIpv4Addr     - Pointer to the IP4 Address         */
/*                  :pIp4StartAddr - Start of the Address range         */
/*                  :pIp4EndAddr   - End of the Address range           */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeMatchv4Range (UINT4 u4Ipv4Addr, UINT4 u4Ip4StartAddr, UINT4 u4Ip4EndAddr)
{
    if ((u4Ipv4Addr >= u4Ip4StartAddr) && (u4Ipv4Addr <= u4Ip4EndAddr))
    {
        return IKE_SUCCESS;
    }

    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   :IkeMatchv4Network                                  */
/*  Description     :A utility function for IP4 Addresses               */
/*  Input(s)        :u4Ipv4Addr    - The IP4 Address                    */
/*                  :u4Ip4ConfAddr - The configured IP4 Address         */
/*                  :u4Ip4SubNet   - The configured IP4 Subnet mask     */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeMatchv4Network (UINT4 u4Ipv4Addr, UINT4 u4Ip4ConfAddr, UINT4 u4Ip4SubNet)
{
    if ((u4Ipv4Addr & u4Ip4SubNet) == (u4Ip4ConfAddr & u4Ip4SubNet))
    {
        return IKE_SUCCESS;
    }
    else
    {
        return IKE_FAILURE;
    }
}

/************************************************************************/
/*  Function Name   :IkeGetIkeSAFromCookies                             */
/*  Description     :This function is used to get Ike SA from cookies   */
/*  Input(s)        :pIkeEngine    - Pointer to the IkeEngine structure */
/*                  :pu1InitCookie - The Initiator cookie               */
/*                  :pu1RespCookie - The Responder cookie               */
/*                  :pPeerTEAddr   - The Peer Tunnel Termination addr   */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the IkeSA structure                     */
/************************************************************************/

tIkeSA             *
IkeGetIkeSAFromCookies (tIkeEngine * pIkeEngine, UINT1 *pu1InitCookie,
                        UINT1 *pu1RespCookie, tIkeIpAddr * pPeerTEAddr)
{
    tIkeSA             *pIkeSA = NULL;

    SLL_SCAN (&pIkeEngine->IkeSAList, pIkeSA, tIkeSA *)
    {
        if (pIkeSA->u1SAStatus != IKE_ACTIVE)
        {
            continue;
        }
        if ((IKE_MEMCMP
             (pIkeSA->au1InitiatorCookie, pu1InitCookie,
              IKE_COOKIE_LEN) == IKE_ZERO)
            &&
            (IKE_MEMCMP
             (pIkeSA->au1ResponderCookie, pu1RespCookie, IKE_COOKIE_LEN)
             == IKE_ZERO))
        {
            if (pPeerTEAddr->u4AddrType == IPV4ADDR)
            {
                if (pIkeSA->IpPeerAddr.Ipv4Addr == pPeerTEAddr->Ipv4Addr)
                {
                    return pIkeSA;
                }
            }
            else
            {
                if (IKE_MEMCMP (&pIkeSA->IpPeerAddr.Ipv6Addr,
                                &pPeerTEAddr->Ipv6Addr, sizeof (tIp6Addr))
                    == IKE_ZERO)
                {
                    return pIkeSA;
                }
            }
        }
    }
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetIkeEngineFromIndex                           */
/*  Description     :This function is used to get IkeEngine using the   */
/*                  :Engine Index                                       */
/*  Input(s)        :u4IkeEngineIndex - The IkeEngine Index             */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the IkeEngine structure                 */
/************************************************************************/

tIkeEngine         *
IkeGetIkeEngineFromIndex (UINT4 u4IkeEngineIndex)
{
    tIkeEngine         *pIkeEngine = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if ((pIkeEngine->u1Status == IKE_ACTIVE) &&
            (pIkeEngine->u4IkeIndex == u4IkeEngineIndex))
        {
            return pIkeEngine;
        }
    }

    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetId                                           */
/*  Description     :This function is used to store the recd Id payload */
/*                  :into a network structure                           */
/*                  :                                                   */
/*  Input(s)        :pId : Pointer to pId                               */
/*                  :pu1PayLoad : pointer to recd payload               */
/*                  :                                                   */
/*  Output(s)       :pIkeNetwork : Pointer to the network structure     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT1
IkeGetId (tIdPayLoad * pId, UINT1 *pu1PayLoad, tIkeNetwork * pIkeNetwork)
{
    UINT4               u4IdLen = IKE_ZERO;

    u4IdLen = (UINT4) (pId->u2PayLoadLen - IKE_ID_PAYLOAD_SIZE);
    pIkeNetwork->u4Type = pId->u1IdType;
    pIkeNetwork->u1HLProtocol = pId->u1Protocol;
    switch (pId->u1IdType)
    {
        case IKE_IPSEC_ID_IPV4_ADDR:
        {
            if (u4IdLen != IKE_IP4_ADDR_LEN)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip4Addr, pu1PayLoad,
                        IKE_IP4_ADDR_LEN);
            pIkeNetwork->uNetwork.Ip4Addr =
                IKE_NTOHL (pIkeNetwork->uNetwork.Ip4Addr);
            break;
        }

        case IKE_IPSEC_ID_IPV4_SUBNET:
        {
            if (u4IdLen != (IKE_TWO * IKE_IP4_ADDR_LEN))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr, pu1PayLoad,
                        IKE_IP4_ADDR_LEN);
            pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr =
                IKE_NTOHL (pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr);
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask,
                        (pu1PayLoad + IKE_IP4_ADDR_LEN), IKE_IP4_ADDR_LEN);
            pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask =
                IKE_NTOHL (pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask);
            break;
        }

        case IKE_IPSEC_ID_IPV4_RANGE:
        {
            if (u4IdLen != (IKE_TWO * IKE_IP4_ADDR_LEN))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr,
                        pu1PayLoad, IKE_IP4_ADDR_LEN);
            pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr =
                IKE_NTOHL (pIkeNetwork->uNetwork.Ip4Range.Ip4StartAddr);
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr,
                        (pu1PayLoad + IKE_IP4_ADDR_LEN), IKE_IP4_ADDR_LEN);
            pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr =
                IKE_NTOHL (pIkeNetwork->uNetwork.Ip4Range.Ip4EndAddr);
            break;
        }

        case IKE_IPSEC_ID_IPV6_ADDR:
        {
            if (u4IdLen != IKE_IP6_ADDR_LEN)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Addr.u1_addr, pu1PayLoad,
                        IKE_IP6_ADDR_LEN);
            break;
        }

        case IKE_IPSEC_ID_IPV6_SUBNET:
        {
            if (u4IdLen != (IKE_TWO * IKE_IP6_ADDR_LEN))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Subnet.Ip6Addr.u1_addr,
                        pu1PayLoad, IKE_IP6_ADDR_LEN);
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask.u1_addr,
                        (pu1PayLoad + IKE_IP6_ADDR_LEN), IKE_IP6_ADDR_LEN);
            break;
        }

        case IKE_IPSEC_ID_IPV6_RANGE:
        {
            if (u4IdLen != (IKE_TWO * IKE_IP6_ADDR_LEN))
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessPhase2Id:Addr Len wrong\n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Range.Ip6StartAddr,
                        pu1PayLoad, IKE_IP6_ADDR_LEN);
            IKE_MEMCPY (&pIkeNetwork->uNetwork.Ip6Range.Ip6EndAddr,
                        (pu1PayLoad + IKE_IP6_ADDR_LEN), IKE_IP6_ADDR_LEN);
            break;
        }
        default:
            break;
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeGetIkeSAFromPeerAddress                         */
/*  Description     :This function is used to get IkeSA from Peer       */
/*                  :address                                            */
/*  Input(s)        :pIkeEngine - Pointer to the IkeEngine structure    */
/*                  :pPeerAddr  - The Peer Address                      */
/*  Output(s)       :None                                               */
/*  Returns         :Pointer to the IkeSA structure                     */
/************************************************************************/

tIkeSA             *
IkeGetIkeSAFromPeerAddress (tIkeEngine * pIkeEngine, tIkeIpAddr * pPeerAddr)
{
    tIkeSA             *pIkeSA = NULL;

    SLL_SCAN (&pIkeEngine->IkeSAList, pIkeSA, tIkeSA *)
    {
        if (pIkeSA->u1SAStatus != IKE_ACTIVE)
        {
            continue;
        }

        if (pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
        {
            if (pIkeSA->IpPeerAddr.Ipv4Addr == pPeerAddr->Ipv4Addr)
            {
                return pIkeSA;
            }
        }
        else
        {
            if (IKE_MEMCMP
                (&pIkeSA->IpPeerAddr.Ipv6Addr, &pPeerAddr->Ipv6Addr,
                 sizeof (tIp6Addr)) == IKE_ZERO)
            {
                return pIkeSA;
            }

        }
    }
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeDeleteIkeSA                                     */
/*  Description     :This function is used to delete the given IkeSA    */
/*                  :and free the sessions using this SA                */
/*  Input(s)        :pIkeSA - Pointer to the IkeSA                      */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeDeleteIkeSA (tIkeSA * pIkeSA)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSessionInfo    *pSesInfo = NULL;

    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSA: IkeSA NULL!\n");
        return IKE_SUCCESS;
    }

    pIkeEngine = IkeGetIkeEngineFromIndex (pIkeSA->u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSA: Engine not found\n");
        return IKE_FAILURE;
    }

    /* Delete Ongoing sessions with this IkeSA */
    pSesInfo = IkeGetPhase2SessionFromIkeSA (pIkeEngine, pIkeSA);

    /* We can have multiple phase2 sessions with the same peer address */
    while (pSesInfo != NULL)
    {
        if (IkeDeleteSession (pSesInfo) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeDeleteIkeSA: IkeDeleteSession FAILED!\n");
            return (IKE_FAILURE);
        }
        pSesInfo = IkeGetPhase2SessionFromIkeSA (pIkeEngine, pIkeSA);
    }

    if (pIkeSA->u4RefCount != IKE_ZERO)
    {
        IKE_ASSERT (pIkeSA->u4RefCount == IKE_ZERO);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSA:RefCount not zero in SA!\n");
        return (IKE_FAILURE);
    }

    /* Send a INFO (MSG) with Delete Payload , 
     * deletion will happen after we get a reply or when max 
     * retransmissions occur incase of initiator */
    if ((pIkeSA->u1IkeVer == IKE2_MAJOR_VERSION) &&
        (pIkeSA->bInfoDelSent != IKE_TRUE))
    {
        /* Set the state to active as the SA is still required
         * for INFO exchange */
        Ike2UtilIkeSADeleteRequest (pIkeSA);
        pIkeSA->u1SAStatus = IKE_ACTIVE;
        return (IKE_SUCCESS);
    }

    if (((pIkeSA->IkeNattInfo.u1NattFlags &
          IKE_USE_NATT_PORT) == IKE_USE_NATT_PORT) &&
        ((pIkeSA->IkeNattInfo.u1NattFlags &
          IKE_PEER_BEHIND_NAT) == IKE_PEER_BEHIND_NAT))
    {
        IkeStopTimer (&pIkeSA->IkeNatKeepAliveTimer);
    }
    SLL_DELETE (&pIkeEngine->IkeSAList, (t_SLL_NODE *) pIkeSA);
    if (IkeFreeSAInfo ((t_SLL_NODE *) pIkeSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSA: IkeFreeSAInfo FAILED!\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeHandleCoreFailure                               */
/*  Description     :This function handles the IkeCoreProcess and       */
/*                  :Ike2CoreProcess failure condition.                 */
/*  Input(s)        :pSessionInfo - Pointer to the SesInfo structure    */
/*  Output(s)       :None                                               */
/*  Returns         : NONE                                              */
/************************************************************************/
VOID
IkeHandleCoreFailure (tIkeSessionInfo * pSessionInfo)
{
    tIsakmpHdr         *pIsakmpHdr = NULL;

    if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        if (pSessionInfo->bDelSession == IKE_TRUE)
        {
            IncIkeNoOfSessionsFail (&pSessionInfo->RemoteTunnelTermAddr);
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeHandleCoreFailure: IkeDeleteSession Failed\n");
            }
        }
        else
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1LastRcvdPkt);
            pSessionInfo->pu1LastRcvdPkt = pSessionInfo->pu1LastRcvdGoodPkt;
            pSessionInfo->u2LastRcvdPktLen = pSessionInfo->u2LastRcvdGoodPktLen;
            pSessionInfo->pu1LastRcvdGoodPkt = NULL;
            pSessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }
    }
    else
    {
        /* Processing resulted in some error condition,
         * Need to send the response with the notify payload and then
         * Delete the IKE Session (If Established) & the session. */
        if (pSessionInfo->u2NotifyType != IKE_NONE)
        {
            if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
            {
                pIsakmpHdr =
                    ((tIsakmpHdr *) (void *) pSessionInfo->IkeTxPktInfo.
                     pu1RawPkt);
                if (pIsakmpHdr->u1Exch != IKE2_INIT_EXCH)
                {
                    Ike2ConstructEncryptPayLoad (pSessionInfo);
                }
                IkeSendToSocketLayer (pSessionInfo);
            }
        }
        /* In case of Child Exchange, delete the New Ikey SA,
           allocated for Rekeying */
        if (pSessionInfo->u1ExchangeType == IKE2_CHILD_EXCH)
        {
            if (pSessionInfo->pIkeReKeySA != NULL)
            {
                /* Set the InfoDel Flag as the rekey has failed and it 
                 * is no more required, no need to send a Info(Del). */
                printf ("\n  Ike2Handle Core Failure:\
                     Deleting IKE Rekey SA... \r\n");
                pSessionInfo->pIkeReKeySA->bInfoDelSent = IKE_TRUE;
                IkeDeleteIkeSA (pSessionInfo->pIkeReKeySA);
            }
        }

        /* Delete the IKE SA along with  the Session , if the failure
         * is in case  of AUTH Excahnge */
        if ((pSessionInfo->pIkeSA != NULL) &&
            (pSessionInfo->pIkeSA->Ike2SA.bAuthDone != IKE_TRUE))
        {
            pSessionInfo->pIkeSA->bInfoDelSent = IKE_TRUE;
            IkeStopTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer);
            if (IkeStartTimer (&pSessionInfo->pIkeSA->IkeSALifeTimeTimer,
                               IKE_DELETESA_TIMER_ID, IKE_MIN_TIMER_INTERVAL,
                               pSessionInfo->pIkeSA) != IKE_SUCCESS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeHandleCoreFailure: IkeDeleteIkeSA Failed\n");
            }
        }

        if (pSessionInfo->bDelSession == IKE_TRUE)
        {
            /* Delete the Session */
            if (IkeDeleteSession (pSessionInfo) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeHandleCoreFailure: IkeDeleteSession Failed\n");
            }
        }
        else
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pSessionInfo->pu1LastRcvdPkt);
            pSessionInfo->pu1LastRcvdPkt = pSessionInfo->pu1LastRcvdGoodPkt;
            pSessionInfo->u2LastRcvdPktLen = pSessionInfo->u2LastRcvdGoodPktLen;
            pSessionInfo->pu1LastRcvdGoodPkt = NULL;
            pSessionInfo->u2LastRcvdGoodPktLen = IKE_ZERO;
        }
    }
    return;
}

/************************************************************************/
/*  Function Name   :IkeHandleCoreSuccess                               */
/*  Description     :This function handles the IkeCoreProcess and       */
/*                  :Ike2CoreProcess Success condition.                 */
/*  Input(s)        :pSessionInfo - Pointer to the SesInfo structure    */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

VOID
IkeHandleCoreSuccess (tIkeSessionInfo * pSessionInfo)
{
    UINT4               u4DeleteTime = IKE_ZERO;

    if ((pSessionInfo->bPhase2Done == IKE_TRUE) ||
        ((pSessionInfo->u1ExchangeType == IKE2_INFO_EXCH) &&
         (pSessionInfo->bInfoDone == IKE_TRUE)))
    {
        /* Start Timer to delete the session */
        /* Start Timer to delete the session */
        if (pSessionInfo->u1ExchangeType == IKE2_INFO_EXCH)
        {
            u4DeleteTime = IKE_MIN_TIMER_INTERVAL;
        }
        else
        {
            u4DeleteTime = IKE_DELAYED_SESSION_DELETE_INTERVAL;
        }
        if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                           IKE_DELETE_SESSION_TIMER_ID,
                           u4DeleteTime, pSessionInfo) != IKE_SUCCESS)
        {
            /* This is not critical error, so just log and don't  
               return failure */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeHandleCoreSuccess: IkeStartTimer for Delete "
                         "session Failed\n");
        }
    }

    if (pSessionInfo->IkeTxPktInfo.u4PacketLen != IKE_ZERO)
    {
        if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
        {
            if (IKE_TEST_ENCR_MASK (pSessionInfo->IkeTxPktInfo.pu1RawPkt)
                != IKE_ZERO)
            {
                IkeEncryptPayLoad (pSessionInfo);
            }
        }
        else
        {
            if (pSessionInfo->u1ExchangeType != IKE2_INIT_EXCH)
            {
                Ike2ConstructEncryptPayLoad (pSessionInfo);
            }
        }
        IkeSendToSocketLayer (pSessionInfo);
    }
    return;
}

/************************************************************************/
/*  Function Name   :IkeProcessInitialContact                           */
/*  Description     :This function is used to delete any old IkeSA's    */
/*                  :corresponding to the peer address                  */
/*  Input(s)        :pSessionInfo - Pointer to the SesInfo structure    */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeProcessInitialContact (tIkeSessionInfo * pSessionInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSA             *pIkeSA = NULL, *pPrev = NULL;
    tIkeIpAddr         *pPeerAddr = &pSessionInfo->pIkeSA->IpPeerAddr;

    pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessInitialContact: Engine not found\n");
        return IKE_FAILURE;
    }

    SLL_SCAN (&pIkeEngine->IkeSAList, pIkeSA, tIkeSA *)
    {
        /* Don't delete the current SA */
        if ((IKE_MEMCMP (pIkeSA->au1InitiatorCookie,
                         pSessionInfo->pIkeSA->au1InitiatorCookie,
                         IKE_COOKIE_LEN) == IKE_ZERO) &&
            (IKE_MEMCMP (pIkeSA->au1ResponderCookie,
                         pSessionInfo->pIkeSA->au1ResponderCookie,
                         IKE_COOKIE_LEN) == IKE_ZERO))
        {
            continue;
        }

        if (pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
        {
            if (pIkeSA->IpPeerAddr.Ipv4Addr == pPeerAddr->Ipv4Addr)
            {
                if (IkeDeleteIkeSA (pIkeSA) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessInitialContact: "
                                 "IkeDeleteIkeSA returned FAILURE!\n");
                    return (IKE_FAILURE);
                }
                pIkeSA = pPrev;
            }
        }
        else
        {
            if (IKE_MEMCMP
                (&pIkeSA->IpPeerAddr.Ipv6Addr, &pPeerAddr->Ipv6Addr,
                 sizeof (tIp6Addr)) == IKE_ZERO)
            {
                if (IkeDeleteIkeSA (pIkeSA) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessInitialContact: "
                                 "IkeDeleteIkeSA returned FAILURE!\n");
                    return (IKE_FAILURE);
                }
                pIkeSA = pPrev;
            }

        }
        pPrev = pIkeSA;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDeleteIkeSAWithCookies                          */
/*  Description     :This function is used to delete the IkeSA          */
/*                  :corresponding to the given cookies                 */
/*  Input(s)        :pSessionInfo - Pointer to the SesInfo structure    */
/*                  :pu1InitCookie - The Initiator cookie               */
/*                  :pu1RespCookie - The Responder cookie               */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/************************************************************************/

INT4
IkeDeleteIkeSAWithCookies (tIkeSessionInfo * pSessionInfo,
                           UINT1 *pu1InitCookie, UINT1 *pu1RespCookie)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSA             *pIkeSA = NULL;

    pIkeEngine = IkeGetIkeEngineFromIndex (pSessionInfo->u4IkeEngineIndex);

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSAWithCookies: Engine not found\n");
        return IKE_FAILURE;
    }

    pIkeSA =
        IkeGetIkeSAFromCookies (pIkeEngine, pu1InitCookie, pu1RespCookie,
                                &pSessionInfo->pIkeSA->IpPeerAddr);

    if (IkeDeleteIkeSA (pIkeSA) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteIkeSAWithCookies: IkeDeleteIkeSA returned "
                     "FAILURE!\n");
        return (IKE_FAILURE);
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeFreePayload                                     */
/*  Description     :This function is used to free the payload          */
/*                  :structure                                          */
/*  Input(s)        :pNode - The Payload Node                           */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
IkeFreePayload (t_SLL_NODE * pNode)
{
    tIkePayload        *pIkePayload = NULL;

    pIkePayload = (tIkePayload *) pNode;
    MemReleaseMemBlock (IKE_PAYLOAD_MEMPOOL_ID, (UINT1 *) pIkePayload);
}

/************************************************************************/
/*  Function Name   :IkeFreeProposal                                    */
/*  Description     :This function is used to free the proposal info    */
/*                  :structure                                          */
/*  Input(s)        :pNode - The proposal(tIkeProposal) Node            */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
IkeFreeProposal (t_SLL_NODE * pNode)
{
    tIkeProposal       *pIkeProposal = NULL;

    pIkeProposal = (tIkeProposal *) pNode;
    SLL_DELETE_LIST (&(pIkeProposal->IkeTransPayLoadList), IkeFreeTransform);
    if (pIkeProposal->pNextProposal != NULL)
    {
        MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID,
                            (UINT1 *) pIkeProposal->pNextProposal);
        pIkeProposal->pNextProposal = NULL;
    }
    MemReleaseMemBlock (IKE_PROPOSAL_MEMPOOL_ID, (UINT1 *) pIkeProposal);

}

/************************************************************************/
/*  Function Name   :IkeFreeTransform                                   */
/*  Description     :This function is used to free the transform info   */
/*                  :structure                                          */
/*  Input(s)        :pNode - The transform(tIkeTrans) Node              */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/

VOID
IkeFreeTransform (t_SLL_NODE * pNode)
{
    tIkeTrans          *pIkeTrans = NULL;

    pIkeTrans = (tIkeTrans *) pNode;
    MemReleaseMemBlock (IKE_TRANSFORM_MEMPOOL_ID, (UINT1 *) pIkeTrans);
}

/************************************************************************/
/*  Function Name   :IkeComparePhase1IDs                                */
/*  Description     :This function compares two phase1 ID's to check if */
/*                  :they are equal                                     */
/*  Input(s)        :pPhase1ID1 : 1st Phase1 ID                         */
/*                  :pPhase1ID2 : 2nd Phase1 ID                         */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS if equal,                              */
/*                  :IKE_FAILURE if ID Types are not equal              */
/************************************************************************/
INT4
IkeComparePhase1IDs (tIkePhase1ID * pPhase1ID1, tIkePhase1ID * pPhase1ID2)
{
    INT4                i4RetVal = IKE_FAILURE;

    if (pPhase1ID1->i4IdType == pPhase1ID2->i4IdType)
    {
        i4RetVal = IKE_MEMCMP (&pPhase1ID1->uID, &pPhase1ID2->uID,
                               pPhase1ID2->u4Length);
        if (i4RetVal == IKE_ZERO)
        {
            return IKE_SUCCESS;
        }
        else
        {
            return IKE_FAILURE;
        }
    }
    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   : IkeGetRSAKeyEntry                                 */
/*  Description     : This function is used to get the rsa key from the */
/*                    engine data structure                             */
/*  Input(s)        : pu1EngineId - Name of the engine                  */
/*                  : pu1KeyName - Rsa key name                         */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to rsa key else null                      */
/************************************************************************/
tIkeRsaKeyInfo     *
IkeGetRSAKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyName)
{
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information stored in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                      "IkeGetRSAKeyEntry: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&(pIkeEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeRsa->au1RsaKeyName, pu1KeyName, IKE_STRLEN (pu1KeyName))
            == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pIkeRsa;
        }
    }
    GIVE_IKE_SEM ();
    return pIkeRsa;
}

/************************************************************************/
/*  Function Name   : IkeGetRSAKeyEntryByEngine                         */
/*  Description     : This function is used to get the rsa key from the */
/*                    engine data structure                             */
/*                    It is assumed that TAKE_IKE_SEM is called by the  */
/*                    caller.                                           */
/*  Input(s)        : pIkeEngine - Pointer to the engine structure      */
/*                  : pu1KeyName - Rsa key name                         */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to rsa key else NULL                      */
/************************************************************************/
tIkeRsaKeyInfo     *
IkeGetRSAKeyEntryByEngine (tIkeEngine * pIkeEngine, UINT1 *pu1KeyName)
{
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All the certificate related information stored in DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }
    SLL_SCAN (&(pIkeDummyEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeRsa->au1RsaKeyName, pu1KeyName, IKE_STRLEN (pu1KeyName))
            == IKE_ZERO)
        {
            break;
        }
    }

    return pIkeRsa;
}

/************************************************************************/
/*  Function Name   : IkeGetRSAKeyWithCertInfo                          */
/*  Description     : This function is used to get the rsa key from the */
/*                    engine data structure using CA name and serial num*/
/*  Input(s)        : pIkeEngine - Pointer to IkeEngine                 */
/*                  : pCertInfo  - pointer to cert info                 */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to rsa key else null                      */
/************************************************************************/
tIkeRsaKeyInfo     *
IkeGetRSAKeyWithCertInfo (tIkeEngine * pIkeEngine, tIkeCertInfo * pCertInfo)
{
    tIkeRsaKeyInfo     *pIkeRsa = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All the certificate related information stored in DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeDummyEngine->IkeRsaKeyList), pIkeRsa, tIkeRsaKeyInfo *)
    {
        if ((IkeX509NameCmp (pIkeRsa->CertInfo.pCAName, pCertInfo->pCAName) ==
             IKE_ZERO)
            &&
            (IkeASN1IntegerCmp
             (pIkeRsa->CertInfo.pCASerialNum, pCertInfo->pCASerialNum)
             == IKE_ZERO))
        {
            GIVE_IKE_SEM ();
            return pIkeRsa;
        }
    }
    GIVE_IKE_SEM ();

    return pIkeRsa;
}

/************************************************************************/
/*  Function Name   : IkeGetDSAKeyEntry                                 */
/*  Description     : This function is used to get the dsa key from the */
/*                    engine data structure                             */
/*  Input(s)        : pu1EngineId - Name of the engine                  */
/*                  : pu1KeyName - Dsa key name                         */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to dsa key else null                      */
/************************************************************************/
tIkeDsaKeyInfo     *
IkeGetDSAKeyEntry (UINT1 *pu1EngineId, UINT1 *pu1KeyName)
{
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information stored in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                      "IkeGetDSAKeyEntry: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&(pIkeEngine->IkeDsaKeyList), pIkeDsa, tIkeDsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeDsa->au1DsaKeyName, pu1KeyName, IKE_STRLEN (pu1KeyName))
            == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pIkeDsa;
        }
    }
    GIVE_IKE_SEM ();
    return pIkeDsa;
}

/************************************************************************/
/*  Function Name   : IkeGetDSAKeyEntryByEngine                         */
/*  Description     : This function is used to get the Dsa key from the */
/*                    engine data structure                             */
/*                    It is assumed that TAKE_IKE_SEM is called by the  */
/*                    caller.                                           */
/*  Input(s)        : pIkeEngine - Pointer to the engine structure      */
/*                  : pu1KeyName - Dsa key name                         */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to Dsa key else NULL                      */
/************************************************************************/
tIkeDsaKeyInfo     *
IkeGetDSAKeyEntryByEngine (tIkeEngine * pIkeEngine, UINT1 *pu1KeyName)
{
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All the certificate related information stored in DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }

    SLL_SCAN (&(pIkeDummyEngine->IkeDsaKeyList), pIkeDsa, tIkeDsaKeyInfo *)
    {
        if (IKE_MEMCMP
            (pIkeDsa->au1DsaKeyName, pu1KeyName, IKE_STRLEN (pu1KeyName))
            == IKE_ZERO)
        {
            break;
        }
    }

    return pIkeDsa;
}

/************************************************************************/
/*  Function Name   : IkeGetDSAKeyWithCertInfo                          */
/*  Description     : This function is used to get the Dsa key from the */
/*                    engine data structure using CA name and serial num*/
/*  Input(s)        : pIkeEngine - Pointer to IkeEngine                 */
/*                  : pCertInfo  - pointer to cert info                 */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to Dsa key else null                      */
/************************************************************************/
tIkeDsaKeyInfo     *
IkeGetDSAKeyWithCertInfo (tIkeEngine * pIkeEngine, tIkeCertInfo * pCertInfo)
{
    tIkeDsaKeyInfo     *pIkeDsa = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All the certificate related information stored in DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeDummyEngine->IkeDsaKeyList), pIkeDsa, tIkeDsaKeyInfo *)
    {
        if ((IkeX509NameCmp (pIkeDsa->CertInfo.pCAName, pCertInfo->pCAName) ==
             IKE_ZERO)
            &&
            (IkeASN1IntegerCmp
             (pIkeDsa->CertInfo.pCASerialNum, pCertInfo->pCASerialNum)
             == IKE_ZERO))
        {
            GIVE_IKE_SEM ();
            return pIkeDsa;
        }
    }
    GIVE_IKE_SEM ();

    return pIkeDsa;
}

/************************************************************************/
/*  Function Name   : IkeGetCertMapFromPeerAddr                         */
/*  Description     : This function is used to get the certificate for  */
/*                    a peer                                            */
/*  Input(s)        : pIkeEngine - Pointer to IkeEngine                 */
/*                  : pIpPeerAddr - Peer IP address                     */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to cert map else null                     */
/************************************************************************/
tIkeCertMapToPeer  *
IkeGetCertMapFromPeerAddr (tIkeEngine * pIkeEngine, tIkeIpAddr * pIpPeerAddr)
{
    tIkeCertMapToPeer  *pCertMap = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All certificate related information stored only in
     * the DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeDummyEngine->IkeCertMapList), pCertMap, tIkeCertMapToPeer *)
    {
        if (pIpPeerAddr->u4AddrType == IPV4ADDR)
        {
            if (IKE_MEMCMP
                (&pIpPeerAddr->Ipv4Addr, &pCertMap->PeerID.uID.Ip4Addr,
                 sizeof (tIp4Addr)) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertMap;
            }
        }
        else if (pIpPeerAddr->u4AddrType == IPV6ADDR)
        {
            if (IKE_MEMCMP
                (&pIpPeerAddr->Ipv6Addr, &pCertMap->PeerID.uID.Ip6Addr,
                 sizeof (tIp6Addr)) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertMap;
            }
        }
    }
    GIVE_IKE_SEM ();

    return pCertMap;
}

/************************************************************************/
/*  Function Name   : IkeGetCertMapFromDN                               */
/*  Description     : This function is used to get the certificate for  */
/*                    a peer                                            */
/*  Input(s)        : pIkeEngine - Pointer to IkeEngine                 */
/*                  : DN  - DN Name                                     */
/*  Output(s)       : None                                              */
/*  Returns         : pointer to cert map else null                     */
/************************************************************************/

tIkeCertMapToPeer  *
IkeGetCertMapFromRemoteID (tIkeEngine * pIkeEngine, UINT1 *pu1DN,
                           UINT1 u1IdType)
{
    tIkeCertMapToPeer  *pCertMap = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    /* All the certificate related information stored in DUMMY engine */
    UNUSED_PARAM (pIkeEngine);
    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return NULL;
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeDummyEngine->IkeCertMapList), pCertMap, tIkeCertMapToPeer *)
    {
        if (u1IdType == IKE_KEY_DN)
        {
            if (IKE_MEMCMP
                (pu1DN, pCertMap->PeerID.uID.au1Dn, STRLEN (pu1DN)) == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertMap;
            }
        }
        else if (u1IdType == IKE_KEY_EMAIL)
        {
            if (IKE_MEMCMP
                (pu1DN, pCertMap->PeerID.uID.au1Email, STRLEN (pu1DN))
                == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertMap;
            }
        }
        else if (u1IdType == IKE_KEY_FQDN)
        {
            if (IKE_MEMCMP (pu1DN, pCertMap->PeerID.uID.au1Fqdn, STRLEN (pu1DN))
                == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertMap;
            }
        }

    }
    GIVE_IKE_SEM ();
    return pCertMap;
}

/************************************************************************/
/*  Function Name   : IkeCheckPeerCertDB                                */
/*  Description     : This function is used to check for a preloaded    */
/*                    certificate for a peer                            */
/*  Input(s)        : pSesInfo - Pointer to session data structure      */
/*  Output(s)       : Updates pSesInfo if peer certificate is preloaded */
/*  Returns         : IKE_TRUE if peer cert exists else IKE_FALSE       */
/************************************************************************/
BOOLEAN
IkeCheckPeerCertDB (tIkeSessionInfo * pSesInfo)
{
    tIkePhase1ID        Phase1Id;
    tIkeCertDB         *pPeerCert = NULL;
    tIkeX509Name       *pCAName = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;

    IKE_BZERO (&Phase1Id, sizeof (tIkePhase1ID));

    if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV4ADDR)
    {
        Phase1Id.i4IdType = IPV4ADDR;
        Phase1Id.uID.Ip4Addr = pSesInfo->pIkeSA->IpPeerAddr.Ipv4Addr;
        Phase1Id.u4Length = IKE_IPV4_LENGTH;
    }
    else if (pSesInfo->pIkeSA->IpPeerAddr.u4AddrType == IPV6ADDR)
    {
        Phase1Id.i4IdType = IPV6ADDR;
        IKE_MEMCPY (&Phase1Id.uID.Ip6Addr,
                    &pSesInfo->pIkeSA->IpPeerAddr.Ipv6Addr, sizeof (tIp6Addr));
        Phase1Id.u4Length = IKE_IPV6_LENGTH;
    }

    pPeerCert =
        IkeGetPeerCertByPhase1Id (pSesInfo->u4IkeEngineIndex, &Phase1Id);

    if (pPeerCert != NULL)
    {
        /* Check if the certificate is a preloaded one */
        if ((pPeerCert->u4Flag & IKE_PRELOADED_PEER_CERT) != IKE_ZERO)
        {
            /* Load this information in the session structure */
            pCAName = IkeGetX509IssuerName (pPeerCert->pCert);
            pCASerialNum = IkeGetX509SerialNum (pPeerCert->pCert);

            if ((pCAName != NULL) && (pCASerialNum != NULL))
            {
                pSesInfo->PeerCertInfo.pCAName = IkeX509NameDup (pCAName);
                pSesInfo->PeerCertInfo.pCASerialNum =
                    IkeASN1IntegerDup (pCASerialNum);
                return IKE_TRUE;
            }
            else
            {
                return IKE_FALSE;
            }
        }
    }
    /* There is no peer certificate with this peer address */
    return IKE_FALSE;
}

/************************************************************************/
/*  Function Name   : IkeCheckSendingCertPayload                        */
/*  Description     : This function is used to decide whether a cert    */
/*                    need to be send for a peer                        */
/*  Input(s)        : pSesInfo - Pointer to session data structure      */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_TRUE if cert to be send else IKE_FALSE        */
/************************************************************************/
BOOLEAN
IkeCheckSendingCertPayload (tIkeSessionInfo * pSesInfo)
{
    tIkeCertReqInfo    *pCertReq = NULL;

    /* pSesInfo would have my certificate information to be send for 
     * the peer during session initialisation. If a default certificate
     * is going to be send for the peer then bDefaultMycert would true
     * else if a specific certificate is configured this would be false */
    if (pSesInfo->bDefaultMyCert == IKE_FALSE)
    {
        /* There is certificate configured for this peer, 
         * send this certificate */
        return IKE_TRUE;
    }
    else
    {
        /* There is a default certificate, Check if this matches
         * with the peer certificate request received */

        if (SLL_COUNT (&(pSesInfo->PeerCertReq)) == IKE_ZERO)
        {
            /* No cert req received, hence don't send cert  */
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCheckSendingCertPayload: No certificate"
                         "is send to peer\n");
            return IKE_FALSE;
        }
        else
        {
            SLL_SCAN (&(pSesInfo->PeerCertReq), pCertReq, tIkeCertReqInfo *)
            {
                if (pCertReq->pCAName == NULL)
                {
                    /* NO CA is specified in cert-req, send the default cert */
                    return IKE_TRUE;
                }
                else if (pCertReq->pCAName != NULL)
                {
                    /* Match this CA name with the default cert's CA name */
                    if (IkeX509NameCmp (pSesInfo->MyCert.pCAName,
                                        pCertReq->pCAName) == IKE_ZERO)
                    {
                        /* CA names matched, send this certificate */
                        return IKE_TRUE;
                    }
                }
            }
        }

    }

    return IKE_FALSE;
}

/************************************************************************/
/*  Function Name   : IkeSnprintf                                       */
/*  Description     : This function is wrapper for SNPRINTF. On         */
/*                    platforms where SNPRINTF is not defined, it uses  */
/*                    SPRINTF                                           */
/*  Input(s)        : u4BufSize:The size of the buffer, including the   */
/*                    trailing null space                               */
/*                    pu1Format: Format string to use                   */
/*                    ...: Arguments for the format string              */
/*  Output(s)       : pu1Buf: The buffer to place the result into       */
/*  Returns         : Length of the string                              */
/************************************************************************/
UINT4
IkeSnprintf (signed char *pu1Buf, UINT4 u4BufSize, const signed char *pu1Format,
             ...)
{
    va_list             Args;
    UINT4               u4RetVal = IKE_ZERO;

    va_start (Args, pu1Format);

#ifndef NO_SNPRINTF
    u4RetVal =
        SNPRINTF (((CHR1 *) pu1Buf), u4BufSize, ((const CHR1 *) pu1Format),
                  Args);
#else
    UNUSED_PARAM (u4BufSize);
    u4RetVal = SPRINTF (((CHR1 *) pu1Buf), ((CHR1 *) pu1Format), Args);
#endif

    va_end (Args);
    return u4RetVal;
}

INT1
IkeUpdtPostPhase1InfoFromRAInfo (tIkeSessionInfo * pSessionInfo)
{
    UINT4               u4Mask = IKE_MASK;

    /* Phase2 Rekey is requied only at the first time */
    if (pSessionInfo->pIkeSA->bRekey != IKE_POST_RA_REKEY)
    {
        pSessionInfo->PostPhase1Info.u4ActionType = IKE_POST_P1_NEW_SA;
    }
    pSessionInfo->PostPhase1Info.IPSecRequest.u1Protocol = IKE_ZERO;
    pSessionInfo->PostPhase1Info.IPSecRequest.u2PortNumber = IKE_ZERO;
    pSessionInfo->PostPhase1Info.IPSecRequest.bRekey = FALSE;
    IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.LocalTEAddr),
                &(pSessionInfo->IkeTransInfo.IkeRAInfo.LocalAddr),
                sizeof (tIkeIpAddr));
    IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.RemoteTEAddr),
                &(pSessionInfo->IkeTransInfo.IkeRAInfo.PeerAddr),
                sizeof (tIkeIpAddr));

    if (pSessionInfo->IkeTransInfo.IkeRAInfo.bCMEnabled == TRUE)
    {
        if (pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType == IPV4ADDR)
        {
            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.
                          uNetwork.Ip4Subnet.Ip4Addr),
                        &(pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv4Addr),
                        sizeof (UINT4));

            pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.u4Type = IPV4ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.
                          uNetwork.Ip4Subnet.Ip4SubnetMask),
                        &(pSessionInfo->IkeTransInfo.InternalMask.Ipv4Addr),
                        sizeof (UINT4));
        }
        else if (pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType
                 == IPV6ADDR)
        {
            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.
                          uNetwork.Ip6Subnet.Ip6Addr),
                        &(pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv6Addr),
                        sizeof (tIp6Addr));

            pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.u4Type = IPV6ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.
                          uNetwork.Ip6Subnet.Ip6SubnetMask),
                        &(pSessionInfo->IkeTransInfo.InternalMask.Ipv6Addr),
                        sizeof (tIp6Addr));
        }
        else
        {
            return (IKE_FAILURE);
        }
        if (pSessionInfo->IkeTransInfo.ProtectedNetwork[IKE_INDEX_0].u4Type
            == IKE_ZERO)
        {
            if (pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType
                == IPV4ADDR)
            {
                pSessionInfo->IkeTransInfo.ProtectedNetwork[IKE_INDEX_0].u4Type
                    = IKE_IPSEC_ID_IPV4_SUBNET;
            }
            else if (pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType
                     == IPV6ADDR)
            {
                pSessionInfo->IkeTransInfo.ProtectedNetwork[IKE_INDEX_0].u4Type
                    = IKE_IPSEC_ID_IPV6_SUBNET;
            }
        }

        IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.DestNet),
                    &(pSessionInfo->IkeTransInfo.ProtectedNetwork[IKE_INDEX_0]),
                    sizeof (tIkeNetwork));
    }
    else
    {
        if (pSessionInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV4ADDR)
        {
            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          SrcNet.uNetwork.Ip4Subnet.Ip4Addr),
                        &(pSessionInfo->pIkeSA->IpLocalAddr.Ipv4Addr),
                        sizeof (UINT4));

            pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.u4Type = IPV4ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          SrcNet.uNetwork.Ip4Subnet.Ip4SubnetMask),
                        &(u4Mask), sizeof (UINT4));

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          DestNet.uNetwork.Ip4Subnet.Ip4Addr),
                        &(pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr),
                        sizeof (UINT4));

            pSessionInfo->PostPhase1Info.IPSecRequest.DestNet.u4Type = IPV4ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          DestNet.uNetwork.Ip4Subnet.Ip4SubnetMask),
                        &(u4Mask), sizeof (UINT4));
        }
        else
        {
            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          SrcNet.uNetwork.Ip6Subnet.Ip6Addr),
                        &(pSessionInfo->pIkeSA->IpLocalAddr.Ipv6Addr),
                        sizeof (tIp6Addr));

            pSessionInfo->PostPhase1Info.IPSecRequest.SrcNet.u4Type = IPV6ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          SrcNet.uNetwork.Ip6Subnet.Ip6SubnetMask.u1_addr),
                        gaIp6PrefixToMask[IKE_INDEX_127], IKE_IP6_ADDR_LEN);

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          DestNet.uNetwork.Ip6Subnet.Ip6Addr),
                        &(pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr),
                        sizeof (tIp6Addr));

            pSessionInfo->PostPhase1Info.IPSecRequest.DestNet.u4Type = IPV6ADDR;

            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info.IPSecRequest.
                          DestNet.uNetwork.Ip4Subnet.Ip4SubnetMask),
                        gaIp6PrefixToMask[IKE_INDEX_127], IKE_IP6_ADDR_LEN);
        }
    }

    return (IKE_SUCCESS);
}

INT1
IkeMaskToPrefix (UINT4 u4Mask, UINT1 *pu1Prefix)
{
    UINT1               u1Count = IKE_ZERO;

    for (u1Count = IKE_ZERO; u1Count < (IKE_MAX_COUNT - IKE_ONE); u1Count++)
    {
        if (gaIp4PrefixToMask[u1Count] == u4Mask)
        {
            *pu1Prefix = u1Count;
            return (IKE_SUCCESS);
        }
    }
    return (IKE_FAILURE);
}

/************************************************************************/
/*  Function Name   : Ikev6MaskToPrefix                                 */
/*  Description     : This function is used to get prefix length from   */
/*                    ipv6 mask                                         */
/*  Input(s)        : pu1Mask - pointer to mask                         */
/*                  : pu1Prefix - Prefix length                         */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS/IKE_FAILURE                           */
/************************************************************************/
INT1
Ikev6MaskToPrefix (UINT1 *pu1Mask, UINT1 *pu1Prefix)
{
    UINT1               u1Count = IKE_ZERO;

    for (u1Count = IKE_ZERO; u1Count < IKE_MAX_LEN; u1Count++)
    {
        if (IKE_MEMCMP (&gaIp6PrefixToMask[u1Count], pu1Mask,
                        IKE_IP6_ADDR_LEN) == IKE_ZERO)
        {
            *pu1Prefix = u1Count;
            return (IKE_SUCCESS);
        }
    }
    return (IKE_FAILURE);
}

INT1
IkeCreateDynCryptoMapForClient (tIkeSessionInfo * pSessionInfo)
{

    UINT1               au1CryptoMapName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1               au1RemoteAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1              *pu1PeerAddr = NULL;
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT1               u1RemotePrefix = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE MapNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE LocAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE RemAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE TransformsOctetStr;
    INT4                i4Pfs = IKE_ZERO;
    INT4                i4Mode = IKE_ZERO;
    INT4                i4LifetimeType = IKE_ZERO;
    UINT4               u4LifetimeKB = IKE_ZERO;
    UINT4               u4Lifetime = IKE_ZERO;
    UINT4               u4Addr = IKE_ZERO;
    UINT1              *pu1CryptoName = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    tIkeNetwork        *pIkeNetwork = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pu1PeerAddr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (au1CryptoMapName, MAX_NAME_LENGTH);
    IKE_BZERO (au1LocalAddr, IKE_MAX_ADDR_STR_LEN);
    IKE_BZERO (au1RemoteAddr, IKE_MAX_ADDR_STR_LEN);
    IKE_BZERO (pu1PeerAddr, IKE_MAX_ADDR_STR_LEN);

    if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        pIkeRAInfo = &(pSessionInfo->IkeTransInfo.IkeRAInfo);
        pIkeNetwork =
            &(pSessionInfo->IkeTransInfo.ProtectedNetwork[IKE_INDEX_0]);
    }
    else
    {
        pIkeRAInfo = &(pSessionInfo->Ike2AuthInfo.IkeRAInfo);
        pIkeNetwork =
            &(pSessionInfo->Ike2AuthInfo.ProtectedNetwork[IKE_INDEX_0]);
    }
    pu1CryptoName = (UINT1 *) au1CryptoMapName;
    /* Crypto Map name = Policy(Transform Set Name)- Remote IP */
    IKE_STRNCPY (pu1CryptoName, pIkeRAInfo->au1TransformSetBundleDisplay,
                 IKE_STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay));
    pu1CryptoName += IKE_STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay);

    /*Append a seperator '-'  at the end of policy name */
    IKE_STRNCPY (pu1CryptoName, "-", IKE_TWO);
    pu1CryptoName += IKE_ONE;

    u4Addr = pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr;
    u4Addr = IKE_HTONL (u4Addr);
    INET_NTOP4 (&u4Addr, au1RemoteAddr);

    IKE_STRNCPY (pu1CryptoName, au1RemoteAddr,
                 IKE_STRLEN (au1RemoteAddr) - IKE_ONE);

    EngineNameOctetStr.pu1_OctetList = pIkeRAInfo->au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (pIkeRAInfo->au1EngineName);
    MapNameOctetStr.pu1_OctetList = au1CryptoMapName;
    MapNameOctetStr.i4_Length = (INT4) STRLEN (au1CryptoMapName);

    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &MapNameOctetStr,
                                IKE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus == SNMP_ERR_INCONSISTENT_VALUE)
        {
            if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                                     IKE_DESTROY) == SNMP_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForClient: "
                             "Deleting the existing crypto map failed\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);

                return (IKE_FAILURE);
            }
        }
        else
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient:"
                         "nmhTestv2FsIkeCMStatus Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                             IKE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient:"
                     "nmhSetFsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    switch (pIkeRAInfo->PeerAddr.u4AddrType)
    {
        case IPV4ADDR:
        {
            u4Addr = pIkeRAInfo->PeerAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, pu1PeerAddr);
            break;
        }
        case IPV6ADDR:
        {
            INET_NTOP6 (&pIkeRAInfo->PeerAddr.Ipv6Addr, pu1PeerAddr);
            break;
        }
        default:
        {
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return IKE_FAILURE;
        }
    }

    PeerAddrOctetStr.pu1_OctetList = pu1PeerAddr;
    PeerAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (pu1PeerAddr);

    if (nmhTestv2FsIkeCMPeerAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                  &MapNameOctetStr,
                                  &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient:"
                     "nmhTestv2FsIkeCMPeerAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMPeerAddr (&EngineNameOctetStr, &MapNameOctetStr,
                               &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient:"
                     "nmhSetFsIkeCMPeerAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (pIkeRAInfo->bCMEnabled == FALSE)
    {
        switch (pSessionInfo->pIkeSA->IpLocalAddr.u4AddrType)
        {
            case IPV4ADDR:
            {
                u4Addr = pSessionInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
                u4Addr = IKE_HTONL (u4Addr);
                INET_NTOP4 (&u4Addr, au1LocalAddr);
                break;
            }
            case IPV6ADDR:
            {
                INET_NTOP6 (&pSessionInfo->pIkeSA->IpLocalAddr.Ipv6Addr,
                            au1LocalAddr);
                break;
            }
            default:
            {
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);
                return IKE_FAILURE;
            }
        }
        LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
        LocAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1LocalAddr);

        switch (pSessionInfo->RemoteTunnelTermAddr.u4AddrType)
        {
            case IPV4ADDR:
            {
                u4Addr = pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr;
                u4Addr = IKE_HTONL (u4Addr);
                INET_NTOP4 (&u4Addr, au1RemoteAddr);
                break;
            }
            case IPV6ADDR:
            {
                INET_NTOP6 (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr,
                            au1RemoteAddr);
                break;
            }
            default:
            {
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);
                return IKE_FAILURE;
            }
        }
        RemAddrOctetStr.pu1_OctetList = au1RemoteAddr;
        RemAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1RemoteAddr);
    }
    else
    {
        if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
        {
            switch (pSessionInfo->IkeTransInfo.InternalIpAddr.u4AddrType)
            {
                case IPV4ADDR:
                {
                    u4Addr = pSessionInfo->IkeTransInfo.InternalIpAddr.Ipv4Addr;
                    u4Addr = IKE_HTONL (u4Addr);
                    INET_NTOP4 (&u4Addr, au1LocalAddr);
                    break;
                }
                case IPV6ADDR:
                {
                    INET_NTOP6 (&pSessionInfo->IkeTransInfo.
                                InternalIpAddr.Ipv6Addr, au1LocalAddr);
                    break;
                }
                default:
                {
                    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                        (UINT1 *) pu1PeerAddr);
                    return IKE_FAILURE;
                }
            }
        }
        else
        {
            switch (pSessionInfo->Ike2AuthInfo.InternalIpAddr.u4AddrType)
            {
                case IPV4ADDR:
                {
                    u4Addr = pSessionInfo->Ike2AuthInfo.InternalIpAddr.Ipv4Addr;
                    u4Addr = IKE_HTONL (u4Addr);
                    INET_NTOP4 (&u4Addr, au1LocalAddr);
                    break;
                }
                case IPV6ADDR:
                {
                    INET_NTOP6 (&pSessionInfo->Ike2AuthInfo.
                                InternalIpAddr.Ipv6Addr, au1LocalAddr);
                    break;
                }
                default:
                {
                    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                        (UINT1 *) pu1PeerAddr);
                    return IKE_FAILURE;
                }
            }
        }
        LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
        LocAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1LocalAddr);

        if (pIkeNetwork->u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
        {
            IKE_MEMCPY (&u4Addr,
                        &(pIkeNetwork->uNetwork.Ip4Subnet.Ip4Addr),
                        sizeof (UINT4));
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1RemoteAddr);
            if (IkeMaskToPrefix (pIkeNetwork->uNetwork.Ip4Subnet.Ip4SubnetMask,
                                 &u1RemotePrefix) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForClient:"
                             "IkeMaskToPrefix Failed\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);
                return (IKE_FAILURE);
            }
        }
        else
        {
            INET_NTOP6 (&pIkeNetwork->uNetwork.Ip6Subnet.Ip6Addr,
                        au1RemoteAddr);
            if (Ikev6MaskToPrefix
                ((UINT1 *) &pIkeNetwork->uNetwork.Ip6Subnet.Ip6SubnetMask,
                 &u1RemotePrefix) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForClient:"
                             "IkeMaskToPrefix Failed\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);
                return (IKE_FAILURE);
            }
        }

        *(au1RemoteAddr + IKE_STRLEN (au1RemoteAddr)) = '/';
        IKE_SNPRINTF (((CHR1 *) (au1RemoteAddr + IKE_STRLEN (au1RemoteAddr))),
                      (IKE_MAX_ADDR_STR_LEN - IKE_STRLEN (au1RemoteAddr)),
                      "%d", u1RemotePrefix);

        RemAddrOctetStr.pu1_OctetList = au1RemoteAddr;
        RemAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1RemoteAddr);
    }

    if (nmhTestv2FsIkeCMLocalAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                   &MapNameOctetStr,
                                   &LocAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhTestv2FsIkeCMLocalAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMLocalAddr (&EngineNameOctetStr, &MapNameOctetStr,
                                &LocAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhSetFsIkeCMLocalAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhTestv2FsIkeCMRemAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &MapNameOctetStr,
                                 &RemAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhTestv2FsIkeCMRemAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMRemAddr (&EngineNameOctetStr, &MapNameOctetStr,
                              &RemAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhSetFsIkeCMRemAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    TransformsOctetStr.pu1_OctetList = pIkeRAInfo->au1TransformSetBundleDisplay;

    TransformsOctetStr.i4_Length =
        (INT4) STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay);

    if (nmhTestv2FsIkeCMTransformSetBundle (&u4SnmpErrorStatus,
                                            &EngineNameOctetStr,
                                            &MapNameOctetStr,
                                            &TransformsOctetStr) ==
        SNMP_FAILURE)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhTestv2FsIkeCMTransformSetBundle Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMTransformSetBundle (&EngineNameOctetStr,
                                         &MapNameOctetStr,
                                         &TransformsOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhSetFsIkeCMTransformSetBundle Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    i4Mode = (INT4) pIkeRAInfo->u1Mode;
    if (nmhTestv2FsIkeCMMode (&u4SnmpErrorStatus,
                              &EngineNameOctetStr,
                              &MapNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhTestv2FsIkeCMMode Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);

    }
    if (nmhSetFsIkeCMMode (&EngineNameOctetStr,
                           &MapNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient: "
                     "nmhTestv2FsIkeCMMode Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }
    if (pIkeRAInfo->u1Pfs != IKE_ZERO)
    {
        i4Pfs = (INT4) pIkeRAInfo->u1Pfs;
        if (nmhTestv2FsIkeCMPfs (&u4SnmpErrorStatus,
                                 &EngineNameOctetStr,
                                 &MapNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhTestv2FsIkeCMPfs Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMPfs (&EngineNameOctetStr,
                              &MapNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhSetFsIkeCMPfs Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }
    }

    if (pIkeRAInfo->u1LifeTimeType != IKE_ZERO)
    {

        i4LifetimeType = (INT4) pIkeRAInfo->u1LifeTimeType;

        if (nmhTestv2FsIkeCMLifeTimeType (&u4SnmpErrorStatus,
                                          &EngineNameOctetStr,
                                          &MapNameOctetStr,
                                          i4LifetimeType) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhTestv2FsIkeCMLifeTimeType Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMLifeTimeType (&EngineNameOctetStr,
                                       &MapNameOctetStr,
                                       i4LifetimeType) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhSetFsIkeCMLifeTimeType Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }

        /* Set the value of the lifetime */
        u4Lifetime = (UINT4) pIkeRAInfo->u4LifeTime;
        if (nmhTestv2FsIkeCMLifeTime (&u4SnmpErrorStatus,
                                      &EngineNameOctetStr,
                                      &MapNameOctetStr,
                                      u4Lifetime) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhTestv2FsIkeCMLifeTime Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }
        if (nmhSetFsIkeCMLifeTime (&EngineNameOctetStr,
                                   &MapNameOctetStr,
                                   u4Lifetime) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhSetFsIkeCMLifeTime Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }
    }

    if (pIkeRAInfo->u4LifeTimeKB != IKE_ZERO)
    {
        u4LifetimeKB = pIkeRAInfo->u4LifeTimeKB;
        if (nmhTestv2FsIkeCMLifeTimeKB (&u4SnmpErrorStatus,
                                        &EngineNameOctetStr,
                                        &MapNameOctetStr,
                                        u4LifetimeKB) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhTestv2FsIkeCMLifeTimeKB Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMLifeTimeKB (&EngineNameOctetStr,
                                     &MapNameOctetStr,
                                     u4LifetimeKB) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForClient: "
                         "nmhSetFsIkeCMLifeTimeKB Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return (IKE_FAILURE);
        }
    }
    /* Finally Set the CryptoMap as Dynamic */
    if (IkeUtilSetCryptoAsDynamic (&EngineNameOctetStr, &MapNameOctetStr,
                                   IKE_TRUE) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     " IkeUtilSetCryptoAsDynamic Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return (IKE_FAILURE);
    }

    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &MapNameOctetStr, IKE_ACTIVE) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient:"
                     "nmhTestv2FsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                             IKE_ACTIVE) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForClient:"
                     "nmhSetFsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return (IKE_FAILURE);
    }
    if (pSessionInfo->u1IkeVer == IKE2_MAJOR_VERSION)
    {
        if (IkeUpdateAccessPortInfo
            (&EngineNameOctetStr, &MapNameOctetStr,
             IKE_TS_MIN_PORT, IKE_TS_MAX_PORT,
             pSessionInfo->Ike2AuthInfo.ProtectedNetwork[IKE_INDEX_0].
             u2StartPort,
             pSessionInfo->Ike2AuthInfo.ProtectedNetwork[IKE_INDEX_0].
             u2EndPort) == IKE_FAILURE)
        {
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

            return IKE_FAILURE;
        }
    }
    if (IkeUpdateProtocolInfo (&EngineNameOctetStr, &MapNameOctetStr,
                               pIkeNetwork->u1HLProtocol) == IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);

        return IKE_FAILURE;
    }
    return (IKE_SUCCESS);
}

INT1
IkeCreateDynCryptoMapForServer (tIkeSessionInfo * pSessionInfo)
{

    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE MapNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE LocAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE RemAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE TransformsOctetStr;

    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1               au1RemoteAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1              *pu1PeerAddr = NULL;
    UINT1               au1CryptoMapName[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };

    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT1               u1LocalPrefix = IKE_ZERO;
    INT4                i4Pfs = IKE_ZERO;
    INT4                i4Mode = IKE_ZERO;
    INT4                i4LifetimeType = IKE_ZERO;
    UINT4               u4LifetimeKB = IKE_ZERO;
    UINT4               u4Lifetime = IKE_ZERO;
    UINT4               u4Addr = IKE_ZERO;

    if (MemAllocateMemBlock
        (IKE_UINT_BUF_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pu1PeerAddr) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    IKE_BZERO (au1CryptoMapName, (IKE_MAX_ADDR_STR_LEN));
    IKE_BZERO (au1LocalAddr, IKE_MAX_ADDR_STR_LEN);
    IKE_BZERO (au1RemoteAddr, IKE_MAX_ADDR_STR_LEN);
    IKE_BZERO (pu1PeerAddr, IKE_MAX_ADDR_STR_LEN);

    if (pSessionInfo->u1IkeVer == IKE_ISAKMP_VERSION)
    {
        pIkeRAInfo = &(pSessionInfo->IkeTransInfo.IkeRAInfo);
    }
    else
    {
        pIkeRAInfo = &(pSessionInfo->Ike2AuthInfo.IkeRAInfo);
    }
    EngineNameOctetStr.pu1_OctetList = pIkeRAInfo->au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (pIkeRAInfo->au1EngineName);

    /* Crypto Map name = Policy(Transform Set Name)-InternalIP */
    /* eg: Mypolicy-40.0.0.2 */
    IKE_STRNCPY (au1CryptoMapName, pIkeRAInfo->au1TransformSetBundleDisplay,
                 IKE_STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay));
    au1CryptoMapName[(IKE_STRLEN
                      (pIkeRAInfo->au1TransformSetBundleDisplay) + IKE_ONE)]
        = '\0';

    /*Append a seperator '-'  at the end of policy name */
    IKE_STRNCPY ((au1CryptoMapName +
                  IKE_STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay)),
                 "-", IKE_TWO);
    if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        INET_NTOP6 (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr,
                    au1RemoteAddr);
    }
    else
    {
        u4Addr = pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr;
        u4Addr = IKE_HTONL (u4Addr);
        INET_NTOP4 (&u4Addr, au1RemoteAddr);
    }

    IKE_STRNCPY ((au1CryptoMapName + IKE_STRLEN (au1CryptoMapName)),
                 au1RemoteAddr, IKE_STRLEN (au1RemoteAddr));

    MapNameOctetStr.pu1_OctetList = au1CryptoMapName;
    MapNameOctetStr.i4_Length = (INT4) IKE_STRLEN (au1CryptoMapName);

    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &MapNameOctetStr,
                                IKE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus == SNMP_ERR_INCONSISTENT_VALUE)
        {
            /* If it already exists delete and recreate */
            if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                                     IKE_DESTROY) == SNMP_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForServer: "
                             "Deletion of existing CM Failed\n");
                MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID,
                                    (UINT1 *) pu1PeerAddr);
                return (IKE_FAILURE);
            }

        }
        else
        {
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return IKE_FAILURE;
        }
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                             IKE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhSetFsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (pIkeRAInfo->PeerAddr.u4AddrType == IPV6ADDR)
    {
        INET_NTOP6 (&pIkeRAInfo->PeerAddr.Ipv6Addr, pu1PeerAddr);
    }
    else
    {
        u4Addr = pIkeRAInfo->PeerAddr.Ipv4Addr;
        u4Addr = IKE_HTONL (u4Addr);
        INET_NTOP4 (&u4Addr, pu1PeerAddr);
    }
    PeerAddrOctetStr.pu1_OctetList = pu1PeerAddr;
    PeerAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (pu1PeerAddr);

    if (nmhTestv2FsIkeCMPeerAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                  &MapNameOctetStr,
                                  &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     "nmhTestv2FsIkeCMPeerAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMPeerAddr (&EngineNameOctetStr, &MapNameOctetStr,
                               &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     "nmhSetFsIkeCMPeerAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (pIkeRAInfo->bCMEnabled == FALSE)
    {
        if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
        {
            INET_NTOP6 (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr,
                        au1RemoteAddr);
        }
        else
        {
            u4Addr = pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1RemoteAddr);
        }

        if (pIkeRAInfo->bVPNPolicyXAuthEnabled == TRUE)
        {
            if (pIkeRAInfo->CMServerInfo.ProtectedNetwork[IKE_INDEX_0].u4Type
                == IPV6ADDR)
            {
                if (Ikev6MaskToPrefix
                    ((UINT1 *)
                     &(pIkeRAInfo->CMServerInfo.
                       ProtectedNetwork[IKE_INDEX_0].
                       uNetwork.Ip4Subnet.
                       Ip4SubnetMask), &u1LocalPrefix) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCreateDynCryptoMapForServer:"
                                 "IkeMaskToPrefix Failed\n");
                    return (IKE_FAILURE);
                }
                INET_NTOP6
                    (&pIkeRAInfo->CMServerInfo.ProtectedNetwork[IKE_INDEX_0].
                     uNetwork.Ip6Subnet.Ip6Addr, au1LocalAddr);
            }
            else
            {
                if (IkeMaskToPrefix
                    (pIkeRAInfo->CMServerInfo.
                     ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip4Subnet.
                     Ip4SubnetMask, &u1LocalPrefix) == IKE_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeCreateDynCryptoMapForServer:"
                                 "IkeMaskToPrefix Failed\n");
                    return (IKE_FAILURE);
                }

                IKE_MEMCPY (&u4Addr,
                            &(pIkeRAInfo->CMServerInfo.
                              ProtectedNetwork[IKE_INDEX_0].
                              uNetwork.Ip4Subnet.Ip4Addr), sizeof (UINT4));

                u4Addr = IKE_HTONL (u4Addr);
                INET_NTOP4 (&u4Addr, au1LocalAddr);
            }
            *(au1LocalAddr + IKE_STRLEN (au1LocalAddr)) = '/';
            IKE_SNPRINTF (((CHR1 *) (au1LocalAddr + IKE_STRLEN (au1LocalAddr))),
                          (IKE_MAX_ADDR_STR_LEN - IKE_STRLEN (au1LocalAddr)),
                          "%d", u1LocalPrefix);
        }
        else
        {
            if (pSessionInfo->pIkeSA->IpLocalAddr.u4AddrType == IPV6ADDR)
            {
                INET_NTOP6 (&pSessionInfo->pIkeSA->IpLocalAddr.Ipv6Addr,
                            au1LocalAddr);
            }
            else
            {
                u4Addr = pSessionInfo->pIkeSA->IpLocalAddr.Ipv4Addr;
                u4Addr = IKE_HTONL (u4Addr);
                INET_NTOP4 (&u4Addr, au1LocalAddr);
            }
        }
        LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
        LocAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1LocalAddr);

        RemAddrOctetStr.pu1_OctetList = au1RemoteAddr;
        RemAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1RemoteAddr);
    }
    else
    {
        if (pIkeRAInfo->CMServerInfo.InternalIpAddr.u4AddrType == IPV6ADDR)
        {
            INET_NTOP6 (&pIkeRAInfo->CMServerInfo.InternalIpAddr.Ipv4Addr,
                        au1RemoteAddr);
        }
        else
        {
            u4Addr = pIkeRAInfo->CMServerInfo.InternalIpAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1RemoteAddr);
        }
        if (pIkeRAInfo->CMServerInfo.ProtectedNetwork[IKE_INDEX_0].u4Type
            == IPV6ADDR)
        {
            if (Ikev6MaskToPrefix
                ((UINT1 *) &(pIkeRAInfo->CMServerInfo.
                             ProtectedNetwork[IKE_INDEX_0].uNetwork.Ip6Subnet.
                             Ip6SubnetMask), &u1LocalPrefix) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForServer:"
                             "IkeMaskToPrefix Failed\n");
                return (IKE_FAILURE);
            }
            INET_NTOP6 (&pIkeRAInfo->CMServerInfo.ProtectedNetwork[IKE_INDEX_0].
                        uNetwork.Ip6Subnet.Ip6Addr, au1LocalAddr);
        }
        else
        {
            if (IkeMaskToPrefix (pIkeRAInfo->CMServerInfo.
                                 ProtectedNetwork[IKE_INDEX_0].
                                 uNetwork.Ip4Subnet.
                                 Ip4SubnetMask, &u1LocalPrefix) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeCreateDynCryptoMapForServer:"
                             "IkeMaskToPrefix Failed\n");
                return (IKE_FAILURE);
            }

            IKE_MEMCPY (&u4Addr, &(pIkeRAInfo->CMServerInfo.
                                   ProtectedNetwork[IKE_INDEX_0].
                                   uNetwork.Ip4Subnet.Ip4Addr), sizeof (UINT4));

            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1LocalAddr);
        }
        *(au1LocalAddr + IKE_STRLEN (au1LocalAddr)) = '/';
        IKE_SNPRINTF (((CHR1 *) (au1LocalAddr + IKE_STRLEN (au1LocalAddr))),
                      (IKE_MAX_ADDR_STR_LEN - IKE_STRLEN (au1LocalAddr)),
                      "%d", u1LocalPrefix);
        LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
        LocAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1LocalAddr);

        RemAddrOctetStr.pu1_OctetList = au1RemoteAddr;
        RemAddrOctetStr.i4_Length = (INT4) IKE_STRLEN (au1RemoteAddr);

    }
    if (nmhTestv2FsIkeCMLocalAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                   &MapNameOctetStr,
                                   &LocAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhTestv2FsIkeCMLocalAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMLocalAddr (&EngineNameOctetStr, &MapNameOctetStr,
                                &LocAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhSetFsIkeCMLocalAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhTestv2FsIkeCMRemAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &MapNameOctetStr,
                                 &RemAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhTestv2FsIkeCMRemAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMRemAddr (&EngineNameOctetStr, &MapNameOctetStr,
                              &RemAddrOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhSetFsIkeCMRemAddr Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    TransformsOctetStr.pu1_OctetList = pIkeRAInfo->au1TransformSetBundleDisplay;

    TransformsOctetStr.i4_Length =
        (INT4) STRLEN (pIkeRAInfo->au1TransformSetBundleDisplay);

    if (nmhTestv2FsIkeCMTransformSetBundle (&u4SnmpErrorStatus,
                                            &EngineNameOctetStr,
                                            &MapNameOctetStr,
                                            &TransformsOctetStr) ==
        SNMP_FAILURE)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhTestv2FsIkeCMTransformSetBundle Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMTransformSetBundle (&EngineNameOctetStr,
                                         &MapNameOctetStr,
                                         &TransformsOctetStr) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhSetFsIkeCMTransformSetBundle Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    i4Mode = (INT4) pIkeRAInfo->u1Mode;
    if (nmhTestv2FsIkeCMMode (&u4SnmpErrorStatus,
                              &EngineNameOctetStr,
                              &MapNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhTestv2FsIkeCMMode Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);

    }
    if (nmhSetFsIkeCMMode (&EngineNameOctetStr,
                           &MapNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer: "
                     "nmhTestv2FsIkeCMMode Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }
    if (pIkeRAInfo->u1Pfs != IKE_ZERO)
    {
        i4Pfs = (INT4) pIkeRAInfo->u1Pfs;
        if (nmhTestv2FsIkeCMPfs (&u4SnmpErrorStatus,
                                 &EngineNameOctetStr,
                                 &MapNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhTestv2FsIkeCMPfs Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMPfs (&EngineNameOctetStr,
                              &MapNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhSetFsIkeCMPfs Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }
    }

    if (pIkeRAInfo->u1LifeTimeType != IKE_ZERO)
    {
        i4LifetimeType = (INT4) pIkeRAInfo->u1LifeTimeType;

        if (nmhTestv2FsIkeCMLifeTimeType (&u4SnmpErrorStatus,
                                          &EngineNameOctetStr,
                                          &MapNameOctetStr,
                                          i4LifetimeType) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhTestv2FsIkeCMLifeTimeType Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMLifeTimeType (&EngineNameOctetStr,
                                       &MapNameOctetStr,
                                       i4LifetimeType) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhSetFsIkeCMLifeTimeType Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);;
        }

        /* Set the value of the lifetime */
        u4Lifetime = (UINT4) pIkeRAInfo->u4LifeTime;
        if (nmhTestv2FsIkeCMLifeTime (&u4SnmpErrorStatus,
                                      &EngineNameOctetStr,
                                      &MapNameOctetStr,
                                      u4Lifetime) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhTestv2FsIkeCMLifeTime Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }
        if (nmhSetFsIkeCMLifeTime (&EngineNameOctetStr,
                                   &MapNameOctetStr,
                                   u4Lifetime) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhSetFsIkeCMLifeTime Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }
    }

    if (pIkeRAInfo->u4LifeTimeKB != IKE_ZERO)
    {
        u4LifetimeKB = pIkeRAInfo->u4LifeTimeKB;
        if (nmhTestv2FsIkeCMLifeTimeKB (&u4SnmpErrorStatus,
                                        &EngineNameOctetStr,
                                        &MapNameOctetStr,
                                        u4LifetimeKB) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhTestv2FsIkeCMLifeTimeKB Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }

        if (nmhSetFsIkeCMLifeTimeKB (&EngineNameOctetStr,
                                     &MapNameOctetStr,
                                     u4LifetimeKB) == SNMP_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeCreateDynCryptoMapForServer: "
                         "nmhSetFsIkeCMLifeTimeKB Failed\n");
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return (IKE_FAILURE);
        }
    }
    /* Finally Set the CryptoMap as Dynamic */
    if (IkeUtilSetCryptoAsDynamic (&EngineNameOctetStr, &MapNameOctetStr,
                                   IKE_TRUE) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     " IkeUtilSetCryptoAsDynamic Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &MapNameOctetStr, IKE_ACTIVE) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     "nmhTestv2FsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &MapNameOctetStr,
                             IKE_ACTIVE) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeCreateDynCryptoMapForServer:"
                     "nmhSetFsIkeCMStatus Failed\n");
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return (IKE_FAILURE);
    }
    if (pSessionInfo->u1IkeVer == IKE2_MAJOR_VERSION)
    {
        if (IkeUpdateAccessPortInfo
            (&EngineNameOctetStr, &MapNameOctetStr,
             pSessionInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
             ProtectedNetwork[IKE_INDEX_0].u2StartPort,
             pSessionInfo->Ike2AuthInfo.IkeRAInfo.CMServerInfo.
             ProtectedNetwork[IKE_INDEX_0].u2EndPort,
             IKE_TS_MIN_PORT, IKE_TS_MAX_PORT) == IKE_FAILURE)
        {
            MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
            return IKE_FAILURE;
        }
    }
    if (IkeUpdateProtocolInfo (&EngineNameOctetStr, &MapNameOctetStr,
                               pIkeRAInfo->CMServerInfo.
                               ProtectedNetwork[IKE_INDEX_0].
                               u1HLProtocol) == IKE_FAILURE)
    {
        MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
        return IKE_FAILURE;
    }
    MemReleaseMemBlock (IKE_UINT_BUF_MEMPOOL_ID, (UINT1 *) pu1PeerAddr);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeUtilDelDynCryptoMapForServer                   */
/*  Description     : This function is used to Delete the crypto map    */
/*  Input(s)        : pu1EngineIndex, u4Spi                             */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/
INT1
IkeUtilDelDynCryptoMapForServer (UINT4 u4IkeEngineIndex, UINT4 u4Spi)
{

    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkeIpAddr          PeerIpAddr;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE CryptoMapOctStr;
    tVpnIpAddr          VpnIpAddr;

    pIkeEngine = IkeGetIkeEngineFromIndex (u4IkeEngineIndex);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessDelete::"
                     " Error Get IkeEngine from Index Failed\n");
        return IKE_FAILURE;

    }

    SLL_SCAN (&pIkeEngine->IkeCryptoMapList, pCryptoMap, tIkeCryptoMap *)
    {
        if (pCryptoMap->u4CurrSpi == u4Spi)
        {
            /* Crypto Map Found - Proceed to Delete It. */
            break;
        }
    }

    if (pCryptoMap == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteDynamicCryptoMapForServer: "
                     "No Create Map Available for deletion \n");
        return IKE_FAILURE;
    }

    /* Crypto Map name will be same as Policy and Transform   
       Set Name */
    EngineNameOctetStr.pu1_OctetList = pIkeEngine->au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (pIkeEngine->au1EngineName);

    CryptoMapOctStr.pu1_OctetList = pCryptoMap->au1CryptoMapName;
    CryptoMapOctStr.i4_Length = (INT4) STRLEN (pCryptoMap->au1CryptoMapName);

    IKE_MEMCPY (&PeerIpAddr, &pCryptoMap->PeerAddr, sizeof (tIkeIpAddr));

    MEMSET (&VpnIpAddr, IKE_ZERO, sizeof (tVpnIpAddr));

    if (gbCMServer == IKE_TRUE)
    {
        if (pCryptoMap->RemoteNetwork.u4Type == IPV4ADDR)
        {
            VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
            IKE_MEMCPY (&(VpnIpAddr.uIpAddr.Ip4Addr),
                        &(pCryptoMap->RemoteNetwork.uNetwork.Ip4Addr),
                        sizeof (UINT4));
        }
        else
        {
            VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
            IKE_MEMCPY (&(VpnIpAddr.uIpAddr.Ip6Addr),
                        &(pCryptoMap->RemoteNetwork.uNetwork.Ip6Addr),
                        sizeof (tIp6Addr));
        }
        VpnUtilDelAddrFromAllocList (&VpnIpAddr);
    }
    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &CryptoMapOctStr, IKE_DESTROY) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteDynamicCryptoMapForServer: "
                     "nmhTestv2FsIkeCMStatus Failed\n");
        return (IKE_FAILURE);
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &CryptoMapOctStr,
                             IKE_DESTROY) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeDeleteDynamicCryptoMapForServer: "
                     "nmhSetFsIkeCMStatus Failed\n");
        return (IKE_FAILURE);
    }

    /* Delete the Phase2Session for Peer If any */
    IkeDelPhase2SessSAForPeer (pIkeEngine, &PeerIpAddr);
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeUtilDelAllCryptoMapforPolicy                   */
/*  Description     : This function is used to delete all CryptoMap     */
/*                    related to the policy.                            */
/*  Input(s)        : EngineNameOctetStr - Engine Name                  */
/*                    PolicyNameOctStr - Policy Name                    */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/

INT1
IkeUtilDelAllCryptoMapforPolicy (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                                 tSNMP_OCTET_STRING_TYPE * pPolicyNameOctStr)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkeCryptoMap      *pTemp = NULL;
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE CryptoMapOctStr;
    tIkeIpAddr          PeerIpAddr;
    tVpnIpAddr          VpnIpAddr;

    MEMSET (&VpnIpAddr, IKE_ZERO, sizeof (tVpnIpAddr));

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUtilDelAllCryptoMapforPolicy: "
                     "IkeEngine with the Spec. name not available\n");
        return (IKE_FAILURE);
    }
    GIVE_IKE_SEM ();
    /* This macro allows deletion within the loop */
    SLL_DYN_OFFSET_SCAN (&pIkeEngine->IkeCryptoMapList, pCryptoMap, pTemp,
                         tIkeCryptoMap *)
    {
        TAKE_IKE_SEM ();
        if (MEMCMP
            (pPolicyNameOctStr->pu1_OctetList, pCryptoMap->au1CryptoMapName,
             (size_t) pPolicyNameOctStr->i4_Length) == IKE_ZERO)
        {
            /* In RA-VPN Case we will have crypto map installed dynalically 
             * for each client that is conencted to us. The Crypto map name 
             * will be as follows in this case - PolicyName-a.b.c.d , Where 
             * policy name is the Vpn Policy name and a.b.c.d is the private 
             * IP address allocated to the client */

            if (*((pCryptoMap->au1CryptoMapName +
                   IKE_STRLEN (pPolicyNameOctStr->pu1_OctetList))) != '-')
            {
                GIVE_IKE_SEM ();
                continue;
            }

            /* Fill the CryptoMap Name */
            CryptoMapOctStr.pu1_OctetList = pCryptoMap->au1CryptoMapName;
            CryptoMapOctStr.i4_Length =
                (INT4) IKE_STRLEN (pCryptoMap->au1CryptoMapName);

            IKE_MEMCPY (&PeerIpAddr, &pCryptoMap->PeerAddr,
                        sizeof (tIkeIpAddr));
            if (gbCMServer == IKE_TRUE)
            {
                if (pCryptoMap->RemoteNetwork.u4Type == IPV4ADDR)
                {
                    VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
                    IKE_MEMCPY (&(VpnIpAddr.Ipv4Addr),
                                &pCryptoMap->RemoteNetwork.uNetwork.Ip4Addr,
                                sizeof (UINT4));
                }
                else
                {
                    VpnIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
                    IKE_MEMCPY (&(VpnIpAddr.Ipv6Addr),
                                &pCryptoMap->RemoteNetwork.uNetwork.Ip6Addr,
                                sizeof (tIp6Addr));
                }
                /* Remove the IP address to the free pool */
                VpnUtilDelAddrFromAllocList (&VpnIpAddr);
            }
            GIVE_IKE_SEM ();
            if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, pEngineNameOctetStr,
                                        &CryptoMapOctStr,
                                        IKE_DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFsIkeCMStatus (pEngineNameOctetStr, &CryptoMapOctStr,
                                         IKE_DESTROY) == SNMP_FAILURE)
                {
                    continue;
                }
                /* Delete the Phase2Session for Peer If any */
                TAKE_IKE_SEM ();
                IkeDelPhase2SessSAForPeer (pIkeEngine, &PeerIpAddr);
                GIVE_IKE_SEM ();
            }
        }
        else
        {
            GIVE_IKE_SEM ();
        }
    }
    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   : IkeUtilSetCryptoAsDynamic                         */
/*  Description     : This function is used to Set the Crypto map as    */
/*                    Manual or Dynamic                                 */
/*  Input(s)        : pu1EngineName, pu1PeerId, i4PeerIdType,           */
/*                    u4Priority                                        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/
INT1
IkeUtilSetCryptoAsDynamic (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                           tSNMP_OCTET_STRING_TYPE * pCryptoMapOctStr,
                           BOOLEAN bIsDyanmic)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;

    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUtilSetCryptoAsDynamic: "
                     "IkeEngine with the Spec. name not available\n");
        return (IKE_FAILURE);
    }

    SLL_SCAN (&pIkeEngine->IkeCryptoMapList, pCryptoMap, tIkeCryptoMap *)
    {
        if (MEMCMP
            (pCryptoMapOctStr->pu1_OctetList, pCryptoMap->au1CryptoMapName,
             (size_t) pCryptoMapOctStr->i4_Length) == IKE_ZERO)
        {
            pCryptoMap->bIsCryptoDynamic = bIsDyanmic;
            return IKE_SUCCESS;
        }
    }
    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   : IkeUpdatePolicyPeerId                             */
/*  Description     : This function is used to update the peer Id       */
/*                    for IKE Policy                                    */
/*  Input(s)        : pu1EngineName, pu1PeerId, i4PeerIdType,           */
/*                    u4Priority                                        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/
INT1
IkeUpdatePeerIPInPolicy (tSNMP_OCTET_STRING_TYPE * pIkeEngineName,
                         UINT4 u4IkePolicyPriority,
                         tSNMP_OCTET_STRING_TYPE * pPeerAddrOctetStr)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;
    UINT1               u1AddrType = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    tIp4Addr            u4TempIp4Addr = IKE_ZERO;

    pPeerAddrOctetStr->pu1_OctetList[pPeerAddrOctetStr->i4_Length] = '\0';

    u1AddrType = IkeParseAddrString (pPeerAddrOctetStr->pu1_OctetList);

    if ((u1AddrType != IPV4ADDR) && (u1AddrType != IPV6ADDR))
    {
        return (SNMP_FAILURE);
    }

    pIkeEngine = IkeSnmpGetEngine (pIkeEngineName->pu1_OctetList,
                                   pIkeEngineName->i4_Length);
    if (pIkeEngine == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4IkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        pIkePolicy->PeerAddr.u4AddrType = u1AddrType;
        if (IPV4ADDR == u1AddrType)
        {
            i4RetVal = INET_PTON4 (pPeerAddrOctetStr->pu1_OctetList,
                                   &u4TempIp4Addr);
            pIkePolicy->PeerAddr.uIpAddr.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
            if (i4RetVal > IKE_ZERO)
            {
                return (SNMP_SUCCESS);
            }
        }

        if (IPV6ADDR == u1AddrType)
        {
            i4RetVal = INET_PTON6 (pPeerAddrOctetStr->pu1_OctetList,
                                   &pIkePolicy->PeerAddr.uIpAddr.Ip6Addr.
                                   u1_addr);
            if (i4RetVal > IKE_ZERO)
            {
                return (SNMP_SUCCESS);
            }
        }
        /* neither v4 nor v6 address */
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePeerIPInPolicy: Invalid Peer IP type \r\n");
        return (SNMP_FAILURE);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePeerIPInPolicy: Invalid Policy Index \r\n");
        return (SNMP_FAILURE);
    }
}

/************************************************************************/
/*  Function Name   : IkeUpdatePolicyPeerId                             */
/*  Description     : This function is used to update the peer Id       */
/*                    for IKE Policy                                    */
/*  Input(s)        : pu1EngineName, pu1PeerId, i4PeerIdType,           */
/*                    u4Priority                                        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/
INT1
IkeUpdatePolicyPeerId (UINT1 *pu1EngineName, UINT1 *pu1PeerId,
                       INT4 i4PeerIdType, UINT4 u4Priority)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (MEMCMP (pu1EngineName, pIkeEngine->au1EngineName,
                    STRLEN (pIkeEngine->au1EngineName)) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyPeerId: Engine does not exist \r\n");
        return (IKE_FAILURE);
    }

    if (u4Priority == IKE_NONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyPeerId: Invalid Policy Index \r\n");
        return (IKE_FAILURE);
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if ((STRCMP (pu1PeerId, "any") == IKE_ZERO) &&
            (pIkePolicy->PolicyPeerID.i4IdType == i4PeerIdType) &&
            (pIkePolicy->PolicyPeerID.uID.Ip4Addr == IKE_DEFAULT_IP))

        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeUpdatePolicyPeerId: Default policy already "
                         "exists \r\n");
            return (IKE_FAILURE);
        }
        if (pIkePolicy->u4Priority == u4Priority)
        {
            break;
        }
    }
    GIVE_IKE_SEM ();

    if (pIkePolicy == NULL)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyPeerId: Policy does not exist \r\n");
        return (IKE_FAILURE);
    }

    pIkePolicy->PolicyPeerID.i4IdType = i4PeerIdType;

    if (STRCMP (pu1PeerId, "any") == IKE_ZERO)
    {
        pIkePolicy->PolicyPeerID.uID.Ip4Addr = IKE_DEFAULT_IP;
    }
    else
    {

        switch (i4PeerIdType)
        {
            case IKE_KEY_IPV4:
            {
                if (INET_PTON4
                    (pu1PeerId, &pIkePolicy->PolicyPeerID.uID.Ip4Addr)
                    > IKE_ZERO)
                {
                    pIkePolicy->PolicyPeerID.uID.Ip4Addr =
                        IKE_NTOHL (pIkePolicy->PolicyPeerID.uID.Ip4Addr);

                    pIkePolicy->PolicyPeerID.u4Length = sizeof (tIp4Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeUpdatePolicyPeerId:\
                                 Invalid IPv4 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }

            case IKE_KEY_IPV6:
            {
                if (INET_PTON6 (pu1PeerId, &pIkePolicy->PolicyPeerID.
                                uID.Ip6Addr.u1_addr) > IKE_ZERO)
                {
                    pIkePolicy->PolicyPeerID.u4Length = sizeof (tIp6Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeUpdatePolicyPeerId:\
                                 Invalid IPv6 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }

            case IKE_KEY_EMAIL:
            {
                STRCPY (pIkePolicy->PolicyPeerID.uID.au1Email, pu1PeerId);
                pIkePolicy->PolicyPeerID.u4Length = STRLEN (pu1PeerId);
                break;
            }

            case IKE_KEY_FQDN:
            {
                STRCPY (pIkePolicy->PolicyPeerID.uID.au1Fqdn, pu1PeerId);
                pIkePolicy->PolicyPeerID.u4Length = STRLEN (pu1PeerId);
                break;
            }

            case IKE_KEY_DN:
            {
                STRCPY (pIkePolicy->PolicyPeerID.uID.au1Dn, pu1PeerId);
                pIkePolicy->PolicyPeerID.u4Length = STRLEN (pu1PeerId);
                break;
            }
            case IKE_KEY_KEYID:
            {
                STRCPY (pIkePolicy->PolicyPeerID.uID.au1KeyId, pu1PeerId);
                pIkePolicy->PolicyPeerID.u4Length = STRLEN (pu1PeerId);
                break;
            }

            default:
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeUpdatePolicyPeerId:\
                             ID Type not supported\r\n");
                return (IKE_FAILURE);

        }                        /* switch (i4PeerIdType) */
    }
    pIkePolicy->u1PeerIdMask = IKE_POLICY_PEER_ID_SET;
    return (IKE_SUCCESS);
}                                /* End of IkeUpdatePolicyPeerId (...) */

/************************************************************************/
/*  Function Name   : IkeUpdatePolicyLocalId                            */
/*  Description     : This function is used to update the Local Id      */
/*                    for IKE Policy                                    */
/*  Input(s)        : pu1EngineName, pu1LocalId, i4LocalidType          */
/*                    u4Priority                                        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                        */
/************************************************************************/
INT1
IkeUpdatePolicyLocalId (UINT1 *pu1EngineName, UINT1 *pu1LocalId,
                        INT4 i4LocalIdType, UINT4 u4Priority)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        if (MEMCMP (pu1EngineName, pIkeEngine->au1EngineName,
                    STRLEN (pIkeEngine->au1EngineName)) == IKE_ZERO)
        {
            break;
        }
    }

    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyLocalId: Engine does not exist \r\n");
        return (IKE_FAILURE);
    }

    if (u4Priority == IKE_NONE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyLocalId: Invalid Policy Index \r\n");
        return (IKE_FAILURE);
    }

    TAKE_IKE_SEM ();
    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if ((STRCMP (pu1LocalId, "any") == IKE_ZERO) &&
            (pIkePolicy->PolicyLocalID.i4IdType == i4LocalIdType) &&
            (pIkePolicy->PolicyLocalID.uID.Ip4Addr == IKE_DEFAULT_IP))

        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeUpdatePolicyLocalId: Default policy already "
                         "exists \r\n");
            return (IKE_FAILURE);
        }
        if (pIkePolicy->u4Priority == u4Priority)
        {
            break;
        }
    }
    GIVE_IKE_SEM ();

    if (pIkePolicy == NULL)
    {

        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyLocalId: Policy does not exist \r\n");
        return (IKE_FAILURE);
    }

    pIkePolicy->PolicyLocalID.i4IdType = i4LocalIdType;

    if (STRCMP (pu1LocalId, "any") == IKE_ZERO)
    {
        pIkePolicy->PolicyLocalID.uID.Ip4Addr = IKE_DEFAULT_IP;
    }
    else
    {

        switch (i4LocalIdType)
        {
            case IKE_KEY_IPV4:
            {
                if (INET_PTON4
                    (pu1LocalId, &pIkePolicy->PolicyLocalID.uID.Ip4Addr)
                    > IKE_ZERO)
                {
                    pIkePolicy->PolicyLocalID.uID.Ip4Addr =
                        IKE_NTOHL (pIkePolicy->PolicyLocalID.uID.Ip4Addr);

                    pIkePolicy->PolicyLocalID.u4Length = sizeof (tIp4Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeUpdatePolicyLocalId:\
                                 Invalid IPv4 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }

            case IKE_KEY_IPV6:
            {
                if (INET_PTON6 (pu1LocalId, &pIkePolicy->PolicyLocalID.
                                uID.Ip6Addr.u1_addr) > IKE_ZERO)
                {
                    pIkePolicy->PolicyLocalID.u4Length = sizeof (tIp6Addr);
                }
                else
                {
                    IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                                 "IkeUpdatePolicyLocalId:\
                                 Invalid IPv6 Address\n");
                    return (IKE_FAILURE);
                }
                break;
            }

            case IKE_KEY_EMAIL:
            {
                STRCPY (pIkePolicy->PolicyLocalID.uID.au1Email, pu1LocalId);
                pIkePolicy->PolicyLocalID.u4Length = STRLEN (pu1LocalId);
                break;
            }

            case IKE_KEY_FQDN:
            {
                STRCPY (pIkePolicy->PolicyLocalID.uID.au1Fqdn, pu1LocalId);
                pIkePolicy->PolicyLocalID.u4Length = STRLEN (pu1LocalId);
                break;
            }

            case IKE_KEY_DN:
            {
                STRCPY (pIkePolicy->PolicyLocalID.uID.au1Dn, pu1LocalId);
                pIkePolicy->PolicyLocalID.u4Length = STRLEN (pu1LocalId);
                break;
            }
            case IKE_KEY_KEYID:
            {
                STRCPY (pIkePolicy->PolicyLocalID.uID.au1KeyId, pu1LocalId);
                pIkePolicy->PolicyLocalID.u4Length = STRLEN (pu1LocalId);
                break;
            }

            default:
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeUpdatePolicyLocalId:\
                             ID Type not supported\r\n");
                return (IKE_FAILURE);

        }                        /* switch (i4LocalIdType) */
    }
    pIkePolicy->u1LocalIdMask = IKE_POLICY_LOCAL_ID_SET;
    return (IKE_SUCCESS);
}                                /* End of IkeUpdatePolicyLocalId (...) */

/************************************************************************/
/*  Function Name   : IkeDeleteIkeKeyMsr                                */
/*  Description     : This function is used to delete ike key command   */
/*                  : in naetra.conf when no ike key command is         */
/*                  : executed                                          */
/*  Input(s)        : pIkeKey:- Pointerto IKE Key                       */
/*  Output(s)       : Deletes ike key command from naetra.conf          */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeDeleteIkeKeyMsr (tIkeKey * pIkeKey)
{
    UINT1               au1Idtype[IKE_MAX_CMD_STR_LEN] = { IKE_ZERO };
    UINT1               au1IkeCmdStr[IKE_MAX_CMD_STR_LEN] = { IKE_ZERO };

    IKE_BZERO (au1IkeCmdStr, IKE_MAX_CMD_STR_LEN);

    switch (pIkeKey->KeyID.i4IdType)
    {
        case IKE_KEY_IPV4:
            IKE_STRCPY (au1Idtype, "ipv4");
            break;
        case IKE_KEY_IPV6:
            IKE_STRCPY (au1Idtype, "ipv6");
            break;
        case IKE_KEY_EMAIL:
            IKE_STRCPY (au1Idtype, "email");
            break;
        case IKE_KEY_FQDN:
            IKE_STRCPY (au1Idtype, "fqdn");
            break;
        case IKE_KEY_DN:
            IKE_STRCPY (au1Idtype, "dn");
        default:
            break;
    }
    SNPRINTF ((CHR1 *) au1IkeCmdStr, IKE_MAX_CMD_STR_LEN,
              "ike key %s idtype %s id %s", (CHR1 *) pIkeKey->au1KeyString,
              (CHR1 *) au1Idtype, (CHR1 *) pIkeKey->au1DisplayKeyId);
    /* MsrCliDeleteCmd (au1IkeCmdStr, CLI_IKE_MODE); */
    return;
}

INT1
IkeEngineInit (UINT1 *pu1InterfaceName)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, pu1InterfaceName, MAX_NAME_LENGTH - IKE_ONE);
    IkeCreateEngine (au1EngineName);

    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    /* The Engine is now ready to go to ACTIVE state */
    if (nmhTestv2FsIkeEngineRowStatus (&u4SnmpErrorStatus,
                                       &EngineNameOctetStr,
                                       IKE_ACTIVE) == SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "Ike",
                     "IkeEngineInit: Engine initialization failed\n");
        return OSIX_FAILURE;
    }

    if (nmhSetFsIkeEngineRowStatus (&EngineNameOctetStr, IKE_ACTIVE) ==
        SNMP_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "Ike",
                     "IkeEngineInit: Engine initialization failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
IkeCreateEngine (UINT1 *pu1EngineName)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4SnmpErrorStatus = IKE_ZERO;

    OctetStr.pu1_OctetList = pu1EngineName;
    OctetStr.i4_Length = (INT4) STRLEN (pu1EngineName);

    if (nmhTestv2FsIkeEngineRowStatus (&u4SnmpErrorStatus,
                                       &OctetStr,
                                       IKE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIkeEngineRowStatus (&OctetStr, IKE_CREATEANDWAIT) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Restore the saved Certificate DB */
    if (IkeReadCertConfig (pu1EngineName) == IKE_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeDeleteEngine                                  */
/*  Description     : This function deletes the Ike Engine specified    */
/*                  : with the name as argument.                        */
/*  Input(s)        : pu1EngineName - EngineName                        */
/*  Output(s)       : None                                              */
/*  Returns         : IKE_SUCCESS/IKE_FAILURE                           */
/************************************************************************/

INT4
IkeDeleteEngineContents (UINT1 *pu1EngineName)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4SnmpErrorStatus = IKE_ZERO;

    OctetStr.pu1_OctetList = pu1EngineName;
    OctetStr.i4_Length = (INT4) STRLEN (pu1EngineName);

    if (nmhTestv2FsIkeEngineRowStatus (&u4SnmpErrorStatus,
                                       &OctetStr, IKE_DESTROY) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeEngineRowStatus (&OctetStr, IKE_DESTROY) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeGetActiveSessions                              */
/*  Description     : This function is gets the total no.of             */
/*                  : of active sessions                                */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkeActiveSessions                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetActiveSessions (INT4 i4Index, UINT4 *pu4IkeActiveSessions)
{
    *pu4IkeActiveSessions = gpIkeStatisticsInfo[i4Index].u4IkeActiveSessions;

}

/************************************************************************/
/*  Function Name   : IkeGetNoOfPhase1SessionsSucc                      */
/*  Description     : This function is gets the total no.of             */
/*                  : of negotiated sessions                            */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkeNoOfPhase1SessionsSucc                      */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfPhase1SessionsSucc (INT4 i4Index, UINT4 *pu4IkeNoOfPhase1SessionsSucc)
{
    *pu4IkeNoOfPhase1SessionsSucc =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfPhase1SessionsSucceed;
}

/************************************************************************/
/*  Function Name   : GetIkeNoOfSessionsFailure                         */
/*  Description     : This function is gets the total no.of             */
/*                  : of active sessions                                */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkeNoOfPhase1SessionsFailure                        */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfPhase1SessionFailed (INT4 i4Index,
                               UINT4 *pu4IkeNoOfPhase1SessionsFailure)
{
    *pu4IkeNoOfPhase1SessionsFailure =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfPhase1SessionsFailed;
}

/************************************************************************/
/*  Function Name   : IkeGetNoOfSessionsSucc                            */
/*  Description     : This function is gets the total no.of             */
/*                  : of negotiated sessions                            */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkeNoOfSessionsSucc                            */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfSessionsSucc (INT4 i4Index, UINT4 *pu4IkeNoOfSessionsSucc)
{
    *pu4IkeNoOfSessionsSucc =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsSucceed;
}

/************************************************************************/
/*  Function Name   : IkeGetNoOfSessionsSucc                            */
/*  Description     : This function is gets the total no.of             */
/*                  : of negotiated sessions                            */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkeNoOfSessionsSucc                            */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfSessionsFail (INT4 i4Index, UINT4 *pu4IkeNoOfSessionsFail)
{
    *pu4IkeNoOfSessionsFail =
        gpIkeStatisticsInfo[i4Index].u4IkeNoOfSessionsFailed;
}

/************************************************************************/
/*  Function Name   : IkeGetNoOfIkePhase1Rekeys                         */
/*  Description     : This function is gets the total no.of             */
/*                  : of phase1 rekeys                                  */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkePhase1Rekeys                                */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfIkePhase1Rekeys (INT4 i4Index, UINT4 *pu4IkePhase1Rekeys)
{
    *pu4IkePhase1Rekeys = gpIkeStatisticsInfo[i4Index].u4IkePhase1Rekey;
}

/************************************************************************/
/*  Function Name   : IkeGetNoOfIkePhase2Rekeys                         */
/*  Description     : This function is gets the total no.of             */
/*                  : of phase2 rekeys                                  */
/*  Input(s)        : i4Index                                           */
/*  Output(s)       : pu4IkePhase2Rekeys                                */
/*  Returns         : None                                              */
/************************************************************************/
VOID
IkeGetNoOfIkePhase2Rekeys (INT4 i4Index, UINT4 *pu4IkePhase2Rekeys)
{
    *pu4IkePhase2Rekeys = gpIkeStatisticsInfo[i4Index].u4IkePhase2Rekey;
}

/***************************************************************************/
/*  Function Name   : IkeGetActiveSACount                                  */
/*  Description     : This function returns the total active SAs           */
/*  Input(s)        : u4IPSecSAsActive :Refers to the SA count             */
/*  Output(s)       : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/

VOID
IkeGetActiveSACount (UINT4 *u4IkeActiveSA)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4Count = IKE_ZERO;

    *u4IkeActiveSA = IKE_ZERO;
    SLL_SCAN (&gIkeEngineList, pIkeEngine, tIkeEngine *)
    {
        u4Count = IKE_ZERO;
        u4Count = SLL_COUNT (&(pIkeEngine->IkeSAList));
        *u4IkeActiveSA += u4Count;
    }
    return;
}

/************************************************************************/
/*  Function Name   : IkeUtilUpdatePolicyName                           */
/*  Description     : This function is used to update the VPN Policy    */
/*                  : Name in IKE DataStructure                         */
/*  Input(s)        : pEngineNameOctetStr - Engine Name                 */
/*                  : u4IkePolicyPriority - PolicyPriority              */
/*                  : pPolicyNameOctStr - PolicyName                    */
/*  Output(s)       : None                                              */
/*  Returns         : Success/Failure                                   */
/************************************************************************/
INT4
IkeUtilUpdatePolicyName (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                         UINT4 u4IkePolicyPriority,
                         tSNMP_OCTET_STRING_TYPE * pPolicyNameOctStr)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyName: Engine does not exist\r\n");
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4IkePolicyPriority);
    if (pIkePolicy != NULL)
    {
        MEMSET (pIkePolicy->au1PolicyName, IKE_ZERO, MAX_NAME_LENGTH);

        SNPRINTF ((CHR1 *) pIkePolicy->au1PolicyName, MAX_NAME_LENGTH,
                  "%s", pPolicyNameOctStr->pu1_OctetList);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUpdatePolicyName: Invalid Policy Priority \r\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IkeUtilGetPolicyPriotityFromName                  */
/*  Description     : This function is used to get the Policy priority  */
/*                  : given the Engine and PolicyName                   */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pPolicyNameOctStr - PolicyName                    */
/*                  : pu4IkePolicyPriority - PolicyPriority             */
/*  Output(s)       : None                                              */
/*  Returns         : Success/Failure                                   */
/************************************************************************/
INT4
IkeUtilGetPolicyPriotityFromName (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                                  tSNMP_OCTET_STRING_TYPE * pPolicyNameOctStr,
                                  UINT4 *pu4IkePolicyPriority)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeUtilGetPolicyPriotityFromName:"
                     "Engine does not exist\r\n");
        return IKE_FAILURE;
    }

    SLL_SCAN (&(pIkeEngine->IkePolicyList), pIkePolicy, tIkePolicy *)
    {
        if (IKE_MEMCMP
            (pIkePolicy->au1PolicyName, pPolicyNameOctStr->pu1_OctetList,
             pPolicyNameOctStr->i4_Length) == IKE_ZERO)
        {
            *pu4IkePolicyPriority = pIkePolicy->u4Priority;
            GIVE_IKE_SEM ();
            return IKE_SUCCESS;
        }
    }
    GIVE_IKE_SEM ();
    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeUtilGetPolicyPriotityFromName:"
                 "Policy does not exist!!\r\n");
    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   : IkeSetIkeVersion                                  */
/*  Description     : This function is used to set the Ike Version in   */
/*                  : Ike Policy Data Structure.                        */
/*  Input(s)        : pIkeEngine - Pointer to the Engine structure      */
/*                  : pu4IkePolicyPriority - PolicyPriority             */
/*                  : u1IkeVersion - Ike Version                        */
/*  Output(s)       : None                                              */
/*  Returns         : Success/Failure                                   */
/************************************************************************/
INT1
IkeSetIkeVersion (tSNMP_OCTET_STRING_TYPE * pIkeEngineName,
                  UINT4 u4IkePolicyPriority, UINT1 u1IkeVersion)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkePolicy         *pIkePolicy = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pIkeEngineName->pu1_OctetList,
                                   pIkeEngineName->i4_Length);

    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetIkeVersion : Engine not found\n");
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    pIkePolicy = IkeSnmpGetPolicy (pIkeEngine, u4IkePolicyPriority);
    if (pIkePolicy == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSetIkeVersion : Policy not found\n");
        return IKE_FAILURE;
    }
    TAKE_IKE_SEM ();
    pIkePolicy->u1IkeVersion = u1IkeVersion;
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeUpdateAccessPortInfo                             */
/*  Description     : This function is used to update Port details in     */
/*                  : Ike crypto map Data Structure.                      */
/*  Input(s)        : pEngineNameOctetStr-Pointer to the Engine structure */
/*                  : pCryptoMapOctStr - Pointer to Crypto Map Structure  */
/*                  : u2LocalStartPort - Local Start Port                 */
/*                  : u2LocalEndPort - Local Start Port                   */
/*                  : u2RemoteStartPort - Local Start Port                */
/*                  : u2RemoteEndPort - Local Start Port                  */
/*  Output(s)       : None                                                */
/*  Returns         : Success/Failure                                     */
/**************************************************************************/
INT1
IkeUpdateAccessPortInfo (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                         tSNMP_OCTET_STRING_TYPE * pCryptoMapOctStr,
                         UINT2 u2LocalStartPort, UINT2 u2LocalEndPort,
                         UINT2 u2RemoteStartPort, UINT2 u2RemoteEndPort)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pCryptoMapOctStr->pu1_OctetList,
                                         pCryptoMapOctStr->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        TAKE_IKE_SEM ();
        pIkeCryptoMap->LocalNetwork.u2StartPort = u2LocalStartPort;
        pIkeCryptoMap->LocalNetwork.u2EndPort = u2LocalEndPort;
        pIkeCryptoMap->RemoteNetwork.u2StartPort = u2RemoteStartPort;
        pIkeCryptoMap->RemoteNetwork.u2EndPort = u2RemoteEndPort;
        GIVE_IKE_SEM ();
        return IKE_SUCCESS;
    }
    return IKE_FAILURE;
}

/**************************************************************************/
/*  Function Name   : IkeUpdateProtocolInfo                               */
/*  Description     : This function is used to update Protocol details in */
/*                  : Ike crypto map Data Structure.                      */
/*  Input(s)        : pEngineNameOctetStr-Pointer to the Engine structure */
/*                  : pCryptoMapOctStr - Pointer to Crypto Map Structure  */
/*                  : u4IkeHLProtocol - Higher Layer Protocol             */
/*  Output(s)       : None                                                */
/*  Returns         : Success/Failure                                     */
/**************************************************************************/
INT1
IkeUpdateProtocolInfo (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                       tSNMP_OCTET_STRING_TYPE * pCryptoMapOctStr,
                       UINT4 u4IkeHLProtocol)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pIkeCryptoMap = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    pIkeCryptoMap = IkeSnmpGetCryptoMap (pIkeEngine,
                                         pCryptoMapOctStr->pu1_OctetList,
                                         pCryptoMapOctStr->i4_Length);
    if (pIkeCryptoMap != NULL)
    {
        TAKE_IKE_SEM ();
        if ((u4IkeHLProtocol == IKE_PROTO_TCP) ||
            (u4IkeHLProtocol == IKE_PROTO_UDP) ||
            (u4IkeHLProtocol == IKE_PROTO_ICMP))
        {
            pIkeCryptoMap->LocalNetwork.u1HLProtocol = (UINT1) u4IkeHLProtocol;
            pIkeCryptoMap->RemoteNetwork.u1HLProtocol = (UINT1) u4IkeHLProtocol;
        }
        else
        {
            pIkeCryptoMap->LocalNetwork.u1HLProtocol = IKE_PROTO_ANY;
            pIkeCryptoMap->RemoteNetwork.u1HLProtocol = IKE_PROTO_ANY;
        }
        GIVE_IKE_SEM ();
        return IKE_SUCCESS;
    }
    return IKE_FAILURE;
}

/**************************************************************************/
/*  Function Name   : IkeUpdateRaPortInfo                                 */
/*  Description     : This function is used to update Port details in     */
/*                  : Ike crypto map Data Structure.                      */
/*  Input(s)        : pEngineNameOctetStr-Pointer to the Engine structure */
/*                  : pCryptoMapOctStr - Pointer to Crypto Map Structure  */
/*                  : u2LocalStartPort - Local Start Port                 */
/*                  : u2LocalEndPort - Local Start Port                   */
/*                  : u2RemoteStartPort - Local Start Port                */
/*                  : u2RemoteEndPort - Local Start Port                  */
/*  Output(s)       : None                                                */
/*  Returns         : Success/Failure                                     */
/**************************************************************************/
INT1
IkeUpdateRaPortInfo (tSNMP_OCTET_STRING_TYPE * pEngineNameOctetStr,
                     tSNMP_OCTET_STRING_TYPE * pProcNetAddrOctStr,
                     UINT2 u2LocalStartPort, UINT2 u2LocalEndPort,
                     UINT4 u4IkeHLProtocol)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeRAProtectNet   *pIkeRAProtectNet = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pEngineNameOctetStr->pu1_OctetList,
                                   pEngineNameOctetStr->i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();
    pIkeRAProtectNet = IkeSnmpGetProtectNet (pIkeEngine,
                                             pProcNetAddrOctStr->
                                             pu1_OctetList,
                                             pProcNetAddrOctStr->i4_Length);
    if (pIkeRAProtectNet != NULL)
    {
        TAKE_IKE_SEM ();
        pIkeRAProtectNet->ProtectedNetwork.u2StartPort = u2LocalStartPort;
        pIkeRAProtectNet->ProtectedNetwork.u2EndPort = u2LocalEndPort;
        if ((u4IkeHLProtocol == IKE_PROTO_TCP) ||
            (u4IkeHLProtocol == IKE_PROTO_UDP) ||
            (u4IkeHLProtocol == IKE_PROTO_ICMP))
        {
            pIkeRAProtectNet->ProtectedNetwork.u1HLProtocol =
                (UINT1) u4IkeHLProtocol;
        }
        else
        {
            pIkeRAProtectNet->ProtectedNetwork.u1HLProtocol = IKE_PROTO_ANY;
        }

        GIVE_IKE_SEM ();
        return IKE_SUCCESS;
    }
    return IKE_FAILURE;
}

/**************************************************************************/
/*  Function Name   : IkeCreateVpncInterface                              */
/*  Description     : This function is used to create vpnc interface      */
/*                  :                                                     */
/*  Input(s)        : pSessionInfo - Session Info                         */
/*                  :                                                     */
/*  Output(s)       : None                                                */
/*  Returns         : Success/Failure                                     */
/**************************************************************************/
INT1
IkeCreateVpncInterface (tIkeSessionInfo * pSessionInfo)
{
    tIPSecBundle       *pIPSecBundle = NULL;
    tIkeIpv4Subnet     *pLocalv4Subnet = NULL;
    tIkeIpv6Subnet     *pLocalv6Subnet = NULL;
    tIkeIpv4Subnet     *pRemoteSubnet = NULL;
    tIkeIpAddr          VpncAddr;
    UINT4               u4LocalMask = IKE_ZERO;
    UINT4               u4RemoteNetwork = IKE_ZERO;
    UINT4               u4RemoteMask = IKE_ZERO;
    UINT4               u4VpnIfIndex = IKE_ZERO;
    MEMSET (&VpncAddr, IKE_ZERO, sizeof (tIkeIpAddr));

    if (pSessionInfo->u1IkeVer == IKE2_MAJOR_VERSION)
    {
        pIPSecBundle = &pSessionInfo->Ike2AuthInfo.IPSecBundle;
    }
    else
    {
        pIPSecBundle = &pSessionInfo->IkePhase2Info.IPSecBundle;
    }
    /* Store the IP Address which should be assigned to VPN */
    if (pIPSecBundle->LocalProxy.u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        if (pIPSecBundle->bVpnServer == FALSE)
        {
            VpncAddr.u4AddrType = IPV4ADDR;
            VpncAddr.Ipv4Addr = pIPSecBundle->LocalProxy.uNetwork.Ip4Addr;
            u4LocalMask = IKE_MASK;
        }
    }
    else if (pIPSecBundle->LocalProxy.u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        pLocalv4Subnet = &(pIPSecBundle->LocalProxy.uNetwork.Ip4Subnet);
        if (pIPSecBundle->bVpnServer == FALSE)
        {
            VpncAddr.u4AddrType = IPV4ADDR;
            VpncAddr.Ipv4Addr =
                (pLocalv4Subnet->Ip4Addr & pLocalv4Subnet->Ip4SubnetMask);
            u4LocalMask = pLocalv4Subnet->Ip4SubnetMask;
        }
    }
    else if (pIPSecBundle->LocalProxy.u4Type == IKE_IPSEC_ID_IPV6_ADDR)
    {
        if (pIPSecBundle->bVpnServer == FALSE)
        {
            VpncAddr.u4AddrType = IPV6ADDR;
            IKE_MEMCPY (&(VpncAddr.Ipv6Addr), &(pIPSecBundle->LocalProxy.
                                                uNetwork.Ip6Addr),
                        sizeof (tIp6Addr));
            u4LocalMask = IKE_MASK;
        }
    }
    else if (pIPSecBundle->LocalProxy.u4Type == IKE_IPSEC_ID_IPV6_SUBNET)
    {
        pLocalv6Subnet = &(pIPSecBundle->LocalProxy.uNetwork.Ip6Subnet);
        if (pIPSecBundle->bVpnServer == FALSE)
        {
            VpncAddr.u4AddrType = IPV6ADDR;
            IKE_MEMCPY (&(VpncAddr.Ipv6Addr), &(pLocalv6Subnet->Ip6Addr),
                        sizeof (tIp6Addr));
            Ikev6MaskToPrefix ((UINT1 *) &pLocalv6Subnet->Ip6SubnetMask,
                               (UINT1 *) &u4LocalMask);
        }
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreDoPostChildExch: :Unsupported Remote Proxy "
                     "Network Typed\n");
        return (IKE_FAILURE);
    }

    if (pIPSecBundle->RemoteProxy.u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        u4RemoteNetwork = pIPSecBundle->RemoteProxy.uNetwork.Ip4Addr;
        u4RemoteMask = IKE_MASK;
    }
    else if (pIPSecBundle->RemoteProxy.u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        pRemoteSubnet = &(pIPSecBundle->RemoteProxy.uNetwork.Ip4Subnet);
        u4RemoteNetwork = pRemoteSubnet->Ip4Addr & pRemoteSubnet->Ip4SubnetMask;
        u4RemoteMask = pRemoteSubnet->Ip4SubnetMask;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreDoPostChildExch: :Unsupported Remote Proxy "
                     "Network Typed\n");
        return (IKE_FAILURE);
    }
#ifdef VPN_WANTED
    u4VpnIfIndex =
        (UINT4) CfaIfmCreateDynamicVpncInterface (VpncAddr.uIpAddr.Ip4Addr,
                                                  u4LocalMask, IKE_ZERO,
                                                  u4RemoteNetwork,
                                                  u4RemoteMask);
    if ((INT4) u4VpnIfIndex == CFA_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Ike2CoreDoPostChildExch: CfaIfmCreateDynamicVpncInterface"
                     " Failed\n");
        return (IKE_FAILURE);
    }
#endif
    pIPSecBundle->u4VpnIfIndex = u4VpnIfIndex;
    return (IKE_SUCCESS);
}

INT4
IkeUtilGetFreeAddrFromPool (tIkeIpAddr * pIkePeerAddr,
                            tIkeIpAddr * pIkeFreeIpAddr, UINT4 *pu4PrefixLen)
{
    tVpnIpAddr          VpnPeerAddr;
    tVpnIpAddr          VpnFreeIpAddr;

    MEMSET (&VpnPeerAddr, IKE_ZERO, sizeof (tVpnIpAddr));
    MEMSET (&VpnFreeIpAddr, IKE_ZERO, sizeof (tVpnIpAddr));

    if (pIkePeerAddr->u4AddrType == IPV4ADDR)
    {
        MEMCPY (&(VpnPeerAddr.uIpAddr.Ip4Addr), &(pIkePeerAddr->Ipv4Addr),
                sizeof (UINT4));
        VpnPeerAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        MEMCPY (&(VpnPeerAddr.uIpAddr.Ip6Addr), &(pIkePeerAddr->Ipv6Addr),
                sizeof (tIp6Addr));
        VpnPeerAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
    }

    if (VpnUtilGetFreeAddrFromPool (&VpnPeerAddr, &VpnFreeIpAddr,
                                    pu4PrefixLen) == VPN_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (pIkePeerAddr->u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pIkeFreeIpAddr->Ipv4Addr), &(VpnFreeIpAddr.uIpAddr.Ip4Addr),
                sizeof (UINT4));
        pIkeFreeIpAddr->u4AddrType = IPV4ADDR;
    }
    else
    {
        MEMCPY (&(pIkeFreeIpAddr->Ipv6Addr), &(VpnFreeIpAddr.uIpAddr.Ip6Addr),
                sizeof (tIp6Addr));
        pIkeFreeIpAddr->u4AddrType = IPV6ADDR;
    }

    return IKE_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeDeleteAllKeys                                    */
/*  Description     : This function deletes all keys in IKE by deleting   */
/*                  : rsa, dsa, ike key, sa and session datastructures    */
/*                  : maintained in the IkeEngine.                        */
/*  Input(s)        : None                                                */
/*  Output(s)       : None                                                */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                          */
/**************************************************************************/
INT4
IkeDeleteAllKeys (VOID)
{
    UINT1               au1EngineName[MAX_NAME_LENGTH + IKE_ONE] = { IKE_ZERO };
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN] = { IKE_ZERO };
    tIkeEngine         *pIkeEngine = NULL;
    tVpnIdInfo         *pVpnIdInfo = NULL;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4NumOfVpnRemoteEntries = IKE_ZERO;
    UINT4               u4EngineIndex = IKE_ZERO;

    MEMSET (au1FileName, IKE_ZERO, sizeof (au1FileName));

    STRCPY (au1FileName, PRESHARED_KEY_SAVE_FILE);
    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     PRESHARED_KEY_SAVE_FILE, au1FileName);
    TAKE_IKE_SEM ();
    u4NumOfVpnRemoteEntries = TMO_SLL_Count (&gVpnRemoteIdList);
    for (u4Count = IKE_ZERO; u4Count < u4NumOfVpnRemoteEntries; u4Count++)
    {
        pVpnIdInfo = (tVpnIdInfo *) TMO_SLL_First (&gVpnRemoteIdList);
        if (pVpnIdInfo == NULL)
        {
            break;
        }
        TMO_SLL_Delete (&gVpnRemoteIdList, (tTMO_SLL_NODE *) pVpnIdInfo);
        MEMSET (VPN_ID_KEY (pVpnIdInfo), IKE_ZERO,
                VPN_MAX_PRESHARED_KEY_LEN + IKE_ONE);
        MEM_FREE (pVpnIdInfo);
    }

    for (u4EngineIndex = IKE_ZERO; u4EngineIndex < gu4NumIkeEngines;
         u4EngineIndex += IKE_ONE)
    {

        MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

        STRNCPY (au1EngineName, IKE_GET_ENGINE_NAME (u4EngineIndex),
                 MAX_NAME_LENGTH);

        pIkeEngine =
            IkeSnmpGetEngine (au1EngineName, (INT4) STRLEN (au1EngineName));
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        SLL_DELETE_LIST (&(pIkeEngine->IkeKeyList), IkeFreeKeyInfo);
        SLL_DELETE_LIST (&(pIkeEngine->IkeRsaKeyList), IkeFreeRsaKeyInfo);
        SLL_DELETE_LIST (&(pIkeEngine->IkeSessionList), IkeFreeSessionInfo);
        SLL_DELETE_LIST (&(pIkeEngine->IkeSAList), IkeFreeSAInfo);
        SLL_DELETE_LIST (&(pIkeEngine->IkeDsaKeyList), IkeFreeDsaKeyInfo);
        SLL_DELETE_LIST (&(pIkeEngine->MyCerts), IkeFreeCertDBEntry);
        SLL_DELETE_LIST (&(pIkeEngine->CACerts), IkeFreeCertDBEntry);
    }

    /* If file exists, Keys written in the file are removed *
     * If file doesn't exist there is no need to flush the contents */

    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SECURE_MEM_DELETE,
                     au1FileName);

    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeWritePreSharedKeyToNVRAM                         */
/*  Description     : This function writes the preshared key into file    */
/*  Input(s)        : None                                                */
/*  Output(s)       : None                                                */
/*  Returns         : IKE_SUCCESS or IKE_FAILURE                          */
/**************************************************************************/
INT4
IkeWritePreSharedKeyToNVRAM (VOID)
{
    UINT1               au1EngineName[MAX_NAME_LENGTH + IKE_ONE] = { IKE_ZERO };
    UINT1               au1TempBuff[KEY_FILE_LINE_LENGTH] = { IKE_ZERO };
    tIkeEngine         *pIkeEngine = NULL;
    tIkeKey            *pIkeKey = NULL;
    UINT1              *pu1Buff = NULL;
    UINT4               u4Offset = IKE_ZERO;
    UINT4               u4EngineIndex = IKE_ZERO;
    UINT4               u4EngineEntryIndex = IKE_ZERO;
    INT1                i1Length = IKE_ZERO;

    TAKE_IKE_SEM ();

    for (u4EngineEntryIndex = IKE_ZERO; u4EngineEntryIndex < gu4NumIkeEngines;
         u4EngineEntryIndex += IKE_ONE)
    {
        MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

        u4EngineIndex = IKE_GET_ENGINE_INDEX (u4EngineEntryIndex);
        STRNCPY (au1EngineName, IKE_GET_ENGINE_NAME (u4EngineEntryIndex),
                 MAX_NAME_LENGTH);

        pIkeEngine =
            IkeSnmpGetEngine (au1EngineName, (INT4) STRLEN (au1EngineName));
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            break;
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Buff) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeWritePreSharedKeyToNVRAM: unable to allocate "
                         "buffer\n");
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pu1Buff) == MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeWritePreSharedKeyToNVRAM: unable to allocate "
                         "buffer\n");
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        /* Type of remote-id, remote-id and preshared key is written 
         * in the file */
        SLL_SCAN (&(pIkeEngine->IkeKeyList), pIkeKey, tIkeKey *)
        {
            MEMSET (au1TempBuff, IKE_ZERO, KEY_FILE_LINE_LENGTH);
            if (pIkeKey != NULL)
            {
                i1Length = (INT1) SPRINTF ((CHR1 *) au1TempBuff, "%d,%s,%s\r\n",
                                           pIkeKey->KeyID.i4IdType,
                                           pIkeKey->au1DisplayKeyId,
                                           pIkeKey->au1KeyString);

                STRCPY (pu1Buff + u4Offset, au1TempBuff);
                u4Offset = (UINT4) (u4Offset + (UINT2) i1Length);
            }
        }
    }

    if ((IKE_ZERO != u4Offset) &&
        (IKE_SUCCESS !=
         IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SECURE_MEM_SAVE,
                          PRESHARED_KEY_SAVE_FILE, ISS_CHAR_BUFFER, pu1Buff)))
    {
        GIVE_IKE_SEM ();
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Buff);
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1Buff);
    UNUSED_PARAM (u4EngineIndex);
    return IKE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IkeUtilRegCallBackforIKEModule                       */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IKE__SUCCESS/IKE__FAILURE                            */
/*****************************************************************************/
INT4
IkeUtilRegCallBackforIKEModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = IKE_SUCCESS;

    switch (u4Event)
    {
        case IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM:
            IKE_CALLBACK_FN[u4Event].pIkeWritePreSharedKeyToNVRAM =
                pFsCbInfo->pIkeWritePreSharedKeyToNVRAM;
            break;

        case IKE_ACCESS_SECURE_MEMORY:
            IKE_CALLBACK_FN[u4Event].pIkeCustSecureMemAccess =
                pFsCbInfo->pIssCustSecureProcess;
            break;

        default:
            i4RetVal = IKE_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IkeGetBypassCapability                               */
/*                                                                           */
/* Description        : This function is used to get the IKE bypass          */
/*                      capability                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BYPASS_ENABLED or BYPASS_DISABLED                    */
/*                                                                           */
/*****************************************************************************/
INT1
IkeGetBypassCapability (VOID)
{
    return ((INT1) gu1IkeBypassCrypto);
}

/*****************************************************************************/
/* Function Name      : IkeSetBypassCapability                               */
/*                                                                           */
/* Description        : This function is used to set the bypass capability   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IKE_SUCCESS or IKE_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
IkeSetBypassCapability (UINT1 u1BypassStatus)
{
    gu1IkeBypassCrypto = u1BypassStatus;
    return IKE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IkeSetIkeTrcLevel                                    */
/*                                                                           */
/* Description        : This function is used to set the global trace level  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IKE_SUCCESS or IKE_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
IkeSetIkeTrcLevel (UINT4 u4IkeTrcLevel)
{
    gu4IkeTrace = u4IkeTrcLevel;
    return IKE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IkeUtilCallBack                                    */
/*                                                                           */
/*     DESCRIPTION      : This function calls the registered custom function */
/*                                                                           */
/*     INPUT            : u4Event - Event for which callback is registered   */
/*                        Variable arguments - Arguments required for        */
/*                          the custom functions registered.                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : IKE_SUCCESS or IKE_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
IkeUtilCallBack (UINT4 u4Event, ...)
{
    UINT4              *apu4VaArgs[IKE_CALLBACK_MAX_ARGS] = { IKE_ZERO };
    va_list             vaList;
    INT4                i4RetVal = IKE_ZERO;
    UINT4               u4SecMemAction = IKE_ZERO;
    UINT1               u1Index = IKE_ZERO;

    IKE_BZERO (&apu4VaArgs, sizeof (apu4VaArgs));
    IKE_BZERO (&vaList, sizeof (vaList));

    switch (u4Event)
    {
        case IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM:
        {
            if (NULL != IKE_CALLBACK_FN[u4Event].pIkeWritePreSharedKeyToNVRAM)
            {
                i4RetVal = IKE_CALLBACK_FN[u4Event].
                    pIkeWritePreSharedKeyToNVRAM ();
            }
            break;

        }
            break;

        case IKE_ACCESS_SECURE_MEMORY:
        {

            va_start (vaList, u4Event);
            /* Walk through the arguements and store in args array.  */
            for (u1Index = IKE_ZERO; u1Index < IKE_CALLBACK_MAX_ARGS;
                 u1Index = (UINT1) (u1Index + IKE_ONE))
            {
                apu4VaArgs[u1Index] = va_arg (vaList, UINT4 *);
            }
            va_end (vaList);

            if (NULL != IKE_CALLBACK_FN[u4Event].pIkeCustSecureMemAccess)
            {
                u4SecMemAction = PTR_TO_U4 (apu4VaArgs[IKE_INDEX_0]);
                if (ISS_SRAM_FILENAME_MAPPING == u4SecMemAction)
                {
                    i4RetVal =
                        IKE_CALLBACK_FN[u4Event].pIkeCustSecureMemAccess
                        (u4SecMemAction, ISS_FILE, apu4VaArgs[IKE_INDEX_1],
                         apu4VaArgs[IKE_INDEX_2]);
                }
                else if (ISS_SECURE_MEM_SAVE == u4SecMemAction)
                {
                    i4RetVal =
                        IKE_CALLBACK_FN[u4Event].pIkeCustSecureMemAccess
                        (u4SecMemAction, PTR_TO_U4 (apu4VaArgs[IKE_INDEX_2]),
                         (CHR1 *) apu4VaArgs[IKE_INDEX_1],
                         (UINT1 *) apu4VaArgs[IKE_INDEX_3]);
                    /* Action, Type of content, Filename, Buffer */

                }

                if (ISS_FAILURE == i4RetVal)
                {
                    i4RetVal = IKE_FAILURE;
                }
                else
                {
                    i4RetVal = IKE_SUCCESS;
                }
                break;
            }
        }
            break;

        default:
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VpnIkeCheckAndCreateEngine                         */
/*                                                                           */
/*     DESCRIPTION      : This function checks for an Engine associated with */
/*                        a particular interface Index.                      */
/*                        If it doesn't exist it will create a new Engine    */
/*                        and an Entry in the Interface to Engine Mapping    */
/*                        table.This function calls the registered custom    */
/*                        function                                           */
/*                                                                           */
/*     INPUT            :  pVpnPolicy - Pointer to the Vpn Policy Data       */
/*                         Structure                                         */
/*                                                                           */
/*     OUTPUT           :  Interface to Engine Mapping table updation        */
/*                                                                           */
/*     RETURNS          : IKE_SUCCESS or IKE_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
VpnIkeCheckAndCreateEngine (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4EngineEntryIndex = IKE_ZERO;
    UINT4               u4IfIndex = IKE_ZERO;
    UINT4               u4AddrType = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH + IKE_ONE];
    tIkeEngine         *pIkeEngine = NULL;
    tIkeEngine         *pIkeDummyEngine = NULL;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

    pIkeDummyEngine = IkeSnmpGetEngine ((CONST UINT1 *) IKE_DUMMY_ENGINE,
                                        STRLEN (IKE_DUMMY_ENGINE));
    if (pIkeDummyEngine == NULL)
    {
        return IKE_FAILURE;
    }
    u4IfIndex = pVpnPolicy->u4IfIndex;
    for (u4EngineEntryIndex = IKE_ZERO; u4EngineEntryIndex < gu4NumIkeEngines;
         u4EngineEntryIndex += IKE_ONE)
    {
        if (u4IfIndex == IKE_GET_ENGINE_IFINDEX (u4EngineEntryIndex))
        {
            /* There is already an engine created for the
             * interface index specified in policy */
            pIkeEngine = IkeGetIkeEngineFromIndex
                (IKE_GET_ENGINE_INDEX (u4EngineEntryIndex));
            break;
        }
    }

    if (u4EngineEntryIndex == gu4NumIkeEngines)
    {
        /* Engine not present for the corresponding interface index. 
         * Need to create the same */
        SNPRINTF ((CHR1 *) au1EngineName, MAX_NAME_LENGTH, "%s%u",
                  IKE_ENGINE_NAME, u4IfIndex);

        IkeEngineInit (au1EngineName);
        TAKE_IKE_SEM ();

        pIkeEngine =
            IkeSnmpGetEngine (au1EngineName, (INT4) STRLEN (au1EngineName));
        if (pIkeEngine == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "VpnIkeCreateAndUpdatePolicyDB:"
                         "Engine does not exist\r\n");
            return IKE_FAILURE;
        }

        /* Update the Interface to Engine Mapping table */
        /* u4EngineEntryIndex is used as such since the count is always 1 
         * greater than the offset */

        MEMSET (gaIkeEngToIntMap + u4EngineEntryIndex, IKE_ZERO,
                sizeof (tIkeEngToIntMap));
        IKE_GET_ENGINE_IFINDEX (u4EngineEntryIndex) = u4IfIndex;
        STRNCPY (IKE_GET_ENGINE_NAME (u4EngineEntryIndex), au1EngineName,
                 MAX_NAME_LENGTH - IKE_ONE);
        IKE_GET_ENGINE_INDEX (u4EngineEntryIndex) = pIkeEngine->u4IkeIndex;
        gu4NumIkeEngines = gu4NumIkeEngines + IKE_ONE;

        pIkeEngine->i2SocketId = pIkeDummyEngine->i2SocketId;
        pIkeEngine->i2NattSocketFd = pIkeDummyEngine->i2NattSocketFd;
        pIkeEngine->i2V6SocketFd = pIkeDummyEngine->i2V6SocketFd;
        pIkeEngine->i2V6NattSocketFd = pIkeDummyEngine->i2V6NattSocketFd;
    }

    u4AddrType = (pVpnPolicy->LocalTunnTermAddr).u4AddrType;
    if (IPV4ADDR == u4AddrType)
    {
        if (pIkeEngine != NULL)
        {
            (pIkeEngine->Ip4TunnelTermAddr).u4AddrType = u4AddrType;
            IKE_MEMCPY (&((pIkeEngine->Ip4TunnelTermAddr).uIpAddr.Ip4Addr),
                        &((pVpnPolicy->LocalTunnTermAddr).uIpAddr.Ip4Addr),
                        sizeof (tIp4Addr));
        }
    }
    else                        /* IPV6ADDR is the Address type */
    {
        if (pIkeEngine != NULL)
        {
            (pIkeEngine->Ip6TunnelTermAddr).u4AddrType = u4AddrType;
            IKE_MEMCPY (&((pIkeEngine->Ip6TunnelTermAddr).uIpAddr.Ip6Addr),
                        &((pVpnPolicy->LocalTunnTermAddr).uIpAddr.Ip6Addr),
                        sizeof (tIp6Addr));
        }
    }

    GIVE_IKE_SEM ();

    return IKE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IkeGetEngNameFromIfIndex                           */
/*                                                                           */
/*     DESCRIPTION      : This function frames the engine name based on the  */
/*                        interface index to which it is mapped against      */
/*                                                                           */
/*     INPUT            :  u4IfIndex - Interface index                       */
/*                                                                           */
/*     OUTPUT           :  pu1EngName - Engine Name                          */
/*                                                                           */
/*     RETURNS          : IKE_SUCCESS or IKE_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

UINT1              *
IkeGetEngNameFromIfIndex (UINT4 u4IfIndex)
{
    UINT4               u4EngineEntryIndex = IKE_ZERO;

    for (u4EngineEntryIndex = IKE_ZERO; u4EngineEntryIndex < gu4NumIkeEngines;
         u4EngineEntryIndex += IKE_ONE)
    {
        if (u4IfIndex == IKE_GET_ENGINE_IFINDEX (u4EngineEntryIndex))
        {

            IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                          "IkeGetEngNameFromIfIndex:\
                         Got the engine, engine%d\r\n", u4IfIndex);
            /* There is already an engine created for the
             * interface index specified */
            return (IKE_GET_ENGINE_NAME (u4EngineEntryIndex));
        }
    }

    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                 "IkeGetEngNameFromIfIndex: Didn't Get the engine\r\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VpnIkeCheckAndDeleteEngine                         */
/*                                                                           */
/*     DESCRIPTION      : This function checks for an Engine associated with */
/*                        a particular interface Index.                      */
/*                        If it exists it will delete the particular Engine  */
/*                        and also deletes the entry in the Interface to 
 *                        Engine Mapping table.                              */
/*                                                                           */
/*     INPUT            :  pVpnPolicy - Pointer to the Vpn Policy Data       */
/*                         Structure                                         */
/*                                                                           */
/*     OUTPUT           :  Interface to Engine Mapping table updation        */
/*                                                                           */
/*     RETURNS          : IKE_SUCCESS or IKE_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
VpnIkeCheckAndDeleteEngine (UINT4 u4IfIndex)
{
    UINT4               u4EngineEntryIndex = IKE_ZERO;
    UINT4               u4EngineEntryIndex1 = IKE_ZERO;

    for (u4EngineEntryIndex = IKE_ZERO; u4EngineEntryIndex < gu4NumIkeEngines;
         u4EngineEntryIndex += IKE_ONE)
    {
        if (u4IfIndex == IKE_GET_ENGINE_IFINDEX (u4EngineEntryIndex))
        {
            break;
        }
    }

    if (u4EngineEntryIndex != gu4NumIkeEngines)
    {
        /* Engine present with the specified interface index associated 
         * with it, delete the same */
        IkeDeleteEngineContents (IKE_GET_ENGINE_NAME (u4EngineEntryIndex));

        /* Shifting the contents in the table */
        for (u4EngineEntryIndex1 = u4EngineEntryIndex + IKE_ONE;
             u4EngineEntryIndex1 < gu4NumIkeEngines;
             u4EngineEntryIndex1 += IKE_ONE)
        {
            MEMCPY (gaIkeEngToIntMap + u4EngineEntryIndex,
                    gaIkeEngToIntMap + u4EngineEntryIndex1,
                    sizeof (tIkeEngToIntMap));
            u4EngineEntryIndex += IKE_ONE;
        }
        MEMSET (gaIkeEngToIntMap + u4EngineEntryIndex, IKE_ZERO,
                sizeof (tIkeEngToIntMap));
        gu4NumIkeEngines -= IKE_ONE;

    }

    return IKE_SUCCESS;
}
