/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsopenssl.c,v 1.4 2013/11/12 11:16:59 siva Exp $
 *
 * Description: This has the functions to interface with OpenSSL      
 *
 ***********************************************************************/

#include "ikegen.h"
#include "fsopenssl.h"

#ifdef OpenSSL_add_all_algorithms
#undef OpenSSL_add_all_algorithms
#endif

extern VOID         OpenSSL_add_all_algorithms (VOID);

/**************************************************************************/
/*  Function Name   : FSOpenSslInit                                       */
/*  Description     : This function is to initialize the OpenSSL crypto   */
/*                    library                                             */
/*  Input(s)        : None                                                */
/*  Output(s)       : None                                                */
/*  Returns         : None                                                */
/**************************************************************************/
VOID
FSOpenSslInit (VOID)
{
    OpenSSL_add_all_algorithms ();
    return;
}

/**************************************************************************/
/*  Function Name   : FSOpenSslDhGenerateKeyPair                          */
/*  Description     : This function is an interface to OpenSSL which      */
/*                    generates the Diffie-Helman key pair                */
/*  Input(s)        : pDhInfo - Info about DH Group                       */
/*  Output(s)       : pPub, pPrv - The key pair                           */
/*  Returns         : Success or Failure                                  */
/**************************************************************************/
INT4
FSOpenSslDhGenerateKeyPair (tDhGroupInfo * pDhInfo, tHexNum * pPub,
                            tHexNum * pPrv)
{
    DH                 *pDh = NULL;
    INT4                i4Ret = IKE_ZERO;

    pDh = DH_new ();
    if (pDh == NULL)
    {

        goto DH_ERROR;
    }

    pDh->g = BN_new ();
    if (pDh->g == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_set_word (pDh->g, pDhInfo->u4Generator) == FS_SSL_ERROR)
    {
        goto BN_ERROR;
    }

    pDh->p = BN_new ();
    if (pDh->p == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_bin2bn (pDhInfo->pPrime.pu1Value, pDhInfo->pPrime.i4Length, pDh->p)
        == NULL)
    {
        goto BN_ERROR;
    }

    pDh->length = (INT4) pDhInfo->u4Length;

    if (DH_generate_key (pDh) == FS_SSL_ERROR)
    {
        goto DH_ERROR;
    }

    pPub->i4Length = BN_bn2bin (pDh->pub_key, pPub->pu1Value);
    pPrv->i4Length = BN_bn2bin (pDh->priv_key, pPrv->pu1Value);

    goto DH_END;

  DH_ERROR:
  BN_ERROR:
    i4Ret = IKE_INVALID;
  DH_END:
    return i4Ret;
}

/**************************************************************************/
/*  Function Name   : FSOpenSslDhCompute                                  */
/*  Description     : This function is an interface to OpenSSL which      */
/*                    generates the shared secret given the DH keys       */
/*  Input(s)        : pDhInfo - Info about DH Group                       */
/*                  : pPub1   - Our public key                            */
/*                  : pPrv1   - Our private key                           */
/*                  : pPub2   - His public key                            */
/*  Output(s)       : pKey - The shared secret                            */
/*  Returns         : Success or Failure                                  */
/**************************************************************************/

INT4
FSOpenSslDhCompute (tDhGroupInfo * pDhInfo, tHexNum * pPub1, tHexNum * pPrv1,
                    tHexNum * pPub2, tHexNum * pKey)
{
    BIGNUM             *pDhPub2 = NULL;
    DH                 *pDh = NULL;
    INT4                i4Ret = IKE_ZERO;

    pDh = DH_new ();
    if (pDh == NULL)
    {
        goto DH_ERROR;
    }

    pDh->g = BN_new ();
    if (pDh->g == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_set_word (pDh->g, pDhInfo->u4Generator) == FS_SSL_ERROR)
    {
        goto BN_ERROR;
    }

    pDh->p = BN_new ();
    if (pDh->p == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_bin2bn (pDhInfo->pPrime.pu1Value, pDhInfo->pPrime.i4Length, pDh->p)
        == NULL)
    {
        goto BN_ERROR;
    }

    pDh->pub_key = BN_new ();
    if (pDh->pub_key == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_bin2bn (pPub1->pu1Value, pPub1->i4Length, pDh->pub_key) == NULL)
    {
        goto BN_ERROR;
    }

    pDh->priv_key = BN_new ();
    if (pDh->priv_key == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_bin2bn (pPrv1->pu1Value, pPrv1->i4Length, pDh->priv_key) == NULL)
    {
        goto BN_ERROR;
    }

    pDh->length = (INT4) pDhInfo->u4Length;

    pDhPub2 = BN_new ();
    if (pDhPub2 == NULL)
    {
        goto BN_ERROR;
    }

    if (BN_bin2bn (pPub2->pu1Value, pPub2->i4Length, pDhPub2) == NULL)
    {
        goto BN_ERROR;
    }

    pKey->i4Length = DH_compute_key (pKey->pu1Value, pDhPub2, pDh);
    if (pKey->i4Length == FS_SSL_INVALID_KEY_LEN)
    {
        goto DH_ERROR;
    }
    goto DH_END;

  DH_ERROR:
  BN_ERROR:
    i4Ret = IKE_INVALID;
  DH_END:
    if (pDh != NULL)
    {
        DH_free (pDh);
    }

    if (pDhPub2 != NULL)
    {
        BN_free (pDhPub2);
    }

    return i4Ret;
}
