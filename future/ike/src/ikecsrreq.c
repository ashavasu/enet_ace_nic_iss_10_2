/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikecsrreq.c,v 1.12 2014/01/25 13:54:11 siva Exp $
 *
 * Description: This file has RSA wrapper functions.
 *
 ***********************************************************************/

#include "ikegen.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :IkeGenerateCertificateRequest                      */
/*  Description     :This function is used to generate certificate req. */
/*                  :with the given subject altname and subject name    */
/*                  :The function is called from CLI                    */
/*  Input(s)        :pu1EngineId      : Engine Identifier               */
/*                  :pu1KeyId         : Key Identifier                  */
/*                  :pu1SubjectAltName: Subject Alt Name                */
/*                  :pu1SubjectName   : Subject Name                    */
/*                  :u4Type - Type of algorithm whether RSA/DSA         */
/*  Output(s)       :pu4ErrNo         :Error Number in case of failure  */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeGenerateCertificateRequest (UINT1 *pu1EngineId, UINT1 *pu1KeyId,
                               UINT1 *pu1SubjectAltName, UINT1 *pu1SubjectName,
                               UINT1 **ppu1TextOut, UINT4 u4Type,
                               UINT4 *pu4ErrNo)
{
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;
    tIkePKey           *pIkePKey = NULL;
    X509_REQ           *pCertReq = NULL;
    X509V3_CTX          ExtCtx;
    BIO                *pWriteBio = NULL;
    tIkeRSA            *pIkeRSA = NULL;
    signed char         au1FileName[IKE_MAX_FILE_NAME_LEN] = { IKE_ZERO };
    INT4                i4RetVal = IKE_ZERO;
    const EVP_MD       *pDigest = NULL;
    UINT4               u4Len = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;

    *ppu1TextOut = NULL;

    if (u4Type == IKE_RSA_SIGNATURE)
    {
        pIkeRsaKeyInfo = IkeGetRsaKeyEntry (pu1EngineId, pu1KeyId);
        pIkePKey = &pIkeRsaKeyInfo->RsaKey;
    }
    else if (u4Type == IKE_DSS_SIGNATURE)
    {
        pIkeDsaKeyInfo = IkeGetDsaKeyEntry (pu1EngineId, pu1KeyId);
        pIkePKey = &pIkeDsaKeyInfo->DsaKey;
    }
    if ((pIkeRsaKeyInfo == NULL) && (pIkeDsaKeyInfo == NULL))
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Key Does not exist" "\n");
        *pu4ErrNo = IKE_ERR_NO_KEY_INFO_FOR_KEY_ID;
        return IKE_FAILURE;
    }

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        pIkeRSA = EVP_PKEY_get1_RSA (pIkePKey);
        u4Bits = (UINT4) IKE_BYTES2BITS (RSA_size (pIkeRSA));
        if ((u4Bits == RSA_KEY_512) ||
            (u4Bits == RSA_KEY_768) || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    pCertReq = X509_REQ_new ();
    if (pCertReq == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Memory allocation"
                     "(X509_REQ_new) failed\n");
        *pu4ErrNo = IKE_ERR_MEM_ALLOC;
        return IKE_FAILURE;
    }

    i4RetVal = IkeConstructPKCS10CertRequest (pCertReq, pIkePKey,
                                              pu1SubjectName);
    if (i4RetVal == IKE_FAILURE)
    {
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Failed to construct Request"
                     "\n");
        *pu4ErrNo = IKE_ERR_CONSTRUCT_PKCS10_CERT_REQ;
        return IKE_FAILURE;
    }

    X509V3_set_ctx (&ExtCtx, NULL, NULL, pCertReq, NULL, IKE_ZERO);
    i4RetVal = IkeAddX509V3Ext (pCertReq, &ExtCtx, pu1SubjectAltName);
    if (i4RetVal == IKE_FAILURE)
    {
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Failed to add extensions"
                     "Request\n");
        *pu4ErrNo = IKE_ERR_ADD_X509V3_EXT;
        return IKE_FAILURE;
    }

    pDigest = EVP_md5 ();
    i4RetVal = X509_REQ_sign (pCertReq, pIkePKey, pDigest);

    if (i4RetVal == IKE_NONE)
    {
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Failed to "
                     "sign Request\n");
        *pu4ErrNo = IKE_ERR_X509_REQ_SIGN;
        return IKE_FAILURE;
    }
    pWriteBio = BIO_new (BIO_s_file ());
    if (pWriteBio == NULL)
    {
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Call to BIO_new failed,"
                     "Unable to create BIO for reading\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    u4Len = IKE_STRLEN (IKE_DEF_USER_DIR) + IKE_STRLEN (pu1KeyId) +
        IKE_STRLEN (".pkcs10") + IKE_STRLEN ("/");
    if (u4Len >= IKE_MAX_FILE_NAME_LEN)
    {
        BIO_free_all (pWriteBio);
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Filename too long" "\n");
        *pu4ErrNo = IKE_ERR_FILE_NAME_TOO_LONG;
        return IKE_FAILURE;
    }

    IKE_SNPRINTF (((CHR1 *) au1FileName), IKE_MAX_FILE_NAME_LEN,
                  "%s/%s.pkcs10", IKE_DEF_USER_DIR, pu1KeyId);

    if (BIO_write_filename (pWriteBio, au1FileName) <= IKE_NONE)
    {
        BIO_free_all (pWriteBio);
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Call to BIO_write_filename"
                     " failed\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    i4RetVal = PEM_write_bio_X509_REQ (pWriteBio, pCertReq);
    if (i4RetVal == IKE_ZERO)
    {
        BIO_free_all (pWriteBio);
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Failed to write X509"
                     "request\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    i4RetVal = IkeConvertX509RequestToText (pCertReq, ppu1TextOut);

    if (i4RetVal == IKE_FAILURE)
    {
        BIO_free_all (pWriteBio);
        X509_REQ_free (pCertReq);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeGenerateCertificateRequest:Failed to"
                     "convert cert request to text format\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    BIO_free_all (pWriteBio);
    X509_REQ_free (pCertReq);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConstructPKCS10CertRequest                      */
/*  Description     :This function is used to make certificate request  */
/*                  :with the given subject name.                       */
/*                  :The function is called from IkeGenerateCertificateR*/
/*                  :euest function.                                    */
/*  Input(s)        :pCertReq         : Pointer to empty certificate req*/
/*                  :pKey             : Pointer to key structure        */
/*                  :pu1SubjectName   : Subject Name                    */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeConstructPKCS10CertRequest (X509_REQ * pCertReq, tIkePKey * pKey,
                               UINT1 *pu1SubjectName)
{
    INT4                i4RetVal = IKE_ZERO;
    X509_NAME          *pSubName = NULL;
    signed char         au1Name[IKE_SUB_NAME] = { IKE_ZERO };

    i4RetVal = X509_REQ_set_version (pCertReq, 0L);
    if (i4RetVal == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConstructCertRequest:Failed to set X509 request"
                     "version\n");
        return IKE_FAILURE;
    }
    IKE_STRNCPY (au1Name, "commonName", IKE_SUB_NAME);
    pSubName = X509_REQ_get_subject_name (pCertReq);
    if (pSubName == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConstructCertRequest:Failed to get subject name" "\n");
        return IKE_FAILURE;
    }

    if (X509_NAME_add_entry_by_txt (pSubName, ((CHR1 *) au1Name),
                                    MBSTRING_ASC,
                                    pu1SubjectName, IKE_INVALID,
                                    IKE_INVALID, IKE_ZERO) == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConstructCertRequest:Failed to add entry subject name"
                     "\n");
        return IKE_FAILURE;
    }

    if (X509_REQ_set_pubkey (pCertReq, pKey) == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConstructCertRequest:Failed to set public key\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;

}

/************************************************************************/
/*  Function Name   :IkeAddX509V3Ext                                    */
/*  Description     :This function is used to add X509V3 extensions to  */
/*                  :certificate request.                               */
/*  Input(s)        :pCertReq         : Pointer to empty certificate req*/
/*                  :pCtx             : Pointer to X509V3 context struct*/
/*                  :pu1SubjectAltName: Subject Alt Name                */
/*  Output(s)       :None                                               */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/************************************************************************/

INT1
IkeAddX509V3Ext (X509_REQ * pCertReq, X509V3_CTX * pCtx,
                 UINT1 *pu1SubjectAltName)
{
    X509_EXTENSION     *pExtension = NULL;
    X509V3_EXT_METHOD  *pExtMethod = NULL;
    STACK_OF (X509_EXTENSION) * pExtList = NULL;
    STACK_OF (CONF_VALUE) * pNVal = NULL;
    UINT4               u4ExtNid = IKE_ZERO;
    INT4                i4Critical = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    VOID               *pExtStruct = NULL;

    i4Critical = IKE_ONE;
    u4ExtNid = (UINT4) OBJ_sn2nid ("subjectAltName");
    if (u4ExtNid == IKE_NID_UNDEF)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:NID not defined\n");
        return IKE_FAILURE;
    }

    pExtMethod = X509V3_EXT_get_nid ((INT4) u4ExtNid);
    if (pExtMethod == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:Call to" "X509V3_EXT_get_nid failed\n");
        return IKE_FAILURE;
    }

    pNVal = X509V3_parse_list ((CHR1 *) pu1SubjectAltName);
    if (pNVal == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:X509V3_parse_list" "failed\n");
        return IKE_FAILURE;
    }

    pExtStruct = pExtMethod->v2i (pExtMethod, pCtx, pNVal);
    if (pExtStruct == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:Call to vtoi()" "failed\n");
        return IKE_FAILURE;
    }

    pExtension = X509V3_EXT_i2d ((INT4) u4ExtNid, i4Critical, pExtStruct);
    if (pExtension == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:Call to do_ext_i2d" "failed\n");
        return IKE_FAILURE;
    }
    if (pExtMethod->it)
    {
        ASN1_item_free (pExtStruct, ASN1_ITEM_ptr (pExtMethod->it));
    }

    else
    {
        pExtMethod->ext_free (pExtStruct);
    }

    pExtList = sk_X509_EXTENSION_new_null ();
    if (pExtList == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:sk_X509_EXTENSION_new_null" "failed\n");
        return IKE_FAILURE;
    }

    if (!(sk_X509_EXTENSION_push (pExtList, pExtension)))
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:Failed in sk_X509_EXTENSION_push\n");
        return IKE_FAILURE;
    }

    i4RetVal = X509_REQ_add_extensions (pCertReq, pExtList);
    if (i4RetVal == IKE_ZERO)
    {
        sk_X509_EXTENSION_pop_free (pExtList, X509_EXTENSION_free);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeAddX509V3Ext:Failed to add X509V3 extensions\n");
        return IKE_FAILURE;
    }
    sk_X509_EXTENSION_pop_free (pExtList, X509_EXTENSION_free);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConvertX509RequestToText                        */
/*  Description     :This function is called to convert a cert request  */
/*                  :in X509_REQ format to text format                  */
/*                  :                                                   */
/*  Input(s)        :pX509CertReq    : Certificate Request              */
/*                  :                                                   */
/*  Output(s)       :ppu1OutText : Certificate request in text format   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertX509RequestToText (X509_REQ * pX509CertReq, UINT1 **ppu1OutText)
{
    BIO                *pReqBio = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pReqBio = BIO_new (BIO_s_mem ());
    if (pReqBio == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509RequestToText: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1OutText)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509RequestToText: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1OutText == NULL)
    {
        BIO_free_all (pReqBio);
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509RequestToText: MALLOC failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = X509_REQ_print (pReqBio, pX509CertReq);
    if (i4RetVal == IKE_ZERO)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) *ppu1OutText);
        BIO_free_all (pReqBio);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509RequestToText:X509_REQ_print" "failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pReqBio, *ppu1OutText, IKE_MAX_CERT_REQ_LEN);
    if (i4RetVal <= IKE_ZERO)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) *ppu1OutText);
        BIO_free_all (pReqBio);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToASCII: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }
    (*ppu1OutText)[i4RetVal] = '\0';
    BIO_free_all (pReqBio);
    return IKE_SUCCESS;
}
