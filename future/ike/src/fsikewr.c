/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsikewr.c,v 1.3 2010/11/02 09:39:22 prabuc Exp $
 *
 * Description: This has functions for wrapper routines 
 *
 ***********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsikelow.h"
# include  "fsikewr.h"

INT4
FsikeGlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsikeGlobalDebug (&(pMultiData->i4_SLongValue)));
}

INT4
FsikeRAXAuthModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsikeRAXAuthMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsikeRACMModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsikeRACMMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsikeGlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsikeGlobalDebug (pMultiData->i4_SLongValue));
}

INT4
FsikeRAXAuthModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsikeRAXAuthMode (pMultiData->i4_SLongValue));
}

INT4
FsikeRACMModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsikeRACMMode (pMultiData->i4_SLongValue));
}

INT4
FsikeGlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsikeGlobalDebug (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsikeRAXAuthModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsikeRAXAuthMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsikeRACMModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsikeRACMMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
GetNextIndexFsIkeEngineTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeEngineTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeEngineTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeEngineTunnelTermAddrTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeEngineTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeEngineTunnelTermAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeEngineTunnelTermAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeEngineTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeEngineTunnelTermAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeEngineRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeEngineTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeEngineRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeEngineTunnelTermAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeEngineTunnelTermAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeEngineRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeEngineRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsIkeEngineTunnelTermAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeEngineTunnelTermAddr (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsIkeEngineRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeEngineRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkePolicyTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkePolicyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkePolicyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkePolicyAuthMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyAuthMethod (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyHashAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyEncryptionAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyDHGroupGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyDHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyLifeTimeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyLifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyLifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyLifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIkePolicyLifeTimeKBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyLifeTimeKB (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsIkePolicyModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyMode (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkePolicyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkePolicyStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsIkePolicyAuthMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyAuthMethod (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyHashAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyEncryptionAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyDHGroupSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyDHGroup (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyLifeTimeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyLifeTimeType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyLifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyLifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsIkePolicyLifeTimeKBSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyLifeTimeKB (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsIkePolicyModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyMode (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkePolicyStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyAuthMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyAuthMethod (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyHashAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyHashAlgo (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyEncryptionAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyEncryptionAlgo (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyDHGroupTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyDHGroup (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyLifeTimeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyLifeTimeType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyLifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyLifeTime (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsIkePolicyLifeTimeKBTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyLifeTimeKB (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsIkePolicyModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyMode (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsIkePolicyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIkePolicyStatus (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeRAInfoTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeRAInfoTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeRAInfoTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeRAInfoPeerIdTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInfoPeerIdType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAInfoPeerIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInfoPeerId (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAXAuthEnable (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRACMEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRACMEnable (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAXAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAXAuthType (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAXAuthUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAXAuthUserName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthUserPasswdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAXAuthUserPasswd (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsIkeRAIPAddressAllocMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAIPAddressAllocMethod
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAInternalIPAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInternalIPAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAInternalIPAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInternalIPAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInternalIPNetMaskTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInternalIPNetMaskType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAInternalIPNetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInternalIPNetMask
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInfoProtectedNetBundleGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInfoProtectedNetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRATransformSetBundleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRATransformSetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRAModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAMode (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAPfsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAPfs (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiIndex->pIndex[1].pOctetStrValue,
                              &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRALifeTimeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRALifeTimeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRALifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRALifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeRAInfoStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeRAInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeRAInfoStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeRAInfoPeerIdTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInfoPeerIdType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInfoPeerIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInfoPeerId (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAXAuthEnable (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsIkeRACMEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRACMEnable (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsIkeRAXAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAXAuthType (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsIkeRAXAuthUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAXAuthUserName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthUserPasswdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAXAuthUserPasswd (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsIkeRAIPAddressAllocMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAIPAddressAllocMethod
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInternalIPAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInternalIPAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInternalIPNetMaskTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInternalIPNetMaskType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPNetMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInternalIPNetMask
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInfoProtectedNetBundleSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInfoProtectedNetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRATransformSetBundleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRATransformSetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeRAModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAMode (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiData->i4_SLongValue));

}

INT4
FsIkeRAPfsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAPfs (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiIndex->pIndex[1].pOctetStrValue,
                              pMultiData->i4_SLongValue));

}

INT4
FsIkeRALifeTimeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRALifeTimeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsIkeRALifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRALifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsIkeRAInfoStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeRAInfoStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInfoPeerIdTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInfoPeerIdType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInfoPeerIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInfoPeerId (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAXAuthEnable (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkeRACMEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRACMEnable (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsIkeRAXAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAXAuthType (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsIkeRAXAuthUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAXAuthUserName (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsIkeRAXAuthUserPasswdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAXAuthUserPasswd (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsIkeRAIPAddressAllocMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAIPAddressAllocMethod (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInternalIPAddrType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInternalIPAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInternalIPNetMaskTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInternalIPNetMaskType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsIkeRAInternalIPNetMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInternalIPNetMask (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsIkeRAInfoProtectedNetBundleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInfoProtectedNetBundle (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsIkeRATransformSetBundleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRATransformSetBundle (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsIkeRAModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAMode (pu4Error,
                                  pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsIkeRAPfsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAPfs (pu4Error,
                                 pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsIkeRALifeTimeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRALifeTimeType (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIkeRALifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRALifeTime (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsIkeRAInfoStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeRAInfoStatus (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeProtectNetTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeProtectNetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeProtectNetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeProtectNetTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeProtectNetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeProtectNetType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeProtectNetAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeProtectNetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeProtectNetAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsIkeProtectNetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeProtectNetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeProtectNetStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeProtectNetTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeProtectNetType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsIkeProtectNetAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeProtectNetAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsIkeProtectNetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeProtectNetStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkeProtectNetTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeProtectNetType (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIkeProtectNetAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeProtectNetAddr (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsIkeProtectNetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeProtectNetStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeKeyTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeKeyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeKeyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeKeyIdTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeKeyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeKeyIdType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeKeyStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeKeyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeKeyString (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsIkeKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeKeyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeKeyStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeKeyIdTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeKeyIdType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsIkeKeyStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeKeyString (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsIkeKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeKeyStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsIkeKeyIdTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeKeyIdType (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsIkeKeyStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeKeyString (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsIkeKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeKeyStatus (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeTransformSetTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeTransformSetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeTransformSetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeTSEspHashAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeTSEspHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeTSEspEncryptionAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeTSEspEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeTSAhHashAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeTSAhHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeTSStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeTransformSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeTSStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeTSEspHashAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeTSEspHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsIkeTSEspEncryptionAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeTSEspEncryptionAlgo
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsIkeTSAhHashAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeTSAhHashAlgo (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsIkeTSStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeTSStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsIkeTSEspHashAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeTSEspHashAlgo (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsIkeTSEspEncryptionAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeTSEspEncryptionAlgo (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsIkeTSAhHashAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeTSAhHashAlgo (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsIkeTSStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeTSStatus (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeCryptoMapTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeCryptoMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeCryptoMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeCMLocalAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMLocalAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMLocalAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMLocalAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsIkeCMRemAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMRemAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMRemAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMRemAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsIkeCMPeerAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMPeerAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMPeerAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMPeerAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsIkeCMTransformSetBundleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMTransformSetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeCMModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMMode (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMPfsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMPfs (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiIndex->pIndex[1].pOctetStrValue,
                              &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMLifeTimeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMLifeTimeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMLifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMLifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeCMLifeTimeKBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMLifeTimeKB (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeCMStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeCryptoMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeCMStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsIkeCMLocalAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMLocalAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsIkeCMRemAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMRemAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsIkeCMPeerAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMPeerAddr (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsIkeCMTransformSetBundleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMTransformSetBundle
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsIkeCMModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMMode (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiData->i4_SLongValue));

}

INT4
FsIkeCMPfsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMPfs (pMultiIndex->pIndex[0].pOctetStrValue,
                              pMultiIndex->pIndex[1].pOctetStrValue,
                              pMultiData->i4_SLongValue));

}

INT4
FsIkeCMLifeTimeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMLifeTimeType (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsIkeCMLifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMLifeTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsIkeCMLifeTimeKBSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMLifeTimeKB (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsIkeCMStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIkeCMStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsIkeCMLocalAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMLocalAddr (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsIkeCMRemAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMRemAddr (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsIkeCMPeerAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMPeerAddr (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsIkeCMTransformSetBundleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMTransformSetBundle (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsIkeCMModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMMode (pu4Error,
                                  pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsIkeCMPfsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMPfs (pu4Error,
                                 pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsIkeCMLifeTimeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMLifeTimeType (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIkeCMLifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMLifeTime (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsIkeCMLifeTimeKBTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMLifeTimeKB (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsIkeCMStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsIkeCMStatus (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsIkeStatTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIkeStatTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIkeStatTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIkeSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeSAs (pMultiIndex->pIndex[0].pOctetStrValue,
                            &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeInitSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeInitSessions (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeAcceptSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeAcceptSessions (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeActiveSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeActiveSessions (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeInPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeInPackets (pMultiIndex->pIndex[0].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeOutPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeOutPackets (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeDropPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeDropPackets (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeNoOfNotifySentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeNoOfNotifySent (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeNoOfNotifyRecvGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeNoOfNotifyRecv (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeNoOfSessionsSuccGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeNoOfSessionsSucc (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsIkeNoOfSessionsFailureGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIkeStatTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIkeNoOfSessionsFailure
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}
