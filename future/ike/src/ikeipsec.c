/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikeipsec.c,v 1.19 2015/04/04 10:33:51 siva Exp $
 *
 * Description: This file has IKE-IPSEC interface functions
 *
 ***********************************************************************/

#include "ikegen.h"
#include "cfa.h"
#include "sec.h"
#include "ikememmac.h"

static INT1         IkeProcessIPSecNotifyRequest (tIkePhase1PostProcessInfo *
                                                  pNotifyInfo,
                                                  tIkeIpAddr * pLocalAddr,
                                                  tIkeIpAddr * pRemoteAddr,
                                                  UINT1);
static INT1         IkeProcessIPSecDuplicateSPIReply (tIkeDupSpi * pDupSpiInfo);
static INT1         IkeProcessIPSecDeleteSARequest (tIkeDelSpi * pDelSpiInfo);
#if IKE_ZERO
static INT1         IkeProcessIPSecInvalidSpiRequest (tIkeInvalidSpi *
                                                      pInvalidSpiInfo);
#endif
static INT1         IkeProcessIPSecSaTriggerRequest (tIkeNewSA *
                                                     pSaTriggerInfo);

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeSendIPSecIfParam                 
 *  Description   : Function to request to IPSec                          
 *  Input(s)      : pIPSecIfParam - The params required by IPSec
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeSendIPSecIfParam (tIkeIPSecIfParam * pIPSecIfParam)
{
    tIkeIPSecQMsg       IPSecMsg;
    UINT4               u4Count = IKE_ZERO;
    UINT4               u4SpiCount = IKE_ZERO;
    UINT2               u2NumSPI = IKE_ZERO;
    BOOLEAN             bIsv4Addr = IKE_FALSE;

    IKE_BZERO (&IPSecMsg, sizeof (tIkeIPSecQMsg));

    switch (pIPSecIfParam->u4MsgType)
    {
        case IKE_IPSEC_INSTALL_SA:
            IPSecMsg.u4MsgType = IKE_HTONL (IKE_IPSEC_INSTALL_SA);

            for (u4Count = IKE_ZERO; u4Count < IKE_TWO; u4Count++)
            {
                if (pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u1IPSecProtocol == IKE_IPSEC_AH)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u1IPSecProtocol = IKE_IP_PROTO_AH;
                }

                if (pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u1IPSecProtocol == IKE_IPSEC_ESP)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u1IPSecProtocol = IKE_IP_PROTO_ESP;
                }

                /* Make sure that AH or ESP is configured, otherwise
                 * don't do this */
                if (pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u2HLProtocol == IKE_ZERO)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u2HLProtocol = IKE_HTONS (IKE_IPSEC_PROTO_ANY);
                }
                else
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u2HLProtocol =
                        IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                                   aInSABundle[u4Count].u2HLProtocol);
                }

                if (pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u2HLPortNumber == IKE_ZERO)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u2HLPortNumber = IKE_HTONS (IKE_IPSEC_PORT_ANY);
                }
                else
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                        u2HLPortNumber =
                        IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                                   aInSABundle[u4Count].u2HLPortNumber);
                }

                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u4Spi =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aInSABundle[u4Count].u4Spi);

                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u4LifeTime =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aInSABundle[u4Count].u4LifeTime);
                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u4LifeTimeKB =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aInSABundle[u4Count].u4LifeTimeKB);
                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u2AuthKeyLen =
                    IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                               aInSABundle[u4Count].u2AuthKeyLen);
                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[u4Count].
                    u2EncrKeyLen =
                    IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                               aInSABundle[u4Count].u2EncrKeyLen);
            }

            if ((pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[IKE_AH_SA_INDEX].
                 u4Spi != IKE_ZERO)
                && (pIPSecIfParam->IPSecReq.InstallSA.
                    aInSABundle[IKE_ESP_SA_INDEX].u4Spi != IKE_ZERO))
            {
                pIPSecIfParam->IPSecReq.InstallSA.aInSABundle[IKE_AH_SA_INDEX].
                    u1Mode = IKE_IPSEC_TRANSPORT_MODE;
            }

            for (u4Count = IKE_ZERO; u4Count < IKE_TWO; u4Count++)
            {
                if (pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u1IPSecProtocol == IKE_IPSEC_AH)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u1IPSecProtocol = IKE_IP_PROTO_AH;
                }

                if (pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u1IPSecProtocol == IKE_IPSEC_ESP)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u1IPSecProtocol = IKE_IP_PROTO_ESP;
                }

                if (pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u2HLProtocol == IKE_ZERO)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u2HLProtocol = IKE_HTONS (IKE_IPSEC_PROTO_ANY);
                }
                else
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u2HLProtocol =
                        IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                                   aOutSABundle[u4Count].u2HLProtocol);
                }

                if (pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u2HLPortNumber == IKE_ZERO)
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u2HLPortNumber = IKE_HTONS (IKE_IPSEC_PORT_ANY);
                }
                else
                {
                    pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                        u2HLPortNumber =
                        IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                                   aOutSABundle[u4Count].u2HLPortNumber);
                }

                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u4Spi =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aOutSABundle[u4Count].u4Spi);

                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u4LifeTime =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aOutSABundle[u4Count].u4LifeTime);
                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u4LifeTimeKB =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               aOutSABundle[u4Count].u4LifeTimeKB);
                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u2AuthKeyLen =
                    IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                               aOutSABundle[u4Count].u2AuthKeyLen);
                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[u4Count].
                    u2EncrKeyLen =
                    IKE_HTONS (pIPSecIfParam->IPSecReq.InstallSA.
                               aOutSABundle[u4Count].u2EncrKeyLen);
            }

            if ((pIPSecIfParam->IPSecReq.InstallSA.
                 aOutSABundle[IKE_AH_SA_INDEX].u4Spi != IKE_ZERO)
                && (pIPSecIfParam->IPSecReq.InstallSA.
                    aOutSABundle[IKE_ESP_SA_INDEX].u4Spi != IKE_ZERO))
            {
                pIPSecIfParam->IPSecReq.InstallSA.aOutSABundle[IKE_AH_SA_INDEX].
                    u1Mode = IKE_IPSEC_TRANSPORT_MODE;
            }

            if (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.u4Type == IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.uNetwork.Ip4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.
                               uNetwork.Ip4Addr);

                bIsv4Addr = IKE_TRUE;
            }
            else if (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.u4Type ==
                     IKE_IPSEC_ID_IPV4_SUBNET)
            {
                pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.uNetwork.Ip4Subnet.
                    Ip4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.
                               uNetwork.Ip4Subnet.Ip4Addr);

                pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.uNetwork.Ip4Subnet.
                    Ip4SubnetMask =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.
                               uNetwork.Ip4Subnet.Ip4SubnetMask);
                bIsv4Addr = IKE_TRUE;
            }
            else if (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.u4Type ==
                     IKE_IPSEC_ID_IPV4_RANGE)
            {
                pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.uNetwork.Ip4Range.
                    Ip4StartAddr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.
                               uNetwork.Ip4Range.Ip4StartAddr);

                pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.uNetwork.Ip4Range.
                    Ip4EndAddr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.LocalProxy.
                               uNetwork.Ip4Range.Ip4EndAddr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.u4Type ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.uNetwork.Ip4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.
                               uNetwork.Ip4Addr);
            }
            else if (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.u4Type ==
                     IKE_IPSEC_ID_IPV4_SUBNET)
            {
                pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.uNetwork.
                    Ip4Subnet.Ip4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.
                               uNetwork.Ip4Subnet.Ip4Addr);

                pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.uNetwork.
                    Ip4Subnet.Ip4SubnetMask =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.
                               uNetwork.Ip4Subnet.Ip4SubnetMask);
            }
            else if (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.u4Type ==
                     IKE_IPSEC_ID_IPV4_RANGE)
            {
                pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.uNetwork.
                    Ip4Range.Ip4StartAddr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.
                               uNetwork.Ip4Range.Ip4StartAddr);

                pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.uNetwork.
                    Ip4Range.Ip4EndAddr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.RemoteProxy.
                               uNetwork.Ip4Range.Ip4EndAddr);
            }

            if (pIPSecIfParam->IPSecReq.InstallSA.LocTunnTermAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.InstallSA.LocTunnTermAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               LocTunnTermAddr.Ipv4Addr);
            }

            if (pIPSecIfParam->IPSecReq.InstallSA.RemTunnTermAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.InstallSA.RemTunnTermAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.InstallSA.
                               RemTunnTermAddr.Ipv4Addr);
            }

            IKE_MEMCPY (&IPSecMsg.IkeIfParam.InstallSA,
                        &pIPSecIfParam->IPSecReq.InstallSA,
                        sizeof (tIPSecBundle));

            break;

        case IKE_IPSEC_DUPLICATE_SPI_REQ:
            IPSecMsg.u4MsgType = IKE_HTONL (IKE_IPSEC_DUPLICATE_SPI_REQ);

            pIPSecIfParam->IPSecReq.DupSpiInfo.u4EspSpi =
                IKE_HTONL (pIPSecIfParam->IPSecReq.DupSpiInfo.u4EspSpi);
            pIPSecIfParam->IPSecReq.DupSpiInfo.u4AhSpi =
                IKE_HTONL (pIPSecIfParam->IPSecReq.DupSpiInfo.u4AhSpi);

            if (pIPSecIfParam->IPSecReq.DupSpiInfo.LocalAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DupSpiInfo.LocalAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DupSpiInfo.LocalAddr.
                               Ipv4Addr);

                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DupSpiInfo.RemoteAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DupSpiInfo.RemoteAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DupSpiInfo.RemoteAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            IKE_MEMCPY (&IPSecMsg.IkeIfParam.DupSpiInfo,
                        &pIPSecIfParam->IPSecReq.DupSpiInfo,
                        sizeof (tIkeDupSpi));

            break;

        case IKE_IPSEC_PROCESS_DELETE_SA:
            IPSecMsg.u4MsgType = IKE_HTONL (IKE_IPSEC_PROCESS_DELETE_SA);

            u2NumSPI = (UINT2) ((pIPSecIfParam->IPSecReq.DelSpiInfo.u2NumSPI <=
                                 IKE_MAX_NUM_SPI) ?
                                pIPSecIfParam->IPSecReq.DelSpiInfo.
                                u2NumSPI : IKE_MAX_NUM_SPI);

            for (u4SpiCount = IKE_ZERO; u4SpiCount < u2NumSPI; u4SpiCount++)
            {
                pIPSecIfParam->IPSecReq.DelSpiInfo.au4SPI[u4SpiCount] =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSpiInfo.
                               au4SPI[u4SpiCount]);
            }

            pIPSecIfParam->IPSecReq.DelSpiInfo.u2NumSPI =
                IKE_HTONS (pIPSecIfParam->IPSecReq.DelSpiInfo.u2NumSPI);
            if (pIPSecIfParam->IPSecReq.DelSpiInfo.LocalAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSpiInfo.LocalAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSpiInfo.LocalAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DelSpiInfo.RemoteAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSpiInfo.RemoteAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSpiInfo.RemoteAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DelSpiInfo.u1Protocol == IKE_IPSEC_ESP)
            {
                pIPSecIfParam->IPSecReq.DelSpiInfo.u1Protocol =
                    IKE_IP_PROTO_ESP;
            }
            else if (pIPSecIfParam->IPSecReq.DelSpiInfo.u1Protocol ==
                     IKE_IPSEC_AH)
            {
                pIPSecIfParam->IPSecReq.DelSpiInfo.u1Protocol = IKE_IP_PROTO_AH;
            }

            IKE_MEMCPY (&IPSecMsg.IkeIfParam.DelSpiInfo,
                        &pIPSecIfParam->IPSecReq.DelSpiInfo,
                        sizeof (tIkeDelSpi));
            break;

        case IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR:
            IPSecMsg.u4MsgType = IKE_HTONL (IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR);

            if (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }
            IKE_MEMCPY (&IPSecMsg.IkeIfParam.DelSAInfo,
                        &pIPSecIfParam->IPSecReq.DelSAInfo,
                        sizeof (tIkeDelSAByAddr));
            break;
        case IKE_IPSEC_PROCESS_ADD_TIME_DEL_SA:
            IPSecMsg.u4MsgType = IKE_HTONL (IKE_IPSEC_PROCESS_ADD_TIME_DEL_SA);

            if (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }
            IKE_MEMCPY (&IPSecMsg.IkeIfParam.DelSAInfo,
                        &pIPSecIfParam->IPSecReq.DelSAInfo,
                        sizeof (tIkeDelSAByAddr));
            break;

        case IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG:
            IPSecMsg.u4MsgType =
                IKE_HTONL (IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG);

            if (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.LocalAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }

            if (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.u4AddrType ==
                IPV4ADDR)
            {
                pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.Ipv4Addr =
                    IKE_HTONL (pIPSecIfParam->IPSecReq.DelSAInfo.RemoteAddr.
                               Ipv4Addr);
                bIsv4Addr = IKE_TRUE;
            }
            IKE_MEMCPY (&IPSecMsg.IkeIfParam.DelSAInfo,
                        &pIPSecIfParam->IPSecReq.DelSAInfo,
                        sizeof (tIkeDelSAByAddr));

            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSendIPSecIfParam:Unsupported request type for "
                         "IPSec\n");
            return IKE_FAILURE;
    }

    /* Send message to CFA Queue */
    if (bIsv4Addr == IKE_TRUE)
    {
#ifdef IPSECv4_WANTED
        IkeHandleMsgToSecv4 (&IPSecMsg);
#endif
    }
    else
    {
#ifdef IPSECv6_WANTED
        Secv6ProcessIKEMsg (&IPSecMsg);
#endif
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeHandleMsgToSecv4                
 *  Description   : Function to Post the Message to IPSEC directly
 *                  or using a ioctl call when IPSEC is in kernel.
 *  Input(s)      : pIPSecIfParam - The params required by IPSec
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeHandleMsgToSecv4 (tIkeIPSecQMsg * pIPSecMsg)
{
    Secv4ProcessIKEMsg (pIPSecMsg);
    return IKE_SUCCESS;
}

/***********************************************************************/
/*  Function Name : IkeProcessKernelIpsecRequest                       */
/*  Description   : This function is used to post the message to IKE   */
/*                : from kernel space                                  */
/*  Input(s)      : tIkeQMsg - structure to hold the IKE Message       */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/
VOID
IkeProcessKernelIpsecRequest (void *pMsg)
{
    tIkeQMsg           *pIkeQMsg = NULL;

    if (MemAllocateMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 **) (VOID *) &pIkeQMsg) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessKernelIpsecRequest: unable to allocate "
                     "buffer\n");
        return;
    }

    if (pIkeQMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessKernelIpsecRequest: unable to allocate "
                     "buffer\n");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
        return;
    }

    IKE_MEMCPY (pIkeQMsg, (tIkeQMsg *) pMsg, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_IF_MSG;
    IkeHandlePktFromIpsec (pIkeQMsg);
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIpsecRequest 
 *  Description   : This will be called from ike main if any data
 *                  received from ipsec queue.
 *  Input(s)      : tIkeIPSecIfParam - IPSec Information
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */

VOID
IkeProcessIpsecRequest (tIkeIPSecIfParam * pIPSecParam)
{

    pIPSecParam->u4MsgType = IKE_NTOHL (pIPSecParam->u4MsgType);
    switch (pIPSecParam->u4MsgType)
    {
        case IKE_IPSEC_REKEY:
            /* Increment Phase2 rekey statistics */
            IncIkePhase2ReKey ();
        case IKE_IPSEC_NEW_KEY:
            IkeProcessIPSecSaTriggerRequest (&pIPSecParam->IPSecReq.
                                             SaTriggerInfo);
            break;

        case IKE_IPSEC_INVALID_SPI:
#if IKE_ZERO
            IkeProcessIPSecInvalidSpiRequest (&pIPSecParam->IPSecReq.
                                              InvalidSpiInfo);
#endif
            break;

        case IKE_IPSEC_SEND_DELETE_SA:
            IkeProcessIPSecDeleteSARequest (&pIPSecParam->IPSecReq.DelSpiInfo);
            break;

        case IKE_IPSEC_DUPLICATE_SPI_REPLY:
            IkeProcessIPSecDuplicateSPIReply (&pIPSecParam->IPSecReq.
                                              DupSpiInfo);
            break;

        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIpsecRequest: Unsupported message "
                         "event from ipsec\n");
            break;
    }
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIPSecSaTriggerRequest
 *  Description   : Function to Process IPSec trigger                     
 *  Input(s)      : pSaTriggerInfo - The IPSec Trigger Info
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeProcessIPSecSaTriggerRequest (tIkeNewSA * pSaTriggerInfo)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCryptoMap      *pCryptoMap = NULL;
    tIkeIpAddr          IpPeerAddr;
    tIkeSA             *pIkeSA = NULL;
    tIkeSessionInfo    *pSessionInfo = NULL;
    tIkeRemoteAccessInfo *pRAInfo = NULL;
    UINT4               u4CurrTime = IKE_ZERO;
    UINT4               u4Diff = IKE_ZERO;
    INT1                i1RetVal = IKE_FAILURE;
    UINT1               u1ChildType = IKE_ZERO;

    /* Convert the information from IPSec to host order */

    pSaTriggerInfo->u2PortNumber = IKE_NTOHS (pSaTriggerInfo->u2PortNumber);

    pSaTriggerInfo->LocalTEAddr.u4AddrType =
        IKE_NTOHL (pSaTriggerInfo->LocalTEAddr.u4AddrType);

    if (pSaTriggerInfo->LocalTEAddr.u4AddrType == IPV4ADDR)
    {
        pSaTriggerInfo->LocalTEAddr.Ipv4Addr =
            IKE_NTOHL (pSaTriggerInfo->LocalTEAddr.Ipv4Addr);
    }

    pIkeEngine = IkeGetIkeEngine (&pSaTriggerInfo->LocalTEAddr);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequest: Unable to find "
                     "Engine\n");
        return IKE_FAILURE;
    }

    if (pIkeEngine->i2SocketId == IKE_INVALID)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequest: No socket is bound "
                     "for the engine\n");
        return IKE_FAILURE;
    }

    pSaTriggerInfo->RemoteTEAddr.u4AddrType =
        IKE_NTOHL (pSaTriggerInfo->RemoteTEAddr.u4AddrType);

    if (pSaTriggerInfo->RemoteTEAddr.u4AddrType == IPV4ADDR)
    {
        pSaTriggerInfo->RemoteTEAddr.Ipv4Addr =
            IKE_NTOHL (pSaTriggerInfo->RemoteTEAddr.Ipv4Addr);
    }
    TAKE_IKE_SEM ();
    pRAInfo = IkeGetRAInfoFromPeerIpAddr (pIkeEngine,
                                          &pSaTriggerInfo->RemoteTEAddr);
    if (pRAInfo != NULL)
    {
        IKE_MEMCPY (&IpPeerAddr, &pRAInfo->PeerAddr, sizeof (tIkeIpAddr));
        GIVE_IKE_SEM ();
    }
    else
    {

        GIVE_IKE_SEM ();

        pSaTriggerInfo->SrcNet.u4Type =
            IKE_NTOHL (pSaTriggerInfo->SrcNet.u4Type);

        switch (pSaTriggerInfo->SrcNet.u4Type)
        {
            case IKE_IPSEC_ID_IPV4_ADDR:
            {
                pSaTriggerInfo->SrcNet.uNetwork.Ip4Addr =
                    IKE_NTOHL (pSaTriggerInfo->SrcNet.uNetwork.Ip4Addr);
                break;
            }
            case IKE_IPSEC_ID_IPV4_SUBNET:
            {
                pSaTriggerInfo->SrcNet.uNetwork.Ip4Subnet.Ip4Addr =
                    IKE_NTOHL (pSaTriggerInfo->SrcNet.uNetwork.Ip4Subnet.
                               Ip4Addr);

                pSaTriggerInfo->SrcNet.uNetwork.Ip4Subnet.Ip4SubnetMask =
                    IKE_NTOHL (pSaTriggerInfo->SrcNet.uNetwork.Ip4Subnet.
                               Ip4SubnetMask);
                break;
            }
            case IKE_IPSEC_ID_IPV4_RANGE:
            {
                pSaTriggerInfo->SrcNet.uNetwork.Ip4Range.Ip4StartAddr =
                    IKE_NTOHL (pSaTriggerInfo->SrcNet.uNetwork.Ip4Range.
                               Ip4StartAddr);
                pSaTriggerInfo->SrcNet.uNetwork.Ip4Range.Ip4EndAddr =
                    IKE_NTOHL (pSaTriggerInfo->SrcNet.uNetwork.Ip4Range.
                               Ip4EndAddr);
                break;
            }
            case IKE_IPSEC_ID_IPV6_ADDR:
            case IKE_IPSEC_ID_IPV6_SUBNET:
            case IKE_IPSEC_ID_IPV6_RANGE:
                break;
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                             "IkeProcessIPSecSaTriggerRequest: "
                             "Invalid Address Type\n");
                return IKE_FAILURE;
            }

        }

        /* Copy The Protocol & Port Info */
        pSaTriggerInfo->SrcNet.u2StartPort =
            IKE_NTOHS (pSaTriggerInfo->SrcNet.u2StartPort);
        pSaTriggerInfo->SrcNet.u2EndPort =
            IKE_NTOHS (pSaTriggerInfo->SrcNet.u2EndPort);

        pSaTriggerInfo->DestNet.u4Type =
            IKE_NTOHL (pSaTriggerInfo->DestNet.u4Type);

        switch (pSaTriggerInfo->DestNet.u4Type)
        {
            case IPV4ADDR:
            {
                pSaTriggerInfo->DestNet.uNetwork.Ip4Addr =
                    IKE_NTOHL (pSaTriggerInfo->DestNet.uNetwork.Ip4Addr);
                break;
            }
            case IKE_IPSEC_ID_IPV4_SUBNET:
            {
                pSaTriggerInfo->DestNet.uNetwork.Ip4Subnet.Ip4Addr =
                    IKE_NTOHL (pSaTriggerInfo->DestNet.uNetwork.Ip4Subnet.
                               Ip4Addr);

                pSaTriggerInfo->DestNet.uNetwork.Ip4Subnet.Ip4SubnetMask =
                    IKE_NTOHL (pSaTriggerInfo->DestNet.uNetwork.Ip4Subnet.
                               Ip4SubnetMask);
                break;
            }
            case IKE_IPSEC_ID_IPV4_RANGE:
            {
                pSaTriggerInfo->DestNet.uNetwork.Ip4Range.Ip4StartAddr =
                    IKE_NTOHL (pSaTriggerInfo->DestNet.uNetwork.Ip4Range.
                               Ip4StartAddr);
                pSaTriggerInfo->DestNet.uNetwork.Ip4Range.Ip4EndAddr =
                    IKE_NTOHL (pSaTriggerInfo->DestNet.uNetwork.Ip4Range.
                               Ip4EndAddr);
            }
            case IKE_IPSEC_ID_IPV6_ADDR:
            case IKE_IPSEC_ID_IPV6_SUBNET:
            case IKE_IPSEC_ID_IPV6_RANGE:
                break;
            default:
            {
                IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                             "IkeProcessIPSecSaTriggerRequest: "
                             "Invalid Address Type\n");
                return IKE_FAILURE;
            }

        }
        /* Copy The Protocol & Port Info */
        pSaTriggerInfo->DestNet.u2StartPort =
            IKE_NTOHS (pSaTriggerInfo->DestNet.u2StartPort);
        pSaTriggerInfo->DestNet.u2EndPort =
            IKE_NTOHS (pSaTriggerInfo->DestNet.u2EndPort);

        TAKE_IKE_SEM ();
        pCryptoMap = IkeGetCryptoMapFromIDs (pIkeEngine,
                                             &pSaTriggerInfo->SrcNet,
                                             &pSaTriggerInfo->DestNet,
                                             pSaTriggerInfo->u1IkeVersion);
        if (pCryptoMap == NULL)
        {
            GIVE_IKE_SEM ();
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecSaTriggerRequest:\
                         No Crypto Map found\n");
            return IKE_FAILURE;
        }

        IKE_MEMCPY (&IpPeerAddr, &pCryptoMap->PeerAddr, sizeof (tIkeIpAddr));
        GIVE_IKE_SEM ();
    }
    /* Check for ongoing negotiation */
    SLL_SCAN (&pIkeEngine->IkeSessionList, pSessionInfo, tIkeSessionInfo *)
    {
        if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV4ADDR)
        {
            if (IKE_MEMCMP
                (&pSessionInfo->RemoteTunnelTermAddr.Ipv4Addr,
                 &IpPeerAddr.Ipv4Addr, sizeof (tIp4Addr)) == IKE_ZERO)
            {
                break;
            }
        }
        else
        {
            if (IKE_MEMCMP
                (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr,
                 &IpPeerAddr.Ipv6Addr, sizeof (tIp6Addr)) == IKE_ZERO)
            {
                break;
            }
        }
    }

    if ((pSaTriggerInfo->bRekey != TRUE) && (pSessionInfo != NULL))
    {
        /* We have to queue new IpSec request for NEW KEY in case of parallel
         * VPNs */
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequest: IKE session is "
                     "ongoing\n");

        OsixGetSysTime (&u4CurrTime);
        if (u4CurrTime > pSessionInfo->u4TimeStamp)
        {
            u4Diff = u4CurrTime - pSessionInfo->u4TimeStamp;
            u4Diff = u4Diff / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
            if (u4Diff > IKE_DEFAULT_SESSION_LIFETIME)
            {
                pSessionInfo->u4TimeStamp = u4CurrTime;
                if (IkeStartTimer (&pSessionInfo->IkeSessionDeleteTimer,
                                   IKE_DELETE_SESSION_TIMER_ID,
                                   IKE_DELAYED_SESSION_DELETE_INTERVAL,
                                   pSessionInfo) != IKE_SUCCESS)
                {
                    /* Not critical error, just log */
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessIPSecSaTriggerRequest: "
                                 "IkeStartTimer failed for Delete Session!\n");
                }
            }
        }
        return IKE_SUCCESS;
    }

    /* Search for ike sa and then look for phase1 */
    /* If Ike sa is existing, then jump to phase 2 depending on the 
       request type */
    pIkeSA = IkeGetIkeSAFromPeerAddress (pIkeEngine, &IpPeerAddr);
    if ((pIkeSA != NULL) && ( pSaTriggerInfo->bRekey == IKE_TRUE ))
    {
        if (pSaTriggerInfo->u1IkeVersion == IKE_ISAKMP_VERSION)
        {
            /* Phase 1 SA is existing */
            pSessionInfo = IkePhase2SessionInit (pIkeSA, pSaTriggerInfo,
                                                 IKE_ZERO, IKE_INITIATOR);
        }

        if (pSaTriggerInfo->u1IkeVersion == IKE2_MAJOR_VERSION)
        {
            /* Phase 1 SA is existing */
            if (pIkeSA->Ike2SA.bAuthDone != IKE_TRUE)
            {
                pSessionInfo =
                    Ike2SessAuthExchSessInit (pIkeSA, pSaTriggerInfo,
                                              IKE2_AUTH_MSG_ID, IKE_INITIATOR);
            }
            else
            {
                if (pSaTriggerInfo->bRekey == IKE_TRUE)
                {
                    u1ChildType = IKE2_IPSEC_REKEY_CHILD;
                }
                else
                {
                    u1ChildType = IKE2_IPSEC_NEW_CHILD;
                }
                /* Auth is already Done... Directly go to Child Exch .. */
                pSessionInfo =
                    Ike2SessChildExchSessInit (pIkeSA, pSaTriggerInfo, IKE_ZERO,
                                               IKE_INITIATOR, u1ChildType);
                if (pSessionInfo == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessIPSecSaTriggerRequest:\
                                 Phase2 Session " "Init Failed\n");
                    return IKE_FAILURE;
                }
                if (pSaTriggerInfo->bRekey == IKE_TRUE)
                {
                    pSessionInfo->Ike2AuthInfo.u4ReKeyingSpi =
                        IKE_NTOHL (pSaTriggerInfo->u4ReKeyingSpi);
                    pSessionInfo->Ike2AuthInfo.u4ReKeyingSpi = sizeof (UINT4);
                }
            }
        }

        if (pSessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecSaTriggerRequest: Phase2 Session "
                         "Init Failed\n");
            return IKE_FAILURE;
        }

        if ((pSessionInfo->u1FsmState == IKE_QM_WAIT_ON_IPSEC) ||
            (pSessionInfo->u1FsmState == IKE2_WAIT_ON_IPSEC))
        {
            /* Phase2SessionInit would have sent a message to
               IPSec to check for duplicate SPI, will have to
               wait for the reply before starting the Phase2
               negotiation */
            return IKE_SUCCESS;
        }

        if (pSaTriggerInfo->u1IkeVersion == IKE_ISAKMP_VERSION)
        {
            i1RetVal = IkeCoreProcess (pSessionInfo);
        }

        if (pSaTriggerInfo->u1IkeVersion == IKE2_MAJOR_VERSION)
        {
            i1RetVal = Ike2CoreProcess (pSessionInfo);
        }

        if (i1RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecSaTriggerRequest: IkeCoreProcess "
                         "Failed\n");
            IkeHandleCoreFailure (pSessionInfo);
            return IKE_FAILURE;
        }
        /* Core Process is Success, Send the Packet Out on the 
         * wire */
        IkeHandleCoreSuccess (pSessionInfo);
        return IKE_SUCCESS;
    }

    if (pSaTriggerInfo->u1IkeVersion == IKE_ISAKMP_VERSION)
    {
        pSessionInfo = IkePhase1SessionInit (IKE_INITIATOR,
                                             pIkeEngine, IKE_ZERO, &IpPeerAddr);
    }
    else if (pSaTriggerInfo->u1IkeVersion == IKE2_MAJOR_VERSION)
    {
        pSessionInfo = Ike2SessInitExchSessInit (pIkeEngine, IKE_INITIATOR,
                                                 &IpPeerAddr);
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequest: Invalid IKE "
                     "version\n");
        return IKE_FAILURE;
    }

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequeste: Unable to create "
                     "SessionInfo\n");
        return IKE_FAILURE;
    }

    /* Store the new SA information */
    pSessionInfo->PostPhase1Info.u4ActionType = IKE_POST_P1_NEW_SA;

    IKE_MEMCPY (&pSessionInfo->PostPhase1Info.IPSecRequest, pSaTriggerInfo,
                sizeof (tIkeNewSA));

    if (pSaTriggerInfo->u1IkeVersion == IKE_ISAKMP_VERSION)
    {
        i1RetVal = IkeCoreProcess (pSessionInfo);
    }
    if (pSaTriggerInfo->u1IkeVersion == IKE2_MAJOR_VERSION)
    {
        i1RetVal = Ike2CoreProcess (pSessionInfo);
    }
    if (i1RetVal == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecSaTriggerRequest: Phase1/Init Session "
                     "initiation Failed\n");

        IkeHandleCoreFailure (pSessionInfo);
        return IKE_FAILURE;
    }
    /* Core Process is Success, Send the Packet Out on the 
     * wire */
    IkeHandleCoreSuccess (pSessionInfo);
    return IKE_SUCCESS;
}

#if IKE_ZERO
/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIPSecInvalidSpiRequest
 *  Description   : Function to Process IPSec Invalid SPI request         
 *  Input(s)      : pInvalidSpiInfo - Invalid SPI Info                    
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeProcessIPSecInvalidSpiRequest (tIkeInvalidSpi * pInvalidSpiInfo)
{
    tIkePhase1PostProcessInfo PostPhase1Info;

    IKE_BZERO (&PostPhase1Info, sizeof (tIkePhase1PostProcessInfo));

    pInvalidSpiInfo->LocalAddr.u4AddrType =
        IKE_NTOHL (pInvalidSpiInfo->LocalAddr.u4AddrType);
    if (pInvalidSpiInfo->LocalAddr.u4AddrType == IPV4ADDR)
    {
        pInvalidSpiInfo->LocalAddr.Ipv4Addr =
            IKE_NTOHL (pInvalidSpiInfo->LocalAddr.Ipv4Addr);
    }

    pInvalidSpiInfo->RemoteAddr.u4AddrType =
        IKE_NTOHL (pInvalidSpiInfo->RemoteAddr.u4AddrType);
    if (pInvalidSpiInfo->RemoteAddr.u4AddrType == IPV4ADDR)
    {
        pInvalidSpiInfo->RemoteAddr.Ipv4Addr =
            IKE_NTOHL (pInvalidSpiInfo->RemoteAddr.Ipv4Addr);
    }

    PostPhase1Info.u4ActionType = IKE_POST_P1_NOTIFY;
    if (pInvalidSpiInfo->u1Protocol == IKE_IP_PROTO_ESP)
    {
        pInvalidSpiInfo->u1Protocol = IKE_IPSEC_ESP;
    }
    else if (pInvalidSpiInfo->u1Protocol == IKE_IP_PROTO_AH)
    {
        pInvalidSpiInfo->u1Protocol = IKE_IPSEC_AH;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessIPSecInvalidSpiRequest: Invalid protocol"
                     "received from IPSec\n");
        return IKE_FAILURE;
    }

    PostPhase1Info.IkeNotifyInfo.u1Protocol = pInvalidSpiInfo->u1Protocol;
    PostPhase1Info.IkeNotifyInfo.u1SpiSize = IKE_ZERO;
    PostPhase1Info.IkeNotifyInfo.u1CryptFlag = IKE_ENCRYPTION;
    PostPhase1Info.IkeNotifyInfo.u2NotifyType = IKE_INVALID_SPI;
    PostPhase1Info.IkeNotifyInfo.u2NotifyDataLen = sizeof (UINT4);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &PostPhase1Info.IkeNotifyInfo.pu1NotifyData) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecInvalidSpiRequest: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    if (PostPhase1Info.IkeNotifyInfo.pu1NotifyData == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessIPSecInvalidSpiRequest: Not Enough Memory\n");
        return IKE_FAILURE;
    }
    IKE_BZERO (PostPhase1Info.IkeNotifyInfo.pu1NotifyData, sizeof (UINT4));

    IKE_MEMCPY (PostPhase1Info.IkeNotifyInfo.pu1NotifyData,
                &pInvalidSpiInfo->u4Spi, sizeof (UINT4));

    if (IkeProcessIPSecNotifyRequest
        (&PostPhase1Info, &pInvalidSpiInfo->LocalAddr,
         &pInvalidSpiInfo->RemoteAddr) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecInvalidSpiRequest: Unable to find "
                     "Engine\n");
    }

    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                        (UINT1 *) PostPhase1Info.IkeNotifyInfo.pu1NotifyData);
    return IKE_SUCCESS;
}
#endif

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIPSecDeleteSARequest  
 *  Description   : Function to Process IPSec Delete SA request           
 *  Input(s)      : pDelSpiInfo - The Delete SPI Info                     
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeProcessIPSecDeleteSARequest (tIkeDelSpi * pDelSpiInfo)
{
    tIkePhase1PostProcessInfo PostPhase1Info;
    UINT4               u4Spi;
    UINT4               u4SpiCount;
    UINT2               u2NumSPI = IKE_ZERO;

    IKE_BZERO (&PostPhase1Info, sizeof (tIkePhase1PostProcessInfo));

    /* Convert IPSec information to host order */
    pDelSpiInfo->u2NumSPI = IKE_NTOHS (pDelSpiInfo->u2NumSPI);

    pDelSpiInfo->LocalAddr.u4AddrType =
        IKE_NTOHL (pDelSpiInfo->LocalAddr.u4AddrType);
    if (pDelSpiInfo->LocalAddr.u4AddrType == IPV4ADDR)
    {
        pDelSpiInfo->LocalAddr.Ipv4Addr =
            IKE_NTOHL (pDelSpiInfo->LocalAddr.Ipv4Addr);
    }

    pDelSpiInfo->RemoteAddr.u4AddrType =
        IKE_NTOHL (pDelSpiInfo->RemoteAddr.u4AddrType);
    if (pDelSpiInfo->RemoteAddr.u4AddrType == IPV4ADDR)
    {
        pDelSpiInfo->RemoteAddr.Ipv4Addr =
            IKE_NTOHL (pDelSpiInfo->RemoteAddr.Ipv4Addr);
    }
    if (pDelSpiInfo->u1Protocol == IKE_IP_PROTO_ESP)
    {
        pDelSpiInfo->u1Protocol = IKE_IPSEC_ESP;
    }
    else if (pDelSpiInfo->u1Protocol == IKE_IP_PROTO_AH)
    {
        pDelSpiInfo->u1Protocol = IKE_IPSEC_AH;
    }
    else
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessIPSecDeleteSARequest: Invalid protocol "
                     "received from IPSec\n");
        return IKE_FAILURE;
    }

    PostPhase1Info.u4ActionType = IKE_POST_P1_DELETE;
    PostPhase1Info.IkeNotifyInfo.u1Protocol = pDelSpiInfo->u1Protocol;
    PostPhase1Info.IkeNotifyInfo.u1SpiSize = IKE_IPSEC_SPI_SIZE;
    PostPhase1Info.IkeNotifyInfo.u1CryptFlag = IKE_ENCRYPTION;
    PostPhase1Info.IkeNotifyInfo.u2NotifyType = IKE_POST_P1_DELETE;

    PostPhase1Info.IkeNotifyInfo.u2SpiNum = pDelSpiInfo->u2NumSPI;
    PostPhase1Info.IkeNotifyInfo.u2NotifyDataLen = (UINT2)
        (pDelSpiInfo->u2NumSPI * IKE_IPSEC_SPI_SIZE);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &PostPhase1Info.IkeNotifyInfo.pu1NotifyData) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecDeleteSARequest: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (PostPhase1Info.IkeNotifyInfo.pu1NotifyData == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeProcessIPSecDeleteSARequest: Not Enough Memory\n");
        return IKE_FAILURE;
    }

    u2NumSPI = (UINT2) ((pDelSpiInfo->u2NumSPI <= IKE_MAX_NUM_SPI) ?
                        pDelSpiInfo->u2NumSPI : IKE_MAX_NUM_SPI);

    for (u4SpiCount = IKE_ZERO; u4SpiCount < u2NumSPI; u4SpiCount++)
    {
        u4Spi = pDelSpiInfo->au4SPI[u4SpiCount];
        IKE_MEMCPY (PostPhase1Info.IkeNotifyInfo.pu1NotifyData +
                    (u4SpiCount * IKE_IPSEC_SPI_SIZE), &u4Spi,
                    IKE_IPSEC_SPI_SIZE);
    }

    if (IkeProcessIPSecNotifyRequest (&PostPhase1Info, &pDelSpiInfo->LocalAddr,
                                      &pDelSpiInfo->RemoteAddr,
                                      pDelSpiInfo->u1IkeVersion) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecDeleteSARequest: "
                     "IkeProcessIPSecNotifyRequest failed \n");
    }

    if (PostPhase1Info.IkeNotifyInfo.pu1NotifyData != NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) PostPhase1Info.IkeNotifyInfo.
                            pu1NotifyData);
        PostPhase1Info.IkeNotifyInfo.pu1NotifyData = NULL;
    }
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIPSecDuplicateSPIReply  
 *  Description   : Function to Process IPSec Duplicate SPI request       
 *  Input(s)      : pDupSpiInfo - The Duplicate SPI Info                  
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeProcessIPSecDuplicateSPIReply (tIkeDupSpi * pDupSpiInfo)
{
    tIkeSessionInfo    *pSessionInfo = NULL;
    UINT4               u4Spi = IKE_ZERO;
    tIkeIPSecIfParam   *pIkeIfParam = NULL;
    INT1                i1RetVal = IKE_FAILURE;
    CHR1               *pu1PeerAddrStr = NULL;

    if (MemAllocateMemBlock
        (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pIkeIfParam) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecDuplicateSPIReply: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }

    /* Convert IPSec information to host order */
    pDupSpiInfo->u4AhSpi = IKE_NTOHL (pDupSpiInfo->u4AhSpi);
    pDupSpiInfo->u4EspSpi = IKE_NTOHL (pDupSpiInfo->u4EspSpi);

    pDupSpiInfo->LocalAddr.u4AddrType =
        IKE_NTOHL (pDupSpiInfo->LocalAddr.u4AddrType);
    if (pDupSpiInfo->LocalAddr.u4AddrType == IPV4ADDR)
    {
        pDupSpiInfo->LocalAddr.Ipv4Addr =
            IKE_NTOHL (pDupSpiInfo->LocalAddr.Ipv4Addr);
    }

    pDupSpiInfo->RemoteAddr.u4AddrType =
        IKE_NTOHL (pDupSpiInfo->RemoteAddr.u4AddrType);
    if (pDupSpiInfo->RemoteAddr.u4AddrType == IPV4ADDR)
    {
        pDupSpiInfo->RemoteAddr.Ipv4Addr =
            IKE_NTOHL (pDupSpiInfo->RemoteAddr.Ipv4Addr);
    }

    pSessionInfo = (tIkeSessionInfo *) pDupSpiInfo->u4Context;

    if (pSessionInfo == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecDuplicateSPIReply  : Session "
                     "context NULL!\n");
        MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                            (UINT1 *) pIkeIfParam);
        return IKE_FAILURE;
    }

    if (pSessionInfo->u1SessionStatus != IKE_ACTIVE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecDuplicateSPIReply  : Session context "
                     "no longer valid\n");
        MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                            (UINT1 *) pIkeIfParam);
        return IKE_FAILURE;
    }

    pSessionInfo->u4RefCount--;
    if (pDupSpiInfo->bDupSpi == IKE_TRUE)
    {
        /* re-generate SPI numbers */
        IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
        pDupSpiInfo->u4AhSpi = IKE_HTONL (u4Spi);
        pSessionInfo->IkePhase2Info.IPSecBundle.
            aInSABundle[IKE_AH_SA_INDEX].u4Spi = u4Spi;

        IkeGetRandom ((UINT1 *) &u4Spi, sizeof (UINT4));
        pDupSpiInfo->u4EspSpi = IKE_HTONL (u4Spi);
        pSessionInfo->IkePhase2Info.IPSecBundle.
            aInSABundle[IKE_ESP_SA_INDEX].u4Spi = u4Spi;

        pIkeIfParam->u4MsgType = IKE_IPSEC_DUPLICATE_SPI_REQ;
        IKE_MEMCPY (&pIkeIfParam->IPSecReq.DupSpiInfo, pDupSpiInfo,
                    sizeof (tIkeDupSpi));
        pSessionInfo->u4RefCount++;

        if (IkeSendIPSecIfParam (pIkeIfParam) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecDuplicateSPIReply  : Send to "
                         "IPSec Q Failed\n");
            MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                (UINT1 *) pIkeIfParam);
            return IKE_FAILURE;
        }
    }
    else
    {
        /* This is a not a duplicate SPI, Proceed for Phase2 negotiation */
        if (pDupSpiInfo->u1IkeVersion == IKE_ISAKMP_VERSION)
        {
            pSessionInfo->u1FsmState = IKE_QM_IDLE;
            i1RetVal = IkeCoreProcess (pSessionInfo);
        }
        else
        {
            if (pSessionInfo->u1ExchangeType == IKE2_AUTH_EXCH)
            {
                pSessionInfo->u1FsmState = IKE2_AUTH_IDLE;
            }
            else
            {
                pSessionInfo->u1FsmState = IKE2_CHILD_IDLE;
            }
            i1RetVal = Ike2CoreProcess (pSessionInfo);
        }

        if (i1RetVal == IKE_FAILURE)
        {
            if (pSessionInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
            {
                pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
                    (&pSessionInfo->RemoteTunnelTermAddr.Ipv6Addr);
            }
            else
            {
                UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                        pSessionInfo->RemoteTunnelTermAddr.
                                        Ipv4Addr);
            }
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecDuplicateSPIReply : Quick Mode/Auth/"
                         "Child Session Initiation Failed\n");

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                          "IkeProcessIPSecDuplicateSPIReply: "
                          "Quick Mode/Auth/Child Session Initiation Failed for peer: %s\n",
                          pu1PeerAddrStr));

            IkeHandleCoreFailure (pSessionInfo);
            MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID,
                                (UINT1 *) pIkeIfParam);
            return IKE_FAILURE;
        }
        /* Core Process is Success, Send the Packet Out on the 
         * wire */
        IkeHandleCoreSuccess (pSessionInfo);
    }
    MemReleaseMemBlock (IKE_IPSEC_IFPARAM_MEMPOOL_ID, (UINT1 *) pIkeIfParam);
    return IKE_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessIPSecNotifyRequest        
 *  Description   : Function to Process IPSec Notify request              
 *  Input(s)      : pNotifyInfo - The Notification Info                   
 *                : pLocalAddr  - The Local Address                       
 *                : pRemoteAddr - The Remote Address                      
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */

INT1
IkeProcessIPSecNotifyRequest (tIkePhase1PostProcessInfo * pNotifyInfo,
                              tIkeIpAddr * pLocalAddr, tIkeIpAddr * pRemoteAddr,
                              UINT1 u1IkeVersion)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeSA             *pIkeSA = NULL;
    tIkeSessionInfo    *pSessionInfo = NULL;
    INT1                i1RetVal = IKE_FAILURE;

    pIkeEngine = IkeGetIkeEngine (pLocalAddr);
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecNotifyRequest: Unable to find Engine\n");
        return IKE_FAILURE;
    }

    if (pIkeEngine->i2SocketId == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecNotifyRequest: No socket is bound for "
                     "the engine\n");
        return IKE_FAILURE;
    }

    pIkeSA = IkeGetIkeSAFromPeerAddress (pIkeEngine, pRemoteAddr);
    if (pIkeSA == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessIPSecNotifyRequest: No IkeSA found\n");
    }

    if (pIkeSA != NULL)
    {
        if (pIkeSA->u1IkeVer == IKE_ISAKMP_VERSION)
        {
            if (IkeConstructInfoExchPacket (&pNotifyInfo->IkeNotifyInfo, pIkeSA)
                == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessIPSecNotifyRequest: Construct "
                             "Informational exchange Message Failed\n");
                return IKE_FAILURE;
            }
        }
        else
        {
            pSessionInfo =
                Ike2SessInfoExchSessInit (pIkeSA, IKE_ZERO, IKE_INITIATOR);
            if (pSessionInfo == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessIPSecNotifyRequest: "
                             "Ike2SessInfoExchSessInit failed \n");
                return (IKE_FAILURE);
            }
            IKE_MEMCPY (&(pSessionInfo->PostPhase1Info), pNotifyInfo,
                        sizeof (tIkePhase1PostProcessInfo));

            /* Allocate memory for the notify data if any */
            if (pNotifyInfo->IkeNotifyInfo.u2NotifyDataLen != IKE_ZERO)
            {
                pSessionInfo->PostPhase1Info.IkeNotifyInfo.pu1NotifyData = NULL;

                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pSessionInfo->PostPhase1Info.
                     IkeNotifyInfo.pu1NotifyData) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessIPSecNotifyRequest: unable to allocate "
                                 "buffer\n");
                    return IKE_FAILURE;
                }
                if (pSessionInfo->PostPhase1Info.IkeNotifyInfo.
                    pu1NotifyData == NULL)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeProcessIPSecNotifyRequest: Allocation "
                                 "of memory for notify data failed \n");
                    return IKE_FAILURE;
                }
                IKE_MEMCPY (pSessionInfo->PostPhase1Info.IkeNotifyInfo.
                            pu1NotifyData,
                            pNotifyInfo->IkeNotifyInfo.pu1NotifyData,
                            pNotifyInfo->IkeNotifyInfo.u2NotifyDataLen);
            }

            if ((Ike2CoreProcess (pSessionInfo)) == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessIPSecNotifyRequest:Send Delete SA "
                             "Informational Msg Failed!\n");
                IkeHandleCoreFailure (pSessionInfo);
                return IKE_FAILURE;
            }

            /* Core Process is Success, Send the Packet Out on the 
             * wire */
            IkeHandleCoreSuccess (pSessionInfo);
        }
        return IKE_SUCCESS;
    }
    else                        /* Copy Notify Info and Trigger Phase1 */
    {
        if (u1IkeVersion == IKE_ISAKMP_VERSION)
        {
            pSessionInfo = IkePhase1SessionInit (IKE_INITIATOR,
                                                 pIkeEngine, IKE_ZERO,
                                                 pRemoteAddr);
        }
        if (u1IkeVersion == IKE2_MAJOR_VERSION)
        {
            pSessionInfo = Ike2SessInitExchSessInit (pIkeEngine, IKE_INITIATOR,
                                                     pRemoteAddr);
        }

        if (pSessionInfo == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecNotifyRequest: Unable to create "
                         "Phase1/Init SessionInfo\n");
            return IKE_FAILURE;
        }
        IKE_MEMCPY (&pSessionInfo->PostPhase1Info, pNotifyInfo,
                    sizeof (tIkePhase1PostProcessInfo));
        /* Allocate memory for the notify data if any */
        if (pNotifyInfo->IkeNotifyInfo.u2NotifyDataLen != IKE_ZERO)
        {
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pSessionInfo->PostPhase1Info.
                 IkeNotifyInfo.pu1NotifyData) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessIPSecNotifyRequest: unable to allocate "
                             "buffer\n");
                return IKE_FAILURE;
            }
            if (pSessionInfo->PostPhase1Info.IkeNotifyInfo.pu1NotifyData ==
                NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeProcessIPSecNotifyRequest: Allocation "
                             "of memory for notify data failed \n");
                return IKE_FAILURE;
            }
            IKE_MEMCPY (pSessionInfo->PostPhase1Info.IkeNotifyInfo.
                        pu1NotifyData, pNotifyInfo->IkeNotifyInfo.pu1NotifyData,
                        pNotifyInfo->IkeNotifyInfo.u2NotifyDataLen);
        }

        if (u1IkeVersion == IKE_ISAKMP_VERSION)
        {
            i1RetVal = IkeCoreProcess (pSessionInfo);
        }
        else
        {
            i1RetVal = Ike2CoreProcess (pSessionInfo);
        }
        if (i1RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessIPSecNotifyRequest: IkeCoreProcess/"
                         "Ike2CoreProcess failed for Phase1/Init Session\n");
            IkeHandleCoreFailure (pSessionInfo);
            return IKE_FAILURE;
        }
        /* Core Process is Success, Send the Packet Out on the 
         * wire */
        IkeHandleCoreSuccess (pSessionInfo);
    }
    return IKE_SUCCESS;
}

/***********************************************************************/
/*  Function Name : IkeHandlePktFromIpsec                              */
/*  Description   : This function is used to post the message to IKE   */
/*                :                                                    */
/*  Input(s)      : tIkeQMsg - structure to hold the IKE Message       */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
IkeHandlePktFromIpsec (tIkeQMsg * pMsg)
{
    tIkeQMsg           *pIkeQMsg = NULL;

    if (MemAllocateMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 **) (VOID *) &pIkeQMsg) ==
                                                    MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
            "IkeHandlePktFromIpsec: unable to allocate "
                                                    "buffer\n");
        return;
    }
    IKE_MEMCPY (pIkeQMsg, (tIkeQMsg *) pMsg, sizeof (tIkeQMsg));
    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 *) pMsg);

    if (OsixSendToQ
        (SELF, (const UINT1 *) IKE_QUEUE_NAME, (tOsixMsg *) pIkeQMsg,
         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Send to Queue Failed\n");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQMsg);
        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) IKE_TASK_NAME,
                       IKE_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "Send Event Failed\n");
        /* No need to free the pMsg as it is already queued up */
        return;
    }
}

/***********************************************************************/
/*  Function Name : TriggerIke                                         */
/*  Description   : This function is used to trigger ike negotiation   */
/*                : for Ravpn client                                   */
/*  Input(s)      : Peer ip address - u4Addr                           */
/*                : Ike Version     - u1IkeVersion                     */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/
VOID
TriggerIke (tIkeIpAddr * pIpAddr, UINT1 u1IkeVersion)
{
    tIkeQMsg           *pIkeQMsg = NULL;
    tIkeIpAddr          IkeSrcAddr;

    if (MemAllocateMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 **) (VOID *) &pIkeQMsg)
        == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "TriggerIke: Session memory Allocation Fail\n");;
        return;
    }

    if (pIkeQMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "TriggerIke: Session memory Allocation Fail\n");
        return;
    }

    MEMSET (pIkeQMsg, IKE_ZERO, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_TASK;
    pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
        IKE_HTONL (IKE_IPSEC_NEW_KEY);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.bRekey = FALSE;

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1Protocol =
        IKE_ZERO;
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u2PortNumber =
        IKE_ZERO;

    if (pIpAddr->u4AddrType == IPV4ADDR)
    {
        if (NetIpv4GetSrcAddrToUseForDest (pIpAddr->Ipv4Addr,
                                           &(IkeSrcAddr.Ipv4Addr)) ==
            IP_FAILURE)
        {
            MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQMsg);
            return;
        }

        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
            u4AddrType = IKE_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
            Ipv4Addr = IKE_HTONL (IkeSrcAddr.Ipv4Addr);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.RemoteTEAddr.
            u4AddrType = IKE_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.RemoteTEAddr.
            Ipv4Addr = IKE_HTONL (pIpAddr->Ipv4Addr);
    }
    else
    {

#if defined IP6_WANTED && !defined LNXIP6_WANTED
        if (Ip6SrcAddrForDestAddr (&(pIpAddr->Ipv6Addr),
                                   &(IkeSrcAddr.Ipv6Addr)) == IP6_FAILURE)
        {
            MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQMsg);
            return;
        }
#endif
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
            u4AddrType = IKE_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
        IKE_MEMCPY (&(pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                      SaTriggerInfo.LocalTEAddr.
                      Ipv6Addr), &(IkeSrcAddr.Ipv6Addr), sizeof (tIp6Addr));

        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.RemoteTEAddr.
            u4AddrType = IKE_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
        IKE_MEMCPY (&(pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                      SaTriggerInfo.RemoteTEAddr.
                      Ipv6Addr), &(pIpAddr->Ipv6Addr), sizeof (tIp6Addr));
    }

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1IkeVersion =
        u1IkeVersion;

    IkeHandlePktFromIpsec (pIkeQMsg);
}
