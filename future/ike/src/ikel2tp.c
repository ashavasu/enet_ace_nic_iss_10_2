/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikel2tp.c,v 1.5 2013/11/12 11:17:41 siva Exp $
 *
 * Description: This file has IKE-L2TP interface functions
 *
 ***********************************************************************/

#include "ikegen.h"
#include "cfa.h"
#include "l2tp.h"

INT1                IkeProcessL2tpDeleteSaRequest (tIkeDelSAByAddr *
                                                   pDelSAByAddr);

/***********************************************************************/
/*  Function Name : IkeHandlePktFromL2TP                               */
/*  Description   : This function is used to post the message to IKE   */
/*                :                                                    */
/*  Input(s)      : tIkeQMsg - structure to hold the IKE Message       */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
IkeHandlePktFromL2TP (tIkeQMsg * pMsg)
{
    if (OsixSendToQ
        (SELF, (const UINT1 *) IKE_QUEUE_NAME, (tOsixMsg *) pMsg,
         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "Send to Queue Failed\n");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) IKE_TASK_NAME,
                       IKE_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "Send Event Failed\n");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
        return;
    }
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessL2tpRequest 
 *  Description   : This will be called from ike main if any data
 *                  received from l2tp queue.
 *  Input(s)      : tIkeL2tpIfParam - L2TP Information
 *  Output(s)     : None.
 *  Return Values : None.
 * ---------------------------------------------------------------
 */

VOID
IkeProcessL2tpRequest (tIkeL2TPIfParam * pL2TPIfParam)
{
    tIkeEngine         *pIkeEngine = NULL;

    pL2TPIfParam->u4MsgType = IKE_NTOHL (pL2TPIfParam->u4MsgType);
    switch (pL2TPIfParam->u4MsgType)
    {
        case IKE_L2TP_DELETE_SA:
            if (pL2TPIfParam->u1L2TPSysMode == L2TP_LAC_IF)
            {
                pIkeEngine =
                    IkeGetIkeEngine (&(pL2TPIfParam->DelSAByAddr.LocalAddr));
                if (pIkeEngine != NULL)
                {
                    if (IkeDelSessSAForPeer (pIkeEngine,
                                             &(pL2TPIfParam->DelSAByAddr.
                                               RemoteAddr)) == IKE_FAILURE)
                    {
                        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                     "IkeProcessL2tpRequest: \
                                     IkeDelSessSAForPeer " "Failed\n");
                        return;
                    }
                }
            }
            else
            {
                IkeProcessL2tpDeleteSaRequest (&pL2TPIfParam->DelSAByAddr);
            }
            break;
        default:
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeProcessL2tpRequest: Unsupported message "
                         "event from l2tp\n");
            break;
    }
    return;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : IkeProcessL2tpDeleteSaRequest  
 *  Description   : Function to Process L2tp Delete SA request           
 *  Input(s)      : pDelSAByAddr - Pointer to the address of the
 *                : tunnel to be deleted
 *  Output(s)     : None.
 *  Return Values : IKE_SUCCESS or IKE_FAILURE      
 * ---------------------------------------------------------------
 */
INT1
IkeProcessL2tpDeleteSaRequest (tIkeDelSAByAddr * pDelSAByAddr)
{

    tIkeIPSecIfParam    IkeIPSecIfParam;

    IKE_BZERO (&IkeIPSecIfParam, sizeof (tIkeIPSecIfParam));

    IkeIPSecIfParam.u4MsgType = IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR;
    IKE_MEMCPY (&IkeIPSecIfParam.IPSecReq.DelSAInfo,
                pDelSAByAddr, sizeof (tIkeDelSAByAddr));

    if (IkeSendIPSecIfParam (&IkeIPSecIfParam) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessL2tpDeleteSaRequest:"
                     "Unable to send to Initial "
                     "contact information to IPSec\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}
