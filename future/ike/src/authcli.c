/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authcli.c,v 1.4 2013/08/22 14:54:29 siva Exp $
 *
 * Description: This file has cli function relted to auth module
 *
 ***********************************************************************/
#ifdef CLI_WANTED

#include "lr.h"
#include "cli.h"
#include "authcli.h"

#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "midconst.h"
#include "fswebnm.h"

typedef VOID        (*ActionRoutine) (tUserAuth *, UINT1 **);

/* The array of Function pointers for all the commands.
 * The Index of the array is the command that is passed by CLI module 
 */
ActionRoutine       aAuthCliActionRoutine[] = {
    NULL,
    AuthCliAddUser,                /* AUTH_ADD_USER */
    AuthCliDeleteUser,            /* AUTH_DELETE_USER */
    AuthCliModifyUser,            /* AUTH_MODIFY_USER */
    AuthCliShowStatus,            /* AUTH_SHOW_STATUS */
};

/***************************************************************************  
 * FUNCTION NAME : AuthCliCmdActionRoutine 
 * DESCRIPTION   : This function process the pptp commands 
 * INPUT         : pInMsg    - The input buffer containing the CLI command
 *                             and parameters
 * OUTPUT        : ppRespMsg - Formatted output message
 * RETURNS       : NONE
 ***************************************************************************/

VOID
AuthCliCmdActionRoutine (UINT1 *pInMsg, UINT1 **ppRespMsg)
{
    if (NULL == pInMsg)
    {
        IKE_NO_RESPONSE_MESSAGE (ppRespMsg);
        return;
    }
    AuthProcessCliMsg (pInMsg, ppRespMsg);
}

/***************************************************************************  
 * FUNCTION NAME : AuthProcessCliMsg 
 * DESCRIPTION   : This function process the pptp commands 
 * INPUT         : pInMsg    - The input buffer containing the CLI command
 *                             and parameters
 * OUTPUT        : ppRespMsg - Formatted output message
 * RETURNS       : NONE
 ***************************************************************************/

VOID
AuthProcessCliMsg (UINT1 *pInMsg, UINT1 **ppRespMsg)
{
    UINT4               u4Command;
    tUserAuth          *pUserInfo;

    CLI_GET_COMMAND (u4Command, pInMsg);
    pUserInfo = (tUserAuth *) CLI_GET_DATA_PTR (pInMsg);

    /* Check if the command falls within the valid range */
    if ((u4Command == 0) || (u4Command > AUTH_CLI_MAX_COMMANDS))
    {
        IKE_FORMAT_ERROR_MESSAGE ("\nInvalid Command\n", ppRespMsg);
        CLI_FREE (pInMsg);
        return;
    }

    /* Call the action routine to excecute the appropriate CLI command */
    aAuthCliActionRoutine[u4Command] (pUserInfo, ppRespMsg);
    CLI_FREE (pInMsg);
}

VOID
AuthCliAddUser (tUserAuth * pUserInfo, UINT1 **ppRespMsg)
{
    if (AuthDbAddUser (pUserInfo->au1UserName, pUserInfo->au1Password,
                       pUserInfo->au1Protocol) == USER_AUTH_FAILURE)
    {
        IKE_FORMAT_ERROR_MESSAGE ("Could not add the user\n", ppRespMsg);
        return;
    }
    IKE_NO_RESPONSE_MESSAGE (ppRespMsg);
    return;
}

VOID
AuthCliDeleteUser (tUserAuth * pUserInfo, UINT1 **ppRespMsg)
{
    if (AuthDbDeleteUser (pUserInfo->au1UserName) == USER_AUTH_FAILURE)
    {
        IKE_FORMAT_ERROR_MESSAGE ("Could not delete User\n", ppRespMsg);
        return;
    }
    IKE_NO_RESPONSE_MESSAGE (ppRespMsg);
    return;
}

VOID
AuthCliModifyUser (tUserAuth * pUserInfo, UINT1 **ppRespMsg)
{
    if (AuthDbModifyUser (pUserInfo->au1UserName, pUserInfo->au1Password,
                          pUserInfo->au1Protocol) == USER_AUTH_FAILURE)

    {
        IKE_FORMAT_ERROR_MESSAGE ("Could not modify the user\n", ppRespMsg);
        return;
    }
    IKE_NO_RESPONSE_MESSAGE (ppRespMsg);
    return;
}

VOID
AuthCliShowStatus (tUserAuth * pUserInfo, UINT1 **ppRespMsg)
{
    tUserAuth          *pAuthNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1              *pu1Tmp = NULL;

    UNUSED_PARAM (pUserInfo);

#if 0
    if (gu4WebUserInput != TRUE)
    {
#endif
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &(*ppRespMsg)) == MEM_FAILURE)
        {
            return;
        }
        CLI_SET_RESP_STATUS (*ppRespMsg, TRUE);
        pu1Tmp = CLI_GET_DATA_PTR (*ppRespMsg);
        SPRINTF ((char *) pu1Tmp, "\nUser Authentication Information :\r\n");
        pu1Tmp += STRLEN (pu1Tmp);
        SPRINTF ((char *) pu1Tmp, "-----------------------------------\r\n");
        pu1Tmp += STRLEN (pu1Tmp);
        SPRINTF ((char *) pu1Tmp, "UserName\t\t\t\tProtocol\r\n");
        pu1Tmp += STRLEN (pu1Tmp);
        SPRINTF ((char *) pu1Tmp, "--------\t\t\t\t--------\r\n");
        pu1Tmp += STRLEN (pu1Tmp);

        TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
        {
            pAuthNode = (tUserAuth *) pNode;
            SPRINTF ((char *) pu1Tmp, "%-32s\t%s\r\n",
                     pAuthNode->au1UserName, pAuthNode->au1Protocol);
            pu1Tmp += STRLEN (pu1Tmp);
        }
#if 0
    }
    else
    {
        tHttp              *pgcontent = HttpCommonPtr ();
        MEMSET (pgcontent, 0, sizeof (tHttp));
        if (DslReadPage ("userdb.html", pgcontent) == FAILURE)
        {
            return;
        }
        SearchReplace (pgcontent, "<!TABLE_INSERT>", " ");
        TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
        {
            pAuthNode = (tUserAuth *) pNode;
            pu1Tmp = gpWebCliBuffer;
            SPRINTF (pu1Tmp, "<TR><TD><INPUT TYPE=RADIO NAME=RADIO_SELECT>"
                     "</TD><TD>%s<INPUT TYPE=HIDDEN NAME=USER VALUE=%s>",
                     pAuthNode->au1UserName, pAuthNode->au1UserName);
            SPRINTF (pu1Tmp + STRLEN (pu1Tmp),
                     "<INPUT TYPE=HIDDEN NAME=TABLE_INDEX>"
                     "<INPUT TYPE=HIDDEN NAME=TABLE_END></TD>");
            if (STRCMP (pAuthNode->au1Protocol, "IKE") == 0)
            {
                SPRINTF (pu1Tmp + STRLEN (pu1Tmp), "<TD>IKE</TD></TR>");
            }
            else if (STRCMP (pAuthNode->au1Protocol, "PPP") == 0)
            {
                SPRINTF (pu1Tmp + STRLEN (pu1Tmp), "<TD>PPP</TD></TR>");
            }

            WebnmSockWrite (pgcontent, gpWebCliBuffer, STRLEN (gpWebCliBuffer));
        }
        webnm_write_source (pgcontent);
        DslWebSendRespStatus ();
        IKE_NO_RESPONSE_MESSAGE (ppRespMsg);
    }
#endif
    return;
}

#endif /*CLI_WANTED */
