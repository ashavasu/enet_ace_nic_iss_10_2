/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ikecert.c,v 1.8 2014/04/23 12:21:39 siva Exp $
 * 
 * Description:This file contains Certificate related functions
 * 
 ********************************************************************/

#include "ikegen.h"
#include "iketdfs.h"
#include "ikecert.h"
#include "ikememmac.h"

/************************************************************************/
/*  Function Name   :IkeFreeCertDBEntry                                 */
/*  Description     :This function is used to free the Cert DB entry    */
/*                  :                                                   */
/*  Input(s)        :pNode       : The Node to be freed                 */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/
VOID
IkeFreeCertDBEntry (t_SLL_NODE * pNode)
{
    tIkeCertDB         *pIkeCertDBEntry = NULL;

    pIkeCertDBEntry = (tIkeCertDB *) pNode;
    IkeX509Free (pIkeCertDBEntry->pCert);
    MemReleaseMemBlock (IKE_CERTDB_MEMPOOL_ID, (UINT1 *) pIkeCertDBEntry);
}

/* For MY Certs */
/************************************************************************/
/*  Function Name   :IkeAddCertToMyCerts                                */
/*  Description     :This function is used to add a certificate to the  */
/*                  :MyCerts database                                   */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1CertId   : The CertId of the Cert to be added   */
/*                  :pCert       : The Cert to be imported              */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeAddCertToMyCerts (UINT1 *pu1EngineId, UINT1 *pu1CertId, tIkeX509 * pCert)
{
    tIkeCertDB         *pCertNode = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeAddCertToMyCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pCertNode) == MEM_FAILURE)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAddCertToMyCerts: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pCertNode == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace,
                     MGMT_TRC | CONTROL_PLANE_TRC | OS_RESOURCE_TRC, "IKE",
                     "IkeAddCertToMyCerts: Allocation failed\n");
        return IKE_FAILURE;
    }
    IKE_BZERO ((UINT1 *) pCertNode, sizeof (tIkeCertDB));

    IKE_STRNCPY (pCertNode->au1CertId, pu1CertId, MAX_NAME_LENGTH);
    pCertNode->pCert = pCert;
    SLL_ADD (&(pIkeEngine->MyCerts), (t_SLL_NODE *) pCertNode);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetMyCertById                                   */
/*  Description     :This function is used to get a cert from MyCerts   */
/*                  :database given the CertId, which in this case is   */
/*                  :the KeyId                                          */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pu1KeyId     : The Key Identifier                  */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :NULL on FAILURE                                    */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetMyCertById (UINT1 *pu1EngineId, UINT1 *pu1KeyId)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeGetMyCertById: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->MyCerts, pCertEntry, tIkeCertDB *)
    {
        if (IKE_STRCMP (pCertEntry->au1CertId, pu1KeyId) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pCertEntry;
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetCertFromMyCerts                              */
/*  Description     :This function is used to get a cert from MyCerts   */
/*                  :database given the CAName and Serial Number        */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pCAName      : The CA(Issuer) Name                 */
/*                  :pCASerialNum : The CA Serial Number                */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :NULL on FAILURE                                    */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetCertFromMyCerts (UINT1 *pu1EngineId, tIkeX509Name * pCAName,
                       tIkeASN1Integer * pCASerialNum)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeGetCertFromMyCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }
    if ((pCAName == NULL) || (pCASerialNum == NULL))
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeGetCertFromMyCerts: Input CA Name and SerialNum are NULL\n");
        return NULL;

    }
    SLL_SCAN (&pIkeEngine->MyCerts, pCertEntry, tIkeCertDB *)
    {
        if ((IkeX509NameCmp (IkeGetX509IssuerName (pCertEntry->pCert),
                             pCAName) == IKE_ZERO) &&
            (IkeASN1IntegerCmp (IkeGetX509SerialNum (pCertEntry->pCert),
                                pCASerialNum) == IKE_ZERO))
        {
            GIVE_IKE_SEM ();
            return (pCertEntry);
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeDeleteCertInMyCerts                             */
/*  Description     :This function is used to delete a specified cert   */
/*                  :from MyCerts database                              */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pIkeCertEntry: The Cert DB Entry                   */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDeleteCertInMyCerts (UINT1 *pu1EngineId, tIkeCertDB * pIkeCertEntry)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    if (pIkeCertEntry == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteCertInMyCerts: pIkeCertEntry NULL\n");
        return IKE_FAILURE;
    }

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDeleteCertInMyCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    SLL_DELETE (&(pIkeEngine->MyCerts), (t_SLL_NODE *) pIkeCertEntry);
    GIVE_IKE_SEM ();
    IkeFreeCertDBEntry ((t_SLL_NODE *) pIkeCertEntry);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelAllMyCerts                                   */
/*  Description     :This function is used to delete All the certs      */
/*                  :from MyCerts database                              */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelAllMyCerts (UINT1 *pu1EngineId)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDelAllMyCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }
    SLL_DELETE_LIST (&pIkeEngine->IkeCertMapList, IkeFreeCertMapInfo);
    SLL_DELETE_LIST (&pIkeEngine->MyCerts, IkeFreeCertDBEntry);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/* For PEER Certs */
/************************************************************************/
/*  Function Name   :IkeAddCertToPeerCerts                              */
/*  Description     :This function is used to add a certificate to the  */
/*                  :PeerCerts database                                 */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pu1CertId   : The CertId of the Cert to be added   */
/*                  :pCert       : The Cert to be imported              */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeAddCertToPeerCerts (UINT1 *pu1EngineId, UINT1 *pu1CertId,
                       tIkeX509 * pCert, UINT4 u4Flag)
{
    tIkeCertDB         *pCertNode = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                      "IkeAddCertToPeerCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pCertNode) == MEM_FAILURE)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAddCertToPeerCerts: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pCertNode == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeAddCertToPeerCerts: Allocation failed\n");
        return IKE_FAILURE;
    }
    IKE_BZERO ((UINT1 *) pCertNode, sizeof (tIkeCertDB));

    if ((pu1CertId != NULL) && ((u4Flag & IKE_DYNAMIC_PEER_CERT) == IKE_ZERO))
    {
        IKE_STRNCPY (pCertNode->au1CertId, pu1CertId, MAX_NAME_LENGTH);
    }

    pCertNode->pCert = pCert;
    pCertNode->u4Flag |= u4Flag;
    SLL_ADD (&(pIkeEngine->TrustedPeerCerts), (t_SLL_NODE *) pCertNode);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetPeerCertById                                 */
/*  Description     :This function is used to get a cert from PeerCerts */
/*                  :database given the CertId                          */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pu1CertId    : The Cert Identifier                 */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :NULL on FAILURE                                    */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetPeerCertById (UINT1 *pu1EngineId, UINT1 *pu1CertId)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeGetPeerCertById: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->TrustedPeerCerts, pCertEntry, tIkeCertDB *)
    {
        if (IKE_STRCMP (pCertEntry->au1CertId, pu1CertId) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pCertEntry;
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetCertFromPeerCerts                            */
/*  Description     :This function is used to get a cert from PeerCerts */
/*                  :database given the CAName and Serial Number        */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pCAName      : The CA(Issuer) Name                 */
/*                  :pCASerialNum : The CA Serial Number                */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetCertFromPeerCerts (UINT1 *pu1EngineId, tIkeX509Name * pCAName,
                         tIkeASN1Integer * pCASerialNum)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeGetCertFromPeerCerts: Engine %s does not exist\n");
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->TrustedPeerCerts, pCertEntry, tIkeCertDB *)
    {
        if ((IkeX509NameCmp (IkeGetX509IssuerName (pCertEntry->pCert),
                             pCAName) == IKE_ZERO) &&
            (IkeASN1IntegerCmp (IkeGetX509SerialNum (pCertEntry->pCert),
                                pCASerialNum) == IKE_ZERO))
        {
            GIVE_IKE_SEM ();
            return (pCertEntry);
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetPeerCertByPhase1Id                           */
/*  Description     :This function is used to get a cert from PeerCerts */
/*                  :database given the Phase1 Id                       */
/*                  :                                                   */
/*  Input(s)        :u4EngineIdx  : The Engine Index                    */
/*                  :pCAName      : The CA(Issuer) Name                 */
/*                  :pCASerialNum : The CA Serial Number                */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetPeerCertByPhase1Id (UINT4 u4EngineIdx, tIkePhase1ID * pPhase1Id)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkePhase1ID        CertPhase1Id;
    INT4                i4RetVal = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;

    TAKE_IKE_SEM ();
    pIkeEngine = IkeGetIkeEngineFromIndex (u4EngineIdx);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeGetPeerCertByPhase1Id: Engine does not exist\n");
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->TrustedPeerCerts, pCertEntry, tIkeCertDB *)
    {

        i4RetVal = IkeGetIdFromCert (pCertEntry->pCert, &CertPhase1Id);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeGetPeerCertById: IkeGetIdFromCert failed!\n");
        }
        else
        {
            i4RetVal = IkeComparePhase1IDs (&CertPhase1Id, pPhase1Id);
            if (i4RetVal == IKE_ZERO)
            {
                GIVE_IKE_SEM ();
                return pCertEntry;
            }
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeDeleteCertInPeerCerts                           */
/*  Description     :This function is used to delete a specified cert   */
/*                  :from PeerCerts database                            */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pIkeCertEntry: The Cert DB Entry                   */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDeleteCertInPeerCerts (UINT1 *pu1EngineId, tIkeCertDB * pIkeCertEntry)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    if (pIkeCertEntry == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteCertInPeerCerts: pIkeCertEntry NULL\n");
        return IKE_FAILURE;
    }
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDeleteCertInPeerCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    SLL_DELETE (&(pIkeEngine->TrustedPeerCerts), (t_SLL_NODE *) pIkeCertEntry);
    GIVE_IKE_SEM ();
    IkeFreeCertDBEntry ((t_SLL_NODE *) pIkeCertEntry);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelAllPeerCerts                                 */
/*  Description     :This function is used to delete All the certs      */
/*                  :from PeerCerts database                            */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelAllPeerCerts (UINT1 *pu1EngineId)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDelAllPeerCerts: Engine %s does not exist \n",
                      pu1EngineId);
        return IKE_FAILURE;
    }
    SLL_DELETE_LIST (&pIkeEngine->TrustedPeerCerts, IkeFreeCertDBEntry);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelDynamicPeerCerts                             */
/*  Description     :This function is used to delete the dynamically    */
/*                  :learnt certs from the PeerCerts database           */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelDynamicPeerCerts (UINT1 *pu1EngineId)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;
    tIkeCertDB         *pIkeCertEntry = NULL;
    tIkeCertDB         *pPrev = NULL;

    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDelAllPeerCerts: Engine %s does not exist \n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    SLL_SCAN (&(pIkeEngine->TrustedPeerCerts), pIkeCertEntry, tIkeCertDB *)
    {
        if ((pIkeCertEntry->u4Flag & IKE_DYNAMIC_PEER_CERT) != IKE_ZERO)
        {
            /* Dynamically learnt cert, delete it */
            SLL_DELETE (&(pIkeEngine->TrustedPeerCerts),
                        (t_SLL_NODE *) pIkeCertEntry);
            IkeFreeCertDBEntry ((t_SLL_NODE *) pIkeCertEntry);
            pIkeCertEntry = pPrev;
        }
        pPrev = pIkeCertEntry;
    }
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/* For CA Certs */
/************************************************************************/
/*  Function Name   :IkeAddCertToCACerts                                */
/*  Description     :This function is used to add a certificate to the  */
/*                  :CaCerts database                                   */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pu1CertId   : The CertId of the Cert to be added   */
/*                  :pCert       : The Cert to be imported              */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeAddCertToCACerts (UINT1 *pu1EngineId, UINT1 *pu1CertId, tIkeX509 * pCert)
{
    tIkeCertDB         *pCertNode = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeAddCertToCACerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pCertNode) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeAddCertToCACerts: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    if (pCertNode == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC, "IKE",
                     "IkeAddCertToCACerts: Allocation failed\n");
        return IKE_FAILURE;
    }
    IKE_BZERO ((UINT1 *) pCertNode, sizeof (tIkeCertDB));
    IKE_STRNCPY (pCertNode->au1CertId, pu1CertId, MAX_NAME_LENGTH);
    pCertNode->pCert = pCert;
    SLL_ADD (&(pIkeEngine->CACerts), (t_SLL_NODE *) pCertNode);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetCaCertById                                   */
/*  Description     :This function is used to get a cert from CaCerts   */
/*                  :database given the CertId                          */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pu1CertId    : The Cert Identifier                 */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :NULL on FAILURE                                    */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetCaCertById (UINT1 *pu1EngineId, UINT1 *pu1CertId)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    /* All the certificate related information available in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeGetCaCertById: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->CACerts, pCertEntry, tIkeCertDB *)
    {
        if (IKE_STRCMP (pCertEntry->au1CertId, pu1CertId) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return pCertEntry;
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeGetCertFromCaCerts                              */
/*  Description     :This function is used to get a cert from CaCerts   */
/*                  :database given the CAName and Serial Number        */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pCAName      : The CA(Issuer) Name                 */
/*                  :pCASerialNum : The CA Serial Number                */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to IkeCertDB Entry                         */
/*                  :                                                   */
/************************************************************************/
tIkeCertDB         *
IkeGetCertFromCaCerts (UINT1 *pu1EngineId, tIkeX509Name * pCAName,
                       tIkeASN1Integer * pCASerialNum)
{
    tIkeCertDB         *pCertEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    UNUSED_PARAM (pCASerialNum);

    /* All certificate related information stored only in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeGetCertFromCaCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return NULL;
    }

    SLL_SCAN (&pIkeEngine->CACerts, pCertEntry, tIkeCertDB *)
    {
        if (IkeX509NameCmp (IkeGetX509IssuerName (pCertEntry->pCert),
                            pCAName) == IKE_ZERO)
        {
            GIVE_IKE_SEM ();
            return (pCertEntry);
        }
    }
    GIVE_IKE_SEM ();
    return NULL;
}

/************************************************************************/
/*  Function Name   :IkeDeleteCertInCaCerts                             */
/*  Description     :This function is used to delete a specified cert   */
/*                  :from CaCerts database                              */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :pIkeCertEntry: The Cert DB Entry                   */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDeleteCertInCaCerts (UINT1 *pu1EngineId, tIkeCertDB * pIkeCertEntry)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    if (pIkeCertEntry == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeDeleteCertInCaCerts: pIkeCertEntry NULL\n");
        return IKE_FAILURE;
    }

    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDeleteCertInCaCerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    SLL_DELETE (&(pIkeEngine->CACerts), (t_SLL_NODE *) pIkeCertEntry);
    GIVE_IKE_SEM ();
    IkeFreeCertDBEntry ((t_SLL_NODE *) pIkeCertEntry);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelAllCACerts                                   */
/*  Description     :This function is used to delete All the certs      */
/*                  :from CaCerts database                              */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId  : The Engine Identifier               */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelAllCACerts (UINT1 *pu1EngineId)
{
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    u4NameLen = IKE_STRLEN (pu1EngineId);
    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeDelAllCACerts: Engine %s does not exist\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }
    SLL_DELETE_LIST (&pIkeEngine->CACerts, IkeFreeCertDBEntry);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/* General */
/************************************************************************/
/*  Function Name   :IkeGetIdFromCert                                   */
/*  Description     :This function is used to get the Phase1 Id from    */
/*                  :the SubjectName/SubjectAltName of the cert         */
/*                  :                                                   */
/*  Input(s)        :pCert      : The Certificate                       */
/*                  :                                                   */
/*  Output(s)       :pPhase1Id  : The Phase1 Id                         */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeGetIdFromCert (tIkeX509 * pCert, tIkePhase1ID * pPhase1Id)
{
    X509_NAME          *pX509Name = NULL;
    BIO                *pBio = NULL;
    UINT1              *pu1SubjAltName = NULL;
    UINT4               u4SubjAltNameLen = IKE_ZERO;
    INT4                i4Type;
    UINT1              *pu1SubjName = NULL;
    UINT4               u4NameLen = IKE_ZERO;
    INT1                i1Buf[MAX_NAME_LENGTH];

    i4Type = IkeGetX509SubjectAltName (pCert, &pu1SubjAltName,
                                       &u4SubjAltNameLen);
    if (i4Type == IKE_FAILURE)
    {
        /* No SubjAltName, try to get SubjName */

        pX509Name = X509_get_subject_name (pCert);

        pBio = BIO_new (BIO_s_mem ());
        X509_NAME_print_ex (pBio, pX509Name, IKE_ZERO, IKE_ZERO);
        u4NameLen = (UINT4) BIO_get_mem_data (pBio, &pu1SubjName);

        if (u4NameLen > MAX_NAME_LENGTH)
            u4NameLen = MAX_NAME_LENGTH;

        MEMSET (i1Buf, IKE_ZERO, sizeof (i1Buf));
        IKE_MEMCPY (i1Buf, pu1SubjName, u4NameLen);
        BIO_free (pBio);

        if (u4NameLen > MAX_NAME_LENGTH)
        {
            IKE_RELEASE (pu1SubjName);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeGetIdFromCert: SubjName too long!\n");
            return IKE_FAILURE;
        }
        IKE_MEMCPY (pPhase1Id->uID.au1Dn, i1Buf, u4NameLen);
        pPhase1Id->i4IdType = IKE_IPSEC_ID_DER_ASN1_DN;
        pPhase1Id->u4Length = u4NameLen;
        return IKE_SUCCESS;
    }

    switch (i4Type)
    {
        case IKE_X509_USER_FQDN:
        {
            /* E-mail address */
            pPhase1Id->i4IdType = IKE_IPSEC_ID_USER_FQDN;
            pPhase1Id->u4Length = u4SubjAltNameLen;
            if (u4SubjAltNameLen > MAX_NAME_LENGTH)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pu1SubjAltName);
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeGetIdFromCert: SubjAltName too long!\n");
                return IKE_FAILURE;
            }
            IKE_MEMCPY (pPhase1Id->uID.au1Email, pu1SubjAltName,
                        u4SubjAltNameLen);
            break;
        }

        case IKE_X509_DNS_NAME:
        {
            /* FQDN */
            pPhase1Id->i4IdType = IKE_IPSEC_ID_FQDN;
            pPhase1Id->u4Length = u4SubjAltNameLen;
            if (u4SubjAltNameLen > MAX_NAME_LENGTH)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pu1SubjAltName);
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeGetIdFromCert: SubjAltName too long!\n");
                return IKE_FAILURE;
            }
            IKE_MEMCPY (pPhase1Id->uID.au1Fqdn, pu1SubjAltName,
                        u4SubjAltNameLen);
            break;
        }

        case IKE_X509_IP_ADDR:
        {
            /* IP Address */
            if (u4SubjAltNameLen == IKE_IPV4_LENGTH)
            {
                tIp4Addr            u4TempIp4Addr = IKE_ZERO;

                pPhase1Id->i4IdType = IKE_IPSEC_ID_IPV4_ADDR;
                pPhase1Id->u4Length = u4SubjAltNameLen;

                u4TempIp4Addr = *(UINT4 *) (void *) pu1SubjAltName;
                pPhase1Id->uID.Ip4Addr = IKE_NTOHL (u4TempIp4Addr);
            }
            else if (u4SubjAltNameLen == IKE_IPV6_LENGTH)
            {
                pPhase1Id->i4IdType = IKE_IPSEC_ID_IPV6_ADDR;
                pPhase1Id->u4Length = u4SubjAltNameLen;
                IKE_MEMCPY (pPhase1Id->uID.Ip6Addr.u1_addr, pu1SubjAltName,
                            IKE_IPV6_LENGTH);
            }
            else
            {
                /* Error */
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pu1SubjAltName);
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeGetIdFromCert: IP Address length in"
                             "SubjAltName wrong\n");
                return IKE_FAILURE;
            }
            break;
        }

        default:
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1SubjAltName);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeGetIdFromCert: Unknown SubjectAltName type\n");
            return IKE_FAILURE;
        }
    }
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pu1SubjAltName);
    return IKE_SUCCESS;
}
