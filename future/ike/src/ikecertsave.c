/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ikecertsave.c,v 1.9 2014/04/23 12:21:39 siva Exp $
 * 
 * Description:This file contains Certificate related functions
 * 
 ********************************************************************/

#include "ikegen.h"
#include "iketdfs.h"
#include "ikex509.h"
#include "ikecert.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "ikecertsave.h"
#include "ikememmac.h"
/* Functions to Save Certificate Config to File */
/************************************************************************/
/*  Function Name   :IkeSaveCertConfig                                  */
/*  Description     :This function saves all certificate related        */
/*                   configuration to persistent storage.               */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSaveCertConfig (UINT1 *pu1EngineId)
{
    FILE               *pFp = NULL;

    pFp = FOPEN ((CHR1 *) pu1EngineId, "w");

    if (pFp == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeSaveCertConfig:Failed to open %s file for writing\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (IkeSaveMyCertConfig (pu1EngineId, pFp) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSaveCertConfig:IkeSaveMyCertConfig failed\n");
        fclose (pFp);
        return IKE_FAILURE;
    }

    if (IkeSavePeerCertConfig (pu1EngineId, pFp) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSaveCertConfig:IkeSavePeerCertConfig failed\n");
        fclose (pFp);
        return IKE_FAILURE;
    }

    if (IkeSaveCaCertConfig (pu1EngineId, pFp) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSaveCertConfig:IkeSaveCaCertConfig failed\n");
        fclose (pFp);
        return IKE_FAILURE;
    }

    fclose (pFp);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeMySaveCertConfig                                */
/*  Description     :This function saves my certificate related         */
/*                   configuration to persistent storage.               */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                  :pFp: File in which to write the certificate        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSaveMyCertConfig (UINT1 *pu1EngineId, FILE * pFp)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertRecord     *pCertRecord = NULL;
    tIkeCertDB         *pCertEntry = NULL;
    tIkeRsaKeyInfo     *pIkeRsaKeyInfo = NULL;
    tIkeDsaKeyInfo     *pIkeDsaKeyInfo = NULL;
    UINT4               u4Length = IKE_ZERO;
    INT4                i4RetVal = IKE_ZERO;
    BOOLEAN             bAlgoType = IKE_FALSE;

    TAKE_IKE_SEM ();
    u4Length = IKE_STRLEN (pu1EngineId);
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeSaveMyCertConfig: IkeEngine %s does not exist!\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }
    if (MemAllocateMemBlock
        (IKE_CERTRECORD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(pCertRecord)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSaveMyCertConfig: unable to allocate " "buffer\n");
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    SLL_SCAN (&pIkeEngine->MyCerts, pCertEntry, tIkeCertDB *)
    {
        IKE_BZERO (pCertRecord, sizeof (tIkeCertRecord));

        if (IkeConvertX509ToPEM (pCertEntry->pCert, pCertRecord->au1Cert,
                                 IKE_MAX_PEM_CERT_LENGTH) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSaveMyCertConfig: IkeConvertX509ToPEM failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        pIkeRsaKeyInfo = IkeGetRSAKeyEntryByEngine (pIkeEngine,
                                                    pCertEntry->au1CertId);
        if (pIkeRsaKeyInfo == NULL)
        {
            if ((pIkeDsaKeyInfo =
                 IkeGetDSAKeyEntryByEngine (pIkeEngine,
                                            pCertEntry->au1CertId)) == NULL)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeSaveMyCertConfig: Failed to get RSA/DSA "
                             "Key!\n");
                MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                    (UINT1 *) pCertRecord);
                GIVE_IKE_SEM ();
                return IKE_FAILURE;
            }
            bAlgoType = IKE_TRUE;
        }

        if (bAlgoType == IKE_TRUE)
        {
            i4RetVal = IkeConvertDsaKeyToPEM (pIkeDsaKeyInfo,
                                              pCertRecord->au1Key,
                                              IKE_MAX_PEM_KEY_LENGTH);
        }
        else
        {
            i4RetVal = IkeConvertRsaKeyToPEM (pIkeRsaKeyInfo,
                                              pCertRecord->au1Key,
                                              IKE_MAX_PEM_KEY_LENGTH);
        }
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSaveMyCertConfig: Converting Key to PEM "
                         "failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        pCertRecord->u4Flag = IKE_CERT_RECORD_MYCERT;
        IKE_MEMCPY (pCertRecord->au1CertId, pCertEntry->au1CertId,
                    MAX_NAME_LENGTH);

        if (FWRITE (pCertRecord, sizeof (tIkeCertRecord), IKE_ONE, pFp) !=
            IKE_ONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSaveMyCertConfig: FWRITE failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }
    }
    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID, (UINT1 *) pCertRecord);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSaveCaCertConfig                                */
/*  Description     :This function saves Ca certificate related         */
/*                   configuration to persistent storage.               */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                  :pFp: File in which to write the certificate        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSaveCaCertConfig (UINT1 *pu1EngineId, FILE * pFp)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertRecord     *pCertRecord = NULL;
    tIkeCertDB         *pCertEntry = NULL;
    UINT4               u4Length;

    TAKE_IKE_SEM ();
    u4Length = IKE_STRLEN (pu1EngineId);
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeSaveCaCertConfig: IkeEngine does not exist!\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTRECORD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(pCertRecord)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSaveCaCertConfig: unable to allocate " "buffer\n");
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    SLL_SCAN (&pIkeEngine->CACerts, pCertEntry, tIkeCertDB *)
    {
        IKE_BZERO (pCertRecord, sizeof (tIkeCertRecord));

        if (IkeConvertX509ToPEM (pCertEntry->pCert, pCertRecord->au1Cert,
                                 IKE_MAX_PEM_CERT_LENGTH) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSaveCaCertConfig: IkeConvertX509ToPEM failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        pCertRecord->u4Flag = IKE_CERT_RECORD_CACERT;
        IKE_MEMCPY (pCertRecord->au1CertId, pCertEntry->au1CertId,
                    MAX_NAME_LENGTH);

        if (FWRITE (pCertRecord, sizeof (tIkeCertRecord), IKE_ONE, pFp) !=
            IKE_ONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSaveCaCertConfig: FWRITE failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }
    }
    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID, (UINT1 *) pCertRecord);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeSavePeerCertConfig                              */
/*  Description     :This function saves peer certificate related       */
/*                   configuration to persistent storage.               */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                  :pFp: File in which to write the certificate        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeSavePeerCertConfig (UINT1 *pu1EngineId, FILE * pFp)
{
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertRecord     *pCertRecord = NULL;
    tIkeCertDB         *pCertEntry = NULL;
    UINT4               u4Length;

    TAKE_IKE_SEM ();
    u4Length = IKE_STRLEN (pu1EngineId);
    pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeSaveMyCertConfig: IkeEngine does not exist!\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTRECORD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(pCertRecord)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeSavePeerCertConfig: unable to allocate " "buffer\n");
        GIVE_IKE_SEM ();
        return IKE_FAILURE;
    }

    SLL_SCAN (&pIkeEngine->TrustedPeerCerts, pCertEntry, tIkeCertDB *)
    {
        if ((pCertEntry->u4Flag & IKE_DYNAMIC_PEER_CERT) ==
            IKE_DYNAMIC_PEER_CERT)
        {
            continue;
        }

        IKE_BZERO (pCertRecord, sizeof (tIkeCertRecord));

        if (IkeConvertX509ToPEM (pCertEntry->pCert, pCertRecord->au1Cert,
                                 IKE_MAX_PEM_CERT_LENGTH) == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSavePeerCertConfig: IkeConvertX509ToPEM failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }

        pCertRecord->u4Flag = IKE_CERT_RECORD_PEERCERT;
        IKE_MEMCPY (pCertRecord->au1CertId, pCertEntry->au1CertId,
                    MAX_NAME_LENGTH);

        if (FWRITE (pCertRecord, sizeof (tIkeCertRecord), IKE_ONE, pFp) !=
            IKE_ONE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeSavePeerCertConfig: FWRITE failed!\n");
            MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                (UINT1 *) pCertRecord);
            GIVE_IKE_SEM ();
            return IKE_FAILURE;
        }
    }
    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID, (UINT1 *) pCertRecord);
    GIVE_IKE_SEM ();
    return IKE_SUCCESS;
}

/* Functions to read certificate config from file */
/************************************************************************/
/*  Function Name   :IkeReadCertConfig                                  */
/*  Description     :This function reads all certificate related        */
/*                   configuration from persistent storage.             */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which    */
/*                   this service is requested                          */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeReadCertConfig (UINT1 *pu1EngineId)
{
    FILE               *pFp = NULL;
    tIkeCertRecord     *pCertRecord;

    pFp = FOPEN ((CHR1 *) pu1EngineId, "r");

    if (pFp == NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeSaveCertConfig:Failed to open %s file for reading\n",
                      pu1EngineId);
        return IKE_FAILURE;
    }

    if (MemAllocateMemBlock
        (IKE_CERTRECORD_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(pCertRecord)) == MEM_FAILURE)
    {
        fclose (pFp);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadCertConfig: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }

    while (IKE_TRUE)
    {
        if (fread (pCertRecord, sizeof (tIkeCertRecord), IKE_ONE, pFp) !=
            IKE_ONE)
        {
            break;
        }

        switch (pCertRecord->u4Flag)
        {
            case IKE_CERT_RECORD_MYCERT:
                if (IkeReadMyCertConfig (pu1EngineId, pCertRecord)
                    == IKE_FAILURE)
                {
                    fclose (pFp);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeReadCertConfig:IkeSaveMyCertConfig failed\n");
                    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                        (UINT1 *) pCertRecord);
                    return IKE_FAILURE;
                }
                break;

            case IKE_CERT_RECORD_PEERCERT:
                if (IkeReadPeerCertConfig (pu1EngineId, pCertRecord)
                    == IKE_FAILURE)
                {
                    fclose (pFp);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeReadCertConfig:IkeSavePeerCertConfig failed\n");
                    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                        (UINT1 *) pCertRecord);
                    return IKE_FAILURE;
                }
                break;

            case IKE_CERT_RECORD_CACERT:
                if (IkeReadCaCertConfig (pu1EngineId, pCertRecord)
                    == IKE_FAILURE)
                {
                    fclose (pFp);
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeReadCertConfig:IkeSaveCaCertConfig failed\n");
                    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                        (UINT1 *) pCertRecord);
                    return IKE_FAILURE;
                }
                break;

            default:
                fclose (pFp);
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeReadCertConfig: CertRecord corrupt !!!\n");
                MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID,
                                    (UINT1 *) pCertRecord);
                return IKE_FAILURE;
        }
    }

    if (feof (pFp))
    {
        fclose (pFp);
        MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID, (UINT1 *) pCertRecord);
        return IKE_SUCCESS;
    }

    fclose (pFp);
    MemReleaseMemBlock (IKE_CERTRECORD_MEMPOOL_ID, (UINT1 *) pCertRecord);
    return IKE_FAILURE;
}

/************************************************************************/
/*  Function Name   :IkeReadMyCertConfig                                */
/*  Description     :This function reads my certificate related         */
/*                   configuration from persistent storage.             */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                   pCertRecord: CertRecord from which to read cert    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeReadMyCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord)
{
    BIO                *pInCert = NULL;
    BIO                *pInKey = NULL;
    tIkeX509           *pCert = NULL;
    tIkeRSA            *pIkeRsa = NULL;
    tIkeDSA            *pIkeDsa = NULL;
    INT4                i4RetVal = IKE_ZERO;
    tIkeRsaKeyInfo     *pKeyEntry = NULL;
    tIkeDsaKeyInfo     *pDsaKeyEntry = NULL;
    tIkeX509Name       *pCAName = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;

    pInCert = BIO_new (BIO_s_mem ());

    if (pInCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to BIO_new fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_write (pInCert, pCertRecord->au1Cert,
                          IKE_MAX_PEM_CERT_LENGTH);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to BIO_write failed!\n");
        return IKE_FAILURE;
    }

    pCert = PEM_read_bio_X509 (pInCert, NULL, NULL, NULL);
    if (NULL == pCert)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    if (IkeAddCertToMyCerts (pu1EngineId, pCertRecord->au1CertId, pCert) ==
        IKE_FAILURE)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pInCert);
    pInCert = NULL;

    pInKey = BIO_new (BIO_s_mem ());

    if (pInKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to BIO_new fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_write (pInKey, pCertRecord->au1Key, IKE_MAX_PEM_KEY_LENGTH);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInKey);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadMyCertConfig: Call to BIO_write failed!\n");
        return IKE_FAILURE;
    }

    if ((IKE_MEMCMP (pCertRecord->au1Key, IKE_RSA_ALGO_STRING_IN_KEY_FILE,
                     IKE_STRLEN (IKE_RSA_ALGO_STRING_IN_KEY_FILE))) == IKE_ZERO)
    {
        if ((pIkeRsa = PEM_read_bio_RSAPrivateKey (pInKey,
                                                   NULL, NULL, NULL)) == NULL)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig:Unable to load key from file\n");
            return IKE_FAILURE;
        }
        if (IkeAddRsaKeyEntryToDB (pu1EngineId, pCertRecord->au1CertId,
                                   pIkeRsa) == IKE_FAILURE)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig:IkeAddRsaKeyEntryToDB "
                         "failed !!!\n");
            return IKE_FAILURE;
        }

        /* Retrieve the key using KeyId */
        pKeyEntry = IkeGetRSAKeyEntry (pu1EngineId, pCertRecord->au1CertId);
        if (pKeyEntry == NULL)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig: Call to IkeGetRSAKeyEntry"
                         "failed !!!\n");
            return (IKE_FAILURE);
        }
        /* Add cert-info to KeyTable */
        pCAName = IkeGetX509IssuerName (pCert);
        pCASerialNum = IkeGetX509SerialNum (pCert);
        pKeyEntry->CertInfo.pCAName = IkeX509NameDup (pCAName);
        pKeyEntry->CertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);
    }
    else if ((IKE_MEMCMP (pCertRecord->au1Key, IKE_DSA_ALGO_STRING_IN_KEY_FILE,
                          IKE_STRLEN (IKE_DSA_ALGO_STRING_IN_KEY_FILE)))
             == IKE_ZERO)
    {
        if ((pIkeDsa = PEM_read_bio_DSAPrivateKey (pInKey,
                                                   NULL, NULL, NULL)) == NULL)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig:Unable to load key from file\n");
            return IKE_FAILURE;
        }
        if (IkeAddDsaKeyEntryToDB (pu1EngineId, pCertRecord->au1CertId,
                                   pIkeDsa) == IKE_FAILURE)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig:IkeAddDsaKeyEntryToDB "
                         "failed !!!\n");
            return IKE_FAILURE;
        }

        /* Retrieve the key using KeyId */
        pDsaKeyEntry = IkeGetDSAKeyEntry (pu1EngineId, pCertRecord->au1CertId);
        if (pDsaKeyEntry == NULL)
        {
            BIO_free_all (pInKey);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeReadMyCertConfig: Call to IkeGetDSAKeyEntry"
                         "failed !!!\n");
            return (IKE_FAILURE);
        }
        /* Add cert-info to KeyTable */
        pCAName = IkeGetX509IssuerName (pCert);
        pCASerialNum = IkeGetX509SerialNum (pCert);
        pDsaKeyEntry->CertInfo.pCAName = IkeX509NameDup (pCAName);
        pDsaKeyEntry->CertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);
    }
    else
    {
        BIO_free_all (pInKey);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadMyCertConfig: Unknown Algorithm Type!\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pInKey);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeReadPeerCertConfig                              */
/*  Description     :This function reads peer certificate related       */
/*                   configuration from persistent storage.             */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                   pCertRecord: CertRecord from which to read cert    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeReadPeerCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord)
{
    BIO                *pInCert = NULL;
    tIkeX509           *pCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pInCert = BIO_new (BIO_s_mem ());

    if (pInCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadPeerCertConfig: Call to BIO_new fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_write (pInCert, pCertRecord->au1Cert,
                          IKE_MAX_PEM_CERT_LENGTH);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadPeerCertConfig: Call to BIO_write failed!\n");
        return IKE_FAILURE;
    }

    pCert = PEM_read_bio_X509 (pInCert, NULL, NULL, NULL);
    if (NULL == pCert)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeReadPeerCertConfig:\
                     Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    if (IkeAddCertToPeerCerts (pu1EngineId, pCertRecord->au1CertId, pCert,
                               IKE_PRELOADED_PEER_CERT) == IKE_FAILURE)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeReadPeerCertConfig:\
                     Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pInCert);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeReadCaCertConfig                              */
/*  Description     :This function reads Ca certificate related       */
/*                   configuration from persistent storage.             */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId: index of the engine for which         */
/*                   this service is requested                          */
/*                   pCertRecord: CertRecord from which to read cert    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Returns IKE_SUCCESS or INE_FAILURE                 */
/*                  :                                                   */
/************************************************************************/
INT1
IkeReadCaCertConfig (UINT1 *pu1EngineId, tIkeCertRecord * pCertRecord)
{
    BIO                *pInCert = NULL;
    tIkeX509           *pCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pInCert = BIO_new (BIO_s_mem ());

    if (pInCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadCertConfig: Call to BIO_new fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_write (pInCert, pCertRecord->au1Cert,
                          IKE_MAX_PEM_CERT_LENGTH);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeReadCertConfig: Call to BIO_write failed!\n");
        return IKE_FAILURE;
    }

    pCert = PEM_read_bio_X509 (pInCert, NULL, NULL, NULL);
    if (NULL == pCert)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadCertConfig: Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    if (IkeAddCertToCACerts (pu1EngineId, pCertRecord->au1CertId, pCert) ==
        IKE_FAILURE)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeReadCertConfig: Call to PEM_read_bio_X509 fails\n");
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/* Move to IkeX509.c */
/************************************************************************/
/*  Function Name   :IkeConvertX509ToPEM                                */
/*  Description     :This function converts X509 structure to PEM format*/
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Cert                           */
/*                  :pu4PEMLen: The length of the buffer pu1PEM         */
/*                  :                                                   */
/*  Output(s)       :pu1PEM: The cert in PEM format                     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertX509ToPEM (tIkeX509 * pCert, UINT1 *pu1PEM, UINT4 u4PEMLen)
{
    BIO                *pOutCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pOutCert = BIO_new (BIO_s_mem ());
    if (pOutCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToPEM: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = PEM_write_bio_X509 (pOutCert, pCert);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pOutCert);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConvertX509ToPEM: Call to PEM_write_bio_X509 fail\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pOutCert, pu1PEM, (INT4) u4PEMLen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pOutCert);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToPEM: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pOutCert);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConvertRsaKeyToPEM                              */
/*  Description     :This function converts X509 structure to PEM format*/
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Cert                           */
/*                  :pu4PEMLen: The length of the buffer pu1PEM         */
/*                  :                                                   */
/*  Output(s)       :pu1PEM: The cert in PEM format                     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertRsaKeyToPEM (tIkeRsaKeyInfo * pRsaInfo, UINT1 *pu1PEM, UINT4 u4PEMLen)
{
    tIkeRSA            *pIkeRsa = NULL;
    BIO                *pOutKey = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pIkeRsa = pRsaInfo->RsaKey.pkey.rsa;

    pOutKey = BIO_new (BIO_s_mem ());
    if (pOutKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertRsaKeyToPEM: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    if (PEM_write_bio_RSAPrivateKey (pOutKey, pIkeRsa, NULL, NULL, IKE_ZERO,
                                     NULL, NULL) == IKE_ZERO)
    {
        BIO_free_all (pOutKey);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConvertRsaKeyToPEM: Call to "
                     "PEM_write_bio_RSAPrivateKey fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pOutKey, pu1PEM, (INT4) u4PEMLen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pOutKey);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToPEM: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pOutKey);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeConvertDsaKeyToPEM                              */
/*  Description     :This function converts X509 structure to PEM format*/
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Cert                           */
/*                  :pu4PEMLen: The length of the buffer pu1PEM         */
/*                  :                                                   */
/*  Output(s)       :pu1PEM: The cert in PEM format                     */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertDsaKeyToPEM (tIkeDsaKeyInfo * pDsaInfo, UINT1 *pu1PEM, UINT4 u4PEMLen)
{
    tIkeDSA            *pIkeDsa = NULL;
    BIO                *pOutKey = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pIkeDsa = pDsaInfo->DsaKey.pkey.dsa;

    pOutKey = BIO_new (BIO_s_mem ());
    if (pOutKey == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertDsaKeyToPEM: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    if (PEM_write_bio_DSAPrivateKey (pOutKey, pIkeDsa, NULL, NULL, IKE_ZERO,
                                     NULL, NULL) == IKE_ZERO)
    {
        BIO_free_all (pOutKey);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeConvertDsaKeyToPEM: Call to "
                     "PEM_write_bio_DSAPrivateKey fails\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pOutKey, pu1PEM, (INT4) u4PEMLen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pOutKey);
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToPEM: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pOutKey);
    return IKE_SUCCESS;
}
