/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authdb.c,v 1.7 2014/01/25 13:54:11 siva Exp $
 *
 * Description: This file handles user Data base Managmnt 
 *
 ***********************************************************************/
#include "auth_db.h"
#include "ikememmac.h"
tTMO_SLL            gCommonAuthList;

/****************************************************************************
 Function   :AuthInit 
 Description: This routine inits the common user data base and uploads the 
           the users from file to linked list    .
 Input      : None. 
 Output     : None.
 Returns    : None. 
****************************************************************************/

VOID
AuthInit (VOID)
{
    TMO_SLL_Init (&gCommonAuthList);
    return;
}

/****************************************************************************
 Function   : AuthDbAddUser 
 Description: This routine adds the given user in to local linked list and also
           writes to a static file.
 Input      : UINT1       : User name.
            : UINT1       : User Password.                  
            : UINT1       : Protocol
 Output     : None.
 Returns    : Returns USER_AUTH_FAILURE or  USER_AUTH_SUCCESS. 
****************************************************************************/

INT1
AuthDbAddUser (UINT1 *pu1UserName, UINT1 *pu1Password, UINT1 *pu1Protocol)
{
    UINT4               u4NumOfUser = IKE_ZERO;

    /* check total users in the List */
    if ((u4NumOfUser = (TMO_SLL_Count (&gCommonAuthList))) > MAX_USER)
    {
        /* mmi_printf ("ERROR:Total User Limit Exceded !\r\n"); */
        return USER_AUTH_FAILURE;
    }

    /* add user to list */
    if (auth_add_user_list (pu1UserName, pu1Password, pu1Protocol)
        != USER_AUTH_SUCCESS)
    {
        /* mmi_printf ("ERROR:Failure adding user info to the database!\r\n"); */
        return USER_AUTH_FAILURE;
    }

    /* mmi_printf ("Addition of User Success\n"); */
    return USER_AUTH_SUCCESS;
}

/****************************************************************************
 Function   : AuthDbDeleteUser 
 Description: This routine Deletes the given user from local linked list 
              and also update in to a static file.
 Input      : UINT1       : User name.
 Output     : None.
 Returns    : Returns USER_AUTH_FAILURE or  USER_AUTH_SUCCESS. 
****************************************************************************/

INT1
AuthDbDeleteUser (UINT1 *pu1UserName)
{
    tUserAuth          *pUserNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1Deleted = IKE_ZERO;

    /* delete from Local list */
    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        pUserNode = (tUserAuth *) pNode;
        if (STRCMP (pUserNode->au1UserName, pu1UserName) == IKE_ZERO)
        {
            TMO_SLL_Delete (&gCommonAuthList, pNode);
            MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pUserNode);

            /* mmi_printf ("Deletion of User Success\n"); */
            u1Deleted = IKE_ONE;
            break;
        }
    }
    if (u1Deleted == IKE_ONE)
    {
        return USER_AUTH_SUCCESS;
    }
    else
    {
        /*mmi_printf ("User to be deleted does not exist\n"); */
        return USER_AUTH_FAILURE;
    }
}

/****************************************************************************
 Function   : AuthDbModifyUser 
 Description: This routine modify the given user in to local linked list and also
           writes to a static file.
 Input      : UINT1       : User name.
            : UINT1       : User Password.                  
            : UINT1       : protocol type.                  
 Output     : None.
 Returns    : Returns USER_AUTH_FAILURE or  USER_AUTH_SUCCESS. 
****************************************************************************/

INT1
AuthDbModifyUser (UINT1 *pu1UserName, UINT1 *pu1Password, UINT1 *pu1Protocol)
{
    tUserAuth          *pUserNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               u1Modified = IKE_ZERO;

    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        pUserNode = (tUserAuth *) pNode;
        if ((STRCMP (pUserNode->au1UserName, pu1UserName) == IKE_ZERO) &&
            (STRCMP (pUserNode->au1Password, pu1Password) == IKE_ZERO))
        {
            if (STRLEN (pu1Protocol) > MAX_PROTO_LENGTH)
            {
                return USER_AUTH_FAILURE;
            }
            STRNCPY (pUserNode->au1Protocol, pu1Protocol, STRLEN (pu1Protocol));
            u1Modified = IKE_ONE;
            /* mmi_printf ("Modification of User Success\n"); */
        }
    }
    if (u1Modified == IKE_ONE)
    {
        /* mmi_printf ("Failed to update the user database file\n"); */
        return USER_AUTH_SUCCESS;

    }
    /* mmi_printf ("User/Password combination does not exist.\n"); */
    return USER_AUTH_FAILURE;
}

/****************************************************************************
 Function   : AuthDbValidateUser
 Description: This routine Validates the given user in to local linked list .
 Input      : UINT1       : User name.
            : UINT1       : User Password.                  
            : UINT1       : protocol type.                  
 Output     : None.
 Returns    : Returns USER_AUTH_FAILURE or  USER_AUTH_SUCCESS. 
****************************************************************************/

INT1
AuthDbValidateUser (UINT1 *pu1UserName, UINT1 *pu1Password, UINT1 *pu1Protocol)
{
    tUserAuth          *pUserNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        pUserNode = (tUserAuth *) pNode;
        if (STRCMP (pUserNode->au1Protocol, pu1Protocol) != IKE_ZERO)
        {
            continue;
        }
        if ((STRCMP (pUserNode->au1UserName, pu1UserName) == IKE_ZERO) &&
            (STRCMP (pUserNode->au1Password, pu1Password) == IKE_ZERO))
        {
            return USER_AUTH_SUCCESS;
        }
    }
    return USER_AUTH_FAILURE;
}

INT1
auth_add_user_list (UINT1 *pu1UserName, UINT1 *pu1Password, UINT1 *pu1Protocol)
{
    tUserAuth          *pUserNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        pUserNode = (tUserAuth *) pNode;
        if (STRCMP (pUserNode->au1UserName, pu1UserName) == IKE_ZERO)
        {
            return USER_AUTH_FAILURE;
        }
    }

    if (MemAllocateMemBlock
        (IKE_AUTHDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pUserNode) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "auth_add_user_list: unable to allocate " "buffer\n");
        return USER_AUTH_FAILURE;
    }
    if (pUserNode == NULL)
    {
        return USER_AUTH_FAILURE;
    }

    MEMSET (pUserNode, IKE_ZERO, sizeof (tUserAuth));
    if (pu1UserName != NULL)
    {
        if (STRLEN (pu1UserName) > AUTH_MAX_USER_SIZE)
        {
            MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pUserNode);
            return USER_AUTH_FAILURE;
        }
        STRNCPY (pUserNode->au1UserName, pu1UserName, STRLEN (pu1UserName));
    }
    if (pu1Password != NULL)
    {
        if (STRLEN (pu1Password) > AUTH_MAX_USER_SIZE)
        {
            MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pUserNode);
            return USER_AUTH_FAILURE;
        }
        STRNCPY (pUserNode->au1Password, pu1Password, STRLEN (pu1Password));
    }
    if (pu1Protocol != NULL)
    {
        if (STRLEN (pu1Protocol) > MAX_PROTO_LENGTH)
        {
            MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pUserNode);
            return USER_AUTH_FAILURE;
        }
        STRNCPY (pUserNode->au1Protocol, pu1Protocol, STRLEN (pu1Protocol));
    }

    TMO_SLL_Add (&gCommonAuthList, (tTMO_SLL_NODE *) pUserNode);
    return USER_AUTH_SUCCESS;
}

INT1
auth_user_update_file ()
{
    /* tUserAuth          *pNode = NULL; */
    tTMO_SLL_NODE      *pNode = NULL;
    tUserAuth          *pUserNode = NULL;
    INT4                i4Fd = IKE_INVALID;
    INT4                i4RetVal = IKE_INVALID;

    if ((FileDelete ((UINT1 *) COMMON_USER_FILE_NAME)) == IKE_INVALID)
    {
        /* mmi_printf ("Failure to delete Auth DB File\n"); */
        return USER_AUTH_FAILURE;
    }
    if ((i4Fd = FileOpen ((UINT1 *) COMMON_USER_FILE_NAME,
                          OSIX_FILE_WO | OSIX_FILE_CR | OSIX_FILE_AP)) ==
        IKE_INVALID)
    {
        /* mmi_printf ("Failure to open Auth DB File for writing\n"); */
        return USER_AUTH_FAILURE;
    }

    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        if (pNode != NULL)
        {
            pUserNode = (tUserAuth *) pNode;
            if (auth_write_file (i4Fd, pUserNode->au1UserName,
                                 pUserNode->au1Password,
                                 pUserNode->au1Protocol) != USER_AUTH_SUCCESS)
            {
                FileClose (i4Fd);
                return USER_AUTH_FAILURE;
            }
        }
    }
    if ((i4RetVal = FileClose (i4Fd)) == IKE_INVALID)
    {
        /* mmi_printf ("Failure to close Auth DB File\n"); */
        return USER_AUTH_FAILURE;
    }
    return USER_AUTH_SUCCESS;
}

INT1
auth_write_file (INT4 i4Fd, UINT1 *pu1UserName, UINT1 *pu1Password,
                 UINT1 *pu1Protocol)
{
    INT1               *pi1Buff = NULL;
    INT4                i4Length;
    INT4                i4RetVal = IKE_INVALID;

    if (MemAllocateMemBlock
        (IKE_AUTHDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pi1Buff) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "auth_write_file: unable to allocate " "buffer\n");
        return USER_AUTH_FAILURE;
    }

    if (pi1Buff == NULL)
    {
        /* mmi_printf ("Failed to Allocate Memory\n"); */
        return USER_AUTH_FAILURE;
    }

    i4Length = SPRINTF ((CHR1 *) pi1Buff, "%s  %s  %s\r\n", pu1UserName,
                        pu1Password, pu1Protocol);
    if ((i4RetVal =
         (INT4) FileWrite (i4Fd, (CHR1 *) pi1Buff,
                           (UINT4) i4Length)) != i4Length)
    {
        MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pi1Buff);
        /* mmi_printf ("Failed to write to user database file\n"); */
        return USER_AUTH_FAILURE;
    }
    MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pi1Buff);

    return USER_AUTH_SUCCESS;
}

INT1
AuthReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen)
{
    INT1                i1Char = IKE_INVALID;

    *pi2ReadLen = IKE_ZERO;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, IKE_ONE) == IKE_ONE)
    {
        if (i1Char == '\n')
        {
            pi1Buf[*pi2ReadLen] = '\0';
            return (AUTH_NO_EOF);
        }
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = IKE_ZERO;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (AUTH_EOF);
}

INT1
auth_upload_list (INT1 *pi1Buff)
{
    tUserAuth          *pUserNode = NULL;
    tUserAuth          *pTempNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    if (MemAllocateMemBlock
        (IKE_AUTHDB_MEMPOOL_ID, (UINT1 **) (VOID *) &pTempNode) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "auth_upload_list: unable to allocate " "buffer\n");
        return USER_AUTH_FAILURE;
    }
    if (pTempNode == NULL)
    {
        return USER_AUTH_FAILURE;
    }
    MEMSET (pTempNode, IKE_ZERO, sizeof (tUserAuth));

    SSCANF ((CHR1 *) pi1Buff, "%32s %32s %12s ", pTempNode->au1UserName,
            pTempNode->au1Password, pTempNode->au1Protocol);

    TMO_SLL_Scan (&gCommonAuthList, pNode, tTMO_SLL_NODE *)
    {
        pUserNode = (tUserAuth *) pNode;
        if (STRCMP (pUserNode->au1UserName, pTempNode->au1UserName) == IKE_ZERO)
        {
            /* mmi_printf ("ERROR:User Already Exist! \r\n"); */
            MemReleaseMemBlock (IKE_AUTHDB_MEMPOOL_ID, (UINT1 *) pTempNode);
            return USER_AUTH_SUCCESS;
        }
    }

    TMO_SLL_Add (&gCommonAuthList, (tTMO_SLL_NODE *) pTempNode);
    return USER_AUTH_SUCCESS;
}
