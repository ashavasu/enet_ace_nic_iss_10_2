/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ikex509.c,v 1.17 2014/07/16 12:47:03 siva Exp $
 * 
 * Description:This file contains wrapper functions for Certificate 
 *             related APIs 
 * 
 ********************************************************************/

#include "ikegen.h"
#include "iketdfs.h"
#include "ikex509.h"
#include "ikecert.h"
#include "ikersa.h"
#include "ikedsa.h"
#include "fsike.h"
#include "ikememmac.h"
#include "sntp.h"
#include "fssyslog.h"

/* Functions for MY Certs */
/************************************************************************/
/*  Function Name   :IkeImportMyCertInCLI                               */
/*  Description     :This function is used to import our Certificates   */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify the Certificate with the Key and add   */
/*                  :to the Certificate Database pIkeEngine->MyCerts    */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key Identifier                   */
/*                  :pu1FileName : The file from which cert is imported */
/*                  :u1EncType   : PEM or DER                           */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo    : Error in case of FAILURE             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/

INT1
IkeImportMyCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1KeyId, UINT1 *pu1FileName,
                      UINT1 u1EncType, UINT4 *pu4ErrNo)
{
    signed char         pu1ReadFileName[IKE_MAX_PATH_SIZE] = { IKE_ZERO };
    BIO                *pInFile = NULL;
    tIkeX509           *pCert = NULL;
    tIkePKey           *pPkey = NULL;
    INT4                i4RetVal = IKE_ZERO;
    UINT4               u4Bits = IKE_ZERO;
    tIkeRsaKeyInfo     *pRsaKeyEntry = NULL;
    tIkeDsaKeyInfo     *pDsaKeyEntry = NULL;
    tIkeX509Name       *pCAName = NULL;
    tIkeASN1Integer    *pCASerialNum = NULL;
    tIkeRSA            *pIkeRSA = NULL;

    /* Retrieve the key using KeyId */
    pRsaKeyEntry = IkeGetRSAKeyEntry (pu1EngineId, pu1KeyId);
    if (pRsaKeyEntry != NULL)
    {
        pPkey = &pRsaKeyEntry->RsaKey;
    }
    else
    {
        pDsaKeyEntry = IkeGetDSAKeyEntry (pu1EngineId, pu1KeyId);
        if (pDsaKeyEntry != NULL)
        {
            pPkey = &pDsaKeyEntry->DsaKey;
        }
    }
    if (pPkey == NULL)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportMyCertInCLI: Call to get key entry " "fails\n");
        *pu4ErrNo = IKE_ERR_KEY_NOT_EXIST;
        return (IKE_FAILURE);
    }

    /* Check if the cert already exists */
    if (IkeGetMyCertById (pu1EngineId, pu1KeyId) != NULL)
    {
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportMyCertInCLI: Cert for key %s exists\n",
                      pu1KeyId);
        *pu4ErrNo = IKE_ERR_CERT_EXIST;
        return IKE_FAILURE;
    }

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        pIkeRSA = EVP_PKEY_get1_RSA (pPkey);
        u4Bits = (UINT4) IKE_BYTES2BITS (RSA_size (pIkeRSA));
        if ((u4Bits == RSA_KEY_512) || (u4Bits == RSA_KEY_768)
            || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return IKE_FAILURE;
        }
    }

    /* Read from file into x509 cert */
    IKE_SNPRINTF (((CHR1 *) pu1ReadFileName), IKE_MAX_PATH_SIZE, "%s/%s",
                  IKE_DEF_USER_DIR, pu1FileName);
    pInFile = BIO_new (BIO_s_file ());
    if (NULL == pInFile)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportMyCertInCLI: Call to BIO_new fails\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }
    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     pu1FileName, pu1ReadFileName);
    i4RetVal = BIO_read_filename (pInFile, pu1ReadFileName);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportMyCertInCLI: Call to BIO_read_filename fails"
                      "for %s\n", pu1ReadFileName);
        *pu4ErrNo = IKE_ERR_FILE_NOT_EXIST;
        return IKE_FAILURE;
    }

    /*PEM formated file */
    if (u1EncType == IKE_PEM_ENCODED)
    {
        pCert = PEM_read_bio_X509 (pInFile, NULL, NULL, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportMyCertInCLI:\
                         Call to PEM_read_bio_X509 fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }
    /*DER formated file */
    else
    {
        pCert = d2i_X509_bio (pInFile, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportMyCertInCLI:\
                         Call to PEM_read_bio_X509 fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }

    /* Verify that the private key matches the public key in the cert */
    i4RetVal = X509_check_private_key (pCert, pPkey);
    if (i4RetVal == IKE_ZERO)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportMyCertInCLI: Call to X509_check_private_key"
                     "fails\n");
        *pu4ErrNo = IKE_ERR_CERT_KEY_MISMATCH;
        return (IKE_FAILURE);
    }

    i4RetVal = IkeAddCertToMyCerts (pu1EngineId, pu1KeyId, pCert);
    if (i4RetVal == IKE_FAILURE)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportMyCertInCLI: Call to IkeAddCertToMyCerts "
                     "fails\n");
        return IKE_FAILURE;
    }

    /* Add cert-info to KeyTable */
    pCAName = IkeGetX509IssuerName (pCert);
    pCASerialNum = IkeGetX509SerialNum (pCert);
    if (pRsaKeyEntry != NULL)
    {
        pRsaKeyEntry->CertInfo.pCAName = IkeX509NameDup (pCAName);
        pRsaKeyEntry->CertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);
    }
    else
    {
        pDsaKeyEntry->CertInfo.pCAName = IkeX509NameDup (pCAName);
        pDsaKeyEntry->CertInfo.pCASerialNum = IkeASN1IntegerDup (pCASerialNum);
    }

    BIO_free_all (pInFile);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelMyCert                                       */
/*  Description     :This function is used Delete either all certs or   */
/*                  :the cert specified by pu1KeyId. MyCerts are specd  */
/*                  :using pu1KeyId. ppCAName and ppCASerialNum are to  */
/*                  :be used by IKE task for deleting the SAs created   */
/*                  :using that certificate. If pu1KeyId is NULL, these */
/*                  :will be NULL, as all the certs are being deleted   */
/*                  :and hence all the SAs will have to be deleted.     */
/*                  :ppCAName and ppCASerialNum have to be FREED BY IKE */
/*                  :after usage.                                       */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1KeyId    : The key identifier                   */
/*                  :                                                   */
/*  Output(s)       :ppCAName      : Output the CA Name                 */
/*                  :ppCASerialNum : Output the CA Serial Number        */
/*                  :pu4ErrNo      : The Error Num in case of FAILURE   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelMyCert (UINT1 *pu1EngineId, UINT1 *pu1KeyId, tIkeX509Name ** ppCAName,
              tIkeASN1Integer ** ppCASerialNum, UINT4 *pu4ErrNo)
{
    INT4                i4RetVal = IKE_ZERO;
    tIkeRsaKeyInfo     *pIkeRsaKeyEntry = NULL;
    tIkeEngine         *pIkeEngine = NULL;
    UINT4               u4NameLen = IKE_ZERO;

    if (pu1KeyId == NULL)        /* Delete all certificates */
    {
        i4RetVal = IkeDelAllMyCerts (pu1EngineId);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelMyCert: Call to IkeDelAllMyCerts fails\n");
            *pu4ErrNo = IKE_ERR_GEN_ERR;
            return IKE_FAILURE;
        }

        /* Free the reference to the certs in all the KeyTableEntries */
        u4NameLen = IKE_STRLEN (pu1EngineId);
        TAKE_IKE_SEM ();
        pIkeEngine = IkeSnmpGetEngine (pu1EngineId, (INT4) u4NameLen);
        if (pIkeEngine == NULL)
        {
            IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                          "IkeDelMyCert: Engine %s does not exist\n",
                          pu1EngineId);
            *pu4ErrNo = IKE_ERR_GEN_ERR;
            return IKE_FAILURE;
        }

        SLL_SCAN (&(pIkeEngine->IkeRsaKeyList), pIkeRsaKeyEntry,
                  tIkeRsaKeyInfo *)
        {
            IkeX509NameFree (pIkeRsaKeyEntry->CertInfo.pCAName);
            IkeASN1IntegerFree (pIkeRsaKeyEntry->CertInfo.pCASerialNum);
            pIkeRsaKeyEntry->CertInfo.pCAName = NULL;
            pIkeRsaKeyEntry->CertInfo.pCASerialNum = NULL;
            IKE_BZERO (&pIkeRsaKeyEntry->CertInfo, sizeof (tIkeCertInfo));
        }
        GIVE_IKE_SEM ();
        return IKE_SUCCESS;
    }
    else                        /* Delete cert associated with pu1KeyId */
    {
        tIkeCertDB         *pIkeCertEntry = NULL;
        tIkeX509Name       *pCAName = NULL;
        tIkeASN1Integer    *pCASerialNum = NULL;

        pIkeCertEntry = IkeGetMyCertById (pu1EngineId, pu1KeyId);
        if (pIkeCertEntry == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelMyCert: Call to IkeGetMyCertById fails\n");
            *pu4ErrNo = IKE_ERR_CERT_NOT_EXIST;
            return IKE_FAILURE;
        }

        /* Get the CA Name and Serial Number to pass to IKE ,
         * This will be used by IKE to delete the SAs created using
         * this certificate
         */
        pCAName = IkeGetX509IssuerName (pIkeCertEntry->pCert);
        *ppCAName = IkeX509NameDup (pCAName);

        pCASerialNum = IkeGetX509SerialNum (pIkeCertEntry->pCert);
        *ppCASerialNum = IkeASN1IntegerDup (pCASerialNum);

        i4RetVal = IkeDeleteCertInMyCerts (pu1EngineId, pIkeCertEntry);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeDelMyCert:\
                         Call to IkeDeleteCertInMyCerts fails\n");
            *pu4ErrNo = IKE_ERR_GEN_ERR;
            return IKE_FAILURE;
        }

        /* Free the reference to the certificate in the PKey table */
        pIkeRsaKeyEntry = IkeGetRSAKeyEntry (pu1EngineId, pu1KeyId);
        if (pIkeRsaKeyEntry == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelMyCert: Call to IkeGetRSAKeyEntry fails\n");
            *pu4ErrNo = IKE_ERR_KEY_NOT_EXIST;
            return IKE_FAILURE;
        }
        IkeX509NameFree (pIkeRsaKeyEntry->CertInfo.pCAName);
        IkeASN1IntegerFree (pIkeRsaKeyEntry->CertInfo.pCASerialNum);

        pIkeRsaKeyEntry->CertInfo.pCAName = NULL;
        pIkeRsaKeyEntry->CertInfo.pCASerialNum = NULL;

        IKE_BZERO (&pIkeRsaKeyEntry->CertInfo, sizeof (tIkeCertInfo));
        return IKE_SUCCESS;
    }
}

/* Peer Certs */
/************************************************************************/
/*  Function Name   :IkeVerifyCert                                      */
/*  Description     :This function will either be called from           */
/*                  :1. the CLI when loading peer certificates          */
/*                  :2. IKE certificate payload processing routine,     */
/*                  :   when verifiying a recd. Cert payload            */
/*                  :It is used to verify whether a peer cert is signed */
/*                  :by a CA trusted by us.                             */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pCert       : The Cert in tIkeX509 * format        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeVerifyCert (UINT1 *pu1EngineId, tIkeX509 * pCert)
{
    X509_STORE         *pIkeX509Store = NULL;
    X509_STORE_CTX      IkeStoreCtx;
    INT4                i4RetVal = IKE_ZERO;
    INT4                i4Err = IKE_ZERO;
    tIkeEngine         *pIkeEngine = NULL;
    tIkeCertDB         *pIkeCertEntry = NULL;

    /* All the certificate related information stored in DUMMY engine */
    pu1EngineId = (UINT1 *) IKE_DUMMY_ENGINE;
    pIkeEngine =
        IkeSnmpGetEngine (pu1EngineId, (INT4) IKE_STRLEN (pu1EngineId));
    if (pIkeEngine == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeVerifyCert: Call to IkeSnmpGetEngine fails\n");
        return IKE_FAILURE;
    }

    /* Populate the X509_STORE structure */
    pIkeX509Store = X509_STORE_new ();

    if (pIkeX509Store == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeVerifyCert: malloc to pIkeX509Store fails\n");
        return IKE_FAILURE;
    }

    SLL_SCAN (&pIkeEngine->CACerts, pIkeCertEntry, tIkeCertDB *)
    {
        X509_STORE_add_cert (pIkeX509Store, pIkeCertEntry->pCert);
    }

    if ((i4RetVal =
         (X509_STORE_CTX_init (&IkeStoreCtx, pIkeX509Store, pCert, NULL))) ==
        IKE_ZERO)
    {
        return IKE_FAILURE;
    }

    i4RetVal = X509_verify_cert (&IkeStoreCtx);
    i4Err = IkeStoreCtx.error;
    /* Cleanup */
    X509_STORE_CTX_cleanup (&IkeStoreCtx);
    X509_STORE_free (pIkeX509Store);
    if (i4RetVal <= IKE_ZERO)
    {
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                      "IkeVerifyCert: %.100s \n",
                      X509_verify_cert_error_string (i4Err)));
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                      "IkeVerifyCert: %.100s\n",
                      X509_verify_cert_error_string (i4Err));
        return IKE_FAILURE;
    }
    else
    {
        return IKE_SUCCESS;
    }
}

/* Convert Public key to DER */
/************************************************************************/
/*  Function Name   :IkeConvertPublicKeyToDER                           */
/*  Description     :This function is used to get the Public Key from   */
/*                  :the cert                                           */
/*                  :                                                   */
/*  Input(s)        : pPubkey  : The Public Key                         */
/*                  :                                                   */
/*  Output(s)       : DER format of the Pub Key is returned             */
/*                  :                                                   */
/*  Returns         : Length of the Public key                          */
/*                  :                                                   */
/************************************************************************/
INT4
IkeConvertPublicKeyToDER (tIkePKey * pPubkey, UINT1 **ppu1Public)
{
    INT4                i4Len = IKE_ZERO;
    UINT1              *pTempPubKey = NULL;

    *ppu1Public = NULL;
    i4Len = i2d_PublicKey (pPubkey, NULL);
    if (i4Len == IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertPublicKeyToDER: Unable to extract the length"
                     " of the public key\r\n");
        return IKE_FAILURE;
    }
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1Public)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertPublicKeyToDER: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1Public == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertPublicKeyToDER: Alloc failed "
                     " for the public key\r\n");
        return IKE_FAILURE;
    }
    /* Take a copy of the allocated block as it will be modified
       by the call i2d_PublicKey */
    pTempPubKey = *ppu1Public;
    IKE_BZERO (*ppu1Public, (size_t) (i4Len + IKE_ONE));
    i4Len = i2d_PublicKey (pPubkey, ppu1Public);
    /* Restore back the base pointer */
    *ppu1Public = pTempPubKey;
    return (i4Len);
}

/************************************************************************/
/*  Function Name   :IkeImportPeerCertInCLI                             */
/*  Description     :This function is used to import Peer Certs         */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will verify the Certificate is signed by a trusted */
/*                  :CA if u1Flag is set to UNTRUSTED. Then the file is */
/*                  :added to the Certificate database pIkeEngine->     */
/*                  :TrustedPeerCerts                                   */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1CertId   : The Cert Identifier                  */
/*                  :pu1FileName : The file from which cert is imported */
/*                  :u1Flag      : TRUSTED/UNTRUSTED                    */
/*                  :u1EncType   : PEM or DER                           */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo    : Error in case of FAILURE             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeImportPeerCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1CertId,
                        UINT1 *pu1FileName, UINT1 u1Flag, UINT1 u1EncType,
                        UINT4 *pu4ErrNo)
{
    signed char         pu1ReadFileName[IKE_MAX_PATH_SIZE] = { IKE_ZERO };
    BIO                *pInFile = NULL;
    tIkeX509           *pCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    /* Check if Cert file already exists */
    if (IkeGetPeerCertById (pu1EngineId, pu1CertId) != NULL)
    {
        /* Cert already exists */
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportPeerCertInCLI: Cert %s Already exists\n",
                      pu1CertId);
        *pu4ErrNo = IKE_ERR_CERT_EXIST;
        return IKE_FAILURE;
    }

    /* Read from file into x509 cert */
    IKE_SNPRINTF (((CHR1 *) pu1ReadFileName), IKE_MAX_PATH_SIZE, "%s/%s",
                  IKE_DEF_USER_DIR, pu1FileName);
    pInFile = BIO_new (BIO_s_file ());
    if (NULL == pInFile)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportPeerCertInCLI: Call to BIO_new fails\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     pu1FileName, pu1ReadFileName);
    i4RetVal = BIO_read_filename (pInFile, pu1ReadFileName);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC1 (gu4IkeTrace, MGMT_TRC, "IKE",
                      "IkeImportPeerCertInCLI: Call to BIO_read_filename"
                      "fails for %s\n", pu1ReadFileName);
        *pu4ErrNo = IKE_ERR_FILE_NOT_EXIST;
        return IKE_FAILURE;
    }

    /*PEM formated file */
    if (u1EncType == IKE_PEM_ENCODED)
    {
        pCert = PEM_read_bio_X509 (pInFile, NULL, NULL, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportPeerCertInCLI:\
                         Call to PEM_read_bio_X509 fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }
    /*DER formated file */
    else
    {
        pCert = d2i_X509_bio (pInFile, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportPeerCertInCLI:\
                         Call to d2i_x509_bio fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }

    if (u1Flag != IKE_TRUSTED_CERT)
    {
        /* Verify the peer cert with the CA certs */
        i4RetVal = IkeVerifyCert (pu1EngineId, pCert);
        if (i4RetVal == IKE_FAILURE)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeImportPeerCertInCLI: Call to IkeVerifyCert fails,"
                         "Peer cert not acceptable\n");
            *pu4ErrNo = IKE_ERR_CA_NOT_TRUSTED;
            return IKE_FAILURE;
        }
    }

    i4RetVal = IkeAddCertToPeerCerts (pu1EngineId, pu1CertId, pCert,
                                      IKE_PRELOADED_PEER_CERT);
    if (i4RetVal == IKE_FAILURE)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportPeerCertInCLI: Call to IkeAddCertToPeerCerts"
                     "fails\n");
        return IKE_FAILURE;
    }

    BIO_free_all (pInFile);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelPeerCert                                     */
/*  Description     :This function is used Delete either all Peer certs */
/*                  :or the cert specified by pu1CertId. In case of ALL,*/
/*                  :depending on u1Flag, either ALL the certificates   */
/*                  :or all DYNAMICally learnt certificates will be     */
/*                  :deleted.                                           */
/*                  :pPhase1ID is the output which is to be used by IKE */
/*                  :to delete the SAs created with this peer. pPhase1ID*/
/*                  :should be ALLOCATED before passing to the function.*/
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1CertId   : The Cert Identifier                  */
/*                  :u1Flag      : DYNAMICALLY learnt/ALL               */
/*                  :                                                   */
/*  Output(s)       :pPhase1ID   : The Phase1Id in the Subject<alt>Name */
/*                  :pu4ErrNo      : The Error Num in case of FAILURE   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelPeerCert (UINT1 *pu1EngineId, UINT1 *pu1CertId, UINT1 u1Flag,
                tIkePhase1ID * pPhase1ID, UINT4 *pu4ErrNo)
{
    INT4                i4RetVal = IKE_ZERO;

    if (pu1CertId == NULL)        /* Delete all certificates */
    {
        if (u1Flag == IKE_DEL_ALL)
        {
            /* Delete all the certificates from the TrustedPeerCert DB for
             * ALL case
             */
            i4RetVal = IkeDelAllPeerCerts (pu1EngineId);
            if (i4RetVal == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeDelPeerCert: IkeDelAllPeerCerts fails\n");
                *pu4ErrNo = IKE_ERR_GEN_ERR;
                return IKE_FAILURE;
            }
            return IKE_SUCCESS;
        }
        else if (u1Flag == IKE_DEL_DYNAMIC)
        {
            /* Delete only the DYNAMICally learnt certificates from the 
             * TrustedPeerCert DB for DYNAMIC case
             */
            i4RetVal = IkeDelDynamicPeerCerts (pu1EngineId);
            if (i4RetVal == IKE_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                             "IkeDelPeerCert: IkeDelDynamicPeerCerts fails\n");
                *pu4ErrNo = IKE_ERR_GEN_ERR;
                return IKE_FAILURE;
            }
            return IKE_SUCCESS;
        }
    }
    else                        /* Delete cert associated with pu1KeyId */
    {
        tIkeCertDB         *pIkeCertEntry = NULL;

        pIkeCertEntry = IkeGetPeerCertById (pu1EngineId, pu1CertId);
        if (pIkeCertEntry == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeDelPeerCert: Call "
                         "to IkeGetPeerCertById fails\n");
            *pu4ErrNo = IKE_ERR_CERT_NOT_EXIST;
            return IKE_FAILURE;
        }

        /* The peer Phase1ID is returned so that IKE can delete the SA 
           established with this peer */
        i4RetVal = IkeGetIdFromCert (pIkeCertEntry->pCert, pPhase1ID);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelPeerCert: Call to IkeGetIdFromCert fails\n");
            *pu4ErrNo = IKE_ERR_GEN_ERR;
            return IKE_FAILURE;
        }

        i4RetVal = IkeDeleteCertInPeerCerts (pu1EngineId, pIkeCertEntry);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeDelPeerCert: Call "
                         "to IkeDeleteCertInPeerCerts fails\n");
            *pu4ErrNo = IKE_ERR_CERT_NOT_EXIST;
            return IKE_FAILURE;
        }
    }
    return IKE_SUCCESS;
}

/* Functions for CA Certs */
/************************************************************************/
/*  Function Name   :IkeImportCaCertInCLI                               */
/*  Description     :This function is used to import CA  Certificates   */
/*                  :from the CLI. CLI will call this function, which   */
/*                  :will add the cert to the CA Certs database         */
/*                  :pIkeEngine->CACerts                                */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1CertId   : The Cert Identifier                  */
/*                  :pu1FileName : The file from which cert is imported */
/*                  :u1EncType   : PEM or DER                           */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo    : Error in case of FAILURE             */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeImportCaCertInCLI (UINT1 *pu1EngineId, UINT1 *pu1CertId, UINT1 *pu1FileName,
                      UINT1 u1EncType, UINT4 *pu4ErrNo)
{
    signed char         pu1ReadFileName[IKE_MAX_PATH_SIZE] = { IKE_ZERO };
    BIO                *pInFile = NULL;
    tIkeX509           *pCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    if (IkeGetCaCertById (pu1EngineId, pu1CertId) != NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportCaCertInCLI: Cert already exists\n");
        *pu4ErrNo = IKE_ERR_CERT_EXIST;
        return IKE_FAILURE;
    }

    /* Read from file into x509 cert */
    IKE_SNPRINTF (((CHR1 *) pu1ReadFileName), IKE_MAX_PATH_SIZE, "%s/%s",
                  IKE_DEF_USER_DIR, pu1FileName);
    pInFile = BIO_new (BIO_s_file ());
    if (NULL == pInFile)
    {
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportCaCertInCLI: Call to BIO_new\n");
        *pu4ErrNo = IKE_ERR_GEN_ERR;
        return IKE_FAILURE;
    }

    IkeUtilCallBack (IKE_ACCESS_SECURE_MEMORY, ISS_SRAM_FILENAME_MAPPING,
                     pu1FileName, pu1ReadFileName);
    i4RetVal = BIO_read_filename (pInFile, pu1ReadFileName);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportCaCertInCLI: Call to BIO_read_filename fails\n");
        *pu4ErrNo = IKE_ERR_FILE_NOT_EXIST;
        return IKE_FAILURE;
    }

    /*PEM formated file */
    if (u1EncType == IKE_PEM_ENCODED)
    {
        pCert = PEM_read_bio_X509 (pInFile, NULL, NULL, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeImportCaCertInCLI:\
                         Call to PEM_read_bio_X509 fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }
    /*DER formated file */
    else
    {
        pCert = d2i_X509_bio (pInFile, NULL);
        if (pCert == NULL)
        {
            BIO_free_all (pInFile);
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeImportCaCertInCLI: Call to d2i_x509_bio fails\n");
            *pu4ErrNo = IKE_ERR_INVALID_FORMAT;
            return IKE_FAILURE;
        }
    }

    i4RetVal = IkeAddCertToCACerts (pu1EngineId, pu1CertId, pCert);
    if (i4RetVal == IKE_FAILURE)
    {
        BIO_free_all (pInFile);
        IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                     "IkeImportCaCertInCLI: IkeAddCertToCACerts fails!\n");
        return (IKE_FAILURE);
    }

    BIO_free_all (pInFile);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeDelCaCert                                       */
/*  Description     :This function is used Delete either all certs or   */
/*                  :the cert specified by pu1CertId.                   */
/*                  :                                                   */
/*  Input(s)        :pu1EngineId : The Engine Identifier                */
/*                  :pu1CertId   : The Cert Identifier                  */
/*                  :                                                   */
/*  Output(s)       :pu4ErrNo      : The Error Num in case of FAILURE   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeDelCaCert (UINT1 *pu1EngineId, UINT1 *pu1CertId, UINT4 *pu4ErrNo)
{
    INT4                i4RetVal = IKE_ZERO;

    if (pu1CertId == NULL)        /* Delete all certificates */
    {
        /* Delete all the Certificate from Cert Database */
        i4RetVal = IkeDelAllCACerts (pu1EngineId);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelCaCert: Call to IkeDelAllCACerts fails\n");
            *pu4ErrNo = IKE_ERR_GEN_ERR;
            return IKE_FAILURE;
        }
        return IKE_SUCCESS;
    }
    else                        /* Delete cert associated with CertId */
    {
        tIkeCertDB         *pIkeCertEntry = NULL;

        pIkeCertEntry = IkeGetCaCertById (pu1EngineId, pu1CertId);
        if (pIkeCertEntry == NULL)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE",
                         "IkeDelCaCert: Call to IkeGetCaCertById fails\n");
            *pu4ErrNo = IKE_ERR_CERT_NOT_EXIST;
            return IKE_FAILURE;
        }

        i4RetVal = IkeDeleteCertInCaCerts (pu1EngineId, pIkeCertEntry);
        if (i4RetVal == IKE_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, MGMT_TRC, "IKE", "IkeDelCaCert: Call to "
                         "IkeDeleteCertInCaCerts fails\n");
            *pu4ErrNo = IKE_ERR_CERT_NOT_EXIST;
            return IKE_FAILURE;
        }
        return IKE_SUCCESS;
    }
}

/* Get Cert params */
/************************************************************************/
/*  Function Name   :IkeGetX509PubKey                                   */
/*  Description     :This function is used to get the Public Key from   */
/*                  :the cert                                           */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to the Public key                          */
/*                  :                                                   */
/************************************************************************/
tIkePKey           *
IkeGetX509PubKey (tIkeX509 * pCert)
{
    tIkePKey           *pPKey = NULL;

    pPKey = X509_get_pubkey (pCert);
    return (pPKey);
}

/************************************************************************/
/*  Function Name   :IkeGetX509IssuerName                               */
/*  Description     :This function is used to get the Issuer Name from  */
/*                  :the cert                                           */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to the Issuer Name                         */
/*                  :                                                   */
/************************************************************************/
tIkeX509Name       *
IkeGetX509IssuerName (tIkeX509 * pCert)
{
    tIkeX509Name       *pCAName = NULL;

    pCAName = X509_get_issuer_name (pCert);
    return (pCAName);
}

/************************************************************************/
/*  Function Name   :IkeGetX509SubjectName                              */
/*  Description     :This function is used to get the Subject Name from */
/*                  :the cert. ppu1SubjAltName is the output of the func*/
/*                  :and is allocated by the function. It has to be     */
/*                  :FREED by the CALLER.                               */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :ppu1SubjName : Subject Name in printable format    */
/*                  :pu4Namelen   : Subject Name Length                 */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeGetX509SubjectName (tIkeX509 * pCert, UINT1 **ppu1SubjName,
                       UINT4 *pu4Namelen)
{
    UINT1              *pu1Pos = NULL;
    UINT4               u4BufLen = IKE_ZERO;

    u4BufLen = (UINT4) i2d_X509 (pCert, NULL);
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1SubjName)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGetX509SubjectName: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1SubjName == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509SubjectName: Memory allocation of ppu1SubjName failed!\n");
        return IKE_FAILURE;
    }

    pu1Pos = *ppu1SubjName;
    *pu4Namelen =
        (UINT4) i2d_X509_NAME (X509_get_subject_name (pCert), &pu1Pos);
    UNUSED_PARAM (u4BufLen);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetX509SubjectAltName                           */
/*  Description     :This function is used to get the SubjectAltName    */
/*                  :from the cert. ppu1SubjAltName is allocated by the */
/*                  :function and has to be freed by the CALLER.        */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :ppu1SubjAltName : The SubjectAltName               */
/*                  :pu4Namelen   : The SubjectAltName Length           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT4
IkeGetX509SubjectAltName (tIkeX509 * pCert, UINT1 **ppu1SubjAltName,
                          UINT4 *pu4Namelen)
{
    X509_EXTENSION     *pX509SubjAltName = NULL;
    INT4                i4ExtPos = IKE_ZERO;
    UINT1              *pu1SANData = NULL;
    INT4                i4SANType = IKE_ZERO,
        i4SANLen = IKE_ZERO, i4LastPos = IKE_INVALID;

    i4ExtPos = X509_get_ext_by_NID (pCert, NID_subject_alt_name, i4LastPos);
    if (i4ExtPos == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509SubjectAltName:\
                     Cert does not have SubjAltName\n");
        return IKE_FAILURE;
    }

    pX509SubjAltName = X509_get_ext (pCert, i4ExtPos);

    if (pX509SubjAltName == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509SubjectAltName: Get SubjectAltName for Cert"
                     "failed!\n");
        return IKE_FAILURE;
    }

    pu1SANData = pX509SubjAltName->value->data;
    i4SANType = pu1SANData[IKE_INDEX_2] & IKE_OFFSET_3f;
    i4SANLen = pu1SANData[IKE_INDEX_3];
    pu1SANData += IKE_FOUR;

    if ((i4SANLen + IKE_FOUR) != pX509SubjAltName->value->length)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509SubjectAltName: Get SubjectAltName for Cert"
                     "failed!\n");
        return IKE_FAILURE;
    }
    *pu4Namelen = (UINT4) i4SANLen;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(*ppu1SubjAltName)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeGetX509SubjectAltName: unable to allocate "
                     "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1SubjAltName == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509SubjectAltName: Memory Allocation failed!\n");
        return IKE_FAILURE;
    }
    IKE_MEMCPY (*ppu1SubjAltName, pu1SANData, i4SANLen);
    /* Do we have to free pX509SubjAltName ??? */
    return i4SANType;
}

/************************************************************************/
/*  Function Name   :IkeGetNotBefore                                    */
/*  Description     :This function is used to get the NotBefore validity*/
/*                  :time from the cert                                 */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :pointer to the ASN1_TIME                           */
/*                  :                                                   */
/************************************************************************/
tIkeASN1Time       *
IkeGetNotBefore (tIkeX509 * pCert)
{
    tIkeASN1Time       *pASN1Time = NULL;

    pASN1Time = X509_get_notBefore (pCert);
    return (pASN1Time);
}

/************************************************************************/
/*  Function Name   :IkeGetNotAfter                                     */
/*  Description     :This function is used to get the NotAfter validity */
/*                  :time from the cert                                 */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :pointer to the ASN1_TIME                           */
/*                  :                                                   */
/************************************************************************/
tIkeASN1Time       *
IkeGetNotAfter (tIkeX509 * pCert)
{
    return (X509_get_notAfter (pCert));
}

/************************************************************************/
/*  Function Name   :IkeGetX509SerialNum                                */
/*  Description     :This function is used to get the Serial Number     */
/*                  :from the cert                                      */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Certificate                    */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :pointer to the ASN1_TIME                           */
/*                  :                                                   */
/************************************************************************/
tIkeASN1Integer    *
IkeGetX509SerialNum (tIkeX509 * pCert)
{
    return (X509_get_serialNumber (pCert));
}

/* Conversion functions */
/************************************************************************/
/*  Function Name   :IkeGetX509FromDER                                  */
/*  Description     :This function is called when we get a cert in DER  */
/*                  :format in a paylaod and want to convert to X509    */
/*                  :                                                   */
/*  Input(s)        :pu1DER   : The cert payload in DER format          */
/*                  :u4Len    : The cert payload length                 */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :pointer to the X509 structure                      */
/*                  :                                                   */
/************************************************************************/
tIkeX509           *
IkeGetX509FromDER (UINT1 *pu1DER, UINT4 u4Len)
{
    BIO                *pInCert = NULL;
    tIkeX509           *pX509Cert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pInCert = BIO_new (BIO_s_mem ());
    if (pInCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509FromDER: Call to BIO_new failed!\n");
        return NULL;
    }

    i4RetVal = BIO_write (pInCert, pu1DER, (INT4) u4Len);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509FromDER: Call to BIO_write failed!\n");
        return NULL;
    }

    pX509Cert = d2i_X509_bio (pInCert, NULL);
    if (pX509Cert == NULL)
    {
        BIO_free_all (pInCert);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeGetX509FromDER: Call to d2i_X509_bio!\n");
        return NULL;
    }
    BIO_free_all (pInCert);
    return pX509Cert;
}

/************************************************************************/
/*  Function Name   :IkeConvertX509ToDER                                */
/*  Description     :This function is called to convert a cert in X509  */
/*                  :format to DER format to send in Cert Payload.      */
/*                  :ppu1DER is allocated by the function and has to be */
/*                  :FREED by the caller.                               */
/*                  :                                                   */
/*  Input(s)        :pCert    : The X509 Cert                           */
/*                  :                                                   */
/*  Output(s)       :ppu1DER   : The Cert in DER format                 */
/*                  :pu4DERLen : The Cert Length (DER format)           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertX509ToDER (tIkeX509 * pCert, UINT1 **ppu1DER, UINT4 *pu4DERLen)
{
    BIO                *pOutCert = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pOutCert = BIO_new (BIO_s_mem ());
    if (pOutCert == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToDER: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = i2d_X509_bio (pOutCert, pCert);
    if (i4RetVal == IKE_ZERO)
    {
        BIO_free_all (pOutCert);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToDER: Call to i2d_X509_bio failed!\n");
        return IKE_FAILURE;
    }

    *pu4DERLen = (UINT4) i2d_X509 (pCert, NULL);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &(*ppu1DER)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToDER: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1DER == NULL)
    {
        BIO_free_all (pOutCert);
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToDER: Call to Memory allocation failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pOutCert, *ppu1DER, (INT4) *pu4DERLen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pOutCert);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToDER: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }
    BIO_free_all (pOutCert);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetSignatureAlgo                                */
/*                  :                                                   */
/*  Description     :This function is called to get Signature Algorithm */
/*                  :from the certificate information.                  */
/*                  :                                                   */
/*  Input(s)        :pX509Cert : The X509 Cert                          */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Type of Signature Algorithm                        */
/*                  :                                                   */
/************************************************************************/
UINT4
IkeGetSignatureAlgo (tIkeX509 * pX509Cert)
{
    BIO                *pBIOCertASCII = NULL;
    UINT1               au1SignAlgo[IKE_MAX_SIGN_NAME_SIZE] = { IKE_ZERO };
    UINT4               u4ASCIILen = IKE_MAX_SIGN_NAME_SIZE;
    INT4                i4RetVal = IKE_ZERO;

    IKE_BZERO (au1SignAlgo, IKE_MAX_SIGN_NAME_SIZE);

    pBIOCertASCII = BIO_new (BIO_s_mem ());
    if (pBIOCertASCII == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     ":IkeGetSignatureAlgo: Call to BIO_new failed!\n");
        return IKE_NONE;
    }

    if (i2a_ASN1_OBJECT (pBIOCertASCII,
                         pX509Cert->cert_info->signature->algorithm)
        <= IKE_ZERO)
    {
        BIO_free_all (pBIOCertASCII);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     ":IkeGetSignatureAlgo: Call to i2a_ASN1_OBJECT failed!\n");
        return IKE_NONE;
    }

    i4RetVal = BIO_read (pBIOCertASCII, au1SignAlgo, (INT4) u4ASCIILen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pBIOCertASCII);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     ":IkeGetSignatureAlgo: Call to BIO_read failed!\n");
        return IKE_NONE;
    }
    BIO_free_all (pBIOCertASCII);
    if ((IKE_MEMCMP (au1SignAlgo, IKE_SIGN_ALGO_SHA1,
                     IKE_STRLEN (IKE_SIGN_ALGO_SHA1))) == IKE_ZERO)
    {
        return IKE_SHA1;
    }
    else if ((IKE_MEMCMP (au1SignAlgo, IKE_SIGN_ALGO_SHA224,
                          IKE_STRLEN (IKE_SIGN_ALGO_SHA224))) == IKE_ZERO)
    {
        return IKE_SHA224;
    }
    else if ((IKE_MEMCMP (au1SignAlgo, IKE_SIGN_ALGO_SHA256,
                          IKE_STRLEN (IKE_SIGN_ALGO_SHA256))) == IKE_ZERO)
    {
        return IKE_SHA256;
    }
    else if ((IKE_MEMCMP (au1SignAlgo, IKE_SIGN_ALGO_SHA384,
                          IKE_STRLEN (IKE_SIGN_ALGO_SHA384))) == IKE_ZERO)
    {
        return IKE_SHA384;
    }
    else if ((IKE_MEMCMP (au1SignAlgo, IKE_SIGN_ALGO_SHA512,
                          IKE_STRLEN (IKE_SIGN_ALGO_SHA512))) == IKE_ZERO)
    {
        return IKE_SHA512;
    }
    return IKE_NONE;
}

/* For printing */
/************************************************************************/
/*  Function Name   :IkeConvertX509ToASCII                              */
/*  Description     :This function is called to convert a cert in X509  */
/*                  :format to ASCII format for printing. ppu1ASCII is  */
/*                  :allocated by the function and should be FREED by   */
/*                  :the CALLER.                                        */
/*                  :                                                   */
/*  Input(s)        :pX509Cert : The X509 Cert                          */
/*                  :                                                   */
/*  Output(s)       :ppu1ASCII   : The Cert in printable format         */
/*                  :pu4ASCIILen : The Cert Length                      */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS/IKE_FAILURE                            */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertX509ToASCII (tIkeX509 * pX509Cert, UINT1 **ppu1ASCII,
                       UINT4 *pu4ASCIILen)
{
    BIO                *pBIOCertASCII = NULL;
    INT4                i4RetVal = IKE_ZERO;

    pBIOCertASCII = BIO_new (BIO_s_mem ());
    if (pBIOCertASCII == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToASCII: Call to BIO_new failed!\n");
        return IKE_FAILURE;
    }

    *pu4ASCIILen = IKE_MAX_CERT_SIZE;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &(*ppu1ASCII)) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeConvertX509ToASCII: unable to allocate " "buffer\n");
        return IKE_FAILURE;
    }
    if (*ppu1ASCII == NULL)
    {
        BIO_free_all (pBIOCertASCII);
        IKE_MOD_TRC (gu4IkeTrace, OS_RESOURCE_TRC | CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToASCII: Memory allocation of ppu1ASCII failed!\n");
        return IKE_FAILURE;
    }

    MEMSET (*ppu1ASCII, IKE_ZERO, *pu4ASCIILen);

    i4RetVal = X509_print (pBIOCertASCII, pX509Cert);
    if (i4RetVal == IKE_ZERO)
    {
        BIO_free_all (pBIOCertASCII);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToASCII: Call to X509_print failed!\n");
        return IKE_FAILURE;
    }

    i4RetVal = BIO_read (pBIOCertASCII, *ppu1ASCII, (INT4) *pu4ASCIILen);
    if (i4RetVal <= IKE_ZERO)
    {
        BIO_free_all (pBIOCertASCII);
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertX509ToASCII: Call to BIO_read failed!\n");
        return IKE_FAILURE;
    }
    (*ppu1ASCII)[IKE_MAX_CERT_SIZE - IKE_ONE] = '\0';
    *pu4ASCIILen = IKE_STRLEN (*ppu1ASCII);
    BIO_free_all (pBIOCertASCII);
    return IKE_SUCCESS;
}

/* Compare functions */
/************************************************************************/
/*  Function Name   :IkeX509NameCmp                                     */
/*  Description     :This function is called to compare two X509 Names  */
/*                  :                                                   */
/*  Input(s)        :pName1    : The first name to be compared          */
/*                  :pName2    : The second name to be compared         */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :0 if equal                                         */
/*                  :                                                   */
/************************************************************************/
INT4
IkeX509NameCmp (tIkeX509Name * pName1, tIkeX509Name * pName2)
{
    return (X509_NAME_cmp (pName1, pName2));
}

/************************************************************************/
/*  Function Name   :IkeASN1IntegerCmp                                  */
/*  Description     :This function is called to compare two ASN1        */
/*                  :Integers                                           */
/*                  :                                                   */
/*  Input(s)        :pASN1Int1 : The first Integer to be compared       */
/*                  :pASN1Int2 : The second Integer to be compared      */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :0 if equal                                         */
/*                  :                                                   */
/************************************************************************/
INT4
IkeASN1IntegerCmp (tIkeASN1Integer * pASN1Int1, tIkeASN1Integer * pASN1Int2)
{
    return (M_ASN1_INTEGER_cmp (pASN1Int1, pASN1Int2));
}

/* Duplicate functions */
/************************************************************************/
/*  Function Name   :IkeX509NameDup                                     */
/*  Description     :This function is called to duplicate an X509 Name  */
/*                  :                                                   */
/*  Input(s)        :pX509Name : The X509 Name to be duplicated         */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to the X509Name copy                       */
/*                  :                                                   */
/************************************************************************/
tIkeX509Name       *
IkeX509NameDup (tIkeX509Name * pX509Name)
{
    return (X509_NAME_dup (pX509Name));
}

/************************************************************************/
/*  Function Name   :IkeASN1IntegerDup                                  */
/*  Description     :This function is called to duplicate an ASN1       */
/*                  :ASN1 Integer                                       */
/*                  :                                                   */
/*  Input(s)        :pASN1Integer : The ASN1 Integer to be duplicated   */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :Pointer to the X509Integer copy                    */
/*                  :                                                   */
/************************************************************************/
tIkeASN1Integer    *
IkeASN1IntegerDup (tIkeASN1Integer * pASN1Integer)
{
    return (M_ASN1_INTEGER_dup (pASN1Integer));
}

/* Free functions */
/************************************************************************/
/*  Function Name   :IkeX509Free                                        */
/*  Description     :This function is called to free the Certificate    */
/*                  :structure                                          */
/*                  :                                                   */
/*  Input(s)        :pCert   : The certificate to be freed              */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/
VOID
IkeX509Free (tIkeX509 * pCert)
{
    X509_free (pCert);
}

/************************************************************************/
/*  Function Name   :IkeASN1IntegerFree                                 */
/*  Description     :This function is called to free the ASN1 Integer   */
/*                  :                                                   */
/*  Input(s)        :pASN1Integer : The ASN1 Integer to be freed        */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/
VOID
IkeASN1IntegerFree (tIkeASN1Integer * pASN1Integer)
{
    M_ASN1_INTEGER_free (pASN1Integer);
}

/************************************************************************/
/*  Function Name   :IkeX509NameFree                                    */
/*  Description     :This function is called to free the X509 Name      */
/*                  :                                                   */
/*  Input(s)        :pX509Name : The X509 Name to be freed              */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/
VOID
IkeX509NameFree (tIkeX509Name * pX509Name)
{
    X509_NAME_free (pX509Name);
}

/************************************************************************/
/*  Function Name   :IkeGetCurTime                                      */
/*  Description     :This function returns the current time, GM adjusted*/
/*                  :                                                   */
/*  Input(s)        :None                                               */
/*                  :                                                   */
/*  Output(s)       :pIkeTime : The current time, GM adjusted           */
/*                  :                                                   */
/*  Returns         :None                                               */
/*                  :                                                   */
/************************************************************************/
VOID
IkeGetCurTime (tIkeTime * pIkeTime)
{
#ifdef SNTP_WANTED
    /* If SNTP module is enabled, daylight difference and timezone differences
     * has to be adjusted 
     */
    *pIkeTime = (tIkeTime) SntpGetTimeSinceEpoch ();
#else
    time (pIkeTime);
#endif
    return;
}

/************************************************************************/
/*  Function Name   :IkeX509TimeCmp                                     */
/*  Description     :This function compares the two times, one in ASN1  */
/*                  :format, the other in tIkeTime format               */
/*                  :                                                   */
/*  Input(s)        :pASN1Time : The time in ASN1 format                */
/*                  :pIkeTime  : The time in tIkeTime format            */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :0   if equal,                                      */
/*                  :> 0 if pASN1Time > pIkeTime                        */
/*                  :< 0 if pASN1Time < pIkeTime                        */
/*                  :                                                   */
/************************************************************************/
INT4
IkeX509TimeCmp (tIkeASN1Time * pASN1Time, tIkeTime * pIkeTime)
{
    INT4                i4RetVal = IKE_ZERO;

    i4RetVal = X509_cmp_time (pASN1Time, pIkeTime);
    return (i4RetVal);
}

/************************************************************************/
/*  Function Name   :IkeValidatePeerCert                                */
/*  Description     :This function is used to verify the certificate    */
/*                  :parameters                                         */
/*                  :                                                   */
/*  Input(s)        :pSesInfo :Pointer to the Session Info DataBase     */
/*                  :pX509Cert:Pointer to peer X509 certificate         */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeValidatePeerCert (tIkeSessionInfo * pSesInfo, tIkeX509 * pX509Cert)
{
    tIkePhase1ID       *pOtherID = NULL;
    tIkeTime            CurrTime;
    tIkeTime            IkeSATime;
    tIkeTime            NATime;
    tIkeASN1Time       *pNBTime = NULL;
    tIkeASN1Time       *pNATime = NULL;
    tIkePhase1ID        Phase1Id;
    CHR1               *pu1PeerAddrStr = NULL;

    IKE_BZERO (&Phase1Id, sizeof (tIkePhase1ID));

    pOtherID = &pSesInfo->pIkeSA->ResponderID;

    if (pSesInfo->RemoteTunnelTermAddr.u4AddrType == IPV6ADDR)
    {
        pu1PeerAddrStr = (CHR1 *) Ip6PrintNtop
            (&pSesInfo->RemoteTunnelTermAddr.Ipv6Addr);
    }
    else
    {
        UtilConvertIpAddrToStr (&pu1PeerAddrStr,
                                pSesInfo->RemoteTunnelTermAddr.Ipv4Addr);
    }

    if (IkeGetIdFromCert (pX509Cert, &Phase1Id) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeValidatePeerCert: Unable to get ID from peer cert\n");
        return (IKE_FAILURE);
    }

    /* Match the cert ID with the received id payload */
    if (IkeComparePhase1IDs (pOtherID, &Phase1Id) != IKE_ZERO)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE", "IkeValidatePeerCert:\
                     Received ID doesn't match with cert ID\n");
        return (IKE_FAILURE);
    }

    /* Validate the certificate validity period */
    IKE_BZERO (&CurrTime, sizeof (tIkeTime));

    IkeGetCurTime (&CurrTime);

    pNBTime = IkeGetNotBefore (pX509Cert);
    pNATime = IkeGetNotAfter (pX509Cert);

    if ((IkeX509TimeCmp (pNBTime, &CurrTime) > IKE_ZERO) ||
        (IkeX509TimeCmp (pNATime, &CurrTime) < IKE_ZERO))
    {

        IKE_MOD_TRC1 (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                      "IkeValidatePeerCert: Certificate time verification"
                      " failed for peer: %s\n", pu1PeerAddrStr);
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4IkeSysLogId,
                      "IkeValidatePeerCert: Certificate time verification failed for peer: %s\n",
                      pu1PeerAddrStr));

        return (IKE_FAILURE);
    }

    /* Check if the lifetime for Ike SA is greater than the cert 
     * validity period */
    if (pSesInfo->pIkeSA->u4LifeTime != IKE_ZERO)
    {
        IkeSATime = CurrTime + (tIkeTime) pSesInfo->pIkeSA->u4LifeTime;
        if (IkeX509TimeCmp (pNATime, &IkeSATime) < IKE_ZERO)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeValidatePeerCert: Certificate validity time less"
                         "than IKE Phase1 Lifetime! Reducing Phase1 Lifetime"
                         "to match cert validity period\n");
            IkeGetTimeFromASN1Time (pNATime, &NATime);
            /* Reduce the lifetime value to match the cert validity period */
            pSesInfo->pIkeSA->u4LifeTime = (UINT4) (NATime - CurrTime);
            if (pSesInfo->pIkeSA->u4LifeTime < FSIKE_MIN_LIFETIME_SECS)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeValidatePeerCert:\
                             Certificate validity time less" "than MIN_LIFETIME value\n");
                return IKE_FAILURE;
            }
        }
    }

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConvertDERToX509Name                            */
/*  Description     :This function is used to convert an X509 Name in   */
/*                  :DER format to X509_NAME format                     */
/*                  :                                                   */
/*  Input(s)        :pu1DERName : The Name in DER format                */
/*                  :u2Length   : The length of the DER input           */
/*                  :                                                   */
/*  Output(s)       :ppX509Name : The Name in X509_NAME format          */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeConvertDERToX509Name (UINT1 *pu1DERName, UINT4 u4Length,
                         tIkeX509Name ** ppX509Name)
{
    *ppX509Name =
        d2i_X509_NAME (ppX509Name, (VOID *) &pu1DERName, (long int) u4Length);
    if (*ppX509Name == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertDERToX509Name: d2i_X509_NAME failed!\n");
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeGetTimeFromASN1Time                             */
/*  Description     :This function is used to convert ASN1_TIME to      */
/*                  :tIkeTime format.                                   */
/*                  :                                                   */
/*  Input(s)        :pASN1Time  : The time in ASN1_TIME format          */
/*                  :                                                   */
/*  Output(s)       :pIkeTime   : The time in tIkeTime format           */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/
INT1
IkeGetTimeFromASN1Time (tIkeASN1Time * pASN1Time, tIkeTime * pIkeTime)
{
    struct tm           ts;
    signed char        *pu1Data = NULL;

    pu1Data = (signed char *) pASN1Time->data;
    if (pu1Data == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertASN1Time: data field of ASN1_TIME NULL!!\n");
        return IKE_FAILURE;
    }

    if (pASN1Time->type == V_ASN1_UTCTIME)
    {
        IKE_SSCANF (((CHR1 *) pu1Data), "%02d%02d%02d%02d%02d%02dZ",
                    &ts.tm_year, &ts.tm_mon, &ts.tm_mday, &ts.tm_hour,
                    &ts.tm_min, &ts.tm_sec);
        /* OpenSSL uses UTC time only for 1950 - 2050 */
        if (ts.tm_year <= IKE_YEAR_50)
        {
            ts.tm_year += IKE_YEAR_100;
        }
        ts.tm_mon -= IKE_ONE;
    }
    else if (pASN1Time->type == V_ASN1_GENERALIZEDTIME)
    {
        IKE_SSCANF (((CHR1 *) pu1Data), "%04d%02d%02d%02d%02d%02dZ",
                    &ts.tm_year, &ts.tm_mon, &ts.tm_mday, &ts.tm_hour,
                    &ts.tm_min, &ts.tm_sec);
        /* GENERALIZED TIME is YYYY format */
        ts.tm_year -= IKE_YEAR_1900;
        ts.tm_mon -= IKE_ONE;
    }
    else
    {
        /* Invalid time format */
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertASN1Time: Invalid ASN1_TIME type!!\n");
        return IKE_FAILURE;
    }

    /* Convert to time_t */
    *pIkeTime = mktime (&ts);
    return IKE_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IkeCompareDNs                                      */
/*  Description     :This function is called to compare two DER encoded */
/*                  :DNs                                                */
/*                  :                                                   */
/*  Input(s)        :pu1DN1    : The first name to be compared          */
/*                  :u4Len1    : The first name length                  */
/*                  :pu1DN2    : The second name to be compared         */
/*                  :u4Len2    : The second name length                 */
/*                  :                                                   */
/*  Output(s)       :None                                               */
/*                  :                                                   */
/*  Returns         :0 if equal                                         */
/*                  :                                                   */
/************************************************************************/
INT4
IkeCompareDEREncDNs (UINT1 *pu1DN1, UINT4 u4Len1, UINT1 *pu1DN2, UINT4 u4Len2)
{
    tIkeX509Name       *pX509Name1 = NULL;
    tIkeX509Name       *pX509Name2 = NULL;
    INT4                i4RetVal = IKE_ZERO;

    if (IkeConvertDERToX509Name (pu1DN1, u4Len1, &pX509Name1) == IKE_FAILURE)
    {
        return (IKE_FAILURE);
    }

    if (IkeConvertDERToX509Name (pu1DN2, u4Len2, &pX509Name2) == IKE_FAILURE)
    {
        return (IKE_FAILURE);
    }

    i4RetVal = IkeX509NameCmp (pX509Name1, pX509Name2);
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   :IkeConvertDERToStr                                 */
/*  Description     :This function is called to DER dncode the ASN.1 DN */
/*                  :                                                   */
/*  Input(s)        :pi1DERBuf  : DER to be converted to ASN.1 String   */
/*                  :                                                   */
/*  Output(s)       :i4Len      : len of string.                        */
/*                  :pi1Buf     : Output ASN.1 String                   */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT4
IkeConvertDERToStr (UINT1 *pi1DERBuf, INT4 i4Len, INT1 *pi1Buf)
{
    X509_NAME          *pX509Name = NULL;
    BIO                *pBio = NULL;

    if (d2i_X509_NAME (&pX509Name, (void *) &pi1DERBuf, i4Len) == NULL)
    {
        return (IKE_FAILURE);
    }

    pBio = BIO_new (BIO_s_mem ());
    X509_NAME_print_ex (pBio, pX509Name, 0, 0);
    i4Len = BIO_get_mem_data (pBio, &pi1DERBuf);
    if (i4Len > MAX_NAME_LENGTH)
    {
        i4Len = MAX_NAME_LENGTH;
    }

    if (i4Len > IKE_ZERO)
    {
        IKE_MEMCPY (pi1Buf, pi1DERBuf, i4Len);
        pi1Buf[i4Len] = '\0';
    }

    BIO_free (pBio);
    X509_NAME_free (pX509Name);

    return (IKE_SUCCESS);
}

/************************************************************************/
/*  Function Name   :IkeConvertStrToDER                                 */
/*  Description     :This function is called to DER encode the ASN.1 DN */
/*                  :                                                   */
/*  Input(s)        :pi1Str     : ASN.1 String to be converted to DER   */
/*                  :                                                   */
/*  Output(s)       :pi1DERBuf : Output Binary Encoded DER.            */
/*                  :pi4BufLen  : Output Binary Encoded DER's len.      */
/*                  :                                                   */
/*  Returns         :IKE_SUCCESS or IKE_FAILURE                         */
/*                  :                                                   */
/************************************************************************/

INT4
IkeConvertStrToDER (INT1 *pi1Str, INT1 *pi1DERBuf, INT4 *pi4BufLen)
{
    X509_NAME          *pX509Name;
    UINT1               i1TmpStr[IKE_MAX_STRING_LEN + 1];
    UINT1              *pi1DERField, *pi1DERVal;
    INT1               *pi1TmpBuf;
    UINT4               u4Count, u4TmpCount;

    if (IKE_STRLEN (pi1Str) > IKE_MAX_STRING_LEN)
    {
        IKE_MOD_TRC (gu4IkeTrace, CONTROL_PLANE_TRC, "IKE",
                     "IkeConvertStrToDER : failed!\n");

        return (IKE_FAILURE);
    }

    IKE_BZERO (i1TmpStr, IKE_MAX_STRING_LEN + 1);

    IKE_MEMCPY (i1TmpStr, pi1Str, IKE_STRLEN (pi1Str));

    pX509Name = X509_NAME_new ();

    pi1DERField = &i1TmpStr[0];
    pi1DERVal = NULL;
    for (u4Count = 0; u4Count < IKE_STRLEN (pi1Str); u4Count++)
    {
        if ((pi1DERVal == NULL) && (i1TmpStr[u4Count] == '='))
        {
            i1TmpStr[u4Count] = '\0';
            pi1DERVal = &i1TmpStr[u4Count + 1];
            continue;
        }
        else if (i1TmpStr[u4Count] == ',' || i1TmpStr[u4Count] == '/')
        {
            i1TmpStr[u4Count] = '\0';
            if (X509_NAME_add_entry_by_txt (pX509Name, (CHR1 *) pi1DERField,
                                            MBSTRING_ASC, pi1DERVal, -1, -1,
                                            0) == 0)
            {
                if (pX509Name)
                {
                    X509_NAME_free (pX509Name);
                }
                return (IKE_FAILURE);
            }
            for (u4TmpCount = u4Count + 1; u4TmpCount < IKE_STRLEN (pi1Str);
                 u4TmpCount++)
            {
                if (i1TmpStr[u4TmpCount] != ' ')
                {
                    break;
                }
            }
            pi1DERField = &i1TmpStr[u4TmpCount];
            pi1DERVal = NULL;
            continue;
        }
    }
    i1TmpStr[IKE_STRLEN (pi1Str)] = '\0';

    if (X509_NAME_add_entry_by_txt (pX509Name, (CHR1 *) pi1DERField,
                                    MBSTRING_ASC, pi1DERVal, -1, -1, 0) == 0)
    {
        if (pX509Name)
        {
            X509_NAME_free (pX509Name);
        }
        return (IKE_FAILURE);
    }

    u4Count = (UINT4) i2d_X509_NAME (pX509Name, NULL);
    if (u4Count == 0)
    {
        if (pX509Name)
        {
            X509_NAME_free (pX509Name);
        }
        return (IKE_FAILURE);
    }

    pi1TmpBuf = pi1DERBuf;
    *pi4BufLen = (INT4) u4Count;
    u4Count = (UINT4) i2d_X509_NAME (pX509Name, (void *) &pi1TmpBuf);
    if (u4Count == 0)
    {
        if (pX509Name)
        {
            X509_NAME_free (pX509Name);
        }
        return (IKE_FAILURE);
    }

    return (IKE_SUCCESS);
}
