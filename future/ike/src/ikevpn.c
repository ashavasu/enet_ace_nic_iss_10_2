/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikevpn.c,v 1.21 2017/06/19 12:25:57 siva Exp $
 *
 * Description: This has functions for function related to VPN module
 *
 ***********************************************************************/
#ifndef __IKEVPN_C__
#define __IKEVPN_C__

#include "ikegen.h"
#include "fsike.h"
#include "vpn.h"
#include "cfa.h"
#include "fssocket.h"
#include "fsikelow.h"

#include "ikecli.h"
#ifdef VPN_WANTED
#include "vpnglob.h"
#endif
#include "ikememmac.h"
INT4                VpnIkeCertMapPeer (UINT1 *, UINT1 *, UINT1 *);
extern INT4         VpnIkeCheckAndDeleteEngine (tVpnPolicy * pVpnPolicy);
extern INT1         nmhGetFsVpnRaServer (INT4 *pi4RetValFsVpnRaServer);

/**************************************************************************/
INT1
VpnUtilFillIkeParams (tVpnPolicy * pVpnPolicy)
{

    INT4                i4PeerIdType = IKE_ZERO;
    tVpnIdInfo         *pVpnIdInfo = NULL;
    UINT1              *pu1IkeEngineName = NULL;
    UINT1               au1PeerIdValue[VPN_MAX_NAME_LENGTH + IKE_ONE]
        = { IKE_ZERO };
    tSNMP_OCTET_STRING_TYPE PolicyNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerIdValOctStr;
    UINT1               au1IdType[IKE_MAX_TYPE] = { IKE_ZERO };

    MEMSET (au1IdType, IKE_ZERO, sizeof (au1IdType));

    if (VpnIkeCheckAndCreateEngine (pVpnPolicy) == IKE_FAILURE)
    {
        VpnUtilDelIkeParams (pVpnPolicy);
        return SNMP_FAILURE;
    }

    pu1IkeEngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);
    if (NULL == pu1IkeEngineName)
    {
        /* Engine Not Found */
        return SNMP_FAILURE;
    }

    i4PeerIdType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    PeerIdValOctStr.pu1_OctetList = au1PeerIdValue;
    PolicyNameOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    PolicyNameOctetStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    nmhGetFsVpnIkePhase1PeerIdValue (&PolicyNameOctetStr, &PeerIdValOctStr);

    pVpnIdInfo =
        VpnGetRemoteIdInfo (i4PeerIdType, PeerIdValOctStr.pu1_OctetList,
                            PeerIdValOctStr.i4_Length);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnIdInfo->u4AuthType == VPN_IKE_PRESHAREDKEY)
    {
        if (IKE_FAILURE ==
            VpnIkeCreateAndUpdateKeyDB (pVpnIdInfo, pu1IkeEngineName))
        {
            return SNMP_FAILURE;
        }
    }
    else if (pVpnIdInfo->u4AuthType == VPN_IKE_CERTIFICATE)
    {
        /* If Authtype is Certificate then map the inputed ID with the 
         * appropriate RSA Key so that later while verifying identity
         * The Appropriate certificate can be picked from CERTDB */
        if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_IPV4)
        {
            STRCPY (au1IdType, "ipv4");
        }
        else if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_EMAIL)
        {
            STRCPY (au1IdType, "email");
        }
        else if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_FQDN)
        {
            STRCPY (au1IdType, "fqdn");
        }
        else if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_IPV6)
        {
            STRCPY (au1IdType, "ipv6");
        }
        else if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_DN)
        {
            STRCPY (au1IdType, "dn");
        }
        else if (pVpnIdInfo->i2IdType == VPN_ID_TYPE_KEYID)
        {
            STRCPY (au1IdType, "keyid");
        }
        if (VpnIkeCertMapPeer ((UINT1 *) pVpnIdInfo->ac1IdKey, au1IdType,
                               (UINT1 *) pVpnIdInfo->ac1IdValue) ==
            OSIX_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    /* 1. Create/Update and enable a VPn Ike Policy 
     * 2. Set the Encryption Algorithm for the Policy
     * 3. Create/Update and enable the Crypto Map for the Policy */
    if (VpnIkeCreateAndUpdatePolicyDB (pVpnPolicy) == IKE_FAILURE)
    {
        VpnUtilDelIkeParams (pVpnPolicy);
        return SNMP_FAILURE;
    }

    if (VpnIkeCreateAndUpdtTrnsfmSetDB (pVpnPolicy) == IKE_FAILURE)
    {
        VpnUtilDelIkeParams (pVpnPolicy);
        return SNMP_FAILURE;
    }
    if ((pVpnPolicy->u4VpnPolicyType != VPN_XAUTH) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_PRESHAREDKEY) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_CERT) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_XAUTH_CERT))
    {
        if (VpnIkeCreateAndUpdateCryptoMapDB (pVpnPolicy) == IKE_FAILURE)
        {
            VpnUtilDelIkeParams (pVpnPolicy);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (VpnIkeCreateAndUpdateRaInfoDB (pVpnPolicy) == IKE_FAILURE)
        {
            VpnUtilDelIkeParams (pVpnPolicy);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/**************************************************************************/
VOID
VpnUtilDelIkeParams (tVpnPolicy * pVpnPolicy)
{
    /* Delete all the IKE related configuration in fs-ike 
     * 1. Delete IKE Policy
     * 2. Delete the Transform Set (Encryption Algorithm for the Policy)
     * 3. Delete the Crypto Map for the Policy
     */
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PolicyNameOctStr;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1               au1CryptoMapName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT4               u4IkePolicyPriority = IKE_ZERO;
    UINT1              *pu1EngineName = NULL;
    INT1                i1RetVal = IKE_ZERO;

    /* Fill the required IKE related parameters from the VPN Policy */
    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));
    MEMSET (au1CryptoMapName, IKE_ZERO, MAX_NAME_LENGTH);

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);

    if (NULL == pu1EngineName)
    {
        return;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    /* Delete the POLICY */
    /* Delete the CryptoMap DB */
    /* Delete the TransformSet - PHASE II Enctyption Algo etc */
    PolicyNameOctStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    PolicyNameOctStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    if (IkeUtilGetPolicyPriotityFromName (&EngineNameOctetStr,
                                          &PolicyNameOctStr,
                                          &u4IkePolicyPriority) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "VpnUtilDelIkeParams: Failed to"
                     "get the Priority from PolicyName\r\n");
        return;
    }

    if ((pVpnPolicy->u4VpnPolicyType != VPN_XAUTH) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_PRESHAREDKEY) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_XAUTH_CERT) &&
        (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_CERT))
    {
        if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                    &PolicyNameOctStr, DESTROY) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhSetFsIkeCMStatus (&EngineNameOctetStr, &PolicyNameOctStr,
                                     DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                return;
            }
        }
    }
    else
    {
        /* In RA-VPN Case we will have crypto map 
         * installed dynalically for each client
         * that is conencted to us. The Crypto map name
         * will be as follows in this
         * case - PolicyName-a.b.c.d , 
         * Where policy name is the Vpn Policy name and
         * a..b.c.d is the private IP address allocated to the client */
        IkeUtilDelAllCryptoMapforPolicy (&EngineNameOctetStr,
                                         &PolicyNameOctStr);
    }

    /* If the PolicyType is XAUTH, the delete the RAINFO & 
     * Protected Network Configuration in IKE */

    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {

        /*  Create the RAINFO Data base in IKE */
        if (nmhTestv2FsIkeRAInfoStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                        &PolicyNameOctStr,
                                        DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsIkeRAInfoStatus (&EngineNameOctetStr, &PolicyNameOctStr,
                                     DESTROY);
        }

        /* Delete the Pretected Network Status */
        if (nmhTestv2FsIkeProtectNetStatus
            (&u4SnmpErrorStatus, &EngineNameOctetStr, &PolicyNameOctStr,
             DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsIkeProtectNetStatus (&EngineNameOctetStr, &PolicyNameOctStr,
                                         DESTROY);
        }
    }

    /* Delete the Transform Set */
    if (nmhTestv2FsIkeTSStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &PolicyNameOctStr, DESTROY) == SNMP_SUCCESS)
    {
        nmhSetFsIkeTSStatus (&EngineNameOctetStr, &PolicyNameOctStr, DESTROY);
    }

    /* Delete the Policy */
    if (nmhTestv2FsIkePolicyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                    u4IkePolicyPriority,
                                    DESTROY) == SNMP_SUCCESS)
    {
        nmhSetFsIkePolicyStatus (&EngineNameOctetStr, u4IkePolicyPriority,
                                 DESTROY);
    }
    return;
}

/**************************************************************************/
INT1
VpnUtilDeleteKeyDB (tVpnIdInfo * pVpnIdInfo)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT4               u4EngineEntryIndex = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    UINT1               au1EngineName[MAX_NAME_LENGTH + IKE_ONE] = { IKE_ZERO };
    tSNMP_OCTET_STRING_TYPE KeyIdOctetStr;

    /* Key ID */
    KeyIdOctetStr.pu1_OctetList = (UINT1 *) VPN_ID_VALUE (pVpnIdInfo);
    KeyIdOctetStr.i4_Length = (INT4) STRLEN (VPN_ID_VALUE (pVpnIdInfo));

    for (u4EngineEntryIndex = IKE_ZERO; u4EngineEntryIndex < gu4NumIkeEngines;
         u4EngineEntryIndex += IKE_ONE)
    {
        /* Fill the required IKE related parameters from the VPN Policy */
        MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

        STRNCPY (au1EngineName, IKE_GET_ENGINE_NAME (u4EngineEntryIndex),
                 MAX_NAME_LENGTH);

        EngineNameOctetStr.pu1_OctetList = au1EngineName;
        EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

        /* Delete the row with the Key ID */

        if (nmhTestv2FsIkeKeyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                     &KeyIdOctetStr, DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsIkeKeyStatus (&EngineNameOctetStr, &KeyIdOctetStr, DESTROY);
        }
    }

    /* If the key does not present, it might have been deleted at the time of
     * engine deletion itself. If the policy is configured for IPSEC manual 
     * case, Ike Engine would have not been created itself. */
    return SNMP_SUCCESS;

}

/*****************************************************************************/
INT1
VpnUtilIkeUpdateRaUserDetails (tVpnRaUserInfo * pVpnRaUserInfoNode,
                               INT4 i4AddDelFlag)
{

    UINT1               au1Protocol[IKE_MAX_TYPE] = "IKE";

    if (i4AddDelFlag == VPN_ADD_RA_USER_INFO)
    {
        if (AuthDbAddUser (pVpnRaUserInfoNode->au1VpnRaUserName,
                           pVpnRaUserInfoNode->au1VpnRaUserSecret,
                           au1Protocol) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4AddDelFlag == VPN_DEL_RA_USER_INFO)
    {
        if (AuthDbDeleteUser (pVpnRaUserInfoNode->au1VpnRaUserName) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VpnIkeCreateAndUpdatePolicyDB 
*  Description   : This function Creates or updates the VPN IKE Policer 
*                  i.e IKE Phase I related parameters.
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT4
VpnIkeCreateAndUpdatePolicyDB (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT4               u4IkePolicyPriority = IKE_ZERO;
    INT4                i4AuthMode = IKE_ZERO;
    UINT1               au1PeerAddr[IKE_MAX_ADDR_STR_LEN + IKE_ONE]
        = { IKE_ZERO };
    UINT1               au1PeerIdValue[VPN_MAX_NAME_LENGTH + IKE_ONE]
        = { IKE_ZERO };
    UINT1               au1LocalIdValue[VPN_MAX_NAME_LENGTH + IKE_ONE]
        = { IKE_ZERO };
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PolicyNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerIdValOctStr;
    tSNMP_OCTET_STRING_TYPE LocalIdValOctStr;
    tSNMP_OCTET_STRING_TYPE PeerAddrOctetStr;
    tIkeEngine         *pIkeEngine = NULL;

    INT4                i4HashAlgo = IKE_ZERO;
    INT4                i4EncrAlgo = IKE_ZERO;
    INT4                i4DHGrp = IKE_ZERO;
    INT4                i4LifetimeType = IKE_ZERO;
    UINT4               u4LifeTime = IKE_ZERO;
    INT4                i4ExchMode = IKE_ZERO;
    INT4                i4PeerIdType = IKE_ZERO;
    INT4                i4LocalIdType = IKE_ZERO;
    UINT4               u4Addr = IKE_ZERO;
    UINT1              *pu1EngineName = NULL;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));
    MEMSET (au1PeerAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN);
    MEMSET (au1PeerIdValue, IKE_ZERO, VPN_MAX_NAME_LENGTH + IKE_ONE);
    MEMSET (au1LocalIdValue, IKE_ZERO, VPN_MAX_NAME_LENGTH + IKE_ONE);
    MEMSET (au1PeerAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN + IKE_ONE);

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);
    if (NULL == pu1EngineName)
    {
        return IKE_FAILURE;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    PolicyNameOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    PolicyNameOctetStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    TAKE_IKE_SEM ();
    pIkeEngine = IkeSnmpGetEngine (EngineNameOctetStr.pu1_OctetList,
                                   EngineNameOctetStr.i4_Length);
    if (pIkeEngine == NULL)
    {
        GIVE_IKE_SEM ();
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "VpnIkeCreateAndUpdatePolicyDB:"
                     "Engine does not exist\r\n");
        return IKE_FAILURE;
    }
    GIVE_IKE_SEM ();

    /* Get the Free Policy Priority */

    for (u4IkePolicyPriority = IKE_ONE;
         u4IkePolicyPriority <= VPN_SYS_MAX_NUM_TUNNELS; u4IkePolicyPriority++)
    {
        if (IkeSnmpGetPolicy (pIkeEngine, u4IkePolicyPriority) != NULL)
        {
            continue;
        }
        break;
    }
    /* Create the POLICER */
    if (nmhTestv2FsIkePolicyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                    u4IkePolicyPriority,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkePolicyStatus (&EngineNameOctetStr, u4IkePolicyPriority,
                                 CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Update the Policy name in the Policy Table. Policy Name will be used to 
       get the policy priority while deleting */
    if (IkeUtilUpdatePolicyName (&EngineNameOctetStr, u4IkePolicyPriority,
                                 &PolicyNameOctetStr) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "VpnIkeCreateAndUpdatePolicyDB:"
                     "Failed to update Policy Name in IKE Structure\r\n");
        return IKE_FAILURE;
    }

    /* Set the Ike Version (v1 or v2) */
    if (IkeSetIkeVersion (&EngineNameOctetStr, u4IkePolicyPriority,
                          pVpnPolicy->u1VpnIkeVer) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Set all the Policy Attributes */
    /*  Set the Phase I Peer Id Type and Value */
    i4PeerIdType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    PeerIdValOctStr.pu1_OctetList = au1PeerIdValue;

    nmhGetFsVpnIkePhase1PeerIdValue (&PolicyNameOctetStr, &PeerIdValOctStr);

    if (IkeUpdatePolicyPeerId (au1EngineName, PeerIdValOctStr.pu1_OctetList,
                               i4PeerIdType,
                               u4IkePolicyPriority) != IKE_SUCCESS)
    {
        return IKE_FAILURE;
    }

    /*  Set the Phase I Peer Id Type and Value */
    i4LocalIdType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
        i2IdType;

    LocalIdValOctStr.pu1_OctetList = au1LocalIdValue;

    nmhGetFsVpnIkePhase1LocalIdValue (&PolicyNameOctetStr, &LocalIdValOctStr);

    if (IkeUpdatePolicyLocalId (au1EngineName, LocalIdValOctStr.pu1_OctetList,
                                i4LocalIdType,
                                u4IkePolicyPriority) != IKE_SUCCESS)
    {
        return IKE_FAILURE;
    }

    /* Set the Authentication Type - Pre-Shared Key/RSA Certificate */
    i4AuthMode = (INT4) pVpnPolicy->u4VpnPolicyType;
    switch (i4AuthMode)
    {
            /* In Case of XAuth &  RA- Pre shared Key - Preshared Key as 
               well as extended authentication will be present */
        case VPN_XAUTH:
        case VPN_IKE_PRESHAREDKEY:
        case VPN_IKE_RA_PRESHAREDKEY:
            if (pVpnPolicy->u1VpnIkeVer == IKE_ISAKMP_VERSION)
            {
                i4AuthMode = IKE_PRESHARED_KEY;
            }
            else
            {
                i4AuthMode = IKE2_AUTH_PSK;
            }
            break;

        case VPN_IKE_CERTIFICATE:
        case VPN_IKE_RA_CERT:
        case VPN_IKE_XAUTH_CERT:
            if (pVpnPolicy->u1VpnIkeVer == IKE_ISAKMP_VERSION)
            {
                if (pVpnPolicy->u1AuthAlgoType == VPN_AUTH_RSA)
                {
                    i4AuthMode = IKE_RSA_SIGNATURE;
                }
                else if (pVpnPolicy->u1AuthAlgoType == VPN_AUTH_DSA)
                {
                    i4AuthMode = IKE_DSS_SIGNATURE;
                }
            }
            else
            {
                if (pVpnPolicy->u1AuthAlgoType == VPN_AUTH_RSA)
                {
                    i4AuthMode = IKE2_AUTH_RSA;
                }
                else if (pVpnPolicy->u1AuthAlgoType == VPN_AUTH_DSA)
                {
                    i4AuthMode = IKE2_AUTH_DSA;
                }
            }
            break;
        default:
            return IKE_FAILURE;
    }

    if (nmhTestv2FsIkePolicyAuthMethod (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                        u4IkePolicyPriority,
                                        i4AuthMode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkePolicyAuthMethod (&EngineNameOctetStr, u4IkePolicyPriority,
                                     i4AuthMode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* PHASE I - HASH Algorithm */
    i4HashAlgo = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo;

    if (nmhTestv2FsIkePolicyHashAlgo (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                      u4IkePolicyPriority,
                                      i4HashAlgo) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkePolicyHashAlgo (&EngineNameOctetStr, u4IkePolicyPriority,
                                   i4HashAlgo) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* PHASE I - Encryption Algorithm */
    i4EncrAlgo =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4EncryptionAlgo;

    switch (i4EncrAlgo)
    {
        case VPN_DES_CBC:
            if (pVpnPolicy->u1VpnIkeVer == IKE2_MAJOR_VERSION)
            {
                i4EncrAlgo = IKE_IPSEC_ESP_DES_CBC;
            }
            else
            {
                i4EncrAlgo = IKE_DES_CBC;
            }
            break;

        case VPN_3DES_CBC:
            if (pVpnPolicy->u1VpnIkeVer == IKE2_MAJOR_VERSION)
            {
                i4EncrAlgo = IKE_IPSEC_ESP_3DES_CBC;
            }
            else
            {
                i4EncrAlgo = IKE_3DES_CBC;
            }
            break;

        case VPN_AES_128:
            i4EncrAlgo = IKE_AES_KEY_LEN_128;
            break;

        case VPN_AES_192:
            i4EncrAlgo = IKE_AES_DEF_KEY_LEN_VAL;
            break;

        case VPN_AES_256:
            i4EncrAlgo = IKE_AES_KEY_LEN_256;
            break;

        default:
            return IKE_FAILURE;
    }

    if (nmhTestv2FsIkePolicyEncryptionAlgo (&u4SnmpErrorStatus,
                                            &EngineNameOctetStr,
                                            u4IkePolicyPriority,
                                            i4EncrAlgo) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkePolicyEncryptionAlgo (&EngineNameOctetStr,
                                         u4IkePolicyPriority,
                                         i4EncrAlgo) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* PHASE I - DH Group */
    i4DHGrp = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4DHGroup;
    if (nmhTestv2FsIkePolicyDHGroup (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                     u4IkePolicyPriority,
                                     i4DHGrp) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkePolicyDHGroup (&EngineNameOctetStr, u4IkePolicyPriority,
                                  i4DHGrp) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Life TimeType, Value and KB */
    i4LifetimeType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTimeType;
    u4LifeTime = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTime;

    if ((i4LifetimeType != IKE_ZERO) && (u4LifeTime != IKE_ZERO))
    {
        if (nmhTestv2FsIkePolicyLifeTimeType
            (&u4SnmpErrorStatus, &EngineNameOctetStr, u4IkePolicyPriority,
             i4LifetimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        if (nmhSetFsIkePolicyLifeTimeType
            (&EngineNameOctetStr, u4IkePolicyPriority,
             i4LifetimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        if (i4LifetimeType == FSIKE_LIFETIME_KB)
        {
            /* Set the value of the lifetime */
            if (nmhTestv2FsIkePolicyLifeTimeKB
                (&u4SnmpErrorStatus, &EngineNameOctetStr, u4IkePolicyPriority,
                 u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }

            if (nmhSetFsIkePolicyLifeTimeKB
                (&EngineNameOctetStr, u4IkePolicyPriority,
                 u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        else
        {
            /* Set the value of the lifetime */
            if (nmhTestv2FsIkePolicyLifeTime
                (&u4SnmpErrorStatus, &EngineNameOctetStr, u4IkePolicyPriority,
                 u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }

            if (nmhSetFsIkePolicyLifeTime
                (&EngineNameOctetStr, u4IkePolicyPriority,
                 u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
    }

    /* Set Policy Mode - Main/Aggressive */
    i4ExchMode = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4Mode;

    if (nmhTestv2FsIkePolicyMode (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                  u4IkePolicyPriority,
                                  i4ExchMode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkePolicyMode (&EngineNameOctetStr, u4IkePolicyPriority,
                               i4ExchMode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
/* Set the PEER Ip address */
    /* In some cases the peer IP address is not set. So use protected network
     * type inted of the tunnle termination type */
    switch (pVpnPolicy->u4ProtectNetType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            u4Addr = pVpnPolicy->RemoteTunnTermAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1PeerAddr);
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_NTOP6 (&pVpnPolicy->RemoteTunnTermAddr.Ipv6Addr, au1PeerAddr);
            break;
        }
        default:
        {
            return IKE_FAILURE;
        }
    }
    PeerAddrOctetStr.pu1_OctetList = au1PeerAddr;
    PeerAddrOctetStr.i4_Length = (INT4) STRLEN (au1PeerAddr);
    if (IkeUpdatePeerIPInPolicy (&EngineNameOctetStr,
                                 u4IkePolicyPriority,
                                 &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    /* Since all the attributes are set,
     * now we can set the row status to acive */
    /* Create the POLICER */
    if (nmhTestv2FsIkePolicyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                    u4IkePolicyPriority,
                                    ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkePolicyStatus (&EngineNameOctetStr, u4IkePolicyPriority,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*********************************************************************
*  Function Name : IkeVpnCreateAndUpdateKeyDB 
*  Description   : This function Creates or updates the VPN IKE Key 
*                  i.e Pre-Shared Key.
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT4
VpnIkeCreateAndUpdateRaInfoDB (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4Error = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1               au1PeerIdValue[VPN_MAX_NAME_LENGTH + IKE_ONE]
        = { IKE_ZERO };
    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };

    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PolicyNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerIdOctetStr;
    tSNMP_OCTET_STRING_TYPE LocAddrOctetStr;
#ifdef VPN_WANTED
    tSNMP_OCTET_STRING_TYPE OctetStr1;
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;
#endif

    tIkeEngine         *pIkeEngine = NULL;
    tIkeRemoteAccessInfo *pIkeRAInfo = NULL;

    INT4                i4Pfs = IKE_ZERO;
    INT4                i4Mode = IKE_ZERO;
    INT4                i4LifeTimeType = IKE_ZERO;
    INT4                i4LifeTime = IKE_ZERO;
    INT4                i4RAVPNServer = IKE_INVALID;
    UINT4               u4IpAddr = IKE_ZERO;
    UINT4               u4Addr = IKE_ZERO;
    UINT4               u4PrefixLen = IKE_ZERO;
    UINT1              *pu1EngineName = NULL;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));
    MEMSET (au1PeerIdValue, IKE_ZERO, VPN_MAX_NAME_LENGTH + IKE_ONE);
    MEMSET (au1LocalAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN);

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);
    if (NULL == pu1EngineName)
    {
        return IKE_FAILURE;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    /* RAInfo name and TransformSet Name will be same as Policy Name */
    PolicyNameOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    PolicyNameOctetStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    /* Enable Global Xauth mode as  server and config mode as server, 
     * if the user has not already set it as RAVPN_CLIENT */

    nmhGetFsVpnRaServer (&i4RAVPNServer);
    if (i4RAVPNServer != RAVPN_CLIENT)
    {
        nmhSetFsikeRAXAuthMode (TRUE);
        nmhSetFsikeRACMMode (TRUE);
    }
    /*  Create the RAINFO Data base in IKE */
    if (nmhTestv2FsIkeRAInfoStatus (&u4Error, &EngineNameOctetStr,
                                    &PolicyNameOctetStr,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeRAInfoStatus (&EngineNameOctetStr, &PolicyNameOctetStr,
                                 CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* User Name /Password Configuration is needed for ravpn client in
     * xauth mode.Get the first username/passwd from the Vpn usrname/passwd
     * database and configure RaDatabase */
    if ((i4RAVPNServer == RAVPN_CLIENT) &&
        ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
         (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT)))
    {
#ifdef VPN_WANTED
        if ((TMO_SLL_Count (&gVpnRaUserList) != IKE_ZERO))
        {
            pVpnRaUserInfoNode =
                (tVpnRaUserInfo *) TMO_SLL_First (&gVpnRaUserList);
            OctetStr1.pu1_OctetList = pVpnRaUserInfoNode->au1VpnRaUserName;
            OctetStr1.i4_Length =
                (INT4) STRLEN (pVpnRaUserInfoNode->au1VpnRaUserName);
            if (nmhSetFsIkeRAXAuthUserName
                (&EngineNameOctetStr, &PolicyNameOctetStr,
                 &OctetStr1) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
            OctetStr1.pu1_OctetList = pVpnRaUserInfoNode->au1VpnRaUserSecret;
            OctetStr1.i4_Length =
                (INT4) STRLEN (pVpnRaUserInfoNode->au1VpnRaUserSecret);
            if (nmhSetFsIkeRAXAuthUserPasswd
                (&EngineNameOctetStr, &PolicyNameOctetStr,
                 &OctetStr1) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        else
        {
            return IKE_FAILURE;
        }
#endif
    }
    /* Configure all the Parameters related to the RAInfo table */
    if (nmhSetFsIkeRACMEnable (&EngineNameOctetStr, &PolicyNameOctetStr,
                               IKE_CM_ENABLE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    /* Enable the Xauth Server only of Policy type is Xauth,
     * Else not do not */
    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {
        if (nmhSetFsIkeRAXAuthEnable (&EngineNameOctetStr, &PolicyNameOctetStr,
                                      IKE_XAUTH_ENABLE) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
        /* Set the Xauth Authentication type for server as Generic 
         * For now Generic is only supported, radius is not supprted */
        if (nmhSetFsIkeRAXAuthType (&EngineNameOctetStr,
                                    &PolicyNameOctetStr,
                                    IKE_XAUTH_GENERIC) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    /* Set the Peer ID */
    switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType)
    {
        case VPN_ID_TYPE_IPV4:
            u4IpAddr =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                Ip4Addr;
            u4Addr = IKE_HTONL (u4IpAddr);
            INET_NTOP4 (&u4Addr, au1PeerIdValue);
            PeerIdOctetStr.pu1_OctetList = au1PeerIdValue;
            PeerIdOctetStr.i4_Length = (INT4) STRLEN (au1PeerIdValue);
            break;
        case VPN_ID_TYPE_IPV6:
            INET_NTOP6 (&(pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                          IkePhase1.PolicyPeerID.uID.Ip6Addr), au1PeerIdValue);
            PeerIdOctetStr.pu1_OctetList = au1PeerIdValue;
            PeerIdOctetStr.i4_Length = (INT4) STRLEN (au1PeerIdValue);
            break;
        case VPN_ID_TYPE_EMAIL:
            PeerIdOctetStr.pu1_OctetList =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                au1Email;
            PeerIdOctetStr.i4_Length =
                (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                               PolicyPeerID.uID.au1Email);
            break;
        case VPN_ID_TYPE_FQDN:
            PeerIdOctetStr.pu1_OctetList =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                au1Fqdn;
            PeerIdOctetStr.i4_Length =
                (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                               PolicyPeerID.uID.au1Fqdn);
            break;
        case VPN_ID_TYPE_DN:
            PeerIdOctetStr.pu1_OctetList =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                au1Dn;
            PeerIdOctetStr.i4_Length =
                (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                               PolicyPeerID.uID.au1Dn);
            break;
        case VPN_ID_TYPE_KEYID:
            PeerIdOctetStr.pu1_OctetList =
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                au1KeyId;
            PeerIdOctetStr.i4_Length =
                (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                               PolicyPeerID.uID.au1KeyId);
            break;
        default:
            return (IKE_FAILURE);
    }
    if (nmhTestv2FsIkeRAInfoPeerId (&u4Error, &EngineNameOctetStr,
                                    &PolicyNameOctetStr,
                                    &PeerIdOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeRAInfoPeerId (&EngineNameOctetStr, &PolicyNameOctetStr,
                                 &PeerIdOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhTestv2FsIkeRAInfoPeerIdType (&u4Error,
                                        &EngineNameOctetStr,
                                        &PolicyNameOctetStr,
                                        pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                        IkePhase1.PolicyPeerID.i2IdType) ==
        SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeRAInfoPeerIdType (&EngineNameOctetStr,
                                     &PolicyNameOctetStr,
                                     pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                     IkePhase1.PolicyPeerID.i2IdType) ==
        SNMP_FAILURE)

    {
        return IKE_FAILURE;
    }

    /* Apply the Transform Set - TransformSet name is same as policy and 
     * RAINFO name*/
    if (nmhSetFsIkeRATransformSetBundle (&EngineNameOctetStr,
                                         &PolicyNameOctetStr,
                                         &PolicyNameOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* If the Policy Type is XAUTH then
     * set the Protected Network as the Local IP in the 
     * access list */
    switch (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            u4IpAddr = (i4RAVPNServer == RAVPN_CLIENT) ?
                pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv4Addr :
                pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv4Addr;

            u4Addr = IKE_HTONL (u4IpAddr);
            INET_NTOP4 (&u4Addr, au1LocalAddr);
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            if (i4RAVPNServer == RAVPN_CLIENT)
            {
                INET_NTOP6 (&pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv6Addr,
                            au1LocalAddr);
            }
            else
            {
                INET_NTOP6 (&pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv6Addr,
                            au1LocalAddr);
            }
            break;
        }
        default:
        {
            return IKE_FAILURE;
        }
    }

    u4PrefixLen = (i4RAVPNServer == RAVPN_CLIENT) ?
        pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen :
        pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen;

    /* Local Network is to be confugured as a.b.c.d/prefix (eg. 30.0.0.0/8) */
    (*(au1LocalAddr + STRLEN (au1LocalAddr))) = '/';

    SNPRINTF (((CHR1 *) (au1LocalAddr + STRLEN (au1LocalAddr))),
              (IKE_MAX_ADDR_STR_LEN - STRLEN (au1LocalAddr)), "%d",
              u4PrefixLen);

    LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
    LocAddrOctetStr.i4_Length = (INT4) STRLEN (au1LocalAddr);

    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {

        if (VpnIkeCreateAndUpdateProtNetDB (pVpnPolicy, &LocAddrOctetStr) ==
            IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    /* Apply the Protected Network - ProtectedNet Name will be same as policy
     * and RAINFO */
    if (nmhTestv2FsIkeRAInfoProtectedNetBundle (&u4Error,
                                                &EngineNameOctetStr,
                                                &PolicyNameOctetStr,
                                                &PolicyNameOctetStr) ==
        SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeRAInfoProtectedNetBundle (&EngineNameOctetStr,
                                             &PolicyNameOctetStr,
                                             &PolicyNameOctetStr) ==
        SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Set the Mode Tunnel / Transport */
    i4Mode = (INT4) pVpnPolicy->u1VpnMode;
    if (nmhTestv2FsIkeRAMode (&u4Error, &EngineNameOctetStr,
                              &PolicyNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeRAMode (&EngineNameOctetStr, &PolicyNameOctetStr,
                           i4Mode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Set the  CMPfs - DH group (Group 1 | Group 2| group 3) */
    i4Pfs = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1DHGroup;
    if (i4Pfs != IKE_ZERO)
    {
        if (nmhTestv2FsIkeRAPfs (&u4Error, &EngineNameOctetStr,
                                 &PolicyNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        if (nmhSetFsIkeRAPfs (&EngineNameOctetStr, &PolicyNameOctetStr,
                              i4Pfs) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    i4LifeTimeType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1LifeTimeType;
    i4LifeTime = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u4LifeTime;

    if ((i4LifeTimeType != IKE_ZERO) && (i4LifeTime != IKE_ZERO))
    {
        /* Set the Life Time Type and Value */
        if (nmhTestv2FsIkeRALifeTimeType (&u4Error, &EngineNameOctetStr,
                                          &PolicyNameOctetStr,
                                          i4LifeTimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
        if (nmhSetFsIkeRALifeTimeType (&EngineNameOctetStr, &PolicyNameOctetStr,
                                       i4LifeTimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        if (nmhSetFsIkeRALifeTime (&EngineNameOctetStr, &PolicyNameOctetStr,
                                   (UINT4) i4LifeTime) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    /* In case of Ravpn client configure Peer Ip address */
    if (i4RAVPNServer == RAVPN_CLIENT)
    {
        pIkeEngine = IkeSnmpGetEngine (EngineNameOctetStr.pu1_OctetList,
                                       EngineNameOctetStr.i4_Length);
        if (pIkeEngine == NULL)
        {
            return (IKE_FAILURE);
        }

        pIkeRAInfo =
            IkeSnmpGetRAInfo (pIkeEngine, PolicyNameOctetStr.pu1_OctetList,
                              PolicyNameOctetStr.i4_Length);
        if (pIkeRAInfo == NULL)
        {
            return (IKE_FAILURE);
        }

        if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            pIkeRAInfo->PeerAddr.u4AddrType = IPV4ADDR;
            pIkeRAInfo->PeerAddr.Ipv4Addr =
                pVpnPolicy->RemoteTunnTermAddr.Ipv4Addr;
        }
        else
        {
            pIkeRAInfo->PeerAddr.u4AddrType = IPV6ADDR;
            IKE_MEMCPY (&(pIkeRAInfo->PeerAddr.Ipv6Addr),
                        &(pVpnPolicy->RemoteTunnTermAddr.Ipv6Addr),
                        sizeof (tIp6Addr));
        }
    }
    if (nmhSetFsIkeRAInfoStatus (&EngineNameOctetStr,
                                 &PolicyNameOctetStr, ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*********************************************************************
*  Function Name : IkeVpnCreateAndUpdateKeyDB 
*  Description   : This function Creates or updates the VPN IKE Key 
*                  i.e Pre-Shared Key.
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*                  u4IfIndex - Interface Index
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT1
VpnIkeCreateAndUpdateKeyDB (tVpnIdInfo * pVpnIdInfo, UINT1 *pu1IkeEngineName)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    INT4                i4KeyIdType = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE KeyIdOctetStr;
    tSNMP_OCTET_STRING_TYPE KeyStringOctetStr;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

    STRNCPY (au1EngineName, pu1IkeEngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    /* Key ID */
    KeyIdOctetStr.pu1_OctetList = (UINT1 *) VPN_ID_VALUE (pVpnIdInfo);
    KeyIdOctetStr.i4_Length = (INT4) STRLEN (VPN_ID_VALUE (pVpnIdInfo));

    /* Create the row with the Key ID */
    if (nmhTestv2FsIkeKeyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &KeyIdOctetStr,
                                 CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsIkeKeyStatus (&EngineNameOctetStr, &KeyIdOctetStr,
                              CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Set All the Attributes of the Key table */

    i4KeyIdType = (INT4) VPN_ID_TYPE (pVpnIdInfo);
    if (nmhTestv2FsIkeKeyIdType (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &KeyIdOctetStr, i4KeyIdType) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsIkeKeyIdType (&EngineNameOctetStr, &KeyIdOctetStr,
                              i4KeyIdType) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Set the Secret Key */
    KeyStringOctetStr.pu1_OctetList = (UINT1 *) VPN_ID_KEY (pVpnIdInfo);
    KeyStringOctetStr.i4_Length = (INT4) STRLEN (VPN_ID_KEY (pVpnIdInfo));

    if (nmhTestv2FsIkeKeyString (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &KeyIdOctetStr,
                                 &KeyStringOctetStr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsIkeKeyString (&EngineNameOctetStr, &KeyIdOctetStr,
                              &KeyStringOctetStr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Create the row with the Key ID */
    if (nmhTestv2FsIkeKeyStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &KeyIdOctetStr, ACTIVE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsIkeKeyStatus (&EngineNameOctetStr, &KeyIdOctetStr,
                              ACTIVE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VpnIkeCreateAndUpdtTrnsfmSetDB 
*  Description   : This function Creates or updates the VPN IKE 
*                  Trasnform Set data base - IKE Phase II Encryption
*                  algorithm
*
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT4
VpnIkeCreateAndUpdtTrnsfmSetDB (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    tSNMP_OCTET_STRING_TYPE TransformOctetStr;
    INT4                i4EspEncrAlgo = IKE_ZERO;
    INT4                i4EspHashAlgo = IKE_ZERO;
    INT4                i4AhHashAlgo = IKE_ZERO;
    UINT1              *pu1EngineName = NULL;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);
    if (NULL == pu1EngineName)
    {
        return IKE_FAILURE;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    TransformOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    TransformOctetStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    if (nmhTestv2FsIkeTSStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &TransformOctetStr,
                                CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeTSStatus (&EngineNameOctetStr, &TransformOctetStr,
                             CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Set All the Attributes */
    if (pVpnPolicy->u4VpnSecurityProtocol == SEC_ESP)
    {
        i4EspEncrAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo;
        switch (i4EspEncrAlgo)
        {
            case VPN_DES_CBC:
                i4EspEncrAlgo = IKE_IPSEC_ESP_DES_CBC;
                break;

            case VPN_3DES_CBC:
                i4EspEncrAlgo = IKE_IPSEC_ESP_3DES_CBC;
                break;

            case VPN_NULLESPALGO:
                i4EspEncrAlgo = IKE_IPSEC_ESP_NULL;
                break;

            case VPN_AES_128:
                i4EspEncrAlgo = IKE_AES_KEY_LEN_128;
                break;
            case VPN_AES_192:
                i4EspEncrAlgo = IKE_AES_DEF_KEY_LEN_VAL;
                break;
            case VPN_AES_256:
                i4EspEncrAlgo = IKE_AES_KEY_LEN_256;
                break;
            case SEC_AESCTR:
                i4EspEncrAlgo = SEC_AESCTR;
                break;
            case SEC_AESCTR192:
                i4EspEncrAlgo = SEC_AESCTR192;
                break;
            case SEC_AESCTR256:
                i4EspEncrAlgo = SEC_AESCTR256;
                break;

            default:
                return IKE_FAILURE;
        }

        if (i4EspEncrAlgo != IKE_ZERO)
        {
            if (nmhTestv2FsIkeTSEspEncryptionAlgo (&u4SnmpErrorStatus,
                                                   &EngineNameOctetStr,
                                                   &TransformOctetStr,
                                                   i4EspEncrAlgo) ==
                SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        if (nmhSetFsIkeTSEspEncryptionAlgo (&EngineNameOctetStr,
                                            &TransformOctetStr,
                                            i4EspEncrAlgo) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        /* Set the EspHashAlgorithm either MD5 or SHA1 , if configured */
        i4EspHashAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo;
        switch (i4EspHashAlgo)
        {
            case VPN_HMACMD5:
                i4EspHashAlgo = IKE_IPSEC_HMAC_MD5;
                break;
            case VPN_HMACSHA1:
                i4EspHashAlgo = IKE_IPSEC_HMAC_SHA1;
                break;
            case SEC_XCBCMAC:
                i4EspHashAlgo = SEC_XCBCMAC;
                break;
            case HMAC_SHA_256:
                i4EspHashAlgo = HMAC_SHA_256;
                break;
            case HMAC_SHA_384:
                i4EspHashAlgo = HMAC_SHA_384;
                break;
            case HMAC_SHA_512:
                i4EspHashAlgo = HMAC_SHA_512;
                break;
            default:
                break;
        }
        if (i4EspHashAlgo != IKE_ZERO)
        {
            if (nmhTestv2FsIkeTSEspHashAlgo (&u4SnmpErrorStatus,
                                             &EngineNameOctetStr,
                                             &TransformOctetStr,
                                             i4EspHashAlgo) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }

            if (nmhSetFsIkeTSEspHashAlgo (&EngineNameOctetStr,
                                          &TransformOctetStr,
                                          i4EspHashAlgo) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
    }
    else
    {
        /* User has specified AH Hash Algo (md5/sha1)
         * ESP is not set by the user */
        i4AhHashAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo;
        if (pVpnPolicy->u1VpnIkeVer != IKE2_MAJOR_VERSION)
        {
            switch (i4AhHashAlgo)
            {
                case VPN_HMACMD5:
                    i4AhHashAlgo = IKE_IPSEC_AH_MD5;
                    break;
                case VPN_HMACSHA1:
                    i4AhHashAlgo = IKE_IPSEC_AH_SHA1;
                    break;
                case SEC_XCBCMAC:
                    i4AhHashAlgo = SEC_XCBCMAC;
                    break;
                case HMAC_SHA_256:
                    i4AhHashAlgo = HMAC_SHA_256;
                    break;
                case HMAC_SHA_384:
                    i4AhHashAlgo = HMAC_SHA_384;
                    break;
                case HMAC_SHA_512:
                    i4AhHashAlgo = HMAC_SHA_512;
                    break;
                default:
                    break;
            }
        }
        if (i4AhHashAlgo != IKE_ZERO)
        {
            if (nmhTestv2FsIkeTSAhHashAlgo (&u4SnmpErrorStatus,
                                            &EngineNameOctetStr,
                                            &TransformOctetStr,
                                            i4AhHashAlgo) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        if (nmhSetFsIkeTSAhHashAlgo (&EngineNameOctetStr,
                                     &TransformOctetStr,
                                     i4AhHashAlgo) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    /* i4EspEncrAlgo = IKE_NONE (only AH Hash algo is set) */
    /* Set the Transform Set to be applied on this bundle */
    /* All the attributes are set, so set the row status to active */
    if (nmhTestv2FsIkeTSStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &TransformOctetStr, ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeTSStatus (&EngineNameOctetStr, &TransformOctetStr,
                             ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;

}

/*********************************************************************
*  Function Name : IkeVpnCreateAndUpdateCryptoDB 
*  Description   : This function Creates or updates the VPN IKE 
*                  Crypto map data base - IKE Phase II Parameters 
*
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT4
VpnIkeCreateAndUpdateCryptoMapDB (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4SnmpErrorStatus = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };

    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE PeerAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE CryptoMapNameOctetStr;
    tSNMP_OCTET_STRING_TYPE LocAddrOctetStr;
    tSNMP_OCTET_STRING_TYPE RemAddrOctetStr;

    INT4                i4Pfs = IKE_ZERO;
    INT4                i4LifeTimeType = IKE_ZERO;
    UINT4               u4LifeTime = IKE_ZERO;
    UINT4               u4Addr = IKE_ZERO;
    UINT4               u4IpAddr = IKE_ZERO;
    INT4                i4Mode = IKE_ZERO;
    UINT4               u4IkePolicyPriority = IKE_ZERO;

    UINT1               au1PeerAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1               au1LocalAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1               au1RemoteAddr[IKE_MAX_ADDR_STR_LEN] = { IKE_ZERO };
    UINT1              *pu1EngineName = NULL;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));
    MEMSET (au1PeerAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN);
    MEMSET (au1LocalAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN);
    MEMSET (au1RemoteAddr, IKE_ZERO, IKE_MAX_ADDR_STR_LEN);

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);
    if (NULL == pu1EngineName)
    {
        return IKE_FAILURE;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    CryptoMapNameOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    CryptoMapNameOctetStr.i4_Length =
        (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &CryptoMapNameOctetStr, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                             CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    /* Set the attributes */
    /* Set the  CMPfs - DH group (Group 1 | Group 2| gropup 3 */
    i4Pfs = (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1DHGroup;

    if (i4Pfs != IKE_ZERO)
    {
        if (nmhTestv2FsIkeCMPfs (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &CryptoMapNameOctetStr, i4Pfs) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    if (nmhSetFsIkeCMPfs (&EngineNameOctetStr,
                          &CryptoMapNameOctetStr, i4Pfs) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    /* Set the Life TimeType, LifeTime, LifeTimeKB */
    i4LifeTimeType =
        (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1LifeTimeType;
    u4LifeTime = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u4LifeTime;

    if ((i4LifeTimeType != IKE_ZERO) && (u4LifeTime != IKE_ZERO))
    {
        if (nmhTestv2FsIkeCMLifeTimeType (&u4SnmpErrorStatus,
                                          &EngineNameOctetStr,
                                          &CryptoMapNameOctetStr,
                                          i4LifeTimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }

        if (nmhSetFsIkeCMLifeTimeType (&EngineNameOctetStr,
                                       &CryptoMapNameOctetStr,
                                       i4LifeTimeType) == SNMP_FAILURE)
        {
            return IKE_FAILURE;
        }
        if (i4LifeTimeType == FSIKE_LIFETIME_KB)
        {
            /* Set the value of the lifetime */
            if (nmhTestv2FsIkeCMLifeTimeKB (&u4SnmpErrorStatus,
                                            &EngineNameOctetStr,
                                            &CryptoMapNameOctetStr,
                                            u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }

            if (nmhSetFsIkeCMLifeTimeKB (&EngineNameOctetStr,
                                         &CryptoMapNameOctetStr,
                                         u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
        else
        {
            /* Set the value of the lifetime */
            if (nmhTestv2FsIkeCMLifeTime
                (&u4SnmpErrorStatus, &EngineNameOctetStr,
                 &CryptoMapNameOctetStr, u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }

            if (nmhSetFsIkeCMLifeTime (&EngineNameOctetStr,
                                       &CryptoMapNameOctetStr,
                                       u4LifeTime) == SNMP_FAILURE)
            {
                return IKE_FAILURE;
            }
        }
    }

    /* Set the PEER Ip address */
    switch (pVpnPolicy->u4TunTermAddrType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            u4Addr = pVpnPolicy->RemoteTunnTermAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4Addr);
            INET_NTOP4 (&u4Addr, au1PeerAddr);
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_NTOP6 (&pVpnPolicy->RemoteTunnTermAddr.Ipv6Addr, au1PeerAddr);
            break;
        }
        default:
        {
            return IKE_FAILURE;
        }
    }
    PeerAddrOctetStr.pu1_OctetList = au1PeerAddr;
    PeerAddrOctetStr.i4_Length = (INT4) STRLEN (au1PeerAddr);

    if (nmhTestv2FsIkeCMPeerAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                  &CryptoMapNameOctetStr,
                                  &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeCMPeerAddr (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                               &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Apply the trasnform set to the Crypto-map */
    /*Transform Set name is same as Policy Name */
    if (nmhTestv2FsIkeCMTransformSetBundle
        (&u4SnmpErrorStatus, &EngineNameOctetStr, &CryptoMapNameOctetStr,
         &CryptoMapNameOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeCMTransformSetBundle (&EngineNameOctetStr,
                                         &CryptoMapNameOctetStr,
                                         &CryptoMapNameOctetStr) ==
        SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    /* Set the MODE - Tunnel/Trasnport */
    i4Mode = (INT4) pVpnPolicy->u1VpnMode;
    if (nmhTestv2FsIkeCMMode (&u4SnmpErrorStatus, &EngineNameOctetStr,
                              &CryptoMapNameOctetStr, i4Mode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeCMMode (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                           i4Mode) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Set the Accesslist - Local and remote networks */

    switch (pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            u4IpAddr = pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4IpAddr);
            INET_NTOP4 (&u4Addr, au1LocalAddr);
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_NTOP6 (&pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv6Addr,
                        au1LocalAddr);
            break;
        }
        default:
            break;
    }

    /* Local Network is to be confugured as a.b.c.d/prefix (eg. 30.0.0.0/8) */
    (*(au1LocalAddr + STRLEN (au1LocalAddr))) = '/';

    SNPRINTF (((CHR1 *) (au1LocalAddr + STRLEN (au1LocalAddr))),
              (IKE_MAX_ADDR_STR_LEN - STRLEN (au1LocalAddr)), "%d",
              pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen);

    LocAddrOctetStr.pu1_OctetList = au1LocalAddr;
    LocAddrOctetStr.i4_Length = (INT4) STRLEN (au1LocalAddr);

    if (nmhTestv2FsIkeCMLocalAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                   &CryptoMapNameOctetStr,
                                   &LocAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeCMLocalAddr (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                                &LocAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* Similarly set the remote network in the a.b.c.d/8 form */
    switch (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            u4IpAddr = pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv4Addr;
            u4Addr = IKE_HTONL (u4IpAddr);
            INET_NTOP4 (&u4Addr, au1RemoteAddr);
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_NTOP6 (&pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv6Addr,
                        au1RemoteAddr);
            break;
        }
        default:
            break;
    }

    (*(au1RemoteAddr + STRLEN (au1RemoteAddr))) = '/';
    SNPRINTF (((CHR1 *) (au1RemoteAddr + STRLEN (au1RemoteAddr))),
              (IKE_MAX_ADDR_STR_LEN - STRLEN (au1RemoteAddr)), "%d",
              pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen);

    RemAddrOctetStr.pu1_OctetList = au1RemoteAddr;
    RemAddrOctetStr.i4_Length = (INT4) STRLEN (au1RemoteAddr);
    if (nmhTestv2FsIkeCMRemAddr (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                 &CryptoMapNameOctetStr,
                                 &RemAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeCMRemAddr (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                              &RemAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (IkeUtilGetPolicyPriotityFromName (&EngineNameOctetStr,
                                          &CryptoMapNameOctetStr,
                                          &u4IkePolicyPriority) == IKE_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "VpnUtilDelIkeParams: Failed to"
                     "get the Priority from PolicyName\r\n");
        return IKE_FAILURE;
    }

    if (IkeUpdatePeerIPInPolicy (&EngineNameOctetStr,
                                 u4IkePolicyPriority,
                                 &PeerAddrOctetStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    /* All the attributes are set, make the crypto-map ACTIVE */
    if (nmhTestv2FsIkeCMStatus (&u4SnmpErrorStatus, &EngineNameOctetStr,
                                &CryptoMapNameOctetStr, ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (nmhSetFsIkeCMStatus (&EngineNameOctetStr, &CryptoMapNameOctetStr,
                             ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (pVpnPolicy->u1VpnIkeVer == IKE2_MAJOR_VERSION)
    {
        if (IkeUpdateAccessPortInfo
            (&EngineNameOctetStr, &CryptoMapNameOctetStr,
             pVpnPolicy->LocalProtectNetwork.u2StartPort,
             pVpnPolicy->LocalProtectNetwork.u2EndPort,
             pVpnPolicy->RemoteProtectNetwork.u2StartPort,
             pVpnPolicy->RemoteProtectNetwork.u2EndPort) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }
    if (IkeUpdateProtocolInfo (&EngineNameOctetStr,
                               &CryptoMapNameOctetStr,
                               pVpnPolicy->u4VpnProtocol) == IKE_FAILURE)
    {
        return IKE_FAILURE;
    }

    return IKE_SUCCESS;
}

/*********************************************************************
*  Function Name : VpnIkeCreateAndUpdateProtNetDB 
*  Description   : This function Creates or updates the VPN IKE 
*                  Protected Network DB - IKE Remote Access Parameters
*
*  Input(s)      : pVpnPolicy - Pointer to the Vpn Policy Data Structure 
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
INT4
VpnIkeCreateAndUpdateProtNetDB (tVpnPolicy * pVpnPolicy,
                                tSNMP_OCTET_STRING_TYPE * ProcNetAddrOctStr)
{
    UINT4               u4Error = IKE_ZERO;
    UINT1               au1EngineName[MAX_NAME_LENGTH] = { IKE_ZERO };
    UINT1              *pu1EngineName = NULL;

    tSNMP_OCTET_STRING_TYPE EngineNameOctetStr;
    tSNMP_OCTET_STRING_TYPE ProtNetNameOctetStr;

    MEMSET (au1EngineName, IKE_ZERO, sizeof (au1EngineName));

    pu1EngineName = IkeGetEngNameFromIfIndex (pVpnPolicy->u4IfIndex);

    if (NULL == pu1EngineName)
    {
        return IKE_FAILURE;
    }

    STRNCPY (au1EngineName, pu1EngineName, MAX_NAME_LENGTH - IKE_ONE);

    /* Fill the required IKE related parameters from the VPN Policy */
    EngineNameOctetStr.pu1_OctetList = au1EngineName;
    EngineNameOctetStr.i4_Length = (INT4) STRLEN (au1EngineName);

    /* RAInfo name and TransformSet Name will be same as Policy Name */
    ProtNetNameOctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
    ProtNetNameOctetStr.i4_Length =
        (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);

    if (nmhTestv2FsIkeProtectNetStatus (&u4Error, &EngineNameOctetStr,
                                        &ProtNetNameOctetStr,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeProtectNetStatus (&EngineNameOctetStr, &ProtNetNameOctetStr,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhTestv2FsIkeProtectNetAddr (&u4Error, &EngineNameOctetStr,
                                      &ProtNetNameOctetStr,
                                      ProcNetAddrOctStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeProtectNetAddr (&EngineNameOctetStr, &ProtNetNameOctetStr,
                                   ProcNetAddrOctStr) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    if (pVpnPolicy->u1VpnIkeVer == IKE2_MAJOR_VERSION)
    {
        if (IkeUpdateRaPortInfo
            (&EngineNameOctetStr, &ProtNetNameOctetStr,
             pVpnPolicy->LocalProtectNetwork.u2StartPort,
             pVpnPolicy->LocalProtectNetwork.u2EndPort,
             pVpnPolicy->u4VpnProtocol) == IKE_FAILURE)
        {
            return IKE_FAILURE;
        }
    }

    if (nmhTestv2FsIkeProtectNetStatus (&u4Error, &EngineNameOctetStr,
                                        &ProtNetNameOctetStr,
                                        ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }

    if (nmhSetFsIkeProtectNetStatus (&EngineNameOctetStr, &ProtNetNameOctetStr,
                                     ACTIVE) == SNMP_FAILURE)
    {
        return IKE_FAILURE;
    }
    return IKE_SUCCESS;
}

/*Certificate related wrapper functions - Start */

/**************************************************************************/
/*  Function Name   : IkeVpnGenKeyPair                                    */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which generates rsa/dsa key pair                    */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyName - Key Name used to store in Database     */
/*                    u4Bits  - Length of the RSA/DSA Key                 */
/*                    u4Type  - Type of algorithm whether RSA/DSA         */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnGenKeyPair (UINT1 *pu1EngineName, UINT1 *pu1KeyName, UINT4 u4Bits,
                  UINT4 u4Type)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;

#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((u4Bits == RSA_KEY_512) || (u4Bits == RSA_KEY_768)
            || (u4Bits == RSA_KEY_1024))
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "RSA keys of size 512, 768 and 1024 bits are disabled"
                         " in FIPS mode \n");
            return OSIX_FAILURE;
        }
    }

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenKeyPair: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenKeyPair: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }
    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeGenKey, IKE_ZERO, sizeof (tCliIkeGenKey));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenKey.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenKeyPair: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeGenKey.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenKey.pu1EngineName, IKE_ZERO,
            (MAX_NAME_LENGTH + IKE_ONE));
    MEMCPY (pBuffer->CliIkeGenKey.pu1EngineName, pu1EngineName,
            MAX_NAME_LENGTH);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenKey.pu1KeyName) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenKeyPair: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeGenKey.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenKey.pu1KeyName, IKE_ZERO,
            (STRLEN (pu1KeyName) + IKE_ONE));
    MEMCPY (pBuffer->CliIkeGenKey.pu1KeyName, pu1KeyName, STRLEN (pu1KeyName));

    pBuffer->CliIkeGenKey.i4Bits = (INT4) u4Bits;
    pBuffer->CliIkeGenKey.u4Type = u4Type;

    IkeCliGenKeyPair (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeVpnImportKey                                     */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which imports the rsa/dsa key                       */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyName - Key Name used to store in Database     */
/*                    u4Type  - Type of algorithm whether RSA/DSA         */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnImportKey (UINT1 *pu1EngineName, UINT1 *pu1KeyName,
                 UINT1 *pu1Filename, UINT4 u4Type)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
    UINT4               u4RespStatus = IKE_ZERO;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportKey: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportKey: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeImportKey, IKE_ZERO, sizeof (tCliIkeImportKey));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportKey.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportKey: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeImportKey.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportKey.pu1EngineName, IKE_ZERO,
            (MAX_NAME_LENGTH + IKE_ONE));
    MEMCPY (pBuffer->CliIkeImportKey.pu1EngineName, pu1EngineName,
            MAX_NAME_LENGTH);

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportKey.pu1KeyName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportKey: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportKey.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportKey.pu1KeyName, IKE_ZERO,
            (STRLEN (pu1KeyName) + IKE_ONE));
    MEMCPY (pBuffer->CliIkeImportKey.pu1KeyName, pu1KeyName,
            STRLEN (pu1KeyName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportKey.pu1FileName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportKey: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportKey.pu1FileName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportKey.pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportKey.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportKey.pu1FileName, IKE_ZERO,
            (STRLEN (pu1Filename) + IKE_ONE));
    MEMCPY (pBuffer->CliIkeImportKey.pu1FileName, pu1Filename,
            STRLEN (pu1Filename));
    pBuffer->CliIkeImportKey.u4Type = u4Type;

    IkeCliImportKey (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeVpnShowKeys                                      */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which displays the rsa/dsa key                      */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnShowKeys (UINT1 *pu1EngineName, UINT4 u4Type)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowKeys: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowKeys: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeGenKey, IKE_ZERO, sizeof (tCliIkeGenKey));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenKey.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowKeys: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeGenKey.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenKey.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenKey.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));
    pBuffer->CliIkeGenKey.u4Type = u4Type;

    IkeCliShowKeys (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }
    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnDelKeys                                       */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which deletes the specified key                     */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyName - RSA Key Name used to store in Database */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnDelKeys (UINT1 *pu1EngineName, UINT1 *pu1KeyID, UINT4 u4Type)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelKeys: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelKeys: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeGenKey, IKE_ZERO, sizeof (tCliIkeGenKey));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenKey.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelKeys: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeGenKey.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenKey.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenKey.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (STRCMP (pu1KeyID, CLI_IKE_DEL_ALL) != IKE_ZERO)
    {
        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pBuffer->CliIkeGenKey.pu1KeyName) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeVpnDelKeys: unable to allocate " "buffer\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeGenKey.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }

        if (pBuffer->CliIkeGenKey.pu1KeyName == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeGenKey.pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pBuffer->CliIkeGenKey.pu1KeyName, IKE_ZERO,
                (STRLEN (pu1KeyID) + IKE_ONE));
        MEMCPY (pBuffer->CliIkeGenKey.pu1KeyName, pu1KeyID, STRLEN (pu1KeyID));
    }
    pBuffer->CliIkeGenKey.u4Type = u4Type;

    IkeCliDelKeyPair (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnGenCertReq                                    */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which generates Certificate Request                 */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyName - Key Name                               */
/*                    pu1SubName - Subject Name String                    */
/*                    pu1KeyName - Subject Alternate Name String          */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnGenCertReq (UINT1 *pu1EngineName, UINT1 *pu1KeyID,
                  UINT1 *pu1SubName, UINT1 *pu1SubAltName, UINT4 u4Type)
{
    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeGenCertReq, IKE_ZERO, sizeof (tCliIkeGenCertReq));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenCertReq.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeGenCertReq.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenCertReq.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenCertReq.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenCertReq.pu1KeyName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeGenCertReq.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenCertReq.pu1KeyName, IKE_ZERO,
            STRLEN (pu1KeyID) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenCertReq.pu1KeyName, pu1KeyID, STRLEN (pu1KeyID));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenCertReq.pu1SubjectName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeGenCertReq.pu1SubjectName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenCertReq.pu1SubjectName, IKE_ZERO,
            STRLEN (pu1SubName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenCertReq.pu1SubjectName, pu1SubName,
            STRLEN (pu1SubName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeGenCertReq.pu1SubjectAltName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnGenCertReq: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeGenCertReq.pu1SubjectAltName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1KeyName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeGenCertReq.pu1SubjectName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeGenCertReq.pu1SubjectAltName, IKE_ZERO,
            STRLEN (pu1SubAltName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeGenCertReq.pu1SubjectAltName, pu1SubAltName,
            STRLEN (pu1SubAltName));
    pBuffer->CliIkeGenCertReq.u4Type = u4Type;

    IkeCliGenCertReq (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnImportCert                                    */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which import the signed Certificate                 */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1Filename - Certificate File Name                 */
/*                    u4EncodeType - Certificate format type PEM/DER      */
/*                    pu1Key - RSA Key Name to which this Certficate      */
/*                              is related                                */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/
INT4
IkeVpnImportCert (UINT1 *pu1EngineName, UINT1 *pu1Filename, UINT4 u4EncodeType,
                  UINT1 *pu1Key)
{
    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;

#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "pInputMsg: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportCert: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeImportCert, IKE_ZERO, sizeof (tCliIkeImportCert));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "pInputMsg: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportCert.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCert.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCert.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.pu1FileName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "pInputMsg: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeImportCert.pu1FileName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCert.pu1FileName, IKE_ZERO,
            STRLEN (pu1Filename) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCert.pu1FileName, pu1Filename,
            STRLEN (pu1Filename));

    pBuffer->CliIkeImportCert.u1EncodeType = (UINT1) u4EncodeType;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.pu1KeyName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "pInputMsg: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportCert.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCert.pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCert.pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCert.pu1KeyName,
            IKE_ZERO, STRLEN (pu1Key) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCert.pu1KeyName, pu1Key, STRLEN (pu1Key));

    IkeCliImportCert (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeVpnImportPeerCert                                */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which imports the Peer Certificate                  */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1PeerCertID - Peer Certificate Name for Index     */
/*                    pu1Filename - Certificate File Name                 */
/*                    u4EncodeType - Certificate format type PEM/DER      */
/*                    u1Flag - Flag which denotes whether the peer        */
/*                             certificate is trusted or untrusted        */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnImportPeerCert (UINT1 *pu1EngineName, UINT1 *pu1PeerCertID,
                      UINT1 *pu1FileName, UINT1 u1EncodeType, UINT1 u1Flag)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportPeerCert: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportPeerCert: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }
    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeImportPeerCert, IKE_ZERO,
            sizeof (tCliIkeImportPeerCert));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportPeerCert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportPeerCert.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportPeerCert.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportPeerCert.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.pu1FileName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportPeerCert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportPeerCert.pu1FileName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportPeerCert.pu1FileName, IKE_ZERO,
            STRLEN (pu1FileName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportPeerCert.pu1FileName, pu1FileName,
            STRLEN (pu1FileName));

    pBuffer->CliIkeImportPeerCert.u1EncodeType = u1EncodeType;
    pBuffer->CliIkeImportPeerCert.u1Flag = u1Flag;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.pu1CertName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportPeerCert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportPeerCert.pu1CertName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportPeerCert.
                            pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportPeerCert.pu1CertName, IKE_ZERO,
            STRLEN (pu1PeerCertID) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportPeerCert.pu1CertName, pu1PeerCertID,
            STRLEN (pu1PeerCertID));

    IkeCliImportPeerCert (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeVpnImportCACert                                  */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which imports the CA Certificate                    */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1PeerCertID - CA Certificate Name for Index       */
/*                    pu1Filename - Certificate File Name                 */
/*                    u4EncodeType - Certificate format type PEM/DER      */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                         */
/**************************************************************************/

INT4
IkeVpnImportCACert (UINT1 *pu1EngineName, UINT1 *pu1CaCertName,
                    UINT1 *pu1FileName, UINT1 u1EncodeType)
{
    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;
    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeProcessKernelIpsecRequest: unable to allocate "
                     "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportCACert: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeImportCaCert, IKE_ZERO,
            sizeof (tCliIkeImportCaCert));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportCACert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportCaCert.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCaCert.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCaCert.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.pu1FileName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportCACert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    if (pBuffer->CliIkeImportCaCert.pu1FileName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCaCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCaCert.pu1FileName, IKE_ZERO,
            STRLEN (pu1FileName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCaCert.pu1FileName, pu1FileName,
            STRLEN (pu1FileName));

    pBuffer->CliIkeImportCaCert.u1EncodeType = u1EncodeType;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.pu1CertName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnImportCACert: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCaCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeImportCaCert.pu1CertName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCaCert.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeImportCaCert.pu1FileName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeImportCaCert.pu1CertName, IKE_ZERO,
            STRLEN (pu1CaCertName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeImportCaCert.pu1CertName, pu1CaCertName,
            STRLEN (pu1CaCertName));

    IkeCliImportCaCert (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnShowCerts                                     */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which displays the Certificates                     */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    u4Option - States the type of Certificate to be     */
/*                             displayed (CA/Peer/Self)                     */
/*                  pu1Key   - Specifies whether all certs have to be       */
/*                             displayed or specific cert needs to be       */
/*                             displayed                                    */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnShowCerts (UINT1 *pu1EngineName, UINT4 u4Option, UINT1 *pu1Key)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowCerts: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }

    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnShowCerts: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    switch (u4Option)
    {
        case CLI_IKE_SHOW_CERTS:
        {
            MEMSET (&pBuffer->CliIkeImportCert, IKE_ZERO,
                    sizeof (tCliIkeImportCert));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnShowCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }

            if (pBuffer->CliIkeImportCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportCert.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (STRCMP (pu1Key, CLI_IKE_SHOW_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.
                     pu1KeyName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnShowCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }

                if (pBuffer->CliIkeImportCert.pu1KeyName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportCert.pu1KeyName, IKE_ZERO,
                        STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportCert.pu1KeyName, pu1Key,
                        STRLEN (pu1Key));
            }
            IkeCliShowCerts (pBuffer, &pRespMsg);
            break;
        }
        case CLI_IKE_SHOW_PEER_CERTS:
        {
            MEMSET (&pBuffer->CliIkeImportPeerCert, IKE_ZERO,
                    sizeof (tCliIkeImportPeerCert));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnShowCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeImportPeerCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportPeerCert.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportPeerCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (STRCMP (pu1Key, CLI_IKE_SHOW_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.
                     pu1CertName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnShowCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportPeerCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                if (pBuffer->CliIkeImportPeerCert.pu1CertName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportPeerCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportPeerCert.pu1CertName, IKE_ZERO,
                        STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportPeerCert.pu1CertName, pu1Key,
                        STRLEN (pu1Key));
            }
            IkeCliShowPeerCert (pBuffer, &pRespMsg);
            break;
        }
        case CLI_IKE_SHOW_CA_CERTS:
        {
            MEMSET (&pBuffer->CliIkeImportCaCert, IKE_ZERO,
                    sizeof (tCliIkeImportCaCert));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnShowCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeImportCaCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportCaCert.pu1EngineName,
                    IKE_ZERO, STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportCaCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (STRCMP (pu1Key, CLI_IKE_SHOW_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.
                     pu1CertName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnShowCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportPeerCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                if (pBuffer->CliIkeImportCaCert.pu1CertName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportCaCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportCaCert.pu1CertName,
                        IKE_ZERO, STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportCaCert.pu1CertName, pu1Key,
                        STRLEN (pu1Key));
            }
            IkeCliShowCaCert (pBuffer, &pRespMsg);
            break;
        }
        case CLI_IKE_SHOW_CERT_MAP:
        {
            MEMSET (&pBuffer->CliIkeMapCertToPeer, IKE_ZERO,
                    sizeof (tCliIkeMapCertToPeer));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnShowCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeMapCertToPeer.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeMapCertToPeer.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));
            IkeCliShowCertMap (pBuffer, &pRespMsg);

            break;
        }
        default:
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
    }

    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : IkeVpnDelCerts                                      */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which deletes the Certificates                      */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    u4Option - States the type of Certificate to be     */
/*                         deleted (CA/Peer/Self)                           */
/*                    pu1Key   - Specifies whether all certs have to be   */
/*                             deleted or specific cert needs to be         */
/*                             deleted                                      */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnDelCerts (UINT1 *pu1EngineName, UINT4 u4Option, UINT1 *pu1Key)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelCerts: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelCerts: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    switch (u4Option)
    {
        case CLI_IKE_DEL_CERT:
        {
            MEMSET (&pBuffer->CliIkeImportCert, IKE_ZERO,
                    sizeof (tCliIkeImportCert));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnDelCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeImportCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportCert.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (STRCMP (pu1Key, CLI_IKE_DEL_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportCert.
                     pu1KeyName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnDelCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                if (pBuffer->CliIkeImportCert.pu1KeyName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportCert.pu1KeyName, IKE_ZERO,
                        STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportCert.pu1KeyName, pu1Key,
                        STRLEN (pu1Key));
            }
            IkeCliDelCert (pBuffer, &pRespMsg);
        }
            break;
        case CLI_IKE_DEL_PEER_CERT:
        {
            MEMSET (&pBuffer->CliIkeImportPeerCert, IKE_ZERO,
                    sizeof (tCliIkeImportPeerCert));
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnDelCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeImportPeerCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportPeerCert.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportPeerCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));
            if (pu1Key == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }

            if (STRCMP (pu1Key, CLI_IKE_DEL_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportPeerCert.
                     pu1CertName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnDelCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                if (pBuffer->CliIkeImportPeerCert.pu1CertName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportPeerCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportPeerCert.pu1CertName, IKE_ZERO,
                        STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportPeerCert.pu1CertName, pu1Key,
                        STRLEN (pu1Key));
            }
            if (pu1Key != NULL)
            {
                pBuffer->CliIkeImportPeerCert.u1DelFlag = (UINT1) ATOI (pu1Key);
            }
            IkeCliDelPeerCert (pBuffer, &pRespMsg);

        }
            break;
        case CLI_IKE_DEL_CA_CERT:
        {
            MEMSET (&pBuffer->CliIkeImportCaCert, IKE_ZERO,
                    sizeof (tCliIkeImportCaCert));
            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnDelCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }

            if (pBuffer->CliIkeImportCaCert.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeImportCaCert.pu1EngineName,
                    IKE_ZERO, STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeImportCaCert.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (STRCMP (pu1Key, CLI_IKE_DEL_ALL) != IKE_ZERO)
            {
                if (MemAllocateMemBlock
                    (IKE_UINT_MEMPOOL_ID,
                     (UINT1 **) (VOID *) &pBuffer->CliIkeImportCaCert.
                     pu1CertName) == MEM_FAILURE)
                {
                    IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                                 "IkeVpnDelCerts: unable to allocate "
                                 "buffer\n");
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                if (pBuffer->CliIkeImportCaCert.pu1CertName == NULL)
                {
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pBuffer->CliIkeImportCaCert.
                                        pu1EngineName);
                    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                        (UINT1 *) pInputMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pBuffer->CliIkeImportCaCert.pu1CertName,
                        IKE_ZERO, STRLEN (pu1Key) + IKE_ONE);
                MEMCPY (pBuffer->CliIkeImportCaCert.pu1CertName, pu1Key,
                        STRLEN (pu1Key));
            }
            IkeCliDelCaCert (pBuffer, &pRespMsg);
        }
            break;
        case CLI_IKE_DEL_CERT_MAP:
        {
            MEMSET (&pBuffer->CliIkeMapCertToPeer, IKE_ZERO,
                    sizeof (tCliIkeMapCertToPeer));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.
                 pu1EngineName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnDelCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeMapCertToPeer.pu1EngineName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeMapCertToPeer.pu1EngineName, IKE_ZERO,
                    STRLEN (pu1EngineName) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1EngineName, pu1EngineName,
                    STRLEN (pu1EngineName));

            if (MemAllocateMemBlock
                (IKE_UINT_MEMPOOL_ID,
                 (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.
                 pu1KeyName) == MEM_FAILURE)
            {
                IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                             "IkeVpnDelCerts: unable to allocate " "buffer\n");
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            if (pBuffer->CliIkeMapCertToPeer.pu1KeyName == NULL)
            {
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                    (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                    pu1EngineName);
                MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pBuffer->CliIkeMapCertToPeer.pu1KeyName, IKE_ZERO,
                    STRLEN (pu1Key) + IKE_ONE);
            MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1KeyName, pu1Key,
                    STRLEN (pu1Key));

            IkeCliDelCertMap (pBuffer, &pRespMsg);

        }
            break;
        default:
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
    }

    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     ** by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnCertMapPeer                                   */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which maps a particular key to the specified Peer ID*/
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyId - Key ID to which the peer has to be mapped*/
/*                      pu1IdType - Type of Peer ID                         */
/*                      pu1PeerId - Peer ID which is used for Mapping       */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnCertMapPeer (UINT1 *pu1EngineName, UINT1 *pu1KeyId,
                   UINT1 *pu1IdType, UINT1 *pu1PeerId)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnCertMapPeer: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnCertMapPeer: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeMapCertToPeer, IKE_ZERO,
            sizeof (tCliIkeMapCertToPeer));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnCertMapPeer: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeMapCertToPeer.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeMapCertToPeer.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1KeyName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnCertMapPeer: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeMapCertToPeer.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeMapCertToPeer.pu1KeyName, IKE_ZERO,
            STRLEN (pu1KeyId) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1KeyName, pu1KeyId,
            STRLEN (pu1KeyId));

    if (pu1IdType == NULL)
    {
        pBuffer->CliIkeMapCertToPeer.u1KeyIdType =
            (UINT1) CLI_ATOL (CLI_IKE_MAP_ALL);
    }
    else
    {
        if (STRCMP (pu1IdType, "ipv4") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_IPV4;
        }
        else if (STRCMP (pu1IdType, "ipv6") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_IPV6;
        }
        else if (STRCMP (pu1IdType, "email") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_EMAIL;
        }
        else if (STRCMP (pu1IdType, "fqdn") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_FQDN;
        }
        else if (STRCMP (pu1IdType, "dn") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_DN;
        }
        else
        {
            mmi_printf ("\nINVALID Input(ipv4/ipv6/email/fqdn/dn) \n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1KeyId) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeVpnSaveCerts: unable to allocate " "buffer\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }
        if (pBuffer->CliIkeMapCertToPeer.pu1KeyId == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pBuffer->CliIkeMapCertToPeer.pu1KeyId, 0,
                STRLEN (pu1PeerId) + 1);
        MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1KeyId, pu1PeerId,
                STRLEN (pu1PeerId));
    }

    IkeCliCertMapPeer (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }
    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnDelCertMap                                    */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which deletes the map of a particular key           */
/*                                        to the specified Peer ID        */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*                    pu1KeyId - Key ID to which the peer has to be mapped*/
/*                      pu1IdType - Type of Peer ID                         */
/*                      pu1PeerId - Peer ID which is used for Mapping       */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnDelCertMap (UINT1 *pu1EngineName, UINT1 *pu1KeyId,
                  UINT1 *pu1IdType, UINT1 *pu1PeerId)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnDelCertMap: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnCertMapPeer: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeMapCertToPeer, IKE_ZERO,
            sizeof (tCliIkeMapCertToPeer));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnSaveCerts: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeMapCertToPeer.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeMapCertToPeer.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1KeyName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnSaveCerts: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeMapCertToPeer.pu1KeyName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                            (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                            pu1EngineName);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeMapCertToPeer.pu1KeyName, IKE_ZERO,
            STRLEN (pu1KeyId) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1KeyName, pu1KeyId,
            STRLEN (pu1KeyId));

    if (pu1IdType == NULL)
    {
        pBuffer->CliIkeMapCertToPeer.u1KeyIdType =
            (UINT1) CLI_ATOL (CLI_IKE_MAP_ALL);
    }
    else
    {
        if (STRCMP (pu1IdType, "ipv4") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_IPV4;
        }
        else if (STRCMP (pu1IdType, "ipv6") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_IPV6;
        }
        else if (STRCMP (pu1IdType, "email") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_EMAIL;
        }
        else if (STRCMP (pu1IdType, "fqdn") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_FQDN;
        }
        else if (STRCMP (pu1IdType, "dn") == IKE_ZERO)
        {
            pBuffer->CliIkeMapCertToPeer.u1KeyIdType = CLI_IKE_KEY_DN;
        }
        else
        {
            mmi_printf ("\nINVALID Input(ipv4/ipv6/email/fqdn/dn) \n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }

        if (MemAllocateMemBlock
            (IKE_UINT_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pBuffer->CliIkeMapCertToPeer.pu1KeyId) ==
            MEM_FAILURE)
        {
            IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                         "IkeVpnSaveCerts: unable to allocate " "buffer\n");
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }
        if (pBuffer->CliIkeMapCertToPeer.pu1KeyId == NULL)
        {
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1EngineName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID,
                                (UINT1 *) pBuffer->CliIkeMapCertToPeer.
                                pu1KeyName);
            MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
            return OSIX_FAILURE;
        }
        MEMSET (pBuffer->CliIkeMapCertToPeer.pu1KeyId, IKE_ZERO,
                STRLEN (pu1PeerId) + IKE_ONE);
        MEMCPY (pBuffer->CliIkeMapCertToPeer.pu1KeyId, pu1PeerId,
                STRLEN (pu1PeerId));
    }

    IkeCliDelCertMap (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_SUCCESS;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }
    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : IkeVpnSaveCerts                                     */
/*  Description     : This function calls the IKE CLI Interface API       */
/*                    which Saves all the certificates and keys which     */
/*                    are related to the specified Engine                 */
/*  Input(s)        : pu1EngineName - Ike Engine Name                     */
/*  Output(s)       : NONE                                                */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                           */
/**************************************************************************/

INT4
IkeVpnSaveCerts (UINT1 *pu1EngineName)
{

    UINT1              *pInputMsg = NULL;
    UINT1              *pRespMsg = NULL;
#ifdef CLI_WANTED
    UINT1              *pDisplayBuffer = NULL;
#endif
    UINT4               u4RespStatus = IKE_ZERO;

    tIkeCliConfigParams *pBuffer = NULL;

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID, (UINT1 **) (VOID *) &pInputMsg) == MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnSaveCerts: unable to allocate " "buffer\n");
        return OSIX_FAILURE;
    }
    if (pInputMsg == NULL)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnSaveCerts: Failed to allocate memory"
                     "using MemAllocateMemBlock \r\n");
        return OSIX_FAILURE;
    }

    CLI_SET_RESP_STATUS (pInputMsg, u4RespStatus);
    pBuffer = (tIkeCliConfigParams *) (void *) CLI_GET_DATA_PTR (pInputMsg);

    MEMSET (&pBuffer->CliIkeSaveCert, IKE_ZERO, sizeof (tCliIkeSaveCert));

    if (MemAllocateMemBlock
        (IKE_UINT_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pBuffer->CliIkeSaveCert.pu1EngineName) ==
        MEM_FAILURE)
    {
        IKE_MOD_TRC (gu4IkeTrace, DATA_PATH_TRC, "IKE",
                     "IkeVpnSaveCerts: unable to allocate " "buffer\n");
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    if (pBuffer->CliIkeSaveCert.pu1EngineName == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }
    MEMSET (pBuffer->CliIkeSaveCert.pu1EngineName, IKE_ZERO,
            STRLEN (pu1EngineName) + IKE_ONE);
    MEMCPY (pBuffer->CliIkeSaveCert.pu1EngineName, pu1EngineName,
            STRLEN (pu1EngineName));

    IkeCliSaveCert (pBuffer, &pRespMsg);
    if (pRespMsg == NULL)
    {
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        return OSIX_FAILURE;
    }

    CLI_GET_RESP_STATUS (u4RespStatus, pRespMsg);
    if (u4RespStatus == TRUE)
    {
#ifdef CLI_WANTED
        pDisplayBuffer = CLI_GET_DATA_PTR (pRespMsg);
        mmi_more_printf ((INT1 *) pDisplayBuffer);
#endif
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
        MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
        return OSIX_FAILURE;
    }

    /* free the memory allocated for the response message
     * by the protocol module */
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pInputMsg);
    MemReleaseMemBlock (IKE_UINT_MEMPOOL_ID, (UINT1 *) pRespMsg);
    return OSIX_SUCCESS;

}

#endif
