/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikesz.c,v 1.1 2013/08/22 14:56:40 siva Exp $
 *
 * Description: This file contains the IKE sizing routines    
 *
 ***********************************************************************/
#define _IKESZ_C
#include "ikememmac.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
INT4
IkeSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IKE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsIKESizingParams[i4SizingId].u4StructSize,
                                     FsIKESizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(IKEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IkeSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IkeSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIKESizingParams);
    return OSIX_SUCCESS;
}

VOID
IkeSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IKE_MAX_SIZING_ID; i4SizingId++)
    {
        if (IKEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IKEMemPoolIds[i4SizingId]);
            IKEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
