#ifndef __ERPSMBSM_H__
#define __ERPSMBSM_H__

PUBLIC INT4
ErpsMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo);

PUBLIC INT4
ErpsMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                             tMbsmSlotInfo * pSlotInfo);

#endif /*__ERPSMBSM_H__*/
