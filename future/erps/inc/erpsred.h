/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsred.h,v 1.8 2014/07/19 13:05:30 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for ERPS 
 *              module.
 *
 * *******************************************************************/
#ifndef __ERPSRED_H__
#define __ERPSRED_H__

enum{
    ERPS_RING_DYN_INFO,        
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as Ring Dynamic info. */
    ERPS_MAX_DYN_INFO_TYPE,    
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
};

/* HITLESS RESTART */
enum{
    ERPS_RED_BULK_REQUEST_MSG = RM_BULK_UPDT_REQ_MSG,
             /* Bulk request message type */
    ERPS_RED_BULK_UPDT_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
             /* Bulk update tail message type */
    ERPS_RED_DELETE_ALL_RING,
             /* Delete all ring message type */
    ERPS_RED_ENABLE_RING_MSG,
             /* Enable Ring message type */
    ERPS_RED_DISABLE_RING_MSG,
             /* Disable Ring message type */
    ERPS_RED_TIMER_REM_VALUE,
             /* Remaining timer value message type */
    ERPS_RED_NPSYNC_MSG
             /* Np Sync message type */
};

#define ERPS_RED_PKT_TYPE_FIELD_SIZE   1
#define ERPS_RED_MIN_LENGTH_FIELD_SIZE 2
#define ERPS_RED_BULK_REQ_SIZE         3 /* MsgType(1Byte) + Length (2Byte) */
#define ERPS_RED_BULK_UPDT_TAIL_SIZE   3 /* MsgType(1Byte) + Length (2Byte) */
#define ERPS_RED_DEL_ALL_RING_MSG_SIZE 7 /* MsgType(1Byte) + Length (2Byte) + 
                                           ContextId (4Byte)*/
#define ERPS_RED_MODULE_STATUS_SIZE    7 /* MsgType(1Byte) + Length (2Byte) + 
                                           ContextId (4Byte) + RingId (4Byte) */
#define ERPS_RED_TIMER_MSG_SIZE       23 /* MsgType(1Byte) + Length (2Byte) + 
                                           ContextId (4Byte) + RingId (4Byte) +
                                           Holdoff remaining value (4Byte) +
                                           Guard time remaining value (4Byte) +
                                           WTR time remaining value (4Byte)
                                         */
/*This macro is calculated based on the size of the members present in the structure 
 * tErpsHwRingInfo. Each member in this structure should be properly synched within 
 * the functions ErpsSyncNpFsErpsHwRingConfigSync and ErpsSyncNpProcessSyncMsg present in 
 * erpssync.c . Failing to do so, will affect the ERPS HA functionality*/

#define ERPS_RED_NP_SYNC_MSG_SIZE     (VLAN_DEV_VLAN_LIST_SIZE_EXT + MAX_ERPS_VLAN_GROUP_LIST_SIZE + 35 + ERPS_MAX_HW_HANDLE ) 
                                         /* MsgType(1Byte) + Length (2Byte) +
                                          * Vlan List - (VLAN_DEV_VLAN_LIST_SIZE_EXT bytes) +
                                          * VlanGroupList - (MAX_ERPS_VLAN_GROUP_LIST_SIZE) +
                                            ContextId(4Byte) + 
                                            Port1IfIndex (4Byte) +
                                            Port2IfIndex (4Byte) +
                                            RingId (4Byte) +
                                            HwIdHandle (4Byte) +
                                            VlanId (2Byte) +
                                            VlanGroupId (2Byte) + 
                                            Port1Action (1Byte)
                                            + Port2Action (1Byte) +
                                            RingAction (1Byte) +
                                            ProtectionType (1Byte) + 
                                            HwUpdatedPort (1Byte) +
                                            UnBlockRAPSChannel (1Byte)+
                                            HighestPriorityRequest (1 Byte) +
         u1Service (1 Byte) + 
                                            ERPS_MAX_HW_HANDLE size */

/* HITLESS RESTART */
#define ERPS_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define ERPS_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define ERPS_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL
#define ERPS_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */

#define ERPS_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ERPS_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ERPS_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ERPS_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define ERPS_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ERPS_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ERPS_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ERPS_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)


#endif /* __ERPSRED_H__ */
