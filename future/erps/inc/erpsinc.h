/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsinc.h,v 1.10 2015/07/18 06:36:45 siva Exp $
 *
 * Description: This file contains header files included in ERPS module.
 *****************************************************************************/
#ifndef __ERPSINC_H__
#define __ERPSINC_H__

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "iss.h"

#include "fsvlan.h"
#include "vcm.h"
#include "fm.h"
#include "ecfm.h"
#include "rstp.h"
#include "npapi.h"

#include "dbutil.h"

#include "erps.h"
#include "erpscons.h"
#include "fssyslog.h"
#ifdef L2RED_WANTED
#include "erpsred.h"
#endif
#include "erpsmacs.h"
#include "erpstdfs.h"

#ifdef NPAPI_WANTED
#include "erpsnp.h"
#include "nputil.h"
#ifdef L2RED_WANTED
#include "hwaud.h"
#include "hwaudmap.h"
#endif
#endif

#include "erpsprot.h"
#include "erpstrc.h"
#include "erpsglob.h"
#include "erpscli.h"
#include "fserpslw.h"
#include "fserpswr.h"
#include "erpsz.h"
#ifdef MBSM_WANTED
#include "erpsmbsm.h"
#endif

#ifdef ICCH_WANTED
#include "icch.h"
#endif
#ifdef LA_WANTED
#include "la.h"
#endif
#endif /*__ERPSINC_H__*/
