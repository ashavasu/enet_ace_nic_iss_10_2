/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: erpsprot.h,v 1.33 2016/09/17 12:41:26 siva Exp $
*
* Description: This file contains the function prototypes for
*              ERPS module
*******************************************************************/

#ifndef __ERPSPROT_H__
#define __ERPSPROT_H__
/*************************** erpsmain.c ************************************/
PUBLIC INT4 
ErpsMainModuleShutDown (VOID);
PUBLIC INT4 
ErpsMainModuleStart (VOID);

/*************************** erpstmr.c ************************************/
PUBLIC VOID ErpsTmrExpHandler (VOID);
PUBLIC VOID ErpsTmrInitTimerDesc (VOID);
PUBLIC INT4 ErpsTmrStartTimer (tErpsRingInfo *pRingInfo, UINT1 u1TimerType, 
                               UINT4 u4Duration);
PUBLIC INT4 ErpsTmrStopTimer (tErpsRingInfo *pRingInfo, UINT1 u1TimerType);
PUBLIC VOID ErpsGetSysTime (tOsixSysTime * pSysTime);
/*************************p erpsring.c ************************************/
PUBLIC tErpsRingInfo *
ErpsRingCreateRingNode (VOID);
PUBLIC INT4
ErpsRingAddNodeToRingRBTree (tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsRingDelNodeFromRBTree (tErpsRingInfo *pRingInfo);
PUBLIC tErpsRingInfo *
ErpsRingGetFirstNodeFrmRingTable (VOID);
PUBLIC tErpsRingInfo *
ErpsRingGetNextNodeFromRingTable (tErpsRingInfo *pCurrentRingEntry);
PUBLIC tErpsRingInfo *
ErpsRingGetNodeFromPortVlanTable (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId, UINT1 u1RingMacId);
PUBLIC tErpsRingInfo *
ErpsPortVlanTblGetFirstNodeOfPortAndVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          tVlanId VlanId);
PUBLIC tErpsRingInfo *
ErpsPortVlanTblGetNextNodeOfPortAndVlan (tErpsRingInfo *pCurrRingInfo,
                                         UINT4 u4IfIndex);
PUBLIC VOID
ErpsPortExternalEventNotify (tEcfmEventNotification *pEvent);
PUBLIC INT4
ErpsRingIdCmpFn (tRBElem *pRBElem1, tRBElem *pRBElem2);
PUBLIC INT4
ErpsRingPortVlanCmpFn (tRBElem *pRBElem1, tRBElem *pRBElem2);
PUBLIC INT4
ErpsRingDeleteAllRingsInContext (UINT4 u4ContextId);
PUBLIC INT4
ErpsRingAddTcNode (tErpsRingInfo *pSubRingEntry, tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsRingDelTcNode (tErpsRingInfo *pSubRingEntry, tErpsRingInfo *pRingInfo);
PUBLIC tErpsRingInfo *
ErpsRingGetTcNode (tErpsRingInfo *pSubRingEntry, tErpsRingInfo *pRingInfo);
PUBLIC tErpsRingInfo *
ErpsRingGetFirstTcNode (tErpsRingInfo *pSubRingEntry);
PUBLIC tErpsRingInfo *
ErpsRingGetNextTcNode (tErpsRingInfo *pSubRingEntry, tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsRingDyanamicSllCmpFn (tDynSLLElem * pDynElem1, tDynSLLElem * pDynElem2);
PUBLIC VOID
ErpsRingClearRingStats (tErpsRingInfo *pRingInfo);
PUBLIC tErpsRingInfo *
ErpsRingGetNodeFromRingTable (tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsRingActivateRing (tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsRingDeActivateRing (tErpsRingInfo *pRingInfo,UINT1 u1NewRowStatus);
PUBLIC tErpsRingInfo *
ErpsRingGetRingEntry (UINT4 u4ContextId, UINT4 u4RingId);
PUBLIC INT4
ErpsRingAddNodeToPortVlanRBTree (tErpsRingInfo * pRingInfo);
PUBLIC INT4
ErpsRingRemNodeFromPortVlanTable (tErpsRingInfo *pRingInfo);
tSubRingEntry *
ErpsGetSubRingEntry (tErpsRingInfo *pRingInfo,UINT4 u4SubRingId);
PUBLIC INT4
ErpsMepIdCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2);
PUBLIC INT4
ErpsRingAddNodeToMepRingIdRBTree (tErpsRingInfo * pRingInfo);
PUBLIC INT4
ErpsRingRemNodeFromMepRingIdTable (tErpsRingInfo * pRingInfo);
PUBLIC tErpsRingInfo *
ErpsGetNodeOfMepAndRingId (UINT4 u4ContextId, UINT4 u4MegId,
                           UINT4 u4MeId, UINT4 u4MepId,UINT4 u4RingId);

/**************************** erpssm.c ****************************************/
PUBLIC INT4         ErpsSmRingStateMachine
(UINT2 u2Event, tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);

PUBLIC INT4        ErpsSmRingHandleDisable
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleInit
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleInit
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleLocalSF
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleLocalClearSF
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleSFMsg
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleWtrExpiry
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleWtbExpiry
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleNRRBMsg
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleLocalSF
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleRAPSSF
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleRAPSNRRB
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingHandleNRMsg
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleRAPSNR
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingImpossibleEvent
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmRingNoAction 
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleLocalFS 
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleRAPSFS
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleLocalMS
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleRAPSMS
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleClearCmd
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4        ErpsSmV2RingHandleLocalClearSF
    (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg);

PUBLIC INT4         ErpsSmCalTopPriorityRequest
(tErpsRingInfo * pRingInfo, UINT1 u1Request, tErpsRcvdMsg * pErpsRcvdMsg);
PUBLIC INT4
ErpsSmMainRingStatusIndication(UINT4 u4ContextId,UINT4 u4RingId);

/*************************** erpsmsg.c ****************************************/

PUBLIC INT4 ErpsMsgEnque (tErpsQMsg * pMsg);
PUBLIC VOID ErpsMsgQueueHandler (VOID);
PUBLIC INT4 ErpsMsgProcessLinkSF (tErpsRingInfo * pRingInfo,
                                  tErpsCfmMepInfo MepInfo);
PUBLIC INT4        ErpsMsgProcessLinkClearSF (tErpsRingInfo * pRingInfo,
                                              tErpsCfmMepInfo MepInfo);

/*************************** erpstx.c *****************************************/
PUBLIC INT4
ErpsTxStartRapsMessages (tErpsRingInfo *pRingInfo, UINT1 u1StatusBits,
                         UINT1 u1RapsMessageType);

PUBLIC INT4 
ErpsTxStopRapsMessages (tErpsRingInfo *pRingInfo);

PUBLIC INT4
ErpsTxFormAndSendRAPSPdu (tErpsRingInfo * pRingInfo);

PUBLIC VOID ErpsTxInitRApsTxInfoSLL (VOID);

PUBLIC VOID ErpsTxEventHandler (VOID);


/*************************** erpsutil.c ***************************************/
PUBLIC INT4 ErpsTxInitiateFastTxn (tErpsRingInfo * pRingInfo);
PUBLIC INT4
ErpsUtilRegisterGetFltAndApply (tErpsRingInfo *pRingInfo);
PUBLIC VOID
ErpsUtilDeRegisterRing (tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsUtilDeRegisterRingPort (tErpsRingInfo * pRingInfo, UINT4 u4PortIndex);
PUBLIC VOID
ErpsUtilSetPortState (tErpsRingInfo *pRingInfo, UINT4 u4IfIndex,
                      UINT1 u1PortState);

PUBLIC VOID
ErpsUtilFlushFdbTable (tErpsRingInfo *pRingInfo, INT4 i4OptimizeFlag);
PUBLIC INT4
ErpsUtilFormAndSendRapsMsg (tErpsRingInfo *pRingInfo, UINT1 u1MsgType);
PUBLIC INT4
ErpsUtilIsErpsEnabled (UINT4 u4ContextId);
PUBLIC INT4
ErpsUtilIsErpsStarted (UINT4 u4ContextId);
INT4 
ErpsPortGetVlanIdEntry (UINT4 u4ContextId, UINT2 u2FsErpsVlanId); 

PUBLIC tErpsRingInfo *
ErpsUtilValidateCxtAndRingInfo (UINT4 u4ContextId, UINT4 u4RingId,
                                UINT4 *pu4ErrorCode);
PUBLIC INT4
ErpsUtilValidateContext (UINT4 u4ContextId, UINT4 *pu4ErrorCode);
PUBLIC tErpsRingInfo *
ErpsUtilValContextAndRingId (UINT4 u4ContextId, UINT4 u4RingId,
                             UINT4 *pu4ErrorCode);

PUBLIC VOID
ErpsUtilRingCfgNotInServ (tErpsRingInfo *pRingInfo);
PUBLIC INT4
ErpsUtilRapsTxDbLock (VOID);
PUBLIC INT4
ErpsUtilRapsTxDbUnLock (VOID);
PUBLIC VOID
ErpsUtilUpdateRingDynInfo (tErpsRingInfo *pRingInfo, 
                           unErpsRingDynInfo *pRingDynInfo, UINT1 u1DynInfoType);

PUBLIC INT4
ErpsUtilConfRingEntryInHw (tErpsRingInfo *pRingInfo, UINT1 u1RingStatus,
                           UINT1 u1HwPortUpdated);
PUBLIC VOID
ErpsUtilUpdateRingStatistics(tErpsRingInfo *pRingInfo, tErpsCfmMepInfo MepInfo,
                             UINT1 u1Type);
PUBLIC tErpsContextInfo *
ErpsUtilValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode);
PUBLIC INT4
ErpsUtilDelFdbAndTxRaps (tDynSLLElem *pRingTcPropNode);
VOID
ErpsUtilHandleHwConfigFailure (tErpsRingInfo *pRingInfo);
PUBLIC VOID
ErpsUtilMeasureTime(tErpsRingInfo *pRingInfo, UINT4 u4IfIndex, UINT1 u1Flag);

/*To give notification to RM and MSR */

PUBLIC VOID
ErpsUtilNotifyConfig(tErpsRingInfo *pRingInfo,UINT1 u1Type);

INT4
ErpsUtilValidateVlanGroup (UINT4 u4FsErpsContextId, UINT2 i4FsErpsVlanGroupId);

#ifdef TRACE_WANTED
VOID
ErpsUtilContextTrc (UINT4 u4ContextId, UINT4 u4RingId, UINT4 u4Mask,
                    const char *fmt, ...);

VOID ErpsUtilContextInitTrc (UINT4 u4ContextId,UINT4 u4Mask, const char *fmt,
                             ...);
#endif
PUBLIC INT4
ErpsUtilSetVlanGroupIdForVlan (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2VlanGroupId);
#ifdef NPAPI_WANTED
INT4
ErpsUtilHandleVlanGroupHwFailure (UINT4 u4ContextId,
                                  tVlanId VlanId,
                                  UINT2 u2OldGroupId,
                                  UINT2 u2VlanGroupId,
                                  UINT1 u1ActionFlag);
INT4
ErpsHandleNpCallbackEvents (tErpsQMsg * pErpsMsg);
#endif
PUBLIC INT4
ErpsUtilCompareNodeIdBprPair(tErpsPortNodeIdBPRPair *pNodeIdBPRPair1,
                             tErpsPortNodeIdBPRPair *pNodeIdBPRPair2);
/* HITLESS RESTART */
extern INT1 MsrIsMibRestoreInProgress (VOID);
INT4
ErpsUtilGetPeriodicTime (UINT4 u4IfIndex, UINT4 *pu4RetValErpsApsPeriodicTime);

/*************************** erpscxt.c ****************************************/
PUBLIC tErpsContextInfo * 
ErpsCxtMemInit (UINT4 u4ContextId);
PUBLIC INT4 
ErpsCxtHandleDeleteContxt (UINT4  u4ContextId);
PUBLIC tErpsContextInfo *
ErpsCxtGetContextEntry (UINT4 u4ContextId);
PUBLIC INT4
ErpsCxtEnableAllRings (UINT4 u4ContextId);
PUBLIC VOID
ErpsCxtClearAllRingStatsCounters (UINT4 u4ContextId);

PUBLIC INT4
ErpsCxtDisableAllRings (UINT4 u4ContextId);
PUBLIC INT4
ErpsCxtHandleUpdateCxtName (UINT4 u4ContextId);

/*************************** erpstrap.c ***************************************/
VOID
ErpsTrapSendTrapNotifications (tErpsRingInfo * pRingInfo, UINT1 u1TrapEvent); 


/*************************** erpsport.c ***************************************/
PUBLIC INT4
ErpsPortEcfmInitiateExPdu (tEcfmMepInfoParams * pMepInfo, UINT1 u1TlvOffset,
                           UINT1 u1SubOpCode, UINT1 *pu1PduData,
                           UINT4 u4PduDataLen, tMacAddr TxDestMacAddr,
                           UINT1 u1OpCode);
PUBLIC INT4
ErpsPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId);
PUBLIC INT4
ErpsPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);
PUBLIC VOID
ErpsPortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                        UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                        UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName);
PUBLIC VOID
ErpsPortIssGetContextMacAddress (UINT4 u4ContextId);
PUBLIC VOID
ErpsPortNotifyProtocolShutStatus (UINT4 u4ContextId);
PUBLIC INT4
ErpsPortVlanDeleteFdbEntries (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1ProtectionType, 
                              UINT2 u2VlanGroupId, VOID *pContext,tErpsLspPwInfo *pErpsLspPwInfo);
PUBLIC INT4
ErpsPortVcmIsVcExist (UINT4 u4ContextId);
PUBLIC INT4
ErpsPortEcfmMepRegAndGetFltStat (tEcfmRegParams * pEcfmReg, 
                                        tEcfmEventNotification *pMepInfo);
PUBLIC INT4
ErpsPortEcfmMepDeRegister (tEcfmRegParams * pEcfmReg, 
                           tEcfmEventNotification *pMepInfo);

INT4
ErpsPortL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                                UINT1 *pu1OperStatus);

INT4
ErpsPortL2IwfSetInstPortState (UINT2 u2InstanceId, UINT4 u4IfIndex, 
                                               UINT1 u1PortState);
INT4
ErpsPortL2IwfSetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 u1ProtId,
                            UINT1 u1OwnerStatus);
INT4
ErpsPortIssGetAsyncMode (INT4 i4ModuleId);

INT4 
ErpsPortL2IwfIsRapsVlanPresent (UINT4 u4ContextId, UINT2 u2RapsVlanId, 
                                UINT2 u2MstInst); 

PUBLIC INT4
ErpsPortRmRegisterProtocols (tRmRegParams * pRmReg);
PUBLIC INT4
ErpsPortRmDeRegisterProtocols (VOID);
PUBLIC UINT4
ErpsPortRmGetNodeState (VOID);
PUBLIC UINT1
ErpsPortRmGetStandbyNodeCount (VOID);
PUBLIC INT4
ErpsPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt);
PUBLIC INT4
ErpsPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId);
PUBLIC INT4
ErpsPortRmReleaseMemoryForMsg (UINT1 *pu1Block);
PUBLIC INT4
ErpsPortRmSetBulkUpdatesStatus (UINT4 u4AppId);
PUBLIC INT4
ErpsPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck);

PUBLIC INT4
ErpsPortMplsApiHandleExtRequest (UINT4 u4MainReqType,
                                 tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo);

PUBLIC INT4
ErpsPortL2VpnApiGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex);

/*************************** erpsl2wr.c ***************************************/
PUBLIC UINT2
ErpsL2wrMiGetVlanInstMapping (UINT4 u4ContextId,
                                   tVlanId VlanId);
PUBLIC INT4
ErpsL2wrSetVlanInstMapping (UINT4 u4ContextId, 
                            tVlanId VlanId,
                            UINT2 u2VlanGroupId);
PUBLIC UINT2
ErpsL2wrMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2VlanGroupId,
                                 UINT1 *pu1VlanList);
PUBLIC INT4
ErpsL2wrGetInstanceInfo (UINT4 u4ContextId, UINT2 u2Instance,
                         tStpInstanceInfo * pStpInstInfo);
PUBLIC INT4
ErpsL2wrDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);
PUBLIC INT4
ErpsL2wrCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);

PUBLIC VOID
ErpsDsyncUpdateDERPSRingDynInfo (tErpsRingInfo * pRingInfo,
                                UINT1 u1DynInfoType);
        
PUBLIC VOID
ErpsDsyncDynSyncMsgHandler(tErpsRingInfo * pRingInfo, tErpsQMsg * pQMsg);
           
PUBLIC INT4
ErpsDsyncFormAndSendDERPSPdu (tErpsRingInfo * pRingInfo, tErpsDsyncInfo *pDsyncInfo);

PUBLIC INT4
ErpsTxAllocBuffAndFormRapsPdu (tErpsRapsTxInfo * pRaps, tErpsRingInfo * pRingInfo);
PUBLIC INT4
ErpsTxSendPduToCfm (tErpsRapsTxInfo * pRaps, VOID *pContext);

#ifdef L2RED_WANTED
/*************************** erpssync.c ***************************************/
PUBLIC VOID
ErpsSyncNpProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet);
/*************************** erpsred.c ***************************************/
PUBLIC INT4
ErpsRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
PUBLIC VOID
ErpsRedHandleRmEvents (tErpsQMsg * pMsg);
PUBLIC VOID 
ErpsRedHwAudCreateOrFlushBuffer (unNpSync *pNpSync, UINT4 u4NpApiId,
                                   UINT4 u4EventId);

#else
/*************************** erpsred.c ***************************************/
#define ErpsRedHandleRmEvents(pMsg)
#endif /* L2RED_WANTED */

PUBLIC VOID 
ErpsRedDynDataDescInit (VOID);
PUBLIC VOID 
ErpsRedDescrTblInit (VOID);
PUBLIC VOID
ErpsRedHwAudCrtNpSyncBufferTable (VOID);
PUBLIC VOID 
ErpsRedSyncRingDynInfo (VOID);
PUBLIC VOID
ErpsRedDbNodeInit (tErpsRingInfo *pRingInfo);
PUBLIC VOID
ErpsRedHwAuditIncBlkCounter (VOID);
PUBLIC VOID
ErpsRedHwAuditDecBlkCounter (VOID);
PUBLIC VOID 
ErpsRedSyncDeleteAllRingInCtxt (UINT4 u4ContextId);
PUBLIC INT4
ErpsRedInitGlobalInfo (VOID);
PUBLIC VOID
ErpsRedDeInitGlobalInfo (VOID);
PUBLIC VOID 
ErpsRedDbUtilAddTblNode (tDbTblDescriptor *pErpsDataDesc, 
                         tDbTblNode *pErpsDbNode);
PUBLIC VOID 
ErpsRedDbUtilDelTblNode (tDbTblDescriptor *pErpsDataDesc, 
                         tDbTblNode *pErpsDbNode);
PUBLIC VOID ErpsRedUpdateSyncTable (tErpsRingInfo *pRingInfo);
PUBLIC VOID ErpsRedDelRingDynInfo (tErpsRingInfo *pRingInfo);
PUBLIC VOID 
ErpsRedSyncModuleEnableStatus (UINT4 u4ContextId);
PUBLIC VOID 
ErpsRedSyncModuleDisableStatus (UINT4 u4ContextId);
/* HITLESS RESTART */
VOID 
ErpsRedHRProcStdyStPktReq  (VOID);
INT1
ErpsRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER *pu1LinBuf,
        UINT4 u4PktLen, UINT2 u2Port, UINT4 u4TimeOut);
INT1 
ErpsRedHRSendStdyStTailMsg (VOID);
UINT1
ErpsRedGetHRFlag (VOID);
INT4 ErpsUtilDelVlanGroupTable(UINT4 u4ContextId);

INT4
ErpsPortCfaGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                     UINT4 u4IfListArrayLen);
INT4
ErpsUtilGetOctetStrFromPortList (tPortListExt *pPortList,
                                tSNMP_OCTET_STRING_TYPE *pOctetStr);
#ifdef ERPS_ARRAY_TO_RBTREE_WANTED
/*RBTREE UTILITIES FOR SUBPORTLIST */
INT4
ErpsSubPortListTblCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
VOID
ErpsDelAllSubPortEntries (tRBTree SubPortListTable);
tSubPortEntry  *
ErpsSubPortListTblGetNextEntry (tRBTree SubPortListTable, UINT4 u4Index);
tSubPortEntry  *
ErpsGetSubPortListEntry (tRBTree SubPortListTable, UINT4 u4Index);
INT4
ErpsDeleteSubPortEntry (tRBTree SubPortListTable, tSubPortEntry * pSubPortEntry);
INT4
ErpsCreateSubPortListEntry (tRBTree SubPortListTable,UINT4 u4Index,
                            tSubPortEntry ** ppSubPortEntry);
VOID
ErpsIsMemberPort (tRBTree SubPortListTable, UINT4 u4Index, UINT2 u2Flag,UINT1 *pu1Result);
VOID
ErpsGetSubPortsFromDB (tRBTree SubPortListTable, UINT2 u2Flag, tErpsSubPortList SubPortList);
INT4
ErpsAddSingleSubPortInDB (tRBTree SubPortListTable, UINT4 u4Index,  UINT2 u2Flag);
VOID
ErpsResetAllSubPortInDB(tRBTree SubPortListTable, UINT2 u2Flag);
#endif
INT4
ErpsUtilGetMcLagStatus (VOID);
INT4
ErpsUtilCheckIfIcclInterface (UINT4 u4Index);
INT4
ErpsUtilCheckIfMcLagInterface (UINT4 u4Index);
#ifdef ICCH_WANTED
VOID ErpsTmrHandleFdbSyncTimerExp(VOID *pArg);
#endif
#endif
