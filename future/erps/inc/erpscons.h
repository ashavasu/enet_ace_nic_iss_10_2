/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpscons.h,v 1.43 2016/09/17 12:41:26 siva Exp $
 *
 * Description: This file contains the function prototypes and macros of 
 *        ERPS module*
 * *******************************************************************/
#ifndef __ERPSCONS_H__
#define __ERPSCONS_H__

/* Timer types used by ERPS module */
enum { 
    ERPS_HOLD_OFF_TMR,
    ERPS_GUARD_TMR,
    ERPS_WTR_TMR,
    ERPS_PERIODIC_TMR,
    ERPS_WTB_TMR,
    ERPS_TFOP_TMR,
    ERPS_FOP_PM_TMR,
#ifdef ICCH_WANTED
    ERPS_ICCH_FDB_SYNC_TMR,
#endif
    ERPS_MAX_TMR_TYPES
};

enum {
    ERPS_DELETE_CONTEXT_MSG,
    ERPS_RAPS_PDU_MSG,
    ERPS_CFM_SIGNAL_MSG,
    ERPS_UPDATE_CONTEXT_NAME,
    ERPS_RM_MSG,
    ERPS_NP_CALLBACK
};

enum {
    ERPS_RING_LAST_MSG_TYPE,
    ERPS_RING_TMR_STATUS,
    ERPS_RING_PRIO_REQUEST,
    ERPS_RING_PORT1_STATUS,
    ERPS_RING_PORT2_STATUS,
    ERPS_RING_PORT1_HW_STATUS,
    ERPS_RING_PORT2_HW_STATUS,
    ERPS_RING_STATE,
    ERPS_RING_NODE_STATUS,
    ERPS_RING_PORT1_RCVD_NODE_ID,
    ERPS_RING_PORT2_RCVD_NODE_ID,
    ERPS_RING_START_WTR_TMR,
    ERPS_RING_STOP_WTR_TMR,
    ERPS_RING_START_WTB_TMR,
    ERPS_RING_STOP_WTB_TMR,
    ERPS_RING_START_GUARD_TMR,
    ERPS_RING_STOP_GUARD_TMR,
    ERPS_RING_FLUSH_FDB,
    ERPS_RING_CMD_TYPE,
    ERPS_RING_CMD_INDEX,
    ERPS_RING_FOP_DEFECT,
    ERPS_RING_PORT1_BPR_PAIR_NODE_ID,
    ERPS_RING_PORT2_BPR_PAIR_NODE_ID
};

/* Used for calculate the Performance */
enum {
       ERPS_DEFECT_ENC_TIME, 
       ERPS_DEFECT_CLR_TIME,
       ERPS_DEFECT_ENC_SWITCH_CMD,
       ERPS_DEFECT_CLR_SWITCH_CMD,
       ERPS_RPL_PORT_STATUS_CHANGE_TIME,
       ERPS_RPL_NBR_PORT_STATUS_CHANGE_TIME,
       ERPS_RAPS_DEFECT_ENC_TIME,/* Updation of defect encountered time ,
                                  both Sec and NanoSec  for  performance measurement*/
       ERPS_RAPS_DEFECT_CLR_TIME /* Updation of defect clear  time ,
       ERPS_RPL_NBR_PORT_STATUS_CHANGE_TIME*/
};

#define ERPS_PORT_STATE(x)               (x & ERPS_PORT_BLOCKING) ? "Blocked" : "UnBlocked"


#define ERPS_TASK_NAME                   ((UINT1 *)"ERPT")
#define ERPS_RAPS_TX_TASK_NAME           ((UINT1 *)"RTXT")
#define ERPS_TASK_QUEUE_NAME             ((UINT1 *)"ERPQ")
#define ERPS_PROTO_SEM                   ((UINT1 *)"ERPS")
#define ERPS_RAPS_TX_DB_SEM              ((UINT1 *)"RTXS")

#define ERPS_MAX_Q_DEPTH                 (2 * ERPS_SYS_MAX_RING) 
#define ERPS_QMSG_MEMBLK_COUNT            ERPS_MAX_Q_DEPTH

/* Events of ERPS Main task */
#define ERPS_TMR_EXPIRY_EVENT             0x01 /* Bit 1 */
#define ERPS_QMSG_EVENT                   0x02 /* Bit 2 */
#define ERPS_ALL_EVENTS                   (ERPS_TMR_EXPIRY_EVENT | \
                                           ERPS_QMSG_EVENT)

#define ERPS_RED_MAX_NPSYNC_BUF_ENTRIES 32
#ifdef MBSM_WANTED
#define ERPS_MBSM_BLK      MBSM_MAX_LC_SLOTS
#else
#define ERPS_MBSM_BLK      1
#endif
/* Events of ERPS TX task */
#define ERPS_RAPS_TX_EVENT                0x01

/****** Port status flags ***************
 *     Bit 0: failed state, 
 *     Bit 1: blocked state 
****************************************/
#define ERPS_FOP_DEFECT                   0x01

#define ERPS_PORT_FAILED                  0x01
#define ERPS_PORT_BLOCKING                0x02
#define ERPS_PORT_UNBLOCKING              0x00

#define ERPS_SNMP_PORT_BLOCK              1
#define ERPS_SNMP_PORT_UNBLOCK            2

#define ERPS_RING_PDU_SEND                1
#define ERPS_RING_PDU_DONT_SEND           2

/* ERPS Context Info */
#define ERPS_DEFAULT_CONTEXT_ID          L2IWF_DEFAULT_CONTEXT
#define ERPS_SIZING_CONTEXT_COUNT   FsERPSSizingParams[MAX_ERPS_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits
#define ERPS_INVALID_CONTEXT_ID (ERPS_SIZING_CONTEXT_COUNT + 1)

#define ERPS_MAX_SUB_RINGS               (ERPS_SIZING_MAX_RING -1)


#define ERPS_MONITOR_MECH_CFM              1


#define ERPS_VERSION2_VALUE                (UINT1)0x01
#define ERPS_STATUS_BIT_NONE               (UINT1)0x00
#define ERPS_RB_BIT_SET                    (UINT1)0x80
#define ERPS_DNF_BIT_SET                   (UINT1)0x40

#define ERPS_MAX_SIMULTANEOUS_CFM_SIGNAL   1024 /* Maximum number of CFM signals
                                                   handled simultaneously */

#define ERPS_RAPS_PDU_PADDING              3
#define ERPS_SHIFT_8BITS                   8
#define ERPS_BPR_BIT_SET                   (UINT1)0x20

#define ERPS_SNMP_TRUE                     1
#define ERPS_SNMP_FALSE                    2

#define ERPS_MAX_MEP_ID                ECFM_MEPID_MAX
/* R-APS Message type */
/*[Reference - Section :10.3 of ITU-T standard G.8032/Y.1344 (03/2010) standard] */
#define ERPS_MESSAGE_TYPE_FS             0XD
#define ERPS_MESSAGE_TYPE_SF             0xB
#define ERPS_MESSAGE_TYPE_MS             0X7
#define ERPS_MESSAGE_TYPE_NR             0X0
#define ERPS_MESSAGE_TYPE_EVENT          0xE


#define ERPS_HOLD_OFF_TMR_DEF_VAL          0 
#define ERPS_GUARD_TMR_DEF_VAL             500  
#define ERPS_WTR_TMR_DEF_VAL               300000
#define ERPS_PERIODIC_TMR_DEF_VAL          5000
#define ERPS_VC_REC_PERIODIC_TMR_DEF_VAL   5560
#define ERPS_WTB_TMR_DEF_VAL               5500 
#define ERPS_CLEAR_DEF_VAL                 1 
#define ERPS_HOLD_OFF_TMR_MAX_VAL          3600000 
#define ERPS_GUARD_TMR_MAX_VAL             3600000  
#define ERPS_WTR_TMR_MAX_VAL               86400000
#define ERPS_PERIODIC_TMR_MAX_VAL          3600000
#define ERPS_WTB_TMR_MAX_VAL               86400000
#define ERPS_INIT_VAL                      0
#define ERPS_FLT_VAL_10                    10.0 
#define ERPS_AVLBLTY_KVALUE_LENGTH         4
#define ERPS_KVALUE_DEF_VAL                3.5
#define ERPS_FOP_TO_DEFECT                 1
#define ERPS_FOP_PM_DEFECT                 1
#define ERPS_FOP_PM_CLEAR                  3.5

#define ERPS_ICCH_FDB_SYNC_TMR_DEF_VAL     5000




#define ERPS_CFM_SIGNAL_OK                 1
#define ERPS_CFM_SIGNAL_FAIL               2

#define ERPS_FLUSH_INTERVAL_TIME           1

/* First three R-APS PDU's must be transmitted within 3.3 milli-seconds */
#define ERPS_FAST_TRANSMISSION_INTERVAL    3.3  
#define ERPS_PERIODIC_TIMER_INTERVAL(pRingInfo)\
                                            pRingInfo->u4PeriodicTimerValue +\
                                    2 * (UINT4) ERPS_FAST_TRANSMISSION_INTERVAL         
#define ERPS_VC_RECOVERY_PERIODIC_TIMER_INTERVAL(pRingInfo)\
                                    pRingInfo->u4VcRecoveryPeriodicTime
/* 2 * periodic timer +  Guard timer of Main Ring+ (3 * UP MEP interval ) + 2 * Guard timer of UP MEP recovered nodes  + WTR timer of sub-ring*/

#define ERPS_MAX_RAPS_PDU_TRANSMISSION     2

#define ERPS_CMP_GREATER                   1
#define ERPS_CMP_LESS                     -1
#define ERPS_CMP_EQUAL                     0

#define ERPS_START                         1
#define ERPS_SHUTDOWN                      2

#define ERPS_OPTIMIZE                      1
#define ERPS_NO_OPTIMIZE                   2


/* ERPS Statistics types */
#define ERPS_STATS_RAPS_SENT               1
#define ERPS_STATS_RAPS_RCVD               2
#define ERPS_STATS_RAPS_DISC               3
#define ERPS_STATS_PORT_BLOCK              4
#define ERPS_STATS_PORT_UNBLOCK            5
#define ERPS_STATS_PORT_FAIL               6
#define ERPS_STATS_PORT_RECOVER            7
#define ERPS_STATS_VERSION_DISC            8
#define ERPS_STATS_FS_RCVD                 9
#define ERPS_STATS_FS_SENT                 10
#define ERPS_STATS_MS_RCVD                 11
#define ERPS_STATS_MS_SENT                 12
#define ERPS_STATS_EVENT_RCVD              13
#define ERPS_STATS_EVENT_SENT              14
#define ERPS_STATS_SF_RCVD                 15
#define ERPS_STATS_SF_SENT                 16
#define ERPS_STATS_NR_RCVD                 17
#define ERPS_STATS_NR_SENT                 18
#define ERPS_STATS_NRRB_RCVD               19
#define ERPS_STATS_NRRB_SENT               20
/* R-APS PDU Information */
#define ERPS_RAPS_PDU_VERSION              0
#define ERPS_RAPS_PDU_FLAGS                0
#define ERPS_RAPS_PDU_OPCODE               0x28
#define ERPS_RAPS_PDU_TLV_OFFSET           32
#define ERPS_RAPS_PDU_END_TLV              0
#define ERPS_RAPS_PDU_HDR_LEN              4
#define ERPS_RAPS_PDU_RAPS_INFO_LEN        8
#define ERPS_RAPS_PDU_LEN                  40 /* MEL, Version(1) + Opcode(1) +
                                               Flags(1) + TLV Offset(1) + 
                                               RAPS Info (8) + Reserved(24) +
                                               End TLV(1) + Padding (3) */
#define ERPS_VERSION_FIELD                 0x1F /* Version - 5 LSB bits of first byte of R-APS PDU */
#define ERPS_RAPS_V1_PDU                   0
#define ERPS_RAPS_V2_PDU                   0x01
#define ERPS_REQ_FIELD                     0xF0
#define ERPS_SUB_CODE_FLUSH_REQ            0x0

#define ERPS_RES1_FIELD                    0x0F
#define ERPS_RB_FIELD                      0x80
#define ERPS_DNF_FIELD                     0x40

#define ERPS_V1_STATUS_RES_FIELD           0x3F
#define ERPS_V2_STATUS_RES_FIELD           0x1F

#define ERPS_NODE_ID_FIELD                 2

#define ERPS_REQ_FIELD_OFFSET              4

/* Trap Types */
#define ERPS_TRAP_STATE_CHANGE             1
#define ERPS_TRAP_FAILURE                  2

#define ERPS_PORTS_PER_BYTE                8
#define ERPS_VAL_30      30
#define ERPS_VAL_100      100

#define ERPS_NANO_SECONDS                  1000000000

/* Trap Events */
enum {
     ERPS_TRAP_AUTO_SWITCH = 1,
     ERPS_TRAP_FORCE_SWITCH,
     ERPS_TRAP_MANUAL_SWITCH,
     ERPS_TRAP_RAPS_TX_FAIL,
     ERPS_TRAP_HW_CONFIG_FAIL,
     ERPS_TRAP_BUF_ALLOC_FAIL,
     ERPS_TRAP_TIMER_FAIL,
     ERPS_VERSION_MISMATCH,
     ERPS_TRAP_PROV_MISMATCH,
     ERPS_TRAP_PROV_MISMATCH_CLEAR,
     ERPS_TRAP_FORCE_SWITCH_PRESENT,
     ERPS_TRAP_TFOP_DEFECT,
     ERPS_TRAP_TFOP_DEFECT_CLEAR
};

/* Ring port selection macros for RAPS Packet transmission 
 * Added for Flush optimization to implement packet transmission
 * ITU-T G.8032/Y.1344 (03/2010) standard. */
enum {
    ERPS_TX_RAPS_ON_BOTH_RING_PORTS = 0, /* Tx on Both ring ports */
    ERPS_TX_RAPS_ON_RING_PORT_1,         /* Tx on Port 1 */
    ERPS_TX_RAPS_ON_RING_PORT_2,         /* Tx on Port 1 */
};


/* To identify port on which R-APS PDU is received. Added for
 * Flush Logic Feature implementation as per Section 10.1.10
 * of ITU-T G.8032/Y.1344 (03/2010) standard. */
enum {
    ERPS_RAPS_PDU_RCVD_RING_PORT_1 = 1,
    ERPS_RAPS_PDU_RCVD_RING_PORT_2,
};


/* Return Values of utility function ErpsUtilCompareNodeIdBprPair
 * Used to compare two (Node ID, BPR) pairs. Added for 
 * Flush Logic Feature implementation as per Section 10.1.10
 * of ITU-T G.8032/Y.1344 (03/2010) standard. */
enum {
    ERPS_NODE_ID_BPR_PAIRS_MATCH,
    ERPS_NODE_ID_BPR_PAIRS_DIFFER,
};


#define ERPS_OBJECT_NAME_LEN               256
#define ERPS_OBJECT_VALUE_LEN              128
#define ERPS_SNMPV2_TRAP_OID_LEN           10

#define ERPS_MAX_TRAP_STR_LEN              256

#define ERPS_MAX_TRC_TYPE                  9
#define ERPS_TRC_MIN_SIZE                  1
#define ERPS_TRC_MAX_SIZE                  255

#define ERPS_CENTI_TO_MILLI_SECOND         10

#define ERPS_INVALID_RING_MACID        0
#define ERPS_MIN_RING_MACID            1
#define ERPS_MAX_RING_MACID            255

#define ERPS_DEFAULT_VLAN_GROUP_ID         0

/* HITLESS RESTART */

#define ERPS_MILLI_SEC_TO_SEC_UNIT      1000

#define ERPS_TMR_RUNNING                0x1
#define ERPS_TMR_EXPIRED                0x2

/* When Ring Information (tErpsRingInfo) is updated by SEM events the following is done : 
 *  1. The bitmask ( u2BitMask ) is updated in ( tErpsDsyncInfo ) structure. 
 *  2. This bitmask is stored in the D-ERPS PDU and send to the remote line card.
 *  3. On reception of D-ERPS sync PDU (explained in erpsdd.doc Appendix-D, section 7.2.1),
 *     the bitmask present in the PDU is examined by masking the below defined Macros.
 *  4. Based on the value obtained from the bitmask, the corresponding Ring Info is updated.
 *     in the remote line card */

/* Example: If u1HighestPriorityRequest is updated in ( tErpsDsyncInfo ) structure,
 *  1. The bitmask ( u2BitMask ) is updated by the below macro which is defined for HighPriorityRequest.
 *  2. D-ERPS PDU is contructed with the information present in tErpsDsyncInfo structure and sent
 *     to the remote ERP.
 *  3. On receiving side, the remote ERP will examin the bitmask value present in the D-ERPS PDU by masking
 *     the same macro. If it is set, remote ERP will read the value of u1HighestPriorityRequest
 *     in DERPS PDU and update the value in Ring Information ( tErpsRingInfo ) structure. */

#define ERPS_DSYNC_LOCAL_PORT_STATE        (UINT2)0x0001
                /* This marco is used to update the bitmask, to notify the change in the local Ring port
   * status to remote ERP. */
#define ERPS_DSYNC_REMOTE_PORT_STATE       (UINT2)0x0002
                /* This marco is used to update the bitmask to notify the change in the  Remote Ring port
   * status to remote ERP. */
#define ERPS_DSYNC_RING_STATE              (UINT2)0x0004
                /* This marco is used to update the bitmask to notify the SemState change
   * to remote ERP. */
#define ERPS_DSYNC_HIGHEST_PRIO_REQ        (UINT2)0x0008
                /* This marco is used to update the bitmask to notify current HighPriority
   * request to remote ERP. */
#define ERPS_DSYNC_TIMER_INFO              (UINT2)0x0010
                /* This marco is used to update the bitmask to notify the change in Timer info
   * to remote ERP. */
#define ERPS_DSYNC_LAST_MSG_SEND           (UINT2)0x0020
                /* This marco is used to update the bitmask to notify the change in LastMsgSend
   * to remote ERP. */
#define ERPS_DSYNC_TRIGGER_FLUSH           (UINT2)0x0040
                /* This marco is used to update the bitmask to notify the Flush request
   * to remote ERP. */
#define ERPS_DSYNC_RING_NODE_STATUS        (UINT2)0x0080
                /* This marco is used to update the bitmask to notify the New Ring node
   * status to remote ERP. */
#define ERPS_DSYNC_NODE_ID_BPR_PAIR        (UINT2)0x0100
                /* This marco is used to update the bitmask to notify the change in BPR Pair
   * to remote ERP. */
#define ERPS_DSYNC_RAPS_TX_REQUEST           (UINT2)0x0200
                /* This marco is used to update the bitmask to notify the PDU Tx request
   * to remote ERP. */
#define ERPS_DSYNC_LAST_MSG                (UINT2)0x0400

/*When Timer is started or stopped by SEM events the following is done:
 *  1. The bitmask ( u2BitMask ) is updated in ( tErpsDsyncInfo ) structure to notify the timer event.
 *  2. Timer info field ( u1TimerInfo ) is updated in ( tErpsDsyncInfo ) structure by using the below
 *     macros.
 *  3. On receiving side, the remote ERP will examin the TimerInfo value present in the D-ERPS PDU 
 *     by masking the below macros. If it is set, remote ERP will start/stop the particular Timer.
 */

/* Example: If WTR timer is started in Local ERP,
 *  1. Bitmask ( u2BitMask ) is updated by the macro which is defined for TimerInfo ( ERPS_DSYNC_TIMER_INFO )
 *  2. TimerInfo ( u1TimerInfo ) is updated by using the macro ( ERPS_DSYNC_TIMER_INFO_START) to indicate
 *     a timer is started.
 *  3. TimerInfo ( u1TimerInfo ) is updated by using the macro defined for WTR timer ( ERPS_DSYNC_TIMER_INFO_START ).
 *  4. D-ERPS PDU is contructed with the information present in tErpsDsyncInfo structure and sent
 *     to the remote ERP.
 *  5. On receiving side, the remote ERP will examin the bitmask value present in the D-ERPS PDU by masking
 *     the same macro. If it is set, remote ERP will read and examin the value of u1TimerInfo in DERPS PDU
 *     by masking the below macros. In this case, WTR and start bit will be set, So remote ERP will start WTR
 *     Timer in remote line card.
*/
#define ERPS_DSYNC_TIMER_INFO_START (UINT1)0x01
                /* This marco is used to update timerInfo to notify remote ERP to start timers. */
#define ERPS_DSYNC_TIMER_INFO_WTR (UINT1)0x02
                /* This marco is used to update timerInfo filed to notify remote to start/stop 
   * WTR Timer. */
#define ERPS_DSYNC_TIMER_INFO_WTB (UINT1)0x04
                /* This marco is used to update timerInfo filed to notify remote to start/stop 
   * WTB Timer. */
#define ERPS_DSYNC_TIMER_INFO_GUARD (UINT1)0x08
                /* This marco is used to update timerInfo filed to notify remote to start/stop 
   * Guard Timer. */

/* AND Mask's for Extracting Dynamic sync info from Received D-ERPS PDU */
#define ERPS_LOCAL_PORT_LAST_RCVD_BPR_BIT     (UINT1) 0x01
                /* To extract Local Port Last Received BPR info */
#define ERPS_REMOTE_PORT_STATE_CHANGE_IND     (UINT1) 0x06
                /* To extract remote h/w port state change indication info */
#define ERPS_LOCAL_PORT_STATE_UPDATE_NOTIFY   (UINT1) 0x18
                /* To extract Local Port state change notification info */
#define ERPS_REMOTE_ERP_RAPS_TX               (UINT1) 0x20
                /* To extract Tx Start or Stop bit */
#define ERPS_REMOTE_ERP_TRIGGER_FDB_FLUSH     (UINT1) 0x40
                /* To extract FDB Flush at Remote ERP info */
#define ERPS_REMOTE_ERP_RING_STATE            (UINT1) 0x07
                /* To extract Next Remote ERP Ring state info */
#define ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ      (UINT1) 0xF8
                /* To extract Next Remote ERP Highest priority request */

/* Offset used to extract the Info in bits from D-ERPS PDU */
#define ERPS_REMOTE_PORT_STATE_CHANGE_OFFSET        0x01
#define ERPS_LOCAL_PORT_STATE_UPDATE_OFFSET       0x03
#define ERPS_REMOTE_ERP_RAPS_TX_OFFSET             0x05
#define ERPS_REMOTE_ERP_TRIGGER_FLUSH_OFFSET      0x06
#define ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ_OFFSET   0x03

#define ERPS_DERPS_BIT_SET                 (UINT1)0x01
#define ERPS_DERPS_DATA_LENGTH        (UINT1)0x18
#define ERPS_RAPS_DSYNC_INFO_LEN           24  /* Length of D-ERPS Information */
#define ERPS_RAPS_DSYNC_INFO_OFFSET        (ERPS_RAPS_PDU_HDR_LEN + ERPS_RAPS_PDU_RAPS_INFO_LEN)
#define ERPS_SUBPORTS_PLUS_RINGPORTS ((ERPS_MAX_SIZING_SUB_PORTS) + 2)

#define ERPS_RBTREE_KEY_LESSER         -1 
#define ERPS_RBTREE_KEY_GREATER         1
#define ERPS_RBTREE_KEY_EQUAL           0

#define ERPS_SUBPORT_PROPERTY_PORT1     1
#define ERPS_SUBPORT_PROPERTY_PORT2     2
#endif /*end of __ERPSCONS_H__*/
