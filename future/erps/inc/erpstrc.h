/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpstrc.h,v 1.6 2017/09/22 12:34:48 siva Exp $
 *
 * Description:This file contains procedures and definitions
 * used for debugging.
 ********************************************************************/

#ifndef __ERPSTRC_H__
#define __ERPSTRC_H__

#define  ERPS_MODULE_TRACE(u4ContextId)     \
                  gErpsGlobalInfo.apRingContextInfo[u4ContextId]->u4TraceOption

extern UINT4 gu4ErpsTrcLvl; 
#define ERPS_TRC_LVL        gu4ErpsTrcLvl

#ifdef TRACE_WANTED

#define ERPS_CRITICAL_TRC        0x00000100
#define ERPS_MODULE_NAME        ((const char *)"[ERPS]")

#define ERPS_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | DATA_PATH_TRC |\
                      CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                      ALL_FAILURE_TRC | BUFFER_TRC | ERPS_CRITICAL_TRC)

#define  ERPS_CONTEXT_TRC ErpsUtilContextTrc 
#define  ERPS_CONTEXT_INIT_TRC ErpsUtilContextInitTrc

#define  ERPS_GLOBAL_TRC(args) \
     UtlTrcLog (ERPS_CRITICAL_TRC, ERPS_CRITICAL_TRC, ERPS_MODULE_NAME, args)
#define ERPS_PKT_DUMP(CtxId, TraceType, pBuf, Length, args)                           \
    MOD_PKT_DUMP(ERPS_MODULE_TRACE(CtxId), TraceType, ERPS_MODULE_NAME, pBuf, Length,\
     args)

#else

#define  ERPS_CONTEXT_TRC(CtxId, RingId, Mask, ...) \
         {\
             UNUSED_PARAM(CtxId); \
             UNUSED_PARAM(RingId); \
         }
#define  ERPS_GLOBAL_TRC(args) do{ }while(0);
#define  ERPS_CONTEXT_INIT_TRC(CtxId,Mask, ...) do{ }while(0);
#define ERPS_CRITICAL_TRC        0
#define ERPS_ALL_TRC 0
#define ERPS_PKT_DUMP(CtxId, TraceType, pBuf, Length, args)
#endif /*TRACE_WANTED*/

#endif /*__ERPSTRC_H__*/
