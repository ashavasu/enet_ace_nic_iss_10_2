/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fserpsdb.h,v 1.28 2016/01/11 13:18:56 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSERPSDB_H
#define _FSERPSDB_H

UINT1 FsErpsContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsVlanGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsErpsRingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingCfmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingTcPropTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingConfigExtTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingMplsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsErpsRingStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fserps [] ={1,3,6,1,4,1,29601,2,40};
tSNMP_OID_TYPE fserpsOID = {9, fserps};


UINT4 FsErpsContextId [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,1};
UINT4 FsErpsCtxtName [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,2};
UINT4 FsErpsCtxtSystemControl [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,3};
UINT4 FsErpsCtxtModuleStatus [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,4};
UINT4 FsErpsCtxtTraceInput [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,5};
UINT4 FsErpsCtxtTrapStatus [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,6};
UINT4 FsErpsCtxtClearRingStats [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,7};
UINT4 FsErpsCtxtRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,8};
UINT4 FsErpsCtxtVlanGroupManager [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,9};
UINT4 FsErpsCtxtProprietaryClearFS [ ] ={1,3,6,1,4,1,29601,2,40,1,1,1,10};
UINT4 FsErpsVlanId [ ] ={1,3,6,1,4,1,29601,2,40,1,2,1,1};
UINT4 FsErpsVlanGroupId [ ] ={1,3,6,1,4,1,29601,2,40,1,2,1,2};
UINT4 FsErpsVlanGroupRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,1,2,1,3};
UINT4 FsErpsRingId [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,1};
UINT4 FsErpsRingVlanId [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,2};
UINT4 FsErpsRingName [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,3};
UINT4 FsErpsRingPort1 [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,4};
UINT4 FsErpsRingPort2 [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,5};
UINT4 FsErpsRingRplPort [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,6};
UINT4 FsErpsRingPortBlockingOnVcRecovery [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,7};
UINT4 FsErpsRingNodeType [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,8};
UINT4 FsErpsRingOperatingMode [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,9};
UINT4 FsErpsRingMonitorMechanism [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,10};
UINT4 FsErpsRingPort1Status [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,11};
UINT4 FsErpsRingPort2Status [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,12};
UINT4 FsErpsRingSemState [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,13};
UINT4 FsErpsRingNodeStatus [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,14};
UINT4 FsErpsRingRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,15};
UINT4 FsErpsRingMacId [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,16};
UINT4 FsErpsRingProtectedVlanGroupId [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,17};
UINT4 FsErpsRingProtectionType [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,18};
UINT4 FsErpsRingRAPSCompatibleVersion [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,19};
UINT4 FsErpsRingRplNeighbourPort [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,20};
UINT4 FsErpsRingRAPSSubRingWithoutVC [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,21};
UINT4 FsErpsRingRplNextNeighbourPort [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,22};
UINT4 FsErpsRingPort1NodeID [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,23};
UINT4 FsErpsRingPort2NodeID [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,24};
UINT4 FsErpsRingPort1BPRBitVal [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,25};
UINT4 FsErpsRingPort2BPRBitVal [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,26};
UINT4 FsErpsRingProtectedVlanGroupList [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,27};
UINT4 FsErpsRingServiceType [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,28};
UINT4 FsErpsRingPort1SubPortList [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,29};
UINT4 FsErpsRingPort2SubPortList [ ] ={1,3,6,1,4,1,29601,2,40,2,1,1,30};
UINT4 FsErpsRingMEG1 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,1};
UINT4 FsErpsRingCfmME1 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,2};
UINT4 FsErpsRingCfmMEP1 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,3};
UINT4 FsErpsRingMEG2 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,4};
UINT4 FsErpsRingCfmME2 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,5};
UINT4 FsErpsRingCfmMEP2 [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,6};
UINT4 FsErpsRingCfmRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,2,2,1,7};
UINT4 FsErpsRingConfigHoldOffTime [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,1};
UINT4 FsErpsRingConfigGuardTime [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,2};
UINT4 FsErpsRingConfigWTRTime [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,3};
UINT4 FsErpsRingConfigPeriodicTime [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,4};
UINT4 FsErpsRingConfigSwitchPort [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,5};
UINT4 FsErpsRingConfigSwitchCmd [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,6};
UINT4 FsErpsRingConfigRecoveryMethod [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,7};
UINT4 FsErpsRingConfigPropagateTC [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,8};
UINT4 FsErpsRingConfigWTBTime [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,9};
UINT4 FsErpsRingConfigClear [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,10};
UINT4 FsErpsRingConfigInterConnNode [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,11};
UINT4 FsErpsRingConfigMultipleFailure [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,12};
UINT4 FsErpsRingConfigIsPort1Present [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,13};
UINT4 FsErpsRingConfigIsPort2Present [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,14};
UINT4 FsErpsRingConfigInfoDistributingPort [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,15};
UINT4 FsErpsRingConfigKValue [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,16};
UINT4 FsErpsRingConfigFailureOfProtocol [ ] ={1,3,6,1,4,1,29601,2,40,2,3,1,17};
UINT4 FsErpsRingTcPropRingId [ ] ={1,3,6,1,4,1,29601,2,40,2,4,1,1};
UINT4 FsErpsRingTcPropRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,2,4,1,2};
UINT4 FsErpsRingConfigExtVCRecoveryPeriodicTime [ ] ={1,3,6,1,4,1,29601,2,40,2,5,1,1};
UINT4 FsErpsRingConfigExtMainRingId [ ] ={1,3,6,1,4,1,29601,2,40,2,5,1,2};
UINT4 FsErpsRingPwVcId1 [ ] ={1,3,6,1,4,1,29601,2,40,2,6,1,1};
UINT4 FsErpsRingPwVcId2 [ ] ={1,3,6,1,4,1,29601,2,40,2,6,1,2};
UINT4 FsErpsRingMplsRowStatus [ ] ={1,3,6,1,4,1,29601,2,40,2,6,1,3};
UINT4 FsErpsMemFailCount [ ] ={1,3,6,1,4,1,29601,2,40,3,1};
UINT4 FsErpsBufFailCount [ ] ={1,3,6,1,4,1,29601,2,40,3,2};
UINT4 FsErpsTimerFailCount [ ] ={1,3,6,1,4,1,29601,2,40,3,3};
UINT4 FsErpsRingClearRingStats [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,1};
UINT4 FsErpsRingPort1RapsPduSentCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,2};
UINT4 FsErpsRingPort2RapsPduSentCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,3};
UINT4 FsErpsRingPort1RapsPduRcvdCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,4};
UINT4 FsErpsRingPort2RapsPduRcvdCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,5};
UINT4 FsErpsRingPort1RapsPduDiscardCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,6};
UINT4 FsErpsRingPort2RapsPduDiscardCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,7};
UINT4 FsErpsRingPort1BlockedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,8};
UINT4 FsErpsRingPort2BlockedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,9};
UINT4 FsErpsRingPort1UnblockedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,10};
UINT4 FsErpsRingPort2UnblockedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,11};
UINT4 FsErpsRingPort1FailedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,12};
UINT4 FsErpsRingPort2FailedCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,13};
UINT4 FsErpsRingPort1RecoveredCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,14};
UINT4 FsErpsRingPort2RecoveredCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,15};
UINT4 FsErpsRingPort1VersionDiscardCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,16};
UINT4 FsErpsRingPort2VersionDiscardCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,17};
UINT4 FsErpsRingPort1RapsFSPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,18};
UINT4 FsErpsRingPort1RapsFSPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,19};
UINT4 FsErpsRingPort2RapsFSPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,20};
UINT4 FsErpsRingPort2RapsFSPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,21};
UINT4 FsErpsRingPort1RapsMSPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,22};
UINT4 FsErpsRingPort1RapsMSPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,23};
UINT4 FsErpsRingPort2RapsMSPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,24};
UINT4 FsErpsRingPort2RapsMSPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,25};
UINT4 FsErpsRingPort1RapsEventPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,26};
UINT4 FsErpsRingPort1RapsEventPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,27};
UINT4 FsErpsRingPort2RapsEventPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,28};
UINT4 FsErpsRingPort2RapsEventPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,29};
UINT4 FsErpsRingPort1RapsSFPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,30};
UINT4 FsErpsRingPort1RapsSFPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,31};
UINT4 FsErpsRingPort2RapsSFPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,32};
UINT4 FsErpsRingPort2RapsSFPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,33};
UINT4 FsErpsRingPort1RapsNRPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,34};
UINT4 FsErpsRingPort1RapsNRPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,35};
UINT4 FsErpsRingPort2RapsNRPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,36};
UINT4 FsErpsRingPort2RapsNRPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,37};
UINT4 FsErpsRingPort1RapsNRRBPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,38};
UINT4 FsErpsRingPort1RapsNRRBPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,39};
UINT4 FsErpsRingPort2RapsNRRBPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,40};
UINT4 FsErpsRingPort2RapsNRRBPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,41};
UINT4 FsErpsRingGeneratedTrapsCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,42};
UINT4 FsErpsRingPort1DefectEncTimeSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,43};
UINT4 FsErpsRingPort2DefectEncTimeSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,44};
UINT4 FsErpsRingPort1DefectClearedTimeSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,45};
UINT4 FsErpsRingPort2DefectClearedTimeSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,46};
UINT4 FsErpsRingRplPortStatChgTimeSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,47};
UINT4 FsErpsRingRplNbrPortStatChgTime [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,48};
UINT4 FsErpsRingDistPortPduRxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,49};
UINT4 FsErpsRingDistPortPduTxCount [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,50};
UINT4 FsErpsRingRapsPort1DefectEncTimeNSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,51};
UINT4 FsErpsRingRapsPort1DefectClearedTimeNSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,52};
UINT4 FsErpsRingRapsPort2DefectEncTimeNSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,53};
UINT4 FsErpsRingRapsPort2DefectClearedTimeNSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,54};
UINT4 FsErpsRingRapsRplPortStatChgTimeNSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,55};
UINT4 FsErpsRingDefectSwitchOverTimeMSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,56};
UINT4 FsErpsRingDefectClearedSwitchOverTimeMSec [ ] ={1,3,6,1,4,1,29601,2,40,3,4,1,57};
UINT4 FsErpsTypeOfFailure [ ] ={1,3,6,1,4,1,29601,2,40,4,1};
UINT4 FsErpsTrapSwitchingMechanism [ ] ={1,3,6,1,4,1,29601,2,40,4,2};




tMbDbEntry fserpsMibEntry[]= {

{{13,FsErpsContextId}, GetNextIndexFsErpsContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsErpsContextTableINDEX, 1, 0, 0, NULL},

{{13,FsErpsCtxtName}, GetNextIndexFsErpsContextTable, FsErpsCtxtNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsErpsContextTableINDEX, 1, 0, 0, NULL},

{{13,FsErpsCtxtSystemControl}, GetNextIndexFsErpsContextTable, FsErpsCtxtSystemControlGet, FsErpsCtxtSystemControlSet, FsErpsCtxtSystemControlTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsErpsCtxtModuleStatus}, GetNextIndexFsErpsContextTable, FsErpsCtxtModuleStatusGet, FsErpsCtxtModuleStatusSet, FsErpsCtxtModuleStatusTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsErpsCtxtTraceInput}, GetNextIndexFsErpsContextTable, FsErpsCtxtTraceInputGet, FsErpsCtxtTraceInputSet, FsErpsCtxtTraceInputTest, FsErpsContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "critical"},

{{13,FsErpsCtxtTrapStatus}, GetNextIndexFsErpsContextTable, FsErpsCtxtTrapStatusGet, FsErpsCtxtTrapStatusSet, FsErpsCtxtTrapStatusTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "1"},

{{13,FsErpsCtxtClearRingStats}, GetNextIndexFsErpsContextTable, FsErpsCtxtClearRingStatsGet, FsErpsCtxtClearRingStatsSet, FsErpsCtxtClearRingStatsTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsErpsCtxtRowStatus}, GetNextIndexFsErpsContextTable, FsErpsCtxtRowStatusGet, FsErpsCtxtRowStatusSet, FsErpsCtxtRowStatusTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 1, NULL},

{{13,FsErpsCtxtVlanGroupManager}, GetNextIndexFsErpsContextTable, FsErpsCtxtVlanGroupManagerGet, FsErpsCtxtVlanGroupManagerSet, FsErpsCtxtVlanGroupManagerTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "1"},

{{13,FsErpsCtxtProprietaryClearFS}, GetNextIndexFsErpsContextTable, FsErpsCtxtProprietaryClearFSGet, FsErpsCtxtProprietaryClearFSSet, FsErpsCtxtProprietaryClearFSTest, FsErpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsErpsVlanId}, GetNextIndexFsErpsVlanGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsErpsVlanGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsErpsVlanGroupId}, GetNextIndexFsErpsVlanGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsErpsVlanGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsErpsVlanGroupRowStatus}, GetNextIndexFsErpsVlanGroupTable, FsErpsVlanGroupRowStatusGet, FsErpsVlanGroupRowStatusSet, FsErpsVlanGroupRowStatusTest, FsErpsVlanGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsVlanGroupTableINDEX, 3, 0, 1, NULL},

{{13,FsErpsRingId}, GetNextIndexFsErpsRingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingVlanId}, GetNextIndexFsErpsRingTable, FsErpsRingVlanIdGet, FsErpsRingVlanIdSet, FsErpsRingVlanIdTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingName}, GetNextIndexFsErpsRingTable, FsErpsRingNameGet, FsErpsRingNameSet, FsErpsRingNameTest, FsErpsRingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1}, GetNextIndexFsErpsRingTable, FsErpsRingPort1Get, FsErpsRingPort1Set, FsErpsRingPort1Test, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2}, GetNextIndexFsErpsRingTable, FsErpsRingPort2Get, FsErpsRingPort2Set, FsErpsRingPort2Test, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRplPort}, GetNextIndexFsErpsRingTable, FsErpsRingRplPortGet, FsErpsRingRplPortSet, FsErpsRingRplPortTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingPortBlockingOnVcRecovery}, GetNextIndexFsErpsRingTable, FsErpsRingPortBlockingOnVcRecoveryGet, FsErpsRingPortBlockingOnVcRecoverySet, FsErpsRingPortBlockingOnVcRecoveryTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingNodeType}, GetNextIndexFsErpsRingTable, FsErpsRingNodeTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingOperatingMode}, GetNextIndexFsErpsRingTable, FsErpsRingOperatingModeGet, FsErpsRingOperatingModeSet, FsErpsRingOperatingModeTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingMonitorMechanism}, GetNextIndexFsErpsRingTable, FsErpsRingMonitorMechanismGet, FsErpsRingMonitorMechanismSet, FsErpsRingMonitorMechanismTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingPort1Status}, GetNextIndexFsErpsRingTable, FsErpsRingPort1StatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2Status}, GetNextIndexFsErpsRingTable, FsErpsRingPort2StatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingSemState}, GetNextIndexFsErpsRingTable, FsErpsRingSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingNodeStatus}, GetNextIndexFsErpsRingTable, FsErpsRingNodeStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRowStatus}, GetNextIndexFsErpsRingTable, FsErpsRingRowStatusGet, FsErpsRingRowStatusSet, FsErpsRingRowStatusTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 1, NULL},

{{13,FsErpsRingMacId}, GetNextIndexFsErpsRingTable, FsErpsRingMacIdGet, FsErpsRingMacIdSet, FsErpsRingMacIdTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingProtectedVlanGroupId}, GetNextIndexFsErpsRingTable, FsErpsRingProtectedVlanGroupIdGet, FsErpsRingProtectedVlanGroupIdSet, FsErpsRingProtectedVlanGroupIdTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingProtectionType}, GetNextIndexFsErpsRingTable, FsErpsRingProtectionTypeGet, FsErpsRingProtectionTypeSet, FsErpsRingProtectionTypeTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingRAPSCompatibleVersion}, GetNextIndexFsErpsRingTable, FsErpsRingRAPSCompatibleVersionGet, FsErpsRingRAPSCompatibleVersionSet, FsErpsRingRAPSCompatibleVersionTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingRplNeighbourPort}, GetNextIndexFsErpsRingTable, FsErpsRingRplNeighbourPortGet, FsErpsRingRplNeighbourPortSet, FsErpsRingRplNeighbourPortTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingRAPSSubRingWithoutVC}, GetNextIndexFsErpsRingTable, FsErpsRingRAPSSubRingWithoutVCGet, FsErpsRingRAPSSubRingWithoutVCSet, FsErpsRingRAPSSubRingWithoutVCTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingRplNextNeighbourPort}, GetNextIndexFsErpsRingTable, FsErpsRingRplNextNeighbourPortGet, FsErpsRingRplNextNeighbourPortSet, FsErpsRingRplNextNeighbourPortTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingPort1NodeID}, GetNextIndexFsErpsRingTable, FsErpsRingPort1NodeIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2NodeID}, GetNextIndexFsErpsRingTable, FsErpsRingPort2NodeIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1BPRBitVal}, GetNextIndexFsErpsRingTable, FsErpsRingPort1BPRBitValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2BPRBitVal}, GetNextIndexFsErpsRingTable, FsErpsRingPort2BPRBitValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingProtectedVlanGroupList}, GetNextIndexFsErpsRingTable, FsErpsRingProtectedVlanGroupListGet, FsErpsRingProtectedVlanGroupListSet, FsErpsRingProtectedVlanGroupListTest, FsErpsRingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingServiceType}, GetNextIndexFsErpsRingTable, FsErpsRingServiceTypeGet, FsErpsRingServiceTypeSet, FsErpsRingServiceTypeTest, FsErpsRingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingPort1SubPortList}, GetNextIndexFsErpsRingTable, FsErpsRingPort1SubPortListGet, FsErpsRingPort1SubPortListSet, FsErpsRingPort1SubPortListTest, FsErpsRingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingPort2SubPortList}, GetNextIndexFsErpsRingTable, FsErpsRingPort2SubPortListGet, FsErpsRingPort2SubPortListSet, FsErpsRingPort2SubPortListTest, FsErpsRingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsRingTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingMEG1}, GetNextIndexFsErpsRingCfmTable, FsErpsRingMEG1Get, FsErpsRingMEG1Set, FsErpsRingMEG1Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingCfmME1}, GetNextIndexFsErpsRingCfmTable, FsErpsRingCfmME1Get, FsErpsRingCfmME1Set, FsErpsRingCfmME1Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingCfmMEP1}, GetNextIndexFsErpsRingCfmTable, FsErpsRingCfmMEP1Get, FsErpsRingCfmMEP1Set, FsErpsRingCfmMEP1Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingMEG2}, GetNextIndexFsErpsRingCfmTable, FsErpsRingMEG2Get, FsErpsRingMEG2Set, FsErpsRingMEG2Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingCfmME2}, GetNextIndexFsErpsRingCfmTable, FsErpsRingCfmME2Get, FsErpsRingCfmME2Set, FsErpsRingCfmME2Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingCfmMEP2}, GetNextIndexFsErpsRingCfmTable, FsErpsRingCfmMEP2Get, FsErpsRingCfmMEP2Set, FsErpsRingCfmMEP2Test, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingCfmRowStatus}, GetNextIndexFsErpsRingCfmTable, FsErpsRingCfmRowStatusGet, FsErpsRingCfmRowStatusSet, FsErpsRingCfmRowStatusTest, FsErpsRingCfmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingCfmTableINDEX, 2, 0, 1, NULL},

{{13,FsErpsRingConfigHoldOffTime}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigHoldOffTimeGet, FsErpsRingConfigHoldOffTimeSet, FsErpsRingConfigHoldOffTimeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingConfigGuardTime}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigGuardTimeGet, FsErpsRingConfigGuardTimeSet, FsErpsRingConfigGuardTimeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "500"},

{{13,FsErpsRingConfigWTRTime}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigWTRTimeGet, FsErpsRingConfigWTRTimeSet, FsErpsRingConfigWTRTimeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "300000"},

{{13,FsErpsRingConfigPeriodicTime}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigPeriodicTimeGet, FsErpsRingConfigPeriodicTimeSet, FsErpsRingConfigPeriodicTimeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "5000"},

{{13,FsErpsRingConfigSwitchPort}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigSwitchPortGet, FsErpsRingConfigSwitchPortSet, FsErpsRingConfigSwitchPortTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingConfigSwitchCmd}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigSwitchCmdGet, FsErpsRingConfigSwitchCmdSet, FsErpsRingConfigSwitchCmdTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingConfigRecoveryMethod}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigRecoveryMethodGet, FsErpsRingConfigRecoveryMethodSet, FsErpsRingConfigRecoveryMethodTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingConfigPropagateTC}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigPropagateTCGet, FsErpsRingConfigPropagateTCSet, FsErpsRingConfigPropagateTCTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingConfigWTBTime}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigWTBTimeGet, FsErpsRingConfigWTBTimeSet, FsErpsRingConfigWTBTimeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "5500"},

{{13,FsErpsRingConfigClear}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigClearGet, FsErpsRingConfigClearSet, FsErpsRingConfigClearTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingConfigInterConnNode}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigInterConnNodeGet, FsErpsRingConfigInterConnNodeSet, FsErpsRingConfigInterConnNodeTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingConfigMultipleFailure}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigMultipleFailureGet, FsErpsRingConfigMultipleFailureSet, FsErpsRingConfigMultipleFailureTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingConfigIsPort1Present}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigIsPort1PresentGet, FsErpsRingConfigIsPort1PresentSet, FsErpsRingConfigIsPort1PresentTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingConfigIsPort2Present}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigIsPort2PresentGet, FsErpsRingConfigIsPort2PresentSet, FsErpsRingConfigIsPort2PresentTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsErpsRingConfigInfoDistributingPort}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigInfoDistributingPortGet, FsErpsRingConfigInfoDistributingPortSet, FsErpsRingConfigInfoDistributingPortTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsErpsRingConfigKValue}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigKValueGet, FsErpsRingConfigKValueSet, FsErpsRingConfigKValueTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "3.50"},

{{13,FsErpsRingConfigFailureOfProtocol}, GetNextIndexFsErpsRingConfigTable, FsErpsRingConfigFailureOfProtocolGet, FsErpsRingConfigFailureOfProtocolSet, FsErpsRingConfigFailureOfProtocolTest, FsErpsRingConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingConfigTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingTcPropRingId}, GetNextIndexFsErpsRingTcPropTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsErpsRingTcPropTableINDEX, 3, 0, 0, NULL},

{{13,FsErpsRingTcPropRowStatus}, GetNextIndexFsErpsRingTcPropTable, FsErpsRingTcPropRowStatusGet, FsErpsRingTcPropRowStatusSet, FsErpsRingTcPropRowStatusTest, FsErpsRingTcPropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingTcPropTableINDEX, 3, 0, 1, NULL},

{{13,FsErpsRingConfigExtVCRecoveryPeriodicTime}, GetNextIndexFsErpsRingConfigExtTable, FsErpsRingConfigExtVCRecoveryPeriodicTimeGet, FsErpsRingConfigExtVCRecoveryPeriodicTimeSet, FsErpsRingConfigExtVCRecoveryPeriodicTimeTest, FsErpsRingConfigExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigExtTableINDEX, 2, 0, 0, "5560"},

{{13,FsErpsRingConfigExtMainRingId}, GetNextIndexFsErpsRingConfigExtTable, FsErpsRingConfigExtMainRingIdGet, FsErpsRingConfigExtMainRingIdSet, FsErpsRingConfigExtMainRingIdTest, FsErpsRingConfigExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingConfigExtTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPwVcId1}, GetNextIndexFsErpsRingMplsTable, FsErpsRingPwVcId1Get, FsErpsRingPwVcId1Set, FsErpsRingPwVcId1Test, FsErpsRingMplsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingMplsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPwVcId2}, GetNextIndexFsErpsRingMplsTable, FsErpsRingPwVcId2Get, FsErpsRingPwVcId2Set, FsErpsRingPwVcId2Test, FsErpsRingMplsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsErpsRingMplsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingMplsRowStatus}, GetNextIndexFsErpsRingMplsTable, FsErpsRingMplsRowStatusGet, FsErpsRingMplsRowStatusSet, FsErpsRingMplsRowStatusTest, FsErpsRingMplsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingMplsTableINDEX, 2, 0, 1, NULL},

{{11,FsErpsMemFailCount}, NULL, FsErpsMemFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsErpsBufFailCount}, NULL, FsErpsBufFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsErpsTimerFailCount}, NULL, FsErpsTimerFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsErpsRingClearRingStats}, GetNextIndexFsErpsRingStatsTable, FsErpsRingClearRingStatsGet, FsErpsRingClearRingStatsSet, FsErpsRingClearRingStatsTest, FsErpsRingStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsErpsRingStatsTableINDEX, 2, 0, 0, "2"},

{{13,FsErpsRingPort1RapsPduSentCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsPduSentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsPduSentCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsPduSentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsPduRcvdCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsPduRcvdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsPduRcvdCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsPduRcvdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsPduDiscardCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsPduDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsPduDiscardCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsPduDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1BlockedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1BlockedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2BlockedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2BlockedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1UnblockedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1UnblockedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2UnblockedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2UnblockedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1FailedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1FailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2FailedCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2FailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RecoveredCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RecoveredCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RecoveredCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RecoveredCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1VersionDiscardCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1VersionDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2VersionDiscardCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2VersionDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsFSPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsFSPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsFSPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsFSPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsFSPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsFSPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsFSPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsFSPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsMSPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsMSPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsMSPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsMSPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsMSPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsMSPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsMSPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsMSPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsEventPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsEventPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsEventPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsEventPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsEventPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsEventPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsEventPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsEventPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsSFPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsSFPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsSFPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsSFPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsSFPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsSFPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsSFPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsSFPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsNRPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsNRPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsNRPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsNRPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsNRPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsNRPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsNRPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsNRPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsNRRBPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsNRRBPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1RapsNRRBPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1RapsNRRBPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsNRRBPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsNRRBPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2RapsNRRBPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2RapsNRRBPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingGeneratedTrapsCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingGeneratedTrapsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1DefectEncTimeSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1DefectEncTimeSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2DefectEncTimeSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2DefectEncTimeSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort1DefectClearedTimeSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort1DefectClearedTimeSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingPort2DefectClearedTimeSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingPort2DefectClearedTimeSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRplPortStatChgTimeSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRplPortStatChgTimeSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRplNbrPortStatChgTime}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRplNbrPortStatChgTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingDistPortPduRxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingDistPortPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingDistPortPduTxCount}, GetNextIndexFsErpsRingStatsTable, FsErpsRingDistPortPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRapsPort1DefectEncTimeNSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRapsPort1DefectEncTimeNSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRapsPort1DefectClearedTimeNSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRapsPort1DefectClearedTimeNSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRapsPort2DefectEncTimeNSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRapsPort2DefectEncTimeNSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRapsPort2DefectClearedTimeNSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRapsPort2DefectClearedTimeNSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingRapsRplPortStatChgTimeNSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingRapsRplPortStatChgTimeNSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingDefectSwitchOverTimeMSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingDefectSwitchOverTimeMSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsErpsRingDefectClearedSwitchOverTimeMSec}, GetNextIndexFsErpsRingStatsTable, FsErpsRingDefectClearedSwitchOverTimeMSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsErpsRingStatsTableINDEX, 2, 0, 0, NULL},

{{11,FsErpsTypeOfFailure}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsErpsTrapSwitchingMechanism}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};

tMibData fserpsEntry = { 136, fserpsMibEntry };
#endif /* _FSERPSDB_H */

