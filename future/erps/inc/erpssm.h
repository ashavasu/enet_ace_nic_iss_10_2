/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: erpssm.h,v 1.7 2011/04/28 11:39:56 siva Exp $
 *
 * Description: This file contains the definitions of ERPS state machines
                and the corresponding event handlers
 ***************************************************************************/

#ifndef __ERPSSM_H__
#define __ERPSSM_H__

/* State Machine handlers are assigned to less letter function pointers,
 * so that these can be easly accomodated in the state/event mapping. */

/* Version 1 Handlers */
INT4 (*INIT)(tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleInit;
INT4 (*DIS) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleDisable;
INT4 (*LSF) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleLocalSF;
INT4 (*CSF) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleLocalClearSF;
INT4 (*SF)  (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleSFMsg;
INT4 (*WTR) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleWtrExpiry;
INT4 (*NRRB)(tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleNRRBMsg;
INT4 (*NR)  (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingHandleNRMsg;
INT4 (*IMP) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingImpossibleEvent;
INT4 (*NO) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmRingNoAction;

/* Version 2 Handlers */
INT4 (*LFS) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleLocalFS;
INT4 (*FS) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleRAPSFS;
INT4 (*LMS) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleLocalMS;
INT4 (*MS) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleRAPSMS; 
INT4 (*CLR) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleClearCmd;
INT4 (*INITV2)(tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleInit;
INT4 (*LSFV2) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleLocalSF;
INT4 (*SFV2) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleRAPSSF;
INT4 (*NRRBV2) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleRAPSNRRB;
INT4 (*NR2) (tErpsRingInfo *pErpsRingEntry,
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleRAPSNR;
INT4 (*WTB) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleWtbExpiry;
INT4 (*LCSFV2) (tErpsRingInfo *pErpsRingEntry, 
             tErpsRcvdMsg *pErpsRcvdMsg)    = ErpsSmV2RingHandleLocalClearSF;


/* State Machine for ERPS Version 1 */

INT4 (**gpErpsStateMachine[ERPS_RING_MAX_RING_STATE] [ERPS_RING_MAX_RING_EVENT])
      (tErpsRingInfo *pErpsRingEntry, tErpsRcvdMsg *pErpsRcvdMsg)=

/*|DISABLE, INIT,  CLEAR,   LocalFS, RAPS-FS, LOCALSF, CLEARSF, R-APSSF, RAPS-MS, LocalMS, WTR,  WTB  NRRB,  NR,    | -- events */
 {{&IMP,    &INIT, &NO,     &NO,     &NO,     &IMP,    &IMP,    &IMP,    &NO,     &NO,     &IMP, &NO, &IMP,  &IMP}, /* |DISABLE  |  */
  {&DIS,    &IMP,  &NO,     &NO,     &NO,     &LSF,    &NO,     &SF,     &NO,     &NO,     &IMP, &NO, &NRRB, &NO},  /* |IDLE     |  */
  {&DIS,    &IMP,  &NO,     &NO,     &NO,     &LSF,    &CSF,    &SF,     &NO,     &NO,     &WTR, &NO, &NRRB, &NR}}; /* |PROTECTION| */


/* State Machine for ERPS Version 2 */
INT4 (**gpErpsV2StateMachine[ERPS_RING_MAX_RING_STATE] [ERPS_RING_MAX_RING_EVENT]) 
    (tErpsRingInfo *pErpsRingEntry, tErpsRcvdMsg *pErpsRcvdMsg)=

/*|DISABLE, INIT,    CLEAR, LocalFS, RAPS-FS,LOCALSF, CLEARSF, R-APSSF,RAPS-MS,LocalMS, WTR,    WTB     NRRB,     NR,    | -- events */
                                                                                                                        
 {{&IMP,    &INITV2, &IMP,  &IMP,    &IMP,  &IMP,    &IMP,    &IMP,   &IMP,  &IMP,    &IMP,   &IMP,  &IMP,     &IMP},   /* DISABLE       */
  {&DIS,    &IMP,    &NO,   &LFS,    &FS,   &LSFV2,  &NO,     &SFV2,  &MS,   &LMS,    &NO,    &NO,   &NRRBV2,  &NR2},   /* IDLE          */
  {&DIS,    &IMP,    &NO,   &LFS,    &FS,   &LSFV2,  &LCSFV2, &NO,    &NO,   &NO,     &NO,    &NO,   &NRRBV2,  &NR2},   /* PROTECTION    */
  {&DIS,    &IMP,    &CLR,  &LFS,    &FS,   &LSFV2,  &NO,     &SFV2,  &MS,   &NO,     &NO,    &NO,   &NRRBV2,  &NR2},   /* MANUAL SWITCH */
  {&DIS,    &IMP,    &CLR,  &LFS,    &NO,   &NO,     &NO,     &NO,    &NO,   &NO,     &NO,    &NO,   &NRRBV2,  &NR2},   /* FORCED SWITCH */
  {&DIS,    &IMP,    &CLR,  &LFS,    &FS,   &LSFV2,  &NO,     &SFV2,  &MS,   &LMS,    &WTR,   &WTB,  &NRRBV2,  &NR2}};  /* PENDING       */


INT1 gaai1RingSmState[ERPS_RING_MAX_RING_STATE][15] = {"DISABLED", 
                                                          "IDLE", 
                                                          "PROTECTION",
                                                          "MANUAL_SWITCH",
                                                          "FORCED-SWITCH",
                                                          "PENDING"
                                                          };

INT1 gaai1RingSmEvent[ERPS_RING_MAX_RING_EVENT][15] = {"DISABLE",
                                                          "INIT",
                                                          "CLEAR",
                                                          "LOCAL-FS",
                                                          "R-APS-FS",
                                                          "LOCAL-SF",
                                                          "CLEAR-SF",
                                                          "R-APS-SF",
                                                          "R-APS-MS",
                                                          "LOCAL-MS",
                                                          "WTR-EXP",
                                                          "WTB-EXP",
                                                          "R-APS-NRRB",
                                                          "R-APS-NR"
                                                          };
#endif
