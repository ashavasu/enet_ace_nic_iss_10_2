/* $Id: erpsmacs.h,v 1.12 2015/12/24 10:35:28 siva Exp $ */

#ifndef __ERPSMACS_H__
#define __ERPSMACS_H__

/* Macro defined for the buffer length of Port1/Port2 subPortList
 * Octet string. 
 */
#define MAX_ERPS_CLI_BUF_LEN 600

/* Macro for storing Syslog ID for ERPS */
#define ERPS_SYSLOG_ID         gErpsGlobalInfo.i4SysLogId


/* ERPS Module APS Tx Info Database Semaphore */
#define ERPS_TX_DB_LOCK()      ErpsUtilRapsTxDbLock ()
#define ERPS_TX_DB_UNLOCK()    ErpsUtilRapsTxDbUnLock ()

#define ERPS_CONTEXT_NAME(u4ContextId) \
        gErpsGlobalInfo.apRingContextInfo[u4ContextId]->au1ContextName
#define ERPS_NODE_ID(u4ContextId) \
        gErpsGlobalInfo.apRingContextInfo[u4ContextId]->NodeId
#define ERPS_TRAP_STATUS(u4ContextId) \
        gErpsGlobalInfo.apRingContextInfo[u4ContextId]->u1TrapStatus
#define ERPS_TRACE_OPTION(u4ContextId) \
        gErpsGlobalInfo.apRingContextInfo[u4ContextId]->u4TraceOption

#define ERPS_GET_TIME_STAMP(pu4StoreTime) \
        *pu4StoreTime = OsixGetSysUpTime () * SYS_TIME_TICKS_IN_A_SEC

#define ERPS_LBUF_PUT_1_BYTE(pBuf, u1Value, u2Offset) \
{\
    *pBuf = u1Value; \
    pBuf += 1;\
    u2Offset++;\
}

/* Encode Module Related Macros used to assign fields to buffer */
#define ERPS_PUT_1BYTE(pu1PktBuf,u1Val) \
{                            \
    *pu1PktBuf = u1Val;          \
    pu1PktBuf += 1;              \
}


#define ERPS_PUT_2BYTE(pu1PktBuf, u2Val)        \
        {                                    \
            u2Val = OSIX_HTONS(u2Val);                  \
            MEMCPY (pu1PktBuf, &u2Val, 2); \
            pu1PktBuf += 2;              \
        }

#define ERPS_PUT_4BYTE(pu1PktBuf, u4Val)       \
        {                                   \
           u4Val = OSIX_HTONL(u4Val);   \
           MEMCPY (pu1PktBuf, &u4Val, 4); \
           pu1PktBuf += 4;             \
        }

#define ERPS_PUT_MAC_ADDR(pu1PktBuf, Value)   \
        {                                      \
           MEMCPY (pu1PktBuf, Value, 6);     \
           pu1PktBuf += 6;                        \
        }

/************* OCTET_STRING conversion Macros ********************/

#define ERPS_OCTETSTRING_TO_FLOAT(pOctetString, f4Val) \
        SSCANF((CHR1 *)pOctetString->pu1_OctetList, "%f", &f4Val)
#define ERPS_FLOAT_TO_OCTETSTRING(f4Value, pOctetString) \
        SPRINTF((CHR1 *)pOctetString->pu1_OctetList, "%f", f4Value)


/* To convert timer duration to seconds and milliseconds*/
#define ERPS_CONVERT_TMR_DURATION_TO_SEC(u4Duration,u4Sec,u4MilliSec) \
    u4Sec = u4Duration / 1000; \
    u4MilliSec = u4Duration % 1000; 
/* HITLESS RESTART */    
#define ERPS_HR_STATUS()               ErpsRedGetHRFlag()
#define ERPS_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define ERPS_HR_STDY_ST_REQ_RCVD()     \
    gErpsGlobalInfo.ErpsRedGlobalInfo.u1StdyStReqRcvd

#define ERPS_SUBPORT_LIST_POOLID()      gErpsGlobalInfo.ErpsUtlSubPortListPoolId

#define ERPS_ALLOC_SUBPORT_LIST_MEM_BLOCK(pNode) \
           (pNode = (UINT1 *) MemAllocMemBlk(ERPS_SUBPORT_LIST_POOLID()))

#define ERPS_RELEASE_SUBPORT_LIST_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(ERPS_SUBPORT_LIST_POOLID(), (UINT1 *)pNode)

#define MAX_ERPS_SUB_PORT_ENTRIES_POOLID()      gErpsGlobalInfo.ErpsSubPortEntryPoolId


#define ERPS_ALLOC_SUB_PORT_ENTRIES_MEM_BLOCK(pNode) \
           (pNode = (tSubPortEntry *) MemAllocMemBlk(MAX_ERPS_SUB_PORT_ENTRIES_POOLID()))

#define ERPS_RELEASE_SUB_PORT_ENTRIES_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(MAX_ERPS_SUB_PORT_ENTRIES_POOLID(), (UINT1 *)pNode)

#ifndef ERPS_ARRAY_TO_RBTREE_WANTED            
/* NOTE: the reason to keep this memory size as 1 is, when pool creation is
 * being done, the number of blocks should be atleast 1 otherwise, the pool
 * creation will fail=.This RBTree wont be used anywhere if this
 * ERPS_ARRAY_TO_RBTREE_WANTED flag is disabled
 */
#define ERPS_MAX_SUB_PORT_ENTRIES       1
#define ERPS_MAX_SIZING_SUB_PORTS       MAX_ERPS_SUB_PORT_LIST_SIZE

#define ERPS_CREATE_SUBPORTLIST_TABLE(pEntry, RetVal)                       \
{                                                                           \
    /*RBTree wont be present in this case*/                                 \
    RetVal = SNMP_SUCCESS;                                                  \
}

#define ERPS_DELETE_SUBPORTLIST_TABLE(pEntry)                               \
{                                                                           \
    /*RBTree wont be present in this case*/                                 \
}

#define ERPS_GET_SUBPORT_PORT1(pEntry, SubPortList)                         \
        MEMCPY(SubPortList, pEntry->Port1SubPortList,                       \
            sizeof(tErpsSubPortList))

#define ERPS_GET_SUBPORT_PORT2(pEntry, SubPortList)                         \
        MEMCPY(SubPortList, pEntry->Port2SubPortList,                       \
            sizeof(tErpsSubPortList))

#define ERPS_SET_SUBPORT_PORT1(pEntry, Index)                               \
    OSIX_BITLIST_SET_BIT (pEntry->Port1SubPortList,                         \
            Index, sizeof (tErpsSubPortList));

#define ERPS_SET_SUBPORT_PORT2(pEntry, Index)                               \
    OSIX_BITLIST_SET_BIT (pEntry->Port2SubPortList,                         \
            Index, sizeof (tErpsSubPortList));

#define ERPS_IS_SUBPORT_PORT1(pEntry, Index, u1Result)                      \
    OSIX_BITLIST_IS_BIT_SET (((pEntry->Port1SubPortList)),                  \
            Index, sizeof (tErpsSubPortList), u1Result);

#define ERPS_IS_SUBPORT_PORT2(pEntry, Index, u1Result)                      \
    OSIX_BITLIST_IS_BIT_SET (((pEntry->Port2SubPortList)),                  \
            Index, sizeof (tErpsSubPortList), u1Result);

#define ERPS_RESET_ALL_SUBPORT_PORT1(pEntry)                                \
    MEMSET (&(pEntry->Port1SubPortList), 0,                                 \
            sizeof (tErpsSubPortList));

#define ERPS_RESET_ALL_SUBPORT_PORT2(pEntry)                                \
    MEMSET (&(pEntry->Port2SubPortList), 0,                                 \
            sizeof (tErpsSubPortList));

#define ERPS_GET_VPN_ID(pEntry, Index)                                      \
    pEntry->au4VpnList[u4Index]

#else
/* the following macro should be defined as
 * MAX_ERPS_LSP_PW_INFO_ENTRIES.
 * This macro should not be used anywhere in the code
 * for arrays. Because this is mapped to sizing variable
 * MAX_ERPS_SUB_PORT_ENTRIES
 */
#define ERPS_MAX_SUB_PORT_ENTRIES       MAX_ERPS_SUB_PORT_LIST_SIZE
#define ERPS_MAX_SIZING_SUB_PORTS       (FsERPSSizingParams[MAX_ERPS_SUB_PORT_ENTRIES_SIZING_ID].u4PreAllocatedUnits)

#define ERPS_CREATE_SUBPORTLIST_TABLE(pEntry, RetVal)                       \
{                                                                           \
    RetVal = SNMP_SUCCESS;                                                  \
    pEntry->SubPortListTable =                                              \
    RBTreeCreateEmbedded (0, (tRBCompareFn) ErpsSubPortListTblCmp);         \
    if (pEntry->SubPortListTable == NULL)                                   \
    {                                                                       \
        RetVal = SNMP_FAILURE;                                              \
    }                                                                       \
}

#define ERPS_DELETE_SUBPORTLIST_TABLE(pEntry)                               \
{                                                                           \
    ErpsDelAllSubPortEntries(pEntry->SubPortListTable);                     \
    if (pEntry->SubPortListTable == NULL)                                   \
    {                                                                       \
        RBTreeDelete (pEntry->SubPortListTable);                            \
        pEntry->SubPortListTable = NULL;                                    \
    }                                                                       \
}

#define ERPS_GET_SUBPORT_PORT1(pEntry, SubPortList)                         \
{                                                                           \
    ErpsGetSubPortsFromDB(pEntry->SubPortListTable,                         \
       ERPS_SUBPORT_PROPERTY_PORT1, SubPortList);                           \
}

#define ERPS_GET_SUBPORT_PORT2(pEntry, SubPortList)                         \
{                                                                           \
    ErpsGetSubPortsFromDB(pEntry->SubPortListTable,                         \
       ERPS_SUBPORT_PROPERTY_PORT2, SubPortList);                           \
}

#define ERPS_SET_SUBPORT_PORT1(pEntry, Index)                               \
{                                                                           \
    ErpsAddSingleSubPortInDB(pEntry->SubPortListTable,                      \
            Index, ERPS_SUBPORT_PROPERTY_PORT1);                            \
}

#define ERPS_SET_SUBPORT_PORT2(pEntry, Index)                               \
{                                                                           \
    ErpsAddSingleSubPortInDB(pEntry->SubPortListTable,                      \
            Index, ERPS_SUBPORT_PROPERTY_PORT2);                            \
}

#define ERPS_IS_SUBPORT_PORT1(pEntry, Index, u1Result)                      \
{                                                                           \
    ErpsIsMemberPort(pEntry->SubPortListTable, Index,                       \
            ERPS_SUBPORT_PROPERTY_PORT1,&u1Result);                         \
}

#define ERPS_IS_SUBPORT_PORT2(pEntry, Index, u1Result)                      \
{                                                                           \
    ErpsIsMemberPort(pEntry->SubPortListTable, Index,                       \
            ERPS_SUBPORT_PROPERTY_PORT2,&u1Result);                         \
}

#define ERPS_RESET_ALL_SUBPORT_PORT1(pEntry)                                \
{                                                                           \
    ErpsResetAllSubPortInDB (pEntry->SubPortListTable,                      \
                    ERPS_SUBPORT_PROPERTY_PORT1);                           \
}

#define ERPS_RESET_ALL_SUBPORT_PORT2(pEntry)                                \
{                                                                           \
    ErpsResetAllSubPortInDB (pEntry->SubPortListTable,                      \
                    ERPS_SUBPORT_PROPERTY_PORT2);                           \
}

#define ERPS_GET_VPN_ID(pEntry, Index)                                      \
    (ErpsGetSubPortListEntry(pEntry->SubPortListTable ,Index))->u4VpnId
#endif

#define ERPS_PORT_ARRAY_POOLID()      gErpsGlobalInfo.ErpsPortArrayPoolId

#define ERPS_SIZING_MAX_RING FsERPSSizingParams[MAX_ERPS_RING_INFO_SIZING_ID].u4PreAllocatedUnits

#define ERPS_CONVERT_TIME_TICKS_TO_MSEC(u4TimeTicks)\
        (u4TimeTicks*(1000/SYS_NUM_OF_TIME_UNITS_IN_A_SEC))

#endif /*__ERPSMACS_H__*/
