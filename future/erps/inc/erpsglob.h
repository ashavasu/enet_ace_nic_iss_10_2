/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *  $Id: erpsglob.h,v 1.11 2017/02/22 13:42:19 siva Exp $
 * 
 *  Description: This file contains the global variables for ERPS
 *  module
*******************************************************************/

#ifndef __ERPSGLOB_H__
#define __ERPSGLOB_H__

#ifdef __ERPSMAIN_C__ 

tErpsGlobalInfo  gErpsGlobalInfo;
tErpsRcvdMsg     gErpsRcvdMsg;
UINT1            gau1ErpsSysCtrlStatus[SYS_DEF_MAX_NUM_CONTEXTS]; /* This variable 
                                                               maintains ERPS 
                                                               module system 
                                                               status of the 
                                                               context. */
#ifdef ICCH_WANTED
UINT1            gu1ErpsDisableInProgress = FALSE;/*This flag is to check ERPS disabling functionlity is in 
                                                    progress or not.*/
#endif

UINT1            gau1ErpsTraceTypes[ERPS_MAX_TRC_TYPE][UTL_MAX_TRC_LEN] =
                        { "start-shutdown", "mgmt", "datapath", "ctrl", 
                           "pkt-dump", "resource", "all-fail", "buffer", 
                           "critical"};

INT1 gaai1TimerState[ERPS_MAX_TMR_TYPES][20] = {"HoldOff Timer",
    "Guard Timer",
    "WTR Timer",
    "Periodic Timer",
    "WTB Timer"};

/* Fill the offset values of the data structure member that needs byte order
 * coversion before sending to standby node.
 */
tDbOffsetTemplate gaErpsRingOffsetTbl[] = 
{{(FSAP_OFFSETOF(tErpsRingInfo, u4ContextId) - 
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 4}, 
 {(FSAP_OFFSETOF(tErpsRingInfo, u4RingId) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 4},
 {(FSAP_OFFSETOF(tErpsRingInfo, u4RingNodeStatus) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 4}, 
 {(FSAP_OFFSETOF(tErpsRingInfo, u4SwitchCmdIfIndex) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 4}, 
 {(FSAP_OFFSETOF(tErpsRingInfo, Port1LastRcvdNodeId) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), sizeof(tMacAddr)},
 {(FSAP_OFFSETOF(tErpsRingInfo, u2LastMsgSend) - 
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 2}, 
 {(FSAP_OFFSETOF(tErpsRingInfo, Port2LastRcvdNodeId) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), sizeof(tMacAddr)},
 {(FSAP_OFFSETOF(tErpsRingInfo, Port1StoredNodeIdBPRPair) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), sizeof(tErpsPortNodeIdBPRPair)},
 {(FSAP_OFFSETOF(tErpsRingInfo, Port2StoredNodeIdBPRPair) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), sizeof(tErpsPortNodeIdBPRPair)},
 {(FSAP_OFFSETOF(tErpsRingInfo, u1Pad) -
   FSAP_OFFSETOF(tErpsRingInfo, RingDbNode) - sizeof(tDbTblNode)), 2},
 {-1, 0}};
UINT1               gau1ErpsVlanList[VLAN_LIST_SIZE_EXT];
UINT1               gau1ErpsVlanDelList[VLAN_LIST_SIZE_EXT];


#else

extern UINT1            gau1ErpsSysCtrlStatus[SYS_DEF_MAX_NUM_CONTEXTS];
#ifdef ICCH_WANTED
extern UINT1            gu1ErpsDisableInProgress;
#endif
extern tErpsGlobalInfo  gErpsGlobalInfo;
extern tErpsRcvdMsg     gErpsRcvdMsg;
extern UINT1            gau1ErpsTraceTypes[ERPS_MAX_TRC_TYPE][UTL_MAX_TRC_LEN];
extern INT1 gaai1TimerState[ERPS_MAX_TMR_TYPES][20];

extern tDbOffsetTemplate gaErpsRingOffsetTbl[];
extern UINT1               gau1ErpsVlanList[VLAN_LIST_SIZE_EXT];
extern UINT1               gau1ErpsVlanDelList[VLAN_LIST_SIZE_EXT];

#endif

#endif /*end of __ERPSGLOB_H__*/
