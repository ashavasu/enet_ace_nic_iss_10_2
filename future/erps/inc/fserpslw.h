/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fserpslw.h,v 1.25 2016/01/11 13:18:56 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsErpsContextTable. */
INT1
nmhValidateIndexInstanceFsErpsContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsContextTable  */

INT1
nmhGetFirstIndexFsErpsContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsCtxtName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsErpsCtxtSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtTraceInput ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsErpsCtxtTrapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtClearRingStats ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtVlanGroupManager ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsErpsCtxtProprietaryClearFS ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsCtxtSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtTraceInput ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsErpsCtxtTrapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtClearRingStats ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtVlanGroupManager ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsErpsCtxtProprietaryClearFS ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsCtxtSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtTraceInput ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsErpsCtxtTrapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtClearRingStats ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtVlanGroupManager ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsCtxtProprietaryClearFS ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsVlanGroupTable. */
INT1
nmhValidateIndexInstanceFsErpsVlanGroupTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsVlanGroupTable  */

INT1
nmhGetFirstIndexFsErpsVlanGroupTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsVlanGroupTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsVlanGroupRowStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsVlanGroupRowStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsVlanGroupRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsVlanGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingTable. */
INT1
nmhValidateIndexInstanceFsErpsRingTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingTable  */

INT1
nmhGetFirstIndexFsErpsRingTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingVlanId ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsErpsRingPort1 ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort2 ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRplPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPortBlockingOnVcRecovery ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingNodeType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingOperatingMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingMonitorMechanism ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort1Status ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort2Status ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingSemState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingNodeStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingMacId ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingProtectedVlanGroupId ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingProtectionType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRAPSCompatibleVersion ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRplNeighbourPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRAPSSubRingWithoutVC ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingRplNextNeighbourPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort1NodeID ARG_LIST((UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsErpsRingPort2NodeID ARG_LIST((UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsErpsRingPort1BPRBitVal ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort2BPRBitVal ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingProtectedVlanGroupList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingVlanId ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsErpsRingPort1 ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingPort2 ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRplPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingPortBlockingOnVcRecovery ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingOperatingMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingMonitorMechanism ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingMacId ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingProtectedVlanGroupId ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingProtectionType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRAPSCompatibleVersion ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRplNeighbourPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRAPSSubRingWithoutVC ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingRplNextNeighbourPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingProtectedVlanGroupList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingVlanId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsErpsRingPort1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingPort2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRplPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingPortBlockingOnVcRecovery ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingOperatingMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingMonitorMechanism ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingMacId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingProtectedVlanGroupId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingProtectionType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRAPSCompatibleVersion ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRplNeighbourPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRAPSSubRingWithoutVC ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingRplNextNeighbourPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingProtectedVlanGroupList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingCfmTable. */
INT1
nmhValidateIndexInstanceFsErpsRingCfmTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingCfmTable  */

INT1
nmhGetFirstIndexFsErpsRingCfmTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingCfmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingMEG1 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingCfmME1 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingCfmMEP1 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingMEG2 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingCfmME2 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingCfmMEP2 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingCfmRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingMEG1 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingCfmME1 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingCfmMEP1 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingMEG2 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingCfmME2 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingCfmMEP2 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingCfmRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingMEG1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingCfmME1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingCfmMEP1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingMEG2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingCfmME2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingCfmMEP2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingCfmRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingCfmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingConfigTable. */
INT1
nmhValidateIndexInstanceFsErpsRingConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingConfigTable  */

INT1
nmhGetFirstIndexFsErpsRingConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingConfigHoldOffTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigGuardTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigWTRTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigPeriodicTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigSwitchPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigSwitchCmd ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigRecoveryMethod ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigPropagateTC ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigWTBTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigClear ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigInterConnNode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigMultipleFailure ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigIsPort1Present ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigIsPort2Present ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigInfoDistributingPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingConfigKValue ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsErpsRingConfigFailureOfProtocol ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingConfigHoldOffTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigGuardTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigWTRTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigPeriodicTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigSwitchPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigSwitchCmd ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigRecoveryMethod ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigPropagateTC ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigWTBTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigClear ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigInterConnNode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigMultipleFailure ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigIsPort1Present ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigIsPort2Present ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigInfoDistributingPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsErpsRingConfigKValue ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsErpsRingConfigFailureOfProtocol ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingConfigHoldOffTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigGuardTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigWTRTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigPeriodicTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigSwitchPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigSwitchCmd ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigRecoveryMethod ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigPropagateTC ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigWTBTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigClear ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigInterConnNode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigMultipleFailure ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigIsPort1Present ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigIsPort2Present ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigInfoDistributingPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingConfigKValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsErpsRingConfigFailureOfProtocol ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingTcPropTable. */
INT1
nmhValidateIndexInstanceFsErpsRingTcPropTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingTcPropTable  */

INT1
nmhGetFirstIndexFsErpsRingTcPropTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingTcPropTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingTcPropRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingTcPropRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingTcPropRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingTcPropTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingConfigExtTable. */
INT1
nmhValidateIndexInstanceFsErpsRingConfigExtTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingConfigExtTable  */

INT1
nmhGetFirstIndexFsErpsRingConfigExtTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingConfigExtTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingConfigExtVCRecoveryPeriodicTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingConfigExtMainRingId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingConfigExtVCRecoveryPeriodicTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingConfigExtMainRingId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingConfigExtVCRecoveryPeriodicTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingConfigExtMainRingId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingConfigExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsErpsRingMplsTable. */
INT1
nmhValidateIndexInstanceFsErpsRingMplsTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingMplsTable  */

INT1
nmhGetFirstIndexFsErpsRingMplsTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingMplsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingPwVcId1 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPwVcId2 ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingMplsRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingPwVcId1 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingPwVcId2 ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsErpsRingMplsRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingPwVcId1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingPwVcId2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsErpsRingMplsRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingMplsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsMemFailCount ARG_LIST((UINT4 *));

INT1
nmhGetFsErpsBufFailCount ARG_LIST((UINT4 *));

INT1
nmhGetFsErpsTimerFailCount ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsErpsRingStatsTable. */
INT1
nmhValidateIndexInstanceFsErpsRingStatsTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsErpsRingStatsTable  */

INT1
nmhGetFirstIndexFsErpsRingStatsTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsErpsRingStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsErpsRingClearRingStats ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsErpsRingPort1RapsPduSentCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsPduSentCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsPduRcvdCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsPduRcvdCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsPduDiscardCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsPduDiscardCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1BlockedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2BlockedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1UnblockedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2UnblockedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1FailedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2FailedCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RecoveredCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RecoveredCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1VersionDiscardCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2VersionDiscardCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsFSPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsFSPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsFSPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsFSPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsMSPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsMSPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsMSPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsMSPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsEventPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsEventPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsEventPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsEventPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsSFPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsSFPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsSFPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsSFPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsNRPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsNRPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsNRPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsNRPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsNRRBPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1RapsNRRBPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsNRRBPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2RapsNRRBPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingGeneratedTrapsCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1DefectEncTimeSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2DefectEncTimeSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort1DefectClearedTimeSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingPort2DefectClearedTimeSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRplPortStatChgTimeSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRplNbrPortStatChgTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingDistPortPduRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingDistPortPduTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRapsPort1DefectEncTimeNSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRapsPort1DefectClearedTimeNSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRapsPort2DefectEncTimeNSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRapsPort2DefectClearedTimeNSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingRapsRplPortStatChgTimeNSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingDefectSwitchOverTimeMSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsErpsRingDefectClearedSwitchOverTimeMSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsErpsRingClearRingStats ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsErpsRingClearRingStats ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsErpsRingStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsErpsRingServiceType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhSetFsErpsRingServiceType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsErpsRingServiceType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhGetFsErpsRingPort1SubPortList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsErpsRingPort2SubPortList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));


INT1
nmhSetFsErpsRingPort1SubPortList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsErpsRingPort2SubPortList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsErpsRingPort1SubPortList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsErpsRingPort2SubPortList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

