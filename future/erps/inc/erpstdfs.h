/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: erpstdfs.h,v 1.54 2016/09/17 12:41:26 siva Exp $
*
* Description: This file contains the structures for ERPS module
*******************************************************************/
#ifndef __ERPSTDFS_H__
#define __ERPSTDFS_H__

#include "hwaud.h"
typedef struct _sErpsRedGlobalInfo{
    tMemPoolId            HwAuditTablePoolId;
                                 /* Np Sync Buffer Table Pool Id. */
    tTMO_SLL              HwAuditTable;
                                 /* SLL header of Hardware Audit Np-Sync Buffer
                                  * Table.*/
    UINT1                 u1NodeStatus;
                                 /* Node Status - Idle/Active/Standby.
                                  * Idle    - Node Status on boot-up.
                                  * Active  - Node where hardware programming
                                  *           is done.
                                  * Standby - Node which is in sync with active
                                  *           of all the information. it can 
                                  *           take Active role, in case of 
                                  *           Active node fails.*/
    UINT1                 u1NumOfStandbyNodesUp;
                                 /* Number of Standby nodes which are booted 
                                  * up. If this value is 0, then dynamic 
                                  * messages are not sent by active node.*/
    INT1                  i1NpSyncBlockCount;
                                 /* At Active Node - NP-Syncups are not sent 
                                  *                  when the value is not 0.
                                  * At Standby Node - NP-Syncup Buffer Table 
                                  *                   is not accessed when the
                                  *                   value is greater than 0.
                                  * Block Counter is incremented when there is
                                  * a need to block the Np Sync-ups. When there
                                  * is a special event (Module shutdown, Module
                                  * Enable and Module disable per context).
                                  * During static also, dynamic bulk updates 
                                  * and Module Shutdown (for all contexts to 
                                  * handle communication lost and restored 
                                  * scenario), the Block counter is 
                                  * incremented. After the completion of these
                                  * special events the counter is decremented.*/
    BOOL1                 bBulkReqRcvd;
                                 /* Bulk Request Received Flag 
                                  * - OSIX_TRUE/OSIX_FALSE. 
                                  * This flag is set when the dynamic bulk 
                                  * request message reaches the active node 
                                  * before the STANDBY_UP event. Bulk updates
                                  * are sent only after STANDBY_UP event is 
                                  * reached.*/
    /* HITLESS RESTART */
    UINT1                 u1StdyStReqRcvd; 
                                /* Steady State Request Received Flag.
                                 *  - OSIX_TRUE/OSIX_FALSE.
                                 *  This flag is set when the Steady State
                                 *  packet request is received from RM after
                                 *  finishing the bulk storage for Hitless
                                 *  Restart */ 
    UINT1                 u1Pad[3];
                                /* Padding */

}tErpsRedGlobalInfo;

#ifdef L2RED_WANTED
typedef struct _ErpsRmMsg {
    tRmMsg               *pData;    
                                 /* RM message pointer */
    UINT2                 u2DataLen; 
                                 /* Length of RM message */
    UINT1                 u1Event;   
                                 /* RM event */
    UINT1                 au1Reserved[1];
}tErpsRmMsg;


#endif
typedef tNpSyncBufferEntry tErpsRedNpSyncEntry;

typedef struct _sErpsRcvdMsg{
    tMacAddr              RcvdNodeId;
    UINT1                 u1MsgType;
    UINT1                 u1SubCode;
    UINT1                 u1RBBitStatus;
    UINT1                 u1DNFBitStatus;
    UINT1                 u1BPRBitStatus;
    UINT1                 u1StatusReserved;
    UINT1                 u1RingMacId;
    UINT1                 u1RingVersion;
    UINT1                 u1DERPSBitStatus;
                              /* This field will hold the status of D-ERPS bit.
          * This bit is used in Distributed ERPS feature
          * to identify D-ERPS sync PDU 
          * (explained in erpsdd.doc Appendix-D, section 7.2.1).
          * This bit will be set to '1' for DERPS PDU and '0' 
          * for R-APS PDUs. */
    UINT1                 au1Padbits[1];
}tErpsRcvdMsg;

typedef struct ErpsCfmMepInfo
{
    UINT4    u4MdId;
    UINT4    u4MaId;
    UINT4    u4MepId;         /* Maintainence End Point Id */
    UINT4    u4IfIndex;       /* Interface of which link status is reported */
    tVlanId  VlanId;          /* R-APS Vlan Id */
    UINT1    u1Direction;     /* Direction of Maintainence End Point */
    UINT1    au1Reserved[1];
}tErpsCfmMepInfo;

/*------------------------------------------------------------------------
 *           R-APS PDU Transmission Information
 * The information required to transmit R-APS PDU on the SEM state change
 * of a Ring group.
 *----------------------------------------------------------------------- */
typedef struct ErpsRapsTxInfo
{
    tTMO_SLL_NODE        SllNode;
    tErpsCfmMepInfo      MepInfo1;
    tErpsCfmMepInfo      MepInfo2;
                            /* MegId,MeId and MepId */
    VOID                   *pContext;
                            /* This field is added for greater flexibility
                             * for porting ERPS stack. This field is place 
                             * holder for context information or database 
                             * created in porting layer  of ERPS. This field 
                             * is updated by porting code, whenever ring is 
                             * created
                             * The same information is passing to all the 
                             * external calls that ERPS is making for ring 
                             * transperantly. This will minimize the need 
                             * for searching in porting code */
    UINT4                u4ContextId;
                           /* Virtual context id. maintained by VCM module */
    UINT4                u4RingId;
                           /* Ring Group Id */
    UINT1                *pu1RApsPdu;
    tMacAddr             au1MacAddr; 
                           /* R-APS specific information in an R-APS PDU */
    UINT1                u1Version; /* Version field information in R-APS PDU */
    UINT1                u1RingTxAction;
                            /* This field selects whether R-APS packets
                             * have to be sent on - Both the ring ports
                             * or on one of the Ring port namely Port 1
                             * or Port 2.  */
}tErpsRapsTxInfo;


/* ERPS Context Data Structure */
typedef struct _sErpsContextInfo{
    UINT4                  u4TraceOption;
                              /* This variable will maintain a bitmap for trace
                               * options. If bit is set, this will represent
                               * option is enabled and if bit is not set then
                               * that particular trace option is disabled. */
    UINT1                  au1ContextName[VCM_ALIAS_MAX_LEN];
                              /* Context alias name */
    tMacAddr               NodeId;
                              /* This will contain 6byte context mac address.
                               * ERPS module will fill this variable, when it
                               * receives context create indication. */
    UINT1                  u1ModuleStatus;
                              /* This variable maintains ERPS module status of
                               * the context. */
    UINT1                  u1TrapStatus;
                              /* This variable maintains Trap enable/disable
                               * status of the ERPS in the context. */
    UINT1                  u1RowStatus;
                              /* Entry will be created if RowStatus is set as
                               * CreateAndGo and entry will be deleted if
                               * RowStatus is set to Destroy. 
                               * CreateAndWait and NotInService will not be
                               * supported.  */
    UINT1                  u1VlanGroupManager;
                              /* identifies the module that manages the 
                               * grouping of vlans in this virtual context.
                               * possible values are - 
                               * ERPS_VLAN_GROUP_MANAGER_MSTP
                               * ERPS_VLAN_GROUP_MANAGER_ERPS
                               */
    UINT1                  u1ProprietaryClearFS;
                              /* - ERPS_SNMP_TRUE / ERPS_SNMP_FALSE
                               * This flag can be set if proprietary
                               * implementation is wanted. The proprietary
                               * implementation includes the functionality
                               * when FS is given over SF. And upon clearance
                               * of FS, this implementation will move the ring
                               * to protection state if SF still exists. This
                               * field is disabled by default.
                               */
    UINT1                  au1Reserved[1];
}tErpsContextInfo;
    
/* D-ERPS sync info Data Structure.
 * -----------------------------------------------------------
 * This structure contains the information which are synced
 * with remote ERP in Distributed ERPS.
 *
 * When Ring Information (tErpsRingInfo) is updated by SEM events the following is done : 
 *  1. The bitmask ( u2BitMask ) is updated in ( tErpsDsyncInfo ) structure. 
 *  2. This bitmask is stored in the D-ERPS PDU and send to the remote line card.
 *  3. On reception of D-ERPS sync PDU (explained in erpsdd.doc Appendix-D, section 7.2.1),
 *     the bitmask present in the PDU is examined by masking the below defined Macros.
 *  4. Based on the value obtained from the bitmask, the corresponding Ring Info is updated.
 *     in the remote line card */

/* Example: If u1HighestPriorityRequest is updated in ( tErpsDsyncInfo ) structure,
 *  1. The bitmask ( u2BitMask ) is updated and the new updated HighPriorityRequest 
 *     ( u1HighestPriorityRequest)value is updated in tErpsDsyncInfo structure.
 *  2. D-ERPS PDU is contructed with the information present in tErpsDsyncInfo structure and sent
 *     to the remote ERP.
 *  3. On receiving side, the remote ERP will examin the bitmask value present in the D-ERPS PDU.
 *     If HighestPriorityRequest bit is set, remote ERP will read the value of u1HighestPriorityRequest
 *     in DERPS PDU and update the value in Ring Information ( tErpsRingInfo ) structure. */

typedef struct _sErpsDsyncInfo{
    UINT4                    u4ContextId;
                              /* This field will have contextId configured for this ring. */
    UINT4                    u4RingId;
                              /* Ring Index will be used by management entity to identify
          * particular ring. */
    UINT2                    u2BitMask;
                              /* Bitmask value, used to identify the type of 
          * dynamic info updation done in Local ERP SEM events.
          * Each bit in bitmask represents different type of dynamic info field.
                               * If a bit is set in Bit mask, then corresponding info field, present in 
          * DERPS PDU (explained in erpsdd.doc Appendix-D, section 7.2.1), should
                               * be examined and updated in Peer Node RingInfo struct from . */
          /* BitMask Info fileds:-
          *      -------------------------------------------------
          *     | 15 - 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
          *      -------------------------------------------------
                               * Bit to field mapping is as defined below:
                               * Bit 0: Local Port state update Notify field.
                               * Bit 1: Remote port h/w status change indication.
                               * Bit 2: Remote ERP next ring state.
                               * Bit 3: Highest Priority request.
                               * Bit 4: Timer Info field.
                               * Bit 5: This field represents R-APS Message to be transmitted from
                               *        Remote ERP.
                               *          - if this bit is set,
                               *               Then start Tx of type u2LastMsgSend
                               * Bit 6: Perform Flush in Remote ERP.
                               * Bit 7: Ring Node status.
                               * Bit 8: (Node ID, BPR) Pair of the sender's Local port.
                               * Bit 9: RapsTxRequest.
          * Bit 10-15: Not Used.
                               */
    UINT2                    u2LastMsgSend;
                              /* This field is used to update the type of last message (SF, NR, NR with RB
          * or event) sent from Local ERP. */
    tMacAddr                 LocalPortLastRcvdNodeId;
                              /* This field will hold the value of sender Node ID in R-APS PDU received on
          * local port. */
    UINT2                    u2RingNodeStatus;
                              /* This variable will hold the current node 
                               * status. Node status represents Port1 and
                               * Port 2, failure status, received Remote 
                               * failure status, RPL block status on RPL
                               * node and WTR, HOLD and GUARD timer status. */  
    UINT1                    u1LocalPortStateUpdateNotify; 
                              /* This field will hold the port status of Local Ring port.
          * Local ERP will notify Remote ERP to update Ring port status in its Ring info.
          * For Example, If SignalFail is received on Local Ring port, Local ERP SEM will
          * updated its RingInfo and send a notification to Remote ERP. On receiving side
          * Remote ERP will examin this value and update its Ring info.
          *  01 - Failed
          *  10 - Blocked
          *  00 - Unblocked.*/
    UINT1                    u1RapsTxRequest;
     /* This field is used to indicate whether R-APS PDU transmission should be
      * started or stopped in remote ERP.
      * This field is set to OSIX_TRUE when R-APS PDU TX is started by Local ERP.
      * This field is set to OSIX_FALSE when R-APS PDU TX is stopped by Local ERP. */
    UINT1                    u1RemotePortHWStateChangeInd; 
                              /* This field represents the indication to call NP in Remote ERP.
          * For Example, On receiving an event the Local ERP SEM is deciding to put the 
          * Remote Ring port to blocking, this variable will be updated and indication
          * will be sent to Remote ERP by using DERPS PDU. On receving this DERPS PDU
          * Remote ERP will examin this value and call NPAPI to block the Ring port.
          * If the value is,
          *  10 - Block the Ring port
          *  00 - Unblock the Ring port.*/
    UINT1                    u1HighestPriorityRequest;
                              /* This field represent the Highest Priority event
                               * identified by Priority Logic of Local ERP. */
    UINT1                    u1RingState;
                              /* This field will hold the ring state machine state of Local ERP. */
    UINT1                    u1BPRStatus;
                              /* This field is used in (Node ID, BPR) comparison for Flush Logic,
                               * This field will hold the BPR bit status of the Last R-APS message over
          * the local ring port. */
    UINT1                    u1TriggerFlush;
     /* This field is used to indicate whether FDB Flush should be
      * triggered or not in remote ERP.
      * This field is set to OSIX_TRUE when Flush is triggered by Local ERP
      * otherwise set to OSIX_FALSE. */
    UINT1                     u1TimerInfo;
                              /* Start or stop the timers in Remote ERP depending on
                               * the bits set in this field.
          * Timer Info bit fileds:-
          *      -------------------------------
          *     | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
          *      -------------------------------
                               * Bit 0: Timer Action.
          *           if this bit is set,
          *                 Timer should be started.
                               *           if this bit is not set,
          *                 Timer should be stopped.
                               * Bit 1: WTR Timer.
          *        If this bit is set the Action (mentioned by Bit-0) should be 
          *        performed on WTR Timer.
                               * Bit 2: WTB Timer.
          *        If this bit is set the Action (mentioned by Bit-0) should be 
          *        performed on WTB Timer.
                               * Bit 3: Guard Timer.
          *        If this bit is set the Action (mentioned by Bit-0) should be 
          *        performed on Guard Timer.
          * Bit 4-7: Not Used.
                               */
}tErpsDsyncInfo;

/* Structure defined for allocating memory for Port1/Port2 
 * Octet string configuration. 
 */


typedef UINT4 tErpsCliMemBlkBuf[MAX_ERPS_CLI_BUF_LEN];

/* ERPS Main Task global Data Structure */
typedef struct _sErpsGlobalInfo{
    tOsixTaskId           MainTaskId;
                              /* ERPS Main Task ID */
    tOsixTaskId           RapsTxTaskId;
                              /* R-APS Transmission Task ID */
    tOsixQId              MainTaskQId;
                              /* ERPS Main Task message Q Id */
    tOsixSemId            MainTaskSemId;
                              /* Main Task Semaphore ID */
    tOsixSemId            RapsTxInfoSemId;
                              /* Semaphore ID used for protecting the 
                               * tErpsGlobalTxInfo and their corresponding
                               * SLLs. */
    tMemPoolId            TaskQMsgPoolId;
                              /* Mempool Id for ERPS Task messages */
    tMemPoolId            ContextPoolId;
                              /* Mempool Id for tErpsContextInfo */
    tMemPoolId            RingInfoPoolId; 
                              /* This variable will hold the memory pool Id
                               * of the Ring memory pool. */
    tMemPoolId            RapsTxInfoTablePoolId; 
                              /* Mempool Id for tErpsGlobalTxInfo */
    tMemPoolId            RapsTxPduPoolId; 
                              /* Mempool Id for TxPDU which is stored in the 
                               * tErpsGlobalTxInfo */
    tMemPoolId            ErpsMbsmPoolId;
                              /* Mempool Id for MBSM card attach/detach details */
    tMemPoolId            SubRingPoolId; 
                              /* Mempool Id for sub ring list which is stored in
                               * tSubRingEntry */
    tMemPoolId            VlanGroupListPoolId;
    tMemPoolId            ErpsCliMemblkBufPoolId;  /* Memory Pool of tErpsCliMemBlkBuf */
                                                   /* memoy unit */
         /* Mempool Id for Vlan Group List*/

    tMemPoolId            MplsApiInInfoPoolId;
                              /* Mempool Id for MPLS OAM registration datastructure*/
    tMemPoolId            MplsApiOutInfoPoolId;
                              /* Mempool Id for MPLS interaction  datastructure*/
    tMemPoolId            LspPwInfoListId;
    tMemPoolId            ErpsUtlSubPortListPoolId;
    tMemPoolId            ErpsSubPortEntryPoolId;
    tMemPoolId            ErpsPortArrayPoolId;
       /* Mempool Id for lsp pw info mempool*/
    tErpsContextInfo     *apRingContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
                              /* This is an array of pointers of context
                               * information of ring. When context create
                               * indication is received by ERPS module,
                               * memory will be allocated for the context
                               * and context information will get initialized
                               * with valid info such as NodeId.
                               * When the context delete is received by the
                               * ERPS module, this context memory will be
                               * released. */
    tRBTree               RingTable;
                              /* RBTree header for {Context, RingId} based
                               * Ring RBTree. */
    tRBTree               RingPortVlanTable;
                              /* RBTree header for {Context, Vlan}
                               * based Ring RBTree. */
    
    tRBTree            RingMepTable;/* RBTree header for {Context, MEG,ME,MEP,RingId} based
                                    Ring RBTree. */
    tTMO_SLL              RapsTxInfoList[ERPS_MAX_RAPS_PDU_TRANSMISSION];
                              /* SLLs that are used to achieve the
                               * transmission of R-APS PDU in a time gap of
                               * 2.5 milliseconds on change of R-APS PDU type.*/
    tTMO_SLL              *pRapsTxSll[ERPS_MAX_RAPS_PDU_TRANSMISSION];
                              /* Pointer to the SLLs, which is used to acheive
                               * the faster movement of SLL entries across
                               * SLLs. */
    tTimerListId          RingTimerListId;
                              /* This variable will hold Timer the timer list
                               * Id. */
    tTmrDesc              aTmrDesc[ERPS_MAX_TMR_TYPES];
                              /* Timer descriptor to hold timer expiry
                               * functions. */
#ifdef L2RED_WANTED
    tDbTblDescriptor      ErpsDynInfoList;
    tDbDataDescInfo       aErpsDynDataDescList[ERPS_MAX_DYN_INFO_TYPE];
#endif
    tErpsRedGlobalInfo    ErpsRedGlobalInfo;
    UINT4                 u4MemFailCount;
                              /* This variable will be incremented, when the
                               * memory allocation failed. */
    UINT4                 u4BuffferAllocFailCount;
                              /* This variable will be incremented, when CRU
                               * Buffer allocation failed. */
    UINT4                 u4TimerFailCount;
                              /* This variable will be incremented, when timer
                               * related failure happens. */
#ifdef SYSLOG_WANTED
    UINT4                 i4SysLogId;
#endif
    BOOL1                  b1InRapsTxEventHandler;
                              /* Flag to indicate whether the Tx Task is in
                               * the Event Handler thread. If this is True,
                               * then no need to post an event to Tx Task
                               * while adding new R-APS Tx node to the SLL. */
    UINT1                 au1Reserved[3];
}tErpsGlobalInfo;


/* ERPS CFM Data Structure */
typedef struct _sErpsCfmInfo{
    UINT4                  u4Port1MEGId;
                              /* This filed will hold Maintenance Entity Group
                               * Id of the Port 1. */
    UINT4                  u4Port1MEId;
                              /* This variable will hold Maintenance Entity Id
                               * for the Port 1. */
    UINT4                  u4Port1MEPId; 
                              /* This filed represent the DOWN MEP Id of the on
                               * Port 1. */
    UINT4                  u4Port2MEGId;
                              /* This filed will hold Maintenance Entity Group
                               * Id of the Port 2 */
    UINT4                  u4Port2MEId;
                              /* This variable will hold Maintenance Entity Id
                               * for the Port 2. if Port 2 port is not present
                               * (in case of Sub Ring) this will represent UP
                               * MEP.s ME Id on Port1. */
    UINT4                  u4Port2MEPId;
                              /* This filed represent the DOWN MEP Id of the
                               * on Port 2. if Port 2 is not present (in
                               * sub-ring), then this will hold UP MEP Id of 
                               * Port 1. */
    UINT1                  u1RowStatus;
                              /* This filed will represent Row Status of CFM 
                               * Table. */
    UINT1                  au1Reserved[3];
}tErpsCfmInfo;


/* Added for as a part of VPLS RESILIENCY development
 *      ERPS - MPLSOAM structured */
typedef struct _sErpsMplsPwInfo{
    UINT4      u4PwVcId1;  /*This field will hold pw id w.r.t. Ring Port1*/
    UINT4       u4PwVcId2; /*This field will hold pw id w.r.t. Ring Port2*/ 
    UINT1                  u1RowStatus;    /* This filed will represent Row Status of MplsOam Table*/
    UINT1                  au1Reserved[3];
}tErpsMplsPwInfo;

/* ERPS Port stats Structure */
typedef struct _sErpsPortStats {
    UINT4                  u4RapsSentCount;
                              /* This count will be incremented each time, 
                               * when R-APS messaged sends out of this port. */
    UINT4                  u4RapsReceivedCount; 
                              /* This count will be incremented each time, 
                               * when R-APS messaged received on this port. */
    UINT4                  u4RapsDiscardCount;
                              /* This count will be incremented each time, 
                               * when R-APS messaged received on this port but
                               * discarded. */
    UINT4                  u4BlockedCount;
                              /* This count will be incremented each time,
                               * when this port is programmed to block the
                               * traffic on this port. */
    UINT4                  u4UnblockedCount;
                              /* This count will be incremented each time,
                               * when this port is programmed to unblock the
                               * traffic on of this port. */
    UINT4                  u4FailedCount;
                              /* This count will be incremented each time,
                               * failure indication received for this port. */
    UINT4                  u4RecoveredCount;
                              /* This count will be incremented each time, when
                               * failure recovery indication received for this
                               * port.*/
    UINT4                  u4VersionDiscardCount;
                              /* This count will be incremented each time, when
                               *  FS,MS PDU's are received (FS,MS) when compatible 
                               *  version is v1 
                               *  and if RAPS PDUs version is not V1 or V2 */
    UINT4                  u4FSPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(FS) is recieved on this  port.*/
    UINT4                  u4FSPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(FS) is  sent from this  port.*/
    UINT4                  u4MSPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(MS) is recieved on this  port.*/    
    UINT4                  u4MSPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(MS) is sent from this  port.*/
    UINT4                  u4EventPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(Event) is recieved on this  port.*/    
    UINT4                  u4EventPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(Event) is sent from this  port.*/
    UINT4                  u4SFPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(SF) is recieved on this  port.*/
    UINT4                  u4SFPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(SF) is  sent from this  port.*/
    UINT4                  u4NRPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(NR) is recieved on this  port.*/
    UINT4                  u4NRPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(NR) is  sent from this  port.*/
    UINT4                  u4NRRBPduRxCount;
                              /* This count will be incremented each time, when
                               * R-APS(NRRB) is recieved on this  port.*/
    UINT4                  u4NRRBPduTxCount;
                              /* This count will be incremented each time, when
                               * R-APS(NRRB) is  sent from this  port.*/
    }tErpsPortStats;
   
/* ERPS Performance Data structure */
typedef struct _sErpsPerf {

    UINT4            u4Port1DefectEncounteredTimeSec;
        /*This object indicates the time,
                              * when the defect occurred for port1 of the ring.*/
    UINT4            u4Port2DefectEncounteredTimeSec;
        /*This object indicates the time,
                              * when the defect occurred for port2 of the ring.*/
    UINT4            u4Port1DefectClearedTimeSec;
        /*This object indicates the time,
                              * when the defect cleared for port1 of the ring.*/
    UINT4            u4Port2DefectClearedTimeSec;
        /*This object indicates the time,
                              * when the defect cleared for port1 of the ring.*/
    UINT4            u4RplPortStatusChangedTimeSec;
        /*This object indicates the time,
                              * when the Rpl Port status change for the ring.*/
    UINT4            u4RplNeighborPortStatusChangedTime;
        /*This object indicates the time,
                              * when the Rpl Nbr Port status change for the ring.*/
    UINT4            u4RapsSFPort1DefectTimeNSec;
        /*This object indicates the time in nanoseconds,
                                * when the RapsSF defect PDU occurred on port1 of the ring.*/

    UINT4            u4RapsSFPort2DefectTimeNSec;
        /*This object indicates the time in nanoseconds,
                        * when the RapsSF defect PDU occurred on port2 of the ring.*/

    UINT4            u4RapsClearSFPort1DefectTimeNSec;
        /*This object indicates the time in nanoseconds,
                           * when the RapsCSF defect cleared PDU occured on port1 of the ring.*/

    UINT4            u4RapsClearSFPort2DefectTimeNSec;
        /*This object indicates the time in nanoseconds,
                           * when the RapsCSF defect cleared PDU occured on port2 of the ring.*/
    UINT4            u4RplPortStatusChangedTimeNSec;
        /*This object indicates the time in nanoseconds,
                 when the Rpl Port status change for the ring.*/



   }tErpsPerf;
 
/* Information of each node in the list of Sub-rings 
 * that are associated with the main ring */
typedef struct SubRingEntry
{
    tTMO_SLL_NODE SubRingNextNode;
    UINT4 u4SubRingId;
}tSubRingEntry;

typedef struct _sErpsPortNodeIdBPRPair {
    tMacAddr               NodeId; 
                              /* Node ID of the last R-APS message 
                               * received over the ring port. */
    UINT1                  u1BPRStatus;
                              /* BPR bit status of the Last R-APS message over
                               * the ring port. This BPR status will be
                               *   a) Set to 0 if ring port1 is blocked or
                               *      if both the ring ports are blocked or if
                               *      it is an interconnected node of the
                               *      subring i.e if the second ring port is 0
                               *   b) Set to 1 if ring port2 is blocked */
    UINT1                  au1Pad[1];
}tErpsPortNodeIdBPRPair;


/**************************************************************************/
    /* CAUTION :
     * This structure tErpsRingInfo is split into two :
     * Only Ring related information. (added before 'tDbTblNode RingDbNode')
     * HA related Ring information.  (added after 'tDbTblNode RingDbNode'
     * 
     * All New structure members added to tErpsRingInfo should be added before 
     * 'tDbTblNode RingDbNode' if they are not related to HA functionality.
     * Any new structure member added (ie.,If the information is HA related and
     * needs to be synched up to the standby node)should be after 'tDbTblNode
     * RingDbNode'.In that case, Padding should be done separately. 
     * 
     * If New Structure Member added after 'tDbTblNode RingDbNode' is UINT4 or 
     * UNIT2 measns, that needs to be updated in erpsglob.h (tDbOffsetTemplate 
     * gaErpsRingOffsetTbl[]to fill the offset value of that variable for 
     * byte order conversion.
     *
     * The padding information will be calculated from tDbTblNode RingDbNode to 
     * the end of this structure.  That is UINT1 au1pad[x] must be added as the 
     * last line of this structure.
     * 
     * So totally this structure will have two paddings one before .tDbTblNode 
     * RingDbNode. and another at the end of the structure. 
     * 
     * Any new HA related member added here should follow the "Synced dynamic info 
     * order" mentioned in ErpsRedProcessRingSyncMsg (erpsred.c). If the order is 
     * not followed HA related information will not be synched up properly to the
     * standby node.
     *
     ***************************************************************************/


/* ERPS Ring Data Structure */
typedef struct _sErpsRingInfo {
    tRBNodeEmbd            Port1RBNode;
                              /* This filed will contain RBTree node pointer
                               * for ring RBTree indexed by Port and Vlan Id. */
    INT4                   i4Port1RBNodeOffset;
                              /* This field will contain the Port1RBNode 
                               * offset value in the tErpsRingInfo data 
                               * structure. */
    tRBNodeEmbd            Port2RBNode;
                              /* This filed will contain RBTree node pointer 
                               * for ring RBTree indexed by Port and Vlan Id. */
    INT4                   i4Port2RBNodeOffset;
                              /* This field will contain the Port1RBNode 
                               * offset value in the tErpsRingInfo data 
                               * structure. */
    tRBNodeEmbd            DistPortRBNode;
                              /* This filed will contain RBTree node pointer 
                               * for ring RBTree indexed by Port and Vlan Id. */
    INT4                   i4DistPortRBNodeOffset;
                              /* This field will contain the Port1RBNode 
                               * offset value in the tErpsRingInfo data 
                               * structure. */
    
    tRBNodeEmbd            Port1MepRBNode; /* This filed will contain RBTree node pointer
                           for ring RBTree indexed by ME,MEG,MEP and Ring Index */

    INT4                   i4Port1MepRBNodeOffset;
      /* This field will contain the Port1MepRBNode
                                    offset value in the tErpsRingInfo data structure. */
  
    tRBNodeEmbd            Port2MepRBNode;/* This filed will contain RBTree node pointer
                                             for ring RBTree indexed by ME,MEG,MEP and Ring Index */

    INT4                   i4Port2MepRBNodeOffset;
                                  /* This field will contain the Port2MepRBNode
                                    offset value in the tErpsRingInfo data structure. */
    tRBNodeEmbd            RingRBNode;
                              /* This filed will contain RBTree node pointer
                               * for ring RBTree indexed by Ring Id. */


    tUtlDynSll             TcPropRingList;
                              /* This filed contain the head pointer for 
                               * topology change propagation SLL. This filed 
                               * maintains SLL for associated rings of the sub 
                               * ring. The topology change event will 
                               * propagated to these ring in the
                               * event of flushing happen in the sub-ring of
                               * interconnected node. */
    tErpsCfmInfo           CfmEntry;
                              /* This filed will contain connectivity fault
                               * management protocol related objects detail to
                               * identify which cfm object will be used for 
          * forming and transmitting R-APS traffic. */
    tErpsMplsPwInfo    MplsPwInfo; 
         /* This field will contain the MPLS pseudo-wire inforamtion
         * in case of ERPS is used to maintain the state of ring in VPLS */ 

    tErpsPortStats         Port1Stats;
                              /* This field will contain the pointer to port1 
                               * entry.
                               * When a port is assigned to ring this port entry
                               * will be created. */
    tErpsPortStats         Port2Stats;
                              /* This field will contain the pointer to port2 
                               * entry. When a port is assigned to ring this 
                               * port entry will be created. */
    tErpsPerf              perf;
                              /* This field will contain the pointer to ErpsPerf                                 
                               *structure,which used to store the time */
    tErpsLspPwInfo         *pErpsLspPwInfo;
                              /*This field contains the pointer to LSP PW info
                               *structure*/

    tTmrBlk                HoldOffTimer;
                              /* This field will store the HoldOff Timer node of
                               * ring. */
    tTmrBlk                GuardTimer;
                              /* This field will store the Guard Timer node of
                               * ring. */
    tTmrBlk                WaitToRestoreTimer; 
                              /* This field will store the WTR Timer node of 
                               * ring. */
    tTmrBlk                PeriodicTimer;
                              /* This field will store the Periodic Timer node 
                               * of the ring. */
    tTmrBlk                WaitToBlockTimer;
                              /* This field will store the WTB Timer node of
                               * ring. */
    tTmrBlk                TfopTimer;                                                     
                              /* This field will store the TFOP Timer node of
                               * ring. */
    tTmrBlk                FopPmTimer;                                                     
                              /* This field will store the FOP_PM Timer node of
                               * ring. */
#ifdef ICCH_WANTED

    tTmrBlk                VlanFdbSyncTimer;
                            /*Purpose of this Timer: In MCLAG Node, if ERPS is doing flushing, start the
                             * FDB Sync Timer and disable the FDB Sync up between MCLAG.
                             * On its expiry, enable the sync up and fetch the remote FDB
                             * This is done to have uniform MAC Entries between MC-LAG Nodes
                             * and also uniform MAC Entry in HW and Control Plane in a MCLAG Node*/
#endif
    VOID                   *pContext;
                            /* This field is added for greater flexibility
                             * for porting ERPS stack. This field is place 
                             * holder for context information or database 
                             * created in porting layer  of ERPS. This field 
                             * is updated by porting code, whenever ring is 
                             * created
                             * The same information is passing to all the 
                             * external calls that ERPS is making for ring 
                             * transperantly. This will minimize the need 
                             * for searching in porting code */
   tErpsVlanGroupList         *pau1RingVlanGroupList;
                             /* This field contains the list of vlan groups mapped to
                              * a ring. This vlan group list can be modified in run
                              *  time without making the ring status as inactive  */
    UINT4                  u4HoldOffTimerValue;
                              /* This field will hold the configured holdoff 
                               * timer value. */
    UINT4                  u4GuardTimerValue;
                              /* This field will hold the configured Guard Timer
                               * value. */
    UINT4                  u4WTRTimerValue;
                              /* This field will hold the configured WTR Timer 
                               * value.*/
    UINT4                  u4PeriodicTimerValue;
                              /* This field will hold the configured Periodic 
                               * Timer value. */
    FLT4                  f4KValue;
                              /* This field will hold the integer value which 
                               * is used for TFOP defet. The timer is running 
                               * by multiplicaiton of this value with periodic
                               * timer value, in this interval any mismatch of 
                               * pdu the TFOP defec will be raised.*/
    UINT4                  u4VcRecoveryPeriodicTime;
                              /* This object is used to configure the time 
                               * interval for which the periodic timer needs 
                               * to be restarted for the sub-ring, when the 
                               * corresponding main ring indicates the virtual 
                               * channel status change to this sub-ring 
                               * and when the virtual channel of this 
                               * sub-ring is in failed state */
    UINT4                  u4WTBTimerValue;
                             /* This field will hold the configured WTB Timer
                              * value.*/
    UINT4                  u4Port1IfIndex;
                              /* This field contains interface index of the 
                               * first ring Port. */
    UINT4                  u4Port2IfIndex;
                              /* This field contains interface index of the 
                               * second ring Port. */
    UINT4                  u4DistIfIndex;
                              /* This field is exclusively used for D-ERPS
                               * implementation for D-ERPS PDU exchange 
                               * between Line Cards and to forward all the
                               * R-APS PDU's to remote ERP. */
    UINT4                  u4RplIfIndex;
                              /* This field will have Interface Index (one of
                               * the ring ports interface Index) identified as 
                               * RPL Port.
                               * If this port is non-zero, this node for this 
                               * ring will be identified as RPLOwner of the 
                               * ring. If it is unassigned (zero) then node 
                               * will be identified as non-RPLOwner node of 
                               * the ring. */
    UINT4                  u4RplNeighbourIfIndex;
                             /* This field will have Interface Index (one of
                              * the ring ports interface Index) identified as
                              * RPL Neighbour Port(adjacent to the RPL node).
                              * If this port is non-zero, this node for this
                              * ring will be identified as RPLNeighbour  of the
                              * ring. If it is unassigned (zero) then node
                              * will be as Ring node of the ring. */
    UINT4                  u4RplNextNeighbourIfIndex;
                             /* This field will have Interface Index (one of
                              * the ring ports interface Index) identified as
                              * RPL Next Neighbour Port(adjacent to the
                              * RPLnode/RPL Neighbour node).
                              * If this port is non-zero, this node for this
                              * ring will be identified as RPLNeighbour  of the
                              * ring. If it is unassigned (zero) then node
                              * will be as Ring node of the ring. */
    UINT4                  u4FlushTime;
                              /* This varibale will hold the time stamp for
                               * last flush time. This will be compared with
                               * current flush time, and flushing will be
                               * ignored if last was done in less than 10 ms
                               * time. This variable will be used to avoid
                               * multiple flush.
                               */
    UINT4                  u4OldSwitchCmdIfIndex;
                            /* This field is used to store the previous
                             * switch port value that has been given while
                             * applying the switch command */
    UINT4                   u4TrapsCount;
                              /* This count will be incremented each time, when 
                               * a trap message is sent from a ring node*/ 
    UINT4                  u4DistributingPortRxCount;
                              /* This count will be incremented each time,
                               * when D-ERPS Sync Pdu is Received on the  
                               * Distributing port .*/
    UINT4                  u4DistributingPortTxCount;
                              /* This count will be incremented each time,
                               * when D-ERPS Sync Pdu is sent out from 
                               * Distributing port .*/
    UINT4                  u4MsrSwitchCmdIfIndex;
                              /* This field is used to store the switch port 
                               * value where manual switch is given 
                               * in save and reload (MSR) cases */ 
    tTMO_SLL               SubRingListHead;
                            /* Head to the SLL that contains the list 
                             * of sub-rings */
    tSubRingEntry          SubRingEntry;
                              /*Contains the list of sub-rings that are 
                               *connected to this ring.
                               *This list is populated with the sub-ring id 
                               *when fsErpsRingConfigMainRingId is configured
                               */

    INT4                   ai4HwRingHandle[ERPS_MAX_HW_HANDLE];
                              /* This field maintains the filterid or RingId 
                               * returned by the hardware for this ring.       
                               * This can be used to delete the ring           
                               * configuration in hardware associated with
                               * this id. This will be initialzed as -1.
                               */
    UINT1                  au1RingName[ERPS_MAX_NAME_LENGTH];
                              /* This field will have name for the ring. By 
                               * default the ring name will be same as of 
                               * ring-id. */
    UINT2                  u2ProtectedVlanGroupId;
                              /* This field identifies the vlan group 
                               * associated with this ring instance. If 
                               * VlanGroupManager is 'mstp', then this 
                               * identifies the Mstp Instance Id. Otherwise 
                               * this object identifies the VlanGroup
                               * created through ERPS Vlan Group 
                               * Configuration.*/
    tVlanId                RapsVlanId;
                              /* This field will have vlanid associated with 
                               * this ring. This vlanid will be used to tag 
                               * the R-APS messages. */
    UINT2                  u2VlanGroupListCount;
                               /* This field used to represent the bit postion set
                                * by VlanGroupList */
    UINT1                  u1PropagateTC; 
                              /* This field will represent the topology change
                               * propagation enable/disable status. */
    UINT1                  u1OperatingMode;
                              /* This field represent the current mode of 
                               * operation of Ring either as Revertve or 
                               * Non-Revertive. */
    UINT1                  u1RecoveryMethod; 
                              /* This recovery method represent as Auto or 
                               * Manual recovery in non-Revertive mode. */
    UINT1                  u1TcPropCount;
                              /*This field is used to count the number of 
                               * ring entries configured this ring as 
                               * TcPropagate ring id. */    
    UINT1                  u1RowStatus;
                              /* This field represent the RowStatus of the 
                               * Ring. */
    UINT1                  u1PortBlockedOnVcRecovery;
    UINT1                  u1RingMacId;
                              /* This field represents the id of the ring 
                               * instance to be communicated via R-APS PDUs
                               * to other ring nodes. For this ring instance, 
                               * the value of this object forms the last
                               * octet of the destination MAC address field
                               * of the R-APS PDUs.*/
    UINT1                  u1ProtectionType;
                              /* This field specifies the type of protection
                               * being provided by this ring instance. 
                               * Possible values are -
                               * ERPS_PORT_BASED_PROTECTION
                               * ERPS_SERVICE_BASED_PROTECTION
                               */      
    UINT1                  u1RAPSSubRingWithoutVirtualChannel;
                              /*This object is set to OSIX_ENABLED 
                               * when SubRing without RAPS Virtual 
                               * channel is configured ; 
                               * By default this object will be set to 
                               * OSIX_DISABLED (Sub Ring with Virtual Channel */
    UINT1                  u1HighestPriorityRequestWithoutVC;
                              /* This object is used only when Subring
                               * is configured without RAPS Virtual channel.
                               * This object is used to communicate the current
                               * Highest priority request to the 
                               * FsErpsHwRingConfig function by copying it in
                               * the structure ErpsHwRingInfo.
                               * The already existing variable
                               * u1HighestPriorityRequest also serves the same
                               * purpose, but the same cannot be used ,
                               * because that is updated after calling the
                               * State m/c in case of Local FS.
                               * If the variable is updated before calling the
                               * state m/c ( similar to local SF) then the
                               * existing variable can be used and this
                               * variable can be removed. 
                               *
                               * This variable takes below mentioned values 
                               *    - ERPS_RING_LOCAL_SF_REQUEST,
                               *    - ERPS_RING_FORCE_SWITCH_REQUEST
                               *    - ERPS_RING_NO_ACTIVE_REQUEST
                               */

    UINT1                  u1RingPduSend;
                              /* This field keeps a track of whether the
                                 R-APS PDUs should be sent when the Async
                                 NPAPI callback is invoked */
    UINT1                  u1RAPSCompatibleVersion;
                              /* This field represents the version number 
                                 of the ring node in the ring.This field can 
                                 take values 1(ERPS Version 1) and 
                                 2 (ERPS Version 2) */
    UINT1                   u1RingClearOperatingModeType; 
                              /* This field contains the value applied for 
                               * clearing the  operating mode or switch 
                               * method(FS or MS). If it is zero, then 
                               * clear command applied.
                               * If it is non-zero , then clear command 
                               * has been triggered on the node */
    BOOL1                  bDNFStatus;
                              /* This field is added to implement the
                               * Optimized flush feature in ERPSv2 and it
                               * stores the DNF status of the last ring
                               * failure condition. */
    BOOL1     bDNFStatusinRPL;
                              /* This field is used to store the DNF status
                               * of RPL/Neighbor node, when the ring is in IDLE
                               * condition and either RPL/Neighbor receives 
                               * LinkSF and ring moves to protection,and in
                               * PROTECTION state other node (RPL/Neighbor)
                               * receives LocalSF,FLUSH sholud not happen in 
                               * RPL Link,This flag is used to store the node
                               * status and avoides FLUSH. */
    UINT1                  u1RingTxAction;
                              /* This field is added to implement the
                               * Optimized flush feature in ERPSv2, It's
                               * majorly used when R-APS Packet
                               * Transmission has to be done on  Failed Next 
                               * Neighbour port only and not on other port of
                               * the node. But this field can generally be
                               * used to select whether R-APS packets have to
                               * be sent on - Both the ring ports or on one
                               * of the Ring port namely Port 1 or Port 2.  */
    UINT1                  u1FlushLogicSupport;
                              /* This object is set to OSIX_ENABLED 
                               * when flush logic is enabled.
                               * By default this object will be set
                               * to OSIX_ENABLED for ERPSv2.
                               * For ERPSv1 default value is OSIX_DISABLED .*/
    UINT1                  u1RingConfigInterConnNode;
                     /* This object is configured in interconnection 
                               * node of the sub-ring for Minimizing segmentation
                               * in interconnected rings. This object can take the
                               * values ERPS_CONFIG_INTER_CONN_NODE_PRIMARY,
                               * ERPS_CONFIG_INTER_CONN_NODE_SECONDARY and 
                               * ERPS_CONFIG_INTER_CONN_NODE_NONE.
                               * Default value is ERPS_CONFIG_INTER_CONN_NODE_NONE.*/
    UINT1                  u1RingConfigMultipleFailure; 
                     /* This object is configured in interconnection 
                               * node of the sub-ring for Minimizing segmentation
                               * in interconnected rings. This object can take 
                               * the values ERPS_CONFIG_MULTIPLE_FAILURE_PRIMARY,
                               * ERPS_CONFIG_MULTIPLE_FAILURE_SECONDARY and
                               * ERPS_CONFIG_MULTIPLE_FAILURE_DISABLED. Default 
                               * value is ERPS_CONFIG_MULTIPLE_FAILURE_DISABLED.*/
    UINT1                  u1IsPort1Present;
                              /* This field contains the presence of the 
                               * first ring Port in local line card or remote line card. */
    UINT1                  u1IsPort2Present;
                               /* This field contains the presence of the 
                                * second ring Port in local line card or remote line card. */
    UINT1                  u1DERPSSyncReqFlag;
                              /* This field used to check whether calling D-ERPS Sync is required or not.
                               * This field will be 
          *  1. set to OSIX_TRUE when D-ERPS sync is required.
          *     In this case, any dynamic information changes in Local ERP 
          *     will be notified to Remote ERP by D-ERPS Sync PDU.
          *  2. Set to OSIX_FALSE, when D-ERPS sync is not required.
          *     In this case any dynamic information changes in Local ERP 
          *     will not be notified to Remote ERP by D-ERPS Sync PDU.
                               * In Distributed ERPS, this bit will be set to OSIX_FALSE while processing
          * D-ERPS sync PDU for avoiding sync looping between Local and Remote ERPs.
          * So that the synced informations will not trigger D-ERPS sync again. */

         /* The type of service to be protected by this ring group. It can be 
          * ERPS_SERVICE_VLAN - Default service. 
          * ERPS_MPLS_LSP - ERPS to protect LSP. 
          * ERPS_MPLS_PW-ERPS to protect Pseudowire
          * ERPS_MPLS_LSP_PW - ERPS to protect the ring Pseudowires created in an LSP, 
          *                    in addition to the psedowires configured as subPortList.
          */
    UINT1                  u1ServiceType;

    UINT1                  u1MonitoringType;
                             /*This field is to specify if the monitoring mechanism is either
                              *ECFM or MPLS OAM*/
    UINT1                  u1ClearSave;
                             /* This field is used to give the indication to MSR,whether the Clear Object 
                              * needs to be restore or not*/
    UINT1                  u1IsMsgRcvd; 
                             /* Indicates the NodeId of the Ringinfo is updated
                              * by the received RAPS message */
    BOOL1                  bDFOP_TO; 
                             /* This variable used to enable or disbale the
                              * DFOP Feature*/  
    BOOL1                  bFopPMDefect; 
                             /* When defect occured,this flag will be set 
                              * to avoid raising continuous trap on 
                              * reception of NRRB packet from other RPL node 
                              * and will be reset when FOP-PM defect clearence 
                              * trap generated */ 
    BOOL1                  bMsrManualSwitchIndication; 
                             /* This field will be set when manual switch is given and it is 
                              * followed by save and reload, Because during reload SF will 
                              * occur and it will override manual switch applied, in order to 
                              * restore manual switch state after reload this filed is used. */ 
 
    UINT1                 u1RapsFlag;
                             /* Flag to indicate the the SF and ClearSF message received*/ 
   
    UINT1                 u1PortRecoveryFlag ;
                             /* This filed will be set to true when port1 is received
                                clear SF event in the presence of virtual channel failure.
                                If the flag is set as true when CSF event comes for VC,
                                state machine will be triggered with clear SF event. */

    UINT1                  au1Reserved[3];

                            /* This data structure has 2 paddings one before tDbTblNode RingDbNode 
                             * and another at the end of the structure to ensure that ERPS HA works 
                             * properly. If this padding is removed, then please ensure that the 
                             * portion of the structure above tDbTblNode RingDbNode is aligned properly */
    tDbTblNode             RingDbNode;
                              /* Data Base node to sync ring dynamic 
                               * information.*/
    UINT4                  u4ContextId;
                              /* This field will have contextId configured for 
                               * this ring. */
    UINT4                  u4RingId;
                              /* Ring Index will be used by management entity to
                               * identify particular ring. */
    UINT4                  u4RingNodeStatus;
                              /* This variable will hold the current node 
                               * status. Node status represents Port1 and
                               * Port 2, failure status, received Remote 
                               * failure status, RPL block status on RPL
                               * node and WTR, HOLD and GUARD timer status.
                               */ 

    UINT4                  u4SwitchCmdIfIndex;
                              /* This field represents interface index to 
                               * which the given command (Manual switch or 
                               * Force Switch) should be applied. */
    tMacAddr               Port1LastRcvdNodeId; 
                               /* Sender nodeId of received 
                                * SF messages on Port 1 of
                                * ring. */
    UINT2                  u2LastMsgSend;
                              /* This field will record last message type (SF, 
                               * NR, NR with RB or event) send from this 
                               * node. */
    tMacAddr               Port2LastRcvdNodeId; 
                              /* Sender nodeId of received 
                               * SF messages on Port 2 of ring. */
    UINT1                  u1RingState;
                              /* This field will state the ring state machine 
                               * state. */
    UINT1                  u1HighestPriorityRequest;
                              /* This field represent the Highest Priority event
                               * identified by Priority Logic. */
    tErpsPortNodeIdBPRPair Port1StoredNodeIdBPRPair;
                              /* This object stores the (Node ID, BPR) pair 
                               * of the Last R-APS message received at port1 */ 
    tErpsPortNodeIdBPRPair Port2StoredNodeIdBPRPair;
                              /* This object stores the (Node ID, BPR) pair 
                               * of the Last R-APS message received at port2 */

    UINT1                  u1Port1Status;
                              /* This field represent the Port 1 status in 
                               * bitmap for failed/non-failed, 
                               * blocked/unblocked state. 
                               *       Bit 0: failed/non-failed state;
                               *       Bit 1: blocked/unblocked state.
                               */
    UINT1                  u1Port2Status;
                              /* This field represent the Port 2 status in 
                               * bitmap for failed/non-failed, 
                               * blocked/unblocked state. 
                               *       Bit 0: failed/non-failed state;
                               *       Bit 1: blocked/unblocked state.
                               */
    UINT1                  u1Port1HwStatus;
                              /* This field represent the Port 1 status 
                               * programmed in the Hardware, this filed will 
                               * be updated after programming hardware for 
                               * port state. 
                               * This variable indicate either Blocked 
                               * Programmed state or Unblocked Programmed 
                               * state.  */
    UINT1                  u1Port2HwStatus;
                              /* This field represent the Port 2 status 
                               * programmed in the Hardware, this filed will 
                               * be updated after programming hardware for 
                               * port state. 
                               * This variable indicate either Blocked 
                               * Programmed state or Unblocked Programmed 
                               * state.  */
    UINT1                  u1SwitchCmdType;
                              /* This represents the command enforced on ring. 
                               * By default it should be initialized with 
                               * value 1(none command). */
   BOOL1                  bFopDefect; 
                              /* When defect occured,this flag will be set 
                               * This value can be set when TFOP 
                               * defect trap generated and will 
                               * be reset when TFOP defect clearence 
                               * trap generated.*/ 

    UINT1                 u1Pad[2];
}tErpsRingInfo;

typedef struct ErpsQRapsPduMsg
{
    tCRU_BUF_CHAIN_DESC *pRapsPdu;   /* Pointer to the RAPS PDU buffer */
    UINT1                u1TlvOffset;/* Offset of TLV info */
    UINT1                au1Reserved[3];
}tErpsQRapsPduMsg;

/* Async NPAPI callback return value and parameres */
typedef  struct {
    UINT4           u4NpCallId; /* Call Id of the NPAPI for which CB invoked */
    unAsyncNpapi    ErpsAsyncNpCbParams; /* Return value of NPAPI and related 
                                            inputs for handling the callback */
}tErpsNpCbMsg;

typedef struct _sErpsQMsg{
    UINT4                  u4MsgType;
    UINT4                  u4ContextId;
    UINT4                  u4MonitoringType;
    tErpsCfmMepInfo        MepInfo;    /* MegId,MeId and MepId */
    union
    {
        tErpsQRapsPduMsg        RapsPduMsg;
        UINT4                   u4Status;      /* MEP status information*/
#ifdef MBSM_WANTED
        tMbsmProtoMsg           *pMbsmProtoMsg; /* MbsmProtoMsg received from
                                                 MBSM */
#endif
#ifdef L2RED_WANTED
        tErpsRmMsg              RmMsg;
#endif
        tErpsNpCbMsg            ErpsNpCbMsg; /* Callback message
                                                received from Hardware when 
                                                NPAPI mode is Asynchronous */
    }unMsgParam;
}tErpsQMsg;

typedef union _uErpsRingDynInfo{
    tErpsPortNodeIdBPRPair Port1StoredNodeIdBPRPair;
    tErpsPortNodeIdBPRPair Port2StoredNodeIdBPRPair;
    UINT4          u4RingNodeStatus;
    UINT4          u4SwitchCmdIfIndex;
    UINT2          u2LastMsgSend;
    UINT1          u1HighestPriorityRequest;
    UINT1          u1Port1Status;
    UINT1          u1Port2Status;
    UINT1          u1Port1HwStatus;
    UINT1          u1Port2HwStatus;
    UINT1          u1RingState;
    UINT1          u1SwitchCmdType;
    UINT1          u1AlarmStatus;
    UINT1          au1Reserved[2];
}unErpsRingDynInfo;

#endif /*end of __ERPSTDFS_H__*/
