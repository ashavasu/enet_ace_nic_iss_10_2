/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    
 *                                                                         
 * $Id: erpsutil.c,v 1.78 2017/02/22 13:42:19 siva Exp $                   
 *                                                                         
 * Description: This file contains the utility functions for ring Data     
 *              Structures.                                                
 ***************************************************************************/

#include "erpsinc.h"
# include  "fssnmp.h"
#ifdef SNMP_2_WANTED
extern UINT4        FsErpsRingConfigClear[13];
extern UINT4        FsErpsRingConfigSwitchPort[13];
#endif
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilRegisterGetFltAndApply                   */
/*                                                                           */
/*    Description         : This function will register ring MEP with monit- */
/*                          oring protocol and it will get the current link  */
/*                          status. If any failure is present, it will be    */
/*                          reported to ERPS state machine.                  */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When link status are handled succ-*/
/*                                         essfully by ring state machine.   */
/*                          OSIX_FAILURE - When state machine returned       */
/*                                         failure.                          */
/*****************************************************************************/
PUBLIC INT4
ErpsUtilRegisterGetFltAndApply (tErpsRingInfo * pRingInfo)
{
    tEcfmRegParams      ErpsRegParams;
    tEcfmEventNotification ErpsRingMepFltStatus;
    tErpsCfmMepInfo     MepInfo;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;

    /* For Port 1's MEP */
    MEMSET (&ErpsRegParams, 0, sizeof (tEcfmRegParams));

    MEMSET (&ErpsRingMepFltStatus, 0, sizeof (tEcfmEventNotification));
    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));

    ErpsRegParams.u4ModId = ECFM_ERPS_APP_ID;

    /*If monitoring mechanism is MPLS OAM then for MEP monitoring
     *the MEP must be registered with MPLS OAM and for RAPS packet
     *transmission the MEP needs to be registered with ECFM. 
     *If the monitoring mechanism is ECFM then MEP needs to be
     *registered with the ECFM module only for both RAPS packet 
     *transmission and reception as well monitoring*/

    if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_CFM)
    {
        ErpsRegParams.u4EventsId = (ECFM_RAPS_FRAME_RECEIVED |
                                    ECFM_DEFECT_CONDITION_CLEARED |
                                    ECFM_DEFECT_CONDITION_ENCOUNTERED |
                                    ECFM_RDI_CONDITION_ENCOUNTERED |
                                    ECFM_RDI_CONDITION_CLEARED);

        ErpsRegParams.u4VlanInd = ECFM_INDICATION_FOR_ALL_VLANS;
    }
    else if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
    {
        ErpsRegParams.u4EventsId = ECFM_RAPS_FRAME_RECEIVED;
    }

    ErpsRegParams.pFnRcvPkt = ErpsPortExternalEventNotify;
    ErpsRegParams.pAppInfo = pRingInfo->pContext;
    /*The registration of MEPs of ring port with ECFM is done only if
     * the ring port presence in the line card is configured as 'local'*/

    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        ErpsRingMepFltStatus.u4ContextId = pRingInfo->u4ContextId;
        ErpsRingMepFltStatus.u4MdIndex = pRingInfo->CfmEntry.u4Port1MEGId;
        ErpsRingMepFltStatus.u4MaIndex = pRingInfo->CfmEntry.u4Port1MEId;
        ErpsRingMepFltStatus.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port1MEPId;

        /* Failure may return from the register/De-register function due to Mep
         * is not present in the ECFM module. This failure will not be pass on to 
         * state machine as ring non-operational. But ring remain non-operational,
         * the only way to make ring operation is make ring row status as not in 
         * service (inactive), create Mep in the the ECFM and then make ring
         * RowStatus as active again. this will to ask erps state machine to 
         * re-register with ECFM.
         * */
        if (ErpsPortEcfmMepRegAndGetFltStat (&ErpsRegParams,
                                             &ErpsRingMepFltStatus) ==
            ECFM_SUCCESS)
        {
            if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_CFM)
            {
                /*Handle the fault only if the monitoring type is CFM. In the case of MPLSOAM
                   registration is done only for RAPS packet */

                if ((ErpsRingMepFltStatus.u4Event ==
                     ECFM_DEFECT_CONDITION_ENCOUNTERED)
                    || (ErpsRingMepFltStatus.u4Event ==
                        ECFM_RDI_CONDITION_ENCOUNTERED))
                {
                    MepInfo.u4MdId = pRingInfo->CfmEntry.u4Port1MEGId;
                    MepInfo.u4MaId = pRingInfo->CfmEntry.u4Port1MEId;
                    MepInfo.u4MepId = pRingInfo->CfmEntry.u4Port1MEPId;
                    MepInfo.u4IfIndex = pRingInfo->u4Port1IfIndex;
                    MepInfo.VlanId = pRingInfo->RapsVlanId;

                    /* Port1 is already in failed state, do nothing. */
                    if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
                        != ERPS_PORT_FAILED)
                    {
                        if (ErpsMsgProcessLinkSF (pRingInfo, MepInfo) ==
                            OSIX_FAILURE)
                        {
                            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                              pRingInfo->u4RingId,
                                              CONTROL_PLANE_TRC |
                                              ALL_FAILURE_TRC,
                                              "ErpsUtilRegisterGetFltAndApply \
                                           failed in handling the Port1 MEP \
                                           failure status.\r\n");
                            return OSIX_FAILURE;
                        }
                    }
                }
                else if (ErpsRingMepFltStatus.u4Event == 0)
                {
                    /* Port1 was in failed state, apply ClearSF. */
                    if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
                        == ERPS_PORT_FAILED)
                    {
                        if (ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo)
                            == OSIX_FAILURE)
                        {

                            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                              pRingInfo->u4RingId,
                                              CONTROL_PLANE_TRC |
                                              ALL_FAILURE_TRC,
                                              "ErpsUtilRegisterGetFltAndApply failed in\
                                          handling the Port2 ClearSF status.\r\n");

                        }
                    }

                }
            }
        }
        else
        {
            /* if Port1 is recovered from the failed state, apply ClearSF. */
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilRegisterGetFltAndApply failed to "
                              "register mep1 and get existing fault.\r\n");
        }
    }
    /* For Port 2's MEP */

    MEMSET (&ErpsRingMepFltStatus, 0, sizeof (tEcfmEventNotification));
    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));
    /*The registration of MEPs of ring port with ECFM is done only if
     * the ring port presence in the line card is configured as 'local'*/

    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
    {
        ErpsRingMepFltStatus.u4ContextId = pRingInfo->u4ContextId;
        ErpsRingMepFltStatus.u4MdIndex = pRingInfo->CfmEntry.u4Port2MEGId;
        ErpsRingMepFltStatus.u4MaIndex = pRingInfo->CfmEntry.u4Port2MEId;
        ErpsRingMepFltStatus.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port2MEPId;
        ErpsRegParams.pAppInfo = pRingInfo->pContext;

        if (ErpsPortEcfmMepRegAndGetFltStat (&ErpsRegParams,
                                             &ErpsRingMepFltStatus) ==
            ECFM_SUCCESS)
        {
            if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_CFM)
            {
                if ((ErpsRingMepFltStatus.u4Event ==
                     ECFM_DEFECT_CONDITION_ENCOUNTERED)
                    || (ErpsRingMepFltStatus.u4Event ==
                        ECFM_RDI_CONDITION_ENCOUNTERED))
                {
                    MepInfo.u4MdId = pRingInfo->CfmEntry.u4Port2MEGId;
                    MepInfo.u4MaId = pRingInfo->CfmEntry.u4Port2MEId;
                    MepInfo.u4MepId = pRingInfo->CfmEntry.u4Port2MEPId;
                    MepInfo.u4IfIndex = pRingInfo->u4Port2IfIndex;
                    MepInfo.VlanId = pRingInfo->RapsVlanId;
                    /* Get the Direction from ECFM to give the UP-MEP failure
                     * Notification to ERPS. So that Ring will move to
                     * Manual switch state if Minimizing segmentation feature
                     * objects are configured with correct value */
                    MepInfo.u1Direction = ErpsRingMepFltStatus.u1Direction;
                    /* Port2 is already in failed state, do nothing. */
                    if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED)
                        != ERPS_PORT_FAILED)
                    {
                        if (ErpsMsgProcessLinkSF (pRingInfo, MepInfo) ==
                            OSIX_FAILURE)
                        {
                            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                              pRingInfo->u4RingId,
                                              CONTROL_PLANE_TRC |
                                              ALL_FAILURE_TRC,
                                              "ErpsUtilRegisterGetFltAndApply failed \
                                           in handling the Port2 MEP failure \
                                           status.\r\n");
                            return OSIX_FAILURE;
                        }
                    }
                }
                else if (ErpsRingMepFltStatus.u4Event == 0)
                {
                    /* Port1 was in failed state, apply ClearSF. */
                    if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED)
                        == ERPS_PORT_FAILED)
                    {
                        if (ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo)
                            == OSIX_FAILURE)
                        {

                            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                              pRingInfo->u4RingId,
                                              CONTROL_PLANE_TRC
                                              | ALL_FAILURE_TRC,
                                              "ErpsUtilRegisterGetFltAndApply failed \
                                           in handling the Port1 ClearSF \
                                           status.\r\n");

                        }
                    }

                }

            }
        }
        /* Since in subring configuration with monitoring mechanism as MPLS-OAM over servicetype LSP-PW,
        registeration with CFM for RING PORT2 will always fail as dummy mep's are created w.r.t. ring port 2, 
        which skips registeration with L2VPN for pw's.*/
        
        else if ((pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM) &&
			(pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW))
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                               "ErpsUtilRegisterGetFltAndApply failed to "
                              "register mep2 with monitoring configured as MPLS-OAM over service-type LSP-PW."
                              "This may be the case of registering dummy subring mep2\r\n");
        }
        else
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilRegisterGetFltAndApply failed to "
                              "register mep2 and get existing fault.\r\n"); 
            return OSIX_SUCCESS;
        }
    }

    if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
    {
        if ((pInMplsApiInfo = (tMplsApiInInfo *)
             MemAllocMemBlk (gErpsGlobalInfo.MplsApiInInfoPoolId)) == NULL)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilRegisterGetFltAndApply failed to "
                              "allocate memory.\r\n");
            return OSIX_FAILURE;
        }

        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->u4SrcModId = ERPS_MODULE_APP;

        /*Register for the following events */

	if((pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW) &&
		 (pRingInfo->MplsPwInfo.u4PwVcId1 != 0))
	{
	    pInMplsApiInfo->InRegParams.u4Events = (MPLS_PW_UP_EVENT | 
			MPLS_PW_DOWN_EVENT |
			MPLS_PW_IF_DOWN |
			MPLS_PW_IF_UP);
	}
	else
	{
	    pInMplsApiInfo->InRegParams.u4Events = (MPLS_MEG_DOWN_EVENT |
		    MPLS_MEG_UP_EVENT |
		    MPLS_AIS_CONDITION_ENCOUNTERED |
		    MPLS_AIS_CONDITION_CLEARED);
	}
	pInMplsApiInfo->InRegParams.pFnRcvPkt = ErpsApiNotificationRoutine;

        pInMplsApiInfo->InRegParams.u4ModId = MPLS_ERPS_APP_ID;

        pInMplsApiInfo->u4SubReqType = MPLS_APP_REGISTER;

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
	    if((pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW) &&
			(pRingInfo->MplsPwInfo.u4PwVcId1 != 0))
	    {
		pInMplsApiInfo->InPathId.PwId.u4VcId = pRingInfo->MplsPwInfo.u4PwVcId1;
		pInMplsApiInfo->u4ContextId = pRingInfo->u4ContextId;	
		pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;
		pInMplsApiInfo->InPathId.PwId.u4MegIndex =
		    pRingInfo->CfmEntry.u4Port1MEGId;

		pInMplsApiInfo->InPathId.PwId.u4MeIndex =
		    pRingInfo->CfmEntry.u4Port1MEId;

		pInMplsApiInfo->InPathId.PwId.u4MpIndex =
		    pRingInfo->CfmEntry.u4Port1MEPId;

		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_REGISTER_PW_FOR_NOTIF, pInMplsApiInfo,
			 NULL) != OSIX_SUCCESS)
		{
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);
		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "Registration failed for PW,"
			    "VC ID: %d\r\n",
			    pInMplsApiInfo->InPathId.PwId.u4VcId);
		    return OSIX_FAILURE;
		}
	    }
	    else
	    {
		pInMplsApiInfo->InPathId.MegId.u4MegIndex =
		    pRingInfo->CfmEntry.u4Port1MEGId;

		pInMplsApiInfo->InPathId.MegId.u4MeIndex =
		    pRingInfo->CfmEntry.u4Port1MEId;

		pInMplsApiInfo->InPathId.MegId.u4MpIndex =
		    pRingInfo->CfmEntry.u4Port1MEPId;

		pInMplsApiInfo->u4ContextId = pRingInfo->u4ContextId;

		pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo,
			 NULL) != OSIX_SUCCESS)
		{
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);

		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "Registration failed for MEP,"
			    "meg=%d, me=%d, mep=%d\r\n",
			    pInMplsApiInfo->InPathId.MegId.u4MegIndex,
			    pInMplsApiInfo->InPathId.MegId.u4MeIndex,
			    pInMplsApiInfo->InPathId.MegId.u4MpIndex);

		    return OSIX_FAILURE;
		}
	    }
	}

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
	    if((pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW) && 
			(pRingInfo->MplsPwInfo.u4PwVcId2 != 0))
	    {
		pInMplsApiInfo->InPathId.PwId.u4VcId = pRingInfo->MplsPwInfo.u4PwVcId2;
		pInMplsApiInfo->u4ContextId = pRingInfo->u4ContextId;	
		pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;
		pInMplsApiInfo->InPathId.PwId.u4MegIndex =
		    pRingInfo->CfmEntry.u4Port2MEGId;

		pInMplsApiInfo->InPathId.PwId.u4MeIndex =
		    pRingInfo->CfmEntry.u4Port2MEId;

		pInMplsApiInfo->InPathId.PwId.u4MpIndex =
		    pRingInfo->CfmEntry.u4Port2MEPId;
		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_REGISTER_PW_FOR_NOTIF, pInMplsApiInfo,
			 NULL) != OSIX_SUCCESS)
		{
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);
		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "Registration failed for PW,"
			    "VC ID2: %d\r\n",
			    pInMplsApiInfo->InPathId.PwId.u4VcId);
		    return OSIX_FAILURE;
		}
	    }
	    else
	    {     
		pInMplsApiInfo->InPathId.MegId.u4MegIndex =
		    pRingInfo->CfmEntry.u4Port2MEGId;

		pInMplsApiInfo->InPathId.MegId.u4MeIndex =
		    pRingInfo->CfmEntry.u4Port2MEId;

		pInMplsApiInfo->InPathId.MegId.u4MpIndex =
		    pRingInfo->CfmEntry.u4Port2MEPId;

		pInMplsApiInfo->u4ContextId = pRingInfo->u4ContextId;

		pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo,
			 NULL) != OSIX_SUCCESS)
		{
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);

		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "Registration failed for MEP,"
			    "meg=%d, me=%d, mep=%d\r\n",
			    pInMplsApiInfo->InPathId.MegId.u4MegIndex,
			    pInMplsApiInfo->InPathId.MegId.u4MeIndex,
			    pInMplsApiInfo->InPathId.MegId.u4MpIndex);

		    return OSIX_FAILURE;
		}
	    }
	}
	MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
		(UINT1 *) pInMplsApiInfo);

    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilDeRegisterRing                           */
/*                                                                           */
/*    Description         : This function will de-register the ring MEP with */
/*                          monitoring protocol.                             */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsUtilDeRegisterRing (tErpsRingInfo * pRingInfo)
{

    if (pRingInfo == NULL)
    {
        return;
    }

    /*The de-registration of MEPs of ring port with ECFM is done only if
     * the ring port presence in the line card is configured as 'local'*/
    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        if (ErpsUtilDeRegisterRingPort (pRingInfo, pRingInfo->u4Port1IfIndex)
            == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilDeRegisterRing failed to "
                              "de-register Ring port1 from CFM.\r\n");
        }
    }
    /*The de-registration of MEPs of ring port with ECFM is done only if
     * the ring port presence in the line card is configured as 'local'*/
    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
    {
        if (ErpsUtilDeRegisterRingPort (pRingInfo, pRingInfo->u4Port2IfIndex)
            == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilDeRegisterRing failed to "
                              "de-register Ring port2 from CFM.\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilDeRegisterRingPort                       */
/*                                                                           */
/*    Description         : This function will de-register the MEP of a given*/
/*                          Ring Port with monitoring protocol.              */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                          u4PortIndex - Ring Port Index, to which MEP has  */
/*                                        to be de-registered.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_FAILURE/OSIX_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsUtilDeRegisterRingPort (tErpsRingInfo * pRingInfo, UINT4 u4PortIndex)
{
    tEcfmRegParams      ErpsRegParams;
    tEcfmEventNotification ErpsRingMepEntry;

    MEMSET (&ErpsRegParams, 0, sizeof (tEcfmRegParams));
    MEMSET (&ErpsRingMepEntry, 0, sizeof (tEcfmEventNotification));

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    ErpsRegParams.u4ModId = ECFM_ERPS_APP_ID;
    ErpsRingMepEntry.u4ContextId = pRingInfo->u4ContextId;

    /*De-registrer MEPs of ring port1 with ECFM if u4PortIndex is port1IfIndex */
    if ((u4PortIndex == pRingInfo->u4Port1IfIndex)
        && (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present))
    {
        ErpsRingMepEntry.u4MdIndex = pRingInfo->CfmEntry.u4Port1MEGId;
        ErpsRingMepEntry.u4MaIndex = pRingInfo->CfmEntry.u4Port1MEId;
        ErpsRingMepEntry.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port1MEPId;
        ErpsRegParams.pAppInfo = pRingInfo->pContext;

        if (ErpsPortEcfmMepDeRegister (&ErpsRegParams, &ErpsRingMepEntry)
            == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilDeRegisterRingPort failed to "
                              "de-register Ring port1 from CFM.\r\n");
            return OSIX_FAILURE;
        }
    }
    /*De-registrer MEPs of ring port2 with ECFM if u4PortIndex is port2IfIndex */
    else if ((u4PortIndex == pRingInfo->u4Port2IfIndex)
             && (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present))
    {
        ErpsRingMepEntry.u4MdIndex = pRingInfo->CfmEntry.u4Port2MEGId;
        ErpsRingMepEntry.u4MaIndex = pRingInfo->CfmEntry.u4Port2MEId;
        ErpsRingMepEntry.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port2MEPId;
        ErpsRegParams.pAppInfo = pRingInfo->pContext;

        if (ErpsPortEcfmMepDeRegister (&ErpsRegParams,
                                       &ErpsRingMepEntry) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilDeRegisterRingPort failed to "
                              "de-register Ring port2 from CFM.\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilSetPortState                             */
/*                                                                           */
/*    Description         : This function will set the port state of the     */
/*                          given Interface Index.                           */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.               */
/*                          u4IfIndex - Interface Index of which port state  */
/*                                      needs to be set.                     */
/*                          u1PortState - Port state that needs to be set.   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsUtilSetPortState (tErpsRingInfo * pRingInfo, UINT4 u4IfIndex,
                      UINT1 u1PortState)
{
    UINT1              *pSubPortList = NULL;
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4RplOldPortStatus = 0;
    UINT4               u4RplNeighbourOldPortStatus = 0;
    UINT4               u4Index = 0;
    UINT2               u2VlanGroupId = 1;
    UINT2               u2InstanceId = ISS_ALL_STP_INSTANCES;
    UINT1               bResult = 0;
    UINT1               u1CurPortState = 0;
    UINT1               u1Flag = 0;
    UINT1               u1HwUpdatedPort = 0;
    UINT1               bRes = 0;

    if (pRingInfo == NULL)
    {
        return;
    }

    /* D-ERPS Requirement:-
     * Port state is changed only if the port is configured as local,
     * and No action is taken if the port is configured as remote */
    if (((pRingInfo->u4Port1IfIndex == u4IfIndex)
         && (ERPS_PORT_IN_REMOTE_LINE_CARD == pRingInfo->u1IsPort1Present))
        || ((pRingInfo->u4Port2IfIndex == u4IfIndex)
            && (ERPS_PORT_IN_REMOTE_LINE_CARD == pRingInfo->u1IsPort2Present)))
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "%s : This port is not present "
                          "in Local Line card, No action is taken.\r\n",
                          __FUNCTION__);
        return;
    }

    /* Storing RPL old Status */
    if (u4IfIndex == pRingInfo->u4RplIfIndex)
    {
        if (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex)

        {
            u4RplOldPortStatus = pRingInfo->u1Port1Status;
        }
        else

        {
            u4RplOldPortStatus = pRingInfo->u1Port2Status;
        }

    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
	    CONTROL_PLANE_TRC, "%s : Port: %d Requested state:%d\r\n ",
	    __FUNCTION__, u4IfIndex, u1PortState);

    /* Storing RPL-Neighbor old Status */
    if (u4IfIndex == pRingInfo->u4RplNeighbourIfIndex)
    {
        if (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplNeighbourIfIndex)
        {
            u4RplNeighbourOldPortStatus = pRingInfo->u1Port1Status;

        }
        else
        {
            u4RplNeighbourOldPortStatus = pRingInfo->u1Port2Status;

        }

    }
    if (pRingInfo->u4Port1IfIndex == u4IfIndex)
    {
        u1CurPortState =
            (UINT1) ((pRingInfo->u1Port1Status) & (~(ERPS_PORT_FAILED)));

        if (u1CurPortState == u1PortState)
        {
            /*
             * when r-aps without virtual channel is enabled, (sub-ring is 
             * configured to run without r-aps virtual channel), and the
             * top priority request is a local sf or local fs request, then
             * both the traffic channel and r-aps channel are  blocked on
             * the appropriate ring port based on the block/unblock ring portsignal.
             * [reference : section : 10.1.14 of itu-t g.8032/y.1344  (03/2010)
             * standard]
             * So Processing the Request to block the RAPS channel for already
             * blocked port if subring node is configured without VC and Current
             * Top priority request is Local SF/FS.
             */

            if ((u1PortState & ERPS_PORT_BLOCKING) &&
                (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED))
            {

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "Port1 is already blocked, But Ring node is configured"
                                  " for Without VC and Current Top Req is Local SF/ FS so Processing Req.\r\n");

            }
            else if ((pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM) &&
				(pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW))
            {
                 
		/* CASE: if subports are added then simply return will skip blocking of sub-ports
		 *                       So returning if PORT STATE requested is not BLOCKING (VPLS-RES CASE)*/
		if (!(u1PortState & ERPS_PORT_BLOCKING))
	       {
		    return;
		}
            }
            else
            {
                 /* If the newly configured port state is same as already existing,
                 * then simply return */
                 return;
           }
        }

        /* Update the dynamic information */
        RingDynInfo.u1Port1Status = pRingInfo->u1Port1Status;

        if (u1PortState == ERPS_PORT_BLOCKING)
        {
            RingDynInfo.u1Port1Status |= u1PortState;
            pRingInfo->Port1Stats.u4BlockedCount++;

            /* When a port moved to blocked state, then (Node ID, BPR) Pairs
             * on the ring ports are deleted as per Flush Logic Implementation 
             * as per Specification ITU-T G.8032/Y.1344(03/2010 Section 10.1.10. */
            if ((pRingInfo->u1RAPSCompatibleVersion ==
                 ERPS_RING_COMPATIBLE_VERSION2)
                && (pRingInfo->u1FlushLogicSupport == OSIX_ENABLED))
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "Port1 moved to blocked, So Deleting "
                                  "(Node ID, BPR) Pairs on both ring ports.\r\n");
                MEMSET (&(pRingInfo->Port1StoredNodeIdBPRPair), 0,
                        sizeof (tErpsPortNodeIdBPRPair));
                MEMSET (&(pRingInfo->Port2StoredNodeIdBPRPair), 0,
                        sizeof (tErpsPortNodeIdBPRPair));
            }
        }
        else
        {
            RingDynInfo.u1Port1Status &= ~ERPS_PORT_BLOCKING;
            pRingInfo->Port1Stats.u4UnblockedCount++;
        }
        /*Informing the Harfware that the port1 information/status is
         * Changed and take action for port1*/

        u1HwUpdatedPort = PORT1_UPDATED;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT1_STATUS);
    }
    else
    {
        u1CurPortState =
            (UINT1) ((pRingInfo->u1Port2Status) & (~(ERPS_PORT_FAILED)));

        if (u1CurPortState == u1PortState)
        {

            /*
             * when r-aps without virtual channel is enabled, (sub-ring is 
             * configured to run without r-aps virtual channel), and the
             * top priority request is a local sf or local fs request, then
             * both the traffic channel and r-aps channel are  blocked on
             * the appropriate ring port based on the block/unblock ring portsignal.
             * [reference : section : 10.1.14 of itu-t g.8032/y.1344  (03/2010)
             * standard]
             * So Processing the Request to block the RAPS channel for already
             * blocked port if subring node is configured without VC and Current
             * Top priority request is Local SF/FS.
             */

            if ((u1PortState & ERPS_PORT_BLOCKING) &&
                (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED))
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "Port2 is already blocked, But Ring node is configured"
                                  " for Without VC and Current Top Req is Local SF/ FS so Processing Req.\r\n");
            }
            else if ((pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM) &&
				(pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW))
            {

		/* CASE: By Default initialisation of Ring ports 
		 * is 0 (i.e. UnBlocking) due to which flow gets returned without requesting L2IWF to mark the state
		 * of PW Interfaces as forwarding, causing drops in packet so not returning 
		 * with monitoring: MPLS-OAM and service type:LSP_PW (VPLS-RESLIENCY CASE)*/
		 ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "Port2 is ublocked, and Ring node is configured"
                                  " for Without VC and Current Top Req is Local SF/ FS so Processing Req.\r\n");
            }
            else
            {
                 /* If the newly configured port state is same as already existing,
                 * then simply return */
                 return;
            }
        }

        /* Update the dynamic information */
        RingDynInfo.u1Port2Status = pRingInfo->u1Port2Status;

        if (u1PortState == ERPS_PORT_BLOCKING)
        {
            RingDynInfo.u1Port2Status |= u1PortState;
            pRingInfo->Port2Stats.u4BlockedCount++;

            /* When a port moved to blocked state, then (Node ID, BPR) Pairs
             * on the ring ports are deleted as per Flush Logic Implementation 
             * as per Specification ITU-T G.8032/Y.1344(03/2010 Section 10.1.10. */
            if ((pRingInfo->u1RAPSCompatibleVersion ==
                 ERPS_RING_COMPATIBLE_VERSION2)
                && (pRingInfo->u1FlushLogicSupport == OSIX_ENABLED))
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "Port2 moved to blocked, So Deleting "
                                  "(Node ID, BPR) Pairs on both ring ports.\r\n");
                MEMSET (&(pRingInfo->Port1StoredNodeIdBPRPair), 0,
                        sizeof (tErpsPortNodeIdBPRPair));
                MEMSET (&(pRingInfo->Port2StoredNodeIdBPRPair), 0,
                        sizeof (tErpsPortNodeIdBPRPair));
            }
        }
        else
        {
            RingDynInfo.u1Port2Status &= ~ERPS_PORT_BLOCKING;
            pRingInfo->Port2Stats.u4UnblockedCount++;
        }
        /*Informing the Harfware that the port2 information/status is
         * Changed and take action for port2*/
        u1HwUpdatedPort = PORT2_UPDATED;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT2_STATUS);

    }

    /* If ring interface is zero, avoid setting port state in L2Iwf or in 
     * Hardware. but ERPS database will be reflecting logical status of the 
     * port2. (here port2 will represent UP MEP status, in case of subring) */

    if (u4IfIndex == 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsUtilSetPortState called for "
                          "Port2 of sub-Ring.\r\n");
        return;
    }

    /* Convert the Port State so that L2IWF can understand it */

    u1CurPortState = ((u1PortState & ERPS_PORT_BLOCKING) ?
                      ERPS_PORT_STATE_BLOCKING : ERPS_PORT_STATE_UNBLOCKING);

    if (pRingInfo->u1ProtectionType == ERPS_SERVICE_BASED_PROTECTION)
    {
        /* Check whether single vlan group exists or
         *  list of vlan groups exists.If the condition is satisfied
         *  then only one vlan group exist */
        if (pRingInfo->u2VlanGroupListCount == 0)
        {
            u2InstanceId = pRingInfo->u2ProtectedVlanGroupId;

            /* This will be applicable for port based as well as service based 
             * protection mode */
            if (ErpsPortL2IwfSetInstPortState (u2InstanceId,
                                               u4IfIndex,
                                               u1CurPortState) == L2IWF_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsUtilSetPortState setting port state for "
                                  "Port %d failed.\r\n", u4IfIndex);
                return;
            }
        }
        else
        {
            for (; u2VlanGroupId <= pRingInfo->u2VlanGroupListCount;
                 u2VlanGroupId++)
            {

                OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)),
                                         u2VlanGroupId,
                                         sizeof (tErpsVlanGroupList),
                                         bResult) if (bResult == OSIX_FALSE)
                {
                    /* If the bit is not set,
                     * continue from the beginning of the for loop */
                    continue;
                }
                /* If the bit is set, assign the
                 * corresponding vlan group id to instance id */
                u2InstanceId = u2VlanGroupId;

                /* This will be applicable for port based as well as service based 
                 * protection mode */
                if (ErpsPortL2IwfSetInstPortState (u2InstanceId,
                                                   u4IfIndex,
                                                   u1CurPortState) ==
                    L2IWF_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsUtilSetPortState setting port state for "
                                      "Port %d failed.\r\n", u4IfIndex);
                    return;
                }

            }
        }
    }
    else
    {
        ERPS_ALLOC_SUBPORT_LIST_MEM_BLOCK (pSubPortList);
        if (pSubPortList == NULL)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilSetPortState"
                              "Failed to allocate memory.\r\n");
            return;
        }
        MEMSET (pSubPortList, 0, sizeof (tErpsSubPortList));

        u2InstanceId = ISS_ALL_STP_INSTANCES;

        /* This will be applicable for port based as well as service based 
         * protection mode */
        if (ErpsPortL2IwfSetInstPortState (u2InstanceId,
                                           u4IfIndex,
                                           u1CurPortState) == L2IWF_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilSetPortState setting port state for "
                              "Port %d failed.\r\n", u4IfIndex);
            ERPS_RELEASE_SUBPORT_LIST_MEM_BLOCK (pSubPortList);
            return;
        }

        if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
        {
            /*Check if the port state to be modified is for 
             *Ring Port1 or for the Ring Port2*/

            if (u4IfIndex == pRingInfo->u4Port1IfIndex)
            {
                ERPS_GET_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo,
                                        pSubPortList);
            }
            else
            {
                ERPS_GET_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo,
                                        pSubPortList);
            }

            /*When the port state of the RAPS pseudowire is modified the
             *port state of the corresponding protected data pseudowires
             *also need to be modified*/

            for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
            {
                OSIX_BITLIST_IS_BIT_SET (pSubPortList, u4Index,
                                         sizeof (tErpsSubPortList), bRes);

                if (bRes == OSIX_TRUE)
                {
                    if (ErpsPortL2IwfSetInstPortState (u2InstanceId,
                                                       (u4Index +
                                                        CFA_MIN_PSW_IF_INDEX
                                                        - 1),
                                                       u1CurPortState) ==
                        L2IWF_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC |
                                          ALL_FAILURE_TRC,
                                          "ErpsUtilSetPortState setting port state for "
                                          "Port %d failed.\r\n",
                                          (u4IfIndex +
                                           CFA_MIN_PSW_IF_INDEX - 1));
                        ERPS_RELEASE_SUBPORT_LIST_MEM_BLOCK (pSubPortList);
                        return;
                    }
                }
            }

        }
        ERPS_RELEASE_SUBPORT_LIST_MEM_BLOCK (pSubPortList);
    }
    /* As per the standard [Rec. ITU-T G.8021/Y.1341 (2010)/Amd.1 (07/2011)]
     * When TFOP feature is enabled and Subring is configured Without VC,
     * if the interconnected node is administratively disabled, FOP defect
     * encountered alarm should not be raised. */
    if((pRingInfo->bDFOP_TO == OSIX_ENABLED) &&
       ((pRingInfo->u4Port2IfIndex == 0)||
         (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)))
    {
        if (u1PortState & ERPS_PORT_BLOCKING)
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
        }
        else
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                               ((pRingInfo->f4KValue) *
                                (pRingInfo->u4PeriodicTimerValue)));
        }
    }

    /* Updating port state in the Hardware */
    if (ErpsUtilConfRingEntryInHw (pRingInfo, ERPS_HW_RING_MODIFY,
                                   u1HwUpdatedPort) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsUtilSetPortState hardware configuration for "
                          "port %d port state failed.\r\n", u4IfIndex);
        return;
    }
    /* Checking the Rpl Changed status for Measuring the Performance */
    /*If port is RPL port and RPL Port status change happend set the flag */

    if ((u4IfIndex == pRingInfo->u4RplIfIndex) &&
        (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex))
    {
        if (((u4RplOldPortStatus == ERPS_PORT_BLOCKING) &&
             (pRingInfo->u1Port1Status == ERPS_PORT_UNBLOCKING)) ||
            ((u4RplOldPortStatus == ERPS_PORT_UNBLOCKING) &&
             (pRingInfo->u1Port1Status == ERPS_PORT_BLOCKING)))

        {
            u1Flag = ERPS_RPL_PORT_STATUS_CHANGE_TIME;

        }
    }

    if ((u4IfIndex == pRingInfo->u4RplIfIndex) &&
        (pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex))
    {
        if (((u4RplOldPortStatus == ERPS_PORT_BLOCKING) &&
             (pRingInfo->u1Port2Status == ERPS_PORT_UNBLOCKING)) ||
            ((u4RplOldPortStatus == ERPS_PORT_UNBLOCKING) &&
             (pRingInfo->u1Port2Status == ERPS_PORT_BLOCKING)))

        {
            u1Flag = ERPS_RPL_PORT_STATUS_CHANGE_TIME;

        }

    }

    /* This Condition will hit only,Compatible Version V2 */
    /*If port is RPL Neighbor port and Port status change happend measure the System Time */
    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        if ((u4IfIndex == pRingInfo->u4RplNeighbourIfIndex) &&
            (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplNeighbourIfIndex))
        {
            if (((u4RplNeighbourOldPortStatus == ERPS_PORT_BLOCKING) &&
                 (pRingInfo->u1Port1Status == ERPS_PORT_UNBLOCKING)) ||
                ((u4RplNeighbourOldPortStatus == ERPS_PORT_UNBLOCKING) &&
                 (pRingInfo->u1Port1Status == ERPS_PORT_BLOCKING)))

            {
                u1Flag = ERPS_RPL_NBR_PORT_STATUS_CHANGE_TIME;
            }
        }

        if ((u4IfIndex == pRingInfo->u4RplNeighbourIfIndex) &&
            (pRingInfo->u4Port2IfIndex == pRingInfo->u4RplNeighbourIfIndex))
        {
            if (((u4RplNeighbourOldPortStatus == ERPS_PORT_BLOCKING) &&
                 (pRingInfo->u1Port2Status == ERPS_PORT_UNBLOCKING)) ||
                ((u4RplNeighbourOldPortStatus == ERPS_PORT_UNBLOCKING) &&
                 (pRingInfo->u1Port2Status == ERPS_PORT_BLOCKING)))

            {
                u1Flag = ERPS_RPL_NBR_PORT_STATUS_CHANGE_TIME;
            }

        }
    }

    /* Call the Function to measure the RPL Port change and Rpl-Nbr Port change */
    ErpsUtilMeasureTime (pRingInfo, 0, u1Flag);
    /* Updation of Hw status should not be done for Async mode here.
       It should rather be done in the callback function depending on
       the Async NPAPI return status. So the following check is added */
    /* Updating the port status */
    if (ErpsPortIssGetAsyncMode (ERPS_MODULE) == ISS_NPAPI_MODE_SYNCHRONOUS)
    {
        if (pRingInfo->u4Port1IfIndex == u4IfIndex)
        {
            /* Updating Hardware Status flag */
            RingDynInfo.u1Port1HwStatus = u1CurPortState;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT1_HW_STATUS);
        }
        else
        {
            /* Updating Hardware Status flag */
            RingDynInfo.u1Port2HwStatus = u1CurPortState;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT2_HW_STATUS);
        }
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsUtilSetPortState : Port %d is "
                      "set to %s state.\r\n", u4IfIndex,
                      ERPS_PORT_STATE (u1PortState));

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilFlushFdbTable                            */
/*                                                                           */
/*    Description         : This function will be used to Vlan Fdb flush     */
/*                          routing and in case of interconnected node, this */
/*                          flush indication will be propagated through event*/
/*                          messages.                                        */
/*                                                                           */
/*    Input(s)            : pRingInfo     - Pointer to ring node.           */
/*                          i4OptimizeFlag - This flag will be used to       */
/*                                           indicate that do not flush, if  */
/*                                           it is already done in last 10ms.*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsUtilFlushFdbTable (tErpsRingInfo * pRingInfo, INT4 i4OptimizeFlag)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4CurrentTime = 0;
    UINT2               u2VlanGroupId = 1;
    UINT1               bResult = 0;
    UINT1 u1McLagStatus = (UINT1) ErpsUtilGetMcLagStatus ();


    if (pRingInfo == NULL)
    {
        return;
    }
    /* Fdb flush should be invoked from the Active node only */
    if (ErpsPortRmGetNodeState () == RM_STANDBY)
    {
        return;
    }

    MEMSET (&RingDynInfo, 0, sizeof (unErpsRingDynInfo));
    /* Find the first topology change reception node and send 
     * topology change event messages. If topology propgation
     * is disabled, event messages will not be propagated by 
     * the ring node.
     * The topology change propagation can be enabled only on sub-ring
     * interconnected node. This configuration restriction will
     * take care of event messages propagation happaning only in 
     * interconnected node alone. */

    if (i4OptimizeFlag == ERPS_OPTIMIZE)
    {
        ERPS_GET_TIME_STAMP (&u4CurrentTime);

        if ((u4CurrentTime - pRingInfo->u4FlushTime) < ERPS_FLUSH_INTERVAL_TIME)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsUtilFlushFdbTable flush "
                              "was done in last 10ms, so dont flush again\r\n");
            return;
        }

        ERPS_GET_TIME_STAMP (&(pRingInfo->u4FlushTime));
    }

    if (pRingInfo->u1PropagateTC == OSIX_ENABLED)
    {
        UtlDynSllWalk (pRingInfo->TcPropRingList, ErpsUtilDelFdbAndTxRaps);
    }

#ifdef ICCH_WANTED
     /* When ERPS is doing FDB Flushing in MCLAG Enabled Node,
      * call the ICCH API to disable FDB Sync Up
      * Start the FDB Sync timer only when ERPS disabling is not in progress*/
     if ((u1McLagStatus == OSIX_SUCCESS) && (gu1ErpsDisableInProgress == FALSE))
     {
         VlanApiSetFdbFreezeStatus (VLAN_DISABLED);
         /*Start the FDB Sync up timer */
         ErpsTmrStartTimer(pRingInfo,ERPS_ICCH_FDB_SYNC_TMR,ERPS_ICCH_FDB_SYNC_TMR_DEF_VAL);
     }
#else
    UNUSED_PARAM (u1McLagStatus);
 #endif

    /* Check whether single vlan group exists or list of vlan groups exists
     * If the condition is satisfied then only one vlan group exist */
    if (pRingInfo->u2VlanGroupListCount == 0)
    {
        /* Call the function with single vlan group id */
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            ErpsPortVlanDeleteFdbEntries (pRingInfo->u4ContextId,
                                          pRingInfo->u4Port1IfIndex,
                                          pRingInfo->u1ProtectionType,
                                          pRingInfo->u2ProtectedVlanGroupId,
                                          pRingInfo->pContext,
                                          pRingInfo->pErpsLspPwInfo);
        }
        /* Call the function with single vlan group id */
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            ErpsPortVlanDeleteFdbEntries (pRingInfo->u4ContextId,
                                          pRingInfo->u4Port2IfIndex,
                                          pRingInfo->u1ProtectionType,
                                          pRingInfo->u2ProtectedVlanGroupId,
                                          pRingInfo->pContext,
                                          pRingInfo->pErpsLspPwInfo);

        }
    }
    else
    {
        /* Scanning the vlan group list bitmap to identify
         *  the vlan group id's mapped to the ring */
        for (; u2VlanGroupId <= pRingInfo->u2VlanGroupListCount;
             u2VlanGroupId++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)),
                                     u2VlanGroupId, sizeof (tErpsVlanGroupList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                continue;
            }
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                /* If the bit is set, call the function with 
                 * the corresponding vlan group id */
                ErpsPortVlanDeleteFdbEntries (pRingInfo->u4ContextId,
                                              pRingInfo->u4Port1IfIndex,
                                              pRingInfo->u1ProtectionType,
                                              u2VlanGroupId,
                                              pRingInfo->pContext, NULL);
            }
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                /* If the bit is set, call the function with 
                 * the corresponding vlan group id */
                ErpsPortVlanDeleteFdbEntries (pRingInfo->u4ContextId,
                                              pRingInfo->u4Port2IfIndex,
                                              pRingInfo->u1ProtectionType,
                                              u2VlanGroupId,
                                              pRingInfo->pContext, NULL);
            }
        }
    }
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_FLUSH_FDB);
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsUtilFlushFdbTable flush "
                      "completed.\r\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsUtilIsErpsEnabled
 *
 * DESCRIPTION      : This function will check whether ERPS is enabled in the 
 *                    context. If enabled, this function will return OSIX_TRUE, 
 *                    OSIX_FALSE otherwise.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC INT4
ErpsUtilIsErpsEnabled (UINT4 u4ContextId)
{
    if (u4ContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        return OSIX_FALSE;
    }

    if (gErpsGlobalInfo.apRingContextInfo[u4ContextId] == NULL)
    {
        /* ERPS is not initialized in this context */
        return OSIX_FALSE;
    }

    return ((gErpsGlobalInfo.apRingContextInfo[u4ContextId]->u1ModuleStatus
             == OSIX_ENABLED) ? OSIX_TRUE : OSIX_FALSE);
}

/***************************************************************************
 * FUNCTION NAME    : ErpsUtilIsErpsStarted
 *
 * DESCRIPTION      : This function will check whether ERPS is started in the 
 *                    context. If started, this function will return OSIX_TRUE, 
 *                    OSIX_FALSE otherwise.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC INT4
ErpsUtilIsErpsStarted (UINT4 u4ContextId)
{
    if (u4ContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        return OSIX_FALSE;
    }

    if (gErpsGlobalInfo.apRingContextInfo[u4ContextId] == NULL)
    {
        /* ERPS is not initialized in this context */
        return OSIX_FALSE;
    }

    return ((gau1ErpsSysCtrlStatus[u4ContextId]
             == ERPS_START) ? OSIX_TRUE : OSIX_FALSE);
}

/***************************************************************************
 * FUNCTION NAME    : ErpsRapsTxDbLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion R-APS TX database 
 *                    semaphore
 *
 * INPUT            : NONE
 *  
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ErpsUtilRapsTxDbLock (VOID)
{
    if (OsixSemTake (gErpsGlobalInfo.RapsTxInfoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsRapsTxDbUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion R-APS TX 
 *                    database semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ErpsUtilRapsTxDbUnLock (VOID)
{
    if (OsixSemGive (gErpsGlobalInfo.RapsTxInfoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilUpdateRingDynInfo                        */
/*                                                                           */
/*    Description         : This function will be used to update dynamic info*/
/*                          of given ring node.                              */
/*                                                                           */
/*    Input(s)            : pRingInfo     - Pointer to ring node.            */
/*                          pRingDynInfo - Pointer to dynamic info that      */
/*                                         needs to be updated in the ring.  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsUtilUpdateRingDynInfo (tErpsRingInfo * pRingInfo,
                           unErpsRingDynInfo * pRingDynInfo,
                           UINT1 u1DynInfoType)
{
    if (pRingInfo == NULL)
    {
        return;
    }
    switch (u1DynInfoType)
    {
        case ERPS_RING_LAST_MSG_TYPE:
            if (pRingInfo->u4DistIfIndex == 0)
            {
                if (pRingInfo->u2LastMsgSend == pRingDynInfo->u2LastMsgSend)
                {
                    return;
                }
            }
            pRingInfo->u2LastMsgSend = pRingDynInfo->u2LastMsgSend;
            break;

        case ERPS_RING_PRIO_REQUEST:
            if (pRingInfo->u1HighestPriorityRequest
                == pRingDynInfo->u1HighestPriorityRequest)
            {
                return;
            }
            pRingInfo->u1HighestPriorityRequest
                = pRingDynInfo->u1HighestPriorityRequest;
            break;

        case ERPS_RING_PORT1_STATUS:
            pRingInfo->u1Port1Status = pRingDynInfo->u1Port1Status;
            break;

        case ERPS_RING_PORT2_STATUS:
            pRingInfo->u1Port2Status = pRingDynInfo->u1Port2Status;
            break;

        case ERPS_RING_PORT1_HW_STATUS:
            pRingInfo->u1Port1HwStatus = pRingDynInfo->u1Port1HwStatus;
            break;

        case ERPS_RING_PORT2_HW_STATUS:
            pRingInfo->u1Port2HwStatus = pRingDynInfo->u1Port2HwStatus;
            break;

        case ERPS_RING_STATE:
            pRingInfo->u1RingState = pRingDynInfo->u1RingState;

            if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_NONE)
            {
                ErpsTrapSendTrapNotifications (pRingInfo,
                                               ERPS_TRAP_AUTO_SWITCH);
            }
            else if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)
            {
                ErpsTrapSendTrapNotifications (pRingInfo,
                                               ERPS_TRAP_MANUAL_SWITCH);
            }
            else if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_FORCE)
            {
                ErpsTrapSendTrapNotifications (pRingInfo,
                                               ERPS_TRAP_FORCE_SWITCH);
            }
            break;

        case ERPS_RING_NODE_STATUS:
            if (pRingInfo->u4RingNodeStatus == pRingDynInfo->u4RingNodeStatus)
            {
                return;
            }

            pRingInfo->u4RingNodeStatus = pRingDynInfo->u4RingNodeStatus;

            break;

        case ERPS_RING_PORT1_RCVD_NODE_ID:
            if (!MEMCMP
                (pRingInfo->Port1LastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                 sizeof (tMacAddr)))
            {
                return;
            }
            MEMCPY (pRingInfo->Port1LastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                    sizeof (tMacAddr));
            break;

        case ERPS_RING_PORT2_RCVD_NODE_ID:
            if (!MEMCMP
                (pRingInfo->Port2LastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                 sizeof (tMacAddr)))
            {
                return;
            }
            MEMCPY (pRingInfo->Port2LastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                    sizeof (tMacAddr));
            break;
        case ERPS_RING_CMD_TYPE:
            if (pRingInfo->u1SwitchCmdType == pRingDynInfo->u1SwitchCmdType)
            {
                return;
            }
            pRingInfo->u1SwitchCmdType = pRingDynInfo->u1SwitchCmdType;
            break;
        case ERPS_RING_CMD_INDEX:
            if (pRingInfo->u4SwitchCmdIfIndex ==
                pRingDynInfo->u4SwitchCmdIfIndex)
            {
                return;
            }
            pRingInfo->u4SwitchCmdIfIndex = pRingDynInfo->u4SwitchCmdIfIndex;
            break;
        case ERPS_RING_FOP_DEFECT:
            if (pRingDynInfo->u1AlarmStatus & ERPS_FOP_DEFECT)
            {
                pRingInfo->bFopDefect = ERPS_FOP_TO_DEFECT;
            }
            else
            {
                pRingInfo->bFopDefect = ERPS_INIT_VAL;
            }
            break;

        case ERPS_RING_PORT1_BPR_PAIR_NODE_ID:

            MEMCPY (pRingInfo->Port1StoredNodeIdBPRPair.NodeId,
                    pRingDynInfo->Port1StoredNodeIdBPRPair.NodeId,
                    sizeof (tMacAddr));

            pRingInfo->Port1StoredNodeIdBPRPair.u1BPRStatus =
                pRingDynInfo->Port1StoredNodeIdBPRPair.u1BPRStatus;

            break;

        case ERPS_RING_PORT2_BPR_PAIR_NODE_ID:

            MEMCPY (pRingInfo->Port2StoredNodeIdBPRPair.NodeId,
                    pRingDynInfo->Port2StoredNodeIdBPRPair.NodeId,
                    sizeof (tMacAddr));

            pRingInfo->Port2StoredNodeIdBPRPair.u1BPRStatus =
                pRingDynInfo->Port2StoredNodeIdBPRPair.u1BPRStatus;

            break;

        default:
            break;
    }

    ErpsRedUpdateSyncTable (pRingInfo);

    /* D-ERPS Implementation.
     * Synch Up Routine D-ERPS specific fields with Remote ERP */
    ErpsDsyncUpdateDERPSRingDynInfo (pRingInfo, u1DynInfoType);

    ErpsRedSyncRingDynInfo ();
    return;
}

/*****************************************************************************/
/*    Function Name       : ErpsUtilCompareNodeIdBprPair                     */
/*                                                                           */
/*    Description         : This function is used to compare two             */
/*                         (NodeId, BPR) pairs.                              */
/*                                                                           */
/*    Input(s)            : pNodeIdBprPairt1 - Pointer to first              */
/*                          (NodeID, BPR) pair to be compared.               */
/*                          pNodeIdBprPairt2 - Pointer to Second             */
/*                          (NodeID, BPR) pair to be compared.               */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : - ERPS_NODE_ID_BPR_PAIRS_MATCH - When            */
/*                            (NodeId, BPR) pairs are same.                  */
/*                          -  ERPS_NODE_ID_BPR_PAIRS_DIFFER -When           */
/*                            (NodeId, BPR) pairs are different.             */
/*****************************************************************************/
PUBLIC INT4
ErpsUtilCompareNodeIdBprPair (tErpsPortNodeIdBPRPair * pNodeIdBPRPair1,
                              tErpsPortNodeIdBPRPair * pNodeIdBPRPair2)
{
    if ((pNodeIdBPRPair1->u1BPRStatus != pNodeIdBPRPair2->u1BPRStatus) ||
        ((MEMCMP (pNodeIdBPRPair1->NodeId, pNodeIdBPRPair2->NodeId,
                  sizeof (tMacAddr))) != 0))
    {
        return ERPS_NODE_ID_BPR_PAIRS_DIFFER;
    }

    return ERPS_NODE_ID_BPR_PAIRS_MATCH;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsUtilConfRingEntryInHw                        */
/*                                                                           */
/*    Description         : This function will be configure ring entry   into*/
/*                          hardware.                                        */
/*                                                                           */
/*    Input(s)            : pRingInfo     - Pointer to ring node.            */
/*                          u1RingStatus - Ring status to intimate NP layer  */
/*                                         to do appropriate action.         */
/*                                         - ERPS_HW_RING_CREATE,            */
/*                                         - ERPS_HW_RING_MODIFY,            */
/*                                         - ERPS_HW_RING_DELETE             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When hardware successfully updated*/
/*                                         for ring info.                    */
/*                          OSIX_FAILURE - When NP Layer failed to update    */
/*                                         ring info.                        */
/*****************************************************************************/
PUBLIC INT4
ErpsUtilConfRingEntryInHw (tErpsRingInfo * pRingInfo, UINT1 u1RingStatus,
                           UINT1 u1HwUpdatedPort)
{
#ifdef NPAPI_WANTED
    tErpsHwRingInfo     ErpsHwRingInfo;
    UINT2               u2VlanGroupId = 1;
    UINT1               bResult = 0;
    INT4                i4RetVal = 0;
    UINT2               u2RAPSGroupId = 0;
    UINT4               u4PwIfIndex = 0;
    UINT4               u4PwId = 0;
    UINT4               u4Index = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    u2RAPSGroupId = ErpsL2wrMiGetVlanInstMapping(pRingInfo->u4ContextId,
                 pRingInfo->RapsVlanId);

    MEMSET (&ErpsHwRingInfo, 0, sizeof (ErpsHwRingInfo));

    ErpsHwRingInfo.u4ContextId = pRingInfo->u4ContextId;
    ErpsHwRingInfo.u4Port1IfIndex = pRingInfo->u4Port1IfIndex;
    ErpsHwRingInfo.u4Port2IfIndex = pRingInfo->u4Port2IfIndex;
    ErpsHwRingInfo.u4RingId = pRingInfo->u4RingId;
    ErpsHwRingInfo.VlanId = pRingInfo->RapsVlanId;
    ErpsHwRingInfo.u2VlanGroupId = pRingInfo->u2ProtectedVlanGroupId;
    ErpsHwRingInfo.u1ProtectionType = pRingInfo->u1ProtectionType;
    ErpsHwRingInfo.u1HwUpdatedPort = u1HwUpdatedPort;
    ErpsHwRingInfo.u1UnBlockRAPSChannel =
        pRingInfo->u1RAPSSubRingWithoutVirtualChannel;
    ErpsHwRingInfo.u1HighestPriorityRequest =
        pRingInfo->u1HighestPriorityRequestWithoutVC;
    ErpsHwRingInfo.u1Service = pRingInfo->u1ServiceType;
    ErpsHwRingInfo.u2RAPSGroupId = u2RAPSGroupId;

    if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
    {

        if ((u1RingStatus == ERPS_HW_RING_CREATE) ||
            (u1RingStatus == ERPS_HW_RING_MODIFY)||
            (u1RingStatus == ERPS_HW_RING_DELETE))
        {
            if ((pInMplsApiInfo = (tMplsApiInInfo *)
                 MemAllocMemBlk (gErpsGlobalInfo.MplsApiInInfoPoolId)) == NULL)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsUtilConfRingEntryInHw"
                                  "Failed to allocate memory.\r\n");

                return OSIX_FAILURE;
            }

            if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
                 MemAllocMemBlk (gErpsGlobalInfo.MplsApiOutInfoPoolId)) == NULL)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsUtilConfRingEntryInHw "
                                  "Failed to allocate memory.\r\n");
                MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
                                    (UINT1 *) pInMplsApiInfo);

                return OSIX_FAILURE;
            }

            for (u4Index = 1; u4Index <= ERPS_SUBPORTS_PLUS_RINGPORTS;
                 u4Index++)
            {
                MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                /*Ring ports Port1 and Port2 will belong to the same VPLS 
                   and will have same VPN Id. So it is enough to retrieve the 
                   VPN Id of one ring port and store it. */

                /*Ring Port index is available in pRingInfo->u4Port1IfIndex */

                if (u4Index == (ERPS_SUBPORTS_PLUS_RINGPORTS - 1))
                {
                    /*Pseudowire Interface is directly available in the
                     * pRingInfo->u4Port1IfIndex variable. Hence adjustment
                     * is not neccessary*/

                    u4PwIfIndex = pRingInfo->u4Port1IfIndex;
                }
                else if (u4Index == ERPS_SUBPORTS_PLUS_RINGPORTS)
                {
                    if (pRingInfo->u4Port2IfIndex != 0)
                    {
                        u4PwIfIndex = pRingInfo->u4Port2IfIndex;
                    }
                    else
                    {
                        continue;
                    }

                }
                else
                {
                    /* Loop through the Subport List and retrieve VPN Id for each
                     * pseudowire*/

                    /* Assumption: First pseudowire in the subportlist1 and first
                     * pseudowire in the subportlist2 will belong to the same VPN*
                     * The same is applicable for other pseudowires.*/

                    ERPS_IS_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo, u4Index,
                                           bResult);

                    if (bResult == OSIX_FALSE)
                    {
                        /* If the bit is not set, see if the bit is set in
                         * subportlist 2*/

                        ERPS_IS_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo,
                                               u4Index, bResult);

                        if (bResult == OSIX_FALSE)
                        {
                            continue;
                        }
                    }
                    /* This adjustment has been done since in the list First bit
                     * would correspond to first pseudowire and the interface index
                     * value needs to be adjusted*/

                    u4PwIfIndex = (u4Index + CFA_MIN_PSW_IF_INDEX - 1);
                }

                /*Fetch the pseudowire value from the pseudowire index
                 *by calling MPLS API*/

                i4RetVal =
                    ErpsPortL2VpnApiGetPwIndexFromPwIfIndex (u4PwIfIndex,
                                                             &u4PwId);

                if (i4RetVal != OSIX_TRUE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsUtilConfRingEntryInHw"
                                      "Pseudowire Id not present for the pseudowire interface.\r\n");

                    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
                                        (UINT1 *) pOutMplsApiInfo);
         		    continue;
                    return OSIX_FAILURE;
                }

                pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwId;
                pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;

                /*Fetch the VPN Id for the corresponding Pseudowire Id
                 * by calling external MPLS API*/

                if (ErpsPortMplsApiHandleExtRequest
                    (MPLS_GET_PW_INFO, pInMplsApiInfo,
                     pOutMplsApiInfo) != OSIX_SUCCESS)
                {
                    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
                                        (UINT1 *) pOutMplsApiInfo);
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsUtilConfRingEntryInHw"
                                      "Fetching VPLS entry for pseudowire failed");

                    return OSIX_FAILURE;
                }

                /*Registration needs to be done for the RAPS pseudowire
                 *and the corresponding data pseudowires. This informs
                 *the MPLS module that the ownership of these pseudowires belong
                 *to the ERPS module*/
                if(u1RingStatus != ERPS_HW_RING_MODIFY)
                {
                    pInMplsApiInfo->u4SrcModId = ERPS_MODULE_APP;

                    if (u1RingStatus == ERPS_HW_RING_CREATE)
                    {
                        pInMplsApiInfo->u4SubReqType = MPLS_APP_REGISTER;
                    }
                    if (u1RingStatus == ERPS_HW_RING_DELETE)
                    {
                        pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
                    }
                    pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwId;
                    if (ErpsPortMplsApiHandleExtRequest
                        (MPLS_UPDATE_APP_OWNER_FOR_PW, pInMplsApiInfo,
                         pOutMplsApiInfo) != OSIX_SUCCESS)
                    {

                        MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
                                            (UINT1 *) pInMplsApiInfo);
                        MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
                                            (UINT1 *) pOutMplsApiInfo);

                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsUtilConfRingEntryInHw"
                                          "Fetching VPLS entry for pseudowire "
                                          "failed");
                        return OSIX_FAILURE;
                    }
                }
                /*Store the VPN Id's of the Ring Ports */


                if (u4Index == (ERPS_SUBPORTS_PLUS_RINGPORTS - 1))
                {
                    pRingInfo->pErpsLspPwInfo->u4RingPort1VpnId =
                        pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

                    pRingInfo->pErpsLspPwInfo->u4RingPort1PwInLabel =
                        pOutMplsApiInfo->OutPwInfo.u4InVcLabel;
                    pRingInfo->pErpsLspPwInfo->u4RingPort1PhyPort =
                        pOutMplsApiInfo->OutPwInfo.u4PhyPort;

                }
                else if (u4Index == ERPS_SUBPORTS_PLUS_RINGPORTS)
                {
                    pRingInfo->pErpsLspPwInfo->u4RingPort2VpnId =
                        pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

                    pRingInfo->pErpsLspPwInfo->u4RingPort2PwInLabel =
                        pOutMplsApiInfo->OutPwInfo.u4InVcLabel;

                    pRingInfo->pErpsLspPwInfo->u4RingPort2PhyPort =
                        pOutMplsApiInfo->OutPwInfo.u4PhyPort;

                }
                else
                {
                    ERPS_GET_VPN_ID (pRingInfo->pErpsLspPwInfo, u4Index) =
                        pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

                }

            }

            MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
                                (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
                                (UINT1 *) pOutMplsApiInfo);

        }
    }

    ErpsHwRingInfo.pErpsLspPwInfo = pRingInfo->pErpsLspPwInfo;

    if (pRingInfo->u1ProtectionType == ERPS_SERVICE_BASED_PROTECTION)
    {

        /* Check whether single vlan group exists or list of vlan groups exists
           If the condition is satisfied then only one vlan group exist */
        if (pRingInfo->u2VlanGroupListCount == 0)
        {

            ErpsL2wrMiGetVlanListInInstance (pRingInfo->u4ContextId,
                                             pRingInfo->u2ProtectedVlanGroupId,
                                             (UINT1 *)
                                             &ErpsHwRingInfo.VlanList);
        }
        else
        {

            for (; u2VlanGroupId <= pRingInfo->u2VlanGroupListCount;
                 u2VlanGroupId++)
            {
                /* Scanning the vlan group list bitmap to  identify
                 * the vlan group id's mapped to  the ring */
                OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)),
                                         u2VlanGroupId,
                                         sizeof (tErpsVlanGroupList), bResult);
                if (bResult == OSIX_FALSE)
                {
                    /* If the bit is not set, continue the for loop */
                    continue;
                }
                /* If the bit is set, call the function with 
                 * corresponding vlan group id*/
                ErpsL2wrMiGetVlanListInInstance (pRingInfo->u4ContextId,
                                                 u2VlanGroupId,
                                                 (UINT1 *)
                                                 &ErpsHwRingInfo.VlanList);
            }
        }
    }

    ErpsHwRingInfo.u1Port1Action
        = ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ?
           ERPS_PORT_STATE_BLOCKING : ERPS_PORT_STATE_UNBLOCKING);

    ErpsHwRingInfo.u1Port2Action
        = ((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) ?
           ERPS_PORT_STATE_BLOCKING : ERPS_PORT_STATE_UNBLOCKING);

    MEMCPY (ErpsHwRingInfo.ai4HwRingHandle, pRingInfo->ai4HwRingHandle,
            sizeof (ErpsHwRingInfo.ai4HwRingHandle));

    ErpsHwRingInfo.u1RingAction = u1RingStatus;
    ErpsHwRingInfo.pContext = pRingInfo->pContext;

    if (ErpsFsErpsHwRingConfig (&ErpsHwRingInfo) == FNP_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsUtilConfRingEntryInHw hardware "
                          "configuration failed.\r\n");
        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_HW_CONFIG_FAIL);

        return OSIX_FAILURE;
    }
    if (pRingInfo->u2VlanGroupListCount != 0)
    {

        MEMCPY (ErpsHwRingInfo.au1RingVlanGroupList,
                pRingInfo->pau1RingVlanGroupList, sizeof (tErpsVlanGroupList));
        ErpsHwRingInfo.u2VlanGroupListCount = pRingInfo->u2VlanGroupListCount;

        if (ErpsFsErpsHwRingConfig (&ErpsHwRingInfo) == FNP_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilConfRingEntryInHw hardware "
                              "configuration failed.\r\n");
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_HW_CONFIG_FAIL);
            return OSIX_FAILURE;
        }
        ErpsHwRingInfo.u2VlanGroupId = pRingInfo->u2ProtectedVlanGroupId;
    }
    pRingInfo->pContext = ErpsHwRingInfo.pContext;
    MEMCPY (pRingInfo->ai4HwRingHandle, ErpsHwRingInfo.ai4HwRingHandle,
            sizeof (pRingInfo->ai4HwRingHandle));

#else
    /*Below code is added w.r.t. VPLS-RESILIENCY for simulation*/
    UINT1               bResult = 0;
    INT4                i4RetVal = 0;
    UINT4               u4PwIfIndex = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4Index = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    if(pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
    {
	if ((u1RingStatus == ERPS_HW_RING_CREATE) ||
		(u1RingStatus == ERPS_HW_RING_DELETE))
	{
	    if ((pInMplsApiInfo = (tMplsApiInInfo *)
			MemAllocMemBlk (gErpsGlobalInfo.MplsApiInInfoPoolId)) == NULL)
	    {
		ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
			CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			"ErpsUtilConfRingEntryInHw"
			"Failed to allocate memory.\r\n");

		return OSIX_FAILURE;
	    }

	    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
			MemAllocMemBlk (gErpsGlobalInfo.MplsApiOutInfoPoolId)) == NULL)
	    {
		ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
			pRingInfo->u4RingId,
			CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			"ErpsUtilConfRingEntryInHw "
			"Failed to allocate memory.\r\n");
		MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			(UINT1 *) pInMplsApiInfo);

		return OSIX_FAILURE;
	    }

	    for (u4Index = 1; u4Index <= ERPS_SUBPORTS_PLUS_RINGPORTS;
		    u4Index++)
	    {
		MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
		MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

		/*Ring ports Port1 and Port2 will belong to the same VPLS 
		 * and will have same VPN Id. So it is enough to retrieve the 
		 * VPN Id of one ring port and store it. */

		/*Ring Port index is available in pRingInfo->u4Port1IfIndex */

		if (u4Index == (ERPS_SUBPORTS_PLUS_RINGPORTS - 1))
		{
		    /*Pseudowire Interface is directly available in the
		     * * pRingInfo->u4Port1IfIndex variable. Hence adjustment
		     * * is not neccessary*/

		    u4PwIfIndex = pRingInfo->u4Port1IfIndex;
		}
		else if (u4Index == ERPS_SUBPORTS_PLUS_RINGPORTS)
		{
		    if (pRingInfo->u4Port2IfIndex != 0)
		    {
			u4PwIfIndex = pRingInfo->u4Port2IfIndex;
		    }
		    else
		    {
			continue;
		    }

		}
		else
		{
		    /* Loop through the Subport List and retrieve VPN Id for each
		     * * pseudowire*/

		    /* Assumption: First pseudowire in the subportlist1 and first
		     * *pseudowire in the subportlist2 will belong to the same VPN*
		     * * The same is applicable for other pseudowires.*/

		    ERPS_IS_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo, u4Index,
			    bResult);

		    if (bResult == OSIX_FALSE)
		    {
			/* If the bit is not set, see if the bit is set in
			 * * subportlist 2*/

			ERPS_IS_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo,
				u4Index, bResult);

			if (bResult == OSIX_FALSE)
			{
			    continue;
			}
		    }
		    /* This adjustment has been done since in the list First bit
		     * * would correspond to first pseudowire and the interface index
		     * * value needs to be adjusted*/

		    u4PwIfIndex = (u4Index + CFA_MIN_PSW_IF_INDEX - 1);
		}

		/*Fetch the pseudowire value from the pseudowire index
		 * *by calling MPLS API*/

		i4RetVal =
		    ErpsPortL2VpnApiGetPwIndexFromPwIfIndex (u4PwIfIndex,
			    &u4PwIndex);
		if (i4RetVal != OSIX_TRUE)
		{
		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
			    pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "ErpsUtilConfRingEntryInHw"
			    "Pseudowire Id not present for the pseudowire interface.\r\n");
		    /*This may be the case when dummy pw interface is created for subring
		      configuration. That interface is not required to be mapped to any pw*/	
		    continue;
		    return OSIX_FAILURE;
		}

		pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwIndex;
		pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;

		/*Fetch the VPN Id for the corresponding Pseudowire Id
		 * * by calling external MPLS API*/
		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_GET_PW_INFO, pInMplsApiInfo,
			 pOutMplsApiInfo) != OSIX_SUCCESS)
		{
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
			    (UINT1 *) pOutMplsApiInfo);
		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
			    pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "ErpsUtilConfRingEntryInHw"
			    "Fetching VPLS entry for pseudowire failed");

		    return OSIX_FAILURE;
		}

		/*Registration needs to be done for the RAPS pseudowire
		 * *and the corresponding data pseudowires. This informs
		 * *the MPLS module that the ownership of these pseudowires belong
		 * *to the ERPS module*/

		pInMplsApiInfo->u4SrcModId = ERPS_MODULE_APP;

		if (u1RingStatus == ERPS_HW_RING_CREATE)
		{
		    pInMplsApiInfo->u4SubReqType = MPLS_APP_REGISTER;
		}
		if (u1RingStatus == ERPS_HW_RING_DELETE)
		{
		    pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
		}
		pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwIndex;

		if (ErpsPortMplsApiHandleExtRequest
			(MPLS_UPDATE_APP_OWNER_FOR_PW, pInMplsApiInfo,
			 pOutMplsApiInfo) != OSIX_SUCCESS)
		{

		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			    (UINT1 *) pInMplsApiInfo);
		    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
			    (UINT1 *) pOutMplsApiInfo);

		    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
			    pRingInfo->u4RingId,
			    CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
			    "ErpsUtilConfRingEntryInHw"
			    "Fetching VPLS entry for pseudowire "
			    "failed");
		    return OSIX_FAILURE;
		}
		/*Store the VPN Id's of the Ring Ports */
		if (u4Index == (ERPS_SUBPORTS_PLUS_RINGPORTS - 1))
		{
		    pRingInfo->pErpsLspPwInfo->u4RingPort1VpnId =
			pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

		    pRingInfo->pErpsLspPwInfo->u4RingPort1PwInLabel =
			pOutMplsApiInfo->OutPwInfo.u4InVcLabel;

		    pRingInfo->pErpsLspPwInfo->u4RingPort1PhyPort =
			pOutMplsApiInfo->OutPwInfo.u4PhyPort;

		}
		else if (u4Index == ERPS_SUBPORTS_PLUS_RINGPORTS)
		{
		    pRingInfo->pErpsLspPwInfo->u4RingPort2VpnId =
			pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

		    pRingInfo->pErpsLspPwInfo->u4RingPort2PwInLabel =
			pOutMplsApiInfo->OutPwInfo.u4InVcLabel;

		    pRingInfo->pErpsLspPwInfo->u4RingPort2PhyPort =
			pOutMplsApiInfo->OutPwInfo.u4PhyPort;

		}
		else
		{
		    ERPS_GET_VPN_ID (pRingInfo->pErpsLspPwInfo, u4Index) =
			pOutMplsApiInfo->OutPwInfo.u2VplsFdbId;

		}

	    }

	    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
		    (UINT1 *) pInMplsApiInfo);
	    MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
		    (UINT1 *) pOutMplsApiInfo);

	}
    } 
    UNUSED_PARAM (pRingInfo);
    UNUSED_PARAM (u1RingStatus);
    UNUSED_PARAM (u1HwUpdatedPort);
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsUtilUpdateRingStatistics                     */
/*                                                                           */
/*    Description         : This function will be used to update the         */
/*                          statistics of a ring                             */
/*                                                                           */
/*    Input(s)            : pRingInfo  - Pointer to ring node.              */
/*                          MepInfo     - MEP for which the statistics to    */
/*                                        be updated.                        */
/*                          u1Type      - Statistics Type.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
PUBLIC VOID
ErpsUtilUpdateRingStatistics (tErpsRingInfo * pRingInfo,
                              tErpsCfmMepInfo MepInfo, UINT1 u1Type)
{
    tErpsPortStats     *pPortStats = NULL;

    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        pPortStats = &(pRingInfo->Port1Stats);
    }
    else
    {
        pPortStats = &(pRingInfo->Port2Stats);
    }

    switch (u1Type)
    {
        case ERPS_STATS_RAPS_SENT:
            pPortStats->u4RapsSentCount++;
            break;
        case ERPS_STATS_RAPS_RCVD:
            pPortStats->u4RapsReceivedCount++;
            break;
        case ERPS_STATS_RAPS_DISC:
            pPortStats->u4RapsDiscardCount++;
            break;
        case ERPS_STATS_PORT_BLOCK:
            pPortStats->u4BlockedCount++;
            break;
        case ERPS_STATS_PORT_UNBLOCK:
            pPortStats->u4UnblockedCount++;
            break;
        case ERPS_STATS_PORT_FAIL:
            pPortStats->u4FailedCount++;
            break;
        case ERPS_STATS_PORT_RECOVER:
            pPortStats->u4RecoveredCount++;
            break;
        case ERPS_STATS_VERSION_DISC:
            pPortStats->u4VersionDiscardCount++;
            break;
        case ERPS_STATS_FS_RCVD:
            pPortStats->u4FSPduRxCount++;
            break;
        case ERPS_STATS_FS_SENT:
            pPortStats->u4FSPduTxCount++;
            break;
        case ERPS_STATS_MS_RCVD:
            pPortStats->u4MSPduRxCount++;
            break;
        case ERPS_STATS_MS_SENT:
            pPortStats->u4MSPduTxCount++;
            break;
        case ERPS_STATS_EVENT_RCVD:
            pPortStats->u4EventPduRxCount++;
            break;
        case ERPS_STATS_EVENT_SENT:
            pPortStats->u4EventPduTxCount++;
            break;
        case ERPS_STATS_SF_RCVD:
            pPortStats->u4SFPduRxCount++;
            break;
        case ERPS_STATS_SF_SENT:
            pPortStats->u4SFPduTxCount++;
            break;
        case ERPS_STATS_NR_RCVD:
            pPortStats->u4NRPduRxCount++;
            break;
        case ERPS_STATS_NR_SENT:
            pPortStats->u4NRPduTxCount++;
            break;
        case ERPS_STATS_NRRB_RCVD:
            pPortStats->u4NRRBPduRxCount++;
            break;
        case ERPS_STATS_NRRB_SENT:
            pPortStats->u4NRRBPduTxCount++;
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/*    Function Name       : ErpsUtilDelFdbAndTxRaps                          */
/*                                                                           */
/*    Description         : This function is given as input to the           */
/*                          UtlDynSllWalk function                           */
/*                                                                           */
/*    Input(s)            : pRingTcPropNode - Pointer to TcPropNode.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
PUBLIC INT4
ErpsUtilDelFdbAndTxRaps (tDynSLLElem * pElem)
{
    tErpsRingInfo      *pRingTcPropNode = (tErpsRingInfo *) pElem;
    tErpsContextInfo   *pContextInfo = NULL;
    UINT2               u2VlanGroupId = 1;
    UINT1               bResult = 0;
    INT4                i4RetVal = 0;

    pContextInfo = ErpsCxtGetContextEntry (pRingTcPropNode->u4ContextId);
    /* Check whether single vlan group exists or list of vlan groups exists
     *  If the condition is satisfied then only one vlan group exist */

    if (pRingTcPropNode->u2VlanGroupListCount == 0)
    {
        /* Call the function with single vlan group id */

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingTcPropNode->u1IsPort1Present)
        {
            ErpsPortVlanDeleteFdbEntries (pRingTcPropNode->u4ContextId,
                                          pRingTcPropNode->u4Port1IfIndex,
                                          pRingTcPropNode->u1ProtectionType,
                                          pRingTcPropNode->
                                          u2ProtectedVlanGroupId,
                                          pRingTcPropNode->pContext,
                                          pRingTcPropNode->pErpsLspPwInfo);
        }
        if ((pRingTcPropNode->u4Port2IfIndex != 0)
            && (ERPS_PORT_IN_LOCAL_LINE_CARD ==
                pRingTcPropNode->u1IsPort2Present))
        {
            /* Call the function with single vlan group id */
            ErpsPortVlanDeleteFdbEntries (pRingTcPropNode->u4ContextId,
                                          pRingTcPropNode->u4Port2IfIndex,
                                          pRingTcPropNode->u1ProtectionType,
                                          pRingTcPropNode->
                                          u2ProtectedVlanGroupId,
                                          pRingTcPropNode->pContext,
                                          pRingTcPropNode->pErpsLspPwInfo);
        }
    }
    else
    {
        /* Scanning the vlan group list bitmap to identify the
         *  vlan group id's mapped to the ring */
        for (; u2VlanGroupId <= pRingTcPropNode->u2VlanGroupListCount;
             u2VlanGroupId++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*
                                      (pRingTcPropNode->pau1RingVlanGroupList)),
                                     u2VlanGroupId, sizeof (tErpsVlanGroupList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                continue;
            }
            /* If the bit is set, Call the function with corresponding
             * vlan group id */
            if (ERPS_PORT_IN_LOCAL_LINE_CARD ==
                pRingTcPropNode->u1IsPort1Present)
            {
                ErpsPortVlanDeleteFdbEntries (pRingTcPropNode->u4ContextId,
                                              pRingTcPropNode->u4Port1IfIndex,
                                              pRingTcPropNode->u1ProtectionType,
                                              u2VlanGroupId,
                                              pRingTcPropNode->pContext, NULL);
            }
            if ((pRingTcPropNode->u4Port2IfIndex != 0)
                && (ERPS_PORT_IN_LOCAL_LINE_CARD ==
                    pRingTcPropNode->u1IsPort2Present))
            {
                ErpsPortVlanDeleteFdbEntries (pRingTcPropNode->u4ContextId,
                                              pRingTcPropNode->u4Port2IfIndex,
                                              pRingTcPropNode->u1ProtectionType,
                                              u2VlanGroupId,
                                              pRingTcPropNode->pContext, NULL);
            }
        }
    }
    if (pContextInfo != NULL)
    {
        /* ErpsTxStartRapsMessages function is called only when ERPS module 
         * status is enabled */
        if (pContextInfo->u1ModuleStatus == OSIX_ENABLED)
        {
            i4RetVal =
                ErpsTxStartRapsMessages (pRingTcPropNode, ERPS_STATUS_BIT_NONE,
                                         ERPS_MESSAGE_TYPE_EVENT);
        }
    }

    ERPS_CONTEXT_TRC (pRingTcPropNode->u4ContextId, pRingTcPropNode->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsUtilDelFdbAndTxRaps: Event  "
                      "message is propagated to this ring.\r\n");

    UNUSED_PARAM (i4RetVal);
    return OSIX_SUCCESS;
}

#ifdef TRACE_WANTED
/*****************************************************************************/
/*    Function Name       : ErpsUtilContextTrc                               */
/*                                                                           */
/*    Description         : This function prints the context traces          */
/*                                                                           */
/*    Input(s)            : Context Identifier,Ring Identifier,Trace Mask    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
ErpsUtilContextTrc (UINT4 u4ContextId, UINT4 u4RingId, UINT4 u4Mask,
                    const char *fmt, ...)
{
    va_list             ap;
    static CHR1         ac1Buf[ERPS_TRC_MAX_SIZE];
    UINT4               u4OffSet = 0;

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));

    if (ErpsUtilIsErpsStarted (u4ContextId) == OSIX_FALSE)
    {
        return;
    }

    SPRINTF (ac1Buf, "[Switch:%s][RingId:%u]",
             ERPS_CONTEXT_NAME (u4ContextId), u4RingId);
    u4OffSet = STRLEN (ac1Buf);

    va_start (ap, fmt);
    vsprintf (&ac1Buf[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcLog (ERPS_MODULE_TRACE (u4ContextId), u4Mask,
               ERPS_MODULE_NAME, ac1Buf);
    return;
}

/*****************************************************************************/
/*    Function Name       : ErpsUtilContextInitTrc                           */
/*                                                                           */
/*    Description         : This function prints the context traces          */
/*                                                                           */
/*    Input(s)            : Context Identifier,Trace Mask                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
ErpsUtilContextInitTrc (UINT4 u4ContextId, UINT4 u4Mask, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         ac1Buf[ERPS_TRC_MAX_SIZE];
    UINT4               u4OffSet = 0;

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));
    if (ErpsUtilIsErpsStarted (u4ContextId) == OSIX_FALSE)
    {
        return;
    }
    SPRINTF (ac1Buf, "[Switch:%s]", ERPS_CONTEXT_NAME (u4ContextId));

    u4OffSet = STRLEN (ac1Buf);

    va_start (ap, fmt);
    vsprintf (&ac1Buf[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcLog (ERPS_MODULE_TRACE (u4ContextId), u4Mask,
               ERPS_MODULE_NAME, ac1Buf);
    return;
}
#endif

/***************************************************************************
 * FUNCTION NAME    : ErpsUtilSetVlanGroupIdForVlan
 *
 * DESCRIPTION      : This function mapped a vlan to a vlan group.
 *
 * INPUT            : u4ContextId - virtual context id
 *                    VlanId      - vlan id that needs to be mapped to the
 *                                  provided vlan group.
 *                    u2VlanGroupId - vlan group id.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsUtilSetVlanGroupIdForVlan (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2VlanGroupId)
{
    tStpInstanceInfo    InstInfo;
#ifdef NPAPI_WANTED
    tErpsHwRingInfo     ErpsHwRingInfo;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pRingNextInfo = NULL;
    UINT2               u2VlanGroup = 1;
    UINT1               bResult = 0;
    UINT1               u1NpFlag = OSIX_FALSE;
    UINT1               u1ActionFlag = 0;
#endif
    UINT2               u2OldGroupId = ERPS_DEFAULT_VLAN_GROUP_ID;

    MEMSET (&InstInfo, 0, sizeof (InstInfo));

    if (u2VlanGroupId == ERPS_DEFAULT_VLAN_GROUP_ID)
    {
        /* Get the currently mapped instance id for this vlan */
        u2OldGroupId = ErpsL2wrMiGetVlanInstMapping (u4ContextId, VlanId);

        /* unmapping the vlan from instance u2VlanGroupId and
         * adding it to default group. */
        InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_MAP_COUNT;
        if (L2IwfGetInstanceInfo (u4ContextId, u2OldGroupId, &InstInfo)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2VlanGroupId)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (InstInfo.u2MappingCount == 1)
        {
            /* Delete this instance */
            L2IwfDeleteSpanningTreeInstance (u4ContextId, u2OldGroupId);
#ifdef NPAPI_WANTED
            u1ActionFlag = ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING;
#endif
        }
        else
        {
            /* only needs to remove the mapping, instance need not be
             * deleted */
#ifdef NPAPI_WANTED
            u1ActionFlag = ERPS_HW_DEL_VLAN_MAPPING;
#endif
        }
    }
    else
    {
        InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_STATUS;
        if (L2IwfGetInstanceInfo (u4ContextId, u2VlanGroupId, &InstInfo)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (InstInfo.u1InstanceStatus == OSIX_FALSE)
        {
            /* Given Instance is not Active */
            if (L2IwfCreateSpanningTreeInstance (u4ContextId, u2VlanGroupId)
                == L2IWF_FAILURE)
            {
                return OSIX_FAILURE;
            }
#ifdef NPAPI_WANTED
            u1ActionFlag = ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING;
#endif
        }
        else
        {
#ifdef NPAPI_WANTED
            u1ActionFlag = ERPS_HW_ADD_VLAN_MAPPING;
#endif
        }

        if (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2VlanGroupId)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

#ifdef NPAPI_WANTED
    MEMSET (&ErpsHwRingInfo, 0, sizeof (ErpsHwRingInfo));

    /* Program the hardware - add this vlan to the vlan group */
    ErpsHwRingInfo.u4ContextId = u4ContextId;

    if ((u1ActionFlag == ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING) ||
        (u1ActionFlag == ERPS_HW_DEL_VLAN_MAPPING))
    {
        /* This is the group needs to be deleted in hw */
        ErpsHwRingInfo.u2VlanGroupId = u2OldGroupId;
    }
    else
    {
        ErpsHwRingInfo.u2VlanGroupId = u2VlanGroupId;
    }

    ErpsHwRingInfo.u1RingAction = u1ActionFlag;
    ErpsHwRingInfo.VlanId = VlanId;

    /* The below code will get the port index for the Vlan group Id 
     * This will give the port index only in below scenario
     * 1. Configure PG
     * 2. Configure Vlan Group Manager as erps
     * 3. Create VlanGroup
     * 4. Map the VlanGroup to the PG
     * 5. Map vlans to the Vlan Group
     *
     * When Map the vlans to the VlanGroup This while loop will findout the 
     * corresponding PG RingInfo and give the port information to HW
     * */
    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pRingInfo != NULL)
    {
        if (pRingInfo->u2VlanGroupListCount != 0)
        {
            for (u2VlanGroup = 1;
                 u2VlanGroup <= pRingInfo->u2VlanGroupListCount; u2VlanGroup++)
            {
                /* Scanning the vlan group list bitmap to identify the
                 *  vlan group id's mapped to the ring */
                OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)),
                                         u2VlanGroup,
                                         sizeof (tErpsVlanGroupList), bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }
                /* If the bit is set,check whether the corresponding vlan group id
                   is equal to ErpsHwRingInfo structure variable */
                if ((pRingInfo->u4ContextId == u4ContextId) &&
                    u2VlanGroup == ErpsHwRingInfo.u2VlanGroupId)

                {
                    ErpsHwRingInfo.u4Port1IfIndex = pRingInfo->u4Port1IfIndex;
                    ErpsHwRingInfo.u4Port2IfIndex = pRingInfo->u4Port2IfIndex;
                    ErpsHwRingInfo.u1HwUpdatedPort = PORT1_PORT2_UPDATED;
                    ErpsHwRingInfo.pContext = pRingInfo->pContext;
                    ErpsHwRingInfo.u4RingId = pRingInfo->u4RingId;
                    ErpsHwRingInfo.u1UnBlockRAPSChannel =
                        pRingInfo->u1RAPSSubRingWithoutVirtualChannel;
                    ErpsHwRingInfo.u1HighestPriorityRequest =
                        pRingInfo->u1HighestPriorityRequestWithoutVC;

                    u1NpFlag = OSIX_TRUE;

                    if (ErpsFsErpsHwRingConfig (&ErpsHwRingInfo) == FNP_FAILURE)
                    {
                        ErpsUtilHandleVlanGroupHwFailure (u4ContextId, VlanId,
                                                          u2OldGroupId,
                                                          u2VlanGroupId,
                                                          u1ActionFlag);
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        pRingNextInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
        pRingInfo = pRingNextInfo;
    }

    if (u1NpFlag == OSIX_FALSE)
    {
        if (ErpsFsErpsHwRingConfig (&ErpsHwRingInfo) == FNP_FAILURE)
        {
            ErpsUtilHandleVlanGroupHwFailure (u4ContextId, VlanId,
                                              u2OldGroupId, u2VlanGroupId,
                                              u1ActionFlag);
            return OSIX_FAILURE;
        }
    }

#endif

    return OSIX_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************************
 * FUNCTION NAME    : ErpsUtilHandleVlanGroupHwFailure
 *
 * DESCRIPTION      : This function handles the software updations when hardware *                    call fails
 *
 * INPUT            : u4ContextId - virtual context id
 *                    VlanId      - vlan id that needs to be mapped to the
 *                                  provided vlan group.
 *                    u2VlanGroupId - vlan group id.
 *                    u2OldGroupId  - Priviously mapped vlan group id
 *                    u1ActionFlag -  
 *                      o ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING -create the
 *                                vlan group in the h/w and add the vlan to it.
 *                      o ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING - Unmap the     
 *                                   vlan from the vlan group and then       
 *                                   delete the vlan group from the h/w      
 *                      o ERPS_HW_ADD_VLAN_MAPPING - add vlan to vlan        
 *                                   group in h/w                            
 *                      o ERPS_HW_DEL_VLAN_MAPPING - delete/unmap vlan       
 *                                   from the vlan group in h/w              
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
ErpsUtilHandleVlanGroupHwFailure (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2OldGroupId, UINT2 u2VlanGroupId,
                                  UINT1 u1ActionFlag)
{
    /* Creation of group entry fails in hardware, then taknig
     * action to revert the updations done in software */
    if ((u1ActionFlag == ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING) ||
        (u1ActionFlag == ERPS_HW_ADD_VLAN_MAPPING))
    {
        L2IwfSetVlanInstMapping (u4ContextId, VlanId,
                                 (UINT2) ERPS_DEFAULT_VLAN_GROUP_ID);

        if (u1ActionFlag == ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING)
        {
            /* Delete this instance */
            L2IwfDeleteSpanningTreeInstance (u4ContextId, u2VlanGroupId);
        }
    }

    /* Deletion of group entry fails in hardware, then taknig
     * action to revert the updations done in software */
    if ((u1ActionFlag == ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING) ||
        (u1ActionFlag == ERPS_HW_DEL_VLAN_MAPPING))
    {
        if (u1ActionFlag == ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING)
        {
            /* create the instance */
            L2IwfCreateSpanningTreeInstance (u4ContextId, u2OldGroupId);
        }
        L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2OldGroupId);
    }

    return OSIX_SUCCESS;
}
#endif

/**************************************************************************/
/* FUNCTION NAME    : ErpsUtilHandleHwConfigFailure                       */
/*                                                                        */
/* DESCRIPTION      : This function handles the case when a hardware call */
/*                     fails to execute and returns failure               */
/*                                                                        */
/* INPUT            : pRingInfo - Ring Data structure                     */
/*                                                                        */
/* OUTPUT           : None                                                */
/*                                                                        */
/* RETURNS          : None                                                */
/*                                                                        */
/**************************************************************************/
VOID
ErpsUtilHandleHwConfigFailure (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo->u1RowStatus != NOT_IN_SERVICE)
    {
        ErpsRingDeActivateRing (pRingInfo, DESTROY);
        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_HW_CONFIG_FAIL);

        if (ErpsRingRemNodeFromPortVlanTable (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsUtilHandleHwConfigFailure: Ring "
                              "failed to remove node from VlanTable\r\n");
            return;
        }
        pRingInfo->u1RowStatus = NOT_IN_SERVICE;
    }
    else
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                          pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsUtilHandleHwConfigFailure not executed"
                          "to handle the failure of H/w configuration\r\n");
        return;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                      pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsUtilHandleHwConfigFailure successfully executed"
                      "to handle the failure of H/w configuration\r\n");
}

#ifdef NPAPI_WANTED
/**************************************************************************/
/* FUNCTION NAME    : ErpsHandleNpCallbackEvents                          */
/* DESCRIPTION      : This routine will transmit the appropriate R-APS    */
/*                    PDUs if the NPAPI is successful. Otherwise  the     */
/*                   exception handling routine                           */
/*                   (ErpsUtilHandleHwConfigFailure) is called which      */
/*                   also sends a trap to the administrator               */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* INPUT            : pErpsMsg  - MsgQ Data structure                     */
/*                                                                        */
/* OUTPUT           : None                                                */
/*                                                                        */
/* RETURNS          : OSIX_SUCCESS                                        */
/*                                                                        */
/**************************************************************************/

INT4
ErpsHandleNpCallbackEvents (tErpsQMsg * pErpsMsg)
{
    unAsyncNpapi        Np_Params;
    tErpsRingInfo       ErpsRingInfo;
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1Statusbit = 0;
    UINT1               u1Msgtype = 0;

    MEMSET (&Np_Params, 0, sizeof (unAsyncNpapi));
    MEMSET (&ErpsRingInfo, 0, sizeof (ErpsRingInfo));
    MEMCPY (&Np_Params.FsErpsHwRingConfig,
            (tAsNpwFsErpsHwRingConfig *)
            & (pErpsMsg->unMsgParam.ErpsNpCbMsg.
               ErpsAsyncNpCbParams.FsErpsHwRingConfig),
            sizeof (tAsNpwFsErpsHwRingConfig));
    ErpsRingInfo.u4ContextId =
        Np_Params.FsErpsHwRingConfig.ErpsHwRingInfo.u4ContextId;
    ErpsRingInfo.u4RingId =
        Np_Params.FsErpsHwRingConfig.ErpsHwRingInfo.u4RingId;
    pRingInfo = ErpsRingGetNodeFromRingTable (&ErpsRingInfo);

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    if (Np_Params.FsErpsHwRingConfig.rval == FNP_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                          pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "Erps failed to configure Hardware\r\n");
        ErpsUtilHandleHwConfigFailure (pRingInfo);
    }
    else
    {
        /* Update the H/w port status */
        pRingInfo->u1Port1HwStatus =
            Np_Params.FsErpsHwRingConfig.ErpsHwRingInfo.u1Port1Action;
        pRingInfo->u1Port2HwStatus =
            Np_Params.FsErpsHwRingConfig.ErpsHwRingInfo.u1Port2Action;

        /* Compute the first two bytes of R-APS message */
        u1Msgtype = (UINT1) (pRingInfo->u2LastMsgSend >> 12);
        u1Statusbit = (UINT1) ((pRingInfo->u2LastMsgSend) & 0x00FF);

        if (pRingInfo->u1RingPduSend == ERPS_RING_PDU_SEND)
        {
            if (ErpsTxStartRapsMessages (pRingInfo, u1Statusbit, u1Msgtype)
                == OSIX_SUCCESS)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsHandleNpCallbackEvents:"
                                  "Exit after successful "
                                  "transmission of R-APS PDUs"
                                  "with Status bit:0x%x," "Message Type:0x%x"
                                  " \r\n", u1Statusbit, u1Msgtype);

                return OSIX_SUCCESS;
            }
        }
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                      pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsHandleNpCallbackEvents failed to "
                      "transmit RAPS messages with"
                      "Status bit:0x%x,"
                      "Message Type:0x%x" " \r\n", u1Statusbit, u1Msgtype);

    return OSIX_FAILURE;
}
#endif

/* HITLESS RESTART */
/*****************************************************************************/
/* FUNCTION NAME    : ErpsUtilGetPeriodicTime                                */
/*                                                                           */
/* DESCRIPTION      : This is the function used to get the value of the      */
/*                    Periodic Timeout when the steady state packet is to    */
/*                    be sent during hitless restart                         */
/*                                                                           */
/* INPUT            : u4IfIndex - Interface Index                            */
/*                                                                           */
/* OUTPUT           : pu42RetValErpsApsPeriodicTime - Periodic Timeout Value */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
ErpsUtilGetPeriodicTime (UINT4 u4IfIndex, UINT4 *pu4RetValErpsApsPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    *pu4RetValErpsApsPeriodicTime = 0;
    while (pRingInfo != NULL)
    {
        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) == OSIX_FALSE) ||
            (pRingInfo->u1RowStatus != ACTIVE))
        {
            continue;
        }
        if (pRingInfo->u4Port1IfIndex == u4IfIndex ||
            pRingInfo->u4Port2IfIndex == u4IfIndex)
        {
            *pu4RetValErpsApsPeriodicTime = pRingInfo->u4PeriodicTimerValue;
            return OSIX_SUCCESS;
        }
        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);

    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsUtilDelVlanGroupTable                              */
/*                                                                           */
/* DESCRIPTION      : This is the function used to delete the vlan group     */
/*                    table for the given context                            */
/*                                                                           */
/* INPUT            : u4ContextId                                            */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
ErpsUtilDelVlanGroupTable (UINT4 u4ContextId)
{
    INT4                i4VlanId = 0;
    UINT2               u2VlanGroupId = 0;

    for (i4VlanId = 1; i4VlanId <= VLAN_DEV_MAX_VLAN_ID_EXT; i4VlanId++)
    {
        /* Move all the vlans to ERPS_DEFAULT_VLAN_GROUP_ID */
        u2VlanGroupId = ErpsL2wrMiGetVlanInstMapping (u4ContextId, i4VlanId);

        if (u2VlanGroupId != ERPS_DEFAULT_VLAN_GROUP_ID)
        {
            if (ErpsUtilSetVlanGroupIdForVlan (u4ContextId, i4VlanId,
                                               ERPS_DEFAULT_VLAN_GROUP_ID) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ErpsGetSysTime                                     */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ErpsGetSysTime (tOsixSysTime * pSysTime)
{

    tUtlSysPreciseTime  SysPreciseTime;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    /* Function used to return the system time in seconds and nanoseconds */
    UtlGetPreciseSysTime (&SysPreciseTime);
    /* Time returned in milliseconds */
    *pSysTime =
        ((SysPreciseTime.u4Sec * 1000) + (SysPreciseTime.u4NanoSec / 1000000));
    return;
}

/***********************************************************************/
/*  Function Name   : ErpsUtilMeasureTime                              */
/*  Description     : This is the function used to Store the System    */
/*                    current time when topology change occur         */
/*  Input(s)        : pRingInfo                                        */
/*                    u1DefectType                                     */
/*                    u4IfIndex                                        */
/*  Output(s)       : pSysTime - The System time.                      */
/*  Returns         : None                                             */
/* *********************************************************************/
VOID
ErpsUtilMeasureTime (tErpsRingInfo * pRingInfo, UINT4 u4IfIndex, UINT1 u1Flag)
{
    tUtlSysPreciseTime  SysPreciseTime;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    UtlGetPreciseSysTime (&SysPreciseTime);
   

    switch (u1Flag)
    {
        case ERPS_DEFECT_ENC_SWITCH_CMD:

            if (u4IfIndex == pRingInfo->u4Port1IfIndex)
            {
                pRingInfo->perf.u4Port1DefectEncounteredTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsSFPort1DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;

            }
            else
            {
                pRingInfo->perf.u4Port2DefectEncounteredTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsSFPort2DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;
            }

            break;
        case ERPS_RPL_PORT_STATUS_CHANGE_TIME:
        {
            pRingInfo->perf.u4RplPortStatusChangedTimeSec =
                SysPreciseTime.u4Sec;
            pRingInfo->perf.u4RplPortStatusChangedTimeNSec =
                SysPreciseTime.u4NanoSec;
        }


            break;
        case ERPS_RPL_NBR_PORT_STATUS_CHANGE_TIME:
        {
            pRingInfo->perf.u4RplNeighborPortStatusChangedTime =
                SysPreciseTime.u4Sec;
        }
            break;
        case ERPS_RAPS_DEFECT_ENC_TIME:
            if (u4IfIndex == pRingInfo->u4Port1IfIndex)
            {
                pRingInfo->perf.u4Port1DefectEncounteredTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsSFPort1DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;

            }
            else
            {
                pRingInfo->perf.u4Port2DefectEncounteredTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsSFPort2DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;

            }

            break;
        case ERPS_RAPS_DEFECT_CLR_TIME:
            if (u4IfIndex == pRingInfo->u4Port1IfIndex)
            {
                pRingInfo->perf.u4Port1DefectClearedTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsClearSFPort1DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;
            }
            else
            {
                pRingInfo->perf.u4Port2DefectClearedTimeSec =
                    SysPreciseTime.u4Sec;
                pRingInfo->perf.u4RapsClearSFPort2DefectTimeNSec =
                    SysPreciseTime.u4NanoSec;

            }

            break;

        default:
            break;
    }
}

/***********************************************************************/
/*  Function Name   : ErpsUtilGetOctetStrFromPortList                  */
/*  Description     : This is the function used to convert the portlist*/
/*                    to Octet string of the format Port1, Port2...    */
/*                    Portn.                                           */
/*  Input(s)        : pPortList - PortList information                 */
/*                    pOctetStr - OctetString                          */
/*                    u4IfIndex                                        */
/*  Output(s)       : pSysTime - The System time.                      */
/*  Returns         : None                                             */
/* *********************************************************************/
INT4
ErpsUtilGetOctetStrFromPortList (tPortListExt * pPortList,
                                 tSNMP_OCTET_STRING_TYPE * pOctetStr)
{
    UINT4               u4IfIndex = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (MEMCMP (pOctetStr->pu1_OctetList, "", pOctetStr->i4_Length) != 0)
    {
        SPRINTF ((CHR1 *) pOctetStr->pu1_OctetList, "%s" "%s",
                 pOctetStr->pu1_OctetList, ",");
    }

    for (u4IfIndex = 1; u4IfIndex <= (ERPS_MAX_SIZING_SUB_PORTS); u4IfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET ((*pPortList),
                                 (u4IfIndex + CFA_MIN_PSW_IF_INDEX - 1),
                                 sizeof (tPortListExt), bResult);
        if (bResult == OSIX_TRUE)
        {
            SPRINTF ((CHR1 *) pOctetStr->pu1_OctetList, "%s%u%s",
                     pOctetStr->pu1_OctetList, u4IfIndex + CFA_MIN_PSW_IF_INDEX - 1, ",");
        }
    }                            /* End of for */

    pOctetStr->i4_Length = STRLEN (pOctetStr->pu1_OctetList);
    pOctetStr->pu1_OctetList[pOctetStr->i4_Length - 1] = '\0';
    return OSIX_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : ErpsGetClearValue                                */
/*  Description     : This is the function used to Get the ClearSave   */
/*                    Value to store the ClearObject Value as 1 or 2   */
/*                    in conf file to get the Correct Ring state in    */
/*                    Restoration                                      */
/*  Input(s)        : u4FsErpsContextId                                */
/*                    u4FsErpsRingId                                   */
/*                    pi4RetValFsErpsRingConfigClear                   */
/*  Output(s)       : None                                             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                        */
/* *********************************************************************/
UINT1
ErpsGetClearValue (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                   INT4 *pi4RetValFsErpsRingConfigClear)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */

    ERPS_LOCK ();

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        ERPS_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Getting the ClearSave value to store the value in conf file */

    *pi4RetValFsErpsRingConfigClear = pRingInfo->u1ClearSave;

    ERPS_UNLOCK ();

    return SNMP_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : ErpsUtilNotifyConfig                             */
/*  Description     : This is the function used to store the 
 *                    dynamically updated values becuas of some other 
 *                    events.*/
/*  Input(s)        : pRingInfo - Ring Info structure                  */
/*                    u1Flag - To update Mib object                    */
/*  Output(s)       : None                                             */
/*  Returns         : None                                             */
/* *********************************************************************/
VOID
ErpsUtilNotifyConfig (tErpsRingInfo * pRingInfo, UINT1 u1Flag)
{
#ifdef SNMP_2_WANTED

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    switch (u1Flag)
    {
        case ERPS_SWITCH_PORT:

            RM_GET_SEQ_NUM (&u4SeqNum);
            SnmpNotifyInfo.pu4ObjectId = FsErpsRingConfigSwitchPort;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsErpsRingConfigSwitchPort) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;    /*There is no Row status object is present in Table */
            SnmpNotifyInfo.pLockPointer = ErpsApiLock;
            SnmpNotifyInfo.pUnLockPointer = ErpsApiUnLock;
            SnmpNotifyInfo.u4Indices = ERPS_NODE_ID_FIELD;    /*This macro is used to get the value 2 because
                                                             * Clear object table is having index 2*/
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                              pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              pRingInfo->u4SwitchCmdIfIndex));
            break;

        case ERPS_CLEAR_COMMAND_NONE:

            RM_GET_SEQ_NUM (&u4SeqNum);
            SnmpNotifyInfo.pu4ObjectId = FsErpsRingConfigClear;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsErpsRingConfigClear) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;    /*There is no Row status object is present in Table */
            SnmpNotifyInfo.pLockPointer = ErpsApiLock;
            SnmpNotifyInfo.pUnLockPointer = ErpsApiUnLock;
            SnmpNotifyInfo.u4Indices = ERPS_NODE_ID_FIELD;    /*This macro is used to get the value 2 because 
                                                             * Clear object table is having index 2*/
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                              pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              pRingInfo->u1ClearSave));

            break;
        default:
            break;
    }
#else
       UNUSED_PARAM (pRingInfo);
       UNUSED_PARAM (u1Flag);
#endif
}

/***********************************************************************/
/*  Function Name   : ErpsUtilValidateVlanGroup                        */
/*  Description     : This is the function used to validate whether    */
/*                    Vlan Group ID is present in the rings, which     */
/*                    are mapped to the context                        */
/*  Input(s)        : u4FsErpsContextId - Context Id                   */
/*                    i4FsErpsVlanGroupId - Vlan group Id              */
/*  Output(s)       : None                                             */
/*  Returns         : None                                             */
/* *********************************************************************/
INT4
ErpsUtilValidateVlanGroup (UINT4 u4FsErpsContextId, UINT2 i4FsErpsVlanGroupId)
{
    tErpsRingInfo       RingInfo;
    tErpsRingInfo      *pNextRingEntry = NULL;
    UINT2               u2VlanGroup = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    RingInfo.u4ContextId = u4FsErpsContextId;
    RingInfo.u4RingId = 0;
    pNextRingEntry = ErpsRingGetNextNodeFromRingTable (&RingInfo);

    while (pNextRingEntry != NULL)

    {
        for (u2VlanGroup = 1;
             u2VlanGroup <= pNextRingEntry->u2VlanGroupListCount; u2VlanGroup++)
        {
            /* Scanning the vlan group list bitmap to identify the
             * vlan group id's mapped to the ring */
            OSIX_BITLIST_IS_BIT_SET ((*(pNextRingEntry->pau1RingVlanGroupList)),
                                     u2VlanGroup, sizeof (tErpsVlanGroupList),
                                     bResult);
            if (bResult == OSIX_FALSE)
            {
                continue;
            }

            /* If the bit is set,check whether the vlan group id
             * is used by Ring entry. */
            if ((pNextRingEntry->u4ContextId == u4FsErpsContextId) &&
                u2VlanGroup == (UINT2) i4FsErpsVlanGroupId)
            {
                if (pNextRingEntry->u1RowStatus == ACTIVE)
                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId,
                                      pNextRingEntry->u4RingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Ring Group %d active.Make the row status "
                                      "to Not In Service to delete the given "
                                      "instance\n", pNextRingEntry->u4RingId);

                    return SNMP_FAILURE;
                }

                if (pNextRingEntry->u1RowStatus == NOT_IN_SERVICE)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;
        }
        RingInfo.u4ContextId = pNextRingEntry->u4ContextId;
        RingInfo.u4RingId = pNextRingEntry->u4RingId;
        pNextRingEntry = ErpsRingGetNextNodeFromRingTable (&RingInfo);
    }
    return SNMP_SUCCESS;
}
