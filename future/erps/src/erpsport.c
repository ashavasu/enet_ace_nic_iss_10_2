/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsport.c,v 1.32 2017/02/27 13:52:08 siva Exp $
 *
 * Description: This file contains the ERPS API related functions        
 *                                                        
 *****************************************************************************/
#ifndef _ERPSPORT_C
#define _ERPSPORT_C

#include "erpsinc.h"
#include "snmputil.h"
extern UINT4        fserps[9];
extern UINT4        FsErpsCtxtSystemControl[13];

extern VOID VlanVplsDelPwFdbEntries (UINT4 u4PwVcIndex);

/***************************************************************************
 * FUNCTION NAME    : ErpsPortEcfmInitiateExPdu
 *
 * DESCRIPTION      : Routine used by protocols to register with ECFM.
 *
 * INPUT            : pMepInfo      - Pointer to structure having information 
 *                                    to select a MEP node
 *                    u1TlvOffset   - TLV offset for the PDU
 *                    u1SubOpCode   - SubOpCode for the External PDU
 *                    pu1PduData    - Pointer to External data
 *                    u4PduDataLen  - Length of External data
 *                    TxDestMacAddr - Destination Mac Address
 *                    u1OpCode      - Opcode of the PDU to be sent
 *  
 * OUTPUT           : None
 *  
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *  
 **************************************************************************/
PUBLIC INT4
ErpsPortEcfmInitiateExPdu (tEcfmMepInfoParams * pMepInfo, UINT1 u1TlvOffset,
                           UINT1 u1SubOpCode, UINT1 *pu1PduData,
                           UINT4 u4PduDataLen, tMacAddr TxDestMacAddr,
                           UINT1 u1OpCode)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = EcfmInitiateExPdu (pMepInfo, u1TlvOffset, u1SubOpCode,
                                  pu1PduData, u4PduDataLen, TxDestMacAddr,
                                  u1OpCode);

    return (((i4RetVal == ECFM_SUCCESS) ? OSIX_SUCCESS : OSIX_FAILURE));
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortVcmIsSwitchExist 
 *
 * DESCRIPTION      : Routine used to get if the context exists for the input 
 *                    Switch Alias name Information 
 *
 * INPUT            : pu1Alias - Context Name
 *
 * OUTPUT           : pu4ContextId - Context Identifier  
 * 
 * RETURNS          : OSIX_TRUE / OSIX_FALSE   
 * 
 **************************************************************************/
PUBLIC INT4
ErpsPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsSwitchExist (pu1Alias, pu4ContextId);

    return (((i4RetVal == VCM_FALSE) ? OSIX_FALSE : OSIX_TRUE));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ErpsPortVcmIsVcExist                               */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSIX_TRUE / OSIX_FALSE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortVcmIsVcExist (UINT4 u4ContextId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsL2VcExist (u4ContextId);

    return ((i4RetVal == VCM_FALSE) ? OSIX_FALSE : OSIX_TRUE);
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortVcmGetAliasName 
 *
 * DESCRIPTION      : Routine used to get the Alias Name for the context 
 *
 * INPUT            : u4ContextId - Context Identifier 
 *
 * OUTPUT           : pu1Alias - Context Name
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ErpsPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = VCM_FAILURE;

    i4RetVal = VcmGetAliasName (u4ContextId, pu1Alias);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortFmNotifyFaults
 *
 * DESCRIPTION      : This function Sends the trap message to the Fault  
 *                    Manager.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo - pointer to the protection group info structure
 *                    u4PgStatus - status of the protection group. this 
 *                                 can contain the following values -
 *                                 o ERPS_PG_CREATE
 *                                 o ERPS_PG_DELETE
 *                                 o ERPS_PG_SWITCH_TO_WORKING
 *                                 o ERPS_PG_SWITCH_TO_PROTECTION
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ErpsPortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                        UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                        UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_ERPS;

    /*  The following information can be added in new version of FM module */
    /*
       FmFaultMsg.pEnterpriseOid = pEnterpriseOid;
       FmFaultMsg.pCxtName = pContextName;
       FmFaultMsg.u4GenTrapType = u4GenTrapType;
       FmFaultMsg.u4SpecTrapType = u4SpecTrapType;
     */
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (pContextName);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC
            ("ErpsPortFmNotifyFaults: Sending Notification to FM failed.\r\n");
        if (pTrapMsg != NULL)
        {
            SNMP_AGT_FreeVarBindList (pTrapMsg);
        }

    }
#else
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (pContextName);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pTrapMsg);
    UNUSED_PARAM (pu1Msg);
    if (pTrapMsg != NULL)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
    }

#endif
    return;
}

/******************************************************************************
 * Function Name      : ErpsPortNotifyProtocolShutStatus
 *
 * Description        : This function sends indication to MSR on module
 *                      shutdown in a context
 *
 * Input(s)           : apu1Token      - Poiner to token array
 *                      pu4TraceOption - Pointer to trace option
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
PUBLIC VOID
ErpsPortNotifyProtocolShutStatus (UINT4 u4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 3;

    /* The oid list contains the list of Oids that has been registered
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fserps, (sizeof (fserps) / sizeof (UINT4)),
                      au1ObjectOid[0]);

    SNMPGetOidString (FsErpsCtxtSystemControl,
                      (sizeof (FsErpsCtxtSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the erps shutdown, with
     * erps oids and its system control object
     */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, (INT4) u4ContextId);
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return;
}

/******************************************************************************
 * Function Name      : ErpsPortIssGetContextMacAddress 
 *
 * Description        : This function used to get the Context's MAC address
 *                      from the ISS module. This MAC address will be used
 *                      as Node ID in the R-APS PDU.
 *
 * Input(s)           : u4ContextId   - Context Identifier.ray
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
ErpsPortIssGetContextMacAddress (UINT4 u4ContextId)
{
    IssGetContextMacAddress (u4ContextId, ERPS_NODE_ID (u4ContextId));

    return;
}

/******************************************************************************
 * Function Name      : ErpsPortVlanDeleteFdbEntries 
 *
 * Description        : This function flushes the FDB entries in FDB table 
 *                      learnt on this port. ERPS will invoke this function for
 *                      both the ring ports, when it requires to flush the FDB
 *                      table for a ring node.
 *
 * Input(s)           : u4IfIndex   - Interface index of the port
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ErpsPortVlanDeleteFdbEntries (UINT4 u4ContextId,
                              UINT4 u4IfIndex, UINT1 u1ProtectionType,
                              UINT2 u2VlanGroupId, VOID *pContext,
                              tErpsLspPwInfo * pErpsLspPwInfo)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;
    tVlanNpWrFsMiVlanHwFlushFdbId *pEntry = NULL;
    UINT4               u4HwVpnId = 0;
    UINT4               u4Index = 0;
    UINT1               u1RetVal = 0;
    BOOL1               bResult = 0;
#endif
    INT4                i4RetVal = VLAN_FAILURE;
    UINT1               u1ProtocolId = 0;
#ifndef NPAPI_WANTED
    UINT4               u4Index = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    BOOL1               bResult = 0;
    UINT4               u4PwIfIndex=0;
    UINT4               u4PwId=0;
#endif
    UNUSED_PARAM (pContext);
    if (u4IfIndex == 0)
    {
        return OSIX_FAILURE;
    }

    if ((u4IfIndex >= CFA_MIN_PSW_IF_INDEX && u4IfIndex <= CFA_MAX_PSW_IF_INDEX)
        && (pErpsLspPwInfo != NULL))
    {
#ifdef NPAPI_WANTED

        for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
        {

            ERPS_IS_SUBPORT_PORT1 (pErpsLspPwInfo, u4Index, u1RetVal);

            if (bResult == OSIX_TRUE)
            {
                /* Flush the MAC entries learned on the VPN */

                u4HwVpnId = ERPS_GET_VPN_ID (pErpsLspPwInfo, u4Index);

                MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

                NP_UTIL_FILL_PARAMS (FsHwNp, NP_VLAN_MOD,
                                     FS_MI_VLAN_HW_FLUSH_FDB_ID, 0, 0, 0);

                pVlanNpModInfo = &(FsHwNp.VlanNpModInfo);
                pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushFdbId;

                pEntry->u4ContextId = u4ContextId;
                pEntry->u4Fid = u4HwVpnId;

                u1RetVal = VlanNpWrHwProgram (&FsHwNp);

            }
        }
        UNUSED_PARAM(u1RetVal); 
#else
	if ((pInMplsApiInfo = (tMplsApiInInfo *)
				MemAllocMemBlk (gErpsGlobalInfo.MplsApiInInfoPoolId)) == NULL)
	{                
		ERPS_GLOBAL_TRC("ErpsPortVlanDeleteFdbEntries: Failed to allocate memory. ");
		return OSIX_FAILURE;
	}

	if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
				MemAllocMemBlk (gErpsGlobalInfo.MplsApiOutInfoPoolId)) == NULL)
	{
		ERPS_GLOBAL_TRC("ErpsPortVlanDeleteFdbEntries: Failed to allocate memory. ");
		MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
				(UINT1 *) pInMplsApiInfo);

		return OSIX_FAILURE;
	}

	for (u4Index = 1; u4Index <= ERPS_MAX_SIZING_SUB_PORTS;
			u4Index++)
	{
		MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
		MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));
		/* Loop through the Subport List corresponding to both ring ports */
		ERPS_IS_SUBPORT_PORT1 (pErpsLspPwInfo, u4Index, bResult);
		if (bResult == OSIX_FALSE)
		{
			/* If the bit is not set, see if the bit is set in
			 *                          * subportlist 2*/
			ERPS_IS_SUBPORT_PORT2 (pErpsLspPwInfo, u4Index, bResult);
			if (bResult == OSIX_FALSE)
			{
				continue;
			}
		}
		/* This adjustment has been done since in the list First bit
		 *                      * would correspond to first pseudowire and the interface index
		 *                                           * value needs to be adjusted*/                
		u4PwIfIndex = (u4Index + CFA_MIN_PSW_IF_INDEX - 1);                

		/*Fetch the pseudowire value from the pseudowire index
		 *                  *by calling MPLS API*/

		i4RetVal =
			ErpsPortL2VpnApiGetPwIndexFromPwIfIndex (u4PwIfIndex,
					&u4PwId);
		if (i4RetVal != OSIX_TRUE)
		{
			ERPS_CONTEXT_INIT_TRC (u4ContextId,
					CONTROL_PLANE_TRC,
					"ErpsPortVlanDeleteFdbEntries: Failed in quering Pwid mapped to PwIf: %d Continue Scan..\n",
					u4PwIfIndex);

			MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
					(UINT1 *) pInMplsApiInfo);
			MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
					(UINT1 *) pOutMplsApiInfo);
                        /* Querying will fail for second pw if of subring, as dummy pw if is configured for second port of sub-ring
                        so continue the scan*/
                        continue;
			/*return OSIX_FAILURE;*/
		}                
		pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwId;
		pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;                

		if (ErpsPortMplsApiHandleExtRequest
				(MPLS_GET_PW_INFO, pInMplsApiInfo,
				 pOutMplsApiInfo) != OSIX_SUCCESS)
		{
			ERPS_CONTEXT_INIT_TRC (u4ContextId,
					CONTROL_PLANE_TRC,
					"ErpsPortVlanDeleteFdbEntries: Failed in quering PwInfo from PwIndex:%d\n",
					pInMplsApiInfo->InPathId.PwId.u4PwIndex);

			MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
					(UINT1 *) pInMplsApiInfo);
			MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
					(UINT1 *) pOutMplsApiInfo);

			return OSIX_FAILURE;
		}
                ERPS_CONTEXT_INIT_TRC (u4ContextId,
	                         CONTROL_PLANE_TRC,
	                         "ErpsPortVlanDeleteFdbEntries: Flushing FDB learnt for PW-VCIndex: %d)\n",
	                         pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4PwIndex);
		VlanVplsDelPwFdbEntries(pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4PwIndex);
	}
	MemReleaseMemBlock (gErpsGlobalInfo.MplsApiInInfoPoolId,
			(UINT1 *) pInMplsApiInfo);
	MemReleaseMemBlock (gErpsGlobalInfo.MplsApiOutInfoPoolId,
			(UINT1 *) pOutMplsApiInfo);	
                                                                                    
        return OSIX_SUCCESS;
#endif
    }
    else
    {
        /* OPTIMIZE flag indicates, whether this call can be grouped with the flush
         * call for other ports as a single flush call. ERPS will pass only
         * NO_OPTIMIZE flag */
        if (u1ProtectionType == ERPS_PORT_BASED_PROTECTION)
        {
            i4RetVal = VlanDeleteFdbEntries (u4IfIndex, VLAN_NO_OPTIMIZE);
            return (((i4RetVal == VLAN_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
        }
        else if (u1ProtectionType == ERPS_SERVICE_BASED_PROTECTION)
        {
            if (OSIX_SUCCESS == ErpsUtilCheckIfIcclInterface (u4IfIndex))
            {
                    return OSIX_SUCCESS;
            }
            if (L2IwfGetPortStateCtrlOwner (u4IfIndex, &u1ProtocolId,OSIX_TRUE) ==
                L2IWF_SUCCESS)
            {

                if (u1ProtocolId == ERPS_MODULE)
                {
                    ERPS_CONTEXT_INIT_TRC (u4ContextId,
                                           CONTROL_PLANE_TRC,
                                           "L2IwfFlushFdbForGivenInst function called for ERPS_SERVICE_BASED_PROTECTION\n");
                    i4RetVal =
                        L2IwfFlushFdbForGivenInst (u4ContextId, u4IfIndex,
                                                   u2VlanGroupId,
                                                   VLAN_NO_OPTIMIZE,
                                                   ERPS_MODULE, TRUE);
                    return (((i4RetVal ==
                              L2IWF_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
                }

            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : ErpsPortEcfmMepRegAndGetFltStat                
 *                                                                            
 * Description        : Routine used by protocols to register with ECFM.      
 *                                                                            
 * Input(s)           : pEcfmReg - Reg. params to be provided by protocols    
 *                      pMepInfo - Pointer to Mep information        
 *                      that needs to be monitored by the ECFM.               
 *                                                                            
 * Output(s)          : None                                                  
 *                                                                            
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                            
 *****************************************************************************/
PUBLIC INT4
ErpsPortEcfmMepRegAndGetFltStat (tEcfmRegParams * pEcfmReg,
                                 tEcfmEventNotification * pMepInfo)
{
    INT4                i4RetVal = ECFM_FAILURE;

    i4RetVal = EcfmMepRegisterAndGetFltStatus (pEcfmReg, pMepInfo);

    return (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/*****************************************************************************
 * Function Name      : ErpsPortEcfmMepDeRegister                
 *                                                                            
 * Description        : Routine used by protocols to deregister with ECFM.      
 *                                                                            
 * Input(s)           : pEcfmReg - Reg. params to be provided by protocols    
 *                      pMepInfo - Pointer to Mep information        
 *                      that needs to be monitored by the ECFM.               
 *                                                                            
 * Output(s)          : None                                                  
 *                                                                            
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                            
 *****************************************************************************/
PUBLIC INT4
ErpsPortEcfmMepDeRegister (tEcfmRegParams * pEcfmReg,
                           tEcfmEventNotification * pMepInfo)
{
    INT4                i4RetVal = ECFM_FAILURE;

    i4RetVal = EcfmMepDeRegister (pEcfmReg, pMepInfo);

    return (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/*****************************************************************************/
/* Function Name      : ErpsPortL2IwfGetPortOperStatus                       */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u4IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
ErpsPortL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                                UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u4IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : ErpsPortL2IwfSetInstPortState                    */
/*                                                                           */
/* Description        : This routine update the Port's state for that        */
/*                       instance in the L2Iwf common database.              */
/*                                                                           */
/* Input(s)           : u2InstanceId - InstanceId for which the port state   */
/*                                  is to be updated.                        */
/*                    : u4IfIndex - Global IfIndex of the port whose state   */
/*                                  is to be updated.                        */
/*                      u1PortState - Port state to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
ErpsPortL2IwfSetInstPortState (UINT2 u2InstanceId, UINT4 u4IfIndex,
                               UINT1 u1PortState)
{
    UINT4 u4ContextId = ERPS_DEFAULT_CONTEXT_ID;
    UNUSED_PARAM (u4ContextId);
    ERPS_CONTEXT_INIT_TRC (u4ContextId,
	    CONTROL_PLANE_TRC,
	    "ErpsPortL2IwfSetInstPortState: Request to set PORT:%d state ((Disabled:1 Discard:2 Fwd:5): %d)\n",
             u4IfIndex, u1PortState);
    return (L2IwfSetInstPortState (u2InstanceId, u4IfIndex, u1PortState));
}

/*****************************************************************************/
/* Function Name      : ErpsPortL2IwfSetPortStateCtrlOwner                   */
/*                                                                           */
/* Description        : This routine is called to set the Owner protocol for */
/*                      the given port to avoid race condition between       */
/*                      protocol, while updating same port info.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose owner   */
/*                                  protocol to be set.                      */
/*                      u1ProtId  - Owner Protocol Id.                       */
/*                      u1OwnerStatus - L2IWF_SET - to set the given Owner Id*/
/*                                      L2IWF_RESET - to reset Owner Id.     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/
INT4
ErpsPortL2IwfSetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 u1ProtId,
                                    UINT1 u1OwnerStatus)
{
    return (L2IwfSetPortStateCtrlOwner (u4IfIndex, u1ProtId, u1OwnerStatus));
}

/*****************************************************************************/
/* Function Name      : ErpsPortIssGetAsyncMode                              */
/*                                                                           */
/* Description        : This function calls the ISS module to check          */
/*                      NPAPI operating in Synchronous or Asynchronous mode  */
/*                                                                           */
/* Input(s)           : i4ProtoId - Protocol Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
ErpsPortIssGetAsyncMode (INT4 i4ModuleId)
{
    return (IssGetAsyncMode (i4ModuleId));
}

/*****************************************************************************/
/* Function Name      : ErpsPortExternalEventNotify                          */
/*                                                                           */
/* Description        : This routine populates the tErpsEventNotification    */
/*                      structure from the tEcfmEventNotification structure. */
/*                      tErpsEventNotification can be used without           */
/*                      restrictions within the ERPS module                  */
/*                                                                           */
/* Input(s)           : pEvent - pointer to tEcfmEventNotification structure */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
ErpsPortExternalEventNotify (tEcfmEventNotification * pEvent)
{
    tErpsEventNotification ErpsEvent;
    MEMSET (&ErpsEvent, 0, sizeof (tErpsEventNotification));

    ErpsEvent.u4Event = pEvent->u4Event;
    ErpsEvent.u4ContextId = pEvent->u4ContextId;
    ErpsEvent.u4MdIndex = pEvent->u4MdIndex;
    ErpsEvent.u4MaIndex = pEvent->u4MaIndex;
    ErpsEvent.u2MepId = pEvent->u2MepId;
    ErpsEvent.u4IfIndex = pEvent->u4IfIndex;
    ErpsEvent.u4VlanIdIsid = pEvent->u4VlanIdIsid;
    ErpsEvent.u1CfmPduOffset = pEvent->PduInfo.u1CfmPduOffset;
    ErpsEvent.pu1Data = pEvent->PduInfo.pu1Data;
    ErpsEvent.u1Direction = pEvent->u1Direction;
    ErpsApiExternalEventNotify (&ErpsEvent);
    return;
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmRegisterProtocols                          */
/*                                                                           */
/* Description        : This function calls the RM module to register ERPS.  */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmRegisterProtocols (pRmReg);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmDeRegisterProtocols                        */
/*                                                                           */
/* Description        : This function deregisters ERPS with RM module.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortRmDeRegisterProtocols (VOID)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmDeRegisterProtocols (RM_ERPS_APP_ID);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmGetNodeState                               */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
ErpsPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmGetStandbyNodeCount                        */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Booted up standby nodes                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
ErpsPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return 0;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmApiHandleProtocolEvent                     */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pEvt);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmEnqMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                                     u4SrcEntId, u4DestEntId);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmReleaseMemoryForMsg                        */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ErpsPortRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmReleaseMemoryForMsg (pu1Block);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortRmSetBulkUpdatesStatus                       */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsPortRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmSetBulkUpdatesStatus (u4AppId);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (u4AppId);
    return OSIX_SUCCESS;
#endif
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortRmApiSendProtoAckToRM
 *
 * DESCRIPTION      : This is the function used by protocols/applications
 *                    to send acknowledgement to RM after processing the
 *                    sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pProtoAck);
    return OSIX_SUCCESS;
#endif
}

/* HITLESS RESTART */
/****************************************************************************/
/* FUNCTION NAME    : ErpsPortSendStdyStPkt                                 */
/*                                                                          */
/* DESCRIPTION      : This function is called by L2IWF Module when steady   */
/*                    state packet is to be sent during hitless restart     */
/*                    ERPS_LOCK should be taken before calling this API     */
/*                                                                          */
/* INPUT            : pBuf      - R-APS PDU                                 */
/*                    u4PktSize - Packet Size                               */
/*                    u4IfIndex - Interface Index                           */
/*                                                                          */
/* OUTPUT           : None                                                  */
/*                                                                          */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE.                            */
/*                                                                          */
/****************************************************************************/
PUBLIC INT4
ErpsPortSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
                       UINT4 u4IfIndex)
{
    UINT4               u4PeriodicTime = 0;

    if (ERPS_HR_STDY_ST_REQ_RCVD () == OSIX_FALSE)
    {
        return OSIX_FAILURE;
    }

    if (ErpsUtilGetPeriodicTime (u4IfIndex, &u4PeriodicTime) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (ErpsRedHRSendStdyStPkt (pBuf, u4PktSize, (UINT2) u4IfIndex,
                                u4PeriodicTime) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortMplsApiHandleExtRequest
 *
 * DESCRIPTION      : This function gets the MPLS information based on the
 *                    request (Tunnel/Pw). 
 *
 * INPUT            : u4MainReqType - Request type
 *                    pInMplsApiInfo - Pointer to the input API information.
 *
 * OUTPUT           : pOutMplsApiInfo - Pointer to the output API information.
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsPortMplsApiHandleExtRequest (UINT4 u4MainReqType,
                                 tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo)
{
#ifdef MPLS_WANTED
    if (MplsApiHandleExternalRequest (u4MainReqType, pInMplsApiInfo,
                                      pOutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4MainReqType);
    UNUSED_PARAM (pInMplsApiInfo);
    UNUSED_PARAM (pOutMplsApiInfo);
#endif
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsPortL2VpnApiGetPwIndexFromPwIfIndex
 *
 * Description   : This function check that whether pseudowire entry is      
 *                 present for the PW IF INDEX passed and returns PW INDEX   
 * Input(s)      : u4PwIfIndex - Index of the Pseudowire IfIndex in Iftable  
 * Output(s)     : pu4PwIndex  - pseudowire index in mpls                    
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsPortL2VpnApiGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex)
{
#ifdef MPLS_WANTED
    if (L2VpnApiGetPwIndexFromPwIfIndex (u4PwIfIndex, pu4PwIndex) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4PwIfIndex);
    UNUSED_PARAM (pu4PwIndex);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ErpsPortCfaCliGetIfList                              */
/*                                                                           */
/* Description        : This function calls the functions which convert      */
/*                      the given string to interface list                   */
/*                                                                           */
/* Input(s)           : pi1IfName :Port Number                               */
/*                      pi1IfListStr : Pointer to the string                 */
/*                      pu1IfListArray:Pointer to the string in which the    */
/*                      bits for the port list will be set                   */
/*                      u4IfListArrayLen:Array Length                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
ErpsPortCfaGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr,
                      UINT1 *pu1IfListArray, UINT4 u4IfListArrayLen)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfList (pi1IfName, pi1IfListStr, pu1IfListArray,
                             u4IfListArrayLen));
#else
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ErpsPortL2IwfIsRapsVlanPresent                       */
/*                                                                           */
/* Description        : This routine validates whether RAPS_VLAN ID is       */
/*                      mapped to the instance.                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                    : u2MstInst   - InstanceId to which the list of vlan   */
/*                      mapped are obtained.                   */
/*                      u2RapsVlanId  - RAPS VlanId which is mappped to the    */
/*                      Instance.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
ErpsPortL2IwfIsRapsVlanPresent (UINT4 u4ContextId, UINT2 u2RapsVlanId,
                                UINT2 u2MstInst)
{
    return (L2IwfIsRapsVlanPresent (u4ContextId, u2RapsVlanId, u2MstInst));
}

/*****************************************************************************/
/*                                                                           */
/* Function Name       : ErpsPortGetVlanIdEntry                              */
/*                                                                           */
/* Description         : This function checks that whether this VLAN is      */
/*                       already present or not                              */
/*                                                                           */
/*                                                                           */
/* Input(s)            : u4FsErpsContextId - Context Identifier              */
/*                       u2FsErpsVlanId - Vlan Identifier                    */
/*                                                                           */
/* Output(s)           : NONE                                                */
/*                                                                           */
/*                                                                           */
/* Returns             : VLAN_FAILURE/VLAN_SUCCESS.                          */
/*****************************************************************************/
INT4
ErpsPortGetVlanIdEntry (UINT4 u4FsErpsContextId, tVlanId u2FsErpsVlanId)
{
    return (VlanGetVlanIdEntry (u4FsErpsContextId, u2FsErpsVlanId));
}

/***********************************************************************/
/*  Function Name   : ErpsUtilGetMcLagStatus                           */
/*  Description     : The function returns the MC-LAG status of the    */
/*                    node                                             */
/*  Input(s)        : None                                             */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
ErpsUtilGetMcLagStatus (VOID)
{
#ifdef ICCH_WANTED
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
         return OSIX_SUCCESS;
    }
#endif
    return OSIX_FAILURE;
}

/***********************************************************************/
/*  Function Name   : ErpsUtilCheckIfIcclInterface                     */
/*  Description     : The function checks if the given interface is an */
/*                    ICCL interface                                   */
/*  Input(s)        : u4Index                                          */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
ErpsUtilCheckIfIcclInterface (UINT4 u4Index)
{
#ifdef ICCH_WANTED
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
       if (IcchIsIcclInterface(u4Index) == OSIX_TRUE)
       {
            return OSIX_SUCCESS;
       }
    }
#endif
    UNUSED_PARAM (u4Index);
    return OSIX_FAILURE;
}

/***********************************************************************/
/*  Function Name   : ErpsUtilCheckIfMcLagInterface                    */
/*  Description     : The function checks if the given interface is an */
/*                    MC-LAG interface                                 */
/*  Input(s)        : u4Index                                          */
/*  Output(s)       : None                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                        */
/* *********************************************************************/
INT4
ErpsUtilCheckIfMcLagInterface (UINT4 u4Index)
{
#ifdef ICCH_WANTED
    UINT1        u1McLagInterface = OSIX_FALSE;
    if (LaGetMCLAGSystemStatus() == LA_ENABLED)
    {
        LaApiIsMclagInterface (u4Index, &u1McLagInterface);
        if (u1McLagInterface == OSIX_TRUE)
        {
            return OSIX_SUCCESS;
        }
    }
#endif
    UNUSED_PARAM (u4Index);
    return OSIX_FAILURE;
}

#endif
