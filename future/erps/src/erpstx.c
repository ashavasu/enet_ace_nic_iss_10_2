/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpstx.c,v 1.26 2016/01/11 13:18:53 siva Exp $
 *
 * Description: This file contains the ERPS Msg related functions        
 *                                                        
 *****************************************************************************/
#ifndef _ERPSTX_C
#define _ERPSTX_C

#include "erpsinc.h"

PRIVATE VOID        ErpsTxAddRapsTxInfoToSll (tErpsRapsTxInfo * pRaps);
PRIVATE VOID        ErpsTxCalculateTxEntryCount (UINT4 *pu4Count);
PRIVATE VOID        ErpsTxScanSllsAndTxPdu (VOID);
PRIVATE VOID        ErpsTxFormRapsPdu (tErpsRingInfo * pRingInfo,
                                       UINT1 **ppu1ApsPdu);
PRIVATE VOID        ErpsTxDelEntriesInSll (tTMO_SLL * pTxSll);

/*****************************************************************************/
/*    Function Name       : ErpsTxStartRapsMessages                          */
/*                                                                           */
/*    Description         : This function will start the transmission of     */
/*                          RAPS PDU with the Request/State field set to the */
/*                          value defined by message type and status bits    */
/*                          RAPS messages are transmitted over both ring     */
/*                          ports if they are operational.                   */
/*                                                                           */
/*    Input (s)           : pRingInfo - Pointer to ring node for which a    */
/*                                       RAPS messages needs to be transmitt-*/
/*                                       ed.                                 */
/*                          u1StatusBits - Status bit that needs to be encod-*/
/*                                         ded into RAPS messages.           */
/*                          u1RapsMessageType - RAPS message type that needs */
/*                                         to be sent out.                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS -                                   */
/*                          OSIX_FAILURE -                                   */
/*****************************************************************************/
PUBLIC INT4
ErpsTxStartRapsMessages (tErpsRingInfo * pRingInfo, UINT1 u1StatusBits,
                         UINT1 u1RapsMessageType)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT2               u2ReqSubcodeAndStatus = 0;
    UINT2               u2PrevReqSubcodeAndStatus = 0;

    /* NOTE: In the current ERPS Version 2, Subcode is always encoded as 
     * 0 and other values are of future use. Hence currently u1SubCode is not
     * passed as an function argument or stored in an global structure */
    UINT1               u1SubCode = ERPS_SUB_CODE_FLUSH_REQ;

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsTxStartRapsMessages Ring Info is NULL\r\n");
        return OSIX_FAILURE;
    }

    /* ERPS messages should be transmitted only from Active Node */
    if (ErpsPortRmGetNodeState () == RM_STANDBY)
    {
        return OSIX_SUCCESS;
    }

    /* RAPS Packets Received by Sub-Ring Interconnected node via
     * Virtual Channel with UP-MEP level, will forward the 
     * received packet only changing the TxAction of Ring Node.
     * So we should not store LastMsg Send and
     * Port status updation*/

    if (pRingInfo->u1IsMsgRcvd == OSIX_FALSE)
    {
        /* Store the Previous msg send */
        u2PrevReqSubcodeAndStatus = pRingInfo->u2LastMsgSend;

        /* Compute the first two bytes of R-APS message */
        u2ReqSubcodeAndStatus = u1RapsMessageType;
        u2ReqSubcodeAndStatus =
            (UINT2) (((u2ReqSubcodeAndStatus) << 12) | ((u1SubCode) << 8) |
                     (u1StatusBits));

        /* PDUs have to be sent */
        pRingInfo->u1RingPduSend = ERPS_RING_PDU_SEND;

        /* Store the R-APS Message that is going to be transmitted in 
         * Ring's u2LastMsgSend field. This will be used in periodic
         * transmission */
        RingDynInfo.u2LastMsgSend = u2ReqSubcodeAndStatus;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_LAST_MSG_TYPE);

        if (ErpsPortIssGetAsyncMode (ERPS_MODULE) ==
            ISS_NPAPI_MODE_ASYNCHRONOUS)
        {

            if ((((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ==
                  ERPS_PORT_BLOCKING)
                 && ((pRingInfo->u1Port1HwStatus & ERPS_PORT_STATE_BLOCKING) !=
                     ERPS_PORT_STATE_BLOCKING))
                ||
                (((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) !=
                  ERPS_PORT_BLOCKING)
                 && ((pRingInfo->u1Port1HwStatus & ERPS_PORT_BLOCKING) ==
                     ERPS_PORT_STATE_BLOCKING)))
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "Npapi mode is ASYNC ; H/w status of port with "
                                  "IfIndex %d has not been updated; So R-APS msg "
                                  "type %d is not transmitted. This port's "
                                  "current state in S/w is %d and H/w is %d\r\n",
                                  pRingInfo->u4Port1IfIndex, u1RapsMessageType,
                                  pRingInfo->u1Port1Status,
                                  pRingInfo->u1Port1HwStatus);

                return OSIX_SUCCESS;
            }

            if ((((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) ==
                  ERPS_PORT_BLOCKING)
                 && ((pRingInfo->u1Port2HwStatus & ERPS_PORT_STATE_BLOCKING) !=
                     ERPS_PORT_STATE_BLOCKING))
                ||
                (((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) !=
                  ERPS_PORT_BLOCKING)
                 && ((pRingInfo->u1Port2HwStatus & ERPS_PORT_BLOCKING) ==
                     ERPS_PORT_STATE_BLOCKING)))
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "Npapi mode is ASYNC ; H/w status of port with "
                                  "IfIndex %d has not been updated; So R-APS msg "
                                  "type %d is not transmitted. This port's "
                                  "current state in S/w is %d and H/w is %d\r\n",
                                  pRingInfo->u4Port2IfIndex, u1RapsMessageType,
                                  pRingInfo->u1Port2Status,
                                  pRingInfo->u1Port2HwStatus);

                return OSIX_SUCCESS;
            }
        }
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
	    CONTROL_PLANE_TRC,
	    "TX RAPS PDU MSGTYPE (FS:0xD  SF:0xB  MS:0x7  EVENT:0xE  NR:0x0):--%d "
	    "Status Bit (RB:0x80  DNF:0x40): --%d \r\n",
	    u1RapsMessageType, u1StatusBits);
    if (ErpsTxFormAndSendRAPSPdu (pRingInfo) != OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTxStartRapsMessages cannot send the first"
                          "RAPS PDU\r\n");
        return OSIX_FAILURE;
    }

    if ((pRingInfo->u1IsMsgRcvd == OSIX_TRUE)
        && (pRingInfo->u4Port2IfIndex == 0))
    {
        if (u1RapsMessageType == ERPS_MESSAGE_TYPE_EVENT)
        {
            /* Periodic Timer will not be started for Event PDU, Also LastMsgSend
             * will not be stored as Event. LastMsgSend variable will be resetted
             * to previous value, after sending the FastTxn of Event Msg */

            RingDynInfo.u2LastMsgSend = u2PrevReqSubcodeAndStatus;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_LAST_MSG_TYPE);
        }
        return OSIX_SUCCESS;

    }
    else
    {
        if (ErpsTxInitiateFastTxn (pRingInfo) != OSIX_SUCCESS)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTxStartRapsMessages cannot initiate the "
                              "transmission RAPS PDU\r\n");
            return OSIX_FAILURE;
        }

        if (u1RapsMessageType == ERPS_MESSAGE_TYPE_EVENT)
        {
            /* Periodic Timer will not be started for Event PDU, Also LastMsgSend
             * will not be stored as Event. LastMsgSend variable will be resetted
             * to previous value, after sending the FastTxn of Event Msg */

            RingDynInfo.u2LastMsgSend = u2PrevReqSubcodeAndStatus;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_LAST_MSG_TYPE);
            return OSIX_SUCCESS;
        }

        /* Start the first periodic timer for periodic timer duration 
         * + ERPS_FAST_TRANSMISSION_INTERVAL, so that first will be sent only 
         * after periodic time value from the last Fast transmission */

        /* Stop periodic timer before restarting */

        if (ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTxStartRapsMessages periodic timer stop  "
                              "failed\r\n");
            return OSIX_FAILURE;
        }

        if (ErpsTmrStartTimer (pRingInfo, ERPS_PERIODIC_TMR,
                               ERPS_PERIODIC_TIMER_INTERVAL (pRingInfo))
            == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTxStartRapsMessages periodic timer start "
                              "failed\r\n");
            return OSIX_FAILURE;

        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTxStopRapsMessages                           */
/*                                                                           */
/*    Description         : This function will stops the transmission of any */
/*                          type of RAPS messages, except event messages.    */
/*                                                                           */
/*    Input (s)           : pRingInfo - Pointer to ring node for which a    */
/*                                       RAPS messages needs to be transmitt-*/
/*                                       ed.                                 */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS -                                   */
/*                          OSIX_FAILURE -                                   */
/*****************************************************************************/
PUBLIC INT4
ErpsTxStopRapsMessages (tErpsRingInfo * pRingInfo)
{
    unErpsRingDynInfo   RingDynInfo;

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTxStopRapsMessages periodic timer stop  "
                          "failed\r\n");
        return OSIX_FAILURE;
    }

    RingDynInfo.u2LastMsgSend = 0;
    pRingInfo->u1RingPduSend = ERPS_RING_PDU_DONT_SEND;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                               ERPS_RING_LAST_MSG_TYPE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ErpsTxFormRapsPdu
 *                                                                          
 *    DESCRIPTION      : This function forms the R-APS PDU with necessary info
 *
 *    INPUT            :
 *                                                                          
 *    OUTPUT           :
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ErpsTxFormRapsPdu (tErpsRingInfo * pRingInfo, UINT1 **ppu1ApsPdu)
{
    UINT1               u1RequestAndSubCode = 0;
    UINT1               u1Status = 0;

    /* Currently the MEL, OpCode, Version are filled by ECFM-Y.1731 itself, 
       hence R-APS specific information alone is filled here */

    /* RAPS Packets Received by Sub-Ring Interconnected node via
     * Virtual Channel with UP-MEP level, will forward the 
     * received packet only changing the TxAction of Ring Node.
     * So we should not store LastMsg Send and action should not 
     * be taken based on LastMsgSend which is set by Node 
     * Transmitting the RAPS Packets*/

    if (pRingInfo->u1IsMsgRcvd == OSIX_FALSE)
    {
        u1RequestAndSubCode |=
            ((pRingInfo->u2LastMsgSend & 0xFF00) >> ERPS_SHIFT_8BITS);
        u1Status |= (pRingInfo->u2LastMsgSend & 0x00FF);

        /* BPR bit in R-APS Pdu is Set, When Ring Port 2 is blocked.
         * As per ITU-T G.8032/Y.1344 (03/2010) in Section 10.3, C.3 BPR bit
         * encoding.
         * BPR bit should not be set when both the ports are Blocked.
         */
        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)
            && (pRingInfo->u1FlushLogicSupport == OSIX_ENABLED))
        {
            if ((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)
                && ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)
                    != ERPS_PORT_BLOCKING))
            {
                u1Status |= ERPS_BPR_BIT_SET;
            }
        }

        ERPS_PUT_1BYTE (*ppu1ApsPdu, u1RequestAndSubCode);
        ERPS_PUT_1BYTE (*ppu1ApsPdu, u1Status);

        /* Filling up NodeId field */
        MEMCPY (*ppu1ApsPdu, ERPS_NODE_ID (pRingInfo->u4ContextId),
                sizeof (tMacAddr));
    }

    /* Filling up the received node id instead of node which
     * is sending the packet.
     * Filling up the Status Bits which is received on RAPS packets
     * When u1IsMsgRcvd is set as True:

     * 1. Node which is calling the ErpsTxFormRapsPdu function 
     *    is the interconnected node and not the one
     *    which is initiating the RAPS packets

     * 2. To comply with Implementation of  Failure of protocol defect (FOP)
     *    feature as per Section 10.4 in ITU-T G.8032 Y.1344(03/2010).
     *    we are putting the received RAPS packet node Id */

    if (pRingInfo->u1IsMsgRcvd == OSIX_TRUE)
    {
        if (gErpsRcvdMsg.u1RBBitStatus)
        {
            u1Status = u1Status | ERPS_RB_BIT_SET;
        }
        if (gErpsRcvdMsg.u1DNFBitStatus)
        {
            u1Status = u1Status | ERPS_DNF_BIT_SET;
        }
        if (gErpsRcvdMsg.u1BPRBitStatus)
        {
            u1Status = u1Status | ERPS_BPR_BIT_SET;
        }

        u1RequestAndSubCode =
            (UINT1) ((gErpsRcvdMsg.u1MsgType << ERPS_REQ_FIELD_OFFSET));

        ERPS_PUT_1BYTE (*ppu1ApsPdu, u1RequestAndSubCode);
        ERPS_PUT_1BYTE (*ppu1ApsPdu, u1Status);
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
	    CONTROL_PLANE_TRC,
	    "TX RAPS PDU with Req&Subcode:%d, Status:%d\r\n",
	    u1RequestAndSubCode, u1Status);
        MEMCPY (*ppu1ApsPdu, gErpsRcvdMsg.RcvdNodeId, sizeof (tMacAddr));
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ErpsTxAddApsTxInfoToSll
 *                                                                          
 *    DESCRIPTION      : This function adds the APS Tx Info to the SLL and 
 *                       post the event to APS task. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
ErpsTxInitiateFastTxn (tErpsRingInfo * pRingInfo)
{
    tErpsRapsTxInfo    *pRaps = NULL;
    UINT2               u2LastMsgSend = 0;

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if ((pRaps = (tErpsRapsTxInfo *)
         MemAllocMemBlk (gErpsGlobalInfo.RapsTxInfoTablePoolId)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsTxInitiateFastTxn: Mem alloc FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTxAllocBuffAndFormRapsPdu (pRaps, pRingInfo) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gErpsGlobalInfo.RapsTxInfoTablePoolId,
                            (UINT1 *) pRaps);
        return OSIX_FAILURE;
    }

    /* R-APS PDU's will be sent out on both the Ring Ports by Default.
     * But in Some Scenarios like when Optimized flush feature is enabled
     * and if the port is remote then R-APS PDU's should not be sent on Remote
     * port, this is handled here */
    switch (pRingInfo->u1RingTxAction)
    {
        case ERPS_TX_RAPS_ON_BOTH_RING_PORTS:
            if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
                && (pRingInfo->u1IsPort2Present ==
                    ERPS_PORT_IN_LOCAL_LINE_CARD))
            {
                pRaps->u1RingTxAction = pRingInfo->u1RingTxAction;
            }
            else if (pRingInfo->u1IsPort1Present ==
                     ERPS_PORT_IN_LOCAL_LINE_CARD)
            {
                pRaps->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;
            }
            else
            {
                pRaps->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;
            }
            break;

        case ERPS_TX_RAPS_ON_RING_PORT_1:
            if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
            {
                MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                                    pRaps->pu1RApsPdu);
                MemReleaseMemBlock (gErpsGlobalInfo.RapsTxInfoTablePoolId,
                                    (UINT1 *) pRaps);
                return OSIX_SUCCESS;
            }
            pRaps->u1RingTxAction = pRingInfo->u1RingTxAction;
            break;

        case ERPS_TX_RAPS_ON_RING_PORT_2:
            if (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
            {
                MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                                    pRaps->pu1RApsPdu);
                MemReleaseMemBlock (gErpsGlobalInfo.RapsTxInfoTablePoolId,
                                    (UINT1 *) pRaps);
                return OSIX_SUCCESS;
            }
            pRaps->u1RingTxAction = pRingInfo->u1RingTxAction;
            break;

        default:
            MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                                pRaps->pu1RApsPdu);
            MemReleaseMemBlock (gErpsGlobalInfo.RapsTxInfoTablePoolId,
                                (UINT1 *) pRaps);
            return OSIX_SUCCESS;
    }

    ErpsTxAddRapsTxInfoToSll (pRaps);

    u2LastMsgSend = pRingInfo->u2LastMsgSend >> 12;

    /* updating the RAPS FS messages transmitted count on both the ports */
    if (u2LastMsgSend == ERPS_MESSAGE_TYPE_FS)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4FSPduTxCount =
                pRingInfo->Port1Stats.u4FSPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4FSPduTxCount =
                pRingInfo->Port2Stats.u4FSPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }
    }
    /* updating the RAPS MS messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_MS)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4MSPduTxCount =
                pRingInfo->Port1Stats.u4MSPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4MSPduTxCount =
                pRingInfo->Port2Stats.u4MSPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }
    }
    /* updating the RAPS NR/NRRB messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_NR)
    {
        /* IF RB bit is set then NRRB Message else NR Message */
        if ((pRingInfo->u2LastMsgSend >> 7) & 0x1)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4NRRBPduTxCount =
                    pRingInfo->Port1Stats.u4NRRBPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4NRRBPduTxCount =
                    pRingInfo->Port2Stats.u4NRRBPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }
        }
        else
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4NRPduTxCount =
                    pRingInfo->Port1Stats.u4NRPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4NRPduTxCount =
                    pRingInfo->Port2Stats.u4NRPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }

        }
    }
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_SF)
    {
        if (pRingInfo->u1RingTxAction == ERPS_TX_RAPS_ON_BOTH_RING_PORTS)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4SFPduTxCount =
                    pRingInfo->Port1Stats.u4SFPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4SFPduTxCount =
                    pRingInfo->Port2Stats.u4SFPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }
        }
        else if (pRingInfo->u1RingTxAction == ERPS_TX_RAPS_ON_RING_PORT_1)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4SFPduTxCount =
                    pRingInfo->Port1Stats.u4SFPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }
        }
        else
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4SFPduTxCount =
                    pRingInfo->Port2Stats.u4SFPduTxCount +
                    ERPS_MAX_RAPS_PDU_TRANSMISSION;
            }
        }
    }
    /* updating the RAPS Event messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_EVENT)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4EventPduTxCount =
                pRingInfo->Port1Stats.u4EventPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4EventPduTxCount =
                pRingInfo->Port2Stats.u4EventPduTxCount +
                ERPS_MAX_RAPS_PDU_TRANSMISSION;
        }
    }

    /* Updation of RingStatistics */
    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        pRingInfo->Port1Stats.u4RapsSentCount =
            pRingInfo->Port1Stats.u4RapsSentCount +
            ERPS_MAX_RAPS_PDU_TRANSMISSION;
    }

    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
    {
        pRingInfo->Port2Stats.u4RapsSentCount =
            pRingInfo->Port2Stats.u4RapsSentCount +
            ERPS_MAX_RAPS_PDU_TRANSMISSION;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxAddRapsTxInfoToSll
 *
 *    DESCRIPTION      : This function adds the R-APS Tx Info to the SLL and
 *                       post the event to R-APS task.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
ErpsTxAddRapsTxInfoToSll (tErpsRapsTxInfo * pRaps)
{
    ERPS_TX_DB_LOCK ();

    TMO_SLL_Add (gErpsGlobalInfo.pRapsTxSll[0],
                 (tTMO_SLL_NODE *) (VOID *) (pRaps));

    if (gErpsGlobalInfo.b1InRapsTxEventHandler == OSIX_FALSE)
    {
        if (OsixEvtSend (gErpsGlobalInfo.RapsTxTaskId, ERPS_RAPS_TX_EVENT)
            == OSIX_FAILURE)
        {
            ERPS_GLOBAL_TRC ("Osix Event send to ERPS Tx Task failed!!!\r\n");
        }
    }

    ERPS_TX_DB_UNLOCK ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxEventHandler
 *
 *    DESCRIPTION      : This function handles the Tx Event raised by ERPS
 *                       main task.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
ErpsTxEventHandler (VOID)
{
    tTMO_SLL           *pTempSll = NULL;
    UINT4               u4Count = 0;

    ERPS_TX_DB_LOCK ();
    gErpsGlobalInfo.b1InRapsTxEventHandler = OSIX_TRUE;

    ErpsTxCalculateTxEntryCount (&u4Count);

    while (u4Count > 0)
    {
        /* Scan all the entries in the SLL and transmit the RAPS PDU */
        ErpsTxScanSllsAndTxPdu ();

        /* Since the entries present in the second SLL completed sending the
         * RAPS PDU (there PDUs) delete the entries present in 2nd SLL */
        ErpsTxDelEntriesInSll (gErpsGlobalInfo.pRapsTxSll[1]);

        /* Move the SLLs accordingly */
        pTempSll = gErpsGlobalInfo.pRapsTxSll[1];
        gErpsGlobalInfo.pRapsTxSll[1] = gErpsGlobalInfo.pRapsTxSll[0];
        gErpsGlobalInfo.pRapsTxSll[0] = pTempSll;

        ERPS_TX_DB_UNLOCK ();

        /* The task has to be delayed by 3.3 milli seconds i.e.,3300000 
         * nanoseconds.
         * To delay the task by 3.3 milliseconds,we are passing a value 
         * of 3300 to OsixDelay, which internally converts the value to 
         * 3300000 nanoseconds.*/

        OsixDelay ((UINT4) (ERPS_FAST_TRANSMISSION_INTERVAL * 1000),
                   (INT4) OSIX_MICRO_SECONDS);

        ERPS_TX_DB_LOCK ();

        ErpsTxCalculateTxEntryCount (&u4Count);
    }

    gErpsGlobalInfo.b1InRapsTxEventHandler = OSIX_FALSE;

    ERPS_TX_DB_UNLOCK ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxCalculateTxEntryCount
 *
 *    DESCRIPTION      : This function calculates the number of entries
 *                       present in all the Tx SLLs.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Entries Count
 *
 ****************************************************************************/
PRIVATE VOID
ErpsTxCalculateTxEntryCount (UINT4 *pu4Count)
{
    UINT4               u4Count = 0;
    UINT1               u1Index = 0;

    for (; u1Index < ERPS_MAX_RAPS_PDU_TRANSMISSION; u1Index++)
    {
        u4Count += TMO_SLL_Count (gErpsGlobalInfo.pRapsTxSll[u1Index]);
    }

    *pu4Count = u4Count;
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxScanSllsAndTxPdu
 *
 *    DESCRIPTION      : This function scans all the 3 SLLs and transmits
 *                       the RAPS PDUs accordingly.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
ErpsTxScanSllsAndTxPdu (VOID)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tErpsRapsTxInfo    *pTxInfo = NULL;
    UINT1               u1Index = 0;

    /* HITLESS RESTART */
    if (ERPS_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
    {
        return;
    }

    for (; u1Index < ERPS_MAX_RAPS_PDU_TRANSMISSION; u1Index++)
    {
        TMO_SLL_Scan (gErpsGlobalInfo.pRapsTxSll[u1Index], pNode,
                      tTMO_SLL_NODE *)
        {
            pTxInfo =
                (tErpsRapsTxInfo *) (pNode - FSAP_OFFSETOF (tErpsRapsTxInfo,
                                                            SllNode));
            ErpsTxSendPduToCfm (pTxInfo, pTxInfo->pContext);
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxSendPduToCfm
 *
 *    DESCRIPTION      : This function transmits the PDU to the CFM module
 *                       for transmitting out.
 *
 *    INPUT            : pRaps  - Pointer to the RAPS Tx Info structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
ErpsTxSendPduToCfm (tErpsRapsTxInfo * pRaps, VOID *pContext)
{
    tEcfmMepInfoParams  MepInfo;

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    /* Tx R-APS packets on Port 1 */
    if ((pRaps->u1RingTxAction == ERPS_TX_RAPS_ON_BOTH_RING_PORTS)
        || (pRaps->u1RingTxAction == ERPS_TX_RAPS_ON_RING_PORT_1))
    {
        MepInfo.u4ContextId = pRaps->u4ContextId;
        MepInfo.u4MdIndex = pRaps->MepInfo1.u4MdId;
        MepInfo.u4MaIndex = pRaps->MepInfo1.u4MaId;
        MepInfo.u2MepId = (UINT2) pRaps->MepInfo1.u4MepId;
        MepInfo.u4VlanIdIsid = (UINT4) pRaps->MepInfo1.VlanId;
        MepInfo.u4IfIndex = pRaps->MepInfo1.u4IfIndex;
        MepInfo.pAppInfo = pContext;
        MepInfo.u1Version = pRaps->u1Version;

        if (ErpsPortEcfmInitiateExPdu (&MepInfo, ERPS_RAPS_PDU_TLV_OFFSET, 0,
                                       pRaps->pu1RApsPdu,
                                       (ERPS_RAPS_PDU_LEN -
                                        ERPS_RAPS_PDU_PADDING),
                                       pRaps->au1MacAddr,
                                       ERPS_RAPS_PDU_OPCODE) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRaps->u4ContextId, pRaps->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTxSendPduToCfm: RAPS PDU transmission "
                              "Failed for MEG1.\r\n");
        }
    }

    /* Tx R-APS packets on Port 2 */
    if ((pRaps->u1RingTxAction == ERPS_TX_RAPS_ON_BOTH_RING_PORTS)
        || (pRaps->u1RingTxAction == ERPS_TX_RAPS_ON_RING_PORT_2))
    {
        MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

        MepInfo.u4ContextId = pRaps->u4ContextId;
        MepInfo.u4MdIndex = pRaps->MepInfo2.u4MdId;
        MepInfo.u4MaIndex = pRaps->MepInfo2.u4MaId;
        MepInfo.u2MepId = (UINT2) pRaps->MepInfo2.u4MepId;
        MepInfo.u4VlanIdIsid = (UINT4) pRaps->MepInfo2.VlanId;
        MepInfo.u4IfIndex = pRaps->MepInfo2.u4IfIndex;
        MepInfo.pAppInfo = pContext;
        MepInfo.u1Version = pRaps->u1Version;

        if (ErpsPortEcfmInitiateExPdu (&MepInfo, ERPS_RAPS_PDU_TLV_OFFSET, 0,
                                       pRaps->pu1RApsPdu,
                                       (ERPS_RAPS_PDU_LEN -
                                        ERPS_RAPS_PDU_PADDING),
                                       pRaps->au1MacAddr,
                                       ERPS_RAPS_PDU_OPCODE) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRaps->u4ContextId, pRaps->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTxSendPduToCfm: RAPS PDU transmission "
                              "Failed for MEG2.\r\n");
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxInitRApsTxInfoSLL
 *
 *    DESCRIPTION      : This function initializes the SLL maintained for
 *                       RAPS Tx information.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
ErpsTxInitRApsTxInfoSLL (VOID)
{
    UINT1               u1Index = 0;

    ERPS_TX_DB_LOCK ();

    for (; u1Index < ERPS_MAX_RAPS_PDU_TRANSMISSION; u1Index++)
    {
        /* Initialize the SLL */
        TMO_SLL_Init (&(gErpsGlobalInfo.RapsTxInfoList[u1Index]));

        /* Store the address of the SLL to the below  array of pointers
         * variable. This will be used for swapping the SLLs. */
        gErpsGlobalInfo.pRapsTxSll[u1Index] =
            &(gErpsGlobalInfo.RapsTxInfoList[u1Index]);
    }

    ERPS_TX_DB_UNLOCK ();
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxAllocBuffAndFormRapsPdu
 *
 *    DESCRIPTION      : This function initializes the SLL maintained for
 *                       RAPS Tx information.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC INT4
ErpsTxAllocBuffAndFormRapsPdu (tErpsRapsTxInfo * pRaps,
                               tErpsRingInfo * pRingInfo)
{
    UINT1              *pu1Pdu = NULL;
    UINT1              *pu1TempPdu = NULL;
    tMacAddr            MacAddr = { 0x01, 0x19, 0xA7, 0x00, 0x00, 0x01 };

    /* Change the last octet of the dest mac address and put the value
     * of the Ring MAC id */
    MacAddr[MAC_ADDR_LEN - 1] = pRingInfo->u1RingMacId;

    pRaps->MepInfo1.u4MdId = pRingInfo->CfmEntry.u4Port1MEGId;
    pRaps->MepInfo1.u4MaId = pRingInfo->CfmEntry.u4Port1MEId;
    pRaps->MepInfo1.u4MepId = pRingInfo->CfmEntry.u4Port1MEPId;
    pRaps->MepInfo1.VlanId = pRingInfo->RapsVlanId;
    pRaps->MepInfo1.u4IfIndex = pRingInfo->u4Port1IfIndex;

    pRaps->MepInfo2.u4MdId = pRingInfo->CfmEntry.u4Port2MEGId;
    pRaps->MepInfo2.u4MaId = pRingInfo->CfmEntry.u4Port2MEId;
    pRaps->MepInfo2.u4MepId = pRingInfo->CfmEntry.u4Port2MEPId;
    pRaps->MepInfo2.VlanId = pRingInfo->RapsVlanId;
    pRaps->MepInfo2.u4IfIndex = pRingInfo->u4Port2IfIndex;

    pRaps->u4ContextId = pRingInfo->u4ContextId;
    pRaps->u4RingId = pRingInfo->u4RingId;
    pRaps->pContext = pRingInfo->pContext;

    pRaps->u1Version = 0;
    /* Populate the Version field with value 0x01 when Ring COmpatible Version 
     * number is configured as 2 */
    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        pRaps->u1Version = ERPS_VERSION2_VALUE;
    }

    MEMCPY (pRaps->au1MacAddr, MacAddr, sizeof (tMacAddr));

    if ((pu1Pdu = MemAllocMemBlk (gErpsGlobalInfo.RapsTxPduPoolId)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsTxAllocBuffAndFormRapsPdu: PDU Memory alloc "
                         "FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    pu1TempPdu = pu1Pdu;

    ErpsTxFormRapsPdu (pRingInfo, &pu1TempPdu);

    pRaps->pu1RApsPdu = pu1Pdu;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxFormAndSendRAPSPdu
 *
 *    DESCRIPTION      : This function is called by the periodic timer
 *                       expiry handler routine for transmitting RAPS PDU.
 *
 *    INPUT            : pRingInfo - Pointer to the Ring Group structure.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC INT4
ErpsTxFormAndSendRAPSPdu (tErpsRingInfo * pRingInfo)
{
    tErpsRapsTxInfo     Raps;
    UINT2               u2LastMsgSend = 0;
    MEMSET (&Raps, 0, sizeof (tErpsRapsTxInfo));

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsTxFormAndSendRAPSPdu: Ring info is NULL "
                         "!!!\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTxAllocBuffAndFormRapsPdu (&Raps, pRingInfo) != OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTxFormAndSendRAPSPdu: Failed to allocate"
                          " PDU buffer.\r\n");
        return OSIX_FAILURE;
    }

    Raps.u1Version = 0;
    /* Populate the Version field with value 0x01 when Ring COmpatible Version 
     * number is configured as 2 */
    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        Raps.u1Version = ERPS_VERSION2_VALUE;
    }

    switch (pRingInfo->u1RingTxAction)
    {
        case ERPS_TX_RAPS_ON_BOTH_RING_PORTS:
            if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
                && (pRingInfo->u1IsPort2Present ==
                    ERPS_PORT_IN_LOCAL_LINE_CARD))
            {
                Raps.u1RingTxAction = pRingInfo->u1RingTxAction;
            }
            else if (pRingInfo->u1IsPort1Present ==
                     ERPS_PORT_IN_LOCAL_LINE_CARD)
            {
                Raps.u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;
            }
            else
            {
                Raps.u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;
            }
            break;

        case ERPS_TX_RAPS_ON_RING_PORT_1:
            if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
            {
                return OSIX_SUCCESS;
            }
            Raps.u1RingTxAction = pRingInfo->u1RingTxAction;
            break;

        case ERPS_TX_RAPS_ON_RING_PORT_2:
            if (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
            {
                return OSIX_SUCCESS;
            }
            Raps.u1RingTxAction = pRingInfo->u1RingTxAction;
            break;

        default:
            return OSIX_SUCCESS;
    }

    ErpsTxSendPduToCfm (&Raps, pRingInfo->pContext);

    MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                        (UINT1 *) Raps.pu1RApsPdu);

    /* Check is added to not increment the TX send count
     * for the interconnected node which is receiving
     * the UP-MEP packets via virtual channel and sending the same
     * packet to other nodes in sub-Ring*/
    if (pRingInfo->u1IsMsgRcvd == OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    u2LastMsgSend = pRingInfo->u2LastMsgSend >> 12;

    /* updating the RAPS FS messages transmitted count on both the ports */
    if (u2LastMsgSend == ERPS_MESSAGE_TYPE_FS)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4FSPduTxCount++;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4FSPduTxCount++;
        }
    }
    /* updating the RAPS MS messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_MS)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4MSPduTxCount++;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4MSPduTxCount++;
        }
    }
    /* updating the RAPS SF messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_SF)
    {
        if (pRingInfo->u1RingTxAction == ERPS_TX_RAPS_ON_BOTH_RING_PORTS)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4SFPduTxCount++;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4SFPduTxCount++;
            }
        }
        else if (pRingInfo->u1RingTxAction == ERPS_TX_RAPS_ON_RING_PORT_1)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4SFPduTxCount++;
            }
        }
        else
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4SFPduTxCount++;
            }
        }
    }
    /* updating the RAPS NR/NRRB messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_NR)
    {
        /* IF RB bit is set then NRRB Message else NR Message */
        if ((pRingInfo->u2LastMsgSend >> 7) & 0x1)
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4NRRBPduTxCount++;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4NRRBPduTxCount++;
            }
        }
        else
        {
            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
            {
                pRingInfo->Port1Stats.u4NRPduTxCount++;
            }

            if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
            {
                pRingInfo->Port2Stats.u4NRPduTxCount++;
            }

        }
    }
    /* updating the RAPS Event messages transmitted count on both the ports */
    else if (u2LastMsgSend == ERPS_MESSAGE_TYPE_EVENT)
    {
        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
        {
            pRingInfo->Port1Stats.u4EventPduTxCount++;
        }

        if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
        {
            pRingInfo->Port2Stats.u4EventPduTxCount++;
        }
    }

    /* Updation of RingStatistics */
    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        pRingInfo->Port1Stats.u4RapsSentCount++;
    }

    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present)
    {
        pRingInfo->Port2Stats.u4RapsSentCount++;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ErpsTxDelEntriesInSll
 *
 *    DESCRIPTION      : This function deletes all the RAPS Tx Info entries
 *                       present in the specified SLL.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
ErpsTxDelEntriesInSll (tTMO_SLL * pTxSll)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tErpsRapsTxInfo    *pTxInfo = NULL;

    TMO_DYN_SLL_Scan (pTxSll, pNode, pTempNode, tTMO_SLL_NODE *)
    {
        pTxInfo =
            (tErpsRapsTxInfo *) (pNode - FSAP_OFFSETOF (tErpsRapsTxInfo,
                                                        SllNode));
        TMO_SLL_Delete (pTxSll, pNode);

        MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                            pTxInfo->pu1RApsPdu);
        MemReleaseMemBlock (gErpsGlobalInfo.RapsTxInfoTablePoolId,
                            (UINT1 *) pTxInfo);
    }
}

#endif
