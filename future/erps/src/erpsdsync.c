/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsdsync.c,v 1.5 2013/12/07 11:07:27 siva Exp $
 *
 * Description: This file contains the functions related to Distributed-ERPS 
 *              feature. 
 *              This includes the functions used to form D-ERPS PDU and
 *              function used to process the received D-ERPS sync PDU.
 *                                                        
 *****************************************************************************/

#include "erpsinc.h"

PRIVATE VOID        ErpsDsyncFormDERPSPdu (tErpsRingInfo * pRingInfo,
                                           tErpsDsyncInfo * pDsyncInfo,
                                           UINT1 **ppu1ApsPdu);

/********************************************************************************
 *                                                                              *
 *    FUNCTION NAME    : ErpsDsyncFormDERPSPdu                                  *
 *                                                                              *
 *    DESCRIPTION      : In Distributed ERPS, dynamic info changes in Local ERP *
 *                       is synced with remote ERP using D-ERPS PDU (explained  *
 *                       in erpsdd.doc Appendix-D, section 7.2.1).              *
 *                       This function forms the D-ERPS PDU with all dynamic    *
 *                       information present in Dsync structure                 *
 *                       ( tErpsDsyncInfo ).                                    *
 *                                                                              *
 *    Input (s)        : pRingInfo - Pointer to ring node for which a D-ERPS    *
 *                                       sync messages needs to be transmitted  *
 *                       pDsyncInfo - Pointer to DsyncInfo structure            *
 *                                       which contains the dynamic info need   *
 *                                       to be synced.                          *
 *    OUTPUT           : None                                                   *
 *                                                                              *
 *    RETURNS          : None                                                   * 
 *                                                                              *
 ********************************************************************************/
PRIVATE VOID
ErpsDsyncFormDERPSPdu (tErpsRingInfo * pRingInfo, tErpsDsyncInfo * pDsyncInfo,
                       UINT1 **ppu1ApsPdu)
{
    UINT1               u1RequestAndSubCode = 0;
    UINT1               u1Status = 0;
    UINT1               u1PortStateAndBPR = 0;
    UINT1               u1SemAndHPRValue = 0;
    UINT1               u1Length = ERPS_DERPS_DATA_LENGTH;

    u1RequestAndSubCode |=
        ((pRingInfo->u2LastMsgSend & 0xFF00) >> ERPS_SHIFT_8BITS);
    u1Status |= (pRingInfo->u2LastMsgSend & 0x00FF);

    u1Status |= ERPS_DERPS_BIT_SET;

    ERPS_PUT_1BYTE (*ppu1ApsPdu, u1RequestAndSubCode);
    ERPS_PUT_1BYTE (*ppu1ApsPdu, u1Status);
    /* Filling up NodeId field */
    MEMCPY (*ppu1ApsPdu, ERPS_NODE_ID (pRingInfo->u4ContextId),
            sizeof (tMacAddr));
    *ppu1ApsPdu += sizeof (tMacAddr);

    /* Filling up D-ERPS sync Info */

    ERPS_PUT_1BYTE (*ppu1ApsPdu, u1Length);    /* Filling Length of DERPS info. */

    /* Filling Bitmask value which identifies the type of dynamic info changes in Local ERP. */
    ERPS_PUT_2BYTE (*ppu1ApsPdu, pDsyncInfo->u2BitMask);

    /* Filling ContextID configured for the Ring */
    ERPS_PUT_4BYTE (*ppu1ApsPdu, pDsyncInfo->u4ContextId);

    /* Filling RingID */
    ERPS_PUT_4BYTE (*ppu1ApsPdu, pDsyncInfo->u4RingId);

    /* Filling TriggerFlush bit to indicate FDB Flush. */
    u1PortStateAndBPR =
        ((pDsyncInfo->
          u1TriggerFlush << ERPS_REMOTE_ERP_TRIGGER_FLUSH_OFFSET) &
         ERPS_REMOTE_ERP_TRIGGER_FDB_FLUSH);
    /* Filling TxRequest bit to indicate PDU transmission. */
    u1PortStateAndBPR |=
        ((pDsyncInfo->
          u1RapsTxRequest << ERPS_REMOTE_ERP_RAPS_TX_OFFSET) &
         ERPS_REMOTE_ERP_RAPS_TX);
    /* Filling Port State of Local Ring port */
    u1PortStateAndBPR |=
        ((pDsyncInfo->
          u1LocalPortStateUpdateNotify << ERPS_LOCAL_PORT_STATE_UPDATE_OFFSET) &
         ERPS_LOCAL_PORT_STATE_UPDATE_NOTIFY);
    /* Filling Port State of Remote Ring port */
    u1PortStateAndBPR |=
        ((pDsyncInfo->
          u1RemotePortHWStateChangeInd << ERPS_REMOTE_PORT_STATE_CHANGE_OFFSET)
         & ERPS_REMOTE_PORT_STATE_CHANGE_IND);
    /* Filling BPR bit */
    u1PortStateAndBPR |=
        (pDsyncInfo->u1BPRStatus & ERPS_LOCAL_PORT_LAST_RCVD_BPR_BIT);

    ERPS_PUT_1BYTE (*ppu1ApsPdu, u1PortStateAndBPR);

    /* Filling HighPriority Request of Local ERP */
    u1SemAndHPRValue =
        ((pDsyncInfo->
          u1HighestPriorityRequest << ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ_OFFSET) &
         ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ);
    /* Filling SEM State of Local ERP */
    u1SemAndHPRValue |= (pDsyncInfo->u1RingState & ERPS_REMOTE_ERP_RING_STATE);

    ERPS_PUT_1BYTE (*ppu1ApsPdu, u1SemAndHPRValue);

    /* Filling Timer Event of Local ERP */
    ERPS_PUT_1BYTE (*ppu1ApsPdu, (pDsyncInfo->u1TimerInfo));

    /* Filling LastMsgSend type of Local ERP */
    ERPS_PUT_2BYTE (*ppu1ApsPdu, pDsyncInfo->u2LastMsgSend);

    /* Filling Ring NodeStatus of Local ERP */
    ERPS_PUT_2BYTE (*ppu1ApsPdu, (pDsyncInfo->u2RingNodeStatus));

    /* Filling NodeId present in Last received ERPS PDU on Local Ring Port */
    ERPS_PUT_MAC_ADDR (*ppu1ApsPdu, (pDsyncInfo->LocalPortLastRcvdNodeId));

    return;
}

/********************************************************************************
 *                                                                              *
 *    FUNCTION NAME    : ErpsDsyncFormAndSendDERPSPdu                           *
 *                                                                              *
 *    DESCRIPTION      : This function will form and send D-ERPS sync PDU       *
 *                       to remote ERP.                                         *
 *                       D-ERPS sync PDU will be send only on Remote Ring Port. *
 *                                                                              *
 *    Input (s)        : pRingInfo - Pointer to ring node for which a D-ERPS    *
 *                                       sync messages needs to be transmitted  *
 *                       pDsyncInfo - Pointer to DsyncInfo structure            *
 *                                       which contains the dynamic info need   *
 *                                       to be synced.                          *
 *    OUTPUT           : None                                                   *
 *                                                                              *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              *
 *                                                                              *
 ********************************************************************************/
PUBLIC INT4
ErpsDsyncFormAndSendDERPSPdu (tErpsRingInfo * pRingInfo,
                              tErpsDsyncInfo * pDsyncInfo)
{
    tErpsRapsTxInfo     Raps;
    UINT1              *pu1TempPdu = NULL;

    MEMSET (&Raps, 0, sizeof (tErpsRapsTxInfo));

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsDsyncFormAndSendDERPSPdu: Ring info is NULL "
                         "!!!\r\n");
        return OSIX_FAILURE;
    }

    /* If Both the Ring ports are Local then return from the function.
     * No need to send DERPS PDU. */
    if ((ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present) &&
        (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present))
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s: Both the ring ports are configured as Local,"
                          " So Not Sending D-ERPS PDU.\r\n", __FUNCTION__);
        return OSIX_SUCCESS;
    }

    /* Allocate Mem Buffer for D-ERPS PDU. */
    if (ErpsTxAllocBuffAndFormRapsPdu (&Raps, pRingInfo) != OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsDsyncFormAndSendDERPSPdu: Failed to allocate"
                          " PDU buffer.\r\n");
        return OSIX_FAILURE;
    }

    pu1TempPdu = Raps.pu1RApsPdu;
    /* To fill DERPS dynamic information in D-ERPS PDU. */
    ErpsDsyncFormDERPSPdu (pRingInfo, pDsyncInfo, &pu1TempPdu);

    Raps.u1Version = 0;

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        Raps.u1Version = ERPS_VERSION2_VALUE;
    }

    /* D-ERPS PDU should be send on Remote Port. So if port1 is remote
     * then set txAction as Port1, else set TxAction as port2. */
    if (ERPS_PORT_IN_REMOTE_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        Raps.u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;
    }
    else
    {
        Raps.u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;
    }

    /* Increment the Dynamic Sync Pdu Transmitted Count */
    pRingInfo->u4DistributingPortTxCount++;

    /* Send the PDU to CFM for transmission. */
    ErpsTxSendPduToCfm (&Raps, pRingInfo->pContext);

    MemReleaseMemBlock (gErpsGlobalInfo.RapsTxPduPoolId,
                        (UINT1 *) Raps.pu1RApsPdu);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsDsyncUpdateDERPSRingDynInfo                  */
/*                                                                           */
/*    Description         : This function will be used to update DsyncInfo   */
/*                          for the given ring node. And will trigger the    */
/*                          D-ERPS sync with the changed info.               */
/*                                                                           */
/*    Input(s)            : pRingInfo     - Pointer to ring node.            */
/*                          u1DynInfoType - Type of the Dynamic info change  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsDsyncUpdateDERPSRingDynInfo (tErpsRingInfo * pRingInfo, UINT1 u1DynInfoType)
{
    tErpsDsyncInfo      DsyncInfo;

    if (pRingInfo == NULL)
    {
        return;
    }

    /* If both the Ring ports are Local, ERP is not distributed, so no
     * need to sync dynamic informations. */
    if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
        && (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_LOCAL_LINE_CARD))
    {
        return;
    }
    /* If u1DERPSSyncReqFlag is not set to OSIX_TRUE, the no need to sync the 
     * dynamic information to Remote ERP */
    if (pRingInfo->u1DERPSSyncReqFlag != OSIX_TRUE)
    {
        return;
    }
    MEMSET (&DsyncInfo, 0, sizeof (tErpsDsyncInfo));

    DsyncInfo.u4RingId = pRingInfo->u4RingId;
    DsyncInfo.u4ContextId = pRingInfo->u4ContextId;

    switch (u1DynInfoType)
    {
        case ERPS_RING_LAST_MSG_TYPE:
            DsyncInfo.u2BitMask |= ERPS_DSYNC_LAST_MSG_SEND;
            DsyncInfo.u2LastMsgSend = pRingInfo->u2LastMsgSend;
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Last Message "
                              "send : < %u >.\r\n", __FUNCTION__,
                              OSIX_HTONS (DsyncInfo.u2LastMsgSend));

            if (pRingInfo->u1RingPduSend == ERPS_RING_PDU_DONT_SEND)
            {
                DsyncInfo.u1RapsTxRequest = OSIX_FALSE;
            }
            else
            {
                DsyncInfo.u1RapsTxRequest = OSIX_TRUE;
            }
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send R-APS Tx"
                              " Request val : < %u >.\r\n", __FUNCTION__,
                              DsyncInfo.u1RapsTxRequest);
            DsyncInfo.u2BitMask |= ERPS_DSYNC_RAPS_TX_REQUEST;
            break;

        case ERPS_RING_PRIO_REQUEST:
            DsyncInfo.u1HighestPriorityRequest =
                pRingInfo->u1HighestPriorityRequest;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_HIGHEST_PRIO_REQ;
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Highest Priority"
                              " Request val : < %u >.\r\n", __FUNCTION__,
                              DsyncInfo.u1HighestPriorityRequest);
            break;

        case ERPS_RING_PORT1_STATUS:
            if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
            {
                DsyncInfo.u1LocalPortStateUpdateNotify =
                    pRingInfo->u1Port1Status;
                DsyncInfo.u2BitMask |= ERPS_DSYNC_LOCAL_PORT_STATE;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Send Local Port "
                                  ", port 1 status change val : < %u >.\r\n",
                                  __FUNCTION__, pRingInfo->u1Port1Status);
            }
            else
            {
                DsyncInfo.u1RemotePortHWStateChangeInd =
                    pRingInfo->u1Port1Status;
                DsyncInfo.u2BitMask |= ERPS_DSYNC_REMOTE_PORT_STATE;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Send port 1 H/w port"
                                  " status change val : < %u >.\r\n",
                                  __FUNCTION__, pRingInfo->u1Port1Status);
            }
            break;

        case ERPS_RING_PORT2_STATUS:
            if (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
            {
                DsyncInfo.u1LocalPortStateUpdateNotify =
                    pRingInfo->u1Port2Status;
                DsyncInfo.u2BitMask |= ERPS_DSYNC_LOCAL_PORT_STATE;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Send Local Port,"
                                  " port 2 status change val : < %u >.\r\n",
                                  __FUNCTION__, pRingInfo->u1Port2Status);
            }
            else
            {
                DsyncInfo.u1RemotePortHWStateChangeInd =
                    pRingInfo->u1Port2Status;
                DsyncInfo.u2BitMask |= ERPS_DSYNC_REMOTE_PORT_STATE;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Send port 2 H/w port"
                                  " status change val : < %u >.\r\n",
                                  __FUNCTION__, pRingInfo->u1Port2Status);
            }
            break;

        case ERPS_RING_STATE:
            DsyncInfo.u1RingState = pRingInfo->u1RingState;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_RING_STATE;
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Ring State "
                              "change val : < %u >.\r\n", __FUNCTION__,
                              DsyncInfo.u1RingState);
            break;

        case ERPS_RING_NODE_STATUS:
            DsyncInfo.u2RingNodeStatus = (UINT2) pRingInfo->u4RingNodeStatus;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_RING_NODE_STATUS;
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Ring node status "
                              "change val : < %u >.\r\n", __FUNCTION__,
                              DsyncInfo.u2RingNodeStatus);
            break;

        case ERPS_RING_PORT1_RCVD_NODE_ID:
            MEMCPY (DsyncInfo.LocalPortLastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                    sizeof (tMacAddr));
            DsyncInfo.u1BPRStatus = gErpsRcvdMsg.u1BPRBitStatus;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_NODE_ID_BPR_PAIR;
            break;

        case ERPS_RING_PORT2_RCVD_NODE_ID:
            MEMCPY (DsyncInfo.LocalPortLastRcvdNodeId, gErpsRcvdMsg.RcvdNodeId,
                    sizeof (tMacAddr));
            DsyncInfo.u1BPRStatus = gErpsRcvdMsg.u1BPRBitStatus;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_NODE_ID_BPR_PAIR;
            break;

        case ERPS_RING_START_WTB_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Start WTB timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_WTB;
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_STOP_WTB_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Stop WTB timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_WTB;
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_WTB;
            DsyncInfo.u1TimerInfo &= ~ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_START_WTR_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Start WTR timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_WTR;
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_STOP_WTR_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Stop WTR timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_WTR;
            DsyncInfo.u1TimerInfo &= ~ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_START_GUARD_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Start Guard timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_GUARD;
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_STOP_GUARD_TMR:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Stop Guard timer "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TimerInfo |= ERPS_DSYNC_TIMER_INFO_GUARD;
            DsyncInfo.u1TimerInfo &= ~ERPS_DSYNC_TIMER_INFO_START;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TIMER_INFO;
            break;

        case ERPS_RING_FLUSH_FDB:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "%s : Send Flush FDB "
                              "Request.\r\n", __FUNCTION__);
            DsyncInfo.u1TriggerFlush = OSIX_TRUE;
            DsyncInfo.u2BitMask |= ERPS_DSYNC_TRIGGER_FLUSH;
            break;

        default:
            return;
            break;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "%s : Send Bit Mask Val : < %u >.\r\n",
                      __FUNCTION__, OSIX_HTONS (DsyncInfo.u2BitMask));

    ErpsDsyncFormAndSendDERPSPdu (pRingInfo, &DsyncInfo);

    return;
}

/********************************************************************************
 *                                                                              *
 *    FUNCTION NAME    : ErpsDsyncDynSyncMsgHandler                             *
 *                                                                              *
 *    DESCRIPTION      : This function will process D-ERPS sync PDU and update  *
 *                       RingInfo structure.                                    *
 *                                                                              *
 *    Input (s)        : pRingInfo - Pointer to ring node for which a D-ERPS    *
 *                                       PDU is received.                       *
 *                       pQMsg     - Pointer to received D-ERPS message info.   *
 *                                                                              *
 *    OUTPUT           : None                                                   *
 *                                                                              *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              *
 *                                                                              *
 ********************************************************************************/
PUBLIC VOID
ErpsDsyncDynSyncMsgHandler (tErpsRingInfo * pRingInfo, tErpsQMsg * pQMsg)
{
    tErpsDsyncInfo      DsyncInfo;
    UINT2               u2Mask = 0;
    UINT1               au1RapsSyncData[ERPS_RAPS_DSYNC_INFO_LEN] = { 0 };
    UINT1               u1DsyncInfoLenth = 0;
    UINT1               u1Offset = 0;
    UINT1               u1FirstRingStatusByte = 0;
    UINT1               u1SecRingStatusByte = 0;
    UINT1               u1PortState;
    UINT1               u1RapsMessageType = 0;
    UINT1               u1StatusBits = 0;

    MEMSET (&DsyncInfo, 0, sizeof (DsyncInfo));

    /* Extract D-Sync Info Length field */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu,
                               &(u1DsyncInfoLenth),
                               (pQMsg->unMsgParam.RapsPduMsg.u1TlvOffset +
                                ERPS_RAPS_DSYNC_INFO_OFFSET),
                               sizeof (u1DsyncInfoLenth));

    /* Copy u1DsyncInfoLenth size data from Buffer */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu,
                               au1RapsSyncData,
                               (pQMsg->unMsgParam.RapsPduMsg.u1TlvOffset +
                                ERPS_RAPS_DSYNC_INFO_OFFSET +
                                sizeof (u1DsyncInfoLenth)), u1DsyncInfoLenth);

    /* All R-APS PDU's going out of Remote or Local ERP should have 
     * same Node ID, since Remote and Local ERP together represent
     * a Node.
     * So To achieve this Node ID comparison will be done and highest
     * node ID among the two will be made as the common node ID and
     * will be used in all the messages sent out.
     */
    if ((MEMCMP (gErpsRcvdMsg.RcvdNodeId, ERPS_NODE_ID (pRingInfo->u4ContextId),
                 sizeof (gErpsRcvdMsg.RcvdNodeId))) > 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "%s : Highest Node ID : %02d:%02d:%02d:%02d:%02d:%02d "
                          "will be used in all PDU's Tx out of both Remote and "
                          "Local ERP after Node ID comparison between distributed "
                          "nodes.\r\n", __FUNCTION__,
                          gErpsRcvdMsg.RcvdNodeId[0],
                          gErpsRcvdMsg.RcvdNodeId[1],
                          gErpsRcvdMsg.RcvdNodeId[2],
                          gErpsRcvdMsg.RcvdNodeId[3],
                          gErpsRcvdMsg.RcvdNodeId[4],
                          gErpsRcvdMsg.RcvdNodeId[5]);

        MEMCPY (ERPS_NODE_ID (pRingInfo->u4ContextId),
                gErpsRcvdMsg.RcvdNodeId, sizeof (tMacAddr));
    }

    /* Bit Mask Field */
    MEMCPY (&(DsyncInfo.u2BitMask), &au1RapsSyncData[u1Offset],
            sizeof (DsyncInfo.u2BitMask));
    DsyncInfo.u2BitMask = OSIX_NTOHS (DsyncInfo.u2BitMask);
    if (DsyncInfo.u2BitMask == 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsDsyncDynSyncMsgHandler: Invalid Bitmask "
                          "Received in D-ERPS PDU, returning.\r\n");
        return;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "%s : Received Bit Mask"
                      ": < %u >.\r\n", __FUNCTION__, DsyncInfo.u2BitMask);

    /* Increment the Dynamic Sync Pdu Received Count */
    pRingInfo->u4DistributingPortRxCount++;

    u1Offset += sizeof (DsyncInfo.u2BitMask);

    MEMCPY (&(DsyncInfo.u4ContextId), &au1RapsSyncData[u1Offset],
            sizeof (DsyncInfo.u4ContextId));
    DsyncInfo.u4ContextId = OSIX_NTOHL (DsyncInfo.u4ContextId);
    u1Offset += sizeof (DsyncInfo.u4ContextId);

    MEMCPY (&(DsyncInfo.u4RingId), &au1RapsSyncData[u1Offset],
            sizeof (DsyncInfo.u4RingId));
    DsyncInfo.u4RingId = OSIX_NTOHL (DsyncInfo.u4RingId);
    u1Offset += sizeof (DsyncInfo.u4RingId);

    u1FirstRingStatusByte = au1RapsSyncData[u1Offset++];
    u1SecRingStatusByte = au1RapsSyncData[u1Offset++];
    DsyncInfo.u1TimerInfo = au1RapsSyncData[u1Offset++];

    MEMCPY (&(DsyncInfo.u2LastMsgSend), &au1RapsSyncData[u1Offset],
            sizeof (DsyncInfo.u2LastMsgSend));
    DsyncInfo.u2LastMsgSend = OSIX_NTOHS (DsyncInfo.u2LastMsgSend);
    u1Offset += sizeof (DsyncInfo.u2LastMsgSend);

    MEMCPY (&(DsyncInfo.u2RingNodeStatus), &au1RapsSyncData[u1Offset],
            sizeof (DsyncInfo.u2RingNodeStatus));
    DsyncInfo.u2RingNodeStatus = OSIX_NTOHS (DsyncInfo.u2RingNodeStatus);
    u1Offset += sizeof (DsyncInfo.u2RingNodeStatus);

    MEMCPY (DsyncInfo.LocalPortLastRcvdNodeId, &au1RapsSyncData[u1Offset],
            sizeof (tMacAddr));

    /* u1DERPSSyncReqFlag in pRingInfo is set to OSIX_FALSE for avoiding 
     * sync looping between Local and Remote ERPs. By doing this, while syncing
     * dynamic information, D-ERPS sync PDU will not be send. 
     * i.e for example say when WTR timer is started, Linecard-1 will send D-ERPS PDU.
     * While processing this D-ERPS PDU in Linecard-2, ErpsDsyncDynSyncMsgHandler
     * will call ErpsTmrStartTimer with timertype as WTR. Here in this function again 
     * D-ERPS PDU will be sent out for WTR timer event from Linecard-2, and 
     * again the same thing will happen in Linecard-1. Hence both the linecards will
     * keep on sending the same D-ERPS PDU info to each other.
     * To avoid this the flag is set to OSIX_FALSE during the process of syncing 
     * dynamic information. */
    pRingInfo->u1DERPSSyncReqFlag = OSIX_FALSE;

    for (u2Mask = 1; u2Mask != ERPS_DSYNC_LAST_MSG; u2Mask <<= 1)
    {
        switch (DsyncInfo.u2BitMask & u2Mask)
        {
            case ERPS_DSYNC_LOCAL_PORT_STATE:
                DsyncInfo.u1LocalPortStateUpdateNotify =
                    (u1FirstRingStatusByte &
                     ERPS_LOCAL_PORT_STATE_UPDATE_NOTIFY);
                DsyncInfo.
                    u1LocalPortStateUpdateNotify >>=
                    ERPS_LOCAL_PORT_STATE_UPDATE_OFFSET;

                /* Local Port Update notification is sent by Local ERP to Remote ERP.
                 * if port 1 is remote port then, update the ring port status in pRingInfo
                 * for the same port, Calling in npapi to update port state in Remote ERP
                 * is not required. */
                if (pRingInfo->u1IsPort1Present ==
                    ERPS_PORT_IN_REMOTE_LINE_CARD)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : modifying Local "
                                      "port state for port 1 to < %u >.\r\n",
                                      __FUNCTION__,
                                      DsyncInfo.u1LocalPortStateUpdateNotify);
                    pRingInfo->u1Port1Status =
                        DsyncInfo.u1LocalPortStateUpdateNotify;
                }
                else
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : modifying Local "
                                      "port state for port 2 to < %u >.\r\n",
                                      __FUNCTION__,
                                      DsyncInfo.u1LocalPortStateUpdateNotify);
                    pRingInfo->u1Port2Status =
                        DsyncInfo.u1LocalPortStateUpdateNotify;
                }
                break;

            case ERPS_DSYNC_REMOTE_PORT_STATE:
                /* This Field is used to instruct the Remote ERP to change its local port
                 * port state, so npapi should be called to update the H/w Port status */
                DsyncInfo.u1RemotePortHWStateChangeInd =
                    (u1FirstRingStatusByte & ERPS_REMOTE_PORT_STATE_CHANGE_IND);
                DsyncInfo.u1RemotePortHWStateChangeInd >>=
                    ERPS_REMOTE_PORT_STATE_CHANGE_OFFSET;
                if (DsyncInfo.u1RemotePortHWStateChangeInd & ERPS_PORT_BLOCKING)
                {
                    u1PortState = ERPS_PORT_BLOCKING;
                }
                else
                {
                    u1PortState = ERPS_PORT_UNBLOCKING;
                }
                /* Calling npapi to update Local Ring port state in HW */
                if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : modifying H/w "
                                      "port state of local port: port 1 to "
                                      "< %u >.\r\n", __FUNCTION__, u1PortState);
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                          u1PortState);
                }
                else
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : modifying H/w "
                                      "port state of local port: port 2 to "
                                      "< %u >.\r\n", __FUNCTION__, u1PortState);
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                          u1PortState);
                }
                break;

            case ERPS_DSYNC_RING_STATE:
                /* Update the Ring State in Remote ERP */
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Updating Ring State "
                                  "to < %u >.\r\n", __FUNCTION__,
                                  DsyncInfo.u1RingState);
                DsyncInfo.u1RingState =
                    (UINT1) (u1SecRingStatusByte & ERPS_REMOTE_ERP_RING_STATE);
                pRingInfo->u1RingState = DsyncInfo.u1RingState;
                break;

            case ERPS_DSYNC_HIGHEST_PRIO_REQ:
                /* Update the Highest priority Request in Remote ERP */
                DsyncInfo.u1HighestPriorityRequest =
                    (UINT1) (u1SecRingStatusByte &
                             ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ);

                DsyncInfo.u1HighestPriorityRequest >>=
                    ERPS_REMOTE_ERP_HIGHEST_PRIO_REQ_OFFSET;

                pRingInfo->u1HighestPriorityRequest =
                    DsyncInfo.u1HighestPriorityRequest;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Updating Highest "
                                  "priority request to < %u >.\r\n",
                                  __FUNCTION__,
                                  DsyncInfo.u1HighestPriorityRequest);
                break;

            case ERPS_DSYNC_TRIGGER_FLUSH:
                /* Trigger Flush if this bit is set */
                DsyncInfo.u1TriggerFlush =
                    (UINT1) (u1FirstRingStatusByte &
                             ERPS_REMOTE_ERP_TRIGGER_FDB_FLUSH);
                DsyncInfo.
                    u1TriggerFlush >>= ERPS_REMOTE_ERP_TRIGGER_FLUSH_OFFSET;

                if (DsyncInfo.u1TriggerFlush)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : Calling"
                                      " ErpsUtilFlushFdbTable() .\r\n",
                                      __FUNCTION__);
                    ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
                }
                break;

            case ERPS_DSYNC_TIMER_INFO:
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Received Dsync Timer"
                                  " info : < %u >.\r\n", __FUNCTION__,
                                  DsyncInfo.u1TimerInfo);
                /* If First bit in Timer Info (u1TimerInfo) field is set then start 
                 * the timer whose corresponding bit is set in the TimerInfo field. */
                if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_START)
                {
                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_WTR)
                    {
                        ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                                           pRingInfo->u4WTRTimerValue);
                    }

                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_WTB)
                    {
                        ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR,
                                           pRingInfo->u4WTBTimerValue);
                    }

                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_GUARD)
                    {
                        ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR,
                                           pRingInfo->u4GuardTimerValue);
                    }
                }
                /* Stop the Timer whose corresponding bit is set in the TimerInfo field. */
                else
                {
                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_WTR)
                    {
                        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
                    }
                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_WTB)
                    {
                        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
                    }
                    if (DsyncInfo.u1TimerInfo & ERPS_DSYNC_TIMER_INFO_GUARD)
                    {
                        ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR);
                    }
                }
                break;

            case ERPS_DSYNC_LAST_MSG_SEND:
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Received Last Msg "
                                  "send : < %u >.\r\n", __FUNCTION__,
                                  DsyncInfo.u2LastMsgSend);
                /* Update the Last Message send in Remote ERP */
                pRingInfo->u2LastMsgSend = DsyncInfo.u2LastMsgSend;
                break;

            case ERPS_DSYNC_RING_NODE_STATUS:
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "%s : Received ring node "
                                  "status : < %u >.\r\n", __FUNCTION__,
                                  DsyncInfo.u2RingNodeStatus);
                /* Updatation of NodeStatus from D-ERPS PDU should happen only for Remote
                 * port status (like LocalSF and RemoteSF). So if the Ring port1 is remote
                 * update the port status in RingNodeStatus. */
                if (pRingInfo->u1IsPort1Present ==
                    ERPS_PORT_IN_REMOTE_LINE_CARD)
                {
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2)
                         ERPS_NODE_STATUS_PORT1_LOCAL_SF) ? (pRingInfo->
                                                             u4RingNodeStatus |
                                                             ERPS_NODE_STATUS_PORT1_LOCAL_SF)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_NODE_STATUS_PORT1_LOCAL_SF);
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2)
                         ERPS_NODE_STATUS_PORT1_RAPS_SF) ? (pRingInfo->
                                                            u4RingNodeStatus |
                                                            ERPS_NODE_STATUS_PORT1_RAPS_SF)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_NODE_STATUS_PORT1_RAPS_SF);
                }
                /* Update Ring port2 status in RingNodeStatus if port 2 is remote. */
                else
                {
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2)
                         ERPS_NODE_STATUS_PORT2_LOCAL_SF) ? (pRingInfo->
                                                             u4RingNodeStatus |
                                                             ERPS_NODE_STATUS_PORT2_LOCAL_SF)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_NODE_STATUS_PORT2_LOCAL_SF);
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2)
                         ERPS_NODE_STATUS_PORT2_RAPS_SF) ? (pRingInfo->
                                                            u4RingNodeStatus |
                                                            ERPS_NODE_STATUS_PORT2_RAPS_SF)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_NODE_STATUS_PORT2_RAPS_SF);
                }
                /* RB, Running WTR and WTB be done by the ERP where RPL is present. So if RPL
                 * is present in Local ERP, ignore those status bits in RingNodeStatus.
                 * If RPL is present in Remote ERP (i.e RPL is remote port) then, update 
                 * ERPS_NODE_STATUS_RB, ERPS_WTR_TIMER_RUNNING and ERPS_WTB_TIMER_RUNNING
                 * in RingNodeStatus from Dsync info */
                if ((pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex) ?
                    (pRingInfo->u1IsPort1Present ==
                     ERPS_PORT_IN_REMOTE_LINE_CARD) : (pRingInfo->
                                                       u1IsPort2Present ==
                                                       ERPS_PORT_IN_REMOTE_LINE_CARD))
                {
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2) ERPS_NODE_STATUS_RB)
                        ? (pRingInfo->
                           u4RingNodeStatus | ERPS_NODE_STATUS_RB)
                        : (pRingInfo->u4RingNodeStatus & ~ERPS_NODE_STATUS_RB);
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2) ERPS_WTR_TIMER_RUNNING)
                        ? (pRingInfo->
                           u4RingNodeStatus | ERPS_WTR_TIMER_RUNNING)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_WTR_TIMER_RUNNING);
                    pRingInfo->u4RingNodeStatus =
                        (DsyncInfo.
                         u2RingNodeStatus & (UINT2) ERPS_WTB_TIMER_RUNNING)
                        ? (pRingInfo->
                           u4RingNodeStatus | ERPS_WTB_TIMER_RUNNING)
                        : (pRingInfo->
                           u4RingNodeStatus & ~ERPS_WTB_TIMER_RUNNING);
                }
                /* Both the ERP will run Hold-Off timer, so Sync Hold-Off timer status in RingNodeStatus */
                pRingInfo->u4RingNodeStatus =
                    (DsyncInfo.
                     u2RingNodeStatus & (UINT2) ERPS_HOLDOFF_TIMER_RUNNING)
                    ? (pRingInfo->
                       u4RingNodeStatus | ERPS_HOLDOFF_TIMER_RUNNING)
                    : (pRingInfo->
                       u4RingNodeStatus & ~ERPS_HOLDOFF_TIMER_RUNNING);
                break;

            case ERPS_DSYNC_NODE_ID_BPR_PAIR:
                DsyncInfo.u1BPRStatus =
                    (UINT1) (u1FirstRingStatusByte &
                             ERPS_LOCAL_PORT_LAST_RCVD_BPR_BIT);

                /* when (Node ID, BPR) pair is changed in Remote ERP for its Local
                 * Ring port then in the Remote Line card changed remote port
                 * (Node ID, BPR) pair value should be synched. */
                if (pRingInfo->u1IsPort1Present ==
                    ERPS_PORT_IN_REMOTE_LINE_CARD)
                {
                    MEMCPY (pRingInfo->Port1StoredNodeIdBPRPair.NodeId,
                            DsyncInfo.LocalPortLastRcvdNodeId,
                            sizeof (tMacAddr));
                    pRingInfo->Port1StoredNodeIdBPRPair.u1BPRStatus =
                        DsyncInfo.u1BPRStatus;

                }
                else
                {
                    MEMCPY (pRingInfo->Port2StoredNodeIdBPRPair.NodeId,
                            DsyncInfo.LocalPortLastRcvdNodeId,
                            sizeof (tMacAddr));
                    pRingInfo->Port2StoredNodeIdBPRPair.u1BPRStatus =
                        DsyncInfo.u1BPRStatus;
                }
                break;

            case ERPS_DSYNC_RAPS_TX_REQUEST:
                DsyncInfo.u1RapsTxRequest =
                    (UINT1) (u1FirstRingStatusByte & ERPS_REMOTE_ERP_RAPS_TX);
                DsyncInfo.u1RapsTxRequest >>= ERPS_REMOTE_ERP_RAPS_TX_OFFSET;

                /* If u1RapsTxRequest is OSIX_TRUE, then R-APS PDU transmission should be started
                 * else Stop R-APS PDU transmission. */
                if (DsyncInfo.u1RapsTxRequest == OSIX_TRUE)
                {
                    u1RapsMessageType = (UINT1) (DsyncInfo.u2LastMsgSend >> 12);
                    u1StatusBits = (UINT1) (DsyncInfo.u2LastMsgSend & 0x00FF);

                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : Received Indication"
                                      " to start R-APS Tx.\r\n", __FUNCTION__);
                    if (ErpsTxStartRapsMessages
                        (pRingInfo, u1StatusBits,
                         u1RapsMessageType) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsDsyncDynSyncMsgHandler failed to start Tx"
                                          " R-APS messages.\r\n");
                    }
                }
                else
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "%s : Received Indication"
                                      " to stop R-APS Tx.\r\n", __FUNCTION__);
                    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsDsyncDynSyncMsgHandler failed to stop Tx"
                                          " R-APS messages.\r\n");
                    }
                }

                break;

            default:
                break;
        }
    }
    pRingInfo->u1DERPSSyncReqFlag = OSIX_TRUE;
    return;
}
