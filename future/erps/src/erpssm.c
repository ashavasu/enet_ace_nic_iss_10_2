/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: erpssm.c,v 1.46 2017/12/16 11:48:40 siva Exp $                 
 *                                                                         *
 * Description: This file contains the State Machine event handling and    *
 *              Priority Logic on functions.                               *
 ***************************************************************************/

#include "erpsinc.h"
#include "erpssm.h"

/****************************************************************************
 *                           erpssm.c prototypes                            *
 ****************************************************************************/
PRIVATE INT4        ErpsSmStopAllTimer (tErpsRingInfo * pRingInfo);

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingStateMachine                           */
/*                                                                           */
/*    Description         : This function will be called with one of the     */
/*                          event and ring entry to which the event will be  */
/*                          applied. This will call already assigned event   */
/*                          handler function based on the event and state    */
/*                          of the ring to take appropriate action.          */
/*                                                                           */
/*    Input(s)            : u2Event    - Event that triggers change in state */
/*                                       of the given ring node.             */
/*                          pRingInfo - Pointer to ring node for which a    */
/*                                       event has occuered.                 */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When a event handler successfully */
/*                                         completed action based on the     */
/*                                         received event.                   */
/*                          OSIX_FAILURE - When a event handler failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingStateMachine (UINT2 u2Event, tErpsRingInfo * pRingInfo,
                        tErpsRcvdMsg * pErpsRcvdMsg)
{
    UINT1               u1OldState = 0;
    UINT1               u1NewState = 0;

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsSmRingStateMachine called with null ring "
                         "entry\r\n");
        return OSIX_FAILURE;
    }

    u1OldState = pRingInfo->u1RingState;

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        if ((*gpErpsStateMachine[pRingInfo->u1RingState][u2Event])
            (pRingInfo, pErpsRcvdMsg) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              ERPS_CRITICAL_TRC,
                              "ErpsSmRingStateMachine returns fail for %s Event"
                              " in %s State\r\n", gaai1RingSmEvent[u2Event],
                              gaai1RingSmState[pRingInfo->u1RingState]);
            return OSIX_FAILURE;
        }
    }
    else if (pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)
    {
        if ((*gpErpsV2StateMachine[pRingInfo->u1RingState][u2Event])
            (pRingInfo, pErpsRcvdMsg) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              ERPS_CRITICAL_TRC,
                              "ErpsSmRingStateMachine returns fail for %s Event"
                              " in %s State\r\n", gaai1RingSmEvent[u2Event],
                              gaai1RingSmState[pRingInfo->u1RingState]);
            return OSIX_FAILURE;
        }
    }

    u1NewState = pRingInfo->u1RingState;

    /* Print the trace messages only for state change */
    if (u1OldState != u1NewState)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsSmRingStateMachine moves to "
                          "state %s on reception of %s event\r\n",
                          gaai1RingSmState[pRingInfo->u1RingState],
                          gaai1RingSmEvent[u2Event]);

#ifdef SYSLOG_WANTED
        SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, ERPS_SYSLOG_ID,
                      "ErpsSmRingStateMachine moves to state %s on reception of %s event"
                      "\r\n", gaai1RingSmState[pRingInfo->u1RingState],
                      gaai1RingSmEvent[u2Event]));
#endif
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleDisable                          */
/*                                                                           */
/*    Description         : Following action will be done in the disable     */
/*                          state:                                           */
/*                          o Stop all Timer,                                */
/*                          o Stop Tx RAPS messages,                         */
/*                          o Flush on ring ports,                           */
/*                          o Set Ring ports to unblocking state,            */
/*                          o Reset all dynamic information of the ring      */
/*                            (counters, ring ports defect status, top       */
/*                             priority event, last message received, port   */
/*                             state and hardware port state)                */
/*                          o Set ring state as .disabled. and remove ring   */
/*                            entry configuration from the hardware.         */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       disable event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         state as disabled state.          */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleDisable (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    UNUSED_PARAM (pErpsRcvdMsg);

    if (ErpsSmStopAllTimer (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleDisable failed to stop all "
                          "timers.\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleDisable failed to stop R-APS "
                          "message transmission\r\n");
    }

    if (ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_NO_ACTIVE_REQUEST,
                                     NULL) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    RingDynInfo.u1RingState = ERPS_RING_DISABLED_STATE;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    /* While ring row status is set as NOT_IN_SERVICE, 
     * if we make ring ports state as forwarding,
     * it will lead to loop in a ring topology.
     * To avoid this kind of scenario, making port state as discarding and 
     * while making row status as DESTROY, ring port state will be 
     * updated as forwarding and port owner also will be 
     * updated to its default owner*/

    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                          ERPS_PORT_BLOCKING);

    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                          ERPS_PORT_BLOCKING);
#ifdef ICCH_WANTED
    /*This flag is set to avoid starting the ERPS_ICCH_FDB_SYNC_TMR timer again in 
     * flush call.*/
    gu1ErpsDisableInProgress = TRUE;
#endif
    ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
    /* Clear previous failed status of ports, as during re-registration 
     * ports will be updated for the current status of the port,
     * that may or may not be same.
     */
    RingDynInfo.u1Port1Status = 2;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_PORT1_STATUS);

    RingDynInfo.u1Port2Status = 2;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_PORT2_STATUS);

    /* Resetting node status (link status) to "not failed" status
     * when the ring is made in-active
     */

    RingDynInfo.u4RingNodeStatus = 0;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);
    if ((pRingInfo->bFopPMDefect == ERPS_FOP_PM_DEFECT) &&
        (pRingInfo->u4RplIfIndex != 0))
    {
        ErpsTrapSendTrapNotifications (pRingInfo,
                                       ERPS_TRAP_PROV_MISMATCH_CLEAR);
        /* Reset the bFopPMDefect to init value after the clearance of FOP-PM */
        pRingInfo->bFopPMDefect = ERPS_INIT_VAL;
    }

    ErpsRingClearRingStats (pRingInfo);

    ErpsUtilDeRegisterRing (pRingInfo);
#ifdef ICCH_WANTED
    gu1ErpsDisableInProgress = FALSE;
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleInit                             */
/*                                                                           */
/*    Description         : Following action will be done in the initializat-*/
/*                          ion of ring state machine:                       */
/*                          o Stop guard timer,                              */
/*                          o Stop WTR timer,                                */
/*                          o Blocks RPL Port, if node is RPL owner,         */
/*                            Unblock non-RPL Port.                          */
/*                          o Block both the port, if it non-RPL owner.      */
/*                          o Stop transmission of R-APS messages.           */
/*                          o Register ring MEPs with link monitoring protoc-*/
/*                            ol and get the current status of the ring links*/
/*                            If any link status is identified, Priority     */
/*                            logic will identify the current top priority   */
/*                            request. This top priority event will be       */
/*                            handed over to state machine to take further   */
/*                            action.                                        */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       disable event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         state as IDLE state.              */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleInit (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* No need to stop the "Guard timer" and "WTR timer", as given in state
     * machine, because in state machine it is not possible to have
     * these two timer running when this function get called.
     */

    RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    RingDynInfo.u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_PRIO_REQUEST);

    /* If it is RPL Owner, block the RPL Port and Unblock non-RPL Ports.
     * If it is non-RPL Owner, Block both the ports */
    if (pRingInfo->u4RplIfIndex != 0)
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                              ERPS_PORT_BLOCKING);

        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);

        if (pRingInfo->u4Port1IfIndex != pRingInfo->u4RplIfIndex)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
        else
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                              ERPS_PORT_BLOCKING);

        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                              ERPS_PORT_BLOCKING);
    }

    /* Register ERPS for the ring MEPs and get the current MEP 
     * status.
     */

    if (ErpsUtilRegisterGetFltAndApply (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleInit failed in mep registration "
                          "with cfm.\r\n");
    }

    /* Check if Force Switch or Manual Switch is present call Priority 
     * request to handle that.
     */

    switch (pRingInfo->u1SwitchCmdType)
    {
        case ERPS_SWITCH_COMMAND_FORCE:
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_FORCE_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmRingHandleInit failed in priority"
                                  " request handling for force switch.\r\n");
            }

            break;
        case ERPS_SWITCH_COMMAND_MANUAL:
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_MANUAL_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmRingHandleInit failed in priority"
                                  " request handling for manual switch.\r\n");
            }

            break;
        case ERPS_SWITCH_COMMAND_NONE:
            if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
                (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
            {
                return OSIX_SUCCESS;
            }

            /* If no failure present, start tx the NRRB from RPL node */
            if (pRingInfo->u4RplIfIndex != 0)
            {
                if (ErpsTxStartRapsMessages (pRingInfo, ERPS_RB_FIELD,
                                             ERPS_MESSAGE_TYPE_NR)
                    == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmRingHandleInit failed to start"
                                      " R-APS messages\r\n");
                }
            }

            break;
        default:
            break;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleLocalSF                          */
/*                                                                           */
/*    Description         : Following action will be done in in case LocalSF-*/
/*                          event handling:                                  */
/*                          o Block switch port in case of LocalSF is raised */
/*                            by Force switch or Manual Switch Command.      */
/*                          o Block failed port, if LocalSF event is received*/
/*                            due to link failure of one of the ring link.   */
/*                          o Unblock other port, which is neither failed nor*/
/*                            one of force switch or manual switch port.     */
/*                          o Transmit SF messages, in case of change in     */
/*                            blocking port.                                 */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       localSF event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         node to update the ring info.     */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleLocalSF (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4BlockIfIndex = 0;
    UINT4               u4UnblockIfIndex = 0;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;
    UINT1               u1FlushStatus = OSIX_FALSE;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* Local SF is raised because of force switch, it is possible that,
     * force switch is overriding already failed port.
     * In this case a switch port should be blocked and failed port should
     * get unblock, to keep only one of the port is blocked by the SEM.
     * Manual switch can happen, if both the ports are non-failed and
     * Local SF raised from link failure, means one of the port link is failed,
     * but other link is not failed.
     * So, We have to either block force switch port, if force switch is
     * invoked, OR
     * We have to block manual switch port, if manual switch is invoked, OR
     * We have to block failed linked port and other port should be unblocked.
     */

    /* if force switch is applied on a failed link port, then there will be no
     * action take place.
     */
    /* Measure the Port stats Changes time When only FS or MS issued */
    /* If Port is blocked because of FS ot MS issued by administrator 
     * Measure the SystemTime for Performance */
    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        if ((pRingInfo->u1HighestPriorityRequest ==
             ERPS_RING_FORCE_SWITCH_REQUEST)
            || (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_MANUAL_SWITCH_REQUEST))
        {
            ErpsUtilMeasureTime (pRingInfo, 0, ERPS_DEFECT_ENC_SWITCH_CMD);
        }
    }
    /*End of Performance Measure Time */

    if ((pRingInfo->u1HighestPriorityRequest
         == ERPS_RING_FORCE_SWITCH_REQUEST) ||
        (pRingInfo->u1HighestPriorityRequest ==
         ERPS_RING_MANUAL_SWITCH_REQUEST))
    {
        if (pRingInfo->u4Port1IfIndex == pRingInfo->u4SwitchCmdIfIndex)
        {
            u4BlockIfIndex = pRingInfo->u4Port1IfIndex;
            u4UnblockIfIndex = pRingInfo->u4Port2IfIndex;

            if (pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
            {
                return OSIX_SUCCESS;
            }
        }
        else
        {
            u4BlockIfIndex = pRingInfo->u4Port2IfIndex;
            u4UnblockIfIndex = pRingInfo->u4Port1IfIndex;

            if (pRingInfo->u1Port2Status & ERPS_PORT_FAILED)
            {
                return OSIX_SUCCESS;
            }
        }
    }
    else if (pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
    {
        u4BlockIfIndex = pRingInfo->u4Port1IfIndex;

        /* Unblock the other port if its not failed */
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            u4UnblockIfIndex = pRingInfo->u4Port2IfIndex;
        }
    }
    else
    {
        u4BlockIfIndex = pRingInfo->u4Port2IfIndex;

        /* Unblock the other port if its not failed */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            u4UnblockIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }

    /* Local SF can override Manual Switch, If failed port is different
     * from the Switch Port, block the failed port. otherwise,
     * just override highest priority event as Local SF and do nothing.*/

    if ((pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL) &&
        (pRingInfo->u1HighestPriorityRequest == ERPS_RING_LOCAL_SF_REQUEST))
    {
        pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;

        if (u4BlockIfIndex == pRingInfo->u4SwitchCmdIfIndex)
        {
            return OSIX_SUCCESS;
        }
    }

    if ((pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE) &&
        (pRingInfo->u1RecoveryMethod == ERPS_RING_MANUAL_RECOVERY))
    {
        /* In NonRevertive mode, if port is blocked but not failed,
         * don't unblock the blocked port.
         */
        if ((!(pRingInfo->u1Port1Status & ERPS_PORT_FAILED)) &&
            (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING))
        {
            u4BlockIfIndex = pRingInfo->u4Port1IfIndex;
            u4UnblockIfIndex = 0;
        }
        else if ((!(pRingInfo->u1Port2Status & ERPS_PORT_FAILED)) &&
                 (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
        {
            u4BlockIfIndex = pRingInfo->u4Port2IfIndex;
            u4UnblockIfIndex = 0;
        }
    }

    ErpsUtilSetPortState (pRingInfo, u4BlockIfIndex, ERPS_PORT_BLOCKING);

    if (u4UnblockIfIndex != 0)
    {
        ErpsUtilSetPortState (pRingInfo, u4UnblockIfIndex,
                              ERPS_PORT_UNBLOCKING);
    }

    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleLocalSF failed to stop R-APS "
                          "messages.\r\n");
    }

    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        /* RPL Port is getting blocked in IDLE state, means there is no
         * change of path will take place as this RPL link already
         * would have blocked in IDLE state of ring */

        if (u4BlockIfIndex == pRingInfo->u4RplIfIndex)
        {
            u1Status = ERPS_DNF_BIT_SET;
        }
        else
        {
            u1FlushStatus = OSIX_TRUE;
        }

        if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                     ERPS_MESSAGE_TYPE_SF) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmRingHandleLocalSF failed to start "
                              "R-APS messages.\r\n");
        }
        if (u1FlushStatus == OSIX_TRUE)
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
            u1FlushStatus = OSIX_FALSE;
        }

        RingDynInfo.u1RingState = ERPS_RING_PROTECTION_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }
    else
    {
        if (pRingInfo->u1HighestPriorityRequest == ERPS_RING_LOCAL_SF_REQUEST)
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_AUTO_SWITCH);
        }
        /* It is possible that WTR may not be running,  but ErpsTmrStopTimer 
         * will take care of handling case when timer is not running  */

        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);

        /* If ring was in protection state and Local SF is indicated by 
         * priority logic, this case can happen only if Local SF event 
         * occures on Manual Switch command. So,override manual switch 
         * command. */

        if (ErpsTxStartRapsMessages (pRingInfo, ERPS_STATUS_BIT_NONE,
                                     ERPS_MESSAGE_TYPE_SF) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmRingHandleLocalSF failed to start R-APS "
                              "messages.\r\n");
        }
        /* Even if the ring is in Protection state, flushing has to be done */
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);

    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleLocalClearSF                     */
/*                                                                           */
/*    Description         : Following action will be done in in case ClearSF-*/
/*                          event handling:                                  */
/*                          o Unblock the cleared port for recover port or   */
/*                            force switch port.                             */
/*                          o Start the guard timer.                         */
/*                          o Clear the switch port command.                 */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       clearSF event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         clearSF event.                    */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleLocalClearSF (tErpsRingInfo * pRingInfo,
                              tErpsRcvdMsg * pErpsRcvdMsg)
{
    UINT4               u4NonSwitchedPort = 0;
    UINT1               u1SwitchedPortStatus = 0;
    UINT1               u1NonSwitchedPortStatus = 0;
    UNUSED_PARAM (pErpsRcvdMsg);

    /* In Non Revertive mode, if the recovery method is configured as 'Manual',
     * then the failure on the port will be cleared only by the administrator
     * configurations */

    if ((pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE) &&
        (pRingInfo->u1RecoveryMethod == ERPS_RING_MANUAL_RECOVERY))
    {
        return OSIX_SUCCESS;
    }

    /* Measure the Port stats Changes time to calculate the Performance,
     *  When only Clear FS or MS issued */

    /* If ClearFS or ClearMS called,store the System time to measure the Performance time */

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        if ((pRingInfo->u1HighestPriorityRequest ==
             ERPS_RING_FORCE_SWITCH_REQUEST)
            || (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_MANUAL_SWITCH_REQUEST))
        {

            ErpsUtilMeasureTime (pRingInfo, 0, ERPS_DEFECT_CLR_SWITCH_CMD);
        }

        /* In version v1, When FS is applied and reloaded, Signal failure will 
         * be triggered during reboot. 
         * FS is treated as link failure in version v1. 
         * So if it is reload thread and highest priority is FS 
         * Port where FS appiled is blocked and all other port is unblocked */

        if ((pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE) &&
            (pRingInfo->u1HighestPriorityRequest ==
             ERPS_RING_FORCE_SWITCH_REQUEST))
        {
            if ((pRingInfo->u4SwitchCmdIfIndex == pRingInfo->u4Port1IfIndex) &&
                (pRingInfo->u1Port2Status & ~ERPS_PORT_FAILED))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                      ERPS_PORT_UNBLOCKING);
            }
            else if ((pRingInfo->u4SwitchCmdIfIndex ==
                      pRingInfo->u4Port2IfIndex)
                     && (pRingInfo->u1Port1Status & ~ERPS_PORT_FAILED))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                      ERPS_PORT_UNBLOCKING);
            }
            return OSIX_SUCCESS;
        }

        /* When clearSF is triggered to clear the operator command FS/MS 
         * then update the switch port to its default value. 
         * SwitchCommand Type is already updated to its default value. 
         * so updating switch port to its default value is done here.*/

        pRingInfo->u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;
    }
    /*End of Performance Measure Time */

    /* State machine will be triggered for ClearSF if,
     * - force switch command is revoked,
     *   ** If force switch was applied on the failed port, no action required.
     *   ** If failure is present and force switch
     *      was applied on the different port, then block the failed port and
     *      unblock the non-failed port.
     *
     * - Manual switch is revoked,
     *   ** If Manual switch was applied just start sending NR msgs
     *
     * - Local ClearSF received for the failed port.
     *   ** If ClearSF is received for one of the failed port, start
     *      sending NR messages.
     */

    if (pRingInfo->u1HighestPriorityRequest == ERPS_RING_FORCE_SWITCH_REQUEST)
    {
        /* If clear SF occured, because of resetting the ForceSwitch, then
         * any failed port present has to be blocked.(Because Local SF event 
         * occured when ForceSwitch was configured would have been overridden)*/

        /* Compute the Switched port is Port1 or Port2 */

        if (pRingInfo->u4Port1IfIndex == pRingInfo->u4SwitchCmdIfIndex)
        {
            /* Store the Switched & NonSwitched port status */
            u1SwitchedPortStatus = pRingInfo->u1Port1Status;
            u1NonSwitchedPortStatus = pRingInfo->u1Port2Status;
            u4NonSwitchedPort = pRingInfo->u4Port2IfIndex;
        }
        else
        {
            /* Store the Switched & NonSwitched port status */
            u1SwitchedPortStatus = pRingInfo->u1Port2Status;
            u1NonSwitchedPortStatus = pRingInfo->u1Port1Status;
            u4NonSwitchedPort = pRingInfo->u4Port1IfIndex;
        }

        /* If the Switched port is failed, simply return */
        if (u1SwitchedPortStatus & ERPS_PORT_FAILED)
        {
            return OSIX_SUCCESS;
        }

        if (u1NonSwitchedPortStatus & ERPS_PORT_FAILED)
        {
            /* If the NonSwitched port is also failed, then
             * a) Switched port has to be unblocked and
             * b) NonSwitched port has to be blocked */

            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                                  ERPS_PORT_UNBLOCKING);

            ErpsUtilSetPortState (pRingInfo, u4NonSwitchedPort,
                                  ERPS_PORT_BLOCKING);

            /* If there is still any failure, NR messages should
             * not  be sent. Return here */

            return OSIX_SUCCESS;
        }

        /* None of the port has failed, NR messages can be send now */
    }

    /* If Local ClearSF received, when guard timer is running, stop the 
     * running guard timer and restart it.
     */
    ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR);

    ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR, pRingInfo->u4GuardTimerValue);

    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleLocalClearSF failed to stop R-APS "
                          "messages.\r\n");
    }

    if (ErpsTxStartRapsMessages (pRingInfo, ERPS_STATUS_BIT_NONE,
                                 ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleLocalClearSF failed to start R-APS "
                          "messages.\r\n");
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleSFMsg                            */
/*                                                                           */
/*    Description         : Following action will be done in in case RAPS(SF)*/
/*                          message reception:                               */
/*                          o Stop tranmission of R-APS messages.            */
/*                          o Unblocked non-failed ports.                    */
/*                          o Stop WTR if node is PROTECTION State.          */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       RAPS(SF)event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         R-APS(SF) message.                */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleSFMsg (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    if ((pRingInfo->u1OperatingMode != ERPS_RING_NON_REVERTIVE_MODE) ||
        (pRingInfo->u1RecoveryMethod != ERPS_RING_MANUAL_RECOVERY) ||
        (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE))
    {
        /* Open Non-failed or Manual switch port, When
         * Ring is in Revertive Mode of operation or
         * Non-Revertive mode and Auto recovery mode.
         */
        /* Unblocking Non-failed ports */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }

        /* Manual switch configured will be overridden */
        if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)
        {
            pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
        }

        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_RB);
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);

    }

    /* Stopping the transmission of R-APS messages */
    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleSFMsg failed to stop R-APS "
                          "messages.\r\n");
    }

    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        /* When the ring is moving from idle to protection state,
         * Flushing has to be done, when DNF bit is not set */

        if (!(pErpsRcvdMsg->u1DNFBitStatus))
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        }

        /* Move to protection state */
        RingDynInfo.u1RingState = ERPS_RING_PROTECTION_STATE;

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }
    else
    {
        /* Stop the WTR timer, if the ring is in Protection state */
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);

        if (pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE)
        {
            /* If source of SF messages changes, due to multiple failure or
             * higher priority event occures:
             * Force switch applied in ring in presence of failure or manual
             * switch or
             * Failure occurs in the ring in the presence of manual switch.
             * Flush learnt entries.
             */
            if ((MEMCMP
                 (gErpsRcvdMsg.RcvdNodeId,
                  pRingInfo->Port1LastRcvdNodeId, sizeof (tMacAddr)) == 0) ||
                (MEMCMP
                 (gErpsRcvdMsg.RcvdNodeId,
                  pRingInfo->Port2LastRcvdNodeId, sizeof (tMacAddr)) == 0))
            {
                return OSIX_SUCCESS;

            }
        }

        if ((!(pErpsRcvdMsg->u1DNFBitStatus)))
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_OPTIMIZE);
        }

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleWtrExpiry                        */
/*                                                                           */
/*    Description         : Following action will be done in in case RAPS(SF)*/
/*                          message reception:                               */
/*                          o Block the RPL Port and unblock the non RPL Port*/
/*                          o Start transmitting NR-RB messages              */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       WTR expiry is received.             */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         WTR expiry event.                 */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleWtrExpiry (tErpsRingInfo * pRingInfo,
                           tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT1               u1RplPortStatus = 0;
    UINT4               u4NonRplIfIndex = 0;
    UINT1               u1StatusBits = ERPS_RB_FIELD;
    UINT1               u1FlushStatus = OSIX_FALSE;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* When WTR Timer expiry Event is received in a RPL Owner node
     * do the following.
     * If ERPSv2
     *     - Stop WTB
     *
     * If RPL port is blocked
     *     - Tx RAPS (NR, RB, DNF)
     * else
     *     - Block RPL port 
     *     If ERPSv2 and DNF Status is SET
     *         - Tx RAPS (NR, RB, DNF)
     *     else
     *         - Flush FDB.
     *         - Tx RAPS (NR, RB)
     * Unblock the NON RPL port
     * if ERPSv2
     *      - Clear DNF
     */

    /* When manual switch applied on RPL node and it is followed by save 
     * and reload, Signal failure will occur during reload, which will 
     * override the manual switch applied. In order to restore the manual 
     * switch, MsrManualSwitchIndication variable is set and 
     * once after WTR Expiry, SEM is triggered 
     * for manual switch request for the corresponding IfIndex */

    if (pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE)
    {
        pRingInfo->u4OldSwitchCmdIfIndex = pRingInfo->u4SwitchCmdIfIndex;
        pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4MsrSwitchCmdIfIndex;

        if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                         ERPS_RING_MANUAL_SWITCH_REQUEST,
                                         NULL) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmRingHandleWtrExpiry failed in priority"
                              " request handling for manual switch.\r\n");
        }
        return OSIX_SUCCESS;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }

    /* Identify the Non RPL Port and RPL Port status */
    if (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex)
    {
        u4NonRplIfIndex = pRingInfo->u4Port2IfIndex;
        u1RplPortStatus = pRingInfo->u1Port1Status;
    }
    else
    {
        u4NonRplIfIndex = pRingInfo->u4Port1IfIndex;
        u1RplPortStatus = pRingInfo->u1Port2Status;
    }

    if (u1RplPortStatus & ERPS_PORT_BLOCKING)
    {
        u1StatusBits |= ERPS_DNF_FIELD;
        /* Block RPL port */
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                              ERPS_PORT_BLOCKING);
    }
    else
    {
        /* Block RPL port */
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                              ERPS_PORT_BLOCKING);
        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)
            && (pRingInfo->bDNFStatus == OSIX_TRUE))
        {
            u1StatusBits |= ERPS_DNF_FIELD;
        }
        else
        {
            u1FlushStatus = OSIX_TRUE;
        }
    }
    /* Unblock the Non RPL Port */
    ErpsUtilSetPortState (pRingInfo, u4NonRplIfIndex, ERPS_PORT_UNBLOCKING);

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    if (ErpsTxStartRapsMessages (pRingInfo, u1StatusBits,
                                 ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmRingHandleWtrExpiry failed to start R-APS "
                          "messages.\r\n");
    }

    if (u1FlushStatus == OSIX_TRUE)
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        u1FlushStatus = OSIX_FALSE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        pRingInfo->bDNFStatus = OSIX_FALSE;
    }

    RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    /* According to the standard G.8021/Y.1341 - Amendment 1(07/2011) in section .6.1.4.3.4
     * When RPL node state is changed from disable to active, RPL node will not receive NR-RB
     * Packet when RPL neighbour is configured,at that time T-FOP defect should not be 
     * encountered. So stop the timer */
    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingHandleNRRBMsg                          */
/*                                                                           */
/*    Description         : Following action will be done in in case NRRB    */
/*                          message reception:                               */
/*                          o Unblock non-RPL port in RPL Owner node         */
/*                          o Unblock both the ports in non-RPL node.        */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       NRRB message is received.           */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         NRRB reception.                   */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleNRRBMsg (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    /* In Non RPL Owner node, unblock both the port. */
    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                          ERPS_PORT_UNBLOCKING);

    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                          ERPS_PORT_UNBLOCKING);

    if (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE)
    {
        if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmRingHandleNRRBMsg failed to stop R-APS "
                              "messages.\r\n");
        }

        /* Flush, before moving from Protection to Idle state */
        if (!(pErpsRcvdMsg->u1DNFBitStatus))
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        }

        RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsSmRingHandleNRMsg                            */
/*                                                                           */
/*    Description         : Following action will be done in in case NRRB    */
/*                          message reception:                               */
/*                          o Start WTR timer,                               */
/*                          o If it is non-revertive mode and NR msg received*/
/*                            if any port is blocked and received node       */
/*                            priority is better than node's. then unblock,  */
/*                            blocked port.                                  */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       NR   message is received.           */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         NR   reception.                   */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingHandleNRMsg (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    UINT4               u4BlockIfIndex = 0;

    /* When ring is non-revertive mode of operation and there is no failure
     * present in the node links, but one of the ring port still in blocked
     * state, determines that there was a failure in one of the ring link,
     * recovered but not opened up.
     * If this is the case and NR msg is received, so compare the nodeid and 
     * check if port has to unblock.
     *
     * There is possibility that blocked port is present in the RPL Owner
     * node */

    if ((pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE) &&
        (pRingInfo->u4RplIfIndex != 0))
    {
        /* In revertive mode, on reception of NR Message in RPL owner node,
         * start WTR timer */
        if (pRingInfo->u4WTRTimerValue == 0)
        {
            /* If WTR timer value is zero, immediatly revert */
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_WTR_EXP_REQUEST,
                                             pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrWTRTimerExp returned failure"
                                  " from Priority Request handling.\r\n");
            }
        }
        else
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                               pRingInfo->u4WTRTimerValue);
        }
    }

    /* The Node Id comparison has to be done for two cases
     * 1. When the ERPS compatible version is 1, and when the mode of operation is 
     *    non-revertive.
     * 2. When the ERPS compatible version is 2,and when the mode of operation is 
     *    revertive in a non-RPL owner or non-revertive. 
     *    [Reference : As per ITU-T G.8032/Y.1344 (03/2010) standard ]
     * 
     * The following If condition  ensures that the above 2 cases are taken care.
     *
     * Because of the Node Id comparsion done in both modes of operation for Version 2.
     * it is sufficient if the appropriate mode is configured only in the RPL owner.
     */

    if ((pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE) ||
        ((pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE)
         && (pRingInfo->u4RplIfIndex == 0)
         && (pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)))
    {
        if (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)
        {
            u4BlockIfIndex = pRingInfo->u4Port1IfIndex;
        }
        else if (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)
        {
            u4BlockIfIndex = pRingInfo->u4Port2IfIndex;
        }
        else
        {
            /* If there is no block port present in the node, then no
             * action is required */
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              " ErpsSmRingHandleNRMsg :EXIT as none of the ring ports"
                              "are in blocking state \r\n");
            return OSIX_SUCCESS;
        }

        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
        {
            /* If ERPS version is v1 and if the ring nodeid is lesser than received nodeid,
             * then no action is required */

            if ((MEMCMP (&(ERPS_NODE_ID (pRingInfo->u4ContextId)),
                         &(pErpsRcvdMsg->RcvdNodeId), sizeof (tMacAddr))) < 0)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmRingHandleNRMsg : EXIT :The Received nodeid "
                                  "%02d:%02d:%02d:%02d:%02d:%02d"
                                  "is greater than ring node id "
                                  "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                                  pErpsRcvdMsg->RcvdNodeId[1],
                                  pErpsRcvdMsg->RcvdNodeId[2],
                                  pErpsRcvdMsg->RcvdNodeId[3],
                                  pErpsRcvdMsg->RcvdNodeId[4],
                                  pErpsRcvdMsg->RcvdNodeId[5]);
                return OSIX_SUCCESS;
            }
        }

        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            /* If ERPS version is v1 and if the ring nodeid is greater than received nodeid,
             * then no action is required */
            if ((MEMCMP (&(ERPS_NODE_ID (pRingInfo->u4ContextId)),
                         &(pErpsRcvdMsg->RcvdNodeId), sizeof (tMacAddr))) > 0)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmRingHandleNRMsg : EXIT :The Received nodeid "
                                  "%02d:%02d:%02d:%02d:%02d:%02d"
                                  "is lesser than ring node id "
                                  "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                                  pErpsRcvdMsg->RcvdNodeId[1],
                                  pErpsRcvdMsg->RcvdNodeId[2],
                                  pErpsRcvdMsg->RcvdNodeId[3],
                                  pErpsRcvdMsg->RcvdNodeId[4],
                                  pErpsRcvdMsg->RcvdNodeId[5]);

                return OSIX_SUCCESS;
            }
        }

        /* If the ring node has received its own R-APS NR message , and if both the ring ports 
         *  of the node are in blocked state (This condition will occur, when both the ring ports of 
         *  the same ring node have failed and recovered simultaneously), then one of the ring ports
         *  (Here it is always ring port1 ) of the ring is moved to unblocked state */

        if ((MEMCMP (&(ERPS_NODE_ID (pRingInfo->u4ContextId)),
                     &(pErpsRcvdMsg->RcvdNodeId), sizeof (tMacAddr))) == 0)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              " ErpsSmRingHandleNRMsg : The Received nodeid %02d:%02d:%02d:%02d:%02d:%02d"
                              " is equal to the ring node id "
                              "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                              pErpsRcvdMsg->RcvdNodeId[1],
                              pErpsRcvdMsg->RcvdNodeId[2],
                              pErpsRcvdMsg->RcvdNodeId[3],
                              pErpsRcvdMsg->RcvdNodeId[4],
                              pErpsRcvdMsg->RcvdNodeId[5]);

            if ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) &&
                (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
            {
                u4BlockIfIndex = pRingInfo->u4Port1IfIndex;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmRingHandleNRMsg : Ring Port1 is moved to unblocking"
                                  " state when the R-APS NR is received from self \r\n");

            }
            else
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmRingHandleNRMsg : EXIT :The Received nodeid"
                                  " %02d:%02d:%02d:%02d:%02d:%02d is equal to the  ring"
                                  "node id, but one of the ports is in unblocked state "
                                  "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                                  pErpsRcvdMsg->RcvdNodeId[1],
                                  pErpsRcvdMsg->RcvdNodeId[2],
                                  pErpsRcvdMsg->RcvdNodeId[3],
                                  pErpsRcvdMsg->RcvdNodeId[4],
                                  pErpsRcvdMsg->RcvdNodeId[5]);
                return OSIX_SUCCESS;
            }

        }

        if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmRingHandleNRMsg failed to stop R-APS "
                              "messages.\r\n");
        }

        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                          pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          " ErpsSmRingHandleNRMsg : Ring Port %u  is moved to unblocking"
                          " state \r\n", u4BlockIfIndex);
        ErpsUtilSetPortState (pRingInfo, u4BlockIfIndex, ERPS_PORT_UNBLOCKING);

    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmStopAllTimer                               */
/*                                                                           */
/*    Description         : This function will be used to stop all running   */
/*                          timers of the given ring node.                   */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       all timer has to be stopped.        */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         all timers are stopped.           */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         stop any timer.                   */
/*****************************************************************************/
PRIVATE INT4
ErpsSmStopAllTimer (tErpsRingInfo * pRingInfo)
{

    if (ErpsTmrStopTimer (pRingInfo, ERPS_HOLD_OFF_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop holdoff Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop guard Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop wtr Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop periodic Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }
    if (ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop Tfop Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }
    if (ErpsTmrStopTimer (pRingInfo, ERPS_FOP_PM_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop Fop-Pm Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }

#ifdef ICCH_WANTED

    if (ErpsTmrStopTimer (pRingInfo, ERPS_ICCH_FDB_SYNC_TMR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmStopAllTimer failed to stop FDB Sync Timer."
                          "\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingImpossibleEvent                        */
/*                                                                           */
/*    Description         : This function will get called when an impossible */
/*                          event received for current state of the ring.    */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                      event is received.                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_FAILURE                                     */
/*****************************************************************************/
PUBLIC INT4
ErpsSmRingImpossibleEvent (tErpsRingInfo * pRingInfo,
                           tErpsRcvdMsg * pErpsRcvdMsg)
{
    UNUSED_PARAM (pErpsRcvdMsg);

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | ERPS_CRITICAL_TRC,
                      "ErpsSmRingImpossibleEvent: Impossible State-Event "
                      "reached\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmRingNoAction                               */
/*                                                                           */
/*    Description         : This function will get called for events to      */
/*                          accept event for No Action.                      */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                      event is received.                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_FAILURE                                     */
/*****************************************************************************/

PUBLIC INT4
ErpsSmRingNoAction (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    UNUSED_PARAM (pErpsRcvdMsg);

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsSmRingNoAction: No Action.\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmCalTopPriorityRequest                      */
/*                                                                           */
/*    Description         : Following action will be used to identify the    */
/*                          top priority request and this function will      */
/*                          trigger appropriate state/event machine.         */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a    */
/*                                       top priority request to be determi- */
/*                                       nd.                                 */
/*                          u1Request - Input request/state to priority logic*/
/*                                                                           */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         returned from state/event machine.*/
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete handling of input request*/
/*****************************************************************************/
PUBLIC INT4
ErpsSmCalTopPriorityRequest (tErpsRingInfo * pRingInfo, UINT1 u1Request,
                             tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsContextInfo   *pErpsContextInfo = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSubRingEntry      *pSubRingEntry = NULL;
    UINT1               u1Flag = 0;

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pErpsContextInfo->u1ModuleStatus != OSIX_ENABLED)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsSmCalTopPriorityRequest: ERPS disabled "
                          "in this context\r\n");
        return OSIX_SUCCESS;
    }

    /* Top Priority event will be calculated in following manner:
     * - No Active Request will override all other request and 
     *   set ERPS_RING_NO_ACTIVE_REQUEST as top priority request. 
     *   Clear Request will override all other requests. 
     * - Force Switch request will override all other defect and
     *   Local SF event will be notified to State Machine.
     * - If Local SF is indicated but,
     *   Force Switch is present. Then this Local SF
     *   will not be reported to State Machine.
     * - If Local SF is indicated but current top priority is not Local
     *   SF (first Local SF), then this Local SF will be reported to State
     *   Machine.
     * - Clear SF indicated, but if Force switch
     *   is present in the ring this Local SF will not be reported to
     *   State Machine.
     * - Apart from above scenario; higher priority event will override  
     *   lower priority request and lower priority request will be ignored,
     *   in the presence of higher priority request.
     *     
     *          Request/State + Status                   Priority
     *        ERPS_RING_NO_ACTIVE_REQUEST               (highest)
     *        ERPS_RING_NO_SWITCH_REQUEST (V1 only)         |
     *        ERPS_RING_CLEAR_REQUEST     (V2 only)         |
     *        ERPS_RING_FORCE_SWITCH_REQUEST                |
     *        ERPS_RING_RAPS_FS_REQUEST                     |
     *        ERPS_RING_LOCAL_SF_REQUEST                    |
     *        ERPS_RING_LOCAL_CLEAR_SF_REQUEST              | 
     *        ERPS_RING_RAPS_SF_REQUEST                     |
     *        ERPS_RING_RAPS_MS_REQUEST                     |
     *        ERPS_RING_MANUAL_SWITCH_REQUEST               |
     *        ERPS_RING_WTR_EXP_REQUEST                     |
     *        ERPS_RING_WTR_STOP_REQUEST                    |
     *        ERPS_RING_WTR_RUNNING_REQUEST                 |
     *        ERPS_RING_WTB_EXP_REQUEST                     |
     *        ERPS_RING_WTB_STOP_REQUEST                    |
     *        ERPS_RING_WTB_RUNNING_REQUEST                 |
     *        ERPS_RING_RAPS_NRRB_REQUEST                   |
     *        ERPS_RING_RAPS_NR_REQUEST                  (Lowest) 
     */

    switch (u1Request)
    {
        case ERPS_RING_NO_ACTIVE_REQUEST:

            /* No request will override all the requests */
            RingDynInfo.u1HighestPriorityRequest = u1Request;

            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);
            break;

        case ERPS_RING_NO_SWITCH_REQUEST:
            /* This request need to be handled only in v1
             * To clear the force switch or manual in v2, clear request need to be used  */
            /* MsrManualSwitchIndication variable should be reset when applied 
             * switch command is cleared */
            if (pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE)
            {
                pRingInfo->bMsrManualSwitchIndication = OSIX_FALSE;
                pRingInfo->u4MsrSwitchCmdIfIndex = OSIX_FALSE;
            }

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION1)
            {
                if ((pRingInfo->u1HighestPriorityRequest ==
                     ERPS_RING_FORCE_SWITCH_REQUEST) ||
                    (pRingInfo->u1HighestPriorityRequest ==
                     ERPS_RING_MANUAL_SWITCH_REQUEST))
                {
                    if (ErpsSmRingStateMachine
                        (ERPS_RING_LOCAL_CLEAR_SF_EVENT, pRingInfo,
                         pErpsRcvdMsg) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsSmCalTopPriorityRequest returns "
                                          "fail for state %s and event %s\r\n",
                                          gaai1RingSmState[pRingInfo->
                                                           u1RingState],
                                          gaai1RingSmEvent
                                          [ERPS_RING_LOCAL_CLEAR_SF_EVENT]);
                        return OSIX_FAILURE;
                    }

                    RingDynInfo.u1HighestPriorityRequest =
                        ERPS_RING_NO_ACTIVE_REQUEST;

                    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                               ERPS_RING_PRIO_REQUEST);
                }
            }
            break;

        case ERPS_RING_CLEAR_REQUEST:
            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {
                /* As clear command can be given even when there are no active MS or FS 
                 * Requests No need to check */
                if (ErpsSmRingStateMachine (ERPS_RING_CLEAR_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent[ERPS_RING_CLEAR_EVENT]);
                    return OSIX_FAILURE;
                }

                /* While processing clear request, if u1ProprietaryClearFS is enabled and 
                 * also any one of the ring port is failed means, SEM will be  
                 * triggered internally for ERPS_RING_LOCAL_SF_REQUEST and  
                 * the u1HighestPriorityRequest will be updated properly.  
                 * That should not be overwritten here. */
                if (!
                    ((pErpsContextInfo->u1ProprietaryClearFS == ERPS_SNMP_TRUE)
                     && ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
                         || (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))))
                {
                    if (((pRingInfo->u4RplIfIndex == 0) &&
                         (pRingInfo->u1OperatingMode ==
                          ERPS_RING_REVERTIVE_MODE))
                        || (pRingInfo->u1OperatingMode ==
                            ERPS_RING_NON_REVERTIVE_MODE))
                    {
                        RingDynInfo.u1HighestPriorityRequest =
                            ERPS_RING_NO_ACTIVE_REQUEST;
                        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                                   ERPS_RING_PRIO_REQUEST);
                    }
                }
            }
            break;

        case ERPS_RING_FORCE_SWITCH_REQUEST:

            /* In Version 1 proprietary implementation of Forced switch,FS is treated as Local SF 
             * In Version 2, FS is treated with a new event (LOCAL_FS_EVENT) according to the state
             * machine implementation given in ITU-T G.8032 03/2010 standard.*/
            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION1)
            {

                /* When the thread is MSR Restoration and if force switch is 
                 * applied before save and reload, MsrManualSwitchIndication 
                 * variable is set and corresponding IfIndex is stored. */
                if (MsrIsMibRestoreInProgress () == OSIX_SUCCESS)
                {
                    pRingInfo->bMsrManualSwitchIndication = OSIX_TRUE;
                }

                RingDynInfo.u1HighestPriorityRequest = u1Request;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
                if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_SF_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_LOCAL_SF_EVENT]);
                    return OSIX_FAILURE;
                }
            }
            else if (pRingInfo->u1RAPSCompatibleVersion ==
                     ERPS_RING_COMPATIBLE_VERSION2)
            {

                pRingInfo->u1HighestPriorityRequestWithoutVC = u1Request;

                if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_FS_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_LOCAL_FS_EVENT]);
                    return OSIX_FAILURE;
                }

                RingDynInfo.u1HighestPriorityRequest = u1Request;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);

                pRingInfo->u1HighestPriorityRequestWithoutVC =
                    ERPS_RING_NO_ACTIVE_REQUEST;
            }

            break;

        case ERPS_RING_RAPS_FS_REQUEST:

            /* RAPS FS is supported only in ERPS V2 */
            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {
                if (pRingInfo->u1HighestPriorityRequest ==
                    ERPS_RING_FORCE_SWITCH_REQUEST)
                {

                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "ErpsSmCalTopPriorityRequest RAPS FS"
                                      "request received, when force switch is "
                                      "present: request ignored.\r\n");
                    break;
                }

                if (ErpsSmRingStateMachine (ERPS_RING_RAPS_FS_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_RAPS_FS_EVENT]);
                    return OSIX_FAILURE;
                }

                RingDynInfo.u1HighestPriorityRequest =
                    ERPS_RING_NO_ACTIVE_REQUEST;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;

        case ERPS_RING_LOCAL_SF_REQUEST:

            /* Local SF cannot be applied if the node is in Local FS 
             * or RAPS FS state,So Ring state is checked instead of 
             * checking Highest priority request */
            if (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest Local SF "
                                  "request received, when force switch is "
                                  "present: request ignored.\r\n");
                break;
            }
            /* Request updation has to be done, before triggering to SEM,
             * because SEM will operate based on this request for LOCAL SF */

            pRingInfo->u1HighestPriorityRequestWithoutVC = u1Request;
            RingDynInfo.u1HighestPriorityRequest = u1Request;

            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);

            if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_SF_EVENT, pRingInfo,
                                        pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent[ERPS_RING_LOCAL_SF_EVENT]);
                return OSIX_FAILURE;
            }

            pRingInfo->u1HighestPriorityRequestWithoutVC =
                ERPS_RING_NO_ACTIVE_REQUEST;

            break;

        case ERPS_RING_LOCAL_CLEAR_SF_REQUEST:

            /* ClearSF cannot be applied if force switch is configured */
            if (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest Local Clear SF "
                                  "request received, when force switch is "
                                  "present: request ignored.\r\n");
                break;
            }

            if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_CLEAR_SF_EVENT,
                                        pRingInfo, pErpsRcvdMsg)
                == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent
                                  [ERPS_RING_LOCAL_CLEAR_SF_EVENT]);
                return OSIX_FAILURE;
            }

            /* In Version v1, Applied FS is tried to clear after reload, 
             * it is not cleared, because HighestPriority is changed to 
             * NO_ACTIVE_REQUEST and it gets failed in 
             * ERPS_RING_NO_SWITCH_REQUEST. So when the switch command 
             * is FS and variable "bMsrManualSwitchIndication" is set 
             * its highest priority is not changed */
            if ((pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_FORCE)
                && (pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE))
            {
                break;
            }

            /* Local clear SF is higher priority request than R-APS (SF),
             * So, local clear SF will override R-APS (SF) state and
             * Local clear SF will be applied to State Machine.
             * Once the local clear SF is applied, it can be ignored by
             * subsequent lower priority message to move forward.
             */

            if (((pRingInfo->u1RAPSCompatibleVersion ==
                  ERPS_RING_COMPATIBLE_VERSION2)
                 &&
                 ((pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE)
                  || (pRingInfo->u4RplIfIndex == 0)))
                || (pRingInfo->u1RAPSCompatibleVersion ==
                    ERPS_RING_COMPATIBLE_VERSION1))
            {
                if (pRingInfo->u1RingState != ERPS_RING_MANUAL_SWITCH_STATE)
                {
                    RingDynInfo.u1HighestPriorityRequest =
                        ERPS_RING_NO_ACTIVE_REQUEST;
                    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                               ERPS_RING_PRIO_REQUEST);
                }
            }
            break;

        case ERPS_RING_RAPS_SF_REQUEST:

            /* Flush is not seen after WTR expiry, when link failure occurs on 
             * both ports of Neighbor node, DNF status will be set to TRUE either 
             * through LocalSF/RAPSSF. When signal failure in non RPL port of 
             * Neighbor node recovers, It will send packet with DNF not set. 
             * On receiving it RPL node has local signal failure and will not 
             * process received RAPS-SF.So to avoid this issue DNFStatus can be 
             * set to FALSE,So that flush will happen after WTR expiry. */

            if ((pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE)
                && (pRingInfo->u4RplIfIndex != 0)
                && (pRingInfo->bDNFStatus == OSIX_TRUE)
                && (pErpsRcvdMsg->u1DNFBitStatus == 0))
            {
                pRingInfo->bDNFStatus = OSIX_FALSE;
            }

            /* R-APS SF will be ignored, when ForceSwitch exists */

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_FORCE_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest RAPS SF request "
                                  "received, when force switch is present:"
                                  " request ignored.\r\n");
                break;
            }

            /* R-APS SF will be ignored, when Local SF exists */

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_LOCAL_SF_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest RAPS SF request "
                                  "received, when Local SF is present: request "
                                  "ignored.\r\n");
                break;
            }
            if (ErpsSmRingStateMachine (ERPS_RING_RAPS_SF_EVENT, pRingInfo,
                                        pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent[ERPS_RING_RAPS_SF_EVENT]);
                return OSIX_FAILURE;
            }
            /* Remote request will not be stored as highest priority request */
            RingDynInfo.u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);
            break;

        case ERPS_RING_RAPS_MS_REQUEST:

            /* RAPS MS is supported only in ERPS V2 */
            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {
                /* When RAPS MS is received, the current high priority request was not
                 * checked earlier. In such a scenario, the SEM will result in No action.
                 * However, after the SEM returns success, we are resetting the High
                 * priority request from SF or FS. Such a scenario will occur during MSR
                 * when switches in the rings are coming up at different times.
                 *
                 * Example: A switch during reload will have Local SF. If it receives
                 * RAPS MS from a VC interconnected node, SEM will result in no action.
                 * But the highest priority request will be reset. */
                if (pRingInfo->u1HighestPriorityRequest ==
                    ERPS_RING_FORCE_SWITCH_REQUEST)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "ErpsSmCalTopPriorityRequest RAPS SF request "
                                      "received, when force switch is present:"
                                      " request ignored.\r\n");
                    break;
                }

                /* R-APS SF will be ignored, when Local SF exists */

                if (pRingInfo->u1HighestPriorityRequest ==
                    ERPS_RING_LOCAL_SF_REQUEST)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "ErpsSmCalTopPriorityRequest RAPS SF request "
                                      "received, when Local SF is present: request "
                                      "ignored.\r\n");
                    break;
                }

                if (ErpsSmRingStateMachine (ERPS_RING_RAPS_MS_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail"
                                      " for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_RAPS_MS_EVENT]);

                    return OSIX_FAILURE;
                }

                RingDynInfo.u1HighestPriorityRequest =
                    ERPS_RING_NO_ACTIVE_REQUEST;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;

        case ERPS_RING_MANUAL_SWITCH_REQUEST:

            /* When the thread is MSR Restoration and if manual switch is 
             * applied before save and reload, MsrManualSwitchIndication 
             * variable is set and corresponding IfIndex is stored. */
            if ((MsrIsMibRestoreInProgress () == OSIX_SUCCESS) &&
                (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL))
            {
                pRingInfo->bMsrManualSwitchIndication = OSIX_TRUE;
                pRingInfo->u4MsrSwitchCmdIfIndex =
                    pRingInfo->u4SwitchCmdIfIndex;
            }

            /* Manual switch cannot be applied in the presence of
             * Local FS, RAPS FS or LocalSF. */
            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_FORCE_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest Manual Switch"
                                  " request received, when force switch is "
                                  "present: request ignored.\r\n");

                break;
            }
            /* As per standard Rec. ITU-T G.8032/Y.1344 (03/2010),RAPS MS
             * is of high priority than Local Ms, So Local MS cannot be 
             * applied in the presence of RAPS MS */
            if (pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE)
            {
                /* When Manual switch is appiled because of VC failure, Switch
                 * port is updated, but ring state is not moved to manual switch
                 * as the ring is already in MANUAL SWITCH state.So, MSR should
                 * be updated with old port number. */
                pRingInfo->u4SwitchCmdIfIndex =
                    pRingInfo->u4OldSwitchCmdIfIndex;
                u1Flag = ERPS_SWITCH_PORT;
                ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest Manual Switch"
                                  " request received, when RAPS MS is "
                                  "present: request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_LOCAL_SF_REQUEST)

            {
                /* When Manual switch is appiled because of VC failure, Switch
                 * port is updated, but ring state is not moved to manual 
                 * switch as the ring is already in PROTECTION state. So, MSR 
                 * should be updated with old port No */
                pRingInfo->u4SwitchCmdIfIndex =
                    pRingInfo->u4OldSwitchCmdIfIndex;
                u1Flag = ERPS_SWITCH_PORT;
                ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest Manual Switch"
                                  " request received, when Ring is in"
                                  "protection: request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION1)
            {
                RingDynInfo.u1HighestPriorityRequest = u1Request;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);

                if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_SF_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_LOCAL_SF_EVENT]);
                    return OSIX_FAILURE;
                }

            }
            else if (pRingInfo->u1RAPSCompatibleVersion ==
                     ERPS_RING_COMPATIBLE_VERSION2)
            {
                if (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE)

                {
                    /* When Manual switch is applied because of VC failure, Switch 
                     * port is updated, but ring state is not moved to manual 
                     * switch as the ring is already in PROTECTION state. So, MSR 
                     * should be updated with old port No */
                    pRingInfo->u4SwitchCmdIfIndex =
                        pRingInfo->u4OldSwitchCmdIfIndex;
                    u1Flag = ERPS_SWITCH_PORT;
                    ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "ErpsSmCalTopPriorityRequest Manual Switch"
                                      " request received, when Ring is in"
                                      "protection: request ignored.\r\n");
                    break;
                }

                if (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE)
                {
                    /* When Manual switch is appiled because of VC failure, 
                     * Switch port is updated, but ring state is not moved to 
                     * manual switch as the ring is already in FORCE SWITCH 
                     * state. So, MSR should be updated with old port number.*/
                    pRingInfo->u4SwitchCmdIfIndex =
                        pRingInfo->u4OldSwitchCmdIfIndex;
                    u1Flag = ERPS_SWITCH_PORT;
                    ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId, CONTROL_PLANE_TRC,
                                      "ErpsSmCalTopPriorityRequest Manual Switch"
                                      " request received, when force switch is "
                                      "present: request ignored.\r\n");
                    break;
                }

                if (ErpsSmRingStateMachine (ERPS_RING_LOCAL_MS_EVENT, pRingInfo,
                                            pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_LOCAL_MS_EVENT]);
                    return OSIX_FAILURE;
                }

                RingDynInfo.u1HighestPriorityRequest = u1Request;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }

            break;

        case ERPS_RING_WTR_EXP_REQUEST:

            /* In the present of  Local SF or force switch as top priority 
             * event WTR expiry should not be handled as these two are higher 
             * priority request than WTR expiry.
             * But When the Local SF or force switch events handled they will
             * stop the already running WTR timer. WTR expiry is not possible
             * in present of Local SF or force switch requests.
             */

            if (ErpsSmRingStateMachine (ERPS_RING_WTR_EXP_EVENT, pRingInfo,
                                        pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent[ERPS_RING_WTR_EXP_EVENT]);
                return OSIX_FAILURE;
            }

            /* Once the WTR expiry is applied, it can be ignored by
             * subsequent lower priority message to move forward  */

            /* When clearing manual switch after reload, its 
             * HighestPriorityRequest is changed to NO_ACTIVE_REQUEST 
             * So it fails in case "ERPS_RING_NO_SWITCH_REQUEST" 
             * and manual switch case is not cleared. So highest 
             * priority is not set to NO_ACTIVE_REQUEST when 
             * MsrManualSwitchIndication variable is set. */
            if (pRingInfo->bMsrManualSwitchIndication != OSIX_TRUE)
            {
                RingDynInfo.u1HighestPriorityRequest =
                    ERPS_RING_NO_ACTIVE_REQUEST;

                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;

        case ERPS_RING_WTR_STOP_REQUEST:

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_WTR_RUNNING_REQUEST)
            {
                RingDynInfo.u1HighestPriorityRequest
                    = ERPS_RING_NO_ACTIVE_REQUEST;

                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;

        case ERPS_RING_WTR_RUNNING_REQUEST:

            /* In the present of  Local SF or R-APS (SF) as top priority 
             * event WTR running status should not be handled as these two
             * are higher priority request than WTR running.
             * But When the Local SF or R-APS (SF) events handled they will
             * stop the already running WTR timer. WTR running is not possible
             * in present of Local SF or R-APS (SF) requests.

             No action identified for WTR running status */

            RingDynInfo.u1HighestPriorityRequest = u1Request;

            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);
            break;

        case ERPS_RING_WTB_EXP_REQUEST:

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {
                /* In the present of  Local SF or force switch as top priority 
                 * event WTB expiry should not be handled as these two are higher 
                 * priority request than WTB expiry.
                 * But When the Local SF or force switch events handled they will
                 * stop the already running WTB timer. WTB expiry is not possible
                 * in present of Local SF or force switch requests.
                 */

                if (ErpsSmRingStateMachine
                    (ERPS_RING_WTB_EXP_EVENT, pRingInfo,
                     pErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest returns fail "
                                      "for state %s and event %s\r\n",
                                      gaai1RingSmState[pRingInfo->u1RingState],
                                      gaai1RingSmEvent
                                      [ERPS_RING_WTB_EXP_EVENT]);
                    return OSIX_FAILURE;
                }

                /* Once the WTB expiry is applied, it can be ignored by
                 * subsequent lower priority message to move forward  */

                RingDynInfo.u1HighestPriorityRequest =
                    ERPS_RING_NO_ACTIVE_REQUEST;

                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;

        case ERPS_RING_WTB_STOP_REQUEST:

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {
                if (pRingInfo->u1HighestPriorityRequest ==
                    ERPS_RING_WTB_RUNNING_REQUEST)
                {
                    RingDynInfo.u1HighestPriorityRequest
                        = ERPS_RING_NO_ACTIVE_REQUEST;

                    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                               ERPS_RING_PRIO_REQUEST);
                }
            }
            break;

        case ERPS_RING_WTB_RUNNING_REQUEST:

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION2)
            {

                /* In the present of  Local SF or R-APS (SF) as top priority 
                 * event WTB running status should not be handled as these two
                 * are higher priority request than WTB running.
                 * But When the Local SF or R-APS (SF) events handled they will
                 * stop the already running WTB timer. WTB running is not possible
                 * in present of Local SF or R-APS (SF) requests.

                 No action identified for WTB running status */

                RingDynInfo.u1HighestPriorityRequest = u1Request;

                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);
            }
            break;
        case ERPS_RING_RAPS_NRRB_REQUEST:

            /* NR-RB state request will be ignored, if any higher priority 
             * request present in ring */

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_FORCE_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NRRB request "
                                  "received, when force switch is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_LOCAL_SF_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NRRB request "
                                  "received, when Local SF is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_MANUAL_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NRRB request "
                                  "received, when Manual Switch is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_WTR_RUNNING_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NRRB request "
                                  "received, when WTR running: request "
                                  "ignored.\r\n");
                break;
            }

            if (ErpsSmRingStateMachine (ERPS_RING_RAPS_NRRB_EVENT, pRingInfo,
                                        pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent[ERPS_RING_RAPS_NRRB_EVENT]);
                return OSIX_FAILURE;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_WTB_RUNNING_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request"
                                  "received, when WTB running is:request "
                                  "ignored\n");
                break;
            }

            /* Remote request will not be stored as highest priority request */

            RingDynInfo.u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;

            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);
            break;

        case ERPS_RING_RAPS_NR_REQUEST:

            /* This event in the main ring can cause virtual channel
             * of the associated sub-ring to become up. Hence inform 
             * associated sub-rings to take appropriate action.*/

            if (pRingInfo->u1RAPSCompatibleVersion ==
                ERPS_RING_COMPATIBLE_VERSION1)
            {
                if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
                    (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
                {

                    pSllNode = TMO_SLL_First (&pRingInfo->SubRingListHead);

                    while (pSllNode != NULL)
                    {
                        pSubRingEntry = (tSubRingEntry *) pSllNode;
                        if (pSubRingEntry->u4SubRingId != 0)
                        {
                            ErpsSmMainRingStatusIndication (pRingInfo->
                                                            u4ContextId,
                                                            pSubRingEntry->
                                                            u4SubRingId);
                        }

                        pSllNode =
                            TMO_SLL_Next (&pRingInfo->SubRingListHead,
                                          pSllNode);
                    }
                }
            }
            /* NR state request will be ignored, if any higher priority 
             * request present in ring */

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_FORCE_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request "
                                  "received, when force switch is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_LOCAL_SF_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request "
                                  "received, when Local SF is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_MANUAL_SWITCH_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request "
                                  "received, when Manual Switch is present:"
                                  " request ignored.\r\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_WTR_RUNNING_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request"
                                  "received, when WTR running is:request "
                                  "ignored\n");
                break;
            }

            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_WTB_RUNNING_REQUEST)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsSmCalTopPriorityRequest NR request"
                                  "received, when WTB running is:request "
                                  "ignored\n");
                break;
            }

            if (ErpsSmRingStateMachine (ERPS_RING_RAPS_NR_EVENT, pRingInfo,
                                        pErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest returns fail "
                                  "for state %s and event %s\r\n",
                                  gaai1RingSmState[pRingInfo->u1RingState],
                                  gaai1RingSmEvent[ERPS_RING_RAPS_NRRB_EVENT]);
                return OSIX_FAILURE;
            }
            /* While processing clear request, if u1ProprietaryClearFS is enabled and 
             * also any one of the ring port is failed means, SEM will be  
             * triggered internally for ERPS_RING_LOCAL_SF_REQUEST and  
             * the u1HighestPriorityRequest will be updated properly.  
             * That should not be overwritten here. */
            if (!((pErpsContextInfo->u1ProprietaryClearFS == ERPS_SNMP_TRUE) &&
                  ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
                   (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))))
            {
                /* Remote request will not be stored as highest priority request */
                if (pRingInfo->u4RplIfIndex == 0)
                {
                    RingDynInfo.u1HighestPriorityRequest =
                        ERPS_RING_NO_ACTIVE_REQUEST;
                    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                               ERPS_RING_PRIO_REQUEST);
                }
            }
            break;

        default:

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmCalTopPriorityRequest invalid request."
                              "\r\n");
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainRingStatusIndication                     */
/*                                                                           */
/*    Description         : This function is called on reception of R-APS NR */
/*                          or local clear SF when there is a virtual channel*/
/*                          failure.                                         */
/*                          On reception of R-APS NR or LCSF , R-APS NR's are*/
/*                          propagated to the sub-rings in the sub-ring list */
/*                          of the main ring,which causes the RPL port  to be*/
/*                          be blocked in the sub-ring,thus avoiding the     */
/*                          super-loop.                                      */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       top priority request to be determi- */
/*                                       nd.                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         returned from state/event machine.*/
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete handling of input request*/
/*****************************************************************************/
PUBLIC INT4
ErpsSmMainRingStatusIndication (UINT4 u4ContextId, UINT4 u4RingId)
{
    tErpsRingInfo      *pSubRingInfo = NULL;

    /*Get the sub-ring info */
    pSubRingInfo = ErpsRingGetRingEntry (u4ContextId, u4RingId);

    if (pSubRingInfo != NULL)
    {
        /* If obtained ring is a sub-ring (RingPort2 == 0) 
         * and if there is an UP MEP failure */
        if ((pSubRingInfo->u4Port2IfIndex == 0)
            && (pSubRingInfo->u1Port2Status & ERPS_PORT_FAILED))
        {
            if (pSubRingInfo->u4RingNodeStatus & ERPS_PERIODIC_TIMER_RUNNING)
            {
                if (ErpsTmrStopTimer (pSubRingInfo,
                                      ERPS_PERIODIC_TMR) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (u4ContextId, u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmStopAllTimer failed to stop periodic timer."
                                      "\r\n");
                    return OSIX_FAILURE;
                }

                if (ErpsTmrStartTimer (pSubRingInfo,
                                       ERPS_PERIODIC_TMR,
                                       ERPS_VC_RECOVERY_PERIODIC_TIMER_INTERVAL
                                       (pSubRingInfo)) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (u4ContextId, u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmStopAllTimer failed to start periodic timer for the vc recovery periodic timer interval."
                                      "\r\n");
                    return OSIX_FAILURE;
                }
            }

            if (ErpsTxStopRapsMessages (pSubRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (u4ContextId, u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmMainRingStatusIndication failed to stop R-APS "
                                  "message transmission\r\n");
                return OSIX_FAILURE;
            }
            if (ErpsTxStartRapsMessages (pSubRingInfo,
                                         ERPS_STATUS_BIT_NONE,
                                         ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (u4ContextId, u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmMainRingStatusIndication failed to start  R-APS NR "
                                  "message transmission\r\n");
                return OSIX_FAILURE;
            }
        }                        /*Message Type == NR */

    }

    return OSIX_SUCCESS;
}
