/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erprbutl.c,v 1.1 2014/02/24 11:33:22 siva Exp $
 *
 * Description:This file contains the functions for RBTree utilities
 *             for ERPS used when enabling the ERPS_ARRAY_TO_RBTREE_WANTED flag
 *
 *****************************************************************************/
#include<erpsinc.h>

/*****************************************************************************
 *                  UTILITIES FOR SUBPORTLIST RBTREE
 ******************************************************************************/
/*****************************************************************************/
/* Function Name      : ErpsSubPortListTblCmp                                */
/*                                                                           */
/* Description        : This routine is used to Compare RBTree nodes of      */
/*                      SubPortList Table.                                   */
/*                                                                           */
/* Input(s)           : Two Subport entries to be compared                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
ErpsSubPortListTblCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = 0;
    UINT4               u4Index2 = 0;

    /* Interface Index is the primary index for the RBTree*/
    u4Index1 = ((tSubPortEntry *) pRBElem1)->u4Index;
    u4Index2 = ((tSubPortEntry *) pRBElem2)->u4Index;

    if (u4Index1 < u4Index2)
    {
        return (ERPS_RBTREE_KEY_LESSER);
    }
    if (u4Index1 > u4Index2)
    {
        return (ERPS_RBTREE_KEY_GREATER);
    }

    return (ERPS_RBTREE_KEY_EQUAL);
}

/*****************************************************************************/
/* Function Name      : ErpsDelAllSubPortEntries                             */
/*                                                                           */
/* Description        : This routine is used to Delete all the entries and   */
/*                      frees all the allocated memory for nodes             */
/*                                                                           */
/* Input(s)           : SubPortListTable - Table Name                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ErpsDelAllSubPortEntries (tRBTree SubPortListTable)
{
    tSubPortEntry  *pSubPortEntry = NULL;
    tSubPortEntry  *pNextEntry = NULL;
    
    pSubPortEntry = ErpsSubPortListTblGetNextEntry(SubPortListTable, 0);

    while (pSubPortEntry != NULL)
    {
        /* Delete all the nodes */
        pNextEntry = ErpsSubPortListTblGetNextEntry
            (SubPortListTable, pSubPortEntry->u4Index);

        ErpsDeleteSubPortEntry (SubPortListTable, pSubPortEntry);

        pSubPortEntry = pNextEntry;
    }
}

/*****************************************************************************/
/* Function Name      : ErpsSubPortListTblGetNextEntry                       */
/*                                                                           */
/* Description        : This routine is used to get the next Sub Port List   */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : SubPortListTable - Table Name                        */
/*                      u4Index  - Interface Index                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pSubPortEntry - Pointer to Sub Port List entry       */
/*****************************************************************************/
tSubPortEntry  *
ErpsSubPortListTblGetNextEntry (tRBTree SubPortListTable, UINT4 u4Index)
{
    tSubPortEntry   SubPortEntry;
    tSubPortEntry  *pSubPortEntry = NULL;

    MEMSET (&SubPortEntry, 0, sizeof (tSubPortEntry));

    SubPortEntry.u4Index = u4Index;

    pSubPortEntry = (tSubPortEntry *)
        RBTreeGetNext (SubPortListTable,
                       (tRBElem *) (&SubPortEntry), NULL);

    return pSubPortEntry;
}

/*****************************************************************************/
/* Function Name      : ErpsGetSubPortListEntry                              */
/*                                                                           */
/* Description        : This routine is used to get the Sub Port List Entry  */
/*                      for the given Index                                  */
/*                                                                           */
/* Input(s)           : SubPortListTable - Table Name                        */
/*                      u4Index  - Interface Index                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pSubPortEntry - Pointer to Sub Port List entry       */
/*****************************************************************************/
tSubPortEntry  *
ErpsGetSubPortListEntry (tRBTree SubPortListTable, UINT4 u4Index)
{
    tSubPortEntry   SubPortEntry;
    tSubPortEntry  *pSubPortEntry = NULL;

    MEMSET (&SubPortEntry, 0, sizeof (tSubPortEntry));

    SubPortEntry.u4Index = u4Index;

    pSubPortEntry = (tSubPortEntry *)
        RBTreeGet (SubPortListTable,(tRBElem *) (&SubPortEntry));

    return pSubPortEntry;
}

/*****************************************************************************/
/* Function Name      : ErpsDeleteSubPortEntry                               */
/*                                                                           */
/* Description        : This routine is used to Delete a Sub Port List Entry */
/*                                                                           */
/* Input(s)           : pSubPortEntry - Entry to be deleted                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
INT4
ErpsDeleteSubPortEntry (tRBTree SubPortListTable, tSubPortEntry * pSubPortEntry)
{
    if (pSubPortEntry != NULL)
    {
        if (RBTreeRemove (SubPortListTable,
                          (tRBElem *) pSubPortEntry) == RB_FAILURE)
        {
            return OSIX_FAILURE;
        }

        ERPS_RELEASE_SUB_PORT_ENTRIES_MEM_BLOCK (pSubPortEntry);
        pSubPortEntry = NULL;

        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}


/*****************************************************************************/
/* Function Name      : ErpsCreateSubPortListEntry                           */
/*                                                                           */
/* Description        : This routine is used to Create and add an Entry to   */
/*                      SubPortList Table                                    */
/*                                                                           */
/* Input(s)           : pSubPortEntry - Entry to be deleted                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
INT4
ErpsCreateSubPortListEntry (tRBTree SubPortListTable,UINT4 u4Index,
                            tSubPortEntry ** ppSubPortEntry)
{
    *ppSubPortEntry = NULL;

    if (((*ppSubPortEntry) = ErpsGetSubPortListEntry(SubPortListTable, u4Index))
        != NULL)
    {
        return OSIX_SUCCESS;
    }

    ERPS_ALLOC_SUB_PORT_ENTRIES_MEM_BLOCK(*ppSubPortEntry);

    if ((*ppSubPortEntry) == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (*ppSubPortEntry, 0, sizeof (tSubPortEntry));
    (*ppSubPortEntry)->u4Index = u4Index;

    if (RBTreeAdd (SubPortListTable,
                   (tRBElem *) ((*ppSubPortEntry))) == RB_FAILURE)
    {
        ERPS_RELEASE_SUB_PORT_ENTRIES_MEM_BLOCK(*ppSubPortEntry);
        (*ppSubPortEntry) = NULL;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : ErpsIsMemberPort                                    */
 /*                                                                           */
 /* Description         :  This function checks the particular SubPortProperty*/
 /*                        is set for the given Index                         */
 /*                        Result OSIX_TRUE/OSIX_FALSE is the output.         */
 /*                                                                           */
 /* Input(s)            : SubPortListTable - Table Name                       */
 /*                       u4Index  - Interface Index                          */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : pu1Result   - Pointer that holds the output         */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
ErpsIsMemberPort (tRBTree SubPortListTable, UINT4 u4Index, UINT2 u2Flag,UINT1 *pu1Result)
{
    tSubPortEntry  *pSubPortEntry = NULL;

    *pu1Result =  OSIX_FALSE;

    if ((pSubPortEntry = ErpsGetSubPortListEntry(SubPortListTable, u4Index))
        != NULL)
    {
        if ((pSubPortEntry->u2BitMask & u2Flag) == u2Flag)
        {
            *pu1Result = OSIX_TRUE;
        }
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : ErpsGetSubPortsFromDB                               */
 /*                                                                           */
 /* Description         : This function reads the Index and if the u2Flag is  */             
 /*                       set in the data base, then it will be set in the    */
 /*                       LocalPortList.                                      */
 /*                                                                           */
 /* Input(s)            : SubPortListTable - Table Name                       */
 /*                       u4Index  - Interface Index                          */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : SubPortList - LocalPortList                         */
 /*                                                                           */
 /* Returns             :  NONE.                                              */
 /*****************************************************************************/
VOID
ErpsGetSubPortsFromDB (tRBTree SubPortListTable, UINT2 u2Flag, tErpsSubPortList SubPortList)
{
    tSubPortEntry  *pSubPortEntry = NULL;
    UINT4           u4Index = 0;

    while ((pSubPortEntry = ErpsSubPortListTblGetNextEntry
            (SubPortListTable, u4Index)) != NULL)
    {
        if ((pSubPortEntry->u2BitMask & u2Flag) == u2Flag)
        {
            OSIX_BITLIST_SET_BIT(SubPortList, pSubPortEntry->u4Index,
                                    sizeof(tErpsSubPortList));
        }
        u4Index = pSubPortEntry->u4Index;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : ErpsAddSingleSubPortInDB                            */
 /*                                                                           */
 /* Description         : This function reads the Index and if the Index  is  */             
 /*                       Present it sets the flag, else it will cretae a     */
 /*                       Entry  and then set the flag.                       */
 /*                                                                           */
 /* Input(s)            : SubPortListTable - Table Name                       */
 /*                       u4Index  - Interface Index                          */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : None                                                */
 /*                                                                           */
 /* Returns             :  OSIX_FAILURE/OSIX_SUCCESS                          */
 /*****************************************************************************/
INT4
ErpsAddSingleSubPortInDB (tRBTree SubPortListTable, UINT4 u4Index,  UINT2 u2Flag)
{
    tSubPortEntry  *pSubPortEntry = NULL;

    if (ErpsCreateSubPortListEntry (SubPortListTable, u4Index, &pSubPortEntry)
        == OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }
    pSubPortEntry->u2BitMask |= u2Flag;
    return OSIX_SUCCESS;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : ErpsResetAllSubPortInDB                             */
 /*                                                                           */
 /* Description         : This function reset all the Portlist which is having*/
 /*                       the Flag Property and if no other property is defined*/
 /*                       then the Sub Port Entry is deleted.                  */
 /*                                                                           */
 /* Input(s)            : SubPortListTable - Table Name                       */
 /*                       u4Index  - Interface Index                          */
 /*                       u2Flag        - Port property flag                  */
 /*                                                                           */
 /* Output(s)           : None                                                */
 /*                                                                           */
 /* Returns             : OSIX_FAILURE/OSIX_SUCCESS                           */
 /*****************************************************************************/
VOID
ErpsResetAllSubPortInDB(tRBTree SubPortListTable, UINT2 u2Flag)
{
    tSubPortEntry  *pSubPortEntry = NULL;
    UINT4           u4Index = 0;

    while ((pSubPortEntry =  ErpsSubPortListTblGetNextEntry
            (SubPortListTable, u4Index)) != NULL)
    {
        if ((pSubPortEntry->u2BitMask & u2Flag) == u2Flag)
        {
            pSubPortEntry->u2BitMask =
                (UINT2) ((pSubPortEntry->u2BitMask) & (~u2Flag));
        }

        if (pSubPortEntry->u2BitMask == 0)
        {
            if (ErpsDeleteSubPortEntry (SubPortListTable, pSubPortEntry) == OSIX_FAILURE)
            {
                return;
            }
        }
        u4Index = pSubPortEntry->u4Index;
    }
    return;
}
