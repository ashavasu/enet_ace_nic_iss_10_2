/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fserpslw.c,v 1.98 2017/11/13 12:36:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "erpsinc.h"
/****************************************************************************
 *                           fserpslw.c prototypes                          *
 ****************************************************************************/
PRIVATE tErpsRingInfo *ErpsLwUtilValidateCxtAndRing (UINT4 u4ContextId,
                                                     UINT4 u4RingId,
                                                     UINT4 *pu4ErrorCode);
PRIVATE tErpsContextInfo *ErpsLwUtilValidateContextInfo (UINT4 u4ContextId,
                                                         UINT4 *pu4ErrorCode);
PRIVATE VOID        ErpsLwUtilRingCfmCfgNotInServ (tErpsRingInfo * pRingInfo);
PRIVATE VOID        ErpsLwUtilRingCfgNotInServ (tErpsRingInfo * pRingInfo);
PRIVATE INT4
 
 
 
 ErpsLwValidateCfmEntry (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 *pu4ErrorCode);

PRIVATE INT4
 
 
 
 ErpsLwValidateMplsEntry (UINT4 u4FsErpsContextId,
                          UINT4 u4FsErpsRingId, UINT4 *pu4ErrorCode);

/* LOW LEVEL Routines for Table : FsErpsContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsContextTable
 Input       :  The Indices
                FsErpsContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsContextTable (UINT4 u4FsErpsContextId)
{
    if (ErpsCxtGetContextEntry (u4FsErpsContextId) == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsContextTable
 Input       :  The Indices
                FsErpsContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsContextTable (UINT4 *pu4FsErpsContextId)
{
    UINT4               u4ContextId = 0;

    *pu4FsErpsContextId = 0;

    for (; u4ContextId < ERPS_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (ErpsCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4FsErpsContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsContextTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsContextTable (UINT4 u4FsErpsContextId,
                                   UINT4 *pu4NextFsErpsContextId)
{
    UINT4               u4ContextId = u4FsErpsContextId + 1;

    *pu4NextFsErpsContextId = 0;

    for (; u4ContextId < ERPS_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (ErpsCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4NextFsErpsContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtName
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtName (UINT4 u4FsErpsContextId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsErpsCtxtName)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo != NULL)
    {
        pRetValFsErpsCtxtName->i4_Length =
            sizeof (pContextInfo->au1ContextName);

        MEMCPY (pRetValFsErpsCtxtName->pu1_OctetList,
                pContextInfo->au1ContextName, pRetValFsErpsCtxtName->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtSystemControl
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtSystemControl (UINT4 u4FsErpsContextId,
                               INT4 *pi4RetValFsErpsCtxtSystemControl)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo != NULL)
    {
        *pi4RetValFsErpsCtxtSystemControl =
            gau1ErpsSysCtrlStatus[u4FsErpsContextId];

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtModuleStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtModuleStatus (UINT4 u4FsErpsContextId,
                              INT4 *pi4RetValFsErpsCtxtModuleStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo != NULL)
    {
        *pi4RetValFsErpsCtxtModuleStatus = pContextInfo->u1ModuleStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtTraceInput
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtTraceInput
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtTraceInput (UINT4 u4FsErpsContextId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsErpsCtxtTraceInput)
{
    tErpsContextInfo   *pContextInfo = NULL;
    tUtlValidTraces     UtlValidTraces;
    UINT2               u2TraceLen = 0;

    MEMSET (&UtlValidTraces, 0, sizeof (tUtlValidTraces));

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    UtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1ErpsTraceTypes;
    UtlValidTraces.u2MaxTrcTypes = (UINT2) ERPS_MAX_TRC_TYPE;

    UtlGetTraceOptionString (UtlValidTraces,
                             ERPS_TRACE_OPTION (u4FsErpsContextId),
                             pRetValFsErpsCtxtTraceInput->pu1_OctetList,
                             &u2TraceLen);

    pRetValFsErpsCtxtTraceInput->i4_Length = (INT4) u2TraceLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtTrapStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtTrapStatus (UINT4 u4FsErpsContextId,
                            INT4 *pi4RetValFsErpsCtxtTrapStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsErpsCtxtTrapStatus = pContextInfo->u1TrapStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtClearRingStats
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtClearRingStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtClearRingStats (UINT4 u4FsErpsContextId,
                                INT4 *pi4RetValFsErpsCtxtClearRingStats)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsCtxtClearRingStats = ERPS_SNMP_FALSE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtRowStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtRowStatus (UINT4 u4FsErpsContextId,
                           INT4 *pi4RetValFsErpsCtxtRowStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsCtxtRowStatus = pContextInfo->u1RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtVlanGroupManager
 Input       :  The Indices
                FsErpsContextId

                The Object 
                retValFsErpsCtxtVlanGroupManager
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtVlanGroupManager (UINT4 u4FsErpsContextId,
                                  INT4 *pi4RetValFsErpsCtxtVlanGroupManager)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsCtxtVlanGroupManager = pContextInfo->u1VlanGroupManager;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsCtxtProprietaryClearFS
 Input       :  The Indices
                FsErpsContextId

                The Object
                retValFsErpsCtxtProprietaryClearFS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsCtxtProprietaryClearFS (UINT4 u4FsErpsContextId,
                                    INT4 *pi4RetValFsErpsCtxtProprietaryClearFS)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsCtxtProprietaryClearFS = pContextInfo->u1ProprietaryClearFS;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtSystemControl
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtSystemControl (UINT4 u4FsErpsContextId,
                               INT4 i4SetValFsErpsCtxtSystemControl)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsErpsCtxtSystemControl ==
        gau1ErpsSysCtrlStatus[u4FsErpsContextId])
    {
        /* If the SystemControl Status present in the context is 
         * same as new SystemControl Status then return success */
        return SNMP_SUCCESS;
    }
    if (i4SetValFsErpsCtxtSystemControl == ERPS_SHUTDOWN)
    {
        /* While shutting the ERPS in a context will do the following 
         * - Delete all the ring configurations in the context
         * - Notify protocol shutdown status to the MSR */
        if (ErpsRingDeleteAllRingsInContext (u4FsErpsContextId) == OSIX_FAILURE)
        {
            /* Traces are present inside this function so no need to print 
             * here also */
            return SNMP_FAILURE;
        }
        ErpsPortNotifyProtocolShutStatus (u4FsErpsContextId);
        ERPS_CONTEXT_INIT_TRC (u4FsErpsContextId,
                               INIT_SHUT_TRC,
                               "ERPS system control is shutdown\n");
    }

    gau1ErpsSysCtrlStatus[u4FsErpsContextId] =
        (UINT1) i4SetValFsErpsCtxtSystemControl;

    if (i4SetValFsErpsCtxtSystemControl == ERPS_START)
    {
        ERPS_CONTEXT_INIT_TRC (u4FsErpsContextId,
                               INIT_SHUT_TRC, "ERPS system control is start\n");
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtModuleStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtModuleStatus (UINT4 u4FsErpsContextId,
                              INT4 i4SetValFsErpsCtxtModuleStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pContextInfo->u1ModuleStatus == i4SetValFsErpsCtxtModuleStatus)
    {
        /* If module is already enabled in that context then return 
         * success */
        return SNMP_SUCCESS;
    }

    pContextInfo->u1ModuleStatus = (UINT1) i4SetValFsErpsCtxtModuleStatus;

    if ((ErpsUtilGetMcLagStatus () == OSIX_SUCCESS) &&
        (i4SetValFsErpsCtxtModuleStatus == OSIX_ENABLED))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) ERPS_SYSLOG_ID,
                      "MC-LAG configurations shall be done prior to enabling "
                      "ERPS in MC-LAG aware system\r\n"));
    }

    if (i4SetValFsErpsCtxtModuleStatus == OSIX_ENABLED)
    {
        /* Inside this function loop all the Ring entries and do 
         * activate one by one */
        if (ErpsCxtEnableAllRings (u4FsErpsContextId) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        ERPS_CONTEXT_INIT_TRC (u4FsErpsContextId,
                               INIT_SHUT_TRC,
                               "ERPS module status is enabled\n");
    }
    else if (i4SetValFsErpsCtxtModuleStatus == OSIX_DISABLED)
    {
        /* Inside this function loop all the Ring entries and do 
         * de-activate one by one. */
        ErpsCxtDisableAllRings (u4FsErpsContextId);
        ERPS_CONTEXT_INIT_TRC (u4FsErpsContextId,
                               INIT_SHUT_TRC,
                               "ERPS module status is disabled\n");
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtTraceInput
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtTraceInput
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtTraceInput (UINT4 u4FsErpsContextId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsErpsCtxtTraceInput)
{
    tUtlValidTraces     UtlValidTraces;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;

    MEMSET (&UtlValidTraces, 0, sizeof (tUtlValidTraces));

    UtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1ErpsTraceTypes;
    UtlValidTraces.u2MaxTrcTypes = (UINT2) ERPS_MAX_TRC_TYPE;

    UtlGetTraceOptionValue (UtlValidTraces,
                            pSetValFsErpsCtxtTraceInput->pu1_OctetList,
                            (UINT2) pSetValFsErpsCtxtTraceInput->i4_Length,
                            &u4TrcOption, &u1TrcStatus);

    if (u1TrcStatus == (UINT1) UTL_TRACE_ENABLE)
    {
        ERPS_TRACE_OPTION (u4FsErpsContextId) |= u4TrcOption;
    }
    else
    {
        ERPS_TRACE_OPTION (u4FsErpsContextId) &= ~u4TrcOption;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtTrapStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtTrapStatus (UINT4 u4FsErpsContextId,
                            INT4 i4SetValFsErpsCtxtTrapStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextInfo->u1TrapStatus = (UINT1) i4SetValFsErpsCtxtTrapStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtClearRingStats
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtClearRingStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtClearRingStats (UINT4 u4FsErpsContextId,
                                INT4 i4SetValFsErpsCtxtClearRingStats)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsErpsCtxtClearRingStats == ERPS_SNMP_TRUE)
    {
        /* Clear statistics counters for all rings in this context */
        ErpsCxtClearAllRingStatsCounters (u4FsErpsContextId);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtRowStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtRowStatus (UINT4 u4FsErpsContextId,
                           INT4 i4SetValFsErpsCtxtRowStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    switch (i4SetValFsErpsCtxtRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values 
             * to the context are handled inside the below function and 
             * rowstatus is initialized here */
            pContextInfo = ErpsCxtMemInit (u4FsErpsContextId);

            if (pContextInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            pContextInfo->u1RowStatus = ACTIVE;

            break;

        case DESTROY:

            pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

            if (pContextInfo == NULL)
            {
                return SNMP_SUCCESS;
            }
            /* Delete the context entry */
            ErpsCxtHandleDeleteContxt (u4FsErpsContextId);
            /* Destroy the context entry is equal to the protocol system
             * control status is set to shutdown. So should indicate the
             * MSR aslo to delete the ring entries also */
            ErpsPortNotifyProtocolShutStatus ((INT4) u4FsErpsContextId);

            gau1ErpsSysCtrlStatus[u4FsErpsContextId] = ERPS_SHUTDOWN;

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtVlanGroupManager
 Input       :  The Indices
                FsErpsContextId

                The Object 
                setValFsErpsCtxtVlanGroupManager
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtVlanGroupManager (UINT4 u4FsErpsContextId,
                                  INT4 i4SetValFsErpsCtxtVlanGroupManager)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextInfo->u1VlanGroupManager =
        (UINT1) i4SetValFsErpsCtxtVlanGroupManager;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsCtxtProprietaryClearFS
 Input       :  The Indices
                FsErpsContextId

                The Object
                setValFsErpsCtxtProprietaryClearFS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsCtxtProprietaryClearFS (UINT4 u4FsErpsContextId,
                                    INT4 i4SetValFsErpsCtxtProprietaryClearFS)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextInfo->u1ProprietaryClearFS =
        (UINT1) i4SetValFsErpsCtxtProprietaryClearFS;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtSystemControl
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtSystemControl (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                  INT4 i4TestValFsErpsCtxtSystemControl)
{
    tErpsContextInfo   *pContextInfo = NULL;

    if ((i4TestValFsErpsCtxtSystemControl != ERPS_START) &&
        (i4TestValFsErpsCtxtSystemControl != ERPS_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (u4FsErpsContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        CLI_SET_ERR (CLI_ERPS_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtModuleStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtModuleStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                 INT4 i4TestValFsErpsCtxtModuleStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    if ((i4TestValFsErpsCtxtModuleStatus != OSIX_ENABLED) &&
        (i4TestValFsErpsCtxtModuleStatus != OSIX_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ErpsLwUtilValidateContextInfo (u4FsErpsContextId,
                                                  pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtTraceInput
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtTraceInput
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtTraceInput (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsErpsCtxtTraceInput)
{
    tUtlValidTraces     UtlValidTraces;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;

    MEMSET (&UtlValidTraces, 0, sizeof (tUtlValidTraces));

    if ((pTestValFsErpsCtxtTraceInput->i4_Length < ERPS_TRC_MIN_SIZE) ||
        (pTestValFsErpsCtxtTraceInput->i4_Length > ERPS_TRC_MAX_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (ErpsCxtGetContextEntry (u4FsErpsContextId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    UtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1ErpsTraceTypes;
    UtlValidTraces.u2MaxTrcTypes = (UINT2) ERPS_MAX_TRC_TYPE;

    UtlGetTraceOptionValue (UtlValidTraces,
                            pTestValFsErpsCtxtTraceInput->pu1_OctetList,
                            (UINT2) pTestValFsErpsCtxtTraceInput->i4_Length,
                            &u4TrcOption, &u1TrcStatus);

    if (!u4TrcOption)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtTrapStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtTrapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                               INT4 i4TestValFsErpsCtxtTrapStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    if ((i4TestValFsErpsCtxtTrapStatus != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsCtxtTrapStatus != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ErpsLwUtilValidateContextInfo (u4FsErpsContextId,
                                                  pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtClearRingStats
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtClearRingStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtClearRingStats (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                   INT4 i4TestValFsErpsCtxtClearRingStats)
{
    if ((i4TestValFsErpsCtxtClearRingStats != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsCtxtClearRingStats != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwUtilValidateContextInfo (u4FsErpsContextId, pu4ErrorCode) == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtRowStatus
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                              INT4 i4TestValFsErpsCtxtRowStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;

    switch (i4TestValFsErpsCtxtRowStatus)
    {
        case CREATE_AND_GO:

            if (ErpsPortVcmIsVcExist (u4FsErpsContextId) == VCM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

            /* If Context is present and trying to create the same entry then
             * return failure */
            if (pContextInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (MemGetFreeUnits (gErpsGlobalInfo.ContextPoolId) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtVlanGroupManager
 Input       :  The Indices
                FsErpsContextId

                The Object 
                testValFsErpsCtxtVlanGroupManager
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtVlanGroupManager (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     INT4 i4TestValFsErpsCtxtVlanGroupManager)
{
    tErpsContextInfo   *pContextInfo = NULL;

    if ((i4TestValFsErpsCtxtVlanGroupManager != ERPS_VLAN_GROUP_MANAGER_MSTP) &&
        (i4TestValFsErpsCtxtVlanGroupManager != ERPS_VLAN_GROUP_MANAGER_ERPS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ErpsLwUtilValidateContextInfo (u4FsErpsContextId,
                                                  pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsCtxtProprietaryClearFS
 Input       :  The Indices
                FsErpsContextId

                The Object
                testValFsErpsCtxtProprietaryClearFS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsCtxtProprietaryClearFS (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsErpsContextId,
                                       INT4 i4TestValFsErpsCtxtPropClearFS)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsLwUtilValidateContextInfo (u4FsErpsContextId,
                                                  pu4ErrorCode);

    if (pContextInfo == NULL)
    {
        CLI_SET_ERR (CLI_ERPS_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsCtxtPropClearFS != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsCtxtPropClearFS != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsContextTable
 Input       :  The Indices
                FsErpsContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsContextTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsVlanGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsVlanGroupTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsVlanGroupTable (UINT4 u4FsErpsContextId,
                                              INT4 i4FsErpsVlanId,
                                              INT4 i4FsErpsVlanGroupId)
{
    tErpsContextInfo   *pContextInfo = NULL;
    tVlanId             VlanId;
    UINT2               u2VlanGroupId = ERPS_DEFAULT_VLAN_GROUP_ID;

    VlanId = (tVlanId) i4FsErpsVlanId;

    /* Validate the Context Id */
    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Validate the Vlan Id Range */
    if ((i4FsErpsVlanId < 1) || (i4FsErpsVlanId > VLAN_DEV_MAX_VLAN_ID_EXT))
    {
        return SNMP_FAILURE;
    }

    /* Validate the Vlan Group Id Range */
    u2VlanGroupId = ErpsL2wrMiGetVlanInstMapping (u4FsErpsContextId, VlanId);

    if (u2VlanGroupId != (UINT2) i4FsErpsVlanGroupId)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsVlanGroupTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsVlanGroupTable (UINT4 *pu4FsErpsContextId,
                                      INT4 *pi4FsErpsVlanId,
                                      INT4 *pi4FsErpsVlanGroupId)
{
    return (nmhGetNextIndexFsErpsVlanGroupTable (0, pu4FsErpsContextId,
                                                 0, pi4FsErpsVlanId,
                                                 0, pi4FsErpsVlanGroupId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsVlanGroupTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsVlanId
                nextFsErpsVlanId
                FsErpsVlanGroupId
                nextFsErpsVlanGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsVlanGroupTable (UINT4 u4FsErpsContextId,
                                     UINT4 *pu4NextFsErpsContextId,
                                     INT4 i4FsErpsVlanId,
                                     INT4 *pi4NextFsErpsVlanId,
                                     INT4 i4FsErpsVlanGroupId,
                                     INT4 *pi4NextFsErpsVlanGroupId)
{
    tErpsContextInfo   *pContextInfo = NULL;
    UINT4               u4ContextId = u4FsErpsContextId;
    INT4                i4VlanId = i4FsErpsVlanId;
    INT4                i4NextVlanGroupId = 0;
    INT4                i4VlanGroupId = i4FsErpsVlanGroupId;
    UINT1               u1AvoidGroupIdCheck = OSIX_FALSE;

    /* loop over the first index = context id */
    while (u4ContextId <= ERPS_SIZING_CONTEXT_COUNT)
    {
        pContextInfo = ErpsCxtGetContextEntry (u4ContextId);

        if ((pContextInfo != NULL) &&
            (gau1ErpsSysCtrlStatus[u4ContextId] == ERPS_START))
        {
            /* loop over the second index = vlan id */
            while (i4VlanId <= VLAN_DEV_MAX_VLAN_ID_EXT)
                /* VLAN_DEV_MAX_VLAN_ID_EXT is used here for looping as
                 * vlan group table has to be walked for all VLANs
                 * for target enviornment supports. While Set routine 
                 * is restricted by VLAN_MAX_VLAN_ID_EXT macro */
            {
                if (i4VlanId == 0)
                {
                    i4VlanId++;
                    /* reset the vlan group id for next vlan */
                    i4VlanGroupId = 0;
                    u1AvoidGroupIdCheck = OSIX_TRUE;
                    continue;
                }

                i4NextVlanGroupId =
                    ErpsL2wrMiGetVlanInstMapping (u4ContextId, i4VlanId);

                /* Here i4NextVlanGroupId is the current group id, if the passed
                 * group id is less than current group id then return the
                 * values. */

                if (u1AvoidGroupIdCheck == OSIX_FALSE)
                {
                    if (i4VlanGroupId < i4NextVlanGroupId)
                    {
                        *pu4NextFsErpsContextId = u4ContextId;
                        *pi4NextFsErpsVlanId = i4VlanId;
                        *pi4NextFsErpsVlanGroupId = i4NextVlanGroupId;
                        return SNMP_SUCCESS;
                    }
                }
                else
                {
                    *pu4NextFsErpsContextId = u4ContextId;
                    *pi4NextFsErpsVlanId = i4VlanId;
                    *pi4NextFsErpsVlanGroupId = i4NextVlanGroupId;
                    return SNMP_SUCCESS;
                }
                /* considering the third index - vlan group id */
                /* go to the next entry and return the value */
                /* Check with the next vlan Id */

                i4VlanId++;
                /* reset the vlan group id for next vlan */
                i4VlanGroupId = 0;
                u1AvoidGroupIdCheck = OSIX_TRUE;
            }
        }
        /* need to go to the next context */
        u4ContextId++;
        /* reset the vlan id and vlan group id for the next context */
        i4VlanId = 0;
        i4VlanGroupId = 0;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsVlanGroupRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId

                The Object 
                retValFsErpsVlanGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsVlanGroupRowStatus (UINT4 u4FsErpsContextId, INT4 i4FsErpsVlanId,
                                INT4 i4FsErpsVlanGroupId,
                                INT4 *pi4RetValFsErpsVlanGroupRowStatus)
{
    /* Row Status of Vlan Group will always reaturn ACTIVE for all valid 
     * indices */
    /* Indices are already validated by Validate Routine */
    UNUSED_PARAM (u4FsErpsContextId);
    UNUSED_PARAM (i4FsErpsVlanGroupId);
    UNUSED_PARAM (i4FsErpsVlanId);

    *pi4RetValFsErpsVlanGroupRowStatus = ACTIVE;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsVlanGroupRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId

                The Object 
                setValFsErpsVlanGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsVlanGroupRowStatus (UINT4 u4FsErpsContextId, INT4 i4FsErpsVlanId,
                                INT4 i4FsErpsVlanGroupId,
                                INT4 i4SetValFsErpsVlanGroupRowStatus)
{
    tErpsContextInfo   *pContextInfo = NULL;
    UINT2               u2VlanGroupId = ERPS_DEFAULT_VLAN_GROUP_ID;

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);
    if (pContextInfo == NULL)
    {
        CLI_SET_ERR (CLI_ERPS_CONTEXT_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Do not allow this configuration if the vlan group manager 
     * for this context is not ERPS */
    if (pContextInfo->u1VlanGroupManager != ERPS_VLAN_GROUP_MANAGER_ERPS)
    {
        CLI_SET_ERR (CLI_ERPS_ERR_GROUP_MANAGER_ERPS);
        return SNMP_FAILURE;
    }

    /* Get the current vlan group entry */
    u2VlanGroupId = ErpsL2wrMiGetVlanInstMapping (u4FsErpsContextId,
                                                  i4FsErpsVlanId);

    switch (i4SetValFsErpsVlanGroupRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* create and go is allowed only when the vlan is mapped to 
             * default group i.e. group id 0 */
            if (u2VlanGroupId != ERPS_DEFAULT_VLAN_GROUP_ID)
            {
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_MAP);
                return SNMP_FAILURE;
            }

            if (ErpsUtilSetVlanGroupIdForVlan (u4FsErpsContextId,
                                               i4FsErpsVlanId,
                                               (UINT2) i4FsErpsVlanGroupId) ==
                OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_MAP);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            /* Destroy will remove the vlan from its current group
             * and add it to the default group (id 0) 
             */

            if (u2VlanGroupId == ERPS_DEFAULT_VLAN_GROUP_ID)
            {
                /* Vlan is already part of default group 
                 * so no need to take any separate action. */
                break;
            }

            if (ErpsUtilSetVlanGroupIdForVlan (u4FsErpsContextId,
                                               i4FsErpsVlanId,
                                               (UINT2)
                                               ERPS_DEFAULT_VLAN_GROUP_ID) ==
                OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_UN_MAP);
                return SNMP_FAILURE;
            }

            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsVlanGroupRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId

                The Object 
                testValFsErpsVlanGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsVlanGroupRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                   INT4 i4FsErpsVlanId,
                                   INT4 i4FsErpsVlanGroupId,
                                   INT4 i4TestValFsErpsVlanGroupRowStatus)
{

    /* Validate the i4FsErpsVlanGroupId */
    tStpInstanceInfo    InstInfo;
    tErpsRingInfo       RingInfo;
    /* Validate context Id */
    tErpsContextInfo   *pContextInfo = NULL;
    tErpsRingInfo      *pNextRingEntry = NULL;
    UINT2               u2VlanGroupId = ERPS_DEFAULT_VLAN_GROUP_ID;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));
    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Do not allow this configuration if the vlan group manager 
     * for this context is not ERPS */
    if (pContextInfo->u1VlanGroupManager != ERPS_VLAN_GROUP_MANAGER_ERPS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_GROUP_MANAGER_ERPS);
        return SNMP_FAILURE;
    }

    /* Validate Vlan Id */
    if (VLAN_IS_VLAN_ID_VALID (i4FsErpsVlanId) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_ID_INVALID);
        return SNMP_FAILURE;
    }

    /* Validate the Group Vlan Id */
    if ((i4FsErpsVlanGroupId < ERPS_MIN_VLAN_GROUP_ID) ||
        (i4FsErpsVlanGroupId > ERPS_MAX_VLAN_GROUP_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_INVALID);
        return SNMP_FAILURE;
    }
    if (ErpsPortGetVlanIdEntry (u4FsErpsContextId, (UINT2) i4FsErpsVlanId)
        == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_IS_NOT_CREATED);
        return SNMP_FAILURE;
    }

    /* Get the current vlan group entry */
    u2VlanGroupId = ErpsL2wrMiGetVlanInstMapping (u4FsErpsContextId,
                                                  i4FsErpsVlanId);

    /* Validate the Row status value */

    switch (i4TestValFsErpsVlanGroupRowStatus)
    {
        case CREATE_AND_GO:
            if (u2VlanGroupId != ERPS_DEFAULT_VLAN_GROUP_ID)
            {
                /* when one vlan needs to be mapped to a group
                 * current group id of that vlan should be 0 */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_MAP);
                return SNMP_FAILURE;
            }
            /* Here, Ring Structure is used to check the Row status
             * of ring before adding the vlan to vlan Group instance
             * to avoid the traffic flow/loop on blocked port.
             * Issue is observed, when the below actions are done.
             - Vlan Group Manager configured as ERPS in RPL owner
             - Created vlan geoup instance and associated
             RAPS Vlan and Data vlan
             - Observed loop and traffic flow on RPL port
             * Reason for the issue is port states were set to Forwarding
             * in hardware While creating vlan group instance.

             * To avoid this issue, Ring row status can be set as NOT IN SERVICE
             * before associating RAPS/data Vlan to Vlan Group Instance. */
            if (ErpsUtilValidateVlanGroup
                (u4FsErpsContextId,
                 (UINT2) i4FsErpsVlanGroupId) == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
                return SNMP_FAILURE;
            }

            RingInfo.u4ContextId = u4FsErpsContextId;
            RingInfo.u4RingId = 0;
            pNextRingEntry = ErpsRingGetNextNodeFromRingTable (&RingInfo);

            while (pNextRingEntry != NULL)
            {
                /* If the ring entry in the ring table doesnot belong to the incoming
                 * context then break from the loop */

                if (pNextRingEntry->u4ContextId != u4FsErpsContextId)
                {
                    break;
                }
                if ((pNextRingEntry->u1RowStatus == ACTIVE) &&
                    (pNextRingEntry->RapsVlanId == (UINT2) i4FsErpsVlanId))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    ERPS_CONTEXT_TRC (u4FsErpsContextId,
                                      pNextRingEntry->u4RingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Ring Group %d active.Make the row status "
                                      "to Not In Service to add the vlan to "
                                      "instance\n", pNextRingEntry->u4RingId);
                    CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
                    return SNMP_FAILURE;
                }

                RingInfo.u4ContextId = pNextRingEntry->u4ContextId;
                RingInfo.u4RingId = pNextRingEntry->u4RingId;
                pNextRingEntry = ErpsRingGetNextNodeFromRingTable (&RingInfo);
            }
            break;
        case DESTROY:
            if (u2VlanGroupId == ERPS_DEFAULT_VLAN_GROUP_ID)
            {
                /* vlan is already part of default group */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_UN_MAP);
                return SNMP_FAILURE;
            }

            /* Validate the vlan group before removing the vlan from 
             * vlan group. */

            InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_STATUS;
            if (L2IwfGetInstanceInfo
                (u4FsErpsContextId, (UINT2) i4FsErpsVlanGroupId,
                 &InstInfo) == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* Vlan group is not active then vlan removal should not be allowed */
            if (InstInfo.u1InstanceStatus == OSIX_FALSE)
            {
                /* Given VlanGroup Id is not present */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_NOT_PRESENT);
                return SNMP_FAILURE;

            }

            /* Vlan Group is active but the vlan given is not present in
             * that group means, vlan removal should not be allowed */

            if (u2VlanGroupId != (UINT2) i4FsErpsVlanGroupId)
            {
                /* Vlan Id is not present in the Vlan Group Id */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_NOT_PRESENT_IN_VLAN_GROUP);
                return SNMP_FAILURE;
            }

            /* Here, Ring Structure is used to validate the given Group id
             * before removing the vlan from that Group to avoid
             * the traffic flow on blocked port.
             * This issue is observed, when instances created by ERPS
             * are deleted but instance mapping to ring do not have any change 
             * and same instances are created by MSTP and initiated traffic.
             * If the Vlan Group Id is used by ERPS then deletion will not be
             * allowed. This instance can be deleted after making the ring
             * row status as NOT IN SERVICE*/
            if (ErpsUtilValidateVlanGroup
                (u4FsErpsContextId,
                 (UINT2) i4FsErpsVlanGroupId) == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
                return SNMP_FAILURE;
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsVlanGroupTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsVlanId
                FsErpsVlanGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsVlanGroupTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingTable (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingTable (UINT4 *pu4FsErpsContextId,
                                 UINT4 *pu4FsErpsRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    if (pRingInfo != NULL)
    {
        *pu4FsErpsContextId = pRingInfo->u4ContextId;
        *pu4FsErpsRingId = pRingInfo->u4RingId;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingTable (UINT4 u4FsErpsContextId,
                                UINT4 *pu4NextFsErpsContextId,
                                UINT4 u4FsErpsRingId,
                                UINT4 *pu4NextFsErpsRingId)
{

    tErpsRingInfo       RingConfigInfo;
    tErpsRingInfo      *pNextRingCfgInfo = NULL;

    MEMSET (&RingConfigInfo, 0, sizeof (tErpsRingInfo));

    RingConfigInfo.u4ContextId = u4FsErpsContextId;
    RingConfigInfo.u4RingId = u4FsErpsRingId;
    pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingConfigInfo);

    if (pNextRingCfgInfo != NULL)
    {
        *pu4NextFsErpsContextId = pNextRingCfgInfo->u4ContextId;
        *pu4NextFsErpsRingId = pNextRingCfgInfo->u4RingId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingVlanId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingVlanId (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        INT4 *pi4RetValFsErpsRingVlanId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingVlanId = (INT4) pRingInfo->RapsVlanId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingName
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingName (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsErpsRingName)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsErpsRingName->i4_Length = sizeof (pRingInfo->au1RingName);
    MEMCPY (pRetValFsErpsRingName->pu1_OctetList, pRingInfo->au1RingName,
            pRetValFsErpsRingName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 *pi4RetValFsErpsRingPort1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingPort1 = (INT4) pRingInfo->u4Port1IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 *pi4RetValFsErpsRingPort2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingPort2 = (INT4) pRingInfo->u4Port2IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRplPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRplPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRplPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         INT4 *pi4RetValFsErpsRingRplPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRplPort = (INT4) pRingInfo->u4RplIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPortBlockingOnVcRecovery
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPortBlockingOnVcRecovery
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPortBlockingOnVcRecovery (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          INT4
                                          *pi4RetValFsErpsRingPortBlockingOnVcRecovery)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingPortBlockingOnVcRecovery =
        pRingInfo->u1PortBlockedOnVcRecovery;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingNodeType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingNodeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingNodeType (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                          INT4 *pi4RetValFsErpsRingNodeType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u4RplIfIndex == 0)
    {
        /* If RplPort is zero then nodetype is non-RplOwner */
        *pi4RetValFsErpsRingNodeType = ERPS_NON_RPL_OWNER;
    }
    else
    {
        /* If RplPort is non zero then nodetype is RplOwner */
        *pi4RetValFsErpsRingNodeType = ERPS_RPL_OWNER;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingOperatingMode
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingOperatingMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingOperatingMode (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               INT4 *pi4RetValFsErpsRingOperatingMode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingOperatingMode = (INT4) pRingInfo->u1OperatingMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingMonitorMechanism
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingMonitorMechanism
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingMonitorMechanism (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 *pi4RetValFsErpsRingMonitorMechanism)
{
    tErpsRingInfo      *pRingInfo = NULL;
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingMonitorMechanism = (INT4) pRingInfo->u1MonitoringType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1Status
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1Status
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1Status (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 *pi4RetValFsErpsRingPort1Status)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)
    {
        *pi4RetValFsErpsRingPort1Status = ERPS_SNMP_PORT_BLOCK;
    }
    else
    {
        *pi4RetValFsErpsRingPort1Status = ERPS_SNMP_PORT_UNBLOCK;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2Status
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2Status
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2Status (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 *pi4RetValFsErpsRingPort2Status)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)
    {
        *pi4RetValFsErpsRingPort2Status = ERPS_SNMP_PORT_BLOCK;
    }
    else
    {
        *pi4RetValFsErpsRingPort2Status = ERPS_SNMP_PORT_UNBLOCK;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingSemState
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingSemState (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                          INT4 *pi4RetValFsErpsRingSemState)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingSemState = (INT4) pRingInfo->u1RingState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingNodeStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingNodeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingNodeStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                            INT4 *pi4RetValFsErpsRingNodeStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4RingNodeStatus = 0;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u4RingNodeStatus = pRingInfo->u4RingNodeStatus;

    /* Mib object does not contain Periodic Timer */
    u4RingNodeStatus &= ~(ERPS_PERIODIC_TIMER_RUNNING);

    *pi4RetValFsErpsRingNodeStatus = u4RingNodeStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                           INT4 *pi4RetValFsErpsRingRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRowStatus = pRingInfo->u1RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingMacId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingMacId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingMacId (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 *pi4RetValFsErpsRingMacId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingMacId = (INT4) pRingInfo->u1RingMacId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingProtectedVlanGroupId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingProtectedVlanGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingProtectedVlanGroupId (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingProtectedVlanGroupId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingProtectedVlanGroupId =
        (INT4) pRingInfo->u2ProtectedVlanGroupId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingProtectionType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingProtectionType (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                INT4 *pi4RetValFsErpsRingProtectionType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingProtectionType = (INT4) pRingInfo->u1ProtectionType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRAPSCompatibleVersion
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRAPSCompatibleVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRAPSCompatibleVersion (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       INT4
                                       *pi4RetValFsErpsRingRAPSCompatibleVersion)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRAPSCompatibleVersion =
        (INT4) pRingInfo->u1RAPSCompatibleVersion;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRplNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRplNeighbourPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRplNeighbourPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 *pi4RetValFsErpsRingRplNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRplNeighbourPort =
        (INT4) pRingInfo->u4RplNeighbourIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRAPSSubRingWithoutVC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRAPSSubRingWithoutVC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRAPSSubRingWithoutVC (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingRAPSSubRingWithoutVC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRAPSSubRingWithoutVC =
        (INT4) pRingInfo->u1RAPSSubRingWithoutVirtualChannel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRplNextNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRplNextNeighbourPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRplNextNeighbourPort (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingRplNextNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingRplNextNeighbourPort =
        (INT4) pRingInfo->u4RplNextNeighbourIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1NodeID
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1NodeID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1NodeID (UINT4 u4FsErpsContextId,
                             UINT4 u4FsErpsRingId,
                             tMacAddr * pRetValFsErpsRingPort1NodeID)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRetValFsErpsRingPort1NodeID, 0, sizeof (tMacAddr));
    MEMCPY (pRetValFsErpsRingPort1NodeID,
            &(pRingInfo->Port1StoredNodeIdBPRPair.NodeId), sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsErpsRingPort2NodeID
Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2NodeID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2NodeID (UINT4 u4FsErpsContextId,
                             UINT4 u4FsErpsRingId,
                             tMacAddr * pRetValFsErpsRingPort2NodeID)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRetValFsErpsRingPort2NodeID, 0, sizeof (tMacAddr));
    MEMCPY (pRetValFsErpsRingPort2NodeID,
            &(pRingInfo->Port2StoredNodeIdBPRPair.NodeId), sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1BPRBitVal
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1BPRBitVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1BPRBitVal (UINT4 u4FsErpsContextId,
                                UINT4 u4FsErpsRingId,
                                INT4 *pi4RetValFsErpsRingPort1BPRBitVal)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingPort1BPRBitVal =
        (INT4) pRingInfo->Port1StoredNodeIdBPRPair.u1BPRStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2BPRBitVal
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2BPRBitVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2BPRBitVal (UINT4 u4FsErpsContextId,
                                UINT4 u4FsErpsRingId,
                                INT4 *pi4RetValFsErpsRingPort2BPRBitVal)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingPort2BPRBitVal =
        (INT4) pRingInfo->Port2StoredNodeIdBPRPair.u1BPRStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingProtectedVlanGroupList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingProtectedVlanGroupList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingProtectedVlanGroupList (UINT4 u4FsErpsContextId,
                                        UINT4 u4FsErpsRingId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValFsErpsRingProtectedVlanGroupList)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    /*When Vlan Group List is populated, u2VlanGroupListCount will be updated 
     * So pu1_OctetList and i4_Length of pRetValFsErpsRingProtectedVlanGroupList are 
     * populated only if the count is not equal to zero*/

    MEMSET (pRetValFsErpsRingProtectedVlanGroupList->pu1_OctetList, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    if (pRingInfo->u2VlanGroupListCount != 0)
    {

        MEMCPY (pRetValFsErpsRingProtectedVlanGroupList->pu1_OctetList,
                pRingInfo->pau1RingVlanGroupList, ERPS_MAX_VLAN_GROUP_LIST);

        pRetValFsErpsRingProtectedVlanGroupList->i4_Length =
            ERPS_MAX_VLAN_GROUP_LIST;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingVlanId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingVlanId (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        INT4 i4SetValFsErpsRingVlanId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->RapsVlanId = (UINT2) i4SetValFsErpsRingVlanId;

    /* This function is to called to make the rowstatus of Ring Table 
     * from NotReady to NotInService */
    ErpsLwUtilRingCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingName
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingName (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsErpsRingName)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRingInfo->au1RingName, 0, ERPS_MAX_NAME_LENGTH);

    MEMCPY (pRingInfo->au1RingName, pSetValFsErpsRingName->pu1_OctetList,
            pSetValFsErpsRingName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPort1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPort1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPort1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 i4SetValFsErpsRingPort1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4Port1IfIndex = (UINT4) i4SetValFsErpsRingPort1;

    /* This function is to called to make the rowstatus of Ring Table 
     * from NotReady to NotInService */
    ErpsLwUtilRingCfgNotInServ (pRingInfo);

    /* When sub-ring is configured without virtual channel,in interconnection
     * node, the Sub-ring R-APS packet should be tx only to the Ring port 1
     * as per the standard (G.8032, 2010). But it is transmitted in both 
     * the ring ports (Ringport1 of Sub-ring and considering RingPort2 IfIndex
     * as 0).User can configure port1 and port2 after enabling without virtual 
     * channel so below check is included in set routines of port1 and port2 */

    if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
    {
        if ((pRingInfo->u4Port1IfIndex != 0)
            && (pRingInfo->u4Port2IfIndex != 0))
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
        }
        else if (pRingInfo->u4Port1IfIndex != 0)
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPort2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPort2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPort2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 i4SetValFsErpsRingPort2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4Port2IfIndex = (UINT4) i4SetValFsErpsRingPort2;
    if (!((pRingInfo->u4SwitchCmdIfIndex == pRingInfo->u4Port1IfIndex) ||
          (pRingInfo->u4SwitchCmdIfIndex == pRingInfo->u4Port2IfIndex)))
    {
        pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
    }

    if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
    {
        if ((pRingInfo->u4Port1IfIndex != 0)
            && (pRingInfo->u4Port2IfIndex != 0))
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
        }
        else if (pRingInfo->u4Port2IfIndex != 0)
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRplPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRplPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRplPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         INT4 i4SetValFsErpsRingRplPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4RplIfIndex = (UINT4) i4SetValFsErpsRingRplPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPortBlockingOnVcRecovery
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPortBlockingOnVcRecovery
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPortBlockingOnVcRecovery (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          INT4
                                          i4SetValFsErpsRingPortBlockingOnVcRecovery)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1PortBlockedOnVcRecovery =
        (UINT1) i4SetValFsErpsRingPortBlockingOnVcRecovery;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingOperatingMode
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingOperatingMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingOperatingMode (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               INT4 i4SetValFsErpsRingOperatingMode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1OperatingMode = (UINT1) i4SetValFsErpsRingOperatingMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingMonitorMechanism
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                setValFsErpsRingMonitorMechanism
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingMonitorMechanism (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 i4SetValFsErpsRingMonitorMechanism)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1MonitoringType = (UINT1) i4SetValFsErpsRingMonitorMechanism;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                           INT4 i4SetValFsErpsRingRowStatus)
{
    tErpsRingInfo       RingInfo;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pNextRingEntry = NULL;
    tErpsContextInfo   *pContextInfo = NULL;
    INT4                i4ErpsRingVlanId = 0;
    UINT4               u4Port1IfIndex = 0;
    UINT4               u4Port2IfIndex = 0;
    INT4                i4PrvRowStatus = 0;
    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    pContextInfo = ErpsCxtGetContextEntry (u4FsErpsContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsErpsRingRowStatus)
    {
        case CREATE_AND_WAIT:

            pRingInfo = ErpsRingCreateRingNode ();

            if (pRingInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            /* Initialize all the default values */
            pRingInfo->u4RingId = u4FsErpsRingId;
            pRingInfo->u1RingMacId = 1;
            pRingInfo->u4ContextId = u4FsErpsContextId;
            pRingInfo->u1ProtectionType = ERPS_PORT_BASED_PROTECTION;
            pRingInfo->u1ServiceType = ERPS_SERVICE_VLAN;

            SPRINTF ((CHR1 *) pRingInfo->au1RingName, "Ring%u", u4FsErpsRingId);

            if (ErpsRingAddNodeToRingRBTree (pRingInfo) == OSIX_FAILURE)
            {
                /* Release memory allocated for RING Node */
                MemReleaseMemBlock (gErpsGlobalInfo.VlanGroupListPoolId,
                                    (UINT1 *) pRingInfo->pau1RingVlanGroupList);
                if (pRingInfo->pErpsLspPwInfo != NULL)
                {
                    /* RBTree Deletion for SubPortList Table */
                    ERPS_DELETE_SUBPORTLIST_TABLE (pRingInfo->pErpsLspPwInfo);
                    MemReleaseMemBlock (gErpsGlobalInfo.LspPwInfoListId,
                                        (UINT1 *) pRingInfo->pErpsLspPwInfo);
                }
                MemReleaseMemBlock (gErpsGlobalInfo.RingInfoPoolId,
                                    (UINT1 *) pRingInfo);

                return SNMP_FAILURE;
            }

            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set */
            pRingInfo->u1RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:

            RingInfo.u4ContextId = u4FsErpsContextId;
            RingInfo.u4RingId = u4FsErpsRingId;

            pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

            if (pRingInfo == NULL)
            {
                ERPS_GLOBAL_TRC ("Ring Group entry is not created\r\n");
                return SNMP_FAILURE;
            }
            if (pRingInfo->u1RowStatus == NOT_IN_SERVICE)
            {
                if (ErpsUtilCheckIfIcclInterface
                    ((UINT4) pRingInfo->u4Port1IfIndex) != OSIX_SUCCESS)
                {
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                          ERPS_PORT_BLOCKING);
                }

                if (ErpsUtilCheckIfIcclInterface
                    ((UINT4) pRingInfo->u4Port2IfIndex) != OSIX_SUCCESS)
                {
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                          ERPS_PORT_BLOCKING);
                }
                return SNMP_SUCCESS;
            }

            if (ErpsUtilIsErpsEnabled (u4FsErpsContextId) == OSIX_TRUE)
            {
                ErpsRingDeActivateRing (pRingInfo, NOT_IN_SERVICE);

                if (ErpsRingRemNodeFromPortVlanTable (pRingInfo) ==
                    OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "nmhSetFsErpsRingRowStatus ring "
                                      "failed to remove node from VlanTable\r\n");
                    return SNMP_FAILURE;
                }

                if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
                {
                    if (ErpsRingRemNodeFromMepRingIdTable (pRingInfo) ==
                        OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          MGMT_TRC | ALL_FAILURE_TRC,
                                          "nmhSetFsErpsRingRowStatus ring "
                                          "failed to remove node from MepRingIdTable\r\n");
                        return SNMP_FAILURE;
                    }
                }

            }

            pRingInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            RingInfo.u4ContextId = u4FsErpsContextId;
            RingInfo.u4RingId = u4FsErpsRingId;

            pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

            if (pRingInfo == NULL)
            {
                ERPS_GLOBAL_TRC ("Ring Group entry is not created\r\n");
                return SNMP_SUCCESS;
            }

            /* Timer values also needs to be deleted while issuing clear config */
            if (ErpsUtilIsErpsEnabled (u4FsErpsContextId) == OSIX_TRUE)
            {
                ErpsRingDeActivateRing (pRingInfo, DESTROY);
            }

            /* Ring need not be deactivated, in case of DESTROY,
             * Reason : To delete the ring, CFM entries have to be 
             * deleted first. To delete the CFM entries RIng has to be 
             * made to NOT_IN_SERVICE which takes care of deactivating
             * the ring */

            ErpsRingDelNodeFromRBTree (pRingInfo);
            break;

        case ACTIVE:

            RingInfo.u4ContextId = u4FsErpsContextId;
            RingInfo.u4RingId = u4FsErpsRingId;

            pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

            if (pRingInfo == NULL)
            {
                ERPS_GLOBAL_TRC ("Ring Group entry is not created\r\n");
                return SNMP_FAILURE;
            }

            if (pRingInfo->u1RowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            i4ErpsRingVlanId = pRingInfo->RapsVlanId;
            u4Port1IfIndex = pRingInfo->u4Port1IfIndex;
            u4Port2IfIndex = pRingInfo->u4Port2IfIndex;

            i4PrvRowStatus = pRingInfo->u1RowStatus;
            /* Ring has to be activated, only when the ERPS module is
             * enabled on the context */
            pRingInfo->u1RowStatus = ACTIVE;

            if (ErpsUtilIsErpsEnabled (u4FsErpsContextId) == OSIX_TRUE)
            {
                if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
                {

                    if (ErpsRingAddNodeToMepRingIdRBTree (pRingInfo) ==
                        OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                          MGMT_TRC | ALL_FAILURE_TRC,
                                          "Ring node addition to MepRingId table failed\r\n");
                        pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus;
                        return SNMP_FAILURE;
                    }
                }

                /*Different ERPS Ring Instances can share the same RAPS-VLAN 
                 * those ring instances  resides in  different physical ports 
                 * in both Port-Based & Service Based Protection */
                if (pRingInfo->u1ProtectionType ==
                    ERPS_SERVICE_BASED_PROTECTION)
                {
                    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));
                    RingInfo.u4ContextId = u4FsErpsContextId;
                    RingInfo.u4RingId = 0;
                    pNextRingEntry = &RingInfo;
                    while ((pNextRingEntry =
                            ErpsRingGetNextNodeFromRingTable (pNextRingEntry))
                           != NULL)
                    {
                        if (pNextRingEntry->u4ContextId != u4FsErpsContextId)
                        {
                            break;
                        }
                        if (pNextRingEntry->u4RingId == u4FsErpsRingId)
                        {
                            continue;
                        }
                        if (pNextRingEntry->RapsVlanId != i4ErpsRingVlanId)
                        {
                            continue;
                        }
                        if ((pNextRingEntry->u4Port1IfIndex == u4Port1IfIndex)
                            || (pNextRingEntry->u4Port2IfIndex ==
                                u4Port1IfIndex))
                        {
                            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                              MGMT_TRC | ALL_FAILURE_TRC,
                                              "Ring vlan Id can't"
                                              "be configured when RingPort1 is member of "
                                              "another Ring.\n");
                            CLI_SET_ERR (CLI_ERPS_ERR_RING_RAPS_VLAN);
                            pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus;
                            return SNMP_FAILURE;
                        }

                        /* Interconnected node of the subring will always have
                         * a second port index as Zero. issues are observed,
                         * when main ring and Sub-Ring are configured with different
                         * physical ports on a single context.
                         * Two interconnected nodes are configured for the Sub-Ring
                         * on the same context,same RAPS Vlan with different Ring Id
                         * Below check will fail for sub-ring by comparing the Port2Index
                         * of first interconnect node with second interconnected node
                         * and ring will not activated. To avoid this kind of scenario's
                         * the below check is added */

                        if ((pNextRingEntry->u4Port1IfIndex == u4Port2IfIndex)
                            || ((u4Port2IfIndex != 0)
                                && (pNextRingEntry->u4Port2IfIndex ==
                                    u4Port2IfIndex)))
                        {
                            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                              MGMT_TRC | ALL_FAILURE_TRC,
                                              "Ring vlan Id can't"
                                              "be configured when RingPort2 is member of "
                                              "another Ring.\n");
                            CLI_SET_ERR (CLI_ERPS_ERR_RING_RAPS_VLAN);
                            pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus;
                            return SNMP_FAILURE;
                        }
                    }
                }

                if (ErpsRingActivateRing (pRingInfo) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Unable to activate Ring Group entry\n");
                    CLI_SET_ERR (CLI_ERPS_ERR_RING_GROUP_UNABLE_ACTIVE);
                    pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus;
                    return SNMP_FAILURE;
                }
                if (ErpsRingAddNodeToPortVlanRBTree (pRingInfo) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Ring node addition to Vlan table fail\r\n");
                    ErpsRingDeActivateRing (pRingInfo, DESTROY);
                    pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus;
                    return SNMP_FAILURE;
                }
                if ((pRingInfo->u4PeriodicTimerValue != ERPS_INIT_VAL) &&
                    (pRingInfo->bDFOP_TO == OSIX_ENABLED))
                {
                    if (!((pRingInfo->u4RplIfIndex != 0) &&
                          (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)))
                    {
                        if (ErpsTmrStartTimer
                            (pRingInfo, ERPS_TFOP_TMR,
                             ((pRingInfo->f4KValue) *
                              (pRingInfo->u4PeriodicTimerValue))) ==
                            OSIX_FAILURE)
                        {
                            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                              MGMT_TRC | ALL_FAILURE_TRC,
                                              "ErpsTmrStartTimer returned failure "
                                              "from starting TFOP timer\r\n");
                            /*pRingInfo->u1RowStatus = (UINT1) i4PrvRowStatus; */
                            /*  return SNMP_FAILURE; */
                        }
                    }
                }

            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingMacId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingMacId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingMacId (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                       INT4 i4SetValFsErpsRingMacId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1RingMacId = (UINT1) i4SetValFsErpsRingMacId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingProtectedVlanGroupId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingProtectedVlanGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingProtectedVlanGroupId (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingProtectedVlanGroupId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT2               u2Index = 1;
    UINT1               bResult = 0;
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRingInfo->u2ProtectedVlanGroupId != 0)
    {
        /* If the vlan group id is already set,
         * Remove  the vlan group from the vlan group list */
        OSIX_BITLIST_RESET_BIT ((*(pRingInfo->pau1RingVlanGroupList)),
                                pRingInfo->u2ProtectedVlanGroupId,
                                sizeof (tErpsVlanGroupList));
    }

    pRingInfo->u2ProtectedVlanGroupId =
        (UINT2) i4SetValFsErpsRingProtectedVlanGroupId;

    /* Adding the vlan group id to vlan group list */
    OSIX_BITLIST_SET_BIT ((*(pRingInfo->pau1RingVlanGroupList)),
                          pRingInfo->u2ProtectedVlanGroupId,
                          sizeof (tErpsVlanGroupList));
    for (; u2Index <= ERPS_MAX_VLAN_GROUP_LIST * 8; u2Index++)
    {
        /* Scanning the vlan group list bitmap to identify the vlan group id's mapped to
         * the ring */
        OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)), u2Index,
                                 sizeof (tErpsVlanGroupList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue */
            continue;
        }
        /* If the bit is set, print the corresponding vlan group id */
        pRingInfo->u2VlanGroupListCount = u2Index;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingProtectionType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingProtectionType (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                INT4 i4SetValFsErpsRingProtectionType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1ProtectionType = (UINT1) i4SetValFsErpsRingProtectionType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRAPSCompatibleVersion
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRAPSCompatibleVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRAPSCompatibleVersion (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       INT4
                                       i4SetValFsErpsRingRAPSCompatibleVersion)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1RAPSCompatibleVersion =
        (UINT1) i4SetValFsErpsRingRAPSCompatibleVersion;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRplNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRplNeighbourPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRplNeighbourPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 i4SetValFsErpsRingRplNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4RplNeighbourIfIndex =
        (UINT4) i4SetValFsErpsRingRplNeighbourPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRAPSSubRingWithoutVC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRAPSSubRingWithoutVC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRAPSSubRingWithoutVC (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingRAPSSubRingWithoutVC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1RAPSSubRingWithoutVirtualChannel =
        (UINT1) i4SetValFsErpsRingRAPSSubRingWithoutVC;

    /* When sub-ring is configured without virtual channel,in interconnection
     * node, the Sub-ring R-APS packet should be tx only to the Ring port 1
     * as per the standard (G.8032, 2010). But it is transmitted in both
     * the ring ports (Ringport1 of Sub-ring and considering RingPort2 IfIndex
     * as 0). */

    if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
    {
        if ((pRingInfo->u4Port1IfIndex != 0)
            && (pRingInfo->u4Port2IfIndex != 0))
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
        }
        else if (pRingInfo->u4Port1IfIndex != 0)
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;
        }
        else if (pRingInfo->u4Port2IfIndex != 0)
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingRplNextNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingRplNextNeighbourPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingRplNextNeighbourPort (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingRplNextNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4RplNextNeighbourIfIndex =
        (UINT4) i4SetValFsErpsRingRplNextNeighbourPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingProtectedVlanGroupList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                setValFsErpsRingProtectedVlanGroupList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingProtectedVlanGroupList (UINT4 u4FsErpsContextId,
                                        UINT4 u4FsErpsRingId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValFsErpsRingProtectedVlanGroupList)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT2               u2Index = 1;
    UINT1               bResult = 0;
    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRingInfo->pau1RingVlanGroupList,
            pSetValFsErpsRingProtectedVlanGroupList->pu1_OctetList,
            pSetValFsErpsRingProtectedVlanGroupList->i4_Length);

    /* The above operation resets the vlan group list. So, again adding
       the vlan group id to vlan group list */
    if (pRingInfo->u2ProtectedVlanGroupId != 0)
    {
        OSIX_BITLIST_SET_BIT ((*(pRingInfo->pau1RingVlanGroupList)),
                              pRingInfo->u2ProtectedVlanGroupId,
                              sizeof (tErpsVlanGroupList));
    }
    for (; u2Index <= ERPS_MAX_VLAN_GROUP_LIST * 8; u2Index++)
    {
        /* Scanning the vlan group list bitmap to identify the vlan group id's mapped to
         * the ring */
        OSIX_BITLIST_IS_BIT_SET ((*(pRingInfo->pau1RingVlanGroupList)), u2Index,
                                 sizeof (tErpsVlanGroupList), bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue */
            continue;
        }
        /* If the bit is set, print the corresponding vlan group id */
        pRingInfo->u2VlanGroupListCount = u2Index;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingVlanId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingVlanId (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                           UINT4 u4FsErpsRingId, INT4 i4TestValFsErpsRingVlanId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       NextRingEntry;

    if (VLAN_IS_VLAN_ID_VALID (i4TestValFsErpsRingVlanId) != VLAN_TRUE)
    {
        ERPS_GLOBAL_TRC ("Ring vlan id is not valid\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC, "Ring vlan Id can't"
                          "be configured when ring group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    if (ErpsPortGetVlanIdEntry
        (u4FsErpsContextId, (UINT2) i4TestValFsErpsRingVlanId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_IS_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    MEMSET (&NextRingEntry, 0, sizeof (tErpsRingInfo));
    NextRingEntry.u4ContextId = u4FsErpsContextId;
    NextRingEntry.u4RingId = u4FsErpsRingId;
    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingName
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingName (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsErpsRingName)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((pTestValFsErpsRingName->i4_Length <= 0) ||
        (pTestValFsErpsRingName->i4_Length >= ERPS_MAX_NAME_LENGTH))
    {
        ERPS_GLOBAL_TRC ("Invalid length ring name.\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Here, Ring Active check is not required while configuring ring name 
     * because ring name will not effect the ring behaviour/functionality 
     * so, it can be configured irrespective of the ring state. */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPort1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPort1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPort1 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                          UINT4 u4FsErpsRingId, INT4 i4TestValFsErpsRingPort1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pRingInfo->u4Port2IfIndex == (UINT4) i4TestValFsErpsRingPort1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Both the ring ports can't be same\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_SAME_RING_PORT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingPort1 < 0) ||
        (i4TestValFsErpsRingPort1 > ERPS_SYS_DEF_MAX_INTERFACES))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Invalid RingPort1 configured\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsUtilCheckIfMcLagInterface ((UINT4) i4TestValFsErpsRingPort1) ==
        OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "MC-LAG interface can not be added as a ring port\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MCLAG_PORT);
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Port cannot be configured when "
                          "ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    /* While modifying the ring ports say port1 as port3, 
     * Ring row status will be in NOT_IN_SERVICE and 
     * Ring port state will be in discarding state.
     * To make the default state and default port owner, 
     * port1 will be set as forwarding owner 
     * will be set as STP MODULE.*/
    if (pRingInfo->u1RowStatus == NOT_IN_SERVICE)
    {

        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                              ERPS_PORT_UNBLOCKING);

        ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port1IfIndex,
                                            ERPS_MODULE, L2IWF_RESET);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPort2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPort2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPort2 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                          UINT4 u4FsErpsRingId, INT4 i4TestValFsErpsRingPort2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (i4TestValFsErpsRingPort2 != 0)
    {
        if (pRingInfo->u4Port1IfIndex == (UINT4) i4TestValFsErpsRingPort2)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Both the ring ports can't be same\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_SAME_RING_PORT);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsErpsRingPort2 < 0) ||
        (i4TestValFsErpsRingPort2 > ERPS_SYS_DEF_MAX_INTERFACES))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Invalid RingPort2 configured\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    }

    if (ErpsUtilCheckIfMcLagInterface ((UINT4) i4TestValFsErpsRingPort2) ==
        OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "MC-LAG interface cannot be added as a ring port\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MCLAG_PORT);
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC, "Ring Port can't"
                          "be configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    /* While modifying the ring ports say port1 as port3, 
     * Ring row status will be in NOT_IN_SERVICE and 
     * Ring port state will be in discarding state.
     * To make the default state and default port owner, 
     * port1 will be set as forwarding owner 
     * will be set as STP MODULE.*/
    if (pRingInfo->u1RowStatus == NOT_IN_SERVICE)
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                              ERPS_PORT_UNBLOCKING);
        ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port2IfIndex,
                                            ERPS_MODULE, L2IWF_RESET);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRplPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRplPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRplPort (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                            UINT4 u4FsErpsRingId,
                            INT4 i4TestValFsErpsRingRplPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (i4TestValFsErpsRingRplPort != 0)
    {
        /* RPL port should be any one of the ring ports that configured 
         * for that RING */
        if ((i4TestValFsErpsRingRplPort != (INT4) pRingInfo->u4Port1IfIndex) &&
            (i4TestValFsErpsRingRplPort != (INT4) pRingInfo->u4Port2IfIndex))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "RPL Port number is not valid\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_PORT);
            return SNMP_FAILURE;
        }
        /* Both RPL port and RPL neighbor port cannot be enabled at the same ring node 
         *(Reference :- G.8032 Y.1344 ITU-T (03/2010) standard :  Section 10.1) */

        if (pRingInfo->u4RplNeighbourIfIndex != 0)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Both RPL port and RPL Neighbor Port can't be configured "
                              "on the same ring node.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_OR_RPL_RPL_NEIGHBOUR);
            return SNMP_FAILURE;
        }

        if (ErpsUtilCheckIfIcclInterface ((UINT4) i4TestValFsErpsRingRplPort) ==
            OSIX_SUCCESS)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ICCL interface cannot be configured as RPL port.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_ICCL_RPL_PORT);
            return SNMP_FAILURE;
        }
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC, "RPL Port can't be"
                          " configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPortBlockingOnVcRecovery
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPortBlockingOnVcRecovery
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPortBlockingOnVcRecovery (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsErpsContextId,
                                             UINT4 u4FsErpsRingId,
                                             INT4
                                             i4TestValFsErpsRingPortBlockingOnVcRecovery)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingPortBlockingOnVcRecovery != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsRingPortBlockingOnVcRecovery != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* If the Ring Port2 is 0 then that ring is considered as sub ring 
     * This configuration is applicable only for the sub ring */
    if (pRingInfo->u4Port2IfIndex != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_BLOCK_PORT_VC_RECOVERY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingOperatingMode
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingOperatingMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingOperatingMode (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                  UINT4 u4FsErpsRingId,
                                  INT4 i4TestValFsErpsRingOperatingMode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingOperatingMode != ERPS_RING_REVERTIVE_MODE) &&
        (i4TestValFsErpsRingOperatingMode != ERPS_RING_NON_REVERTIVE_MODE))
    {
        ERPS_GLOBAL_TRC ("Ring Group Operation Type is not valid\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsUtilGetMcLagStatus () == OSIX_SUCCESS)
    {
        if (i4TestValFsErpsRingOperatingMode == ERPS_RING_NON_REVERTIVE_MODE)
        {
            ERPS_GLOBAL_TRC
                ("Non revertive mode operation is not valid if MC-LAG is enabled\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ICCL_ERR_RING_MODE);
            return SNMP_FAILURE;
        }
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Operation Type cannot be configured "
                          "when ring group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingMonitorMechanism
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                testValFsErpsRingMonitorMechanism
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingMonitorMechanism (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     INT4 i4TestValFsErpsRingMonitorMechanism)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingMonitorMechanism != ERPS_MONITOR_MECH_CFM) &&
        (i4TestValFsErpsRingMonitorMechanism != ERPS_MONITOR_MECH_MPLSOAM))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Monitoring Mechanism is not valid\r\n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Monitoring Mechanism cannot be configured "
                          "when ring group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }
    if ((pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW) &&
        (i4TestValFsErpsRingMonitorMechanism == ERPS_MONITOR_MECH_CFM))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Monitor Mechanism CFM is not supported for Service type as MPLS_LSP_PW.\n");
        CLI_SET_ERR (CLI_ERPS_ERR_MONITOR_INVALID);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                              UINT4 u4FsErpsRingId,
                              INT4 i4TestValFsErpsRingRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pRingInfo1 = NULL;
    UINT4               u4EntryCount = 0;
    UINT2               u2VlanGroupId = 1;
    BOOL1               bResult = OSIX_FALSE;
    UINT2               u2InstanceId = ISS_ALL_STP_INSTANCES;
    UINT1               u1Flag = OSIX_FALSE;

    if (ErpsLwUtilValidateContextInfo (u4FsErpsContextId, pu4ErrorCode) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u4FsErpsRingId == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Id should be greater than zero\n",
                          u4FsErpsRingId);
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ID_ZERO);

        return SNMP_FAILURE;
    }

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if ((pRingInfo == NULL) &&
        ((i4TestValFsErpsRingRowStatus == NOT_IN_SERVICE) ||
         (i4TestValFsErpsRingRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ERPS_GLOBAL_TRC ("Ring Group not created\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValFsErpsRingRowStatus)
    {
            /* Intentional Fall-Thorugh */
        case CREATE_AND_WAIT:

            if (pRingInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d is already created\n",
                                  u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RING_ALREADY_CREATED);
                return SNMP_FAILURE;
            }
            /* Number of rings created, count is fetched from RBTreeCount 
             * datastructure and it is compared to maximum Number of
             * rings that are allowed to create*/
            RBTreeCount (gErpsGlobalInfo.RingTable, &u4EntryCount);
            if (u4EntryCount == ERPS_SYS_MAX_RING)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Number of Rings configured exceeds the maximum limit\n",
                                  u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RINGS_EXCEEDED);

                return SNMP_FAILURE;
            }
            break;

        case DESTROY:

            if (pRingInfo == NULL)
            {
                ERPS_GLOBAL_TRC ("Ring Group is not present\r\n");
                return SNMP_SUCCESS;
            }
            /* Check the TcPropRingList that configured for this ring. If it 
             * is present then should not allow to delete this before deleting 
             * that TcPropList */
            if (pRingInfo->TcPropRingList != NULL)
            {
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d cannot be removed without "
                                  "deleting the TC propagate list\n",
                                  u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RING_DEL_IF_CFM_EXIST);
                return SNMP_FAILURE;
            }
            /* This ring may be configured as the TC propagation 
             * ring list in some other sub ring. So that should be 
             * deleted before deleting this ring  */
            if (pRingInfo->u1TcPropCount > 0)
            {
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d cannot be removed without "
                                  "deleting this ring form the TC propagate"
                                  "list of some other ring\n", u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RING_DEL_IF_CFM_EXIST);
                return SNMP_FAILURE;
            }
            if (pRingInfo->CfmEntry.u1RowStatus != 0)
            {
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d cannot be removed without "
                                  "deleting the Ring Cfm Entry\n",
                                  u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_CFM_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            if (pRingInfo->u1MonitoringType != ERPS_MONITOR_MECH_CFM)
            {
                if (pRingInfo->MplsPwInfo.u1RowStatus != 0)
                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Ring Group %d cannot be removed without "
                                      "deleting the Ring Mpls Pw Entry\n",
                                      u4FsErpsRingId);
                    CLI_SET_ERR (CLI_ERPS_ERR_MPLS_ENTRY_EXISTS);
                    return SNMP_FAILURE;
                }

            }

            break;

        case NOT_IN_SERVICE:

            if (pRingInfo->u1RowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d cannot move from Not "
                                  "Ready to Not In Service\n", u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RING_NOT_READY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:

            /* The RowStatus can be set to Active only if all the mandatory
             * objects in the Table are set. When they are not set, the
             * RowStatus will be in Not Ready State. RowStatus can move to
             * Active from Not In Service State only */

            if (pRingInfo->u1RowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d should be in NotInService to "
                                  "be activated\n", u4FsErpsRingId);
                CLI_SET_ERR (CLI_ERPS_ERR_RING_NOT_READY);
                return SNMP_FAILURE;
            }

            if (pRingInfo->CfmEntry.u1RowStatus == NOT_IN_SERVICE)
            {
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Ring Group %d shouldn't be activate when cfm "
                                  "row status is in NotInSevice.\n",
                                  u4FsErpsRingId);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (pRingInfo->u1MonitoringType != ERPS_MONITOR_MECH_CFM)
            {
                if (pRingInfo->MplsPwInfo.u1RowStatus == NOT_IN_SERVICE)
                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "Ring Group %d with monitoring type: MPLSOAM shouldn't move to active state when "
                                      "MPLS PW Row status is in NotInService\n",
                                      u4FsErpsRingId);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

            }

            if (pRingInfo->u4Port2IfIndex != 0)
            {
                if (pRingInfo->u1PropagateTC == OSIX_ENABLED)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_ERPS_ERR_NOT_A_SUBRING);
                    return SNMP_FAILURE;
                }

                if (pRingInfo->u1PortBlockedOnVcRecovery == ERPS_SNMP_TRUE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_ERPS_ERR_BLOCK_PORT_VC_RECOVERY);
                    return SNMP_FAILURE;
                }
            }
            if (pRingInfo->u1ProtectionType == ERPS_SERVICE_BASED_PROTECTION)
            {
                if (pRingInfo->u2VlanGroupListCount != 0)
                {
                    for (; u2VlanGroupId <= pRingInfo->u2VlanGroupListCount;
                         u2VlanGroupId++)
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            ((*(pRingInfo->pau1RingVlanGroupList)),
                             u2VlanGroupId,
                             sizeof (tErpsVlanGroupList), bResult)
                            if (bResult == OSIX_FALSE)
                        {
                            continue;
                        }
                        u2InstanceId = u2VlanGroupId;

                        if (ErpsPortL2IwfIsRapsVlanPresent
                            (u4FsErpsContextId, pRingInfo->RapsVlanId,
                             u2InstanceId) == L2IWF_SUCCESS)
                        {
                            u1Flag = OSIX_TRUE;
                            break;
                        }
                    }
                }
                if (u1Flag == OSIX_FALSE)

                {
                    ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "RAPS-Vlan is not mapped to Vlan Group\n");
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_ERPS_ERR_RAPS_VLAN_NOT_MAPPED);
                    return SNMP_FAILURE;
                }
            }

            if (pRingInfo->u1RowStatus != ACTIVE)
            {
                /* Check Ring Info is already present for the given index and vlan id 
                 **/
                pRingInfo1 =
                    ErpsRingGetNodeFromPortVlanTable (u4FsErpsContextId,
                                                      pRingInfo->u4Port1IfIndex,
                                                      pRingInfo->RapsVlanId,
                                                      pRingInfo->u1RingMacId);
                if (pRingInfo1 != NULL)
                {
                    if (((pRingInfo1->u4Port1IfIndex ==
                          pRingInfo->u4Port1IfIndex)
                         || (pRingInfo1->u4Port1IfIndex ==
                             pRingInfo->u4DistIfIndex))
                        && ((pRingInfo1->RapsVlanId == pRingInfo->RapsVlanId)
                            || (pRingInfo1->u1RingMacId ==
                                pRingInfo->u1RingMacId)))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                          MGMT_TRC | ALL_FAILURE_TRC,
                                          "Ring port and RAPS VLAN already present\n",
                                          u4FsErpsRingId);
                        CLI_SET_ERR
                            (CLI_ERPS_ERR_RING_RAPS_VLAN_ALREADY_CREATED);

                        return SNMP_FAILURE;
                    }
                }
                /* Check Ring Info is already present for the given index and vlan id 
                 **/
                pRingInfo1 =
                    ErpsRingGetNodeFromPortVlanTable (u4FsErpsContextId,
                                                      pRingInfo->u4Port2IfIndex,
                                                      pRingInfo->RapsVlanId,
                                                      pRingInfo->u1RingMacId);
                if (pRingInfo1 != NULL)
                {
                    if (((pRingInfo1->u4Port2IfIndex ==
                          pRingInfo->u4Port2IfIndex)
                         || (pRingInfo1->u4Port2IfIndex ==
                             pRingInfo->u4DistIfIndex))
                        && ((pRingInfo1->RapsVlanId == pRingInfo->RapsVlanId)
                            || (pRingInfo1->u1RingMacId ==
                                pRingInfo->u1RingMacId)))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                          MGMT_TRC | ALL_FAILURE_TRC,
                                          "Ring port and RAPS VLAN already present\n",
                                          u4FsErpsRingId);
                        CLI_SET_ERR
                            (CLI_ERPS_ERR_RING_RAPS_VLAN_ALREADY_CREATED);

                        return SNMP_FAILURE;
                    }
                }
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingMacId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingMacId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingMacId (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                          UINT4 u4FsErpsRingId, INT4 i4TestValFsErpsRingMacId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Validate the Context and Ring Id */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Validate the Ring MAC Id */
    if ((i4TestValFsErpsRingMacId < ERPS_MIN_RING_MACID) ||
        (i4TestValFsErpsRingMacId > ERPS_MAX_RING_MACID))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring MAC Id value is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the Ring Row Status. Ring MAC id cannot be chaged if
     * Ring Row Status is ACTIVE */
    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC, "Ring MAC id can't"
                          "be configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingProtectedVlanGroupId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingProtectedVlanGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingProtectedVlanGroupId (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingProtectedVlanGroupId)
{
    /* Validate the i4FsErpsVlanGroupId */
    tStpInstanceInfo    InstInfo;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingInfo;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    /* Validate the Context and Ring Id */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error codep CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Validate the Ring MAC Id */
    if ((i4TestValFsErpsRingProtectedVlanGroupId < ERPS_MIN_VLAN_GROUP_ID) ||
        (i4TestValFsErpsRingProtectedVlanGroupId > ERPS_MAX_VLAN_GROUP_ID))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Vlan Group Id value is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_INVALID);
        return SNMP_FAILURE;
    }

    InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_STATUS;

    if (L2IwfGetInstanceInfo
        (u4FsErpsContextId, (UINT2) i4TestValFsErpsRingProtectedVlanGroupId,
         &InstInfo) == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (InstInfo.u1InstanceStatus == OSIX_FALSE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Vlan Group Id entered is not active"
                          "First create a Vlan Group\n");

        /* Given VlanGroup Id is not present */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_INVALID);
        return SNMP_FAILURE;
    }

    /* Validate the Ring Row Status. Ring MAC id cannot be chaged if
     * Ring Row Status is ACTIVE */
    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Vlan Group Id can't be mapped with the ring "
                          "while the ring is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingProtectionType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingProtectionType (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   INT4 i4TestValFsErpsRingProtectionType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Validate the Context and Ring Id */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Validate the Ring MAC Id */
    if ((i4TestValFsErpsRingProtectionType != ERPS_PORT_BASED_PROTECTION) &&
        (i4TestValFsErpsRingProtectionType != ERPS_SERVICE_BASED_PROTECTION))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Protection Type value is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Only Port based protection type is offered for LSP  Pseudowire service
     * types. 
     */
    if ((pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW) &&
        (i4TestValFsErpsRingProtectionType == ERPS_SERVICE_BASED_PROTECTION))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Protection Type value is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate the Ring Row Status. Ring protection type cannot be chaged if
     * Ring Row Status is ACTIVE */
    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Protection Type value cannot be configured "
                          "while the ring is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRAPSCompatibleVersion
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRAPSCompatibleVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRAPSCompatibleVersion (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          INT4
                                          i4TestValFsErpsRingRAPSCompatibleVersion)
{
    tErpsRingInfo      *pRingInfo = NULL;
    /* Validate the Context and Ring Id */
    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
           of filling the error code, CLI set error and trace messages.
           So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED) &&
        (i4TestValFsErpsRingRAPSCompatibleVersion ==
         ERPS_RING_COMPATIBLE_VERSION1))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Version Type is not valid\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_VER_NOT_COM);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingRAPSCompatibleVersion !=
         ERPS_RING_COMPATIBLE_VERSION1)
        && (i4TestValFsErpsRingRAPSCompatibleVersion !=
            ERPS_RING_COMPATIBLE_VERSION2))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Version Type is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* when compatible version is v2 and Recovery Method is set 
     * as Manual recovery method throw an error */

    if ((pRingInfo->u1RecoveryMethod == ERPS_RING_MANUAL_RECOVERY) &&
        (i4TestValFsErpsRingRAPSCompatibleVersion ==
         ERPS_RING_COMPATIBLE_VERSION2))
    {

        CLI_SET_ERR (CLI_ERPS_ERR_MAN_VER_NOT_COM);
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Manual recovery method is not valid for"
                          "Compatible version V2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring version number cannot be configured"
                          "when ring group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRplNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRplNeighbourPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRplNeighbourPort (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     INT4 i4TestValFsErpsRingRplNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          " RPL Neighbor Port can't be configured "
                          "when ERPS compatible version is 1\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    if (i4TestValFsErpsRingRplNeighbourPort != 0)
    {
        /* Both RPL port and RPL neighbor port cannot be enabled at the same ring node 
         *(Reference :- G.8032 Y.1344 ITU-T (03/2010) standard :  Section 10.1) */
        if (pRingInfo->u4RplIfIndex != 0)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Both RPL port and RPL Neighbor Port can't be configured "
                              "on the same ring node.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_OR_RPL_RPL_NEIGHBOUR);
            return SNMP_FAILURE;
        }

        /* RPL Neighbor port should be any one of the ring ports that configured 
         * for that RING */
        if ((i4TestValFsErpsRingRplNeighbourPort !=
             (INT4) pRingInfo->u4Port1IfIndex)
            && (i4TestValFsErpsRingRplNeighbourPort !=
                (INT4) pRingInfo->u4Port2IfIndex))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "RPL Neighbor Port number is not valid\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_PORT);
            return SNMP_FAILURE;
        }

        if (ErpsUtilCheckIfIcclInterface
            ((UINT4) i4TestValFsErpsRingRplNeighbourPort) == OSIX_SUCCESS)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ICCL interface cannot be configured as RPL neighbor port.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_ICCL_RPL_NEIGHBOR_PORT);
            return SNMP_FAILURE;
        }
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "RPL Neighbor Port can't be"
                          " configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRAPSSubRingWithoutVC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRAPSSubRingWithoutVC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRAPSSubRingWithoutVC (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingRAPSSubRingWithoutVC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingRAPSSubRingWithoutVC != OSIX_ENABLED) &&
        (i4TestValFsErpsRingRAPSSubRingWithoutVC != OSIX_DISABLED))
    {
        ERPS_GLOBAL_TRC ("Invalid value for object RAPSSubRingWithoutVC\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsUtilGetMcLagStatus () == OSIX_SUCCESS)
    {
        if (i4TestValFsErpsRingRAPSSubRingWithoutVC == OSIX_ENABLED)
        {
            ERPS_GLOBAL_TRC
                ("The object can't be configured when MC-LAG is enabled on the node\r\n");
            CLI_SET_ERR (CLI_ERPS_MCLAG_WITHOUT_VC);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "The object can't be configured "
                          "when ERPS compatible version is 1\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "The object can't be"
                          " configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingRplNextNeighbourPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingRplNextNeighbourPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingRplNextNeighbourPort (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingRplNextNeighbourPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          " RPL Next Neighbor Port can't be configured "
                          "when ERPS compatible version is 1\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    if (i4TestValFsErpsRingRplNextNeighbourPort != 0)
    {
        /* RPL Next Neighbor port should be any one of the ring ports that configured 
         * for that RING */
        if ((i4TestValFsErpsRingRplNextNeighbourPort !=
             (INT4) pRingInfo->u4Port1IfIndex)
            && (i4TestValFsErpsRingRplNextNeighbourPort !=
                (INT4) pRingInfo->u4Port2IfIndex))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "RPL Next Neighbor Port number is not valid\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_PORT);
            return SNMP_FAILURE;
        }

        /* RPL port, RPL neighbor port and RPL Next Neighbour port cannot be
         * enabled at the same ring node (Reference :- G.8032 Y.1344
         * ITU-T (03/2010) standard :  Section 10.1 and Appendix VIII) */
        if ((pRingInfo->u4RplIfIndex != 0)
            || (pRingInfo->u4RplNeighbourIfIndex != 0))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "RPL Next neighbour port can't be configured if RPL port "
                              "or RPL neighbor port already exist in the same ring node.\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RPL_NEXT_NEIGHBOUR_ON_RPL_PORTS);
            return SNMP_FAILURE;
        }

        /* RPL Next Neighbor Node is configured on only one end connected to RPL Owner or RPL Neighbour node
         *(Reference :- G.8032 Y.1344 ITU-T (03/2010) standard :  Appendix VIII.3) 
         * So, Only one port in the Ring Node can be configured as RPL Next Neighbor port not both.
         */

        if ((pRingInfo->u4RplNextNeighbourIfIndex != 0)
            && (i4TestValFsErpsRingRplNextNeighbourPort !=
                (INT4) pRingInfo->u4RplNextNeighbourIfIndex))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Both the ring ports can't be configured "
                              "as RPL Next Neighbor port.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_SAME_RPL_NEXT_NEIGHBOUR);
            return SNMP_FAILURE;
        }

    }

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "RPL Next Neighbor Port can't be"
                          " configured when ring group is Active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingProtectedVlanGroupList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                testValFsErpsRingProtectedVlanGroupList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingProtectedVlanGroupList (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsErpsContextId,
                                           UINT4 u4FsErpsRingId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValFsErpsRingProtectedVlanGroupList)
{

    tErpsRingInfo      *pRingInfo = NULL;
    tStpInstanceInfo    InstInfo;
    tErpsRingInfo       RingInfo;
    UINT2               u2Index = 1;
    UINT1               bResult = 0;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pTestValFsErpsRingProtectedVlanGroupList->i4_Length >
        ERPS_MAX_VLAN_GROUP_LIST)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Vlan Group Id List is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MAX_VLAN_GROUP_LIST);
        return SNMP_FAILURE;

    }
    for (; u2Index <= ERPS_MAX_VLAN_GROUP_LIST * 8; u2Index++)
    {
        /* Scanning the vlan group list bitmap to identify the vlan group id is valid */
        OSIX_BITLIST_IS_BIT_SET ((pTestValFsErpsRingProtectedVlanGroupList->
                                  pu1_OctetList), u2Index,
                                 sizeof (tErpsVlanGroupList), bResult);
        if (bResult != OSIX_FALSE)
        {
            InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_STATUS;

            if (L2IwfGetInstanceInfo (u4FsErpsContextId, u2Index,
                                      &InstInfo) == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (InstInfo.u1InstanceStatus == OSIX_FALSE)
            {
                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Vlan Group Id entered is not active"
                                  "First create a Vlan Group\n");

                /* Given VlanGroup Id is not present */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_VLAN_GROUP_ID_INVALID);
                return SNMP_FAILURE;
            }

        }
        /* If the bit is set, print the corresponding vlan group id */
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingCfmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingCfmTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingCfmTable (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "CFM Table entry for Ring Group %d "
                          "not found\n", u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingCfmTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingCfmTable (UINT4 *pu4FsErpsContextId,
                                    UINT4 *pu4FsErpsRingId)
{
    tErpsRingInfo      *pNextRingInfo = NULL;

    pNextRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pNextRingInfo != NULL)
    {
        if (pNextRingInfo->CfmEntry.u1RowStatus != 0)
        {
            *pu4FsErpsContextId = pNextRingInfo->u4ContextId;
            *pu4FsErpsRingId = pNextRingInfo->u4RingId;
            return SNMP_SUCCESS;
        }
        else
        {
            pNextRingInfo = ErpsRingGetNextNodeFromRingTable (pNextRingInfo);
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingCfmTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingCfmTable (UINT4 u4FsErpsContextId,
                                   UINT4 *pu4NextFsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   UINT4 *pu4NextFsErpsRingId)
{
    tErpsRingInfo       RingInfo;
    tErpsRingInfo      *pNextRingCfgInfo = NULL;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    RingInfo.u4RingId = u4FsErpsRingId;
    RingInfo.u4ContextId = u4FsErpsContextId;

    pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingInfo);

    if (pNextRingCfgInfo != NULL)
    {
        if (pNextRingCfgInfo->CfmEntry.u1RowStatus != 0)
        {
            *pu4NextFsErpsContextId = pNextRingCfgInfo->u4ContextId;
            *pu4NextFsErpsRingId = pNextRingCfgInfo->u4RingId;
            return SNMP_SUCCESS;
        }
        else
        {
            pNextRingCfgInfo =
                ErpsRingGetNextNodeFromRingTable (pNextRingCfgInfo);
        }
    }
    UNUSED_PARAM (pNextRingCfgInfo);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingMEG1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingMEG1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingMEG1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      UINT4 *pu4RetValFsErpsRingMEG1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingMEG1 = pRingInfo->CfmEntry.u4Port1MEGId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingCfmME1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingCfmME1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingCfmME1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        UINT4 *pu4RetValFsErpsRingCfmME1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingCfmME1 = pRingInfo->CfmEntry.u4Port1MEId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingCfmMEP1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingCfmMEP1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingCfmMEP1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         UINT4 *pu4RetValFsErpsRingCfmMEP1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingCfmMEP1 = pRingInfo->CfmEntry.u4Port1MEPId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingMEG2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingMEG2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingMEG2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      UINT4 *pu4RetValFsErpsRingMEG2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingMEG2 = pRingInfo->CfmEntry.u4Port2MEGId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingCfmME2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingCfmME2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingCfmME2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        UINT4 *pu4RetValFsErpsRingCfmME2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingCfmME2 = pRingInfo->CfmEntry.u4Port2MEId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingCfmMEP2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingCfmMEP2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingCfmMEP2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         UINT4 *pu4RetValFsErpsRingCfmMEP2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingCfmMEP2 = pRingInfo->CfmEntry.u4Port2MEPId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingCfmRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingCfmRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingCfmRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                              INT4 *pi4RetValFsErpsRingCfmRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* When CFM entry is not configured, RowStatus will be initialised with 0 */

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingCfmRowStatus = pRingInfo->CfmEntry.u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingMEG1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingMEG1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingMEG1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      UINT4 u4SetValFsErpsRingMEG1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port1MEGId = u4SetValFsErpsRingMEG1;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingCfmME1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingCfmME1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingCfmME1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        UINT4 u4SetValFsErpsRingCfmME1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port1MEId = u4SetValFsErpsRingCfmME1;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingCfmMEP1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingCfmMEP1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingCfmMEP1 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         UINT4 u4SetValFsErpsRingCfmMEP1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port1MEPId = u4SetValFsErpsRingCfmMEP1;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingMEG2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingMEG2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingMEG2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                      UINT4 u4SetValFsErpsRingMEG2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port2MEGId = u4SetValFsErpsRingMEG2;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingCfmME2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingCfmME2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingCfmME2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                        UINT4 u4SetValFsErpsRingCfmME2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port2MEId = u4SetValFsErpsRingCfmME2;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingCfmMEP2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingCfmMEP2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingCfmMEP2 (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                         UINT4 u4SetValFsErpsRingCfmMEP2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->CfmEntry.u4Port2MEPId = u4SetValFsErpsRingCfmMEP2;

    ErpsLwUtilRingCfmCfgNotInServ (pRingInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingCfmRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingCfmRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingCfmRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                              INT4 i4SetValFsErpsRingCfmRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pRingInfo->CfmEntry.u1RowStatus == 0) &&
        ((i4SetValFsErpsRingCfmRowStatus == ACTIVE) ||
         (i4SetValFsErpsRingCfmRowStatus == NOT_IN_SERVICE)))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Cfm Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsErpsRingCfmRowStatus)
    {
        case CREATE_AND_WAIT:

            pRingInfo->CfmEntry.u1RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:

            pRingInfo->CfmEntry.u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            MEMSET (&(pRingInfo->CfmEntry), 0, sizeof (tErpsCfmInfo));
            pRingInfo->u1RowStatus = NOT_READY;
            break;

        case ACTIVE:

            if (pRingInfo->CfmEntry.u1RowStatus != NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }
            pRingInfo->CfmEntry.u1RowStatus = ACTIVE;
            ErpsLwUtilRingCfgNotInServ (pRingInfo);
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingMEG1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingMEG1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingMEG1 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 u4TestValFsErpsRingMEG1)
{

    if (u4TestValFsErpsRingMEG1 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working MEG is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingCfmME1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingCfmME1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingCfmME1 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                           UINT4 u4FsErpsRingId,
                           UINT4 u4TestValFsErpsRingCfmME1)
{

    if (u4TestValFsErpsRingCfmME1 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working ME is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingCfmMEP1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingCfmMEP1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingCfmMEP1 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                            UINT4 u4FsErpsRingId,
                            UINT4 u4TestValFsErpsRingCfmMEP1)
{

    if ((u4TestValFsErpsRingCfmMEP1 == 0) ||
        (u4TestValFsErpsRingCfmMEP1 > ERPS_MAX_MEP_ID))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group MEP is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingMEG2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingMEG2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingMEG2 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 u4TestValFsErpsRingMEG2)
{

    if (u4TestValFsErpsRingMEG2 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working MEG is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingCfmME2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingCfmME2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingCfmME2 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                           UINT4 u4FsErpsRingId,
                           UINT4 u4TestValFsErpsRingCfmME2)
{
    if (u4TestValFsErpsRingCfmME2 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working ME is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingCfmMEP2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingCfmMEP2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingCfmMEP2 (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                            UINT4 u4FsErpsRingId,
                            UINT4 u4TestValFsErpsRingCfmMEP2)
{

    if ((u4TestValFsErpsRingCfmMEP2 == 0) ||
        (u4TestValFsErpsRingCfmMEP2 > ERPS_MAX_MEP_ID))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group MEP is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateCfmEntry (u4FsErpsContextId, u4FsErpsRingId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingCfmRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingCfmRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingCfmRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                 UINT4 u4FsErpsRingId,
                                 INT4 i4TestValFsErpsRingCfmRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingCfmRowStatus < ACTIVE) ||
        (i4TestValFsErpsRingCfmRowStatus > DESTROY))
    {
        ERPS_GLOBAL_TRC ("Ring Group CFM Rowstatus is not valid\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingCfmRowStatus == CREATE_AND_WAIT) &&
        (pRingInfo->CfmEntry.u1RowStatus != 0))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring CFM entry already exists.\n", u4FsErpsRingId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* The RowStatus can be set to Active only if all the mandatory
     * objects in the Table are set. When they are not set, the
     * RowStatus will be in Not Ready State. RowStatus can move to
     * Active from Not In Service State only
     */

    if ((i4TestValFsErpsRingCfmRowStatus == ACTIVE)
        || (i4TestValFsErpsRingCfmRowStatus == NOT_IN_SERVICE))
    {
        if (pRingInfo->CfmEntry.u1RowStatus == NOT_READY)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Ring Group %d CFM entry is not ready to"
                              " be activated\n", u4FsErpsRingId);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_CFM_NOT_READY);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsErpsRingCfmRowStatus == DESTROY)
        || (i4TestValFsErpsRingCfmRowStatus == NOT_IN_SERVICE))
    {
        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Rowstatus of Ring Group %d is active\n",
                              u4FsErpsRingId);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
            return SNMP_FAILURE;

        }

    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingCfmTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingCfmTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingConfigTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingConfigTable (UINT4 u4FsErpsContextId,
                                               UINT4 u4FsErpsRingId)
{
    return (nmhValidateIndexInstanceFsErpsRingTable (u4FsErpsContextId,
                                                     u4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingConfigTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingConfigTable (UINT4 *pu4FsErpsContextId,
                                       UINT4 *pu4FsErpsRingId)
{
    return (nmhGetFirstIndexFsErpsRingTable (pu4FsErpsContextId,
                                             pu4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingConfigTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingConfigTable (UINT4 u4FsErpsContextId,
                                      UINT4 *pu4NextFsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      UINT4 *pu4NextFsErpsRingId)
{
    return (nmhGetNextIndexFsErpsRingTable (u4FsErpsContextId,
                                            pu4NextFsErpsContextId,
                                            u4FsErpsRingId,
                                            pu4NextFsErpsRingId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigHoldOffTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigHoldOffTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigHoldOffTime (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   UINT4 *pu4RetValFsErpsRingConfigHoldOffTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigHoldOffTime = pRingInfo->u4HoldOffTimerValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigGuardTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigGuardTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigGuardTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 UINT4 *pu4RetValFsErpsRingConfigGuardTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigGuardTime = pRingInfo->u4GuardTimerValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigWTRTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigWTRTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigWTRTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               UINT4 *pu4RetValFsErpsRingConfigWTRTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigWTRTime = pRingInfo->u4WTRTimerValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigPeriodicTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigPeriodicTime (UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4
                                    *pu4RetValFsErpsRingConfigPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigPeriodicTime = pRingInfo->u4PeriodicTimerValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigSwitchPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigSwitchPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigSwitchPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 *pi4RetValFsErpsRingConfigSwitchPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigSwitchPort = pRingInfo->u4SwitchCmdIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigSwitchCmd
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigSwitchCmd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigSwitchCmd (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 INT4 *pi4RetValFsErpsRingConfigSwitchCmd)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigSwitchCmd = pRingInfo->u1SwitchCmdType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigRecoveryMethod
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigRecoveryMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigRecoveryMethod (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingConfigRecoveryMethod)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigRecoveryMethod = pRingInfo->u1RecoveryMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigPropagateTC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigPropagateTC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigPropagateTC (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   INT4 *pi4RetValFsErpsRingConfigPropagateTC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigPropagateTC = (INT4) pRingInfo->u1PropagateTC;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigWTBTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigWTBTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigWTBTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               UINT4 *pu4RetValFsErpsRingConfigWTBTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigWTBTime = pRingInfo->u4WTBTimerValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigClear
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigClear (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 *pi4RetValFsErpsRingConfigClear)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*This variable have the non zero value when clear is applied
     *to remove Forced or Manual switch, trigger reversion in revertive
     *or non-revertive mode */

    *pi4RetValFsErpsRingConfigClear = pRingInfo->u1RingClearOperatingModeType;

    /* MSR - Incremental Save disabled */
    /* If Ring state is in FS or MS,have clearSave value as 1,
     * So in Restoration,Ring will move to Correct state(FS or MS).
     *If Ring state is in Idle state,have clearSave value as 2,
     *so in restoration,Ring will move to correct state(Idle)*/

    if (pRingInfo->u1ClearSave != ERPS_CLEAR_COMMAND)
    {

        pRingInfo->u1ClearSave = ERPS_CLEAR_COMMAND_NONE;
    }
    else
    {

        pRingInfo->u1ClearSave = ERPS_CLEAR_COMMAND;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigInterConnNode
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigInterConnNode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigInterConnNode (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     INT4
                                     *pi4RetValFsErpsRingConfigInterConnNode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigInterConnNode =
        (INT4) pRingInfo->u1RingConfigInterConnNode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigMultipleFailure
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigMultipleFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigMultipleFailure (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       INT4
                                       *pi4RetValFsErpsRingConfigMultipleFailure)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigMultipleFailure =
        (INT4) pRingInfo->u1RingConfigMultipleFailure;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigIsPort1Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                The Object 
                retValFsErpsRingConfigIsPort1Present
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigIsPort1Present (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingConfigIsPort1Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigIsPort1Present =
        (INT4) pRingInfo->u1IsPort1Present;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigIsPort2Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigIsPort2Present
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigIsPort2Present (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      *pi4RetValFsErpsRingConfigIsPort2Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigIsPort2Present =
        (INT4) pRingInfo->u1IsPort2Present;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigInfoDistributingPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigInfoDistributingPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigInfoDistributingPort (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            INT4
                                            *pi4RetValFsErpsRingConfigInfoDistributingPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigInfoDistributingPort =
        (INT4) pRingInfo->u4DistIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigKValue
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingConfigKValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigKValue (UINT4 u4FsErpsContextId,
                              UINT4 u4FsErpsRingId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsErpsRingConfigKValue)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (ERPS_FLOAT_TO_OCTETSTRING (pRingInfo->f4KValue,
                                   pRetValFsErpsRingConfigKValue) == EOF)
    {
        return SNMP_FAILURE;
    }
    pRetValFsErpsRingConfigKValue->i4_Length =
        STRLEN (pRetValFsErpsRingConfigKValue);
    pRetValFsErpsRingConfigKValue->pu1_OctetList[pRetValFsErpsRingConfigKValue->
                                                 i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigFailureOfProtocol
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigFailureOfProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigFailureOfProtocol (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         *pi4RetValFsErpsRingConfigFailureOfProtocol)
{

    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingConfigFailureOfProtocol = pRingInfo->bDFOP_TO;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigHoldOffTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigHoldOffTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigHoldOffTime (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   UINT4 u4SetValFsErpsRingConfigHoldOffTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4HoldOffTimerValue = u4SetValFsErpsRingConfigHoldOffTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigGuardTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigGuardTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigGuardTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 UINT4 u4SetValFsErpsRingConfigGuardTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4GuardTimerValue = u4SetValFsErpsRingConfigGuardTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigWTRTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigWTRTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigWTRTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               UINT4 u4SetValFsErpsRingConfigWTRTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4WTRTimerValue = u4SetValFsErpsRingConfigWTRTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigPeriodicTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigPeriodicTime (UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4 u4SetValFsErpsRingConfigPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4PeriodicTimerValue = u4SetValFsErpsRingConfigPeriodicTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigSwitchPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigSwitchPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigSwitchPort (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  INT4 i4SetValFsErpsRingConfigSwitchPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The switch command port is stored in u4OldSwitchCmdIfIndex to restore
     * the old value in case the nmh routine of Switch command fails */

    pRingInfo->u4OldSwitchCmdIfIndex = pRingInfo->u4SwitchCmdIfIndex;

    pRingInfo->u4SwitchCmdIfIndex = (UINT4) i4SetValFsErpsRingConfigSwitchPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigSwitchCmd
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigSwitchCmd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigSwitchCmd (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 INT4 i4SetValFsErpsRingConfigSwitchCmd)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1Request = 0;
    UINT1               u1Flag = 0;
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1SwitchCmdType == i4SetValFsErpsRingConfigSwitchCmd)
    {
        return SNMP_SUCCESS;
    }

    pRingInfo->u1SwitchCmdType = (UINT1) i4SetValFsErpsRingConfigSwitchCmd;

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        switch (i4SetValFsErpsRingConfigSwitchCmd)
        {
            case ERPS_SWITCH_COMMAND_NONE:
                u1Request = ERPS_RING_NO_SWITCH_REQUEST;
                break;
            case ERPS_SWITCH_COMMAND_FORCE:
                u1Request = ERPS_RING_FORCE_SWITCH_REQUEST;
                break;
            case ERPS_SWITCH_COMMAND_MANUAL:
                u1Request = ERPS_RING_MANUAL_SWITCH_REQUEST;
                break;
            default:
                break;
        }

        if (((pRingInfo->u4SwitchCmdIfIndex == pRingInfo->u4Port1IfIndex)
             && (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
            || ((pRingInfo->u4SwitchCmdIfIndex == pRingInfo->u4Port2IfIndex)
                && (pRingInfo->u1IsPort2Present ==
                    ERPS_PORT_IN_REMOTE_LINE_CARD)))

        {
            return SNMP_SUCCESS;
        }
        if (ErpsSmCalTopPriorityRequest (pRingInfo, u1Request, NULL)
            == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    /* Incremental save */
    pRingInfo->u1ClearSave = ERPS_CLEAR_COMMAND_NONE;
    /* Notify function is called to gice notification to RM and MSR */
    u1Flag = ERPS_CLEAR_COMMAND_NONE;
    ErpsUtilNotifyConfig (pRingInfo, u1Flag);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigRecoveryMethod
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigRecoveryMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigRecoveryMethod (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingConfigRecoveryMethod)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RecoveryMethod == i4SetValFsErpsRingConfigRecoveryMethod)
    {
        return SNMP_SUCCESS;
    }

    pRingInfo->u1RecoveryMethod =
        (UINT1) i4SetValFsErpsRingConfigRecoveryMethod;

    /* If ring is active and operating in Non-Revertive mode,
     * if no failure is present, but non-RPL port is blocked indicate
     * to state machine.
     */
    if ((pRingInfo->u1RowStatus == ACTIVE) &&
        (pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE))
    {
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
            (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValFsErpsRingConfigRecoveryMethod == ERPS_RING_AUTO_RECOVERY)
        {
            if (((pRingInfo->u4Port1IfIndex != pRingInfo->u4RplIfIndex) &&
                 (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)) ||
                ((pRingInfo->u4Port2IfIndex != pRingInfo->u4RplIfIndex) &&
                 (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)))
            {
                if (ErpsSmCalTopPriorityRequest
                    (pRingInfo, ERPS_RING_LOCAL_CLEAR_SF_REQUEST,
                     &gErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "nmhSetFsErpsRingConfigRecoveryMethod"
                                      " failed returned from Priority"
                                      " Logic.\r\n");
                }

            }
        }
        else
        {
            if (((pRingInfo->u4Port1IfIndex != pRingInfo->u4RplIfIndex) &&
                 (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)) ||
                ((pRingInfo->u4Port2IfIndex != pRingInfo->u4RplIfIndex) &&
                 (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)))
            {
                if (ErpsSmCalTopPriorityRequest
                    (pRingInfo, ERPS_RING_LOCAL_SF_REQUEST,
                     &gErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "nmhSetFsErpsRingConfigRecoveryMethod"
                                      " failed returned from"
                                      " Priority Logic.\r\n");
                }
            }

        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigPropagateTC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigPropagateTC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigPropagateTC (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   INT4 i4SetValFsErpsRingConfigPropagateTC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1PropagateTC = (UINT1) i4SetValFsErpsRingConfigPropagateTC;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigWTBTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigWTBTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigWTBTime (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                               UINT4 u4SetValFsErpsRingConfigWTBTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4WTBTimerValue = u4SetValFsErpsRingConfigWTBTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigClear
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigClear (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 i4SetValFsErpsRingConfigClear)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRingInfo->u1RingClearOperatingModeType =
        (UINT1) i4SetValFsErpsRingConfigClear;
    /* MsrManualSwitchIndication variable should be reset when applied
     * manual switch is cleared */
    if (pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE)
    {
        pRingInfo->bMsrManualSwitchIndication = OSIX_FALSE;
        pRingInfo->u4MsrSwitchCmdIfIndex = OSIX_FALSE;
    }

    if ((pRingInfo->u1RowStatus == ACTIVE) &&
        (pRingInfo->u1RingClearOperatingModeType == ERPS_CLEAR_COMMAND))
    {
        /*Clear Event is passed to state machine 
         *to remove Forced or Manual switch, trigger reversion in revertive
         *or non-revertive mode */
        if (ErpsSmCalTopPriorityRequest
            (pRingInfo, ERPS_RING_CLEAR_REQUEST, NULL) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    /* Incremental MSR */
    pRingInfo->u1ClearSave = (UINT1) i4SetValFsErpsRingConfigClear;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigInterConnNode
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigInterConnNode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigInterConnNode (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     INT4 i4SetValFsErpsRingConfigInterConnNode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1RingConfigInterConnNode =
        (UINT1) i4SetValFsErpsRingConfigInterConnNode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigMultipleFailure
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigMultipleFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigMultipleFailure (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       INT4
                                       i4SetValFsErpsRingConfigMultipleFailure)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u1RingConfigMultipleFailure =
        (UINT1) i4SetValFsErpsRingConfigMultipleFailure;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigIsPort1Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigIsPort1Present
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigIsPort1Present (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingConfigIsPort1Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        /* When the ring is active and when the port presence in line card
         * is changed from 'remote' to 'local', then the MEPs for that port
         * should be registered with ECFM */
        if ((i4SetValFsErpsRingConfigIsPort1Present ==
             ERPS_PORT_IN_LOCAL_LINE_CARD)
            && (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
        {
            pRingInfo->u1IsPort1Present =
                (UINT1) i4SetValFsErpsRingConfigIsPort1Present;

            if (ErpsUtilRegisterGetFltAndApply (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "nmhSetFsErpsRingIsPort1Present failed to "
                                  "register mep and get existing fault.\r\n");
                /*In case of failure, set the old value itself */
                pRingInfo->u1IsPort1Present = ERPS_PORT_IN_REMOTE_LINE_CARD;
                return SNMP_FAILURE;
            }

        }
        /* When the ring is active and when the port presence in line card
         * is changed from 'local' to 'remote', then the MEPs for that port
         * should be de-registered with ECFM */
        else if ((i4SetValFsErpsRingConfigIsPort1Present ==
                  ERPS_PORT_IN_REMOTE_LINE_CARD)
                 && (pRingInfo->u1IsPort1Present ==
                     ERPS_PORT_IN_LOCAL_LINE_CARD))
        {
            /* Unblock the Remote Port if the port is blocked when moving to 
             * Pending State, or if port is moved to blocked because of Node ID
             * comparison in Pending State.
             * Note : This should be done before changing the port presence as 
             * remote in pRingInfo, otherwise calling ErpsUtilSetPortState,
             * will not have any effect */
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);

            if (ErpsUtilDeRegisterRingPort
                (pRingInfo, pRingInfo->u4Port1IfIndex) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "nmhSetFsErpsRingIsPort1Present failed in "
                                  "deregistering the RingPort from CFM.\r\n");
                return SNMP_FAILURE;
            }
            pRingInfo->u1IsPort1Present =
                (UINT1) i4SetValFsErpsRingConfigIsPort1Present;
        }

    }
    else
    {
        pRingInfo->u1IsPort1Present =
            (UINT1) i4SetValFsErpsRingConfigIsPort1Present;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigIsPort2Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigIsPort2Present
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigIsPort2Present (UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4
                                      i4SetValFsErpsRingConfigIsPort2Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* When the ring is active and when the port presence in line card
     * is changed from 'remote' to 'local', then the MEPs for that port
     * should be registered with ECFM */

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        if ((i4SetValFsErpsRingConfigIsPort2Present ==
             ERPS_PORT_IN_LOCAL_LINE_CARD)
            && (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
        {

            pRingInfo->u1IsPort2Present =
                (UINT1) i4SetValFsErpsRingConfigIsPort2Present;

            if (ErpsUtilRegisterGetFltAndApply (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "nmhSetFsErpsRingIsPort2Present failed to "
                                  "register mep2 and get existing fault.\r\n");
                /*In case of failure, set the old value itself */
                pRingInfo->u1IsPort2Present = ERPS_PORT_IN_REMOTE_LINE_CARD;

                return SNMP_FAILURE;
            }

        }
        /* When the ring is active and when the port presence in line card
         * is changed from 'local' to 'remote', then the MEPs for that port
         * should be de-registered with ECFM */

        else if ((i4SetValFsErpsRingConfigIsPort2Present ==
                  ERPS_PORT_IN_REMOTE_LINE_CARD)
                 && (pRingInfo->u1IsPort2Present ==
                     ERPS_PORT_IN_LOCAL_LINE_CARD))
        {
            /* Unblock the Remote Port if the port is blocked when moving to 
             * Pending State, or if port is moved to blocked because of Node ID
             * comparison in Pending State.
             * Note : This should be done before changing the port presence as 
             * remote in pRingInfo, otherwise calling ErpsUtilSetPortState,
             * will not have any effect */
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
            if (ErpsUtilDeRegisterRingPort
                (pRingInfo, pRingInfo->u4Port2IfIndex) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "nmhSetFsErpsRingIsPort2Present failed in "
                                  "deregistering the RingPort2 from CFM.\r\n");
                return SNMP_FAILURE;
            }
            pRingInfo->u1IsPort2Present =
                (UINT1) i4SetValFsErpsRingConfigIsPort2Present;
        }
    }
    else
    {
        pRingInfo->u1IsPort2Present =
            (UINT1) i4SetValFsErpsRingConfigIsPort2Present;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigInfoDistributingPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigInfoDistributingPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigInfoDistributingPort (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            INT4
                                            i4SetValFsErpsRingConfigInfoDistributingPort)
{
    tEcfmRegParams      ErpsRegParams;
    tEcfmEventNotification ErpsRingMepFltStatus;
    tErpsCfmMepInfo     MepInfo;
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the New value input is already configured. */
    if (pRingInfo->u4DistIfIndex ==
        (UINT4) i4SetValFsErpsRingConfigInfoDistributingPort)
    {
        return SNMP_SUCCESS;
    }

    MEMSET (&ErpsRegParams, 0, sizeof (tEcfmRegParams));
    MEMSET (&ErpsRingMepFltStatus, 0, sizeof (tEcfmEventNotification));
    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));

    ErpsRegParams.u4ModId = ECFM_ERPS_APP_ID;
    ErpsRegParams.u4EventsId = (ECFM_RAPS_FRAME_RECEIVED |
                                ECFM_DEFECT_CONDITION_CLEARED |
                                ECFM_DEFECT_CONDITION_ENCOUNTERED |
                                ECFM_RDI_CONDITION_ENCOUNTERED |
                                ECFM_RDI_CONDITION_CLEARED);

    ErpsRegParams.u4VlanInd = ECFM_INDICATION_FOR_ALL_VLANS;
    ErpsRegParams.pFnRcvPkt = ErpsPortExternalEventNotify;
    ErpsRegParams.pAppInfo = pRingInfo->pContext;
    ErpsRingMepFltStatus.u4ContextId = pRingInfo->u4ContextId;

    /*The registration of MEPs of ring port with ECFM is done only on 
     * the ring port which is configured as 'remote'*/
    if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
    {
        ErpsRingMepFltStatus.u4MdIndex = pRingInfo->CfmEntry.u4Port1MEGId;
        ErpsRingMepFltStatus.u4MaIndex = pRingInfo->CfmEntry.u4Port1MEId;
        ErpsRingMepFltStatus.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port1MEPId;
    }
    else if (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
    {
        ErpsRingMepFltStatus.u4MdIndex = pRingInfo->CfmEntry.u4Port2MEGId;
        ErpsRingMepFltStatus.u4MaIndex = pRingInfo->CfmEntry.u4Port2MEId;
        ErpsRingMepFltStatus.u2MepId = (UINT2) pRingInfo->CfmEntry.u4Port2MEPId;
    }

    /* if `no aps distribute` command is called:
     * MEP's configured should be de-registered. */
    if (i4SetValFsErpsRingConfigInfoDistributingPort == 0)
    {
        if (ErpsPortEcfmMepDeRegister (&ErpsRegParams,
                                       &ErpsRingMepFltStatus) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "%s : failed in deregistering the mep from "
                              "CFM.\r\n", __FUNCTION__);
        }
        else
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "%s : deregistering the mep from CFM is "
                              "successful.\r\n", __FUNCTION__);
            pRingInfo->u4DistIfIndex =
                (UINT4) i4SetValFsErpsRingConfigInfoDistributingPort;
        }
        return SNMP_SUCCESS;
    }

    /* if configure Distributing port is called on port other than the port
     * which is already configured, then Re-Registration is enough
     * No Need to DeRegister and Register again, as we pass only MdIndex,
     * MaIndex and MepID only while registering and not the port index,
     * rest all will be changed via eCFM commands */

    /* Register the MEP's on the Reserved port instead of Remote port,
     * MEP's configured for the Remote Port will be used for MEP
     * registration. */
    if (ErpsPortEcfmMepRegAndGetFltStat (&ErpsRegParams,
                                         &ErpsRingMepFltStatus) == ECFM_SUCCESS)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "%s : registering mep on distributing port is "
                          "successfull.\r\n", __FUNCTION__);
        pRingInfo->u4DistIfIndex =
            (UINT4) i4SetValFsErpsRingConfigInfoDistributingPort;
    }
    else
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s : registering mep on distributing port is "
                          "unsuccessfull.\r\n", __FUNCTION__);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigKValue
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                setValFsErpsRingConfigKValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigKValue (UINT4 u4FsErpsContextId,
                              UINT4 u4FsErpsRingId,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsErpsRingConfigKValue)
{
    tErpsRingInfo      *pRingInfo = NULL;
    /* Inside this function the following are validated.
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (ERPS_OCTETSTRING_TO_FLOAT (pSetValFsErpsRingConfigKValue,
                                   pRingInfo->f4KValue) == EOF)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s : Availability KValue Conversion Error\r\n",
                          __FUNCTION__);
        return SNMP_FAILURE;
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "%s : Availability KValue Conversion successful.\r\n",
                      __FUNCTION__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigFailureOfProtocol
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigFailureOfProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigFailureOfProtocol (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4SetValFsErpsRingConfigFailureOfProtocol)
{

    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->bDFOP_TO = i4SetValFsErpsRingConfigFailureOfProtocol;
    /* According to the standard G.8021/Y.1341 - Amendment 1(07/2011) in section .6.1.4.3.4
     * Start the TFOP timer when failureofprotocol feature is enabled and
     * Ring is in active state */
    if ((pRingInfo->bDFOP_TO == OSIX_ENABLED) &&
        (pRingInfo->u1RowStatus == ACTIVE) &&
        (pRingInfo->u4PeriodicTimerValue != ERPS_INIT_VAL))
    {
        /* When FOP feature is enabled and RPL is in IDLE condition
         *  it wont receive any packets as neighbor port is configured
         *  So FOP timer should be stopped for RPL in IDLE condition*/
        if (!((pRingInfo->u4RplIfIndex != 0) &&
              (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)))
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                               ((pRingInfo->f4KValue) *
                                (pRingInfo->u4PeriodicTimerValue)));
        }
    }
    else if (pRingInfo->bDFOP_TO == OSIX_DISABLED)
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigHoldOffTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigHoldOffTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigHoldOffTime (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      UINT4
                                      u4TestValFsErpsRingConfigHoldOffTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if (ErpsUtilGetMcLagStatus () == OSIX_SUCCESS)
    {
        if (u4TestValFsErpsRingConfigHoldOffTime != 0)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "nmhTestv2FsErpsRingConfigHoldOffTime failed as "
                              "a non-zero value can't be configured on a MC-LAG enabled node.\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ICCL_HOLD_OFF_ERR);
            return SNMP_FAILURE;
        }
    }

    if (u4TestValFsErpsRingConfigHoldOffTime > ERPS_HOLD_OFF_TMR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigGuardTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigGuardTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigGuardTime (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4 u4TestValFsErpsRingConfigGuardTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if (u4TestValFsErpsRingConfigGuardTime > ERPS_GUARD_TMR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
    {
        /* As per standard ITU-T G.8032 Y.1344 (03/2010)Section 10.1.4,
         * WTB timer is defined to be 5 seconds longer than the guard timer.
         * so when guard timer value is less than five seconds of WTB timer
         * error is thrown */
        if (pRingInfo->u4WTBTimerValue < (u4TestValFsErpsRingConfigGuardTime
                                          + ERPS_PERIODIC_TMR_DEF_VAL))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "WTB timer is not five seconds longer than \n"
                              "the guard timer.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_WTB_GUARD_TIMER_VALUE_MISMATCH);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigWTRTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigWTRTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigWTRTime (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                  UINT4 u4FsErpsRingId,
                                  UINT4 u4TestValFsErpsRingConfigWTRTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if (u4TestValFsErpsRingConfigWTRTime > ERPS_WTR_TMR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigPeriodicTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigPeriodicTime (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       u4TestValFsErpsRingConfigPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((u4TestValFsErpsRingConfigPeriodicTime < 1) ||
        (u4TestValFsErpsRingConfigPeriodicTime > ERPS_PERIODIC_TMR_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigSwitchPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigSwitchPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigSwitchPort (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     INT4 i4TestValFsErpsRingConfigSwitchPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u4SwitchCmdIfIndex ==
        (UINT4) i4TestValFsErpsRingConfigSwitchPort)
    {
        return SNMP_SUCCESS;
    }

    /* Switch port should be any one of the ring ports that configured 
     * for that RING */
    if ((i4TestValFsErpsRingConfigSwitchPort !=
         (INT4) pRingInfo->u4Port1IfIndex)
        && (i4TestValFsErpsRingConfigSwitchPort !=
            (INT4) pRingInfo->u4Port2IfIndex))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Port number is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RPL_PORT);
        return SNMP_FAILURE;
    }

    if (ErpsUtilCheckIfIcclInterface
        ((UINT4) i4TestValFsErpsRingConfigSwitchPort) == OSIX_SUCCESS)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Force/Manual switch cannot be applied on an ICCL interface.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_ICCL_SWITCH_CMD);
        return SNMP_FAILURE;
    }

    /* This configuration allowed only when FsErpsRingConfigSwitchCmd 
     * is set to NONE */
    if ((pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
        && (pRingInfo->u1SwitchCmdType != ERPS_SWITCH_COMMAND_NONE))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Switch Command Type should be set to NONE\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_SW_CMD_TYPE_NOT_NONE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigSwitchCmd
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigSwitchCmd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigSwitchCmd (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    INT4 i4TestValFsErpsRingConfigSwitchCmd)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1Flag = 0;

    /* 0 is valid value for the ring port2. So checking against the less 
       than 0 and maximum port is done */
    if ((i4TestValFsErpsRingConfigSwitchCmd < ERPS_SWITCH_COMMAND_NONE) ||
        (i4TestValFsErpsRingConfigSwitchCmd > ERPS_SWITCH_COMMAND_MANUAL))
    {
        ERPS_GLOBAL_TRC ("Invalid switch command\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        if (pRingInfo->u1SwitchCmdType == i4TestValFsErpsRingConfigSwitchCmd)
        {
            return SNMP_SUCCESS;
        }
    }

    if (i4TestValFsErpsRingConfigSwitchCmd == ERPS_SWITCH_COMMAND_MANUAL)
    {
        if (pRingInfo->u1HighestPriorityRequest ==
            ERPS_RING_FORCE_SWITCH_REQUEST)
        {
            pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4OldSwitchCmdIfIndex;
            /* when port is in forced switch
             * Manual switch is given to the same port, Request will not be accepted
             * Latest port number in the issinc.conf file will be the port where manual switch is given
             * After rebooting the switch, MSR is updated with the port number where we gave FS
             * MSR when auto save and incremental save is enabled */

            u1Flag = ERPS_SWITCH_PORT;
            ErpsUtilNotifyConfig (pRingInfo, u1Flag);

            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC, "Manual switch "
                              "is not allowed when ring is in force switch\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_MS_FS_EXISTS);
            return SNMP_FAILURE;
        }

        if (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE)
        {
            pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4OldSwitchCmdIfIndex;

            /* when ring is in protection state
             * Manual switch is given to some port, Request will not be accepted
             * but switch port number alone will be updated in issinc.conf file.
             * This should be cleared from conf file and notification should be given to RM. 
             * This will be applicable
             * when auto save and incremental save is enabled */

            u1Flag = ERPS_SWITCH_PORT;
            ErpsUtilNotifyConfig (pRingInfo, u1Flag);

            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Manual switch is not allowed when local SF or "
                              "R-APS(SF) exists in the ring\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_MS_LOCAL_RAPS_SF_EXISTS);
            return SNMP_FAILURE;
        }

        /* In ERPS V1: Manual switch state does not exist and also
         * RAPS FS messages will not be handled. Hence the following
         * conditions are checked only for ERPS V2 */
        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            if (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE)
            {

                /* when ring is in Forced switch state
                 * Manual switch is given to same or any port, Request will not be accepted
                 * but switch port number alone will be updated in issinc.conf file.
                 * This value has to reset to its default in conf file.
                 * This will be applicable
                 * when auto save and incremental save is enabled */

                pRingInfo->u4SwitchCmdIfIndex =
                    pRingInfo->u4OldSwitchCmdIfIndex;

                u1Flag = ERPS_SWITCH_PORT;
                ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Manual switch is not allowed when "
                                  "RAPS FS exist in the ring\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_MS_FS_EXISTS);
                return SNMP_FAILURE;
            }

            if (pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE)
            {
                /* Restore the old switch port value */

                pRingInfo->u4SwitchCmdIfIndex =
                    pRingInfo->u4OldSwitchCmdIfIndex;
                /* when ring is in Manual switch state
                 * Manual switch is given to same or any port, Request will not be accepted
                 * but switch port number alone will be updated in issinc.conf file.
                 * This updated value has to be reset to its default in conf file.
                 * This will be applicable
                 * when auto save and incremental save is enabled */

                u1Flag = ERPS_SWITCH_PORT;
                ErpsUtilNotifyConfig (pRingInfo, u1Flag);

                ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Manual switch is not allowed when "
                                  "MS is already present in the ring\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERPS_ERR_MS_MS_EXISTS);
                return SNMP_FAILURE;
            }
        }
    }

    /* ITU-T G.8032 (03/2010)While an existing forced switch request is 
     * present any new forcedswitch request is accepted, except for 
     * the Ethernet ring node  having a prior local forced switch request.*/
    if (i4TestValFsErpsRingConfigSwitchCmd == ERPS_SWITCH_COMMAND_FORCE)
    {
        if (pRingInfo->u1HighestPriorityRequest ==
            ERPS_RING_FORCE_SWITCH_REQUEST)
        {
            /* Restore the old switch port value */

            pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4OldSwitchCmdIfIndex;

            /* when ring is in Forced switch state
             * Force switch is given to same or any port, Request will not be accepted
             * but switch port number alone will be updated in issinc.conf file.
             * This updated value has to reset to its default in conf file.
             * This will be applicable
             * when auto save and incremental save is enabled */

            u1Flag = ERPS_SWITCH_PORT;
            ErpsUtilNotifyConfig (pRingInfo, u1Flag);

            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC, "Force switch "
                              "is not allowed when local request in the ring is force switch\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_FS_FS_EXISTS);
            return SNMP_FAILURE;
        }
    }

    /* The force switch / manual switch cannot be cleared when 
     * when compatible version is 2
     * Instead the mib object fsErpsRingConfigClear is used for same effect*/

    if ((pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2) &&
        (i4TestValFsErpsRingConfigSwitchCmd == ERPS_SWITCH_COMMAND_NONE))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Switch command cannot be set to None when the compatible version number is 2\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_MANUAL_NO_APS_SWITCH_METHOD);
        return SNMP_FAILURE;
    }

    /* If the command is force / manual switch then the switch port should 
     * be configured previously */
    if (i4TestValFsErpsRingConfigSwitchCmd != ERPS_SWITCH_COMMAND_NONE)
    {
        if (pRingInfo->u4SwitchCmdIfIndex == 0)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Manual and force switch will not be allowed to"
                              "configure when the switch port is 0\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigRecoveryMethod
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigRecoveryMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigRecoveryMethod (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingConfigRecoveryMethod)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* 0 is valid value for the ring port2. So checking against the less 
       than 0 and maximum port is done */
    if ((i4TestValFsErpsRingConfigRecoveryMethod != ERPS_RING_AUTO_RECOVERY) &&
        (i4TestValFsErpsRingConfigRecoveryMethod != ERPS_RING_MANUAL_RECOVERY))
    {
        ERPS_GLOBAL_TRC ("Invalid recovery mode\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Return failure when the user is trying to set manual recovery 
     *  method when the ERPS Version number is 2 */
    if ((pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        && (i4TestValFsErpsRingConfigRecoveryMethod ==
            ERPS_RING_MANUAL_RECOVERY))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Configuring the manual recovery method is not supported"
                          "when the compatible version number is 2\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_MANUAL_RECOVERY_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigPropagateTC
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigPropagateTC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigPropagateTC (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      INT4 i4TestValFsErpsRingConfigPropagateTC)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingConfigPropagateTC != OSIX_ENABLED) &&
        (i4TestValFsErpsRingConfigPropagateTC != OSIX_DISABLED))
    {
        ERPS_GLOBAL_TRC ("Invalid recovery mode\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* If the Ring Port2 is 0 then that ring is considered as sub ring 
     * This configuration is applicable only for the sub ring */
    if (pRingInfo->u4Port2IfIndex != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_NOT_A_SUBRING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigWTBTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigWTBTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigWTBTime (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                  UINT4 u4FsErpsRingId,
                                  UINT4 u4TestValFsErpsRingConfigWTBTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if (u4TestValFsErpsRingConfigWTBTime > ERPS_WTB_TMR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* WTB Timer is only applicable when the Version number is 2 */
    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "WTB timer can't be configured when ERPS compatible version is 1\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    /* As per standard ITU-T G.8032 Y.1344 (03/2010)Section 10.1.4,
     * WTB timer is defined to be 5 seconds longer than the guard timer.
     * so if WTB is configured less than five seconds of guard timer,
     * error is thrown */
    if (u4TestValFsErpsRingConfigWTBTime < (pRingInfo->u4GuardTimerValue
                                            + ERPS_PERIODIC_TMR_DEF_VAL))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "WTB timer is not five seconds longer than the \n"
                          "guard timer.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_WTB_GUARD_TIMER_VALUE_MISMATCH);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigClear
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigClear (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                UINT4 u4FsErpsRingId,
                                INT4 i4TestValFsErpsRingConfigClear)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingConfigClear != ERPS_CLEAR_COMMAND_NONE) &&
        (i4TestValFsErpsRingConfigClear != ERPS_CLEAR_COMMAND))
    {
        ERPS_GLOBAL_TRC (" The value passed to issue clear is invalid\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Clear command is not allowed when the ERPS compatible version is 1\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    if ((pRingInfo->u1HighestPriorityRequest != ERPS_RING_FORCE_SWITCH_REQUEST)
        && (pRingInfo->u1HighestPriorityRequest !=
            ERPS_RING_MANUAL_SWITCH_REQUEST) &&
        (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_NONE))

    {
        if (pRingInfo->u4RplIfIndex == 0)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Clear command is not allowed in a node when Local "
                              "Forced switch /Manual Switch is not present\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_CLEAR_CMD_NOT_ALLOWED);
            return SNMP_FAILURE;
        }

        else if ((pRingInfo->u4RplIfIndex != 0) &&
                 (pRingInfo->u1RingState != ERPS_RING_PENDING_STATE))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Clear command is not allowed on RPL "
                              "node when Local FS/MS is not present "
                              "or WTR/WTB not Running or not in Non-Revertive mode)\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_CLEAR_CMD_NOT_ALLOWED_ON_RPL);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigInterConnNode
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigInterConnNode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigInterConnNode (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsErpsContextId,
                                        UINT4 u4FsErpsRingId,
                                        INT4
                                        i4TestValFsErpsRingConfigInterConnNode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Minimizing Segmentation in interconnected rings"
                          " feature is supported, only when ERPS compatible"
                          " version is 2.\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    /* Input Value for Inter connection node parameter should
     * be none, Primary or Secondary */
    if ((i4TestValFsErpsRingConfigInterConnNode !=
         ERPS_RING_INTER_CONN_NODE_NONE)
        && (i4TestValFsErpsRingConfigInterConnNode !=
            ERPS_RING_INTER_CONN_NODE_PRIMARY)
        && (i4TestValFsErpsRingConfigInterConnNode !=
            ERPS_RING_INTER_CONN_NODE_SECONDARY))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Input Ring Config Inter Connection Node object"
                          " value is wrong.\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_WRONG_INTER_CONN_NODE_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigMultipleFailure
                This mib object is used in Minimizing Segmenation in
                Inter connected Rings as per Appendix X of
                ITU-T G.8032/Y.1344 (03/2010).
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigMultipleFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigMultipleFailure (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          INT4
                                          i4TestValFsErpsRingConfigMultipleFailure)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Minimizing Segmentation in interconnected rings"
                          " feature is supported, only when ERPS compatible"
                          " version is 2.\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_COMP_VERSION_MISMATCH);
        return SNMP_FAILURE;
    }

    /* Input Value for Multiple failure parameter should
     * be disabled, Primary or Secondary */
    if ((i4TestValFsErpsRingConfigMultipleFailure !=
         ERPS_RING_MULTIPLE_FAILURE_DISABLED)
        && (i4TestValFsErpsRingConfigMultipleFailure !=
            ERPS_RING_MULTIPLE_FAILURE_PRIMARY)
        && (i4TestValFsErpsRingConfigMultipleFailure !=
            ERPS_RING_MULTIPLE_FAILURE_SECONDARY))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Input Config Multiple Failure object"
                          " value is wrong.\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_WRONG_MULTIPLE_FAILURE_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigIsPort1Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigIsPort1Present
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigIsPort1Present (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingConfigIsPort1Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigIsPort1Present !=
         ERPS_PORT_IN_LOCAL_LINE_CARD)
        && (i4TestValFsErpsRingConfigIsPort1Present !=
            ERPS_PORT_IN_REMOTE_LINE_CARD))
    {
        /*Ring port presence vlaue can not be other than local(1) or remote(2) */
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring port should present either in Local or Remote line card\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_PORT_PRESENCE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigIsPort1Present ==
         ERPS_PORT_IN_REMOTE_LINE_CARD)
        && (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Remove remote port configuration from port 2,"
                          " Before Making port 1 as remote.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_PORT2_PRESENCE_REMOTE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigIsPort1Present ==
         ERPS_PORT_IN_REMOTE_LINE_CARD) && (pRingInfo->u4Port1IfIndex == 0))
    {
        /* When Ring port ifIndex is not configured, configuration of Port presence
         * as 'remote' for the corresponding port should be rejected */
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring port with interface index 0 cannot be Remote\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_PORT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigIsPort2Present
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigIsPort2Present
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigIsPort2Present (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         INT4
                                         i4TestValFsErpsRingConfigIsPort2Present)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigIsPort2Present !=
         ERPS_PORT_IN_LOCAL_LINE_CARD)
        && (i4TestValFsErpsRingConfigIsPort2Present !=
            ERPS_PORT_IN_REMOTE_LINE_CARD))
    {
        /*Ring port presence vlaue can not be other than local(1) or remote(2) */
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring port should present either in Local or Remote line card\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_PORT_PRESENCE);
        return SNMP_FAILURE;
    }

    /* Both the port of ring node cannot be configured as remote */
    if ((i4TestValFsErpsRingConfigIsPort2Present ==
         ERPS_PORT_IN_REMOTE_LINE_CARD)
        && (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Remove remote port configuration from port 1,"
                          " Before Making port 2 as remote.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_PORT1_PRESENCE_REMOTE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigIsPort2Present ==
         ERPS_PORT_IN_REMOTE_LINE_CARD) && (pRingInfo->u4Port2IfIndex == 0))
    {
        /*When Ring port ifIndex is not configured, configuration of Port presence
         * as 'remote' for the corresponding port should be rejected */
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring port with interface index 0 cannot be Remote\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_PORT_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigInfoDistributingPort
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigInfoDistributingPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigInfoDistributingPort (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsErpsContextId,
                                               UINT4 u4FsErpsRingId,
                                               INT4
                                               i4TestValFsErpsRingConfigInfoDistributingPort)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* If the New value is already present, then simply return */
    if (pRingInfo->u4DistIfIndex ==
        (UINT4) i4TestValFsErpsRingConfigInfoDistributingPort)
    {
        return SNMP_SUCCESS;
    }

    if (i4TestValFsErpsRingConfigInfoDistributingPort != 0)
    {
        /* Ring Ports of a node cannot be used as Distributing Ring port */
        if ((i4TestValFsErpsRingConfigInfoDistributingPort ==
             (INT4) pRingInfo->u4Port1IfIndex)
            || (i4TestValFsErpsRingConfigInfoDistributingPort ==
                (INT4) pRingInfo->u4Port2IfIndex))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Distributing Port number is already used as Ring Port.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_DISTRIBUTE_PORT_IS_RING_PORT);
            return SNMP_FAILURE;
        }

        /* Distributing Port configuration is possible only if one of the 
         * Ring port is configured as remote ie., This configuration is 
         * useful only for distributed line cards.*/
        if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD) &&
            (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_LOCAL_LINE_CARD))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "The Distributing port configuration is allowed only "
                              "if atleast one of the ring ports is configured "
                              "as remote.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_NO_REMOTE_PORTS);
            return SNMP_FAILURE;

        }

        /* MEP info should be avialable for Remote port, for Remote port
         * configuration to succeed. */
        if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD) &&
            (pRingInfo->CfmEntry.u4Port1MEPId == 0))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "The reserved port configuration is allowed only if"
                              " MEP info is available for the remote port.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_NO_MEP_INFO_CONFIGURED);
            return SNMP_FAILURE;

        }
        else if ((pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
                 && (pRingInfo->CfmEntry.u4Port2MEPId == 0))
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "The reserved port configuration is allowed only if"
                              " MEP info is available for the remote port.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_NO_MEP_INFO_CONFIGURED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigKValue
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                testValFsErpsRingConfigKValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigKValue (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsErpsContextId,
                                 UINT4 u4FsErpsRingId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsErpsRingConfigKValue)
{
    tErpsRingInfo      *pRingInfo = NULL;
    FLT4                f4KValue = 0;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (ERPS_OCTETSTRING_TO_FLOAT (pTestValFsErpsRingConfigKValue,
                                   f4KValue) == EOF)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    if ((f4KValue <= ERPS_INIT_VAL) || (f4KValue > ERPS_FLT_VAL_10))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_INVALID_KVALUE);
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "\tSNMP: Invalid Value\n");
        return SNMP_FAILURE;
    }
    /* If the New value is already present, then simply return */
    if (pRingInfo->f4KValue == f4KValue)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigFailureOfProtocol
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigFailureOfProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigFailureOfProtocol (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            INT4
                                            i4TestValFsErpsRingConfigFailureOfProtocol)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingConfigFailureOfProtocol != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsRingConfigFailureOfProtocol != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingConfigTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingTcPropTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingTcPropTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingTcPropTable (UINT4 u4FsErpsContextId,
                                               UINT4 u4FsErpsRingId,
                                               UINT4 u4FsErpsRingTcPropRingId)
{
    tErpsRingInfo      *pSubRing = NULL;
    tErpsRingInfo      *pRingTcInfo = NULL;
    tErpsRingInfo       RingInfo;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */

    /* To get the SUB RING */

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    pSubRing = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pSubRing == NULL)
    {
        ERPS_GLOBAL_TRC ("Sub Ring Entry not present\r\n");
        return SNMP_FAILURE;
    }

    /* To get the Ring entry that configured as TC propagate list */
    RingInfo.u4ContextId = u4FsErpsContextId;
    RingInfo.u4RingId = u4FsErpsRingTcPropRingId;

    /* Validate tne node is present or not */
    pRingTcInfo = ErpsRingGetTcNode (pSubRing, &RingInfo);

    if (pRingTcInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("TC node Entry not present\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingTcPropTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingTcPropTable (UINT4 *pu4FsErpsContextId,
                                       UINT4 *pu4FsErpsRingId,
                                       UINT4 *pu4FsErpsRingTcPropRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pSubRing = NULL;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (pRingInfo->TcPropRingList != NULL)
        {
            pSubRing = ErpsRingGetFirstTcNode (pRingInfo);

            if (pSubRing != NULL)
            {
                *pu4FsErpsContextId = pRingInfo->u4ContextId;
                *pu4FsErpsRingId = pRingInfo->u4RingId;
                *pu4FsErpsRingTcPropRingId = pSubRing->u4RingId;
                return SNMP_SUCCESS;
            }
        }
        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }
    while (pRingInfo != NULL);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingTcPropTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
                FsErpsRingTcPropRingId
                nextFsErpsRingTcPropRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingTcPropTable (UINT4 u4FsErpsContextId,
                                      UINT4 *pu4NextFsErpsContextId,
                                      UINT4 u4FsErpsRingId,
                                      UINT4 *pu4NextFsErpsRingId,
                                      UINT4 u4FsErpsRingTcPropRingId,
                                      UINT4 *pu4NextFsErpsRingTcPropRingId)
{
    tErpsRingInfo       SubRingInfo;
    tErpsRingInfo      *pNextTcNode = NULL;
    tErpsRingInfo      *pNextSubRing = NULL;
    UINT4               u4RingId = 0;
    UINT4               u4SubRingId = 0;

    u4SubRingId = u4FsErpsRingTcPropRingId;
    u4RingId = u4FsErpsRingId;

    MEMSET (&SubRingInfo, 0, sizeof (tErpsRingInfo));

    SubRingInfo.u4RingId = u4FsErpsRingId;
    SubRingInfo.u4ContextId = u4FsErpsContextId;

    pNextSubRing = ErpsRingGetNodeFromRingTable (&SubRingInfo);
    if (pNextSubRing == NULL)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (pNextSubRing->TcPropRingList != NULL)
        {
            if (pNextSubRing->u4RingId != u4RingId)
            {
                pNextTcNode = ErpsRingGetFirstTcNode (pNextSubRing);
            }
            else
            {
                MEMSET (&SubRingInfo, 0, sizeof (tErpsRingInfo));
                SubRingInfo.u4ContextId = u4FsErpsContextId;
                SubRingInfo.u4RingId = u4SubRingId;

                pNextTcNode = ErpsRingGetNextTcNode (pNextSubRing,
                                                     &SubRingInfo);
            }
            if (pNextTcNode != NULL)
            {
                *pu4NextFsErpsContextId = pNextSubRing->u4ContextId;
                *pu4NextFsErpsRingId = pNextSubRing->u4RingId;
                *pu4NextFsErpsRingTcPropRingId = pNextTcNode->u4RingId;
                return SNMP_SUCCESS;
            }
        }

        pNextSubRing = ErpsRingGetNextNodeFromRingTable (pNextSubRing);
    }
    while (pNextSubRing != NULL);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingTcPropRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId

                The Object 
                retValFsErpsRingTcPropRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingTcPropRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 UINT4 u4FsErpsRingTcPropRingId,
                                 INT4 *pi4RetValFsErpsRingTcPropRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pRingTcInfo = NULL;
    tErpsRingInfo      *pSubRing = NULL;
    tErpsRingInfo       RingInfo;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    pSubRing = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pSubRing == NULL)
    {
        return SNMP_FAILURE;
    }

    /* To get the Ring entry that configured as TC propagate list */
    RingInfo.u4ContextId = u4FsErpsContextId;
    RingInfo.u4RingId = u4FsErpsRingTcPropRingId;

    pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("Ring Entry not present\r\n");
        CLI_SET_ERR (CLI_ERPS_ERR_RING_NOT_CREATED);
        return SNMP_FAILURE;
    }
    pRingTcInfo = ErpsRingGetTcNode (pSubRing, pRingInfo);

    if (pRingTcInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("TC Ring Entry not present\r\n");
        CLI_SET_ERR (CLI_ERPS_ERR_TC_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingTcPropRowStatus = ACTIVE;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingTcPropRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId

                The Object 
                setValFsErpsRingTcPropRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingTcPropRowStatus (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                 UINT4 u4FsErpsRingTcPropRingId,
                                 INT4 i4SetValFsErpsRingTcPropRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pSubRing = NULL;
    UINT4               u4TcPropCount = 0;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */
    pSubRing = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pSubRing == NULL)
    {
        ERPS_GLOBAL_TRC ("Sub Ring Entry not present\n");
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId,
                                      u4FsErpsRingTcPropRingId);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("Ring Entry not present\r\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFsErpsRingTcPropRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            if (pSubRing->TcPropRingList == NULL)
            {
                pSubRing->TcPropRingList =
                    UtlDynSllInit (ERPS_SIZING_MAX_RING - 1,
                                   ErpsRingDyanamicSllCmpFn);
            }

            if (ErpsRingAddTcNode (pSubRing, pRingInfo) == OSIX_FAILURE)
            {
                UtlDynSllShut (pSubRing->TcPropRingList, NULL);
                return SNMP_FAILURE;
            }
            /* Increment the TcPorp Counter */
            pRingInfo->u1TcPropCount++;
            break;
        case DESTROY:
            if (pSubRing->TcPropRingList == NULL)
            {
                return SNMP_SUCCESS;
            }

            ErpsRingDelTcNode (pSubRing, pRingInfo);
            pRingInfo->u1TcPropCount--;

            /* Get the Node count to find the DynSLL is enpty or not */
            if (UtlDynSllCount (pSubRing->TcPropRingList,
                                &u4TcPropCount) == DYN_SLL_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* If it is 0 then the DynSLL is empty so return SUCCESS 
             * silently */
            if (u4TcPropCount == 0)
            {
                /* If there is no nodes in the TC propagate table then remove 
                 * memory allocated for the table */
                UtlDynSllShut (pSubRing->TcPropRingList, NULL);
                pSubRing->TcPropRingList = NULL;
                return SNMP_SUCCESS;
            }

            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingTcPropRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId

                The Object 
                testValFsErpsRingTcPropRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingTcPropRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4 u4FsErpsRingTcPropRingId,
                                    INT4 i4TestValFsErpsRingTcPropRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pSubRing = NULL;
    tErpsRingInfo      *pRingTcInfo = NULL;

    if ((i4TestValFsErpsRingTcPropRowStatus != CREATE_AND_GO) &&
        (i4TestValFsErpsRingTcPropRowStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pSubRing = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pSubRing == NULL)
    {
        ERPS_GLOBAL_TRC ("Sub Ring Entry not present\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId,
                                      u4FsErpsRingTcPropRingId);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("Propagation Ring Entry not present\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_RING_TC_ENTRY_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    pRingTcInfo = ErpsRingGetTcNode (pSubRing, pRingInfo);

    if ((pRingTcInfo != NULL) &&
        (i4TestValFsErpsRingTcPropRowStatus == CREATE_AND_GO))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring TC Entry is present\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* If the ring is not a sub ring then return failure */
    if (pSubRing->u4Port2IfIndex != 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Node is not a interconnected node \n");
        CLI_SET_ERR (CLI_ERPS_ERR_RING_AS_TC_PROP_ENTRY);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pSubRing->u4RingId == u4FsErpsRingTcPropRingId)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Id should not be self id \n");
        CLI_SET_ERR (CLI_ERPS_ERR_RING_SELF_TC_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingConfigExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingConfigExtTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingConfigExtTable (UINT4 u4FsErpsContextId,
                                                  UINT4 u4FsErpsRingId)
{
    return (nmhValidateIndexInstanceFsErpsRingTable (u4FsErpsContextId,
                                                     u4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingConfigExtTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingConfigExtTable (UINT4 *pu4FsErpsContextId,
                                          UINT4 *pu4FsErpsRingId)
{
    return (nmhGetFirstIndexFsErpsRingTable (pu4FsErpsContextId,
                                             pu4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingConfigExtTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingConfigExtTable (UINT4 u4FsErpsContextId,
                                         UINT4 *pu4NextFsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         UINT4 *pu4NextFsErpsRingId)
{
    return (nmhGetNextIndexFsErpsRingTable (u4FsErpsContextId,
                                            pu4NextFsErpsContextId,
                                            u4FsErpsRingId,
                                            pu4NextFsErpsRingId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigExtVCRecoveryPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigExtVCRecoveryPeriodicTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigExtVCRecoveryPeriodicTime (UINT4 u4FsErpsContextId,
                                                 UINT4 u4FsErpsRingId,
                                                 UINT4
                                                 *pu4RetValFsErpsRingConfigExtVCRecoveryPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingConfigExtVCRecoveryPeriodicTime =
        pRingInfo->u4VcRecoveryPeriodicTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingConfigExtMainRingId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingConfigExtMainRingId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingConfigExtMainRingId (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingConfigExtMainRingId)
{
    tErpsRingInfo       RingConfigInfo;
    tErpsRingInfo      *pNextRingCfgInfo = NULL;
    UNUSED_PARAM (u4FsErpsRingId);

    MEMSET (&RingConfigInfo, 0, sizeof (tErpsRingInfo));
    RingConfigInfo.u4ContextId = u4FsErpsContextId;
    RingConfigInfo.u4RingId = 0;

    pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingConfigInfo);
    /* Scan through the ring table and obtain the sub-ring list. 
     * If incoming ring id (u4FsErpsRingId) is present in the sub-ring list 
     * then return it's ring Id  as Main RingId */
    while (pNextRingCfgInfo != NULL)
    {
        /* If the next ring in the ring table doesnot belong to the incoming
         * context then break from the loop */
        if (pNextRingCfgInfo->u4ContextId != u4FsErpsContextId)
        {
            break;
        }
        if (ErpsGetSubRingEntry (pNextRingCfgInfo, u4FsErpsRingId) != NULL)
        {
            *pu4RetValFsErpsRingConfigExtMainRingId =
                pNextRingCfgInfo->u4RingId;
            return SNMP_SUCCESS;
        }
        RingConfigInfo.u4ContextId = pNextRingCfgInfo->u4ContextId;
        RingConfigInfo.u4RingId = pNextRingCfgInfo->u4RingId;
        pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingConfigInfo);
    }
    *pu4RetValFsErpsRingConfigExtMainRingId = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigExtVCRecoveryPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigExtVCRecoveryPeriodicTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigExtVCRecoveryPeriodicTime (UINT4 u4FsErpsContextId,
                                                 UINT4 u4FsErpsRingId,
                                                 UINT4
                                                 u4SetValFsErpsRingConfigExtVCRecoveryPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRingInfo->u4VcRecoveryPeriodicTime =
        u4SetValFsErpsRingConfigExtVCRecoveryPeriodicTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingConfigExtMainRingId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingConfigExtMainRingId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingConfigExtMainRingId (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     u4SetValFsErpsRingConfigExtMainRingId)
{
    tErpsRingInfo      *pMainRingInfo = NULL;
    tErpsRingInfo       RingConfigInfo;
    tErpsRingInfo      *pNextRingCfgInfo = NULL;
    tSubRingEntry      *pSubRingEntry = NULL;
    tSubRingEntry      *pSubRingCfgEntry = NULL;
    /*Get the RingInfo of the Main Ring
     * Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    if (u4SetValFsErpsRingConfigExtMainRingId != 0)
    {
        pMainRingInfo =
            ErpsRingGetRingEntry (u4FsErpsContextId,
                                  u4SetValFsErpsRingConfigExtMainRingId);

        if (pMainRingInfo == NULL)
        {
            return SNMP_FAILURE;
        }

        /* If the sub-ring is already present in the sub-ring list
         * then return success */
        if (ErpsGetSubRingEntry (pMainRingInfo, u4FsErpsRingId) != NULL)
        {
            return SNMP_SUCCESS;
        }

        /* Allocate memory and 
         * Populate the sub-ring list of the Main Ring if 
         * the sub-ring list is not configured for the 
         * main ring*/

        if ((pSubRingEntry = (tSubRingEntry *)
             MemAllocMemBlk (gErpsGlobalInfo.SubRingPoolId)) == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMSET (pSubRingEntry, 0, sizeof (tSubRingEntry));
        pSubRingEntry->u4SubRingId = u4FsErpsRingId;
        TMO_SLL_Add (&(pMainRingInfo->SubRingListHead),
                     &(pSubRingEntry->SubRingNextNode));

        /* Proceed to remove the sub-ring from the sub-ring list
         * of the older main ring */
    }

    MEMSET (&RingConfigInfo, 0, sizeof (tErpsRingInfo));
    RingConfigInfo.u4ContextId = u4FsErpsContextId;
    RingConfigInfo.u4RingId = 0;
    pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingConfigInfo);

    while (pNextRingCfgInfo != NULL)
    {
        /* If the next ring in the ring table doesnot belong to the incoming
         * context then break from the loop */
        if (pNextRingCfgInfo->u4ContextId != u4FsErpsContextId)
        {
            break;
        }
        /* Remove the sub-ring from the sub-ring list 
         * if user has entered the main ring id as 0
         * or */
        /* If the entered main ring id is different from the previously 
         * entered main ring id for the sub-ring, then delete the sub-ring 
         * from the sub-ring list of the previous main ring */
        if ((u4SetValFsErpsRingConfigExtMainRingId == 0)
            ||
            (pNextRingCfgInfo->u4RingId !=
             u4SetValFsErpsRingConfigExtMainRingId))
        {
            pSubRingCfgEntry =
                ErpsGetSubRingEntry (pNextRingCfgInfo, u4FsErpsRingId);
            if (pSubRingCfgEntry != NULL)
            {
                TMO_SLL_Delete (&(pNextRingCfgInfo->SubRingListHead),
                                &(pSubRingCfgEntry->SubRingNextNode));

                if (MemReleaseMemBlock (gErpsGlobalInfo.SubRingPoolId,
                                        (UINT1 *) pSubRingCfgEntry) !=
                    MEM_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                return SNMP_SUCCESS;
            }
        }

        RingConfigInfo.u4ContextId = pNextRingCfgInfo->u4ContextId;
        RingConfigInfo.u4RingId = pNextRingCfgInfo->u4RingId;
        pNextRingCfgInfo = ErpsRingGetNextNodeFromRingTable (&RingConfigInfo);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigExtVCRecoveryPeriodicTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigExtVCRecoveryPeriodicTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigExtVCRecoveryPeriodicTime (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsErpsContextId,
                                                    UINT4 u4FsErpsRingId,
                                                    UINT4
                                                    u4TestValFsErpsRingConfigExtVCRecoveryPeriodicTime)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if (u4TestValFsErpsRingConfigExtVCRecoveryPeriodicTime >
        ERPS_PERIODIC_TMR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the Ring Port2 is 0 then that ring is considered as sub ring 
     * This configuration is applicable only for the sub ring */
    if (pRingInfo->u4Port2IfIndex != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_NOT_A_SUBRING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingConfigExtMainRingId
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingConfigExtMainRingId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingConfigExtMainRingId (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsErpsContextId,
                                        UINT4 u4FsErpsRingId,
                                        UINT4
                                        u4TestValFsErpsRingConfigExtMainRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pMainRingInfo = NULL;

    /* The u4TestValFsErpsRingConfigExtMainRingId can be zero; Hence
     * validation is not done for MIN RING ID */
    if (u4TestValFsErpsRingConfigExtMainRingId > ERPS_MAX_RING_ID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the Ring Port2 is 0 then that ring is considered as sub ring 
     * This configuration is applicable only for the sub ring */
    if (pRingInfo->u4Port2IfIndex != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_NOT_A_SUBRING);
        return SNMP_FAILURE;
    }

    /* If the value is set to zero then the sub-ring id has to be 
     * removed from the sub-ring list of the older main ring; 
     * So don't validate the Main Ring Info*/
    if (u4TestValFsErpsRingConfigExtMainRingId == 0)
    {
        return SNMP_SUCCESS;
    }
    /* If the Main Ring does not exist return failure */
    pMainRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                                  u4TestValFsErpsRingConfigExtMainRingId,
                                                  pu4ErrorCode);

    if (pMainRingInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MAIN_RING_NOT_EXISTS);
        return SNMP_FAILURE;
    }

    /* If the maximum number of sub-rings have exceeded 
     * then return failure */

    if (TMO_SLL_Count (&pMainRingInfo->SubRingListHead) == ERPS_MAX_SUB_RINGS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_SUB_RING_LIST_EXCEEDED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingConfigExtTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingConfigExtTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingTcPropTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
                FsErpsRingTcPropRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingTcPropTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsMemFailCount
 Input       :  The Indices

                The Object 
                retValFsErpsMemFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsMemFailCount (UINT4 *pu4RetValFsErpsMemFailCount)
{
    *pu4RetValFsErpsMemFailCount = gErpsGlobalInfo.u4MemFailCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsBufFailCount
 Input       :  The Indices

                The Object 
                retValFsErpsBufFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsBufFailCount (UINT4 *pu4RetValFsErpsBufFailCount)
{

    *pu4RetValFsErpsBufFailCount = gErpsGlobalInfo.u4BuffferAllocFailCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsTimerFailCount
 Input       :  The Indices

                The Object 
                retValFsErpsTimerFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsTimerFailCount (UINT4 *pu4RetValFsErpsTimerFailCount)
{
    *pu4RetValFsErpsTimerFailCount = gErpsGlobalInfo.u4TimerFailCount;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingStatsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingStatsTable (UINT4 u4FsErpsContextId,
                                              UINT4 u4FsErpsRingId)
{
    return (nmhValidateIndexInstanceFsErpsRingTable (u4FsErpsContextId,
                                                     u4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingStatsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingStatsTable (UINT4 *pu4FsErpsContextId,
                                      UINT4 *pu4FsErpsRingId)
{
    return (nmhGetFirstIndexFsErpsRingTable (pu4FsErpsContextId,
                                             pu4FsErpsRingId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingStatsTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingStatsTable (UINT4 u4FsErpsContextId,
                                     UINT4 *pu4NextFsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4 *pu4NextFsErpsRingId)
{
    return (nmhGetNextIndexFsErpsRingTable (u4FsErpsContextId,
                                            pu4NextFsErpsContextId,
                                            u4FsErpsRingId,
                                            pu4NextFsErpsRingId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingClearRingStats
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingClearRingStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingClearRingStats (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                INT4 *pi4RetValFsErpsRingClearRingStats)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingClearRingStats = OSIX_FALSE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsPduSentCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsPduSentCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsPduSentCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsPduSentCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsPduSentCount =
        pRingInfo->Port1Stats.u4RapsSentCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsPduSentCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsPduSentCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsPduSentCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsPduSentCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsPduSentCount =
        pRingInfo->Port2Stats.u4RapsSentCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsPduRcvdCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsPduRcvdCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsPduRcvdCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsPduRcvdCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsPduRcvdCount =
        pRingInfo->Port1Stats.u4RapsReceivedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsPduRcvdCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsPduRcvdCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsPduRcvdCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsPduRcvdCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsPduRcvdCount =
        pRingInfo->Port2Stats.u4RapsReceivedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsPduDiscardCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsPduDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsPduDiscardCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort1RapsPduDiscardCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsPduDiscardCount =
        pRingInfo->Port1Stats.u4RapsDiscardCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsPduDiscardCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsPduDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsPduDiscardCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort2RapsPduDiscardCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsPduDiscardCount =
        pRingInfo->Port2Stats.u4RapsDiscardCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1BlockedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1BlockedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1BlockedCount (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   UINT4 *pu4RetValFsErpsRingPort1BlockedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1BlockedCount =
        pRingInfo->Port1Stats.u4BlockedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2BlockedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2BlockedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2BlockedCount (UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   UINT4 *pu4RetValFsErpsRingPort2BlockedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2BlockedCount =
        pRingInfo->Port2Stats.u4BlockedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1UnblockedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1UnblockedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1UnblockedCount (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingPort1UnblockedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1UnblockedCount =
        pRingInfo->Port1Stats.u4UnblockedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2UnblockedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2UnblockedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2UnblockedCount (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingPort2UnblockedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2UnblockedCount =
        pRingInfo->Port2Stats.u4UnblockedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1FailedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1FailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1FailedCount (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  UINT4 *pu4RetValFsErpsRingPort1FailedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1FailedCount = pRingInfo->Port1Stats.u4FailedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2FailedCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2FailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2FailedCount (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  UINT4 *pu4RetValFsErpsRingPort2FailedCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2FailedCount = pRingInfo->Port2Stats.u4FailedCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RecoveredCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RecoveredCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RecoveredCount (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingPort1RecoveredCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RecoveredCount =
        pRingInfo->Port1Stats.u4RecoveredCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RecoveredCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RecoveredCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RecoveredCount (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingPort2RecoveredCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RecoveredCount =
        pRingInfo->Port2Stats.u4RecoveredCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1VersionDiscardCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1VersionDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1VersionDiscardCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort1VersionDiscardCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1VersionDiscardCount =
        pRingInfo->Port1Stats.u4VersionDiscardCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2VersionDiscardCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2VersionDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2VersionDiscardCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort2VersionDiscardCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2VersionDiscardCount =
        pRingInfo->Port2Stats.u4VersionDiscardCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsFSPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsFSPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsFSPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsFSPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsFSPduRxCount =
        pRingInfo->Port1Stats.u4FSPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsFSPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsFSPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsFSPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsFSPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsFSPduTxCount =
        pRingInfo->Port1Stats.u4FSPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsFSPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsFSPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsFSPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsFSPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsFSPduRxCount =
        pRingInfo->Port2Stats.u4FSPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsFSPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsFSPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsFSPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsFSPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsFSPduTxCount =
        pRingInfo->Port2Stats.u4FSPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsMSPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsMSPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsMSPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsMSPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsMSPduRxCount =
        pRingInfo->Port1Stats.u4MSPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsMSPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsMSPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsMSPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsMSPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsMSPduTxCount =
        pRingInfo->Port1Stats.u4MSPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsMSPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsMSPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsMSPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsMSPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsMSPduRxCount =
        pRingInfo->Port2Stats.u4MSPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsMSPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsMSPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsMSPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsMSPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsMSPduTxCount =
        pRingInfo->Port2Stats.u4MSPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsEventPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsEventPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsEventPduRxCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort1RapsEventPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsEventPduRxCount =
        pRingInfo->Port1Stats.u4EventPduRxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsEventPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsEventPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsEventPduTxCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort1RapsEventPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsEventPduTxCount =
        pRingInfo->Port1Stats.u4EventPduTxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsEventPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsEventPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsEventPduRxCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort2RapsEventPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsEventPduRxCount =
        pRingInfo->Port2Stats.u4EventPduRxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsEventPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsEventPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsEventPduTxCount (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingPort2RapsEventPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsEventPduTxCount =
        pRingInfo->Port2Stats.u4EventPduTxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsSFPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsSFPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsSFPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsSFPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsSFPduRxCount =
        pRingInfo->Port1Stats.u4SFPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsSFPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsSFPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsSFPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsSFPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsSFPduTxCount =
        pRingInfo->Port1Stats.u4SFPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsSFPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsSFPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsSFPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsSFPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsSFPduRxCount =
        pRingInfo->Port2Stats.u4SFPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsSFPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsSFPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsSFPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsSFPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsSFPduTxCount =
        pRingInfo->Port2Stats.u4SFPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsNRPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsNRPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsNRPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsNRPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsNRPduRxCount =
        pRingInfo->Port1Stats.u4NRPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsNRPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsNRPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsNRPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1RapsNRPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsNRPduTxCount =
        pRingInfo->Port1Stats.u4NRPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsNRPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsNRPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsNRPduRxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsNRPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsNRPduRxCount =
        pRingInfo->Port2Stats.u4NRPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsNRPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsNRPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsNRPduTxCount (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2RapsNRPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsNRPduTxCount =
        pRingInfo->Port2Stats.u4NRPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsNRRBPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsNRRBPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsNRRBPduRxCount (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         UINT4
                                         *pu4RetValFsErpsRingPort1RapsNRRBPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsNRRBPduRxCount =
        pRingInfo->Port1Stats.u4NRRBPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1RapsNRRBPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1RapsNRRBPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1RapsNRRBPduTxCount (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         UINT4
                                         *pu4RetValFsErpsRingPort1RapsNRRBPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1RapsNRRBPduTxCount =
        pRingInfo->Port1Stats.u4NRRBPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsNRRBPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsNRRBPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsNRRBPduRxCount (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         UINT4
                                         *pu4RetValFsErpsRingPort2RapsNRRBPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsNRRBPduRxCount =
        pRingInfo->Port2Stats.u4NRRBPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2RapsNRRBPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2RapsNRRBPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2RapsNRRBPduTxCount (UINT4 u4FsErpsContextId,
                                         UINT4 u4FsErpsRingId,
                                         UINT4
                                         *pu4RetValFsErpsRingPort2RapsNRRBPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2RapsNRRBPduTxCount =
        pRingInfo->Port2Stats.u4NRRBPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingGeneratedTrapsCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingGeneratedTrapsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingGeneratedTrapsCount (UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     UINT4
                                     *pu4RetValFsErpsRingGeneratedTrapsCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingGeneratedTrapsCount = pRingInfo->u4TrapsCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1DefectEncTimeSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1DefectEncTimeSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1DefectEncTimeSec (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort1DefectEncTimeSec)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1DefectEncTimeSec =
        pRingInfo->perf.u4Port1DefectEncounteredTimeSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2DefectEncTimeSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2DefectEncTimeSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2DefectEncTimeSec (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingPort2DefectEncTimeSec)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2DefectEncTimeSec =
        pRingInfo->perf.u4Port2DefectEncounteredTimeSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1DefectClearedTimeSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1DefectClearedTimeSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1DefectClearedTimeSec (UINT4 u4FsErpsContextId,
                                           UINT4 u4FsErpsRingId,
                                           UINT4
                                           *pu4RetValFsErpsRingPort1DefectClearedTimeSec)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort1DefectClearedTimeSec =
        pRingInfo->perf.u4Port1DefectClearedTimeSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2DefectClearedTimeSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2DefectClearedTimeSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2DefectClearedTimeSec (UINT4 u4FsErpsContextId,
                                           UINT4 u4FsErpsRingId,
                                           UINT4
                                           *pu4RetValFsErpsRingPort2DefectClearedTimeSec)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPort2DefectClearedTimeSec =
        pRingInfo->perf.u4Port2DefectClearedTimeSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRplPortStatChgTimeSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRplPortStatChgTimeSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRplPortStatChgTimeSec (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingRplPortStatChgTimeSec)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRplPortStatChgTimeSec =
        pRingInfo->perf.u4RplPortStatusChangedTimeSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRplNbrPortStatChgTime
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingRplNbrPortStatChgTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRplNbrPortStatChgTime (UINT4 u4FsErpsContextId,
                                       UINT4 u4FsErpsRingId,
                                       UINT4
                                       *pu4RetValFsErpsRingRplNbrPortStatChgTime)
{

    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRplNbrPortStatChgTime =
        pRingInfo->perf.u4RplNeighborPortStatusChangedTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingDistPortPduRxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingDistPortPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingDistPortPduRxCount (UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4
                                    *pu4RetValFsErpsRingDistPortPduRxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingDistPortPduRxCount =
        pRingInfo->u4DistributingPortRxCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingDistPortPduTxCount
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingDistPortPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingDistPortPduTxCount (UINT4 u4FsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4
                                    *pu4RetValFsErpsRingDistPortPduTxCount)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingDistPortPduTxCount =
        pRingInfo->u4DistributingPortTxCount;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingClearRingStats
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingClearRingStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingClearRingStats (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                INT4 i4SetValFsErpsRingClearRingStats)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsErpsRingClearRingStats == OSIX_TRUE)
    {
        ErpsRingClearRingStats (pRingInfo);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingClearRingStats
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingClearRingStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingClearRingStats (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                   UINT4 u4FsErpsRingId,
                                   INT4 i4TestValFsErpsRingClearRingStats)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId, u4FsErpsRingId,
                                              pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsErpsRingClearRingStats != ERPS_SNMP_TRUE) &&
        (i4TestValFsErpsRingClearRingStats != ERPS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingStatsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingStatsTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingServiceType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingServiceType (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 *pi4RetValFsErpsRingServiceType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingServiceType = (INT4) pRingInfo->u1ServiceType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingServiceType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingServiceType (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                             INT4 i4SetValFsErpsRingServiceType)
{
    tErpsRingInfo      *pRingInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*To avoid the flushing of sub-port list, if user configures service type MPLS_LSP_PW again */
    if (i4SetValFsErpsRingServiceType == ERPS_SERVICE_MPLS_LSP_PW &&
        pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
    {
        ERPS_GLOBAL_TRC
            ("Ring is already configured with service type MPLS-LSP-PW\r\n");
        return SNMP_SUCCESS;
    }

    pRingInfo->u1ServiceType = i4SetValFsErpsRingServiceType;

    if (i4SetValFsErpsRingServiceType == ERPS_SERVICE_MPLS_LSP_PW)
    {
        /*Allocate memory for LSP PW info structure */

        if (pRingInfo->pErpsLspPwInfo == NULL)
        {
            if ((pRingInfo->pErpsLspPwInfo =
                 MemAllocMemBlk (gErpsGlobalInfo.LspPwInfoListId)) == NULL)
            {
                ERPS_GLOBAL_TRC
                    ("nmhSetFsErpsRingServiceType: Memory Allocation"
                     "failed for Lsp Pw Info List\r\n");

                return SNMP_FAILURE;
            }
            MEMSET (pRingInfo->pErpsLspPwInfo, 0, sizeof (tErpsLspPwInfo));

            /* RBTree Creation for SubPortList Table */
            ERPS_CREATE_SUBPORTLIST_TABLE (pRingInfo->pErpsLspPwInfo, i1RetVal);
        }

        MEMSET (pRingInfo->pErpsLspPwInfo, 0, sizeof (tErpsLspPwInfo));
        pRingInfo->pErpsLspPwInfo->u1Port1TunnelStatus =
            ERPS_TUNNEL_STATE_FAILED;
        pRingInfo->pErpsLspPwInfo->u1Port2TunnelStatus =
            ERPS_TUNNEL_STATE_FAILED;
    }
    UNUSED_PARAM (i1RetVal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingServiceType
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingServiceType (UINT4 *pu4ErrorCode, UINT4 u4FsErpsContextId,
                                UINT4 u4FsErpsRingId,
                                INT4 i4TestValFsErpsRingServiceType)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Validate the Service types */
    if ((i4TestValFsErpsRingServiceType != ERPS_SERVICE_VLAN) &&
        (i4TestValFsErpsRingServiceType != ERPS_SERVICE_MPLS_LSP) &&
        (i4TestValFsErpsRingServiceType != ERPS_SERVICE_MPLS_PW) &&
        (i4TestValFsErpsRingServiceType != ERPS_SERVICE_MPLS_LSP_PW))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (u4FsErpsContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if ((pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_CFM) &&
        (i4TestValFsErpsRingServiceType == ERPS_SERVICE_MPLS_LSP_PW))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Service type as MPLS-LSP-PW is not supported for monitoring mechanism as CFM\n");
        CLI_SET_ERR (CLI_ERPS_ERR_MONITOR_INVALID);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort1SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort1SubPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort1SubPortList (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsErpsRingPort1SubPortList)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tPortListExt       *pPort1List = NULL;
    UINT4               u4Index = 0;
    UINT1               bResult = OSIX_FALSE;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*subport List is supported only for ERPS_SERVICE_MPLS_LSP_PW */

    MEMSET (pRetValFsErpsRingPort1SubPortList->pu1_OctetList, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    if (pRingInfo->u1ServiceType != ERPS_SERVICE_MPLS_LSP_PW)
    {
        return SNMP_SUCCESS;
    }

    pPort1List = (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

    if (pPort1List == NULL)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhGetFsErpsRingPort1SubPortList :"
                          "Memory allocation failed for PortList",
                          u4FsErpsRingId);

        return SNMP_FAILURE;
    }

    MEMSET (*pPort1List, 0, sizeof (tPortListExt));

    for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
    {
        ERPS_IS_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo, u4Index, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue */
            continue;
        }

        OSIX_BITLIST_SET_BIT ((*pPort1List),
                              (u4Index + CFA_MIN_PSW_IF_INDEX - 1),
                              sizeof (tPortListExt));

    }

    if (OSIX_FAILURE ==
        ErpsUtilGetOctetStrFromPortList (pPort1List,
                                         pRetValFsErpsRingPort1SubPortList))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhGetFsErpsRingPort2SubPortList :"
                          "Converting Port list into Octet String failed",
                          u4FsErpsRingId);

        FsUtilReleaseBitList ((UINT1 *) pPort1List);

        return SNMP_FAILURE;
    }

    /* Update the length of the OctetList */
    pRetValFsErpsRingPort1SubPortList->i4_Length =
        STRLEN (pRetValFsErpsRingPort1SubPortList->pu1_OctetList);

    FsUtilReleaseBitList ((UINT1 *) pPort1List);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPort1SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPort1SubPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPort1SubPortList (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsErpsRingPort1SubPortList)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4              *pu4PortArray = NULL;
    UINT4               u4Index = 0;

    pu4PortArray = (UINT4 *) MemAllocMemBlk (ERPS_PORT_ARRAY_POOLID ());
    if (pu4PortArray == NULL)
    {
        ERPS_GLOBAL_TRC ("Memory allocation failed for PortArray\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pu4PortArray, 0, sizeof (UINT4) * MAX_ERPS_SUB_PORT_LIST_SIZE);

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);
    if (pRingInfo == NULL)
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Allow setting of empty string as SubPortList. */
    if (MEMCMP (pSetValFsErpsRingPort1SubPortList->pu1_OctetList, "",
                pSetValFsErpsRingPort1SubPortList->i4_Length) == 0)
    {
        if (pRingInfo->pErpsLspPwInfo != NULL)
        {
            ERPS_RESET_ALL_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo);
        }
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_SUCCESS;
    }

    /* Converts the OctetList to array of ports. 
     * It is required to identify the bit to be set in the 
     * PortList,  pRingInfo->Port1SubPortList. 
     */
    if (OSIX_FAILURE ==
        ConvertStrToPortArray (pSetValFsErpsRingPort1SubPortList->pu1_OctetList,
                               pu4PortArray, MAX_ERPS_SUB_PORT_LIST_SIZE,
                               ERPS_SYS_DEF_MAX_INTERFACES))
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Scan for all the Pseudowire interfaces and set the bit information
     * in the portList. 
     */

    for (u4Index = 0; u4Index < MAX_ERPS_SUB_PORT_LIST_SIZE; u4Index++)
    {
        if (pu4PortArray[u4Index] == 0)
        {
            break;
        }

        ERPS_SET_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo,
                                (pu4PortArray[u4Index] - CFA_MIN_PSW_IF_INDEX +
                                 1));
    }                            /* End of for */

    MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPort1SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPort1SubPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPort1SubPortList (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsErpsRingPort1SubPortList)
{
    UINT4              *pu4PortArray = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1              *pau1OctetSting = NULL;
    UINT4               u4Index = 0;

    pu4PortArray = (UINT4 *) MemAllocMemBlk (ERPS_PORT_ARRAY_POOLID ());
    if (pu4PortArray == NULL)
    {
        ERPS_GLOBAL_TRC ("Memory allocation failed for PortArray\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pu4PortArray, 0, sizeof (UINT4) * MAX_ERPS_SUB_PORT_LIST_SIZE);

    if (u4FsErpsContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId, u4FsErpsRingId,
                                              pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        CLI_SET_ERR (CLI_ERPS_CONTEXT_NOT_PRESENT);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Block the configuration when the service type is other than MPLS_LSP_PW 
     */
    if (ERPS_SERVICE_MPLS_LSP_PW != pRingInfo->u1ServiceType)
    {
        CLI_SET_ERR (CLI_ERPS_ERR_INVALID_SERVICE);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    if (MEMCMP (pTestValFsErpsRingPort1SubPortList->pu1_OctetList, "",
                pTestValFsErpsRingPort1SubPortList->i4_Length) == 0)
    {
        /* Allow the default value for configuration */
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_SUCCESS;
    }

    pau1OctetSting =
        (UINT1 *) MemAllocMemBlk (gErpsGlobalInfo.ErpsCliMemblkBufPoolId);
    if (NULL == pau1OctetSting)
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    MEMSET (pau1OctetSting, 0, sizeof (tErpsCliMemBlkBuf));

    MEMCPY (pau1OctetSting,
            pTestValFsErpsRingPort1SubPortList->pu1_OctetList,
            pTestValFsErpsRingPort1SubPortList->i4_Length);

    /* Conver the OctetList to array of ports to validate the 
     * interface indices. 
     */
    if (OSIX_FAILURE == ConvertStrToPortArray (pau1OctetSting,
                                               pu4PortArray,
                                               MAX_ERPS_SUB_PORT_LIST_SIZE,
                                               ERPS_SYS_DEF_MAX_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                            (UINT1 *) pau1OctetSting);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    for (u4Index = 0; u4Index < MAX_ERPS_SUB_PORT_LIST_SIZE; u4Index++)
    {
        if (pu4PortArray[u4Index] == 0)
        {
            /* Break the loop if all the valid indices are validated. */
            break;
        }
        /* If the indices range of the subPortList falls beyond
         * the pseudowire range, throw CLI error.
         */
        if ((pu4PortArray[u4Index] < CFA_MIN_PSW_IF_INDEX) ||
            (pu4PortArray[u4Index] > CFA_MAX_PSW_IF_INDEX))
        {
            CLI_SET_ERR (CLI_ERPS_ERR_SUBPORTLIST_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                                (UINT1 *) pau1OctetSting);
            MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (),
                                (UINT1 *) pu4PortArray);
            return SNMP_FAILURE;
        }
    }                            /* End of for */

    MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                        (UINT1 *) pau1OctetSting);
    MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPort2SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPort2SubPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPort2SubPortList (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsErpsRingPort2SubPortList)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tPortListExt       *pPort2List = NULL;
    UINT4               u4Index = 0;
    UINT1               bResult = OSIX_FALSE;

    /* Inside this function the following are validated. 
     * - Context Info
     * - RingInfo */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*subport List is supported only for ERPS_SERVICE_MPLS_LSP_PW */

    MEMSET (pRetValFsErpsRingPort2SubPortList->pu1_OctetList, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    if (pRingInfo->u1ServiceType != ERPS_SERVICE_MPLS_LSP_PW)
    {
        return SNMP_SUCCESS;
    }

    pPort2List = (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));

    if (pPort2List == NULL)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhGetFsErpsRingPort2SubPortList :"
                          "Memory allocation failed for PortList",
                          u4FsErpsRingId);

        return SNMP_FAILURE;
    }

    MEMSET (*pPort2List, 0, sizeof (tPortListExt));

    for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
    {
        ERPS_IS_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo, u4Index, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue */
            continue;
        }

        OSIX_BITLIST_SET_BIT ((*pPort2List),
                              (u4Index + CFA_MIN_PSW_IF_INDEX - 1),
                              sizeof (tPortListExt));
    }

    if (OSIX_FAILURE ==
        ErpsUtilGetOctetStrFromPortList (pPort2List,
                                         pRetValFsErpsRingPort2SubPortList))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhGetFsErpsRingPort2SubPortList :"
                          "Converting Port list into Octet String failed",
                          u4FsErpsRingId);

        FsUtilReleaseBitList ((UINT1 *) pPort2List);
        return SNMP_FAILURE;
    }

    /* Update the length of the OctetList */
    pRetValFsErpsRingPort2SubPortList->i4_Length =
        STRLEN (pRetValFsErpsRingPort2SubPortList->pu1_OctetList);

    FsUtilReleaseBitList ((UINT1 *) pPort2List);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPort2SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPort2SubPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPort2SubPortList (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsErpsContextId,
                                     UINT4 u4FsErpsRingId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsErpsRingPort2SubPortList)
{

    tErpsRingInfo      *pRingInfo = NULL;
    UINT4              *pu4PortArray = NULL;
    UINT4               u4Index = 0;
    UINT1              *pau1OctetSting = NULL;

    pu4PortArray = (UINT4 *) MemAllocMemBlk (ERPS_PORT_ARRAY_POOLID ());
    if (pu4PortArray == NULL)
    {
        ERPS_GLOBAL_TRC ("Memory allocation failed for PortArray\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pu4PortArray, 0, sizeof (UINT4) * MAX_ERPS_SUB_PORT_LIST_SIZE);

    if (u4FsErpsContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId, u4FsErpsRingId,
                                              pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        CLI_SET_ERR (CLI_ERPS_CONTEXT_NOT_PRESENT);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Block the configuration when the service type is other than MPLS_LSP_PW 
     */
    if (ERPS_SERVICE_MPLS_LSP_PW != pRingInfo->u1ServiceType)
    {
        CLI_SET_ERR (CLI_ERPS_ERR_INVALID_SERVICE);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Allow the default value for configuration */
    if (MEMCMP (pTestValFsErpsRingPort2SubPortList->pu1_OctetList, "",
                pTestValFsErpsRingPort2SubPortList->i4_Length) == 0)
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_SUCCESS;
    }

    pau1OctetSting =
        (UINT1 *) MemAllocMemBlk (gErpsGlobalInfo.ErpsCliMemblkBufPoolId);
    if (NULL == pau1OctetSting)
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    MEMSET (pau1OctetSting, 0, sizeof (tErpsCliMemBlkBuf));

    MEMCPY (pau1OctetSting, pTestValFsErpsRingPort2SubPortList->pu1_OctetList,
            pTestValFsErpsRingPort2SubPortList->i4_Length);

    /* Conver the OctetList to array of ports to validate the 
     * interface indices. 
     */
    if (OSIX_FAILURE == ConvertStrToPortArray (pau1OctetSting,
                                               pu4PortArray,
                                               MAX_ERPS_SUB_PORT_LIST_SIZE,
                                               ERPS_SYS_DEF_MAX_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                            (UINT1 *) pau1OctetSting);
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    for (u4Index = 0; u4Index < MAX_ERPS_SUB_PORT_LIST_SIZE; u4Index++)
    {
        if (pu4PortArray[u4Index] == 0)
        {
            /* Break the loop if all the valid indices are validated. */
            break;
        }
        /* If the indices range of the subPortList falls beyond
         * the pseudowire range, throw CLI error.
         */
        if ((pu4PortArray[u4Index] < CFA_MIN_PSW_IF_INDEX) &&
            (pu4PortArray[u4Index] > CFA_MAX_PSW_IF_INDEX))
        {
            CLI_SET_ERR (CLI_ERPS_ERR_SUBPORTLIST_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                                (UINT1 *) pau1OctetSting);
            MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (),
                                (UINT1 *) pu4PortArray);
            return SNMP_FAILURE;
        }
    }                            /* End of for */

    MemReleaseMemBlock (gErpsGlobalInfo.ErpsCliMemblkBufPoolId,
                        (UINT1 *) pau1OctetSting);
    MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPort2SubPortList
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPort2SubPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPort2SubPortList (UINT4 u4FsErpsContextId, UINT4 u4FsErpsRingId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsErpsRingPort2SubPortList)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4              *pu4PortArray = NULL;
    UINT4               u4Index = 0;

    pu4PortArray = (UINT4 *) MemAllocMemBlk (ERPS_PORT_ARRAY_POOLID ());
    if (pu4PortArray == NULL)
    {
        ERPS_GLOBAL_TRC ("Memory allocation failed for PortArray\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pu4PortArray, 0, sizeof (UINT4) * MAX_ERPS_SUB_PORT_LIST_SIZE);

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);
    if (pRingInfo == NULL)
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Allow setting of empty string as SubPortList. */
    if (MEMCMP (pSetValFsErpsRingPort2SubPortList->pu1_OctetList, "",
                pSetValFsErpsRingPort2SubPortList->i4_Length) == 0)
    {
        if (pRingInfo->pErpsLspPwInfo != NULL)
        {
            ERPS_RESET_ALL_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo);
        }
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_SUCCESS;
    }

    /* Converts the OctetList to array of ports.
     * It is required to identify the bit to be set in the
     * PortList,  pRingInfo->Port2SubPortList.
     */
    if (OSIX_FAILURE ==
        ConvertStrToPortArray (pSetValFsErpsRingPort2SubPortList->pu1_OctetList,
                               pu4PortArray, MAX_ERPS_SUB_PORT_LIST_SIZE,
                               ERPS_SYS_DEF_MAX_INTERFACES))
    {
        MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
        return SNMP_FAILURE;
    }

    /* Scan for all the Pseudowire interfaces and set the bit information
     * in the portList. 
     */
    for (u4Index = 0; u4Index < MAX_ERPS_SUB_PORT_LIST_SIZE; u4Index++)
    {
        if (pu4PortArray[u4Index] == 0)
        {
            break;
        }
        ERPS_SET_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo,
                                (pu4PortArray[u4Index] - CFA_MIN_PSW_IF_INDEX +
                                 1));
    }                            /* End of for */

    MemReleaseMemBlock (ERPS_PORT_ARRAY_POOLID (), (UINT1 *) pu4PortArray);
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsLwUtilValidateCxtAndRing
 *
 * DESCRIPTION      : This function will check whether the context is created
 *                    and also the system control of the context. After that
 *                    this function will check for the existance of Ring entry
 *                    to the given Ring Id in that context.
 *
 *                    This function will be called by the low level        
 *                    routines and this function will take care of filling the
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4RingId      - Ring Group Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 *
 * RETURNS          : Pointer to the tErpsRingInfo structure.
 *
 **************************************************************************/
PRIVATE tErpsRingInfo *
ErpsLwUtilValidateCxtAndRing (UINT4 u4ContextId, UINT4 u4RingId,
                              UINT4 *pu4ErrorCode)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingInfo;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    if (ErpsLwUtilValidateContextInfo (u4ContextId, pu4ErrorCode) == NULL)
    {
        return NULL;
    }

    if (u4RingId == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return NULL;
    }

    RingInfo.u4ContextId = u4ContextId;
    RingInfo.u4RingId = u4RingId;

    pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("Ring Group not created\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }

    return pRingInfo;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsLwUtilRingCfgNotInServ                       */
/*                                                                           */
/*    Description         : This function will configure the rowstatus of    */
/*                          ring table in to NotInService once all the       */
/*                          mandatory parameters are configured.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to newly created ring node.  */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ErpsLwUtilRingCfgNotInServ (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
    {
        if ((pRingInfo->u4Port1IfIndex != 0) &&
            (pRingInfo->RapsVlanId != 0) &&
            (pRingInfo->CfmEntry.u1RowStatus == ACTIVE) &&
            (pRingInfo->MplsPwInfo.u1RowStatus == ACTIVE))
        {
            pRingInfo->u1RowStatus = NOT_IN_SERVICE;
        }
    }
    else
    {
        if ((pRingInfo->u4Port1IfIndex != 0) &&
            (pRingInfo->RapsVlanId != 0) &&
            (pRingInfo->CfmEntry.u1RowStatus == ACTIVE))
        {
            pRingInfo->u1RowStatus = NOT_IN_SERVICE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsLwUtilRingCfmCfgNotInServ                    */
/*                                                                           */
/*    Description         : This function will configure the rowstatus of    */
/*                          ring cfm table in to NotInService once all the   */
/*                          mandatory parameters are configured.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to newly created ring node.  */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ErpsLwUtilRingCfmCfgNotInServ (tErpsRingInfo * pRingInfo)
{
    if ((pRingInfo->CfmEntry.u4Port1MEGId != 0) &&
        (pRingInfo->CfmEntry.u4Port1MEId != 0) &&
        (pRingInfo->CfmEntry.u4Port1MEPId != 0) &&
        (pRingInfo->CfmEntry.u4Port2MEGId != 0) &&
        (pRingInfo->CfmEntry.u4Port2MEId != 0) &&
        (pRingInfo->CfmEntry.u4Port2MEPId != 0))
    {
        pRingInfo->CfmEntry.u1RowStatus = NOT_IN_SERVICE;
    }
}

/* Low Level GET Routine for All Objects  */
/***************************************************************************
 * FUNCTION NAME    : ErpsLwUtilValidateContextInfo
 *
 * DESCRIPTION      : This function will check whether the context is created
 *                    and also the system control of the context.
 *
 *                    This function will be called by the nmhGet/nmhSet
 *                    routines and this function will take care of filling the
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 *
 * RETURNS          : Pointer to the tErpsContextInfo structure.
 *
 **************************************************************************/
PRIVATE tErpsContextInfo *
ErpsLwUtilValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERPS_ERR_MODULE_SHUTDOWN);
        return NULL;
    }

    if (ErpsUtilIsErpsStarted (u4ContextId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MODULE_SHUTDOWN);
        return NULL;
    }

    return pContextInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsLwValidateCfmEntry
 *
 * DESCRIPTION      : This function will check whether the ring info is
 *                    present and the CFM entry is in active state.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 *
 * RETURNS          : Pointer to the tErpsContextInfo structure.
 *
 **************************************************************************/
PRIVATE INT4
ErpsLwValidateCfmEntry (UINT4 u4FsErpsContextId,
                        UINT4 u4FsErpsRingId, UINT4 *pu4ErrorCode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return OSIX_FAILURE;
    }

    if (pRingInfo->CfmEntry.u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Row Status of CFM Entry is Active\n",
                          u4FsErpsRingId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_CFM_ROW_ACTIVE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRapsPort1DefectEncTimeNSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingRapsPort1DefectEncTimeNSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRapsPort1DefectEncTimeNSec (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            UINT4
                                            *pu4RetValFsErpsRingRapsPort1DefectEncTimeNSec)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRapsPort1DefectEncTimeNSec =
        pRingInfo->perf.u4RapsSFPort1DefectTimeNSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRapsPort1DefectClearedTimeNSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingRapsPort1DefectClearedTimeNSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRapsPort1DefectClearedTimeNSec (UINT4 u4FsErpsContextId,
                                                UINT4 u4FsErpsRingId,
                                                UINT4
                                                *pu4RetValFsErpsRingRapsPort1DefectClearedTimeNSec)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRapsPort1DefectClearedTimeNSec =
        pRingInfo->perf.u4RapsClearSFPort1DefectTimeNSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRapsPort2DefectEncTimeNSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingRapsPort2DefectEncTimeNSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRapsPort2DefectEncTimeNSec (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            UINT4
                                            *pu4RetValFsErpsRingRapsPort2DefectEncTimeNSec)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRapsPort2DefectEncTimeNSec =
        pRingInfo->perf.u4RapsSFPort2DefectTimeNSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRapsPort2DefectClearedTimeNSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingRapsPort2DefectClearedTimeNSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRapsPort2DefectClearedTimeNSec (UINT4 u4FsErpsContextId,
                                                UINT4 u4FsErpsRingId,
                                                UINT4
                                                *pu4RetValFsErpsRingRapsPort2DefectClearedTimeNSec)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRapsPort2DefectClearedTimeNSec =
        pRingInfo->perf.u4RapsClearSFPort2DefectTimeNSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingRapsRplPortStatChgTimeNSec
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingRapsRplPortStatChgTimeNSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingRapsRplPortStatChgTimeNSec (UINT4 u4FsErpsContextId,
                                            UINT4 u4FsErpsRingId,
                                            UINT4
                                            *pu4RetValFsErpsRingRapsRplPortStatChgTimeNSec)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingRapsRplPortStatChgTimeNSec =
        pRingInfo->perf.u4RplPortStatusChangedTimeNSec;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingDefectSwitchOverTimeMSec
 Description :  This function calculates the Defect switchover Time
                to milliseconds.
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingDefectSwitchOverTimeMSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingDefectSwitchOverTimeMSec (UINT4 u4FsErpsContextId,
                                          UINT4 u4FsErpsRingId,
                                          UINT4
                                          *pu4RetValFsErpsRingDefectSwitchOverTimeMSec)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4PortStausInSec = 0;
    UINT4               u4PortStausInNSec = 0;
    UINT4               u4TimeInMSec = 0;

    *pu4RetValFsErpsRingDefectSwitchOverTimeMSec = 0;
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*DefectSwitchOvertime is calculated in RPL node only */
    if (pRingInfo->u4RplIfIndex != 0)
    {
        u4PortStausInSec = pRingInfo->perf.u4RplPortStatusChangedTimeSec;
        u4PortStausInNSec = pRingInfo->perf.u4RplPortStatusChangedTimeNSec;

        /*If the RPL port status change time is greater than port1 defect encountered time in Seconds,
         *then , check the nano seconds of RPL port state change time and port defect encountered time.
         * If it less, then subtracat the seconds by 1 and Add the ERPS_NANO_SECONDS with port1 Nano seconds.*/
        if (pRingInfo->perf.u4Port1DefectEncounteredTimeSec != 0)
        {
            if (pRingInfo->perf.u4RplPortStatusChangedTimeSec >=
                pRingInfo->perf.u4Port1DefectEncounteredTimeSec)
            {
                if (pRingInfo->perf.u4RplPortStatusChangedTimeNSec <
                    pRingInfo->perf.u4RapsSFPort1DefectTimeNSec)
                {
                    u4PortStausInSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeSec;
                    u4PortStausInNSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeNSec;
                    u4PortStausInSec--;
                    u4PortStausInNSec += ERPS_NANO_SECONDS;
                }

                u4PortStausInSec =
                    u4PortStausInSec -
                    pRingInfo->perf.u4Port1DefectEncounteredTimeSec;

                u4PortStausInNSec =
                    u4PortStausInNSec -
                    pRingInfo->perf.u4RapsSFPort1DefectTimeNSec;

                u4TimeInMSec =
                    ((u4PortStausInSec * 1000) + (u4PortStausInNSec / 1000000));
                if ((u4PortStausInNSec > 0) && (u4PortStausInNSec < 1000000))
                {
                    *pu4RetValFsErpsRingDefectSwitchOverTimeMSec = ERPS_1_MS;
                }
                else
                {
                    *pu4RetValFsErpsRingDefectSwitchOverTimeMSec = u4TimeInMSec;
                }

            }
        }
        /*If the RPL port status change time is greater than port2 defect encountered time in Seconds,
         *then , check the nano seconds of RPL port state change time and port defect encountered time.
         * If it less, then subtract the seconds by 1 and Add the ERPS_NANO_SECONDS with port2 Nano seconds.*/
        else if (pRingInfo->perf.u4Port2DefectEncounteredTimeSec != 0)
        {
            if (pRingInfo->perf.u4RplPortStatusChangedTimeSec >=
                pRingInfo->perf.u4Port2DefectEncounteredTimeSec)
            {
                if (pRingInfo->perf.u4RplPortStatusChangedTimeNSec <
                    pRingInfo->perf.u4RapsSFPort2DefectTimeNSec)
                {
                    u4PortStausInSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeSec;
                    u4PortStausInNSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeNSec;
                    u4PortStausInSec--;
                    u4PortStausInNSec += ERPS_NANO_SECONDS;
                }

                u4PortStausInSec =
                    u4PortStausInSec -
                    pRingInfo->perf.u4Port2DefectEncounteredTimeSec;
                u4PortStausInNSec =
                    u4PortStausInNSec -
                    pRingInfo->perf.u4RapsSFPort2DefectTimeNSec;

                u4TimeInMSec =
                    ((u4PortStausInSec * 1000) + (u4PortStausInNSec / 1000000));
                if ((u4PortStausInNSec > 0) && (u4PortStausInNSec < 1000000))
                {
                    *pu4RetValFsErpsRingDefectSwitchOverTimeMSec = ERPS_1_MS;
                }
                else
                {
                    *pu4RetValFsErpsRingDefectSwitchOverTimeMSec = u4TimeInMSec;
                }

            }

        }

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingDefectClearedSwitchOverTimeMSec
 Description :  This function calculates the Defect Cleared Switchover Time
                to milliseconds.

 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object
                retValFsErpsRingDefectClearedSwitchOverTimeMSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingDefectClearedSwitchOverTimeMSec (UINT4 u4FsErpsContextId,
                                                 UINT4 u4FsErpsRingId,
                                                 UINT4
                                                 *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec)
{

    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4PortStausInSec = 0;
    UINT4               u4PortStausInNSec = 0;
    UINT4               u4TimeInMSec = 0;

    *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec = 0;
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    /*DefectClearSwitchOvertime is calculated in RPL node only */
    if (pRingInfo->u4RplIfIndex != 0)
    {
        u4PortStausInSec = pRingInfo->perf.u4RplPortStatusChangedTimeSec;
        u4PortStausInNSec = pRingInfo->perf.u4RplPortStatusChangedTimeNSec;
        /*If the RPL port status change time is greater than port1 defect cleared time in Seconds,
         *then , check the nano seconds of RPL port state change time and port defect cleared time.
         * If it less, then subtract the seconds by 1 and Add the ERPS_NANO_SECONDS with port1 Nano seconds.*/

        if (pRingInfo->perf.u4Port1DefectClearedTimeSec != 0)
        {
            if (pRingInfo->perf.u4RplPortStatusChangedTimeSec >=
                pRingInfo->perf.u4Port1DefectClearedTimeSec)
            {
                if (pRingInfo->perf.u4RplPortStatusChangedTimeNSec <
                    pRingInfo->perf.u4RapsClearSFPort1DefectTimeNSec)
                {
                    u4PortStausInSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeSec;
                    u4PortStausInNSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeNSec;
                    u4PortStausInSec--;
                    u4PortStausInNSec += ERPS_NANO_SECONDS;
                }

                u4PortStausInSec =
                    u4PortStausInSec -
                    pRingInfo->perf.u4Port1DefectClearedTimeSec;
                u4PortStausInNSec =
                    u4PortStausInNSec -
                    pRingInfo->perf.u4RapsClearSFPort1DefectTimeNSec;

                u4TimeInMSec =
                    ((u4PortStausInSec * 1000) + (u4PortStausInNSec / 1000000));
                if ((u4PortStausInNSec > 0) && (u4PortStausInNSec < 1000000))
                {
                    *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec =
                        ERPS_1_MS;
                }
                else
                {
                    *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec =
                        u4TimeInMSec;
                }

            }
        }
        /*If the RPL port status change time is greater than port2 defect cleared time in Seconds,
         *then , check the nano seconds of RPL port state change time and port defect cleared time.
         * If it less, then subtracat the seconds by 1 and Add the ERPS_NANO_SECONDS with port2 Nano seconds.*/

        else if (pRingInfo->perf.u4Port2DefectClearedTimeSec != 0)
        {
            if (pRingInfo->perf.u4RplPortStatusChangedTimeSec >=
                pRingInfo->perf.u4Port2DefectClearedTimeSec)
            {
                if (pRingInfo->perf.u4RplPortStatusChangedTimeNSec <
                    pRingInfo->perf.u4RapsClearSFPort2DefectTimeNSec)
                {
                    u4PortStausInSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeSec;
                    u4PortStausInNSec =
                        pRingInfo->perf.u4RplPortStatusChangedTimeNSec;
                    u4PortStausInSec--;
                    u4PortStausInNSec += ERPS_NANO_SECONDS;
                }

                u4PortStausInSec =
                    u4PortStausInSec -
                    pRingInfo->perf.u4Port2DefectClearedTimeSec;
                u4PortStausInNSec =
                    u4PortStausInNSec -
                    pRingInfo->perf.u4RapsClearSFPort2DefectTimeNSec;

                u4TimeInMSec =
                    ((u4PortStausInSec * 1000) + (u4PortStausInNSec / 1000000));
                if ((u4PortStausInNSec > 0) && (u4PortStausInNSec < 1000000))
                {
                    *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec =
                        ERPS_1_MS;
                }
                else
                {
                    *pu4RetValFsErpsRingDefectClearedSwitchOverTimeMSec =
                        u4TimeInMSec;
                }
            }

        }

    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsErpsRingMplsTable. */

/***************************************************************************
 * FUNCTION NAME    : ErpsLwValidateMplsEntry
 *
 * DESCRIPTION      : This function will check whether the ring info is
 *                    present and the Mpls Pw entry is in active state.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 *
 * RETURNS          : Pointer to the tErpsContextInfo structure.
 *
 **************************************************************************/
PRIVATE INT4
ErpsLwValidateMplsEntry (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 *pu4ErrorCode)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsLwUtilValidateCxtAndRing
        (u4FsErpsContextId, u4FsErpsRingId, pu4ErrorCode);

    if (pRingInfo == NULL)
    {
        /* As the function ErpsLwUtilValidateCxtAndRing itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return OSIX_FAILURE;
    }

    if (pRingInfo->MplsPwInfo.u1RowStatus == ACTIVE)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Row Status of Mpls Pw Info Entry is Active\n",
                          u4FsErpsRingId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MPLS_ROW_ACTIVE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsErpsRingMplsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsErpsRingMplsTable (UINT4 u4FsErpsContextId,
                                             UINT4 u4FsErpsRingId)
{
    tErpsRingInfo      *pRingInfo = NULL;

    /* Inside this function the following are validated 
     * - Ring Id
     * - Context
     * - Ring Table */
    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRingInfo->MplsPwInfo.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Mpls Table entry for Ring Group %d "
                          "not found\n", u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsErpsRingMplsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsErpsRingMplsTable (UINT4 *pu4FsErpsContextId,
                                     UINT4 *pu4FsErpsRingId)
{
    tErpsRingInfo      *pNextRingInfo = NULL;

    pNextRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pNextRingInfo != NULL)
    {
        if (pNextRingInfo->MplsPwInfo.u1RowStatus != 0)
        {
            *pu4FsErpsContextId = pNextRingInfo->u4ContextId;
            *pu4FsErpsRingId = pNextRingInfo->u4RingId;
            return SNMP_SUCCESS;
        }
        else
        {
            pNextRingInfo = ErpsRingGetNextNodeFromRingTable (pNextRingInfo);
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsErpsRingMplsTable
 Input       :  The Indices
                FsErpsContextId
                nextFsErpsContextId
                FsErpsRingId
                nextFsErpsRingId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsErpsRingMplsTable (UINT4 u4FsErpsContextId,
                                    UINT4 *pu4NextFsErpsContextId,
                                    UINT4 u4FsErpsRingId,
                                    UINT4 *pu4NextFsErpsRingId)
{
    tErpsRingInfo       RingInfo;
    tErpsRingInfo      *pNextRingMplsInfo = NULL;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    RingInfo.u4RingId = u4FsErpsRingId;
    RingInfo.u4ContextId = u4FsErpsContextId;

    pNextRingMplsInfo = ErpsRingGetNextNodeFromRingTable (&RingInfo);

    if (pNextRingMplsInfo != NULL)
    {
        if (pNextRingMplsInfo->MplsPwInfo.u1RowStatus != 0)
        {
            *pu4NextFsErpsContextId = pNextRingMplsInfo->u4ContextId;
            *pu4NextFsErpsRingId = pNextRingMplsInfo->u4RingId;
            return SNMP_SUCCESS;
        }
        else
        {
            pNextRingMplsInfo =
                ErpsRingGetNextNodeFromRingTable (pNextRingMplsInfo);
        }
    }
    UNUSED_PARAM (pNextRingMplsInfo);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsErpsRingPwVcId1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPwVcId1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPwVcId1 (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId,
                         UINT4 *pu4RetValFsErpsRingPwVcId1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPwVcId1 = pRingInfo->MplsPwInfo.u4PwVcId1;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingPwVcId2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingPwVcId2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingPwVcId2 (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId,
                         UINT4 *pu4RetValFsErpsRingPwVcId2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsErpsRingPwVcId2 = pRingInfo->MplsPwInfo.u4PwVcId2;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsErpsRingMplsRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                retValFsErpsRingMplsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsErpsRingMplsRowStatus (UINT4 u4FsErpsContextId,
                               UINT4 u4FsErpsRingId,
                               INT4 *pi4RetValFsErpsRingMplsRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* When MplsPwInfo is not configured, RowStatus will be initialised with 0 */

    if (pRingInfo->MplsPwInfo.u1RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsErpsRingMplsRowStatus = pRingInfo->MplsPwInfo.u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsErpsRingPwVcId1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPwVcId1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPwVcId1 (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 u4SetValFsErpsRingPwVcId1)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->MplsPwInfo.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Mpls Pw Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->MplsPwInfo.u4PwVcId1 = u4SetValFsErpsRingPwVcId1;

    if ((pRingInfo->MplsPwInfo.u4PwVcId1 != 0) &&
        (pRingInfo->MplsPwInfo.u4PwVcId2 != 0))
    {
        pRingInfo->MplsPwInfo.u1RowStatus = NOT_IN_SERVICE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingPwVcId2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingPwVcId2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingPwVcId2 (UINT4 u4FsErpsContextId,
                         UINT4 u4FsErpsRingId, UINT4 u4SetValFsErpsRingPwVcId2)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->MplsPwInfo.u1RowStatus == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d Mpls Pw Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    pRingInfo->MplsPwInfo.u4PwVcId2 = u4SetValFsErpsRingPwVcId2;

    if ((pRingInfo->MplsPwInfo.u4PwVcId1 != 0) &&
        (pRingInfo->MplsPwInfo.u4PwVcId2 != 0))
    {
        pRingInfo->MplsPwInfo.u1RowStatus = NOT_IN_SERVICE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsErpsRingMplsRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                setValFsErpsRingMplsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsErpsRingMplsRowStatus (UINT4 u4FsErpsContextId,
                               UINT4 u4FsErpsRingId,
                               INT4 i4SetValFsErpsRingMplsRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetRingEntry (u4FsErpsContextId, u4FsErpsRingId);

    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pRingInfo->MplsPwInfo.u1RowStatus == 0) &&
        ((i4SetValFsErpsRingMplsRowStatus == ACTIVE) ||
         (i4SetValFsErpsRingMplsRowStatus == NOT_IN_SERVICE)))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group %d MPLS PW Entry is not created\n",
                          u4FsErpsRingId);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsErpsRingMplsRowStatus)
    {
        case CREATE_AND_WAIT:

            pRingInfo->MplsPwInfo.u1RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:

            pRingInfo->MplsPwInfo.u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            MEMSET (&(pRingInfo->MplsPwInfo), 0, sizeof (tErpsMplsPwInfo));
            pRingInfo->u1RowStatus = NOT_READY;
            break;

        case ACTIVE:

            if (pRingInfo->MplsPwInfo.u1RowStatus != NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }
            pRingInfo->MplsPwInfo.u1RowStatus = ACTIVE;
            ErpsLwUtilRingCfgNotInServ (pRingInfo);
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPwVcId1
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPwVcId1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPwVcId1 (UINT4 *pu4ErrorCode,
                            UINT4 u4FsErpsContextId,
                            UINT4 u4FsErpsRingId,
                            UINT4 u4TestValFsErpsRingPwVcId1)
{

    if (u4TestValFsErpsRingPwVcId1 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working Mpls Pw info is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateMplsEntry (u4FsErpsContextId, u4FsErpsRingId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingPwVcId2
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingPwVcId2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingPwVcId2 (UINT4 *pu4ErrorCode,
                            UINT4 u4FsErpsContextId,
                            UINT4 u4FsErpsRingId,
                            UINT4 u4TestValFsErpsRingPwVcId2)
{

    if (u4TestValFsErpsRingPwVcId2 == 0)
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Group Working Mpls Pw is not valid\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ErpsLwValidateMplsEntry (u4FsErpsContextId, u4FsErpsRingId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsErpsRingMplsRowStatus
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId

                The Object 
                testValFsErpsRingMplsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsErpsRingMplsRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsErpsContextId,
                                  UINT4 u4FsErpsRingId,
                                  INT4 i4TestValFsErpsRingMplsRowStatus)
{
    tErpsRingInfo      *pRingInfo = NULL;

    if ((i4TestValFsErpsRingMplsRowStatus < ACTIVE) ||
        (i4TestValFsErpsRingMplsRowStatus > DESTROY))
    {
        ERPS_GLOBAL_TRC ("Ring Group Mpls Pw Info Rowstatus is not valid\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pRingInfo = ErpsLwUtilValidateCxtAndRing (u4FsErpsContextId,
                                              u4FsErpsRingId, pu4ErrorCode);
    if (pRingInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRingInfo->u1ServiceType != ERPS_SERVICE_MPLS_LSP_PW)
    {

        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Serivce type is not MPLS_LSP_PW.\n",
                          u4FsErpsRingId);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERPS_ERR_MPLS_SERVICE_TYPE);

        return SNMP_FAILURE;
    }
    if ((i4TestValFsErpsRingMplsRowStatus == CREATE_AND_WAIT) &&
        (pRingInfo->MplsPwInfo.u1RowStatus != 0))
    {
        ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Ring Mmpls Pw Info entry already exists.\n",
                          u4FsErpsRingId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* The RowStatus can be set to Active only if all the mandatory
     * objects in the Table are set. When they are not set, the
     * RowStatus will be in Not Ready State. RowStatus can move to
     * Active from Not In Service State only
     */

    if ((i4TestValFsErpsRingMplsRowStatus == ACTIVE)
        || (i4TestValFsErpsRingMplsRowStatus == NOT_IN_SERVICE))
    {
        if (pRingInfo->MplsPwInfo.u1RowStatus == NOT_READY)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Ring Group %d Mpls Pw Info entry is not ready to"
                              " be activated\n", u4FsErpsRingId);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_MPLS_NOT_READY);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsErpsRingMplsRowStatus == DESTROY)
        || (i4TestValFsErpsRingMplsRowStatus == NOT_IN_SERVICE))
    {
        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            ERPS_CONTEXT_TRC (u4FsErpsContextId, u4FsErpsRingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Mpls Pw Info Rowstatus of Ring Group %d is active\n",
                              u4FsErpsRingId);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERPS_ERR_RING_ACTIVE);
            return SNMP_FAILURE;

        }

    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsErpsRingMplsTable
 Input       :  The Indices
                FsErpsContextId
                FsErpsRingId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsErpsRingMplsTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
