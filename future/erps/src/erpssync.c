/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpssync.c,v 1.7 2014/07/19 13:05:31 siva Exp $
 *
 * Description: This file contains the ERPS API related functions
 *
 *****************************************************************************/

#include "erpsinc.h"

#ifdef NPAPI_WANTED
/***************************************************************************/
/* FUNCTION NAME    : ErpsSyncNpFsErpsHwRingConfig                         */
/*                                                                         */
/* DESCRIPTION      : This routine updates the ring in hardware and call   */
/*                    function to sync Np info to standby node.            */
/*                                                                         */
/* INPUT            : pErpsHwRingInfo - Ring info received to update ring  */
/*                                      status in hardware.                */
/*                                                                         */
/* OUTPUT           : None.                                                */
/*                                                                         */
/* RETURNS          : FNP_SUCCESS - When NPAPI return success, after       */
/*                                  successful updation of hardware.       */
/*                    FNP_FAILURE - When NPAPI return failure, after it    */
/*                                  failed to update ring info in hardware.*/
/***************************************************************************/

PUBLIC INT4
ErpsSyncNpFsErpsHwRingConfig (tErpsHwRingInfo * pErpsHwRingInfo)
{
    INT4                i4RetVal = FNP_SUCCESS;
    unNpSync            NpSync;
    UINT4               u4NodeState = RM_INIT;

    MEMSET (&NpSync, 0, sizeof (NpSync));

    MEMCPY (&(NpSync.FsErpsHwRingConfig.ErpsHwRingInfo), pErpsHwRingInfo,
            sizeof (tErpsHwRingInfo));

    u4NodeState = ErpsPortRmGetNodeState ();

    if (u4NodeState == RM_ACTIVE)
    {
        if (ErpsPortRmGetStandbyNodeCount () != 0)
        {
            if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount == 0)
            {
                ErpsSyncNpFsErpsHwRingConfigSync (NpSync.FsErpsHwRingConfig,
                                                  RM_ERPS_APP_ID,
                                                  ERPS_RED_NPSYNC_MSG);
            }
        }
#undef FsErpsHwRingConfig

        /* If Async NPAPI is enabled, FsErpsHwRingConfig will be defined
           as AsyncFsErpsHwRingConfig , which will conflict with the definition
           (ErpsSyncNpFsErpsHwRingConfig) that is given when L2RED_WANTED 
           switch is enabled
           So the following switch condition (ifndef NPSIM_ASYNC_WANTED) is 
           given to avoid multiple definitions. */
#ifndef NPSIM_ASYNC_WANTED
        i4RetVal = FsErpsHwRingConfig (pErpsHwRingInfo);
#endif
        if (i4RetVal == FNP_FAILURE)
        {
            if (ErpsPortRmGetStandbyNodeCount () != 0)
            {
                if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount == 0)
                {
                    ErpsSyncNpFsErpsHwRingConfigSync (NpSync.FsErpsHwRingConfig,
                                                      RM_ERPS_APP_ID,
                                                      ERPS_RED_NPSYNC_MSG);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount == 0)
        {
            ErpsRedHwAudCreateOrFlushBuffer (&NpSync, ERPS_RED_NPSYNC_MSG, 0);
        }
    }

    return (i4RetVal);
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsSyncNpFsErpsHwRingConfigSync                     */
/*                                                                         */
/* DESCRIPTION      : This routine syncs NPAPI call info to standby node.  */
/*                                                                         */
/* INPUT            : HwRingConfig - Ring info received to sync NP Info    */
/*                                   to standby.                           */
/*                    u4AppId   - ERPS App Id                              */
/*                    u4NpApiId - NPAPI enum value, that will be used to   */
/*                                identify NP Sync messages in standby node*/
/* OUTPUT           : None.                                                */
/*                                                                         */
/* RETURNS          : None.                                                */
/*                                                                         */
/***************************************************************************/

VOID
ErpsSyncNpFsErpsHwRingConfigSync (tNpSyncFsErpsHwRingConfig HwRingConfig,
                                  UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet;
    UINT1               u1Index = 0;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if ((pMsg = RM_ALLOC_TX_BUF (ERPS_RED_NP_SYNC_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    /* All the member variables present in the structure tErpsHwRingInfo should be 
     *  synched properly in this function for HA functionality to work */
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, (UINT1) u4NpApiId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, ERPS_RED_NP_SYNC_MSG_SIZE);

        NPSYNC_RM_PUT_N_BYTE (pMsg, HwRingConfig.ErpsHwRingInfo.VlanList,
                              &u2OffSet, VLAN_DEV_VLAN_LIST_SIZE_EXT);

        NPSYNC_RM_PUT_N_BYTE (pMsg,
                              HwRingConfig.ErpsHwRingInfo.au1RingVlanGroupList,
                              &u2OffSet, MAX_ERPS_VLAN_GROUP_LIST_SIZE);

        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u4Port1IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u4Port2IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u4RingId);
        for (u1Index = 0; u1Index < ERPS_MAX_HW_HANDLE; u1Index++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                  HwRingConfig.ErpsHwRingInfo.
                                  ai4HwRingHandle[u1Index]);
        }

        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.VlanId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u2VlanGroupId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u2VlanGroupListCount);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1Port1Action);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1Port2Action);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1RingAction);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1ProtectionType);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1HwUpdatedPort);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1UnBlockRAPSChannel);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.
                              u1HighestPriorityRequest);

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              HwRingConfig.ErpsHwRingInfo.u1Service);

        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}
#endif

/***************************************************************************/
/* FUNCTION NAME    : ErpsSyncNpProcessSyncMsg                             */
/*                                                                         */
/* DESCRIPTION      : This routine form and sync NPAPI info to standby node*/
/*                                                                         */
/* INPUT            : pMsg - Pointer to message info, that needs to synced*/
/*                           up.                                           */
/*                    pu2OffSet - offset value to extract Npsync info.     */
/*                                                                         */
/* OUTPUT           : None.                                                */
/*                                                                         */
/* RETURNS          : None.                                                */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsSyncNpProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    unNpSync            NpSync;
    UINT1               u1Index = 0;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    /*All the member variables present in the structure tErpsHwRingInfo should be 
     *synched properly in this function for HA functionality to work*/
    NPSYNC_RM_GET_N_BYTE (pMsg,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.VlanList,
                          pu2OffSet, VLAN_DEV_VLAN_LIST_SIZE_EXT);

    NPSYNC_RM_GET_N_BYTE (pMsg,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          au1RingVlanGroupList, (UINT4 *) (VOID *) pu2OffSet,
                          MAX_ERPS_VLAN_GROUP_LIST_SIZE);

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.u4ContextId);
    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u4Port1IfIndex);
    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u4Port2IfIndex);
    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.u4RingId);
    for (u1Index = 0; u1Index < ERPS_MAX_HW_HANDLE; u1Index++)
    {
        NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                              NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                              ai4HwRingHandle[u1Index]);
    }

    NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.VlanId);
    NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u2VlanGroupId);
    NPSYNC_RM_GET_2_BYTE (pMsg, (UINT4 *) (VOID *) pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u2VlanGroupListCount);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1Port1Action);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1Port2Action);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1RingAction);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1ProtectionType);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1HwUpdatedPort);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1UnBlockRAPSChannel);
    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.
                          u1HighestPriorityRequest);

    NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                          NpSync.FsErpsHwRingConfig.ErpsHwRingInfo.u1Service);

    ErpsRedHwAudCreateOrFlushBuffer (&NpSync, ERPS_RED_NPSYNC_MSG, 0);

    return;
}
