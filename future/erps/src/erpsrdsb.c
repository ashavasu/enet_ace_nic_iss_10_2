/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: erpsrdsb.c,v 1.4 2011/09/05 07:13:52 siva Exp $                    
 *                                                                         *
 * Description: This file contains the utility functions for ERPS stubs for*
 *              redundancy.                                                *
 ***************************************************************************/
#include "erpsinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize ring dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Erps descriptor    */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register ERPS with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ErpsRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAudCrtNpSyncBufferTable                       */
/*                                                                           */
/* DESCRIPTION      : This routine creates Hardware audit buffer table in the*/
/*                    standby node. Previosly present entries are deleted    */
/*                    before creating the table.                             */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ErpsRedHwAudCrtNpSyncBufferTable (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedSyncRingDynInfo                           */
/*                                                                           */
/*    Description         : This function will initiate ring dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedSyncRingDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbNodeInit                               */
/*                                                                           */
/*    Description         : This function will initialize Erps ring db node. */
/*                                                                           */
/*    Input(s)            : pRingInfo - pointer to ring entry.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbNodeInit (tErpsRingInfo * pRingInfo)
{
    UNUSED_PARAM (pRingInfo);

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAuditIncBlkCounter                            */
/*                                                                           */
/* DESCRIPTION      : This routine increments the NP Sync Block Counter.     */
/*                    If the value of this Block flag is not equal to 0, then*/
/*                    it is intended to block NP Sync-ups.                   */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ErpsRedHwAuditIncBlkCounter (VOID)
{
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAuditDecBlkCounter                          */
/*                                                                         */
/* DESCRIPTION      : This routine decrements the NP Sync Block Counter.   */
/*                    If the value of this Block flag is not equal to 0.   */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedHwAuditDecBlkCounter (VOID)
{
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncDeleteAllRingInCtxt                    */
/*                                                                         */
/* DESCRIPTION      : This routine sends delete all sync up message to     */
/*                    standby node for given context.                      */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which delete all info is */
/*                                  invoked.                               */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncDeleteAllRingInCtxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedDeInitGlobalInfo                              */
/*                                                                         */
/* DESCRIPTION      : Deinitializes redundancy global variables.           */
/*                    This function will deregisters ERPS with RM.         */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedDeInitGlobalInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pErpsDataDesc - This is Erps sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pErpsDbNode - This is db node defined in the ERPS*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbUtilAddTblNode (tDbTblDescriptor * pErpsDataDesc,
                         tDbTblNode * pErpsDbNode)
{
    UNUSED_PARAM (pErpsDataDesc);
    UNUSED_PARAM (pErpsDbNode);
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncModuleEnableStatus                        */
/*                                                                         */
/* DESCRIPTION      : This routine sends module status sync up enable msg  */
/*                    to standby node for given context.                   */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which module status needs*/
/*                                  to be synced                           */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncModuleEnableStatus (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncModuleDisableStatus                       */
/*                                                                         */
/* DESCRIPTION      : This routine sends module status sync up disable msg */
/*                    to standby node for given context.                   */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which module status needs*/
/*                                  to be synced                           */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncModuleDisableStatus (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedUpdateSyncTable                           */
/*                                                                           */
/*    Description         : This function will update the sync table for the */
/*                          given ring info.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedUpdateSyncTable (tErpsRingInfo * pRingInfo)
{
    UNUSED_PARAM (pRingInfo);
    return;
}

/*****************************************************************************/
/*    Function Name       : ErpsRedDelRingDynInfo                            */
/*                                                                           */
/*    Description         : This function will remove ring info from the     */
/*                          sync table                                       */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDelRingDynInfo (tErpsRingInfo * pRingInfo)
{

    UNUSED_PARAM (pRingInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbUtilDelTblNode                          */
/*                                                                           */
/*    Description         : This function will be used to delete a dynamic   */
/*                          info entry from database table.                  */
/*                                                                           */
/*    Input(s)            : pErpsDataDesc - This is Erps sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pErpsDbNode - This is db node defined in the ERPS*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbUtilDelTblNode (tDbTblDescriptor * pErpsDataDesc,
                         tDbTblNode * pErpsDbNode)
{

    UNUSED_PARAM (pErpsDataDesc);
    UNUSED_PARAM (pErpsDbNode);
    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : ErpsRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      ERPS to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ErpsRedHRProcStdyStPktReq (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : ErpsRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS/FAILURE.                                     */
/*****************************************************************************/
INT1
ErpsRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktLen,
                        UINT2 u2Port, UINT4 u4TimeOut)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4TimeOut);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ErpsRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
ErpsRedHRSendStdyStTailMsg (VOID)
{
    return OSIX_SUCCESS;
}

/******************************************************************************/
/*  Function           : ErpsRedGetHRFlag                                          */
/*  Input(s)           : None                                                 */
/*  Output(s)          : None                                                 */
/*  Returns            : Hitless restart flag value.                          */
/*  Action             : This API returns the hitless restart flag value.     */
/******************************************************************************/
UINT1
ErpsRedGetHRFlag (VOID)
{
    return OSIX_SUCCESS;
}
