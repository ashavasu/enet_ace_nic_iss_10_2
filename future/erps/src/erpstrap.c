/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpstrap.c,v 1.16 2016/05/04 11:36:33 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "erpsinc.h"
#include "fserps.h"

CHR1               *gac1ErpsTrapMsg[] = {
    NULL,
    "Auto Protection Switch.",
    "Forced Switch.",
    "Manual Switch.",
    "R-APS Pdu Tx Failure.",
    "Hw Program Failure.",
    "Buffer Alloc Failure.",
    "Timer Failure.",
    "Version Mismatch.",
    "Provisioning Mismatch.",
    "Provisioning Mismatch cleared.",
    "Forced switch is present.",
    "TFOP defect encountered.",
    "TFOP defect Cleared."
};

/****************************************************************************
 *                           erpstrap.c prototypes                          *
 ****************************************************************************/
#ifdef SNMP_3_WANTED
PRIVATE tSNMP_VAR_BIND *ErpsTrapConstructString (tErpsRingInfo * pRingInfo,
                                                 UINT1 *pu1ObjName,
                                                 UINT1 *pu1ObjValue,
                                                 UINT2 u2ObjLen);
PRIVATE tSNMP_VAR_BIND *ErpsTrapConstructInteger (tErpsRingInfo * pRingInfo,
                                                  UINT1 *pu1ObjName,
                                                  INT4 i4ObjVal);
PRIVATE tSNMP_OID_TYPE *ErpsTrapMakeObjIdFrmString (INT1 *pi1TextStr,
                                                    UINT1 *pu1TableName);

PRIVATE INT4
       ErpsTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value);
#endif

/****************************************************************************/

/*****************************************************************************/
/* Function Name      : ErpsTrapSendTrapNotifications                        */
/*                                                                           */
/* Description        : This function will send an SNMP trap to the          */
/*                      administrator for various ERPS conditions.           */
/*                                                                           */
/* Input(s)           : pNotifyInfo - Information to be sent in the Trap     */
/*                                   message.                                */
/*                      u1TrapEvent- Specific Type for Trap Message          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
PUBLIC VOID
ErpsTrapSendTrapNotifications (tErpsRingInfo * pRingInfo, UINT1 u1TrapEvent)
{
#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               ERPS_TRAP_OID[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 40, 4 };
    UINT4               u4SpecTrapType = 0;
    UINT4               u4CtxtNameLen = 0;
    UINT1               au1Buf[ERPS_OBJECT_NAME_LEN];
    UINT1               au1Val[ERPS_OBJECT_VALUE_LEN];
    UINT1               au1SyslogStr[ERPS_MAX_TRAP_STR_LEN];
    UINT2               u2Len = 0;

    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    if (pRingInfo == NULL)
    {
        return;
    }
    if (ERPS_TRAP_STATUS (pRingInfo->u4ContextId) == ERPS_SNMP_FALSE)
    {
        /* Trap is not enabled for this context */
        return;
    }

    pEnterpriseOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, ERPS_TRAP_OID, sizeof (ERPS_TRAP_OID));
    pEnterpriseOid->u4_Length = sizeof (ERPS_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4SpecTrapType;

    /* Allocate the Memory for SNMP Trap OID to form the VbList */
    pSnmpTrapOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    MEMSET (au1SyslogStr, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1SyslogStr, "[Ctx : %s][Ring : %s]-",
             ERPS_CONTEXT_NAME (pRingInfo->u4ContextId),
             pRingInfo->au1RingName);

    switch (u1TrapEvent)
    {
        case ERPS_TRAP_AUTO_SWITCH:
        case ERPS_TRAP_FORCE_SWITCH:
        case ERPS_TRAP_MANUAL_SWITCH:
        case ERPS_TRAP_FORCE_SWITCH_PRESENT:
            u4SpecTrapType = ERPS_TRAP_STATE_CHANGE;
            break;

        case ERPS_TRAP_RAPS_TX_FAIL:
        case ERPS_TRAP_HW_CONFIG_FAIL:
        case ERPS_TRAP_BUF_ALLOC_FAIL:
        case ERPS_TRAP_TIMER_FAIL:
        case ERPS_TRAP_PROV_MISMATCH:
        case ERPS_TRAP_PROV_MISMATCH_CLEAR:
        case ERPS_TRAP_TFOP_DEFECT:
        case ERPS_TRAP_TFOP_DEFECT_CLEAR:
            u4SpecTrapType = ERPS_TRAP_FAILURE;
            break;
    }

    pStartVb = pVbList;

    /* Filling the Context-Name */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (au1Val, 0, sizeof (au1Val));
    SPRINTF ((char *) au1Buf, "fsErpsCtxtName");
    u4CtxtNameLen = sizeof (ERPS_CONTEXT_NAME (pRingInfo->u4ContextId));
    STRCPY (au1Val, "Context:");
    STRNCAT (au1Val, ERPS_CONTEXT_NAME (pRingInfo->u4ContextId), u4CtxtNameLen);
    pVbList->pNextVarBind
        =
        ErpsTrapConstructString (pRingInfo, au1Buf, au1Val,
                                 (UINT2) STRLEN (au1Val));

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }
    pVbList = pVbList->pNextVarBind;

    /* Filling the Ring Name */
    MEMSET (au1Val, 0, sizeof (au1Val));
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsErpsRingName");
    STRCPY (au1Val, "Ring: ");
    STRNCAT (au1Val, pRingInfo->au1RingName, STRLEN (pRingInfo->au1RingName));

    pVbList->pNextVarBind
        =
        ErpsTrapConstructString (pRingInfo, au1Buf, au1Val,
                                 (UINT2) STRLEN (au1Val));

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    if (u4SpecTrapType == ERPS_TRAP_STATE_CHANGE)
    {
        /* Filling the Trap Switching mechanism */
        MEMSET (au1Val, 0, sizeof (au1Val));
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        SPRINTF ((char *) au1Buf, "fsErpsTrapSwitchingMechanism");
        u2Len =
            (UINT2) (STRLEN (gac1ErpsTrapMsg[u1TrapEvent]) <
                    (sizeof(au1Val)-1) ?
                    STRLEN (gac1ErpsTrapMsg[u1TrapEvent]) : (sizeof(au1Val)-1));

        STRNCPY (au1Val, gac1ErpsTrapMsg[u1TrapEvent],
                 u2Len);
        au1Val[u2Len] = '\0';
        STRCAT (au1SyslogStr, au1Val);

        pVbList->pNextVarBind
            = ErpsTrapConstructString (pRingInfo, au1Buf, au1Val,
                                       (UINT2) STRLEN (au1Val));
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            SNMP_FreeOid (pSnmpTrapOid);
            return;
        }

        pVbList = pVbList->pNextVarBind;

        /* Filling the Ring Sem State */
        MEMSET (au1Val, 0, sizeof (au1Val));
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        SPRINTF ((char *) au1Buf, "fsErpsRingSemState");
        SPRINTF ((char *) au1Val, " SemState :%u", pRingInfo->u1RingState);
        STRCAT (au1SyslogStr, au1Val);

        pVbList->pNextVarBind
            =
            ErpsTrapConstructInteger (pRingInfo, au1Buf,
                                      pRingInfo->u1RingState);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            SNMP_FreeOid (pSnmpTrapOid);
            return;
        }
        pVbList = pVbList->pNextVarBind;

        /* Filling the Ring Status */
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        MEMSET (au1Val, 0, sizeof (au1Val));
        SPRINTF ((char *) au1Buf, "fsErpsRingNodeStatus");
        SPRINTF ((char *) au1Val, " Nodestatus :%u",
                 pRingInfo->u4RingNodeStatus);
        STRCAT (au1SyslogStr, au1Val);

        pVbList->pNextVarBind
            =
            ErpsTrapConstructInteger (pRingInfo, au1Buf,
                                      pRingInfo->u4RingNodeStatus);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            SNMP_FreeOid (pSnmpTrapOid);
            return;
        }
    }
    else
    {
        /* Filling the failure type */
        MEMSET (au1Val, 0, sizeof (au1Val));
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        SPRINTF ((char *) au1Buf, "fsErpsTypeOfFailure");

        STRNCPY (au1Val, gac1ErpsTrapMsg[u1TrapEvent],
                 STRLEN (gac1ErpsTrapMsg[u1TrapEvent]));
        au1Val[STRLEN (gac1ErpsTrapMsg[u1TrapEvent])] = '\0';
        STRCAT (au1SyslogStr, au1Val);

        pVbList->pNextVarBind
            = ErpsTrapConstructString (pRingInfo, au1Buf, au1Val,
                                       (UINT2) STRLEN (au1Val));

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            SNMP_FreeOid (pSnmpTrapOid);
            return;
        }
    }

    pContextName =
        SNMP_AGT_FormOctetString (ERPS_CONTEXT_NAME (pRingInfo->u4ContextId),
                                  u4CtxtNameLen);

    if (pContextName == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ERPS_GLOBAL_TRC ("ErpsTrapSendTrapNotifications: Failed to form"
                         "Octet string for ContextName.\r\n");
        return;
    }
    /*Increment trap sent count */
    pRingInfo->u4TrapsCount++;
    ErpsPortFmNotifyFaults (pEnterpriseOid, ENTERPRISE_SPECIFIC, u4SpecTrapType,
                            pStartVb, (UINT1 *) au1SyslogStr, pContextName);

    SNMP_AGT_FreeOctetString (pContextName);
#else
    UNUSED_PARAM (pRingInfo);
    UNUSED_PARAM (u1TrapEvent);
#endif
    return;
}

#ifdef SNMP_3_WANTED
/******************************************************************************
* Function :   ErpsTrapConstructString 
*
* Description: This function used to construct the string based
*              on the object name and value.
*
* Input    :   pRingInfo  - Pointer to the ring entry.
*              pu1ObjName  - Pointer to the array of Object name.
*              pu1ObjValue - Pointer to the array of object value.
*              u2ObjLen    - Length of the object value.
*
* Output   :   None.
*
* Returns  :   pVbList or NULL 
*******************************************************************************/
PRIVATE tSNMP_VAR_BIND *
ErpsTrapConstructString (tErpsRingInfo * pRingInfo, UINT1 *pu1ObjName,
                         UINT1 *pu1ObjValue, UINT2 u2ObjLen)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;

    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    pOid = ErpsTrapMakeObjIdFrmString ((INT1 *) pu1ObjName,
                                       (UINT1 *) fs_erps_orig_mib_oid_table);
    if (pOid == NULL)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          ALL_FAILURE_TRC, "ErpsTrapMakeObjIdFrmString: OID "
                          "for %s Not Found. Failed!!!\r\n", pu1ObjName);
        return NULL;
    }

    pOstring = SNMP_AGT_FormOctetString (pu1ObjValue, u2ObjLen);

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          ALL_FAILURE_TRC, "ErpsTrapMakeObjIdFrmString: Failed "
                          "to form Octet string for fsErpsCtxtName.\r\n");
        return NULL;
    }

    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                    pOstring, NULL, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          ALL_FAILURE_TRC, "ErpsTrapMakeObjIdFrmString: Failed "
                          "to form VarBind for fsErpsCtxtName.\r\n");
        return NULL;
    }

    return pVbList;
}

/******************************************************************************
* Function :   ErpsTrapConstructInteger  
*
* Description: This function used to construct the integer based
*              on the object name and value.
*
* Input    :   pRingInfo  - Pointer to the ring entry.
*              pu1ObjName  - Pointer to the array of Object name.
*              pu1ObjValue - Pointer to the array of object value.
*              u2ObjLen    - Length of the object value.
*
* Output   :   None.
*
* Returns  :   pVbList or NULL 
*******************************************************************************/
PRIVATE tSNMP_VAR_BIND *
ErpsTrapConstructInteger (tErpsRingInfo * pRingInfo, UINT1 *pu1ObjName,
                          INT4 i4ObjVal)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;

    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    pOid = ErpsTrapMakeObjIdFrmString ((INT1 *) pu1ObjName,
                                       (UINT1 *) fs_erps_orig_mib_oid_table);
    if (pOid == NULL)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          ALL_FAILURE_TRC, "ErpsTrapMakeObjIdFrmString: OID "
                          "for %s Not Found. Failed!!!\r\n", pu1ObjName);
        return NULL;
    }

    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0, i4ObjVal,
                                    NULL, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          ALL_FAILURE_TRC, "ErpsTrapMakeObjIdFrmString: Failed "
                          "to form VarBind for fsErpsCtxtName.\r\n");
        return NULL;
    }

    return pVbList;
}

/******************************************************************************
* Function :   ErpsTrapMakeObjIdFrmString
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/
PRIVATE tSNMP_OID_TYPE *
ErpsTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[ERPS_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;
    UINT2               u2Len = 0;

    MEMSET (ai1TempBuffer, 0, ERPS_OBJECT_NAME_LEN);

    /* see if there is an alpha descriptor at begining */
    if ((ISALPHA (*pi1TextStr)) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) && (u2Index < 256)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         ERPS_OBJECT_NAME_LEN);
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u2Len =
            (UINT2) ((ERPS_OBJECT_NAME_LEN-STRLEN(ai1TempBuffer)-1) <
                    STRLEN (pi1DotPtr) ?
                    (ERPS_OBJECT_NAME_LEN-STRLEN(ai1TempBuffer)-1) :  STRLEN (pi1DotPtr));

        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 u2Len);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;

    for (u2Index = 0; ((u2Index <= ERPS_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (ErpsTrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   ErpsTrapParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : ppu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
PRIVATE INT4
ErpsTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}
#endif
