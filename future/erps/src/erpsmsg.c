/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsmsg.c,v 1.63 2017/09/22 12:34:48 siva Exp $
 *
 * Description: This file contains the ERPS Msg related functions        
 *                                                        
 *****************************************************************************/
#ifndef _ERPSMSG_C
#define _ERPSMSG_C

#include "erpsinc.h"

/****************************************************************************
 *                           erpsmsg.c prototypes                           *
 ****************************************************************************/
PRIVATE INT4        ErpsMsgProcessRapsMsg (tErpsQMsg * pMsg);
PRIVATE INT4        ErpsMsgValidateRapsMsg (VOID);
PRIVATE VOID        ErpsMsgHandleCfmSignal (tErpsQMsg * pMsg);
PRIVATE VOID        ErpsMsgExtractRapsInfo (tErpsQMsg * pQMsg);
PRIVATE VOID        ErpsMsgComputeRequestType (UINT1 *pu1Request);
PRIVATE INT4        ErpsMsgValidateMepInfo (tErpsRingInfo * pRingInfo,
                                            tErpsCfmMepInfo MepInfo);
PRIVATE VOID        ErpsMsgFlushLogic (tErpsRingInfo * pRingInfo,
                                       UINT4 u4RcvdPort);
PRIVATE VOID        ErpsMsgBlockIndicationLogic (tErpsRingInfo * pRingInfo,
                                                 UINT4 u4Status);
PRIVATE VOID        ErpsMsgHandleCfmSignalFromEcfm (tErpsQMsg * pMsg);

PRIVATE VOID        ErpsMsgHandleCfmSignalFromMpls (tErpsQMsg * pMsg);

/****************************************************************************/

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgEnque                                         */
/*                                                                         */
/* DESCRIPTION      : Function is used to post queue message to ERPS task. */
/*                                                                         */
/* INPUT            : pMsg - Pointer to the message to be posted.          */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
ErpsMsgEnque (tErpsQMsg * pMsg)
{
    if (OsixQueSend (gErpsGlobalInfo.MainTaskQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
        ERPS_GLOBAL_TRC ("ErpsMsgEnque: Osix Queue Send Failed!!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gErpsGlobalInfo.MainTaskId, ERPS_QMSG_EVENT)
        == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMsgEnque: Osix Event Send Failed!!!\r\n");
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgQueueHandler                              */
/*                                                                           */
/*    Description         : This function will receive messages posted to    */
/*                          ERPS task to indicate occurance of event or to   */
/*                          handover the received R-APS messages.            */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
ErpsMsgQueueHandler (VOID)
{
    tErpsQMsg          *pMsg = NULL;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif

    while (OsixQueRecv (gErpsGlobalInfo.MainTaskQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->u4MsgType)
        {
            case ERPS_DELETE_CONTEXT_MSG:
                ErpsCxtHandleDeleteContxt (pMsg->u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            case ERPS_UPDATE_CONTEXT_NAME:
                ErpsCxtHandleUpdateCxtName (pMsg->u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            case ERPS_RAPS_PDU_MSG:
                ErpsMsgProcessRapsMsg (pMsg);
                CRU_BUF_Release_MsgBufChain
                    (pMsg->unMsgParam.RapsPduMsg.pRapsPdu, FALSE);
                break;
            case ERPS_CFM_SIGNAL_MSG:
                ErpsMsgHandleCfmSignal (pMsg);
                break;
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
                i4ProtoId = pMsg->unMsgParam.pMbsmProtoMsg->i4ProtoCookie;
                pSlotInfo = &(pMsg->unMsgParam.pMbsmProtoMsg->MbsmSlotInfo);
                pPortInfo = &(pMsg->unMsgParam.pMbsmProtoMsg->MbsmPortInfo);

                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus
                    = ErpsMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);

                MbsmSendAckFromProto (&MbsmProtoAckMsg);
                MemReleaseMemBlock (gErpsGlobalInfo.ErpsMbsmPoolId,
                                    (UINT1 *) pMsg->unMsgParam.pMbsmProtoMsg);

                break;
            case MBSM_MSG_CARD_REMOVE:

                i4ProtoId = pMsg->unMsgParam.pMbsmProtoMsg->i4ProtoCookie;
                pSlotInfo = &(pMsg->unMsgParam.pMbsmProtoMsg->MbsmSlotInfo);
                pPortInfo = &(pMsg->unMsgParam.pMbsmProtoMsg->MbsmPortInfo);

                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus
                    = ErpsMbsmUpdateOnCardRemoval (pPortInfo, pSlotInfo);

                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MemReleaseMemBlock (gErpsGlobalInfo.ErpsMbsmPoolId,
                                    (UINT1 *) pMsg->unMsgParam.pMbsmProtoMsg);
                break;
#endif
            case ERPS_RM_MSG:
                ErpsRedHandleRmEvents (pMsg);
                break;
#ifdef NPAPI_WANTED
            case ERPS_NP_CALLBACK:
                ErpsHandleNpCallbackEvents (pMsg);
                break;
#endif
            default:
                break;
        }

        /* Release the buffer to pool */
        if (MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            ERPS_GLOBAL_TRC ("ErpsMsgQueueHandler:Free MemBlock QMsg FAILED\n");
            return;
        }

    }

    return;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgProcessRapsMsg                            */
/*                                                                           */
/*    Description         : This function process the received R-APS messages*/
/*                          decode the type of message and invoke function   */
/*                          to handle "event" message or invoke Priority     */
/*                          Logic re-evaluation.                             */
/*                                                                           */
/*    Input(s)            : pMsg - Pointer to received message info.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         returned form the Priority logic, */
/*                                         or message validation succeeded.  */
/*                          OSIX_FAILURE - When this function failed in      */
/*                                         priority logic evaluation         */
/*                                         or message validation failed.     */
/*****************************************************************************/
PRIVATE INT4
ErpsMsgProcessRapsMsg (tErpsQMsg * pMsg)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsCfmMepInfo     MepInfo;
    unErpsRingDynInfo   RingDynInfo;
    tVlanId             VlanId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1Request = 0;
    UINT4               u4RcvdPort = 0;
    UINT1               u1Statusbit = ERPS_STATUS_BIT_NONE;

    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));
    MEMSET (&RingDynInfo, 0, sizeof (unErpsRingDynInfo));
    MEMCPY (&MepInfo, &(pMsg->MepInfo), sizeof (tErpsCfmMepInfo));

    u4ContextId = pMsg->u4ContextId;
    VlanId = pMsg->MepInfo.VlanId;
    u4IfIndex = pMsg->MepInfo.u4IfIndex;

    /* Extract the R-APS PDU information */
    ErpsMsgExtractRapsInfo (pMsg);

    /* Get the Ring Info for the received R-APS PDU */
    pRingInfo =
        ErpsRingGetNodeFromPortVlanTable (u4ContextId, u4IfIndex, VlanId,
                                          gErpsRcvdMsg.u1RingMacId);

    if (pRingInfo == NULL)
    {
        /* R-APS PDU received for non-existing ring entry */
        /* Also the PortVlan table Get will fail,when ERPS module is disabled */
        return OSIX_FAILURE;
    }

    if (ErpsMsgValidateMepInfo (pRingInfo, MepInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgProcessRapsMsg: Received indication for "
                          "unknown MEP.\r\n");
        return OSIX_FAILURE;
    }

    /* D-ERPS PDU processing */
    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        if (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
        {
            if (gErpsRcvdMsg.u1DERPSBitStatus)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg: "
                                  "D-ERPS PDU received on ring port 1, calling"
                                  " ErpsDsyncDynSyncMsgHandler().\r\n");
                ErpsDsyncDynSyncMsgHandler (pRingInfo, pMsg);
            }
            return OSIX_SUCCESS;
        }

    }
    else
    {
        if (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD)
        {
            if (gErpsRcvdMsg.u1DERPSBitStatus)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg: "
                                  "D-ERPS PDU received on ring port 2, calling"
                                  " ErpsDsyncDynSyncMsgHandler().\r\n");
                ErpsDsyncDynSyncMsgHandler (pRingInfo, pMsg);
            }
            return OSIX_SUCCESS;
        }
    }

    /* If Both are local ports and still D-ERPS bit is set in Received PDU
     * then the Received PDU should be dropped */
    if (gErpsRcvdMsg.u1DERPSBitStatus)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgProcessRapsMsg: Un-expected D-ERPS sync"
                          " PDU, So Dropping PDU.\r\n");
        return OSIX_SUCCESS;
    }

    /* The R-APS PDU which are recieved with the invalid version field 
     * (other than 00-Version1 && 0x01-version2) will be discarded and trap will be sent
     * */
    if ((gErpsRcvdMsg.u1RingVersion != ERPS_RAPS_V1_PDU) &&
        (gErpsRcvdMsg.u1RingVersion != ERPS_RAPS_V2_PDU))
    {
        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_VERSION_MISMATCH);
        ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                      ERPS_STATS_VERSION_DISC);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "The received R-APS PDU does not have valid "
                          "Version field. So packet is discarded " "\r\n");
        return OSIX_FAILURE;
    }

    /* Validate the R-APS information fields */
    if (ErpsMsgValidateRapsMsg () == OSIX_FAILURE)
    {
        /* Update the R-APS discarded message received count */
        ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo, ERPS_STATS_RAPS_DISC);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "The received R-APS PDU is not a valid "
                          "Raps pdu.\r\n");
        return OSIX_FAILURE;
    }
    else
    {
        /* Stop the TFOP timer and restart the T-FOP timer when it receives valid packet
         * If TFOP defect trap already raised then clear the trap.*/
        if (pRingInfo->bDFOP_TO == OSIX_ENABLED)
        {

            ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
            /* When RPL node is in IDLE condition, there is a chance
             *  of not receiving any RAPS packets from adjacent node, 
             *  so FOP-TO timer will be stopped here */
            if (!((pRingInfo->u4RplIfIndex != 0) &&
                  (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)))
            {
                ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                                   ((pRingInfo->f4KValue) *
                                    (pRingInfo->u4PeriodicTimerValue)));
            }
            if (pRingInfo->bFopDefect == ERPS_FOP_TO_DEFECT)
            {
                ErpsTrapSendTrapNotifications (pRingInfo,
                                               ERPS_TRAP_TFOP_DEFECT_CLEAR);
                RingDynInfo.u1AlarmStatus &= ~ERPS_FOP_DEFECT;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_FOP_DEFECT);
            }
            if ((pRingInfo->u4RplIfIndex != 0) &&
                (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE))
            {
                ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
            }
        }
    }

    ErpsMsgComputeRequestType (&u1Request);
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC,
                      "ErpsMsgProcessRapsMsg rcvd at IfIndex:%d "
                      "with request (5:FS  8:SF  9:MS  17:NR-RB  18:NR) --:%d\r\n\r\n",
                      pMsg->MepInfo.u4IfIndex, u1Request);

    /* When manual switch applied is followed by save and reload, Signal 
     * failure will occur during reload, which will override the manual switch 
     * applied. In order to restore the manual switch, MsrManualSwitchIndication 
     * variable is set and once after receiving NRRB request from RPL, 
     * SEM is triggered for manual switch request for the IfIndex where 
     * manual switch is applied.Same scenario is handled for non-revertive case*/

    if (((pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE) &&
         (u1Request == ERPS_RING_RAPS_NRRB_REQUEST)) ||
        ((pRingInfo->bMsrManualSwitchIndication == OSIX_TRUE) &&
         (u1Request == ERPS_RING_RAPS_NR_REQUEST) &&
         (pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE) &&
         (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)))
    {
        pRingInfo->u4OldSwitchCmdIfIndex = pRingInfo->u4SwitchCmdIfIndex;
        pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4MsrSwitchCmdIfIndex;
        if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                         ERPS_RING_MANUAL_SWITCH_REQUEST,
                                         NULL) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgProcessRapsMsg failed in priority "
                              "request handling for manual switch.\r\n\r\n");
        }
        return OSIX_SUCCESS;
    }

    /* Implementation of  Failure of protocol defect (FOP)
     * feature as per Section 10.4 in ITU-T G.8032 Y.1344(03/2010).
     * Trap and trace message is generated when a R-APS NR-RB 
     * message from a different node is received at the 
     * RPL Owner node.*/
    if ((u1Request == ERPS_RING_RAPS_NRRB_REQUEST) &&
        (pRingInfo->u4RplIfIndex != 0) &&
        (MEMCMP (gErpsRcvdMsg.RcvdNodeId,
                 ERPS_NODE_ID (pRingInfo->u4ContextId),
                 sizeof (tMacAddr)) != 0))
    {
        if (pRingInfo->bFopPMDefect != ERPS_FOP_PM_DEFECT)
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_PROV_MISMATCH);
            pRingInfo->bFopPMDefect = ERPS_FOP_PM_DEFECT;
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg"
                              " NR-RB message received on RPL node, From other node"
                              ", Failure of Protocol - Provisioning Mismatch.\r\n");
        }
        /* Implementation clearance of  Provisioning Mismatch
         * Timer will be restarted for every NR-RB message
         * received on RPL node from other node.
         * Provisioning Mismatch will be cleared if RPL node
         * does not receive NR-RB message from any other node */

        ErpsTmrStopTimer (pRingInfo, ERPS_FOP_PM_TMR);
        ErpsTmrStartTimer (pRingInfo, ERPS_FOP_PM_TMR, (UINT4)
                           (ERPS_FOP_PM_CLEAR *
                            pRingInfo->u4PeriodicTimerValue));
        return OSIX_FAILURE;
    }

    /* Flush Logic Implementation as per Specification
     * ITU-T G.8032/Y.1344 (03/2010 Section 10.1.10. */
    if ((pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2) &&
        (pRingInfo->u1FlushLogicSupport == OSIX_ENABLED))
    {
        if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
            (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
            (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
        {
            u4RcvdPort = ERPS_RAPS_PDU_RCVD_RING_PORT_1;
        }
        else
        {
            u4RcvdPort = ERPS_RAPS_PDU_RCVD_RING_PORT_2;
        }
        ErpsMsgFlushLogic (pRingInfo, u4RcvdPort);
    }

    /* Update the R-APS Valid message received count */
    ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo, ERPS_STATS_RAPS_RCVD);

    /* As per standard, the self Node Id packets are ignored from processing.
     * So only the processed packets are incremented in ring statistics.*/
    if (MEMCMP (gErpsRcvdMsg.RcvdNodeId, ERPS_NODE_ID (pRingInfo->u4ContextId),
                sizeof (tMacAddr)) != 0)
    {
        if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_EVENT)
        {
            ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                          ERPS_STATS_EVENT_RCVD);
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_OPTIMIZE);

            return OSIX_SUCCESS;
        }
    }

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;

    /* Updating the R-APS Status */
    if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_SF)
    {
        /* Update the fsErpsRingPortDefectEncTime object to measure the ERPS performance time when it receives the SF message.
           Difference between the DefectEncTime and PortStatChange can be measured. */

        if (pRingInfo->u4RplIfIndex != 0)
        {
            if (pRingInfo->u1RapsFlag == OSIX_FALSE)

            {
                /*Measuring the defect /clear time between the nodes
                   when the first SF/CSF packet arrives */
                ErpsUtilMeasureTime (pRingInfo, u4IfIndex,
                                     ERPS_RAPS_DEFECT_ENC_TIME);

                pRingInfo->u1RapsFlag = OSIX_TRUE;

            }
        }

        /* Update the R-APS PDU received status here. */
        if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
            (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
            (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
        {
            /* If RAPS SF packets generated, 
             * because of Manual Switch or Force Switch                     
             * are received on the Switch Port will be dropped.
             */

            if (((pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_FORCE) ||
                 (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)) &&
                (MEMCMP (gErpsRcvdMsg.RcvdNodeId,
                         ERPS_NODE_ID (pRingInfo->u4ContextId),
                         sizeof (tMacAddr)) == 0) &&
                (pRingInfo->u4Port1IfIndex == pRingInfo->u4SwitchCmdIfIndex))
            {
                return OSIX_SUCCESS;
            }

            RingDynInfo.u4RingNodeStatus |= ERPS_NODE_STATUS_PORT1_RAPS_SF;
        }
        else
        {
            /* If RAPS SF packets generated, 
             * because of Manual Switch or Force Switch                     
             * are received on the Switch Port will be dropped.
             */

            if (((pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_FORCE) ||
                 (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)) &&
                (MEMCMP (gErpsRcvdMsg.RcvdNodeId,
                         ERPS_NODE_ID (pRingInfo->u4ContextId),
                         sizeof (tMacAddr)) == 0) &&
                (pRingInfo->u4Port2IfIndex == pRingInfo->u4SwitchCmdIfIndex))
            {
                return OSIX_SUCCESS;
            }

            RingDynInfo.u4RingNodeStatus |= ERPS_NODE_STATUS_PORT2_RAPS_SF;
        }
        ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo, ERPS_STATS_SF_RCVD);
    }
    else if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_NR)
    {

        /* Update the fsErpsRingPortDefectClearedTime object to measure the ERPS performance time when it receives the ClearSF message.
           Difference between the DefectClearedTime and PortStatChange can be measured. */

        if (pRingInfo->u4RplIfIndex != 0)
        {
            if (pRingInfo->u1RapsFlag == OSIX_TRUE)
            {

                /*Measuring the defect /clear time between the nodes
                   when the first SF/CSF packet arrives */
                ErpsUtilMeasureTime (pRingInfo, u4IfIndex,
                                     ERPS_RAPS_DEFECT_CLR_TIME);
                pRingInfo->u1RapsFlag = OSIX_FALSE;
            }
        }

        /* Update the Port failed status here. */
        if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
            (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
            (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
        {
            RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_PORT1_RAPS_SF);
        }
        else
        {
            RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_PORT2_RAPS_SF);
        }
    }

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);
    if (u1Request == ERPS_RING_RAPS_NR_REQUEST)
    {
        /* FOP-TO trap should not be encountered if the switch has its 
         * highest priority as LOCAL SF(As per std.  trap should not be 
         * raised on a ring port reporting  link  level  failure */
        if (pRingInfo->u1HighestPriorityRequest == ERPS_RING_LOCAL_SF_REQUEST)
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
        }
    }

    /* If guard timer is running, all R-APS messages  received for the ring 
     * will be ignored  */

    if (pRingInfo->u4RingNodeStatus & ERPS_GUARD_TIMER_RUNNING)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg guard "
                          "timer is running, received R-APS msg dropped.\r\n");
        return OSIX_SUCCESS;
    }

    if ((u1Request == ERPS_RING_RAPS_NRRB_REQUEST) &&
        (pRingInfo->u4RplIfIndex != 0))
    {
        /* NR RB is received on RPL owner ignore the message. With this 
         * in case of more than one RPL owner in a ring then considring 
         * the ring is segmented  */
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg "
                          "NR-RB message received on RPL node, "
                          "Message ignored.\r\n");
        return OSIX_SUCCESS;
    }

    if (MEMCMP (gErpsRcvdMsg.RcvdNodeId, ERPS_NODE_ID (pRingInfo->u4ContextId),
                sizeof (tMacAddr)) == 0)
    {
        /* R-APS NR of the same node will be given to SEM. This will be
         * used in RPL Owner to start the WTR, if the RPL port is failed
         * and recovered. 
         *
         * Other than NR, all other R-APS Pkts of his own node will not
         * be given to SEM */

        if (u1Request != ERPS_RING_RAPS_NR_REQUEST)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg "
                              "receives its own RAPS PDU. R-APS message ignored."
                              "\r\n");
            return OSIX_SUCCESS;
        }

        /* When ERPS Compatible version is V1 then Recvd Self Node NR msg will be handled
         * in both Revertive and Non-Revertive mode, where as when ERPS compatible 
         * version is V2 then Self Node NR msg will be handled only if Received Node is
         * RPL Owner node and if node is operating in Non-Revertive mode else Received
         * R-APS NR will be  dropped. */
        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2) && (pRingInfo->u4RplIfIndex != 0)
            && (pRingInfo->u1OperatingMode != ERPS_RING_NON_REVERTIVE_MODE))
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsMsgProcessRapsMsg "
                              "receives its own RAPS PDU. R-APS message ignored."
                              "\r\n");
            return OSIX_SUCCESS;
        }
    }

    /* NR and NRRB  received PDU counters are incremented after own 
     * node id comparison. As per standard, the self Node Id packets  
     * are ignored from processing. So only the processed packets 
     * are incremented in ring statistics  */
    if (u1Request == ERPS_RING_RAPS_NRRB_REQUEST)
    {
        ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo, ERPS_STATS_NRRB_RCVD);
    }

    if (u1Request == ERPS_RING_RAPS_NR_REQUEST)
    {
        ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo, ERPS_STATS_NR_RCVD);
    }
    /* When Sub_Ring RPL is configured on interconnected node
     * 1. RPL initiated RAPS packets are not forwarded via virtual channel
     * 2. if the packets reached by other interconnected
     * node via Virtual Channel,its consumed and same packet
     * is forwarded to other nodes in Sub-Ring with same level
     * 3. packet which is received with UP-MEP level are dropped in 
     * other than interconnected node due to level validation 

     * To overcome the problem the below changes are done
     * when RAPS packets are received on interconnected node by 
     * virtual channel(UP-MEP) from other interconnected node
     * Construct the packet with received RAPS information 
     * and send out the packet to other nodes with Down MEP level
     * so that ring will move to proper state */

    if ((pRingInfo->CfmEntry.u4Port2MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port2MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port2MEPId == MepInfo.u4MepId))
    {
        if ((pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_DISABLED) &&
            (pRingInfo->u4Port2IfIndex == 0))
        {
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;

            /* This flag will be set, when sub-Ring interconnected node
             * (Port = 0)receives packets from other interconnected node
             * Via Virtual channel to forward the received packets
             * to other ring nodes in sub-Ring with without change
             * any packet bit status/bit field*/
            pRingInfo->u1IsMsgRcvd = OSIX_TRUE;

            if (ErpsTxStartRapsMessages (pRingInfo, u1Statusbit,
                                         gErpsRcvdMsg.u1MsgType) ==
                OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTxStartRapsMessages failed to forward"
                                  "R-APS messages which is received on"
                                  "Virtual Channel\r\n");
            }
            /* Resetting flag to default */
            pRingInfo->u1IsMsgRcvd = OSIX_FALSE;
            pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
        }
    }
    /* If both the ring ports are in  failed state and if one of the 
     * ring ports have recovered then unblock the recovered port on 
     * reception of R-APS SF */

    /* If the recovered port is unblocked immediately on receiving 
     * a Local Clear SF if the other port in the ring node is in failed
     * state it can result in loop between main ring 
     * and sub-ring , if the same is a virtual channel recovery */

    /* To avoid the loop, rather than unblocking the recovered port
     * on reception of local clear SF ( if the other port in the same 
     * node is in failed state ) we are unblocking the recovered port
     * on reception of R-APS SF */

    if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_SF)
    {
        if ((pRingInfo->u1OperatingMode != ERPS_RING_NON_REVERTIVE_MODE) ||
            (pRingInfo->u1RecoveryMethod != ERPS_RING_MANUAL_RECOVERY) ||
            (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE))
        {
            /* Open Non-failed or Manual switch port, When
             * Ring is in Revertive Mode of operation or
             * Non-Revertive mode and Auto recovery mode.
             */
            /* Unblocking Non-failed ports if the other port 
             * in the ring node is in failed state (i.e., the 
             * highest priority request is Local SF)*/
            if (pRingInfo->u1HighestPriorityRequest ==
                ERPS_RING_LOCAL_SF_REQUEST)
            {
                if (pRingInfo->u4Port1IfIndex != pRingInfo->u4SwitchCmdIfIndex)
                {
                    if (!(pRingInfo->u1Port1Status & ERPS_PORT_FAILED))
                    {
                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port1IfIndex,
                                              ERPS_PORT_UNBLOCKING);
                    }
                }
                if (pRingInfo->u4Port2IfIndex != pRingInfo->u4SwitchCmdIfIndex)
                {
                    if (!(pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
                    {
                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port2IfIndex,
                                              ERPS_PORT_UNBLOCKING);
                    }
                }
            }
        }
    }
    if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_FS)
    {
        /* Case 1 :If RAPS FS packets are  received on the Switch and  
         *     Port configured ERPS compatible version is v1. 
         * Case 2: If received packet has Ring Version as V1 but the Message type is FS 
         *         In both the above cases packets will be dropped ,respective Trap is generated 
         *         and also discard counters are incremented*/

        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION1)
            || (gErpsRcvdMsg.u1RingVersion == ERPS_RAPS_V1_PDU))
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_VERSION_MISMATCH);
            ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                          ERPS_STATS_VERSION_DISC);
            return OSIX_SUCCESS;
        }

        /*Measuring the defect /clear time between the nodes
           when the first FS packet arrives to RPL  */
        if (pRingInfo->u4RplIfIndex != 0)
        {
            if (pRingInfo->u1RapsFlag == OSIX_FALSE)
            {
                ErpsUtilMeasureTime (pRingInfo, u4IfIndex,
                                     ERPS_DEFECT_ENC_SWITCH_CMD);

                pRingInfo->u1RapsFlag = OSIX_TRUE;
            }
        }

        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                          ERPS_STATS_FS_RCVD);
        }
    }
    if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_MS)
    {
        /* Case 1 :If RAPS MS packets are  received on the Switch and
         *         Port configured ERPS compatible version is v1.
         * Case 2: If received packet has Ring Version as V1 but the Message type is MS
         *         In both the above cases packets will be dropped ,respective Trap is generated
         *         and also discard counters are incremented*/

        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION1)
            || (gErpsRcvdMsg.u1RingVersion == ERPS_RAPS_V1_PDU))
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_VERSION_MISMATCH);
            ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                          ERPS_STATS_VERSION_DISC);
            return OSIX_SUCCESS;
        }

        /*Measuring the defect /clear time between the nodes
           when the first MS  packet arrives to RPL  */
        if (pRingInfo->u4RplIfIndex != 0)
        {
            if (pRingInfo->u1RapsFlag == OSIX_FALSE)
            {
                ErpsUtilMeasureTime (pRingInfo, u4IfIndex,
                                     ERPS_DEFECT_ENC_SWITCH_CMD);

                pRingInfo->u1RapsFlag = OSIX_TRUE;
            }
        }

        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            ErpsUtilUpdateRingStatistics (pRingInfo, MepInfo,
                                          ERPS_STATS_MS_RCVD);
        }
    }

    if (ErpsSmCalTopPriorityRequest (pRingInfo, u1Request, &gErpsRcvdMsg)
        == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgProcessRapsMsg failed in PriorityLogic.\r\n");
        return OSIX_FAILURE;
    }

    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT1_RCVD_NODE_ID);

    }
    else
    {
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT2_RCVD_NODE_ID);

    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgExtractRapsInfo                           */
/*                                                                           */
/*    Description         : This function used to extract the R-APS specific */
/*                          information received from the R-APS messages.    */
/*                                                                           */
/*    Input(s)            : pQMsg        - Queue Message                     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsMsgExtractRapsInfo (tErpsQMsg * pQMsg)
{
    UINT4               u4PduSize = 0;
    UINT1               au1RapsData[ERPS_RAPS_PDU_RAPS_INFO_LEN];
    UINT1               u1RingMacId = ERPS_MIN_RING_MACID;
    UINT1               u1RapsVersion = 0;

    u4PduSize =
        CRU_BUF_Get_ChainValidByteCount (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu);
    ERPS_PKT_DUMP (pQMsg->u4ContextId, DUMP_TRC,
                   pQMsg->unMsgParam.RapsPduMsg.pRapsPdu, u4PduSize,
                   "ErpsMsgExtractRapsInfo: Dumping the received RAPS PDU\n");

    /* Get the Last octect of the destination MAC address,
     * which will be considered as ring mac id */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu,
                               &u1RingMacId, (MAC_ADDR_LEN - 1), 1);

    /*  get the RAPS (version+MELinfo) from the packet) */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu,
                               &u1RapsVersion,
                               pQMsg->unMsgParam.RapsPduMsg.u1TlvOffset, 1);

    /* Get the R-APS PDU from the packet */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.RapsPduMsg.pRapsPdu,
                               au1RapsData,
                               (pQMsg->unMsgParam.RapsPduMsg.u1TlvOffset +
                                ERPS_RAPS_PDU_HDR_LEN),
                               ERPS_RAPS_PDU_RAPS_INFO_LEN);

    gErpsRcvdMsg.u1MsgType = (UINT1) ((au1RapsData[0] & ERPS_REQ_FIELD)
                                      >> ERPS_REQ_FIELD_OFFSET);
    gErpsRcvdMsg.u1SubCode = (au1RapsData[0] & ERPS_RES1_FIELD);
    gErpsRcvdMsg.u1RBBitStatus = (au1RapsData[1] & ERPS_RB_FIELD) ? 1 : 0;
    gErpsRcvdMsg.u1DNFBitStatus = (au1RapsData[1] & ERPS_DNF_FIELD) ? 1 : 0;
    gErpsRcvdMsg.u1BPRBitStatus = (au1RapsData[1] & ERPS_BPR_BIT_SET) ? 1 : 0;
    gErpsRcvdMsg.u1DERPSBitStatus =
        (au1RapsData[1] & ERPS_DERPS_BIT_SET) ? 1 : 0;

    /* Computing the version field */
    gErpsRcvdMsg.u1RingVersion = u1RapsVersion & ERPS_VERSION_FIELD;

    if (gErpsRcvdMsg.u1RingVersion == ERPS_VERSION2_VALUE)
    {
        gErpsRcvdMsg.u1StatusReserved =
            (au1RapsData[1] & ERPS_V2_STATUS_RES_FIELD);
    }
    else
    {
        gErpsRcvdMsg.u1StatusReserved =
            (au1RapsData[1] & ERPS_V1_STATUS_RES_FIELD);
    }
    MEMCPY (gErpsRcvdMsg.RcvdNodeId,
            &au1RapsData[ERPS_NODE_ID_FIELD], sizeof (tMacAddr));

    gErpsRcvdMsg.u1RingMacId = u1RingMacId;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgValidateRapsMsg                           */
/*                                                                           */
/*    Description         : This function used to validate the R-APS         */
/*                          information received from the R-APS messages.    */
/*                                                                           */
/*    Input(s)            : ErpsRcvdMsg - R-APS information received in PDU  */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - If the packet validation succeeded*/
/*                        : OSIX_FAILURE - If the packet validation failed   */
/*****************************************************************************/
PRIVATE INT4
ErpsMsgValidateRapsMsg (VOID)
{
    if ((gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_SF) &&
        (gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_NR) &&
        (gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_FS) &&
        (gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_MS) &&
        (gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_EVENT))
    {
        return OSIX_FAILURE;
    }

    if (gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_EVENT)
    {
        if ((gErpsRcvdMsg.u1RBBitStatus != 0) ||
            (gErpsRcvdMsg.u1DNFBitStatus != 0) ||
            (gErpsRcvdMsg.u1StatusReserved != 0))
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgFlushLogic                                */
/*                                                                           */
/*    Description         : This function implements the Flush Logic feature */
/*                          described in Section 10.1.10 of                  */
/*                          ITU-T G.8032/Y.1344 (03/2010).                   */
/*                                                                           */
/*    Input(s)            : pRingInfo   - Pointer to ring node for which     */
/*                        :               R-APS PDU is received.             */
/*                        : u4RcvdPort  - Port on which R-APS PDU was        */
/*                                        received.                          */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsMsgFlushLogic (tErpsRingInfo * pRingInfo, UINT4 u4RcvdPort)
{
    tErpsPortNodeIdBPRPair RcvdNodeIdBPRPair;
    tErpsPortNodeIdBPRPair *pRcvdPortPrevNodeIdBPRPair = NULL;
    tErpsPortNodeIdBPRPair *pOtherPortPrevNodeIdBPRPair = NULL;
    unErpsRingDynInfo   RingDynInfo;
    /* The following logic shall be implemented on the extracted
     * (Node Id, BPR) pair from the PDU on ring port :-
     * The extracted pair shall be compared with the already stored
     * pair on the received ring port and then,
     *   a)If there is no difference, then return from the flush logic
     *   b)If there is difference, then following logic shall be
     *     implemented
     *        - Delete the previously stored pair.
     *        - If the R-APS PDU is not R-APS NR, then store the received
     *          (NodeId, BPR) pair. 
     *        - If DNF bit is not set and the received packet is not having
     *          self Node Id then do the following :
     *              -0- If the Received (Node ID, BPR) pair is different 
     *                  from the (Node Id, BPR) pair already stored at the
     *                  other ring port.
     *                      := then Flush FDB.
     *   [Reference Section 10.1.10 of ITU-T G.8032 / Y.1344 standard ]
     */

    MEMSET (&RcvdNodeIdBPRPair, 0, sizeof (tErpsPortNodeIdBPRPair));

    MEMSET (&RingDynInfo, 0, sizeof (unErpsRingDynInfo));

    if (!((gErpsRcvdMsg.u1MsgType == ERPS_MESSAGE_TYPE_NR) &&
          ((gErpsRcvdMsg.u1RBBitStatus) == 0)))
    {
        /*All packets except NR pdu recieved nodeId pair will be stored. */
        RcvdNodeIdBPRPair.u1BPRStatus = gErpsRcvdMsg.u1BPRBitStatus;
        MEMCPY (RcvdNodeIdBPRPair.NodeId, gErpsRcvdMsg.RcvdNodeId,
                sizeof (tMacAddr));
    }
    else
    {
        /*while recieving NR pdu deleting the stored nodeId pair. */
        if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
        {
            MEMSET (&(pRingInfo->Port1StoredNodeIdBPRPair), 0,
                    sizeof (tErpsPortNodeIdBPRPair));
            MEMSET (&(pRingInfo->Port2StoredNodeIdBPRPair), 0,
                    sizeof (tErpsPortNodeIdBPRPair));
        }
        else if (u4RcvdPort == ERPS_RAPS_PDU_RCVD_RING_PORT_1)
        {
            MEMSET (&(RingDynInfo.Port1StoredNodeIdBPRPair), 0,
                    sizeof (tErpsPortNodeIdBPRPair));
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT1_BPR_PAIR_NODE_ID);
        }
        else
        {

            MEMSET (&(RingDynInfo.Port2StoredNodeIdBPRPair), 0,
                    sizeof (tErpsPortNodeIdBPRPair));
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT2_BPR_PAIR_NODE_ID);
        }

    }

    if (u4RcvdPort == ERPS_RAPS_PDU_RCVD_RING_PORT_1)
    {
        pRcvdPortPrevNodeIdBPRPair = &(pRingInfo->Port1StoredNodeIdBPRPair);
        pOtherPortPrevNodeIdBPRPair = &(pRingInfo->Port2StoredNodeIdBPRPair);
    }
    else
    {
        pRcvdPortPrevNodeIdBPRPair = &(pRingInfo->Port2StoredNodeIdBPRPair);
        pOtherPortPrevNodeIdBPRPair = &(pRingInfo->Port1StoredNodeIdBPRPair);
    }

    if ((ErpsUtilCompareNodeIdBprPair (&RcvdNodeIdBPRPair,
                                       pRcvdPortPrevNodeIdBPRPair)
         == ERPS_NODE_ID_BPR_PAIRS_MATCH))
    {

        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsMsgFlushLogic in Port %u\n"
                          "Received (Node ID, BPR) pair : "
                          "  (%02d:%02d:%02d:%02d:%02d:%02d, %u)\r\n"
                          " Previous(Node ID, BPR) pair : "
                          "  (%02d:%02d:%02d:%02d:%02d:%02d, %u)\r\n"
                          "Same as previous pair, So No Action.\r\n",
                          u4RcvdPort, gErpsRcvdMsg.RcvdNodeId[0],
                          gErpsRcvdMsg.RcvdNodeId[1],
                          gErpsRcvdMsg.RcvdNodeId[2],
                          gErpsRcvdMsg.RcvdNodeId[3],
                          gErpsRcvdMsg.RcvdNodeId[4],
                          gErpsRcvdMsg.RcvdNodeId[5],
                          gErpsRcvdMsg.u1BPRBitStatus,
                          pRcvdPortPrevNodeIdBPRPair->NodeId[0],
                          pRcvdPortPrevNodeIdBPRPair->NodeId[1],
                          pRcvdPortPrevNodeIdBPRPair->NodeId[2],
                          pRcvdPortPrevNodeIdBPRPair->NodeId[3],
                          pRcvdPortPrevNodeIdBPRPair->NodeId[4],
                          pRcvdPortPrevNodeIdBPRPair->NodeId[5],
                          pRcvdPortPrevNodeIdBPRPair->u1BPRStatus);
        return;
    }

    MEMSET (pRcvdPortPrevNodeIdBPRPair, 0, sizeof (tErpsPortNodeIdBPRPair));
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsMsgFlushLogic Deleting Previous"
                      " Node ID, BPR pair : (%02d:%02d:%02d:%02d:%02d:%02d, %u)"
                      " for port %d.\r\n",
                      pRcvdPortPrevNodeIdBPRPair->NodeId[0],
                      pRcvdPortPrevNodeIdBPRPair->NodeId[1],
                      pRcvdPortPrevNodeIdBPRPair->NodeId[2],
                      pRcvdPortPrevNodeIdBPRPair->NodeId[3],
                      pRcvdPortPrevNodeIdBPRPair->NodeId[4],
                      pRcvdPortPrevNodeIdBPRPair->NodeId[5],
                      pRcvdPortPrevNodeIdBPRPair->u1BPRStatus, u4RcvdPort);

    /* IF message received is not NR type or NR type but
     * RB bit is set which makes NR-RB type. Then only 
     * store the Recieved (NodeID, BPR) pair.
     */
    if ((gErpsRcvdMsg.u1MsgType != ERPS_MESSAGE_TYPE_NR) ||
        (gErpsRcvdMsg.u1RBBitStatus))
    {
        if (u4RcvdPort == ERPS_RAPS_PDU_RCVD_RING_PORT_1)
        {

            MEMCPY (&RingDynInfo.Port1StoredNodeIdBPRPair, &RcvdNodeIdBPRPair,
                    sizeof (tErpsPortNodeIdBPRPair));
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT1_BPR_PAIR_NODE_ID);
        }
        else
        {
            MEMCPY (&RingDynInfo.Port2StoredNodeIdBPRPair, &RcvdNodeIdBPRPair,
                    sizeof (tErpsPortNodeIdBPRPair));
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PORT2_BPR_PAIR_NODE_ID);
        }
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsMsgFlushLogic Storing Received"
                          " Node ID, BPR pair : (%02d:%02d:%02d:%02d:%02d:%02d, %u)"
                          " for port %d as R-APS MSG received is not NR.\r\n",
                          gErpsRcvdMsg.RcvdNodeId[0],
                          gErpsRcvdMsg.RcvdNodeId[1],
                          gErpsRcvdMsg.RcvdNodeId[2],
                          gErpsRcvdMsg.RcvdNodeId[3],
                          gErpsRcvdMsg.RcvdNodeId[4],
                          gErpsRcvdMsg.RcvdNodeId[5],
                          gErpsRcvdMsg.u1BPRBitStatus, u4RcvdPort);
    }

    if ((!(gErpsRcvdMsg.u1DNFBitStatus)) &&
        (MEMCMP (gErpsRcvdMsg.RcvdNodeId, ERPS_NODE_ID (pRingInfo->u4ContextId),
                 sizeof (tMacAddr)) != 0))
    {
        if ((ErpsUtilCompareNodeIdBprPair (&RcvdNodeIdBPRPair,
                                           pOtherPortPrevNodeIdBPRPair))
            == ERPS_NODE_ID_BPR_PAIRS_DIFFER)
        {

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsMsgFlushLogic Received "
                              "Node ID, BPR pair different from Stored Node ID, BPR "
                              "pair in both ports, So Trigger Flush FDB.\r\n");
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_OPTIMIZE);
        }
    }
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgComputeRequestType                        */
/*                                                                           */
/*    Description         : This function used to compute the request type   */
/*                          based on the R-APS message received.             */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu1Request   - Request type                      */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsMsgComputeRequestType (UINT1 *pu1Request)
{
    switch (gErpsRcvdMsg.u1MsgType)
    {
        case ERPS_MESSAGE_TYPE_SF:
            *pu1Request = ERPS_RING_RAPS_SF_REQUEST;
            break;

        case ERPS_MESSAGE_TYPE_NR:
            *pu1Request = ((gErpsRcvdMsg.u1RBBitStatus) ?
                           ERPS_RING_RAPS_NRRB_REQUEST :
                           ERPS_RING_RAPS_NR_REQUEST);
            break;

        case ERPS_MESSAGE_TYPE_FS:
            *pu1Request = ERPS_RING_RAPS_FS_REQUEST;
            break;

        case ERPS_MESSAGE_TYPE_MS:
            *pu1Request = ERPS_RING_RAPS_MS_REQUEST;
            break;

        default:
            break;
    }
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgHandleCfmSignal                               */
/*                                                                         */
/* DESCRIPTION      : Function is used to handle the CFM signal status for */
/*                    a MEP.                                               */
/*                                                                         */
/* INPUT            : pMsg - Pointer to the message to be posted.          */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : NONE                                                 */
/*                                                                         */
/***************************************************************************/
PRIVATE VOID
ErpsMsgHandleCfmSignal (tErpsQMsg * pMsg)
{
    /*Check for the underlying MEP monitoring type */

    if (pMsg->u4MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
    {
        ErpsMsgHandleCfmSignalFromMpls (pMsg);
    }
    else if (pMsg->u4MonitoringType == ERPS_MONITOR_MECH_CFM)
    {
        ErpsMsgHandleCfmSignalFromEcfm (pMsg);
    }
    return;
}

/*****************************************************************************/
/*    Function Name       : ErpsMsgBlockIndicationLogic                      */
/*                                                                           */
/*    Description         : This function implements the Block Indication    */
/*                          Logic procedure used in Minimizing Segmenation   */
/*                          in Inter connected Rings as per Appendix X of    */
/*                          ITU-T G.8032/Y.1344 (03/2010).                   */
/*                                                                           */
/*    Input(s)            : pRingInfo   - Pointer to ring node for which     */
/*                          Local fault /Local fault clear indication is     */
/*                          received from eCFM.                              */
/*                        : u4Status    - Indication type from eCFM, this    */
/*                          can be one of the following 2 values             */
/*                          - ERPS_CFM_SIGNAL_FAIL                           */
/*                          - ERPS_CFM_SIGNAL_OK                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsMsgBlockIndicationLogic (tErpsRingInfo * pRingInfo, UINT4 u4Status)
{
    UINT1               u1RingConfigInterConnNode =
        ERPS_RING_INTER_CONN_NODE_NONE;
    UINT1               u1RingConfigMultipleFailure =
        ERPS_RING_MULTIPLE_FAILURE_DISABLED;

    u1RingConfigInterConnNode = pRingInfo->u1RingConfigInterConnNode;
    u1RingConfigMultipleFailure = pRingInfo->u1RingConfigMultipleFailure;

    /* Minimizing Segmentation Procedure :-
     * The Following procedure is used to minimize segmentation of the traffic
     * from main ring to subring.
     *   On Detection of Loss of connectivity between the interconnection nodes.
     *    - UP MEP when detects SF sends an indication to the Block 
     *      Indication Logic.
     *    - Block Indication Logic of the inter-connection node accepts and
     *    and compares the two values and if
     *       * two values are identical then, performs MS command on sub-ring port.
     *       * If the two values are different - then ignore.
     *    Similarly when the connection between the inter-connection nodes are
     *    recovered.
     *    - UP MEP sends an indication to the Block Indication Logic Procedure
     *    - Block Indication Logic accepts and compares the two values and if
     *       * two values are identical then, Clear MS to the sub-ring port.
     *       * if the two values are different, then ignore.
     */

    if (((u1RingConfigInterConnNode == ERPS_RING_INTER_CONN_NODE_PRIMARY) &&
         (u1RingConfigMultipleFailure == ERPS_RING_MULTIPLE_FAILURE_PRIMARY)) ||
        ((u1RingConfigInterConnNode == ERPS_RING_INTER_CONN_NODE_SECONDARY) &&
         (u1RingConfigMultipleFailure == ERPS_RING_MULTIPLE_FAILURE_SECONDARY)))
    {
        switch (u4Status)
        {
            case ERPS_CFM_SIGNAL_FAIL:
                /* When VC failure is simulated in main ring,interonnected
                 * node moves to Manual Switch state,which Will send MS 
                 * packets but it will not receive any packets,So FOP defect 
                 * should not be encountered for interconnected node */
                if (pRingInfo->bDFOP_TO == OSIX_ENABLED)
                {
                    ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
                }
                /* Note:- 
                 * Switch command is not set to ERPS_SWITCH_COMMAND_MANUAL
                 * Here as MS is applied by Block Indication Logic Procedure, and 
                 * Not by Operator.*/

                /* When Manual switch is appiled because of VC failure, Switch
                 * port updation should be done only for the interconnected node
                 * were "interconnected node" and "Multiple failures" objects 
                 * are configured identically. */
                pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4Port1IfIndex;

                if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                 ERPS_RING_MANUAL_SWITCH_REQUEST,
                                                 &gErpsRcvdMsg) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMsgBlockIndicationLogic failed returned"
                                      " from Priority Logic for Local MS"
                                      "Request.\r\n");
                }
                break;

            case ERPS_CFM_SIGNAL_OK:
                /* If Top Priority request is MS then Local MS is present
                 * in the ring node, But if MS was applied by Operator then
                 * SwitchCmdType will not be None. If SwitchCmdType is None, Then it means
                 * MS was applied on the Node by Block Indication Logic
                 * procedure as part of Minimizing segmenation.
                 * From the Specification 
                 * - It is Clear that Force Switch present in the
                 * Subring should not be cleared after multiple failure 
                 * recovery in the main ring as part of Block Indication
                 * Logic Procedure.
                 * - But what should be done when Manual switch is applied in the 
                 * subring interconnected node by operator, after multiple failure
                 * recovery in the main ring as part of Block indication Logic
                 * Procedure is not clear. So in the Current Implementation only
                 * MS applied as part of Block indication will be cleared by
                 * checking Switch command type.
                 * if MS is operator given then Switch command type
                 * will be set to ERPS_SWITCH_COMMAND_MANUAL, but if it is
                 * set to ERPS_SWITCH_COMMAND_NONE then it means MS was 
                 * applied by Block indication Logic procedure so it will 
                 * be cleared. */
                /* FOP-TO defect should be cleared for interconnected node 
                 * When there is VC failure in main ring */
                if (pRingInfo->bDFOP_TO == OSIX_ENABLED)
                {
                    ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                                       (((UINT4) (pRingInfo->f4KValue)) *
                                        (pRingInfo->u4PeriodicTimerValue)));
                }
                if ((pRingInfo->u1HighestPriorityRequest ==
                     ERPS_RING_MANUAL_SWITCH_REQUEST) &&
                    (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_NONE))
                {
                    pRingInfo->u4SwitchCmdIfIndex = pRingInfo->u4Port1IfIndex;

                    if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                     ERPS_RING_CLEAR_REQUEST,
                                                     NULL) == OSIX_FAILURE)
                    {

                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsMsgBlockIndicationLogic failed returned"
                                          " from Priority Logic for Clear MS"
                                          "Request.\r\n");
                    }
                }
                break;
            default:
                break;
        }
    }

    if ((pRingInfo->u1PortRecoveryFlag == OSIX_TRUE) &&
        (u4Status == ERPS_CFM_SIGNAL_OK) &&
        pRingInfo->u1HighestPriorityRequest == ERPS_RING_LOCAL_SF_REQUEST)
    {
        if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                         ERPS_RING_LOCAL_CLEAR_SF_REQUEST,
                                         NULL) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgBlockIndicationLogic failed returned from "
                              "Priority Logic.\r\n");
        }
        pRingInfo->u1PortRecoveryFlag = OSIX_FALSE;

        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgBlockIndicationLogic u1PortRecoveryFlag reset "
                          "as FALSE\r\n");
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgProcessLinkSF                                 */
/*                                                                         */
/* DESCRIPTION      : Function is used to process the local fault          */
/*                    indication received from the ECFM module.            */
/*                                                                         */
/* INPUT            : pRingInfo  - Pointer to the ring entry.             */
/*                    MepInfo     - MEP info for which the fault is        */
/*                                  reported                               */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
ErpsMsgProcessLinkSF (tErpsRingInfo * pRingInfo, tErpsCfmMepInfo MepInfo)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT1               u1IsRplBlocked = OSIX_FALSE;

    /* Update the Port Failed Status here. */
    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        /* When Signal Failure is triggered for already failed port it 
         * should not be processed. */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) == ERPS_PORT_FAILED)
        {
            return OSIX_SUCCESS;
        }
        RingDynInfo.u1Port1Status = pRingInfo->u1Port1Status;
        RingDynInfo.u1Port1Status |= ERPS_PORT_FAILED;

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT1_STATUS);
        pRingInfo->Port1Stats.u4FailedCount++;

        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus |= ERPS_NODE_STATUS_PORT1_LOCAL_SF;

        /* If the other port is already failed, then BLOCK this port
         * here and and don't give it to sem,
         * Else
         * don't block the port here and give it to sem. Sem will
         * block the port
         * If Ring is not in Forced Switch State and other port is already
         * failed and blocked, then BLOCK this port here.*/
        if (pRingInfo->u1RingState != ERPS_RING_FORCED_SWITCH_STATE)
        {
            if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
                (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
            {
                /* As per the standard [Rec. ITU-T G.8032/Y.1344 (03/2010)]
                 * section[10.1.14 R-APS block logic] RAPS channel should 
                 * be blocked when top priority request is Local SF/FS.
                 * When 1st port fails sem is triggered and 
                 * HighestPriorityRequestWithoutVC parameter is reset to 
                 * NO_ACTIVE_REQUEST.When 2nd port fails sem will not be 
                 * triggered and its priority is "NO_ACTIVE_REQUEST" instead 
                 * of "LOCAL_SF" which is used for RAPS channel blocking.*/
                pRingInfo->u1HighestPriorityRequestWithoutVC =
                    ERPS_RING_LOCAL_SF_REQUEST;
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                      ERPS_PORT_BLOCKING);
                pRingInfo->u1HighestPriorityRequestWithoutVC =
                    ERPS_RING_NO_ACTIVE_REQUEST;
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMsgProcessLinkSF: Setting Port %u to Blocked state "
                                  ".\r\n", pRingInfo->u4Port1IfIndex);

                if (pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex)
                {
                    u1IsRplBlocked = OSIX_TRUE;
                }
            }
        }
    }
    else
    {
        /* UP MEP is installed in the interconnected nodes of the sub-ring
         * to monitor the virtual channel and take subsequent action in case
         * of UP MEP failure and UP MEP recovery.
         *
         * In Version 1 standard, the approach to minimize segmentation was 
         * not discussed. So in Version 1, the action taken on UP MEP 
         * failure is to process it as Link Failure which will send SF's
         * and will open up the RPL port of the sub-ring. 
         * But this is a proprietary implementation and
         * should not be continued when the compatible Version number is 2. */

        /* Implementation of Minimizing the segmentation in interconnected rings 
         * as per Appendix X of ITU-T G.8032 Y.1344(03/2010)standard.
         * when ERPS Compatibility version is 2, and UP MEP detects Singal Fault
         * in Intermediate Ring node then Block indication Logic Procedure should
         * be called. */

        /* When Signal Failure is triggered for already failed port it 
         * should not be processed. */
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) == ERPS_PORT_FAILED)
        {
            return OSIX_SUCCESS;
        }
        RingDynInfo.u1Port2Status = pRingInfo->u1Port2Status;
        RingDynInfo.u1Port2Status |= ERPS_PORT_FAILED;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT2_STATUS);

        pRingInfo->Port2Stats.u4FailedCount++;
        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus |= ERPS_NODE_STATUS_PORT2_LOCAL_SF;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);

        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)
            && (MepInfo.u1Direction == ERPS_MP_DIR_UP))
        {
            if (pRingInfo->u4Port2IfIndex == 0)
            {
                ErpsMsgBlockIndicationLogic (pRingInfo, ERPS_CFM_SIGNAL_FAIL);
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsMsgProcessLinkSF : UP MEP failure detected"
                                  " on port2 of sub-ring;\r\n");
                return OSIX_SUCCESS;
            }

        }

        /* If the other port is already failed, then BLOCK this port
         * here and and don't give it to sem,
         * Else
         * don't block the port here and give it to sem. Sem will
         * block the port
         * If Ring is not in Forced Switch State and other port is already
         * failed and blocked, then BLOCK this port here.*/

        if (pRingInfo->u1RingState != ERPS_RING_FORCED_SWITCH_STATE)
        {
            if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) &&
                (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING))
            {
                pRingInfo->u1HighestPriorityRequestWithoutVC =
                    ERPS_RING_LOCAL_SF_REQUEST;

                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                      ERPS_PORT_BLOCKING);
                pRingInfo->u1HighestPriorityRequestWithoutVC =
                    ERPS_RING_NO_ACTIVE_REQUEST;

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMsgProcessLinkSF: Setting Port %u to Blocked state "
                                  ".\r\n", pRingInfo->u4Port2IfIndex);

                if (pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex)
                {
                    u1IsRplBlocked = OSIX_TRUE;
                }
            }
        }
    }

    if (u1IsRplBlocked == OSIX_TRUE)
    {
        RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);
    }

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    /* According to the standard G.8021/Y.1341 - Amendment 1(07/2011) 
     * in section 6.1.4.3.4,failure should not be encountered when we 
     * administratively disable the ring ports,When T-FOP feature is enabled 
     * and port or ports got signal failure,then stop the timer so that 
     * T-FOP failure is not encountered */

    ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);

    /* Only first port failed will be intimated to Priority Logic for
     * top priority re-evaluation. There is no possibility of simultanous 
     * link failure as monitoring protocol will notify link failure one by 
     * one. So link failure notification will always be sequential.
     */
    if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) &&
        (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
    {
        /* When link failure occurs on RPL/Neighbor node, DNF status will 
         * be set either thro LocalSF or RAPSSF. If link failure occurs 
         * again on non rpl port of RPL/Neighbor node, DNF status will not be 
         * cleared as SEM is not called and flush will not occur in WTRExpiry */
        if (((pRingInfo->u4RplIfIndex != 0) ||
             (pRingInfo->u4RplNeighbourIfIndex != 0)) &&
            (pRingInfo->bDNFStatus == OSIX_TRUE))
        {
            pRingInfo->bDNFStatus = OSIX_FALSE;
        }

        if (pRingInfo->u1HighestPriorityRequest ==
            ERPS_RING_FORCE_SWITCH_REQUEST)
        {
            ErpsTrapSendTrapNotifications (pRingInfo,
                                           ERPS_TRAP_FORCE_SWITCH_PRESENT);
        }
        else if (pRingInfo->u1HighestPriorityRequest ==
                 ERPS_RING_LOCAL_SF_REQUEST)
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_AUTO_SWITCH);
        }

        /* When hold of timer is configured to a non-zero value and 
         * one of the ring ports failed in the ring, the hold off timer is 
         * started . Now, when the other ring port fails and if hold off 
         * timer is still running, then the failure in the 2nd 
         * ring port must not be handled and flush will not happen
         * when hold-off timer is running
         * [Reference : Section 10.1.8 : G.8032/Y.1344 (03/2010) standard]*/

        if (pRingInfo->u4RingNodeStatus & ERPS_HOLDOFF_TIMER_RUNNING)
        {
            return OSIX_SUCCESS;
        }
        /* If the Second port is failed, SEM won't be triggered,
         * but flushing for the Ring will be done */
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsMsgProcessLinkSF both ports are in failed"
                          "state - StateMachine not triggered.\r\n");
        return OSIX_SUCCESS;
    }

    /* If Hold-Off timer is zero, trigger the state machine directly */

    if (pRingInfo->u4HoldOffTimerValue != 0)
    {
        if (pRingInfo->u4RingNodeStatus & ERPS_HOLDOFF_TIMER_RUNNING)
        {
            /* If Hold-Off timer is running, no action will be taken */
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC, "ErpsMsgProcessLinkSF "
                              "holdoff timer is running, no action.\r\n");
            return OSIX_SUCCESS;
        }
        if (ErpsTmrStartTimer (pRingInfo, ERPS_HOLD_OFF_TMR,
                               pRingInfo->u4HoldOffTimerValue) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgProcessLinkSF failed to started holdoff "
                              "timer.\r\n");
            return OSIX_FAILURE;
        }
    }
    else if (ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_LOCAL_SF_REQUEST,
                                          &gErpsRcvdMsg) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgProcessLinkSF failed returned from Priority "
                          "Logic.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgProcessLinkClearSF                            */
/*                                                                         */
/* DESCRIPTION      : Function is used to process the local fault          */
/*                    clear indication received from the ECFM module.      */
/*                                                                         */
/* INPUT            : pRingInfo  - Pointer to the ring entry.             */
/*                    MepInfo     - MEP info for which the fault is        */
/*                                  reported.                              */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
ErpsMsgProcessLinkClearSF (tErpsRingInfo * pRingInfo, tErpsCfmMepInfo MepInfo)
{
    unErpsRingDynInfo   RingDynInfo;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSubRingEntry      *pSubRingEntry = NULL;
    UINT1               u1IsRplUnBlock = OSIX_FALSE;
    UINT4               u4RemainingTime = 0;
    UINT1               u1RecoveredFlag = OSIX_FALSE;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;

    /* Update the Port Failed Status here. */
    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        RingDynInfo.u1Port1Status = pRingInfo->u1Port1Status;
        RingDynInfo.u1Port1Status &= ~ERPS_PORT_FAILED;
        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
        {
            u1RecoveredFlag = OSIX_TRUE;
        }

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT1_STATUS);

        pRingInfo->Port1Stats.u4RecoveredCount++;

        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_PORT1_LOCAL_SF);

        /* if the recovered port is set as force switch then 
         * do not unblock it */
        /* but if other port is force switch port, and not fail
         * unblock this port, as already one port is blocked.
         */
        if (((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) ||
             (pRingInfo->u1HighestPriorityRequest ==
              ERPS_RING_FORCE_SWITCH_REQUEST)) &&
            (pRingInfo->u4Port1IfIndex != pRingInfo->u4SwitchCmdIfIndex))
        {
            if ((pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex) &&
                (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                      ERPS_PORT_BLOCKING);

            }
        }
        if (pRingInfo->bDNFStatusinRPL == OSIX_TRUE)
        {
            pRingInfo->bDNFStatusinRPL = OSIX_FALSE;
        }
        /* When both the ring ports in the interconnected node
         * of a ERPS main ring have failed,the RPL ports of both
         * the main ring and sub-ring would have opened because of 
         * virtual channel failure. If the interconnected  link
         * in the main ring  recovers, then the recovered port can be 
         * unblocked on reception of R-APS SF from another ring node, 
         * since there is another failure in the ring which will 
         * ensure that no loop can be formed. 
         *
         * But if the periodic timer in the recovered interconnected
         * node expires quickly, the recovered ports will open 
         * immediately and then a loop will be formed in the sub-ring.
         * (Remember that the sub-ring's RPL port would have opened up
         * due to virtual channel failure, and that the guard timer 
         * will not run in the recovered link, because there is local 
         * SF in the same ring node. )
         *
         * So,to ensure ensure that the R-APS SF from the failed ring port
         * in the same ring node does not reach the recovered port immediately 
         * and unblock the recovered port,restart the periodic timer for the 
         * guard timer value if the remaining value of periodic timer is 
         * less than the guard timer value of the ring node.This will prevent
         * the R-APS SF from being send out immediately. */

        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
            (pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION1))
        {
            if (TMO_SLL_Count (&pRingInfo->SubRingListHead) != 0)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->PeriodicTimer.TimerNode,
                                     &u4RemainingTime);

                if (u4RemainingTime < pRingInfo->u4GuardTimerValue)
                {
                    ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR);
                    if (ErpsTmrStartTimer (pRingInfo, ERPS_PERIODIC_TMR,
                                           pRingInfo->u4GuardTimerValue) ==
                        OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsMsgProcessLinkClearSF returned"
                                          "failure from "
                                          "starting periodic timer\r\n");
                    }
                }
            }
        }
        /*In the event of two failed links (Port1 = !0 and port2 = 0) in
         *interconnected node of sub-ring, when signal failure is cleared
         *for port1 and another port (Virtual Channel) is still in failed state,
         *SEM will not be triggered. When SF clearance event comes for Virtual Channel,
         *Block indication logic will be called to trigger the SEM but no action
         *will be taken in SEM and still node continue to transmit RAPS-SF messages,
         *In such scenario to call the SEM with clearSF event, Flag is set here when SF
         *clearance event comes for Port 1*/
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
            (pRingInfo->u4Port2IfIndex == 0) &&
            (pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2))
        {
            pRingInfo->u1PortRecoveryFlag = OSIX_TRUE;

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgProcessLinkClearSF u1PortRecoveryFlag"
                              " set as TRUE \r\n");
        }

    }
    else
    {
        /* UP MEP is installed in the interconnected nodes of the sub-ring
         * to monitor the virtual channel and take subsequent action in case
         * of UP MEP failure and UP MEP recovery.
         *
         * In Version 1 standard, the approach to minimize segmentation was 
         * not discussed. So in Version 1, the action taken on UP MEP 
         * recovery is to process it as Link clear SF which will send NR's
         * and will block the RPL port of the sub-ring. 
         * But this is a proprietary implementation and
         * should not be continued when the compatible Version number is 2.
         */

        /* Implementation of Minimizing the segmentation in interconnected rings 
         * as per Appendix X of ITU-T G.8032 Y.1344(03/2010)standard.
         * when ERPS Compatibility version is 2, and UP MEP detects Singal Fault
         * Clear in Intermediate Ring node then Block indication Logic
         * Procedure should be called.
         */
        RingDynInfo.u1Port2Status = pRingInfo->u1Port2Status;
        RingDynInfo.u1Port2Status &= ~ERPS_PORT_FAILED;

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_PORT2_STATUS);

        pRingInfo->Port2Stats.u4RecoveredCount++;

        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_PORT2_LOCAL_SF);

        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);
        if (pRingInfo->bDNFStatusinRPL == OSIX_TRUE)
        {
            pRingInfo->bDNFStatusinRPL = OSIX_FALSE;
        }

        if ((pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION2)
            && (MepInfo.u1Direction == ERPS_MP_DIR_UP))
        {
            if (pRingInfo->u4Port2IfIndex == 0)
            {
                ErpsMsgBlockIndicationLogic (pRingInfo, ERPS_CFM_SIGNAL_OK);
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC,
                                  "ErpsMsgProcessLinkClearSF : UP MEP recovery "
                                  "detected on port2 of sub-ring\r\n");
                return OSIX_SUCCESS;
            }

        }

        if (pRingInfo->u1RAPSCompatibleVersion == ERPS_RING_COMPATIBLE_VERSION1)
        {
            if (pRingInfo->u4Port2IfIndex == 0)
            {
                /* Virtual channel recovers, block the sub-ring port and
                 * this blocked port will get opened up, when RPL Port is 
                 * blocked in the sub-ring.
                 */
                if (pRingInfo->u1PortBlockedOnVcRecovery == ERPS_SNMP_TRUE)
                {
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                          ERPS_PORT_BLOCKING);
                }
            }

            u1RecoveredFlag = OSIX_TRUE;
        }

        /* If the recovered port is set as force switch then 
         * do not unblock it */

        if (((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
             (pRingInfo->u1HighestPriorityRequest ==
              ERPS_RING_FORCE_SWITCH_REQUEST)) &&
            (pRingInfo->u4Port2IfIndex != pRingInfo->u4SwitchCmdIfIndex))
        {
            if ((pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex) &&
                (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                      ERPS_PORT_BLOCKING);

            }
            else
            {
                if (pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex)
                {
                    u1IsRplUnBlock = OSIX_TRUE;
                }

            }
        }

        /* When both the ring ports in the interconnected node
         * of a ERPS main ring have failed,the RPL ports of both
         * the main ring and sub-ring would have opened because of 
         * virtual channel failure. If the interconnected  link
         * in the main ring  recovers, then the recovered port can be 
         * unblocked on reception of R-APS SF from another ring node, 
         * since there is another failure in the ring which will 
         * ensure that no loop can be formed. 
         *
         * But if the periodic timer in the recovered interconnected
         * node expires quickly, the recovered ports will open 
         * immediately and then a loop will be formed in the sub-ring.
         * (Remember that the sub-ring's RPL port would have opened up
         * due to virtual channel failure, and that the guard timer 
         * will not run in the recovered link, because there is local 
         * SF in the same ring node. )
         *
         * So,to ensure ensure that the R-APS SF from the failed ring port
         * in the same ring node does not reach the recovered port immediately 
         * and unblock the recovered port,restart the periodic timer for the 
         * guard timer value if the remaining value of periodic timer is 
         * less than the guard timer value of the ring node.This will prevent
         * the R-APS SF from being send out immediately. */

        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) &&
            (pRingInfo->u1RAPSCompatibleVersion ==
             ERPS_RING_COMPATIBLE_VERSION1))
        {
            if (TMO_SLL_Count (&pRingInfo->SubRingListHead) != 0)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->PeriodicTimer.TimerNode,
                                     &u4RemainingTime);

                if (u4RemainingTime < pRingInfo->u4GuardTimerValue)
                {
                    ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR);

                    if (ErpsTmrStartTimer (pRingInfo, ERPS_PERIODIC_TMR,
                                           pRingInfo->u4GuardTimerValue) ==
                        OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsMsgProcessLinkClearSF returned"
                                          "failure from "
                                          "starting periodic timer\r\n");
                    }
                }
            }
        }
    }

    if (u1IsRplUnBlock == OSIX_TRUE)
    {
        RingDynInfo.u4RingNodeStatus &= (~ERPS_NODE_STATUS_RB);
    }

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    /* According to the standard G.8021/Y.1341 - Amendment 1(07/2011) 
     * in section .6.1.4.3.4 When T-FOP feature is enabled and 
     * one of the port is recovered from signal failure
     * Then start the timer */
    if (pRingInfo->bDFOP_TO == OSIX_ENABLED)
    {
        /* The RPL node is in IDLE condition and receiving clear SF event 
         * for one of its port and there is a chance of not receiving any RAPS 
         * packets from adjacent node. 
         * so FOP-TO timer should not be started for RPL node. */

        if (!((pRingInfo->u4RplIfIndex != 0) &&
              (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)))
        {

            ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                               ((pRingInfo->f4KValue) *
                                (pRingInfo->u4PeriodicTimerValue)));
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgProcessLinkClearSF :"
                              "FOP-TO timer has been started" "\r\n");
        }
    }
    /* If the interconnected link between the main ring and sub-ring has 
     * recovered, then the recovered status will be updated in the 
     * u1Port1Status and u1Port2Status in RingInfo structure. So 
     * a flag u1RecoveredFlag is used to indicate the same. 
     * If a port has recovered and if it is an interconnected node (
     * SubRingList will be filled) then this event in the main ring can 
     * cause virtual channel
     * of the associated sub-ring to become up. Hence inform 
     * associated sub-rings to take appropriate action.*/

    if (u1RecoveredFlag == OSIX_TRUE)
    {
        pSllNode = TMO_SLL_First (&pRingInfo->SubRingListHead);

        while (pSllNode != NULL)
        {
            pSubRingEntry = (tSubRingEntry *) pSllNode;
            if (pSubRingEntry->u4SubRingId != 0)
            {
                ErpsSmMainRingStatusIndication (pRingInfo->u4ContextId,
                                                pSubRingEntry->u4SubRingId);
            }

            pSllNode = TMO_SLL_Next (&pRingInfo->SubRingListHead, pSllNode);
        }
    }

    /* Only second port clear will be intimated to Priority Logic for
     * top priority re-evaluation, in the event of two failed links.
     */

    if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
        (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
    {
        if (pRingInfo->u1HighestPriorityRequest ==
            ERPS_RING_FORCE_SWITCH_REQUEST)
        {
            ErpsTrapSendTrapNotifications (pRingInfo,
                                           ERPS_TRAP_FORCE_SWITCH_PRESENT);
        }
        else if (pRingInfo->u1HighestPriorityRequest ==
                 ERPS_RING_LOCAL_SF_REQUEST)
        {
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_AUTO_SWITCH);
        }

        /* When received LocalClearSF, sem will not be triggered if there is
         * LocalSF in another ring port. So this port will remain in Blocked state.
         * Loop will not be formed if this Non failed port becomes UNBLOCKED */
        if (pRingInfo->u1HighestPriorityRequest !=
            ERPS_RING_FORCE_SWITCH_REQUEST)
        {
            if ((!(pRingInfo->u1Port1Status & ERPS_PORT_FAILED)) &&
                (pRingInfo->u4Port2IfIndex != 0))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                      ERPS_PORT_UNBLOCKING);
            }
            else
            {
                if (!(pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
                {
                    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                          ERPS_PORT_UNBLOCKING);
                }
            }
            if (((pRingInfo->u4RplIfIndex != 0) ||
                 (pRingInfo->u4RplNeighbourIfIndex != 0)) &&
                (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE))
            {

                /* In the event of two failed links, When signal failure is cleared   
                 * for RPL/Neighbor port and another port is still in failed and   
                 * blocked state, SEM wont be triggered So RAPS packet will be   
                 * transmitted with DNF not set and flush will not happen. In such   
                 * scenario RAPS message is stopped and Transmitted again with DNF   
                 * set.*/
                if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) &&
                    (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING))
                {
                    if ((pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex)
                        || (pRingInfo->u4Port1IfIndex ==
                            pRingInfo->u4RplNeighbourIfIndex))
                    {
                        u1Status |= ERPS_DNF_BIT_SET;
                    }
                }
                else if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
                         (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
                {
                    if ((pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex)
                        || (pRingInfo->u4Port2IfIndex ==
                            pRingInfo->u4RplNeighbourIfIndex))
                    {
                        u1Status |= ERPS_DNF_BIT_SET;
                    }
                }
                if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMsgProcessLinkClearSF failed to stop R-APS "
                                      "messages.\r\n");
                }

                if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                             ERPS_MESSAGE_TYPE_SF) ==
                    OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMsgProcessLinkClearSF failed to start R-APS "
                                      "messages.\r\n");
                }

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMsgProcessLinkClearSF : Transmits R-APS "
                                  "SF messages.\r\n");
            }

        }
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsMsgProcessLinkClearSF: "
                          "One one out of two failed link is recovered - "
                          "State machine not triggered\r\n");
        return OSIX_SUCCESS;
    }
    if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                     ERPS_RING_LOCAL_CLEAR_SF_REQUEST,
                                     &gErpsRcvdMsg) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMsgProcessLinkClearSF failed returned from "
                          "Priority Logic.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgValidateMepInfo                               */
/*                                                                         */
/* DESCRIPTION      : Function is used to validate, whether the indication */
/*                    is received for the configured MEP or not.           */
/*                                                                         */
/* INPUT            : pRingInfo  - Pointer to the ring entry.              */
/*                    MepInfo    - MEP info for which the fault is         */
/*                                 reported.                               */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
PRIVATE INT4
ErpsMsgValidateMepInfo (tErpsRingInfo * pRingInfo, tErpsCfmMepInfo MepInfo)
{
    if ((pRingInfo->CfmEntry.u4Port1MEGId == MepInfo.u4MdId) &&
        (pRingInfo->CfmEntry.u4Port1MEId == MepInfo.u4MaId) &&
        (pRingInfo->CfmEntry.u4Port1MEPId == MepInfo.u4MepId))
    {
        return OSIX_SUCCESS;
    }
    else if ((pRingInfo->CfmEntry.u4Port2MEGId == MepInfo.u4MdId) &&
             (pRingInfo->CfmEntry.u4Port2MEId == MepInfo.u4MaId) &&
             (pRingInfo->CfmEntry.u4Port2MEPId == MepInfo.u4MepId))
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgHandleCfmSignalFromEcfm                       */
/*                                                                         */
/* DESCRIPTION      : Function is used to handle the signal status for     */
/*                    a MEP received from ECFM                             */
/*                                                                         */
/* INPUT            : pMsg - Pointer to the message to be posted.          */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : NONE                                                 */
/*                                                                         */
/***************************************************************************/
PRIVATE VOID
ErpsMsgHandleCfmSignalFromEcfm (tErpsQMsg * pMsg)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsCfmMepInfo     MepInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4LinkStatus = 0;
    tVlanId             VlanId = 0;

    u4ContextId = pMsg->u4ContextId;

    VlanId = pMsg->MepInfo.VlanId;
    u4LinkStatus = pMsg->unMsgParam.u4Status;
    u4IfIndex = pMsg->MepInfo.u4IfIndex;

    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));
    MEMCPY (&MepInfo, &(pMsg->MepInfo), sizeof (tErpsCfmMepInfo));

    /* Ring MAC id is not known for the indication received from MEP,
     * and it is possible to have multiple ring instances associated with
     * the same MEP. So scan the port vlan table to get all the
     * active rings using this MEP and apply the signal to all of them */

    pRingInfo = ErpsPortVlanTblGetFirstNodeOfPortAndVlan (u4ContextId,
                                                          u4IfIndex, VlanId);

    if (pRingInfo != NULL)
    {
        if (ErpsMsgValidateMepInfo (pRingInfo, MepInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgHandleCfmSignal: Received indication for "
                              "unknown MEP.\r\n");
            return;
        }
        switch (u4LinkStatus)
        {
            case ERPS_CFM_SIGNAL_FAIL:
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMsgHandleCfmSignal: Received Failure indication "
                                  "from ECFM.\r\n");

                ErpsMsgProcessLinkSF (pRingInfo, MepInfo);
                break;

            case ERPS_CFM_SIGNAL_OK:
                /* As per Section 10.1.3 (R-APS message transmission)
                 * in ITU-T G.8032 Y.1344(03/2010).
                 * "Unless otherwise stated, all R-APS messages are
                 * transmitted on both ring ports."
                 *
                 * But As Per Appendix VIII (Flush Optmization)
                 * of ITU-T G.8032(03/2010)
                 * "VIII.3 Rule 2: When detecting a failure from an RPL
                 * next-neighbour port, in idle state, transmit R-APS
                 * (SF) message only on the RPL next-neighbour port
                 * and do not transmit R-APS messages on the other ring port."
                 *
                 * But After on Receiving Local Clear SF on failed RPL Next
                 * Neighbour port, Default R-APS message Tx on both ports
                 * as per section 10.1.3 should be resumed.
                 * So if Recovered port is RPL Next Neighbour port then
                 * restoring R-APS Tx on both the Ring ports.
                 */
                if (pRingInfo->u4RplNextNeighbourIfIndex == u4IfIndex)
                {
                    pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
                }

                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMsgHandleCfmSignal: Received Failure Clear indication "
                                  "from ECFM.\r\n");
                ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo);
                break;
            default:
                break;
        }

    }
    return;

}

/***************************************************************************/
/* FUNCTION NAME    : ErpsMsgHandleCfmSignalFromMpls                       */
/*                                                                         */
/* DESCRIPTION      : Function is used to handle the signal status for     */
/*                    a MEP received from MPLS OAM                         */
/*                                                                         */
/* INPUT            : pMsg - Pointer to the message to be posted.          */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : NONE                                                 */
/*                                                                         */
/***************************************************************************/
PRIVATE VOID
ErpsMsgHandleCfmSignalFromMpls (tErpsQMsg * pMsg)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4LinkStatus = 0;
    UINT4               u4RingId = 0;
    UINT4               u4RingPort = 0;
    tErpsCfmMepInfo     MepInfo;

    u4ContextId = pMsg->u4ContextId;
    u4LinkStatus = pMsg->unMsgParam.u4Status;

    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));
    MEMCPY (&MepInfo, &(pMsg->MepInfo), sizeof (tErpsCfmMepInfo));

    /* Ring ID is not known for the indication received from MEP,
     * and it is possible to have multiple ring instances associated with
     * the same MEP. So scan the MEP RingID table to get all the
     * active rings using this MEP and apply the signal to all of them */

    /*Loop through the MEPRingId RBTree to get all the nodes for
       the particular MEP */

    while ((pRingInfo = ErpsGetNodeOfMepAndRingId (u4ContextId,
                                                   pMsg->MepInfo.u4MdId,
                                                   pMsg->MepInfo.u4MaId,
                                                   pMsg->MepInfo.u4MepId,
                                                   u4RingId)) != NULL)
    {

        if (pRingInfo != NULL)
        {
            if (pRingInfo->u4RingId == u4RingId)
            {
                continue;
            }

            switch (u4LinkStatus)
            {
                case ERPS_CFM_SIGNAL_FAIL:
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMsgHandleCfmSignal: Received Failure indication "
                                      "from ECFM.\r\n");
                    if ((pMsg->MepInfo.u4MdId ==
                         pRingInfo->CfmEntry.u4Port1MEGId)
                        && (pMsg->MepInfo.u4MaId ==
                            pRingInfo->CfmEntry.u4Port1MEId)
                        && (pMsg->MepInfo.u4MepId ==
                            pRingInfo->CfmEntry.u4Port1MEPId))
                    {
                        pRingInfo->pErpsLspPwInfo->u1Port1TunnelStatus =
                            ERPS_TUNNEL_STATE_FAILED;
                    }
                    else
                    {
                        pRingInfo->pErpsLspPwInfo->u1Port2TunnelStatus =
                            ERPS_TUNNEL_STATE_FAILED;
                    }

                    ErpsMsgProcessLinkSF (pRingInfo, MepInfo);
                    break;

                case ERPS_CFM_SIGNAL_OK:
                    if ((pMsg->MepInfo.u4MdId ==
                         pRingInfo->CfmEntry.u4Port1MEGId)
                        && (pMsg->MepInfo.u4MaId ==
                            pRingInfo->CfmEntry.u4Port1MEId)
                        && (pMsg->MepInfo.u4MepId ==
                            pRingInfo->CfmEntry.u4Port1MEPId))
                    {
                        u4RingPort = pRingInfo->u4Port1IfIndex;
                        pRingInfo->pErpsLspPwInfo->u1Port1TunnelStatus =
                            ERPS_TUNNEL_STATE_OK;
                    }
                    else
                    {
                        u4RingPort = pRingInfo->u4Port2IfIndex;
                        pRingInfo->pErpsLspPwInfo->u1Port2TunnelStatus =
                            ERPS_TUNNEL_STATE_OK;
                    }

                    if (pRingInfo->u4RplNextNeighbourIfIndex == u4RingPort)
                    {
                        pRingInfo->u1RingTxAction =
                            ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
                    }

                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMsgHandleCfmSignal: Received Failure Clear indication "
                                      "from ECFM.\r\n");
                    ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo);
                    break;
                default:
                    break;
            }

        }

        u4RingId = pRingInfo->u4RingId;
    }

    return;

}

#endif /*_ERPSMSG_C */
