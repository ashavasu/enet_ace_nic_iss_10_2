/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: erpsmain.c,v 1.17 2014/02/24 11:30:59 siva Exp $
*
* Description: This file contains the init and de-init routines for ERPS 
*              Task,MemPool and Timers.
* .
*****************************************************************************/

#ifndef __ERPSMAIN_C__
#define __ERPSMAIN_C__

#include "erpsinc.h"
/****************************************************************************
 *                           erpsmain.c prototypes                          *
 ****************************************************************************/

PRIVATE INT4        ErpsMainTaskInit (VOID);
PRIVATE VOID        ErpsMainTaskDeInit (VOID);
PRIVATE INT4        ErpsMainTmrInit (VOID);
PRIVATE INT4        ErpsMainCreateRingTable (VOID);
PRIVATE INT4        ErpsMainCreatePortVlanTable (VOID);
PRIVATE VOID        ErpsMainTmrDeInit (VOID);
PRIVATE VOID        ErpsMainAssignMemPools (VOID);
PRIVATE INT4        ErpsMainCreateMepRingIdTable (VOID);

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainTask                                     */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Initialized the ERPS task (Sem, queue,        */
/*                             mempool creation).                            */
/*                          o  Register Erps Mib with SNMP module            */
/*                          o  Wait for external event and call the          */
/*                             corresponding even handler routines           */
/*                             on receiving the events.                      */
/*                                                                           */
/*    Input(s)            : pi1Arg -  Pointer to input arguments             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;

#ifdef SYSLOG_WANTED
    INT4                i4SysLogId = 0;
#endif

    UNUSED_PARAM (pi1Arg);
    if (ErpsMainTaskInit () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTask failed to initialize\r\n");

        /* Deinitialize ERPS task */
        ErpsMainTaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to log messages. */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) ERPS_TASK_NAME,
                                   SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        ERPS_GLOBAL_TRC ("Erps registration with Syslog server failed\r\n");

        /* Deinitialize ERPS task */
        ErpsMainTaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }
    ERPS_SYSLOG_ID = (UINT4) i4SysLogId;

    /* De-registration is not called now as there are no further registrations 
     * done below which could fail. Should there be registrations below syslog 
     * reg added later, the de-reg should be called appropriately.
     */
#endif

#ifdef SNMP_2_WANTED
    RegisterFSERPS ();
#endif

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gErpsGlobalInfo.MainTaskId, ERPS_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            ERPS_LOCK ();

            if (u4Events & ERPS_QMSG_EVENT)
            {
                ErpsMsgQueueHandler ();

            }

            if (u4Events & ERPS_TMR_EXPIRY_EVENT)
            {
                ErpsTmrExpHandler ();
            }

            ERPS_UNLOCK ();
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsMainRApsTxTask
 *
 * DESCRIPTION      : Entry point function of ERPS RAPS Transmission task.
 *
 * INPUT            : pi1Arg - Pointer to the arguments
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
ErpsMainRApsTxTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Arg);

    if (OsixGetTaskId (SELF, ERPS_RAPS_TX_TASK_NAME,
                       &(gErpsGlobalInfo.RapsTxTaskId)) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainRApsTxTask: Obtaining Task Id FAILED !!\r\n");
        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Initialize the RAPS Tx SLL's */
    ErpsTxInitRApsTxInfoSLL ();

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gErpsGlobalInfo.RapsTxTaskId, ERPS_RAPS_TX_EVENT,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            ErpsTxEventHandler ();
        }
    }                            /* end of while */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainTaskInit                                 */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Initializes global variables                  */
/*                          o  Create queues, semaphore, mempools for ERPS   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         allocated Erps Module resource.   */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         allocate Erps Module resources.   */
/*****************************************************************************/
PRIVATE INT4
ErpsMainTaskInit (VOID)
{
    MEMSET (&gErpsGlobalInfo, 0, sizeof (tErpsGlobalInfo));
    MEMSET (&gau1ErpsSysCtrlStatus, ERPS_SHUTDOWN,
            sizeof (UINT1) * SYS_DEF_MAX_NUM_CONTEXTS);

    if (OsixGetTaskId (SELF, ERPS_TASK_NAME, &(gErpsGlobalInfo.MainTaskId))
        == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit: Get Task Id FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixSemCrt (ERPS_PROTO_SEM, &(gErpsGlobalInfo.MainTaskSemId))
        == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit Sem Creation failed\r\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (gErpsGlobalInfo.MainTaskSemId);

    /* Semaphore for protecting APS Tx Info */
    if (OsixSemCrt (ERPS_RAPS_TX_DB_SEM,
                    &(gErpsGlobalInfo.RapsTxInfoSemId)) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit Sem Creation failed\r\n");
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gErpsGlobalInfo.RapsTxInfoSemId);

    if (OsixQueCrt ((UINT1 *) ERPS_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsERPSSizingParams[MAX_ERPS_Q_MESG_SIZING_ID].
                    u4PreAllocatedUnits,
                    &(gErpsGlobalInfo.MainTaskQId)) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit Q Creation failed\r\n");
        return OSIX_FAILURE;
    }

    ErpsRedInitGlobalInfo ();
    if (ErpsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsSizingMemCreateMemPools MemPool init failed\r\n");
        return OSIX_FAILURE;
    }
    ErpsMainAssignMemPools ();
    if (ErpsMainTmrInit () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit Timer init failed\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsMainCreateRingTable () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit RingTable creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsMainCreatePortVlanTable () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit PortVlanTable creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsMainCreateMepRingIdTable () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit MepRingTable creation failed\r\n");
        return OSIX_FAILURE;
    }

    ErpsRedDynDataDescInit ();

    ErpsRedDescrTblInit ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainTaskDeInit                               */
/*                                                                           */
/*    Description         : This function will perform deinitialization of   */
/*                          ERPS Task                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ErpsMainTaskDeInit (VOID)
{
    /* Delete Port Vlan RB Tree */
    if (gErpsGlobalInfo.RingPortVlanTable != 0)
    {
        RBTreeDestroy (gErpsGlobalInfo.RingPortVlanTable, NULL, 0);
    }

    /* Delete Ring table RB Tree */
    if (gErpsGlobalInfo.RingTable != 0)
    {
        RBTreeDestroy (gErpsGlobalInfo.RingTable, NULL, 0);
    }

    if (gErpsGlobalInfo.RingMepTable != 0)
    {
        RBTreeDestroy (gErpsGlobalInfo.RingMepTable, NULL, 0);
    }

    /* Delete the Timers */
    ErpsMainTmrDeInit ();

    /* Delete the MemPools */
    ErpsRedDeInitGlobalInfo ();
    ErpsSizingMemDeleteMemPools ();

    if (gErpsGlobalInfo.MainTaskQId != 0)
    {
        OsixQueDel (gErpsGlobalInfo.MainTaskQId);
    }
    if (gErpsGlobalInfo.RapsTxInfoSemId != 0)
    {
        OsixSemDel (gErpsGlobalInfo.RapsTxInfoSemId);
    }

    if (gErpsGlobalInfo.MainTaskSemId != 0)
    {
        OsixSemDel (gErpsGlobalInfo.MainTaskSemId);
    }

    MEMSET (&gErpsGlobalInfo, 0, sizeof (tErpsGlobalInfo));
    MEMSET (&gau1ErpsSysCtrlStatus, ERPS_SHUTDOWN,
            sizeof (UINT1) * SYS_DEF_MAX_NUM_CONTEXTS);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainAssignMemPools                           */
/*                                                                           */
/*    Description         : This function will Assign the mempool Id's       */
/*                          created for ERPS Module:                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ErpsMainAssignMemPools (VOID)
{
    gErpsGlobalInfo.TaskQMsgPoolId = ERPSMemPoolIds[MAX_ERPS_Q_MESG_SIZING_ID];
    gErpsGlobalInfo.ContextPoolId =
        ERPSMemPoolIds[MAX_ERPS_CONTEXT_INFO_SIZING_ID];
    gErpsGlobalInfo.RingInfoPoolId =
        ERPSMemPoolIds[MAX_ERPS_RING_INFO_SIZING_ID];
    gErpsGlobalInfo.RapsTxInfoTablePoolId =
        ERPSMemPoolIds[MAX_ERPS_RAPS_TX_INFO_SIZING_ID];
    gErpsGlobalInfo.RapsTxPduPoolId =
        ERPSMemPoolIds[MAX_ERPS_SIMULTANEOUS_CFM_SIGNAL_SIZING_ID];
    gErpsGlobalInfo.ErpsMbsmPoolId =
        ERPSMemPoolIds[MAX_ERPS_MBSM_MESG_SIZING_ID];
    gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTablePoolId =
        ERPSMemPoolIds[MAX_ERPS_RED_NP_SYNC_ENTRIES_SIZING_ID];
    gErpsGlobalInfo.SubRingPoolId =
        ERPSMemPoolIds[MAX_ERPS_SUB_RING_ENTRIES_SIZING_ID];
    gErpsGlobalInfo.VlanGroupListPoolId =
        ERPSMemPoolIds[MAX_ERPS_RING_VLAN_INFO_SIZING_ID];
    gErpsGlobalInfo.MplsApiInInfoPoolId =
        ERPSMemPoolIds[MAX_ERPS_MPLS_API_IN_INFO_SIZING_ID];
    gErpsGlobalInfo.MplsApiOutInfoPoolId =
        ERPSMemPoolIds[MAX_ERPS_MPLS_API_OUT_INFO_SIZING_ID];
    gErpsGlobalInfo.ErpsCliMemblkBufPoolId =
        ERPSMemPoolIds[MAX_ERPS_CLI_BUF_ENTRIES_SIZING_ID];
    gErpsGlobalInfo.LspPwInfoListId =
        ERPSMemPoolIds[MAX_ERPS_LSP_PW_INFO_ENTRIES_SIZING_ID];
    ERPS_SUBPORT_LIST_POOLID () =
        ERPSMemPoolIds[MAX_ERPS_SUB_PORTLIST_COUNT_SIZING_ID];

    MAX_ERPS_SUB_PORT_ENTRIES_POOLID () =
        ERPSMemPoolIds[MAX_ERPS_SUB_PORT_ENTRIES_SIZING_ID];

    ERPS_PORT_ARRAY_POOLID () =
        ERPSMemPoolIds[MAX_ERPS_UTIL_PORT_ARRAY_COUNT_SIZING_ID];

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : TmrCreateTimerList                               */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Creates timer list for ERPS timers.           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         created timer list.               */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         create timer list.                */
/*****************************************************************************/
PRIVATE INT4
ErpsMainTmrInit (VOID)
{
    if (TmrCreateTimerList (ERPS_TASK_NAME, ERPS_TMR_EXPIRY_EVENT,
                            NULL, &(gErpsGlobalInfo.RingTimerListId))
        == TMR_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTmrInit Timer list creation failed\r\n");

        return OSIX_FAILURE;
    }

    ErpsTmrInitTimerDesc ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainTmrDeInit                                */
/*                                                                           */
/*    Description         : If timer list was created, this function will    */
/*                          delete the timer List.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ErpsMainTmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gErpsGlobalInfo.RingTimerListId) == TMR_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTmrInit Timer list deletion failed\r\n");
        return;
    }

    MEMSET (gErpsGlobalInfo.aTmrDesc, 0,
            sizeof (tTmrDesc) * ERPS_MAX_TMR_TYPES);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainCreateRingTable                          */
/*                                                                           */
/*    Description         : This function will create RBTree for RingTable.  */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         created RingTable.                */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         create RingTable.                 */
/*****************************************************************************/
PRIVATE INT4
ErpsMainCreateRingTable (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset = FSAP_OFFSETOF (tErpsRingInfo, RingRBNode);

    if ((gErpsGlobalInfo.RingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, ErpsRingIdCmpFn)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsMainCreateRingTable RingTable creation "
                         "failed\r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainCreatePortVlanTable                      */
/*                                                                           */
/*    Description         : This function will create RBTree for             */
/*                          PortVlanTable.                                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         created PortVlanTable RBTree.     */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         create PortVlanTable RBTree.      */
/*****************************************************************************/
PRIVATE INT4
ErpsMainCreatePortVlanTable (VOID)
{
    if ((gErpsGlobalInfo.RingPortVlanTable =
         RBTreeCreateEmbedded (0, ErpsRingPortVlanCmpFn)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsMainCreatePortVlanTable RingPortVlanTable "
                         "creation failed\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************/
/* FUNCTION NAME    : ErpsMainModuleShutDown                                  */
/*                                                                            */
/* DESCRIPTION      : This function will be called by RM module to shutdown   */
/*                    the ERPS module, in the event of cable pullout scenario.*/
/*                                                                            */
/* INPUT            : None                                                    */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                            */
/******************************************************************************/

PUBLIC INT4
ErpsMainModuleShutDown (VOID)
{
    tErpsQMsg          *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* Delete all ring entries of all the context. */
    for (; u4ContextId < ERPS_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (ErpsCxtGetContextEntry (u4ContextId) != NULL)
        {
            ErpsRingDeleteAllRingsInContext (u4ContextId);

            gErpsGlobalInfo.apRingContextInfo[u4ContextId] = NULL;

            /* Notify to MSR also */
            ErpsPortNotifyProtocolShutStatus (u4ContextId);
        }
    }

    while (OsixQueRecv (gErpsGlobalInfo.MainTaskQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

    {
        if (MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            ERPS_GLOBAL_TRC
                ("ErpsMainModuleShutDown:Free MemBlock QMsg FAILED\n");
        }

    }

    ErpsRedHwAudCrtNpSyncBufferTable ();

    /* Delete Port Vlan RB Tree */
    if (gErpsGlobalInfo.RingPortVlanTable != 0)
    {
        RBTreeDestroy (gErpsGlobalInfo.RingPortVlanTable, NULL, 0);
        gErpsGlobalInfo.RingPortVlanTable = 0;
    }

    /* Delete Ring table RB Tree */
    if (gErpsGlobalInfo.RingTable != 0)
    {
        RBTreeDestroy (gErpsGlobalInfo.RingTable, NULL, 0);
        gErpsGlobalInfo.RingTable = 0;
    }

    /* Delete the Timers */
    ErpsMainTmrDeInit ();

    /* Delete the MemPools */

    ErpsRedDeInitGlobalInfo ();
    ErpsSizingMemDeleteMemPools ();

    MEMSET (&gau1ErpsSysCtrlStatus, ERPS_SHUTDOWN,
            sizeof (UINT1) * SYS_DEF_MAX_NUM_CONTEXTS);

    return OSIX_SUCCESS;
}

/******************************************************************************/
/* FUNCTION NAME    : ErpsMainModuleStart                                     */
/*                                                                            */
/* DESCRIPTION      : This function will be called by RM module to start the  */
/*                    ERPS module, in the event of cable pullout scenario.    */
/*                                                                            */
/* INPUT            : None                                                    */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                            */
/******************************************************************************/

PUBLIC INT4
ErpsMainModuleStart (VOID)
{

    ErpsRedInitGlobalInfo ();

    if (ErpsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsSizingMemCreateMemPools MemPool init failed\r\n");
        return OSIX_FAILURE;
    }
    ErpsMainAssignMemPools ();
    if (ErpsMainTmrInit () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit Timer init failed\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsMainCreateRingTable () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit RingTable creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (ErpsMainCreatePortVlanTable () == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMainTaskInit PortVlanTable creation failed\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMainCreateMeRingIdTable                      */
/*                                                                           */
/*    Description         : This function will create RBTree for             */
/*                          MepRingIdTable.                                  */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS                                     */
/*                                                                       */
/*                          OSIX_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
ErpsMainCreateMepRingIdTable (VOID)
{
    if ((gErpsGlobalInfo.RingMepTable =
         RBTreeCreateEmbedded (0, ErpsMepIdCmpFn)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsMainCreateMepRingIdTable MepRingIdTable creation "
                         "failed\r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif /*__ERPSMAIN_C__*/
