/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: erpsmbsm.c,v 1.9 2014/11/14 12:16:34 siva Exp $                                                                   *
 *                                                                         *
 * Description: This file contains the ERPS Mbsm functions.                *
 ***************************************************************************/

#include "erpsinc.h"

/****************************************************************************
 *                           erpsmbsm.c prototypes                          *
 ****************************************************************************/
PRIVATE UINT1
 
 
 
ErpsMbsmIsRingPortPresentInCard (UINT4 u4RingIfIndex,
                                 tMbsmPortInfo * pPortInfo);
PRIVATE INT4
 
 
 
 ErpsMbsmConfRingEntryInHw (tErpsRingInfo * pRingInfo, UINT1 u1RingStatus,
                            tErpsMbsmInfo * pErpsMbsmInfo);

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMbsmPostMessage                              */
/*                                                                           */
/*    Description         : This function will allocates mbsm message memory */
/*                          and post the mbsm event to erps task Q.          */
/*                                                                           */
/*    Input(s)            : pProtoMsg  - Pointer to slot and port info.      */
/*                                                                           */
/*                          i4Event    - card insert or removed event.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS - When this function successfully   */
/*                                         posted mbsm event message to task.*/
/*                          MBSM_FAILURE - When this function failed to      */
/*                                         allocate memory or failed to post */
/*                                         erps task.                        */
/*****************************************************************************/
PUBLIC INT4
ErpsMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tErpsQMsg          *pMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsMbsmPostMessage: pProtoMsg is NULL!!!\r\n");
        return MBSM_FAILURE;
    }

    MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));

    if ((pMsg = (tErpsQMsg *)
         MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsMbsmPostMessage: message q memory allocation "
                         "failed!!!\r\n");
        gErpsGlobalInfo.u4MemFailCount++;
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tErpsQMsg));

    pMsg->u4MsgType = (UINT4) i4Event;

    if ((pMsg->unMsgParam.pMbsmProtoMsg = (tMbsmProtoMsg *)
         MemAllocMemBlk (gErpsGlobalInfo.ErpsMbsmPoolId)) == NULL)
    {
        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
        gErpsGlobalInfo.u4MemFailCount++;

        return MBSM_FAILURE;
    }

    MEMCPY (pMsg->unMsgParam.pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));

    if (ErpsMsgEnque (pMsg) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gErpsGlobalInfo.ErpsMbsmPoolId,
                            (UINT1 *) pMsg->unMsgParam.pMbsmProtoMsg);
        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);

        ERPS_GLOBAL_TRC ("ErpsMbsmPostMessage: postng to ERPS Q Failed!!!\r\n");

        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMbsmUpdateOnCardInsertion                    */
/*                                                                           */
/*    Description         : This function will be call to handle card insert */
/*                          event in ERPS module.                            */
/*                                                                           */
/*    Input(s)            : pPortInfo  - Pointer to list ports of inserted   */
/*                                       card.                               */
/*                          pSlotInfo  - Information about inserted slot.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS - When this function successfully   */
/*                                         updated erps ring info.           */
/*                          MBSM_FAILURE - When this function failed to      */
/*                                         update the ring infor to inserted */
/*                                         card.                             */
/*****************************************************************************/
PUBLIC INT4
ErpsMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pRingNextInfo = NULL;
    tErpsMbsmInfo       ErpsMbsmInfo;
    UINT1               u1Result = MBSM_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               u1IsSetInPortList = OSIX_FALSE;

    /* When a card is inserted scan erps database and do the 
     * following action:
     *
     * - When both ring ports or port1 (in case of sub-ring) are
     *   present in the newly inserted card, then create ring 
     *   entry in hardware for the newly inserted card.
     *
     * - When only one port of the ring is present in the newly
     *   inserted card, do not create ring entry in the hardware
     *   for the port which is not present in the hardware. It 
     *   is possible that other port may present in another link card, 
     *   which would not have added yet or it may be present in one 
     *   of the existing card.
     *
     *   If only one port of the is present in the operating cards, the 
     *   ring will remained failed as it will receive failed status 
     *   from the link monitoring protocol.
     * - When both the port or port1 (in case of sub-ring) present in 
     *   in one of the unaffected card. do nothing  */

    ErpsMbsmInfo.pSlotInfo = pSlotInfo;

    ErpsMbsmInfo.u1CardAction = MBSM_MSG_CARD_INSERT;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while ((pRingInfo != NULL) && (pPortInfo->u4PortCount > 0))
    {
        pRingNextInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);

        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                 (pPortInfo->u4StartIfIndex +
                                  pPortInfo->u4PortCount - 1),
                                 sizeof (tPortListExt), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 (pPortInfo->u4StartIfIndex +
                                  pPortInfo->u4PortCount - 1),
                                 sizeof (tPortListExt),
                                 u1IsSetInPortListStatus);
        if ((OSIX_TRUE == u1IsSetInPortList)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) != OSIX_TRUE) ||
                (pRingInfo->u1RowStatus != ACTIVE))
            {
                pRingInfo = pRingNextInfo;
                continue;
            }
            u1Result =
                ErpsMbsmIsRingPortPresentInCard (pRingInfo->u4Port1IfIndex,
                                                 pPortInfo);

            if ((u1Result == MBSM_FALSE) && (pRingInfo->u4Port2IfIndex != 0))
            {
                u1Result = ErpsMbsmIsRingPortPresentInCard
                    (pRingInfo->u4Port2IfIndex, pPortInfo);
            }

            if (u1Result == MBSM_TRUE)
            {
                if (ErpsMbsmConfRingEntryInHw (pRingInfo, ERPS_HW_RING_CREATE,
                                               &ErpsMbsmInfo) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsMbsmUpdateOnCardInsertion hardware "
                                      "configuration failed for inserted card\r\n");
                    return MBSM_FAILURE;
                }
            }
        }
        else
        {
            pPortInfo->u4PortCount--;
        }

        pRingInfo = pRingNextInfo;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMbsmUpdateOnCardRemoval                      */
/*                                                                           */
/*    Description         : This function will be call to handle card remove */
/*                          event in ERPS module.                            */
/*                                                                           */
/*    Input(s)            : pPortInfo  - Pointer to list ports of removed    */
/*                                       card.                               */
/*                          pSlotInfo  - Information about removed  slot.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS - When this function successfully   */
/*                                         updated erps ring info.           */
/*                          MBSM_FAILURE - When this function failed to      */
/*                                         update the ring info for removed  */
/*                                         card.                             */
/*****************************************************************************/
PUBLIC INT4
ErpsMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo      *pRingNextInfo = NULL;
    tErpsMbsmInfo       ErpsMbsmInfo;
    UINT1               u1Result;

    /* When card is removed, do the following action:
     * - When any one of the ring port was present in the removed 
     * card.
     *    - call ERPS npapi with ring update status with all information
     *    this can be used to update hardware present in the hardware.
     */

    ErpsMbsmInfo.pSlotInfo = pSlotInfo;

    ErpsMbsmInfo.u1CardAction = MBSM_MSG_CARD_REMOVE;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while ((pRingInfo != NULL) && (pPortInfo->u4PortCount > 0))
    {
        pRingNextInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);

        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) != OSIX_TRUE) ||
            (pRingInfo->u1RowStatus != ACTIVE))
        {
            pRingInfo = pRingNextInfo;
            continue;
        }

        u1Result = ErpsMbsmIsRingPortPresentInCard
            (pRingInfo->u4Port1IfIndex, pPortInfo);

        if ((u1Result == MBSM_FALSE) && (pRingInfo->u4Port2IfIndex != 0))
        {
            u1Result = ErpsMbsmIsRingPortPresentInCard
                (pRingInfo->u4Port2IfIndex, pPortInfo);
        }

        if (u1Result == MBSM_TRUE)
        {
            if (ErpsMbsmConfRingEntryInHw (pRingInfo, ERPS_HW_RING_MODIFY,
                                           &ErpsMbsmInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsMbsmUpdateOnCardRemoval hardware "
                                  "configuration failed for removed card.\r\n");
                return MBSM_FAILURE;
            }
        }

        pRingInfo = pRingNextInfo;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMbsmIsRingPortPresentInCard                  */
/*                                                                           */
/*    Description         : This function is a utility function to check if  */
/*                          inserted or removed card port list contains, ring*/
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : pPortInfo  - Pointer to list ports of removed    */
/*                                       card.                               */
/*                          u4RingIfIndex - Ring interface index that needs  */
/*                                          be checked in the card port list.*/
/*                                                                           */
/*    Output(s)           : pPortInfo - Port count will be reduced  for index*/
/*                                      found in the port list.              */
/*                                                                           */
/*    Returns             : MBSM_TRUE  - When given port or one of port      */
/*                                       channel port is present in the list.*/
/*                          MBSM_FALSE - When given port or any of the port  */
/*                                       channel port is not present in the  */
/*                                       port list.                          */
/*****************************************************************************/
PRIVATE UINT1
ErpsMbsmIsRingPortPresentInCard (UINT4 u4RingIfIndex, tMbsmPortInfo * pPortInfo)
{
    tCfaIfInfo          CfaIfInfo;
    UINT2               au2LagPorts[LA_MAX_PORTS_PER_AGG];
    UINT2               u2LagPortCount;
    INT4                i4PortIndex = 0;
    UINT1               u1PortPresent = MBSM_FALSE;
    UINT1               u1Result = MBSM_FALSE;

    /* Identify, whether the Ring port is a LAG Port */

    if (CfaGetIfInfo (u4RingIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return u1PortPresent;
    }

    if (CfaIfInfo.u1IfType == CFA_LAGG)
    {
        MEMSET (au2LagPorts, 0, LA_MAX_PORTS_PER_AGG);

        L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4RingIfIndex,
                                               au2LagPorts, &u2LagPortCount);

        for (i4PortIndex = 0; i4PortIndex < u2LagPortCount; i4PortIndex++)
        {
            MBSM_IS_MEMBER_PORT (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                 au2LagPorts[i4PortIndex], u1Result);

            /* when at least one member port of the port channel or
             * all ports of the port channel is present in the card
             * list, return port present. So that NP Layer can take
             * appropriate action, wrt port channel ring port.
             */
            if (u1Result == MBSM_TRUE)
            {
                pPortInfo->u4PortCount--;
                u1PortPresent = MBSM_TRUE;
            }
        }
    }
    else
    {
        MBSM_IS_MEMBER_PORT (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                             u4RingIfIndex, u1PortPresent);

        if (u1PortPresent == MBSM_TRUE)
        {
            pPortInfo->u4PortCount--;
        }
    }

    return u1PortPresent;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMbsmConfRingEntryInHw                        */
/*                                                                           */
/*    Description         : This function will be configure ring entry   into*/
/*                          hardware.                                        */
/*                                                                           */
/*    Input(s)            : pRingInfo     - Pointer to ring node.           */
/*                          u1RingStatus - Ring status to intimate NP layer  */
/*                                         to do appropriate action.         */
/*                                         - ERPS_HW_RING_CREATE,            */
/*                                         - ERPS_HW_RING_MODIFY,            */
/*                                         - ERPS_HW_RING_DELETE             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS - When hardware successfully updated*/
/*                                         for ring info.                    */
/*                          MBSM_FAILURE - When NP Layer failed to update    */
/*                                         ring info.                        */
/*****************************************************************************/

PRIVATE INT4
ErpsMbsmConfRingEntryInHw (tErpsRingInfo * pRingInfo, UINT1 u1RingStatus,
                           tErpsMbsmInfo * pErpsMbsmInfo)
{
    tErpsHwRingInfo     ErpsHwRingInfo;

    MEMSET (&ErpsHwRingInfo, 0, sizeof (tErpsHwRingInfo));

    ErpsHwRingInfo.u4ContextId = pRingInfo->u4ContextId;
    ErpsHwRingInfo.u4Port1IfIndex = pRingInfo->u4Port1IfIndex;
    ErpsHwRingInfo.u4Port2IfIndex = pRingInfo->u4Port2IfIndex;
    ErpsHwRingInfo.VlanId = pRingInfo->RapsVlanId;
    ErpsHwRingInfo.u1ProtectionType = pRingInfo->u1ProtectionType;
    ErpsHwRingInfo.u1HwUpdatedPort = PORT1_PORT2_UPDATED;

    ErpsHwRingInfo.u1Port1Action
        = ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ?
           ERPS_PORT_STATE_BLOCKING : ERPS_PORT_STATE_UNBLOCKING);

    ErpsHwRingInfo.u1Port2Action
        = ((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) ?
           ERPS_PORT_STATE_BLOCKING : ERPS_PORT_STATE_UNBLOCKING);

    MEMCPY (ErpsHwRingInfo.ai4HwRingHandle, pRingInfo->ai4HwRingHandle,
            sizeof (ErpsHwRingInfo.ai4HwRingHandle));

    ErpsHwRingInfo.u1RingAction = u1RingStatus;

    if (ErpsFsErpsMbsmHwRingConfig (&ErpsHwRingInfo, pErpsMbsmInfo) ==
        FNP_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsMbsmConfRingEntryInHw hardware "
                          "configuration failed.\r\n");
        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_HW_CONFIG_FAIL);

        return MBSM_FAILURE;
    }

    MEMCPY (pRingInfo->ai4HwRingHandle, ErpsHwRingInfo.ai4HwRingHandle,
            sizeof (pRingInfo->ai4HwRingHandle));

    return MBSM_SUCCESS;
}
