/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpscxt.c,v 1.9 2014/07/19 13:05:31 siva Exp $
 *
 * Description: This file contains the ERPS Context related functions        
 *                                                        
 *****************************************************************************/

#include "erpsinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtMemInit                                   */
/*                                                                           */
/*    Description         : This function used to create and do the proper   */
/*                          initialization for the context entry.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC tErpsContextInfo *
ErpsCxtMemInit (UINT4 u4ContextId)
{
    tErpsContextInfo   *pErpsContextEntry = NULL;

    if ((pErpsContextEntry = (tErpsContextInfo *) MemAllocMemBlk
         (gErpsGlobalInfo.ContextPoolId)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsCtxtMemInit ctxt memory allocation failed\r\n");
        return NULL;
    }

    MEMSET (pErpsContextEntry, 0, sizeof (tErpsContextInfo));

    pErpsContextEntry->u1ModuleStatus = OSIX_DISABLED;
    pErpsContextEntry->u1TrapStatus = OSIX_ENABLED;
    pErpsContextEntry->u4TraceOption = ERPS_CRITICAL_TRC;
    pErpsContextEntry->u1VlanGroupManager = ERPS_VLAN_GROUP_MANAGER_MSTP;
    pErpsContextEntry->u1ProprietaryClearFS = ERPS_SNMP_FALSE;

    gErpsGlobalInfo.apRingContextInfo[u4ContextId] = pErpsContextEntry;

    ErpsPortVcmGetAliasName (u4ContextId, pErpsContextEntry->au1ContextName);

    ErpsPortIssGetContextMacAddress (u4ContextId);

    return pErpsContextEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsHandleDeleteContxt                           */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Shutdown ERPS for this context and delete     */
/*                             Context                                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index off context that     */
/*                                        needs to be deleted from the ERPS  */
/*                                        module.                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - on successful deletion of ring    */
/*                                         entries of the given context and  */
/*                                         release of context memory.        */
/*                          OSIX_FAILURE - on failure of deleting ring       */
/*                                         entries of the context or failure */
/*                                         to release the context memory.    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsCxtHandleDeleteContxt (UINT4 u4ContextId)
{
    tErpsContextInfo   *pErpsContextEntry = NULL;

    pErpsContextEntry = gErpsGlobalInfo.apRingContextInfo[u4ContextId];

    if (pErpsContextEntry == NULL)
    {
        /* It is possible that context is not created in the ERPS,
         * but a delete indication is received */
        return OSIX_SUCCESS;
    }

    if (ErpsRingDeleteAllRingsInContext (u4ContextId) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsCxtHandleDeleteContxt ctext delete failed\r\n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (gErpsGlobalInfo.ContextPoolId,
                        (UINT1 *) pErpsContextEntry);

    gErpsGlobalInfo.apRingContextInfo[u4ContextId] = NULL;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtHandleUpdateCxtName                       */
/*                                                                           */
/*    Description         : This function will update the context name in    */
/*                          ERPS module whenever context name changes in VCM */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index off context that     */
/*                                        needs to be deleted from the ERPS  */
/*                                        module.                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS                                     */
/*                          OSIX_FAILURE                                     */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsCxtHandleUpdateCxtName (UINT4 u4ContextId)
{
    tErpsContextInfo   *pContextInfo = NULL;

    if ((pContextInfo = ErpsCxtGetContextEntry (u4ContextId)) == NULL)
    {
        return OSIX_FAILURE;
    }

    if (ErpsPortVcmGetAliasName (u4ContextId,
                                 pContextInfo->au1ContextName) != VCM_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtGetContextEntry                           */
/*                                                                           */
/*    Description         : This function used to get the pointer to the     */
/*                          context info entry.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : pointer to tErpsContextInfo                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC tErpsContextInfo *
ErpsCxtGetContextEntry (UINT4 u4ContextId)
{
    if (u4ContextId >= ERPS_SIZING_CONTEXT_COUNT)
    {
        return NULL;
    }

    return gErpsGlobalInfo.apRingContextInfo[u4ContextId];
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtEnableAllRings                            */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Enable ERPS for this context and configure    */
/*                             all the rings present in control plane to     */
/*                             hardware for that particular context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index off context that     */
/*                                        needs to be Enabled  the ERPS      */
/*                                        module.                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - on successful config of ring      */
/*                                         entries of the given context.     */
/*                          OSIX_FAILURE - on failure of config the ring     */
/*                                         entries of the context.           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsCxtEnableAllRings (UINT4 u4ContextId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;

    pRingInfo = ErpsRingGetNextNodeFromRingTable (&RingEntry);

    /* Increment the sync counter to block syncing NP call info
     * to standby node. As this function will invoke multple
     * NPAPI call from this thread.
     */
    ErpsRedHwAuditIncBlkCounter ();

    /* Sync the EnableAll Ring action to Stanby node allow
     * standby taking same action as Active node.
     * If Active failed, before completing this process, standby will
     * complete the this process without getting affected by 
     * Active node failure.
     */
    ErpsRedSyncModuleEnableStatus (u4ContextId);

    while (pRingInfo != NULL)
    {
        if (pRingInfo->u4ContextId != u4ContextId)
        {
            break;
        }
        if ((pRingInfo->u1RowStatus == ACTIVE) &&
            (pRingInfo->u1RingState == ERPS_RING_DISABLED_STATE))
        {
            if (ErpsRingActivateRing (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (u4ContextId, pRingInfo->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "ErpsCxtEnableAllRings unable to activate"
                                  " Ring Group entry\n");
            }
            /* Add ring in to Port Vlan table before making ring active
             * as Ring can start functioning only, if it is present in
             * the port vlan table.
             */
            if (ErpsRingAddNodeToPortVlanRBTree (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (u4ContextId, pRingInfo->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "ErpsCxtEnableAllRings Ring node addition"
                                  " to Vlan table fail\r\n");
            }

            if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
            {
                if (ErpsRingAddNodeToMepRingIdRBTree (pRingInfo) ==
                    OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (u4ContextId, pRingInfo->u4RingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "ErpsCxtEnableAllRings Ring node addition"
                                      " to MepRingId table failed\r\n");
                }
            }
        }
        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    /* Decrement the sync counter to allow syncing NP call info
     * to standby node. 
     */

    ErpsRedHwAuditDecBlkCounter ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtDisableAllRings                           */
/*                                                                           */
/*    Description         : This function will perform following task in ERPS*/
/*                          Module:                                          */
/*                          o  Disable ERPS for this context and delete      */
/*                             all the rings present in hardware for that    */
/*                             given context                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index off context that     */
/*                                        needs to be Enabled  the ERPS      */
/*                                        module.                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - on successful delete of ring      */
/*                                         entries of the given context.     */
/*                          OSIX_FAILURE - on failure of delete the ring     */
/*                                         entries in the hardware of the    */
/*                                         given context.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsCxtDisableAllRings (UINT4 u4ContextId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;

    /* Increment the sync counter to block syncing NP call info
     * to standby node. As this function will invoke multple
     * NPAPI call from this thread.
     */
    ErpsRedHwAuditIncBlkCounter ();

    /* Sync the DisableAll Ring action to Stanby node allow
     * standby taking same action as Active node.
     * If Active failed, before completing this process, standby will
     * complete the this process without getting affected by 
     * Active node failure.
     */

    ErpsRedSyncModuleDisableStatus (u4ContextId);

    pRingInfo = ErpsRingGetNextNodeFromRingTable (&RingEntry);

    while (pRingInfo != NULL)
    {
        if (pRingInfo->u4ContextId != u4ContextId)
        {
            break;
        }
        if ((pRingInfo->u1RowStatus == ACTIVE) &&
            (pRingInfo->u1RingState != ERPS_RING_DISABLED_STATE))
        {
            if (ErpsRingDeActivateRing (pRingInfo, DESTROY) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (u4ContextId, pRingInfo->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "ErpsCxtDisableAllRings unable to deactivate"
                                  " Ring Group entry\n");
            }
            /* Remove the inactive ring from the port vlan table.
             * if ring remain in port vlan table then received link
             * status will get appiled.
             */
            if (ErpsRingRemNodeFromPortVlanTable (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "ErpsCxtDisableAllRings ring "
                                  "failed to remove node from VlanTable\r\n");
            }

            if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
            {
                if (ErpsRingRemNodeFromMepRingIdTable (pRingInfo) ==
                    OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      MGMT_TRC | ALL_FAILURE_TRC,
                                      "ErpsCxtDisableAllRings ring "
                                      "failed to remove node from MepRingIdTable\r\n");
                }
            }
        }
        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    /* Decrement the sync counter to allow syncing NP call info
     * to standby node. 
     */
    ErpsRedHwAuditDecBlkCounter ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsCxtClearAllRingStatsCounters                 */
/*                                                                           */
/*    Description         : This function will reset all the portstats       */
/*                          counter in the ring tables in the given context. */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to the ring node.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsCxtClearAllRingStatsCounters (UINT4 u4ContextId)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;

    pRingInfo = ErpsRingGetNextNodeFromRingTable (&RingEntry);

    while (pRingInfo != NULL)
    {
        if (pRingInfo->u4ContextId != u4ContextId)
        {
            break;
        }

        ErpsRingClearRingStats (pRingInfo);

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }
}
