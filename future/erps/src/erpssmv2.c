/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: erpssmv2.c,v 1.49 2017/09/22 12:34:48 siva Exp $                 
 *                                                                         *
 * Description: This file contains the State Machine event handling and    *
 *              Priority Logic functions for ERPS Version 2.               *
 ***************************************************************************/
#include "erpsinc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleInit                           */
/*                                                                           */
/*    Description         : Following action will be done in the initializat-*/
/*                          ion of ring state machine:                       */
/*                          o Stop guard timer,                              */
/*                          o Stop WTR timer,WTB Timer                       */
/*                          o Blocks RPL Port, if node is RPL owner or       */
/*                            RPL-Neighbour, Unblock non-RPL Port.           */
/*                          o Block Port1 and Unblock Port2 if it            */
/*                            is non-RPL owner.                              */
/*                          o Start WTR Tmr if RPL Owner and mode revertive  */
/*                          o Start Tx of R-APS (NR) messages                */
/*                          o Register ring MEPs with link monitoring protoc-*/
/*                            ol and get the current status of the ring links*/
/*                            If any link status is identified, Priority     */
/*                            logic will identify the current top priority   */
/*                            request. This top priority event will be       */
/*                            handed over to state machine to take further   */
/*                            action.                                        */
/*                          o Move to pending state                          */
/*                                                                           */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       disable event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         state as IDLE state.              */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleInit (tErpsRingInfo * pRingInfo, tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4RplPortIfIndex = 0;
    UINT4               u4NonRplPortIfIndex = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* No need to stop the "Guard timer" "WTB timer" and "WTR timer", as given in state
     * machine, because in state machine it is not possible to have
     * these two timer running when this function get called.
     */
    RingDynInfo.u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_PRIO_REQUEST);

    /* Requirement : Appendix VIII.1 Flush Optimization
     * CLEAR DNF */
    pRingInfo->bDNFStatus = OSIX_FALSE;

    /* A port is considered an RPL port if it is connected to the RPL link.
       non-RPL port is the one which is not connected to the RPL Link. */

    if (pRingInfo->u4RplIfIndex != 0)
    {
        if (pRingInfo->u4RplIfIndex == pRingInfo->u4Port1IfIndex)
        {
            u4RplPortIfIndex = pRingInfo->u4RplIfIndex;
            u4NonRplPortIfIndex = pRingInfo->u4Port2IfIndex;
        }
        else if (pRingInfo->u4RplIfIndex == pRingInfo->u4Port2IfIndex)
        {
            u4RplPortIfIndex = pRingInfo->u4RplIfIndex;
            u4NonRplPortIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }

    if (pRingInfo->u4RplNeighbourIfIndex != 0)
    {
        if (pRingInfo->u4RplNeighbourIfIndex == pRingInfo->u4Port1IfIndex)
        {
            u4RplPortIfIndex = pRingInfo->u4RplNeighbourIfIndex;
            u4NonRplPortIfIndex = pRingInfo->u4Port2IfIndex;
        }
        else if (pRingInfo->u4RplNeighbourIfIndex == pRingInfo->u4Port2IfIndex)
        {
            u4RplPortIfIndex = pRingInfo->u4RplNeighbourIfIndex;
            u4NonRplPortIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }

    RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    /* If it is RPL Owner or RPL neighbour, block the RPL Port and
       unblock non-RPL Port. If it is nither RPL owner nor RPL neighbour,
       block Port1 and unblock Port2 of the ring node. */
    if (u4RplPortIfIndex != 0)
    {
        ErpsUtilSetPortState (pRingInfo, u4RplPortIfIndex, ERPS_PORT_BLOCKING);
        ErpsUtilSetPortState (pRingInfo, u4NonRplPortIfIndex,
                              ERPS_PORT_UNBLOCKING);
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                              ERPS_PORT_BLOCKING);
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                              ERPS_PORT_UNBLOCKING);
    }

    /* Start WTR Timer if RPL Owner */
    if (pRingInfo->u4RplIfIndex != 0 &&
        pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE)
    {
        if (pRingInfo->u4WTRTimerValue == 0)
        {
            /* If WTR timer value is zero, immediatly revert */
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_WTR_EXP_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrWTRTimerExp returned failure"
                                  " from Priority Request handling.\r\n");
            }
        }
        else
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                               pRingInfo->u4WTRTimerValue);
        }
    }

    /* Register ERPS for the ring MEPs and get the current MEP 
     * status.
     */
    if (ErpsUtilRegisterGetFltAndApply (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleInit failed in mep registration "
                          "with cfm.\r\n");
    }

    /* When Minimizing Segmentation feature objects are configured in 
     * interconnected node of the sub-ring and that nodes are 
     * coming up with Virtual channel failure due to multiple failure
     * in main ring then Block Indication logic will be called for this
     * UP-MEP failure to apply Manual Switch Request and Ring Will 
     * move to Manual state and starts transmitting the RAPSMS packets
     * But SwitchCmd Type will not be updated as ERPS_SWITCH_COMMAND_MANUAL
     * Due to this, below switch case will be executed with 
     * SWITCH_COMMAND_NONE for this same ring node and starts transmitting 
     * RAPS-NR packets.Because of this, other ring nodes in the
     * ring will move to idle state except the node where Local MS is applied
     * To avoid this issue,we should not call the SEM again 
     * with No Request */

    if ((pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_NONE) &&
        (pRingInfo->u1HighestPriorityRequest ==
         ERPS_RING_MANUAL_SWITCH_REQUEST))
    {
        return OSIX_SUCCESS;
    }

    /* Check if Force Switch or Manual Switch is present call Priority 
     * request to handle that.
     */
    switch (pRingInfo->u1SwitchCmdType)
    {
        case ERPS_SWITCH_COMMAND_FORCE:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleInit: Handling Forced Switch"
                              "Command\r\n");
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_FORCE_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleInit failed in priority"
                                  " request handling for force switch.\r\n");
            }

            break;
        case ERPS_SWITCH_COMMAND_MANUAL:

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleInit: Handling Manual Switch"
                              "Command\r\n");
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_MANUAL_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleInit failed in priority"
                                  " request handling for manual switch.\r\n");
            }

            break;

        case ERPS_SWITCH_COMMAND_NONE:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                              pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleInit:Switch"
                              " Command is None \r\n");
            if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
                (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
            {
                return OSIX_SUCCESS;
            }

            /* If no failure is present, start tx of NR messages */
            if (ErpsTxStartRapsMessages (pRingInfo, ERPS_STATUS_BIT_NONE,
                                         ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleInit failed to start"
                                  " R-APS messages\r\n");
            }
            break;
        default:
            break;
    }

    /* When the ring is deactivated and FS is applied, FS will be enforced in
     * the ring after activating it back.
     * If the ring is de-activated again and CLEAR command is applied and then
     * the ring is made active then CLEAR command has to be enforced in the
     * ring and the ring has to move to idle / pending state. So clear
     * request is handled in init function itself */
    if (pRingInfo->u1RingClearOperatingModeType == ERPS_CLEAR_COMMAND)
    {
        i4RetVal =
            ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_CLEAR_REQUEST,
                                         NULL);
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                      pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsSmV2RingHandleInit : R-APS NR messages "
                      "are transmitted\r\n");

    UNUSED_PARAM (i4RetVal);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleClearCmd                       */
/*                                                                           */
/*    Description         : This function takes necessary actions upon       */
/*                          reception of a CLEAR command from the network    */
/*                          administrator.                                   */
/*    Input(s)              pRingInfo - Pointer to ring node for which the   */
/*                                      CLEAR command has been issued.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         returned from state/event machine.*/
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete handling of input request*/
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleClearCmd (tErpsRingInfo * pRingInfo,
                            tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsContextInfo   *pErpsContextInfo = NULL;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;
    UINT1               u1RplPortStatus = 0;
    UINT1               u1FlushStatus = OSIX_FALSE;
    UINT4               u4NonRplPortIfIndex = 0;
    UNUSED_PARAM (pErpsRcvdMsg);

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Clear command is issued to achieve the following : 
     * Clearing Forced switch.
     * Clearing Manual Switch.
     * Trigger reversion before the WTR or WTB timer expires in revertive mode.
     * Trigger reversion in non-revertive mode 
     * [Reference - Section 8 of ITU-T G.8032 Y.1344 (03/2010)std]
     */

    if (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE)
    {
        /* When clear command is issued in pending state do the following : 
         * If RPL owner, stop WTR and WTB.
         * Unblock the non-RPL port.
         * If RPL port is blocked , Transmit (NR,RB,DNF).
         * If RPL port is not blocked,Block RPL port, Transmit (NR,RB)and flush
         * Move to IDLE state.
         * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
         * */

        u1Status = ERPS_RB_BIT_SET;

        if (pRingInfo->u4RplIfIndex != 0)
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
            ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);

            u1RplPortStatus =
                (pRingInfo->u4RplIfIndex ==
                 pRingInfo->u4Port1IfIndex) ? pRingInfo->
                u1Port1Status : pRingInfo->u1Port2Status;
            u4NonRplPortIfIndex =
                (pRingInfo->u4RplIfIndex ==
                 pRingInfo->u4Port1IfIndex) ? pRingInfo->
                u4Port2IfIndex : pRingInfo->u4Port1IfIndex;

            if (u1RplPortStatus & ERPS_PORT_BLOCKING)
            {
                u1Status |= ERPS_DNF_BIT_SET;
            }
            else
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                                      ERPS_PORT_BLOCKING);
                u1FlushStatus = OSIX_TRUE;
            }
            /* On clearing the appiled FS in Non-RPL node, As per standard
             * RPL is blocked first which is followed by Un blocking the
             *  non RPL port*/
            ErpsUtilSetPortState (pRingInfo, u4NonRplPortIfIndex,
                                  ERPS_PORT_UNBLOCKING);
            RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
            RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_NODE_STATUS);

            if (u1FlushStatus == OSIX_TRUE)
            {
                ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
            }
            if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                         ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleClearCmd failed to transmit"
                                  "R-APS NR,RB messages.\r\n");
            }

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleClearCmd :Ring Node is RPL owner"
                              "R-APS NR,RB messages are transmitted.\r\n");
        }
        pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
        RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
        /* When APS clear command is given, there is a chance for RPL
         * not receiving any packets , so inorder to avoid FOP trap at 
         * certain situations FOP timer is stopped*/
        if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
        }
    }
    else if (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE ||
             pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE)
    {
        /* When clear command is issued in forced switch or manual switch state
         * do the following : 
         * If any ring port is blocked :
         *        Start guard timer, Tx R-APS (NR)
         *        If RPL owner and revertive mode, start WTB.
         * Move to PENDING state.
         * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
         * */

        if ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ||
            (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR,
                               pRingInfo->u4GuardTimerValue);
            if (ErpsTxStartRapsMessages
                (pRingInfo, u1Status, ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleClearCmd failed to transmit"
                                  "R-APS NR messages.\r\n");
            }

            /* The ring state should be moved to pending before triggering
             * the WTB or WTB expiry event.
             */

            /* When the ring is working in non-revertive mode, ring 
             * will be in pending state and u1SwitchCmdType will be none 
             * only. So there is  no need to reset the switch command 
             * type as NONE again in pending state
             * But after the clearance of FS/MS, it needs to be reset as None*/

            pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
            RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_STATE);

            if ((pRingInfo->u4RplIfIndex != 0) &&
                (pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE))
            {
                if (pRingInfo->u4WTBTimerValue == 0)
                {

                    /* If WTB timer value is zero, immediatly revert */
                    if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                     ERPS_RING_WTB_EXP_REQUEST,
                                                     NULL) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsTmrWTBTimerExp returned failure"
                                          " from Priority Request handling.\r\n");
                    }

                }
                else
                {
                    ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR,
                                       pRingInfo->u4WTBTimerValue);
                }

            }
            /* As per the standard [Rec. ITU-T G.8032/Y.1344 (03/2010)]
             * section[10.1.14 R-APS block logic] RAPS channel should
             * not be blocked when top priority request is WTB_RUNNING. */
            if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                                      ERPS_PORT_BLOCKING);
            }

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleClearCmd : "
                              "R-APS NR messages are transmitted.\r\n");

            /* when SF is simulated on a link, ring moves to protection state. 
             * if fs command is issued on another node or in same node,it overrides 
             * sf and ring moves to forced switch state. if fs clear is given now, 
             * the ring will move to idle state upon expiry of wtb timer 
             * even though sf still exists. this situation will lead to ring 
             * segmentation. to avoid this, a proprietary approach is done so  
             * that if fs is given over sf,the state machine           
             * is triggered with local sf request and the ring will move to           
             * protection state. this proprietary implementation will work           
             * only if u1proprietaryclearfs is set to true */
            if ((pErpsContextInfo->u1ProprietaryClearFS == ERPS_SNMP_TRUE) &&
                ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
                 ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
                  (pRingInfo->u4Port2IfIndex != 0))))
            {

                if ((pRingInfo->u4Port1IfIndex ==
                     pRingInfo->u4RplNextNeighbourIfIndex)
                    && (pRingInfo->u1Port1Status & ERPS_PORT_FAILED))
                {
                    pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;

                }

                if ((pRingInfo->u4Port2IfIndex ==
                     pRingInfo->u4RplNextNeighbourIfIndex)
                    && (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
                {
                    pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;

                }

                RingDynInfo.u1HighestPriorityRequest =
                    ERPS_RING_NO_ACTIVE_REQUEST;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_PRIO_REQUEST);

                if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                 ERPS_RING_LOCAL_SF_REQUEST,
                                                 pErpsRcvdMsg) != OSIX_SUCCESS)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsSmCalTopPriorityRequest Called for "
                                      "Local SF request returned failure.\r\n");
                    return OSIX_FAILURE;
                }
            }
        }
    }

    /* The Switchtype(FS/MS) setting default value hence cleared */
    pRingInfo->u1RingClearOperatingModeType = ERPS_CLEAR_COMMAND_NONE;
    pRingInfo->u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleLocalFS                        */
/*                                                                           */
/*    Description         : This function handles the reception of Local     */
/*                          Forced Switch event. It happens when a Forced    */
/*                          Switch command is issued by the network admin on */
/*                          an ERPS node. Effectively, this function is an   */
/*                          implementation of Row 59 of the State Machine    */
/*                          which is described in Section 10.1.2 of          */
/*                          ITU-T G.8032/Y.1344 (03/2010).                   */
/*                                                                           */
/*    Input(s)              pRingInfo    - Pointer to ring node for which    */
/*                                         Forced Switch has been issued     */
/*                          pErpsRcvdMsg - This parameter is ignored         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         handles the Local FS Event        */
/*                          OSIX_FAILURE - When a this function failes to    */
/*                                         complete the action.              */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleLocalFS (tErpsRingInfo * pRingInfo,
                           tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsContextInfo   *pErpsContextInfo = NULL;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;
    UINT1               u1SwitchCmdIfIndexPortStatus = 0;
    UINT1               u1FlushStatus = OSIX_FALSE;
    UINT4               u4NonSwitchCmdIfIndex = 0;
    UINT1               u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
    UNUSED_PARAM (pErpsRcvdMsg);

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    /* When forced switch command is issued on a given ring port
     * do the following :
     *
     * If requested ring port is not already blocked or 
     * if the current ring state is forced switch
     *        Block the requested port.
     *        Tx R-APS (FS)
     *        Flush FDB
     * else      
     *        Tx R-APS(FS,DNF)
     * Unblock the non-requested ring port.
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     * Move to FORCED SWITCH state.
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     */

    if (pRingInfo->u4RplIfIndex != 0 &&
        (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE))
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }

    /* Get requested ring port status */
    u1SwitchCmdIfIndexPortStatus =
        (pRingInfo->u4SwitchCmdIfIndex ==
         pRingInfo->u4Port1IfIndex) ? pRingInfo->u1Port1Status : pRingInfo->
        u1Port2Status;

    /* Get Non requested ring port index */
    u4NonSwitchCmdIfIndex =
        (pRingInfo->u4SwitchCmdIfIndex ==
         pRingInfo->u4Port1IfIndex) ? pRingInfo->u4Port2IfIndex : pRingInfo->
        u4Port1IfIndex;

    if ((!(u1SwitchCmdIfIndexPortStatus & ERPS_PORT_BLOCKING)) ||
        (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE))
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                              ERPS_PORT_BLOCKING);
        u1FlushStatus = OSIX_TRUE;
    }
    else
    {
        u1Status |= ERPS_DNF_FIELD;

        /*
         * When R-APS without virtual channel is enabled, (sub-ring is 
         * configured to run without R-APS virtual channel), and the
         * top priority request is a Local SF or local FS request, then
         * both the traffic channel and R-APS channel are  blocked on
         * the appropriate ring port based on the block/unblock ring portsignal.
         * [Reference : Section : 10.1.14 of ITU-T G.8032/Y.1344 
         * (03/2010) standard]
         *
         */
        if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                                  ERPS_PORT_BLOCKING);
        }
    }

    if (pRingInfo->u1RingState != ERPS_RING_FORCED_SWITCH_STATE)
    {
        ErpsUtilSetPortState (pRingInfo, u4NonSwitchCmdIfIndex,
                              ERPS_PORT_UNBLOCKING);
        /* if other then the RPL port was failed, set RPL unblock */
        if ((0 != pRingInfo->u4RplIfIndex)
            && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
        {
            RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
            RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_NODE_STATUS);
        }
    }

    /* As per standard Rec. ITU-T G.8032/Y.1344 (03/2010), 
     * Appendix VIII, Flush optimization
     * If failed ring port is RPL Next-Neighbour port
     * Tx R-APS (SF) from failed ring port then ring action will be 
     * updated for failed port.
     * If FS is applied on any port on same node
     * Tx RAPS (FS) should happen from  both the ring ports
     * So updating Ring Action to both ports*/

    pRingInfo->u1RingTxAction = u1RingTxAction;

    if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                 ERPS_MESSAGE_TYPE_FS) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalFS failed to start "
                          "R-APS messages.\r\n");
    }
    if (u1FlushStatus == OSIX_TRUE)
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        u1FlushStatus = OSIX_FALSE;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsSmV2RingHandleLocalFS : "
                      "R-APS FS messages are transmitted.\r\n");

    if (pRingInfo->u1RingState != ERPS_RING_FORCED_SWITCH_STATE)
    {
        RingDynInfo.u1RingState = ERPS_RING_FORCED_SWITCH_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }
    /* Send FS trap when more than one FS is present in Ring */
    else
    {
        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_FORCE_SWITCH);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleRAPSFS                         */
/*                                                                           */
/*    Description         : This function handles the reception of R-APS(FS) */
/*                          PDU. It happens when a Forced Switch command is  */
/*                          issued by the network admin on an ERPS node      */
/*                          other than this node. Effectively, this          */
/*                          function is an implementation of Row 60 of the   */
/*                          State Machine described in Section 10.1.2 of     */
/*                          ITU-T G.8032/Y.1344 (03/2010).                   */
/*                                                                           */
/*    Input(s)              pRingInfo    - Pointer to ring node which has    */
/*                                         received the R-APS(FS) PDU.       */
/*                          pErpsRcvdMsg - R-APS(FS) PDU                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         handles the reception of FS Msg   */
/*                          OSIX_FAILURE - When a this function failes to    */
/*                                         complete the action.              */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleRAPSFS (tErpsRingInfo * pRingInfo,
                          tErpsRcvdMsg * pErpsRcvdMsg)
{
    tErpsContextInfo   *pErpsContextInfo = NULL;
    unErpsRingDynInfo   RingDynInfo;

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* When R-APS FS message is received in the ring node 
     * do the following :
     *
     * Unblock the ring ports.
     * Stop Tx of R-APS Messages
     * Flush FDB if DNF bit is not set.
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     * Move to FORCED SWITCH state.
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     * */

    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                          ERPS_PORT_UNBLOCKING);
    ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                          ERPS_PORT_UNBLOCKING);
    /* if other then the RPL port was failed, set RPL unblock */
    if ((0 != pRingInfo->u4RplIfIndex)
        && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
    {
        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);
    }

    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleRAPSFS failed to stop Tx of "
                          "R-APS messages.\r\n");
    }

    if ((pRingInfo->u1FlushLogicSupport == OSIX_DISABLED) &&
        (pErpsRcvdMsg->u1DNFBitStatus == 0))
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
    }

    if (pRingInfo->u4RplIfIndex != 0 &&
        (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE))
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsSmV2RingHandleRAPSFS : Both the ring ports are set "
                      "to unblocked state.\r\n");

    RingDynInfo.u1RingState = ERPS_RING_FORCED_SWITCH_STATE;
    /*
     * If Manual switch is already configured in this Switch,
     * and if it receive the R-APS FS packet, Manual Switch should 
     * be cleared in that Ring Node 
     */
    if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)
    {

        RingDynInfo.u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_CMD_INDEX);
        RingDynInfo.u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_CMD_TYPE);
    }

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleLocalSF                        */
/*                                                                           */
/*    Description         : Following action will be done in in case LocalSF-*/
/*                          event handlingcin Version 2:                     */
/*                          o Block switch port in case of LocalSF is raised */
/*                            by Force switch or Manual Switch Command.      */
/*                          o Block failed port, if LocalSF event is received*/
/*                            due to link failure of one of the ring link.   */
/*                          o Unblock other port, which is neither failed nor*/
/*                            one of force switch or manual switch port.     */
/*                          o Transmit SF messages, in case of change in     */
/*                            blocking port.                                 */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       localSF event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         node to update the ring info.     */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleLocalSF (tErpsRingInfo * pRingInfo,
                           tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    /* LocalSF event is generated when a port gets Signal Fail notification.
       u4BlockIfIndex     - Index of port for which the event has been generated
       u1BlockPortStatus  - s/w state of the port for which the event
       has been generated
       u4UnblockIfIndex   - Port other than the port for which the event
       has been generated
       u1UnblockPortStatus - s/w state of the port other than the port for which
       the event has been generated */

    UINT4               u4BlockIfIndex = 0;
    UINT4               u4UnblockIfIndex = 0;
    UINT1               u1BlockPortStatus = 0;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;
    UINT1               u1RingTxAction = ERPS_TX_RAPS_ON_BOTH_RING_PORTS;
    UINT1               u1FlushStatus = OSIX_FALSE;
    UNUSED_PARAM (pErpsRcvdMsg);

    /* Checking Blocked/Unblocked port if index,
     * Blocked/Unblocked port status */
    if (pRingInfo->u1Port1Status & ERPS_PORT_FAILED)
    {
        u4BlockIfIndex = pRingInfo->u4Port1IfIndex;
        u1BlockPortStatus = pRingInfo->u1Port1Status;
        u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;

        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            u4UnblockIfIndex = pRingInfo->u4Port2IfIndex;

        }
    }
    else
    {
        u4BlockIfIndex = pRingInfo->u4Port2IfIndex;
        u1BlockPortStatus = pRingInfo->u1Port2Status;
        u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;

        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            u4UnblockIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }

    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        /* When Local SF is received in the ring node in IDLE state
         * do the following :
         * Block Failed Ring port
         * If Failed Ring port is RPL port (i.e if either port
         * connected to RPL in RPL Owner node and RPL Neighbor node)
         *       Tx R-APS (SF,DNF)
         *       set DNF Status
         * else If Failed ring port is RPL Next-Neighbour port
         *       Tx R-APS (SF) from failed ring port
         * Else
         *       Tx R-APS (SF)
         *       Flush FDB
         */

        /* Withou Virtual Channel case is addressed here
         * blocking is called Even if its already blocked */
        ErpsUtilSetPortState (pRingInfo, u4BlockIfIndex, ERPS_PORT_BLOCKING);
        /* To avoid the temporary loop when SF is detected in
         * Non-RPL port of RPL node,block the Failed port first
         * then Unblock the non-failed ring port */
        if (u4UnblockIfIndex != 0)
        {
            ErpsUtilSetPortState (pRingInfo, u4UnblockIfIndex,
                                  ERPS_PORT_UNBLOCKING);
            /* if other then the RPL port was failed, set RPL unblock */
            if ((0 != pRingInfo->u4RplIfIndex)
                && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
            {
                RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
                RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_NODE_STATUS);
            }
        }

        if ((u4BlockIfIndex == pRingInfo->u4RplIfIndex)
            || (u4BlockIfIndex == pRingInfo->u4RplNeighbourIfIndex))
        {
            u1Status |= ERPS_DNF_BIT_SET;
            pRingInfo->bDNFStatus = OSIX_TRUE;
        }
        else if (u4BlockIfIndex == pRingInfo->u4RplNextNeighbourIfIndex)
        {
            pRingInfo->u1RingTxAction = u1RingTxAction;
        }
        else
        {
            u1FlushStatus = OSIX_TRUE;
        }

        if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleLocalSF failed to stop R-APS "
                              "messages.\r\n");
        }

        if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                     ERPS_MESSAGE_TYPE_SF) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleLocalSF failed to start "
                              "R-APS (SF) messages.\r\n");
        }

        if (u1FlushStatus == OSIX_TRUE)
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
            u1FlushStatus = OSIX_FALSE;
        }

        RingDynInfo.u1RingState = ERPS_RING_PROTECTION_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

        return OSIX_SUCCESS;
    }

    /* If MS command is currently active, it should be cleared
     * when local SF or RAPS-SF is received
     */
    if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)
    {
        RingDynInfo.u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_CMD_INDEX);
        RingDynInfo.u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_CMD_TYPE);

    }

    /* When Local SF is received in the ring node 
     * do the following :
     *
     * If failed port is already blocked
     *       Tx R-APS (SF,DNF)
     * else
     *       Block failed ring port
     *       Tx R-APS (SF)
     *       Flush FDB
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     * Move to protection state. 
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     */

    /* If the failed port is already blocked */
    if (u1BlockPortStatus & ERPS_PORT_BLOCKING)
    {
        u1Status |= ERPS_DNF_BIT_SET;

        /*
         * When R-APS without virtual channel is enabled, (i.e sub-ring is 
         * configured to run without R-APS virtual channel), and the
         * top priority request is a LocalSF or LocalFS, then both the
         * Traffic and R-APS channel are  blocked on the appropriate
         * ring port based on the block/unblock ring port signal.
         * [Reference : section : 10.1.14 of ITU-T G.8032/Y.1344  (03/2010)
         * Standard]
         */
        if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
        {
            ErpsUtilSetPortState (pRingInfo, u4BlockIfIndex,
                                  ERPS_PORT_BLOCKING);
        }
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, u4BlockIfIndex, ERPS_PORT_BLOCKING);

        if (pRingInfo->bDNFStatusinRPL == OSIX_TRUE)
        {
            u1Status |= ERPS_DNF_BIT_SET;
            u1FlushStatus = OSIX_FALSE;
        }
        else
        {
            u1FlushStatus = OSIX_TRUE;
            /* When link failure is occurred on RPL/Neighbor node, DNF status 
             * will be set either through LocalSF or RAPSSF and If failure got 
             * cleared on RPL/Neighbor and failure occurs on non rpl port 
             * of RPL node, DNF status will not be cleared and flush 
             * will not occur. 
             * To avoid this issue, bDNFStatus can be set to FALSE. */

            if (((pRingInfo->u4RplIfIndex != 0) ||
                 (pRingInfo->u4RplNeighbourIfIndex != 0)) &&
                (pRingInfo->bDNFStatus == OSIX_TRUE))
            {
                pRingInfo->bDNFStatus = OSIX_FALSE;
            }
        }
    }
    /* when SF is detected in Ring node,block the Failed port first
     * then Unblock the non-failed ring port */
    if (u4UnblockIfIndex != 0)
    {
        ErpsUtilSetPortState (pRingInfo, u4UnblockIfIndex,
                              ERPS_PORT_UNBLOCKING);
        /* if other then the RPL port was failed, set RPL unblock */
        if ((0 != pRingInfo->u4RplIfIndex)
            && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
        {
            RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
            RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_NODE_STATUS);
        }
    }
    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalSF failed to stop R-APS "
                          "messages.\r\n");
    }

    if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                 ERPS_MESSAGE_TYPE_SF) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalSF failed to start R-APS "
                          "messages.\r\n");
    }

    if (u1FlushStatus == OSIX_TRUE)
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ErpsSmV2RingHandleLocalSF : Transmits R-APS "
                      "SF messages.\r\n");

    if ((pRingInfo->u4RplIfIndex != 0) &&
        (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE))
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }

    if (pRingInfo->u1RingState != ERPS_RING_PROTECTION_STATE)
    {
        RingDynInfo.u1RingState = ERPS_RING_PROTECTION_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleLocalClearSF                   */
/*                                                                           */
/*    Description         : Following action will be done in in case LocalSF-*/
/*                          event handlingcin Version 2:                     */
/*                          o Block switch port in case of LocalSF is raised */
/*                            by Force switch or Manual Switch Command.      */
/*                          o Block failed port, if LocalSF event is received*/
/*                            due to link failure of one of the ring link.   */
/*                          o Unblock other port, which is neither failed nor*/
/*                            one of force switch or manual switch port.     */
/*                          o Transmit SF messages, in case of change in     */
/*                            blocking port.                                 */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       localSF event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to make ring     */
/*                                         node to update the ring info.     */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleLocalClearSF (tErpsRingInfo * pRingInfo,
                                tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* If Local ClearSF received, when guard timer is running, stop the
     * running guard timer and restart it.
     */
    ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR);

    ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR, pRingInfo->u4GuardTimerValue);

    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalClearSF failed to stop R-APS "
                          "messages.\r\n");
    }

    if (ErpsTxStartRapsMessages (pRingInfo, ERPS_STATUS_BIT_NONE,
                                 ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalClearSF failed to start R-APS "
                          "messages.\r\n");
    }

    /* The state should be moved to pending before WTR is triggered.
     * If WTRvalue = 0, the expiry event will be triggered and it
     * changes the state to idle. So, the ring state should be changed
     * before calling the WTR timer or WTR expiry invocation
     */
    RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    if ((pRingInfo->u4RplIfIndex != 0) &&
        (pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE))
    {
        if (pRingInfo->u4WTRTimerValue == 0)
        {
            /* If WTR timer value is zero, immediatly revert */
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_WTR_EXP_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrWTRTimerExp returned failure"
                                  " from Priority Request handling.\r\n");
            }
        }
        else
        {
            ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                               pRingInfo->u4WTRTimerValue);
        }
    }
    /* As per the standard [Rec. ITU-T G.8032/Y.1344 (03/2010)]
     * section[10.1.14 R-APS block logic] RAPS channel should
     * not be blocked when top priority request is WTR_RUNNING. */
    if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
    {
        if ((pRingInfo->u1Port1Status & ~ERPS_PORT_FAILED) &&
            (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING))
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_BLOCKING);
        }
        else if ((pRingInfo->u1Port2Status & ~ERPS_PORT_FAILED) &&
                 (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_BLOCKING);
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleRAPSSF                         */
/*                                                                           */
/*    Description         : Following action will be done in in case RAPS(SF)*/
/*                          message reception in Version 2                   */
/*                          o Stop tranmission of R-APS messages.            */
/*                          o Unblocked non-failed ports.                    */
/*                          o Stop WTR if node is PROTECTION State.          */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       RAPS(SF)event has occuered.         */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         R-APS(SF) message.                */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleRAPSSF (tErpsRingInfo * pRingInfo,
                          tErpsRcvdMsg * pErpsRcvdMsg)
{
    tErpsContextInfo   *pErpsContextInfo = NULL;
    unErpsRingDynInfo   RingDynInfo;

    /* When R-APS SF is received in the ring node 
     * do the following :
     *
     * Unblock the non-failed ring ports.
     * Stop Tx R-APS messages.
     * Flush FDB if DNF bit is not set.
     * If Ring state is IDLE
     *    - If DNF bit is not set
     *         . Flush FDB.
     *         . If RPL Next Neighbour port
     *            = Tx three R-APS (SF) 
     *         . If RPL Owner Node
     *            = Tx three R-APS SF
     *  Clear DNF Status
     *    - Else
     *        . set DNF Status
     *
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     *        If DNF bit is not set
     *            - Clear DNF Status 
     * Move to protection state. 
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     */

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    /* When the ring is in IDLE condition and SF is given to RPL port
     * All non-failed ring ports will move to unblocked condition
     * So RPL-Neighbor will also moves to unblocked state after CC miss
     * RPL-Neighbor detects local failure and change its port again 
     * to Blocked condition, due to this state change from Unblock to Block
     * DNF bit is not set in RAPS pdu transmitted from RPL-Neighbor */
    /* Unblocking Non-failed ports */
    /* When RPL node is killed and ports connected to RPL node is made link 
     * down. Now reboot the RPL, hold off timer will be running in RPL, 
     * Now bring up the port which is connected to non-RPL port of RPL node 
     * first and bring up the other failed port. Both are made up before 
     * hold-off timer expiry in RPL. Now all the ports are unblocked and loop 
     * will be formed. To avoid this, When hold-off timer is running non-failed 
     * ring ports are not unblocked.*/
    if (!(pRingInfo->u4RingNodeStatus & ERPS_HOLDOFF_TIMER_RUNNING))
    {

        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
    }
    if (((pRingInfo->u4RplIfIndex != 0)
         || (pRingInfo->u4RplNeighbourIfIndex != 0))
        && ((pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
            && (pErpsRcvdMsg->u1DNFBitStatus == OSIX_TRUE)))
    {
        pRingInfo->bDNFStatusinRPL = OSIX_TRUE;
    }

    /* if other then the RPL port was failed, set RPL unblock */
    if ((0 != pRingInfo->u4RplIfIndex)
        && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
    {
        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);
    }

    if ((pRingInfo->u1FlushLogicSupport == OSIX_DISABLED) &&
        (pErpsRcvdMsg->u1DNFBitStatus == 0))
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
    }

    /* If MS command is currently active, it should be cleared
     * when local SF or RAPS-SF is received
     */

    if (pRingInfo->u1SwitchCmdType == ERPS_SWITCH_COMMAND_MANUAL)
    {
        RingDynInfo.u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_CMD_INDEX);
        RingDynInfo.u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_CMD_TYPE);
    }

    /* Stop the transmission of R-APS messages */
    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleSFMsg failed to stop R-APS "
                          "messages.\r\n");
    }

    if (gErpsRcvdMsg.u1RingVersion != ERPS_RAPS_V1_PDU)
    {
        if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
        {
            /* If DNF bit is not set, flush FDB */
            if (pErpsRcvdMsg->u1DNFBitStatus == 0)
            {
                /* FDB Flush As Per Appendix VIII (Flush Optmization)
                 * of ITU-T G.8032(03/2010) is not done here, as FDB Flush
                 * is already done in the begining, irrespective of the
                 * Ring State. */
                if ((pRingInfo->u4RplNextNeighbourIfIndex != 0)
                    || ((pRingInfo->u4RplIfIndex != 0)))
                {

                    /* Stop the transmission of R-APS messages */
                    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC |
                                          ALL_FAILURE_TRC,
                                          "ErpsSmV2RingHandleSFMsg failed to stop R-APS "
                                          "messages.\r\n");
                    }

                    if (ErpsTxStartRapsMessages
                        (pRingInfo, ERPS_STATUS_BIT_NONE,
                         ERPS_MESSAGE_TYPE_SF) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC |
                                          ALL_FAILURE_TRC,
                                          "ErpsSmV2RingHandleRAPSSF failed to start "
                                          "R-APS messages.\r\n");
                    }

                    /* Stop the transmission of R-APS messages */
                    if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC |
                                          ALL_FAILURE_TRC,
                                          "ErpsSmV2RingHandleSFMsg failed to stop R-APS "
                                          "messages.\r\n");
                    }

                    if (pRingInfo->u4RplIfIndex != 0)
                    {
                        pRingInfo->bDNFStatus = OSIX_FALSE;
                    }
                }
            }
            else if (pRingInfo->u4RplIfIndex != 0)
            {
                pRingInfo->bDNFStatus = OSIX_TRUE;
            }
        }
    }

    /* If RPL Owner, stop WTR and WTB timers */
    if ((pRingInfo->u4RplIfIndex != 0) &&
        (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE))
    {
        /* If R-APS SF is received in RPL Owner node in Pending state
         * and If DNF bit is not set then set DNF status. as per 
         * Appendix VIII.1 Optimized flush. */
        if (gErpsRcvdMsg.u1RingVersion != ERPS_RAPS_V1_PDU)
        {
            if (pErpsRcvdMsg->u1DNFBitStatus == 0)
            {
                pRingInfo->bDNFStatus = OSIX_FALSE;
            }
        }
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }
    /* Move to protection state */
    RingDynInfo.u1RingState = ERPS_RING_PROTECTION_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleRAPSMS                         */
/*                                                                           */
/*    Description         : This function handles the reception of R-APS(MS) */
/*                          PDU. It happens when a Manual Switch command is  */
/*                          issued by the network admin on an ERPS node      */
/*                          other than this node. Effectively, this          */
/*                          function is an implementation of Row 8, Row 36,  */
/*                          and Row 64 of the State Machine described in     */
/*                          Section 10.1.2 of ITU-T G.8032/Y.1344 (03/2010). */
/*                                                                           */
/*    Input(s)              pRingInfo    - Pointer to ring node which has    */
/*                                         received the R-APS(MS) PDU.       */
/*                          pErpsRcvdMsg - R-APS(MS) PDU                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         handles the reception of MS Msg   */
/*                          OSIX_FAILURE - When a this function failes to    */
/*                                         complete the action.              */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleRAPSMS (tErpsRingInfo * pRingInfo,
                          tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;

    /* When R-APS MS is received in the ring node 
     * do the following :
     *
     * If current state is not manual switch 
     *           Unblock the non-failed ring ports.
     *           Stop Tx R-APS messages.
     *
     * If current state is manual switch and if any ring port is blocked
     *           Start guard timer
     *           Tx R-APS (NR)
     *           If RPL owner and revertive mode, start WTB
     *           If both ring ports are not unblocked, move to
     *           pending State.
     *
     * Flush FDB if DNF bit is not set.
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     * If current state is not manual switch 
     *        Move to Manual switch state. 
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     */

    /* If Ring node is in Manual switch state and R-APS message is received */
    if (pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE)
    {
        /* If any ring port is blocked */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ||
            (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
        {
            /* start guard timer */
            ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR,
                               pRingInfo->u4GuardTimerValue);

            /* Tx R-APS NR Messages */
            if (ErpsTxStartRapsMessages (pRingInfo, ERPS_STATUS_BIT_NONE,
                                         ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleRAPSMS failed to start "
                                  "R-APS messages.\r\n");
            }

            /* If RPL owner node and in revertive mode */
            if ((pRingInfo->u4RplIfIndex != 0) &&
                (pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE))
            {
                /* Start WTB timer */
                if (pRingInfo->u4WTBTimerValue == 0)
                {

                    /* If WTB timer value is zero, immediatly revert */
                    if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                     ERPS_RING_WTB_EXP_REQUEST,
                                                     NULL) == OSIX_FAILURE)
                    {
                        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                          pRingInfo->u4RingId,
                                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                          "ErpsTmrWTBTimerExp returned failure"
                                          " from Priority Request handling.\r\n");
                    }

                }
                else
                {
                    ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR,
                                       pRingInfo->u4WTBTimerValue);
                }
            }

            RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_STATE);
            /* When R-APS MS is received in a ring node (R-APS MS packet 
             * is injected in the ring) , where  the ring port is in 
             * blocked state (because of Local MS and the ring is in 
             * Manual Switch state), the ring starts ending RAPS-NR packets 
             * and moves to pending state and local MS is cleared in the 
             * ring. After WTB timer expiry , the ring moves to idle state. 
             * Now if the ring node is deactivated and activated, ring should 
             * still remain in the previous state i.e., idle state. 
             * To achieve that the switch command index and switch command 
             * are set to default values. */

            RingDynInfo.u4SwitchCmdIfIndex = ERPS_SWITCH_PORT_NONE;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_CMD_INDEX);
            RingDynInfo.u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_CMD_TYPE);
        }
    }
    else
    {
        /* When R-APS MS is received when the ring is in IDLE, 
           and Pending state the following block will handle the request */

        /* Unblocking Non-Failed ring ports */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }

        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
        /* if other then the RPL port was failed, set RPL unblock */
        if ((0 != pRingInfo->u4RplIfIndex)
            && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
        {
            RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
            RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_NODE_STATUS);
        }

        /* Stop Tx R-APS */
        if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleRAPSMS failed to stop Tx of "
                              "R-APS messages.\r\n");
        }

        /* If ring node is in pending state and if it is RPL Owner 
           node then stop wtr, wtb timers */
        if ((pRingInfo->u1RingState == ERPS_RING_PENDING_STATE) &&
            (pRingInfo->u4RplIfIndex != 0))
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
            ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
        }

        /* Flush is done only when the node state is changed.
         * The flush logic is added here to avoid repetitive flush on 
         * reception of R-APS MS packets as when the ring is in manual
         * state R-APS MS will continously flow in the ring until a 
         * clear command has been issued to remove the manual switch */
        if ((pRingInfo->u1FlushLogicSupport == OSIX_DISABLED) &&
            (pErpsRcvdMsg->u1DNFBitStatus == 0))
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        }

        /* Move from IDLE and Pending to Manual switch state */
        RingDynInfo.u1RingState = ERPS_RING_MANUAL_SWITCH_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleLocalMS                        */
/*                                                                           */
/*    Description         : This function handles the reception of Local     */
/*                          Manual Switched event. It happens when a Manual  */
/*                          Switch command is issued by the network admin on */
/*                          an ERPS node. Effectively, this function is an   */
/*                          implementation of Row 9 and Row 65 of the State  */
/*                          Machine                                          */
/*                          which is described in Section 10.1.2 of          */
/*                          ITU-T G.8032/Y.1344 (03/2010).                   */
/*                                                                           */
/*    Input(s)              pRingInfo    - Pointer to ring node for which    */
/*                                         Manual Switch has been issued     */
/*                          pErpsRcvdMsg - This parameter is ignored         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         handles the Local MS Event        */
/*                          OSIX_FAILURE - When a this function failes to    */
/*                                         complete the action.              */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleLocalMS (tErpsRingInfo * pRingInfo,
                           tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT1               u1Status = ERPS_STATUS_BIT_NONE;
    UINT1               u1SwitchCmdPortStatus = 0;
    UINT1               u1FlushStatus = OSIX_FALSE;
    UINT4               u4NonSwitchCmdIfIndex = 0;
    UNUSED_PARAM (pErpsRcvdMsg);

    /* When manual switch command is issued on a given ring port
     * do the following :
     *
     * Unblock the non-requested ring port.
     * If requested ring port is not already blocked
     *        Tx R-APS (MS, DNF)
     * else      
     *        Block Requested port
     *        Tx R-APS(MS)
     *        Flush FDB
     * If the current ring state is PENDING and if node is RPL owner
     *        Stop WTR, STB.
     * Move to MANUAL SWITCH state.
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     * */

    /* Get Requested Port status */
    u1SwitchCmdPortStatus =
        (pRingInfo->u4SwitchCmdIfIndex ==
         pRingInfo->u4Port1IfIndex) ? pRingInfo->u1Port1Status : pRingInfo->
        u1Port2Status;

    /* Get Non Requested port index */
    u4NonSwitchCmdIfIndex =
        (pRingInfo->u4SwitchCmdIfIndex ==
         pRingInfo->u4Port1IfIndex) ? pRingInfo->u4Port2IfIndex : pRingInfo->
        u4Port1IfIndex;

    /* If Ring node is in pending state and if it is RPL Owner node
     * then Stop WTR and WTB */
    if ((pRingInfo->u1RingState == ERPS_RING_PENDING_STATE) &&
        (pRingInfo->u4RplIfIndex != 0))
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
    }

    if (u1SwitchCmdPortStatus & ERPS_PORT_BLOCKING)
    {
        u1Status |= ERPS_DNF_FIELD;

        if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                                  ERPS_PORT_BLOCKING);
        }
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4SwitchCmdIfIndex,
                              ERPS_PORT_BLOCKING);
        u1FlushStatus = OSIX_TRUE;
    }

    /* Unblock non requested ring port */
    ErpsUtilSetPortState (pRingInfo, u4NonSwitchCmdIfIndex,
                          ERPS_PORT_UNBLOCKING);
    /* if other then the RPL port was failed, set RPL unblock */
    if ((0 != pRingInfo->u4RplIfIndex)
        && (pRingInfo->u4SwitchCmdIfIndex != pRingInfo->u4RplIfIndex))
    {
        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus &= ~(ERPS_NODE_STATUS_RB);
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);
    }

    if (ErpsTxStartRapsMessages (pRingInfo, u1Status,
                                 ERPS_MESSAGE_TYPE_MS) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleLocalMS failed to start "
                          "R-APS messages.\r\n");
    }

    if (u1FlushStatus == OSIX_TRUE)
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        u1FlushStatus = OSIX_FALSE;
    }

    RingDynInfo.u1RingState = ERPS_RING_MANUAL_SWITCH_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleWtbExpiry                      */
/*                                                                           */
/*    Description         : Following action will be done in in case RAPS(FS)*/
/*                          or RAPS(MS) message reception:                   */
/*                          o Block the RPL Port and unblock the non RPL Port*/
/*                          o Start transmitting NR-RB messages              */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       WTB expiry is received.             */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         WTB expiry event.                 */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleWtbExpiry (tErpsRingInfo * pRingInfo,
                             tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4RecoveredIfIndex = 0;
    UINT4               u4NonRplIfIndex = 0;
    UINT1               u1StatusBits = ERPS_RB_FIELD;
    UINT1               u1FlushStatus = OSIX_FALSE;

    UNUSED_PARAM (pErpsRcvdMsg);

    ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);

    /*  There is possibility that a RPL Owner itself has a failed port that 
     *  has recovered, so check is any blocked port is present that needs to 
     *  be unblocked.  If RPL port itself was failed and recovered, then 
     *  blocking of RPL port, while handling this event will not make any 
     *  difference. So, no again blocking is required. 
     *  If recovered link port 
     *  is non-RPL but it is one of RPL link port, then block RPL but do not 
     *  unblock recovered link port as it will be unblocked on the reception
     *  of R-APS (NR, RB).
     *
     *  - If recovered port is not any of RPL owner port then 
     *    block the RPL port and unblock non-RPL port.
     *  - If recovered port is on non-RPL owner then do nothing.
     *    as WTR itself would not have started on non-RPL owner node.
     */

    /* Identify the Non RPL Port */
    if (pRingInfo->u4Port1IfIndex != pRingInfo->u4RplIfIndex)
    {
        u4NonRplIfIndex = pRingInfo->u4Port1IfIndex;
    }
    else
    {
        u4NonRplIfIndex = pRingInfo->u4Port2IfIndex;
    }

    /* Identify, If the recovered port is the RPL owner node */
    if (pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING)
    {
        u4RecoveredIfIndex = pRingInfo->u4Port1IfIndex;
    }
    else if (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING)
    {
        u4RecoveredIfIndex = pRingInfo->u4Port2IfIndex;
    }

    /* If the recovered port is the RPL Port, then 
     * a) We  don't need to do the flushing. 
     * b) R-APS (NR, RB) will be send with DNF bit set */

    if (u4RecoveredIfIndex == pRingInfo->u4RplIfIndex)
    {
        u1StatusBits |= ERPS_DNF_FIELD;

        /* When subring is configured without virtual channel, it is mandatory 
         * to inform hardware about the RPL port recovery so that the R-APS channel 
         * will be mapped to this port again
         */
        if (pRingInfo->u1RAPSSubRingWithoutVirtualChannel == OSIX_ENABLED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                                  ERPS_PORT_BLOCKING);
        }

    }
    else
    {
        /* Block the RPL Port */
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplIfIndex,
                              ERPS_PORT_BLOCKING);
        u1FlushStatus = OSIX_TRUE;
    }
    /* Unblock the Non RPL Port */
    ErpsUtilSetPortState (pRingInfo, u4NonRplIfIndex, ERPS_PORT_UNBLOCKING);

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;

    RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    if (ErpsTxStartRapsMessages (pRingInfo, u1StatusBits,
                                 ERPS_MESSAGE_TYPE_NR) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsSmV2RingHandleWtbExpiry failed to start R-APS "
                          "messages.\r\n");
    }
    if (u1FlushStatus == OSIX_TRUE)
    {
        ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        u1FlushStatus = OSIX_FALSE;
    }

    RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;

    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    /* When RPL node is in IDLE condition, it will not receive any RAPS
     * packets from adjacent node, as RPL Neighbor port is configured.
     * so FOP-TO timer will be stopped here */
    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        ErpsTmrStopTimer (pRingInfo, ERPS_TFOP_TMR);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsSmV2RingHandleRAPSNRRB                       */
/*                                                                           */
/*    Description         : Following action will be done in in case NRRB    */
/*                          message reception in Version 2:                  */
/*                          o Unblock non-RPL port in RPL Owner node         */
/*                          o Unblock both the ports in non-RPL node.        */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       NRRB message is received.           */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         NRRB reception.                   */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleRAPSNRRB (tErpsRingInfo * pRingInfo,
                            tErpsRcvdMsg * pErpsRcvdMsg)
{
    unErpsRingDynInfo   RingDynInfo;
    UINT4               u4NonRplPortIfIndex = 0;

    UNUSED_PARAM (pErpsRcvdMsg);

    /* When R-APS NR-RB is received in the ring node 
     * do the following :
     *
     * If the current state is Manual switch or Forced switch
     * of Protection state then move to pending state and FDB flush only.
     *
     * If current state is idle 
     *           Unblock the non-RPL port.
     *           If not RPL owner node 
     *                 Stop Tx R-APS messages.
     *
     * If the current ring state is PENDING 
     *      if node is RPL owner
     *             Stop WTR, STB.
     *      if node is RPL neighbour
     *             Block RPL port
     *             Unblock non-RPL port
     *             Stop Tx R-APS
     *      else
     *            Unblock both ring ports.
     *            Stop Tx R-APS
     *
     * Flush FDB if DNF bit is not set.
     * Move to IDLE state. 
     * [Reference - Table 10-2 of ITU-T G.8032 Y.1344 (03/2010)std]
     * */

    /* If RPL node is in Manual switch state, Forced Switch state, or in 
     * Protection state then move to pending */
    if ((pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE) ||
        (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE) ||
        (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE))
    {

        RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

        return OSIX_SUCCESS;
    }

    /* A port is considered an RPL port if it is connected to the RPL link.
       non-RPL port is the one which is not connected to the RPL Link. */
    if (pRingInfo->u4RplIfIndex != 0)
    {
        if (pRingInfo->u4RplIfIndex == pRingInfo->u4Port1IfIndex)
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port2IfIndex;
        }
        else if (pRingInfo->u4RplIfIndex == pRingInfo->u4Port2IfIndex)
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }

    else if (pRingInfo->u4RplNeighbourIfIndex != 0)
    {
        if (pRingInfo->u4RplNeighbourIfIndex == pRingInfo->u4Port1IfIndex)
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port2IfIndex;
        }
        else if (pRingInfo->u4RplNeighbourIfIndex == pRingInfo->u4Port2IfIndex)
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port1IfIndex;
        }
    }
    else
    {
        /* non RPL node && non RPL neighbor node */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) &&
            ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED))
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port1IfIndex;
        }
        else if ((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) &&
                 ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) !=
                  ERPS_PORT_FAILED))
        {
            u4NonRplPortIfIndex = pRingInfo->u4Port2IfIndex;
        }
    }
    if (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)
    {
        ErpsUtilSetPortState (pRingInfo, u4NonRplPortIfIndex,
                              ERPS_PORT_UNBLOCKING);

        if (pRingInfo->u4RplIfIndex == 0)
        {
            if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleRAPSNRRB failed to stop R-APS "
                                  "messages.\r\n");
            }
        }
        return OSIX_SUCCESS;
    }

    if (pRingInfo->u1RingState == ERPS_RING_PENDING_STATE)
    {
        /* If RPL neighbor, block RPL port and unblock non-RPL port.
           If neither RPL owner nor RPL neighbor, unblock both ports */
        if (pRingInfo->u4RplIfIndex != 0)
        {
            /* In Pending state, RPL Owner stops WTR and WTB timers */
            ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
            ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
        }
        else if (pRingInfo->u4RplNeighbourIfIndex != 0)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4RplNeighbourIfIndex,
                                  ERPS_PORT_BLOCKING);
            ErpsUtilSetPortState (pRingInfo, u4NonRplPortIfIndex,
                                  ERPS_PORT_UNBLOCKING);
            if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleRAPSNRRB failed to stop R-APS "
                                  "messages.\r\n");
            }
        }
        else
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
            if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmV2RingHandleRAPSNRRB failed to stop R-APS "
                                  "messages.\r\n");
            }
        }

        /* If DNF bit is not set flush FDB .
         * The check ( if pending state) ensures that flush does not happen repeatedly
         * on reception of R-APS NR-RB  messages in idle state.   
         */
        if ((pRingInfo->u1FlushLogicSupport == OSIX_DISABLED) &&
            (pErpsRcvdMsg->u1DNFBitStatus == 0))
        {
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        }
    }

    /* Move to idle state */
    RingDynInfo.u1RingState = ERPS_RING_IDLE_STATE;
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : ErpsSmV2RingHandleRAPSNR                         */
/*                                                                           */
/*    Description         : Following action will be done in in case NRRB    */
/*                          message reception in Version 2                   */
/*                          o Start WTR timer,                               */
/*                          o If it is non-revertive mode and NR msg received*/
/*                            if any port is blocked and received node       */
/*                            priority is better than node's. then unblock,  */
/*                            blocked port.                                  */
/*                                                                           */
/*    Input(s)              pRingInfo - Pointer to ring node for which a     */
/*                                       NR   message is received.           */
/*                          pErpsRcvdMsg - Pointer to global data having     */
/*                                         ERPS received msg information.    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         completed action to handle the    */
/*                                         NR   reception.                   */
/*                          OSIX_FAILURE - When a this function failed to    */
/*                                         complete action.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsSmV2RingHandleRAPSNR (tErpsRingInfo * pRingInfo,
                          tErpsRcvdMsg * pErpsRcvdMsg)
{
    tErpsContextInfo   *pErpsContextInfo = NULL;
    unErpsRingDynInfo   RingDynInfo;

    pErpsContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    if (pErpsContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* When ring is non-revertive mode of operation and there is no failure
     * present in the node links, but one of the ring port still in blocked
     * state, determines that there was a failure in one of the ring link,
     * recovered but not opened up.
     * If this is the case and NR msg is received, so compare the nodeid and 
     * check if port has to unblock.
     *
     * There is possibility that blocked port is present in the RPL Owner
     * node */

    if (pRingInfo->u1RingState == ERPS_RING_PROTECTION_STATE)
    {
        if ((pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE) &&
            (pRingInfo->u4RplIfIndex != 0))
        {
            /* In revertive mode, on reception of NR Message in RPL owner node,
             * start WTR timer */
            if (pRingInfo->u4WTRTimerValue == 0)
            {

                /* Before calling the expiry event, the ring state should
                 * be moved to pending.
                 */
                RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_STATE);

                /* If WTR timer value is zero, immediatly revert */
                if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                 ERPS_RING_WTR_EXP_REQUEST,
                                                 NULL) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsTmrWTRTimerExp returned failure"
                                      " from Priority Request handling.\r\n");
                }
            }
            else
            {
                ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                                   pRingInfo->u4WTRTimerValue);
            }
        }
    }
    else if ((pRingInfo->u1RingState == ERPS_RING_MANUAL_SWITCH_STATE) ||
             (pRingInfo->u1RingState == ERPS_RING_FORCED_SWITCH_STATE))
    {

        /* when SF is simulated on a link, ring moves to protection state.
         * if FS command is issued on another node or in same node,it overrides 
         * SF and ring moves to forced switch state. if FS clear is given now, 
         * the ring will move to idle state upon expiry of wtb timer 
         * even though SF still exists. this situation will lead to ring 
         * segmentation. to avoid this, a proprietary approach is done so  
         * that if fs is given over SF,the state machine           
         * is triggered with local SF request and the ring will move to           
         * protection state. This proprietary implementation will work           
         * only if u1proprietaryclearfs is set to true  
         * Node which is receiving RAPS_NR has Local SF on one of the ring  
         * ports,so trigger the LOCAL_SF to avoid the ring segmentation */

        if ((pErpsContextInfo->u1ProprietaryClearFS == ERPS_SNMP_TRUE) &&
            ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
             ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
              (pRingInfo->u4Port2IfIndex != 0))))
        {
            if ((pRingInfo->u4Port1IfIndex ==
                 pRingInfo->u4RplNextNeighbourIfIndex)
                && (pRingInfo->u1Port1Status & ERPS_PORT_FAILED))
            {
                pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_1;

            }
            if ((pRingInfo->u4Port2IfIndex ==
                 pRingInfo->u4RplNextNeighbourIfIndex)
                && (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
            {
                pRingInfo->u1RingTxAction = ERPS_TX_RAPS_ON_RING_PORT_2;

            }

            RingDynInfo.u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_PRIO_REQUEST);

            RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_STATE);

            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_LOCAL_SF_REQUEST,
                                             pErpsRcvdMsg) != OSIX_SUCCESS)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsSmCalTopPriorityRequest Called for "
                                  "Local SF request returned failure.\r\n");
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }

        if ((pRingInfo->u1OperatingMode == ERPS_RING_REVERTIVE_MODE) &&
            (pRingInfo->u4RplIfIndex != 0))
        {
            /* In revertive mode, on reception of NR Message in RPL owner node,
             * start WTB timer */
            if (pRingInfo->u4WTBTimerValue == 0)
            {
                RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_STATE);

                /* If WTB timer value is zero, immediatly revert */
                if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                                 ERPS_RING_WTB_EXP_REQUEST,
                                                 NULL) == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "ErpsTmrWTBTimerExp returned failure"
                                      " from Priority Request handling.\r\n");
                }

                return OSIX_SUCCESS;
            }
            else
            {
                ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR,
                                   pRingInfo->u4WTBTimerValue);
            }
        }
    }

    /* The Node Id comparison has to be done for two cases
     * 1. When the ERPS compatible version is 1, and when the mode of operation is 
     *    non-revertive.
     * 2. When the ERPS compatible version is 2,and when the mode of operation is 
     *    revertive in a non-RPL owner or non-revertive. 
     *    [Reference : As per ITU-T G.8032/Y.1344 (03/2010) standard ]
     * 
     * The following If condition  ensures that the above 2 cases are taken care.
     *
     * Because of the Node Id comparsion done in both modes of operation for Version 2.
     * it is sufficient if the appropriate mode is configured only in the RPL owner.
     */

    else if ((pRingInfo->u1RingState == ERPS_RING_PENDING_STATE) ||
             ((pRingInfo->u1RingState == ERPS_RING_IDLE_STATE) &&
              (pRingInfo->u4RplIfIndex == 0)
              && (pRingInfo->u4RplNeighbourIfIndex == 0)))

    {
        /* If ERPS version is v2 and if the ring nodeid is greater than received nodeid,
         * then no action is required */
        if ((MEMCMP (&(ERPS_NODE_ID (pRingInfo->u4ContextId)),
                     &(pErpsRcvdMsg->RcvdNodeId), sizeof (tMacAddr))) > 0)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              " ErpsSmV2RingHandleRAPSNR : EXIT :The Received nodeid "
                              "%02d:%02d:%02d:%02d:%02d:%02d"
                              "is lesser than ring node id "
                              "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                              pErpsRcvdMsg->RcvdNodeId[1],
                              pErpsRcvdMsg->RcvdNodeId[2],
                              pErpsRcvdMsg->RcvdNodeId[3],
                              pErpsRcvdMsg->RcvdNodeId[4],
                              pErpsRcvdMsg->RcvdNodeId[5]);

            return OSIX_SUCCESS;
        }

        /* If the ring node has received its own R-APS NR message , and if both the ring ports 
         *  of the node are in blocked state (This condition will occur, when both the ring ports of 
         *  the same ring node have failed and recovered simultaneously), then one of the ring ports
         *  (Here it is always ring port1 ) of the ring is moved to unblocked state */

        if ((MEMCMP (&(ERPS_NODE_ID (pRingInfo->u4ContextId)),
                     &(pErpsRcvdMsg->RcvdNodeId), sizeof (tMacAddr))) == 0)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              " ErpsSmV2RingHandleRAPSNR : The Received nodeid %02d:%02d:%02d:%02d:%02d:%02d"
                              " is equal to the ring node id "
                              "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                              pErpsRcvdMsg->RcvdNodeId[1],
                              pErpsRcvdMsg->RcvdNodeId[2],
                              pErpsRcvdMsg->RcvdNodeId[3],
                              pErpsRcvdMsg->RcvdNodeId[4],
                              pErpsRcvdMsg->RcvdNodeId[5]);

            if ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) &&
                (pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING))
            {
                ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                      ERPS_PORT_UNBLOCKING);
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmV2RingHandleRAPSNR : Ring Port1 is moved to unblocking"
                                  " state when the R-APS NR is received from self \r\n");

            }
            else
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  " ErpsSmV2RingHandleRAPSNR : EXIT :The Received nodeid"
                                  " %02d:%02d:%02d:%02d:%02d:%02d is equal to the  ring"
                                  "node id, but one of the ports is in unblocked state "
                                  "\r\n", pErpsRcvdMsg->RcvdNodeId[0],
                                  pErpsRcvdMsg->RcvdNodeId[1],
                                  pErpsRcvdMsg->RcvdNodeId[2],
                                  pErpsRcvdMsg->RcvdNodeId[3],
                                  pErpsRcvdMsg->RcvdNodeId[4],
                                  pErpsRcvdMsg->RcvdNodeId[5]);
                return OSIX_SUCCESS;
            }

            return OSIX_SUCCESS;
        }

        /* Unblocking Non-failed ports */
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) != ERPS_PORT_FAILED)
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
        }

        if (ErpsTxStopRapsMessages (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsSmV2RingHandleNRMsg failed to stop R-APS "
                              "messages.\r\n");
        }

    }
    if ((pRingInfo->u1RingState != ERPS_RING_PENDING_STATE) &&
        (pRingInfo->u1RingState != ERPS_RING_IDLE_STATE))
    {
        RingDynInfo.u1RingState = ERPS_RING_PENDING_STATE;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_STATE);
    }

    return OSIX_SUCCESS;
}
