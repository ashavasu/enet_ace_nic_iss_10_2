/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsz.c,v 1.5 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ERPSSZ_C
#include "erpsinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
ErpsSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ERPS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsERPSSizingParams[i4SizingId].u4StructSize,
                              FsERPSSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(ERPSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            ErpsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
ErpsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsERPSSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ERPSMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
ErpsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ERPS_MAX_SIZING_ID; i4SizingId++)
    {
        if (ERPSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ERPSMemPoolIds[i4SizingId]);
            ERPSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
