/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    
 *                                                                         
 * $Id: erpsred.c,v 1.26 2016/08/24 10:05:02 siva Exp $                    
 *                                                                         
 * Description: This file contains the utility functions for ERPS          
 *              redundancy.                                                
 ***************************************************************************/
#include "erpsinc.h"

/****************************************************************************
 *                           erpsred.c prototypes                           *
 ****************************************************************************/

PRIVATE VOID        ErpsRedApplySwitchCommand (tErpsRingInfo * pRingInfo);
PRIVATE VOID        ErpsRedProcessDeleteAllRingMsg (tRmMsg * pMsg,
                                                    UINT2 *pu2OffSet);
PRIVATE VOID        ErpsRedProcessModuleStatusMsg (tRmMsg * pMsg,
                                                   UINT2 *pu2OffSet,
                                                   UINT1 u1MsgType);
PRIVATE VOID        ErpsRedProcessRingSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet,
                                               UINT2 u2RingDynInfoSize);
PRIVATE VOID        ErpsRedUpdateTimerStatus (tErpsRingInfo * pRingInfo,
                                              UINT4 u4NewNodeStatus);
PRIVATE VOID        ErpsRedProcessRemainingTmrVal (tRmMsg * pMsg,
                                                   UINT2 *pu2OffSet);
PRIVATE VOID        ErpsRedHandleGoActive (VOID);
PRIVATE VOID        ErpsRedHandleGoStandby (VOID);
PRIVATE VOID        ErpsRedHandleStandbyToActive (VOID);
PRIVATE VOID        ErpsRedHandleActiveToStandby (VOID);
PRIVATE VOID        ErpsRedHandleIdleToStandby (VOID);
PRIVATE VOID        ErpsRedHandleIdleToActive (VOID);
PRIVATE VOID        ErpsRedSendBulkRequest (VOID);
PRIVATE VOID        ErpsRedHandleBulkRequest (VOID);
PRIVATE VOID        ErpsRedSendBulkUpdTailMsg (VOID);
PRIVATE VOID        ErpsRedProcessBulkTailMsg (VOID);
PRIVATE VOID        ErpsRedProcessPeerMsgAtActive (tRmMsg * pMsg,
                                                   UINT2 u2DataLen);
PRIVATE VOID        ErpsRedProcessPeerMsgAtStandby (tRmMsg * pMsg,
                                                    UINT2 u2DataLen);
PRIVATE VOID        ErpsRedHwAuditAndUpdateRingInfo (VOID);
PRIVATE INT4        ErpsRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length);
PRIVATE VOID        ErpsRedAddAllNodeInDbTbl (VOID);
PRIVATE VOID        ErpsRedSyncRemainingTimerValue (VOID);
PRIVATE VOID
       ErpsRedSyncModuleStatusMsg (UINT4 u4ContextId, UINT1 u1ModuleStatus);
PRIVATE tRmMsg     *ErpsRedGetMsgBuffer (UINT2 u2BufSize);
PRIVATE VOID        ErpsRedUpdateRingHwHandle (tErpsHwRingInfo *
                                               pErpsHwRingInfo);
static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize ring dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pErpsRingDynDataDesc = NULL;

    pErpsRingDynDataDesc =
        &(gErpsGlobalInfo.aErpsDynDataDescList[ERPS_RING_DYN_INFO]);

    pErpsRingDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption: 
     * All dynamic info in data structure are placed after the DbNode.
     */
    /*
     * Size of the dynamic info = 
     * Total size of structure - offset of first dynamic info
     */

    pErpsRingDynDataDesc->u4DbDataSize =
        sizeof (tErpsRingInfo) - FSAP_OFFSETOF (tErpsRingInfo, u4ContextId);

    pErpsRingDynDataDesc->pDbDataOffsetTbl = gaErpsRingOffsetTbl;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Erps descriptor    */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDescrTblInit (VOID)
{
    tDbDescrParams      ErpsDescrParams;
    MEMSET (&ErpsDescrParams, 0, sizeof (tDbDescrParams));

    ErpsDescrParams.u4ModuleId = RM_ERPS_APP_ID;

    ErpsDescrParams.pDbDataDescList = gErpsGlobalInfo.aErpsDynDataDescList;

    DbUtilTblInit (&gErpsGlobalInfo.ErpsDynInfoList, &ErpsDescrParams);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbNodeInit                               */
/*                                                                           */
/*    Description         : This function will initialize Erps ring db node. */
/*                                                                           */
/*    Input(s)            : pRingInfo - pointer to ring entry.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbNodeInit (tErpsRingInfo * pRingInfo)
{
    DbUtilNodeInit (&pRingInfo->RingDbNode, ERPS_RING_DYN_INFO);

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register ERPS with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ErpsRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;
    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_ERPS_APP_ID;

    RmRegParams.pFnRcvPkt = ErpsRedRmCallBack;

    if (ErpsPortRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC
            ("ErpsRedInitGlobalInfo failed to register with RM\r\n");

        return OSIX_FAILURE;
    }

    MEMSET (&gErpsGlobalInfo.ErpsRedGlobalInfo, 0, sizeof (tErpsRedGlobalInfo));

    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus = RM_INIT;
    gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    ERPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;

    /* In Initialization, Block the NpSync Sending in Active
     * and receiving in StandBy until,  Standby becomes capable of
     * receiving Np Sync messages.  
     *      - let the standby completes bulk update. i.e. allow
     *      NpSync only when standby receives Tail message from
     *      Active node.
     *      - In Active node, allow NpSync send only when Active node
     *      send Tail message to Standby.
     */

    ErpsRedHwAuditIncBlkCounter ();
    return OSIX_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedDeInitGlobalInfo                              */
/*                                                                         */
/* DESCRIPTION      : Deinitializes redundancy global variables.           */
/*                    This function will deregisters ERPS with RM.         */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedDeInitGlobalInfo (VOID)
{
    if (ErpsPortRmDeRegisterProtocols () != OSIX_SUCCESS)
    {
        ERPS_GLOBAL_TRC
            ("ErpsRedDeInitGlobalInfo DeRegistration with RM failed.\r\n");
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedRmCallBack                                    */
/*                                                                         */
/* DESCRIPTION      : This function will be invoked by the RM module to    */
/*                    enqueue events and post messages to ERPS module.     */
/*                                                                         */
/* INPUT            : u1Event - Event given by RM module.                  */
/*                    pData   - Msg to be enqueued, if u1Event is          */
/*                              valid.                                     */
/*                    u2DataLen - Size of the update message.              */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                         */
/***************************************************************************/

PUBLIC INT4
ErpsRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tErpsQMsg          *pMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send these events to ERPS task. */

        ERPS_GLOBAL_TRC ("ErpsRedRmCallBack MSG is NULL\r\n");

        return OSIX_FAILURE;
    }

    if ((pMsg = (tErpsQMsg *) MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId))
        == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedRmCallBack Queue Message memory "
                         "allocation FAILED !!!\r\n");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tErpsQMsg));

    pMsg->u4MsgType = ERPS_RM_MSG;
    pMsg->unMsgParam.RmMsg.pData = pData;
    pMsg->unMsgParam.RmMsg.u1Event = u1Event;
    pMsg->unMsgParam.RmMsg.u2DataLen = u2DataLen;

    if (ErpsMsgEnque (pMsg) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsRedRmCallBack Queue Message memory "
                         "allocation FAILED !!!\r\n");

        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleRmEvents                                */
/*                                                                         */
/* DESCRIPTION      : This function process all the events and messages    */
/*                    from RM module.                                      */
/*                                                                         */
/* INPUT            : pMsg - pointer to ERPS Queue message.                */
/*                                                                         */
/* OUTPUT           : None.                                                */
/*                                                                         */
/* RETURNS          : None.                                                */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedHandleRmEvents (tErpsQMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    switch (pMsg->unMsgParam.RmMsg.u1Event)
    {
        case GO_ACTIVE:
            ErpsRedHandleGoActive ();
            break;
        case GO_STANDBY:
            ErpsRedHandleGoStandby ();
            break;
        case RM_STANDBY_UP:

            pData = (tRmNodeInfo *) pMsg->unMsgParam.RmMsg.pData;

            if (pMsg->unMsgParam.RmMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }

            gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp =
                pData->u1NumStandby;

            ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);

            /* Before the arrival of STANDBY_UP event, Bulk Request has
             * arrived from the Standby node. Send the Bulk updates Now.
             */

            if (gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
            {
                gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                ErpsRedHandleBulkRequest ();
            }

            break;
        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsg->unMsgParam.RmMsg.pData;

            if (pMsg->unMsgParam.RmMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }

            gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp =
                pData->u1NumStandby;

            ErpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);

            break;
        case RM_CONFIG_RESTORE_COMPLETE:

            MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

            ProtoEvt.u4AppId = RM_ERPS_APP_ID;
            ProtoEvt.u4Error = RM_NONE;

            if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_INIT)
            {
                if (ErpsPortRmGetNodeState () == RM_STANDBY)
                {
                    ErpsRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) ==
                        OSIX_FAILURE)
                    {
                        ERPS_GLOBAL_TRC ("ErpsRedHandleRmEvents Acknowledgement"
                                         "to RM for RM_CONFIG_RESTORE_COMPLETE"
                                         "event failed\r\n");
                    }
                }
            }

            break;
        case L2_INITIATE_BULK_UPDATES:
            ErpsRedSendBulkRequest ();
            break;
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->unMsgParam.RmMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->unMsgParam.RmMsg.pData,
                                 pMsg->unMsgParam.RmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_ERPS_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {

                ErpsRedProcessPeerMsgAtActive (pMsg->unMsgParam.RmMsg.pData,
                                               pMsg->unMsgParam.RmMsg.
                                               u2DataLen);

            }
            else if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus
                     == RM_STANDBY)
            {
                ErpsRedProcessPeerMsgAtStandby (pMsg->unMsgParam.RmMsg.pData,
                                                pMsg->unMsgParam.RmMsg.
                                                u2DataLen);

            }
            else
            {
                ERPS_GLOBAL_TRC ("ErpsRedHandleRmEvents message from peer node "
                                 "received at IDLE node\r\n");

            }

            RM_FREE (pMsg->unMsgParam.RmMsg.pData);

            /* Sending ACK to RM */
            ErpsPortRmApiSendProtoAckToRM (&ProtoAck);

            break;
        default:
            ERPS_GLOBAL_TRC
                ("ErpsRedHandleRmEvents Invalid event received\r\n");
            break;
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleGoActive                                */
/*                                                                         */
/* DESCRIPTION      : This routine handles the GO_ACTIVE event and responds*/
/*                    to RM with an acknowledgement.                       */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        return;
    }

    ProtoEvt.u4AppId = RM_ERPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus)
    {
        case RM_INIT:
            ErpsRedHandleIdleToActive ();
            ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            break;
        case RM_STANDBY:
            ErpsRedHandleStandbyToActive ();
            ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            break;
        default:
            break;
    }

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
        gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        ErpsRedHandleBulkRequest ();
    }

    if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsRedHandleGoActive Acknowledgement to RM for"
                         "GO_ACTIVE event failed\r\n");
    }

    ErpsRedHwAudCrtNpSyncBufferTable ();

    return;
}

/****************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleGoStandby                                */
/*                                                                          */
/* DESCRIPTION      : This routine handles the GO_STANDBY event and responds*/
/*                    to RM with an acknowledgement.                        */
/*                                                                          */
/* INPUT            : None                                                  */
/*                                                                          */
/* OUTPUT           : None                                                  */
/*                                                                          */
/* RETURNS          : None                                                  */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
ErpsRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ERPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        return;
    }

    /* GO_STANDBY event for IDLE node, is not handled here.
     * It is done when CONFIG_RESTORE_COMPLETE event is received. 
     * Since static bulk update will be completed only by then, 
     * the acknowledgement can be sent during CONFIG_RESTORE_COMPLETE
     * handling, which will trigger RM to send dynamic bulk update 
     * event to modules.
     */

    ErpsRedHwAudCrtNpSyncBufferTable ();

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus = RM_STANDBY;

        ErpsRedHandleActiveToStandby ();

        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {

            ERPS_GLOBAL_TRC ("ErpsRedHandleGoStandby bulk request ACK to RM"
                             "failed.\r\n");
        }
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleStandbyToActive                         */
/*                                                                         */
/* DESCRIPTION      : On Standby to Active the following actions are       */
/*                    preformed.                                           */
/*                     - Update the Node Status and standby node count     */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedHandleStandbyToActive (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1RapsMessageType = 0;
    UINT1               u1StatusBits = 0;
    INT4                i4RetVal = 0;

    /* NOTE: ERPS Lock should not be released before completion of 
     * all the switchover activities of StandbyToActive process.
     * As reception of RAPS message may change the ring info,
     * and applying recovery process may overwrite the current state
     * with old ring infos.
     */

    /* Step I:
     * Walk Ring table, for each ring ports, get the link status, if 
     * link status different from the current ERPS port state.
     * Apply current link state.
     */
    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus = RM_ACTIVE;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pRingInfo)
    {
        /* If RowStatus is not active, do Hw Audit and apply Sw
         * Port state to Hardware.
         */

        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            /* If mismatch detected in actual Link state and the ERPS port
             * state, then this function will update the hardware 
             * with proper port state and at same time, it will 
             * currect the hardware state also. (i.e. Hw audit node
             * will not be present for ring ports, after correcting the 
             * link state.)
             */

            i4RetVal = ErpsUtilRegisterGetFltAndApply (pRingInfo);

        }

        /* If periodic timer is running send 3 messages and start periodic
         * timer.
         */
        if ((pRingInfo->u4RingNodeStatus & ERPS_PERIODIC_TIMER_RUNNING) ==
            ERPS_PERIODIC_TIMER_RUNNING)
        {
            u1RapsMessageType = (UINT1) (pRingInfo->u2LastMsgSend >> 12);
            u1StatusBits = (UINT1) ((pRingInfo->u2LastMsgSend) & 0x00FF);

            i4RetVal = ErpsTxStartRapsMessages (pRingInfo, u1StatusBits,
                                                u1RapsMessageType);

            u1RapsMessageType = 0;
            u1StatusBits = 0;
        }
        /* When FOP-TO defect is encountered in active node and force
         * switch over is given New active node should come up with 
         * FOP-TO defect trap, So timer is started in standby node
         * when feature is enabled.*/
        if ((pRingInfo->bDFOP_TO == OSIX_ENABLED) &&
            (pRingInfo->u1RowStatus == ACTIVE) &&
            (pRingInfo->u4PeriodicTimerValue != ERPS_INIT_VAL))

        {
            if (!((pRingInfo->u4RplIfIndex != 0) &&
                  (pRingInfo->u1RingState == ERPS_RING_IDLE_STATE)))
            {
                ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                                   (((UINT4) (pRingInfo->f4KValue)) *
                                    (pRingInfo->u4PeriodicTimerValue)));
            }
        }

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    /* Step II:
     * Do the Hw audit and take the corrective measure.
     */
    /* Most likely Hw table will have only the last updated ring node.
     * therefore it will be most iffective to take the action for the
     * ring node present in the Hw Audit table than check for all the 
     * ring node, whether it is present in the Hw Audit table.
     */

    ErpsRedHwAuditAndUpdateRingInfo ();

    /* If Standby was in the position to handle the NpSync. Block it, when
     * it becomes Active node.
     */

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount == 0)
    {
        ErpsRedHwAuditIncBlkCounter ();
    }

    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp =
        ErpsPortRmGetStandbyNodeCount ();

    UNUSED_PARAM (i4RetVal);
    return;
}

/****************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleActiveToStandby                          */
/*                                                                          */
/* DESCRIPTION      : On Active to Standby transition, the following actions*/
/*                    are performed,                                        */
/*                    1. Update the Node Status                             */
/*                    2. Disable and enable the module                      */
/*                                                                          */
/* INPUT            : None                                                  */
/*                                                                          */
/* OUTPUT           : None                                                  */
/*                                                                          */
/* RETURNS          : None                                                  */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
ErpsRedHandleActiveToStandby (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;
    /* If Active node was in the position to Send NpSync. Block it, when
     * it becomes Standby node.
     */

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount == 0)
    {
        ErpsRedHwAuditIncBlkCounter ();
    }

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pRingInfo != NULL)
    {
        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) == OSIX_TRUE) &&
            (pRingInfo->u1RowStatus == ACTIVE))
        {
            /* Stop running periodic timer. */
            ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR);
        }

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleIdleToStandby                           */
/*                                                                         */
/* DESCRIPTION      : This routine updates the node status.                */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedHandleIdleToStandby (VOID)
{
    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp = 0;

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleIdleToActive                              */
/*                                                                           */
/* DESCRIPTION      : This routine updates the node status on transition from*/
/*                    Idle to Active.                                        */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
ErpsRedHandleIdleToActive (VOID)
{
    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp =
        ErpsPortRmGetStandbyNodeCount ();

    gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus = RM_ACTIVE;

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSendBulkRequest                               */
/*                                                                         */
/* DESCRIPTION      : This routine sends bulk request from standby node to */
/*                    active node.                                         */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;

    pMsg = ErpsRedGetMsgBuffer (ERPS_RED_BULK_REQ_SIZE);

    if (pMsg == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedSendBulkRequest Memory allocation"
                         " from RM failed\r\n");

        return;
    }

    ERPS_RM_PUT_1_BYTE (pMsg, &u2Offset, ERPS_RED_BULK_REQUEST_MSG);

    ERPS_RM_PUT_2_BYTE (pMsg, &u2Offset, ERPS_RED_BULK_REQ_SIZE);

    ErpsRedSendMsgToRm (pMsg, u2Offset);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHandleBulkRequest                             */
/*                                                                         */
/* DESCRIPTION      : This routine handles bulk request from standby node. */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                    u2DataLen - Length of data in buffer.                */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedHandleBulkRequest (VOID)
{
    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp == 0)
    {
        ErpsPortRmSetBulkUpdatesStatus (RM_ERPS_APP_ID);

        ErpsRedSendBulkUpdTailMsg ();

        return;
    }

    ErpsRedAddAllNodeInDbTbl ();

    ErpsRedSyncRingDynInfo ();

    /* Timer will be synced up, only after syncing ring dynamic info, 
     * as starting timer in standby could be invalid, ring is not in state
     * to start the timer.
     */

    ErpsRedSyncRemainingTimerValue ();

    ErpsPortRmSetBulkUpdatesStatus (RM_ERPS_APP_ID);

    ErpsRedSendBulkUpdTailMsg ();

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSendBulkUpdTailMsg                            */
/*                                                                         */
/* DESCRIPTION      : This routine handles bulk tail message to active node*/
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;

    pMsg = ErpsRedGetMsgBuffer (ERPS_RED_BULK_UPDT_TAIL_SIZE);

    if (pMsg == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedSendBulkUpdTailMsg Memory allocation"
                         " from RM failed\r\n");

        return;
    }

    ERPS_RM_PUT_1_BYTE (pMsg, &u2Offset, ERPS_RED_BULK_UPDT_TAIL_MSG);

    ERPS_RM_PUT_2_BYTE (pMsg, &u2Offset, ERPS_RED_BULK_UPDT_TAIL_SIZE);

    ErpsRedSendMsgToRm (pMsg, u2Offset);

    ErpsRedHwAuditDecBlkCounter ();

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessBulkTailMsg                            */
/*                                                                         */
/* DESCRIPTION      : This routine process the bulk update tail message.   */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                    u2DataLen - Length of data in buffer.                */
/*                                                                         */
/* OUTPUT           : pu2Offset - Offset Value                             */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessBulkTailMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;

    /* Since Bulk updated is complted in Standby, alllow updating Np Sync
     * Buffer.
     */
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_ERPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsRedProcessBulkTailMsg Bulk update completion "
                         "indication to RM failed. \r\n");

    }

    ErpsRedHwAuditDecBlkCounter ();
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAuditIncBlkCounter                            */
/*                                                                           */
/* DESCRIPTION      : This routine increments the NP Sync Block Counter.     */
/*                    If the value of this Block flag is not equal to 0, then*/
/*                    it is intended to block NP Sync-ups.                   */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ErpsRedHwAuditIncBlkCounter (VOID)
{
    if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount < 0)
    {
        ERPS_GLOBAL_TRC ("ErpsRedHwAuditIncBlkCounter NP Sync count is "
                         "less than 0\r\n");
    }

    gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount++;

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAuditDecBlkCounter                          */
/*                                                                         */
/* DESCRIPTION      : This routine decrements the NP Sync Block Counter.   */
/*                    If the value of this Block flag is not equal to 0.   */
/*                                                                         */
/* INPUT            : None                                                 */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedHwAuditDecBlkCounter (VOID)
{
    if (gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount < 0)
    {
        ERPS_GLOBAL_TRC ("ErpsRedHwAuditDecBlkCounter NP Sync count is "
                         "less than 0\r\n");

    }

    gErpsGlobalInfo.ErpsRedGlobalInfo.i1NpSyncBlockCount--;

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessPeerMsgAtActive                        */
/*                                                                         */
/* DESCRIPTION      : This routine process the peer standby messages at    */
/*                    active node.                                         */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                    u2DataLen - Length of data in buffer.                */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{

    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ERPS_APP_ID;

    ERPS_RM_GET_1_BYTE (pMsg, &u2Offset, u1MsgType);
    ERPS_RM_GET_2_BYTE (pMsg, &u2Offset, u2Length);

    if (u2Offset != u2DataLen)
    {
        /* The only RM packet expected to be processed at active node is 
         * Bulk Request message which has only Type and length. 
         * Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ERPS_GLOBAL_TRC ("ErpsRedProcessPeerMsgAtActive Acknowledgement to"
                             "RM for length mismatch failed\r\n");

        }

        return;
    }

    /*if (u2DataLen != ERPS_RED_BULK_REQ_SIZE)
       {
       ProtoEvt.u4Error = RM_PROCESS_FAIL;

       if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
       {
       ERPS_GLOBAL_TRC ("ErpsRedProcessPeerMsgAtActive Acknowledgement to"
       "RM for invalid length failed\r\n");

       }

       return;
       } */

    if (u1MsgType == ERPS_RED_BULK_REQUEST_MSG)
    {
        if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp == 0)
        {
            /* Before the arrival of STANDBY_UP or GO_ACTIVE event, 
             * Bulk Request has arrived from the Standby node. 
             * Wait for STANDBY_UP or GO_ACTIVE event.
             */

            gErpsGlobalInfo.ErpsRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
        }
        else
        {
            ErpsRedHandleBulkRequest ();
        }
    }

    /* HITLESS RESTART */
    if (u1MsgType == ERPS_HR_STDY_ST_PKT_REQ)
    {
        ERPS_GLOBAL_TRC ("ERPS: Received Steady State "
                         "Pkt Request message. \n");
        ErpsRedHRProcStdyStPktReq ();
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessPeerMsgAtStandby                       */
/*                                                                         */
/* DESCRIPTION      : This routine process the peer active node messages at*/
/*                    standby node.                                        */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                    u2DataLen - Length of data in buffer.                */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = 0;
    UINT2               u2MinLen = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT1               u1MsgType = 0;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ERPS_APP_ID;

    u2MinLen = ERPS_RED_PKT_TYPE_FIELD_SIZE + ERPS_RED_MIN_LENGTH_FIELD_SIZE;

    while (u2Offset <= u2DataLen)
    {
        u2ExtractMsgLen = u2Offset;

        ERPS_RM_GET_1_BYTE (pMsg, &u2Offset, u1MsgType);
        ERPS_RM_GET_2_BYTE (pMsg, &u2Offset, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u2Offset += u2Length;
            continue;
        }

        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen += u2Length;

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
            {
                ERPS_GLOBAL_TRC ("ErpsRedProcessPeerMsgAtStandby"
                                 " Acknowledgement to RM for length mismatch"
                                 "failed\r\n");

            }
            ERPS_GLOBAL_TRC ("ErpsRedProcessPeerMsgAtStandby"
                             " Wrong length message received \r\n");

            return;
        }

        switch (u1MsgType)
        {
            case ERPS_RED_DELETE_ALL_RING:
                ErpsRedProcessDeleteAllRingMsg (pMsg, &u2Offset);
                break;
            case ERPS_RED_ENABLE_RING_MSG:
            case ERPS_RED_DISABLE_RING_MSG:
                ErpsRedProcessModuleStatusMsg (pMsg, &u2Offset, u1MsgType);
                break;
            case ERPS_RED_TIMER_REM_VALUE:
                ErpsRedProcessRemainingTmrVal (pMsg, &u2Offset);
                break;
            case ERPS_RING_DYN_INFO:
                ErpsRedProcessRingSyncMsg (pMsg, &u2Offset, u2Length);
                break;
            case ERPS_RED_NPSYNC_MSG:
                ErpsSyncNpProcessSyncMsg (pMsg, &u2Offset);
                break;
            case ERPS_RED_BULK_UPDT_TAIL_MSG:
                ErpsRedProcessBulkTailMsg ();
                break;
            default:
                break;
        }

        if (u2Offset == u2DataLen)
        {
            /* All data extracted from the buffer */
            break;
        }
    }

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAuditAndUpdateRingInfo                        */
/*                                                                           */
/* DESCRIPTION      : This routine will perform the hardware audit function, */
/*                    If any NpSyn node is present in the database.          */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
ErpsRedHwAuditAndUpdateRingInfo (VOID)
{
    tErpsRedNpSyncEntry *pBuf = NULL;
    tErpsRedNpSyncEntry *pTempBuf = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsHwRingInfo    *pErpsHwRingInfo = NULL;
    tErpsCfmMepInfo     MepInfo;
    tErpsRingInfo       RingInfo;

    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));

    if (TMO_SLL_Count (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable) == 0)
    {
        return;
    }

    TMO_DYN_SLL_Scan (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tErpsRedNpSyncEntry *)
    {
        pErpsHwRingInfo = &(pBuf->unNpData.FsErpsHwRingConfig.ErpsHwRingInfo);

        /* Ring will not be present in the software, if ring creation failed */
        if (pErpsHwRingInfo->u1RingAction != ERPS_HW_RING_CREATE)
        {
            MEMSET (&RingInfo, 0, sizeof (RingInfo));

            RingInfo.u4ContextId = pErpsHwRingInfo->u4ContextId;
            RingInfo.u4RingId = pErpsHwRingInfo->u4RingId;

            pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

            if (pRingInfo == NULL)
            {
                ERPS_GLOBAL_TRC ("ErpsRedHwAuditAndUpdateRingInfo ring"
                                 " does not exist for the node present in"
                                 " the Hw Audit table\r\n");

                continue;
            }

            if (pRingInfo->u1RowStatus != ACTIVE)
            {
                /* if ring is not active no hardware action is required. */
                continue;
            }
        }

        /* If Hw node port state is in UnBlock state, but Sw port state
         * is Block state.
         * Check if Switch Command was applied, Unblock port in Sw and apply 
         * Switch Command again.
         */

        switch (pErpsHwRingInfo->u1RingAction)
        {
            case ERPS_HW_RING_CREATE:
                pErpsHwRingInfo->u1RingAction = ERPS_HW_RING_DELETE;
#ifdef NPAPI_WANTED
                if (ErpsFsErpsHwRingConfig (pErpsHwRingInfo) == FNP_FAILURE)
                {
                    ERPS_GLOBAL_TRC ("ErpsRedHwAuditAndUpdateRingInfo"
                                     " Np ring create action found.\r\n");

                }
#endif
                break;
            case ERPS_HW_RING_DELETE:
                if (ErpsUtilConfRingEntryInHw (pRingInfo, ERPS_HW_RING_CREATE,
                                               PORT1_PORT2_UPDATED)
                    == OSIX_FAILURE)
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo "
                                      " Np ring delete action found. \r\n");
                }

                break;
            case ERPS_HW_RING_MODIFY:
                if ((pErpsHwRingInfo->u1Port1Action ==
                     ERPS_PORT_STATE_UNBLOCKING) &&
                    ((pRingInfo->u1Port1Status & ERPS_PORT_BLOCKING) ==
                     ERPS_PORT_BLOCKING))
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo "
                                      " Ring Port1 - Np state UNBLOCK,"
                                      " SW state BLOCK \r\n");

                    if (pRingInfo->u4Port1IfIndex ==
                        pRingInfo->u4SwitchCmdIfIndex)
                    {
                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port1IfIndex,
                                              ERPS_PORT_UNBLOCKING);

                        ErpsRedApplySwitchCommand (pRingInfo);
                    }
                    else
                    {
                        /* Apply Sw Port state to Hardware. */

                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port1IfIndex,
                                              ERPS_PORT_BLOCKING);
                    }

                }
                else if ((pErpsHwRingInfo->u1Port2Action ==
                          ERPS_PORT_STATE_UNBLOCKING)
                         && ((pRingInfo->u1Port2Status & ERPS_PORT_BLOCKING) ==
                             ERPS_PORT_BLOCKING))
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo "
                                      " Ring Port2 - Np state UNBLOCK,"
                                      " SW state BLOCK \r\n");

                    if (pRingInfo->u4Port2IfIndex ==
                        pRingInfo->u4SwitchCmdIfIndex)
                    {
                        /* NPAPI will not be called for Port2, 
                         * if it is subRing. */
                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port2IfIndex,
                                              ERPS_PORT_UNBLOCKING);

                        ErpsRedApplySwitchCommand (pRingInfo);
                    }
                    else
                    {
                        /* Apply Sw Port state to Hardware. */

                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port2IfIndex,
                                              ERPS_PORT_BLOCKING);
                    }
                }
                else if ((pErpsHwRingInfo->u1Port1Action ==
                          ERPS_PORT_STATE_BLOCKING) &&
                         (pRingInfo->u1Port1Status == ERPS_PORT_UNBLOCKING))
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo "
                                      " Ring Port1 - Np state BLOCK,"
                                      " SW state UNBLOCK \r\n");

                    /* If Hw node port state is in Block state, but Sw port 
                     * state is UnBlock state.
                     * Apply LocalSF on the Blocked Port and then Apply 
                     * ClearSF on the same port.            
                     */

                    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));

                    MepInfo.u4MdId = pRingInfo->CfmEntry.u4Port1MEGId;
                    MepInfo.u4MaId = pRingInfo->CfmEntry.u4Port1MEId;
                    MepInfo.u4MepId = pRingInfo->CfmEntry.u4Port1MEPId;

                    ErpsMsgProcessLinkSF (pRingInfo, MepInfo);
                    ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo);
                }
                else if ((pErpsHwRingInfo->u1Port2Action ==
                          ERPS_PORT_STATE_BLOCKING)
                         && (pRingInfo->u1Port2Status == ERPS_PORT_UNBLOCKING))
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo "
                                      " Ring Port2 - Np state BLOCK,"
                                      " SW state UNBLOCK \r\n");

                    /* NPAPI will not be called for Port2, if it is subRing. */
                    MEMSET (&MepInfo, 0, sizeof (tErpsCfmMepInfo));

                    MepInfo.u4MdId = pRingInfo->CfmEntry.u4Port2MEGId;
                    MepInfo.u4MaId = pRingInfo->CfmEntry.u4Port2MEId;
                    MepInfo.u4MepId = pRingInfo->CfmEntry.u4Port2MEPId;

                    ErpsMsgProcessLinkSF (pRingInfo, MepInfo);
                    ErpsMsgProcessLinkClearSF (pRingInfo, MepInfo);
                }
                else
                {
                    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                      pRingInfo->u4RingId,
                                      CONTROL_PLANE_TRC,
                                      "ErpsRedHwAuditAndUpdateRingInfo no"
                                      " action. \r\n");

                }

                break;
            default:
                break;
        }
    }

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAudCrtNpSyncBufferTable                       */
/*                                                                           */
/* DESCRIPTION      : This routine creates Hardware audit buffer table in the*/
/*                    standby node. Previosly present entries are deleted    */
/*                    before creating the table.                             */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ErpsRedHwAudCrtNpSyncBufferTable (VOID)
{
    tErpsRedNpSyncEntry *pBuf = NULL;
    tErpsRedNpSyncEntry *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries.
     */
    if (TMO_SLL_Count (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable) != 0)
    {
        TMO_DYN_SLL_Scan (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable, pBuf,
                          pTempBuf, tErpsRedNpSyncEntry *)
        {
            TMO_SLL_Delete (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable,
                            &(pBuf->Node));
            MemReleaseMemBlock (gErpsGlobalInfo.ErpsRedGlobalInfo.
                                HwAuditTablePoolId, (UINT1 *) pBuf);
            pBuf = pTempBuf;
        }
    }

    TMO_SLL_Init (&(gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable));
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedHwAudCreateOrFlushBuffer                      */
/*                                                                           */
/* DESCRIPTION      : This routine adds a node if it is not already present  */
/*                    in the NoSync table. Otherwise, if entry is present in */
/*                    the table this function will deleted that entry.       */
/*                                                                           */
/* INPUT            : pNpSync - Pointer to Synced Np data that needs to be   */
/*                              added.                                       */
/*                    u4NpApiId - to identify the specific Np Data structure.*/
/*                    u4EventId - special event id                           */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ErpsRedHwAudCreateOrFlushBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                                 UINT4 u4EventId)
{
    tErpsRedNpSyncEntry *pBuf = NULL;
    tErpsRedNpSyncEntry *pTempBuf = NULL;
    tErpsRedNpSyncEntry *pPrevBuf = NULL;
    UINT1               u1Match = OSIX_FALSE;

    /* If Node is already present, delete the node,
     * If Node is not present, add node to NpSync Table.
     */

    TMO_DYN_SLL_Scan (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tErpsRedNpSyncEntry *)
    {
        if (MEMCMP (&(pBuf->unNpData.FsErpsHwRingConfig),
                    &(pNpSync->FsErpsHwRingConfig),
                    sizeof (tErpsHwRingInfo)) == 0)
        {
            /* Update Hw handle in stanby, on reception of ring create. */

            if (pBuf->unNpData.FsErpsHwRingConfig.ErpsHwRingInfo.
                u1RingAction == ERPS_HW_RING_CREATE)
            {
                ErpsRedUpdateRingHwHandle
                    (&(pBuf->unNpData.FsErpsHwRingConfig.ErpsHwRingInfo));
            }

            TMO_SLL_Delete
                (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable,
                 &(pBuf->Node));

            MemReleaseMemBlock
                (gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTablePoolId,
                 (UINT1 *) pBuf);

            pBuf = NULL;
            u1Match = OSIX_TRUE;
            break;
        }

        pPrevBuf = pBuf;
    }

    if (u1Match == OSIX_FALSE)
    {
        pTempBuf = NULL;

        pTempBuf = (tErpsRedNpSyncEntry *) MemAllocMemBlk
            (gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTablePoolId);

        if (pTempBuf == NULL)
        {
            return;
        }

        TMO_SLL_Init_Node (&(pTempBuf->Node));

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));

        pTempBuf->u4NpApiId = u4NpApiId;

        pTempBuf->u4EventId = u4EventId;

        if (pPrevBuf == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable,
                            NULL, &(pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&gErpsGlobalInfo.ErpsRedGlobalInfo.HwAuditTable,
                            &(pPrevBuf->Node), &(pTempBuf->Node));
        }
    }

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedApplySwitchCommand                              */
/*                                                                           */
/* DESCRIPTION      : This routine applies switch command to the given ring. */
/*                                                                           */
/* INPUT            : pRingInfo - pointer to Ring Info on which the switch   */
/*                                command needs to be applied.               */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : None                                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
ErpsRedApplySwitchCommand (tErpsRingInfo * pRingInfo)
{
    switch (pRingInfo->u1SwitchCmdType)
    {
        case ERPS_SWITCH_COMMAND_FORCE:
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_FORCE_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsRedApplySwitchCommand failed in priority"
                                  " request handling for force switch.\r\n");
            }

            break;
        case ERPS_SWITCH_COMMAND_MANUAL:
            if (ErpsSmCalTopPriorityRequest (pRingInfo,
                                             ERPS_RING_MANUAL_SWITCH_REQUEST,
                                             NULL) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsRedApplySwitchCommand failed in priority"
                                  " request handling for manual switch.\r\n");
            }

            break;
        default:
            break;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC,
                      "ErpsRedApplySwitchCommand switch command"
                      " applied.\r\n");

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedGetMsgBuffer                                    */
/*                                                                           */
/* DESCRIPTION      : This routine returns CRU buffer memory from RM. In case*/
/*                    of failure, it sends indication to RM about it.        */
/*                                                                           */
/* INPUT            : u2BufSize - Buffer Size.                               */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : pMsg - Allocated Tx Buffer                             */
/*                                                                           */
/*****************************************************************************/

PRIVATE tRmMsg     *
ErpsRedGetMsgBuffer (UINT2 u2BufSize)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ERPS_APP_ID;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

        if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ERPS_GLOBAL_TRC ("ErpsRedGetMsgBuffer Memory allocation"
                             " ACK to RM failed\r\n");
        }
    }

    return pMsg;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessDeleteAllRingMsg                       */
/*                                                                         */
/* DESCRIPTION      : This routine process the deleted all message received*/
/*                    from the active node.                                */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                                                                         */
/* OUTPUT           : pu2Offset - Offset Value                             */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessDeleteAllRingMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UINT4               u4ContextId = 0;
    /* If module is already shutdown, do nothing
     * If ERPS is running in the context, apply shutdown.
     */
    /* Give this change notification to MSR also. */

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);

    ErpsRingDeleteAllRingsInContext (u4ContextId);

    ErpsPortNotifyProtocolShutStatus (u4ContextId);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncDeleteAllRingInCtxt                    */
/*                                                                         */
/* DESCRIPTION      : This routine sends delete all sync up message to     */
/*                    standby node for given context.                      */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which delete all info is */
/*                                  invoked.                               */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncDeleteAllRingInCtxt (UINT4 u4ContextId)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;

    if ((gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp != 0) &&
        (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_ACTIVE))
    {
        /* Send sync message to Standby node. */
        pMsg = ErpsRedGetMsgBuffer (ERPS_RED_DEL_ALL_RING_MSG_SIZE);

        if (pMsg == NULL)
        {
            ERPS_GLOBAL_TRC ("ErpsRedSyncDeleteAllRingInCtxt Memory"
                             " allocation from RM failed\r\n");

            return;
        }

        ERPS_RM_PUT_1_BYTE (pMsg, &u2Offset, ERPS_RED_DELETE_ALL_RING);
        ERPS_RM_PUT_2_BYTE (pMsg, &u2Offset, ERPS_RED_DEL_ALL_RING_MSG_SIZE);
        ERPS_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);

        ErpsRedSendMsgToRm (pMsg, u2Offset);
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncModuleEnableStatus                        */
/*                                                                         */
/* DESCRIPTION      : This routine sends module status sync up enable msg  */
/*                    to standby node for given context.                   */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which module status needs*/
/*                                  to be synced                           */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncModuleEnableStatus (UINT4 u4ContextId)
{
    ErpsRedSyncModuleStatusMsg (u4ContextId, ERPS_RED_ENABLE_RING_MSG);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncModuleDisableStatus                       */
/*                                                                         */
/* DESCRIPTION      : This routine sends module status sync up disable msg */
/*                    to standby node for given context.                   */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which module status needs*/
/*                                  to be synced                           */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncModuleDisableStatus (UINT4 u4ContextId)
{
    ErpsRedSyncModuleStatusMsg (u4ContextId, ERPS_RED_DISABLE_RING_MSG);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedSyncModuleStatusMsg                           */
/*                                                                         */
/* DESCRIPTION      : This routine sends module status sync up message to  */
/*                    standby node for given context.                      */
/*                                                                         */
/* INPUT            : u4ContextId - Context Id of which module status needs*/
/*                                  to be synced                           */
/*                    u1ModuleStatus - Module status that needs to be sync-*/
/*                                     ed up.                              */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
ErpsRedSyncModuleStatusMsg (UINT4 u4ContextId, UINT1 u1ModuleStatus)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;

    if ((gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp != 0) &&
        (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus == RM_ACTIVE))
    {
        /* Send sync message to Standby node. */
        pMsg = ErpsRedGetMsgBuffer (ERPS_RED_MODULE_STATUS_SIZE);

        if (pMsg == NULL)
        {
            ERPS_GLOBAL_TRC ("ErpsRedSyncModuleStatusMsg Memory"
                             " allocation from RM failed\r\n");

            return;
        }

        ERPS_RM_PUT_1_BYTE (pMsg, &u2Offset, u1ModuleStatus);
        ERPS_RM_PUT_2_BYTE (pMsg, &u2Offset, ERPS_RED_MODULE_STATUS_SIZE);
        ERPS_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);

        ErpsRedSendMsgToRm (pMsg, u2Offset);
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessModuleStatusMsg                        */
/*                                                                         */
/* DESCRIPTION      : This routine process the module status msg received  */
/*                    from the active node.                                */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                    u1MsgType - message type:                            */
/*                                           - ERPS_RED_ENABLE_RING_MSG    */
/*                                           - ERPS_RED_DISABLE_RING_MSG   */
/*                                                                         */
/* OUTPUT           : pu2Offset - Offset Value                             */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessModuleStatusMsg (tRmMsg * pMsg, UINT2 *pu2OffSet, UINT1 u1MsgType)
{
    UINT4               u4ContextId = 0;
    /* If module status is already updated, do nothing
     * Else apply Module Status.
     */

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);

    if (u1MsgType == ERPS_RED_ENABLE_RING_MSG)
    {
        ErpsCxtEnableAllRings (u4ContextId);
    }
    else
    {
        ErpsCxtDisableAllRings (u4ContextId);
    }

    /* Give this change notification to MSR also. */
    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessRingSyncMsg                            */
/*                                                                         */
/* DESCRIPTION      : This routine process the synced ring dynamic info    */
/*                    received from the active node.                       */
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                                                                         */
/* OUTPUT           : pu2Offset - Offset Value                             */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessRingSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet,
                           UINT2 u2RingDynInfoSize)
{
    tErpsContextInfo   *pContextInfo = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4RingId = 0;
    UINT4               u4NewNodeStatus = 0;
    UINT1               u1Port1Status = 0;
    UINT1               u1Port2Status = 0;

    /* Synced dynamic info order:
     * UINT4                  u4ContextId
     * UINT4                  u4RingId
     * UINT4                  u4RingNodeStatus
     * UINT4                  u4SwitchCmdIfIndex
     * tMacAddr               Port1LastRcvdNodeId
     * UINT2                  u2LastMsgSend
     * tMacAddr               Port2LastRcvdNodeId
     * UINT1                  u1RingState
     * UINT1                  u1HighestPriorityRequest
     * tErpsPortNodeIdBPRPair Port1StoredNodeIdBPRPair
     * tErpsPortNodeIdBPRPair Port2StoredNodeIdBPRPair
     * UINT1                  u1Port1Status
     * UINT1                  u1Port2Status
     * UINT1                  u1Port1HwStatus
     * UINT1                  u1Port2HwStatus
     * UINT1                  u1SwitchCmdType
     * UINT1                  bFopDefect
     */

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4RingId);

    pContextInfo = ErpsCxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        return;
    }

    pRingInfo = ErpsRingGetRingEntry (u4ContextId, u4RingId);

    if (pRingInfo == NULL)
    {
       /* When the ring information null, In order to avoid error messages 
        * displayed in stand by node, increment the offset before returning */ 
       *pu2OffSet = (UINT2) (*pu2OffSet + 
       (u2RingDynInfoSize - (UINT2)(sizeof (u4ContextId) + sizeof (u4RingId)))); 

        return;
    }

    /* Dynamic message may received after applying static configuration, then
     * it is possible that ring is not ACTIVE to hold the received dynamic
     * info. therefore, in this case just increment the OffSet and return.
     */

    if ((pContextInfo == NULL) || (pRingInfo == NULL) ||
        (pContextInfo->u1ModuleStatus == OSIX_DISABLED) ||
        (pRingInfo->u1RowStatus != ACTIVE))
    {
        *pu2OffSet +=
            (u2RingDynInfoSize - (sizeof (u4ContextId) + sizeof (u4RingId)));

        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsRedProcessRingSyncMsg ring is not in active"
                          "state to receive dynamic info\r\n");

        return;
    }

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4NewNodeStatus);

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, pRingInfo->u4SwitchCmdIfIndex);

    /* Update the ring Node status: Link failure status, RPL Port status,
     * and Timers running status.
     */
    ERPS_RM_GET_N_BYTE (pMsg, pRingInfo->Port1LastRcvdNodeId, pu2OffSet,
                        sizeof (tMacAddr));
    ERPS_RM_GET_2_BYTE (pMsg, pu2OffSet, pRingInfo->u2LastMsgSend);
    ERPS_RM_GET_N_BYTE (pMsg, pRingInfo->Port2LastRcvdNodeId, pu2OffSet,
                        sizeof (tMacAddr));
    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->u1RingState);
    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->u1HighestPriorityRequest);

    ERPS_RM_GET_N_BYTE (pMsg, pRingInfo->Port1StoredNodeIdBPRPair.NodeId,
                        pu2OffSet, sizeof (tMacAddr));

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet,
                        pRingInfo->Port1StoredNodeIdBPRPair.u1BPRStatus);

    /*tErpsPortNodeIdBPRPair is having one byte as padding byte.
     * So to get the proper values moving the offset */

    *pu2OffSet = *pu2OffSet + 1;

    ERPS_RM_GET_N_BYTE (pMsg, pRingInfo->Port2StoredNodeIdBPRPair.NodeId,
                        pu2OffSet, sizeof (tMacAddr));

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet,
                        pRingInfo->Port2StoredNodeIdBPRPair.u1BPRStatus);

    /*tErpsPortNodeIdBPRPair is having one byte as padding byte.
     * So to get the proper values moving the offset */

    *pu2OffSet = *pu2OffSet + 1;

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1Port1Status);

    if ((u1Port1Status & ERPS_PORT_BLOCKING) == ERPS_PORT_BLOCKING)
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                              ERPS_PORT_BLOCKING);
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                              ERPS_PORT_UNBLOCKING);

    }

    pRingInfo->u1Port1Status = u1Port1Status;

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1Port2Status);

    if ((u1Port2Status & ERPS_PORT_BLOCKING) == ERPS_PORT_BLOCKING)
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                              ERPS_PORT_BLOCKING);
    }
    else
    {
        ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                              ERPS_PORT_UNBLOCKING);

    }

    pRingInfo->u1Port2Status = u1Port2Status;

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->u1Port1HwStatus);
    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->u1Port2HwStatus);

    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->u1SwitchCmdType);
    ERPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pRingInfo->bFopDefect);

    if (u4NewNodeStatus != pRingInfo->u4RingNodeStatus)
    {
        /* Start/Stop the timer, based on changed status of timers */
        ErpsRedUpdateTimerStatus (pRingInfo, u4NewNodeStatus);
    }

    *pu2OffSet = (UINT2)(*pu2OffSet + 2);
    pRingInfo->u4RingNodeStatus = u4NewNodeStatus;

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedUpdateTimerStatus                             */
/*                                                                         */
/* DESCRIPTION      : This routine updates the ring timer info based on    */
/*                    sycned timer status.                                 */
/*                                                                         */
/* INPUT            : pRingInfo - pointer to ring info that needs to be    */
/*                                updated for timer status.                */
/*                    u4PrevNodeStatus - older ring timer status.          */
/*                                                                         */
/* OUTPUT           : None                                                 */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedUpdateTimerStatus (tErpsRingInfo * pRingInfo, UINT4 u4NewNodeStatus)
{
    UINT4               u4TimersToStart = 0;
    UINT4               u4TimersToStop = 0;

    /* Check the timer change status:
     * if Timer state changed from "Not Running" to "Running" state,
     * start the timer.
     * if timer state changed from "Running" to "Not Running" state,
     * stop the timer.
     * There is possibility that more than one timer are started or stopped 
     * during the time of dynamic update sync up.
     */

    u4TimersToStop = (pRingInfo->u4RingNodeStatus & ~(u4NewNodeStatus));

    u4TimersToStart = (~(pRingInfo->u4RingNodeStatus) & u4NewNodeStatus);

    if (u4TimersToStop != 0)
    {
        if ((u4TimersToStop & ERPS_WTR_TIMER_RUNNING) == ERPS_WTR_TIMER_RUNNING)
        {

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus stop WTR Timer in"
                              " standby node.\r\n");

            ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        }
        if ((u4TimersToStop & ERPS_WTB_TIMER_RUNNING) == ERPS_WTB_TIMER_RUNNING)
        {

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus stop WTB Timer in"
                              " standby node.\r\n");

            ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
        }

        if ((u4TimersToStop & ERPS_HOLDOFF_TIMER_RUNNING) ==
            ERPS_HOLDOFF_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus stop HoldOff Timer in"
                              " standby node.\r\n");

            ErpsTmrStopTimer (pRingInfo, ERPS_HOLD_OFF_TMR);

        }
        if ((u4TimersToStop & ERPS_GUARD_TIMER_RUNNING) ==
            ERPS_GUARD_TIMER_RUNNING)
        {

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus stop Guard Timer in"
                              " standby node.\r\n");

            ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR);

        }
        if ((u4TimersToStop & ERPS_PERIODIC_TIMER_RUNNING) ==
            ERPS_PERIODIC_TIMER_RUNNING)
        {

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus update periodic timer"
                              " status as stop in standby.\r\n");

            /* No need to stop the periodic timer, as it will not
             * be running in standby node.
             */

            pRingInfo->u4RingNodeStatus &= ~(ERPS_PERIODIC_TIMER_RUNNING);
        }

    }

    if (u4TimersToStart != 0)
    {
        if ((u4TimersToStart & ERPS_WTR_TIMER_RUNNING) ==
            ERPS_WTR_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus start WTR Timer in"
                              " standby node.\r\n");

            ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR,
                               pRingInfo->u4WTRTimerValue);
        }

        if ((u4TimersToStart & ERPS_WTB_TIMER_RUNNING) ==
            ERPS_WTB_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus start WTB Timer in"
                              " standby node.\r\n");

            ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR,
                               pRingInfo->u4WTBTimerValue);
        }

        if ((u4TimersToStart & ERPS_HOLDOFF_TIMER_RUNNING) ==
            ERPS_HOLDOFF_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus start HoldOff Timer in"
                              " standby node.\r\n");

            ErpsTmrStartTimer (pRingInfo, ERPS_HOLD_OFF_TMR,
                               pRingInfo->u4HoldOffTimerValue);
        }
        if ((u4TimersToStart & ERPS_GUARD_TIMER_RUNNING) ==
            ERPS_GUARD_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus start Guard Timer in"
                              " standby node.\r\n");

            ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR,
                               pRingInfo->u4GuardTimerValue);
        }
        if ((u4TimersToStart & ERPS_PERIODIC_TIMER_RUNNING) ==
            ERPS_PERIODIC_TIMER_RUNNING)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC,
                              "ErpsRedUpdateTimerStatus update Periodic Timer"
                              " status as Start in standby node.\r\n");

            /* No need to start periodic timer in standby node,
             * After switchover, if periodic timer is running,
             * Start Tx, it will send 3 messages and start 
             * periodic timer.
             */

            pRingInfo->u4RingNodeStatus &= ERPS_PERIODIC_TIMER_RUNNING;
        }

    }

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsRedSendMsgToRm                                     */
/*                                                                           */
/* DESCRIPTION      : This routine enqueues the Message to RM. If the Sending*/
/*                    fails, frees the memory.                               */
/*                                                                           */
/* INPUT            : pMsg - RM Message Data Buffer                          */
/*                    u2Length - Length of the message                       */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
ErpsRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    INT4                i4RetVal = -1;
    tRmProtoEvt         ProtoEvt;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    i4RetVal = ErpsPortRmEnqMsgToRm (pMsg, u2Length,
                                     RM_ERPS_APP_ID, RM_ERPS_APP_ID);

    if (i4RetVal != OSIX_SUCCESS)
    {
        ProtoEvt.u4AppId = RM_ERPS_APP_ID;

        ProtoEvt.u4Error = RM_SENDTO_FAIL;

        ERPS_GLOBAL_TRC ("ErpsRedSendMsgToRm send message to RM failed. \r\n");

        if (ErpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {

            ERPS_GLOBAL_TRC
                ("ErpsRedSendMsgToRm request ACK to RM failed.\r\n");
        }

        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedSyncRingDynInfo                           */
/*                                                                           */
/*    Description         : This function will initiate ring dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedSyncRingDynInfo (VOID)
{
    if ((gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp == 0) ||
        (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gErpsGlobalInfo.ErpsDynInfoList);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedUpdateSyncTable                           */
/*                                                                           */
/*    Description         : This function will update the sync table for the */
/*                          given ring info.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedUpdateSyncTable (tErpsRingInfo * pRingInfo)
{
    ErpsRedDbUtilAddTblNode (&gErpsGlobalInfo.ErpsDynInfoList,
                             &pRingInfo->RingDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDelRingDynInfo                            */
/*                                                                           */
/*    Description         : This function will remove ring info from the     */
/*                          sync table                                       */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDelRingDynInfo (tErpsRingInfo * pRingInfo)
{
    ErpsRedDbUtilDelTblNode (&gErpsGlobalInfo.ErpsDynInfoList,
                             &pRingInfo->RingDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pErpsDataDesc - This is Erps sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pErpsDbNode - This is db node defined in the ERPS*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbUtilAddTblNode (tDbTblDescriptor * pErpsDataDesc,
                         tDbTblNode * pErpsDbNode)
{
    if ((gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp == 0) ||
        (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pErpsDataDesc, pErpsDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedDbUtilDelTblNode                          */
/*                                                                           */
/*    Description         : This function will be used to delete a dynamic   */
/*                          info entry from database table.                  */
/*                                                                           */
/*    Input(s)            : pErpsDataDesc - This is Erps sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pErpsDbNode - This is db node defined in the ERPS*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
ErpsRedDbUtilDelTblNode (tDbTblDescriptor * pErpsDataDesc,
                         tDbTblNode * pErpsDbNode)
{
    if ((gErpsGlobalInfo.ErpsRedGlobalInfo.u1NumOfStandbyNodesUp == 0) ||
        (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilDelTblNode (pErpsDataDesc, pErpsDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PRIVATE VOID
ErpsRedAddAllNodeInDbTbl (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pRingInfo != NULL)
    {
        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) == OSIX_TRUE) &&
            (pRingInfo->u1RowStatus == ACTIVE))
        {

            ErpsRedDbUtilAddTblNode (&gErpsGlobalInfo.ErpsDynInfoList,
                                     &pRingInfo->RingDbNode);
        }

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRedSyncRemainingTimerValue                   */
/*                                                                           */
/*    Description         : This function will be used to sync remaining     */
/*                          timer values to standby node.                    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PRIVATE VOID
ErpsRedSyncRemainingTimerValue (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tRmMsg             *pMsg = NULL;
    UINT4               u4HoldOffRemTime = 0;
    UINT4               u4GuardRemTime = 0;
    UINT4               u4WtrRemTime = 0;
    UINT4               u4WtbRemTime = 0;
    UINT4               u4Offset = 0;
    UINT4               u4BulkUnitSize = 0;

    pMsg = ErpsRedGetMsgBuffer (DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedSyncRemainingTimerValue Memory"
                         " allocation from RM failed\r\n");

        return;
    }

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();

    while (pRingInfo != NULL)
    {
        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) == OSIX_TRUE) &&
            (pRingInfo->u1RowStatus == ACTIVE))
        {
            if ((pRingInfo->u4RingNodeStatus & ERPS_HOLDOFF_TIMER_RUNNING)
                == ERPS_HOLDOFF_TIMER_RUNNING)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->HoldOffTimer.TimerNode,
                                     &u4HoldOffRemTime);

                u4HoldOffRemTime =
                    u4HoldOffRemTime * ERPS_CENTI_TO_MILLI_SECOND;

            }
            if ((pRingInfo->u4RingNodeStatus & ERPS_GUARD_TIMER_RUNNING)
                == ERPS_GUARD_TIMER_RUNNING)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->GuardTimer.TimerNode,
                                     &u4GuardRemTime);

                u4GuardRemTime = u4GuardRemTime * ERPS_CENTI_TO_MILLI_SECOND;

            }

            if ((pRingInfo->u4RingNodeStatus & ERPS_WTR_TIMER_RUNNING)
                == ERPS_WTR_TIMER_RUNNING)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->WaitToRestoreTimer.TimerNode,
                                     &u4WtrRemTime);

                u4WtrRemTime = u4WtrRemTime * ERPS_CENTI_TO_MILLI_SECOND;
            }

            if ((pRingInfo->u4RingNodeStatus & ERPS_WTB_TIMER_RUNNING)
                == ERPS_WTB_TIMER_RUNNING)
            {
                TmrGetRemainingTime (gErpsGlobalInfo.RingTimerListId,
                                     &pRingInfo->WaitToBlockTimer.TimerNode,
                                     &u4WtbRemTime);

                u4WtbRemTime = u4WtbRemTime * ERPS_CENTI_TO_MILLI_SECOND;
            }

            ERPS_RM_PUT_1_BYTE (pMsg, &u4Offset, ERPS_RED_TIMER_REM_VALUE);
            ERPS_RM_PUT_2_BYTE (pMsg, &u4Offset, ERPS_RED_TIMER_MSG_SIZE);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, pRingInfo->u4ContextId);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, pRingInfo->u4RingId);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4HoldOffRemTime);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4GuardRemTime);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4WtrRemTime);
            ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4WtbRemTime);

        }

        if ((DB_MAX_BUF_SIZE - u4Offset) < ERPS_RED_TIMER_MSG_SIZE)
        {
            ErpsRedSendMsgToRm (pMsg, (UINT2) u4Offset);

            u4Offset = 0;
            pMsg = ErpsRedGetMsgBuffer (DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  ALL_FAILURE_TRC,
                                  "ErpsRedSyncRemainingTimerValue memory "
                                  "allocation failed\r\n");

                return;
            }
        }

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }
    if ((u1MemEstFlag == 1) && (ERPS_HR_STATUS () != ERPS_HR_STATUS_DISABLE))

    {
        /* This is to find the memory required for syncing EoamRedFillLOCALInfo */
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4)
            + sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4) +
            sizeof (UINT2) + sizeof (UINT1);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ERPS", (CHR1 *) "tErpsRingInfo",
                                    u4BulkUnitSize);
        /* Memory estimation are done , so changing the flag to next bulk message  */
        u1MemEstFlag = ERPS_RED_TIMER_MSG_SIZE;
    }

    ErpsRedSendMsgToRm (pMsg, (UINT2) u4Offset);

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedProcessRemainingTmrVal                    */
/*                                                                         */
/* DESCRIPTION      : This routine process the synced ramaining timer value*/
/*                                                                         */
/* INPUT            : pMsg - RM Data buffer holding messages               */
/*                                                                         */
/* OUTPUT           : pu2Offset - Offset Value                             */
/*                                                                         */
/* RETURNS          : None                                                 */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedProcessRemainingTmrVal (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4RingId = 0;
    UINT4               u4HoldOffRemTime = 0;
    UINT4               u4GuardRemTime = 0;
    UINT4               u4WtrRemTime = 0;
    UINT4               u4WtbRemTime = 0;

    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4RingId);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4HoldOffRemTime);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4GuardRemTime);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4WtrRemTime);
    ERPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4WtbRemTime);

    pRingInfo = ErpsRingGetRingEntry (u4ContextId, u4RingId);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedProcessRemainingTmrVal Ring info"
                         " received for Invalid ring\r\n");
        return;
    }

    /* If timer value is non-zero, Running status would have synced up 
     * first and timer would have started with the default values.
     * Therefore, first stop the timer and then start the timer with
     * remaining value.
     */
    if (u4HoldOffRemTime != 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsRedProcessRemainingTmrVal hold off timer"
                          " value received: %ld millisecond", u4HoldOffRemTime);

        ErpsTmrStopTimer (pRingInfo, ERPS_HOLD_OFF_TMR);
        ErpsTmrStartTimer (pRingInfo, ERPS_HOLD_OFF_TMR, u4HoldOffRemTime);
    }

    if (u4GuardRemTime != 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsRedProcessRemainingTmrVal guard timer"
                          " value received: %ld millisecond", u4GuardRemTime);

        ErpsTmrStopTimer (pRingInfo, ERPS_GUARD_TMR);
        ErpsTmrStartTimer (pRingInfo, ERPS_GUARD_TMR, u4GuardRemTime);
    }

    if (u4WtrRemTime != 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsRedProcessRemainingTmrVal WTR timer"
                          " value received: %ld millisecond", u4WtrRemTime);

        ErpsTmrStopTimer (pRingInfo, ERPS_WTR_TMR);
        ErpsTmrStartTimer (pRingInfo, ERPS_WTR_TMR, u4WtrRemTime);
    }
    if (u4WtbRemTime != 0)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC,
                          "ErpsRedProcessRemainingTmrVal WTB timer"
                          " value received: %ld millisecond", u4WtbRemTime);

        ErpsTmrStopTimer (pRingInfo, ERPS_WTB_TMR);
        ErpsTmrStartTimer (pRingInfo, ERPS_WTB_TMR, u4WtbRemTime);
    }

    return;
}

/***************************************************************************/
/* FUNCTION NAME    : ErpsRedUpdateRingHwHandle                            */
/*                                                                         */
/* DESCRIPTION      : This routine updates the ring Hw handle info.        */
/*                                                                         */
/* INPUT            : pErpsHwRingInfo - Ring info received to update hw    */
/*                                      status.                            */
/*                                                                         */
/* OUTPUT           : None.                                                */
/*                                                                         */
/* RETURNS          : None.                                                */
/*                                                                         */
/***************************************************************************/

PRIVATE VOID
ErpsRedUpdateRingHwHandle (tErpsHwRingInfo * pErpsHwRingInfo)
{
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingInfo;

    MEMSET (&RingInfo, 0, sizeof (RingInfo));

    RingInfo.u4ContextId = pErpsHwRingInfo->u4ContextId;
    RingInfo.u4RingId = pErpsHwRingInfo->u4RingId;

    pRingInfo = ErpsRingGetNodeFromRingTable (&RingInfo);

    if (pRingInfo == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRedUpdateRingHwHandle Ring is not present in "
                         "standby node.\r\n");

        return;
    }

    MEMCPY (pRingInfo->ai4HwRingHandle, pErpsHwRingInfo->ai4HwRingHandle,
            sizeof (pRingInfo->ai4HwRingHandle));

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC,
                      "ErpsRedUpdateRingHwHandle ring hardware handle"
                      " updated in the standby node.\r\n");

    return;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : ErpsRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      ERPS to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ErpsRedHRProcStdyStPktReq (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1TailFlag = 0;

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE)
    {
        ERPS_GLOBAL_TRC ("ERPS: Node is not active. "
                         "Processing of steady state pkt request failed.\n");
        return;
    }
    if (ERPS_HR_STATUS () == ERPS_HR_STATUS_DISABLE)
    {
        ERPS_GLOBAL_TRC ("ERPS: Hitless restart is not enabled."
                         " Processing of steady state pkt request failed.\n");
        return;
    }
    ERPS_GLOBAL_TRC ("Starts sending steady state packets to RM.\n");
    ERPS_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    pRingInfo = ErpsRingGetFirstNodeFrmRingTable ();
    while (pRingInfo)
    {
        /* Stop the ERPS Periodic Timer and Restart it again with 
         * duration as 0 to get the steady state packet
         */
        if ((ErpsUtilIsErpsEnabled (pRingInfo->u4ContextId) == OSIX_FALSE) ||
            (pRingInfo->u1RowStatus != ACTIVE))
        {
            continue;
        }

        if ((pRingInfo->u4RingNodeStatus & ERPS_PERIODIC_TIMER_RUNNING) ==
            ERPS_PERIODIC_TIMER_RUNNING)
        {
            ErpsTmrStopTimer (pRingInfo, ERPS_PERIODIC_TMR);
            if (ErpsTmrStartTimer (pRingInfo, ERPS_PERIODIC_TMR,
                                   0 /* Duration */ ) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrPeriodicTimerExp returned failure"
                                  " from starting periodic timer\r\n");
            }
            if (u1TailFlag == 0)
            {
                u1TailFlag = ERPS_HR_STDY_ST_PKT_TAIL;
            }
        }

        pRingInfo = ErpsRingGetNextNodeFromRingTable (pRingInfo);
    }

    /* If Timer is not running, send Steady State Tail Msg to move to the next
     * module */
    if (u1TailFlag != ERPS_HR_STDY_ST_PKT_TAIL)
    {
        ErpsRedHRSendStdyStTailMsg ();
        ERPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ErpsRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS/FAILURE.                                     */
/*****************************************************************************/
INT1
ErpsRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktLen,
                        UINT2 u2Port, UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BufSize = 0;

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        ERPS_GLOBAL_TRC ("ERPS: Node is not in Active state. Steady state "
                         "pkt is not sent to RM.\n");
        return OSIX_SUCCESS;
    }
    if (ERPS_HR_STATUS () == ERPS_HR_STATUS_DISABLE)
    {
        ERPS_GLOBAL_TRC ("ERPS: Hitless restart is not enabled. "
                         "Steady state packet is not sent to RM.\n");
        return OSIX_SUCCESS;
    }
    if (ERPS_HR_STDY_ST_REQ_RCVD () == OSIX_FALSE)
    {
        ERPS_GLOBAL_TRC ("ERPS: Steady state request is not received. "
                         "Steady state packet is not sent to RM.\n");
        return OSIX_SUCCESS;
    }

    ERPS_GLOBAL_TRC ("ERPS: sending steady state packet to RM...\n");

    /* Forming  steady state packet
     *
     *   <- 24B --><--------- 1B ---------><-- 2B -->< 2B -><-- 4B -->< 60 B >
     *   _____________________________________________________________________
     *   |        |                       |         |      |         |        |
     *   | RM Hdr | RM_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
     *   |________|_______________________|_________|______|_________|________|
     *
     * The RM Hdr shall be included by RM.
     */
    u2BufSize = ERPS_RED_PKT_TYPE_FIELD_SIZE + ERPS_RED_MIN_LENGTH_FIELD_SIZE + sizeof (UINT2)    /* for port */
        + sizeof (UINT4)        /* Timeout */
        + u4PktLen;                /* Packet Length */

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ERPS_GLOBAL_TRC ("RM ALLOC FAILED\n");
        return (OSIX_FAILURE);
    }
    /* Fill the message type. */
    u2MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    ERPS_RM_PUT_1_BYTE (pMsg, &u4Offset, RM_HR_STDY_ST_PKT_MSG);
    ERPS_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    ERPS_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);
    ERPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimeOut);

    /* copying the packet into the RM steady state msg */
    CRU_BUF_Concat_MsgBufChains (pMsg, pBuf);

    /* Send the packet to RM */
    if (ErpsRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ERPS: steady state pkt sending to RM is failed\n");
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ErpsRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
ErpsRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ERPS_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (gErpsGlobalInfo.ErpsRedGlobalInfo.u1NodeStatus != RM_ACTIVE)
    {
        ERPS_GLOBAL_TRC ("ERPS: Node is not active. "
                         "Steady state tail message is not sent to RM.\n");
        return OSIX_SUCCESS;
    }
    ERPS_GLOBAL_TRC ("ERPS: sending steady state tail message to RM.\n");

    u2BufSize = ERPS_RED_PKT_TYPE_FIELD_SIZE + ERPS_RED_MIN_LENGTH_FIELD_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM 
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ERPS_GLOBAL_TRC ("Rm alloc failed\n");
        return (OSIX_FAILURE);
    }

    /* Form a steady state tail message.
     *
     *             <--------1 Byte----------><---2 Byte--->
     *   __________________________________________________
     *   |        |                          |             |
     *   | RM Hdr | ERPS_HR_STDY_ST_PKT_TAIL | Msg Length  |
     *   |________|__________________________|_____________|
     *
     *  The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    ERPS_RM_PUT_1_BYTE (pMsg, &u4Offset, ERPS_HR_STDY_ST_PKT_TAIL);
    ERPS_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (ErpsRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ERPS: steady state tail message "
                         "sending is failed\n");
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function           : ErpsRedGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/
UINT1
ErpsRedGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;

    u1HRFlag = RmGetHRFlag ();

    return (u1HRFlag);
}
