/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: erpstmr.c,v 1.24 2017/09/22 12:34:49 siva Exp $                     *
 *                                                                         *
 * Description: This file contains the ERPS timer utility and expiry       *
 *              functions.                                                 *
 ***************************************************************************/

#include "erpsinc.h"

/****************************************************************************
 *                           erpstmr.c prototypes                           *
 ****************************************************************************/
PRIVATE VOID        ErpsTmrHoldOffTimerExp (VOID *pArg);
PRIVATE VOID        ErpsTmrGuardTimerExp (VOID *Arg);
PRIVATE VOID        ErpsTmrWTRTimerExp (VOID *Arg);
PRIVATE VOID        ErpsTmrPeriodicTimerExp (VOID *Arg);
PRIVATE VOID        ErpsTmrWTBTimerExp (VOID *Arg);
PRIVATE VOID        ErpsTmrTFOPTimerExp (VOID *Arg);
PRIVATE VOID        ErpsTmrFopPmTimerExp (VOID *Arg);

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrExpHandler                                */
/*                                                                           */
/*    Description         : This function will invoke timer expiry function  */
/*                          based on expiried timers.                        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
ErpsTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimer = NULL;
    INT2                i2Offset = 0;
    UINT1               u1TimerType = 0;
    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;
    while ((pExpiredTimer =
            TmrGetNextExpiredTimer (gErpsGlobalInfo.RingTimerListId)) != NULL)
    {
        /* Expiry of Ring timers will be ignored in StandBy Node.
         * Checking the RM API instead of ERPS node status, ensures that
         * even if ERPS node is not ready to take the expiry, but ERPS will
         * not loose the timer expiry.
         */
        if (ErpsPortRmGetNodeState () != RM_STANDBY)
        {
            u1TimerType = ((tTmrBlk *) pExpiredTimer)->u1TimerId;

            i2Offset = gErpsGlobalInfo.aTmrDesc[u1TimerType].i2Offset;

            (*(gErpsGlobalInfo.aTmrDesc[u1TimerType].TmrExpFn))
                ((UINT1 *) pExpiredTimer - i2Offset);

            /* HITLESS RESTART */
            if ((u1TimerType == ERPS_PERIODIC_TMR) &&
                (ERPS_HR_STATUS () != ERPS_HR_STATUS_DISABLE) &&
                (ERPS_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
                (u1HRTmrFlag == 0))
            {
                /* Periodic timer has been made to expire after processing
                 * steady state pkt request from RM. This flag indicates
                 * to send steady tail msg after sending all the steady
                 * state packets of ERPS to RM. */
                u1HRTmrFlag = 1;
            }
        }
    }
    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        /* sending steady state tail msg to RM */
        ErpsRedHRSendStdyStTailMsg ();
        ERPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrInitTimerDesc                             */
/*                                                                           */
/*    Description         : This function intializes the timer descriptor for*/
/*                          all the timers in ERPS module.                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsTmrInitTimerDesc (VOID)
{
    gErpsGlobalInfo.aTmrDesc[ERPS_HOLD_OFF_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, HoldOffTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_HOLD_OFF_TMR].TmrExpFn =
        ErpsTmrHoldOffTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_GUARD_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, GuardTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_GUARD_TMR].TmrExpFn = ErpsTmrGuardTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_WTR_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, WaitToRestoreTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_WTR_TMR].TmrExpFn = ErpsTmrWTRTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_PERIODIC_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, PeriodicTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_PERIODIC_TMR].TmrExpFn =
        ErpsTmrPeriodicTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_WTB_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, WaitToBlockTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_WTB_TMR].TmrExpFn = ErpsTmrWTBTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_TFOP_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, TfopTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_TFOP_TMR].TmrExpFn = ErpsTmrTFOPTimerExp;

    gErpsGlobalInfo.aTmrDesc[ERPS_FOP_PM_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, FopPmTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_FOP_PM_TMR].TmrExpFn = ErpsTmrFopPmTimerExp;

#ifdef ICCH_WANTED
    gErpsGlobalInfo.aTmrDesc[ERPS_ICCH_FDB_SYNC_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tErpsRingInfo, VlanFdbSyncTimer);
    gErpsGlobalInfo.aTmrDesc[ERPS_ICCH_FDB_SYNC_TMR].TmrExpFn =
        ErpsTmrHandleFdbSyncTimerExp;
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrStartTimer                                */
/*                                                                           */
/*    Description         : This function will start the timer for given ring*/
/*                          time and duration.                               */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.               */
/*                          u1TimerType - Timer type that needs to be started*/
/*                          u4Duration - Timer duration.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         started timer of given timer type.*/
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         start the timer.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsTmrStartTimer (tErpsRingInfo * pRingInfo, UINT1 u1TimerType,
                   UINT4 u4Duration)
{
    unErpsRingDynInfo   RingDynInfo;
    tTmrBlk            *pTimer = NULL;
    UINT4               u4RingNodeStatus = 0;
    UINT4               u4Sec = 0;
    UINT4               u4MilliSec = 0;
    UINT1               u1MsgType = 0;
    INT4                i4RetVal = 0;
    /* This is used to call DERPS sync for Timer start event.
     * u1MsgType will be updated only if the timer type is 
     * WTR/WTB/Guard. If u1MsgType is set with corresponding timer
     * macros, it will be notified to Remote ERP. */

    /* HITLESS RESTART */
    if (ERPS_HR_STATUS () == ERPS_HR_STATUS_DISABLE)
    {
        if (u4Duration == 0)
        {
            return OSIX_SUCCESS;
        }
    }
    switch (u1TimerType)
    {
        case ERPS_HOLD_OFF_TMR:

            pTimer = &(pRingInfo->HoldOffTimer);
            u4RingNodeStatus = ERPS_HOLDOFF_TIMER_RUNNING;
            break;

        case ERPS_GUARD_TMR:

            pTimer = &(pRingInfo->GuardTimer);
            u4RingNodeStatus = ERPS_GUARD_TIMER_RUNNING;
            u1MsgType = ERPS_RING_START_GUARD_TMR;
            break;

        case ERPS_WTR_TMR:

            pTimer = &(pRingInfo->WaitToRestoreTimer);
            u4RingNodeStatus = ERPS_WTR_TIMER_RUNNING;
            u1MsgType = ERPS_RING_START_WTR_TMR;
            break;

        case ERPS_PERIODIC_TMR:

            pTimer = &(pRingInfo->PeriodicTimer);
            u4RingNodeStatus = ERPS_PERIODIC_TIMER_RUNNING;
            break;

        case ERPS_WTB_TMR:

            pTimer = &(pRingInfo->WaitToBlockTimer);
            u4RingNodeStatus = ERPS_WTB_TIMER_RUNNING;
            u1MsgType = ERPS_RING_START_WTB_TMR;
            break;
        case ERPS_TFOP_TMR:
            pTimer = &(pRingInfo->TfopTimer);
            break;

        case ERPS_FOP_PM_TMR:
            pTimer = &(pRingInfo->FopPmTimer);
            break;

#ifdef ICCH_WANTED
        case ERPS_ICCH_FDB_SYNC_TMR:
            pTimer = &(pRingInfo->VlanFdbSyncTimer);
            break;
#endif
        default:

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTmrStartTimer invalid timer type "
                              "received.\r\n");
            return OSIX_FAILURE;
    }

    /* Periodic timer need not to be marked as stop and start
     * this will avoid node syncup on expiry of periodic timer.
     */
    if ((pRingInfo->u4RingNodeStatus & u4RingNodeStatus) &&
        (u4RingNodeStatus != ERPS_PERIODIC_TIMER_RUNNING))
    {
        /* If the timer is already running, don't need to do anything */
        return OSIX_SUCCESS;
    }

    ERPS_CONVERT_TMR_DURATION_TO_SEC (u4Duration, u4Sec, u4MilliSec);

    /* If timer is WTR/WTB, it should be started only if RPL port is present in Local Linecard.
     * Other timers will be started in both the line cards. */
    if ((u1TimerType == ERPS_WTR_TMR) || (u1TimerType == ERPS_WTB_TMR))
    {
        /* Start WTR/WTB if and only If RPL Port is in Local Line Card */
        if ((pRingInfo->u4Port1IfIndex == pRingInfo->u4RplIfIndex) ?
            (pRingInfo->u1IsPort1Present == ERPS_PORT_IN_LOCAL_LINE_CARD) :
            (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_LOCAL_LINE_CARD))
        {
            if (TmrStart
                (gErpsGlobalInfo.RingTimerListId, pTimer, u1TimerType, u4Sec,
                 u4MilliSec) == TMR_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrStartTimer timer start failed\r\n");
                gErpsGlobalInfo.u4TimerFailCount++;
                ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_TIMER_FAIL);
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        if (TmrStart
            (gErpsGlobalInfo.RingTimerListId, pTimer, u1TimerType, u4Sec,
             u4MilliSec) == TMR_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTmrStartTimer timer start failed\r\n");
            gErpsGlobalInfo.u4TimerFailCount++;
            ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_TIMER_FAIL);
            return OSIX_FAILURE;
        }
    }
    /* if the timer is TFOP timer no need for dynamic updation. */
    if (u1TimerType == ERPS_TFOP_TMR)
    {
        return OSIX_SUCCESS;
    }
    /* if the timer is FOP-PM timer no need for dynamic updation. */
    if (u1TimerType == ERPS_FOP_PM_TMR)
    {
        return OSIX_SUCCESS;
    }
#ifdef ICCH_WANTED
    if (u1TimerType == ERPS_ICCH_FDB_SYNC_TMR)
    {
        return OSIX_SUCCESS;
    }
#endif
    /* Calling DERPS sync only for starting WTB/WTR/Guard timers. */
    if (u1MsgType)
    {
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, u1MsgType);
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrStartTimer :%s has been "
                      "started for %lu\r\n", gaai1TimerState[u1TimerType],
                      u4Duration);

    if (u1TimerType == ERPS_WTR_TMR)
    {
        i4RetVal =
            ErpsSmCalTopPriorityRequest (pRingInfo,
                                         ERPS_RING_WTR_RUNNING_REQUEST, NULL);
    }

    if (u1TimerType == ERPS_WTB_TMR)
    {
        i4RetVal =
            ErpsSmCalTopPriorityRequest (pRingInfo,
                                         ERPS_RING_WTB_RUNNING_REQUEST, NULL);
    }

    /* if timer was not running earlier, update dynamic info. */
    if (!(pRingInfo->u4RingNodeStatus & u4RingNodeStatus))
    {
        /* Update the ring node status */
        RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
        RingDynInfo.u4RingNodeStatus |= u4RingNodeStatus;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_NODE_STATUS);

    }

    UNUSED_PARAM (i4RetVal);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrStopTimer                                 */
/*                                                                           */
/*    Description         : This function will stop the given timer type of  */
/*                          the ring.                                        */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.               */
/*                          u1TimerType - Timer type that needs to be stopped*/
/*                          u4Duration - Timer duration.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         stoped timer of given timer type. */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         stop the timer.                   */
/*****************************************************************************/
PUBLIC INT4
ErpsTmrStopTimer (tErpsRingInfo * pRingInfo, UINT1 u1TimerType)
{
    unErpsRingDynInfo   RingDynInfo;
    tTmrBlk            *pTimer = NULL;
    UINT4               u4RingNodeStatus = 0;
    UINT1               u1MsgType = 0;
    INT4                i4RetVal = 0;
    /* This is used to call DERPS sync for Timer start event.
     * u1MsgType will be updated only if the timer type is 
     * WTR/WTB/Guard. If u1MsgType is set with corresponding timer
     * macros, it will be notified to Remote ERP. */

    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (u1TimerType)
    {
        case ERPS_HOLD_OFF_TMR:
            pTimer = &(pRingInfo->HoldOffTimer);
            u4RingNodeStatus = ERPS_HOLDOFF_TIMER_RUNNING;
            break;

        case ERPS_GUARD_TMR:
            pTimer = &(pRingInfo->GuardTimer);
            u4RingNodeStatus = ERPS_GUARD_TIMER_RUNNING;
            u1MsgType = ERPS_RING_STOP_GUARD_TMR;
            break;

        case ERPS_WTR_TMR:
            pTimer = &(pRingInfo->WaitToRestoreTimer);
            u4RingNodeStatus = ERPS_WTR_TIMER_RUNNING;
            u1MsgType = ERPS_RING_STOP_WTR_TMR;
            break;

        case ERPS_PERIODIC_TMR:
            pTimer = &(pRingInfo->PeriodicTimer);
            u4RingNodeStatus = ERPS_PERIODIC_TIMER_RUNNING;
            break;

        case ERPS_WTB_TMR:

            pTimer = &(pRingInfo->WaitToBlockTimer);
            u4RingNodeStatus = ERPS_WTB_TIMER_RUNNING;
            u1MsgType = ERPS_RING_STOP_WTB_TMR;
            break;

        case ERPS_TFOP_TMR:
            pTimer = &(pRingInfo->TfopTimer);
            break;

        case ERPS_FOP_PM_TMR:
            pTimer = &(pRingInfo->FopPmTimer);
            break;

#ifdef ICCH_WANTED
        case ERPS_ICCH_FDB_SYNC_TMR:
            pTimer = &(pRingInfo->VlanFdbSyncTimer);
            break;
#endif
        default:
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTmrStopTimer received invalid timer "
                              "type.\r\n");
            return OSIX_FAILURE;
    }

    if (!(pRingInfo->u4RingNodeStatus & u4RingNodeStatus) &&
        (u1TimerType != ERPS_TFOP_TMR) &&
#ifdef ICCH_WANTED
        (u1TimerType != ERPS_ICCH_FDB_SYNC_TMR) &&
#endif /* ICCH_WANTED */
        (u1TimerType != ERPS_FOP_PM_TMR))
    {
        /* If the timer is not running, don't need to do anything */
        return OSIX_SUCCESS;
    }

    if (TmrStop (gErpsGlobalInfo.RingTimerListId, pTimer) == TMR_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTmrStopTimer timer stop failed\r\n");
        gErpsGlobalInfo.u4TimerFailCount++;

        return OSIX_FAILURE;
    }

    /* if the timer is TFOP timer no need for dynamic updation. */
    if (u1TimerType == ERPS_TFOP_TMR)
    {
        return OSIX_SUCCESS;
    }

    /* if the timer is FOP-PM timer no need for dynamic updation. */
    if (u1TimerType == ERPS_FOP_PM_TMR)
    {
        return OSIX_SUCCESS;
    }
#ifdef ICCH_WANTED
    if (u1TimerType == ERPS_ICCH_FDB_SYNC_TMR)
    {
        return OSIX_SUCCESS;

    }
#endif

    /* Calling DERPS sync only for starting WTB/WTR/Guard timers. */
    if (u1MsgType)
    {
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, u1MsgType);
    }

    if (u1TimerType == ERPS_WTR_TMR)
    {
        i4RetVal =
            ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_WTR_STOP_REQUEST,
                                         NULL);
    }

    if (u1TimerType == ERPS_WTB_TMR)
    {
        i4RetVal =
            ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_WTB_STOP_REQUEST,
                                         NULL);
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrStopTimer :%s is "
                      "stopped\r\n", gaai1TimerState[u1TimerType]);

    /* Update the ring node status */
    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus &= ~(u4RingNodeStatus);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    UNUSED_PARAM (i4RetVal);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrHoldOffTimerExp                           */
/*                                                                           */
/*    Description         : This function will handle holdoff timer expiry   */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrHoldOffTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus &= ~(ERPS_HOLDOFF_TIMER_RUNNING);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    /* During hold of timer, there is possibility that failure signal would 
     * have cleard off, check if Local SF condition still present as top 
     * priority event.
     *
     * If Local SF is present as top priority event, then trigger state 
     * machine.
     *
     * Hold off timer will start only for the first link failure, second link
     * failure will not restart the timer. At the time of expiry, any failed 
     * port can be notified as failed port.
     *
     * No need to check if only one failure is present or both the link are 
     * failed. because holdoff timer would have started when first failure 
     * has occcured. So in expiry of holdoff just ask Priority Logic to
     * take appropriate action.
     */
    /* When hold of timer is configured to a non-zero value and one of the 
     * ring ports fail in the ring, the hold off timer is started . Now, when 
     * the other ring port fails in the same ring node and if hold off timer 
     * is still running, the second failure in the ring port should be ignored 
     * until the hold off timer expires 
     * [Reference : Section 10.1.8 : G.8032/Y.1344 (03/2010) standard] . 
     * Now when the hold off timer expires, the failure in the 1st Ring Port 
     * is Handled by triggering the ERPS state event machine.  The failure 
     * in the 2nd ring port is handled on Hold off is handled 
     * in Hold Off Timer Expiry */

    if (pRingInfo->u1RingState != ERPS_RING_FORCED_SWITCH_STATE)
    {
        if ((pRingInfo->u1Port1Status & ERPS_PORT_FAILED) ||
            (pRingInfo->u1Port2Status & ERPS_PORT_FAILED))
        {
            if (ErpsSmCalTopPriorityRequest
                (pRingInfo, ERPS_RING_LOCAL_SF_REQUEST,
                 &gErpsRcvdMsg) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                  "ErpsTmrHoldOffTimerExp is returned failed from "
                                  "Priority Request handling.\r\n");
            }
        }
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsTmrHoldOffTimerExp :"
                          "HoldOff Timer expired\r\n");
        /* In case of SF in both the ring ports, Port1-SF 
         * is handled in SEM Port2-SF will be handled here
         * SEM won't be triggered */
        if ((pRingInfo->u1Port2Status & ERPS_PORT_FAILED) &&
            (pRingInfo->u1Port1Status & ERPS_PORT_FAILED))
        {

            pRingInfo->u1HighestPriorityRequestWithoutVC =
                ERPS_RING_LOCAL_SF_REQUEST;

            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_BLOCKING);
            pRingInfo->u1HighestPriorityRequestWithoutVC =
                ERPS_RING_NO_ACTIVE_REQUEST;

            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsMsgProcessLinkSF: Setting Port %u to Blocked state "
                              ".\r\n", pRingInfo->u4Port2IfIndex);

            if (pRingInfo->u4Port2IfIndex == pRingInfo->u4RplIfIndex)
            {
                RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
                RingDynInfo.u4RingNodeStatus |= (ERPS_NODE_STATUS_RB);
                ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                           ERPS_RING_NODE_STATUS);
            }

            /* If the Second port is also failed, SEM won't be triggered,
             * but flushing for the Ring will be done */
            ErpsUtilFlushFdbTable (pRingInfo, ERPS_NO_OPTIMIZE);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrGuardTimerExp                             */
/*                                                                           */
/*    Description         : This function will handle guard timer expiry     */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrGuardTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus &= ~(ERPS_GUARD_TIMER_RUNNING);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrGuardTimerExp :"
                      "Guard Timer expired\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrWTRTimerExp                               */
/*                                                                           */
/*    Description         : This function will handle WTR timer expiry       */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrWTRTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus &= ~(ERPS_WTR_TIMER_RUNNING);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    if (ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_WTR_EXP_REQUEST, NULL)
        == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTmrWTRTimerExp returned failure from Priority "
                          "Request handling.\r\n");
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrWTRTimerExp :"
                      "WTR Timer expired\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrPeriodicTimerExp                          */
/*                                                                           */
/*    Description         : This function will handle periodic timer expiry  */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrPeriodicTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    /* R-APS (SF) messages has to be sent with DNF bit set, in NonRevertive
     * mode of operation
     */

    if (pRingInfo->u1OperatingMode == ERPS_RING_NON_REVERTIVE_MODE)
    {
        RingDynInfo.u2LastMsgSend = pRingInfo->u2LastMsgSend;

        if (((RingDynInfo.u2LastMsgSend >> 12) & ERPS_MESSAGE_TYPE_SF) ==
            ERPS_MESSAGE_TYPE_SF)
        {
            RingDynInfo.u2LastMsgSend |= ERPS_DNF_FIELD;

            ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                       ERPS_RING_LAST_MSG_TYPE);
        }
    }

    if (ErpsTxFormAndSendRAPSPdu (pRingInfo) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTmrPeriodicTimerExp returned failure from "
                          "Sending Raps messages.\r\n");
        return;
    }

    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrPeriodicTimerExp:"
                      "Sent R-APS Message %x\r\n", pRingInfo->u2LastMsgSend);

    if (ErpsTmrStartTimer (pRingInfo, ERPS_PERIODIC_TMR,
                           pRingInfo->u4PeriodicTimerValue) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTmrPeriodicTimerExp returned failure from "
                          "starting periodic timer\r\n");
    }

    /* No need to sync expiry of periodic timer, as this would have synced up
     * while starting this timer again.
     */
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrWTBTimerExp                               */
/*                                                                           */
/*    Description         : This function will handle WTB timer expiry       */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrWTBTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    RingDynInfo.u4RingNodeStatus = pRingInfo->u4RingNodeStatus;
    RingDynInfo.u4RingNodeStatus &= ~(ERPS_WTB_TIMER_RUNNING);
    ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo, ERPS_RING_NODE_STATUS);

    if (ErpsSmCalTopPriorityRequest (pRingInfo, ERPS_RING_WTB_EXP_REQUEST, NULL)
        == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "ErpsTmrWTBTimerExp returned failure from Priority "
                          "Request handling.\r\n");
    }
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrWTBTimerExp :"
                      "WTB Timer expired\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrTFOPTimerExp                              */
/*                                                                           */
/*    Description         : This function will handle TFOP timer expiry      */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrTFOPTimerExp (VOID *pArg)
{
    unErpsRingDynInfo   RingDynInfo;
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;

    /* none of the RAPS got received in  (K* u4PeriodicTimerValue), so raise defect  */
    if (pRingInfo->bFopDefect != ERPS_FOP_TO_DEFECT)
    {

        ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_TFOP_DEFECT);
        RingDynInfo.u1AlarmStatus |= ERPS_FOP_DEFECT;
        ErpsUtilUpdateRingDynInfo (pRingInfo, &RingDynInfo,
                                   ERPS_RING_FOP_DEFECT);

        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          CONTROL_PLANE_TRC, "ErpsTmrTFOPTimerExp :"
                          "TFOP Timer expired\r\n");
    }
    if (pRingInfo->bDFOP_TO == OSIX_ENABLED)
    {
        if (ErpsTmrStartTimer (pRingInfo, ERPS_TFOP_TMR,
                               ((pRingInfo->f4KValue) *
                                (pRingInfo->u4PeriodicTimerValue))) ==
            OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "ErpsTmrTFOPTimerExp returned failure from "
                              "starting TFOP timer\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsTmrFopPmTimerExp                             */
/*                                                                           */
/*    Description         : This function will handle FOP-PM timer expiry      */
/*                          event.                                           */
/*                                                                           */
/*    Input(s)            : pArg - Pointer to ring node.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PRIVATE VOID
ErpsTmrFopPmTimerExp (VOID *pArg)
{
    tErpsRingInfo      *pRingInfo = (tErpsRingInfo *) pArg;
    ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                      CONTROL_PLANE_TRC, "ErpsTmrFopPmTimerExp :"
                      "FOP-PM Timer expired\r\n");
    ErpsTrapSendTrapNotifications (pRingInfo, ERPS_TRAP_PROV_MISMATCH_CLEAR);
    /* Reset the bFopPMDefect to init value after the clearance of FOP-PM */
    pRingInfo->bFopPMDefect = ERPS_INIT_VAL;
    return;
}

#ifdef ICCH_WANTED
 /*****************************************************************************/
 /*                                                                           */
 /*    Function Name       : ErpsTmrHandleFdbSyncTimerExp                     */
 /*                                                                           */
 /*    Description         : This function will handle FDB Sync timer expiry  */
 /*                          event.                                           */
 /*                                                                           */
 /*    Input(s)            : None                                             */
 /*                                                                           */
 /*    Output(s)           : None                                             */
 /*                                                                           */
 /*    Returns             : None.                                            */
 /*****************************************************************************/
VOID
ErpsTmrHandleFdbSyncTimerExp (VOID *pArg)
{

    if (VlanApiSetFdbFreezeStatus (VLAN_ENABLED) == VLAN_FAILURE)
    {
        ERPS_GLOBAL_TRC ("Failed to Set FDB Freeze status \r\n");
    }

    UNUSED_PARAM (pArg);
}
#endif
