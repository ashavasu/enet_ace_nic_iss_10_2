/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    
 *                                                                         
 * $Id: erpsring.c,v 1.45 2016/05/04 11:37:44 siva Exp $                   
 * Description: This file contains the utility functions for ring Data     
 *              Structures.                                                
 ***************************************************************************/

#include "erpsinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingCreateRingNode                           */
/*                                                                           */
/*    Description         : This function will allocate ring node and        */
/*                          initialize topology change Dynamic SLL.          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to newly created ring node. */
/*                                                                           */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingCreateRingNode (VOID)
{
    tErpsRingInfo      *pRingInfo = NULL;
    UINT1               u1Index = 0;

    if ((pRingInfo = (tErpsRingInfo *)
         MemAllocMemBlk (gErpsGlobalInfo.RingInfoPoolId)) == NULL)
    {
        gErpsGlobalInfo.u4BuffferAllocFailCount++;
        ERPS_GLOBAL_TRC ("ErpsRingCreateRingNode: Memory Allocation"
                         "failed for RingEntry\r\n");
        return NULL;
    }

    MEMSET (pRingInfo, 0, sizeof (tErpsRingInfo));

    /* Allocate memory for VlanGroup List */
    if ((pRingInfo->pau1RingVlanGroupList =
         MemAllocMemBlk (gErpsGlobalInfo.VlanGroupListPoolId)) == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsRingCreateRingNode: Memory Allocation"
                         "failed for RingVlanGroupList\r\n");
        MemReleaseMemBlock (gErpsGlobalInfo.RingInfoPoolId,
                            (UINT1 *) pRingInfo);
        return NULL;
    }

    MEMSET (pRingInfo->pau1RingVlanGroupList, 0, sizeof (tErpsVlanGroupList));

    pRingInfo->i4Port1RBNodeOffset = FSAP_OFFSETOF (tErpsRingInfo, Port1RBNode);

    pRingInfo->i4Port2RBNodeOffset = FSAP_OFFSETOF (tErpsRingInfo, Port2RBNode);

    pRingInfo->i4DistPortRBNodeOffset =
        FSAP_OFFSETOF (tErpsRingInfo, DistPortRBNode);

    pRingInfo->i4Port1MepRBNodeOffset =
        FSAP_OFFSETOF (tErpsRingInfo, Port1MepRBNode);
    pRingInfo->i4Port2MepRBNodeOffset =
        FSAP_OFFSETOF (tErpsRingInfo, Port2MepRBNode);

    pRingInfo->TcPropRingList = NULL;
    pRingInfo->u1OperatingMode = ERPS_RING_REVERTIVE_MODE;
    pRingInfo->u1RingState = ERPS_RING_DISABLED_STATE;
    pRingInfo->u4HoldOffTimerValue = ERPS_HOLD_OFF_TMR_DEF_VAL;
    pRingInfo->u4GuardTimerValue = ERPS_GUARD_TMR_DEF_VAL;
    pRingInfo->u4WTRTimerValue = ERPS_WTR_TMR_DEF_VAL;
    pRingInfo->u4WTBTimerValue = ERPS_WTB_TMR_DEF_VAL;
    pRingInfo->u1RingClearOperatingModeType = ERPS_CLEAR_DEF_VAL;
    pRingInfo->u4PeriodicTimerValue = ERPS_PERIODIC_TMR_DEF_VAL;
    pRingInfo->u4VcRecoveryPeriodicTime = ERPS_VC_REC_PERIODIC_TMR_DEF_VAL;
    pRingInfo->f4KValue = ERPS_KVALUE_DEF_VAL;
    pRingInfo->bFopDefect = ERPS_INIT_VAL;
    pRingInfo->bFopPMDefect = ERPS_INIT_VAL;
    pRingInfo->bDFOP_TO = OSIX_DISABLED;
    TMO_SLL_Init (&(pRingInfo->SubRingListHead));
    pRingInfo->u1SwitchCmdType = ERPS_SWITCH_COMMAND_NONE;
    pRingInfo->u1PropagateTC = OSIX_DISABLED;
    pRingInfo->u1RecoveryMethod = ERPS_RING_AUTO_RECOVERY;
    pRingInfo->u1HighestPriorityRequest = ERPS_RING_NO_ACTIVE_REQUEST;
    pRingInfo->u1HighestPriorityRequestWithoutVC = ERPS_RING_NO_ACTIVE_REQUEST;
    pRingInfo->u1PortBlockedOnVcRecovery = ERPS_SNMP_FALSE;
    pRingInfo->u1RingPduSend = ERPS_RING_PDU_DONT_SEND;
    pRingInfo->u1RAPSCompatibleVersion = ERPS_RING_COMPATIBLE_VERSION2;
    pRingInfo->u1RAPSSubRingWithoutVirtualChannel = OSIX_DISABLED;
    pRingInfo->bDNFStatus = OSIX_FALSE;
    pRingInfo->u2VlanGroupListCount = 0;
    pRingInfo->u1IsMsgRcvd = OSIX_FALSE;
    /* This flag is added to enable or disable the Flush logic support
     * By default flag is initialized to OSIX_ENABLED, setting this flag
     * equal to OSIX_DISABLED is equal to disabling the Flush logic
     * support */
    pRingInfo->u1FlushLogicSupport = OSIX_ENABLED;
    pRingInfo->u1IsPort1Present = ERPS_PORT_IN_LOCAL_LINE_CARD;
    pRingInfo->u1IsPort2Present = ERPS_PORT_IN_LOCAL_LINE_CARD;
    pRingInfo->u1MonitoringType = ERPS_MONITOR_MECH_CFM;
    pRingInfo->u1RapsFlag = 0;

    /* This Flag is added to avoid Repetitive Dynamic sync
     * between Local and Remote ERP.
     * In function ErpsUtilUpdateRingDynInfo when ever any RingInfo
     * Parameters values is changed, then function
     * ErpsDsyncUpdateDERPSRingDynInfo is called to sync with the
     * Remote ERP.
     * @receiving ERP in ErpsMsgProcessRapsMsg function 
     * ErpsDynSyncMsgHandler will be called to update the RingInfo
     * structure by calling ErpsUtilUpdateRingDynInfo which inturn
     * calls the ErpsDsyncUpdateDERPSRingDynInfo function to update
     * the sender, so if this happens then an Update sync info loop
     * will be formed between Local and Remote ERP. To avoid this
     * situation, the flag u1DERPSSyncReqFlag is used.
     */
    pRingInfo->u1DERPSSyncReqFlag = OSIX_FALSE;

    for (u1Index = 0; u1Index < ERPS_MAX_HW_HANDLE; u1Index++)
    {
        pRingInfo->ai4HwRingHandle[u1Index] = -1;
    }
    pRingInfo->pContext = NULL;

    ErpsRedDbNodeInit (pRingInfo);

    return pRingInfo;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingAddNodeToRingRBTree                      */
/*                                                                           */
/*    Description         : This function will add ring node to RingTable.   */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         added node to RBTree.             */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         add node to RingTable.            */
/*****************************************************************************/
PUBLIC INT4
ErpsRingAddNodeToRingRBTree (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (gErpsGlobalInfo.RingTable, (tRBElem *) pRingInfo)
        == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddNodeToRingRBTree node addition to "
                          "RingTable failed\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingAddNodeToPortVlanRBTree                  */
/*                                                                           */
/*    Description         : This function will add ring node to PortVlanTable*/
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         added node to RBTree.             */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         add node to PortVlanTable         */
/*****************************************************************************/

PUBLIC INT4
ErpsRingAddNodeToPortVlanRBTree (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pRingInfo->u1RingState == ERPS_RING_DISABLED_STATE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddNodeToPortVlanRBTree ring "
                          "Ring is in DISABLE state.\r\n");

        return OSIX_FAILURE;
    }

    /* A ring node will be added at two positions in the Port Vlan based
     * RBTree. To make this happen RBNode itself will be handed over
     * to RBTreeAdd utility.                                       
     * When node is to be added for port1 into RBTree, the comparision
     * between Port1IfIndex will be used to identify its position in the RBTree.
     * When the node is to be added for port2 into RBTree, comparision
     * between Port2IfIndex will tell its appropriate position.     
     * (see comparision function) So, in both of these cases, node will be 
     * added to its given position (that is of offset 0). With this, a 
     * single node will be added at two places in the RBTree. 
     */
    if (RBTreeAdd (gErpsGlobalInfo.RingPortVlanTable,
                   (tRBElem *) & pRingInfo->Port1RBNode) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddNodeToRingRBTree "
                          "node addition to PortVlanTable failed\r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (gErpsGlobalInfo.RingPortVlanTable,
                   (tRBElem *) & pRingInfo->Port2RBNode) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddNodeToRingRBTree "
                          "node addition to PortVlanTable failed\r\n");
        return OSIX_FAILURE;
    }

    if (pRingInfo->u4DistIfIndex != 0)
    {
        if (RBTreeAdd (gErpsGlobalInfo.RingPortVlanTable,
                       (tRBElem *) & pRingInfo->DistPortRBNode) == RB_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingAddNodeToRingRBTree "
                              "node addition to PortVlanTable failed\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingDelNodeFromRBTree                        */
/*                                                                           */
/*    Description         : This function will delete ring node from         */
/*                          RingTable, VlanTable and release the             */
/*                          allocated memory.                                */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         deleted node from RBTree.         */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         delete node from RingTable or     */
/*                                         VlanTable.                        */
/*****************************************************************************/
PUBLIC INT4
ErpsRingDelNodeFromRBTree (tErpsRingInfo * pRingInfo)
{
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (pRingInfo->u4ContextId);

    ErpsRedDelRingDynInfo (pRingInfo);
    if (pContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (gErpsGlobalInfo.RingTable, (tRBElem *) pRingInfo) ==
        RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "RBTreeRemove function failed !!!! \r\n");
    }

    /* Entries get added into Vlan Table only when row status is active.
     * So,this check is done */
    if ((pRingInfo->u1RowStatus == ACTIVE) &&
        (pContextInfo->u1ModuleStatus == OSIX_ENABLED))
    {
        if (ErpsRingRemNodeFromPortVlanTable (pRingInfo) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingDelNodeFromRBTree ring node deletion "
                              "failed from Vlan Table\r\n");
            return OSIX_FAILURE;
        }
    }

    if (pRingInfo->u1MonitoringType == ERPS_MONITOR_MECH_MPLSOAM)
    {
        if ((pRingInfo->u1RowStatus == ACTIVE) &&
            (pContextInfo->u1ModuleStatus == OSIX_ENABLED))
        {
            if (ErpsRingRemNodeFromMepRingIdTable (pRingInfo) == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pRingInfo->u4ContextId,
                                  pRingInfo->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "nmhSetFsErpsRingRowStatus ring "
                                  "failed to remove node from MepRingIdTable\r\n");
                return OSIX_FAILURE;
            }

        }

    }

    MemReleaseMemBlock (gErpsGlobalInfo.VlanGroupListPoolId,
                        (UINT1 *) pRingInfo->pau1RingVlanGroupList);

    if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
    {
        /* RBTree Deletion for SubPortList Table */
        ERPS_DELETE_SUBPORTLIST_TABLE (pRingInfo->pErpsLspPwInfo);

        MemReleaseMemBlock (gErpsGlobalInfo.LspPwInfoListId,
                            (UINT1 *) pRingInfo->pErpsLspPwInfo);
    }

    MemReleaseMemBlock (gErpsGlobalInfo.RingInfoPoolId, (UINT1 *) pRingInfo);

    pRingInfo = NULL;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetNodeFromRingTable                     */
/*                                                                           */
/*    Description         : This function will return node of the RingTable. */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to the ring node of          */
/*                                       ring table.                         */
/*                                       returns NULL, if node is not present*/
/*                                       in the RingTable.                   */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetNodeFromRingTable (tErpsRingInfo * pRingInfo)
{
    return (tErpsRingInfo *) RBTreeGet (gErpsGlobalInfo.RingTable,
                                        (tRBElem *) pRingInfo);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetFirstNodeFrmRingTable                */
/*                                                                           */
/*    Description         : This function will return first node of the      */
/*                          RingTable.                                       */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to the first ring node of    */
/*                                       ring table.                         */
/*                                       returns NULL, if no node is present */
/*                                       in the RingTable.                   */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetFirstNodeFrmRingTable (VOID)
{
    return (tErpsRingInfo *) RBTreeGetFirst (gErpsGlobalInfo.RingTable);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetNextNodeFromRingTable                 */
/*                                                                           */
/*    Description         : This function will return next node of the       */
/*                          RingTable.                                       */
/*                                                                           */
/*    Input(s)            : pCurrentRingEntry - Pointer to ring node.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingNextEntry - pointer to the next ring node of*/
/*                                           ring table.                     */
/*                                           returns NULL, if no next node is*/
/*                                           present in the RingTable.       */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetNextNodeFromRingTable (tErpsRingInfo * pCurrentRingEntry)
{
    return (tErpsRingInfo *)
        RBTreeGetNext (gErpsGlobalInfo.RingTable, pCurrentRingEntry, NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetNodeFromPortVlanTable                 */
/*                                                                           */
/*    Description         : This function will return node of the from       */
/*                          PortVlanTable RBTree.                            */
/*                          This function will return the ring entry only    */
/*                          when the ring is active.                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id,                        */
/*                          u4IfIndex   - Interface Index                    */
/*                          VlanId      - Vlan Id                            */
/*                          u1RingMacId - Mac id of the Ring                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to the ring node of given    */
/*                                      context, port and vlanid from        */
/*                                      PortVlanTable.                       */
/*                                      returns NULL, if ring entry not      */
/*                                      exits, or ring is not active.        */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetNodeFromPortVlanTable (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId VlanId, UINT1 u1RingMacId)
{
    tRBElem            *pRBElem = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;
    INT4                i4RBNodeOffset = 0;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;
    RingEntry.u4Port1IfIndex = u4IfIndex;
    RingEntry.RapsVlanId = VlanId;
    RingEntry.i4Port1RBNodeOffset = -1;
    RingEntry.u1RingMacId = u1RingMacId;

    /* Get the RB node from the PortVlan Table and substract the node offset
     * present in the next 4 byte of the data to get the RingEntry.
     */
    pRBElem = RBTreeGet (gErpsGlobalInfo.RingPortVlanTable,
                         (tRBElem *) & RingEntry.Port1RBNode);

    if (pRBElem != NULL)
    {
        i4RBNodeOffset = *(UINT4 *) ((VOID *) pRBElem + sizeof (tRBNodeEmbd));

        pRingInfo = (tErpsRingInfo *) ((VOID *) pRBElem - i4RBNodeOffset);

        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            return pRingInfo;
        }
    }

    return NULL;
}

/*****************************************************************************
 * Function Name : ErpsPortVlanTblGetFirstNodeOfPortAndVlan
 *
 * Description   : There can be multiple rings associated
 *                 with a port and vlan. 
 *                 This function get the first ring node associated with a
 *                 port and a vlan. 
 *
 * Input(s)      : u4ContextId - virtual context id
 *                 u4IfIndex   - interface index
 *                 VlanId      - vlan id
 *
 * Output(s)     : None
 *
 * Returns       : Pointer to the first ring node associated with the given
 *                 port and vlan. else return NULL.
 *
 *****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsPortVlanTblGetFirstNodeOfPortAndVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          tVlanId VlanId)
{
    tRBElem            *pRBElem = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;
    INT4                i4RBNodeOffset = 0;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;
    RingEntry.u4Port1IfIndex = u4IfIndex;
    RingEntry.RapsVlanId = VlanId;
    RingEntry.i4Port1RBNodeOffset = -1;
    RingEntry.u1RingMacId = 0;    /* intentionaly this index is placed as 0 */

    /* Get the RB node from the PortVlan Table and substract the node offset
     * present in the next 4 byte of the data to get the RingEntry.
     */
    pRBElem = RBTreeGetNext (gErpsGlobalInfo.RingPortVlanTable,
                             (tRBElem *) & RingEntry.Port1RBNode, NULL);

    if (pRBElem != NULL)
    {
        i4RBNodeOffset = *(UINT4 *) ((VOID *) pRBElem + sizeof (tRBNodeEmbd));

        pRingInfo = (tErpsRingInfo *) ((VOID *) pRBElem - i4RBNodeOffset);

        if ((pRingInfo->u4ContextId != u4ContextId) ||
            (pRingInfo->RapsVlanId != VlanId))
        {
            return NULL;
        }
        /* At least one Ring Port should matched with the desired port */
        if ((pRingInfo->u4Port1IfIndex != u4IfIndex) &&
            (pRingInfo->u4Port2IfIndex != u4IfIndex) &&
            (pRingInfo->u4DistIfIndex != u4IfIndex))

        {
            return NULL;
        }

        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            return pRingInfo;
        }
    }

    return NULL;
}

/*****************************************************************************
 * Function Name : ErpsPortVlanTblGetNextNodeOfPortAndVlan
 *
 * Description   : There can be multiple rings associated
 *                 with a port and vlan. if a ring node is passed to this
 *                 function it returns the next  ring node associated with 
 *                 the same port and vlan. else return NULL.
 *
 * Input(s)      : pCurrRingInfo - pointer to the ring node
 *                 u4IfIndex     - interface index
 *
 * Output(s)     : None
 *
 * Returns       : Pointer to the next ring node associated with the same
 *                 port and vlan used by the given ring node, 
 *                 else return NULL.
 *
 *****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsPortVlanTblGetNextNodeOfPortAndVlan (tErpsRingInfo * pCurrRingInfo,
                                         UINT4 u4IfIndex)
{
    tErpsRingInfo       RingEntry;
    tRBElem            *pRBElem = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    INT4                i4RBNodeOffset = 0;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = pCurrRingInfo->u4ContextId;
    RingEntry.u4Port1IfIndex = u4IfIndex;
    RingEntry.RapsVlanId = pCurrRingInfo->RapsVlanId;
    RingEntry.i4Port1RBNodeOffset = -1;
    RingEntry.u1RingMacId = pCurrRingInfo->u1RingMacId;    /* intentionaly this index is placed as 0 */

    pRBElem = RBTreeGetNext (gErpsGlobalInfo.RingPortVlanTable,
                             (tRBElem *) & RingEntry.Port1RBNode, NULL);

    if (pRBElem != NULL)
    {
        i4RBNodeOffset = *(UINT4 *) ((VOID *) pRBElem + sizeof (tRBNodeEmbd));

        pRingInfo = (tErpsRingInfo *) ((VOID *) pRBElem - i4RBNodeOffset);

        if ((pRingInfo->u4ContextId != pCurrRingInfo->u4ContextId) ||
            (pRingInfo->RapsVlanId != pCurrRingInfo->RapsVlanId))
        {
            return NULL;
        }
        if ((pRingInfo->u4Port1IfIndex != u4IfIndex) &&
            (pRingInfo->u4Port2IfIndex != u4IfIndex))
        {
            return NULL;
        }

        if (pRingInfo->u1RowStatus == ACTIVE)
        {
            return pRingInfo;
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingIdCmpFn                                  */
/*                                                                           */
/*    Description         : This is compare function for RingTable RBTree.   */
/*                                                                           */
/*    Input(s)            : pRBElem1 - First RBtree node pointer.            */
/*                          pRBElem2 - Second RBtree node Pointer.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - if first entry is greater than second entry. */
/*                         -1 - if first entry is smaller than second entry. */
/*                          0 - if both first and second entry are same.     */
/*****************************************************************************/
PUBLIC INT4
ErpsRingIdCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tErpsRingInfo      *pRingInfo1 = (tErpsRingInfo *) pRBElem1;
    tErpsRingInfo      *pRingInfo2 = (tErpsRingInfo *) pRBElem2;

    if (pRingInfo1->u4ContextId > pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->u4ContextId < pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_LESS;
    }

    if (pRingInfo1->u4RingId > pRingInfo2->u4RingId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->u4RingId < pRingInfo2->u4RingId)
    {
        return ERPS_CMP_LESS;
    }

    return ERPS_CMP_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingPortVlanCmpFn                            */
/*                                                                           */
/*    Description         : This is compare function for PortVlanTable RBTree*/
/*                                                                           */
/*    Input(s)            : pRBElem1 - First RBtree node pointer.            */
/*                          pRBElem2 - Second RBTrre node Pointer.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - if first entry is greater than second entry. */
/*                         -1 - if first entry is smaller than second entry. */
/*                          0 - if both first and second entry are same.     */
/*****************************************************************************/

PUBLIC INT4
ErpsRingPortVlanCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tErpsRingInfo      *pRingInfo1 = NULL;
    tErpsRingInfo      *pRingInfo2 = NULL;
    INT4                i4Elem1PortRBNodeOffset = 0;
    INT4                i4Elem2PortRBNodeOffset = 0;
    UINT4               u4Ring1IfIndex = 0;
    UINT4               u4Ring2IfIndex = 0;

    /*Compute the offset of Port1RBNode */
    i4Elem1PortRBNodeOffset =
        *(UINT4 *) ((VOID *) pRBElem1 + sizeof (tRBNodeEmbd));

    /* RBNode Offset stored in the RingEntry is the Offset of RBNode present
     * in the tErpsRingInfo data structure. So no need to compute again 
     * offset of the RBNode, substract the offset values to reach the strcuture
     * pointer.
     */

    i4Elem2PortRBNodeOffset =
        *(UINT4 *) ((VOID *) pRBElem2 + sizeof (tRBNodeEmbd));

    pRingInfo2 =
        (tErpsRingInfo *) (VOID *) ((UINT1 *) pRBElem2 -
                                    i4Elem2PortRBNodeOffset);

    /* In ErpsRingGetNodeFromPortVlanTable function, node is formed based on
     * Ring Port1 as index, that needs to be searched. This Port will be 
     * compared with both Ports of the ring entry present in the PortVlanTable.
     * This special need will be identified by offset values as -1.
     * To reach the start address of the tErpsRingInfo substract 
     * Port1RBNode offset.
     */
    if (i4Elem1PortRBNodeOffset == -1)
    {
        pRingInfo1 = (tErpsRingInfo *)
            (pRBElem1 - FSAP_OFFSETOF (tErpsRingInfo, Port1RBNode));
    }
    else
    {
        pRingInfo1 =
            (tErpsRingInfo *) ((VOID *) pRBElem1 - i4Elem1PortRBNodeOffset);
    }

    if (pRingInfo1->u4ContextId > pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->u4ContextId < pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_LESS;
    }

    if ((i4Elem1PortRBNodeOffset == -1) ||
        (i4Elem1PortRBNodeOffset == FSAP_OFFSETOF (tErpsRingInfo, Port1RBNode)))
    {
        u4Ring1IfIndex = pRingInfo1->u4Port1IfIndex;
    }
    else if (i4Elem1PortRBNodeOffset ==
             FSAP_OFFSETOF (tErpsRingInfo, Port2RBNode))
    {
        u4Ring1IfIndex = pRingInfo1->u4Port2IfIndex;
    }
    else
    {
        u4Ring1IfIndex = pRingInfo1->u4DistIfIndex;
    }

    if (i4Elem1PortRBNodeOffset == -1)
    {
        /* If any of the ring node ports and Raps Vlan matches with the 
         * given ring Port and Vlan, return the found status.
         */
        if (((u4Ring1IfIndex == pRingInfo2->u4Port1IfIndex) ||
             (u4Ring1IfIndex == pRingInfo2->u4Port2IfIndex) ||
             (u4Ring1IfIndex == pRingInfo2->u4DistIfIndex)) &&
            (pRingInfo1->RapsVlanId == pRingInfo2->RapsVlanId) &&
            (pRingInfo1->u1RingMacId == pRingInfo2->u1RingMacId))
        {
            return ERPS_CMP_EQUAL;
        }
    }

    /* Find the ring node's Interface based on offset of the node pointer,
     * If it is first offset, then first interface should be considered     
     * If it is the second offset, then second interface should be considered
     */
    if (i4Elem2PortRBNodeOffset == FSAP_OFFSETOF (tErpsRingInfo, Port1RBNode))
    {
        u4Ring2IfIndex = pRingInfo2->u4Port1IfIndex;
    }
    else if (i4Elem2PortRBNodeOffset ==
             FSAP_OFFSETOF (tErpsRingInfo, Port2RBNode))
    {
        u4Ring2IfIndex = pRingInfo2->u4Port2IfIndex;
    }
    else
    {
        u4Ring2IfIndex = pRingInfo2->u4DistIfIndex;
    }

    if (u4Ring1IfIndex > u4Ring2IfIndex)
    {
        return ERPS_CMP_GREATER;
    }

    if (u4Ring1IfIndex < u4Ring2IfIndex)
    {
        return ERPS_CMP_LESS;
    }

    if (pRingInfo1->RapsVlanId > pRingInfo2->RapsVlanId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->RapsVlanId < pRingInfo2->RapsVlanId)
    {
        return ERPS_CMP_LESS;
    }
    if (pRingInfo1->u1RingMacId > pRingInfo2->u1RingMacId)
    {
        return ERPS_CMP_GREATER;
    }
    else if (pRingInfo1->u1RingMacId < pRingInfo2->u1RingMacId)
    {
        return ERPS_CMP_LESS;
    }

    return ERPS_CMP_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingRemNodeFromPortVlanTable              */
/*                                                                           */
/*    Description         : This function will remove ring node from         */
/*                          PortVlanTable.                                   */
/*                                                                           */
/*    Input(s)            : pRingInfo - Node that needs to be removed from  */
/*                                       the PortVlanTable.                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         disable state machine.            */
/*                          OSIX_FAILURE - State machine returns failure.    */
/*****************************************************************************/

PUBLIC INT4
ErpsRingRemNodeFromPortVlanTable (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo->u1RingState != ERPS_RING_DISABLED_STATE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingRemNodeFromPortVlanTable ring "
                          "Ring is already active.\r\n");

        return OSIX_FAILURE;
    }

    if (RBTreeRemove (gErpsGlobalInfo.RingPortVlanTable,
                      (tRBElem *) & pRingInfo->Port1RBNode) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingRemNodeFromPortVlanTable ring "
                          "Port1 node deletion failed\r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (gErpsGlobalInfo.RingPortVlanTable,
                      (tRBElem *) & pRingInfo->Port2RBNode) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingRemNodeFromPortVlanTable ring "
                          "Port2 node deletion failed\r\n");
        return OSIX_FAILURE;
    }

    if (pRingInfo->u4DistIfIndex != 0)
    {
        if (RBTreeRemove (gErpsGlobalInfo.RingPortVlanTable,
                          (tRBElem *) & pRingInfo->DistPortRBNode) ==
            RB_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingRemNodeFromPortVlanTable ring "
                              "Distribute port node deletion failed\r\n");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingDyanamicSllCmpFn                         */
/*                                                                           */
/*    Description         : This function is compare function for dynamic Sll*/
/*                                                                           */
/*    Input(s)            : pRBElem1 - First RBtree node pointer.            */
/*                          pRBElem2 - Second RBTrre node Pointer.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - if second entry is greater than first entry. */
/*                         -1 - if second entry is smaller than first entry. */
/*                          0 - if both first and second entry are same.     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsRingDyanamicSllCmpFn (tDynSLLElem * pDynElem1, tDynSLLElem * pDynElem2)
{
    tErpsRingInfo      *pRingInfo1 = (tErpsRingInfo *) pDynElem1;
    tErpsRingInfo      *pRingInfo2 = (tErpsRingInfo *) pDynElem2;

    if (pRingInfo2->u4RingId > pRingInfo1->u4RingId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo2->u4RingId < pRingInfo1->u4RingId)
    {
        return ERPS_CMP_LESS;
    }

    return ERPS_CMP_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingDeleteAllRingsInContext                  */
/*                                                                           */
/*    Description         : This function will delete all the ring nodes of  */
/*                          the given context.                               */
/*                                                                           */
/*    Input(s)            : *pContext - Pointer to the Context Info.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         deleted ring nodes of given ctxt. */
/*                          OSIX_FAILURE - If Ring node deletion failed.     */
/*****************************************************************************/
PUBLIC INT4
ErpsRingDeleteAllRingsInContext (UINT4 u4ContextId)
{
    tErpsRingInfo       NextRingEntry;
    tErpsRingInfo      *pCurrRingEntry = NULL;
    tErpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ErpsCxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&NextRingEntry, 0, sizeof (tErpsRingInfo));

    NextRingEntry.u4ContextId = u4ContextId;

    /* Increment the sync counter to block syncing NP call info
     * to standby node. As this function will invoke multple
     * NPAPI call from this thread.
     */

    ErpsRedHwAuditIncBlkCounter ();

    /* Sync the DeleteAll Ring action to Stanby node allow
     * standby taking same action as Active node.
     * If Active failed, before completing this process, standby will
     * complete the this process without getting affected by 
     * Active node failure.
     */

    ErpsRedSyncDeleteAllRingInCtxt (u4ContextId);

    pCurrRingEntry = ErpsRingGetNextNodeFromRingTable (&NextRingEntry);

    while (pCurrRingEntry)
    {
        if (pCurrRingEntry->u4ContextId != u4ContextId)
        {
            break;
        }

        MEMCPY (&NextRingEntry, pCurrRingEntry, sizeof (tErpsRingInfo));

        /* If the ERPS is enabled on the context, then the
         * Ring has to be deleted from the hardware and disabled
         * from the SEM */

        if (pContextInfo->u1ModuleStatus == OSIX_ENABLED)
        {
            if (ErpsRingDeActivateRing (pCurrRingEntry, DESTROY)
                == OSIX_FAILURE)
            {
                ERPS_CONTEXT_TRC (pCurrRingEntry->u4ContextId,
                                  pCurrRingEntry->u4RingId,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "ErpsRingDeleteAllRingsInContext ring node "
                                  "Ring DeActivate failed\r\n");
            }
        }

        if (ErpsRingDelNodeFromRBTree (pCurrRingEntry) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pCurrRingEntry->u4ContextId,
                              pCurrRingEntry->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingDeleteAllRingsInContext ring node "
                              "deletion failed\r\n");
        }

        pCurrRingEntry = ErpsRingGetNextNodeFromRingTable (&NextRingEntry);
    }

    /* Decrement the sync counter to allow syncing NP call info
     * to standby node. 
     */
    ErpsRedHwAuditDecBlkCounter ();

    /* if vlan group manager is erps, then update L2IWF and HW */
    if (pContextInfo->u1VlanGroupManager == ERPS_VLAN_GROUP_MANAGER_ERPS)
    {
        if (ErpsUtilDelVlanGroupTable (u4ContextId) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingAddTcNode                                */
/*                                                                           */
/*    Description         : This function will given ring node into TC list  */
/*                          of the given subRing.                            */
/*                                                                           */
/*    Input(s)            : pSubRingEntry - Pointer to sub-Ring node.        */
/*                          pRingInfo - Pointer to the ring node.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         added ring node into tc list.     */
/*                          OSIX_FAILURE - If Ring node failed to add given  */
/*                                         ring node.                        */
/*****************************************************************************/
PUBLIC INT4
ErpsRingAddTcNode (tErpsRingInfo * pSubRingEntry, tErpsRingInfo * pRingInfo)
{
    /* Node is  Populated in *(tErpsRingInfo *)pDynSllList->Tail->pElem */
    if (UtlDynSllAdd (pSubRingEntry->TcPropRingList, pRingInfo)
        == DYN_SLL_FAILURE)
    {
        ERPS_CONTEXT_TRC (pSubRingEntry->u4ContextId, pSubRingEntry->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddTcNode ring %d addition to tc list "
                          "failed\r\n", pRingInfo->u4RingId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingDelTcNode                                */
/*                                                                           */
/*    Description         : This function will remove ring node into TC list */
/*                          of the given subRing TC list.                    */
/*                                                                           */
/*    Input(s)            : pSubRingEntry - Pointer to sub-Ring node.        */
/*                          pRingInfo - Pointer to the ring node.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         deleted ring node from tc list.   */
/*                          OSIX_FAILURE - If Ring node failed to delete     */
/*                                         given ring node.                  */
/*****************************************************************************/
PUBLIC INT4
ErpsRingDelTcNode (tErpsRingInfo * pSubRingEntry, tErpsRingInfo * pRingInfo)
{
    if (UtlDynSllDelete (pSubRingEntry->TcPropRingList, pRingInfo) == NULL)
    {
        ERPS_CONTEXT_TRC (pSubRingEntry->u4ContextId, pSubRingEntry->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingDelTcNode ring %d deletion from tc list "
                          "failed\r\n", pRingInfo->u4RingId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetTcNode                                */
/*                                                                           */
/*    Description         : This function will TC ring node from the TC list */
/*                          of the given subRing TC list.                    */
/*                                                                           */
/*    Input(s)            : pSubRingEntry - Pointer to sub-Ring node.        */
/*                          pRingInfo - Pointer to the TC node info that     */
/*                                       needs to be searched.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : TC node from the TC list of the sub-Ring.        */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetTcNode (tErpsRingInfo * pSubRingEntry, tErpsRingInfo * pRingInfo)
{
    return (tErpsRingInfo *) UtlDynSllGet (pSubRingEntry->TcPropRingList,
                                           pRingInfo);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetFirstTcNode                           */
/*                                                                           */
/*    Description         : This function will return the first node from TC */
/*                          list of the given sub ring.                      */
/*                                                                           */
/*    Input(s)            : pSubRingEntry - Pointer to sub-Ring node.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to ring node, if ring        */
/*                                       entries are present in the tc list. */
/*                                       return NULL, if there is no entry   */
/*                                       present in the ring.                */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetFirstTcNode (tErpsRingInfo * pSubRingEntry)
{
    return (tErpsRingInfo *) UtlDynSllFirst (pSubRingEntry->TcPropRingList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetNextTcNode                            */
/*                                                                           */
/*    Description         : This function will returns next node from TC     */
/*                          list of the given sub ring.                      */
/*                                                                           */
/*    Input(s)            : pSubRingEntry - Pointer to sub-Ring node.        */
/*                          pRingInfo - Pointer to the ring node.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingNextEntry - pointer to ring node, if next   */
/*                                       entry is present in the tc list.    */
/*                                       return NULL, if there is no entry   */
/*                                       present in the ring.                */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetNextTcNode (tErpsRingInfo * pSubRingEntry, tErpsRingInfo * pRingInfo)
{
    return (tErpsRingInfo *)
        UtlDynSllGetNext (pSubRingEntry->TcPropRingList, pRingInfo, NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingClearRingStats                           */
/*                                                                           */
/*    Description         : This function will reset all the portstats       */
/*                          counter in the ring table.                       */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to the ring node.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsRingClearRingStats (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo == NULL)
    {
        return;
    }

    MEMSET (&pRingInfo->Port1Stats, 0, sizeof (tErpsPortStats));
    MEMSET (&pRingInfo->Port2Stats, 0, sizeof (tErpsPortStats));
    MEMSET (&pRingInfo->u4TrapsCount,0,sizeof(pRingInfo->u4TrapsCount));

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingActivateRing                             */
/*                                                                           */
/*    Description         : This function will activate the given rings      */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to the ring node.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsRingActivateRing (tErpsRingInfo * pRingInfo)
{
    UINT4               u4Index = 0;
    UINT1               u1Result = OSIX_FALSE;

    if (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present)
    {
        ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port1IfIndex,
                                            ERPS_MODULE, L2IWF_SET);

        if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
        {
            /*When the ownership of the left RAPS pseudowire is set to be ERPS
             *the ownership of the corresponding protected data pseudowires
             *also need to be modified*/

            for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
            {
                ERPS_IS_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo, u4Index,
                                       u1Result);

                if (u1Result == OSIX_TRUE)
                {
                    ErpsPortL2IwfSetPortStateCtrlOwner ((u4Index +
                                                         CFA_MIN_PSW_IF_INDEX -
                                                         1), ERPS_MODULE,
                                                        L2IWF_SET);
                }

            }
        }
    }

    if ((pRingInfo->u4Port2IfIndex != 0)
        && (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present))
    {
        ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port2IfIndex,
                                            ERPS_MODULE, L2IWF_SET);

        if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
        {

            /*When the ownership of the right RAPS pseudowire is set to be ERPS
             *the ownership of the corresponding protected data pseudowires
             *also need to be modified*/

            for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS); u4Index++)
            {
                ERPS_IS_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo, u4Index,
                                       u1Result);

                if (u1Result == OSIX_TRUE)
                {
                    ErpsPortL2IwfSetPortStateCtrlOwner ((u4Index +
                                                         CFA_MIN_PSW_IF_INDEX -
                                                         1), ERPS_MODULE,
                                                        L2IWF_SET);
                }

            }
        }
    }

    /* Create the ring entry in hardware */
    if (ErpsUtilConfRingEntryInHw (pRingInfo, ERPS_HW_RING_CREATE,
                                   PORT1_PORT2_UPDATED) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingActivateRing ring addition to HW Fail\r\n");
        return OSIX_FAILURE;
    }

    if ((pRingInfo->u1IsPort1Present == ERPS_PORT_IN_REMOTE_LINE_CARD) ||
        (pRingInfo->u1IsPort2Present == ERPS_PORT_IN_REMOTE_LINE_CARD))
    {
        pRingInfo->u1DERPSSyncReqFlag = OSIX_TRUE;
    }

    /* Enable the State Machine */
    if (ErpsSmRingStateMachine (ERPS_RING_INIT_EVENT, pRingInfo,
                                NULL) == OSIX_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingActivateRing ring enabling state "
                          "machine failed\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingDeActivateRing                           */
/*                                                                           */
/*    Description         : This function will deactivate the given rings    */
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to the ring node.         */
/*                    u1NewRowStatus - Row Status of ring node         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsRingDeActivateRing (tErpsRingInfo * pRingInfo, UINT1 u1NewRowStatus)
{
    tErpsRingInfo      *pNextRingEntry = NULL;
    tErpsRingInfo       RingNode;
    UINT4               u4Index = 0;
    BOOL1               bPort1UsageFlag = OSIX_FALSE;
    BOOL1               bPort2UsageFlag = OSIX_FALSE;
    UINT1               u1Result = OSIX_FALSE;

    if (pRingInfo->u1RowStatus == ACTIVE)
    {
        if (ErpsSmRingStateMachine (ERPS_RING_DISABLE_EVENT, pRingInfo, NULL)
            == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingDeActivateRing ring "
                              "disabling state machine failed\r\n");
            return OSIX_FAILURE;
        }
        /* Delete the ring entry from hardware */
        if (ErpsUtilConfRingEntryInHw (pRingInfo, ERPS_HW_RING_DELETE,
                                       PORT1_PORT2_UPDATED) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingDeActivateRing ring deletion of ring from "
                              "HW failed\r\n");
            return OSIX_FAILURE;
        }
    }

    if (u1NewRowStatus == DESTROY)
    {
        /* Before removing the port ownership check the following -
         * - If any other ring is associated with this port1 or port2
         * and that ring is in actie state, then the port ownership should not be 
         * removed.
         */

        MEMSET (&RingNode, 0, sizeof (tErpsRingInfo));
        RingNode.u4ContextId = pRingInfo->u4ContextId;
        RingNode.u4RingId = 0;    /* For Partial Indexing keep it as 0 */

        /* Loop through the ring  table to scan the active rings */

        /* Get the first entry in the context */
        pNextRingEntry = ErpsRingGetNextNodeFromRingTable (&RingNode);

        while (pNextRingEntry != NULL)
        {
            if (pNextRingEntry->u4ContextId != pRingInfo->u4ContextId)
            {
                break;
            }

	    /* Raps Vlan Id check is introduced to reset the port owner to STP
	     * Because when only one ring instance is present, RingInfo and NextRingInfo
	     * will have same entries and variable bPort1UsageFlag or bPort2UsageFlag 
	     * will se bet as TRUE. If bPort1UsageFlag/bPort2UsageFlag is true then 
	     * Port Owner reset function will not be called. To avoid this issue, 
	     * Raps vlan id check is added */
            if (((pRingInfo->u4Port1IfIndex == pNextRingEntry->u4Port1IfIndex) ||
                (pRingInfo->u4Port1IfIndex == pNextRingEntry->u4Port2IfIndex))&&
		(pRingInfo->RapsVlanId != pNextRingEntry->RapsVlanId))
            {
                /* Port1 is used by other active rings in the context */
                bPort1UsageFlag = OSIX_TRUE;
            }

            if (((pRingInfo->u4Port2IfIndex == pNextRingEntry->u4Port1IfIndex) ||
                (pRingInfo->u4Port2IfIndex == pNextRingEntry->u4Port2IfIndex))&&
		(pRingInfo->RapsVlanId != pNextRingEntry->RapsVlanId))
            {
                /* Port2 is used by other active rings in the context */
                bPort2UsageFlag = OSIX_TRUE;
            }

            if ((bPort1UsageFlag == OSIX_TRUE)
                || (bPort2UsageFlag == OSIX_TRUE))
            {
                break;
            }

            pNextRingEntry = ErpsRingGetNextNodeFromRingTable (pNextRingEntry);
        }

        /* re-set the port ownership if no other ring instance is using this port1 */
        if ((bPort1UsageFlag == OSIX_FALSE)
            && (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort1Present))
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port1IfIndex,
                                  ERPS_PORT_UNBLOCKING);

            ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port1IfIndex,
                                                ERPS_MODULE, L2IWF_RESET);

            /*When the ownership of the left RAPS pseudowire is resetted 
             *the ownership of the corresponding protected data pseudowires
             *also need to be modified*/

            if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
            {
                for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS);
                     u4Index++)
                {
                    ERPS_IS_SUBPORT_PORT1 (pRingInfo->pErpsLspPwInfo, u4Index,
                                           u1Result);

                    if (u1Result == OSIX_TRUE)
                    {

                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port1IfIndex,
                                              ERPS_PORT_UNBLOCKING);

                        ErpsPortL2IwfSetPortStateCtrlOwner ((u4Index +
                                                             CFA_MIN_PSW_IF_INDEX
                                                             - 1), ERPS_MODULE,
                                                            L2IWF_RESET);
                    }

                }

            }
        }

        /* re-set the port ownership if no other ring instance is using this port2 */
        if ((bPort2UsageFlag == OSIX_FALSE)
            && (ERPS_PORT_IN_LOCAL_LINE_CARD == pRingInfo->u1IsPort2Present))
        {
            ErpsUtilSetPortState (pRingInfo, pRingInfo->u4Port2IfIndex,
                                  ERPS_PORT_UNBLOCKING);
            ErpsPortL2IwfSetPortStateCtrlOwner (pRingInfo->u4Port2IfIndex,
                                                ERPS_MODULE, L2IWF_RESET);

            /*When the ownership of the right RAPS pseudowire is resetted 
             *the ownership of the corresponding protected data pseudowires
             *also need to be modified*/

            if (pRingInfo->u1ServiceType == ERPS_SERVICE_MPLS_LSP_PW)
            {

                for (u4Index = 1; u4Index <= (ERPS_MAX_SIZING_SUB_PORTS);
                     u4Index++)
                {
                    ERPS_IS_SUBPORT_PORT2 (pRingInfo->pErpsLspPwInfo, u4Index,
                                           u1Result);

                    if (u1Result == OSIX_TRUE)
                    {
                        ErpsUtilSetPortState (pRingInfo,
                                              pRingInfo->u4Port2IfIndex,
                                              ERPS_PORT_UNBLOCKING);
                        ErpsPortL2IwfSetPortStateCtrlOwner ((u4Index +
                                                             CFA_MIN_PSW_IF_INDEX
                                                             - 1), ERPS_MODULE,
                                                            L2IWF_RESET);
                    }

                }

            }
        }
        /* Delete the ring entry from hardware */
        if (ErpsUtilConfRingEntryInHw (pRingInfo, ERPS_HW_RING_DELETE,
                                       PORT1_PORT2_UPDATED) == OSIX_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingDeActivateRing ring deletion of ring from "
                              "HW failed\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingGetRingEntry                             */
/*                                                                           */
/*    Description         : This function will return the ring entry based   */
/*                          on the Context ID and Ring ID                    */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          u4RingId     - Ring Identifier                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pRingInfo - pointer to the ring node of         */
/*                                       ring table.                         */
/*                                       returns NULL, if node is not present*/
/*                                       in the RingTable.                   */
/*****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsRingGetRingEntry (UINT4 u4ContextId, UINT4 u4RingId)
{
    tErpsRingInfo       RingInfo;
    tErpsRingInfo      *pRingInfo = NULL;

    MEMSET (&RingInfo, 0, sizeof (tErpsRingInfo));

    if (ErpsCxtGetContextEntry (u4ContextId) == NULL)
    {
        return NULL;
    }

    RingInfo.u4ContextId = u4ContextId;
    RingInfo.u4RingId = u4RingId;

    pRingInfo = (ErpsRingGetNodeFromRingTable (&RingInfo));
    return pRingInfo;
}

/*****************************************************************************/
/* Function Name      : ErpsRingGetSubRingEntry                              */
/*                                                                           */
/* Description        : This function is called for getting the subring entry*/
/*                                                                           */
/* Input(s)           :u4RingId                                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Ring Entry   - On success                            */
/*                      NULL           - On Failure                          */
/*****************************************************************************/
tSubRingEntry      *
ErpsGetSubRingEntry (tErpsRingInfo * pRingInfo, UINT4 u4SubRingId)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tSubRingEntry      *pSubRingEntry = NULL;

    pSllNode = TMO_SLL_First (&pRingInfo->SubRingListHead);

    while (pSllNode != NULL)
    {
        pSubRingEntry = (tSubRingEntry *) pSllNode;
        /* If the subring entry's id is equal to incoming subring id 
         * then return the entry */
        if (pSubRingEntry->u4SubRingId == u4SubRingId)
        {
            return pSubRingEntry;
        }

        pSllNode = TMO_SLL_Next (&pRingInfo->SubRingListHead, pSllNode);

    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsMepIdCmpFn                                   */
/*                                                                           */
/*    Description         : This is compare function for MepRingId RBTree    */
/*                                                                           */
/*    Input(s)            : pRBElem1 - First RBtree node pointer.            */
/*                          pRBElem2 - Second RBTrre node Pointer.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - if first entry is greater than second entry. */
/*                         -1 - if first entry is smaller than second entry. */
/*                          0 - if both first and second entry are same.     */
/*****************************************************************************/
PUBLIC INT4
ErpsMepIdCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tErpsRingInfo      *pRingInfo1 = NULL;
    tErpsRingInfo      *pRingInfo2 = NULL;
    INT4                i4Elem1PortRBNodeOffset = 0;
    INT4                i4Elem2PortRBNodeOffset = 0;
    UINT4               u4Ring1MegId = 0;
    UINT4               u4Ring1MeId = 0;
    UINT4               u4Ring1MepId = 0;
    UINT4               u4Ring2MegId = 0;
    UINT4               u4Ring2MeId = 0;
    UINT4               u4Ring2MepId = 0;

    /*Compute the offset of Port1RBNode */
    i4Elem1PortRBNodeOffset =
        *(UINT4 *) ((VOID *) pRBElem1 + sizeof (tRBNodeEmbd));

    /* RBNode Offset stored in the RingEntry is the Offset of RBNode present
     * in the tErpsRingInfo data structure. So no need to compute again
     * offset of the RBNode, substract the offset values to reach the strcuture
     * pointer.
     */

    i4Elem2PortRBNodeOffset =
        *(UINT4 *) ((VOID *) pRBElem2 + sizeof (tRBNodeEmbd));

    pRingInfo2 =
        (tErpsRingInfo *) (VOID *) ((UINT1 *) pRBElem2 -
                                    i4Elem2PortRBNodeOffset);

    if (i4Elem1PortRBNodeOffset == -1)
    {
        pRingInfo1 = (tErpsRingInfo *)
            (((VOID *) pRBElem1) -
             FSAP_OFFSETOF (tErpsRingInfo, Port1MepRBNode));
    }
    else
    {
        pRingInfo1 =
            (tErpsRingInfo *) ((VOID *) pRBElem1 - i4Elem1PortRBNodeOffset);
    }

    if (pRingInfo1->u4ContextId > pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->u4ContextId < pRingInfo2->u4ContextId)
    {
        return ERPS_CMP_LESS;
    }

    if ((i4Elem1PortRBNodeOffset == -1) ||
        (i4Elem1PortRBNodeOffset ==
         FSAP_OFFSETOF (tErpsRingInfo, Port1MepRBNode)))
    {
        u4Ring1MegId = pRingInfo1->CfmEntry.u4Port1MEGId;
        u4Ring1MeId = pRingInfo1->CfmEntry.u4Port1MEId;
        u4Ring1MepId = pRingInfo1->CfmEntry.u4Port1MEPId;

    }
    else if (i4Elem1PortRBNodeOffset ==
             FSAP_OFFSETOF (tErpsRingInfo, Port2MepRBNode))
    {
        u4Ring1MegId = pRingInfo1->CfmEntry.u4Port2MEGId;
        u4Ring1MeId = pRingInfo1->CfmEntry.u4Port2MEId;
        u4Ring1MepId = pRingInfo1->CfmEntry.u4Port2MEPId;
    }
    if (i4Elem1PortRBNodeOffset == -1)
    {
        /* If any of the MEPs of port1 or port2 and RingID matches with the
         * given MEP, return the found status.
         */
        if (((((u4Ring1MegId == pRingInfo2->CfmEntry.u4Port1MEGId) &&
               (u4Ring1MeId == pRingInfo2->CfmEntry.u4Port1MEId) &&
               (u4Ring1MepId == pRingInfo2->CfmEntry.u4Port1MEPId)) ||
              ((u4Ring1MegId == pRingInfo2->CfmEntry.u4Port2MEGId) &&
               (u4Ring1MeId == pRingInfo2->CfmEntry.u4Port2MEId) &&
               (u4Ring1MepId == pRingInfo2->CfmEntry.u4Port2MEPId))) &&
             (pRingInfo2->u4RingId == pRingInfo1->u4RingId)))
        {
            return ERPS_CMP_EQUAL;
        }
    }

    /* Find the ring node's Interface based on offset of the node pointer,
     * If it is first offset, then first interface should be considered
     * If it is the second offset, then second interface should be considered
     */
    if (i4Elem2PortRBNodeOffset ==
        FSAP_OFFSETOF (tErpsRingInfo, Port1MepRBNode))
    {
        u4Ring2MegId = pRingInfo2->CfmEntry.u4Port1MEGId;
        u4Ring2MeId = pRingInfo2->CfmEntry.u4Port1MEId;
        u4Ring2MepId = pRingInfo2->CfmEntry.u4Port1MEPId;

    }
    else if (i4Elem2PortRBNodeOffset ==
             FSAP_OFFSETOF (tErpsRingInfo, Port2MepRBNode))
    {
        u4Ring2MegId = pRingInfo2->CfmEntry.u4Port2MEGId;
        u4Ring2MeId = pRingInfo2->CfmEntry.u4Port2MEId;
        u4Ring2MepId = pRingInfo2->CfmEntry.u4Port2MEPId;
    }

    if (u4Ring1MegId > u4Ring2MegId)
    {
        return ERPS_CMP_GREATER;
    }

    if (u4Ring1MegId < u4Ring2MegId)
    {
        return ERPS_CMP_LESS;
    }

    if (u4Ring1MeId > u4Ring2MeId)
    {
        return ERPS_CMP_GREATER;
    }

    if (u4Ring1MeId < u4Ring2MeId)
    {
        return ERPS_CMP_LESS;
    }

    if (u4Ring1MepId > u4Ring2MepId)
    {
        return ERPS_CMP_GREATER;
    }

    if (u4Ring1MepId < u4Ring2MepId)
    {
        return ERPS_CMP_LESS;
    }

    if (pRingInfo1->u4RingId > pRingInfo2->u4RingId)
    {
        return ERPS_CMP_GREATER;
    }

    if (pRingInfo1->u4RingId < pRingInfo2->u4RingId)
    {
        return ERPS_CMP_LESS;
    }

    return ERPS_CMP_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingAddNodeToMepRingIdRBTree                 */
/*                                                                           */
/*    Description         : This function will add ring node to MEPRingIdTabl*/
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         added node to RBTree.             */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         add node                          */
/*****************************************************************************/
PUBLIC INT4
ErpsRingAddNodeToMepRingIdRBTree (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* A ring node will be added at two positions in the Mep RingId Table based
     * RBTree. To make this happen RBNode itself will be handed over
     * to RBTreeAdd utility.
     * When node is to be added for port1 into RBTree, the comparision
     * between Port1MEPs will be used to identify its position in the RBTree.
     * When the node is to be added for port2 into RBTree, comparision
     * between Port2MEPs will tell its appropriate position.
     * (see comparision function) So, in both of these cases, node will be
     * added to its given position (that is of offset 0). With this, a
     * single node will be added at two places in the RBTree.
     */
    if (RBTreeAdd (gErpsGlobalInfo.RingMepTable,
                   (tRBElem *) & (pRingInfo->Port1MepRBNode)) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingAddNodeToMepRingIdRBTree "
                          "node addition to MepRingIdTable for MEG = %d ME = %d MEP =%d"
                          "failed\r\n", pRingInfo->CfmEntry.u4Port1MEGId,
                          pRingInfo->CfmEntry.u4Port1MEId,
                          pRingInfo->CfmEntry.u4Port1MEPId);

        return OSIX_FAILURE;
    }

    if (pRingInfo->u4Port2IfIndex != 0)
    {
        if (RBTreeAdd (gErpsGlobalInfo.RingMepTable,
                       (tRBElem *) & (pRingInfo->Port2MepRBNode)) == RB_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingAddNodeToMepRingIdRBTree "
                              "node addition to MepRingIdTable for MEG = %d ME = %d MEP =%d"
                              "failed\r\n", pRingInfo->CfmEntry.u4Port2MEGId,
                              pRingInfo->CfmEntry.u4Port2MEId,
                              pRingInfo->CfmEntry.u4Port2MEPId);

            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : ErpsRingRemNodeFromMepRingIdTable                */
/*                                                                           */
/*    Description         : This function will remove node from MEPRingId tabl*/
/*                                                                           */
/*    Input(s)            : pRingInfo - Pointer to ring node.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         deletes node                      */
/*                          OSIX_FAILURE - When this function fails to       */
/*                                         delete node                       */
/*****************************************************************************/
PUBLIC INT4
ErpsRingRemNodeFromMepRingIdTable (tErpsRingInfo * pRingInfo)
{
    if (pRingInfo->u1RingState != ERPS_RING_DISABLED_STATE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingRemNodeFromMepRingIdTable"
                          "Ring is already active.\r\n");

        return OSIX_FAILURE;
    }

    if (RBTreeRemove (gErpsGlobalInfo.RingMepTable,
                      (tRBElem *) & pRingInfo->Port1MepRBNode) == RB_FAILURE)
    {
        ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "ErpsRingRemNodeFromMepRingIdTable"
                          "Port1 node deletion failed\r\n");
        return OSIX_FAILURE;
    }

    if (pRingInfo->u4Port2IfIndex != 0)
    {
        if (RBTreeRemove (gErpsGlobalInfo.RingMepTable,
                          (tRBElem *) & pRingInfo->Port2MepRBNode) ==
            RB_FAILURE)
        {
            ERPS_CONTEXT_TRC (pRingInfo->u4ContextId, pRingInfo->u4RingId,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "ErpsRingRemNodeFromMepRingIdTable"
                              "Port2 node deletion failed\r\n");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : ErpsGetNodeOfMepAndRingId
 *
 * Description   : There can be multiple rings associated
 *                 with a particular MEP.
 *                 This function gets the ring node with a particular MEP
 *
 * Input(s)      : u4ContextId - virtual context id
 *                 u4MegId - Meg Id
 *                 u4MeId  - Me Id 
 *                 u4MepId - Mep Id
 *                 u4RingId  - Ring Id
 *
 * Output(s)     : None
 *
 * Returns       : Pointer to the ring node associated with the given
 *                 MEP else returns NULL.
 *
 *****************************************************************************/
PUBLIC tErpsRingInfo *
ErpsGetNodeOfMepAndRingId (UINT4 u4ContextId, UINT4 u4MegId,
                           UINT4 u4MeId, UINT4 u4MepId, UINT4 u4RingId)
{
    tRBElem            *pRBElem = NULL;
    tErpsRingInfo      *pRingInfo = NULL;
    tErpsRingInfo       RingEntry;
    INT4                i4RBNodeOffset = 0;

    MEMSET (&RingEntry, 0, sizeof (tErpsRingInfo));

    RingEntry.u4ContextId = u4ContextId;
    RingEntry.CfmEntry.u4Port1MEGId = u4MegId;
    RingEntry.CfmEntry.u4Port1MEId = u4MeId;
    RingEntry.CfmEntry.u4Port1MEPId = u4MepId;
    RingEntry.i4Port1MepRBNodeOffset = -1;
    RingEntry.u4RingId = u4RingId;

    pRBElem = RBTreeGetNext (gErpsGlobalInfo.RingMepTable,
                             (tRBElem *) & RingEntry.Port1MepRBNode, NULL);

    if (pRBElem != NULL)
    {
        i4RBNodeOffset = *(UINT4 *) ((VOID *) pRBElem + sizeof (tRBNodeEmbd));

        pRingInfo = (tErpsRingInfo *) ((VOID *) pRBElem - i4RBNodeOffset);

        if (pRingInfo->u4ContextId != u4ContextId)
        {
            ERPS_GLOBAL_TRC
                ("ErpsGetNodeOfMepAndRingId: Context Id does not match\r\n");
            return NULL;
        }
        /* At least one of the Ring Port MEPS should matched with the desired MEP */
        if (((pRingInfo->CfmEntry.u4Port1MEGId != u4MegId) ||
             (pRingInfo->CfmEntry.u4Port1MEId != u4MeId) ||
             (pRingInfo->CfmEntry.u4Port1MEPId != u4MepId)) &&
            ((pRingInfo->CfmEntry.u4Port2MEGId != u4MegId) ||
             (pRingInfo->CfmEntry.u4Port2MEId != u4MeId) ||
             (pRingInfo->CfmEntry.u4Port2MEPId != u4MepId)))

        {
            ERPS_GLOBAL_TRC
                ("ErpsGetNodeOfMepAndRingId: Meg Information does not match\r\n");
            return NULL;
        }

        return pRingInfo;
    }

    return NULL;
}
