/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: erpsnpwr.c,v 1.2 2013/02/02 10:06:06 siva Exp $
*
* Description: This file contains the ERPS NPAPI related wrapper routines.
*
*****************************************************************************/
#ifndef _ERPS_NP_WR_C_
#define _ERPS_NP_WR_C_

#include "erpsinc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : ErpsNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tErpsNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
ErpsNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tErpsNpModInfo     *pErpsNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pErpsNpModInfo = &(pFsHwNp->ErpsNpModInfo);

    if (NULL == pErpsNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_ERPS_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsHwRingConfig *pEntry = NULL;
            pEntry = &pErpsNpModInfo->ErpsNpFsErpsHwRingConfig;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsErpsHwRingConfig (pEntry->pErpsHwRingInfo);
            break;
        }
#ifdef MBSM_WANTED
        case FS_ERPS_MBSM_HW_RING_CONFIG:
        {
            tErpsNpWrFsErpsMbsmHwRingConfig *pEntry = NULL;
            pEntry = &pErpsNpModInfo->ErpsNpFsErpsMbsmHwRingConfig;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsErpsMbsmHwRingConfig (pEntry->pErpsHwRingInfo,
                                        pEntry->pErpsMbsmInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _ERPS_NP_WR_C_ */
