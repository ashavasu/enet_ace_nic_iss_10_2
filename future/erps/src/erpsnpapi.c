/**************************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: erpsnpapi.c,v 1.1 2013/02/02 10:08:19 siva Exp $
*
* Description: All Network Processor API Function calls are done here
*
*****************************************************************************/
#ifndef _ERPS_NP_WR_C_
#define _ERPS_NP_WR_C_

#include "erpsinc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : ErpsFsErpsHwRingConfig                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsErpsHwRingConfig
 *                                                                          
 *    Input(s)            : Arguments of FsErpsHwRingConfig
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ErpsFsErpsHwRingConfig (tErpsHwRingInfo * pErpsHwRingInfo)
{
    tFsHwNp             FsHwNp;
    tErpsNpModInfo     *pErpsNpModInfo = NULL;
    tErpsNpWrFsErpsHwRingConfig *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ERPS_MOD,    /* Module ID */
                         FS_ERPS_HW_RING_CONFIG,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pErpsNpModInfo = &(FsHwNp.ErpsNpModInfo);
    pEntry = &pErpsNpModInfo->ErpsNpFsErpsHwRingConfig;

    pEntry->pErpsHwRingInfo = pErpsHwRingInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : ErpsFsErpsMbsmHwRingConfig                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsErpsMbsmHwRingConfig
 *                                                                          
 *    Input(s)            : Arguments of FsErpsMbsmHwRingConfig
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ErpsFsErpsMbsmHwRingConfig (tErpsHwRingInfo * pErpsHwRingInfo,
                            tErpsMbsmInfo * pErpsMbsmInfo)
{
    tFsHwNp             FsHwNp;
    tErpsNpModInfo     *pErpsNpModInfo = NULL;
    tErpsNpWrFsErpsMbsmHwRingConfig *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ERPS_MOD,    /* Module ID */
                         FS_ERPS_MBSM_HW_RING_CONFIG,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pErpsNpModInfo = &(FsHwNp.ErpsNpModInfo);
    pEntry = &pErpsNpModInfo->ErpsNpFsErpsMbsmHwRingConfig;

    pEntry->pErpsHwRingInfo = pErpsHwRingInfo;
    pEntry->pErpsMbsmInfo = pErpsMbsmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */

#endif /* _ERPS_NP_WR_C_ */
