/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsapi.c,v 1.13 2016/01/11 13:18:53 siva Exp $
 *
 * Description: This file contains the ERPS API related functions        
 *                                                        
 *****************************************************************************/
#ifndef _ERPSAPI_C
#define _ERPSAPI_C

#include "erpsinc.h"
/*****************************************************************************/
/* FUNCTION NAME    : ErpsApiExternalEventNotify                             */
/*                                                                           */
/* DESCRIPTION      : For receiving the RAPS, Signal Fail, Signal Ok for MEPs*/
/*                    this function is registered with ECFM module.          */
/*                    ECFM module will call this callback function whenever  */
/*                    a Signal Fail/Signal Ok happens for an MEP, as well as */
/*                    on reception of the R-APS PDU.                         */
/*                                                                           */
/* INPUT            : pEvent  - Pointer to the structure which consist of    */
/*                              the necessary information to identify the    */
/*                              SF, OK, APS PDU reception.                   */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : NONE                                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ErpsApiExternalEventNotify (tErpsEventNotification * pEvent)
{
    tErpsQMsg          *pMsg = NULL;
    UINT4               u4UnitsCount;
    UINT4               u4FreeUnitsCount;

    u4UnitsCount = MemGetTotalUnits (gErpsGlobalInfo.TaskQMsgPoolId);
    u4FreeUnitsCount = MemGetFreeUnits (gErpsGlobalInfo.TaskQMsgPoolId);

    if (gau1ErpsSysCtrlStatus[pEvent->u4ContextId] != ERPS_START)
    {
        /* ERPS is not started in this context */
        return;
    }

    if (u4FreeUnitsCount < ((ERPS_VAL_30 * u4UnitsCount) / ERPS_VAL_100))
    {
        if (pEvent->u4Event == ECFM_RAPS_FRAME_RECEIVED)
        {
            ERPS_GLOBAL_TRC ("ErpsApiExternalEventNotify: Queue Message memory "
                             "free units count is 30 percentage of Total Units; so ignoring RAPS "
                             "Messages \r\n");
            return;
        }
    }
    if ((pMsg = (tErpsQMsg *) MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId))
        == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsApiExternalEventNotify: Queue Message memory "
                         "allocation FAILED !!!\r\n");
        return;
    }

    switch (pEvent->u4Event)
    {
        case ECFM_RAPS_FRAME_RECEIVED:

            pMsg->u4MsgType = ERPS_RAPS_PDU_MSG;
            pMsg->u4ContextId = pEvent->u4ContextId;
            pMsg->MepInfo.u4MdId = pEvent->u4MdIndex;
            pMsg->MepInfo.u4MaId = pEvent->u4MaIndex;
            pMsg->MepInfo.u4MepId = pEvent->u2MepId;
            pMsg->MepInfo.u4IfIndex = pEvent->u4IfIndex;
            pMsg->MepInfo.VlanId = (tVlanId) pEvent->u4VlanIdIsid;
            pMsg->unMsgParam.RapsPduMsg.u1TlvOffset = pEvent->u1CfmPduOffset;
            pMsg->unMsgParam.RapsPduMsg.pRapsPdu = pEvent->pu1Data;
            break;

        case ECFM_DEFECT_CONDITION_ENCOUNTERED:    /* Fall Through */
        case ECFM_DEFECT_CONDITION_CLEARED:

            pMsg->u4MsgType = ERPS_CFM_SIGNAL_MSG;
            pMsg->u4ContextId = pEvent->u4ContextId;
            pMsg->MepInfo.u4MdId = pEvent->u4MdIndex;
            pMsg->MepInfo.u4MaId = pEvent->u4MaIndex;
            pMsg->MepInfo.u4MepId = pEvent->u2MepId;
            pMsg->MepInfo.u4IfIndex = pEvent->u4IfIndex;
            pMsg->MepInfo.VlanId = (tVlanId) pEvent->u4VlanIdIsid;
            pMsg->MepInfo.u1Direction = pEvent->u1Direction;

            if (pEvent->u4Event == ECFM_DEFECT_CONDITION_ENCOUNTERED)
            {
                pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_FAIL;
            }
            else
            {
                pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_OK;
            }

            break;

        default:
            MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
            return;
    }
    /*Since this callback routine is hit from ECFM module
       populate the monitoring type as CFM */

    pMsg->u4MonitoringType = ERPS_MONITOR_MECH_CFM;
    ErpsMsgEnque (pMsg);

    return;
}

/******************************************************************************/
/* FUNCTION NAME    : ErpsApiDeleteContext                                    */
/*                                                                            */
/* DESCRIPTION      : This function will be called by VCM module to indicate  */
/*                    context deletion. This function posts a message to ERPS */
/*                    task's message.                                         */
/*                                                                            */
/* INPUT            : u4ContextId - Context Identifier                        */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                            */
/******************************************************************************/
PUBLIC INT4
ErpsApiDeleteContext (UINT4 u4ContextId)
{
    tErpsQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    ERPS_LOCK ();

    if (gErpsGlobalInfo.apRingContextInfo[u4ContextId] == NULL)
    {
        /* ERPS is not initialized in this context */
        ERPS_UNLOCK ();
        return OSIX_SUCCESS;
    }

    ERPS_UNLOCK ();

    if ((pMsg = (tErpsQMsg *) MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId))
        == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsApiDeleteContext: Allocation of memory for Queue"
                         "Message FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    pMsg->u4MsgType = ERPS_DELETE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    i4RetVal = ErpsMsgEnque (pMsg);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion protocol
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsApiLock (VOID)
{
    if (OsixSemTake (gErpsGlobalInfo.MainTaskSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion protocol
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsApiUnLock (VOID)
{
    if (OsixSemGive (gErpsGlobalInfo.MainTaskSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************/
/* FUNCTION NAME    : ErpsApiUpdateCxtNameFromVcm                             */
/*                                                                            */
/* DESCRIPTION      : This function will be called by VCM module to indicate  */
/*                    context name change in VCM.                             */
/*                                                                            */
/* INPUT            : u4ContextId - Context Identifier                        */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                            */
/******************************************************************************/
PUBLIC INT4
ErpsApiUpdateCxtNameFromVcm (UINT4 u4ContextId)
{
    tErpsQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    ERPS_LOCK ();

    if (gErpsGlobalInfo.apRingContextInfo[u4ContextId] == NULL)
    {
        /* ERPS is not initialized in this context */
        ERPS_UNLOCK ();
        return OSIX_SUCCESS;
    }

    ERPS_UNLOCK ();

    if ((pMsg = (tErpsQMsg *) MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId))
        == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsApiDeleteContext: Allocation of memory for Queue"
                         "Message FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    pMsg->u4MsgType = ERPS_UPDATE_CONTEXT_NAME;
    pMsg->u4ContextId = u4ContextId;

    i4RetVal = ErpsMsgEnque (pMsg);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;

}

PUBLIC INT4
ErpsApiModuleShutDown (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    ERPS_LOCK ();

    i4RetVal = ErpsMainModuleShutDown ();

    ERPS_UNLOCK ();

    return i4RetVal;
}

PUBLIC INT4
ErpsApiModuleStart (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    ERPS_LOCK ();

    i4RetVal = ErpsMainModuleStart ();

    ERPS_UNLOCK ();

    return i4RetVal;
}

/******************************************************************************/
/* FUNCTION NAME    : ErpsAsyncNpUpdateStatus                                 */
/*                                                                            */
/* DESCRIPTION      : This function posts an event to the ERPS task, when an  */
/*                    asynchronous hardware call returns                      */
/*                                                                            */
/* INPUT            : u4NpCallId - Call Id of the NPAPI for which cb has been */
/*                                 invoked                                    */
/*                    NpParams - Parameters returned by the hardware          */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : None                                                    */
/*                                                                            */
/******************************************************************************/
VOID
ErpsAsyncNpUpdateStatus (UINT4 u4NpCallId, unAsyncNpapi * NpParams)
{
    tErpsQMsg          *pMsg = NULL;
    /* Allocate Interface message buffer from the pool */

    /* If the npapi mode for ERPS is not asynchronous , return */
    if (ErpsPortIssGetAsyncMode (ERPS_MODULE) == ISS_NPAPI_MODE_SYNCHRONOUS)
    {
        return;
    }

    if ((pMsg = (tErpsQMsg *)
         MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId)) == NULL)
    {
        gErpsGlobalInfo.u4BuffferAllocFailCount++;
        ERPS_GLOBAL_TRC ("ErpsAsyncNpUpdateStatus: Memory Allocation"
                         "failed for pMsg\r\n");
        return;
    }

    MEMSET (pMsg, 0, sizeof (tErpsQMsg));

    pMsg->u4MsgType = ERPS_NP_CALLBACK;
    pMsg->unMsgParam.ErpsNpCbMsg.u4NpCallId = u4NpCallId;
    MEMCPY (&(pMsg->unMsgParam.ErpsNpCbMsg.ErpsAsyncNpCbParams),
            NpParams, sizeof (unAsyncNpapi));

    if (OsixQueSend (gErpsGlobalInfo.MainTaskQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
        ERPS_GLOBAL_TRC ("ErpsMsgEnque: Osix Queue Send Failed!!!\r\n");
        return;
    }

    if (OsixEvtSend (gErpsGlobalInfo.MainTaskId, ERPS_QMSG_EVENT)
        == OSIX_FAILURE)
    {
        ERPS_GLOBAL_TRC ("ErpsMsgEnque: Osix Event Send Failed!!!\r\n");
    }
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : ErpsApiNotificationRoutine                             */
/*                                                                           */
/* DESCRIPTION      : This function is used to receive the signal fault      */
/*                    and clearance of signal fault on MPLS/MPLS-TP LSP and  */
/*                    PW from the MPLS OAM module for the registered MEPs    */
/*                                                                           */
/*                                                                           */
/* INPUT            : pEventNotification  - Pointer to the structure         */
/*                                          which consist of the necessary   */
/*                                          information to identify the SF   */
/*                    u4ModId - Module Id                                    */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ErpsApiNotificationRoutine (UINT4 u4ModId, VOID *pEventNotification)
{
    tErpsQMsg          *pMsg = NULL;
    tMplsEventNotif    *pMplsEvent = NULL;

    UNUSED_PARAM (u4ModId);

    if ((pMsg = (tErpsQMsg *) MemAllocMemBlk (gErpsGlobalInfo.TaskQMsgPoolId))
        == NULL)
    {
        ERPS_GLOBAL_TRC ("ErpsApiNotificationRoutine: Queue Message memory "
                         "allocation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    pMplsEvent = (tMplsEventNotif *) pEventNotification;

    if (gau1ErpsSysCtrlStatus[pMplsEvent->u4ContextId] != ERPS_START)
    {
        /* ERPS is not started in this context */
        MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
        ERPS_GLOBAL_TRC ("ErpsApiNotificationRoutine: ERPS module not started"
                         "in the context\r\n");
        return OSIX_FAILURE;
    }

    switch (pMplsEvent->u2Event)
    {
        case MPLS_MEG_DOWN_EVENT:
	case MPLS_PW_IF_DOWN:
	case MPLS_PW_IF_UP:
	case MPLS_PW_UP_EVENT:
	case MPLS_PW_DOWN_EVENT:
            /* Fall Through */
        case MPLS_MEG_UP_EVENT:
            pMsg->u4MsgType = ERPS_CFM_SIGNAL_MSG;
            pMsg->u4ContextId = pMplsEvent->u4ContextId;
			
	    if (pMplsEvent->PathId.PwId.u4VcId != 0)
	    {
		pMsg->MepInfo.u4MdId =  pMplsEvent->PathId.PwId.u4MegIndex;
		pMsg->MepInfo.u4MaId =  pMplsEvent->PathId.PwId.u4MeIndex;
		pMsg->MepInfo.u4MepId= pMplsEvent->PathId.PwId.u4MpIndex;
	    }
	    else
	    {
                pMsg->MepInfo.u4MdId = pMplsEvent->PathId.MegId.u4MegIndex;
                pMsg->MepInfo.u4MaId = pMplsEvent->PathId.MegId.u4MeIndex;
                pMsg->MepInfo.u4MepId = pMplsEvent->PathId.MegId.u4MpIndex;
	    }
            pMsg->MepInfo.u4IfIndex = pMplsEvent->u4InIfIndex;
            if (pMplsEvent->u2Event == MPLS_MEG_DOWN_EVENT)
            {
                pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_FAIL;
            }
            else if (pMplsEvent->u2Event == MPLS_MEG_UP_EVENT)
            {
                pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_OK;
            }

	    else if (pMplsEvent->u2Event == MPLS_PW_IF_DOWN)
	    {
		pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_FAIL;
	    }
	    else if (pMplsEvent->u2Event == MPLS_PW_IF_UP)
	    {
		pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_OK;
	    }	
	    else if (pMplsEvent->u2Event == MPLS_PW_DOWN_EVENT)
	    {
		pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_FAIL;
	    }
	    else if (pMplsEvent->u2Event == MPLS_PW_UP_EVENT)
	    {
		pMsg->unMsgParam.u4Status = ERPS_CFM_SIGNAL_OK;
	    }				

            break;

        default:
            MemReleaseMemBlock (gErpsGlobalInfo.TaskQMsgPoolId, (UINT1 *) pMsg);
            return OSIX_FAILURE;

    }
    /*Since this callback routine is hit from ECFM module
       populate the monitoring type as MPLS OAM */

    pMsg->u4MonitoringType = ERPS_MONITOR_MECH_MPLSOAM;
    ErpsMsgEnque (pMsg);

    return OSIX_SUCCESS;

}

/*******************************************************************************/
/*FUNCTION NAME    : ErpsApiIsErpsStartedInContext                             */
/*                                                                             */
/*DESCRIPTION      : This function will be called by L2IWF module to check     */
/*                   ERPS is started in context or not to map the Vlan to      */
/*                   Instance.                                                 */
/*                                                                             */
/*   INPUT         : u4ContextId - Context Identifier                          */
/*                                                                             */
/*   OUTPUT        : NONE                                                      */
/*                                                                             */
/*  RETURNS        : OSIX_TRUE/OSIX_FALSE                                      */
/*******************************************************************************/
PUBLIC INT4
ErpsApiIsErpsStartedInContext (UINT4 u4ContextId)
{

    return (ErpsUtilIsErpsStarted (u4ContextId));
}
#endif
