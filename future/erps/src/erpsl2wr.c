/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsl2wr.c,v 1.1 2010/10/14 14:03:14 kumar Exp $
 *
 * Description: This file contains the L2Iwf related wrapper functions used 
 * by ERPS
 *                                                        
 *****************************************************************************/
#ifndef _ERPSL2WR_C
#define _ERPSL2WR_C

#include "erpsinc.h"

/***************************************************************************
 * FUNCTION NAME    : ErpsL2wrMiGetVlanInstMapping
 *
 * DESCRIPTION      : This function returns the vlan group id associated
 *                    with the given vlan id.
 *
 * INPUT            : u4ContextId - virtual context id
 *                    VlanId      - vlan id for which the associated group 
 *                                  id needs to be returned
 *
 * OUTPUT           : None
 *
 * RETURNS          : vlan group id
 *
 **************************************************************************/
PUBLIC UINT2
ErpsL2wrMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT2 u2VlanGroupId = ERPS_DEFAULT_VLAN_GROUP_ID;

    u2VlanGroupId = L2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    return u2VlanGroupId;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsL2wrSetVlanInstMapping
 *
 * DESCRIPTION      : This function mapped a vlan to a vlan group.
 *
 * INPUT            : u4ContextId - virtual context id
 *                    VlanId      - vlan id that needs to be mapped to the
 *                                  provided vlan group.
 *                    u2VlanGroupId - vlan group id.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ErpsL2wrSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                            UINT2 u2VlanGroupId)
{
    if (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2VlanGroupId)
        == L2IWF_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsL2wrMiGetVlanListInInstance
 *
 * DESCRIPTION      : This function returns the list of vlans mapped with 
 *                    the provided vlan group.
 *
 * INPUT            : u4ContextId   - virtual contexst id
 *                    u2VlanGroupId - vlan group id for which the mapped 
 *                                    vlan list needs to be returned
 *
 * OUTPUT           : pu1VlanList   - List of vlans mapped with the 
 *                                    provided group id.
 *
 * RETURNS          : u2NumOfVlans  - Number of vlans mapped with the 
 *                                    provided group id. If vlan group 
 *                                    does not exist or no vlan is mapped
 *                                    with that group it will return 0.
 *
 **************************************************************************/
PUBLIC UINT2
ErpsL2wrMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2VlanGroupId,
                                      UINT1 *pu1VlanList)
{
    UINT2 u2NumOfVlans = 0;

    u2NumOfVlans = L2IwfMiGetVlanListInInstance (u4ContextId, u2VlanGroupId, 
                                                 pu1VlanList);

    return u2NumOfVlans;
}

/*****************************************************************************
 * Function Name      : ErpsL2wrGetInstanceInfo                                 
 *                                                                           
 * Description        : This function returns the instance info of the given  
 *                      instance id / Vlan Group Id.                             *                                                                           
 * Input(s)           : u4ContextId - Virtual Swith ID
 *                      u2Instance - Instance id (or Vlan group id), for
 *                                   which instance info to be returned.
 *
 *                                                                           
 * Output(s)          : pStpInstInfo - Pointer to Instance info.             
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ErpsL2wrGetInstanceInfo (UINT4 u4ContextId, UINT2 u2Instance,
                         tStpInstanceInfo * pStpInstInfo)
{
    if (L2IwfGetInstanceInfo (u4ContextId, u2Instance, pStpInstInfo)
        == L2IWF_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : ErpsL2wrDeleteSpanningTreeInstance                 
 *                                                                           
 * Description        : This function is called to delete a instance (or
 *                      vlan group) from the instance table maintained in
 *                      L2Iwf database, and make it as not active state.
 *
 * Input(s)           : u4ContextId - Virtual Swith ID 
 *                      u2Instance - Instance id (or Vlan group id), for
 *                                   which needs to be deleted.
 *                                                                           
 * Output(s)          : None
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ErpsL2wrDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    if (L2IwfDeleteSpanningTreeInstance (u4ContextId, u2InstanceId)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : ErpsL2wrCreateSpanningTreeInstance                 
 *                                                                           
 * Description        : This function is called to create a instance (or
 *                      vlan group) in the instance table maintained in
 *                      L2Iwf database, and make it as active .
 *
 * Input(s)           : u4ContextId - Virtual Swith ID 
 *                      u2Instance - Instance id (or Vlan group id), for
 *                                   which needs to created.
 *                                                                           
 * Output(s)          : None
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ErpsL2wrCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    if (L2IwfCreateSpanningTreeInstance (u4ContextId, u2InstanceId)
        == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

#endif /* _ERPSL2WR_C */
