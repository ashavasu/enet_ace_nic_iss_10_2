/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsaoamlw.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsF5OAMEntryStatus ARG_LIST((INT4 *));

INT1
nmhGetFsF5OAMDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsF5OAMEntryStatus ARG_LIST((INT4 ));

INT1
nmhSetFsF5OAMDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsF5OAMEntryStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsF5OAMDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for FsF5OAMLoopbackPingTable. */
INT1
nmhValidateIndexInstanceFsF5OAMLoopbackPingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsF5OAMLoopbackPingTable  */

INT1
nmhGetFirstIndexFsF5OAMLoopbackPingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsF5OAMLoopbackPingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsF5OAMLoopbackPingType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingLocationID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingCorrelationTag ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsF5OAMLoopbackPingSourceIDStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingSourceID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingEntryStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingTxLoopback ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsF5OAMLoopbackPingCurrTxCells ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMLoopbackPingCurrRxCells ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMNoOfCurrSuccessfulPings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMNoOfCurrFailedPings ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsF5OAMLoopbackPingType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingLocationID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingCorrelationTag ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsF5OAMLoopbackPingSourceIDStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingSourceID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingEntryStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsF5OAMLoopbackPingTxLoopback ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsF5OAMLoopbackPingType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingLocationID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingCorrelationTag ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsF5OAMLoopbackPingSourceIDStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingSourceID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsF5OAMLoopbackPingTxLoopback ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsF5OAMLoopbackStatsTable. */
INT1
nmhValidateIndexInstanceFsF5OAMLoopbackStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsF5OAMLoopbackStatsTable  */

INT1
nmhGetFirstIndexFsF5OAMLoopbackStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsF5OAMLoopbackStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsF5OAMLLIDIncorrects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMeTOeLoopbackIncorrects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMLBIndicationIncorrects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMSourceIDIncorrects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMCorrelationTagIncorrects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMLoopbackPingTxCells ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMLoopbackPingRxCells ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMNoOfSuccessfulPings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsF5OAMNoOfFailedPings ARG_LIST((INT4 ,UINT4 *));
