/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsaoamdb.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSAOAMDB_H
#define _FSAOAMDB_H

UINT1 FsF5OAMLoopbackPingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsF5OAMLoopbackStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsaoam [] ={1,3,6,1,4,1,29601,2,10,1};
tSNMP_OID_TYPE fsaoamOID = {10, fsaoam};


UINT4 FsF5OAMEntryStatus [ ] ={1,3,6,1,4,1,29601,2,10,1,1};
UINT4 FsF5OAMDebug [ ] ={1,3,6,1,4,1,29601,2,10,1,2};
UINT4 FsF5OAMLoopbackPingIfIndex [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,1};
UINT4 FsF5OAMLoopbackPingType [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,2};
UINT4 FsF5OAMLoopbackPingLocationID [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,3};
UINT4 FsF5OAMLoopbackPingCount [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,4};
UINT4 FsF5OAMLoopbackPingTimeout [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,5};
UINT4 FsF5OAMLoopbackPingCorrelationTag [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,6};
UINT4 FsF5OAMLoopbackPingSourceIDStatus [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,7};
UINT4 FsF5OAMLoopbackPingSourceID [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,8};
UINT4 FsF5OAMLoopbackPingEntryStatus [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,9};
UINT4 FsF5OAMLoopbackPingOperStatus [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,10};
UINT4 FsF5OAMLoopbackPingTxLoopback [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,11};
UINT4 FsF5OAMLoopbackPingCurrTxCells [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,12};
UINT4 FsF5OAMLoopbackPingCurrRxCells [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,13};
UINT4 FsF5OAMNoOfCurrSuccessfulPings [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,14};
UINT4 FsF5OAMNoOfCurrFailedPings [ ] ={1,3,6,1,4,1,29601,2,10,2,1,1,15};
UINT4 FsF5OAMLoopbackStatsIfIndex [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,1};
UINT4 FsF5OAMLLIDIncorrects [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,2};
UINT4 FsF5OAMeTOeLoopbackIncorrects [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,3};
UINT4 FsF5OAMLBIndicationIncorrects [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,4};
UINT4 FsF5OAMSourceIDIncorrects [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,5};
UINT4 FsF5OAMCorrelationTagIncorrects [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,6};
UINT4 FsF5OAMLoopbackPingTxCells [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,7};
UINT4 FsF5OAMLoopbackPingRxCells [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,8};
UINT4 FsF5OAMNoOfSuccessfulPings [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,9};
UINT4 FsF5OAMNoOfFailedPings [ ] ={1,3,6,1,4,1,29601,2,10,2,2,1,10};


tMbDbEntry fsaoamMibEntry[]= {

{{11,FsF5OAMEntryStatus}, NULL, FsF5OAMEntryStatusGet, FsF5OAMEntryStatusSet, FsF5OAMEntryStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{11,FsF5OAMDebug}, NULL, FsF5OAMDebugGet, FsF5OAMDebugSet, FsF5OAMDebugTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{13,FsF5OAMLoopbackPingIfIndex}, GetNextIndexFsF5OAMLoopbackPingTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingType}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingTypeGet, FsF5OAMLoopbackPingTypeSet, FsF5OAMLoopbackPingTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingLocationID}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingLocationIDGet, FsF5OAMLoopbackPingLocationIDSet, FsF5OAMLoopbackPingLocationIDTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingCount}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingCountGet, FsF5OAMLoopbackPingCountSet, FsF5OAMLoopbackPingCountTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingTimeout}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingTimeoutGet, FsF5OAMLoopbackPingTimeoutSet, FsF5OAMLoopbackPingTimeoutTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingCorrelationTag}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingCorrelationTagGet, FsF5OAMLoopbackPingCorrelationTagSet, FsF5OAMLoopbackPingCorrelationTagTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingSourceIDStatus}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingSourceIDStatusGet, FsF5OAMLoopbackPingSourceIDStatusSet, FsF5OAMLoopbackPingSourceIDStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingSourceID}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingSourceIDGet, FsF5OAMLoopbackPingSourceIDSet, FsF5OAMLoopbackPingSourceIDTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingEntryStatus}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingEntryStatusGet, FsF5OAMLoopbackPingEntryStatusSet, FsF5OAMLoopbackPingEntryStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingOperStatus}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingOperStatusGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingTxLoopback}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingTxLoopbackGet, FsF5OAMLoopbackPingTxLoopbackSet, FsF5OAMLoopbackPingTxLoopbackTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingCurrTxCells}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingCurrTxCellsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackPingCurrRxCells}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMLoopbackPingCurrRxCellsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMNoOfCurrSuccessfulPings}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMNoOfCurrSuccessfulPingsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMNoOfCurrFailedPings}, GetNextIndexFsF5OAMLoopbackPingTable, FsF5OAMNoOfCurrFailedPingsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackPingTableINDEX, 1},

{{13,FsF5OAMLoopbackStatsIfIndex}, GetNextIndexFsF5OAMLoopbackStatsTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMLLIDIncorrects}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMLLIDIncorrectsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMeTOeLoopbackIncorrects}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMeTOeLoopbackIncorrectsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMLBIndicationIncorrects}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMLBIndicationIncorrectsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMSourceIDIncorrects}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMSourceIDIncorrectsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMCorrelationTagIncorrects}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMCorrelationTagIncorrectsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMLoopbackPingTxCells}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMLoopbackPingTxCellsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMLoopbackPingRxCells}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMLoopbackPingRxCellsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMNoOfSuccessfulPings}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMNoOfSuccessfulPingsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},

{{13,FsF5OAMNoOfFailedPings}, GetNextIndexFsF5OAMLoopbackStatsTable, FsF5OAMNoOfFailedPingsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsF5OAMLoopbackStatsTableINDEX, 1},
};
tMibData fsaoamEntry = { 27, fsaoamMibEntry };
#endif /* _FSAOAMDB_H */

