
#ifndef _AOAMGLOB_H
#define _AOAMGLOB_H
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamglob.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : aoamglob.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM module                                 */
/*    MODULE NAME           : ATM OAM module Globals                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains global variables            */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/

tAoamGlobalInfo gAoamGlobalInfo;

#endif
