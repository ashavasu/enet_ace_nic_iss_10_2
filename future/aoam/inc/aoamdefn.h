
#ifndef _AOAMDEFN_H
#define _AOAMDEFN_H
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamdefn.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $        */ 
/*****************************************************************************/
/*    FILE  NAME            : aoamdefn.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Module                                 */
/*    MODULE NAME           : ATM OAM module definitions                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains type definitions            */
/*                            for ATM OAM loopback module                    */
/*---------------------------------------------------------------------------*/

/* Osix, Task, Queue and Event related definitions */
#define AOAM_TASK_NAME                  (const UINT1 *) "AOAM"
#define AOAM_SEM_NAME                   (const UINT1 *) "AOAM"
#define AOAM_CFG_QUEUE_NAME             (const UINT1 *) "AOCQ"
#define AOAM_PKT_QUEUE_NAME             (const UINT1 *) "AOAQ"
#define AOAM_MODULE_NAME                "AOAM"
#define AOAM_SEM_COUNT                  1
#define AOAM_QUEUE_DEPTH                50

#define AOAM_LLID_LENGTH                16
#define AOAM_CORRELATION_TAG_LENGTH     4
#define AOAM_SOURCE_ID_LENGTH           16

/* Different events to be handled */
#define AOAM_CFG_MSG_EVENT              0x01
#define AOAM_TMR_EXP_EVENT              0x02
#define AOAM_PKT_ARRIVED_EVENT          0x04

/* Timer Id */
#define AOAM_LOOPBACK_TIMER             1    

/* Messages to be posted for AOAM_CFG_MSG_EVENT */
#define AOAM_PORT_CREATE_MSG            1
#define AOAM_PORT_DELETE_MSG            2
#define AOAM_PORT_OPER_UP_MSG           3
#define AOAM_PORT_OPER_DOWN_MSG         4

/* Trace Definitions */

#define AOAM_TRC_NONE                   0x00

#define AOAM_MAX_LOG_STR_LEN            256
#define AOAM_MAX_TRC_ARGS               10


#ifdef TRACE_WANTED

#define AOAM_FORCED_TRC(TraceType, Str) \
        AoamUtilTrc(AOAM_TRC_FLAG (), TraceType, __FUNCTION__, Str)

#define AOAM_TRC(TraceType, Str) \
        AoamUtilTrc (AOAM_TRC_FLAG (), TraceType, __FUNCTION__, Str)

#define AOAM_TRC_ARG1(TraceType, Str, Arg1) \
        AoamUtilTrc (AOAM_TRC_FLAG (), TraceType,__FUNCTION__, Str, Arg1)
        
#define AOAM_TRC_ARG2(TraceType, Str, Arg1, Arg2) \
        AoamUtilTrc (AOAM_TRC_FLAG (), TraceType, __FUNCTION__,\
                     Str,Arg1, Arg2)

#define AOAM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3) \
        AoamUtilTrc (AOAM_TRC_FLAG (), TraceType, __FUNCTION__,\
                     Str, Arg1, Arg2, Arg3) 

#define AOAM_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
        AoamUtilTrc (AOAM_TRC_FLAG (), TraceType, __FUNCTION__, \
                     Str,Arg1, Arg2, Arg3, Arg4)

#else
#define AOAM_FORCED_TRC(TraceType, Str)\
        AoamUtilTrc(AOAM_ALL_TRC, TraceType, __FUNCTION__, Str)
#define AOAM_TRC(TraceType, Str)       
#define AOAM_TRC_ARG1(TraceType, Str, Arg1)
#define AOAM_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define AOAM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define AOAM_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)
#endif /* TRACE_WANTED */

/* PVC (port) related macro's */

#define AOAM_MIN_PORT       (SYS_DEF_MAX_PHYSICAL_INTERFACES +\
                             LA_MAX_AGG + 1)
#define AOAM_MAX_PORT       (SYS_DEF_MAX_PHYSICAL_INTERFACES + \
                             LA_MAX_AGG + SYS_DEF_MAX_PVCS)

#define AOAM_MAX_NUM_OF_PVC  SYS_DEF_MAX_PVCS

#define AOAM_IS_VALID_PORT(u4Port) \
    (((u4Port > AOAM_MAX_PORT) || (u4Port < AOAM_MIN_PORT)) ? \
     AOAM_FALSE : AOAM_TRUE)


 /* Definitions related to pkts */
#define AOAM_INIT_VAL               0
#define AOAM_BPDU_HEADER_SIZE       5
#define AOAM_BPDU_PAYLOAD_SIZE      48
#define AOAM_BPDU_SIZE              53
#define AOAM_OFFSET                 0

#define AOAM_CELL_TYPE_SHIFT        4
#define AOAM_CELL_TYPE_FAULT_MGMT   1
#define AOAM_FUNC_TYPE_LOOPBACK     8
#define AOAM_SOURCE_LB_INDICATION   1
#define AOAM_SINK_LB_INDICATION     0

#define AOAM_UNUSED_PAYLOAD         10
#define AOAM_UNUSED_OCTET_VALUE     106

#define AOAM_VPI_SHIFT              20
#define AOAM_VCI_SHIFT              4
#define AOAM_PTI_SHIFT              1
#define AOAM_END_TO_END_PTI         5
#define AOAM_DEF_HEC                0
    

#define AOAM_PTI_BYTE_LOCATION      3
#define AOAM_LB_INDICATION_BYTE     6
#define AOAM_LB_LOCATION_START      11
#define AOAM_SRC_ID_OFFSET          27

#define AOAM_MIN_PING_COUNT         1
#define AOAM_MAX_PING_COUNT         255

#define AOAM_MIN_PING_TIMEOUT       1
#define AOAM_MAX_PING_TIMEOUT       255
#define AOAM_PTI_OFFSET             3
#define AOAM_CORR_TAG_OFFSET        36
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  aoamtrc.h                      */
/*-----------------------------------------------------------------------*/


