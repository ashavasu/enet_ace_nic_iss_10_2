
#ifndef _AOAMPROT_H
#define _AOAMPROT_H 
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: aoamprot.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $         */
/*****************************************************************************/
/*    FILE  NAME            : aoamprot.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Module                                 */
/*    MODULE NAME           : ATM OAM prototypes                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains function prototypes         */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/


/* aoamapi.c */

PUBLIC INT4
AoamApiLock PROTO ((VOID));

PUBLIC INT4
AoamApiUnLock PROTO ((VOID));

/* aoammain.c */

PUBLIC VOID
AoamMainDeInit PROTO ((VOID));

PUBLIC VOID
AoamMainProcessEvent PROTO ((tAoamQMsg *));
    
PUBLIC VOID
AoamMainProcessTmrEvent PROTO ((VOID));
    
/* aoamutil.c */

PUBLIC VOID
AoamUtilTrc PROTO ((INT4, INT4 , const CHR1 *, CHR1 *, ...));

PUBLIC INT4
AoamUtilRBTreeF5OamLBEntryCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4
AoamUtilCreatePvcEntry PROTO ((UINT4));

PUBLIC tF5OamLoopbackTable *
AoamUtilGetPvcEntry PROTO ((UINT4));

PUBLIC VOID
AoamUtilRegisterFsAoamMib PROTO ((VOID));

PUBLIC INT4
AoamUtilDeletePvcEntry PROTO ((UINT4));

PUBLIC VOID 
AoamUtilRBTreeLBEntryFree PROTO ((tF5OamLoopbackTable *));

PUBLIC INT4
AoamCopyPduBufInfo PROTO ((tAoamBufChainHeader *, UINT1 **));

/* aoampkt.c */

PUBLIC INT4 
AoamPktFillHeader PROTO ((tAoamBufChainHeader *,
                          tF5OamLoopbackTable *));

PUBLIC INT4 
AoamPktFormBpdu PROTO ((tAoamBufChainHeader *,
                        tF5OamLoopbackTable *, UINT4 *));

PUBLIC INT4 
AoamPktTxOamCell PROTO ((tF5OamLoopbackTable *));
    
PUBLIC VOID
AoamProcessLBOamCell PROTO ((tAoamBufChainHeader *, UINT4));

PUBLIC INT4
AoamValidateHeaderInfo PROTO ((UINT1 *, UINT4));

PUBLIC INT4
AoamValidatePayload PROTO ((UINT1 *, tF5OamLoopbackTable *, UINT1 *));

PUBLIC INT4
AoamProcessRecvdLBCell PROTO ((UINT1 *, tF5OamLoopbackTable *));

PUBLIC INT4
AoamProcessLoopedBackCell PROTO ((UINT1 *, tF5OamLoopbackTable *));

#endif
