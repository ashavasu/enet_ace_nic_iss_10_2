#ifndef _FSAOAMWR_H
#define _FSAOAMWR_H

VOID RegisterFSAOAM(VOID);

VOID UnRegisterFSAOAM(VOID);
INT4 FsF5OAMEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsF5OAMLoopbackPingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsF5OAMLoopbackPingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingLocationIDGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCountGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCorrelationTagGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTxLoopbackGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCurrTxCellsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCurrRxCellsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMNoOfCurrSuccessfulPingsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMNoOfCurrFailedPingsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingLocationIDSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCountSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCorrelationTagSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTxLoopbackSet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingLocationIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingCorrelationTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingSourceIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTxLoopbackTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsF5OAMLoopbackStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsF5OAMLLIDIncorrectsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMeTOeLoopbackIncorrectsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLBIndicationIncorrectsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMSourceIDIncorrectsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMCorrelationTagIncorrectsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingTxCellsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMLoopbackPingRxCellsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMNoOfSuccessfulPingsGet(tSnmpIndex *, tRetVal *);
INT4 FsF5OAMNoOfFailedPingsGet(tSnmpIndex *, tRetVal *);
#endif /* _FSAOAMWR_H */
