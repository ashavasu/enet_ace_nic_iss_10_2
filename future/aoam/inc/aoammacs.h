
#ifndef _AOAMMACS_H
#define _AOAMMACS_H
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoammacs.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : aoammacs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Module                                 */
/*    MODULE NAME           : ATM OAM loopback Macros                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains macro definitions           */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/

/* Trace related Macro */
#define AOAM_TRC_FLAG()           (gAoamGlobalInfo.u4DebugFlag)

#define AOAM_CREATE_RBTREE      RBTreeCreateEmbedded

/* Task, Queue and Event related macros, OSIX related macros */
#define AOAM_SUCCESS            OSIX_SUCCESS
#define AOAM_FAILURE            OSIX_FAILURE
#define AOAM_TRUE               OSIX_TRUE
#define AOAM_FALSE              OSIX_FALSE

#define AOAM_CREATE_QUEUE       OsixCreateQ
#define AOAM_DELETE_QUEUE       OsixDeleteQ
#define AOAM_QUEUE_SEND         OsixQueSend
#define AOAM_QUEUE_RECEIVE      OsixQueRecv
#define AOAM_CREATE_SEM         OsixCreateSem
#define AOAM_DELETE_SEM         OsixDeleteSem
#define AOAM_SEM_TAKE           OsixSemTake
#define AOAM_SEM_GIVE           OsixSemGive
#define AOAM_EVENT_RECEIVE      OsixEvtRecv
#define AOAM_EVENT_SEND         OsixEvtSend
#define AOAM_GET_TASK_ID        OsixGetTaskId
#define AOAM_CREATE_TIMER_LIST  TmrCreateTimerList
#define AOAM_DELETE_TIMER_LIST  TmrDeleteTimerList
#define AOAM_EVENT_WAIT_FLAG    OSIX_WAIT
#define AOAM_RECEIVE_Q_FLAG     OSIX_NO_WAIT
#define AOAM_SEM_FLAGS          OSIX_DEFAULT_SEM_MODE
#define AOAM_DEF_MSG_LEN        OSIX_DEF_MSG_LEN


#define AOAM_IF_UP                CFA_IF_UP
#define AOAM_IF_DOWN              CFA_IF_DOWN

#define AOAM_CFG_QUEUE_ID()       (gAoamGlobalInfo.AoamCfgQId)
#define AOAM_PKT_QUEUE_ID()       (gAoamGlobalInfo.AoamPktQId)
#define AOAM_SEM_ID()             (gAoamGlobalInfo.AoamSemId) 
#define AOAM_TASK_ID()            (gAoamGlobalInfo.AoamTaskId)

#define AOAM_F5OAM_LB_RBTREE()    (gAoamGlobalInfo.F5AoamLBEntry)
#define AOAM_TMR_LIST_ID()        (gAoamGlobalInfo.AoamTmrListId)
#define AOAM_ADMIN_STATUS()       (gAoamGlobalInfo.u1AoamGlobalStatus)
#define AOAM_TIMER_LIST()         (gAoamGlobalInfo.AoamTmrListId)

/* Memory related MACRO */
#define AOAM_CREATE_MEM_POOL    MemCreateMemPool

#define AOAM_DELETE_MEM_POOL(PoolId) (VOID) MemDeleteMemPool(PoolId)

#define  AOAM_ALLOC_MEM_BLOCK(PoolId, ppu1Msg) \
             MemAllocateMemBlock(PoolId, (UINT1 **)ppu1Msg)

#define  AOAM_RELEASE_MEM_BLOCK(PoolId, pu1Msg) \
{\
    MemReleaseMemBlock(PoolId, (UINT1 *) pu1Msg);\
    pu1Msg = NULL;\
}

#define AOAM_DEINIT_RBTREE(rb) \
{\
    if (rb != NULL) \
    {\
        RBTreeDestroy (rb, (tRBKeyFreeFn) AoamUtilRBTreeLBEntryFree, 0); \
            rb = NULL; \
    }\
}\

#define AOAM_MEMORY_TYPE            MEM_DEFAULT_MEMORY_TYPE

#define AOAM_CFG_Q_POOL_ID()        (gAoamGlobalInfo.AoamCfgQPoolId)
#define AOAM_PKT_Q_POOL_ID()        (gAoamGlobalInfo.AoamPktQPoolId)
#define AOAM_F5OAM_LB_POOL_ID()     (gAoamGlobalInfo.F5AoamLBPoolId)
#define AOAM_MEMBLK_SIZE            sizeof (tAoamQMsg)      
#define AOAM_F5OAM_LB_MEMBLK_SIZE   sizeof (tF5OamLoopbackTable)


/* Lock and Unlock */
#define AOAM_LOCK                   AoamApiLock
#define AOAM_UNLOCK                 AoamApiUnLock


/* CRU buffer related macro's */
#define AOAM_ALLOC_CRU_BUF          CRU_BUF_Allocate_MsgBufChain
#define AOAM_RELEASE_CRU_BUF        CRU_BUF_Release_MsgBufChain
#define AOAM_COPY_OVER_CRU_BUF      CRU_BUF_Copy_OverBufChain
#define AOAM_COPY_FROM_CRU_BUF      CRU_BUF_Copy_FromBufChain
#define AOAM_IS_CRU_BUF_LINEAR      CRU_BUF_Get_DataPtr_IfLinear


#define AOAM_HTONS                  OSIX_HTONS
#define AOAM_HTONL                  OSIX_HTONL
#define AOAM_NTOHS                  OSIX_NTOHS
#define AOAM_NTOHL                  OSIX_NTOHL
#define AOAM_CRU_SUCCESS            CRU_SUCCESS
#define AOAM_CRU_FAILURE            CRU_FAILURE


#define AOAM_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
           pu1PktBuf += 1;        \
        } while(0)

#define AOAM_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                    \
            u2Val = (UINT2)AOAM_HTONS(u2Val);                  \
            MEMCPY (pu1PktBuf, &u2Val, 2); \
            pu1PktBuf += 2;              \
        } while(0)

#define AOAM_PUT_4BYTE(pu1PktBuf, u4Val)       \
        do {                                   \
           u4Val = AOAM_HTONL(u4Val);   \
           MEMCPY (pu1PktBuf, &u4Val, 4); \
           pu1PktBuf += 4;             \
        }while(0)

#define AOAM_PUT_BYTES(pu1PktBuf, u1Val, u1Length)\
{\
    UINT1 u1Len = 0;\
    do {\
        *pu1PktBuf = u1Val; \
        pu1PktBuf += 1;\
        u1Len += 1;\
    }while(u1Len < u1Length);\
}
    
#define AOAM_MAP_LOCATION_ID(u1LLID, u1LLId)\
{\
    if (u1LLID == AOAM_LLID_ALL_ZEROS)\
    {\
        u1LLId = 0;\
    }\
    else if (u1LLID == AOAM_LLID_ALL_6AH)\
    {\
        u1LLId = 106;\
    }\
    else\
    {\
        u1LLId = 255;\
    }\
}

#define AOAM_GET_4BYTE(u4Val, pu1PktBuf)       \
        do {                                 \
           MEMCPY (&u4Val, pu1PktBuf, 4); \
           u4Val = (UINT4) (LA_NTOHL(u4Val));          \
           pu1PktBuf += 4;                   \
        } while(0)

#define AOAM_GET_1BYTE(u1Val, pu1PktBuf)       \
        do {                                 \
           MEMCPY (&u1Val, pu1PktBuf, 1); \
           pu1PktBuf += 1;                   \
        } while(0)


#define AOAM_GET_PTI(pu1Pti, u1PtiByte)\
{\
    u1PtiByte = u1PtiByte << 4;\
    *pu1Pti = u1PtiByte >> 5;\
}

#define AOAM_GET_VCI_VPI(pu1Pti, pu2Vci, pu1Vpi)\
{\
    UINT4 u4Vci = 0;\
    UINT4 u4Vpi = 0;\
    MEMCPY (&u4Vci, pu1Pti, 4);\
    u4Vpi = u4Vpi;\
    u4Vci = u4Vci << 12;\
    u4Vci = u4Vci >> 16;\
    u4Vpi = u4Vpi << 4;\
    u4Vpi = u4Vpi >> 24;\
    *pu1Vpi = (UINT1) u4Vpi;\
    *pu2Vci = (UINT2) u4Vci;\
}
#define AOAM_INCR_FAILED_PINGS(pF5OamLBEntry)\
{\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrFailedPings += 1; \
    pF5OamLBEntry->F5OamLBStatsEntry.u4FailedPings += 1;\
}
                
#define AOAM_INCR_LLID_DISCARDS(pF5OamLBEntry)\
{\
    pF5OamLBEntry->F5OamLBStatsEntry.u4LLIDInCorrects += 1;\
}

                    
#define AOAM_INCR_SUCCESSFUL_PINGS(pF5OamLBEntry)\
{\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrSuccessfulPings += 1;\
    pF5OamLBEntry->F5OamLBStatsEntry.u4SuccessfulPings += 1; \
}

                    
#define AOAM_INCR_CORRELA_TAG_DISCARDS(pF5OamLBEntry)\
pF5OamLBEntry->F5OamLBStatsEntry.u4CorrelationTagIncorrects += 1;
                
#define AOAM_INCR_SRC_ID_DISCARDS(pF5OamLBEntry)\
pF5OamLBEntry->F5OamLBStatsEntry.u4SourceIDIncorrects += 1;
        
#define AOAM_INCR_ETE_LB_DISCARDS(pF5OamLBEntry)\
pF5OamLBEntry->F5OamLBStatsEntry.u4EtoELBIncorrects += 1;

#define AOAM_LB_OFFSET(x, y)      ((UINT4)(&(((x *)0)->y)))

#define AOAM_LOOPBACK_TMR_OFFSET \
    AOAM_LB_OFFSET(tF5OamLoopbackTable, F5OamTmrNode)

#define AOAM_INCR_CURR_PING_COUNT(pF5OamLBEntry) \
    pF5OamLBEntry->F5OamLBPingEntry.u1CurrPingCount += 1

#define AOAM_INIT_CURR_COUNTERS(pF5OamLBEntry) \
{\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrSuccessfulPings = 0;\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrFailedPings = 0;\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrTxCells = 0;\
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrRxCells = 0;\
}
#endif
