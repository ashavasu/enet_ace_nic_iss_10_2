/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: aoamcmds.def,v 1.5 2015/05/27 13:50:14 siva Exp $                                                         
*                                                                    
*********************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : oamcmds.def                                    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : DCS                                            */
/*    MODULE NAME           : ATM OAM CLI commands                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    :                                                */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI config commands         */
/*                            for ATM OAM module.                            */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                       ATM OAM SHOW COMMANDS                               */
/*****************************************************************************/

DEFINE GROUP: AOAM_SHOW_CMDS

    COMMAND : show atm oam [interface <ifXtype> <ifnum>] { summary | details | statistics }
    ACTION  :
             {  
                 UINT4 u4Index = 0;
        		 UINT4 u4Type = 0;

                 if ($3 != NULL)
                 {
                    if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                    {
                        CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                        return CLI_FAILURE;
                    }
                 }

                 if ($6 != NULL)
                 {
                    u4Type = AOAM_SHOW_F5OAM_SUMMARY;
                 }
                 if ($7 != NULL)
                 {
                    u4Type = AOAM_SHOW_F5OAM_DETAILS;
                 }
                 if ($8 != NULL)
                 {
                    u4Type = AOAM_SHOW_F5OAM_STATISTICS;
                 }

                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SHOW_F5OAM_INFO, 
                                      u4Index, u4Type);
              }
    SYNTAX  : show atm oam [interface <port-type> <port-id>] { summary | details | statistics }
    PRVID   : 15
    HELP    : Displays OAM details of a particular PVC or for all the PVC.

END GROUP

/******************************************************************************/
/*                  Global CFG COMMANDS                                       */
/******************************************************************************/

DEFINE GROUP: AOAM_GLBCFG_CMDS

    COMMAND : atm oam interface <ifXtype> <ifnum> 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }

		         cli_process_aoam_cmd (CliHandle,AOAM_CLI_ENABLE_F5OAM_PER_PVC,
                                      u4Index);
 	      }
    SYNTAX  : atm oam interface <port-type> <port-id> 
    PRVID   : 15
    HELP    : To enable F5 OAM operation per PVC basis.


    COMMAND : no atm oam interface <ifXtype> <ifnum>
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
  		 cli_process_aoam_cmd (CliHandle,AOAM_CLI_DISABLE_F5OAM_PER_PVC,
                                      u4Index);
 	      }
    SYNTAX  : no atm oam interface <port-type> <port-id> 
    PRVID   : 15
    HELP    : To disable F5 OAM operation per PVC basis.


    COMMAND : atm oam { enable | disable }
    ACTION  :
              {
                 UINT4 u4Status = 0;

                 if ($2 != NULL)
                 {
                    u4Status = AOAM_ENABLE;
                 }
                 if ($3 != NULL)
                 {
                    u4Status = AOAM_DISABLE;
                 }

	        cli_process_aoam_cmd (CliHandle,AOAM_CLI_GLOBAL_F5OAM_STATUS,
                                     NULL, u4Status);
 	      }
    SYNTAX  : atm oam { enable | disable }
    PRVID   : 15
    HELP    : To enable or disable F5 OAM operation per PVC basis.



    COMMAND : ping atm interface <ifXtype> <ifnum>
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }

		        cli_process_aoam_cmd (CliHandle, AOAM_CLI_PING_ATM,
                                     u4Index);
 	          }
    SYNTAX  : ping atm interface <port-type> < port-id>
    PRVID   : 15
    HELP    : To send OAM cells to check the functionality and integrity of the PVC. 

    COMMAND : atm oam interface <ifXtype> <ifnum> { end-loopback | segment-loopback }
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 if ($5 != NULL)
                 {
                     cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_F5OAM_PING_TYPE,
                                          u4Index, AOAM_END_TO_END_LOOPBACK);
                 }
                 else 
                 {
                     CliPrintf (CliHandle, 
                                "\r%%Segment loopback not supported\r\n");
                     return CLI_FAILURE;
                 }
              }
    SYNTAX  : atm oam interface <port-type> < port-id> { end-loopback | segment-loopback }
    PRVID   : 15
    HELP    : To configure the ping type of OAM loopback cell.

    COMMAND : no atm oam interface <ifXtype> <ifnum> ping-type 
    ACTION  :
              {
                 UINT4 u4Index = 0; 

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                     cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_F5OAM_PING_TYPE,
                                          u4Index, AOAM_LOOPBACK_NONE);
              }
    SYNTAX  : no atm oam interface <port-type> < port-id> ping-type 
    PRVID   : 15
    HELP    : To configure the ping type of OAM loopback cell as none.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-location-ID {all-zeros | all-6AH }
    ACTION  :
              {
                 UINT4 u4Index = 0; 
                 UINT4 u4Value = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 if ($6 != NULL)
                 {
                     u4Value = AOAM_LLID_ALL_ZEROS;
                 }
                 if ($7 != NULL)
                 {
                     u4Value = AOAM_LLID_ALL_6AH;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_OAM_LLID,
                                      u4Index, u4Value);
              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-location-ID {all-zeros | all-6AH }
    PRVID   : 15
    HELP    : To configure the loopback location ID field of OAM cell format.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-location-ID 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_OAM_LLID,
                                      u4Index, AOAM_LLID_ALL_ONES);
              }
    SYNTAX  : no atm oam interface <port-type> < port-id> loopback-location-ID
    PRVID   : 15
    HELP    : To configure the default loopback location ID field of OAM cell format.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-timer <integer(1-255)> 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_TIME_OUT,
                                      u4Index, $7);
              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-timer <tx-timer-value(0-255)>
    PRVID   : 15
    HELP    : Configures the minimum amount of time to wait before sending the next OAM cell.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-timer 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_TIME_OUT,
                                      u4Index, F5OAM_DEF_TIME_OUT);
              }
    SYNTAX  : no atm oam interface <port-type> < port-id> loopback-timer 
    PRVID   : 15
    HELP    : Configures the default minimum amount of time to wait before sending the next OAM cell.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-retry-count <integer(1-255)>
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_F5OAM_RETRY_COUNT,                                       u4Index, $6);

              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-retry-count <integer(1-255)>
    PRVID   : 15
    HELP    : Configure the number of OAM loopback cells to send to the target PVC.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-retry-count 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_F5OAM_RETRY_COUNT,                                       u4Index, F5OAM_DEF_RETRY_COUNT);

              }
    SYNTAX  : no atm oam interface <port-type> < port-id> loopback-retry-count 
    PRVID   : 15
    HELP    : Configure the number of OAM loopback cells to send to the target PVC by default.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-source-ID-status {enable | disable}
    ACTION  :
              {
                 UINT4 u4Index = 0;
                 UINT4 u4Status =0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 if ($6 != NULL)
                 {
                     u4Status = AOAM_ENABLE;
                 }
                 if ($7 != NULL)
                 {
                     u4Status = AOAM_DISABLE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_SOURCE_ID_STATUS,
                                      u4Index, u4Status);

              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-source-ID-status {enable | disable}
    PRVID   : 15
    HELP    : To specify whether source ID needs to be configured or not.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-source-ID-status 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_SOURCE_ID_STATUS,
                                      u4Index, AOAM_DISABLE);

              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-source-ID-status 
    PRVID   : 15
    HELP    : To specify the default source ID status.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-source-ID {all-zeros | all-6AH }
    ACTION  :
              {
                 UINT4 u4Index = 0;
                 UINT4 u4Value = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 if ($6 != NULL)
                 {
                     u4Value = AOAM_LLID_ALL_ZEROS;
                 }
                 if ($7 != NULL)
                 {
                     u4Value = AOAM_LLID_ALL_6AH;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_SOURCE_ID,
                                      u4Index, u4Value);
              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-source-ID {all-zeros | all-6AH }
    PRVID   : 15
    HELP    : To configure the loopback source ID of OAM cell format.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-source-ID 
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_SOURCE_ID,
                                      u4Index, AOAM_LLID_ALL_ONES);
              }
    SYNTAX  : no atm oam interface <port-type> < port-id> loopback-source-ID 
    PRVID   : 15
    HELP    : To configure the default loopback source ID of OAM cell format.

    COMMAND : atm oam interface <ifXtype> <ifnum> loopback-correlation-tag <integer(0-65535)>
    ACTION  :
              {
                 UINT4 u4Index = 0;

                 if (CfaCliGetIfIndex ($3, $4, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_CORRELATION_TAG,
                                      u4Index, $6);
              }
    SYNTAX  : atm oam interface <port-type> < port-id> loopback-correlation-tag <correlation-tag(0-65535)>
    PRVID   : 15
    HELP    : To configure the correlation tag field of OAM cell format.

    COMMAND : no atm oam interface <ifXtype> <ifnum> loopback-correlation-tag 
    ACTION  :
              {
                 UINT4 u4Index;

                 if (CfaCliGetIfIndex ($4, $5, &u4Index) == CLI_FAILURE)
                 {
                     CliPrintf(CliHandle, "\r%%Invalid Interface Index\r\n");
                     return CLI_FAILURE;
                 }
                 cli_process_aoam_cmd (CliHandle, AOAM_CLI_SET_CORRELATION_TAG,
                                      u4Index, u4Index);
              }
    SYNTAX  : no atm oam interface <port-type> < port-id> loopback-correlation-tag 
    PRVID   : 15
    HELP    : To configure the correlation tag field of OAM cell format.


END GROUP

/******************************************************************************/
/*                  EXEC COMMANDS                                             */
/******************************************************************************/

DEFINE GROUP: AOAM_EXEC_CMDS

COMMAND : debug atm oam {[entry][exit][debug][fail] | all}
ACTION  : {
            UINT4 u4Trace = 0;

            if ($3 != NULL)
            {
                u4Trace = u4Trace | AOAM_FN_ENTRY;
            }
            if ($4 != NULL)
            {
                u4Trace = u4Trace | AOAM_FN_EXIT;
            }
            if ($5 != NULL)
            {
                u4Trace = u4Trace | AOAM_DBG_TRC;
            }
            if ($6 != NULL)
            {
                u4Trace = u4Trace | AOAM_FAIL_TRC;
            }
            if ($7 != NULL)
            {
               u4Trace = AOAM_ALL_TRC;
            }
            if (u4Trace == 0)
            {
                CliPrintf(CliHandle, "%% Incomplete command. \r\n");
                return CLI_FAILURE;
            }
            cli_process_aoam_cmd (CliHandle, AOAM_TRACE_ENABLE, NULL, u4Trace);
          }
SYNTAX  : debug atm oam {[entry][exit][debug][fail] | all}
PRVID   : 15
HELP    : Specifies the debug levels for ATM OAM module.

COMMAND : no debug atm oam
ACTION  : cli_process_aoam_cmd (CliHandle, AOAM_TRACE_DISABLE, NULL, NULL);
SYNTAX  : no debug atm oam
PRVID   : 15
HELP    : Disabling ATM OAM module debugging.

END GROUP

