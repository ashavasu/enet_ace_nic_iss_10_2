
#ifndef _AOAMEXTN_H
#define _AOAMEXTN_H 
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamextn.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : aoamextn.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Module                                 */
/*    MODULE NAME           : ATM OAM module externs                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains external variables          */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/


extern tAoamGlobalInfo gAoamGlobalInfo;

#endif
