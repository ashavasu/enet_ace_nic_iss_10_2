
#ifndef _AOAMINC_H
#define _AOAMINC_H
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoaminc.h,v 1.1.1.1 2008/06/04 12:49:53 iss Exp $          */                     
/*****************************************************************************/
/*    FILE  NAME            : aoaminc.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM module                                 */
/*    MODULE NAME           : ATM OAM module Header files                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains include files               */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/


#include "stdio.h"
#include "lr.h"
#include "cli.h"
#include "iss.h"
#include "cfa.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "aoamdefn.h"
#include "aoamtdfs.h"
#include "aoammacs.h"
#include "aoamextn.h"
#include "aoamprot.h"
#include "aoam.h"
#include "fsaoamlw.h"
#include "fsaoamwr.h"
#include "aoamcli.h"

#ifdef SNMP_WANTED
#include "snmp.h"
#endif
#endif
