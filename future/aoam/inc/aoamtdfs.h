
#ifndef _AOAMTDFS_H
#define _AOAMTDFS_H
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: aoamtdfs.h,       */
/*****************************************************************************/
/*    FILE  NAME            : aoamtdfs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Loopback                               */
/*    MODULE NAME           : ATM OAM Loopback structures                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains data structures             */
/*                            for ATM F5-OAM module                          */
/*---------------------------------------------------------------------------*/


typedef tCRU_BUF_CHAIN_HEADER       tAoamBufChainHeader;
typedef tCfaIfInfo                  tAoamCfaIfInfo;


/* Timer entry for F5 OAM */
typedef struct _tF5OamTmrEntry
{
   tTmrBlk          TmrBlk;          /* FSAP Timer block */
   UINT1            u1Status;        /* Status of the timer block */
   UINT1            au1Pad[3];       /* Padding */
}tF5OamTmrEntry;

typedef struct _tF5OamLoopbackPingTable{
   UINT4            u4CurrTxCells;  /* Number of cells transmitted for the current 
				       loopback */
   UINT4            u4CurrRxCells;  /* Number of cells response came after 
				      checking the connectivity */
   UINT4            u4CurrSuccessfulPings; /* Number of successful pings for the 
                                              current loopback */
   UINT4            u4CurrFailedPings;  /* Number of failed pings for the 
                                           current loopback */
   UINT1            au1CorrelationTag[AOAM_CORRELATION_TAG_LENGTH];  
				   /* Correlation tag of the OAM cell 
                                      format */
   UINT1            u1LBLocationId;
				  /* Specifies the Loopback ping location ID
                                     of OAM cell format */
   UINT1            u1SourceId;
				   /* Source ID field of OAM cell format */
   UINT1            u1PingType;   /* Specifies the Loopback ping type */  
   UINT1            u1PingCount;   /* Number of pings to send to test the 
                                     loopback */
   UINT1            u1PingTimeOut; /* Minimum time to wait before sending the 
                                     next OAM loopback cell */
   UINT1            u1SourceIdStatus;  /* Specifies whether Source ID need 
                                          to be configured or not */
   UINT1            u1EntryStatus; /* To enable/disable F5 OAM operation per PVC  */
   UINT1            u1OperStatus;  
   UINT1            u1CurrPingCount;
   BOOL1            b1TxLoopbackStatus; /* To initiate the Loopback test */
   UINT1            au1Pad[2];
}tF5OamLoopbackPingTable;


typedef struct _tF5OamLoopbackStatsTable{
   UINT4            u4LLIDInCorrects; 
   UINT4            u4EtoELBIncorrects; 
   UINT4            u4LBIndicationIncorrects;
   UINT4            u4SourceIDIncorrects;
   UINT4            u4CorrelationTagIncorrects;
   UINT4            u4LBPingTxCells; /* Total Number of cells transmitted for
                                        this PVC */
   UINT4            u4LBPingRxCells;  /* Total Number of cells recceived for
                                        this PVC */
   UINT4            u4SuccessfulPings; /* Total number of successful pings of this PVC */
   UINT4            u4FailedPings;     /* Total number of failed pings of this PVC */
}tF5OamLoopbackStatsTable;

typedef struct _tF5OamLoopbackTable
{
   tRBNodeEmbd      F5OamLBNode;
   UINT4            u4IfIndex;  /* ATM port index (PVC) */
   tF5OamTmrEntry   F5OamTmrNode; 
   tF5OamLoopbackPingTable   F5OamLBPingEntry; 
   tF5OamLoopbackStatsTable  F5OamLBStatsEntry; 
   UINT1            u1PvcOperStatus;
   UINT1            au1Pad[3];
}tF5OamLoopbackTable;

typedef struct _tF5OamGlobalInfo 
{
   tRBTree          F5AoamLBEntry;     /* RBTree for F5 Oam Loopback ping atble */
   tOsixTaskId      AoamTaskId;          /* Task Id fo F5 OAm module */
   tOsixSemId       AoamSemId;           /* Semaphore Id for F5 OAM module */
   tOsixQId         AoamCfgQId;          /* Cfg Queue ID for F5 OAM module */
   tOsixQId         AoamPktQId;           /* Task Queue ID for F5 OAM module */
   tTimerListId     AoamTmrListId;        /* Timer list ID for F5 OAM module */  
   tMemPoolId       AoamCfgQPoolId;  /* Memory pool Id for F5 OAM Cfg Queue */
   tMemPoolId       AoamPktQPoolId;  /* Memory pool Id for F5 OAM Pkt Queue */
   tMemPoolId       F5AoamLBPoolId;      /* Memory pool Id for F5 OAM Loopback Table */
   UINT4            u4DebugFlag;          /* Debug flag used for enable/disable trace */
   UINT1            u1AoamGlobalStatus;   /* Enabled global status of F5 OAM */
   UINT1            au1Pad[3];
}tAoamGlobalInfo; 

/* Used for handling informations about incoming configuration Events */
typedef struct _tAoamQMsg
{
    UINT4                   u4EventType;  /* Type of the event */
    UINT4                   u4IfIndex;     /* Incoming port */
    UINT1                   u1LineStatus;
    UINT1                   au1Pad[3];
} tAoamQMsg;

typedef struct _tAoamQInPkt
{
    tAoamBufChainHeader     *pInQMsg;     /* Packet received */
    UINT4                    u4IfIndex;    /* Interface Index */
}tAoamQInPkt;

#endif


