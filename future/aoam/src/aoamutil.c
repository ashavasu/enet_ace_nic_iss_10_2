
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamutil.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $        */
/*****************************************************************************/
/*    FILE  NAME            : aoamutil.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM                                        */
/*    MODULE NAME           : ATM OAM Util Module                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the utility funtions        */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/

#include "aoaminc.h"

PRIVATE VOID AoamUtilF5OamLBDefInit PROTO ((tF5OamLoopbackTable *));

/*****************************************************************************/
/* Function Name      : AoamUtilTrc                                          */
/*                                                                           */
/* Description        : This function prints AOAM trace message              */
/*                                                                           */
/* Input(s)           : i4TrcFlag - Trace Flag                               */
/*                      i4TraceType - Trace Type                             */
/*                      fname - Function name where this trace msg is used   */
/*                      Str - Explanation for the trace                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamUtilTrc (INT4 i4TrcFlag, INT4 i4TraceType, const CHR1 * fname,
             CHR1 * Str, ...)
{
    const CHR1         *pc1Fname = fname;
    char                ai1LogMsgBuf[AOAM_MAX_LOG_STR_LEN];
    va_list             ap;
    UINT4              *args[AOAM_MAX_TRC_ARGS];
    INT1                i1argno = 0;

    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    /* Starting with trace message */
    va_start (ap, Str);

    /* Gets the arguments if itt has any */
    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == AOAM_MAX_TRC_ARGS)
        {
            break;
        }

    }
    SNPRINTF (ai1LogMsgBuf, sizeof (ai1LogMsgBuf), "%s: %s", pc1Fname, Str);
    Str = (CHR1 *) ai1LogMsgBuf;
    UtlTrcLog (i4TrcFlag, i4TraceType, AOAM_MODULE_NAME, (const char *) Str,
               (const char *) args[0], (const char *) args[1],
               (const char *) args[2], (const char *) args[3]);
}

/*****************************************************************************/
/* Function Name      : AoamUtilRBTreeF5OamLBEntryCmp                        */
/*                                                                           */
/* Description        : Comparison function for two entries in the           */
/*                      f5 OAM loopback table                                */
/*                                                                           */
/* Input(s)           : Two PVC entries of F5 OAM loopback table             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1  = pRBElem1 > pRBElem2                             */
/*                      -1 = pRBElem1 < pRBElem2                             */
/*                      0  = both are equal                                  */
/*****************************************************************************/
INT4
AoamUtilRBTreeF5OamLBEntryCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tF5OamLoopbackTable *pF5OamLBEntry1;
    tF5OamLoopbackTable *pF5OamLBEntry2;

    pF5OamLBEntry1 = (tF5OamLoopbackTable *) pRBElem1;
    pF5OamLBEntry2 = (tF5OamLoopbackTable *) pRBElem2;

    if (pF5OamLBEntry1->u4IfIndex > pF5OamLBEntry2->u4IfIndex)
    {
        return 1;
    }
    else if (pF5OamLBEntry1->u4IfIndex < pF5OamLBEntry2->u4IfIndex)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : AoamUtilCreatePvcEntry                               */
/*                                                                           */
/* Description        : This function is to create F5 OAM PVC entry          */
/*                                                                           */
/* Input(s)           : ATM port Index. (PVC)                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS/AOAM_FAILURE                            */
/*****************************************************************************/
INT4
AoamUtilCreatePvcEntry (UINT4 u4IfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    INT4                i4RetVal = 0;

    AOAM_TRC (AOAM_FN_ENTRY, "Entering the function \r\n");

    /* Allocate memory for the F5 OAM Loopback entry */
    i4RetVal = AOAM_ALLOC_MEM_BLOCK (AOAM_F5OAM_LB_POOL_ID (), &pF5OamLBEntry);
    if ((i4RetVal) != MEM_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  " Allocation of memory for F5 OAM LB table failed." "\r\n");
        return AOAM_FAILURE;
    }

    MEMSET (pF5OamLBEntry, 0, sizeof (tF5OamLoopbackTable));

    pF5OamLBEntry->u4IfIndex = u4IfIndex;

    /* Initialise tF5OamLoopbackTable structure variables with 
     * default values */
    AoamUtilF5OamLBDefInit (pF5OamLBEntry);

    /* Add the entry to RBTree */
    i4RetVal = RBTreeAdd (AOAM_F5OAM_LB_RBTREE (), (tRBElem *) pF5OamLBEntry);

    if (i4RetVal == RB_FAILURE)
    {
        AOAM_RELEASE_MEM_BLOCK (AOAM_F5OAM_LB_POOL_ID (),
                                (UINT1 *) pF5OamLBEntry);

        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Addition of F5OAM loopback entry of line number %d"
                       "to RBTree Failed .\r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }

    AOAM_TRC (AOAM_FN_ENTRY, "Exiting the function \r\n");

    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamUtilGetPvcEntry                                  */
/*                                                                           */
/* Description        : This function is used to get F5 OAM LB entry for the */
/*                      interface index 'u4IfIndex'.                         */
/*                                                                           */
/* Input(s)           : Interface Index                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL / Pointer to the F5 OAM Loopback entry.         */
/*****************************************************************************/
tF5OamLoopbackTable *
AoamUtilGetPvcEntry (UINT4 u4IfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    tF5OamLoopbackTable F5OamLBEntry;

    MEMSET (&F5OamLBEntry, 0, sizeof (tF5OamLoopbackTable));

    F5OamLBEntry.u4IfIndex = u4IfIndex;

    /* Get the pointer to the interface index 'u4IfIndex' */
    pF5OamLBEntry = (tF5OamLoopbackTable *)
        RBTreeGet (AOAM_F5OAM_LB_RBTREE (), (tRBElem *) & (F5OamLBEntry));

    return pF5OamLBEntry;
}

/*****************************************************************************/
/* Function Name      : AoamUtilRegisterFsAoamMib                            */
/*                                                                           */
/* Description        : This function is to register the protocol MIB        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamUtilRegisterFsAoamMib (VOID)
{
    RegisterFSAOAM ();
    return;
}

/*****************************************************************************/
/* Function Name      : AoamUtilF5OamLBDefInit                               */
/*                                                                           */
/* Description        : This function is to initialise the default values    */
/*                      for F5 OAM loopback table                            */
/*                                                                           */
/* Input(s)           : pF5OamLBEntry - Pointer to F5 OAM Loopback entry     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamUtilF5OamLBDefInit (tF5OamLoopbackTable * pF5OamLBEntry)
{
    UINT1              *pu1CorrelationTag = NULL;
    UINT4               u4IfIndex = pF5OamLBEntry->u4IfIndex;

    pu1CorrelationTag = pF5OamLBEntry->F5OamLBPingEntry.au1CorrelationTag;
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrTxCells = AOAM_INIT_VAL;
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrRxCells = AOAM_INIT_VAL;
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrSuccessfulPings = AOAM_INIT_VAL;
    pF5OamLBEntry->F5OamLBPingEntry.u4CurrFailedPings = AOAM_INIT_VAL;
    pF5OamLBEntry->F5OamLBPingEntry.u1LBLocationId = AOAM_LLID_ALL_ONES;
    pF5OamLBEntry->F5OamLBPingEntry.u1SourceId = AOAM_LLID_ALL_ONES;
    pF5OamLBEntry->F5OamLBPingEntry.u1PingType = AOAM_END_TO_END_LOOPBACK;
    pF5OamLBEntry->F5OamLBPingEntry.u1PingCount = F5OAM_DEF_RETRY_COUNT;
    pF5OamLBEntry->F5OamLBPingEntry.u1PingTimeOut = F5OAM_DEF_TIME_OUT;
    pF5OamLBEntry->F5OamLBPingEntry.u1EntryStatus = AOAM_DISABLE;
    pF5OamLBEntry->F5OamLBPingEntry.u1SourceIdStatus = AOAM_DISABLE;
    pF5OamLBEntry->F5OamLBPingEntry.u1OperStatus = AOAM_FAILURE;
    pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus = AOAM_FALSE;

    pF5OamLBEntry->F5OamTmrNode.u1Status = AOAM_ENABLE;

    AOAM_PUT_4BYTE (pu1CorrelationTag, u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : AoamUtilDeletePvcEntry                               */
/*                                                                           */
/* Description        : This function is to delete F5 OAM PVC entry          */
/*                                                                           */
/* Input(s)           : ATM port Index. (PVC)                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS/AOAM_FAILURE                            */
/*****************************************************************************/
INT4
AoamUtilDeletePvcEntry (UINT4 u4IfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    INT4                i4RetVal = 0;

    AOAM_TRC (AOAM_FN_ENTRY, "Entering the function %s \r\n");

    /* Get the F5 OAM Loopback entry */
    pF5OamLBEntry = AoamUtilGetPvcEntry (u4IfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC (AOAM_FAIL_TRC, " Unable to find the F5 OAM LB entry \r\n");
        return AOAM_FAILURE;

    }

    /* Remove the entry from the RBTree */
    i4RetVal = RBTreeRemove (AOAM_F5OAM_LB_RBTREE (),
                             (tRBElem *) pF5OamLBEntry);
    if (i4RetVal != RB_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Removal of RBTree failed for line entry \r\n");
        return AOAM_FAILURE;
    }
    /* Release the memory block to pool */
    MemReleaseMemBlock (AOAM_F5OAM_LB_POOL_ID (), (UINT1 *) pF5OamLBEntry);

    AOAM_TRC (AOAM_FN_EXIT, "Exiting the funtion. \r\n");
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DcsUtilRbTreeLBEntryFree                             */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for F5OAM LB entry         */
/*                                                                           */
/* Input(s)           : pF5OamLBEntry - Pointer to interface entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamUtilRBTreeLBEntryFree (tF5OamLoopbackTable * pF5OamLBEntry)
{
    if (pF5OamLBEntry != NULL)
    {
        AOAM_RELEASE_MEM_BLOCK (AOAM_F5OAM_LB_POOL_ID (), pF5OamLBEntry);
    }
}

/*****************************************************************************/
/* Function Name      : AoamCopyPduBufInfo                                   */
/*                                                                           */
/* Description        : This function is called from a function which        */
/*                      initialises the pointer ppu1ToLinearBuf to point to  */
/*                      an array. This function initializes the pointer to   */
/*                      either point to the start of the valid data byte if  */
/*                      CRU BUF is linear or it fills the array to which the */
/*                      pointer points.                                      */
/*                                                                           */
/* Input(s)           : Pointer to CRU buf chain header.                     */
/*                                                                           */
/* Output(s)          : Pointer to the linear buffer, which contains the PDU */
/*                      bytes. The linear buffer may be data buffer in the   */
/*                      CRU buf chain if the data buffer is linear and       */
/*                      contiguous.Otherwise, the data from the CRU buffer   */
/*                      is put in linear buffer that is passed   .           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS - On success                            */
/*                      AOAM_FAILURE - On failure                            */
/*****************************************************************************/
INT4
AoamCopyPduBufInfo (tAoamBufChainHeader * pBuf, UINT1 **ppu1ToLinearBuf)
{
    UINT1              *pu1Ptr = NULL;

    if ((pBuf == NULL) || (ppu1ToLinearBuf == NULL))
    {

        AOAM_TRC (AOAM_FAIL_TRC, "UTILS: BUFFER POINTER NULL !!!\n");
        return AOAM_FAILURE;
    }

    pu1Ptr = AOAM_IS_CRU_BUF_LINEAR (pBuf, AOAM_OFFSET, AOAM_BPDU_SIZE);

    if (pu1Ptr != NULL)
    {

        *ppu1ToLinearBuf = pu1Ptr;
        return AOAM_SUCCESS;
    }
    else
    {

        if (AOAM_COPY_FROM_CRU_BUF (pBuf, *ppu1ToLinearBuf, AOAM_OFFSET,
                                    AOAM_BPDU_SIZE) != AOAM_CRU_SUCCESS)
        {

            AOAM_TRC (AOAM_FAIL_TRC, "UTILS: Copy From CRU Buffer FAILED !!\n");
            return AOAM_FAILURE;
        }

        return AOAM_SUCCESS;
    }

}
