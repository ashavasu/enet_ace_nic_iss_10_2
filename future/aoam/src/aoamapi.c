
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamapi.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $        */
/*****************************************************************************/
/*    FILE  NAME            : aoamapi.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM module                                 */
/*    MODULE NAME           : ATM OAM global functions definitions Module    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the global functions        */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/

#include "aoaminc.h"

/******************************************************************************
 *
 *    FUNCTION NAME    : AoamApiLock
 *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol 
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
AoamApiLock (VOID)
{

    if (AOAM_SEM_TAKE (AOAM_SEM_ID ()) == OSIX_FAILURE)
    {
        AOAM_TRC (OS_RESOURCE_TRC, "AoamApiLock: Take sem FAILED !!!\n");
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : AoamApiUnLock 
 *                                                                          
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *                                                                          
 *****************************************************************************/
PUBLIC INT4
AoamApiUnLock (VOID)
{

    AOAM_SEM_GIVE (AOAM_SEM_ID ());

    return (SNMP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : AoamApiPostCreatePortMsg                             */
/*                                                                           */
/* Description        : This function is used to post create port message    */
/*                      to ATM OAM module                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC INT4
AoamApiPostCreatePortMsg (UINT4 u4IfIndex)
{
    tAoamQMsg          *pMesg = NULL;

    AOAM_TRC_ARG1 (AOAM_FN_ENTRY,
                   "Called AoamPostCreatePortMsg for Port %ld\n", u4IfIndex);

    if (AOAM_IS_VALID_PORT (u4IfIndex) != AOAM_TRUE)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Port Invalid \n");
        return AOAM_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if (AOAM_ALLOC_MEM_BLOCK (AOAM_CFG_Q_POOL_ID (), (UINT1 **) &pMesg) !=
        MEM_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Queue Message ALLOC_MEM_BLOCK FAILED for port: %ld\n",
                       u4IfIndex);
        return AOAM_FAILURE;
    }

    MEMSET (pMesg, 0, sizeof (tAoamQMsg));

    pMesg->u4EventType = AOAM_PORT_CREATE_MSG;
    pMesg->u4IfIndex = u4IfIndex;

    if (AOAM_QUEUE_SEND (AOAM_CFG_QUEUE_ID (), (UINT1 *) &pMesg,
                         sizeof (tAoamQMsg)) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Port :  %ld : AOAM_Enqueue Control Message FAILED\n",
                       u4IfIndex);

        if (MemReleaseMemBlock (AOAM_CFG_Q_POOL_ID (), (UINT1 *) pMesg) !=
            MEM_SUCCESS)
        {

            AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                           "Port %ld: AOAM FREE MEM BLOCK FAILED\n", u4IfIndex);
        }
        return AOAM_FAILURE;
    }

    if (AOAM_EVENT_SEND (AOAM_TASK_ID (), AOAM_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       " Port %lu :Send Event FAILED\n", u4IfIndex);
        /* Cannot undo enqueue.. */
    }

    L2_SYNC_TAKE_SEM ();
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamApiPostDeletePortMsg                             */
/*                                                                           */
/* Description        : This function is used to post delete port message    */
/*                      to ATM OAM module                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC INT4
AoamApiPostDeletePortMsg (UINT4 u4IfIndex)
{
    tAoamQMsg          *pMesg = NULL;

    AOAM_TRC_ARG1 (AOAM_FN_ENTRY,
                   "Called AoamPostDeletePortMsg for Port %ld\n", u4IfIndex);

    if (AOAM_IS_VALID_PORT (u4IfIndex) != AOAM_TRUE)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Port Invalid \n");
        return AOAM_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if (AOAM_ALLOC_MEM_BLOCK (AOAM_CFG_Q_POOL_ID (), (UINT1 **) &pMesg) !=
        MEM_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Queue Message ALLOC_MEM_BLOCK FAILED for port: %ld\n",
                       u4IfIndex);
        return AOAM_FAILURE;
    }

    MEMSET (pMesg, 0, sizeof (tAoamQMsg));

    pMesg->u4EventType = AOAM_PORT_DELETE_MSG;
    pMesg->u4IfIndex = u4IfIndex;

    if (AOAM_QUEUE_SEND (AOAM_CFG_QUEUE_ID (), (UINT1 *) &pMesg,
                         sizeof (tAoamQMsg)) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Port :  %ld : AOAM_Enqueue Control Message FAILED\n",
                       u4IfIndex);

        if (MemReleaseMemBlock (AOAM_CFG_Q_POOL_ID (), (UINT1 *) pMesg) !=
            MEM_SUCCESS)
        {

            AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                           "Port %ld: AOAM FREE MEM BLOCK FAILED\n", u4IfIndex);
        }
        return AOAM_FAILURE;
    }

    if (AOAM_EVENT_SEND (AOAM_TASK_ID (), AOAM_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       " Port %lu :Send Event FAILED\n", u4IfIndex);
        /* Cannot undo enqueue.. */
    }

    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamApiPostPortStatusMsg                             */
/*                                                                           */
/* Description        : This function is used to post oper status message    */
/*                      of a PVC to ATM OAM module                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC INT4
AoamApiPostPortStatusMsg (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tAoamQMsg          *pMesg = NULL;

    AOAM_TRC_ARG1 (AOAM_FN_ENTRY,
                   "Called AoamApiPostPortStatusMsg for Port %ld\n", u4IfIndex);

    if (AOAM_IS_VALID_PORT (u4IfIndex) != AOAM_TRUE)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Port Invalid \n");
        return AOAM_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if (AOAM_ALLOC_MEM_BLOCK (AOAM_CFG_Q_POOL_ID (), (UINT1 **) &pMesg) !=
        MEM_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Queue Message ALLOC_MEM_BLOCK FAILED for port: %ld\n",
                       u4IfIndex);
        return AOAM_FAILURE;
    }

    MEMSET (pMesg, 0, sizeof (tAoamQMsg));

    pMesg->u4IfIndex = u4IfIndex;

    switch (u1OperStatus)
    {
        case AOAM_IF_UP:
            pMesg->u4EventType = AOAM_PORT_OPER_UP_MSG;
            break;
        case AOAM_IF_DOWN:
            pMesg->u4EventType = AOAM_PORT_OPER_DOWN_MSG;
            break;
        default:
            return AOAM_FAILURE;
    }

    if (AOAM_QUEUE_SEND (AOAM_CFG_QUEUE_ID (), (UINT1 *) &pMesg,
                         sizeof (tAoamQMsg)) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Port :  %ld : AOAM_Enqueue Control Message FAILED\n",
                       u4IfIndex);

        if (MemReleaseMemBlock (AOAM_CFG_Q_POOL_ID (), (UINT1 *) pMesg) !=
            MEM_SUCCESS)
        {

            AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                           "Port %ld: AOAM FREE MEM BLOCK FAILED\n", u4IfIndex);
        }
        return AOAM_FAILURE;
    }

    if (AOAM_EVENT_SEND (AOAM_TASK_ID (), AOAM_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       " Port %lu :Send Event FAILED\n", u4IfIndex);
        /* Cannot undo enqueue.. */
    }

    return AOAM_SUCCESS;
}
