
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoamcli.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : aoamcli.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM                                        */
/*    MODULE NAME           : ATM OAM Cli Module                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI SET/GET/TEST and        */
/*                            GETNEXT routines for the MIB objects           */
/*                            specified in fsaoam.mib funtions for ATM       */
/*                            OAM Cli module                                 */
/*---------------------------------------------------------------------------*/

#ifndef __AOAMCLI_C__
#define __AOAMCLI_C__

#include "aoaminc.h"

/*************************************************************************
 * 
 *  FUNCTION NAME   : cli_process_aoam_cmd 
 *  
 *  DESCRIPTION     : CLI message handler function               
 *                                                                        
 *  INPUT           : CliHandle - CliContext ID                           
 *                    u4Command - Command identifier                      
 *                    .. -Variable command argument list                 
 *                                                                        
 *  OUTPUT          : None                                                
 *                                                                        
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE                             
 *
 **************************************************************************/

INT4
cli_process_aoam_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT4                i4RetStatus = 0;
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4TraceFlag = 0;
    UINT4               u4Inst = 0;

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    u4Inst = (UINT4) va_arg (ap, UINT4 *);

    /* Walk through the rest of the arguments and store in args array,
     * till the number of arguments reaches CLI_MAX_ARGS */
    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    CliRegisterLock (CliHandle, AOAM_LOCK, AOAM_UNLOCK);
    AOAM_LOCK ();

    switch (u4Command)
    {
        case AOAM_CLI_SET_F5OAM_PING_TYPE:
        case AOAM_CLI_SET_OAM_LLID:
        case AOAM_CLI_SET_TIME_OUT:
        case AOAM_CLI_SET_F5OAM_RETRY_COUNT:
        case AOAM_CLI_SET_SOURCE_ID:
        case AOAM_CLI_SET_CORRELATION_TAG:
        {
            i4RetStatus =
                AoamCliConfOAMCell (CliHandle, (INT4) u4Inst,
                                    *(UINT4 *) args[1], u4Command);
            break;
        }
        case AOAM_CLI_GLOBAL_F5OAM_STATUS:
        {
            i4RetStatus =
                AoamCliSetGlobalAoamStatus (CliHandle, (INT4) args[0]);
            break;
        }
        case AOAM_CLI_ENABLE_F5OAM_PER_PVC:
        {
            i4RetStatus =
                AoamCliSetPerPVCAoamStatus (CliHandle, (INT4) u4Inst,
                                            AOAM_ENABLE);
            break;
        }
        case AOAM_CLI_DISABLE_F5OAM_PER_PVC:
        {
            i4RetStatus =
                AoamCliSetPerPVCAoamStatus (CliHandle, (INT4) u4Inst,
                                            AOAM_DISABLE);
            break;
        }
        case AOAM_CLI_SET_SOURCE_ID_STATUS:
        {
            i4RetStatus =
                AoamCliSetSourceIdStatus (CliHandle, (INT4) u4Inst,
                                          (INT4) args[0]);
            break;
        }
        case AOAM_CLI_PING_ATM:
        {
            i4RetStatus = AoamCliSendOamCell (CliHandle, (INT4) u4Inst);
            break;
        }
        case AOAM_TRACE_ENABLE:
        {
            u4TraceFlag = *(UINT4 *) args[0];
            i4RetStatus = AoamCliSetTraceFlag (u4TraceFlag);
            break;
        }
        case AOAM_TRACE_DISABLE:
        {
            u4TraceFlag = 0;
            i4RetStatus = AoamCliSetTraceFlag (u4TraceFlag);
            break;
        }
        default:
            CliPrintf (CliHandle, "\r%% Unknown Command \r\n");
            AOAM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_AOAM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", AoamCliErrorString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    AOAM_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

INT4
AoamCliConfOAMCell (tCliHandle CliHandle, INT4 i4IfIndex,
                    UINT4 u4ConfValue, UINT4 u4Command)
{
    tSNMP_OCTET_STRING_TYPE CorrelationTag;
    UINT1              *pu1CorrTag = NULL;
    UINT1               au1CorrTag[AOAM_CORRELATION_TAG_LENGTH];
    UINT4               u4ErrCode = 0;

    switch (u4Command)
    {
        case AOAM_CLI_SET_F5OAM_PING_TYPE:
        {
            if (nmhTestv2FsF5OAMLoopbackPingType (&u4ErrCode, i4IfIndex,
                                                  (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingType (i4IfIndex, (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        case AOAM_CLI_SET_OAM_LLID:
        {
            if (nmhTestv2FsF5OAMLoopbackPingLocationID (&u4ErrCode, i4IfIndex,
                                                        (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingLocationID (i4IfIndex,
                                                     (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        case AOAM_CLI_SET_TIME_OUT:
        {
            if (nmhTestv2FsF5OAMLoopbackPingTimeout (&u4ErrCode, i4IfIndex,
                                                     (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingTimeout (i4IfIndex, (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        case AOAM_CLI_SET_F5OAM_RETRY_COUNT:
        {
            if (nmhTestv2FsF5OAMLoopbackPingCount (&u4ErrCode, i4IfIndex,
                                                   (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingCount (i4IfIndex, (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return SNMP_SUCCESS;
            }
            break;
        }
        case AOAM_CLI_SET_SOURCE_ID:
        {
            if (nmhTestv2FsF5OAMLoopbackPingSourceID (&u4ErrCode, i4IfIndex,
                                                      (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingSourceID (i4IfIndex,
                                                   (INT4) u4ConfValue)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        case AOAM_CLI_SET_CORRELATION_TAG:
        {
            MEMSET (au1CorrTag, 0, AOAM_CORRELATION_TAG_LENGTH);
            pu1CorrTag = au1CorrTag;
            MEMSET (&CorrelationTag, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            CorrelationTag.i4_Length = AOAM_CORRELATION_TAG_LENGTH;
            CorrelationTag.pu1_OctetList = au1CorrTag;
            AOAM_PUT_4BYTE (pu1CorrTag, u4ConfValue);

            if (nmhTestv2FsF5OAMLoopbackPingCorrelationTag (&u4ErrCode,
                                                            i4IfIndex,
                                                            &CorrelationTag)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsF5OAMLoopbackPingCorrelationTag (i4IfIndex,
                                                         &CorrelationTag)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        default:
            return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
AoamCliSetPerPVCAoamStatus (tCliHandle CliHandle, INT4 i4IfIndex,
                            INT4 i4AoamStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsF5OAMLoopbackPingEntryStatus (&u4ErrCode, i4IfIndex,
                                                 i4AoamStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsF5OAMLoopbackPingEntryStatus (i4IfIndex, i4AoamStatus)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
AoamCliSetSourceIdStatus (tCliHandle CliHandle, INT4 i4IfIndex,
                          INT4 i4SrcIdStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsF5OAMLoopbackPingSourceIDStatus (&u4ErrCode, i4IfIndex,
                                                    i4SrcIdStatus)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsF5OAMLoopbackPingSourceIDStatus (i4IfIndex, i4SrcIdStatus)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
AoamCliSetGlobalAoamStatus (tCliHandle CliHandle, INT4 i4AoamStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsF5OAMEntryStatus (&u4ErrCode, i4AoamStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsF5OAMEntryStatus (i4AoamStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
AoamCliSendOamCell (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsF5OAMLoopbackPingTxLoopback (&u4ErrCode, i4IfIndex,
                                                AOAM_TRUE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsF5OAMLoopbackPingTxLoopback (i4IfIndex,
                                             AOAM_TRUE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
AoamCliSetTraceFlag (UINT4 u4TraceFlag)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsF5OAMDebug (&u4ErrCode, (INT4) u4TraceFlag) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsF5OAMDebug ((INT4) u4TraceFlag) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
