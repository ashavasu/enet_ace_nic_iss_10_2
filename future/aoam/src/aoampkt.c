
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc. All Rights Reserved                       */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: aoampkt.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $          */
/*****************************************************************************/
/*    FILE  NAME            : aoampkt.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM Loopback testing                       */
/*    MODULE NAME           : ATM OAM loopback test packet handline module   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the packet handling         */
/*                            functions for ATM OAM loopback test module     */
/*---------------------------------------------------------------------------*/

#include "aoaminc.h"

/*****************************************************************************/
/* Function Name      : AoamPktTxOamCell                                     */
/*                                                                           */
/* Description        : This function is used to formation of OAM cell       */
/*                      and to transmit the OAM cell.                        */
/*                                                                           */
/* Input(s)           : pF5OamLBEntry - F5 OAM Loopback entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamPktTxOamCell (tF5OamLoopbackTable * pF5OamLBEntry)
{
    tAoamBufChainHeader *pAoamBpdu = NULL;
    UINT4               u4DataLength = AOAM_INIT_VAL;

    /* Allocate buffer for the packet formation */
    if ((pAoamBpdu =
         AOAM_ALLOC_CRU_BUF (AOAM_BPDU_HEADER_SIZE + AOAM_BPDU_PAYLOAD_SIZE,
                             AOAM_OFFSET)) == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in allocating CRU buffer  "
                       "for port: %d\r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }

    /* Formation of 48 bytes payload for OAM end-to-end loopback cell  */
    if (AoamPktFormBpdu (pAoamBpdu, pF5OamLBEntry, &u4DataLength)
        == AOAM_FAILURE)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in formation of payload field of OAM cell for "
                       "port: %d \r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }

    /* Filling the header of OAM loopback cell */
    if (AoamPktFillHeader (pAoamBpdu, pF5OamLBEntry) == AOAM_FAILURE)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in formation of header filed of OAM cell for "
                       " port : %d \r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }
    /* AOAM cell can be send if the global AOAM status and AOAM status 
     * per PVC is enabled */
    if ((AOAM_ADMIN_STATUS () == AOAM_ENABLE) &&
        (pF5OamLBEntry->F5OamLBPingEntry.u1EntryStatus == AOAM_ENABLE) &&
        (pF5OamLBEntry->u1PvcOperStatus == AOAM_IF_UP))
    {
        TmrStart (AOAM_TMR_LIST_ID (),
                  &(pF5OamLBEntry->F5OamTmrNode.TmrBlk),
                  AOAM_LOOPBACK_TIMER,
                  pF5OamLBEntry->F5OamLBPingEntry.u1PingTimeOut, 0);
        /* Set the F5 OAM timer status as enabled */
        pF5OamLBEntry->F5OamTmrNode.u1Status = AOAM_ENABLE;
        /* Increment the current ping count in order to trace the
         * number of OAM cells among the max counts */
        AOAM_INCR_CURR_PING_COUNT (pF5OamLBEntry);
    }
    else
    {
        AOAM_RELEASE_CRU_BUF (pAoamBpdu, AOAM_FALSE);
    }
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamPktTxOamCell                                     */
/*                                                                           */
/* Description        : This function is used to formation of OAM loopback   */
/*                      cell's payload information.                          */
/*                                                                           */
/* Input(s)           : pF5OamLBEntry - F5 OAM Loopback entry                */
/*                      pAoamBpdu - CRU buffer to copy the payload info      */
/*                                                                           */
/* Output(s)          : pu4DataLength - Length of the cell.                  */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamPktFormBpdu (tAoamBufChainHeader * pAoamBpdu,
                 tF5OamLoopbackTable * pF5OamLBEntry, UINT4 *pu4DataLength)
{
    UINT1               au1AoamBpdu[AOAM_BPDU_PAYLOAD_SIZE];
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1SrcId = 0;
    UINT1               u1LBLId = 0;
    UINT1               u1LBIndication = 0;
    UINT1               u1CellType = 0;
    UINT1               u1FuncType = 0;
    UINT1               u1UnusedOctet = 0;
    UINT4               u4Length = 0;

    MEMSET (au1AoamBpdu, 0, AOAM_BPDU_PAYLOAD_SIZE);

    pu1BpduStart = pu1BpduEnd = au1AoamBpdu;

    /* In the OAM payload format, payload field starts from 6th octet. 
     * In the 6th octet, first 4 bits occupies the OAM cell type (In our case, 
     * OAM cell type is fault management) and second 4 bits occupies the 
     * OAM function type (In our case, we have to fill OAM loopback */

    u1CellType = (UINT1) AOAM_CELL_TYPE_FAULT_MGMT;
    u1FuncType = (UINT1) AOAM_FUNC_TYPE_LOOPBACK;
    u1CellType = (u1CellType << AOAM_CELL_TYPE_SHIFT);
    u1CellType = (u1CellType | u1FuncType);

    AOAM_PUT_1BYTE (pu1BpduEnd, u1CellType);

    /* 7th octet in the OAM payload format indicates the loopback 
     * indication. Source fills this field as 00000001 and while 
     * during response sink fills this field as 00000000*/
    u1LBIndication = (UINT1) AOAM_SOURCE_LB_INDICATION;

    AOAM_PUT_1BYTE (pu1BpduEnd, u1LBIndication);

    /* 8th octet to 11th octet needs to filled with correlation tag 
     * field */
    MEMCPY (pu1BpduEnd, pF5OamLBEntry->F5OamLBPingEntry.au1CorrelationTag,
            AOAM_CORRELATION_TAG_LENGTH);

    pu1BpduEnd += AOAM_CORRELATION_TAG_LENGTH;

    /* 12th octet to 27th octet is filled with Loopback location ID */
    AOAM_MAP_LOCATION_ID (pF5OamLBEntry->F5OamLBPingEntry.u1LBLocationId,
                          u1LBLId);

    AOAM_PUT_BYTES (pu1BpduEnd, u1LBLId, (UINT1) AOAM_LLID_LENGTH);

    /* 28th octet to 43rd octet is fiiled with source location ID */
    AOAM_MAP_LOCATION_ID (pF5OamLBEntry->F5OamLBPingEntry.u1SourceId, u1SrcId);

    AOAM_PUT_BYTES (pu1BpduEnd, u1SrcId, (UINT1) AOAM_SOURCE_ID_LENGTH);

    /* 44th octet to 51th octet are used octets in the OAM cell format */
    u1UnusedOctet = (UINT1) AOAM_UNUSED_OCTET_VALUE;

    AOAM_PUT_BYTES (pu1BpduEnd, u1UnusedOctet, AOAM_UNUSED_PAYLOAD);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    u4Length = *pu4DataLength;

    if (AOAM_COPY_OVER_CRU_BUF (pAoamBpdu, au1AoamBpdu, AOAM_BPDU_HEADER_SIZE,
                                u4Length) != AOAM_CRU_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in copying payload over CRU buffer for index :"
                       " %d\r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamPktTxOamCell                                     */
/*                                                                           */
/* Description        : This function is used to formation of OAM loopback   */
/*                      cell's header information.                           */
/*                                                                           */
/* Input(s)           : pF5OamLBEntry - F5 OAM Loopback entry                */
/*                      pAoamBpdu - CRU buffer to copy the header  info      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamPktFillHeader (tAoamBufChainHeader * pAoamBpdu,
                   tF5OamLoopbackTable * pF5OamLBEntry)
{
    INT4                i4Vci = 0;
    INT4                i4Vpi = 0;
    UINT4               u4DslIfIndex = 0;
    UINT4               u4Vci = 0;
    UINT1               u1Pti = 0;
    UINT4               u4Header = 0;
    UINT1               au1AoamHeader[AOAM_BPDU_HEADER_SIZE];
    UINT1              *pu1AoamHeader = NULL;

    MEMSET (au1AoamHeader, 0, sizeof (au1AoamHeader));

    pu1AoamHeader = au1AoamHeader;

    if (CfaGetVciVpiDslIfIndex (pF5OamLBEntry->u4IfIndex, &u4DslIfIndex, &i4Vpi,
                                &i4Vci) != CFA_SUCCESS)
    {
        return AOAM_FAILURE;
    }
    /* Header Info is of the following format */

    /* ---------------------------------------------------------------------
     * | 4 bits   | 8 bits    |   16 bits   |  3 bits  |  1 bit | 8 bits   |   
     * |  GFC     |  VPI      |    VCI      |   PTI    |   CLP  |  HEC     |
     * --------------------------------------------------------------------
     */

    /* Payload type is end to end loopback type */
    u1Pti = AOAM_END_TO_END_PTI;

    u4Header = (((UINT4) i4Vpi) << AOAM_VPI_SHIFT);

    u4Vci = (((UINT4) i4Vci) << AOAM_VCI_SHIFT);

    u4Header = u4Header | u4Vci;

    u1Pti = u1Pti << AOAM_PTI_SHIFT;

    u4Header = (u4Header | ((UINT4) u1Pti));

    AOAM_PUT_4BYTE (pu1AoamHeader, u4Header);

    AOAM_PUT_1BYTE (pu1AoamHeader, AOAM_DEF_HEC);

    if (AOAM_COPY_OVER_CRU_BUF (pAoamBpdu, au1AoamHeader, 0,
                                AOAM_BPDU_HEADER_SIZE) != AOAM_CRU_SUCCESS)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in copying header over CRU buffer for index :"
                       " %d\r\n", pF5OamLBEntry->u4IfIndex);
        return AOAM_FAILURE;
    }
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamPktTxOamCell                                     */
/*                                                                           */
/* Description        : This function is used to handle the OAM loopback     */
/*                      cell received in the AOAM task                       */
/*                                                                           */
/* Input(s)           : pBuf - OAM cell to be processed                      */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamProcessLBOamCell (tAoamBufChainHeader * pBuf, UINT4 u4IfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    UINT1               au1LinearPdu[AOAM_BPDU_SIZE + 1];
    UINT1              *pu1LinearPdu = NULL;
    UINT1              *pu1Buffer = NULL;
    UINT1              *pu1Header = NULL;
    UINT1               au1Header[AOAM_BPDU_HEADER_SIZE + 3];
    UINT1               u1LBId = 0;

    MEMSET (au1LinearPdu, 0, AOAM_BPDU_SIZE + 1);
    MEMSET (au1Header, 0, AOAM_BPDU_HEADER_SIZE + 3);
    pu1Header = au1Header;

    /* Check the global AOAM status , loopback cell can be processed only 
     * when the status is enabled. */
    if (AOAM_ADMIN_STATUS () != AOAM_ENABLE)
    {
        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release while global aoam status is "
                           "disabled failed for index: %d\r\n", u4IfIndex);
        }
        return;
    }
    /* Check whether the CRU buffer has OAM cell information or not */
    if (pBuf == NULL)
    {
        AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                       "CRU buffer received NULL pointer for index : %d \r\n",
                       u4IfIndex);
        return;
    }

    /* Getting pointer to the F5 OAM loopback entry for this port */
    if ((pF5OamLBEntry = AoamUtilGetPvcEntry (u4IfIndex)) == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "AOAM cell received on invalid port : %d\r\n",
                       u4IfIndex);

        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release failed while AOAM cell received"
                           "on invalid port : %d\r\n", u4IfIndex);
        }
        return;
    }
    /* The OAM cells has to processed when oper status of the 
     * PVC is UP. If the oper status is down, discard the packet */
    if (pF5OamLBEntry->u1PvcOperStatus == AOAM_IF_UP)
    {
        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release failed on the "
                           "port : %d\r\n", u4IfIndex);
        }
        return;
    }
    /* Store the address of linear buffer in another pointer */
    pu1LinearPdu = au1LinearPdu;

    /* Copy the OAM cell fields from the CRU buffer */
    if (AoamCopyPduBufInfo (pBuf, &pu1LinearPdu) != AOAM_SUCCESS)
    {
        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release failed when copying to linear "
                           "buffer failed on port : %d\r\n", u4IfIndex);
        }
        return;
    }

    /* Store the pointer to the linear buffer in another pointer */
    pu1Buffer = pu1LinearPdu;

    MEMCPY (pu1Header, pu1LinearPdu, AOAM_BPDU_HEADER_SIZE);

    /* Validation of header infomation (VCI, VPI and PTI values) */
    if (AoamValidateHeaderInfo (pu1Header, u4IfIndex) != AOAM_SUCCESS)
    {
        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release failed  when header "
                           "buffer failed on port : %d\r\n", u4IfIndex);
        }
        return;
    }
    /* Validation of payload information, which analyses whether packet 
     * received is a loopback cell or looped back cell */
    if (AoamValidatePayload (pu1Buffer, pF5OamLBEntry, &u1LBId) != AOAM_SUCCESS)
    {
        if (AOAM_RELEASE_CRU_BUF (pBuf, AOAM_FALSE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (CONTROL_PLANE_TRC | AOAM_FAIL_TRC,
                           "CRU buffer release failed  when header "
                           "buffer failed on port : %d\r\n", u4IfIndex);
        }
        return;
    }
    /* If the Loopback indication is '1'(AOAM_SOURCE_LB_INDICATION), 
     * then OAM cell has to be send back to other end by changing the 
     * loopback indication field as '0'.  */
    if (u1LBId == AOAM_SOURCE_LB_INDICATION)
    {
        au1LinearPdu[AOAM_LB_INDICATION_BYTE] = AOAM_SINK_LB_INDICATION;

        if (AOAM_COPY_OVER_CRU_BUF (pBuf, au1LinearPdu, AOAM_OFFSET,
                                    AOAM_BPDU_SIZE) != AOAM_CRU_SUCCESS)
        {
            AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                           "Failed in copying over CRU buffer for index :"
                           " %d\r\n", u4IfIndex);
            return;
        }
        /* Response has to send to the higher layers (CFA). */
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AoamValidateHeaderInfo                               */
/*                                                                           */
/* Description        : This function is used to validate the header info    */
/*                      received in the OAM cell                             */
/*                                                                           */
/* Input(s)           : pu1Header - Header information                       */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamValidateHeaderInfo (UINT1 *pu1Header, UINT4 u4IfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    UINT4               u4DslIfIndex = 0;
    INT4                i4Vpi = 0;
    INT4                i4Vci = 0;
    UINT2               u2PktVci = 0;
    UINT1               u1PktVpi = 0;
    UINT1               u1PktPti = 0;
    UINT1               u1Pti = 0;

    /* Get the VCI and VPI values present in the received OAM 
     * cell header format */
    AOAM_GET_VCI_VPI (pu1Header, &u1PktVpi, &u2PktVci);

    /* Get the VCI and VPI values for this port */
    if (CfaGetVciVpiDslIfIndex (u4IfIndex, &u4DslIfIndex, &i4Vpi,
                                &i4Vci) != CFA_SUCCESS)
    {
        return AOAM_FAILURE;
    }

    /* VPI and VPI values present in the OAM cell header should be 
     * same as VCI and VPI values of this port */
    if ((u1PktVpi != (UINT1) i4Vpi) || (u2PktVci != (UINT1) i4Vci))
    {
        /* If VPI and VCI values are not same, discard the packet */
        return AOAM_FAILURE;
    }

    pu1Header += AOAM_PTI_OFFSET;

    AOAM_GET_1BYTE (u1PktPti, pu1Header);

    /* Get the Payload type of the received OAM cell */
    AOAM_GET_PTI (&u1Pti, u1PktPti);

    /* As we support only end to end loopback as of now, PTI type 
     * should be END to END loopback in the received OAM pkt */
    if (u1Pti != AOAM_END_TO_END_PTI)
    {
        /* Getting pointer to the F5 OAM loopback entry for this port */
        pF5OamLBEntry = AoamUtilGetPvcEntry (u4IfIndex);
        /* Discard the OAM cell */
        AOAM_INCR_ETE_LB_DISCARDS (pF5OamLBEntry);
        return AOAM_FAILURE;
    }

    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamValidatePayload                                  */
/*                                                                           */
/* Description        : This function is used to validate the payload info   */
/*                      received in the OAM cell                             */
/*                                                                           */
/* Input(s)           : pu1Buffer - OAM cell info                            */
/*                      pF5OamLBEntry - F5 OAM Loopback entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamValidatePayload (UINT1 *pu1Buffer, tF5OamLoopbackTable * pF5OamLBEntry,
                     UINT1 *pu1LBId)
{
    UINT1              *pu1BpduStart = NULL;
    UINT1               u1LBIndication = 0;

    pu1BpduStart = pu1Buffer;

    pu1Buffer += AOAM_BPDU_HEADER_SIZE + 1;

    AOAM_GET_1BYTE (u1LBIndication, pu1Buffer);

    /* Check the loopback indication field of the OAM cell format */
    if (u1LBIndication == AOAM_SOURCE_LB_INDICATION)
    {
        /* Loopback is in progress. Loopback is received from one end, 
         * check whether loopback location Id is correct */
        if (AoamProcessRecvdLBCell (pu1BpduStart,
                                    pF5OamLBEntry) != AOAM_SUCCESS)
        {
            AOAM_TRC (AOAM_FAIL_TRC, "failed in AoamProcessRecvdLBCell. \r\n");
            return AOAM_FAILURE;
        }

    }
    else if (u1LBIndication == AOAM_SINK_LB_INDICATION)
    {
        /* OAM LB cell is received back to the source point. Validate 
         * the other fields in the OAM cell format */
        if (AoamProcessLoopedBackCell (pu1BpduStart,
                                       pF5OamLBEntry) != AOAM_SUCCESS)
        {
            AOAM_TRC (AOAM_FAIL_TRC, "Failed in AoamProcessLoopedBackCell\r\n");
            return AOAM_FAILURE;
        }
    }
    *pu1LBId = u1LBIndication;
    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamProcessRecvdLBCell                               */
/*                                                                           */
/* Description        : This function is used to validate and process the    */
/*                      received in the LB OAM cell                          */
/*                                                                           */
/* Input(s)           : pu1Buffer - OAM cell info                            */
/*                      pF5OamLBEntry - F5 OAM Loopback entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamProcessRecvdLBCell (UINT1 *pu1Buffer, tF5OamLoopbackTable * pF5OamLBEntry)
{
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1LBLId = NULL;
    UINT1               au1LBLId[AOAM_LLID_LENGTH];
    UINT1               u1LBLocationId = 0;

    MEMSET (au1LBLId, 0, AOAM_LLID_LENGTH);

    pu1LBLId = au1LBLId;
    pu1BpduStart = pu1Buffer;

    /* Move the pointer location to the loopback location 
     * Id field in the OAM cell */
    pu1Buffer += AOAM_LB_LOCATION_START;

    AOAM_MAP_LOCATION_ID (pF5OamLBEntry->
                          F5OamLBPingEntry.u1LBLocationId, u1LBLocationId);

    AOAM_PUT_BYTES (pu1LBLId, u1LBLocationId, AOAM_LLID_LENGTH);

    /* Compare the loopback location Id field of the received 
     * OAM cell and the loopback location ID of this port */
    if (MEMCMP (pu1Buffer, pu1LBLId, AOAM_LLID_LENGTH) != 0)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Loopback location ID is incorrect for port : %d\r\n",
                       pF5OamLBEntry->u4IfIndex);
        AOAM_INCR_LLID_DISCARDS (pF5OamLBEntry);
        return AOAM_FAILURE;
    }

    /* Have to send the OAM cell as a response to the other end by 
     * changing the loopback indication as '1' */

    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamProcessLoopedBackCell                            */
/*                                                                           */
/* Description        : This function is used to validate the received       */
/*                      looped back OAM cell                                 */
/*                                                                           */
/* Input(s)           : pu1Buffer - OAM cell info                            */
/*                      pF5OamLBEntry - F5 OAM Loopback entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : AOAM_SUCCESS / AOAM_FAILURE                          */
/*****************************************************************************/
INT4
AoamProcessLoopedBackCell (UINT1 *pu1Buffer,
                           tF5OamLoopbackTable * pF5OamLBEntry)
{
    UINT1              *pu1SrcId = NULL;
    UINT1               au1SrcId[AOAM_SOURCE_ID_LENGTH];
    UINT1               u1SourceId = 0;

    MEMSET (au1SrcId, 0, AOAM_SOURCE_ID_LENGTH);

    pu1SrcId = au1SrcId;

    /* Move the pointer location to the source ID field of the OAM packet */
    pu1Buffer += AOAM_SRC_ID_OFFSET;

    AOAM_MAP_LOCATION_ID (pF5OamLBEntry->
                          F5OamLBPingEntry.u1SourceId, u1SourceId);

    AOAM_PUT_BYTES (pu1SrcId, u1SourceId, AOAM_SOURCE_ID_LENGTH);

    /* Compare the source Id field of the received OAM cell 
     * and the source ID of this port */
    if (MEMCMP (pu1Buffer, pu1SrcId, AOAM_SOURCE_ID_LENGTH) != 0)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Source ID is incorrect for port : %d\r\n",
                       pF5OamLBEntry->u4IfIndex);
        /* Discard the OAM cell as the source ID is anot correct */
        AOAM_INCR_SRC_ID_DISCARDS (pF5OamLBEntry);
        return AOAM_FAILURE;
    }

    pu1Buffer -= AOAM_CORR_TAG_OFFSET;

    /* Check whether the correlation tag is correct */
    if (MEMCMP (pu1Buffer,
                pF5OamLBEntry->F5OamLBPingEntry.au1CorrelationTag,
                AOAM_CORRELATION_TAG_LENGTH) != 0)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Correlation tag is incorrect for port : %d\r\n",
                       pF5OamLBEntry->u4IfIndex);
        /* Discard the OAM cell as the correlation tag is not correct */
        AOAM_INCR_CORRELA_TAG_DISCARDS (pF5OamLBEntry);
        return AOAM_FAILURE;
    }
    /* Loopback successful */
    if (pF5OamLBEntry->F5OamLBPingEntry.u1CurrPingCount ==
        pF5OamLBEntry->F5OamLBPingEntry.u1PingCount)
    {
        /* set the loopback initiating status as 'false' 
         * when loopback is successful */
        pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus = AOAM_FALSE;
        if (pF5OamLBEntry->F5OamLBPingEntry.u4CurrFailedPings == 0)
        {
            pF5OamLBEntry->F5OamLBPingEntry.u1OperStatus = AOAM_SUCCESS;
        }
    }
    else
    {
        /* If the current ping count is less than the max ping count 
         * that have to send, explicitely stop the timer and 
         * resend the OAM cell */
        if (pF5OamLBEntry->F5OamLBPingEntry.u1PingTimeOut != 0)
        {
            if (pF5OamLBEntry->F5OamTmrNode.u1Status == AOAM_ENABLE)
            {
                TmrStop (AOAM_TMR_LIST_ID (),
                         &(pF5OamLBEntry->F5OamTmrNode.TmrBlk));
                AOAM_FORCED_TRC (AOAM_FAIL_TRC, "Tmr Stop. \r\n");

            }
        }
        AoamPktTxOamCell (pF5OamLBEntry);
    }
    AOAM_INCR_SUCCESSFUL_PINGS (pF5OamLBEntry);
    return AOAM_SUCCESS;
}
