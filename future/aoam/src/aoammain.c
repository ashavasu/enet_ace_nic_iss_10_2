
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: aoammain.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : aoammain.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ATM OAM module                                 */
/*    MODULE NAME           : ATM OAM Main Module                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains init, deinit funtions       */
/*                            for ATM OAM module                             */
/*---------------------------------------------------------------------------*/

#include "aoaminc.h"
#include "aoamglob.h"

PRIVATE INT4 AoamMainInit PROTO ((VOID));

/*****************************************************************************/
/* Function Name      : AoamMain                                             */
/*                                                                           */
/* Description        : This function is the main entry point function for   */
/*                      the ATM OAM task                                     */
/*                                                                           */
/* Input(s)           : pi1Param - unused                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AoamMain (INT1 *pi1Params)
{
    tAoamQMsg          *pAoamQMsg = NULL;
    tAoamQInPkt        *pQMsg = NULL;
    UINT4               u4EventsRecvd = 0;

    UNUSED_PARAM (pi1Params);

    AOAM_TRC_FLAG () = AOAM_TRC_NONE;

    if (AoamMainInit () != AOAM_SUCCESS)
    {
        AoamMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the ATM OAM MIB */
    AoamUtilRegisterFsAoamMib ();
#endif

    /* Set the Global status of ATM OAM (F5 OAM ) operation */
    AOAM_ADMIN_STATUS () = AOAM_DISABLE;

    lrInitComplete (OSIX_SUCCESS);

    /* Wait infinitely till an event comes. When the event comes, process it
     * and wait again for further events. */
    while (AOAM_TRUE)
    {
        if (AOAM_EVENT_RECEIVE (AOAM_TASK_ID (),
                                (AOAM_CFG_MSG_EVENT | AOAM_TMR_EXP_EVENT |
                                 AOAM_PKT_ARRIVED_EVENT),
                                AOAM_EVENT_WAIT_FLAG,
                                &u4EventsRecvd) == OSIX_SUCCESS)
        {
            if (u4EventsRecvd & AOAM_CFG_MSG_EVENT)
            {
                while (AOAM_QUEUE_RECEIVE (AOAM_CFG_QUEUE_ID (),
                                           (UINT1 *) &pAoamQMsg,
                                           OSIX_DEF_MSG_LEN,
                                           AOAM_RECEIVE_Q_FLAG) == OSIX_SUCCESS)
                {
                    AOAM_LOCK ();
                    /* Process the events */
                    AoamMainProcessEvent (pAoamQMsg);
                    AOAM_UNLOCK ();
                    MemReleaseMemBlock ((UINT4) AOAM_CFG_Q_POOL_ID (),
                                        (UINT1 *) pAoamQMsg);
                }
            }
            if (u4EventsRecvd & AOAM_PKT_ARRIVED_EVENT)
            {
                while (AOAM_QUEUE_RECEIVE (AOAM_PKT_QUEUE_ID (),
                                           (UINT1 *) &pQMsg,
                                           OSIX_DEF_MSG_LEN,
                                           AOAM_RECEIVE_Q_FLAG) == OSIX_SUCCESS)
                {
                    AOAM_LOCK ();
                    /* Process the events */
                    AoamProcessLBOamCell (pQMsg->pInQMsg, pQMsg->u4IfIndex);
                    AOAM_UNLOCK ();
                    MemReleaseMemBlock ((UINT4) AOAM_PKT_Q_POOL_ID (),
                                        (UINT1 *) pAoamQMsg);
                }
            }
            if (u4EventsRecvd & AOAM_TMR_EXP_EVENT)
            {
                AOAM_LOCK ();
                /* Process the events */
                AoamMainProcessTmrEvent ();
                AOAM_UNLOCK ();
            }

        }
    }
}

/*****************************************************************************/
/* Function Name      : AoamMainInit                                         */
/*                                                                           */
/* Description        : This function creates Semaphore, Queue and MemPools  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
PRIVATE INT4
AoamMainInit (VOID)
{

    /* Create semaphore for mutual exclusion */
    if (AOAM_CREATE_SEM (AOAM_SEM_NAME, AOAM_SEM_COUNT,
                         AOAM_SEM_FLAGS, &(AOAM_SEM_ID ())) != OSIX_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Creation of ATM OAM semaphore failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Create Queue for posting configuration message */
    if (AOAM_CREATE_QUEUE (AOAM_CFG_QUEUE_NAME, AOAM_QUEUE_DEPTH, 0,
                           &(AOAM_CFG_QUEUE_ID ())) != OSIX_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Creation of ATM OAM Queue failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Create Queue for posting packet arrival message */
    if (AOAM_CREATE_QUEUE (AOAM_PKT_QUEUE_NAME, AOAM_QUEUE_DEPTH, 0,
                           &(AOAM_PKT_QUEUE_ID ())) != OSIX_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Creation of ATM OAM Queue failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Memory pool creation for configuration message queue */
    if (AOAM_CREATE_MEM_POOL (AOAM_MEMBLK_SIZE, AOAM_QUEUE_DEPTH,
                              AOAM_MEMORY_TYPE,
                              &(AOAM_CFG_Q_POOL_ID ())) != MEM_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Creation of memory pool for CFG queue failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Memory pool creation for Packet message queue */
    if (AOAM_CREATE_MEM_POOL (AOAM_MEMBLK_SIZE, AOAM_QUEUE_DEPTH,
                              AOAM_MEMORY_TYPE,
                              &(AOAM_PKT_Q_POOL_ID ())) != MEM_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Creation of memory pool for PKT queue failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Create the Memory Pool for F5 OAM Loopback entries */
    if (AOAM_CREATE_MEM_POOL (AOAM_F5OAM_LB_MEMBLK_SIZE, AOAM_MAX_NUM_OF_PVC,
                              AOAM_MEMORY_TYPE,
                              &(AOAM_F5OAM_LB_POOL_ID ())) != MEM_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Creation of memory pool for F5 OAM Loopback entries "
                  "failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Create the RBTree For F5 OAM Loopback nodes */
    AOAM_F5OAM_LB_RBTREE () =
        AOAM_CREATE_RBTREE (0, AoamUtilRBTreeF5OamLBEntryCmp);

    if (AOAM_F5OAM_LB_RBTREE () == NULL)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Creation of RBTree for F5 OAM Loopback failed. \r\n");

        return AOAM_FAILURE;
    }

    if (AOAM_GET_TASK_ID (SELF, AOAM_TASK_NAME, &(AOAM_TASK_ID ())) !=
        OSIX_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Getting task id for ATM OAM failed.\r\n");

        return AOAM_FAILURE;
    }

    /* Create the timer list for AOAM task */
    if (AOAM_CREATE_TIMER_LIST (AOAM_TASK_NAME, AOAM_TMR_EXP_EVENT, NULL,
                                &(AOAM_TIMER_LIST ())) != TMR_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC, "Creation of AOAM Timer list failed.\r\n");

        return AOAM_FAILURE;
    }

    return AOAM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AoamMainDeInit                                       */
/*                                                                           */
/* Description        : This function Deletes Semaphore, Queue and MemPools  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
AoamMainDeInit (VOID)
{

    if (AOAM_SEM_ID () != 0)
    {
        AOAM_DELETE_SEM (0, AOAM_SEM_NAME);
    }

    if (AOAM_CFG_QUEUE_ID () != 0)
    {
        AOAM_DELETE_QUEUE (0, AOAM_CFG_QUEUE_NAME);
    }

    if (AOAM_PKT_QUEUE_ID () != 0)
    {
        AOAM_DELETE_QUEUE (0, AOAM_PKT_QUEUE_NAME);
    }

    if (AOAM_CFG_Q_POOL_ID () != 0)
    {
        AOAM_DELETE_MEM_POOL (AOAM_CFG_Q_POOL_ID ());
    }

    if (AOAM_CFG_Q_POOL_ID () != 0)
    {
        AOAM_DELETE_MEM_POOL (AOAM_CFG_Q_POOL_ID ());
    }

    AOAM_DEINIT_RBTREE (AOAM_F5OAM_LB_RBTREE ());

    if (AOAM_F5OAM_LB_POOL_ID () != 0)
    {
        AOAM_DELETE_MEM_POOL (AOAM_F5OAM_LB_POOL_ID ());
    }

    /* Delete the Timerlist created for the AOAM task */
    if (AOAM_TIMER_LIST () != 0)
    {
        AOAM_DELETE_TIMER_LIST (AOAM_TIMER_LIST ());
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*      FUNCTION NAME   :   AoamMainProcessEvent                             */
/*                                                                           */
/*      DESCRIPTION     :   This function handles the event                  */
/*                                                                           */
/*      INPUT           :   pAoamQMsg - Queue Message                        */
/*                                                                           */
/*      OUTPUT          :   None                                             */
/*                                                                           */
/*      RETURNS         :   None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
AoamMainProcessEvent (tAoamQMsg * pAoamQMsg)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    INT4                i4RetVal = 0;

    switch (pAoamQMsg->u4EventType)
    {
        case AOAM_PORT_CREATE_MSG:

            if (AoamUtilGetPvcEntry (pAoamQMsg->u4IfIndex) == NULL)
            {
                i4RetVal = AoamUtilCreatePvcEntry (pAoamQMsg->u4IfIndex);

                if (i4RetVal == AOAM_FAILURE)
                {
                    AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                                   "PVC creation failed in F5 OAM  %d\r\n",
                                   pAoamQMsg->u4IfIndex);
                }
            }
            L2_SYNC_GIVE_SEM ();
            break;

        case AOAM_PORT_DELETE_MSG:

            if (AoamUtilGetPvcEntry (pAoamQMsg->u4IfIndex) != NULL)
            {
                i4RetVal = AoamUtilDeletePvcEntry (pAoamQMsg->u4IfIndex);

                if (i4RetVal == AOAM_FAILURE)
                {
                    AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                                   "PVC entry deletion failed in F5 OAM "
                                   "for port : %d\r\n", pAoamQMsg->u4IfIndex);
                }
            }
            break;

        case AOAM_PORT_OPER_UP_MSG:

            if ((pF5OamLBEntry =
                 AoamUtilGetPvcEntry (pAoamQMsg->u4IfIndex)) != NULL)
            {
                pF5OamLBEntry->u1PvcOperStatus = AOAM_IF_UP;
            }
            break;

        case AOAM_PORT_OPER_DOWN_MSG:

            if ((pF5OamLBEntry =
                 AoamUtilGetPvcEntry (pAoamQMsg->u4IfIndex)) != NULL)
            {
                pF5OamLBEntry->u1PvcOperStatus = AOAM_IF_DOWN;
            }
            break;

        default:
            return;
    }
}

/*****************************************************************************/
/*                                                                           */
/*      FUNCTION NAME   :   AoamMainProcessTmrEvent                          */
/*                                                                           */
/*      DESCRIPTION     :   This function handles the timer event            */
/*                                                                           */
/*      INPUT           :   pAoamQMsg - Queue Message                        */
/*                                                                           */
/*      OUTPUT          :   None                                             */
/*                                                                           */
/*      RETURNS         :   None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
AoamMainProcessTmrEvent ()
{
    tF5OamTmrEntry     *pExpiredTmr = NULL;
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    AOAM_TRC (AOAM_FN_ENTRY, "Entering the function \r\n");

    /* Get the expired timer from the timer list */
    while ((pExpiredTmr = (tF5OamTmrEntry *)
            TmrGetNextExpiredTimer (AOAM_TIMER_LIST ())) != NULL)
    {
        /* Get the timer ID */
        u1TimerId = ((tF5OamTmrEntry *) pExpiredTmr)->TmrBlk.u1TimerId;

        if (u1TimerId == AOAM_LOOPBACK_TIMER)
        {
            /* Get the offset and get the F5 OAM LB entry */
            i2Offset = AOAM_LOOPBACK_TMR_OFFSET;

            pF5OamLBEntry =
                (tF5OamLoopbackTable *) ((UINT1 *) pExpiredTmr - i2Offset);

            /* If the current number of ping send is less than 
             * the maximum nuber of pings have to be send for this ping, 
             * send the OAM cell again */
            if (pF5OamLBEntry->F5OamLBPingEntry.u1CurrPingCount <
                pF5OamLBEntry->F5OamLBPingEntry.u1PingCount)
            {
                /* To form and send the OAM cell */
                AoamPktTxOamCell (pF5OamLBEntry);
            }
            else
            {
                /* Set the ping initialing status as 'false', 
                 * so that next ping can be send */
                pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus = AOAM_FALSE;
            }

            /* Timer expiry means, OAM LB cell is not replied back 
             * from the other end */
            AOAM_INCR_FAILED_PINGS (pF5OamLBEntry);

        }
    }

    return;
}
