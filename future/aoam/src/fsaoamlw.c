/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsaoamlw.c,v 1.1.1.1 2008/06/04 10:50:36 premap-iss Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fsaoamlw.h"
# include  "aoaminc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsF5OAMEntryStatus
 Input       :  The Indices

                The Object 
                retValFsF5OAMEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMEntryStatus (INT4 *pi4RetValFsF5OAMEntryStatus)
{
    *pi4RetValFsF5OAMEntryStatus = AOAM_ADMIN_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMDebug
 Input       :  The Indices

                The Object 
                retValFsF5OAMDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMDebug (INT4 *pi4RetValFsF5OAMDebug)
{
    *pi4RetValFsF5OAMDebug = AOAM_TRC_FLAG ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsF5OAMEntryStatus
 Input       :  The Indices

                The Object 
                setValFsF5OAMEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMEntryStatus (INT4 i4SetValFsF5OAMEntryStatus)
{
    /* If the AOAM global entry status and 'i4SetValFsF5OAMEntryStatus'
     * are same, return success */
    if (AOAM_ADMIN_STATUS () == i4SetValFsF5OAMEntryStatus)
    {
        return SNMP_SUCCESS;
    }
    AOAM_ADMIN_STATUS () = i4SetValFsF5OAMEntryStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsF5OAMDebug
 Input       :  The Indices

                The Object 
                setValFsF5OAMDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMDebug (INT4 i4SetValFsF5OAMDebug)
{
    AOAM_TRC_FLAG () = i4SetValFsF5OAMDebug;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMEntryStatus
 Input       :  The Indices

                The Object 
                testValFsF5OAMEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMEntryStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsF5OAMEntryStatus)
{
    if ((i4TestValFsF5OAMEntryStatus != AOAM_ENABLE) &&
        (i4TestValFsF5OAMEntryStatus != AOAM_DISABLE))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = (UINT4) SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMDebug
 Input       :  The Indices

                The Object 
                testValFsF5OAMDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsF5OAMDebug)
{
    if ((i4TestValFsF5OAMDebug < AOAM_TRC_NONE) ||
        (i4TestValFsF5OAMDebug > AOAM_ALL_TRC))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsF5OAMLoopbackPingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsF5OAMLoopbackPingTable
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsF5OAMLoopbackPingTable (INT4
                                                  i4FsF5OAMLoopbackPingIfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) &&
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        return SNMP_FAILURE;
    }
    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in validation of F5 OAM Loopback ping table"
                       "for port : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsF5OAMLoopbackPingTable
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsF5OAMLoopbackPingTable (INT4 *pi4FsF5OAMLoopbackPingIfIndex)
{
    if (nmhGetNextIndexFsF5OAMLoopbackPingTable (0,
                                                 pi4FsF5OAMLoopbackPingIfIndex)
        != SNMP_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Failed in Get first of F5 OAM Loopback ping table \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsF5OAMLoopbackPingTable
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex
                nextFsF5OAMLoopbackPingIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsF5OAMLoopbackPingTable (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         INT4
                                         *pi4NextFsF5OAMLoopbackPingIfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    tF5OamLoopbackTable F5OamLBEntry;

    MEMSET (&F5OamLBEntry, 0, sizeof (F5OamLBEntry));

    F5OamLBEntry.u4IfIndex = i4FsF5OAMLoopbackPingIfIndex;

    pF5OamLBEntry = (tF5OamLoopbackTable *)
        RBTreeGetNext (AOAM_F5OAM_LB_RBTREE (),
                       (tRBElem *) & (F5OamLBEntry), NULL);

    if (pF5OamLBEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsF5OAMLoopbackPingIfIndex = pF5OamLBEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingType
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingType (INT4 i4FsF5OAMLoopbackPingIfIndex,
                               INT4 *pi4RetValFsF5OAMLoopbackPingType)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingType for port : %d"
                       "\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingType =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1PingType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingLocationID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingLocationID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingLocationID (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4
                                     *pi4RetValFsF5OAMLoopbackPingLocationID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingLocationID for port"
                       " : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingLocationID =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1LBLocationId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingCount
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingCount (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                INT4 *pi4RetValFsF5OAMLoopbackPingCount)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingCount for port : %d"
                       "\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingCount =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1PingCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingTimeout
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingTimeout (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                  INT4 *pi4RetValFsF5OAMLoopbackPingTimeout)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingTimeout for port :"
                       " %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingTimeout =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1PingTimeOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingCorrelationTag
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingCorrelationTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingCorrelationTag (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsF5OAMLoopbackPingCorrelationTag)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingType for port : %d"
                       "\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pRetValFsF5OAMLoopbackPingCorrelationTag->i4_Length =
        AOAM_CORRELATION_TAG_LENGTH;

    MEMCPY (pRetValFsF5OAMLoopbackPingCorrelationTag->pu1_OctetList,
            pF5OamLBEntry->F5OamLBPingEntry.au1CorrelationTag,
            AOAM_CORRELATION_TAG_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingSourceIDStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingSourceIDStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingSourceIDStatus (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         INT4
                                         *pi4RetValFsF5OAMLoopbackPingSourceIDStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingSourceIDStatus for "
                       "port : %d\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingSourceIDStatus =
        pF5OamLBEntry->F5OamLBPingEntry.u1SourceIdStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingSourceID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingSourceID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingSourceID (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                   INT4 *pi4RetValFsF5OAMLoopbackPingSourceID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingSourceID for port :"
                       " %d\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingSourceID =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1SourceId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingEntryStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingEntryStatus (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      INT4
                                      *pi4RetValFsF5OAMLoopbackPingEntryStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingEntryStatus for "
                       "port : %d\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingEntryStatus =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1EntryStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingOperStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingOperStatus (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4
                                     *pi4RetValFsF5OAMLoopbackPingOperStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingOperStatus for port"
                       " %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingOperStatus =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.u1OperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingTxLoopback
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingTxLoopback
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingTxLoopback (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4
                                     *pi4RetValFsF5OAMLoopbackPingTxLoopback)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingTxLoopback for port "
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pi4RetValFsF5OAMLoopbackPingTxLoopback =
        (INT4) pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingCurrTxCells
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingCurrTxCells
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingCurrTxCells (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      UINT4
                                      *pu4RetValFsF5OAMLoopbackPingCurrTxCells)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingCurrTxCells for "
                       "port %d\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLoopbackPingCurrTxCells =
        pF5OamLBEntry->F5OamLBPingEntry.u4CurrTxCells;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingCurrRxCells
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMLoopbackPingCurrRxCells
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingCurrRxCells (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      UINT4
                                      *pu4RetValFsF5OAMLoopbackPingCurrRxCells)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingCurrRxCells for "
                       "port : %d\r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLoopbackPingCurrRxCells =
        pF5OamLBEntry->F5OamLBPingEntry.u4CurrRxCells;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMNoOfCurrSuccessfulPings
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMNoOfCurrSuccessfulPings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMNoOfCurrSuccessfulPings (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      UINT4
                                      *pu4RetValFsF5OAMNoOfCurrSuccessfulPings)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMNoOfCurrSuccessfulPings for "
                       "port : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMNoOfCurrSuccessfulPings =
        pF5OamLBEntry->F5OamLBPingEntry.u4CurrSuccessfulPings;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMNoOfCurrFailedPings
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                retValFsF5OAMNoOfCurrFailedPings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMNoOfCurrFailedPings (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                  UINT4 *pu4RetValFsF5OAMNoOfCurrFailedPings)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMNoOfCurrFailedPings for port : "
                       "%d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMNoOfCurrFailedPings =
        pF5OamLBEntry->F5OamLBPingEntry.u4CurrFailedPings;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingType
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingType (INT4 i4FsF5OAMLoopbackPingIfIndex,
                               INT4 i4SetValFsF5OAMLoopbackPingType)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingType for port : "
                       "%d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.u1PingType =
        (UINT1) i4SetValFsF5OAMLoopbackPingType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingLocationID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingLocationID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingLocationID (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4 i4SetValFsF5OAMLoopbackPingLocationID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingLocationID for port"
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }

    pF5OamLBEntry->F5OamLBPingEntry.u1LBLocationId =
        (UINT1) i4SetValFsF5OAMLoopbackPingLocationID;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingCount
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingCount (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                INT4 i4SetValFsF5OAMLoopbackPingCount)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingCount for port"
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }

    pF5OamLBEntry->F5OamLBPingEntry.u1PingCount =
        (UINT1) i4SetValFsF5OAMLoopbackPingCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingTimeout
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingTimeout (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                  INT4 i4SetValFsF5OAMLoopbackPingTimeout)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingTimeout for port"
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.u1PingTimeOut =
        (UINT1) i4SetValFsF5OAMLoopbackPingTimeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingCorrelationTag
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingCorrelationTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingCorrelationTag (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValFsF5OAMLoopbackPingCorrelationTag)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingCorrelationTag for"
                       "port : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    MEMCPY (pF5OamLBEntry->F5OamLBPingEntry.au1CorrelationTag,
            pSetValFsF5OAMLoopbackPingCorrelationTag->pu1_OctetList,
            AOAM_CORRELATION_TAG_LENGTH);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingSourceIDStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingSourceIDStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingSourceIDStatus (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         INT4
                                         i4SetValFsF5OAMLoopbackPingSourceIDStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingSourceIDStatus for"
                       "port : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.u1SourceIdStatus =
        (UINT1) i4SetValFsF5OAMLoopbackPingSourceIDStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingSourceID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingSourceID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingSourceID (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                   INT4 i4SetValFsF5OAMLoopbackPingSourceID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingSourceID for port"
                       " : %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.u1SourceId =
        (UINT1) i4SetValFsF5OAMLoopbackPingSourceID;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingEntryStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                setValFsF5OAMLoopbackPingEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingEntryStatus (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      INT4
                                      i4SetValFsF5OAMLoopbackPingEntryStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingEntryStatus for port"
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.u1EntryStatus =
        (UINT1) i4SetValFsF5OAMLoopbackPingEntryStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsF5OAMLoopbackPingTxLoopback
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object
                setValFsF5OAMLoopbackPingTxLoopback
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsF5OAMLoopbackPingTxLoopback (INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4 i4SetValFsF5OAMLoopbackPingTxLoopback)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhSetFsF5OAMLoopbackPingTxLoopback for port"
                       ": %d \r\n", i4FsF5OAMLoopbackPingIfIndex);
        return SNMP_FAILURE;
    }
    pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus =
        (BOOL1) i4SetValFsF5OAMLoopbackPingTxLoopback;

    if (i4SetValFsF5OAMLoopbackPingTxLoopback == AOAM_TRUE)
    {
        AOAM_INIT_CURR_COUNTERS (pF5OamLBEntry);
        AoamPktTxOamCell (pF5OamLBEntry);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingType
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingType (UINT4 *pu4ErrorCode,
                                  INT4 i4FsF5OAMLoopbackPingIfIndex,
                                  INT4 i4TestValFsF5OAMLoopbackPingType)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingType != AOAM_END_TO_END_LOOPBACK) &&
        (i4TestValFsF5OAMLoopbackPingType != AOAM_SEGMENT_LOOPBACK) &&
        (i4TestValFsF5OAMLoopbackPingType != AOAM_LOOPBACK_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingLocationID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingLocationID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingLocationID (UINT4 *pu4ErrorCode,
                                        INT4 i4FsF5OAMLoopbackPingIfIndex,
                                        INT4
                                        i4TestValFsF5OAMLoopbackPingLocationID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingLocationID != AOAM_LLID_ALL_ZEROS) &&
        (i4TestValFsF5OAMLoopbackPingLocationID != AOAM_LLID_ALL_ONES) &&
        (i4TestValFsF5OAMLoopbackPingLocationID != AOAM_LLID_ALL_6AH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingCount
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingCount (UINT4 *pu4ErrorCode,
                                   INT4 i4FsF5OAMLoopbackPingIfIndex,
                                   INT4 i4TestValFsF5OAMLoopbackPingCount)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingCount < AOAM_MIN_PING_COUNT) ||
        (i4TestValFsF5OAMLoopbackPingCount > AOAM_MAX_PING_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingTimeout
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingTimeout (UINT4 *pu4ErrorCode,
                                     INT4 i4FsF5OAMLoopbackPingIfIndex,
                                     INT4 i4TestValFsF5OAMLoopbackPingTimeout)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingTimeout < AOAM_MIN_PING_TIMEOUT) ||
        (i4TestValFsF5OAMLoopbackPingTimeout > AOAM_MAX_PING_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingCorrelationTag
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingCorrelationTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
l
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingCorrelationTag (UINT4 *pu4ErrorCode,
                                            INT4 i4FsF5OAMLoopbackPingIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFsF5OAMLoopbackPingCorrelationTag)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pTestValFsF5OAMLoopbackPingCorrelationTag->i4_Length <
        AOAM_CORRELATION_TAG_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingSourceIDStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingSourceIDStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingSourceIDStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsF5OAMLoopbackPingIfIndex,
                                            INT4
                                            i4TestValFsF5OAMLoopbackPingSourceIDStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingSourceIDStatus != AOAM_ENABLE) &&
        (i4TestValFsF5OAMLoopbackPingSourceIDStatus != AOAM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingSourceID
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingSourceID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingSourceID (UINT4 *pu4ErrorCode,
                                      INT4 i4FsF5OAMLoopbackPingIfIndex,
                                      INT4 i4TestValFsF5OAMLoopbackPingSourceID)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingSourceID != AOAM_LLID_ALL_ZEROS) &&
        (i4TestValFsF5OAMLoopbackPingSourceID != AOAM_LLID_ALL_ONES) &&
        (i4TestValFsF5OAMLoopbackPingSourceID != AOAM_LLID_ALL_6AH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingEntryStatus
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object 
                testValFsF5OAMLoopbackPingEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingEntryStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsF5OAMLoopbackPingIfIndex,
                                         INT4
                                         i4TestValFsF5OAMLoopbackPingEntryStatus)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingEntryStatus != AOAM_ENABLE) &&
        (i4TestValFsF5OAMLoopbackPingEntryStatus != AOAM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsF5OAMLoopbackPingTxLoopback
 Input       :  The Indices
                FsF5OAMLoopbackPingIfIndex

                The Object
                testValFsF5OAMLoopbackPingTxLoopback
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsF5OAMLoopbackPingTxLoopback (UINT4 *pu4ErrorCode,
                                        INT4 i4FsF5OAMLoopbackPingIfIndex,
                                        INT4
                                        i4TestValFsF5OAMLoopbackPingTxLoopback)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackPingIfIndex < AOAM_MIN_PORT) ||
        (i4FsF5OAMLoopbackPingIfIndex > AOAM_MAX_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsF5OAMLoopbackPingTxLoopback != AOAM_TRUE) &&
        (i4TestValFsF5OAMLoopbackPingTxLoopback != AOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackPingIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_AOAM_NO_LB_PING_ENTRY);
        return SNMP_FAILURE;
    }
    if ((pF5OamLBEntry->F5OamLBPingEntry.b1TxLoopbackStatus == AOAM_TRUE) &&
        (i4TestValFsF5OAMLoopbackPingTxLoopback == AOAM_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AOAM_PING_IN_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsF5OAMLoopbackStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsF5OAMLoopbackStatsTable
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsF5OAMLoopbackStatsTable (INT4
                                                   i4FsF5OAMLoopbackStatsIfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    if ((i4FsF5OAMLoopbackStatsIfIndex < AOAM_MIN_PORT) &&
        (i4FsF5OAMLoopbackStatsIfIndex > AOAM_MAX_PORT))
    {
        return SNMP_FAILURE;
    }
    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in validaion of F5 OAM loopback stats table for "
                       "port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsF5OAMLoopbackStatsTable
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsF5OAMLoopbackStatsTable (INT4 *pi4FsF5OAMLoopbackStatsIfIndex)
{
    if (nmhGetNextIndexFsF5OAMLoopbackStatsTable (0,
                                                  pi4FsF5OAMLoopbackStatsIfIndex)
        != SNMP_SUCCESS)
    {
        AOAM_TRC (AOAM_FAIL_TRC,
                  "Failed in Get first of F5 OAM Loopback stats table \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsF5OAMLoopbackStatsTable
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex
                nextFsF5OAMLoopbackStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsF5OAMLoopbackStatsTable (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                          INT4
                                          *pi4NextFsF5OAMLoopbackStatsIfIndex)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;
    tF5OamLoopbackTable F5OamLBEntry;

    MEMSET (&F5OamLBEntry, 0, sizeof (F5OamLBEntry));

    F5OamLBEntry.u4IfIndex = i4FsF5OAMLoopbackStatsIfIndex;

    pF5OamLBEntry = (tF5OamLoopbackTable *)
        RBTreeGetNext (AOAM_F5OAM_LB_RBTREE (),
                       (tRBElem *) & (F5OamLBEntry), NULL);

    if (pF5OamLBEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsF5OAMLoopbackStatsIfIndex = pF5OamLBEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsF5OAMLLIDIncorrects
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMLLIDIncorrects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLLIDIncorrects (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                             UINT4 *pu4RetValFsF5OAMLLIDIncorrects)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLLIDIncorrects "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLLIDIncorrects =
        pF5OamLBEntry->F5OamLBStatsEntry.u4LLIDInCorrects;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMeTOeLoopbackIncorrects
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMeTOeLoopbackIncorrects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMeTOeLoopbackIncorrects (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                     UINT4
                                     *pu4RetValFsF5OAMeTOeLoopbackIncorrects)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMeTOeLoopbackIncorrects "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMeTOeLoopbackIncorrects =
        pF5OamLBEntry->F5OamLBStatsEntry.u4EtoELBIncorrects;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLBIndicationIncorrects
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMLBIndicationIncorrects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLBIndicationIncorrects (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                     UINT4
                                     *pu4RetValFsF5OAMLBIndicationIncorrects)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLBIndicationIncorrects "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLBIndicationIncorrects =
        pF5OamLBEntry->F5OamLBStatsEntry.u4LBIndicationIncorrects;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMSourceIDIncorrects
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMSourceIDIncorrects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMSourceIDIncorrects (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                 UINT4 *pu4RetValFsF5OAMSourceIDIncorrects)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMSourceIDIncorrects "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMSourceIDIncorrects =
        pF5OamLBEntry->F5OamLBStatsEntry.u4SourceIDIncorrects;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMCorrelationTagIncorrects
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMCorrelationTagIncorrects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMCorrelationTagIncorrects (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                       UINT4
                                       *pu4RetValFsF5OAMCorrelationTagIncorrects)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMCorrelationTagIncorrects "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMCorrelationTagIncorrects =
        pF5OamLBEntry->F5OamLBStatsEntry.u4CorrelationTagIncorrects;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingTxCells
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMLoopbackPingTxCells
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingTxCells (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                  UINT4 *pu4RetValFsF5OAMLoopbackPingTxCells)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingTxCells "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLoopbackPingTxCells =
        pF5OamLBEntry->F5OamLBStatsEntry.u4LBPingTxCells;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMLoopbackPingRxCells
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMLoopbackPingRxCells
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMLoopbackPingRxCells (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                  UINT4 *pu4RetValFsF5OAMLoopbackPingRxCells)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMLoopbackPingRxCells "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMLoopbackPingRxCells =
        pF5OamLBEntry->F5OamLBStatsEntry.u4LBPingRxCells;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMNoOfSuccessfulPings
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMNoOfSuccessfulPings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMNoOfSuccessfulPings (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                                  UINT4 *pu4RetValFsF5OAMNoOfSuccessfulPings)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMNoOfSuccessfulPings "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMNoOfSuccessfulPings =
        pF5OamLBEntry->F5OamLBStatsEntry.u4SuccessfulPings;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsF5OAMNoOfFailedPings
 Input       :  The Indices
                FsF5OAMLoopbackStatsIfIndex

                The Object 
                retValFsF5OAMNoOfFailedPings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsF5OAMNoOfFailedPings (INT4 i4FsF5OAMLoopbackStatsIfIndex,
                              UINT4 *pu4RetValFsF5OAMNoOfFailedPings)
{
    tF5OamLoopbackTable *pF5OamLBEntry = NULL;

    pF5OamLBEntry = AoamUtilGetPvcEntry (i4FsF5OAMLoopbackStatsIfIndex);

    if (pF5OamLBEntry == NULL)
    {
        AOAM_TRC_ARG1 (AOAM_FAIL_TRC,
                       "Failed in nmhGetFsF5OAMNoOfFailedPings "
                       "for port %d\r\n", i4FsF5OAMLoopbackStatsIfIndex);
        return SNMP_FAILURE;
    }
    *pu4RetValFsF5OAMNoOfFailedPings =
        pF5OamLBEntry->F5OamLBStatsEntry.u4FailedPings;
    return SNMP_SUCCESS;
}
