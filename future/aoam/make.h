#
# Copyright (C) 2008 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 27 May 2008                                   |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+

#include the LR make.h and make.rule
include ../LR/make.h
include ../LR/make.rule

#Project base directories
PROJECT_NAME			= FutureAOAM
PROJECT_BASE_DIR		= ${BASE_DIR}/aoam
PROJECT_SOURCE_DIR	= $(PROJECT_BASE_DIR)/src
PROJECT_INCLUDE_DIR	= $(PROJECT_BASE_DIR)/inc
PROJECT_OBJECT_DIR	= $(PROJECT_BASE_DIR)/obj

COMN_INC_DIR            = ${BASE_DIR}/inc

#Project compilation switches
PROJECT_COMPILATION_SWITCHES	=

PROJECT_INCLUDE_FILES = $(PROJECT_INCLUDE_DIR)/aoamdefn.h \
                        $(PROJECT_INCLUDE_DIR)/aoaminc.h \
                        $(PROJECT_INCLUDE_DIR)/aoamglob.h \
                        $(PROJECT_INCLUDE_DIR)/aoamextn.h \
                        $(PROJECT_INCLUDE_DIR)/aoammacs.h \
                        $(PROJECT_INCLUDE_DIR)/aoamtdfs.h \
                        $(PROJECT_INCLUDE_DIR)/fsaoamlw.h \
                        $(PROJECT_INCLUDE_DIR)/aoamprot.h


PROJECT_FINAL_INCLUDES_DIRS     = -I$(PROJECT_INCLUDE_DIR) \
                                        $(COMMON_INCLUDE_DIRS)

#Project final include files
PROJECT_FINAL_INCLUDE_FILES   = $(PROJECT_INCLUDE_FILES)

#dependencies
PROJECT_DEPENDENCIES        =  	$(COMMON_DEPENDENCIES) \
											$(PROJECT_FINAL_INCLUDE_FILES) \
											$(PROJECT_BASE_DIR)/Makefile \
											$(PROJECT_BASE_DIR)/make.h
