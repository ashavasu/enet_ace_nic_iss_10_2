/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: webnm.h,v 1.34 2017/12/11 10:02:04 siva Exp $
 *
 * Description: macros, typdefs and proto type for webnm 
 *******************************************************************/

#ifndef _WEBNM_H
#define _WEBNM_H

#ifdef RADIUS_WANTED
#include "radius.h"
#endif
#include "fswebnm.h"

INT1 WEBNM_NO_SUCH_OBJ_STR[] = "No_Such_Object";
INT1 WEBNM_NO_SUCH_INS_STR[] = "No_Such_Instance";
UINT1  WEBNM_ERROR_START_STR [ ] =  "\r\n<HTML>\n<HEAD> <link rel=\"stylesheet\" type=\"text/css\" HREF=\"/style.css\"><font color=red size=+2>\n <br><br><hr>\n<center>";
UINT1  WEBNM_ERROR_END_STR [] =  "</center></font><br><hr>\n </html>\n"; 
INT1  WEBNM_NO_ROW [] = "Row is not selected/Index is not available";

UINT1 WEBNM_LOGIN_EXPIRY[ ] = "\n <HTML><BODY><br><br><br><br><link rel=\"stylesheet\" type=\"text/css\" HREF=\"/style.css\"> <center><font color=red size = +2><STRONG><BLINK>Page Time Expired or Restarted Try From Login Page </BLINK></STRONG></font></center>\n </BLINK></STRONG></font></center>\n click <a href =\" /wmi/login\" target=\"_top\">here</a>to login </BODY></HTML>\n";

UINT1 WEBNM_LOGIN_PAGE[ ] = "\n <HTML><BODY OnLoad=\"document.nameform.Login.focus();\"> <link rel=\"stylesheet\" type=\"text/css\" HREF=\"/style.css\"> <H2 ALIGN=center>Login</H2><BR> <FORM name=\"nameform\" method=\"POST\" ACTION=\"/wmi/page/homepage.html\">\n <CENTER><HR>\n<P>Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <INPUT type=\"Text\" name=\"Login\"></P>\n <P>Password&nbsp;&nbsp;&nbsp;<INPUT type=\"Password\" name=\"Password\"></P><HR><INPUT TYPE=\"Submit\" NAME=\"Send\" VALUE=\"Submit\">\n </CENTER></FORM>\n </BODY></HTML>"; 

UINT1 WEBNM_LOGIN_ERROR[]="<font color=red><center><STRONG> <BLINK>Error In Authentication</BLINK></STRONG> </center></font>\n";

UINT1 WEBNM_HOMEPAGE[] = "<html> <head> <script> function loadHomepage() { document.forms[0].submit(); } </script> </head> <body onload=\"loadHomepage()\"> <form method=\"get\" action=\"/iss/specific/homepage.html\"> <INPUT type=\"Hidden\" name=\"Gambit\" value=\"GAMBIT\">  </form> </body> </html> ";

VOID WebnmModifyPage(tHttp *);
#define    WEBNM_LOCAL_OFFSET  10
#define    WEBNM_TABLE_HTML    "table.html"
#define    WEBNM_PAGE_HTML     "page.html\0"

#define WEBNM_SET_FUNC  1
#define WEBNM_TEST_FUNC 2

#define MAX_SNMP_ERROR      20   
INT1                WebnmSnmpError[19][60] = {
 /* 0  */ "",
 /* 1  */ "SNMP Error Too Big",
 /* 2  */ "SNMP Error No Such Name",
 /* 3  */ "SNMP Error Bad Value",
 /* 4  */ "SNMP Error Read Only",
 /* 5  */ "SNMP General Error",
 /* 6  */ "SNMP Error No Access",
 /* 7  */ "SNMP Error Wrong Type",
 /* 8  */ "SNMP Error Wrong Length",
 /* 9  */ "SNMP Error Wrong Encoding",
 /* 10 */ "SNMP Error Wrong Value",
 /* 11 */ "SNMP Error No Creation",
 /* 12 */ "SNMP Error Inconsistent Value",
 /* 13 */ "SNMP Error Resource Unavailable",
 /* 14 */ "SNMP Error Commit Failed",
 /* 15 */ "SNMP Error Undo Failed",
 /* 16 */ "SNMP Error Authorization Error",
 /* 17 */ "SNMP Error Not Writable",
 /* 18 */ "SNMP Error Inconsistent Name"
      }; 
/* Sizing Macros */
#define WEBNM_MAX_OCTET_STRING    60
#define WENM_MAX_OCTET_STRING_LEN 256
#define WEBNM_MAX_OID_TYPE        60
#define WEBNM_MAX_OID_LEN         256
#define WEBNM_MAX_STRING_LEN      300

#define WEBNM_ALLOC_MULTIDATA(x) if((x=WebnmAllocMultiData()) == NULL){return;}
#define  WEBNM_ALLOC_OID(x) if((x=WebnmAllocOid()) == NULL){return;}
#define  WEBNM_ALLOC_OID_RETURN(x) if((x=WebnmAllocOid()) == NULL)\
{return ENM_FAILURE;}

#ifdef CLI_WANTED
#define  WEBNM_AUTHENTICATE_USER CliCheckUserPasswd
#else
#define  WEBNM_AUTHENTICATE_USER WebnmCheckUserPasswd
#endif

#ifdef RADIUS_WANTED
#define  WEBNM_RADIUS_AUTHENTICATE_USER WebnmRadiusAuthenticate
#endif

#ifdef TACACS_WANTED
#define  WEBNM_TACACS_AUTHENTICATE_USER WebnmTacacsAuthenticate
#endif

#ifdef WEBNM_TEST_WANTED
extern INT1 WebnmDeleteTestSessionEntry PROTO((UINT4 ));
extern INT1 WebnmCreateTestUserNode (INT4, tOsixTaskId, UINT4);
#define WEBNM_TEST_DELETE_SESSION_ENTRY(x) WebnmDeleteTestSessionEntry(x)
#define WEBNM_TEST_CREATE_TEST_NODE(x,y,z) WebnmCreateTestUserNode(x,y,z)
#else
#define WEBNM_TEST_DELETE_SESSION_ENTRY(x) ((void)0)
#define WEBNM_TEST_CREATE_TEST_NODE(x,y,z) ((void)0)
#endif /* WEBNM_TEST_WANTED */

#define WEBNM_GET_TIME() OsixGetSysUpTime() 

#define WEBNM_ACCEPT_EVENT   1
#define WEBNM_REJECT_EVENT   2
#define WEBNM_BANNER_LINE_BREAK   "<br/>"
#define WEBNM_MAX_BANNER_LEN   512
tSNMP_MULTI_DATA_TYPE  gMultiData[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WENM_MAX_MULTI_DATA];
UINT4 gu4MultiDataCount[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
tSNMP_OCTET_STRING_TYPE gOctetString[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_OCTET_STRING];
UINT1 gau1OctetString[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_OCTET_STRING][WENM_MAX_OCTET_STRING_LEN];
UINT4 gu4OctetStrCount[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
tSNMP_OID_TYPE gOidType[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_OID_TYPE];
UINT4 gau4OidType[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_OID_TYPE][WEBNM_MAX_OID_LEN];
UINT4 gu4OidCount[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
extern tHttpInfo gHttpInfo;
extern tHttpServerNonceInfo gaHttpServerNonceInfo[ISS_MAX_WEB_SESSIONS];
extern UINT1 gaHTTP_REALM_ISS[];
extern UINT1 gaHTTP_REALM_TIMEOUT[];
extern UINT1 gaHTTP_REALM_WRONG_LOGIN[];
extern UINT1 gaHTTP_TRUE[];

#define WEBNM_MAX_STR_LEN 256
#define WEBNM_MULTISIZE WEBNM_MAX_STR_LEN + 44
#define WEBNM_MAX_INDICES   SNMP_MAX_INDICES 

tSnmpIndex WebnmIndexPool1[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE WebnmMultiPool1[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE WebnmOctetPool1[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_OID_TYPE  WebnmOIDPool1[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
UINT1 au1WebnmData1[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES][WEBNM_MEMBLK_SIZE];

tSnmpIndex WebnmIndexPool2[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE WebnmMultiPool2[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE WebnmOctetPool2[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
tSNMP_OID_TYPE  WebnmOIDPool2[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES];
UINT1 au1WebnmData2[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][WEBNM_MAX_INDICES][WEBNM_MEMBLK_SIZE];

UINT1 TABLE_FLAG[] ="<! TABLE INSERT>";
UINT1 TABLE_DATA_START_FLAG[]= "<td>";
UINT1 TABLE_DATA_END_FLAG[]= "</td>";
UINT1 TABLE_ROW_END_FLAG[]= "</tr>";
char TABLE_FIRST_ROW_START_FLAG[]= "<TR><TD><INPUT TYPE=\"radio\" \
                CHECKED NAME=\"TABLE_INDEX\" VALUE=\"%s\"></TD>\n";
char TABLE_ROW_START_FLAG[]= "<TR><TD><INPUT TYPE=\"radio\" \
                              NAME=\"TABLE_INDEX\" VALUE=\"%s\"></TD>\n";
char HTML_KEY_STRING[]= "_KEY";
UINT1 WEBNM_TABLE_KEY[]= "TABLEKEY";
UINT1 WEBNM_PARENT_PAGE[]= "ParentPage";
UINT1 WEBNM_TABLE_INDEX[]= "TABLE_INDEX";
char WEBNM_MODIFY[]= "Modify";
char WEBNM_CREATE[]= "Create";
char WEBNM_SET[]= "Set";
UINT1 WEBNM_ACTION[]="ACTION";
char WEBNM_ERROR_STR[] = "Error";
char WEBNM_DOT_STRING[]=".";

UINT1 au1WebnmGambit[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][ENM_MAX_NAME_LEN];

/* Index/Instance Related Proto Types */
VOID WebnmFormInstance PROTO((tHttp *,UINT1 *));


/* Redirect & Relogin pages */
UINT1 gaWEBNM_REDIRECT_PAGE[]="/iss/redirect.html";
UINT1 gaWEBNM_RELOGIN_PAGE[]="/wmi/relogin";

UINT1 WEBNM_SESSION_ERROR[] = "Maximum number of sessions reached...\n Try After Some Time";
UINT1    WEBNM_LOGIN[]="Login";
UINT1    WEBNM_PASSWD[]="Password";
UINT1    WEBNM_GAMBIT[]="Gambit";
UINT1    GAMBIT[] ="GAMBIT";
UINT1 au1WebnmLogin[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][ENM_MAX_NAME_LEN];
UINT1 au1WebnmPasswd[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][ENM_MAX_NAME_LEN];
UINT1 u4SysupTime;
UINT4 au4LastSentTime[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];

INT4 WebnmVerifyLogin PROTO((UINT1 *,UINT1 *));
VOID WebnmEncode PROTO((UINT1 *));
VOID WebnmDecode PROTO((UINT1 *));

#ifdef ISS_WANTED
INT4 IssVerifyLogin (UINT1*, UINT1*, tHttp*, UINT4, UINT1, UINT4*, UINT1);
INT1 IssAuthenticate (tHttp*, UINT1);
VOID IssLogout(tHttp*);
VOID IssSendLoginPage(tHttp*, INT1);
#endif
VOID WebnmSendErrorPage (tHttp * );
/* To send session time out error */
VOID WebnmSendLoginErrorPage (tHttp *, INT1 );

struct
{
    const char         *pi1Name;
    UINT4               u4Access;
}
AuthArray[] =
{
   {"root", 0x08},
   {"iss", 0x04},
   {"guest", 0x01},
};

struct
{
    const char         *pi1Name;
    UINT4               u4GroupId;
}
GroupArray[] =
{
   {"read", 0x01},
   {"write", 0x02},
   {"admin", 0x04},
   {"root", 0x08},
   {0,0},
};

#define WEBNM_SLL_ADD TMO_SLL_Add
#define WEBNM_SLL_DEL TMO_SLL_Delete
#define WEBNM_SLL_INIT TMO_SLL_Init
#define WEBNM_SLL_SCAN TMO_SLL_Scan
#define tWEBNM_SLL  tTMO_SLL
#define tWEBNM_SLL_NODE tTMO_SLL_NODE

typedef struct {
 tWEBNM_SLL UserList;
}tWebnmUserRecord;

tWebnmUserRecord WebnmUserRecord;
INT1 WebnmCheckUserPasswd PROTO((INT1 *, INT1 *));
INT1 WebNMAddUserEntry PROTO(( tWebnmUserNode * ));
INT1 WebNMDeleteUserEntry PROTO((UINT4 ));
tWebnmUserNode * WebNMCheckUserEntry PROTO((UINT4 ));
VOID WebNMCleanEntries PROTO((UINT4 ));

INT1
IssAuthenticateRedirectFile PROTO ((tHttp * , tMacAddr ));
#ifdef RADIUS_WANTED
VOID WebnmFreeRadiusMem (tRadInterface * );
INT1 WebnmRadiusAuthenticate PROTO ((INT1 * , INT1 * , tOsixTaskId ,INT1 *, tMacAddr, BOOL1));
INT1 WebnmWebAuthRadiusAuthentication (INT1 *pi1Name, INT1 *pi1Passwd, tMacAddr au1StaMacAddr);

#endif
#ifdef TACACS_WANTED
INT4 WebnmHandleTacacsResponse (VOID *);
INT1 WebnmTacacsAuthenticate (INT1 * , INT1 * , tOsixTaskId ,INT1 *);
#endif
#endif /* _WEBNM_H */
