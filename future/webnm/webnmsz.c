/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: webnmsz.c,v 1.3 2013/11/29 11:04:16 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _WEBNMSZ_C
#include "enm.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
WebnmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WEBNM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsWEBNMSizingParams[i4SizingId].u4StructSize,
                              FsWEBNMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WEBNMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WebnmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
WebnmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWEBNMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, WEBNMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
WebnmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WEBNM_MAX_SIZING_ID; i4SizingId++)
    {
        if (WEBNMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WEBNMMemPoolIds[i4SizingId]);
            WEBNMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
