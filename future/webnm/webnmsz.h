/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: webnmsz.h,v 1.4 2012/04/25 12:11:24 siva Exp $
 **
 ** Description: macros, typdefs and proto type for webnm
 ********************************************************************/
enum {
    MAX_ENM_BLOCKS_SIZING_ID,
    MAX_HTTP_CLT_INFO_BLOCKS_SIZING_ID,
    MAX_HTTP_DUP_MSG_NODES_SIZING_ID,
    MAX_HTTP_MSG_NODES_SIZING_ID,
    MAX_HTTP_PROCCESSING_TASKS_SIZING_ID,
    MAX_HTTP_Q_DEPTH_SIZING_ID,
    MAX_HTTP_REDIRECTION_ENTRY_BLOCKS_SIZING_ID,
    MAX_HTTP_WEBNM_RESP_BLOCKS_SIZING_ID,
    MAX_HTTP_WEBNM_USER_NODES_SIZING_ID,
    MAX_WEBNM_TACACS_BLOCKS_SIZING_ID,
    WEBNM_MAX_SIZING_ID
};


#ifdef  _WEBNMSZ_C
tMemPoolId WEBNMMemPoolIds[ WEBNM_MAX_SIZING_ID];
INT4  WebnmSizingMemCreateMemPools(VOID);
VOID  WebnmSizingMemDeleteMemPools(VOID);
INT4  WebnmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WEBNMSZ_C  */
extern tMemPoolId WEBNMMemPoolIds[ ];
extern INT4  WebnmSizingMemCreateMemPools(VOID);
extern VOID  WebnmSizingMemDeleteMemPools(VOID);
extern INT4  WebnmSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _WEBNMSZ_C  */


#ifdef  _WEBNMSZ_C
tFsModSizingParams FsWEBNMSizingParams [] = {
{ "UINT1[ENM_MAX_NAME_LEN]", "MAX_ENM_BLOCKS", sizeof(UINT1[ENM_MAX_NAME_LEN]),MAX_ENM_BLOCKS, MAX_ENM_BLOCKS,0 },
{ "tHttpClientInfo", "MAX_HTTP_CLT_INFO_BLOCKS", sizeof(tHttpClientInfo),MAX_HTTP_CLT_INFO_BLOCKS, MAX_HTTP_CLT_INFO_BLOCKS,0 },
{ "tHttp", "MAX_HTTP_DUP_MSG_NODES", sizeof(tHttp),MAX_HTTP_DUP_MSG_NODES, MAX_HTTP_DUP_MSG_NODES,0 },
{ "tHttpMsgNode", "MAX_HTTP_MSG_NODES", sizeof(tHttpMsgNode),MAX_HTTP_MSG_NODES, MAX_HTTP_MSG_NODES,0 },
{ "tHttp", "MAX_HTTP_PROCCESSING_TASKS", sizeof(tHttp),MAX_HTTP_PROCCESSING_TASKS, MAX_HTTP_PROCCESSING_TASKS,0 },
{ "tHttpSockId", "MAX_HTTP_Q_DEPTH", sizeof(tHttpSockId),MAX_HTTP_Q_DEPTH, MAX_HTTP_Q_DEPTH,0 },
{ "tHttpRedirectionBlock", "MAX_HTTP_REDIRECTION_ENTRY_BLOCKS", sizeof(tHttpRedirectionBlock),MAX_HTTP_REDIRECTION_ENTRY_BLOCKS, MAX_HTTP_REDIRECTION_ENTRY_BLOCKS,0 },
{ "tWebnmResponseBlock", "MAX_HTTP_WEBNM_RESP_BLOCKS", sizeof(tWebnmResponseBlock),MAX_HTTP_WEBNM_RESP_BLOCKS, MAX_HTTP_WEBNM_RESP_BLOCKS,0 },
{ "tWebnmUserNode", "MAX_HTTP_WEBNM_USER_NODES", sizeof(tWebnmUserNode),MAX_HTTP_WEBNM_USER_NODES, MAX_HTTP_WEBNM_USER_NODES,0 },
{ "tTacAuthenInput", "MAX_WEBNM_TACACS_BLOCKS", sizeof(tTacAuthenInput),MAX_WEBNM_TACACS_BLOCKS, MAX_WEBNM_TACACS_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WEBNMSZ_C  */
extern tFsModSizingParams FsWEBNMSizingParams [];
#endif /*  _WEBNMSZ_C  */


