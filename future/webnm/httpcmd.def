/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: httpcmd.def,v 1.7 2013/08/21 14:03:04 siva Exp $
*
* Description: Http CLI command file.
*******************************************************************/

DEFINE GROUP : HTTP_GBL_CMDS
#ifdef WEBNM_WANTED

COMMAND : set http authentication-scheme {default | basic | digest}
ACTION  : 
     {
       INT4 i4AuthVal;
       if( $3 != NULL)
       {
         i4AuthVal = CLI_HTTP_DEFAULT;
         cli_process_http_cmd (CliHandle, HTTP_CLI_SET_AUTH_SCHEME,
                               NULL ,&i4AuthVal);
       }
       else if( $4 != NULL)
       {
         i4AuthVal = CLI_HTTP_BASIC;
         cli_process_http_cmd (CliHandle, HTTP_CLI_SET_AUTH_SCHEME,
                               NULL ,&i4AuthVal);
       }
       else if( $5 != NULL)
       {
         i4AuthVal = CLI_HTTP_DIGEST;
         cli_process_http_cmd (CliHandle, HTTP_CLI_SET_AUTH_SCHEME,
                               NULL ,&i4AuthVal);
       }
     }
SYNTAX  : set http authentication-scheme {default | basic | digest}
PRVID   : 15
HELP    : Setting the Authentication scheme attribute of HTTP.
CXT_HELP: set Configures the http related parameters|
          http Http related configuration|
          authentication-scheme configuration of the http authentication scheme|
          default the legacy authentication scheme is used|
          basic the http basic authentication scheme is used|
          digest the http digest authentication scheme is used|
          <CR> Setting the configurable HTTP Authentication scheme value to default or basic or digest.

COMMAND : set http redirection enable
ACTION  : cli_process_http_cmd(CliHandle, HTTP_REDIRECT_ENABLE, NULL);
SYNTAX  : set http redirection enable
PRVID   : 15
HELP    : Enabling HTTP Redirection feature.
CXT_HELP: set Configures the http related parameters|
          http Http related configuration|
          redirection Redirection related configuration|
          enable Enables the feature|
          <CR> Enabling HTTP Redirection feature.

COMMAND : no http redirection enable
ACTION  : cli_process_http_cmd(CliHandle, HTTP_REDIRECT_DISABLE, NULL);
SYNTAX  : no http redirection enable
PRVID   : 15
HELP    : Disabling HTTP Redirection feature.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          http Http related configuration|
          redirection Redirection related parameters|
          enable enables the feature|
          <CR> Disabling HTTP Redirection feature.

COMMAND : http redirect <random_str> server {<ucast_addr> | <ip6_addr> |<random_str>}
ACTION  : 
        {
          if ($4 != NULL)
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_REDIRECT_V4_SERVERIP, NULL, $2, $4);
          }
          else if ($5 != NULL)
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_REDIRECT_V6_SERVERIP, NULL, $2, $5);
          }
          else if ($6 != NULL)
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_REDIRECT_DOMAINNAME, NULL, $2, $6);
          }
        }
SYNTAX  : http redirect <URL to be redirected> server {IPv4 Address |IPv6 Address | Domain name}
PRVID   : 15
HELP    : sets the alternate server for the URL specified, alternate servers IP or Domain name can be specified.
          so on receiving request for the URL, a redirection status is sent as response for the request. 
CXT_HELP: http Http related configuration|
          redirect Configures redirect related parameters|
          <URL to be redirected> domain name|
          server Configures server related parameters|
          <IPv4 Address> ip address in v4 format|
          <IPv6 Address> ip address in v6 format|
          <Domain name> Domain name|
          <CR> sets the alternate server for the URL specified, alternate servers IP or Domain name can be specified.
          
COMMAND : no http redirect [<random_str>]
ACTION  : 
        {
          if ($3 != NULL)
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_NO_REDIRECT_ENTRY, NULL, $3);
          }
          else 
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_NO_REDIRECT_ALL, NULL);
          }
        }
SYNTAX  : no http redirect [<URL to be redirected>]
PRVID   : 15
HELP    : For removing the redirection entry added for the URL, to the 
          server specified.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          http Http related parameters|
          redirect Redirection related parameters|
          <URL to be redirected> Domain name|
          <CR> For removing the redirection entry added for the URL, to the server specified.
          
COMMAND : clear http server statistics
ACTION  :
        {
            cli_process_http_cmd(CliHandle, HTTP_CLI_CLEAR_SRV_STATS, NULL);
        }
SYNTAX  : clear http server statistics
PRVID   : 15
HELP    : For clearing the http request counter
CXT_HELP: clear Performs clear operation |
          http HTTP related configuration |
          server Server related configuration |
          statistics Statistics related information |
          <CR> Clears http requests received and discarded statistics

#endif

#ifdef WEBNM_TEST_WANTED

COMMAND : execute webnm ut { all | [case <integer> in] file {http | webnm | httpcli | fshttplw | file5 | file6 | file7 | file8 | file9}} 
ACTION  :
    {
        UINT4 u4FileNumber = 0;

        if( $8 != NULL )
        {
            u4FileNumber = 1;
        }
        if( $9 != NULL )
        {
            u4FileNumber = 2;
        }
        if( $10 != NULL )
        {
            u4FileNumber = 3;
        }
        if( $11 != NULL )
        {
            u4FileNumber = 4;
        }
        if( $12 != NULL )
        {
            u4FileNumber = 5;
        }
        if( $13 != NULL )
        {
            u4FileNumber = 6;
        }
        if( $14 != NULL )
        {
            u4FileNumber = 7;
        }
        if( $15 != NULL )
        {
            u4FileNumber = 8;
        }
        if( $16 != NULL )
        {
            u4FileNumber = 9;
        }
        cli_process_webnm_test_cmd (CliHandle, CLI_WEBNM_UT_TEST, NULL, 
                                  $3, &u4FileNumber, $5);
    }
SYNTAX  : execute webnm ut { all | [case <case-id> in] file {http | webnm | httpcli | fshttplw | file5 | file6 | file7 | file8 | file9}}
PRVID   : 15
HELP    : WEBNM Unit Testing.

#endif

END GROUP

DEFINE GROUP : HTTP_SHOW_CMDS

#ifdef WEBNM_WANTED

COMMAND : show http authentication-scheme
ACTION  : cli_process_http_cmd(CliHandle, HTTP_CLI_SHOW_AUTH_SCHEME, NULL);
SYNTAX  : show http authentication-scheme
PRVID   : 1
HELP    : Displays the Operational and Configurable HTTP authentication scheme values.
CXT_HELP: show Displays the configuration/statistics/general information|
          http Http related configuration|
          authentication-scheme HTTP authentication scheme|
          <CR> Displays the Operational and Configurable authentication scheme values.

COMMAND : show http redirection [<random_str>]
ACTION  :
        {
          if (($3 != NULL))
          {
            cli_process_http_cmd(CliHandle, HTTP_CLI_SHOW_HTTP_REDIRECT_URL, NULL, $3);
          }
          else 
          {
             cli_process_http_cmd(CliHandle, HTTP_CLI_SHOW_HTTP_REDIRECT_ALL, NULL);
          }
        } 
SYNTAX  : show http redirection [URL]
PRVID   : 1
HELP    : Displays the redirection entries filtered by URL or all.
CXT_HELP: show Displays the configuration/statistics/general information|
          http Http related configuration|
          redirection Redirection related parameters|
          URL Domain name|
          <CR> Displays the redirection entries filtered by URL or all.
#endif
END GROUP
