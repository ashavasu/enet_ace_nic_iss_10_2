/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: webnm.c,v 1.96 2018/01/25 10:03:25 siva Exp $
 *
 * Description: Routines for Webnm Module 
 *******************************************************************/

#include "enm.h"
#include "webnm.h"
#include "webnmutl.h"
#include "vcm.h"
#ifdef TACACS_WANTED
#include "tacacs.h"
#endif
#include "fsvlan.h"
#include "l2iwf.h"
#include "vcm.h"
#include "arp.h"
#include "cli.h"
#include "fsutil.h"
#include "webprivds.h"
#include "webprivid.h"
#include "fpam.h"
#ifdef WLC_WANTED
#include "wssifinc.h"
#include "wsssta.h"
#include "wssstawlcprot.h"

#include "wssifstatrc.h"
extern UINT4        gu4WssStaWebAuthDebugMask;
#endif

extern tHtmlPage    rehtml[];
extern tHtmlPage    lohtml[];
extern tHtmlPage    erhtml[];

extern UINT4        gu4HttpMask;
extern UINT1        gu1HttpTimeOutEnable;
extern BOOL1        gb1HttpLoginReq;

#ifdef ISS_WANTED
#include "iss.h"
extern UINT1        gu1IssLoginAuthMode;
#endif
#include "http.h"

#ifndef CLI_WANTED
#define CLI_SUCCESS OSIX_SUCCESS
#define CLI_FAILURE OSIX_FAILURE
#endif
#ifdef SSL_WANTED
#endif
#define WEB_NO_COMMAND 1155

extern INT4         gi4WebSysLogId;

/* to be used for generating SYSLOG message*/
UINT1               gu1WebnmUserName[ENM_MAX_USERNAME_LEN];

/* to identify if syslog message in SNMP_SET is
 * generated from web or not*/
UINT1               gu1WebnmSnmpQueryFromWebStatus = OSIX_FALSE;

extern struct WebnmUserDetails gaWebnmUser[];

extern struct Ipv4Sec gIpv4Sec[];
extern struct Ipv6Sec gIpv6Sec[];

UINT4               gu4VcmContext = L2IWF_DEFAULT_CONTEXT;
UINT4               gu4VcmL3Context = VCM_DEFAULT_CONTEXT;

UINT4               gi4HttpsTrcLevel = HTTP_ONE;

#ifdef CLI_WANTED
#define MAX_NAME_LEN 20
#endif
PRIVATE INT4 WebnmGetDataFromInstance PROTO ((tSNMP_MULTI_DATA_TYPE *,
                                              tSNMP_OID_TYPE *, UINT1,
                                              UINT4 *));

PRIVATE VOID        WebnmIndexInit (tHttp *);
tSNMP_OID_TYPE     *WebnmAllocOid (VOID);

#ifdef WLC_WANTED
extern VOID         IssProcessWebAuthLoginPageGet (tHttp * pHttp);
extern VOID         IssProcessWebAuthLoginPageSet (tHttp * pHttp);
#endif

PRIVATE tSNMP_OCTET_STRING_TYPE *WebnmAllocOctetString (VOID);
INT4                WebnmTestSet (tHttp *, UINT1, tSNMP_OID_TYPE *);

/************************************************************************
 *  Function Name   : WebNMPolicyCheck
 *  Description     : This function returns the
 *                    access permitted for the user.
 *  Input           : u4UserGroup - User Group
 *                    u4Access    - Access permission in the Page
 *                    u4GroupID   - GroupID in the Page
 *  Output          : Access
 *  Returns         : Access
 ************************************************************************/
UINT4
WebNMPolicyCheck (UINT4 u4UserGroup, UINT4 u4Access, UINT4 u4GroupID)
{
    UINT4               u4PageAccess = ENM_NO_ACCESS;
    /* If In the Page User access is ALL
     * give him READ and WRITE access */
    if (u4Access == ENM_ALL_ACCESS)
    {
        return ENM_READ | ENM_WRITE;
    }

    /* If the USER is ROOT give him READ and WRITE
     * permission*/
    if (u4UserGroup & ENM_ROOT)
    {
        return ENM_READ | ENM_WRITE;
    }

    /* If User is ADMIN give him the ADMIN access */
    if (u4UserGroup & ENM_ADMIN)
    {
        u4PageAccess = (u4Access & ENM_ADMIN_ACCESS) >> 2;
    }

    /* If user and the page belongs to same
     * group give him access permission specified
     * If they belongs to differnt group based on other
     * access permission will be given*/
    if (u4UserGroup & u4GroupID)
    {
        u4PageAccess |= ((u4Access & ENM_SELF_ACCESS) >> 4);
    }
    else
    {
        u4PageAccess |= (u4Access & ENM_OTHER_ACCESS);
    }
    return u4PageAccess;
}

/************************************************************************
 *  Function Name   : WebnmInit 
 *  Description     : Webnm Memory Init Function. Memory Pointer will be
 *                    set to starting of mempool for every request and
 *                    because of this there is no memory free required.
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
WebnmInit (tHttp * pHttp)
{
    INT4                i4Index = ENM_FAILURE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return;
    }

    gu4MultiDataCount[i4Index] = HTTP_ZERO;
    gu4OctetStrCount[i4Index] = HTTP_ZERO;
    gu4OidCount[i4Index] = HTTP_ZERO;
    WebnmIndexInit (pHttp);
    HttpWebnmConfigurePolicy (pHttp);
}

/************************************************************************
 *  Function Name   : WebnmIndexInit 
 *  Description     : Init Index Pool for Index handling in SNMP.
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

PRIVATE VOID
WebnmIndexInit (tHttp * pHttp)
{
    UINT4               u4Count = HTTP_ZERO;
    INT4                i4Index = ENM_FAILURE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return;
    }
    MEMSET (&WebnmIndexPool1[i4Index][0], 0,
            (sizeof (tSnmpIndex) * ISS_MAX_INDICES));
    MEMSET (&WebnmMultiPool1[i4Index][0], 0,
            (sizeof (tSNMP_MULTI_DATA_TYPE) * ISS_MAX_INDICES));
    MEMSET (&WebnmOctetPool1[i4Index][0], 0,
            (sizeof (tSNMP_OCTET_STRING_TYPE) * ISS_MAX_INDICES));
    MEMSET (&WebnmOIDPool1[i4Index][0], 0,
            (sizeof (tSNMP_OID_TYPE) * ISS_MAX_INDICES));
    MEMSET (&au1WebnmData1[i4Index][0][0], 0, 1024);

    for (u4Count = HTTP_ZERO; u4Count < WEBNM_MAX_INDICES; u4Count++)
    {
        WebnmIndexPool1[i4Index][u4Count].pIndex
            = &(WebnmMultiPool1[i4Index][u4Count]);
        WebnmMultiPool1[i4Index][u4Count].pOctetStrValue
            = &(WebnmOctetPool1[i4Index][u4Count]);
        WebnmMultiPool1[i4Index][u4Count].pOidValue
            = &(WebnmOIDPool1[i4Index][u4Count]);
        WebnmMultiPool1[i4Index][u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1WebnmData1[i4Index][u4Count];
        WebnmMultiPool1[i4Index][u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1WebnmData1[i4Index][u4Count];

        WebnmIndexPool2[i4Index][u4Count].pIndex
            = &(WebnmMultiPool2[i4Index][u4Count]);
        WebnmMultiPool2[i4Index][u4Count].pOctetStrValue
            = &(WebnmOctetPool2[i4Index][u4Count]);
        WebnmMultiPool2[i4Index][u4Count].pOidValue
            = &(WebnmOIDPool2[i4Index][u4Count]);
        WebnmMultiPool2[i4Index][u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1WebnmData2[i4Index][u4Count];
        WebnmMultiPool2[i4Index][u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1WebnmData2[i4Index][u4Count];
    }
}

/************************************************************************
 *  Function Name   : WebnmProcessGet 
 *  Description     : Process the Get Request for both scalar and table
 *  Input           : pHttp - Pointer to Http entry
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
WebnmProcessGet (tHttp * pHttp)
{
    UINT4               u4Count = HTTP_ZERO, u4Error = HTTP_ZERO;
    UINT1               u1FirstRow = HTTP_ZERO;
    tSNMP_OID_TYPE     *pCurOid = NULL, *pNextOid = NULL, *pInstance = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSNMP_MULTI_DATA_TYPE *pTableData[WENM_MAX_MULTI_DATA];
    tSnmpIndex         *pCurIndex = NULL, *pNextIndex = NULL;
    CHR1                au1Array[ENM_MAX_METHOD_LENGTH];
    INT4                i4Index = ENM_FAILURE;
    pHttp->i4Write = HTTP_ZERO;
    MEMSET (au1Array, HTTP_ZERO, ENM_MAX_METHOD_LENGTH);

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index)
        == ENM_FAILURE)
    {
        return;
    }

    pCurIndex = WebnmIndexPool1[i4Index];
    pNextIndex = WebnmIndexPool2[i4Index];

    /* Get Process for Table Elements */
    if (pHttp->i4MibType == ENM_TABLE)
    {
        for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
             u4Count++)
        {
        WEBNM_ALLOC_MULTIDATA (pTableData[u4Count])}
        WEBNM_ALLOC_OID (pCurOid);
        WEBNM_ALLOC_OID (pNextOid);
        WEBNM_ALLOC_OID (pInstance);
        WebnmSendString (pHttp, TABLE_FLAG);
        while (HTTP_ONE)
        {
            for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
                 u4Count++)
            {
                pCurOid->u4_Length = HTTP_ZERO;
                WebnmCopyOid (pCurOid, &pHttp->pOid[u4Count]);
                WebnmCopyOid (pCurOid, pInstance);
                if (SNMPGetNextOID (*pCurOid, pNextOid, pTableData[u4Count],
                                    &u4Error, pCurIndex, pNextIndex,
                                    SNMP_DEFAULT_CONTEXT) != SNMP_SUCCESS)
                {
                    ENM_TRACE ("Webnm: Failed to get the next OID\n");
                }
            }
            WebnmGetInstanceOid (&pHttp->pOid[u4Count - 1], pNextOid,
                                 pInstance);
            if (pInstance->u4_Length == HTTP_ZERO)
            {
                break;
            }
            WebnmConvertOidToString (pInstance, pHttp->au1Instance);
            if (u1FirstRow == HTTP_ZERO)
            {
                SPRINTF ((CHR1 *) pHttp->au1Array, TABLE_FIRST_ROW_START_FLAG,
                         pHttp->au1Instance);
                u1FirstRow = HTTP_ONE;
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1Array, TABLE_ROW_START_FLAG,
                         pHttp->au1Instance);
            }
            WebnmSockWrite (pHttp, pHttp->au1Array,
                            HTTP_STRLEN (pHttp->au1Array));
            for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
                 u4Count++)
            {
                WebnmConvertDataToString (pTableData[u4Count],
                                          pHttp->au1DataString,
                                          pHttp->pu1Type[u4Count]);
                WebnmSockWrite (pHttp, TABLE_DATA_START_FLAG,
                                HTTP_STRLEN (TABLE_DATA_START_FLAG));
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                HTTP_STRLEN (pHttp->au1DataString));
                WebnmSockWrite (pHttp, TABLE_DATA_END_FLAG,
                                HTTP_STRLEN (TABLE_DATA_END_FLAG));
            }
            WebnmSockWrite (pHttp, TABLE_ROW_END_FLAG,
                            HTTP_STRLEN (TABLE_ROW_END_FLAG));
        }
    }
    else
    {
        WEBNM_ALLOC_MULTIDATA (pData);
        for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
             u4Count++)
        {
            pCurOid = &pHttp->pOid[u4Count];
            WebnmConvertOidToString (pCurOid, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) au1Array, "%s", HTML_KEY_STRING);
            HTTP_STRNCAT (pHttp->au1KeyString, au1Array, sizeof (au1Array));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
            {
                WebnmConvertDataToString (pData, pHttp->au1DataString,
                                          pHttp->pu1Type[u4Count]);
            }
            else
            {
                pHttp->au1DataString[0] = '\0';
                if (u4Error == NO_SUCH_OBJECT)
                {
                    HTTP_STRCPY (pHttp->au1DataString, WEBNM_NO_SUCH_OBJ_STR);
                }
                else if (u4Error == NO_SUCH_INSTANCE)
                {
                    HTTP_STRCPY (pHttp->au1DataString, WEBNM_NO_SUCH_INS_STR);
                }
                else
                {
                    HTTP_STRCPY (pHttp->au1DataString, WebnmSnmpError[u4Error]);
                }
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            HTTP_STRLEN (pHttp->au1DataString));
            if (pHttp->pu1Access[u4Count] != READONLY)
            {
                if (WebnmSendString (pHttp, pHttp->au1KeyString) == ENM_SUCCESS)
                {
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    HTTP_STRLEN (pHttp->au1DataString));
                }
            }
        }
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/************************************************************************
 *  Function Name   : WebnmProcessPost 
 *  Description     : Process Post Request. This can be Set/Modify/Create
 *  Input           : pHttp - Pointer to Http Entry
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
WebnmProcessPost (tHttp * pHttp)
{
    UINT4               u4Count = HTTP_ZERO, u4Index = HTTP_ZERO, u4Error =
        HTTP_ZERO;
    tSNMP_OID_TYPE     *pOid = NULL, *pInsOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpIndex         *pCurIndex = NULL;
    INT4                i4Index = ENM_FAILURE;
    CHR1                au1Array[ENM_MAX_METHOD_LENGTH];
    WEBNM_ALLOC_OID (pOid);
    WEBNM_ALLOC_OID (pInsOid);
    MEMSET (au1Array, HTTP_ZERO, ENM_MAX_METHOD_LENGTH);

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return;
    }

    pCurIndex = WebnmIndexPool1[i4Index];

    if (HttpGetValuebyName (WEBNM_ACTION, pHttp->au1Array,
                            pHttp->au1PostQuery) != ENM_SUCCESS)
    {
        ENM_TRACE ("Webnm: No ACTION Specified in the HTML Page\n");
        HttpSendError (pHttp, HTTP_BAD_REQUEST);
        return;
    }
    if (HttpGetValuebyName (WEBNM_TABLE_INDEX, pHttp->au1Instance,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Instance, WEBNM_TABLE_KEY) == HTTP_ZERO)
        {
            WebnmFormInstance (pHttp, pHttp->au1Instance);
        }
        WebnmConvertStringToOid (pInsOid, pHttp->au1Instance, ENM_DEC_DATA);
    }
    if (STRCMP (pHttp->au1Array, WEBNM_SET) == HTTP_ZERO)
    {
        if (WebnmTestSet (pHttp, WEBNM_TEST_FUNC, pInsOid) != ENM_SUCCESS)
        {
            return;
        }
        if (WebnmTestSet (pHttp, WEBNM_SET_FUNC, pInsOid) != ENM_SUCCESS)
        {
            return;
        }
        WebnmModifyPage (pHttp);
        WebnmProcessGet (pHttp);
    }
    else if (STRCMP (pHttp->au1Array, WEBNM_MODIFY) == HTTP_ZERO)
    {

        if (pInsOid->u4_Length == HTTP_ZERO)
        {
            WebnmSendErr (pHttp, WEBNM_NO_ROW);
            return;
        }
        WEBNM_ALLOC_MULTIDATA (pData);
        WebnmSendString (pHttp, WEBNM_TABLE_KEY);
        WebnmSockWrite (pHttp, pHttp->au1Instance,
                        HTTP_STRLEN (pHttp->au1Instance));
        for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
             u4Count++)
        {
            pOid->u4_Length = HTTP_ZERO;
            WebnmCopyOid (pOid, &pHttp->pOid[u4Count]);
            WebnmConvertOidToString (pOid, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) au1Array, "%s", HTML_KEY_STRING);
            HTTP_STRNCAT (pHttp->au1KeyString, au1Array, sizeof (au1Array));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmCopyOid (pOid, pInsOid);
            if (pHttp->pu1Access[u4Count] == INDEX)
            {
                if (WebnmGetDataFromInstance (pData, pInsOid,
                                              pHttp->pu1Type[u4Count],
                                              &u4Index) == ENM_SUCCESS)
                {
                    WebnmConvertDataToString (pData, pHttp->au1DataString,
                                              pHttp->pu1Type[u4Count]);
                }
                else
                {
                    pHttp->au1DataString[0] = '\0';
                    HTTP_STRCPY (pHttp->au1DataString, WEBNM_NO_SUCH_INS_STR);
                }
            }
            else
            {
                if (SNMPGet (*pOid, pData, &u4Error, pCurIndex,
                             SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
                {
                    WebnmConvertDataToString (pData, pHttp->au1DataString,
                                              pHttp->pu1Type[u4Count]);
                }
                else
                {
                    pHttp->au1DataString[0] = '\0';
                    if (u4Error == NO_SUCH_OBJECT)
                    {
                        HTTP_STRCPY (pHttp->au1DataString,
                                     WEBNM_NO_SUCH_OBJ_STR);
                    }
                    else if (u4Error == NO_SUCH_INSTANCE)
                    {
                        HTTP_STRCPY (pHttp->au1DataString,
                                     WEBNM_NO_SUCH_INS_STR);
                    }
                    else
                    {
                        HTTP_STRCPY (pHttp->au1DataString,
                                     WebnmSnmpError[u4Error]);
                    }
                }
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            HTTP_STRLEN (pHttp->au1DataString));
            /* Updated for hidden fields */
            if (WebnmSendString (pHttp, pHttp->au1KeyString) == ENM_SUCCESS)
            {
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                HTTP_STRLEN (pHttp->au1DataString));
            }
        }
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    else if (STRCMP (pHttp->au1Array, WEBNM_CREATE) == HTTP_ZERO)
    {
        pHttp->au1DataString[0] = '\0';
        for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid;
             u4Count++)
        {
            pOid->u4_Length = HTTP_ZERO;
            WebnmCopyOid (pOid, &pHttp->pOid[u4Count]);
            WebnmConvertOidToString (pOid, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) au1Array, "%s", HTML_KEY_STRING);
            HTTP_STRNCAT (pHttp->au1KeyString, au1Array, sizeof (au1Array));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            HTTP_STRLEN (pHttp->au1DataString));
            if (WebnmSendString (pHttp, pHttp->au1KeyString) == ENM_SUCCESS)
            {
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                HTTP_STRLEN (pHttp->au1DataString));
            }
        }
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    else
    {
        ENM_TRACE ("Webnm: Unknow Action Type \n");
        HttpSendError (pHttp, HTTP_SERVER_ERROR);
    }
}

/************************************************************************
 *  Function Name   : WebnmSendString 
 *  Description     : Send the Html file content upto the specified 
 *                    string and move the write offset pointer accordingly.
 *  Input           : pHttp- Pointer to the Http entry where html content
 *                    pointer is stored
 *                    pData - Pointer to a string upto which html content
 *                    to be sent.
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

INT4
WebnmSendString (tHttp * pHttp, UINT1 *pData)
{
    UINT1              *pu1StartPtr = NULL, *pu1EndPtr = NULL;
    pu1StartPtr = (UINT1 *) pHttp->pi1Html + pHttp->i4Write;
    pu1EndPtr = (UINT1 *) STRSTR (pu1StartPtr, pData);
    if ((pu1EndPtr != NULL) &&
        (pu1EndPtr < (UINT1 *) (pHttp->pi1Html + pHttp->i4HtmlSize)))
    {
        WebnmSockWrite (pHttp, pu1StartPtr, (pu1EndPtr - pu1StartPtr));
        pHttp->i4Write += (pu1EndPtr - pu1StartPtr);
        pHttp->i4Write += HTTP_STRLEN (pData);
        return ENM_SUCCESS;
    }
    return ENM_FAILURE;
}

/************************************************************************
 *  Function Name   : WebnmAllocMultiData 
 *  Description     : Allocate a Multidata from multidata pool
 *  Input           : None
 *  Output          : None 
 *  Returns         : Allocated Multidata pointer or null
 ************************************************************************/

tSNMP_MULTI_DATA_TYPE *
WebnmAllocMultiData ()
{
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
    INT4                i4Index = ENM_FAILURE;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;

    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }

    if (HttpChildProcessIndex (ProcessingTaskId, &i4Index) == ENM_FAILURE)
    {
        return NULL;
    }

    if ((gu4MultiDataCount[i4Index] >= WENM_MAX_MULTI_DATA) ||
        (gu4OctetStrCount[i4Index] >= WEBNM_MAX_OCTET_STRING) ||
        (gu4OidCount[i4Index] >= WEBNM_MAX_OID_TYPE))
    {
        ENM_TRACE ("Webnm: Unabel to Allocate memory for MultiData\n");
        return NULL;
    }

    pMultiData = &gMultiData[i4Index][gu4MultiDataCount[i4Index]++];
    pMultiData->pOctetStrValue = WebnmAllocOctetString ();
    pMultiData->pOidValue = WebnmAllocOid ();
    if ((pMultiData->pOctetStrValue == NULL) || (pMultiData->pOidValue == NULL))
    {
        ENM_TRACE ("Webnm: Unable to Allocate memory for MultiData\n");
        return NULL;
    }

    return pMultiData;
}

/************************************************************************
 *  Function Name   : WebnmAllocOctetString 
 *  Description     : Allocate octetstring from octetstring pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Pointer to the allocated octetstring or null 
 ************************************************************************/

PRIVATE tSNMP_OCTET_STRING_TYPE *
WebnmAllocOctetString ()
{
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    INT4                i4Index = ENM_FAILURE;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;

    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }

    if (HttpChildProcessIndex (ProcessingTaskId, &i4Index) == ENM_FAILURE)
    {
        return NULL;
    }

    if (gu4OctetStrCount[i4Index] >= WEBNM_MAX_OCTET_STRING)
    {
        ENM_TRACE ("Webnm: Unabel to Allocate memory for Octet String\n");
        return NULL;
    }

    pOctetString = &gOctetString[i4Index][gu4OctetStrCount[i4Index]];
    pOctetString->pu1_OctetList =
        &gau1OctetString[i4Index][gu4OctetStrCount[i4Index]++][0];
    pOctetString->i4_Length = HTTP_ZERO;
    return pOctetString;
}

/************************************************************************
 *  Function Name   : WebnmAllocOid 
 *  Description     : Allocate OID from OID Pool
 *  Input           : None
 *  Output          : None 
 *  Returns         : Pointer to OID or null
 ************************************************************************/

tSNMP_OID_TYPE     *
WebnmAllocOid ()
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT4                i4Index = ENM_FAILURE;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;

    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }

    if (HttpChildProcessIndex (ProcessingTaskId, &i4Index) == ENM_FAILURE)
    {
        return NULL;
    }

    if (gu4OidCount[i4Index] >= WEBNM_MAX_OID_TYPE)
    {
        ENM_TRACE ("Webnm: Unabel to Allocate memory for Oid List\n");
        return NULL;
    }
    pOid = &gOidType[i4Index][gu4OidCount[i4Index]];
    pOid->pu4_OidList = &gau4OidType[i4Index][gu4OidCount[i4Index]++][0];
    pOid->u4_Length = HTTP_ZERO;
    return pOid;
}

/************************************************************************
 *  Function Name   : WebnmGetDataFromInstance 
 *  Description     : Form Data from Instance string
 *  Input           : pInsOid - Instance OID Pointer
 *                    u1Type - Type of the output data to be
 *  Output          : pData - Multidata to be formed
 *                    pu4Count - OID length from where the conversion
 *                    should be started and it will be updated once 
 *                    data is read
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

PRIVATE INT4
WebnmGetDataFromInstance (tSNMP_MULTI_DATA_TYPE * pData,
                          tSNMP_OID_TYPE * pInsOid, UINT1 u1Type,
                          UINT4 *pu4Count)
{
    UINT4               u4Index = HTTP_ZERO, u4Count = HTTP_ZERO;
    if (*pu4Count >= pInsOid->u4_Length)
    {
        return ENM_FAILURE;
    }
    switch (u1Type)
    {
        case ENM_INTEGER32:
            pData->i4_SLongValue = pInsOid->pu4_OidList[*pu4Count];
            (*pu4Count)++;
            break;
        case ENM_UINTEGER32:
            pData->u4_ULongValue = pInsOid->pu4_OidList[*pu4Count];
            (*pu4Count)++;
            break;
        case ENM_IPADDRESS:
            pData->u4_ULongValue = pInsOid->pu4_OidList[*pu4Count];
            pData->u4_ULongValue = pData->u4_ULongValue << ENM_BYTE_LEN;
            (*pu4Count)++;
            pData->u4_ULongValue |= pInsOid->pu4_OidList[*pu4Count];
            pData->u4_ULongValue = pData->u4_ULongValue << ENM_BYTE_LEN;
            (*pu4Count)++;
            pData->u4_ULongValue |= pInsOid->pu4_OidList[*pu4Count];
            pData->u4_ULongValue = pData->u4_ULongValue << ENM_BYTE_LEN;
            (*pu4Count)++;
            pData->u4_ULongValue |= pInsOid->pu4_OidList[*pu4Count];
            (*pu4Count)++;
            break;
        case ENM_OCTETSTRING:
        case ENM_DISPLAYSTRING:
            u4Index = pInsOid->pu4_OidList[*pu4Count];
            (*pu4Count)++;
            pData->pOctetStrValue->i4_Length = HTTP_ZERO;
            for (u4Count = HTTP_ZERO; u4Count < u4Index; u4Count++)
            {
                pData->pOctetStrValue->pu1_OctetList[u4Count] =
                    (UINT1) pInsOid->pu4_OidList[*pu4Count];
                pData->pOctetStrValue->i4_Length++;
                (*pu4Count)++;
            }
            break;
        default:
            ENM_TRACE ("Webnm:Unknow Data Type\n");
            return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : WebnmFormInstance 
 *  Description     : Form Instance from the not accessible element while
 *                    creating new table entry
 *  Input           : pHttp - Pointer to the http entry from this we can
 *                    values for not accessible elements
 *  Output          : pu1Instance - Pointer to the instance in string format
 *  Returns         : None
 ************************************************************************/

VOID
WebnmFormInstance (tHttp * pHttp, UINT1 *pu1Instance)
{
    UINT4               u4Count = HTTP_ZERO, u4First = HTTP_ZERO, u4DCount =
        HTTP_ZERO, u4OctetCount = HTTP_ZERO;
    CHR1                au1Array[ENM_MAX_HA2_LEN];
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT4               u4OidCount = HTTP_ZERO;

    WEBNM_ALLOC_OID (pOid);
    pHttp->au1Name[0] = '\0';
    pHttp->au1Value[0] = '\0';
    pu1Instance[0] = '\0';
    MEMSET (au1Array, HTTP_ZERO, ENM_MAX_HA2_LEN);
    for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid; u4Count++)
    {
        WebnmConvertOidToString (&pHttp->pOid[u4Count], pHttp->au1Name);
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            /* Take care octet string, display string */
            if (pHttp->pu1Access[u4Count] == INDEX)
            {
                if (u4First != HTTP_ZERO)
                {
                    SPRINTF ((CHR1 *) au1Array, "%s", WEBNM_DOT_STRING);
                    HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                }
                if (pHttp->pu1Type[u4Count] == ENM_DISPLAYSTRING)
                {
                    issDecodeSpecialChar (pHttp->au1Value);
                    SPRINTF ((CHR1 *) au1Array, "%u.",
                             HTTP_STRLEN (pHttp->au1Value));
                    HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    for (u4DCount = HTTP_ZERO; u4DCount < (HTTP_STRLEN
                                                           (pHttp->au1Value) -
                                                           HTTP_ONE);
                         u4DCount++)
                    {
                        if (u4DCount < ENM_MAX_NAME_LEN)
                        {
                            SNPRINTF ((CHR1 *) au1Array,
                                      ENM_MAX_HA2_LEN, "%d.",
                                      pHttp->au1Value[u4DCount]);
                            HTTP_STRNCAT (pu1Instance, au1Array,
                                          sizeof (au1Array));
                        }
                    }

                    if (u4DCount < ENM_MAX_NAME_LEN)
                    {
                        SPRINTF ((CHR1 *) au1Array, "%d",
                                 pHttp->au1Value[u4DCount]);
                        HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    }
                }
                else if (pHttp->pu1Type[u4Count] == ENM_IMP_DISPLAYSTRING)
                {
                    for (u4DCount = HTTP_ZERO; u4DCount < (HTTP_STRLEN
                                                           (pHttp->au1Value) -
                                                           HTTP_ONE);
                         u4DCount++)
                    {
                        if (u4DCount < ENM_MAX_NAME_LEN)
                        {
                            SPRINTF ((CHR1 *) au1Array, "%d.",
                                     pHttp->au1Value[u4DCount]);
                            HTTP_STRNCAT (pu1Instance, au1Array,
                                          sizeof (au1Array));
                        }
                    }
                    if (u4DCount < ENM_MAX_NAME_LEN)
                    {
                        SPRINTF ((CHR1 *) au1Array, "%d",
                                 pHttp->au1Value[u4DCount]);
                        HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    }
                }
                else if (pHttp->pu1Type[u4Count] == ENM_OCTETSTRING)
                {
                    issDecodeSpecialChar (pHttp->au1Value);
                    WebnmConvertColonStringToOid (pOid, pHttp->au1Value,
                                                  ENM_HEX_DATA);
                    SPRINTF ((CHR1 *) au1Array, "%u.", pOid->u4_Length);
                    HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));

                    for (u4OctetCount = HTTP_ZERO;
                         u4OctetCount < pOid->u4_Length; u4OctetCount++)
                    {
                        SPRINTF ((CHR1 *) au1Array, "%u",
                                 pOid->pu4_OidList[u4OctetCount]);
                        if (u4OctetCount != pOid->u4_Length - 1)
                        {
                            HTTP_STRNCAT ((CHR1 *) au1Array, WEBNM_DOT_STRING,
                                          sizeof (au1Array));
                        }
                        HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    }
                }
                else if (pHttp->pu1Type[u4Count] == ENM_OIDTYPE)
                {
                    for (u4DCount = HTTP_ZERO; u4DCount <
                         (HTTP_STRLEN (pHttp->au1Value)); u4DCount++)
                    {
                        if (u4DCount < ENM_MAX_NAME_LEN)
                        {
                            if (pHttp->au1Value[u4DCount] == '.')
                            {
                                u4OidCount++;
                            }
                        }
                    }
                    u4OidCount++;

                    SPRINTF (au1Array, "%u", u4OidCount);
                    HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    SNPRINTF (au1Array, ENM_MAX_HEADER_LENGTH, "%s",
                              WEBNM_DOT_STRING);
                    HTTP_STRNCAT (pu1Instance, au1Array, sizeof (au1Array));
                    HTTP_STRNCAT (pu1Instance, pHttp->au1Value,
                                  sizeof (pHttp->au1Value));
                }
                else if (pHttp->pu1Type[u4Count] == ENM_IMP_OIDTYPE)
                {
                    HTTP_STRNCAT (pu1Instance, pHttp->au1Value,
                                  sizeof (pHttp->au1Value));
                }
                else
                {
                    HTTP_STRNCAT (pu1Instance, pHttp->au1Value,
                                  sizeof (pHttp->au1Value));
                }
                u4First++;
            }
        }
    }
}

/*********************************************************************
*  Function Name : WebnmSendErr
*  Description   : Send error page if error occur.
*  Input(s)      : pi1Error- Error Message.
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/

VOID
WebnmSendErr (tHttp * pHttp, CONST INT1 *pi1Error)
{

    UINT1               au1ErrStr[WEBNM_MAX_STRING_LEN];

    MEMSET (au1ErrStr, HTTP_ZERO, WEBNM_MAX_STRING_LEN);

    STRNCPY (au1ErrStr, pi1Error, WEBNM_MAX_STRING_LEN - HTTP_ONE);
    pHttp->i4HtmlSize = HTTP_STRLEN (WEBNM_ERROR_START_STR) +
        HTTP_STRLEN (au1ErrStr) + HTTP_STRLEN (WEBNM_ERROR_END_STR);

    pHttp->u2RespCode = HTTP_UNAUTHORIZED;
    /* Setting the realm to be used in case of
     * Basic / Digest auth schemes */
    STRNCPY (pHttp->au1Realm, gaHTTP_REALM_WRONG_LOGIN,
             ENM_MAX_REALM_LEN - HTTP_ONE);

    HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
    WebnmSockWrite (pHttp, WEBNM_ERROR_START_STR,
                    HTTP_STRLEN (WEBNM_ERROR_START_STR));
    WebnmSockWrite (pHttp, au1ErrStr, HTTP_STRLEN (au1ErrStr));
    WebnmSockWrite (pHttp, WEBNM_ERROR_END_STR,
                    HTTP_STRLEN (WEBNM_ERROR_END_STR));
    return;
}

/*********************************************************************
*  Function Name : WebnmModifyPage
*  Description   : This function will change the html file name if it
*                  ends with table.html.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                    descriptor is stored
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
VOID
WebnmModifyPage (tHttp * pHttp)
{
    INT4                i4Count = HTTP_ZERO;
    if (HttpGetValuebyName (WEBNM_PARENT_PAGE, pHttp->au1Array,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        HTTP_STRCPY (pHttp->ai1HtmlName, pHttp->au1Array);
        if (HttpReadhtml (pHttp) == ENM_FAILURE)
        {
            return;
        }
    }

    i4Count = HTTP_STRLEN (pHttp->ai1HtmlName);
    if (i4Count >= WEBNM_LOCAL_OFFSET)
    {
        i4Count = i4Count - WEBNM_LOCAL_OFFSET;
    }
    if (STRNCMP
        (pHttp->ai1HtmlName + i4Count, WEBNM_TABLE_HTML,
         WEBNM_LOCAL_OFFSET) != HTTP_ZERO)
    {
        return;
    }
    STRNCPY (pHttp->ai1HtmlName + i4Count, WEBNM_PAGE_HTML, WEBNM_LOCAL_OFFSET);
    if (HttpReadhtml (pHttp) == ENM_FAILURE)
    {
        return;
    }
    return;
}

/*********************************************************************
*  Function Name : WebnmTestSet
*  Description   : This function will check the Access Type
*                  and call SNMPTest or SNMPSet function.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                    descriptor is stored
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/

INT4
WebnmTestSet (tHttp * pHttp, UINT1 u1Type, tSNMP_OID_TYPE * pInsOid)
{
    UINT4               u4Count = HTTP_ZERO, u4Error = HTTP_ZERO;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT1               au1Value[64];
    tSnmpIndex         *pCurIndex = NULL;
    INT4                i4Index = ENM_FAILURE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    pCurIndex = WebnmIndexPool1[i4Index];
    WEBNM_ALLOC_OID_RETURN (pOid);

    for (u4Count = HTTP_ZERO; u4Count < (UINT4) pHttp->i4NoOfSnmpOid; u4Count++)
    {
        if ((pHttp->pu1Access[u4Count] == READWRITE)
            || (pHttp->pu1Access[u4Count] == READCREATE))
        {
            pOid->u4_Length = HTTP_ZERO;
            WebnmCopyOid (pOid, &pHttp->pOid[u4Count]);
            WebnmConvertOidToString (pOid, pHttp->au1Name);
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                MEMSET (au1Value, 0, 64);
                if (STRLEN (pHttp->au1Value) < 63)
                {
                    HTTP_STRCPY (au1Value, pHttp->au1Value);
                }
                if (HttpNextGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                            pHttp->au1PostQuery) == ENM_SUCCESS)
                {
                    if (STRCMP (au1Value, pHttp->au1Value) == HTTP_ZERO)
                    {
                        continue;
                    }
                }
                HTTP_STRCPY (pHttp->au1Value, au1Value);
                WebnmCopyOid (pOid, pInsOid);
                if ((pData = WebnmConvertStringToData (pHttp->au1Value,
                                                       pHttp->
                                                       pu1Type[u4Count])) !=
                    NULL)
                {
                    if (u1Type == WEBNM_SET_FUNC)
                    {
                        if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                                     SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                        {
                            WebnmSendErr (pHttp, WebnmSnmpError[HTTP_FIVE]);
                            return ENM_FAILURE;
                        }
                    }
                    else
                    {
                        if (SNMPTest (*pOid, pData, &u4Error, pCurIndex,
                                      SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                        {
                            if (u4Error == HTTP_ZERO)
                            {
                                WebnmSendErr (pHttp, WebnmSnmpError[HTTP_TEN]);
                            }
                            else if (u4Error == NO_SUCH_INSTANCE)
                            {
                                WebnmSendErr (pHttp, WEBNM_NO_SUCH_INS_STR);
                            }
                            else if (u4Error == NO_SUCH_OBJECT)
                            {
                                WebnmSendErr (pHttp, WEBNM_NO_SUCH_OBJ_STR);
                            }
                            else
                            {
                                WebnmSendErr (pHttp, WebnmSnmpError[u4Error]);
                            }
                            return ENM_FAILURE;
                        }
                    }
                }
            }
        }
    }
    return ENM_SUCCESS;
}

/*********************************************************************
*  Function Name : WebnmSendLogin
*  Description   : This function will return login page.
*  Input(s)      : i1Message - Error Message attached with login page.
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
VOID
WebnmSendLogin (tHttp * pHttp, INT1 i1Message)
{
#ifdef ISS_WANTED
    INT4                i4Count = HTTP_ZERO;
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
    INT1                i1FirstLogin = OSIX_FALSE;
#endif
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif

    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
#ifdef ISS_WANTED

        if (STRCMP (pHttp->ai1HtmlName, "index.html") == HTTP_ZERO)
        {
            /* ISS Login page can be viewed in multiple scenarios
             * but syslog message should be sent during establishment of 
             * connection to ISS. During this connection establishment 
             * "i1Message" value will be '2' and the flag 1FirstLogin 
             * is introduced to identify the connection establishment */
            if (i1Message == HTTP_TWO)
            {
                i1FirstLogin = OSIX_TRUE;
            }
            i1Message = HTTP_ZERO;
        }

        /* Send redirect.html page when the auth scheme
         * is Basic/Digest */
        if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
            (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST))
        {
            for (i4Count = HTTP_ZERO;
                 rehtml[i4Count].pi1Name != NULL; i4Count++)
            {
                if (STRCMP (rehtml[i4Count].pi1Name,
                            "redirect.html") == HTTP_ZERO)
                {
                    /* Initially when the client requests for 
                     * the login page, the i1Message is always passed 
                     * as 2 which denotes "Session TimeOut." 
                     * Also the ai1HtmlName is always passed 
                     * as "index.html" which doesnt exist. 
                     * Hence to override this problem we do a STRCMP 
                     * to check whether its the request for 
                     * the login page and change 
                     * the i1Message value to 0. */
                    HTTP_STRCPY (pHttp->ai1HtmlName, "redirect.html");
                    pHttp->pi1Html = (INT1 *) rehtml[i4Count].pi1Ptr;
                    pHttp->i4HtmlSize = rehtml[i4Count].i4Size;

                    pHttp->u2RespCode = HTTP_OK;
                    HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT);
                    IssSendLoginPage (pHttp, i1Message);

                    return;
                }
            }
        }
        /* Send login.html page when the authentication scheme 
         * default */
        else if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            for (i4Count = HTTP_ZERO;
                 lohtml[i4Count].pi1Name != NULL; i4Count++)
            {
                if (STRCMP (lohtml[i4Count].pi1Name, "login.html") == HTTP_ZERO)
                {
                    /* Initially when the client requests for the 
                     * login page, the i1Message is always passed 
                     * as 2 which denotes "Session TimeOut." 
                     * Also the ai1HtmlName is always passed 
                     * as "index.html" which doesnt exist. 
                     * Hence to override this problem we do a 
                     * STRCMP to check whether its the request 
                     * for the login page and change 
                     * the i1Message value to 0. */
                    HTTP_STRCPY (pHttp->ai1HtmlName, "login.html");
                    pHttp->pi1Html = (INT1 *) lohtml[i4Count].pi1Ptr;
                    pHttp->i4HtmlSize = lohtml[i4Count].i4Size;

                    pHttp->u2RespCode = HTTP_OK;
                    HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT);
                    IssSendLoginPage (pHttp, i1Message);
                    for (u4Counter = HTTP_ZERO;
                         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                         u4Counter++)
                    {
                        if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                        {
                            IpAddress.u4Addr =
                                gIpv4Sec[u4Counter].ClientAddr.sin_addr.s_addr;
                            pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
                        }
                    }
#ifdef IP6_WANTED
                    for (u4Counter = HTTP_ZERO;
                         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                         u4Counter++)
                    {
                        if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                        {
                            MEMCPY (In6Addr.u1addr,
                                    gIpv6Sec[u4Counter].ClientV6Addr.sin6_addr.
                                    s6_addr, sizeof (In6Addr.u1addr));
                            pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
                        }
                    }
#endif
                    if ((i1FirstLogin == OSIX_TRUE) &&
                        (pHttp->u1RequestType == SSL_REQTYPE))
                    {
                        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4WebSysLogId,
                                      "WEBNM: HTTPS session established "
                                      "successfully from %s ", pu1String));
                    }
                    return;
                }
            }
        }
#endif
        if (i1Message == HTTP_TWO)
        {
            WebnmSockWrite (pHttp, WEBNM_LOGIN_EXPIRY,
                            HTTP_STRLEN (WEBNM_LOGIN_EXPIRY));
            return;
        }
        if (i1Message == HTTP_ONE)
        {
            WebnmSockWrite (pHttp, WEBNM_LOGIN_ERROR,
                            HTTP_STRLEN (WEBNM_LOGIN_ERROR));
        }
        WebnmSockWrite (pHttp, WEBNM_LOGIN_PAGE,
                        HTTP_STRLEN (WEBNM_LOGIN_PAGE));
        return;
    }
    else
    {
        WebnmSockWrite (pHttp, WEBNM_HOMEPAGE, HTTP_STRLEN (WEBNM_HOMEPAGE));
        return;
    }
}

/*********************************************************************
*  Function Name : WebnmSendLogout
*  Description   : This function will return logout page.
*  Input(s)      : 
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
VOID
WebnmSendLogout (tHttp * pHttp)
{
#ifdef ISS_WANTED
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; lohtml[i4Count].pi1Name != NULL; i4Count++)
    {
        if (STRCMP (lohtml[i4Count].pi1Name, "logout.html") == HTTP_ZERO)
        {
            HTTP_STRCPY (pHttp->ai1HtmlName, "logout.html");
            pHttp->pi1Html = (INT1 *) lohtml[i4Count].pi1Ptr;
            pHttp->i4HtmlSize = lohtml[i4Count].i4Size;
            pHttp->u2RespCode = HTTP_OK;

            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));

            return;
        }
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

/*********************************************************************
*  Function Name : WebnmVerifyLogin 
*  Description   : This will verify the Login Name and Passwd.
*  Parameter(s)  : NONE
*  Return Values : ENM_SUCCESS
* *********************************************************************/
INT4
WebnmVerifyLogin (UINT1 *pi1Login, UINT1 *pi1Passwd)
{
    if (WEBNM_AUTHENTICATE_USER ((INT1 *) pi1Login, (INT1 *) pi1Passwd)
        != CLI_FAILURE)
    {
        return ENM_SUCCESS;
    }
    return ENM_FAILURE;
}

/*********************************************************************
*  Function Name : WebnmEncode
*  Description   : This will encript a string.
*  Input(s)      : pi1_str  - String to be encoded.
*  Output(s)     : pi1_str  - Encoded string.
*  Return Values : None.
*********************************************************************/
VOID
WebnmEncode (UINT1 *pi1Str)
{
    static INT1         ai1NewStr[ENM_MAX_NAME_LEN + 1];
    INT4                i4Count1 = HTTP_ZERO;
    INT4                i4Count2 = HTTP_ZERO;
    INT4                i4Len = HTTP_ZERO;

    MEMSET (ai1NewStr, HTTP_ZERO, ENM_MAX_NAME_LEN + HTTP_ONE);

    if (pi1Str == NULL)
    {
        return;
    }
    i4Len = HTTP_STRLEN (pi1Str);
    if (i4Len > (ENM_MAX_NAME_LEN / 2))
    {
        return;
    }

    for (i4Count1 = HTTP_ZERO, i4Count2 = HTTP_ZERO; i4Count1 < i4Len;
         i4Count1++)
    {
        ai1NewStr[i4Count2++] = (INT1) ('a' + (pi1Str[i4Count1] & 0x0f));
        ai1NewStr[i4Count2++] = (INT1) ('a' + ((pi1Str[i4Count1] & 0xf0) >> 4));
    }
    ai1NewStr[i4Count2] = '\0';
    HTTP_STRCPY (pi1Str, ai1NewStr);
    return;
}

/*********************************************************************
*  Function Name : WebnmDecode
*  Description   : This will decode a encripted string.
*  Input(s)      : pi1_str - String to be decoded.
*  Output(s)     : pi1_str - String after decoded.
*  Return Values : None.
*********************************************************************/

VOID
WebnmDecode (UINT1 *pi1Str)
{
    INT4                i4Count1 = HTTP_ZERO;
    INT4                i4Count2 = HTTP_ZERO;
    INT4                i4Len = HTTP_ZERO;
    INT1                i1Char = HTTP_ZERO;

    if (pi1Str == NULL)
    {
        return;
    }
    i4Len = HTTP_STRLEN (pi1Str) / 2;
    for (i4Count1 = HTTP_ZERO, i4Count2 = HTTP_ZERO; i4Count1 < i4Len;
         i4Count1++)
    {
        i1Char = (INT1) (pi1Str[i4Count2++] - 'a');
        i1Char |= (pi1Str[i4Count2++] - 'a') << 4;
        pi1Str[i4Count1] = i1Char;
    }
    pi1Str[i4Count1] = '\0';
    return;
}

/*********************************************************************
*  Function Name : WebnmAuthenticate
*  Description   : This will Authenticate User Name and Passwd.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                  descriptor is stored
*  Output(s)     : None. 
*  Return Values : None.
*********************************************************************/

INT1
WebnmAuthenticate (tHttp * pHttp, UINT1 u1FirstTimeFlag)
{
    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
        /* Check if the authentication scheme received in the 
         * client request is same as the operational auth scheme.
         * If not prompt the user to clear the browsing history and
         * restart the browser */
        if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
        {
            WebnmSendRestartPage (pHttp);
            return ENM_FAILURE;
        }

        /* To generate the Request Digest */
        if (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST)
        {
            if (HttpGenerateDigest (pHttp,
                                    pHttp->au1Digest,
                                    ENM_REQUEST_DIGEST) == ENM_FAILURE)
            {
                ENM_TRACE ("Unable to calculate Request Digest\n");
                return ENM_FAILURE;
            }
        }
#ifdef ISS_WANTED
        return IssAuthenticate (pHttp, u1FirstTimeFlag);
#else
        UINT4               u4PageTime = HTTP_ZERO;
        UINT4               u4SysCurTime = HTTP_ZERO;
        INT4                i4Index = ENM_FAILURE;
        UINT1               au1Temp[ENM_MAX_NAME_LEN] = { HTTP_ZERO };
        UINT1              *pTemp = NULL;

        if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index)
            == ENM_FAILURE)
        {
            return ENM_FAILURE;
        }

        u4SysCurTime = WEBNM_GET_TIME ();

        /* For Default Authentication scheme,
         * extract the username and password */
        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            if (HttpGetValuebyName (WEBNM_LOGIN, au1WebnmLogin[i4Index],
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                STRNCPY (pHttp->ai1Username, au1WebnmLogin[i4Index],
                         ENM_MAX_USERNAME_LEN);
            }
            if (HttpGetValuebyName (WEBNM_PASSWD, au1WebnmPasswd[i4Index],
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                STRNCPY (pHttp->ai1Password, au1WebnmPasswd[i4Index],
                         ENM_MAX_PASSWORD_LEN);
            }
        }

        /* Verify GAMBIT */
        if (HttpGetValuebyName (WEBNM_GAMBIT, au1WebnmGambit[i4Index],
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            WebnmDecode (au1WebnmGambit[i4Index]);

            SSCANF ((char *) au1WebnmGambit[i4Index], "%u:%s",
                    &u4PageTime, au1Temp);
            pTemp = (UINT1 *) STRTOK (au1Temp, ":");
            if (pTemp == NULL)
            {
                ENM_TRACE ("Unable to Process Gambit\n");
            }
            HTTP_STRCPY (au1WebnmLogin[i4Index], pTemp);

            /*to support syslog messaging with user name */
            HTTP_STRCPY (gu1WebnmUserName, au1WebnmLogin[i4Index]);
            HTTP_STRCPY (pHttp->ai1Username, au1WebnmLogin[i4Index]);

            HTTP_STRCPY (au1WebnmPasswd[i4Index],
                         (pTemp + HTTP_STRLEN (au1WebnmLogin[i4Index]) +
                          HTTP_ONE));

            if ((u4SysCurTime - au4LastSentTime[i4Index]) >
                (UINT4) HTTP_CONNECTION_IDLE_TIMEOUT)
            {
                WebnmSendErrorPage (pHttp);
                return ENM_FAILURE;
            }
            if (((u4SysCurTime - u4PageTime) >
                 (UINT4) HTTP_CONNECTION_IDLE_TIMEOUT) &&
                ((u4SysCurTime - au4LastSentTime[i4Index]) >
                 (UINT4) HTTP_CONNECTION_IDLE_TIMEOUT))
            {
                WebnmSendErrorPage (pHttp);
                return ENM_FAILURE;
            }
            if (u4PageTime > au4LastSentTime[i4Index])
            {
                WebnmSendErrorPage (pHttp);
                return ENM_FAILURE;
            }

        }

        /* To verify the credentials */
        if ((pHttp->ai1Username[HTTP_ZERO] != '\0') &&
            (pHttp->ai1Password[HTTP_ZERO] != '\0'))
        {
            STRNCPY (au1WebnmLogin[i4Index],
                     pHttp->ai1Username, ENM_MAX_USERNAME_LEN);
            STRNCPY (au1WebnmPasswd[i4Index],
                     pHttp->ai1Password, ENM_MAX_PASSWORD_LEN);
            if (WebnmVerifyLogin (au1WebnmLogin[i4Index],
                                  au1WebnmPasswd[i4Index]) != ENM_SUCCESS)
            {
                WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                return ENM_FAILURE;
            }
        }
        else
        {
            if (u1FirstTimeFlag == HTTP_ONE)
            {
                WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                return ENM_FAILURE;
            }
            else
            {
                return ENM_SUCCESS;
            }
        }
        au4LastSentTime[i4Index] = WEBNM_GET_TIME ();
        SPRINTF ((CHR1 *) au1WebnmGambit[i4Index], "%d:%s:%s",
                 au4LastSentTime[i4Index],
                 au1WebnmLogin[i4Index], au1WebnmPasswd[i4Index]);
        WebnmEncode (au1WebnmGambit[i4Index]);
        return ENM_SUCCESS;
#endif
    }
    else
    {
        UINT4               u4ContextId;
        INT4                i4RetVal = CLI_SUCCESS;

        /* Get the context Id from page and store it sll node */
        STRCPY (pHttp->au1Name, "Context_MI");
        if ((HttpGetValuebyName
             (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery))
            == ENM_SUCCESS)
        {
            i4RetVal = VcmIsSwitchExist ((pHttp->au1Value), &u4ContextId);
            if (i4RetVal == VCM_TRUE)
            {
                gu4VcmContext = u4ContextId;
            }
        }

        STRCPY (pHttp->au1Name, "L3Context_MI");
        if ((HttpGetValuebyName
             (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery))
            == ENM_SUCCESS)
        {
            i4RetVal = VcmIsVrfExist ((pHttp->au1Value), &u4ContextId);
            if (i4RetVal == VCM_TRUE)
            {
                gu4VcmL3Context = u4ContextId;
            }
        }

        return ENM_SUCCESS;
    }
}

/************************************************************************
 *  Function Name   : WebnmSockWrite 
 *  Description     : Function to Write the Data to the client socket
 *  Input           : pHttp   - Pointer to the http entry where the socket
 *                    descriptor is stored
 *                    pi1Data - Pointer to the buffer to be written.
 *                    i4Len   - Size of the buffer in bytes
 *  Output          : None
 *  Returns         : Buffer Written Length in bytes
 ************************************************************************/

VOID
WebnmSockWrite (tHttp * pHttp, UINT1 *pData, INT4 i4Len)
{
    INT1               *pi1Start = NULL, *pi1End = NULL;
    INT4                i4Index = ENM_FAILURE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return;
    }

    pi1Start = (INT1 *) pData;
    while (((pi1End = (INT1 *) STRSTR (pi1Start, GAMBIT)) != NULL)
           && ((pi1End - pi1Start) < i4Len))
    {
        SockWrite (pHttp, pi1Start, (pi1End - pi1Start));
        i4Len -= (pi1End - pi1Start);
        i4Len -= (INT4) HTTP_STRLEN (GAMBIT);
        pi1Start = pi1End;
        pi1Start += (INT1) HTTP_STRLEN (GAMBIT);
        SockWrite (pHttp, (INT1 *) au1WebnmGambit[i4Index],
                   HTTP_STRLEN (au1WebnmGambit[i4Index]));
    }
    SockWrite (pHttp, pi1Start, i4Len);
}

#ifdef CLI_WANTED
/************************************************************************
 *  Function Name   : WebNMGetUserGroupsFromCli 
 *  Description     : This Will return the user level from user group. 
 *  Input           : u4UserLevel 
 *  Output          : None
 ************************************************************************/

INT4
WebNMGetUserGroupsFromCli (UINT4 *u4UserLevel)
{
    INT4                i4Count = HTTP_ZERO;
    INT1                ai1Name[ENM_MAX_NAME_LENGTH + HTTP_ONE];
    INT4                i4Index = ENM_FAILURE;
    tOsixTaskId         TaskId = HTTP_ZERO;

    if (OsixTskIdSelf (&TaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }

    if (HttpChildProcessIndex (TaskId, &i4Index) == ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    *u4UserLevel = HTTP_ZERO;
    for (i4Count = HTTP_ZERO; GroupArray[i4Count].pi1Name != NULL; i4Count++)
    {
        MEMSET (ai1Name, HTTP_ZERO, ENM_MAX_NAME_LENGTH + HTTP_ONE);
        STRNCPY (ai1Name, GroupArray[i4Count].pi1Name,
                 MEM_MAX_BYTES (ENM_MAX_NAME_LENGTH,
                                STRLEN (GroupArray[i4Count].pi1Name)));
        if (CliIsUserInGroup ((INT1 *) au1WebnmLogin[i4Index], ai1Name) ==
            CLI_SUCCESS)
        {
            *u4UserLevel = *u4UserLevel | GroupArray[i4Count].u4GroupId;
        }
    }
    return ENM_SUCCESS;
}
#endif
/*********************************************************************
 *  Function Name :WebPagePrivInit
 *  Description   : This function stores the privilege id  for 
 *                  each web page in the globally declared structure
 *  Input(s)      : None
 *  Output(s)     : u4PrivId
 *  Return Values : None
 *
 *********************************************************************/
VOID
WebPagePrivInit (VOID)
{
#ifdef CLI_WANTED
    INT4                i4IndexCount = 0, i4Count = 0;
    INT4                i4CurCmdPriv = 0;
    INT4                i4Retval = CLI_FAILURE;

    for (i4Count = 0; i4Count < (WEB_PRIV_MAX_LENGTH - 1); i4Count++)
    {

        if (gPagePrivMapping[i4Count].u2CliCmd != WEB_NO_COMMAND)
        {

            i4Retval =
                CliGetCommandPriv (giCliCmdMode
                                   [gPagePrivMapping[i4Count].u2CliCmdMode],
                                   giCliCommand[gPagePrivMapping[i4Count].
                                                u2CliCmd], &i4CurCmdPriv);
        }
        else
        {
            /*Putting highest privillage value if the url name does not have the cli commands and cli mode */
            i4CurCmdPriv = ATOI (CLI_ROOT_PRIVILEGE_ID);
        }
        if (CLI_SUCCESS == i4Retval)
        {
            if (gPagePrivMapping[i4Count].u2UrlName ==
                gPagePrivMapping[i4Count + 1].u2UrlName)
            {
                if (gPagePrivMapping[i4IndexCount].i2PrivId <=
                    (INT2) i4CurCmdPriv)
                {
                    gPagePrivMapping[i4IndexCount].i2PrivId =
                        (INT2) i4CurCmdPriv;
                }
            }
            else
            {
                if (gPagePrivMapping[i4IndexCount].i2PrivId <=
                    (INT2) i4CurCmdPriv)
                {
                    gPagePrivMapping[i4IndexCount].i2PrivId =
                        (INT2) i4CurCmdPriv;
                }
                i4IndexCount = i4Count + 1;
            }
        }
    }

#endif
}

#ifdef ISS_WANTED
/*********************************************************************
*  Function Name : IssVerifyLogin 
*  Description   : This will verify the Login Name and Passwd.
*  Parameter(s)  : Login name, password, current system time
*  Return Values : On failure -1, or sessionId on success
* *********************************************************************/

INT4
IssVerifyLogin (UINT1 *pi1Login, UINT1 *pi1Passwd, tHttp * pHttp,
                UINT4 u4Time, UINT1 u1FirstTime, UINT4 *pu4PageSessionId,
                UINT1 u1ReqType)
{
    static UINT4        u4SessionId = HTTP_ZERO;
    UINT4               u4ContextId;
    tWebnmUserNode     *pWebnmUserNode = NULL;
    INT4                i4RetVal = CLI_FAILURE;
    INT1                i1ServerStatus = ISS_FALSE;
    UINT4               u4Event = HTTP_ZERO;
    tOsixTaskId         TaskId = HTTP_ZERO;
    INT4                i4Index = ENM_FAILURE;
    UINT1               u1Index = HTTP_ZERO;
    UINT1               u1NonceMatch = HTTP_ZERO;
    UINT1               u1DigestMatch = HTTP_ZERO;
    tUtlSysPreciseTime  sysPreciseTime = { HTTP_ZERO, HTTP_ZERO, HTTP_ZERO };
    INT4                i4LoginAttempt = 0;

    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
#ifdef  WLC_WANTED
    UINT4               u4CliAddr = 0;
    UINT1               u1EncapType;
#endif

#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, MAC_ADDR_LEN);

    UNUSED_PARAM (u1ReqType);
    if (OsixTskIdSelf (&TaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    if (u1FirstTime)            /* User first time login */
    {
        if ((gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS)
            || (gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
        {
#ifdef  RADIUS_WANTED
#ifdef  WLC_WANTED
            /* Below Line has been added to solve coverity deadcode issue */
            i1ServerStatus = ISS_TRUE;
            /* To give the Station Mac to WSSRadiusAuthentication function 
             * Station IP Address is fetched using socket Id which is required
             * to get the MAC Address through ArpResolve API 
             */
            u4CliAddr = WebnmGetWebAuthClientInfo (pHttp->i4Sockid);
            /* Get the MAC address of the Client using ARP resolve */
            if (ArpResolve (u4CliAddr, (INT1 *) staMacAddr, &u1EncapType) !=
                OSIX_SUCCESS)
            {
                WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                return ENM_FAILURE;
            }
            i4RetVal = WEBNM_RADIUS_AUTHENTICATE_USER ((INT1 *) pi1Login,
                                                       (INT1 *) pi1Passwd,
                                                       TaskId, &i1ServerStatus,
                                                       staMacAddr, OSIX_FALSE);

            if (i4RetVal == ENM_FAILURE)
#endif
#endif
            {
                /* If the radius server is down and local authentication is enable
                 * then local authentication is done*/
                if ((i1ServerStatus == ISS_FALSE)
                    && (gu1IssLoginAuthMode ==
                        REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
                {
                    i4RetVal = WEBNM_AUTHENTICATE_USER ((INT1 *) pi1Login,
                                                        (INT1 *) pi1Passwd);

                    if (i4RetVal == CLI_FAILURE)
                    {
                        WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                        return ENM_FAILURE;
                    }
                }
                else
                {
                    WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                    return ENM_FAILURE;

                }
            }
        }
        else if ((gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS)
                 || (gu1IssLoginAuthMode ==
                     REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
        {
#ifdef TACACS_WANTED
            i4RetVal = WEBNM_TACACS_AUTHENTICATE_USER ((INT1 *) pi1Login,
                                                       (INT1 *) pi1Passwd,
                                                       TaskId, &i1ServerStatus);
            if (i4RetVal == ENM_FAILURE)
#endif /* TACACS_WANTED */
            {
                /* If the tacacs server is down and local authentication is enable
                 * then local authentication is done*/
                if ((i1ServerStatus == ISS_FALSE)
                    && (gu1IssLoginAuthMode ==
                        REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
                {
                    i4RetVal = WEBNM_AUTHENTICATE_USER ((INT1 *) pi1Login,
                                                        (INT1 *) pi1Passwd);

                    if (i4RetVal == CLI_FAILURE)
                    {
                        WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                        return ENM_FAILURE;
                    }
                }
                else
                {
                    WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                    return ENM_FAILURE;

                }
            }
        }
        if ((((gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS)
              || (gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
             && (i1ServerStatus == ISS_TRUE)) ||
            (((gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS)
              || (gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
             && (i1ServerStatus == ISS_TRUE)))
        {
            while (HTTP_ONE)
            {
                if (OsixEvtRecv (pHttp->ProcessingTaskId, (WEBNM_REJECT_EVENT |
                                                           WEBNM_ACCEPT_EVENT),
                                 OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
                {
                    if ((u4Event & WEBNM_REJECT_EVENT) == WEBNM_REJECT_EVENT)
                    {
                        WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                        return ENM_FAILURE;
                    }
                    if ((u4Event & WEBNM_ACCEPT_EVENT) == WEBNM_ACCEPT_EVENT)
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            /* Authentication using Basic/Default */
            if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
                (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT))
            {
                /* checking that the entered user is correct user or not */
                if (FpamGetLoginAttempts ((CHR1 *) pi1Login,
                                          &i4LoginAttempt) == OSIX_SUCCESS)
                {
#ifdef CLI_WANTED
                    if (CliUtilCheckAllowUserLogin ((CHR1 *) pi1Login) !=
                        CLI_SUCCESS)
                    {
                        WebnmSendLoginErrorPage (pHttp, WEBNM_USER_BLOCKED);

                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                                      "WEBNM: User is blocked, Login Not Allowed"));
                        return ENM_FAILURE;
                    }
#endif
                }
                i4RetVal =
                    WEBNM_AUTHENTICATE_USER ((INT1 *) pi1Login,
                                             (INT1 *) pi1Passwd);
            }
            /* Authentication using Digest */
            else if (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST)
            {
                i4RetVal = CLI_FAILURE;
                u1NonceMatch = HTTP_ZERO;
                u1DigestMatch = HTTP_ZERO;

                for (u1Index = HTTP_ZERO;
                     u1Index < HTTP_MAX_CLIENT_CONNECTIONS; u1Index++)
                {
                    /* Check if the client has returned the same Nonce
                     * given by the server */
                    if (STRCMP (pHttp->au1Snonce,
                                gaHttpServerNonceInfo[u1Index].au1Nonce) ==
                        HTTP_ZERO)
                    {
                        /* check if the nonce has not expired */
                        UtlGetPreciseSysTime (&sysPreciseTime);

                        if ((INT4) (sysPreciseTime.u4Sec -
                                    gaHttpServerNonceInfo[u1Index].
                                    u4TimeInSecs) < HTTP_NONCE_EXPIRE_TIME)
                        {
                            u1NonceMatch = HTTP_ONE;
                        }

                        /* If the Nonces match (even if it has expired), 
                         * clear the value from the global buffer */
                        MEMSET (&gaHttpServerNonceInfo[u1Index], HTTP_ZERO,
                                sizeof (tHttpServerNonceInfo));

                        break;
                    }
                }

                /* Compare the request digest sent by the client
                 * with the digest generated by the server */
                if (STRCMP (pHttp->au1Digest,
                            pHttp->au1RequestDigest) == HTTP_ZERO)
                {
                    u1DigestMatch = HTTP_ONE;
                    i4RetVal = CLI_SUCCESS;
                }

                if (u1NonceMatch == HTTP_ZERO)
                {
                    ENM_TRACE ("WEBNM: The Nonce sent \
                            by the client is INVALID");

                    pHttp->u2RespCode = HTTP_UNAUTHORIZED;

                    if (STRCMP (pHttp->ai1Url, gaWEBNM_RELOGIN_PAGE) ==
                        HTTP_ZERO)
                    {
                        STRNCPY (pHttp->au1Realm,
                                 gaHTTP_REALM_TIMEOUT, ENM_MAX_REALM_LEN);
                    }
                    else
                    {
                        STRNCPY (pHttp->au1Realm,
                                 gaHTTP_REALM_ISS, ENM_MAX_REALM_LEN);
                    }

                    if (u1DigestMatch == HTTP_ONE)
                    {
                        STRNCPY (pHttp->au1Stale,
                                 gaHTTP_TRUE, ENM_MAX_STALE_LEN);
                    }

                    HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
                    return ENM_FAILURE;
                }

                if (u1DigestMatch == HTTP_ZERO)
                {
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                                  "WEBNM: The RequestDigest sent by the client is INVALID"));
                }

            }

            if (i4RetVal == CLI_FAILURE)
            {
                /*
                 * Get the count of wrong login attempts*/
                if (FpamGetLoginAttempts ((CHR1 *) pi1Login,
                                          &i4LoginAttempt) == FPAM_SUCCESS)
                {
                    /*
                     * Comare the login attemts with blocked login attempts*/
#ifdef ISS_WANTED
                    if (i4LoginAttempt >= IssGetLoginAttempts ())
                    {
#endif
#ifdef CLI_WANTED
                        /*
                         *Blocks the user if condition is satisfies*/
                        if (CliUtilCheckBlockUserOrNot ((CHR1 *) pi1Login) ==
                            CLI_FAILURE)
                        {
                            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                                          "WEBNM: Attempt to block the user failed"));
                        }
                        else
                        {        /*
                                 *reset the Login attempts when user blocked*/
                            FpamSetLoginAttempts ((CHR1 *) pi1Login,
                                                  FPAM_RESET_LOGIN_ATTEMPT);
                        }
#endif
                    }
                }
                WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                              "WEBNM: Attempt to login with wrong credentials"));
                return ENM_FAILURE;
            }
        }

        /* new session accept this login */
        pWebnmUserNode = (tWebnmUserNode *)
            MemAllocMemBlk (gHttpInfo.WebnmUserNodePoolId);
        if (pWebnmUserNode == NULL)
        {
            /*WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM", "user node allocation failed. Current number of 
               users logged in: %d\n", WebnmUserRecord.UserList.u4_Count); */
            return ENM_FAILURE;
        }
        pWebnmUserNode->u4AccessTime = u4Time;
        pWebnmUserNode->u4SessionId = ++u4SessionId;
        HTTP_LOCK ();
        if (WebNMAddUserEntry (pWebnmUserNode) == ENM_FAILURE)
        {
            MemReleaseMemBlock (gHttpInfo.WebnmUserNodePoolId,
                                (UINT1 *) pWebnmUserNode);
            u4SessionId--;
            WebnmSendLogin (pHttp, HTTP_FOUR);
            FpamUtlUpdateActiveUsers ((CHR1 *) pi1Login, HTTP_ZERO);
            HTTP_UNLOCK ();
            return ENM_FAILURE;
        }
        HTTP_UNLOCK ();
        *pu4PageSessionId = u4SessionId;
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                      "WEBNM: Successfully logged as User - %s ", pi1Login));
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
            {
                IpAddress.u4Addr =
                    gIpv4Sec[u4Counter].ClientAddr.sin_addr.s_addr;
                pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
            }
        }
#ifdef IP6_WANTED
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
            {
                MEMCPY (In6Addr.u1addr,
                        gIpv6Sec[u4Counter].ClientV6Addr.sin6_addr.s6_addr,
                        sizeof (In6Addr.u1addr));
                pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
            }
        }
#endif
        if (pHttp->u1RequestType == HTTP_ONE)
        {
            WEB_SESSION_TRACE1 (MGMT_TRC, "WEBNM",
                                "Login successful from %s", pu1String);
        }

        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            WEBNM_TEST_CREATE_TEST_NODE (pHttp->i4AuthScheme,
                                         pHttp->ProcessingTaskId, u4SessionId);
        }
        KW_FALSEPOSITIVE_FIX (pWebnmUserNode);
        return ENM_SUCCESS;
    }
    else
    {
        if ((pWebnmUserNode = WebNMCheckUserEntry (*pu4PageSessionId)) != NULL)
        {
            /* Valid user, check for time-out */
            if (((u4Time - pWebnmUserNode->u4AccessTime) >
                 (UINT4) HTTP_CONNECTION_IDLE_TIMEOUT)
                && (WEBNM_HTTP_TIME_OUT == OSIX_TRUE))
            {
                HTTP_LOCK ();
                WebNMDeleteUserEntry (*pu4PageSessionId);
                WEBNM_TEST_DELETE_SESSION_ENTRY (*pu4PageSessionId);
                HTTP_UNLOCK ();
                WebnmSendErrorPage (pHttp);
                return ENM_FAILURE;
            }
            else
            {
                pWebnmUserNode->u4AccessTime = u4Time;
                /* Get the context Id from page and store it sll node */
                STRCPY (pHttp->au1Name, "Context_MI");
                if ((HttpGetValuebyName
                     (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery))
                    == ENM_SUCCESS)
                {
                    i4RetVal = VcmIsSwitchExist ((pHttp->au1Value),
                                                 &u4ContextId);
                    if (i4RetVal == VCM_TRUE)
                    {
                        pWebnmUserNode->u4VcmContext = u4ContextId;
                    }
                }

                STRCPY (pHttp->au1Name, "L3Context_MI");
                if ((HttpGetValuebyName
                     (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery))
                    == ENM_SUCCESS)
                {
                    issDecodeSpecialChar (pHttp->au1Value);
                    i4RetVal = VcmIsVrfExist ((pHttp->au1Value), &u4ContextId);
                    if (i4RetVal == VCM_TRUE)
                    {
                        pWebnmUserNode->u4VcmL3Context = u4ContextId;
                    }
                }

                return ENM_SUCCESS;
            }
        }
        else
        {
            /* In valid session, send time-out */
            WebnmSendErrorPage (pHttp);
            return ENM_FAILURE;
        }
    }
}

/*********************************************************************
*  Function Name : WebnmCheckUserPasswd
*  Description   : This will verify the Login Name and Passwd.
*  Parameter(s)  : Login name, password, current system time
*  Return Values : On failure 0, 1 on success
* *********************************************************************/

INT1
WebnmCheckUserPasswd (INT1 *pi1UserName, INT1 *pi1UserPasswd)
{
    INT2                i2Count;
    INT2                i2LenName = HTTP_ZERO;
    INT2                i2LenPass = HTTP_ZERO;

    for (i2Count = HTTP_ZERO;
         gaWebnmUser[i2Count].pi1Name[0] != '\0'; i2Count++)
    {
        i2LenName = (INT2) STRLEN (gaWebnmUser[i2Count].pi1Name);
        i2LenPass = (INT2) STRLEN (gaWebnmUser[i2Count].pi1Passwd);

        if ((STRNCMP (gaWebnmUser[i2Count].pi1Name,
                      pi1UserName, i2LenName)
             == HTTP_ZERO) &&
            (STRNCMP (gaWebnmUser[i2Count].pi1Passwd,
                      pi1UserPasswd, i2LenPass) == HTTP_ZERO))
        {
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;
}

/*********************************************************************
*  Function Name : IssAuthenticate
*  Description   : This will Authenticate User Name and Passwd.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                  descriptor is stored
*  Output(s)     : None. 
*  Return Values : None.
*********************************************************************/
INT1
IssAuthenticate (tHttp * pHttp, UINT1 u1FirstTimeFlag)
{
    UINT4               u4PageTime = HTTP_ZERO;
    UINT4               u4SessionId = HTTP_ZERO;
    static UINT1        au1Temp[ENM_MAX_NAME_LEN] = { HTTP_ZERO };
    CHR1               *pc1Username = NULL;
    CHR1               *pc1Password = NULL;
    UINT4               u4SysCurTime = HTTP_ZERO;
    INT4                i4Index = ENM_FAILURE;
    INT4                i4PageCount = HTTP_ZERO;
    INT4                i4RetVal = CLI_FAILURE;
#ifdef WLC_WANTED
    UINT4               u4CliAddr = 0;
    UINT1               au1DstMac[MAC_ADDR_LEN];
    UINT1               u1EncapType;
    tMacAddr            CliSrcMacAddr;

    MEMSET (au1DstMac, 0, sizeof (tMacAddr));
    MEMSET (CliSrcMacAddr, 0, sizeof (tMacAddr));
#endif

    /* URL Name array that contains the list of URL names that are
     * loaded initially before login. More URLs can be added to this
     * array in case of any issues with login/gambit */
    CHR1               *pai1LoginUrlNames[] = { "redirect.html", NULL };

    /* URL Name array that contains the list of URL names that are
     * loaded during login. More URLs can be added to this
     * array in case of any issues with login/gambit */
    CHR1               *pai1UrlNames[] =
        { "dummy.html", "BrandingFrame.htm", "ActiveObjectFrame.htm",
        "tabbg1.gif", "tab_sr.gif",
        "logo.jpg", "gnavbg.gif", "logo_bg.jpg", "plus.gif", "tab_sl.gif",
        "tab_smr.gif", "tab_sml.gif",
        "join.gif", "page.gif", "Styles.css", "green.gif", "red.gif",
        "tab_m.gif", "tab_l.gif", "tab_r.gif",
        "minus.gif", "greenup.gif", "logo1_bg.jpg", "style.css", "top.html",
        "script.js", "tabbg.gif", "reddown.gif", "wlan_web_auth_login.html",
        NULL
    };

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    MEMSET (au1Temp, HTTP_ZERO, ENM_MAX_NAME_LEN);

    /* Not authenticating top.html. */
    if (STRCMP (pHttp->ai1HtmlName, "top.html") == HTTP_ZERO)
    {
        return ENM_SUCCESS;
    }

    u4SysCurTime = WEBNM_GET_TIME ();

    /* For Default Authentication scheme,
     * the username and password is extracted 
     * 1. from the submitted redirect.html page at the time of login
     * 2. from the gambit for the rest of the pages */
    if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
    {
        if ((STRCMP (pHttp->ai1Url, gaWEBNM_REDIRECT_PAGE)) == HTTP_ZERO)
        {
            u1FirstTimeFlag = HTTP_ONE;
            HttpGetValuebyName (WEBNM_LOGIN, au1WebnmLogin[i4Index],
                                pHttp->au1PostQuery);
            HttpGetValuebyName (WEBNM_PASSWD, au1WebnmPasswd[i4Index],
                                pHttp->au1PostQuery);
        }
    }
    /* For Basic and Digest auth schemes, copy the
     * username and password from pHttp structure */
    else
    {
        STRNCPY (au1WebnmLogin[i4Index], pHttp->ai1Username,
                 ENM_MAX_USERNAME_LEN);
        STRNCPY (au1WebnmPasswd[i4Index], pHttp->ai1Password,
                 ENM_MAX_PASSWORD_LEN);
    }

    /* Verify the GAMBIT and authenticate all the client requests
     * carrying gambit. */
    if (HttpGetValuebyName (WEBNM_GAMBIT, au1WebnmGambit[i4Index],
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        WebnmDecode (au1WebnmGambit[i4Index]);

        SSCANF ((char *) au1WebnmGambit[i4Index], "%u:%u:%1023s",
                &u4SessionId, &u4PageTime, au1Temp);

        /* Extract the credentials from gambit for 
         * default auth scheme */
        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            pc1Username = STRTOK_R (au1Temp, ":", &pc1Password);
            if ((pc1Username == NULL) || (pc1Password == NULL))
            {
                ENM_TRACE ("Unable to Process Gambit\n");
                WebnmSendErr (pHttp,
                              (INT1 *) "ERROR: Session timed-out or in-valid");
                return ENM_FAILURE;
            }
            HTTP_STRCPY (au1WebnmLogin[i4Index], pc1Username);
            HTTP_STRCPY (au1WebnmPasswd[i4Index], pc1Password);
        }

        /* decode special characters to allow usage in
         * usernames and passwords
         */
        issDecodeSpecialChar (au1WebnmLogin[i4Index]);
        issDecodeSpecialChar (au1WebnmPasswd[i4Index]);

        /*this username entry is used in syslog message */
        HTTP_STRCPY (gu1WebnmUserName, au1WebnmLogin[i4Index]);
        HTTP_STRCPY (pHttp->ai1Username, au1WebnmLogin[i4Index]);

        /* Authentication using Basic/Default */
        if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
            (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT))
        {
            if (LOCAL_LOGIN == gu1IssLoginAuthMode)
            {
                i4RetVal =
                    WEBNM_AUTHENTICATE_USER ((INT1 *) au1WebnmLogin[i4Index],
                                             (INT1 *) au1WebnmPasswd[i4Index]);
            }
            else
            {
                /* Only for local database auth, every page post will be 
                 * authenticated. For remote (radius/tacacs) auth only the
                 * initial login is authenticated */
                i4RetVal = CLI_SUCCESS;
            }
        }
        /* Authentication using Digest */
        else
        {
            /* Compare the request digest sent by the client
             * with the digest generated by the server */
            if (STRCMP (pHttp->au1Digest, pHttp->au1RequestDigest) == HTTP_ZERO)
            {
                i4RetVal = CLI_SUCCESS;
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                ENM_TRACE
                    ("WEBNM: The RequestDigest sent by the client is INVALID\n");
            }
        }

        if (i4RetVal == CLI_FAILURE)
        {
            ENM_TRACE ("WEBNM: Attempt to access the page with \
            incorrect credentials \n");
            WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
            return ENM_FAILURE;
        }

        if (IssVerifyLogin (au1WebnmLogin[i4Index], au1WebnmPasswd[i4Index],
                            pHttp, u4SysCurTime, u1FirstTimeFlag,
                            &u4SessionId, pHttp->u1RequestType) == ENM_FAILURE)
        {
            return ENM_FAILURE;
        }

        if ((pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT) && (pc1Password != NULL))
        {
            HTTP_STRCPY (au1WebnmPasswd[i4Index], pc1Password);
        }
        else
        {
            STRNCPY (au1WebnmPasswd[i4Index], pHttp->ai1Password,
                     ENM_MAX_PASSWORD_LEN);
        }
    }

    /* Verify the credentials for the first login attempt */
    else if ((u1FirstTimeFlag == HTTP_ONE)
             || (STRSTR (pHttp->ai1Url, pai1LoginUrlNames[HTTP_ZERO])))
    {
        if ((au1WebnmLogin[i4Index][HTTP_ZERO] == '\0') ||
            (au1WebnmPasswd[i4Index][HTTP_ZERO] == '\0'))
        {
            return ENM_FAILURE;
        }

        /* decode special characters to allow usage in
         * usernames and passwords
         */
        issDecodeSpecialChar (au1WebnmLogin[i4Index]);
        issDecodeSpecialChar (au1WebnmPasswd[i4Index]);

        if (IssVerifyLogin (au1WebnmLogin[i4Index], au1WebnmPasswd[i4Index],
                            pHttp, u4SysCurTime, u1FirstTimeFlag,
                            &u4SessionId, pHttp->u1RequestType) == ENM_FAILURE)
        {
            return ENM_FAILURE;
        }

        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            HttpGetValuebyName (WEBNM_PASSWD, au1WebnmPasswd[i4Index],
                                pHttp->au1PostQuery);
        }
        else
        {
            STRNCPY (au1WebnmPasswd[i4Index], pHttp->ai1Password,
                     ENM_MAX_PASSWORD_LEN);
        }
    }
    else
    {
        /* This portion of code is invoked during login process. 
         * 1) If any of the URLS in pai1UrlNames match with the incoming
         * URL name , then gambit is obtained for the page, 
         *
         * 2) If there is no match, it means the user is trying to access the page
         * without proper credentials - Appropriate Error message is
         * displayed and failure is returned */

        for (i4PageCount = 0; pai1UrlNames[i4PageCount] != NULL; i4PageCount++)
        {
            if (STRSTR (pHttp->ai1Url, pai1UrlNames[i4PageCount]) != NULL)
            {
                SNPRINTF ((CHR1 *) au1WebnmGambit[i4Index], ENM_MAX_NAME_LEN,
                          "%d:%d:%s:%s",
                          u4SessionId,
                          u4SysCurTime, au1WebnmLogin[i4Index],
                          au1WebnmPasswd[i4Index]);
                WebnmEncode (au1WebnmGambit[i4Index]);
                return ENM_SUCCESS;
            }
        }

#ifdef WLC_WANTED
        u4CliAddr = WebnmGetWebAuthClientInfo (pHttp->i4Sockid);
        /* Get the MAC address of the Client using ARP resolve */
        if (ArpResolve (u4CliAddr, (INT1 *) au1DstMac, &u1EncapType) !=
            OSIX_SUCCESS)
        {
            ENM_TRACE ("WEBNM: Attempt to access the page with \
            incorrect credentials\n");
            WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
            return ENM_FAILURE;
        }
        MEMCPY (CliSrcMacAddr, au1DstMac, sizeof (tMacAddr));
        if (IssAuthenticateRedirectFile (pHttp, CliSrcMacAddr) == ENM_SUCCESS)
        {
            SNPRINTF ((CHR1 *) au1WebnmGambit[i4Index], ENM_MAX_NAME_LEN,
                      "%d:%d:%s:%s",
                      u4SessionId,
                      u4SysCurTime, au1WebnmLogin[i4Index],
                      au1WebnmPasswd[i4Index]);
            WebnmEncode (au1WebnmGambit[i4Index]);
            return ENM_SUCCESS;
        }
#endif
        /* If none of the URLS in pai1UrlNames match with the incoming
         * URL name, it means the user is trying to access the page
         * without proper credentials - Appropriate Error message is
         * displayed and failure is returned */

        ENM_TRACE ("WEBNM: Attempt to access the page with \
                    incorrect credentials\n");
        WebnmSendLoginErrorPage (pHttp, WEBNM_AUTH_FAILED);
        return ENM_FAILURE;
    }

    SNPRINTF ((CHR1 *) au1WebnmGambit[i4Index], ENM_MAX_NAME_LEN,
              "%d:%d:%s:%s",
              u4SessionId,
              u4SysCurTime, au1WebnmLogin[i4Index], au1WebnmPasswd[i4Index]);
    WebnmEncode (au1WebnmGambit[i4Index]);
    return ENM_SUCCESS;
}

#ifdef WLC_WANTED
/*********************************************************************
*  Function Name : IssAuthenticateRedirectFile
*  Description   : This will function will re-initialse the session
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                  descriptor is stored
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
INT1
IssAuthenticateRedirectFile (tHttp * pHttp, tMacAddr CliSrcMacAddr)
{
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT1               au1ClientMac[MAC_ADDR_LEN];
    tWssWlanDB          wssWlanDB;
    UINT4               i4Length = 0;
    UINT1               au1FileName[32];

    MEMSET (au1FileName, 0, 32);
    MEMSET (au1ClientMac, 0, sizeof (MAC_ADDR_LEN));
    MEMSET (au1ClientMac, 0, sizeof (MAC_ADDR_LEN));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMCPY (au1ClientMac, CliSrcMacAddr, sizeof (tMacAddr));

    pWssStaWepProcessDB = WssStaProcessEntryGet (au1ClientMac);
    if (pWssStaWepProcessDB != NULL)
    {
        /* Retrivie the profile Index from the local database */
        if (WssIfGetProfileIfIndex (pWssStaWepProcessDB->u4BssIfIndex,
                                    &u4ProfileIndex) != OSIX_FAILURE)
        {
            wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = u4ProfileIndex;
            wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bFsDot11RedirectFileName = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) != OSIX_FAILURE)
            {
                if (wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus ==
                    WSSWLAN_ENABLE)
                {
                    pWssifauthDBMsgStruct =
                        (tWssifauthDBMsgStruct *) (VOID *)
                        UtlShMemAllocWssIfAuthDbBuf ();
                    if (pWssifauthDBMsgStruct == NULL)
                    {
                        return ENM_FAILURE;
                    }
                    MEMSET (pWssifauthDBMsgStruct, 0,
                            sizeof (tWssifauthDBMsgStruct));

                    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.
                            stationMacAddress, au1ClientMac, MAC_ADDR_LEN);

                    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                                  pWssifauthDBMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pWssifauthDBMsgStruct);
                        return ENM_FAILURE;
                    }
                    if (pWssifauthDBMsgStruct->WssIfAuthStateDB.
                        u1StaAuthFinished != OSIX_TRUE)
                    {
                        i4Length =
                            STRLEN (wssWlanDB.WssWlanAttributeDB.
                                    au1FsDot11RedirectFileName);
                        MEMCPY (au1FileName,
                                wssWlanDB.WssWlanAttributeDB.
                                au1FsDot11RedirectFileName, i4Length);

                        if (STRCMP (pHttp->ai1HtmlName, au1FileName) == 0)
                        {
                            if (pHttp->i4Method == ISS_GET)
                            {
                                IssProcessWebAuthLoginPageGet (pHttp);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pWssifauthDBMsgStruct);
                                return ENM_SUCCESS;
                            }
                            else if (pHttp->i4Method == ISS_SET)
                            {
                                IssProcessWebAuthLoginPageSet (pHttp);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pWssifauthDBMsgStruct);
                                return ENM_SUCCESS;
                            }
                        }
                        else
                        {
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        pWssifauthDBMsgStruct);
                            return ENM_FAILURE;
                        }
                    }
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pWssifauthDBMsgStruct);
                    return ENM_SUCCESS;
                }
            }
        }
    }
    return ENM_FAILURE;
}
#endif

/*********************************************************************
*  Function Name : IssLogout
*  Description   : This will function will re-initialse the session
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                  descriptor is stored
*  Output(s)     : None. 
*  Return Values : None.
*********************************************************************/
VOID
IssLogout (tHttp * pHttp)
{
    tUtlInAddr          IpAddress;
    INT1                i1RetVal = ENM_FAILURE;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif
    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
        UINT4               u4PageTime = HTTP_ZERO;
        UINT4               u4SessionId = HTTP_ZERO;
        static UINT1        au1Temp[ENM_MAX_NAME_LEN] = { HTTP_ZERO };
        UINT1              *pu1Temp = NULL;
        INT4                i4Index = ENM_FAILURE;

        MEMSET (au1Temp, HTTP_ZERO, ENM_MAX_NAME_LEN);

        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
            {
                IpAddress.u4Addr =
                    gIpv4Sec[u4Counter].ClientAddr.sin_addr.s_addr;
                pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
            }
        }
#ifdef IP6_WANTED
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
            {
                MEMCPY (In6Addr.u1addr,
                        gIpv6Sec[u4Counter].ClientV6Addr.sin6_addr.s6_addr,
                        sizeof (In6Addr.u1addr));
                pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
            }
        }
#endif

        if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
            ENM_FAILURE)
        {
            ENM_TRACE ("Invalid Index for the given Task Id\n");
            return;
        }
        if (HttpGetValuebyName (WEBNM_GAMBIT, au1WebnmGambit[i4Index],
                                pHttp->au1PostQuery) != ENM_SUCCESS)
        {
            WebnmSendErr (pHttp,
                          (INT1 *) "ERROR: Session timed-out or in-valid");
            if (pHttp->u1RequestType == HTTP_ONE)
            {
                WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                                    "Session terminated due to timeout for %s",
                                    pu1String);
            }
            return;
        }
        WebnmDecode (au1WebnmGambit[i4Index]);

        SSCANF ((char *) au1WebnmGambit[i4Index], "%u:%u:%1023s",
                &u4SessionId, &u4PageTime, au1Temp);
        pu1Temp = (UINT1 *) STRTOK (au1Temp, ":");
        if (pu1Temp == NULL)
        {
            ENM_TRACE ("Unable to Process Gambit\n");
            WebnmSendErr (pHttp,
                          (INT1 *) "ERROR: Session timed-out or in-valid");
            return;
        }
        HTTP_STRCPY (au1WebnmLogin[i4Index], pu1Temp);
        HTTP_LOCK ();
        i1RetVal = WebNMDeleteUserEntry (u4SessionId);
        WEBNM_TEST_DELETE_SESSION_ENTRY (u4SessionId);
        HTTP_UNLOCK ();

        /* Logout handling based on the 
         * authentication scheme */
        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            WebnmSendLogin (pHttp, HTTP_ZERO);
        }
        else
        {
            WebnmSendLogout (pHttp);
        }
        /* Avoid duplicate syslog message, if logout already 
           occurred and user database is not present */
        if (i1RetVal == ENM_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4WebSysLogId,
                          "WEBNM: User %s successfuly logged out",
                          au1WebnmLogin[i4Index]));
        }
        if (pHttp->u1RequestType == HTTP_ONE)
        {
            WEB_SESSION_TRACE1 (MGMT_TRC, "WEBNM",
                                "Session logout successful for %s", pu1String);
        }
        return;
    }
    else
    {
        WebnmSockWrite (pHttp, WEBNM_HOMEPAGE, HTTP_STRLEN (WEBNM_HOMEPAGE));
        return;
    }
}

/*********************************************************************
*  Function Name : IssSendLoginPage
*  Description   : This will append the corresponding login Error Message
*                     with the login page.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                  descriptor is stored
*                   : i1Message - Error Message attached with login page.
*  Output(s)     : None. 
*  Return Values : None 
* *********************************************************************/
VOID
IssSendLoginPage (tHttp * pHttp, INT1 i1Message)
{
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif
    CHR1                au1BannerMsg[WEBNM_MAX_BANNER_LEN];
    UINT4               u4BytesFilled = 0;
    INT4                i4FileId = -1;
    UINT1              *pu1FormatChar = (UINT1 *) WEBNM_BANNER_LINE_BREAK;
    UINT1               u1FormatCharLen = 0;
    UINT1               u1TempSize = 0;
    INT4                i4BytesRead = 0;
    MEMSET (au1BannerMsg, 0, WEBNM_MAX_BANNER_LEN);
    u1FormatCharLen = STRLEN (pu1FormatChar);

    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
        {
            IpAddress.u4Addr = gIpv4Sec[u4Counter].ClientAddr.sin_addr.s_addr;
            pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
        }
    }
#ifdef IP6_WANTED
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
        {
            MEMCPY (In6Addr.u1addr,
                    gIpv6Sec[u4Counter].ClientV6Addr.sin6_addr.s6_addr,
                    sizeof (In6Addr.u1addr));
            pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
        }
    }
#endif

    STRCPY (pHttp->au1KeyString, "<!ERROR_MESSAGE>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    switch (i1Message)
    {
        case 1:
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     "ERROR: Unknown User or wrong password");
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                          "WEBNM: Attempt to Login with Wrong Password"));
            if (pHttp->u1RequestType == HTTP_ONE)
            {
                WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                                    "Failed to Login due to wrong password from %s",
                                    pu1String);
            }
            break;
        case 2:
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     "ERROR: Session timed-out");
            break;
        case 3:
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     "ERROR: User already logged in...");
            break;
        case 4:
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WEBNM_SESSION_ERROR);
            if (pHttp->u1RequestType == HTTP_ONE)
            {
                WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                                    "Session failed from %s due to Max session threshold reached",
                                    pu1String);
            }
            break;
        case 5:
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     "ERROR: User is blocked, Login Not Allowed");
            break;
        default:
            break;

    }
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<!DEFAULT_MESSAGE>");

    WebnmSendString (pHttp, pHttp->au1KeyString);

    if ((i4FileId = FileOpen ((CONST UINT1 *) FLASH ISS_BANNER_FILE_NAME,
                              (OSIX_FILE_RO))) >= 0)
    {
        MEMCPY (au1BannerMsg + u4BytesFilled, pu1FormatChar, u1FormatCharLen);
        u4BytesFilled += u1FormatCharLen;
        while (u4BytesFilled < WEBNM_MAX_BANNER_LEN - 1)
        {
            i4BytesRead = FileRead (i4FileId, au1BannerMsg + u4BytesFilled, 1);
            if (i4BytesRead <= 0)
            {
                break;
            }
            else
            {
                au1BannerMsg[u4BytesFilled + i4BytesRead] = '\0';
            }

            if (au1BannerMsg[u4BytesFilled] == '\n')
            {
                u1TempSize = MEM_MAX_BYTES (u1FormatCharLen,
                                            WEBNM_MAX_BANNER_LEN - 1 -
                                            u4BytesFilled);
                MEMCPY (au1BannerMsg + u4BytesFilled, pu1FormatChar,
                        u1TempSize);
                u4BytesFilled += u1TempSize - 1;
            }
            u4BytesFilled++;
        }
        if (u4BytesFilled < WEBNM_MAX_BANNER_LEN - 1)
            au1BannerMsg[u4BytesFilled] = '\0';
        FileClose (i4FileId);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s %s",
                 "WARNING:", au1BannerMsg);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}
#endif

/*********************************************************************
*  Function Name : WebNmInitUserRecord
*  Description   : This will initialize the User Record Structure.
*  Input(s)      : None
*  Output(s)     : None 
*  Return Values : None 
***********************************************************************/

VOID
WebNmInitUserRecord ()
{
    WEBNM_SLL_INIT (&WebnmUserRecord.UserList);
}

/************************************************************************
 *  Function Name   : WebNMAddUserEntry 
 *  Description     : This function adds an entry into the UserList.
 *  Input           : pWebnmUserNode
 *  Output          : None
 *  Return Values     : ENM_SUCCESS/ENM_FAILURE 
 ************************************************************************/

INT1
WebNMAddUserEntry (tWebnmUserNode * pWebnmUserNode)
{
    if (WebnmUserRecord.UserList.u4_Count >= (UINT4) (HTTP_MAX_LISTEN - 1))
    {
        WebNMCleanEntries (pWebnmUserNode->u4AccessTime);
    }

    pWebnmUserNode->u4VcmContext = L2IWF_DEFAULT_CONTEXT;
    pWebnmUserNode->u4VcmL3Context = VCM_DEFAULT_CONTEXT;
    if (WebnmUserRecord.UserList.u4_Count < (UINT4) HTTP_MAX_LISTEN)
    {
        WEBNM_SLL_ADD (&WebnmUserRecord.UserList,
                       (tTMO_SLL_NODE *) & (pWebnmUserNode->NextNode));
        return ENM_SUCCESS;
    }
    return ENM_FAILURE;
}

/************************************************************************
 *  Function Name   : WebNMDeleteUserEntry 
 *  Description     : This function deletes an entry from the UserList.
 *  Input           : u4SessionId
 *  Output          : None
 *  Return Values     : ENM_SUCCESS/ENM_FAILURE 
 ************************************************************************/

INT1
WebNMDeleteUserEntry (UINT4 u4SessionId)
{
    tWebnmUserNode     *pWebnmUserNode = NULL;
    tWebnmUserNode     *pTemp = NULL;
    INT1                i1RetVal = ENM_FAILURE;

    UTL_SLL_OFFSET_SCAN (&WebnmUserRecord.UserList, pWebnmUserNode,
                         pTemp, tWebnmUserNode *)
    {
        if (pWebnmUserNode->u4SessionId == u4SessionId)
        {
            WEBNM_SLL_DEL (&WebnmUserRecord.UserList,
                           (tTMO_SLL_NODE *) & (pWebnmUserNode->NextNode));
            MemReleaseMemBlock (gHttpInfo.WebnmUserNodePoolId,
                                (UINT1 *) pWebnmUserNode);
            i1RetVal = ENM_SUCCESS;
            break;
        }
    }
    return (i1RetVal);
}

/************************************************************************
 * 
 *  Function Name   : WebNMCheckUserEntry 
 *  Description     : This function checks whether an entry exists in the 
 *                    UserList.
 *  Input           : u4SessionId
 *  Output          : None
 *  Return Values     : ENM_SUCCESS/ENM_FAILURE 
 ************************************************************************/

tWebnmUserNode     *
WebNMCheckUserEntry (UINT4 u4SessionId)
{
    tWebnmUserNode     *pWebnmUserNode = NULL;
    tWebnmUserNode     *pTemp = NULL;

    UTL_SLL_OFFSET_SCAN (&WebnmUserRecord.UserList, pWebnmUserNode,
                         pTemp, tWebnmUserNode *)
    {
        if (pWebnmUserNode->u4SessionId == u4SessionId)
        {
            return pWebnmUserNode;
        }
    }
    return NULL;
}

/************************************************************************
 * 
 *  Function Name   : WebNMCleanEntries
 *  Description     : This function deletes all User Entries for which the Last
 *                    Access Time is greater than the WEBNM Session TimOut.
 *  Input           : u4Time
 *  Output          : None
 ************************************************************************/

VOID
WebNMCleanEntries (UINT4 u4Time)
{
    tWebnmUserNode     *pWebnmUserNode = NULL;
    tWebnmUserNode     *pTemp = NULL;

    UTL_SLL_OFFSET_SCAN (&WebnmUserRecord.UserList, pWebnmUserNode,
                         pTemp, tWebnmUserNode *)
    {
        if (((u4Time - pWebnmUserNode->u4AccessTime) >
             (UINT4) HTTP_CONNECTION_IDLE_TIMEOUT)
            && (WEBNM_HTTP_TIME_OUT == OSIX_TRUE))
        {
            WebNMDeleteUserEntry (pWebnmUserNode->u4SessionId);
        }
    }
}

/*********************************************************************
*  Function Name : WebnmSendErrorPage
*  Description   : This function will return error page.
*  Input(s)      : pHttp - Pointer to the http entry where the socket
*                    descriptor is stored
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
VOID
WebnmSendErrorPage (tHttp * pHttp)
{
    INT4                i4Count = HTTP_ZERO;
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
    MEMSET (&In6Addr, HTTP_ZERO, sizeof (tUtlIn6Addr));

#endif

    MEMSET (&IpAddress, HTTP_ZERO, sizeof (tUtlInAddr));
    for (i4Count = HTTP_ZERO;
         i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; i4Count++)
    {
        if (gIpv4Sec[i4Count].i4SockFd == pHttp->i4Sockid)
        {
            IpAddress.u4Addr = gIpv4Sec[i4Count].ClientAddr.sin_addr.s_addr;
            pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
        }
    }

#ifdef IP6_WANTED
    for (i4Count = HTTP_ZERO;
         i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; i4Count++)
    {
        if (gIpv6Sec[i4Count].i4SockFd == pHttp->i4Sockid)
        {
            MEMCPY (In6Addr.u1addr,
                    gIpv6Sec[i4Count].ClientV6Addr.sin6_addr.s6_addr,
                    sizeof (In6Addr.u1addr));
            pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
        }
    }
#endif

    for (i4Count = HTTP_ZERO; erhtml[i4Count].pi1Name != NULL; i4Count++)
    {
        if (STRCMP (erhtml[i4Count].pi1Name, "error.html") == HTTP_ZERO)
        {
            pHttp->pi1Html = (INT1 *) erhtml[i4Count].pi1Ptr;
            pHttp->i4HtmlSize = erhtml[i4Count].i4Size;

            pHttp->u2RespCode = HTTP_OK;
            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html),
                            (pHttp->i4HtmlSize));

            /* syslog msg for web time-out */
            if (pHttp->u1RequestType == HTTP_ONE)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) gi4WebSysLogId,
                              "WEBNM: Session logout Idle timer expired for web from %s",
                              pu1String));
            }
            else
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) gi4WebSysLogId,
                              "WEBNM: Session logout Idle timer expired for web"));
            }

            return;
        }
    }
}

/*********************************************************************
 *  Function Name : WebnmSendLoginErrorPage
 *  Description   : This function will return Login error page.
 *  Input(s)      : pHttp - Pointer to the http entry where the socket
 *                    descriptor is stored
 *                  i1ErrorValue - The error value that has to be passed
 *                    to WebnmSendLogin function in case of Default Auth
 *                    Scheme
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/
VOID
WebnmSendLoginErrorPage (tHttp * pHttp, INT1 i1ErrorValue)
{
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif

    if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
    {
        WebnmSendLogin (pHttp, i1ErrorValue);
    }
    else
    {
        pHttp->u2RespCode = HTTP_UNAUTHORIZED;
        STRNCPY (pHttp->au1Realm, gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);
        HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
    }

    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
        {
            IpAddress.u4Addr = gIpv4Sec[u4Counter].ClientAddr.sin_addr.s_addr;
            pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
        }
    }
#ifdef IP6_WANTED
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
        {
            MEMCPY (In6Addr.u1addr,
                    gIpv6Sec[u4Counter].ClientV6Addr.sin6_addr.s6_addr,
                    sizeof (In6Addr.u1addr));
            pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
        }
    }
#endif
    if (pHttp->u1RequestType == HTTP_ONE)
    {
        WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                            "Login error happened for %s", pu1String);
    }
    return;
}

/*********************************************************************
 *  Function Name : WebnmSendRestartPage
 *  Description   : This function will return restartbrowser page.
 *  Input(s)      : pHttp - Pointer to the http struct
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/
VOID
WebnmSendRestartPage (tHttp * pHttp)
{
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; rehtml[i4Count].pi1Name != NULL; i4Count++)
    {
        if (STRCMP (rehtml[i4Count].pi1Name, "restartbrowser.html")
            == HTTP_ZERO)
        {
            HTTP_STRCPY (pHttp->ai1HtmlName, "restartbrowser.html");
            pHttp->pi1Html = (INT1 *) rehtml[i4Count].pi1Ptr;
            pHttp->i4HtmlSize = rehtml[i4Count].i4Size;
            pHttp->u2RespCode = HTTP_OK;

            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            WebnmSockWrite (pHttp,
                            (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
        }
    }
}

#ifdef  RADIUS_WANTED
#ifdef WLC_WANTED
VOID
WebnmHandleRadiusResponse (VOID *pRadRespRcvd)
{
    tRadInterface      *pRadResp = NULL;

    pRadResp = (tRadInterface *) pRadRespRcvd;

    switch (pRadResp->Access)
    {
        case ACCESS_ACCEPT:
            OsixEvtSend (pRadResp->TaskId, WLCHDLR_CMN_ACCEPT_EVENT);
            break;
        case ACCESS_REJECT:
            OsixEvtSend (pRadResp->TaskId, WLCHDLR_CMN_REJECT_EVENT);
            break;
        case RAD_TIMEOUT:
            OsixEvtSend (pRadResp->TaskId, WLCHDLR_CMN_REJECT_EVENT);
            break;
        default:
            OsixEvtSend (pRadResp->TaskId, WLCHDLR_CMN_REJECT_EVENT);
            break;
    }
    WebnmFreeRadiusMem ((tRadInterface *) pRadRespRcvd);
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : WebnmFreeRadiusMem                                   */
/*                                                                           */
/* Description        : This routine frees the Radius interface data         */
/*                      structure memory.                                    */
/*                                                                           */
/* Input(s)           : pIface - pointer to Radius Client Interface data     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
WebnmFreeRadiusMem (tRadInterface * pIface)
{
    UtlShMemFreeRadInterface ((UINT1 *) pIface);
}

#ifdef WLC_WANTED
INT1
WebnmRadiusAuthenticate (INT1 *pi1Name, INT1 *pi1Passwd, tOsixTaskId TaskId,
                         INT1 *pi1ServerStatus, tMacAddr au1StaMacAddr,
                         BOOL1 bTriggerUserRole)
{
    tRADIUS_INPUT_AUTH  RadiusInputAuth;
    tUSER_INFO_PAP      UserInfoPap;
    INT4                i4Ret = HTTP_ZERO;

    if ((pi1Name == NULL) || (pi1Passwd == NULL) || (TaskId == HTTP_ZERO))
    {
        WSSSTA_WEBAUTH_TRC (WSSSTAWEBAUTH_MASK, "Invalid Credentials\n");
        return ENM_FAILURE;
    }

    MEMSET (&RadiusInputAuth, HTTP_ZERO, sizeof (tRADIUS_INPUT_AUTH));
    MEMSET (&UserInfoPap, HTTP_ZERO, sizeof (tUSER_INFO_PAP));
    RadiusInputAuth.p_UserInfoPAP = &UserInfoPap;

    /* building Radius Input Auth structure */
    RadiusInputAuth.u1_ProtocolType = PRO_PAP;
    RadiusInputAuth.TaskId = TaskId;
    RadiusInputAuth.p_ServicesAuth = NULL;

    /* building Info Pap structure which is within Radius Input Auth 
     * Structure */
    STRNCPY (UserInfoPap.a_u1UserName, pi1Name,
             MEM_MAX_BYTES (LEN_USER_NAME, STRLEN (pi1Name)));
    STRNCPY (UserInfoPap.a_u1UserPasswd, pi1Passwd,
             MEM_MAX_BYTES ((LEN_PASSWD), STRLEN (pi1Passwd)));

    MEMCPY (RadiusInputAuth.au1StaMac, au1StaMacAddr, MAC_ADDR_LEN);

    if (bTriggerUserRole == OSIX_TRUE)
    {
        if (RadApiHandleRadAuthCB (&RadiusInputAuth, NULL,
                                   HTTP_ZERO, HTTP_ZERO, &i4Ret) == NULL)
        {
            i4Ret = radiusAuthentication (&RadiusInputAuth, NULL,
                                          WebnmHandleRadiusResponse, HTTP_ZERO,
                                          HTTP_ZERO);
        }
    }
    else
    {

        i4Ret = radiusAuthentication (&RadiusInputAuth, NULL,
                                      WebnmHandleRadiusResponse, HTTP_ZERO,
                                      HTTP_ZERO);
    }

    if (i4Ret == NOT_OK)
    {
        *pi1ServerStatus = ISS_FALSE;
        ENM_TRACE ("Unable to send Radius Packets.\n");
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WebnmWebAuthRadiusAuthentication                     */
/*                                                                           */
/* Description        : This routine does Radius authentication for WabAuth  */
/*                                                                           */
/* Input(s)           : pi1Name - Username                                   */
/*                      pi1Passwd - Password                                 */
/*                      au1StatMacAddr - Station MAC Address                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_FAILURE/ENM_SUCCESS                              */
/*****************************************************************************/
INT1
WebnmWebAuthRadiusAuthentication (INT1 *pi1Name, INT1 *pi1Passwd,
                                  tMacAddr au1StaMacAddr)
{
    INT4                i4RetVal = ENM_FAILURE;
    tOsixTaskId         TaskId = 0;
    INT1                i1ServerStatus = ISS_TRUE;
    UINT4               u4Event = 0;
    /* For RADIUS authentication, Local DB check is not necessary */
    UINT1               u1CheckFlag = OSIX_FALSE;

    if (OsixTskIdSelf (&TaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Webnm: Get Osix Id of current Task failed\n");
    }
    i4RetVal = WebnmRadiusAuthenticate (pi1Name, pi1Passwd, TaskId,
                                        &i1ServerStatus, au1StaMacAddr,
                                        OSIX_TRUE);

    /* In WSS User-Role case, there will be scenarios in which the Radius Access-Request
     * will not be sent. This will be due to the user name is blocked, mac-address is
     * blocked or mac-mapping is not found.
     * In these cases, Radius Access-Accept will not be recieved, so
     * the below event (WEBNM_REJECT_EVENT/WEBNM_ACCEPT_EVENT) will not be recieved
     * So the following piece of code is not required in the above mentioned scenarios*/
    if (i1ServerStatus != ISS_FALSE)
    {
        while (1)
        {
            if (OsixEvtRecv (TaskId, (WLCHDLR_CMN_REJECT_EVENT |
                                      WLCHDLR_CMN_ACCEPT_EVENT),
                             OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
            {
                if ((u4Event & WLCHDLR_CMN_REJECT_EVENT) ==
                    WLCHDLR_CMN_REJECT_EVENT)
                {
                    WSSSTA_WEBAUTH_TRC (WSSSTAWEBAUTH_MASK,
                                        "REJECT EVT revd\n");
                    i4RetVal = ENM_FAILURE;
                    break;
                }
                if ((u4Event & WLCHDLR_CMN_ACCEPT_EVENT) ==
                    WLCHDLR_CMN_ACCEPT_EVENT)
                {
                    i4RetVal = ENM_SUCCESS;
                    break;
                }
            }
        }
    }
    else
    {
        WSSSTA_WEBAUTH_TRC1 (WSSSTAWEBAUTH_MASK,
                             "Server Status is false and retval is %d\n",
                             i4RetVal);
    }

    if (i4RetVal != ENM_FAILURE)
    {
        /* The User entry should have already been available in the database.
         * Just change the user's authenticate state info ,
         *  in case u1CheckFlag is false
         */
        if (WssStaWebAuthUsrAuthenticated
            ((UINT1 *) &pi1Name, (UINT1 *) &pi1Passwd, au1StaMacAddr,
             u1CheckFlag) != OSIX_SUCCESS)
        {
            i4RetVal = ENM_FAILURE;
        }
    }
    return i4RetVal;
}
#endif /* WLC_WANTED */
#endif /* RADIUS_WANTED */

#ifdef TACACS_WANTED
/*****************************************************************************/
/* Function Name      : WebnmTacacsAuthenticate                              */
/*                                                                           */
/* Description        : This routine send UserName and Password to Tacacs    */
/*                      Server for authentication                            */
/*                                                                           */
/* Input(s)           : pi1Name    - pointer to UserName                     */
/*                      piPasswd   - pointer to Password                     */
/*                      pu!TskName - pointer to Task                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS - if authenticated                       */
/*                      ENM_FAILURE - if fail to authneticate                */
/*****************************************************************************/

INT1
WebnmTacacsAuthenticate (INT1 *pi1Name, INT1 *pi1Passwd, tOsixTaskId TaskId,
                         INT1 *pi1ServerStatus)
{
    tTacAuthenInput    *pTacAuthenInput = NULL;
    INT4                i4Ret = HTTP_ZERO;
    UINT1               au1Port[] = "login_auth";
    UINT4               u4Len;

    if ((pi1Name == NULL) || (pi1Passwd == NULL) || (TaskId == HTTP_ZERO))
    {
        return ENM_FAILURE;
    }
    if ((pTacAuthenInput =
         (tTacAuthenInput *) MemAllocMemBlk (gHttpInfo.WebnmTacacsPoolId)) ==
        NULL)
    {

        return ENM_FAILURE;
    }
    pTacAuthenInput->u1Action = TAC_AUTHEN_LOGIN;
    pTacAuthenInput->u1PrivilegeLevel = 15;
    pTacAuthenInput->u1Service = TAC_AUTHEN_SVC_LOGIN;
    pTacAuthenInput->u1AuthenType = TAC_AUTHEN_TYPE_PAP;
    STRCPY (pTacAuthenInput->au1Port, au1Port);
    u4Len = STRLEN (pi1Name) < sizeof (pTacAuthenInput->au1UserName)
        ? STRLEN (pi1Name) : (sizeof (pTacAuthenInput->au1UserName) - 1);
    STRNCPY (pTacAuthenInput->au1UserName, pi1Name, u4Len);
    pTacAuthenInput->au1UserName[u4Len] = '\0';
    u4Len = STRLEN (pi1Passwd) < sizeof (pTacAuthenInput->
                                         UserInfo.UserPAP.au1Password) ?
        STRLEN (pi1Passwd) :
        (sizeof (pTacAuthenInput->UserInfo.UserPAP.au1Password) - 1);

    STRNCPY (pTacAuthenInput->UserInfo.UserPAP.au1Password, pi1Passwd, u4Len);
    pTacAuthenInput->UserInfo.UserPAP.au1Password[u4Len] = '\0';
    pTacAuthenInput->AppInfo.TaskId = TaskId;

    pTacAuthenInput->u4AppReqId = HTTP_ONE;
    pTacAuthenInput->AppInfo.fpAppCallBackFn = WebnmHandleTacacsResponse;

    i4Ret = TacacsAuthenticateUser (pTacAuthenInput);

    if (i4Ret != TAC_CLNT_RES_OK)
    {
        *pi1ServerStatus = ISS_FALSE;
        ENM_TRACE ("Unable to send Tacacs Packets.\r\n");
        MemReleaseMemBlock (gHttpInfo.WebnmTacacsPoolId,
                            (UINT1 *) pTacAuthenInput);
        return ENM_FAILURE;
    }
    MemReleaseMemBlock (gHttpInfo.WebnmTacacsPoolId, (UINT1 *) pTacAuthenInput);
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WebnmHandleTacacsResponse                              */
/*                                                                           */
/* Description        : Callback routine for tacacs authentication, This     */
/*                      routine handles the tacacs server response           */
/*                                                                           */
/* Input(s)           : pRadRespRcvd - pointer to structure to receive Radius*/
/*                      status.                                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
WebnmHandleTacacsResponse (VOID *pTacRespRcvd)
{
    tTacMsgOutput      *pTacResp = NULL;

    pTacResp = (tTacMsgOutput *) pTacRespRcvd;

    switch (pTacResp->TacOutput.AuthenOutput.i4AuthenStatus)
    {
        case TAC_AUTHEN_STATUS_PASS:
            OsixEvtSend (pTacResp->TaskId, WEBNM_ACCEPT_EVENT);
            break;
        case TAC_AUTHEN_STATUS_FAIL:
            OsixEvtSend (pTacResp->TaskId, WEBNM_REJECT_EVENT);
            break;
        default:
            OsixEvtSend (pTacResp->TaskId, WEBNM_REJECT_EVENT);
            break;
    }
    UtlShMemFreeTacMsgOutput ((UINT1 *) pTacRespRcvd);
    return ENM_SUCCESS;
}
#endif /* TACACS_WANTED */
/*****************************************************************************/
/*  FUNCTION NAME : WebnmRegisterLock ()                                     */
/*  DESCRIPTION   : This function registers the Lock/UnLock functions of     */
/*                  the Protocol.                                            */
/*  INPUT         :  pHttp  - Pointer to Http entry                          */
/*                   fpLock - Pointer to the Protocol Lock function          */
/*                   fpUnLock - Pointer to the Protocol UnLock function      */
/*  OUTPUT        : None                                                     */
/*****************************************************************************/

VOID
WebnmRegisterLock (tHttp * pHttp,
                   INT4 (*fpLock) (VOID), INT4 (*fpUnLock) (VOID))
{
    pHttp->fpLock = fpLock;
    pHttp->fpUnLock = fpUnLock;
    return;
}

/*****************************************************************************/
/*  FUNCTION NAME : WebnmUnRegisterLock ()                                   */
/*  DESCRIPTION   : This function unregisters the Lock/UnLock functions      */
/*                  registered by WebnmRegisterLock ().                      */
/*  INPUT         :  pHttp  - Pointer to Http entry                          */
/*  OUTPUT        : None                                                     */
/*****************************************************************************/

VOID
WebnmUnRegisterLock (tHttp * pHttp)
{
    pHttp->fpLock = NULL;
    pHttp->fpUnLock = NULL;
    return;
}

/*****************************************************************************/
/*  FUNCTION NAME : WebnmRegisterContext ()                                  */
/*  DESCRIPTION   : This function registers the Lock/UnLock functions of     */
/*                  the Protocol.                                            */
/*  INPUT         :  pHttp  - Pointer to Http entry                          */
/*                   fpSelectContext- Pointer to the Protocol Context
 *                             Select   function                             */
/*                   fpReleaseContext - Pointer to the Protocol Release
 *                              Context function                             */
/*  OUTPUT        : None                                                     */
/*****************************************************************************/
VOID
WebnmRegisterContext (tHttp * pHttp,
                      INT4 (*fpSelectContext) (UINT4),
                      INT4 (*fpReleaseContext) (VOID))
{
    pHttp->fpSelectContext = fpSelectContext;
    pHttp->fpReleaseContext = fpReleaseContext;
    return;
}

/*****************************************************************************/
/*  FUNCTION NAME : WebnmUnRegisterContext ()                                */
/*  DESCRIPTION   : This function unregisters the Lock/UnLock functions      */
/*                  registered by WebnmRegisterLock ().                      */
/*  INPUT         :  pHttp  - Pointer to Http entry                          */
/*  OUTPUT        : None                                                     */
/*****************************************************************************/
VOID
WebnmUnRegisterContext (tHttp * pHttp)
{
    pHttp->fpSelectContext = NULL;
    pHttp->fpReleaseContext = NULL;
    return;
}

/*********************************************************************
 *  Function Name : WebnmReadFromSocket 
 *  Description   : This function gets the value corresponding to the 
 *                  name from the html page and convert it to data. 
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *                  pu1KeyName - Pointer to the Key name
 *                  u1Type - Type of data
 *  Output(s)     : None.
 *  Return Values : pData - pointer to the structure containing 
 *                          multidata
 *********************************************************************/

tSNMP_MULTI_DATA_TYPE *
WebnmReadFromSocket (tHttp * pHttp, CHR1 * pu1KeyName, UINT1 u1Type)
{
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    if (HttpGetValuebyName ((UINT1 *) pu1KeyName, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        pData = (tSNMP_MULTI_DATA_TYPE *) WebnmConvertStringToData
            (pHttp->au1Value, u1Type);
        if (pData == NULL)
        {
            return NULL;
        }
        return pData;
    }
    return NULL;
}

/*********************************************************************
 *  Function Name : WebnmReadNonZeroFromSocket 
 *  Description   : This function gets the value corresponding to the 
 *                  name only if the value id not zero 
 *                  from the html page and convert it to data.
 *          
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *                  pu1KeyName - Pointer to the Key name
 *                  u1Type - Type of data
 *  Output(s)     : None.
 *  Return Values : pData - pointer to the structure containing 
 *                          multidata
 *********************************************************************/

tSNMP_MULTI_DATA_TYPE *
WebnmReadNonZeroFromSocket (tHttp * pHttp, CHR1 * pu1KeyName, UINT1 u1Type)
{
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    if (HttpGetNonZeroValuebyName ((UINT1 *) pu1KeyName, pHttp->au1Value,
                                   pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        pData = (tSNMP_MULTI_DATA_TYPE *) WebnmConvertStringToData
            (pHttp->au1Value, u1Type);
        if (pData == NULL)
        {
            return NULL;
        }
        return pData;
    }
    return NULL;
}

/************************************************************************
 * Function Name : WebnmSendToSocket
 * Description   : This function sends the data to the socket.
 * 
 * Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *                 pu1KeyName - Pointer to the Key name
 *                 pData - pointer to the structure containing
 *                        multidata
 *                 u1Type - Type of data
 * Output(s)     : None.
 * Return Values : None
 * ********************************************************************/

VOID
WebnmSendToSocket (tHttp * pHttp, CHR1 * pu1KeyName,
                   tSNMP_MULTI_DATA_TYPE * pData, UINT1 u1Type)
{

    WebnmConvertDataToString (pData, pHttp->au1DataString, u1Type);
    WebnmSendString (pHttp, (UINT1 *) pu1KeyName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    return;

}

/*********************************************************************
 *  Function Name : WebnmApiGetContextId
 *  Description   : This function gets the value stored in sll
 *
 *
 *  Input(s)      : pHttp - pointer to tHttp Global Structure
 *  Output(s)     : pu4ContextId - Contex id
 *  Return Values : None
 *
 *********************************************************************/
INT4
WebnmApiGetContextId (tHttp * pHttp, UINT4 *pu4ContextId)
{

    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
        UINT4               u4SessionId;
        tWebnmUserNode     *pWebnmUserNode = NULL;

        /* No need to check for failure cases because request will come
         * only when a session is alive and each alive session will have
         * a context id*/
        STRCPY (pHttp->au1Name, "Gambit");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        WebnmDecode (pHttp->au1Value);
        SSCANF ((char *) pHttp->au1Value, "%u", &u4SessionId);
        pWebnmUserNode = (tWebnmUserNode *) WebNMCheckUserEntry (u4SessionId);
        if (pWebnmUserNode != NULL)
        {
            *pu4ContextId = pWebnmUserNode->u4VcmContext;
            return ENM_SUCCESS;
        }
        return ENM_FAILURE;
    }
    else
    {
        *pu4ContextId = gu4VcmContext;
        return ENM_SUCCESS;
    }
}

/*********************************************************************
 *  Function Name : WebnmApiGetL3ContextId
 *  Description   : This function gets the value stored in sll
 *
 *
 *  Input(s)      : pHttp - pointer to tHttp Global Structure
 *  Output(s)     : pu4L3ContextId - L3Context id
 *  Return Values : None
 *
 *********************************************************************/
INT4
WebnmApiGetL3ContextId (tHttp * pHttp, UINT4 *pu4L3ContextId)
{

    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
        UINT4               u4SessionId;
        tWebnmUserNode     *pWebnmUserNode = NULL;

        /* No need to check for failure cases because request will come
         * only when a session is alive and each alive session will have
         * a context id*/
        STRCPY (pHttp->au1Name, "Gambit");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        WebnmDecode (pHttp->au1Value);
        SSCANF ((char *) pHttp->au1Value, "%u", &u4SessionId);
        pWebnmUserNode = (tWebnmUserNode *) WebNMCheckUserEntry (u4SessionId);
        if (pWebnmUserNode != NULL)
        {
            *pu4L3ContextId = pWebnmUserNode->u4VcmL3Context;
            return ENM_SUCCESS;
        }
        return ENM_FAILURE;
    }
    else
    {
        *pu4L3ContextId = gu4VcmL3Context;
        return ENM_SUCCESS;
    }
}

/*********************************************************************
 *  Function Name : WebFindPagePriv
 *  Description   : This function gets the privilege of the web page
 *  Input(s)      : pHttp - pointer to tHttp Global Structure
 *  Output(s)     : None
 *  Return Values : ENM_SUCCESS/ENM_FAILURE
 *
 *********************************************************************/
INT1
WebFindPagePriv (tHttp * pHttp)
{
#ifdef CLI_WANTED
    INT4                i4Count = 0;
    INT2                i2Priv = 0;
    INT4                i4Retval = CLI_FAILURE;
    INT1                ai1Username[MAX_NAME_LEN];

    for (i4Count = 0; i4Count < WEB_PRIV_MAX_LENGTH; i4Count++)
    {
        if (STRCMP
            (pHttp->ai1HtmlName,
             giUrlName[gPagePrivMapping[i4Count].u2UrlName]) == 0)
        {
            MEMSET (ai1Username, 0, MAX_NAME_LEN);
            HTTP_STRCPY (ai1Username, au1WebnmLogin[0]);
            i4Retval = CliGetUserPrivilege ((CHR1 *) ai1Username, &i2Priv);
            if (i4Retval == CLI_FAILURE)
            {
                continue;
            }
            if (i2Priv < gPagePrivMapping[i4Count].i2PrivId)
            {
                IssSendError (pHttp, (INT1 *) "View not permitted");
                return ENM_FAILURE;
            }
        }
    }
#else
    UNUSED_PARAM (pHttp);
#endif
    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : WebnmGetWebSysLogId
 * Description   : This function is used to get the gi4WebSysLogId for
 *                 other web related modules to send syslog messages.
 * Input(s)      : None.
 * Output(s)     : None.
 * Return Values : gi4WebSysLogId
 *********************************************************************/
INT4
WebnmGetWebSysLogId ()
{
    return (gi4WebSysLogId);
}

/*********************************************************************
 * Function Name : WebnmGetWebUsrNameForSysLogMsg
 * Description   : This function is used to get the name of the user  
 *                 for sending in syslog message.
 * Input(s)      : None.
 * Output(s)     : pu1UserName
 * Return Values : ENM_SUCCESS
 *********************************************************************/
INT1
WebnmGetWebUsrNameForSysLogMsg (UINT1 *pu1UserName)
{
    STRNCPY (pu1UserName, gu1WebnmUserName, ENM_MAX_USERNAME_LEN);
    pu1UserName[ENM_MAX_USERNAME_LEN - 1] = '\0';
    return (ENM_SUCCESS);
}

/*********************************************************************
 * Function Name : WebnmSetWebUsrNameForSysLogMsg
 * Description   : This function is used to set the name of the user  
 *                 for sending in syslog message.
 * Input(s)      : pu1UserName
 * Output(s)     : None.
 * Return Values : ENM_SUCCESS
 *********************************************************************/
INT1
WebnmSetWebUsrNameForSysLogMsg (UINT1 *pu1UserName)
{
    STRNCPY (gu1WebnmUserName, pu1UserName, ENM_MAX_USERNAME_LEN);
    gu1WebnmUserName[ENM_MAX_USERNAME_LEN - 1] = '\0';
    return (ENM_SUCCESS);
}

/*********************************************************************
 * Function Name : WebnmGetSnmpQueryFromWebStatus
 * Description   : This function is used to get the current SnmpQueryStatus
 * Input(s)      : None
 * Output(s)     : gu1WebnmSnmpQueryFromWebStatus
 * Return Values : gu1WebnmSnmpQueryFromWebStatus 
 *********************************************************************/
UINT1
WebnmGetSnmpQueryFromWebStatus ()
{
    return (gu1WebnmSnmpQueryFromWebStatus);
}

/*********************************************************************
 * Function Name : WebnmSetWebUsrNameForSysLogMsg
 * Description   : This function is sets gu1WebnmSnmpQueryFromWebStatus
 *                 if the SNMP set request is arised from web page.
 * Input(s)      : None
 * Output(s)     : None
 * Return Values : ENM_SUCCESS
 *********************************************************************/
UINT1
WebnmSetSnmpQueryFromWebStatus (UINT1 u1WebnmSnmpQueryFromWebStatus)
{
    gu1WebnmSnmpQueryFromWebStatus = u1WebnmSnmpQueryFromWebStatus;
    return ENM_SUCCESS;
}
