/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttpdb.h,v 1.3 2013/02/12 12:09:38 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSHTTPDB_H
#define _FSHTTPDB_H

UINT1 FsHttpRedirectionTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,100};

UINT4 fshttp [] ={1,3,6,1,4,1,29601,2,44};
tSNMP_OID_TYPE fshttpOID = {9, fshttp};


UINT4 FsHttpRedirectionStatus [ ] ={1,3,6,1,4,1,29601,2,44,1,1,1};
UINT4 FsOperHttpAuthScheme [ ] ={1,3,6,1,4,1,29601,2,44,1,1,2};
UINT4 FsConfigHttpAuthScheme [ ] ={1,3,6,1,4,1,29601,2,44,1,1,3};
UINT4 FsHttpRequestCount [ ] ={1,3,6,1,4,1,29601,2,44,1,1,4};
UINT4 FsHttpRequestDiscards [ ] ={1,3,6,1,4,1,29601,2,44,1,1,5};
UINT4 FsHttpRedirectionURL [ ] ={1,3,6,1,4,1,29601,2,44,1,2,1,1,1};
UINT4 FsHttpRedirectedSrvAddrType [ ] ={1,3,6,1,4,1,29601,2,44,1,2,1,1,2};
UINT4 FsHttpRedirectedSrvIP [ ] ={1,3,6,1,4,1,29601,2,44,1,2,1,1,3};
UINT4 FsHttpRedirectedSrvDomainName [ ] ={1,3,6,1,4,1,29601,2,44,1,2,1,1,4};
UINT4 FsHttpRedirectionEntryStatus [ ] ={1,3,6,1,4,1,29601,2,44,1,2,1,1,5};




tMbDbEntry fshttpMibEntry[]= {

{{12,FsHttpRedirectionStatus}, NULL, FsHttpRedirectionStatusGet, FsHttpRedirectionStatusSet, FsHttpRedirectionStatusTest, FsHttpRedirectionStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsOperHttpAuthScheme}, NULL, FsOperHttpAuthSchemeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{12,FsConfigHttpAuthScheme}, NULL, FsConfigHttpAuthSchemeGet, FsConfigHttpAuthSchemeSet, FsConfigHttpAuthSchemeTest, FsConfigHttpAuthSchemeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsHttpRequestCount}, NULL, FsHttpRequestCountGet, FsHttpRequestCountSet, FsHttpRequestCountTest, FsHttpRequestCountDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsHttpRequestDiscards}, NULL, FsHttpRequestDiscardsGet, FsHttpRequestDiscardsSet, FsHttpRequestDiscardsTest, FsHttpRequestDiscardsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{14,FsHttpRedirectionURL}, GetNextIndexFsHttpRedirectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsHttpRedirectionTableINDEX, 1, 0, 0, NULL},

{{14,FsHttpRedirectedSrvAddrType}, GetNextIndexFsHttpRedirectionTable, FsHttpRedirectedSrvAddrTypeGet, FsHttpRedirectedSrvAddrTypeSet, FsHttpRedirectedSrvAddrTypeTest, FsHttpRedirectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsHttpRedirectionTableINDEX, 1, 0, 0, NULL},

{{14,FsHttpRedirectedSrvIP}, GetNextIndexFsHttpRedirectionTable, FsHttpRedirectedSrvIPGet, FsHttpRedirectedSrvIPSet, FsHttpRedirectedSrvIPTest, FsHttpRedirectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsHttpRedirectionTableINDEX, 1, 0, 0, NULL},

{{14,FsHttpRedirectedSrvDomainName}, GetNextIndexFsHttpRedirectionTable, FsHttpRedirectedSrvDomainNameGet, FsHttpRedirectedSrvDomainNameSet, FsHttpRedirectedSrvDomainNameTest, FsHttpRedirectionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsHttpRedirectionTableINDEX, 1, 0, 0, NULL},

{{14,FsHttpRedirectionEntryStatus}, GetNextIndexFsHttpRedirectionTable, FsHttpRedirectionEntryStatusGet, FsHttpRedirectionEntryStatusSet, FsHttpRedirectionEntryStatusTest, FsHttpRedirectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsHttpRedirectionTableINDEX, 1, 0, 1, NULL},
};
tMibData fshttpEntry = { 10, fshttpMibEntry };

#endif /* _FSHTTPDB_H */

