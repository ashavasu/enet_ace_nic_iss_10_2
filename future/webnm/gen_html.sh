# $Id: gen_html.sh,v 1.10 2015/02/13 11:17:45 siva Exp $

##############################################################################
#    DESCRIPTION             : Automatic generation of htmldata.h depends    #
#                              upon the package which we are building        #
##############################################################################

# $1 refers the package which we are building
# Example: workgroup, enterprise, metro
# $2 refers the environment for which the package is built
# Example: target, simulation

clear
 
if ( [ "$#" -eq "2" ] && 
       ( [ $2 = "bcm" ] || [ $2 = "xcat" ] || 
         [ $2 = "fulcrum" ] || [ $2 = "mrvlls" ] || 
         [ $2 = "simulation" ] ) && 
       ( [ $1 = "workgroup" ] || [ $1 = "enterprise" ] ||
         [ $1 = "metro" ] || [ $1 = "metro_e" ] )); then

PKG_NAME="$1"

cd tool/Htmlscan/
./mk_HtmlScan

cd ../Htmlgen/
export MKDIR=mkdir
rm exe/ -rf
rm obj/ -rf
make
clear

cd ../../../ISS/common/web/
mkdir HTML
rm -rf htmlpages/workgroup/htmldata.h
rm -rf htmlpages/$1/htmldata.h
cp htmlpages/workgroup/*.* HTML/
if [ $1 == "metro_e" ];then
cp htmlpages/metro/*.* HTML/
cp htmlpages/enterprise/*.* HTML/
rm HTML/htmldata.h
else
cp htmlpages/$1/*.* HTML/
fi
cd HTML

# If $1 is enterprise and $2 is target then only Multicasting IPv4 pages 
# needs to be copied otherwise remove them from the HTML fiolder
if [  $1 != "enterprise"  ] ||  [ $2 = "simulation" ];then
rm -rf ipv4mc*
fi

if [ $2 != "simulation" ] && [ $2 == "xcat" ];then
rm port_ratectrl.html
fi

grep "snmpvaluesstart" -rnlH *>generic
mkdir GENERIC
cp `cat generic` GENERIC/
rm generic
mv GENERIC/ ../../../../webnm/tool/Htmlgen/

cd ../../
cp tool/msrscan/dbfiles_others/ -rf ../../webnm/tool/Htmlgen/

# Cleanup :If $1 is enterprise and $2 is target then only Multicasting IPv4 
# related mib i.e stdmri.mib needs to be copied otherwise remove them 
rm -rf ../../webnm/tool/Htmlgen/dbfiles_others/stdmri.db

# If $1 is enterprise and $2 is target then remove the Multicasting forward mib 
# i.e fsmfwd.mib from the dbfiles_others folder and copy the IPv4 Multicasting mib
# i.e stdmri.mib to the dbfiles_others
if [  $1 = "enterprise"  ] &&  [ $2 != "simulation" ];then
rm -f ../../webnm/tool/Htmlgen/dbfiles_others/fsmfwd.db
cp -f tool/msrscan/dbfiles_$2/stdmri.db ../../webnm/tool/Htmlgen/dbfiles_others/stdmri.db
fi

echo Generating HTML pages with OID using HTMLGEN tool

cd ../../webnm/tool/Htmlgen/exe
./HTMLGEN ../GENERIC ../dbfiles_others

mv *.html ../GENERIC

cd ../
cp GENERIC/*.* -rf ../../../ISS/common/web/HTML
rm GENERIC/ -rf

echo Generating htmldata.h

cd ../../
tool/Htmlscan/HTMLSCAN ../ISS/common/web/HTML
mv htmldata.h ../ISS/common/web/htmlpages/$1
rm -rf ../ISS/common/web/HTML/


if [  $2 != "simulation"  ]; then

#Target HtmlData Generation

cd tool/Htmlscan/
./mk_HtmlScan

cd ../Htmlgen/
export MKDIR=mkdir
rm exe/ -rf
rm obj/ -rf
make
clear

echo Target HtmlData Generation

cd ../../../ISS/$2/
rm -rf HTML/
mkdir HTML
rm -fr inc/trgt_htmldata.h
cp htmlpages/*.html HTML/
cd HTML

# If $1 is enterprise and $2 is target then only Multicasting IPv4 pages 
# needs to be copied otherwise remove them from the HTML fiolder
if [  $1 != "enterprise"  ];then
rm -rf ipv4mc*
fi

grep "snmpvaluesstart" -rnlH *>generic
mkdir GENERIC
cp `cat generic` GENERIC/
rm generic
mv GENERIC/ ../../../webnm/tool/Htmlgen/

cd ../../common
cp tool/msrscan/dbfiles_$2/ -rf ../../webnm/tool/Htmlgen/

# Cleanup :If $1 is enterprise then only Multicasting IPv4 
# related mib i.e stdmri.mib needs to be copied otherwise remove them 
#rm -rf ../../webnm/tool/Htmlgen/dbfiles_mrvlls/stdmri.db

# If $1 is enterprise and $2 is target then remove the Multicasting forward mib 
# i.e fsmfwd.mib from the dbfiles_others folder and copy the IPv4 Multicasting mib
# i.e stdmri.mib to the dbfiles_others
if [  $1 = "enterprise"  ];then
rm -f ../../webnm/tool/Htmlgen/dbfiles_$2/fsmfwd.db
fi

echo Generating HTML pages with OID using HTMLGEN tool

cd ../../webnm/tool/Htmlgen/exe
./HTMLGEN ../GENERIC ../dbfiles_$2

mv *.html ../GENERIC

cd ../
cp GENERIC/*.* -rf ../../../ISS/$2/HTML
rm GENERIC/ -rf

echo Generating target htmldata.h

cd ../../
tool/Htmlscan/HTMLSCAN ../ISS/$2/HTML 1
mv htmldata.h ../ISS/$2/inc/trgt_htmldata.h
rm -rf ../ISS/$2/HTML/

else

#simulation HtmlData Generation

cd tool/Htmlscan/
./mk_HtmlScan

cd ../Htmlgen/
export MKDIR=mkdir
rm exe/ -rf
rm obj/ -rf
make
clear

echo Target HtmlData Generation

cd ../../../ISS/others/
rm -rf HTML/
mkdir HTML
rm -fr inc/trgt_htmldata.h
cp htmlpages/*.html HTML/
cd HTML

# If $1 is enterprise and $2 is target then only Multicasting IPv4 pages 
# needs to be copied otherwise remove them from the HTML fiolder
if [  $1 != "enterprise"  ];then
rm -rf ipv4mc*
fi

grep "snmpvaluesstart" -rnlH *>generic
if [ $? -eq 0 ]
then
mkdir GENERIC
cp `cat generic` GENERIC/
rm generic
mv GENERIC/ ../../../webnm/tool/Htmlgen/

cd ../../common
cp tool/msrscan/dbfiles_others/ -rf ../../webnm/tool/Htmlgen/

# Cleanup :If $1 is enterprise then only Multicasting IPv4 
# related mib i.e stdmri.mib needs to be copied otherwise remove them 
#rm -rf ../../webnm/tool/Htmlgen/dbfiles_mrvlls/stdmri.db

# If $1 is enterprise and $2 is target then remove the Multicasting forward mib 
# i.e fsmfwd.mib from the dbfiles_others folder and copy the IPv4 Multicasting mib
# i.e stdmri.mib to the dbfiles_others
if [  $1 = "enterprise"  ];then
rm -f ../../webnm/tool/Htmlgen/dbfiles_others/fsmfwd.db
fi

echo Generating HTML pages with OID using HTMLGEN tool

cd ../../webnm/tool/Htmlgen/exe
./HTMLGEN ../GENERIC ../dbfiles_others

mv *.html ../GENERIC

cd ../
cp GENERIC/*.* -rf ../../../ISS/others/HTML
rm GENERIC/ -rf
else
cd ../../../webnm/tool/Htmlgen/
fi

echo Generating target htmldata.h

cd ../../
tool/Htmlscan/HTMLSCAN ../ISS/others/HTML 1
mv htmldata.h ../ISS/others/inc/trgt_htmldata.h
rm -rf ../ISS/others/HTML/


fi
#End of Target htmldata


else
   echo -e "                         Usage : `basename $0` PKG_NAME ENVIRONMENT"
   echo -e "*******Please mention the required package Name following the scripts Name ********."
   echo -e "*********************  Example 1: ./gen_html.sh workgroup <bcm/xcat/fulcrum/mrvlls/simulation>*****."
   echo -e "*********************  Example 2: ./gen_html.sh enterprise simulation ********************."
   exit

fi
