/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: enm.h,v 1.36 2017/12/11 10:02:04 siva Exp $
 *
 * Description: macros, typdefs and proto type common to both
 *              http and webnm
 *******************************************************************/

#ifndef _ENM_H
#define _ENM_H

#include "lr.h"
#include "fssocket.h"
#include "fssnmp.h"
#include "fswebnm.h"
#include "fssyslog.h"
#include "tacacs.h"
/* Headers to use util APIs */ 
#include "base64.h"
#include "arMD5_api.h"
#include "utlauth.h"

#define ENM_SCALAR 1
#define ENM_TABLE  2

#define HTTP_REDIRECT_STATUS_DISABLE       1
#define HTTP_REDIRECT_STATUS_ENABLE       2
#define HTTP_SIZE_OF_REDIRECTION_ENTRY    250
/* Definition for the RowStatus values */
#define   HTTP_ACTIVE                 1
#define   HTTP_NOT_IN_SERVICE         2
#define   HTTP_NOT_READY              3
#define   HTTP_CREATE_AND_GO          4
#define   HTTP_CREATE_AND_WAIT        5
#define   HTTP_DESTROY                6

/*-- Definition for users */
#define ENM_READ  0x01
#define ENM_WRITE 0x02
#define ENM_ADMIN 0x04
#define ENM_ROOT  0x08

#define ENM_NO_ACCESS       0x00
#define ENM_OTHER_ACCESS      0x03
#define ENM_OTHER_ACCESS_READ  0x01
#define ENM_OTHER_ACCESS_WRITE  0x02
#define ENM_ADMIN_ACCESS      0x0c
#define ENM_ADMIN_ACCESS_READ  0x0a
#define ENM_ADMIN_ACCESS_WRITE  0x0b
#define ENM_SELF_ACCESS       0x30
#define ENM_SELF_ACCESS_READ   0x10
#define ENM_SELF_ACCESS_WRITE   0x20
#define ENM_ALL_ACCESS        0x3f

/* The maximum number of logins for ISS */
#define HTTP_MAX_NO_OF_USERS    2

#define tWEBNM_SLL_NODE tTMO_SLL_NODE

typedef struct {
 tWEBNM_SLL_NODE  NextNode;
 UINT4            u4SessionId;
 UINT4            u4AccessTime;
 UINT4            u4VcmContext;
 UINT4            u4VcmL3Context;
}tWebnmUserNode;


typedef struct PageAccess
{
UINT4 u4GroupID;  /* Bit map representing Groups */
UINT4 u4Access;   /* Bit map defining access */
                  /* b1b0 - ENM_READ|ENM_WRITE for Others */
                  /* b3b2 - ENM_READ|ENM_WRITE for Admin */
                  /* b5b4 - ENM_READ|ENM_WRITE for Self */
}tPageAccess;


/* Http Status Code */
#define HTTP_CONTINUE                 100
#define HTTP_OK                       200
#define HTTP_FOUND                    302
#define HTTP_NOT_MODIFIED             304
#define HTTP_UNUSED       306
#define HTTP_BAD_REQUEST              400
#define HTTP_UNAUTHORIZED             401
#define HTTP_PAGE_NOT_FOUND           404
#define HTTP_METHOD_NOT_ALLOWED       405
#define HTTP_RESPONSE_NOT_ACCEPTABLE  406
#define HTTP_TOO_BIG                  413 
#define HTTP_REQUEST_URI_TOO_LONG     414
#define HTTP_SERVER_ERROR             500
#define HTTP_NOT_IMP                  501
#define HTTP_VERSION_NOT_SUPPORTED    505

#define HTTP_DYNAMIC_OBJECT           1
#define HTTP_STATIC_OBJECT            2
#define HTTP_UNMODIFIED_OBJECT        3

#define HTTP_CACHE_TIMEOUT            7200

#define WEBNM_LOGIN_WANTED            0
#define WEBNM_AUTH_FAILED             1
#define WEBNM_TIME_EXPIRED            2
#define WEBNM_USER_BLOCKED            5

/* Used as a flag to differentiate Request
 * and Response digest generation */
#define ENM_REQUEST_DIGEST            0
#define ENM_RESPONSE_DIGEST           1

#define HTTPS_MAX_LISTEN       ISS_MAX_SSL_SESSIONS
#define HTTP_MAX_LISTEN        IssGetWebMaxSession ()
#define HTTP_INVALID_INDEX    -1
#define HTTP_ZERO              0
#define HTTP_ONE               1
/* Constants to replace hardcoded numbers */
#define HTTP_TWO               2
#define HTTP_THREE             3
#define HTTP_FOUR              4
#define HTTP_FIVE              5
#define HTTP_TEN               10

#define HTTP_VERSION_LEN       2
#define HTTP_MAX_CLIENT_CONNECTIONS       HTTP_MAX_LISTEN

#define HTTP_MAX_FLASH_FILESIZE   65000


#define HTTP_MAX_VERSION_POS   9
#define HTTP_MIN_VERSION_POS   6

INT4 HttpMgmtLock (VOID);
INT4 HttpMgmtUnLock (VOID);
#define HTTP_MGMT_LOCK()  HttpMgmtLock ()
#define HTTP_MGMT_UNLOCK() HttpMgmtUnLock ()
#define MAX_WEB_SESSION  ISS_MAX_WEB_SESSIONS

enum {
       HTTP_CLT_IDLE_TIMER    = 1,
       HTTP_MAX_TIMER
};

/* The realms used as part of digest auth scheme */
/* Max. number of realms used. 
 * Note:- Technically, the ISS supports only one realm namely ISS.
 * But the realm value "ISS" has been appended with some text to send some 
 * user friendly information to the browser */
enum {
       ENM_REALM_ISS = 0,
       ENM_REALM_TIMEOUT,
       ENM_REALM_WRONG_LOGIN,
       ENM_MAX_NO_OF_REALMS
};

typedef struct HTMLPAGE
{
    tPageAccess *pPageAccess;
    const char *pi1Name;
    INT4  i4Size;
    INT4  i4LastModTime;
    UINT1  *pi1Ptr;
    INT4  i4MibType;
    INT4  i4NoOfSnmpOid;
    tSNMP_OID_TYPE *pOid;
    UINT1  *pu1Type; 
    UINT1  *pu1Access; 
}tHtmlPage;

typedef struct HtmlInfo 
{
   CHR1      *pi1PageIndex;
   tHtmlPage *pPageInfoArray;
}tHtmlInfo;

typedef struct _HttpClientTaskMap {
    tOsixTaskId  HttpProcessingTskId;
    INT4         i4ClientSockId;
    UINT1        u1SockType;
    UINT1        au1Pad[3];
} tHttpClientTaskMap;

typedef struct HttpTaskQueueMap {
    tOsixTaskId  HttpProcessingTskId;
    tOsixQId     HttpProcTskQId;
} tHttpTaskQueueMap;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* This structure holds the timer related information */
typedef struct _HttpTmrNode {
    tTmrAppTimer  TmrLink; /* Link node in the list of timers 
                            * timers running for timer list created
                            * by HTTP (HttpTmrListId)*/
    UINT1         u1TimerId; /* The timer ID of the timer that is running,
                              * Used to timeout the idle connection HTTP. */

    UINT1         u1TmrStatus; /* The Status of the timer whether the
                                * timer is running (SET) or not running
                                * (RESET) */
    UINT2         u2Reserved; /* Alignment Data */

} tHttpTmrNode; 

typedef struct HTTPS {
    VOID    *pSslConnId;
    BOOLEAN  bHttpsConn;
    UINT1    u1Pad;
    UINT2    u2Pad;
} tHttps;

typedef struct HTTPCTRL {
    INT4     i4MaxSocketId;
    INT4     i4HttpsSockFd;
    INT4     i4HttpsSock6Fd;
    tOsixSemId  SemId;
    BOOLEAN  bHttpEnable;
    BOOLEAN  bHttpsEnable;
    UINT2    u2Pad;
} tHttpCtrl;

/* This structure holds the client related information, 
 * this structure will be maintained at
 * main task*/
typedef struct HttpClientInfo {
    tTMO_DLL_NODE     NextSocInfoNode; /*pointer to the next client node
                                        in the linked list*/
    tTMO_DLL_NODE     NextSocTmrNode; /*pointer to the next timer node
                                        in the linked list*/
    tHttpTmrNode      TimeOutTmrLink;
    VOID             *pSslConnId;
    INT4              i4ClientSockId; /*socket Id of the client*/
    UINT1             u1SockType;    /*Type of Socket Ipv6 or IPv6 or Secure*/
    UINT1             au1Padding[3];
} tHttpClientInfo;

/*This is message sent to Main task from processing task 
 *  if request is received on the already accepted task*/
typedef struct HttpSocId {
    INT4              i4SockFd; /*socket Id of the client*/
} tHttpSockId;

typedef struct HttpRedirect {
    tTMO_DLL_NODE   NextRedirectNode;
    INT1            ai1RedirectedUrl [HTTP_MAX_URL_LEN];
    INT4            i4UrlLen;
    tIPvXAddr       RedirectedSrvAddr;
    INT1            ai1RedirectSrvDomainName [HTTP_MAX_URL_LEN];
    INT4            i4DomainLen;
    INT4            i4RowStatus;
}tHttpRedirect;

typedef struct HttpInfo {
    tTMO_DLL            HttpClientList; /*Single Linked List Head for Client related info*/
    tTMO_DLL            HttpClientTmrList; /*Double Linked List Head for Client Timer info*/
    tTMO_DLL            HttpRedirectList; /*Double Linked List Head for Client Timer info*/
    tTimerListId        HttpTmrListId;
    tOsixTaskId         HttpMainTskId;
    tOsixSemId          HttpSemId;
    tOsixSemId          HttpMgmtSemId;
    tOsixSemId          HttpDbSemId;
    tOsixQId            HttpMainTskQId;
    tOsixTaskId         HttpProcTaskId [HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
    tMemPoolId          HttpReqPoolId;
    tMemPoolId          HttpAccCltInfoPoolId;
    tMemPoolId          WebnmResponsePoolId;
    tMemPoolId          HttpRedirectMemPoolId;
    tMemPoolId          HttpSrvPktMsgPoolId;
    tMemPoolId          WebnmUserNodePoolId;
    tMemPoolId          MsrHttpMsgNodePoolId;
    tMemPoolId          MsrHttpDupMsgPoolId;
    tMemPoolId          WebnmTacacsPoolId;
    tHttpClientTaskMap  HttpClientTaskMap [HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
    tHttpCtrl           HttpCtrlInfo;
    fd_set             *pRdTmpFd[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
    fd_set             *pWrTmpFd[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
    INT4                ai4ConnClients [ISS_MAX_WEB_SESSIONS];
    UINT4               au4ConnClientsIp [ISS_MAX_WEB_SESSIONS];
    INT4                i4SrvSockId;
    INT4                i4SrvSock6Id;
    INT4                i4OperHttpAuthScheme;
    INT4                i4ConfigHttpAuthScheme;
    UINT1               u1TotalClientConn;
    BOOL1               bRedirectStat;
    UINT1               au1Rsvd [2];
    UINT4               u4HttpRequestCount;
    UINT4               u4HttpRequestDiscards;
} tHttpInfo;

typedef struct _tHttpRedirectionBlock
{
    UINT1 au1HttpRedirectionBlock[HTTP_SIZE_OF_REDIRECTION_ENTRY];
    UINT1 au1Reserved[2];
}tHttpRedirectionBlock;

typedef struct _tWebnmResponseBlock
{
    UINT1  au1WebnmResponseBlock[WEBNM_MEMBLK_SIZE];
}tWebnmResponseBlock;

/* To store the Username and Password */
struct WebnmUserDetails
{
    const char         *pi1Name;
    const char         *pi1Passwd;
};

/* To store the Username,Password and Digest for
 * Digest authentication scheme */
struct WebnmDigestUserDetails
{
    const INT1  ai1Uname[ENM_MAX_USERNAME_LEN];    /*Username*/
    const INT1  ai1Pwd[ENM_MAX_PASSWORD_LEN];      /*Password*/
    const UINT1 au1HA1[ENM_MAX_NO_OF_REALMS][ENM_MAX_DIGEST_LEN];/*HA1s associated with a 
                                                                   username corresponding 
                                                                   to the realms */

};

/* Global Structure which has info of task Id's, queue Id's, 
 *    Head for Client Information, Pending Requests and 
 *       1 minute timer related information*/


/* Http Export Functions */
#ifdef ISS_WANTED
INT4 HttpReadFlash (INT1 *, INT1 *, UINT4 *);
#endif /* ISS_WANTED */
INT4 HttpReadhtml(tHttp *);
INT4 HttpNextGetValuebyName PROTO((UINT1 *, UINT1 *, UINT1 *));
INT4 HttpGetNonZeroValuebyName PROTO((UINT1 *, UINT1 *, UINT1 *));
VOID HttpSendError PROTO((tHttp *, UINT4));
VOID HttpSendFlashSpecificError PROTO((tHttp *, UINT4));

/* Webnm Export Function */
VOID WebnmProcessGet PROTO((tHttp *));
VOID WebnmProcessPost PROTO((tHttp *));
VOID WebnmInit PROTO((tHttp *pHttp));
INT1 WebnmAuthenticate PROTO((tHttp *, UINT1));
VOID WebnmSendLogin PROTO((tHttp * ,INT1 ));
/* To handle user logout */
VOID WebnmSendLogout PROTO((tHttp * ));
/* To send a restart browser message to user */
VOID WebnmSendRestartPage PROTO ((tHttp * ));
UINT4 WebnmGetTime PROTO((VOID));

INT4 HttpWebnmConfigurePolicy PROTO((tHttp *pHttp));
INT4    HttpModifyRedirectionEntry (tSNMP_OCTET_STRING_TYPE *pRedirectionURL,
                            tHttpRedirect  *pRedirectLink);
INT4    HttpAddRedirectionEntry (tSNMP_OCTET_STRING_TYPE *pRedirectionURL);
tHttpRedirect * 
HttpGetRedirectionEntry (tSNMP_OCTET_STRING_TYPE *pRedirectionURL);


#include "webnmsz.h"

#endif /* _ENM_H */
