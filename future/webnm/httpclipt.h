/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: httpclipt.h,v 1.4 2013/02/12 12:09:38 siva Exp $
 * 
 * Description:  This file includes all the HTTP CLI related
 *               definitions and declarations.
 *********************************************************************/

/* FILE HEADER :
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : fshttpcli.h                                      |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : HTTP configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file includes all the HTTP CLI related      |
 * |                          definitions and declarations.                    |
 * |                                                                           |
 * |                                                                           |
 * -----------------------------------------------------------------------------
 */

#ifndef FSHTTPCLI_H
#define FSHTTPCLI_H

INT4
HttpSetAuthScheme (tCliHandle CliHandle, INT4 i4Scheme);

INT4
HttpSetRedirectionStatus (tCliHandle CliHandle, UINT4 u4Command);

INT4
HttpSetRedirectionSrvIp (tCliHandle CliHandle, UINT1 *pu1Url,
                         UINT1 *pu1SrvAddr, UINT1 u1AddrType);

INT4
HttpSetRedirectionSrvDomainName (tCliHandle CliHandle, UINT1 *pu1Url,
                                 UINT1 *pu1DomainName);

INT4
HttpDelRedirectionEntry (tCliHandle CliHandle, UINT1 *pu1Url);

INT4
HttpShowAuthScheme (tCliHandle CliHandle);

INT4
HttpShowRedirectionEntry (tCliHandle CliHandle, UINT1 *pu1Url);

INT4
HttpClearSrvStats (tCliHandle CliHandle);
#endif
