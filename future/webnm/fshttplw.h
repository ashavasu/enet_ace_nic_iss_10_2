/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttplw.h,v 1.3 2013/02/12 12:09:38 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHttpRedirectionStatus ARG_LIST((INT4 *));

INT1
nmhGetFsOperHttpAuthScheme ARG_LIST((INT4 *));

INT1
nmhGetFsConfigHttpAuthScheme ARG_LIST((INT4 *));

INT1
nmhGetFsHttpRequestCount ARG_LIST((INT4 *));
 
INT1
nmhGetFsHttpRequestDiscards ARG_LIST((INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsHttpRedirectionStatus ARG_LIST((INT4 ));

INT1
nmhSetFsConfigHttpAuthScheme ARG_LIST((INT4 ));

INT1
nmhSetFsHttpRequestCount ARG_LIST((INT4 ));
 
INT1
nmhSetFsHttpRequestDiscards ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsHttpRedirectionStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsConfigHttpAuthScheme ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsHttpRequestCount ARG_LIST((UINT4 *  ,INT4 ));
 
INT1
nmhTestv2FsHttpRequestDiscards ARG_LIST((UINT4 *  ,INT4 ));
 

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsHttpRedirectionStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsConfigHttpAuthScheme ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsHttpRedirectionTable. */
INT1
nmhValidateIndexInstanceFsHttpRedirectionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhDepv2FsHttpRequestCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
 
INT1
nmhDepv2FsHttpRequestDiscards ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Type for Low Level GET FIRST fn for FsHttpRedirectionTable  */

INT1
nmhGetFirstIndexFsHttpRedirectionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsHttpRedirectionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHttpRedirectedSrvAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsHttpRedirectedSrvIP ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsHttpRedirectedSrvDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsHttpRedirectionEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsHttpRedirectedSrvAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsHttpRedirectedSrvIP ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsHttpRedirectedSrvDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsHttpRedirectionEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsHttpRedirectedSrvAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsHttpRedirectedSrvIP ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsHttpRedirectedSrvDomainName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsHttpRedirectionEntryStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsHttpRedirectionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
