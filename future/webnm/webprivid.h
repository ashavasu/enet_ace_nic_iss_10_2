/*$Id: webprivid.h,v 1.4 2014/04/04 10:12:43 siva Exp $ */
/*This is an auto-generated file. So please do not modify anything*/

#ifndef _WEBPRIVID_H_
#define _WEBPRIVID_H_
typedef struct PagePrivMapping {
 INT2        u2CliCmd;
 INT2        u2CliCmdMode;
 INT2        u2UrlName;
 INT2        i2PrivId;
} tPagePrivMapping;

tPagePrivMapping gPagePrivMapping[] =
{
 {
  0,  /*{"clock set hh:mm:ss <day (1-31)> {january|february|march|april|may|june|july|august|september|october|november|december} <year (2000 - 2035)>"},*/
  41,   /*{"USEREXEC"},*/
  0,  /*gen_info.html*/
  0
 },
 {
  1,  /*{"clear config[default-config-restore <filename>]"},*/
  41,   /*{"USEREXEC"},*/
  650,  /*gen_infoclearconfig.html*/
  0
 },
 {
  2,  /*{"set switch temperature {min|max} threshold <celsius (-14 - 40)>}"},*/
  39,   /*{"CONFIGURE"},*/
  1,  /*system_resources.html*/
  0
 },
 {
  3,  /*{"show env {all | temperature | fan | RAM | CPU | flash | power}"},*/
  41,   /*{"USEREXEC"},*/
  2,  /*system_resources_fan.html*/
  0
 },
 {
  206,  /*{"default  mode { manual | dynamic }"},*/
  39,   /*{"CONFIGURE"},*/
  3,  /*nvram_settings.html*/
  0
 },
 {
  4,  /*{"mac-learn-rate {<no of MAC entries(0-2147483647)>} [interval {<milliseconds(1-100000)>}]"},*/
  39,   /*{"CONFIGURE"},*/
  4,  /*protCPUoveloading.html*/
  0
 },
 {
  5,  /*{"shutdown qos"},*/
  39,   /*{"CONFIGURE"},*/
  5,  /*qos_basicsettings.html*/
  0
 },
 {
  6,  /*{"meter <meter-id(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  6,  /*qos_meter.html*/
  0
 },
 {
  6,  /*{"meter <meter-id(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  7,  /*qos_tbparammeter.html*/
  0
 },
 {
  7,  /*{"priority-map <priority-map-Id(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  8,  /*qos_prioritymapsettings.html*/
  0
 },
 {
  8,  /*{"match access-group { [mac-access-list <integer(0-65535)>] [ ip-access-list <integer(0-65535)>]  | priority-map <integer(0-65535)> }"},*/
  0,   /*{"QOSCLSMAP"},*/
  9,  /*qos_classmapsettings.html*/
  0
 },
 {
  9,  /*{"set class <class integer(1-65535)> [pre-color { green | yellow | red | none }] [ regen-priority <integer(0-7)>  group-name <string(31)> ]"},*/
  0,   /*{"QOSCLSMAP"},*/
  10,  /*qos_classtoprisettings.html*/
  0
 },
 {
  10,  /*{"qos interface <iftype> <ifnum> def-user-priority <integer(0-7)>"},*/
  39,   /*{"CONFIGURE"},*/
  11,  /*qos_defaultuserprio.html*/
  0
 },
 {
  11,  /*{"meter-type { simpleTokenBucket | avgRate| srTCM | trTCM | tswTCM | mefCoupled | mefDeCoupled } [ color-mode { aware | blind } ] [interval <short(1-10000)>] [cir <integer(0-10485760)>] [cbs <integer(0-10485760)>] [eir <integer(0-10485760)>] [ebs <integer(0-10485760)>] [next-meter <integer(0-65535)>]"},*/
  1,   /*{"QOSMETER"},*/
  12,  /*qos_metertablesettings.html*/
  0
 },
 {
  12,  /*{"authorized-manager ip-source <ip-address>             [{<subnet-mask> | / <prefix-length(0-32)>}]             [interface [<interface-type <0/a-b, 0/c, ...>]             [<interface-type <0/a-b, 0/c, ...>]]             [vlan <a,b or a-b or a,b,c-d>]             [cpu0]             [service [snmp] [telnet] [http] [https] [ssh]]"},*/
  39,   /*{"CONFIGURE"},*/
  13,  /*ip_auth_mgr.html*/
  0
 },
 {
  13,  /*{"set meter <integer(1-65535)> [conform-action {cos-transmit-set <short(0-7)>|de-transmit-set <short(0-1)>|set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)>|set-port <iftype> <ifnum>|inner-vlan-pri-set <short(0-7)>|inner-vlan-de-set <short(0-1)>|set-inner-vlan-pri <short(0-7)> set-inner-vlan-de <short(0-1)>|set-mpls-exp-transmit <short(0-7)>|set-ip-prec-transmit <short(0-7)>|set-ip-dscp-transmit <short(0-63)>}][ exceed-action {drop|cos-transmit-set <short(0-7)>|de-transmit-set <short(0-1)>|set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)>|inner-vlan-pri-set <short(0-7)|inner-vlan-de-set <short(0-1)>|set-inner-vlan-pri <short(0-7)> set-inner-vlan-de <short(0-1)>|set-mpls-exp-transmit <short(0-7)>|set-ip-prec-transmit <short(0-7)>|set-ip-dscp-transmit <short(0-63)>}][violate-action {drop|cos-transmit-set <short(0-7)>|de-transmit-set <short(0-1)>|set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)>|inner-vlan-pri-set <short(0-7)>|inner-vlan-de-set <short(0-1)>|set-inner-vlan-pri <short(0-7)> set-inner-vlan-de <short(0-1)>|set-mpls-exp-transmit <short(0-7)>|set-ip-prec-transmit <short(0-7)>|set-ip-dscp-transmit <short(0-63)>}][set-conform-newclass <integer(0-65535)>][set-exceed-newclass <integer(0-65535)>][set-violate-newclass <integer(0-65535)>]"},*/
  2,   /*{"QOSPLYMAP"},*/
  14,  /*qos_policymapsettings.html*/
  0
 },
 {
  14,  /*{"port-isolation in_vlan_ID [{add|remove}] port_list"},*/
  42,   /*{"INTERFACE"},*/
  15,  /*portisolation_conf.html*/
  0
 },
 {
  36,  /*{"copy { tftp://ip-address/filename startup-config | sftp://<user-name>:<pass-word>@ip-address/filename startup-config | flash: filename startup-config }"},*/
  41,   /*{"USEREXEC"},*/
  16,  /*save.html*/
  0
 },
 {
  16,  /*{"config-restore {flash | remote ip-addr <ip-address> file <filename> | norestore}"},*/
  41,   /*{"USEREXEC"},*/
  17,  /*restore.html*/
  0
 },
 {
  15,  /*{"erase  { startup-config | nvram: | flash:filename} "},*/
  41,   /*{"USEREXEC"},*/
  18,  /*erase.html*/
  0
 },
 {
  16,  /*{"config-restore {flash | remote ip-addr <ip-address> file <filename> | norestore}"},*/
  41,   /*{"USEREXEC"},*/
  19,  /*remoterestore.html*/
  0
 },
 {
  17,  /*{"copy logs {tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename}"},*/
  41,   /*{"USEREXEC"},*/
  20,  /*log_info.html*/
  0
 },
 {
  18,  /*{"set sntp client {enabled | disabled}"},*/
  3,   /*{"SNTP"},*/
  21,  /*sntp_scalarsconf.html*/
  0
 },
 {
  19,  /*{"set sntp unicast-server {ipv4 <ucast_addr> |ipv6 <ip6_addr> | domain-name <string(64)>} [{primary | secondary}] [version { 3 | 4 }] [port <integer(1025-36564)>]"},*/
  3,   /*{"SNTP"},*/
  22,  /*sntp_unicastconf.html*/
  0
 },
 {
  20,  /*{"set sntp broadcast-mode send-request {enabled | disabled}"},*/
  3,   /*{"SNTP"},*/
  23,  /*sntp_broadcastconf.html*/
  0
 },
 {
  21,  /*{"set sntp multicast-mode send-request {enabled | disabled}"},*/
  3,   /*{"SNTP"},*/
  24,  /*sntp_multicastconf.html*/
  0
 },
 {
  22,  /*{"set sntp manycast-poll-interval [<value (16-16284) seconds>]"},*/
  3,   /*{"SNTP"},*/
  25,  /*sntp_anycastconf.html*/
  0
 },
 {
  23,  /*{"ssh {enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  26,  /*ssh_globalconf.html*/
  0
 },
 {
  24,  /*{"debug ssh ([all] [shut] [mgmt] [data] [ctrl] [dump] [resource] [buffer] [server])"},*/
  41,   /*{"USEREXEC"},*/
  27,  /*ssh_traces.html*/
  0
 },
 {
  25,  /*{"version {all | ssl3 | tls1}"},*/
  39,   /*{"CONFIGURE"},*/
  28,  /*ssl_globalconf.html*/
  0
 },
 {
  26,  /*{"debug ssl ([all] [shut] [mgmt] [data] [ctrl] [dump] [resource] [buffer])"},*/
  41,   /*{"USEREXEC"},*/
  29,  /*ssl_traces.html*/
  0
 },
 {
  27,  /*{"ssl gen cert-req algo rsa sn <SubjectName>"},*/
  41,   /*{"USEREXEC"},*/
  30,  /*ssl_digitalcert.html*/
  0
 },
 {
  28,  /*{"set entity physical-index <integer (1..2147483647)> {[asset-id <SnmpAdminString (Size (1..32))>] [serial-number  <SnmpAdminString (Size (1..32))>] [alias-name <SnmpAdminString (Size (1..32))>] [uris <OCTET-STRING (Size (1..255))>]}"},*/
  39,   /*{"CONFIGURE"},*/
  31,  /*ent_phy.html*/
  0
 },
 {
  29,  /*{"show entity logical [index <integer (1..2147483647)>]"},*/
  41,   /*{"USEREXEC"},*/
  32,  /*ent_log.html*/
  0
 },
 {
  30,  /*{"show entity lp-mapping"},*/
  41,   /*{"USEREXEC"},*/
  33,  /*ent_LPMapping.html*/
  0
 },
 {
  31,  /*{"show entity alias-mapping [index <integer (1..2147483647)>]"},*/
  41,   /*{"USEREXEC"},*/
  34,  /*ent_AliasMapping.html*/
  0
 },
 {
  32,  /*{"show entity phy-containment [index <integer (1..2147483647)>]"},*/
  41,   /*{"USEREXEC"},*/
  35,  /*ent_ContainsMapping.html*/
  0
 },
 {
  33,  /*{"set http authentication-scheme {default | basic | digest}"},*/
  39,   /*{"CONFIGURE"},*/
  36,  /*http_scalarsconf.html*/
  0
 },
 {
  34,  /*{"archive  download-sw  /overwrite [ /reload ] { tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename | flash:filename}"},*/
  41,   /*{"USEREXEC"},*/
  37,  /*upgrade_sw.html*/
  0
 },
 {
  35,  /*{"copy startup-config {flash: filename | tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename}"},*/
  41,   /*{"USEREXEC"},*/
  38,  /*file_upload.html*/
  0
 },
 {
  36,  /*{"copy { tftp://ip-address/filename startup-config | sftp://<user-name>:<pass-word>@ip-address/filename startup-config | flash: filename startup-config }"},*/
  41,   /*{"USEREXEC"},*/
  39,  /*file_download.html*/
  0
 },
 {
  37,  /*{"audit-logging { enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  40,  /*audit_log.html*/
  0
 },
 {
  38,  /*{"reload"},*/
  41,   /*{"USEREXEC"},*/
  41,  /*reboot.html*/
  0
 },
 {
  39,  /*{"tacacs-server host {<ipv4-address> | <ipv6-address> | <host-name>} [single-connection] [port <tcp port (1-65535)>] [timeout <time out in seconds (1-255)>] [key <secret key>]"},*/
  39,   /*{"CONFIGURE"},*/
  42,  /*tacacs_config.html*/
  0
 },
 {
  40,  /*{"debug tacacs { all | info | errors | dumptx | dumprx }"},*/
  41,   /*{"USEREXEC"},*/
  43,  /*tacacs_traces.html*/
  0
 },
 {
  41,  /*{"tacacs use-server address {<ipv4-address> | <ipv6-address>}"},*/
  39,   /*{"CONFIGURE"},*/
  44,  /*tacacs_serverconf.html*/
  0
 },
 {
  42,  /*{"queue-type <Q-Template-Id(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  45,  /*qos_queuetemplate.html*/
  0
 },
 {
  42,  /*{"queue-type <Q-Template-Id(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  46,  /*qos_redconf.html*/
  0
 },
 {
  43,  /*{"shape-template <integer(1-65535)> [cir <integer(1-10485760)>] [cbs <integer(0-10485760)>] [eir <integer(0-10485760)>] [ebs <integer(0-10485760)>]"},*/
  39,   /*{"CONFIGURE"},*/
  47,  /*qos_shapetemplate.html*/
  0
 },
 {
  44,  /*{"scheduler <integer(1-65535)> interface <iftype> <ifnum> [sched-algo {strict-priority | rr | wrr | wfq | strict-rr | strict-wrr | strict-wfq | deficit-rr}] [shaper <integer(0-65535)>] [hierarchy-level <integer(0-10)>]"},*/
  39,   /*{"CONFIGURE"},*/
  48,  /*qos_schedulertable.html*/
  0
 },
 {
  45,  /*{"queue <integer(1-65535)> interface <iftype> <ifnum> [qtype <integer(1-65535)>] [scheduler <integer(1-65535)>] [weight <integer(0-1000)>] [priority <integer(0-15)>] [shaper <integer(0-65535)>] [queue-type {unicast | multicast }]"},*/
  39,   /*{"CONFIGURE"},*/
  49,  /*qos_queuetable.html*/
  0
 },
 {
  46,  /*{"sched-hierarchy interface <iftype> <ifnum> hierarchy-level <integer(1-10)> sched-id <integer(1-65535)> {next-level-queue <integer(0-65535)> | next-level-scheduler <integer(0-65535)>} [priority <integer(0-15)>] [weight <integer(0-1000)>]"},*/
  39,   /*{"CONFIGURE"},*/
  50,  /*qos_hierarchytable.html*/
  0
 },
 {
  47,  /*{"queue-map { CLASS <integer(1-65535)> | regn-priority { vlanPri | ipTos | ipDscp | mplsExp | vlanDEI } <integer(0-63)> } [interface <iftype> <ifnum>] queue-id <integer(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  51,  /*qos_queuemap.html*/
  0
 },
 {
  48,  /*{"cpu rate limit queue <integer(1-65535)> minrate <integer(1-65535)> maxrate <integer(1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  52,  /*qos_stdqueuetable.html*/
  0
 },
 {
  49,  /*{"show qos queue-stats [interface <iftype> <ifnum>]"},*/
  41,   /*{"USEREXEC"},*/
  53,  /*qos_cosstats.html*/
  0
 },
 {
  50,  /*{"show qos meter-stats [<Meter-Id(1-65535)>]"},*/
  41,   /*{"USEREXEC"},*/
  54,  /*qos_policerstats.html*/
  0
 },
 {
  51,  /*{"shutdown cn"},*/
  39,   /*{"CONFIGURE"},*/
  55,  /*cn_sys_control.html*/
  0
 },
 {
  52,  /*{"set cn {enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  56,  /*cn_global_settings.html*/
  0
 },
 {
  53,  /*{"cn cnpv <priority-value(0-7)>"},*/
  39,   /*{"CONFIGURE"},*/
  57,  /*cn_comp_settings.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  58,  /*cn_port_settings.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  59,  /*cn_cp_settings.html*/
  0
 },
 {
  54,  /*{"clear cn counters [ { {interface <ifXtype> <ifnum>} | {switch <context name> } } ]"},*/
  38,   /*{"EXEC"},*/
  60,  /*cn_statistics.html*/
  0
 },
 {
  55,  /*{"cn trap [error-port-table] [cnm]"},*/
  39,   /*{"CONFIGURE"},*/
  61,  /*cn_debug.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  62,  /*dcbx_adminstatus.html*/
  0
 },
 {
  56,  /*{"debug dcbx {all |[mgmt] [sem] [tlv] [resource] [fail] [control-plane]}"},*/
  41,   /*{"USEREXEC"},*/
  63,  /*dcbx_globaltraces.html*/
  0
 },
 {
  57,  /*{"shutdown ets"},*/
  39,   /*{"CONFIGURE"},*/
  64,  /*ets_globalInfo.html*/
  0
 },
 {
  58,  /*{"set  priority-grouping   {enable | disable}"},*/
  8,   /*{"INTERFACE "},*/
  65,  /*ets_adminportconfig.html*/
  0
 },
 {
  59,  /*{"set priority-group bandwidth <prioritygroup-bw0> <prioritygroup-bw1>                 <prioritygroup-bw2> <prioritygroup-bw3> <prioritygroup-bw4>                   <prioritygroup-bw5> <prioritygroup-bw6> <prioritygroup-bw7> "},*/
  8,   /*{"INTERFACE "},*/
  66,  /*ets_bandwidthConfiguration.html */
  0
 },
 {
  60,  /*{"map priority <priority-list> priority-group <integer(0-7) | 15> "},*/
  8,   /*{"INTERFACE "},*/
  67,  /*ets_priToPGIDMapping.html*/
  0
 },
 {
  61,  /*{"set traffic class <tc-group-list> traffic selection algorithm { strict-priority-algorithm | credit-based-shaper-algorithm | enhanced-transmission-selection-algorithm | vendor-specific-algorithm}"},*/
  8,   /*{"INTERFACE "},*/
  68,  /*ets_tsaConfig.html*/
  0
 },
 {
  62,  /*{"show interfaces [<ifXtype> <ifnum> ] priority-grouping [detail]"},*/
  41,   /*{"USEREXEC"},*/
  69,  /*ets_localInfo.html*/
  0
 },
 {
  62,  /*{"show interfaces [<ifXtype> <ifnum> ] priority-grouping [detail]"},*/
  41,   /*{"USEREXEC"},*/
  70,  /*ets_remoteInfo.html*/
  0
 },
 {
  63,  /*{"show interfaces priority-grouping  counters [ <ifXtype> <ifnum> ]"},*/
  41,   /*{"USEREXEC"},*/
  71,  /*ets_portStats.html*/
  0
 },
 {
  64,  /*{"shutdown pfc"},*/
  39,   /*{"CONFIGURE"},*/
  72,  /*pfc_GlobalInfo.html*/
  0
 },
 {
  65,  /*{"set priority-flow-control  {enable | disable}"},*/
  8,   /*{"INTERFACE "},*/
  73,  /*pfc_adminportconfig.html*/
  0
 },
 {
  66,  /*{"set priority <priority-list> flow-control {enable|disable}"},*/
  8,   /*{"INTERFACE "},*/
  74,  /*pfc_statusPerPriority.html*/
  0
 },
 {
  67,  /*{"show interfaces [<ifXtype> <ifnum> ] priority-flow-control [detail]"},*/
  41,   /*{"USEREXEC"},*/
  75,  /*pfc_localInfo.html*/
  0
 },
 {
  67,  /*{"show interfaces [<ifXtype> <ifnum> ] priority-flow-control [detail]"},*/
  41,   /*{"USEREXEC"},*/
  76,  /*pfc_remoteInfo.html*/
  0
 },
 {
  68,  /*{"show interfaces priority-flow-control  counters [ <ifXtype> <ifnum> ]"},*/
  41,   /*{"USEREXEC"},*/
  77,  /*pfc_portStat.html*/
  0
 },
 {
  69,  /*{"shutdown application-priority"},*/
  39,   /*{"CONFIGURE"},*/
  78,  /*ap_GlobalInfo.html*/
  0
 },
 {
  70,  /*{"set application-priority  {enable | disable}"},*/
  8,   /*{"INTERFACE "},*/
  79,  /*ap_adminportconfig.html*/
  0
 },
 {
  71,  /*{"set selector <selector-field> protocol <protocol-id> priority <priority>"},*/
  41,   /*{"USEREXEC"},*/
  80,  /*ap_applicationToPriority.html*/
  0
 },
 {
  72,  /*{"show interfaces [<ifXtype> <ifnum> ] application-priority [detail]"},*/
  41,   /*{"USEREXEC"},*/
  81,  /*ap_localInfo.html*/
  0
 },
 {
  72,  /*{"show interfaces [<ifXtype> <ifnum> ] application-priority [detail]"},*/
  41,   /*{"USEREXEC"},*/
  82,  /*ap_remoteInfo.html*/
  0
 },
 {
  73,  /*{"enable snmpsubagent { master { ip4 <ipv4_address>      | ip6 <ip6_addr>            } [port <number>] [context <context-name>]}"},*/
  39,   /*{"CONFIGURE"},*/
  83,  /*snmp_homepage.html*/
  0
 },
 {
  74,  /*{"snmp community index <CommunityIndex> name <CommunityName>              security <SecurityName> [context <name>]              [{volatile | nonvolatile}] [transporttag <TransportTagIdentifier | none>] [contextengineid <ContextEngineID>]"},*/
  39,   /*{"CONFIGURE"},*/
  84,  /*snmp_communityconf.html*/
  0
 },
 {
  75,  /*{"snmp group <GroupName> user <UserName>               security-model {v1 | v2c | v3 } [{volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  85,  /*snmp_groupconf.html*/
  0
 },
 {
  76,  /*{"snmp access <GroupName> {v1 | v2c | v3 {auth | noauth | priv}}              [read <ReadView | none>] [write <WriteView | none>]              [notify <NotifyView | none>] [{volatile | nonvolatile}] [context <string(32)>]"},*/
  39,   /*{"CONFIGURE"},*/
  86,  /*snmp_accessconf.html*/
  0
 },
 {
  77,  /*{"snmp view <ViewName> <OIDTree> [mask <OIDMask>] {included | excluded}              [{volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  87,  /*snmp_viewconf.html*/
  0
 },
 {
  78,  /*{"snmp targetaddr <TargetAddressName> param <ParamName>              {<IPAddress> | <IP6Address>} [timeout <Seconds(1-1500)]              [retries <RetryCount(1-3)] [taglist <TagIdentifier | none>]              [{volatile | nonvolatile}] [port <integer (1-65535)>]"},*/
  39,   /*{"CONFIGURE"},*/
  88,  /*snmp_tgtaddrconf.html*/
  0
 },
 {
  79,  /*{"snmp targetparams <ParamName> user <UserName>              security-model {v1 | v2c | v3 {auth | noauth | priv}}              message-processing {v1 | v2c | v3} [{volatile | nonvolatile}]              [filterprofile-name <profilename> ] [filter-storagetype {volatile                | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  89,  /*snmp_tgtparamconf.html*/
  0
 },
 {
  79,  /*{"snmp targetparams <ParamName> user <UserName>              security-model {v1 | v2c | v3 {auth | noauth | priv}}              message-processing {v1 | v2c | v3} [{volatile | nonvolatile}]              [filterprofile-name <profilename> ] [filter-storagetype {volatile                | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  90,  /*snmp_filterprof.html*/
  0
 },
 {
  80,  /*{"snmp user <UserName> [auth {md5 | sha} <passwd>              [priv {{{DES | AES_CFB128}  <passwd> } | None}]] [{volatile | nonvolatile}] [EngineId <EngineID>]"},*/
  39,   /*{"CONFIGURE"},*/
  91,  /*snmp_userconf.html*/
  0
 },
 {
  81,  /*{"snmp notify <NotifyName> tag <TagName> type {Trap | Inform}              [{volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  92,  /*snmp_notifconf.html*/
  0
 },
 {
  82,  /*{"snmp filterprofile <profile-name> <OIDTree> [mask <OIDMask>]               {included | excluded} [{volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  93,  /*snmp_filterconf.html*/
  0
 },
 {
  83,  /*{"snmp proxy name <ProxyName> ProxyType {Read | Write | inform | Trap} ContextEngineID <EngineId> TargetParamsIn <TargetParam> TargetOut <TargetOut> [ContextName <ProxyContextName>] [StorageType {volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  94,  /*snmp_proxy.html*/
  0
 },
 {
  84,  /*{"snmp mibproxy name <ProxyName> ProxyType {Read | Write | inform | Trap} mibid <MibId> TargetParamsIn <TargetParam> TargetOut <TargetOut> [StorageType {volatile | nonvolatile}]"},*/
  39,   /*{"CONFIGURE"},*/
  95,  /*snmp_mibproxy.html*/
  0
 },
 {
  85,  /*{"snmp-server trap tcp-port <port>"},*/
  39,   /*{"CONFIGURE"},*/
  96,  /*snmp_basicsettings.html*/
  0
 },
 {
  86,  /*{"show snmp agentx information"},*/
  41,   /*{"USEREXEC"},*/
  97,  /*snmp_agentx_conf.html*/
  0
 },
 {
  87,  /*{"show snmp agentx statistics"},*/
  41,   /*{"USEREXEC"},*/
  98,  /*snmp_agentx_statistics.html*/
  0
 },
 {
  88,  /*{"show snmp"},*/
  41,   /*{"USEREXEC"},*/
  99,  /*snmp_statistics.html*/
  0
 },
 {
  89,  /*{"syslog relay"},*/
  39,   /*{"CONFIGURE"},*/
  100,  /*bsdsyslog_scalarsconf.html*/
  0
 },
 {
  90,  /*{"logging   { buffered <size (1-200)>  | console  |           facility {local0 | local1 | local2 | local3 | local4 | local5 | local6 | local7| } |          severity  [{ <level (0-7)> | alerts | critical | debugging | emergencies | errors |           informational | notification | warnings }] | on }"},*/
  39,   /*{"CONFIGURE"},*/
  101,  /*bsdsyslog_loggingconf.html*/
  0
 },
 {
  91,  /*{"logging-file <short(0-191)> <string(32)>"},*/
  39,   /*{"CONFIGURE"},*/
  102,  /*bsdsyslog_filetable.html*/
  0
 },
 {
  92,  /*{"mail-server <short(0-191)> {ipv4 <ucast_addr> |ipv6 <ip6_addr> | <host-name>} <string(50)> [user <user_name> password <password>]"},*/
  39,   /*{"CONFIGURE"},*/
  103,  /*bsdsyslog_mailtable.html*/
  0
 },
 {
  93,  /*{"logging-server <short(0-191)> {ipv4 <ucast_addr> | ipv6 <ip6_addr> | <host-name>} [ port <integer(0-65535)>] [{udp | tcp | beep}]"},*/
  39,   /*{"CONFIGURE"},*/
  104,  /*bsdsyslog_fwdtable.html*/
  0
 },
 {
  93,  /*{"logging-server <short(0-191)> {ipv4 <ucast_addr> | ipv6 <ip6_addr> | <host-name>} [ port <integer(0-65535)>] [{udp | tcp | beep}]"},*/
  39,   /*{"CONFIGURE"},*/
  105,  /*beep_scalarsconf.html*/
  0
 },
 {
  94,  /*{"set hitless-restart enable"},*/
  38,   /*{"EXEC"},*/
  106,  /*hrconfig.html*/
  0
 },
 {
  518,  /*{"switch <name>"},*/
  39,   /*{"CONFIGURE"},*/
  107,  /*context_creation.html*/
  0
 },
 {
  95,  /*{"bridge-mode {customer | provider | provider-core | provider-edge | provider-backbone-icomp |provider-backbone-bcomp}"},*/
  33,   /*{"VCM"},*/
  108,  /*vcm_bridgemodeselection.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  109,  /*port_creation.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  110,  /*vcm_portmap.html*/
  0
 },
 {
  96,  /*{"map switch <name>"},*/
  8,   /*{"INTERFACE "},*/
  111,  /*context_selection.html*/
  0
 },
 {
  97,  /*{"bridge port-type { providerNetworkPort | customerNetworkPort {port-based | s-tagged| c-tagged} | customerEdgePort | propCustomerEdgePort | propCustomerNetworkPort | propProviderNetworkPort | customerBridgePort| customerBackbonePort }"},*/
  42,   /*{"INTERFACE"},*/
  112,  /*port_settings.html*/
  0
 },
 {
  98,  /*{"monitor session <session-id (1-20)> { source              {interface <interface-type> <interface-id> [{rx|tx|both}] |              tunnel <tunnel-id> [{rx|tx|both}]|              vlan <vlan_range> [switch <context_name> ][{rx|tx|both}]              |mac-acl <acl-id> |ip-acl <acl-id>|remote vlan <vlan-id>              [switch <context_name>]}} [ COMP ] "},*/
  39,   /*{"CONFIGURE"},*/
  113,  /*port_monitoring.html*/
  0
 },
 {
  99,  /*{"vlan map-priority <priority value(0-7)>              traffic-class <Traffic class value(0-7)>"},*/
  42,   /*{"INTERFACE"},*/
  114,  /*vlan_trafficclass.html*/
  0
 },
 {
  100,  /*{"negotiation"},*/
  8,   /*{"INTERFACE "},*/
  115,  /*port_control.html*/
  0
 },
 {
  497,  /*{"shutdown garp"},*/
  39,   /*{"CONFIGURE"},*/
  116,  /*vlan_basicinfo.html*/
  0
 },
 {
  101,  /*{"debug vlan { global | [{fwd | priority | redundancy}              [initshut] [mgmt] [data] [ctpl] [dump] [os] [failall] [buffer] [all]] [switch <context_name>] }"},*/
  41,   /*{"USEREXEC"},*/
  117,  /*vlan_traces.html*/
  0
 },
 {
  498,  /*{"debug garp { global | [{protocol | gmrp | gvrp | redundancy}              [initshut] [mgmt] [data] [ctpl] [dump] [os] [failall] [buffer] [all]] [switch <context_name>] }"},*/
  41,   /*{"USEREXEC"},*/
  625,  /*vlangarp_traces.html*/
  0
 },
 {
  102,  /*{"port mac-vlan"},*/
  42,   /*{"INTERFACE"},*/
  118,  /*vlan_pvidsetting.html*/
  0
 },
 {
  103,  /*{"ports [add] ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [pw <a,b,c-d>])        [untagged (<interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>] [all])]         [forbidden <interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>][ac <a,b,c-d>]]       [name <vlan-name>]"},*/
  37,   /*{"VLAN_CONFIG"},*/
  119,  /*vlan_staticconf.html*/
  0
 },
 {
  104,  /*{"map protocol              {ip | novell | netbios | appletalk | other <aa:aa or aa:aa:aa:aa:aa>}              {enet-v2 | snap | llcOther | snap8021H | snapOther}              protocols-group <Group id integer(0-2147483647)>"},*/
  39,   /*{"CONFIGURE"},*/
  120,  /*vlan_protgrp.html*/
  0
 },
 {
  105,  /*{"switchport map protocols-group <Group id integer(0-2147483647)> vlan <vlan-id/vfi_id>"},*/
  42,   /*{"INTERFACE"},*/
  121,  /*vlan_portvidset.html*/
  0
 },
 {
  106,  /*{"mac-map <aa:aa:aa:aa:aa:aa> vlan <vlan-id/vfi_id> [mcast-bcast {discard | allow}]"},*/
  42,   /*{"INTERFACE"},*/
  122,  /*vlan_portmacmap.html*/
  0
 },
 {
  107,  /*{"set unicast-mac learning { enable | disable | default}"},*/
  37,   /*{"VLAN_CONFIG"},*/
  123,  /*vlan_unicastmac.html*/
  0
 },
 {
  108,  /*{"wildcard {mac-adddress <mac_addr> | broadcast} interface                ([<interface-type> <0/a-b, 0/c, ...>]               [<interface-type> <0/a-b, 0/c, ...>] [port-channel <a,b,c-d>]               [pw <a,b,c-d>] [ac <a,b,c-d>])"},*/
  39,   /*{"CONFIGURE"},*/
  124,  /*vlan_wildcard_settings.html*/
  0
 },
 {
  109,  /*{"switchport filtering-utility-criteria {default | enhanced}"},*/
  42,   /*{"INTERFACE"},*/
  125,  /*vlan_switchport_filtering.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  126,  /*vlan_fdbflush.html*/
  0
 },
 {
  110,  /*{"map subnet <ip-subnet-address> vlan <vlan-id/vfi_id>              [arp {suppress | allow}] [mask <subnet-mask>]"},*/
  42,   /*{"INTERFACE"},*/
  127,  /*vlan_subnetipmap.html*/
  0
 },
 {
  111,  /*{"set gvrp { enable | disable }"},*/
  33,   /*{"VCM"},*/
  128,  /*vlan_gvrpbasicconf.html*/
  0
 },
 {
  113,  /*{"set port gvrp <interface-type> <interface-id> { enable | disable }"},*/
  33,   /*{"VCM"},*/
  129,  /*vlan_gvrpportconf.html*/
  0
 },
 {
  112,  /*{"set garp timer {join | leave | leaveall} <time in milli seconds>"},*/
  42,   /*{"INTERFACE"},*/
  130,  /*vlan_garptimersconf.html*/
  0
 },
 {
  113,  /*{"set port gvrp <interface-type> <interface-id> { enable | disable }"},*/
  33,   /*{"VCM"},*/
  131,  /*vlan_gvrpconf.html*/
  0
 },
 {
  153,  /*{"shutdown spanning-tree"},*/
  39,   /*{"CONFIGURE"},*/
  132,  /*mstp_globalconf.html*/
  0
 },
 {
  120,  /*{"debug spanning-tree { global | { all | errors |init-shut | management | memory | bpdu                | events | timer | state-machine { port-info | port-recieve                          | port-role-selection | role-transition                          | state-transition | protocol-migration                          | topology-change | port-transmit                           | bridge-detection | pseudoInfo} | redundancy | sem-variables} [switch <context_name>]}"},*/
  38,   /*{"EXEC"},*/
  133,  /*mstp_traces.html*/
  0
 },
 {
  114,  /*{"spanning-tree mst max-hops <value(6-40)>"},*/
  39,   /*{"CONFIGURE"},*/
  134,  /*mstp_timersconf.html*/
  0
 },
 {
  154,  /*{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},*/
  8,   /*{"INTERFACE "},*/
  135,  /*mstp_portconf.html*/
  0
 },
 {
  115,  /*{"instance <instance-id(1-64|4094)> vlan <vlan-range>"},*/
  4,   /*{"VCMMST"},*/
  136,  /*msti_vlanmap.html*/
  0
 },
 {
  116,  /*{"spanning-tree mst <instance-id(1-64)> { cost <value(1-200000000)>| port-priority <value(0-240)> | disable }"},*/
  4,   /*{"VCMMST"},*/
  137,  /*msti_portconf.html*/
  0
 },
 {
  117,  /*{"show spanning-tree mst [<instance-id(1-64|4094)>] [detail] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  138,  /*mstp_cistportstatus.html*/
  0
 },
 {
  118,  /*{"spanning-tree mst {instance-id <instance-id(1-64)>} root {primary | secondary} "},*/
  33,   /*{"VCM"},*/
  139,  /*msti_bridgeconf.html*/
  0
 },
 {
  119,  /*{"spanning-tree flush-interval <centi-seconds (0-500)>"},*/
  33,   /*{"VCM"},*/
  140,  /*rstp_globalconf.html*/
  0
 },
 {
  120,  /*{"debug spanning-tree { global | { all | errors |init-shut | management | memory | bpdu                | events | timer | state-machine { port-info | port-recieve                          | port-role-selection | role-transition                          | state-transition | protocol-migration                          | topology-change | port-transmit                           | bridge-detection | pseudoInfo} | redundancy | sem-variables} [switch <context_name>]}"},*/
  38,   /*{"EXEC"},*/
  141,  /*rstp_traces.html*/
  0
 },
 {
  121,  /*{"spanning-tree [mst <instance-id>] priority <value(0-61440)>"},*/
  39,   /*{"CONFIGURE"},*/
  142,  /*rstp_timersconf.html*/
  0
 },
 {
  154,  /*{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},*/
  8,   /*{"INTERFACE "},*/
  143,  /*rstp_portconf.html*/
  0
 },
 {
  122,  /*{"show spanning-tree interface <interface-type> <interface-id> [{ cost | priority | portfast | rootcost | restricted-role | restricted-tcn | state | stats | detail }] "},*/
  38,   /*{"EXEC"},*/
  144,  /*rstp_portstatus.html*/
  0
 },
 {
  416,  /*{"show spanning-tree detail [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  145,  /*rstp_portinfo.html*/
  0
 },
 {
  123,  /*{"shutdown port-channel"},*/
  39,   /*{"CONFIGURE"},*/
  146,  /*la_aggreconf.html*/
  0
 },
 {
  124,  /*{"set port-channel { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  147,  /*la_interfacesettings.html*/
  0
 },
 {
  125,  /*{"channel-group <channel-group-number(1-65535)> mode                                                                {auto [non-silent] | desirable [non-silent] | on | active | passive }"},*/
  8,   /*{"INTERFACE "},*/
  148,  /*la_portchannel.html*/
  0
 },
 {
  126,  /*{"lacp port-priority <0-65535>"},*/
  8,   /*{"INTERFACE "},*/
  149,  /*la_portconf.html*/
  0
 },
 {
  127,  /*{"show interfaces [<interface-type> <interface-id> ] etherchannel "},*/
  41,   /*{"USEREXEC"},*/
  150,  /*la_portstateinfo.html*/
  0
 },
 {
  128,  /*{"port-channel load-balance ([src-mac][dest-mac][src-dest-mac][src-ip][dest-ip][src-dest-ip]              [vlan-id][service-instance][mac-src-vid][mac-dest-vid][mac-src-dest-vid][dest-ip6][src-ip6]              [l3-protocol][dest-l4-port][src-l4-port][mpls-vc-label][mpls-tunnel-label][mpls-vc-tunnel-label])               [ <port-channel-index(1-65535)>]"},*/
  39,   /*{"CONFIGURE"},*/
  151,  /*la_policyconf.html*/
  0
 },
 {
  129,  /*{"show d-lag [<port-channel(1-65535)>] {detail}"},*/
  41,   /*{"USEREXEC"},*/
  152,  /*la_remoteinterfacesettings*/
  0
 },
 {
  129,  /*{"show d-lag [<port-channel(1-65535)>] {detail}"},*/
  41,   /*{"USEREXEC"},*/
  153,  /*la_remoteportsettings.html*/
  0
 },
 {
  422,  /*{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},*/
  41,   /*{"USEREXEC"},*/
  154,  /*la_interfacestats.html*/
  0
 },
 {
  130,  /*{"shutdown lldp"},*/
  39,   /*{"CONFIGURE"},*/
  155,  /*lldp_globalconf.html*/
  0
 },
 {
  131,  /*{"debug lldp [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [neigh-add] [neigh-del] [neigh-updt] [neigh-drop] [neigh-ageout] [critical][tlv {all | [chassis-id][port-id] [ttl] [port-descr] [sys-name] [sys-descr] [sys-capab] [mgmt-addr] [port-vlan] [ppvlan] [vlan-name] [proto-id] [mac-phy] [pwr-mdi] [lagg] [max-frame]}] [redundancy]}]"},*/
  38,   /*{"EXEC"},*/
  156,  /*lldp_traceconf.html*/
  0
 },
 {
  132,  /*{"lldp transmit-interval <seconds(5-32768)>"},*/
  39,   /*{"CONFIGURE"},*/
  157,  /*lldp_basicsettings.html*/
  0
 },
 {
  133,  /*{"lldp {transmit | receive}"},*/
  5,   /*{"PSW"},*/
  158,  /*lldp_interfaces.html*/
  0
 },
 {
  134,  /*{"show lldp neighbors [chassis-id <string(255)> port-id <string(255)>] [<interface-type> <interface-id>][detail]"},*/
  38,   /*{"EXEC"},*/
  159,  /*lldp_neighbors.html*/
  0
 },
 {
  135,  /*{"lldp dest-mac <mac_addr>"},*/
  36,   /*{"REDUNDANCY"},*/
  160,  /*lldp_agentinfo.html*/
  0
 },
 {
  136,  /*{"lldp tlv-select dot3tlv { [macphy-config] [link-aggregation] [max-framesize] }"},*/
  36,   /*{"REDUNDANCY"},*/
  161,  /*lldp_agentdetails.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  162,  /*pbvlan_portinfo.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  163,  /*pbvlan_vidtransl.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  164,  /*pbvlan_cvidregi.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  165,  /*pbvlan_pepconf.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  166,  /*pbvlan_priorityregen.html*/
  0
 },
 {
  137,  /*{"l2protocol-tunnel cos <cos-value(0-7)>"},*/
  39,   /*{"CONFIGURE"},*/
  167,  /*pbtunnel_macconfig.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  168,  /*pbtunnel_statusconfig.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  169,  /*pb_portconf.html*/
  0
 },
 {
  138,  /*{"pcp-encoding <pcpselectionrow(8P0D|7P1D|6P2D|5P3D)> priority     <decoded priority (0-7)> drop-eligible {true|false} pcp <encoding priority (0-7)>"},*/
  8,   /*{"INTERFACE "},*/
  170,  /*pbvlan_pcpencoding.html*/
  0
 },
 {
  139,  /*{"pcp-decoding <pcpselectionrow(8P0D|7P1D|6P2D|5P3D)> pcp <recv    priority (0-7)> priority <decoding priority (0-7)> drop-eligible {true|false}"},*/
  8,   /*{"INTERFACE "},*/
  171,  /*pbvlan_pcpdecoding.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  172,  /*pb_ethertypeswaptable.html*/
  0
 },
 {
  140,  /*{"vlan <vlan-id/vfi_id>"},*/
  39,   /*{"CONFIGURE"},*/
  173,  /*pb_servtypeconf.html*/
  0
 },
 {
  141,  /*{"shutdown dot1x"},*/
  39,   /*{"CONFIGURE"},*/
  174,  /*pnac_basicinfo.html*/
  0
 },
 {
  142,  /*{"debug dot1x {all | errors | events | packets |  state-machine | redundancy | registry}"},*/
  41,   /*{"USEREXEC"},*/
  175,  /*pnac_traces.html*/
  0
 },
 {
  143,  /*{"dot1x port-control {auto|force-authorized|force-unauthorized}"},*/
  42,   /*{"INTERFACE"},*/
  176,  /*pnac_portconfig.html*/
  0
 },
 {
  144,  /*{"dot1x timeout {quiet-period <value (0-65535)> | {reauth-period  | server-timeout  | supp-timeout  | tx-period  | start-period  | held-period  | auth-period }<value (1-65535)>}"},*/
  42,   /*{"INTERFACE"},*/
  177,  /*pnac_porttimer.html*/
  0
 },
 {
  145,  /*{"dot1x local-database <username> password <password> permission {allow | deny} [<auth-timeout (value(1-7200))>] [interface <interface-type> <interface-list>]"},*/
  39,   /*{"CONFIGURE"},*/
  178,  /*pnac_localAS.html*/
  0
 },
 {
  146,  /*{"radius-server host {ipv4-address |ipv6-address | host-name} [auth-port <integer(1-65535)>] [acct-port <integer(1-65535)>] [timeout <1-120>] [retransmit <1-254>] [key <secret-key-string>] [primary]"},*/
  39,   /*{"CONFIGURE"},*/
  179,  /*pnac_radClient.html*/
  0
 },
 {
  147,  /*{"debug radius {all | errors | events | packets | responses | timers}"},*/
  41,   /*{"USEREXEC"},*/
  180,  /*radius_traces.html*/
  0
 },
 {
  426,  /*{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},*/
  41,   /*{"USEREXEC"},*/
  181,  /*pnac_macsessioninfo.html*/
  0
 },
 {
  148,  /*{"aaa authentication dot1x default { group {radius | tacacsplus | tacacs+} | local }"},*/
  39,   /*{"CONFIGURE"},*/
  182,  /*pnac_portsecurity.html*/
  0
 },
 {
  149,  /*{"fid <integer(1-4094)> vlan <vlan-range>"},*/
  39,   /*{"CONFIGURE"},*/
  183,  /*l2_ucastfilter.html*/
  0
 },
 {
  150,  /*{"mac-address-table static               multicast <aa:aa:aa:aa:aa:aa> vlan <vlan-id/vfi_id>              [recv-port <ifXtype> <ifnum>]               [{recv-port <ifXtype> <ifnum> | service-instance <integer(256-16777214)>}]              interface ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>][pw <a,b,c-d>] [ac <a,b,c-d>]])      [forbidden-ports ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>][pw <a,b,c-d>][ac <a,b,c-d>]]) [status { permanent | deleteOnReset | deleteOnTimeout }]"},*/
  39,   /*{"CONFIGURE"},*/
  184,  /*l2_mcastfilter.html*/
  0
 },
 {
  151,  /*{"forward-all ([static-ports ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>] [none])]     [forbidden-ports <interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>]])"},*/
  37,   /*{"VLAN_CONFIG"},*/
  185,  /*vlan_forwardconf.html*/
  0
 },
 {
  152,  /*{"monitor session <session-id (1-20)> { source              {interface <interface-type> <interface-id> [{rx|tx|both}] |              tunnel <tunnel-id> [{rx|tx|both}]|              vlan <vlan_range> [switch <context_name> ][{rx|tx|both}]              |mac-acl <acl-id> |ip-acl <acl-id>|remote vlan <vlan-id>              [switch <context_name>]}} [ COMP ]"},*/
  39,   /*{"CONFIGURE"},*/
  186,  /*mirror_ctrlext.html*/
  0
 },
 {
  153,  /*{"shutdown spanning-tree"},*/
  39,   /*{"CONFIGURE"},*/
  187,  /*pvrst_basicconf.html*/
  0
 },
 {
  154,  /*{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},*/
  6,   /*{"PO "},*/
  188,  /*pvrst_portconf.html*/
  0
 },
 {
  155,  /*{"spanning-tree vlan <vlan-id/vfi_id> {forward-time <seconds(4-30)> | hello-time <seconds(1-10)> | max-age <seconds(6-40)> | hold-count <integer(1-10)> | brg-priority <integer(0-61440)> | root {primary | secondary}}"},*/
  33,   /*{"VCM"},*/
  189,  /*pvrst_instbrigconf.html*/
  0
 },
 {
  156,  /*{"spanning-tree vlan <vlan-id/vfi_id> status {disable | enable}"},*/
  8,   /*{"INTERFACE "},*/
  190,  /*pvrst_instportconf.html*/
  0
 },
 {
  420,  /*{"show spanning-tree vlan <vlan-id/vfi-id> interface <ifXtype> <ifnum> [{ cost | detail | priority | rootcost | state | stats }]"},*/
  38,   /*{"EXEC"},*/
  191,  /*pvrst_instportstatus.html*/
  0
 },
 {
  157,  /*{"shutdown mrp"},*/
  39,   /*{"CONFIGURE"},*/
  192,  /*mrp_instance.html*/
  0
 },
 {
  158,  /*{"set port { mrp | mvrp } <interface-type> <interface-id>               periodictimer { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  193,  /*mrp_portsettings.html*/
  0
 },
 {
  159,  /*{"set { mvrp | mmrp } { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  194,  /*mrp_mvrpstatus.html*/
  0
 },
 {
  159,  /*{"set { mvrp | mmrp } { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  195,  /*mrp_mmrpstatus.html*/
  0
 },
 {
  160,  /*{"set port mrp <interface-type> <interface-id> timer { join | leave |             leaveall } <time in centi seconds>"},*/
  39,   /*{"CONFIGURE"},*/
  196,  /*mrp_timesettings.html*/
  0
 },
 {
  161,  /*{"set port mvrp <interface-type> <interface-id> applicant               { normal | non-participant | active }"},*/
  39,   /*{"CONFIGURE"},*/
  197,  /*mrp_applicantcontrol.html*/
  0
 },
 {
  162,  /*{"clear { mrp | mvrp | mmrp } statistics { all |               port <interface-type> <interface-id> }"},*/
  39,   /*{"CONFIGURE"},*/
  198,  /*mrp_clearstat.html*/
  0
 },
 {
  163,  /*{"shutdown ethernet lmi"},*/
  39,   /*{"CONFIGURE"},*/
  199,  /*elmi_basicconf.html*/
  0
 },
 {
  164,  /*{"ethernet lmi {n391 <value(1-65000)>|n393 <value(2-10)>|t391 <value(5-30)>|t392 <value(0,5-30)>}"},*/
  8,   /*{"INTERFACE "},*/
  200,  /*elmi_portconf.html*/
  0
 },
 {
  165,  /*{"ethernet cfm default-domain global ([level <level(0-7)] [mip-creation-criteria {none | explicit | default}] [sender-id-permission {none | chassis | manage | chassis-mgt-address}])"},*/
  7,   /*{"VCM "},*/
  201,  /*ecfm_globalconf.html*/
  0
 },
 {
  166,  /*{"show ethernet cfm mip-ccm-database [service <service-name> | vlan <vlan-id/vfi-id>] [address <aa:aa:aa:aa:aa:aa>] [interface <interface-type> <interface-id>][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  202,  /*ecfm_mipccmdb.html*/
  0
 },
 {
  167,  /*{"ethernet cfm error-log [enable | disable] [size <entries(1-4096)>]"},*/
  7,   /*{"VCM "},*/
  203,  /*ecfm_miscconf.html*/
  0
 },
 {
  168,  /*{"ethernet cfm enable"},*/
  8,   /*{"INTERFACE "},*/
  204,  /*ecfm_portconf.html*/
  0
 },
 {
  169,  /*{"ethernet cfm default-domain vlan <vlan-id/vfi-id> ([level <level (0-7)>] [mip-creation-criteria {none | explicit | defer | default}] [sender-id-permission {none |chassis | manage | chassis-mgt-address | defer }])"},*/
  7,   /*{"VCM "},*/
  205,  /*ecfm_defmdconf.html*/
  0
 },
 {
  170,  /*{"ethernet cfm domain [format {dns-like-name | mac-addr | char-string | none}] name <domain-name> level <level-id(0-7)> "},*/
  39,   /*{"CONFIGURE"},*/
  206,  /*ecfm_mdconf.html*/
  0
 },
 {
  171,  /*{"service [format {primary-vid | char-string | unsigned-int16 | rfc2865-vpn-id | icc}] name <service_name> [icc <icc_code> umc <umc_code>] [{vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>] [mip-creation-criteria {none | default | explicit | defer}] [sender-id-permission {none | chassis | manage | chassis-mgt-address | defer}]"},*/
  9,   /*{"DOMAIN_CONFIG"},*/
  207,  /*ecfm_maconf.html*/
  0
 },
 {
  172,  /*{"show ethernet cfm maintenance-points remote crosscheck [mpid <mep-id(1-8191)>] [domain <domain-name> | level <level-id(0-7)>]  [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance     <service-instance (256-16777214)>}][switch <context_name)>]"},*/
  38,   /*{"EXEC"},*/
  208,  /*ecfm_mameps.html*/
  0
 },
 {
  173,  /*{"show ethernet cfm maintenance-points remote detail {mpid <mep-id(1-8191)> | mac <aa:aa:aa:aa:aa:aa> }[domain <domain-name> | level <level-id(0-7)> [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  209,  /*ecfm_rmeplist.html*/
  0
 },
 {
  174,  /*{"ethernet cfm associate vlan-id <a,b,c-d> primary-vlan-id <vlan-id/vfi-id>"},*/
  33,   /*{"VCM"},*/
  210,  /*ecfm_vlan.html*/
  0
 },
 {
  175,  /*{"ethernet cfm mep {domain <domain-name> | level <0-7>} [inward] mpid <id(1-8191)>  [{service <service-name> | vlan <vlan-id/vfi_id> | service-instance  <integer(256-16777214)>}] [active]"},*/
  8,   /*{"INTERFACE "},*/
  211,  /*ecfm_mepconf.html*/
  0
 },
 {
  175,  /*{"ethernet cfm mep {domain <domain-name> | level <0-7>} [inward] mpid <id(1-8191)>  [{service <service-name> | vlan <vlan-id/vfi_id> | service-instance  <integer(256-16777214)>}] [active]"},*/
  8,   /*{"INTERFACE "},*/
  212,  /*ecfm_mepparamsconf.html*/
  0
 },
 {
  176,  /*{"show ethernet cfm maintenance-points local detail {mpid <mep-id(1-8191)> | mac <aa:aa:aa:aa:aa:aa>} [domain <domain_name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  213,  /*ecfm_mepdetails.html*/
  0
 },
 {
  177,  /*{"ethernet cfm cc enable {domain <domain-name> | level <a,b,c-d>}  [service <service-name> | vlan <a,b,c-d> | service-instance <integer(256-16777214)>]"},*/
  33,   /*{"VCM"},*/
  214,  /*ecfm_ccm.html*/
  0
 },
 {
  178,  /*{"ping ethernet [mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>]   {domain <domain-name> | level <level-id(0-7)>} [{service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>}]   [interface <interface-type> <interface-number>]  [direction {inward | outward}][lbm-mode {req-resp | burst}] [data-pattern <string> |  test-pattern null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc] [size <pdu-size(64-9000)> | variable-bytes] [interval <milliseconds(1-600000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>] [stop] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  215,  /*ecfm_lbm.html*/
  0
 },
 {
  179,  /*{"traceroute ethernet {mpid <mep-id> | mac <aa:aa:aa:aa:aa:aa>} {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>] [interface <interface-type> <interface-number>] [direction {inward | outward}] [time-to-live <ttl-value(0-255)>] [timeout <milliseconds(10-10000)>] [use-mip-ccm-db] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  216,  /*ecfm_ltm.html*/
  0
 },
 {
  180,  /*{"client-layer-level <level-id(0-7)>"},*/
  9,   /*{"DOMAIN_CONFIG"},*/
  217,  /*ecfm_ais.html*/
  0
 },
 {
  181,  /*{"ethernet cfm throughput type {one-way | two-way } {mpid  <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>}  {domain  <domain-name> | level <level-id(0-7)>} [{service <service-name> | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)> }]  [interface <interface-type> <interface-number>] [direction {inward | outward}]  [test-pattern {null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc}] [init-size <pdu-size(64-9000)>] [rate <pps(1-100000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>][{burst-count <integer(1-8192)> | burst-deadline <milli-seconds(1-172800)>}][stop][switch <string(32)>]"},*/
  38,   /*{"EXEC"},*/
  218,  /*ecfm_th.html*/
  0
 },
 {
  182,  /*{"ethernet cfm frame loss [{start | stop}] {domain <string(43)> | level <level-id(0-7)>} [{ service <service-name> | vlan <vlan_vfi_id> | service-instance <service-instance (256-16777214)>}] [interface <type> <num>] [direction {inward | outward}] {mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>} [interval <milliseconds(100-600000)>] [count <num_of_observations(1-8192)>] [deadline <seconds(1-172800)>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  219,  /*ecfm_fl.html*/
  0
 },
 {
  183,  /*{"ethernet cfm test [mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>] {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>]  [interface <interface-type> <interface-number>] [direction {inward | outward}] [pattern null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc] [size <pdu-size(64-9000)> | variable-bytes] [interval <milliseconds(1-600000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>] [stop] [switch <string(32)>]"},*/
  38,   /*{"EXEC"},*/
  220,  /*ecfm_tst.html*/
  0
 },
 {
  184,  /*{"ethernet cfm frame delay [{start | stop}] type {one-way | two-way} {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [direction {inward | outward}] {mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)} [interval <milliseconds(10-10000)>] [count <num_of_observations(1-8192)>] [deadline <seconds(1-172800)>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  221,  /*ecfm_fd.html*/
  0
 },
 {
  185,  /*{"set out-of-service [enable | disable] [interval {one-sec | one-min}] [period <seconds(0-172800)>] [delay <milliseconds(10-100)>]"},*/
  10,   /*{"MEP_CONFIG"},*/
  222,  /*ecfm_lck.html*/
  0
 },
 {
  186,  /*{"ethernet cfm availability measure {start | stop} {domain <string(43)> | level <integer(0-7)>} [service <string(43)> | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)> ] [interface <ifXtype> <ifnum>] [direction {inward | outward}] {mpid <integer(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>} [interval <<milliseconds(100-86400000)>] [windowsize <num_of_small_intervals(1-4099680000)>] [deadline <seconds(1-409968000)>] [static-window | sliding-window] [ lowerthreshold <string(6)> ] [ upperthreshold <string(6)> ] [modestarea { available | unAvailable}] [ schldDownTime init  <integer(1-172800)> end  <integer(1-172800)>  ] [switch <string(32)>]"},*/
  38,   /*{"EXEC"},*/
  223,  /*ecfm_availability.html*/
  0
 },
 {
  187,  /*{"show ethernet cfm loopback cache [detail] [domain <domain-name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  224,  /*ecfm_lbbrief.html*/
  0
 },
 {
  187,  /*{"show ethernet cfm loopback cache [detail] [domain <domain-name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  225,  /*ecfm_lbdetail.html*/
  0
 },
 {
  188,  /*{"show ethernet cfm frame delay buffer [detail] [one-way | two-way] [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]  [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  226,  /*ecfm_fdbrief.html*/
  0
 },
 {
  188,  /*{"show ethernet cfm frame delay buffer [detail] [one-way | two-way] [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]  [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  227,  /*ecfm_fddetail.html*/
  0
 },
 {
  189,  /*{"show ethernet cfm error-log [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  228,  /*ecfm_errlog.html*/
  0
 },
 {
  192,  /*{"show ethernet cfm traceroute-cache [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  229,  /*ecfm_ltrcache.html*/
  0
 },
 {
  190,  /*{"show ethernet cfm frame loss buffer [detail] [single-ended | dual-ended] [domain <domain-name> | level <level-id(0-7)>]  [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  230,  /*ecfm_flbrief.html*/
  0
 },
 {
  190,  /*{"show ethernet cfm frame loss buffer [detail] [single-ended | dual-ended] [domain <domain-name> | level <level-id(0-7)>]  [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  231,  /*ecfm_fldetail.html*/
  0
 },
 {
  191,  /*{"ethernet cfm mip {domain <domain-name> | level <level-id(0-7)>} {service <service-name> | vlan <vlan-id/vfi-id> | service-instance  <integer(256-16777214)>} [active]"},*/
  42,   /*{"INTERFACE"},*/
  232,  /*ecfm_mipconf.html*/
  0
 },
 {
  459,  /*{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  233,  /*ecfm_lbrs.html*/
  0
 },
 {
  192,  /*{"show ethernet cfm traceroute-cache [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  234,  /*ecfm_ltrs.html*/
  0
 },
 {
  193,  /*{"shutdown aps ring [switch]"},*/
  33,   /*{"VCM"},*/
  235,  /*erps_globalconfig.html*/
  0
 },
 {
  194,  /*{"aps ring group <group-id>"},*/
  39,   /*{"CONFIGURE"},*/
  236,  /*erps_ringtable.html*/
  0
 },
 {
  196,  /*{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  237,  /*erps_ringstatus.html*/
  0
 },
 {
  194,  /*{"aps ring group <group-id>"},*/
  39,   /*{"CONFIGURE"},*/
  238,  /*erps_ringcfm.html*/
  0
 },
 {
  199,  /*{"aps group name <group_name> ring group <group_id>"},*/
  39,   /*{"CONFIGURE"},*/
  239,  /*erps_ringtcprop.html*/
  0
 },
 {
  195,  /*{"aps timers [periodic <integer>  {milliseconds | seconds | minutes | hours}]  [hold-off <integer> {milliseconds | seconds | minutes | hours}] [guard <integer> {milliseconds | seconds | minutes | hours}]"},*/
  11,   /*{"VCMERPS_CONFIG"},*/
  240,  /*erps_ringconfig.html*/
  0
 },
 {
  197,  /*{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>] "},*/
  41,   /*{"USEREXEC"},*/
  241,  /*erps_ringstats.html*/
  0
 },
 {
  196,  /*{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  242,  /*erps_ringstats_rx.html*/
  0
 },
 {
  197,  /*{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>] "},*/
  41,   /*{"USEREXEC"},*/
  243,  /*erps_ringstats_tx.html*/
  0
 },
 {
  198,  /*{"aps ring map vlan-group <short(0-64)> [{add|remove}] <port_list>   "},*/
  39,   /*{"CONFIGURE"},*/
  244,  /*erps_ringvlan.html*/
  0
 },
 {
  199,  /*{"aps group name <group_name> ring group <group_id>"},*/
  33,   /*{"VCM"},*/
  245,  /*erps_ringtimers.html*/
  0
 },
 {
  200,  /*{"ip [vrf <vrf-name>] path mtu discover"},*/
  39,   /*{"CONFIGURE"},*/
  246,  /*vlan_interfacesettings.html*/
  0
 },
 {
  201,  /*{"ip address <ip-address> <subnet-mask> [secondary]"},*/
  19,   /*{"IVR"},*/
  247,  /*ivr_conf.html*/
  0
 },
 {
  202,  /*{"ip route [vrf <vrf-name>] <prefix> <mask> {<next-hop> | Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-id> | Linuxvlan <interface-name> | Cpu0 | tunnel <tunnel-id (0-128)> | <IP-interface-type> <IP-interface-number>} [<distance (1-254)>] [ private ] [ permanent ] [ name <nexthop-name>]"},*/
  39,   /*{"CONFIGURE"},*/
  248,  /*ip_routeconf.html*/
  0
 },
 {
  203,  /*{"ip address  <ip-address> <subnet-mask>"},*/
  12,   /*{"OOB_CONFIG"},*/
  249,  /*loopback_settings.html*/
  0
 },
 {
  204,  /*{"private-vlan mapping [{add | remove}] <vlan-list>"},*/
  42,   /*{"INTERFACE"},*/
  250,  /*ivr_pvlanmapping.html*/
  0
 },
 {
  205,  /*{"ping [vrf <vrf-name>] [ ip ] {IpAddress | hostname } [data (0-65535)] [df-bit] [{repeat|count} packet_count (1-10)] [size packet_size (36-2080)][source <ip-address>] [timeout time_out (1-100)] [validate]"},*/
  38,   /*{"EXEC"},*/
  251,  /*ip_ping.html*/
  0
 },
 {
  206,  /*{"default  mode { manual | dynamic }"},*/
  39,   /*{"CONFIGURE"},*/
  252,  /*ip_settings.html*/
  0
 },
 {
  206,  /*{"default  mode { manual | dynamic }"},*/
  39,   /*{"CONFIGURE"},*/
  253,  /*ip_settings_org.html*/
  0
 },
 {
  207,  /*{"traceroute {<ip-address> | ipv6 <prefix>} [vrf <vrf-name>] [min-ttl <value (1-99)>] [max-ttl <value (1-99)>]"},*/
  38,   /*{"EXEC"},*/
  254,  /*ipv4traceroute.html*/
  0
 },
 {
  208,  /*{"arp [vrf <vrf-name>] <ip address> <hardware address> {Vlan <vlan-id/vfi-id> [switch switch-name] |  <interface-type> <interface-id> | Linuxvlan <interface-name>| Cpu0 | <IP-interface-type> <IP-interface-number>}"},*/
  39,   /*{"CONFIGURE"},*/
  255,  /*arp_entry.html*/
  0
 },
 {
  209,  /*{"ip routing [vrf <vrf-name>]"},*/
  39,   /*{"CONFIGURE"},*/
  256,  /*rarp_info.html*/
  0
 },
 {
  210,  /*{"ip path mtu [vrf <vrf-name>] <dest ip> <tos(0-255)> <mtu(68-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  257,  /*rarp_pmtu.html*/
  0
 },
 {
  211,  /*{"ipv6 unicast-routing [vrf <vrf-name>]"},*/
  39,   /*{"CONFIGURE"},*/
  258,  /*ipv6_global_info.html*/
  0
 },
 {
  212,  /*{"ip vrf forwarding <vrf-name>"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  259,  /*ipv6_if_Forwarding.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  260,  /*ivr6_conf.html*/
  0
 },
 {
  213,  /*{"ipv6 neighbor [vrf <vrf-name>] <prefix> {vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <id>| [<interface-type> <interface-id>] | <IP-interface-type> <IP-interface-number>} <MAC ADDRESS (xx:xx:xx:xx:xx:xx)>"},*/
  39,   /*{"CONFIGURE"},*/
  261,  /*ND_conf.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  262,  /*addr_conf.html*/
  0
 },
 {
  214,  /*{"ipv6 nd prefix {<prefix addr> <prefixlen> | default} [{{<valid lifetime> | infinite | at <var valid lifetime>}{<preferred lifetime> |infinite | at <var preferred lifetime>} | no-advertise}] [off-link] [no-autoconfig] [embedded-rp]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  263,  /*addrprof_conf.html*/
  0
 },
 {
  215,  /*{"ipv6 route [vrf <vrf-name>] <prefix> <prefix len> ([<NextHop>] {vlan <vlan-id/vfi-id> [switch <switch-name> [<administrative distance>] [{unicast | anycast}]]|[tunnel <id>][<administrative distance>] [unicast] | [<administrative distance>] [unicast] | [<interface-type> <interface-id>] [<administrative distance>] [unicast] | <IP-interface-type> <IP-interface-number> [<administrative distance>] [unicast]})    "},*/
  39,   /*{"CONFIGURE"},*/
  264,  /*ip6_routeconf.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  265,  /*pref_conf.html*/
  0
 },
 {
  216,  /*{"ipv6 policy-prefix  <prefix> <prefix Len> precedence <integer> label <integer> [{unicast| anycast}]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  266,  /*policyprefix_conf.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  267,  /*interfacezone_conf.html*/
  0
 },
 {
  217,  /*{"ipv6 default scope-zone {interfacelocal | linklocal | subnetlocal | adminlocal |  sitelocal | scope6 | scope7 | orglocal | scope9 | scopeA | scopeB | scopeC | scopeD } <zone-index>"},*/
  39,   /*{"CONFIGURE"},*/
  268,  /*scopezone_conf.html*/
  0
 },
 {
  218,  /*{"ping [vrf <vrf-name>] ipv6 <prefix%interface> [data <hex_str>] [repeat <count>] [size <value>] [anycast] [source {vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <id> | <source_prefix>}] [timeout <value (1-100)>] "},*/
  38,   /*{"EXEC"},*/
  269,  /*ipv6_ping.html*/
  0
 },
 {
  219,  /*{"ipsecv6 admin <enable/disable>"},*/
  13,   /*{"CRYPTO"},*/
  270,  /*secv6_globalconf.html*/
  0
 },
 {
  220,  /*{"ipsecv6 sa <sa-index> <peeraddress> <spi> {transport|tunnel} [antireplay-enable]"},*/
  13,   /*{"CRYPTO"},*/
  271,  /*secv6_saconf.html*/
  0
 },
 {
  221,  /*{"ipsecv6 policy <policyindex> {apply|bypass} {manual|automatic} <sa-index>"},*/
  13,   /*{"CRYPTO"},*/
  272,  /*secv6_policyconf.html*/
  0
 },
 {
  222,  /*{"v6access-list <accesslist-index> {<any> |<src-netmask> <src-addr} {<any> | <dest-netmask> <dest-addr>}"},*/
  13,   /*{"CRYPTO"},*/
  273,  /*secv6_accessconf.html*/
  0
 },
 {
  223,  /*{"ipsecv6 selector {vlan <id>|tunnel <id>|any} {tcp|udp|icmpv6|ah|esp|any} {port-no|any} {inbound|outbound|any} <accesslist-index> <policy-index> {filter|allow} [{LocalTunnelIP}]"},*/
  13,   /*{"CRYPTO"},*/
  274,  /*secv6_selectorconf.html*/
  0
 },
 {
  224,  /*{"tunnel mode {gre|sixToFour|isatap|compat|ipv6ip} [config-id <ConfId(1-2147483647)>] source <TnlSrcIP/IfName> [dest <TnlDestIP>]"},*/
  16,   /*{"TNL"},*/
  275,  /*L3_tnlconf.html*/
  0
 },
 {
  225,  /*{"ip dhcp bootfile <bootfile (63)>"},*/
  39,   /*{"CONFIGURE"},*/
  276,  /*dhcp_globalbootfileconfig.html*/
  0
 },
 {
  226,  /*{"host hardware-type <type (1-2147483647)> client-identifier <mac-address> { ip <address> | option <code (1-2147483647)> { ascii <string> | hex <Hex String> | ip <address> }}"},*/
  14,   /*{"DHCPPOOL"},*/
  277,  /*dhcp_hostip.html*/
  0
 },
 {
  228,  /*{"ip dhcp pool <index (1-2147483647)>"},*/
  39,   /*{"CONFIGURE"},*/
  278,  /*dhcp_hostopt.html*/
  0
 },
 {
  227,  /*{"service dhcp-server"},*/
  39,   /*{"CONFIGURE"},*/
  279,  /*dhcp_globalconf.html*/
  0
 },
 {
  228,  /*{"ip dhcp pool <index (1-2147483647)>"},*/
  39,   /*{"CONFIGURE"},*/
  280,  /*dhcp_poolconf.html*/
  0
 },
 {
  228,  /*{"ip dhcp pool <index (1-2147483647)>"},*/
  39,   /*{"CONFIGURE"},*/
  281,  /*dhcp_poolopt.html*/
  0
 },
 {
  228,  /*{"ip dhcp pool <index (1-2147483647)>"},*/
  39,   /*{"CONFIGURE"},*/
  282,  /*dhcp_excl.html*/
  0
 },
 {
  229,  /*{"ip dhcp client client-id {<interface-type> <interface-id> | vlan <vlan-id (1-4094)> | port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | loopback <interface-id (0-100)> | ascii <string> | hex <string> }"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  283,  /*dhcpc_confclientId.html*/
  0
 },
 {
  230,  /*{"ip dhcp client request { tftp-server-name | boot-file-name }"},*/
  19,   /*{"IVR"},*/
  284,  /*dhcpc_confopttype.html*/
  0
 },
 {
  231,  /*{"service dhcp-relay"},*/
  39,   /*{"CONFIGURE"},*/
  285,  /*dhcpr_globalconf.html*/
  0
 },
 {
  232,  /*{"ip dhcp relay circuit-id <circuit-id>"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  286,  /*dhcpr_interfacemap.html*/
  0
 },
 {
  307,  /*{"router rip [vrf <name>]"},*/
  39,   /*{"CONFIGURE"},*/
  287,  /*rip_globalconf.html*/
  0
 },
 {
  234,  /*{"network <ip-address>[unnum {vlan <vlan-id/vfi-id> [switch <switch-name>] | <iftype> <ifnum>}]"},*/
  26,   /*{"RIP"},*/
  288,  /*rip_ifaceconf.html*/
  0
 },
 {
  233,  /*{"neighbor <ip address>"},*/
  26,   /*{"RIP"},*/
  289,  /*rip_nbrfilter.html*/
  0
 },
 {
  234,  /*{"network <ip-address>[unnum {vlan <vlan-id/vfi-id> [switch <switch-name>] | <iftype> <ifnum>}]"},*/
  26,   /*{"RIP"},*/
  290,  /*rip_keyconf.html*/
  0
 },
 {
  235,  /*{"ip rip summary-address <ip-address> <mask>"},*/
  30,   /*{"PPP"},*/
  291,  /*rip_aggconf.html*/
  0
 },
 {
  307,  /*{"router rip [vrf <name>]"},*/
  39,   /*{"CONFIGURE"},*/
  292,  /*ripcontext_creation.html*/
  0
 },
 {
  236,  /*{"ipv6 rip enable"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  293,  /*rip6_IfConf.html*/
  0
 },
 {
  237,  /*{"distribute prefix <ip6_addr> {in | out}"},*/
  24,   /*{"RIP6"},*/
  294,  /*rip6_filt.html*/
  0
 },
 {
  238,  /*{"route-map <name(1-20)> [ {permit | deny }] [ <seqnum(1-10)> ]"},*/
  39,   /*{"CONFIGURE"},*/
  295,  /*rmap_conf.html*/
  0
 },
 {
  238,  /*{"route-map <name(1-20)> [ {permit | deny }] [ <seqnum(1-10)> ]"},*/
  39,   /*{"CONFIGURE"},*/
  296,  /*rmapmatch_conf.html*/
  0
 },
 {
  239,  /*{"set next-hop ip <next-hop ip address>"},*/
  15,   /*{"ROUTEMAP"},*/
  297,  /*rmapset_conf.html*/
  0
 },
 {
  240,  /*{"ip prefix-list <list-name(1-20)> [seq <seq-num>] {permit | deny } <ipaddr/prefix-len>           [ge <min-len>] [le <max-len>]"},*/
  39,   /*{"CONFIGURE"},*/
  298,  /*rmap_ipprefix_conf.html*/
  0
 },
 {
  308,  /*{"router ospf [vrf <name>]"},*/
  39,   /*{"CONFIGURE"},*/
  299,  /*ospf_context_creation.html*/
  0
 },
 {
  241,  /*{"set nssa asbr-default-route translator { enable | disable }"},*/
  25,   /*{"ROUTEROSPF"},*/
  300,  /*ospf_globalconf.html*/
  0
 },
 {
  242,  /*{"area <area-id> stub [no-summary]"},*/
  25,   /*{"ROUTEROSPF"},*/
  301,  /*ospf_areaconf.html*/
  0
 },
 {
  243,  /*{"network <Network number> area <area-id> [unnum { Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-num> | <IP-interface-type> <IP-interface-number>}]"},*/
  25,   /*{"ROUTEROSPF"},*/
  302,  /*ospf_ifaceconf.html*/
  0
 },
 {
  244,  /*{"area <area-id> virtual-link <router-id> [authentication { simple | message-digest | sha-1 | sha-224 | sha-256 | sha384 | sha-512 | null}] [hello-interval <value (1-65535)>] [retransmit-interval <value (1-3600)>] [transmit-delay <value (1-3600)>] [dead-interval <value>] [{authentication-key <key (8)> | message-digest-key <Key-id (0-255)> {md5 | sha-1 | sha-224 | sha-256 | sha-384 | sha-512} <key (16)>}]"},*/
  25,   /*{"ROUTEROSPF"},*/
  303,  /*ospf_virtualifaceconf.html*/
  0
 },
 {
  245,  /*{"neighbor <neighbor-id> [priority  <priority value (0-255)>] [poll-interval seconds] [cost number] [database-filter all]"},*/
  25,   /*{"ROUTEROSPF"},*/
  304,  /*ospf_nbrconf.html*/
  0
 },
 {
  246,  /*{"redist-config <Network> <Mask> [metric-value <metric (1 - 16777215)>] [metric-type {asExttype1 | asExttype2}]  [tag <tag-value>}"},*/
  25,   /*{"ROUTEROSPF"},*/
  305,  /*ospf_RRDrouteconf.html*/
  0
 },
 {
  247,  /*{"area <AreaId> range <Network> <Mask> {summary | Type7}              [{advertise | not-advertise}] [tag <value>]"},*/
  25,   /*{"ROUTEROSPF"},*/
  306,  /*ospf_AreaAggregateconf.html*/
  0
 },
 {
  248,  /*{" summary-address <Network> <Mask> <AreaId> [{allowAll | denyAll |  advertise | not-advertise}] [Translation {enabled | disabled}][tag tag-value]"},*/
  25,   /*{"ROUTEROSPF"},*/
  307,  /*ospf_AsExtAggrconf.html*/
  0
 },
 {
  249,  /*{"capability opaque"},*/
  25,   /*{"ROUTEROSPF"},*/
  308,  /*ospf_GRconf.html*/
  0
 },
 {
  250,  /*{"timers spf <spf-delay> <spf-holdtime>"},*/
  23,   /*{"ROUTEROSPF3"},*/
  309,  /*ospfv3global_conf.html*/
  0
 },
 {
  310,  /*{"ipv6 router ospf [vrf <contextname>]"},*/
  39,   /*{"CONFIGURE"},*/
  310,  /*ospfv3context_creation.html*/
  0
 },
 {
  251,  /*{"router-id <IPv4-Address>"},*/
  23,   /*{"ROUTEROSPF3"},*/
  311,  /*ospfv3basic_conf.html*/
  0
 },
 {
  252,  /*{"ipv6 ospf area <IPv4-Address> [instance <instance-id>]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  312,  /*ospfv3if_conf.html*/
  0
 },
 {
  253,  /*{"area <area-id> { { stub | nssa } [no-summary] }"},*/
  23,   /*{"ROUTEROSPF3"},*/
  313,  /*ospfv3area_conf.html*/
  0
 },
 {
  254,  /*{"nsf ietf [restart-interval <grace period (1-1800)>] [plannedOnly]"},*/
  23,   /*{"ROUTEROSPF3"},*/
  314,  /*ospfv3gr_conf.html*/
  0
 },
 {
  255,  /*{"snmp-server enable traps ipv6 dhcp client [invalid-pkt] [auth-fail]"},*/
  39,   /*{"CONFIGURE"},*/
  315,  /*dhcp6c_globalconf.html*/
  0
 },
 {
  256,  /*{"ipv6 dhcp client-id type {llt | en | ll}"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  316,  /*dhcp6c_ifconf.html*/
  0
 },
 {
  256,  /*{"ipv6 dhcp client-id type {llt | en | ll}"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  317,  /*dhcp6c_option.html*/
  0
 },
 {
  257,  /*{"show ipv6 dhcp client statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},*/
  38,   /*{"EXEC"},*/
  318,  /*dhcp6c_ifstats.html*/
  0
 },
 {
  258,  /*{"ipv6 dhcp relay port {listen <value(1-65535)> | client transmit <value(1-65535)> | server transmit <value(1-65535)>}"},*/
  39,   /*{"CONFIGURE"},*/
  319,  /*dhcp6r_globalconf.html*/
  0
 },
 {
  259,  /*{"ipv6 dhcp relay hop-threshold <count>"},*/
  16,   /*{"TNL"},*/
  320,  /*dhcp6r_ifconf.html*/
  0
 },
 {
  260,  /*{"ipv6 dhcp relay [destination <prefix> {link-local | <prefix Len> } [interface {Vlan <vlan-id (1-4094)> | <interface-type> <interface-id>}]]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  321,  /*dhcp6r_serveraddr.html*/
  0
 },
 {
  260,  /*{"ipv6 dhcp relay [destination <prefix> {link-local | <prefix Len> } [interface {Vlan <vlan-id (1-4094)> | <interface-type> <interface-id>}]]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  322,  /*dhcp6r_outifconf.html*/
  0
 },
 {
  261,  /*{"show ipv6 dhcp relay statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},*/
  38,   /*{"EXEC"},*/
  323,  /*dhcp6r_ifstats.html*/
  0
 },
 {
  262,  /*{"ipv6 dhcp server port {listen <value(1-65535)> | client transmit <value(1-65535)> | relay transmit <value(1-65535)>}"},*/
  39,   /*{"CONFIGURE"},*/
  324,  /*dhcp6s_globalconf.html*/
  0
 },
 {
  263,  /*{"ipv6 dhcp server [<pool-name (1-64)> [preference  <value (0-255)>]]"},*/
  16,   /*{"TNL"},*/
  325,  /*dhcp6s_poolconf.html*/
  0
 },
 {
  263,  /*{"ipv6 dhcp server [<pool-name (1-64)> [preference  <value (0-255)>]]"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  326,  /*dhcp6s_ifconf.html*/
  0
 },
 {
  264,  /*{"link-address <IPV6-Prefix>"},*/
  17,   /*{"D6SRVPOOL"},*/
  327,  /*dhcp6s_prefixconf.html*/
  0
 },
 {
  265,  /*{"ipv6 dhcp pool <string (1-64)>"},*/
  39,   /*{"CONFIGURE"},*/
  328,  /*dhcp6s_optionconf.html*/
  0
 },
 {
  265,  /*{"ipv6 dhcp pool <string (1-64)>"},*/
  39,   /*{"CONFIGURE"},*/
  329,  /*dhcp6s_suboptionconf.html*/
  0
 },
 {
  267,  /*{"ipv6 dhcp authentication realm <string(1-128)> key <string(1-64)>"},*/
  18,   /*{"D6SRVCLNT"},*/
  330,  /*dhcp6s_realmconf.html*/
  0
 },
 {
  266,  /*{"ipv6 dhcp authentication server client-id <string(128)> {llt | en | ll}"},*/
  39,   /*{"CONFIGURE"},*/
  331,  /*dhcp6s_clientconf.html*/
  0
 },
 {
  267,  /*{"ipv6 dhcp authentication realm <string(1-128)> key <string(1-64)>"},*/
  18,   /*{"D6SRVCLNT"},*/
  332,  /*dhcp6s_keyconf.html*/
  0
 },
 {
  268,  /*{"show ipv6 dhcp server statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},*/
  38,   /*{"EXEC"},*/
  333,  /*dhcp6s_ifstats.html*/
  0
 },
 {
  269,  /*{"router isis [vrf <contextname>]"},*/
  39,   /*{"CONFIGURE"},*/
  334,  /*isis_sysinstconf.html*/
  0
 },
 {
  275,  /*{"isis metric <default-metric> [delay-metric <metric>] [error-metric <metric>] [expense-metric <metric>] {level-1 | level-2}"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  335,  /*isis_sysinstconf2.html*/
  0
 },
 {
  270,  /*{"net <network-entity-title>"},*/
  28,   /*{"ROUTERISIS"},*/
  336,  /*isis_maaconf.html*/
  0
 },
 {
  275,  /*{"isis metric <default-metric> [delay-metric <metric>] [error-metric <metric>] [expense-metric <metric>] {level-1 | level-2}"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  337,  /*isis_summaddrconf.html*/
  0
 },
 {
  271,  /*{"ipra <ipra-idx> {<ip-address> <ip-mask> <next-hop-ip> |<ip6_addr> <prefixlength> <ip6_addr> } [met-type {internal | external}]"},*/
  28,   /*{"ROUTERISIS"},*/
  338,  /*isis_ipraconf.html*/
  0
 },
 {
  272,  /*{"show ip isis [vrf <contextname>] packet-stats [ { vlan <vlan-id/vfi-id> | <iftype> <ifnum> | <IP-interface-type> <IP-interface-number>}]"},*/
  38,   /*{"EXEC"},*/
  339,  /*isis_pktcountconf.html*/
  0
 },
 {
  273,  /*{"show ip isis [vrf <contextname>] adjacencies [ { vlan <vlan-id/vfi-id> | <iftype> <ifnum> | <IP-interface-type> <IP-interface-number>} ]"},*/
  38,   /*{"EXEC"},*/
  340,  /*isis_adjconf.html*/
  0
 },
 {
  274,  /*{"isis circuit-type {level-1 | level1-2 | level-2-only}"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  341,  /*isis_circconf.html*/
  0
 },
 {
  275,  /*{"isis metric <default-metric> [delay-metric <metric>] [error-metric <metric>] [expense-metric <metric>] {level-1 | level-2}"},*/
  19,   /*{"IVR"},*/
  342,  /*isis_circlevelconf.html*/
  0
 },
 {
  276,  /*{"show ip isis instances"},*/
  38,   /*{"EXEC"},*/
  343,  /*isis_aaconf.html*/
  0
 },
 {
  277,  /*{"area-password <password>"},*/
  28,   /*{"ROUTERISIS"},*/
  344,  /*isis_arearxpwdconf.html*/
  0
 },
 {
  278,  /*{"domain-password <password>"},*/
  28,   /*{"ROUTERISIS"},*/
  345,  /*isis_domainrxpwdconf.html*/
  0
 },
 {
  279,  /*{"nsf ietf { plannedOnly | plannedAndUnplanned }"},*/
  28,   /*{"ROUTERISIS"},*/
  346,  /*isis_GRconf.html*/
  0
 },
 {
  280,  /*{"router bgp <AS no> [vrf  <vrf-name>]"},*/
  39,   /*{"CONFIGURE"},*/
  347,  /*bgp_context_creation.html*/
  0
 },
 {
  281,  /*{"do shutdown ip bgp [ vrf <vrf-name> ]"},*/
  39,   /*{"CONFIGURE"},*/
  348,  /*bgp_globalconf.html*/
  0
 },
 {
  282,  /*{"bgp cluster-id {cluster id value ip_address/integer}"},*/
  27,   /*{"BGP"},*/
  349,  /*bgp_globalconfscalars.htm*/
  0
 },
 {
  283,  /*{"neighbor <ip-address/peer-group-name> remote-as <AS no> [allow-autostart [idlehold-time <integer(1-65535)>]]"},*/
  27,   /*{"BGP"},*/
  350,  /*bgp_peerconf.html*/
  0
 },
 {
  284,  /*{"bgp med <1-100> remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list- AS1,AS2,...>] value <value> direction {in | out} [override]"},*/
  27,   /*{"BGP"},*/
  351,  /*bgp_medconf.html*/
  0
 },
 {
  284,  /*{"bgp med <1-100> remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list- AS1,AS2,...>] value <value> direction {in | out} [override]"},*/
  27,   /*{"BGP"},*/
  352,  /*bgp_localprefconf.html*/
  0
 },
 {
  285,  /*{"bgp update-filter <1-100> {permit|deny} remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list-AS1,AS2,...>] direction { in|out }"},*/
  27,   /*{"BGP"},*/
  353,  /*bgp_filterconf.html*/
  0
 },
 {
  286,  /*{"aggregate-address index <1-100> <ip-address> <prefixlen> [summary-only] [as-set] [suppress-map <map-name>] [advertise-map <map-name>] [attribute-map <map-name>]"},*/
  27,   /*{"BGP"},*/
  354,  /*bgp_routeaggreconf.html*/
  0
 },
 {
  287,  /*{"neighbor <ip-address|peer-group-name> timers {keepalive <(1-21845)seconds> | holdtime <(3-65535)seconds> |delayopentime <(0-65535)seconds>}"},*/
  27,   /*{"BGP"},*/
  355,  /*bgp_timerconf.html*/
  0
 },
 {
  288,  /*{"bgp graceful-restart [restart-time <(1-4096)<seconds>] [stalepath-time <(90-3600)<seconds>]"},*/
  27,   /*{"BGP"},*/
  356,  /*bgp_grconf.html*/
  0
 },
 {
  289,  /*{"tcp-ao mkt key-id <Key Id(0-255)>  receive-key-id <Rcv Key Id (0-255)> algorithm {hmac-sha-1 | aes-128-cmac} key <master-key>  [tcp-option-exclude]"},*/
  20,   /*{"BGP "},*/
  357,  /*bgp_tcpaomktconf.html*/
  0
 },
 {
  291,  /*{"neighbor <peer-group-name> peer-group"},*/
  27,   /*{"BGP"},*/
  358,  /*bgp_peergroupconf1.html*/
  0
 },
 {
  290,  /*{"neighbor <ip-address> peer-group <peer-group-name>"},*/
  27,   /*{"BGP"},*/
  359,  /*bgp_peergroupconf2.html*/
  0
 },
 {
  291,  /*{"neighbor <peer-group-name> peer-group"},*/
  27,   /*{"BGP"},*/
  360,  /*bgp_peergroupadd.html*/
  0
 },
 {
  292,  /*{"clear ip bgp [vrf <string (32)>]  {dampening [<random_str><num_str>] | flap-statistics [<random_str> <num_str>] | { * | <AS no> | external | ipv4 | ipv6 | <random_str> } [soft [{in [prefix-filter]|out}]] }"},*/
  38,   /*{"EXEC"},*/
  361,  /*bgp_clearbgpconf.html*/
  0
 },
 {
  293,  /*{"neighbor <ip-address|peer-group-name> { route-map <name(1-20)> | prefix-list <ipprefixlist_name(1-20)>} {in | out}"},*/
  27,   /*{"BGP"},*/
  362,  /*bgp_routemap.html*/
  0
 },
 {
  294,  /*{"neighbor <ip-address|peer-group-name> capability {ipv4-unicast|ipv6-unicast|route-refresh | orf prefix-list {send | receive | both}} "},*/
  27,   /*{"BGP"},*/
  363,  /*bgp_peerorfconf.html*/
  0
 },
 {
  295,  /*{"show ip bgp [vrf <vrf-name>]{[neighbor [<peer-addr> [received prefix-filter]]]}"},*/
  38,   /*{"EXEC"},*/
  364,  /*bgp_orffilters.html*/
  0
 },
 {
  296,  /*{"bgp confederation peers <AS no(1-65535)>"},*/
  27,   /*{"BGP"},*/
  365,  /*bgp_confedconf.html*/
  0
 },
 {
  297,  /*{"ip bgp dampening [vrf  <vrf-name>] [HalfLife-Time <integer(600-2700)>] [Reuse-Value <integer(100-1999)>] [Suppress-Value <integer(2000-3999)>] [Max-Suppress-Time <integer(1800-10800)>] [-s Decay-Granularity <integer(1-10800)>]  [Reuse-Granularity <integer(15-10800)>] [Reuse-Array-Size <integer(256-65535)>]"},*/
  39,   /*{"CONFIGURE"},*/
  366,  /*bgp_rfdconf.html*/
  0
 },
 {
  298,  /*{"bgp comm-filter <comm-value(4294967041-4294967043,65536-4294901759)> <permit|deny> <in|out>"},*/
  27,   /*{"BGP"},*/
  367,  /*bgp_commfilterconf.html*/
  0
 },
 {
  299,  /*{"bgp comm-policy <ip-address> <prefixlen> <set-add|set-none|modify>"},*/
  27,   /*{"BGP"},*/
  368,  /*bgp_commlocpolicyconf.html*/
  0
 },
 {
  300,  /*{"bgp comm-route {additive|delete} <ip-address> <prefixlen> comm-value <4294967041-4294967043,65536-4294901759>"},*/
  27,   /*{"BGP"},*/
  369,  /*bgp_commrouteconf.html*/
  0
 },
 {
  301,  /*{"bgp ecomm-filter <ecomm-value(xx:xx:xx:xx:xx:xx:xx:xx)> {permit|deny} {in|out}"},*/
  27,   /*{"BGP"},*/
  370,  /*bgp_ecommfilterconf.html*/
  0
 },
 {
  302,  /*{"bgp ecomm-policy <ip-address> <prefixlen> <set-add|set-none|modify>"},*/
  27,   /*{"BGP"},*/
  371,  /*bgp_ecommlocpolicyconf.html*/
  0
 },
 {
  303,  /*{"bgp ecomm-route {additive|delete} <ip-address> <prefixlen> ecomm-value <value(xx:xx:xx:xx:xx:xx:xx:xx)>"},*/
  27,   /*{"BGP"},*/
  372,  /*bgp_ecommrouteconf.html*/
  0
 },
 {
  304,  /*{"import route ip-address prefixlen nexthop metric ifindex protocol action route-count"},*/
  27,   /*{"BGP"},*/
  373,  /*bgp_importconf.html*/
  0
 },
 {
  305,  /*{"as-num <value(1-65535)> [vrf <vrf-name>]"},*/
  39,   /*{"CONFIGURE"},*/
  374,  /*rrd_globalconf.html*/
  0
 },
 {
  306,  /*{"no shutdown ip bgp"},*/
  39,   /*{"CONFIGURE"},*/
  375,  /*rrd_bgpconf.html*/
  0
 },
 {
  307,  /*{"router rip [vrf <name>]"},*/
  39,   /*{"CONFIGURE"},*/
  376,  /*rrd_ripconf.html*/
  0
 },
 {
  308,  /*{"router ospf [vrf <name>]"},*/
  39,   /*{"CONFIGURE"},*/
  377,  /*rrd_ospfconf.html*/
  0
 },
 {
  309,  /*{"redistribute-policy [vrf <vrf-name>] ipv6 {permit|deny}  <DestIp> <DestRange> {static|local|rip|ospf}  {rip|ospf|all}"},*/
  39,   /*{"CONFIGURE"},*/
  378,  /*rrd6_filterconf.html*/
  0
 },
 {
  310,  /*{"ipv6 router ospf [vrf <contextname>]"},*/
  39,   /*{"CONFIGURE"},*/
  379,  /*rrd6_ospfv3conf.html*/
  0
 },
 {
  311,  /*{"ipv6 router rip"},*/
  39,   /*{"CONFIGURE"},*/
  380,  /*rrd6_rip6conf.html*/
  0
 },
 {
  312,  /*{"redistribute <static | connected | rip | all> [route-map <string(20)>] [metric <integer>]"},*/
  27,   /*{"BGP"},*/
  381,  /*rrd6_bgp4conf.html*/
  0
 },
 {
  313,  /*{"mpls ip"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  382,  /*mplsif_conf.html*/
  0
 },
 {
  314,  /*{"mpls static binding ipv4 <prefix> <mask> {input {vlan <in-vlan-id (1-4094)> | <in-interface-type> <in-interface-id>} | output <nexthop>} <label (200001-300000)>"},*/
  39,   /*{"CONFIGURE"},*/
  383,  /*mplsftn_conf.html*/
  0
 },
 {
  315,  /*{"mpls static crossconnect {{vlan <in-vlan-id (1-4094)> | <in-interface-type> <in-interface-id>} <inlabel> <nexthop> {<outlabel> | explicit-null | implicit-null} | pwxconnect <ingress-PW-Id> <egress-PW-Id>}"},*/
  39,   /*{"CONFIGURE"},*/
  384,  /*mplsxc_conf.html*/
  0
 },
 {
  316,  /*{"tunnel mpls destination {<remote ip> | <remote local-map-number>| point-to-multipoint <p2mp_id>} [source {<src ip> | <src local-map-number>}] [lsp-num <tunnel instance(1-65535)>]"},*/
  21,   /*{"MPLS_TUNNEL_MODE"},*/
  385,  /*mplstunnel_conf.html*/
  0
 },
 {
  317,  /*{"l2 vfi <vfi-name (32)> manual"},*/
  33,   /*{"VCM"},*/
  386,  /*mplsvfi_conf.html*/
  0
 },
 {
  318,  /*{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},*/
  39,   /*{"CONFIGURE"},*/
  387,  /*mplspw_conf.html*/
  0
 },
 {
  319,  /*{"map pwid <integer> <ifXtype> <ifnum>"},*/
  39,   /*{"CONFIGURE"},*/
  388,  /*mplsmap_conf.html*/
  0
 },
 {
  320,  /*{"router vrrp"},*/
  39,   /*{"CONFIGURE"},*/
  389,  /*vrrp_basicsettings.html*/
  0
 },
 {
  321,  /*{"vrrp <vrid(1-255)> priority <priority(1-254)>"},*/
  22,   /*{"VRRP_IF"},*/
  390,  /*vrrp_conf.html*/
  0
 },
 {
  322,  /*{"ip nat"},*/
  39,   /*{"CONFIGURE"},*/
  391,  /*nat_basicsettings.html*/
  0
 },
 {
  323,  /*{"interface nat {enable|disable}"},*/
  30,   /*{"PPP"},*/
  392,  /*nat_conf.html*/
  0
 },
 {
  324,  /*{"ip nat { idle | tcp | udp } timeout <seconds ((idle-60|udp-300|tcp-300) - 86400)>"},*/
  39,   /*{"CONFIGURE"},*/
  393,  /*nat_timer.html*/
  0
 },
 {
  325,  /*{"static nat <local ip> <translated local ip>"},*/
  30,   /*{"PPP"},*/
  394,  /*nat_static.html*/
  0
 },
 {
  326,  /*{"ip nat pool <global ip> <mask>"},*/
  30,   /*{"PPP"},*/
  395,  /*nat_dynamic.html*/
  0
 },
 {
  327,  /*{"virtual server <local ip> [<local port number>] { auth | dns | ftp | pop3 | pptp | smtp | telnet | http | nntp | snmp | other [<global port number>] } [<tcp|udp|any>] [<description>]"},*/
  30,   /*{"PPP"},*/
  396,  /*nat_virtualserver.html*/
  0
 },
 {
  328,  /*{"show ip nat { global | static | translations | policy}"},*/
  38,   /*{"EXEC"},*/
  397,  /*nat_translation.html*/
  0
 },
 {
  329,  /*{"static inside <Policy NAT ID (1-65535)> access-list <string>"},*/
  39,   /*{"CONFIGURE"},*/
  398,  /*nat_policy.html*/
  0
 },
 {
  330,  /*{"distance <1-255> [route-map <name(20)>]"},*/
  23,   /*{"ROUTEROSPF3"},*/
  399,  /*fltr_ospfv3conf.html*/
  0
 },
 {
  331,  /*{"distance <1-255> [route-map <name(1-20)>]"},*/
  24,   /*{"RIP6"},*/
  400,  /*fltr_rip6conf.html*/
  0
 },
 {
  331,  /*{"distance <1-255> [route-map <name(1-20)>]"},*/
  25,   /*{"ROUTEROSPF"},*/
  401,  /*fltr_ospfconf.html*/
  0
 },
 {
  331,  /*{"distance <1-255> [route-map <name(1-20)>]"},*/
  26,   /*{"RIP"},*/
  402,  /*fltr_ripconf.html*/
  0
 },
 {
  331,  /*{"distance <1-255> [route-map <name(1-20)>]"},*/
  27,   /*{"BGP"},*/
  403,  /*fltr_bgpconf.html*/
  0
 },
 {
  331,  /*{"distance <1-255> [route-map <name(1-20)>]"},*/
  28,   /*{"ROUTERISIS"},*/
  404,  /*fltr_isisconf.html*/
  0
 },
 {
  332,  /*{"layer4 switch <FilterNo (1-20)>  protocol { any | tcp | udp | <Protocol No (1-255)>} port { any | <PortNo (1-65535)>} interface { <interface type> <interface id> }"},*/
  39,   /*{"CONFIGURE"},*/
  405,  /*l4_switching.html*/
  0
 },
 {
  333,  /*{"shutdown snooping"},*/
  39,   /*{"CONFIGURE"},*/
  406,  /*igs_conf.html*/
  0
 },
 {
  334,  /*{"ip igmp snooping mrouter-time-out <(60 - 600) seconds>"},*/
  39,   /*{"CONFIGURE"},*/
  407,  /*igs_tmrconf.html*/
  0
 },
 {
  335,  /*{"ip igmp snooping fast-leave"},*/
  37,   /*{"VLAN_CONFIG"},*/
  408,  /*igs_vlan.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  409,  /*igs_intf.html*/
  0
 },
 {
  336,  /*{"ip igmp snooping mrouter <interface-type> <0/a-b, 0/c, ...>"},*/
  37,   /*{"VLAN_CONFIG"},*/
  410,  /*igs_rtrportconf.html*/
  0
 },
 {
  337,  /*{"show ip igmp snooping mrouter [Vlan <vlan-id/vfi-id>] [detail]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  411,  /*igs_rtrtable.html*/
  0
 },
 {
  338,  /*{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  412,  /*igs_macfwdtable.html*/
  0
 },
 {
  338,  /*{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  413,  /*igs_ipfwdtable.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  414,  /*igs_mcastrecv.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  415,  /*igs_enhintf.html*/
  0
 },
 {
  338,  /*{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  416,  /*igs_enhipfwdtable.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  417,  /*igs_enhmcastrecv.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  418,  /*igs_intfconf.html*/
  0
 },
 {
  338,  /*{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  419,  /*igs_mcasttable.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  420,  /*igs_recvtable.html*/
  0
 },
 {
  339,  /*{"ip igmp snooping static-group <mcast_addr> ports <ifXtype><iface_list>"},*/
  37,   /*{"VLAN_CONFIG"},*/
  421,  /*igs_static.html*/
  0
 },
 {
  340,  /*{"snooping multicast-forwarding-mode {ip | mac}"},*/
  39,   /*{"CONFIGURE"},*/
  422,  /*mlds_conf.html*/
  0
 },
 {
  341,  /*{"ipv6 mld snooping mrouter-time-out <(60 - 600) seconds>"},*/
  33,   /*{"VCM"},*/
  423,  /*mlds_tmrconf.html*/
  0
 },
 {
  342,  /*{"ipv6 mld snooping version {v1 | v2}"},*/
  29,   /*{"VCMVLAN_CONFIG"},*/
  424,  /*mlds_vlan.html*/
  0
 },
 {
  343,  /*{"ip igmp snooping mrouter-port <ifXtype> <iface_list> time-out <short(60-600)>"},*/
  37,   /*{"VLAN_CONFIG"},*/
  425,  /*mlds_rtrportconf.html*/
  0
 },
 {
  344,  /*{"show ipv6 mld snooping mrouter [Vlan <vlan-id/vfi-id>] [detail] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  426,  /*mlds_rtrtable.html*/
  0
 },
 {
  346,  /*{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  427,  /*mlds_macfwdtable.html*/
  0
 },
 {
  346,  /*{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  428,  /*mlds_ipfwdtable.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  429,  /*mlds_intf.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  430,  /*mlds_enhintf.html*/
  0
 },
 {
  346,  /*{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  431,  /*mlds_enhipfwdtable.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  432,  /*mlds_mcastrecv.html*/
  0
 },
 {
  345,  /*{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  433,  /*mlds_intfconf.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  434,  /*mlds_enhmcastrecv.html*/
  0
 },
 {
  346,  /*{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  435,  /*mlds_mcasttable.html*/
  0
 },
 {
  347,  /*{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  436,  /*mlds_recvtable.html*/
  0
 },
 {
  348,  /*{"set gmrp { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  437,  /*vlan_gmrpbasicconf.html*/
  0
 },
 {
  349,  /*{"set port gmrp <interface-type> <interface-id> { enable | disable }"},*/
  33,   /*{"VCM"},*/
  438,  /*vlan_gmrpportconf.html*/
  0
 },
 {
  349,  /*{"set port gmrp <interface-type> <interface-id> { enable | disable }"},*/
  33,   /*{"VCM"},*/
  439,  /*vlan_gmrpconf.html*/
  0
 },
 {
  350,  /*{"set ip igmp { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  440,  /*igmp_conf.html*/
  0
 },
 {
  351,  /*{"set ip igmp {enable | disable }"},*/
  30,   /*{"PPP"},*/
  441,  /*igmp_iface.html*/
  0
 },
 {
  352,  /*{"ip igmp static-group <Group Address> [source <Source Address>]"},*/
  30,   /*{"PPP"},*/
  442,  /*igmp_grp.html*/
  0
 },
 {
  353,  /*{"show ip igmp sources"},*/
  38,   /*{"EXEC"},*/
  443,  /*igmp_src.html*/
  0
 },
 {
  354,  /*{"ip igmp proxy-service"},*/
  39,   /*{"CONFIGURE"},*/
  444,  /*igp_conf.html*/
  0
 },
 {
  355,  /*{"ip igmp-proxy mrouter-version { 1 | 2 | 3 }"},*/
  30,   /*{"PPP"},*/
  445,  /*igp_upiface.html*/
  0
 },
 {
  356,  /*{"show ip igmp-proxy forwarding-database [{ Vlan <vlan-id/vfi-id> | group <group-address> | source <source-address> }]"},*/
  38,   /*{"EXEC"},*/
  446,  /*igp_mroute.html*/
  0
 },
 {
  356,  /*{"show ip igmp-proxy forwarding-database [{ Vlan <vlan-id/vfi-id> | group <group-address> | source <source-address> }]"},*/
  38,   /*{"EXEC"},*/
  447,  /*igp_nexthop.html*/
  0
 },
 {
  443,  /*{"show ip igmp statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},*/
  38,   /*{"EXEC"},*/
  448,  /*igp_stats.html*/
  0
 },
 {
  357,  /*{"set ip pim { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  449,  /*pim_basicsettings.html*/
  0
 },
 {
  366,  /*{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},*/
  39,   /*{"CONFIGURE"},*/
  450,  /*pim_component.html*/
  0
 },
 {
  358,  /*{"ip pim componentId  <value(1-255)>"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  451,  /*pim_ifaceconf.html*/
  0
 },
 {
  366,  /*{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},*/
  39,   /*{"CONFIGURE"},*/
  452,  /*pim_crpconf.html*/
  0
 },
 {
  359,  /*{"ip pim bidir-offer-interval <offer-interval> msecs"},*/
  39,   /*{"CONFIGURE"},*/
  453,  /*pim_globalconf.html*/
  0
 },
 {
  360,  /*{"ip pim state-refresh disable"},*/
  39,   /*{"CONFIGURE"},*/
  454,  /*pimdm_globconf.html*/
  0
 },
 {
  361,  /*{"show ip pim mroute [bidir] [ {proxy | {compid(1-255) | group-address | source-address } summary } ]"},*/
  38,   /*{"EXEC"},*/
  455,  /*pim_routeinfo.html*/
  0
 },
 {
  362,  /*{"show ip pim rp-set [rp-address] [bidir]"},*/
  38,   /*{"EXEC"},*/
  456,  /*pim_rpsetinfo.html*/
  0
 },
 {
  363,  /*{"show ip pim redundancy state"},*/
  38,   /*{"EXEC"},*/
  457,  /*pim_ha.html*/
  0
 },
 {
  364,  /*{"show ip pim rp-hash [<multicast_Group_address> <Group_mask>]"},*/
  38,   /*{"EXEC"},*/
  458,  /*pim_electedrp.html*/
  0
 },
 {
  365,  /*{"show ip pim interface [{ Vlan <vlan-id/vfi-id> [df] | <interface-type> <interface-id> [df] | <IP-interface-type> <IP-interface-number> | detail }]"},*/
  38,   /*{"EXEC"},*/
  459,  /*pim_dfinfo.html*/
  0
 },
 {
  366,  /*{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},*/
  39,   /*{"CONFIGURE"},*/
  460,  /*pim_srpconf.html*/
  0
 },
 {
  367,  /*{"set ip dvmrp { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  461,  /*dvmrp_conf.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  462,  /*dvmrpiface_conf.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  463,  /*dvmrp_route.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  464,  /*dvmrp_mcastroute.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  465,  /*dvmrp_neighbor.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  466,  /*dvmrp_prune.html*/
  0
 },
 {
  368,  /*{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},*/
  38,   /*{"EXEC"},*/
  467,  /*dvmrp_nexthop.html*/
  0
 },
 {
  370,  /*{"ip mcast profile <profile-id> [description (128)]"},*/
  39,   /*{"CONFIGURE"},*/
  468,  /*tac_profile.html*/
  0
 },
 {
  369,  /*{"debug tacm {all | [entry] [exit] [filter] [critical] [init-shut] [mgmt] [ctrl] [resource] [all-fail]}"},*/
  38,   /*{"EXEC"},*/
  469,  /*tac_traces.html*/
  0
 },
 {
  370,  /*{"ip mcast profile <profile-id> [description (128)]"},*/
  39,   /*{"CONFIGURE"},*/
  470,  /*tac_prffilter.html*/
  0
 },
 {
  371,  /*{"shutdown ethernet-oam"},*/
  39,   /*{"CONFIGURE"},*/
  471,  /*eoam_basicsettings.html*/
  0
 },
 {
  372,  /*{"ethernet-oam {disable | enable}"},*/
  42,   /*{"INTERFACE"},*/
  472,  /*eoam_portsettings.html*/
  0
 },
 {
  373,  /*{"ethernet-oam link-monitor [{symbol-period | frame | frame-period | frame-sec-summary}] {enable | disable}"},*/
  42,   /*{"INTERFACE"},*/
  473,  /*eoam_linkeventsettings.html*/
  0
 },
 {
  374,  /*{"ethernet-oam remote-loopback {disable | enable}"},*/
  42,   /*{"INTERFACE"},*/
  474,  /*eoam_loopbackconfig.html*/
  0
 },
 {
  435,  /*{"show port ethernet-oam [<interface-type> <interface-id>] event-log"},*/
  38,   /*{"EXEC"},*/
  546,  /*eoam_eventlog.html*/
  0
 },
 {
  433,  /*{"show port ethernet-oam [<interface-type> <interface-id>] statistics"},*/
  38,   /*{"EXEC"},*/
  544,  /*eoam_interfacestats.html*/
  0
 },
 {
  434,  /*{"show port ethernet-oam [<interface-type> <interface-id>] neighbor"},*/
  38,   /*{"EXEC"},*/
  545,  /*eoam_neighborstats.html*/
  0
 },
 {
  375,  /*{"shutdown fault-management"},*/
  39,   /*{"CONFIGURE"},*/
  475,  /*fm_basicsettings.html*/
  0
 },
 {
  376,  /*{"fault-management ethernet-oam {critical-event | dying-gasp | link-fault} action {none | warning}"},*/
  42,   /*{"INTERFACE"},*/
  476,  /*fm_portsettings.html*/
  0
 },
 {
  377,  /*{"fault-management ethernet-oam remote-loopback ([test] [count <no of packets(1-1000)>] [packet <size(64-1500)>] [pattern <hex_string(8)>] [wait-time <integer(1-10)>])"},*/
  42,   /*{"INTERFACE"},*/
  477,  /*fm_loopbackconfig.html*/
  0
 },
 {
  378,  /*{"set fault-management ethernet-oam mib-request <branchleaf:branchleaf:...>"},*/
  42,   /*{"INTERFACE"},*/
  478,  /*fm_mibrequestconfig.html*/
  0
 },
 {
  379,  /*{"show port fault-management ethernet-oam [<interface-type> <interface-id>] mib-variable response"},*/
  38,   /*{"EXEC"},*/
  479,  /*fm_mibresponse.html*/
  0
 },
 {
  380,  /*{"peer-dead-interval <interval(40-20000)>"},*/
  36,   /*{"REDUNDANCY"},*/
  480,  /*rmconfig.html*/
  0
 },
 {
  381,  /*{"set rmon { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  481,  /*rmon_globalconf.html*/
  0
 },
 {
  382,  /*{"rmon alarm <alarm-number> <mib-object-id (255)>   <sample-interval-time (1-65535)> {absolute | delta }   rising-threshold <value (0-2147483647)> [rising-event-number (1-65535)]   falling-threshold <value (0-2147483647)> [falling-event-number (1-65535)]   [owner <ownername (127)>]"},*/
  39,   /*{"CONFIGURE"},*/
  482,  /*rmon_alarmconf.html*/
  0
 },
 {
  383,  /*{"rmon collection stats <index (1-65535)> [owner <ownername (127)>]"},*/
  42,   /*{"INTERFACE"},*/
  483,  /*rmon_ethstats.html*/
  0
 },
 {
  384,  /*{"rmon event <number (1-65535)> [description <event-description (127)>] [log] [owner <ownername (127)>] [trap <community (127)>]"},*/
  39,   /*{"CONFIGURE"},*/
  484,  /*rmon_eventconf.html*/
  0
 },
 {
  385,  /*{"rmon collection history <index (1-65535)> [buckets <bucket-number (1-65535)>] [interval <seconds (1-3600)>] [owner <ownername (127)>]"},*/
  42,   /*{"INTERFACE"},*/
  485,  /*rmon_historyconf.html*/
  0
 },
 {
  386,  /*{"stack {priority {PM | BM | PS}}{switchid <NodeId (1-16)>}{ports <StackPortCount(1-4)>}"},*/
  31,   /*{"BOOTCONFIG"},*/
  486,  /*stackconfigure.html*/
  0
 },
 {
  387,  /*{"show stack { brief | counters | switchid <integer(1-16)> | details}"},*/
  31,   /*{"BOOTCONFIG"},*/
  487,  /*stackshowdetails.html*/
  0
 },
 {
  387,  /*{"show stack { brief | counters | switchid <integer(1-16)> | details}"},*/
  31,   /*{"BOOTCONFIG"},*/
  488,  /*stackshowbrief.html*/
  0
 },
 {
  387,  /*{"show stack { brief | counters | switchid <integer(1-16)> | details}"},*/
  31,   /*{"BOOTCONFIG"},*/
  489,  /*stackcounters.html*/
  0
 },
 {
  388,  /*{"rmon2 {enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  490,  /*rmonv2_globalconf.html*/
  0
 },
 {
  389,  /*{"shutdown ptp"},*/
  39,   /*{"CONFIGURE"},*/
  491,  /*ptp_globalconf.html*/
  0
 },
 {
  390,  /*{"debug ptp { global | { vrf | switch } <string(32)> } { all | [init-shut] [mgmt] [datapath] [ctrl] [pkt-dump] [resource] [all-fail] [buffer] [critical] }"},*/
  41,   /*{"USEREXEC"},*/
  492,  /*ptp_traceconf.html*/
  0
 },
 {
  391,  /*{"ptp { mode { ordinary | boundary | e2etransparent | p2ptransparent | forward } | priority1 <value(0-255)> | priority2 <value(0-255)>}"},*/
  32,   /*{"CLKMODE"},*/
  493,  /*ptp_basicsettings.html*/
  0
 },
 {
  392,  /*{"ptp port [{ ipv4 | ipv6 }] {<iftype> <ifindex> | vlan <vlan-id/vfi-id>}"},*/
  32,   /*{"CLKMODE"},*/
  494,  /*ptp_if_conf.html*/
  0
 },
 {
  393,  /*{"show ptp current [{ vrf | switch } <context-name>] [domain <id (0-127)>]"},*/
  41,   /*{"USEREXEC"},*/
  495,  /*ptp_current.html*/
  0
 },
 {
  394,  /*{"show ptp parent [{ vrf | switch } <context-name>] [domain <id (0-127)>] [stats]"},*/
  41,   /*{"USEREXEC"},*/
  496,  /*ptp_parentinfo.html*/
  0
 },
 {
  395,  /*{"show ptp port [{[{ vrf | switch } <string (32)>] | [{ ipv4 | ipv6 }]          {<ifXtype> <ifnum> | vlan <vlan-id/vfi-id> [switch <string(32)>]}}]         [domain <short (0-127)>]"},*/
  41,   /*{"USEREXEC"},*/
  497,  /*ptp_portinfo.html*/
  0
 },
 {
  396,  /*{"show ptp time-property [{ vrf | switch } <context-name>][domain <id(0-127)>]"},*/
  41,   /*{"USEREXEC"},*/
  498,  /*ptp_timeinfo.html*/
  0
 },
 {
  397,  /*{"show ptp foreign-master-record [{ vrf | switch } <context-name>] [domain <id (0-127)>]"},*/
  41,   /*{"USEREXEC"},*/
  499,  /*ptp_foreignmaster.html*/
  0
 },
 {
  398,  /*{"ptp acceptable-master enable [domain <short(0-127)>]"},*/
  42,   /*{"INTERFACE"},*/
  500,  /*ptp_accmaster.html*/
  0
 },
 {
  399,  /*{"ptp alternate-time-scale key <value(0-254)> name <string(10)>"},*/
  32,   /*{"CLKMODE"},*/
  501,  /*ptp_alttime.html*/
  0
 },
 {
  400,  /*{"clock variance <value(0-255)>"},*/
  39,   /*{"CONFIGURE"},*/
  502,  /*clockiwf_settings.html*/
  0
 },
 {
  401,  /*{"dsmon {enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  503,  /*dsmon_globalconf.html*/
  0
 },
 {
  402,  /*{"clear interfaces [ <interface-type> <interface-id> ] counters"},*/
  41,   /*{"USEREXEC"},*/
  504,  /*interface_clearstats.html*/
  0
 },
 {
  403,  /*{"show interfaces [{ <interface-type> <interface-id> | vlan <vlan_vfi_id> [switch <switch-name>] | tunnel <tunnel-id(0-128)> | ppp <short(1-4094)> | HC}] counters"},*/
  41,   /*{"USEREXEC"},*/
  505,  /*interface_stats.html*/
  0
 },
 {
  404,  /*{"show tcp statistics"},*/
  41,   /*{"USEREXEC"},*/
  506,  /*tcp_stats.html*/
  0
 },
 {
  405,  /*{"show tcp listeners"},*/
  41,   /*{"USEREXEC"},*/
  507,  /*tcp_listnrs.html*/
  0
 },
 {
  405,  /*{"show tcp listeners"},*/
  38,   /*{"EXEC"},*/
  507,  /*tcp_listnrs.html*/
  0
 },
 {
  406,  /*{"show tcp connections"},*/
  41,   /*{"USEREXEC"},*/
  508,  /*tcp_conns.html*/
  0
 },
 {
  407,  /*{"show udp statistics [vrf <vrf-name>]"},*/
  41,   /*{"USEREXEC"},*/
  509,  /*udp_stats.html*/
  0
 },
 {
  408,  /*{"show udp connections [vrf <vrf-name>]"},*/
  41,   /*{"USEREXEC"},*/
  510,  /*udp_conns.html*/
  0
 },
 {
  463,  /*{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  511,  /*bridge_info.html*/
  0
 },
 {
  409,  /*{"show vlan [brief | id <vlan-range> | summary | ascending] [ switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  512,  /*vlan_currentdb.html*/
  0
 },
 {
  410,  /*{"show vlan statistics [vlan <vlan-range>] [ switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  513,  /*vlan_portstats.html*/
  0
 },
 {
  411,  /*{"show mac-address-table [vlan <vlan-range>] [address <aa:aa:aa:aa:aa:aa>] [{interface <interface-type> <interface-id> | switch <context_name>}]"},*/
  41,   /*{"USEREXEC"},*/
  514,  /*vlan_dyngrptable.html*/
  0
 },
 {
  412,  /*{"show vlan counters [vlan <vlan-range>] [ switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  515,  /*vlan_counterstats.html*/
  0
 },
 {
  413,  /*{"show vlan device capabilities [ switch <context_name>]"},*/
  41,   /*{"USEREXEC"},*/
  516,  /*vlan_devicecapa.html*/
  0
 },
 {
  414,  /*{"show dot1d mac-address-table [address <aa:aa:aa:aa:aa:aa>] [{interface <interface-type> <interface-id> | switch <context_name>}]"},*/
  41,   /*{"USEREXEC"},*/
  517,  /*vlan_fdbentries.html*/
  0
 },
 {
  416,  /*{"show spanning-tree detail [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  518,  /*mstp_info.html*/
  0
 },
 {
  417,  /*{"clear spanning-tree [mst <instance-id>] counters  [interface <interface-type> <interface-id>]"},*/
  33,   /*{"VCM"},*/
  519,  /*mstp_cistportstats.html*/
  0
 },
 {
  415,  /*{"show spanning-tree mst [<instance-id(1-64|4094)>] interface <interface-type> <interface-id> [{ stats | hello-time | detail }]"},*/
  38,   /*{"EXEC"},*/
  520,  /*mstp_mstiportstats.html*/
  0
 },
 {
  416,  /*{"show spanning-tree detail [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  521,  /*rstp_info.html*/
  0
 },
 {
  417,  /*{"clear spanning-tree [mst <instance-id>] counters  [interface <interface-type> <interface-id>]"},*/
  33,   /*{"VCM"},*/
  522,  /*rstp_portstats.html*/
  0
 },
 {
  418,  /*{"show spanning-tree vlan <vlan-id/vfi-id> bridge [{address | detail | forward-time | hello-time | id | max-age | priority [system-id] | protocol}][ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  523,  /*pvrst_info.html*/
  0
 },
 {
  419,  /*{"show spanning-tree vlan <vlan-id/vfi-id> detail [active] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  524,  /*pvrst_instinfo.html*/
  0
 },
 {
  420,  /*{"show spanning-tree vlan <vlan-id/vfi-id> interface <ifXtype> <ifnum> [{ cost | detail | priority | rootcost | state | stats }]"},*/
  38,   /*{"EXEC"},*/
  525,  /*pvrst_portinfo.html*/
  0
 },
 {
  421,  /*{"show spanning-tree interface <ifXtype> <ifnum> [{ cost | encapsulationtype | priority | portfast | rootcost |rootguard | restricted-role | restricted-tcn | state | stats | detail }]"},*/
  38,   /*{"EXEC"},*/
  526,  /*pvrst_instportinfo.html*/
  0
 },
 {
  422,  /*{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},*/
  41,   /*{"USEREXEC"},*/
  527,  /*la_portlacpstats.html*/
  0
 },
 {
  422,  /*{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},*/
  41,   /*{"USEREXEC"},*/
  528,  /*la_neighbourstats.html*/
  0
 },
 {
  423,  /*{"clear lldp counters"},*/
  39,   /*{"CONFIGURE"},*/
  529,  /*lldp_traffic.html*/
  0
 },
 {
  424,  /*{"show lldp statistics"},*/
  38,   /*{"EXEC"},*/
  530,  /*lldp_stats.html*/
  0
 },
 {
  425,  /*{"show lldp errors"},*/
  38,   /*{"EXEC"},*/
  531,  /*lldp_errors.html*/
  0
 },
 {
  426,  /*{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},*/
  41,   /*{"USEREXEC"},*/
  532,  /*pnac_sessionstats.html*/
  0
 },
 {
  426,  /*{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},*/
  41,   /*{"USEREXEC"},*/
  533,  /*pnac_suppsessionstats.html*/
  0
 },
 {
  426,  /*{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},*/
  41,   /*{"USEREXEC"},*/
  534,  /*pnac_macsessionstats.html*/
  0
 },
 {
  427,  /*{"show radius statistics"},*/
  41,   /*{"USEREXEC"},*/
  535,  /*radius_statistics.html*/
  0
 },
 {
  428,  /*{"ip igmp snooping clear counters [Vlan <vlanid/vfi_id>]"},*/
  39,   /*{"CONFIGURE"},*/
  536,  /*igs_clearstats.html*/
  0
 },
 {
  429,  /*{"show ip igmp snooping statistics [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  537,  /*igs_stats.html*/
  0
 },
 {
  429,  /*{"show ip igmp snooping statistics [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},*/
  41,   /*{"USEREXEC"},*/
  538,  /*igs_v3stats.html*/
  0
 },
 {
  430,  /*{"show ip arp [vrf <vrf-name>][ { Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-id> | <ipiftype> <ifnum> | <ip-address> | <mac-address> | summary | information | statistics }]"},*/
  38,   /*{"EXEC"},*/
  539,  /*arpcache_stats.html*/
  0
 },
 {
  431,  /*{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},*/
  38,   /*{"EXEC"},*/
  540,  /*icmp_stats.html*/
  0
 },
 {
  431,  /*{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},*/
  38,   /*{"EXEC"},*/
  541,  /*ipv4ifsp_stats.html*/
  0
 },
 {
  431,  /*{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},*/
  38,   /*{"EXEC"},*/
  542,  /*ipv4syssp_stats.html*/
  0
 },
 {
  432,  /*{"show rmon [statistics [<stats-index (1-65535)>]] [alarms] [events] [history [history-index (1-65535)] [overview]]"},*/
  38,   /*{"EXEC"},*/
  543,  /*rmon_etherstats.html*/
  0
 },
 {
  433,  /*{"show port ethernet-oam [<interface-type> <interface-id>] statistics"},*/
  38,   /*{"EXEC"},*/
  544,  /*eoam_interfacestats.html*/
  0
 },
 {
  434,  /*{"show port ethernet-oam [<interface-type> <interface-id>] neighbor"},*/
  38,   /*{"EXEC"},*/
  545,  /*eoam_neighborstats.html*/
  0
 },
 {
  435,  /*{"show port ethernet-oam [<interface-type> <interface-id>] event-log"},*/
  38,   /*{"EXEC"},*/
  546,  /*eoam_eventlog.html*/
  0
 },
 {
  436,  /*{"show port fault-management ethernet-oam [<interface-type> <interface-id>] remote-loopback {current-session | last-session} [detail]"},*/
  38,   /*{"EXEC"},*/
  547,  /*fm_lbcurrentstats.html*/
  0
 },
 {
  436,  /*{"show port fault-management ethernet-oam [<interface-type> <interface-id>] remote-loopback {current-session | last-session} [detail]"},*/
  38,   /*{"EXEC"},*/
  548,  /*fm_lblaststats.html*/
  0
 },
 {
  437,  /*{"show ip dhcp client stats"},*/
  38,   /*{"EXEC"},*/
  549,  /*dhcpc_statistics.html*/
  0
 },
 {
  438,  /*{"show ip dhcp server statistics"},*/
  38,   /*{"EXEC"},*/
  550,  /*dhcpserver_stats.html*/
  0
 },
 {
  439,  /*{"show ip dhcp relay information [vlan <vlan-id>]"},*/
  38,   /*{"EXEC"},*/
  551,  /*dhcpr_statistics.html*/
  0
 },
 {
  440,  /*{"show ipv6 mld snooping statistics [Vlan <vlan-id/vfi-id>] [switch <string (32)>]"},*/
  41,   /*{"USEREXEC"},*/
  552,  /*mlds_stats.html*/
  0
 },
 {
  467,  /*{"show ipv6 mld statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},*/
  38,   /*{"EXEC"},*/
  553,  /*mlds_v2stats.html*/
  0
 },
 {
  441,  /*{"show { mrp | mvrp | mmrp } statistics               [{port <interface-type> <interface-id> | switch <context-name>}]"},*/
  41,   /*{"USEREXEC"},*/
  554,  /*mrp_portrxstats.html*/
  0
 },
 {
  441,  /*{"show { mrp | mvrp | mmrp } statistics               [{port <interface-type> <interface-id> | switch <context-name>}]"},*/
  41,   /*{"USEREXEC"},*/
  555,  /*mrp_porttxstats.html*/
  0
 },
 {
  442,  /*{"show ptp counters [{ vrf | switch } <context-name>][domain <id (0-127)>]"},*/
  41,   /*{"USEREXEC"},*/
  556,  /*ptp_portstats.html*/
  0
 },
 {
  443,  /*{"show ip igmp statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},*/
  38,   /*{"EXEC"},*/
  557,  /*igmproute_stats.html*/
  0
 },
 {
  444,  /*{"show ip ospf redundancy"},*/
  38,   /*{"EXEC"},*/
  558,  /*ospfred_stats.html*/
  0
 },
 {
  445,  /*{"show ip ospf [vrf <name>] route"},*/
  38,   /*{"EXEC"},*/
  559,  /*ospfroute_stats.html*/
  0
 },
 {
  446,  /*{"show ip ospf [vrf <name>] [area-id] database [{database-summary | self-originate | adv-router <ip-address>}]"},*/
  38,   /*{"EXEC"},*/
  560,  /*ospf_linkstdb.html*/
  0
 },
 {
  447,  /*{"show ipv6 ospf redundany"},*/
  41,   /*{"USEREXEC"},*/
  561,  /*ospfv3red_stats.html*/
  0
 },
 {
  448,  /*{"show ipv6 ospf [vrf <contextname>] route"},*/
  41,   /*{"USEREXEC"},*/
  562,  /*ospfv3route_stats.html*/
  0
 },
 {
  449,  /*{"show ip rip [vrf <name>] { database [ <ip-address> <ip-mask> ] | statistics }"},*/
  41,   /*{"USEREXEC"},*/
  563,  /*rip_interfacestats.html*/
  0
 },
 {
  450,  /*{"show ipv6 rip stats"},*/
  38,   /*{"EXEC"},*/
  564,  /*rip6_interfacestats.html*/
  0
 },
 {
  451,  /*{"show ipv6 rip {database}"},*/
  41,   /*{"USEREXEC"},*/
  565,  /*rip6_route.html*/
  0
 },
 {
  452,  /*{"show ipsecv6 stat <if/ah-esp/intruder>"},*/
  41,   /*{"USEREXEC"},*/
  566,  /*secv6_stats.html*/
  0
 },
 {
  453,  /*{"show vrrp [interface  { vlan <VlanId/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>} <VrId(1-255)>] [{brief|detail |statistics}]"},*/
  41,   /*{"USEREXEC"},*/
  567,  /*vrrp_stats.html*/
  0
 },
 {
  454,  /*{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},*/
  41,   /*{"USEREXEC"},*/
  568,  /*ipv6if_stats.html*/
  0
 },
 {
  454,  /*{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},*/
  41,   /*{"USEREXEC"},*/
  569,  /*ipv6syssp_stats.html*/
  0
 },
 {
  454,  /*{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},*/
  41,   /*{"USEREXEC"},*/
  570,  /*ipv6ifsp_stats.html*/
  0
 },
 {
  454,  /*{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},*/
  41,   /*{"USEREXEC"},*/
  571,  /*icmp6_stats.html*/
  0
 },
 {
  454,  /*{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},*/
  41,   /*{"USEREXEC"},*/
  572,  /*icmp6if_stats.html*/
  0
 },
 {
  455,  /*{"show ip mroute"},*/
  38,   /*{"EXEC"},*/
  573,  /*ipv4mcroute_stats.html*/
  0
 },
 {
  455,  /*{"show ip mroute"},*/
  38,   /*{"EXEC"},*/
  574,  /*ipv4mcnexthp_stats.html*/
  0
 },
 {
  456,  /*{"show ip bgp summary"},*/
  38,   /*{"EXEC"},*/
  575,  /*bgp_peerstats.html*/
  0
 },
 {
  457,  /*{"show ip bgp info"},*/
  38,   /*{"EXEC"},*/
  576,  /*bgp_grstats.html*/
  0
 },
 {
  458,  /*{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},*/
  38,   /*{"EXEC"},*/
  577,  /*elmi_basicstats.html*/
  0
 },
 {
  458,  /*{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},*/
  38,   /*{"EXEC"},*/
  578,  /*elmi_portstats.html*/
  0
 },
 {
  458,  /*{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},*/
  38,   /*{"EXEC"},*/
  579,  /*elmi_errorstats.html*/
  0
 },
 {
  459,  /*{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  580,  /*ecfm_stats.html*/
  0
 },
 {
  459,  /*{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  581,  /*ecfm_portstats.html*/
  0
 },
 {
  459,  /*{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  582,  /*ecfm_mepstats.html*/
  0
 },
 {
  460,  /*{"show ethernet cfm errors [domain <domain-name> | level <level-id(0-7)>][switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  583,  /*ecfm_errors.html*/
  0
 },
 {
  461,  /*{"show ip dns statistics"},*/
  38,   /*{"EXEC"},*/
  584,  /*dns_stats.html*/
  0
 },
 {
  462,  /*{"show vpn global statistics"},*/
  38,   /*{"EXEC"},*/
  585,  /*vpn_stats.html*/
  0
 },
 {
  463,  /*{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  586,  /*pbrstp_cvlanstats.html*/
  0
 },
 {
  463,  /*{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  587,  /*pbrstp_portinfo.html*/
  0
 },
 {
  463,  /*{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  588,  /*pbrstp_portstats.html*/
  0
 },
 {
  464,  /*{"set ipv6 mld { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  589,  /*mld_conf.html*/
  0
 },
 {
  465,  /*{"ipv6 mld router"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  590,  /*mld_iface.html*/
  0
 },
 {
  466,  /*{"show ipv6 mld sources"},*/
  38,   /*{"EXEC"},*/
  591,  /*mld_src.html*/
  0
 },
 {
  467,  /*{"show ipv6 mld statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},*/
  38,   /*{"EXEC"},*/
  592,  /*mldroute_stats.html*/
  0
 },
 {
  468,  /*{"ip msdp { enable | disable }"},*/
  39,   /*{"CONFIGURE"},*/
  593,  /*msdp_globalconf.html*/
  0
 },
 {
  469,  /*{"ip msdp peer <IP address> connect-source {vlan <id>}"},*/
  39,   /*{"CONFIGURE"},*/
  594,  /*msdp_peertableconf.html*/
  0
 },
 {
  470,  /*{"show ip msdp sa-cache [{ <IP Address> | <Mcast Address>}]"},*/
  38,   /*{"EXEC"},*/
  595,  /*msdp_sacache.html*/
  0
 },
 {
  471,  /*{"ip msdp originator-id <IP Address>"},*/
  39,   /*{"CONFIGURE"},*/
  596,  /*msdp_rp.html*/
  0
 },
 {
  472,  /*{"ip msdp mesh-group <mesh Group Name(64)> <Peer IP address>"},*/
  39,   /*{"CONFIGURE"},*/
  597,  /*msdp_mesh.html*/
  0
 },
 {
  473,  /*{"ip msdp peer-filter {accept-all | deny-all} [{ routemap <route map(20)>}]"},*/
  39,   /*{"CONFIGURE"},*/
  598,  /*msdp_peerfilter.html*/
  0
 },
 {
  474,  /*{"ip msdp redistribute [routemap <routemap name(20)>]"},*/
  39,   /*{"CONFIGURE"},*/
  599,  /*msdp_saredist.html*/
  0
 },
 {
  475,  /*{"shutdown dns"},*/
  39,   /*{"CONFIGURE"},*/
  600,  /*dns_conf.html*/
  0
 },
 {
  476,  /*{"debug dns {init-shutdown | ctrl | query | response | cache | failure | all}"},*/
  38,   /*{"EXEC"},*/
  601,  /*dns_traces.html*/
  0
 },
 {
  477,  /*{"ip domain name <string>"},*/
  39,   /*{"CONFIGURE"},*/
  602,  /*dns_domainname.html*/
  0
 },
 {
  478,  /*{"domain name-server {ipv4 <ucast_addr> | ipv6 <ip6_addr>}"},*/
  39,   /*{"CONFIGURE"},*/
  603,  /*dns_nameserver.html*/
  0
 },
 {
  479,  /*{"clear ip domain cache"},*/
  39,   /*{"CONFIGURE"},*/
  604,  /*dns_cache.html*/
  0
 },
 {
  480,  /*{"enable"},*/
  34,   /*{"FIREWALL"},*/
  605,  /*fwl_basicsettings.html*/
  0
 },
 {
  481,  /*{"untrusted port ([<interface-type> <0/a-b, 0/c, ...>] [<interface-type> <0/a-b, 0/c, ...>] [{ppp|multilink} <a,b,c-d>] [vlan <a,b,c-d>]) [trap-threshold <Max Packet Discard Count>]"},*/
  34,   /*{"FIREWALL"},*/
  606,  /*fwl_interfaceconf.html*/
  0
 },
 {
  482,  /*{"filter add <filter name> {<Source IP range>|any} {<Dest IP range>|any} {<tcp|udp|icmp|igmp|ggp|ip|egp|igp|nvp|rsvp|igrp|ospf|any|other <1-255>>} [srcport <range>] [destport <range>]"},*/
  34,   /*{"FIREWALL"},*/
  607,  /*fwl_filterconf.html*/
  0
 },
 {
  483,  /*{"access-list <acl name > {in|out} <filter name> {permit|deny} <priority val> [log {brief|detail|none}]  [fragment {permit|deny}]"},*/
  34,   /*{"FIREWALL"},*/
  608,  /*fwl_aclconf.html*/
  0
 },
 {
  484,  /*{"dmz <DMZ Host IP>"},*/
  34,   /*{"FIREWALL"},*/
  609,  /*fwl_dmzconf.html*/
  0
 },
 {
  485,  /*{"ipv6 dmz <DMZ IPv6 Host>"},*/
  34,   /*{"FIREWALL"},*/
  610,  /*fwl_dmzconfv6.html*/
  0
 },
 {
  486,  /*{"ipv6 icmp inspect [destination-unreachable] [time-exceeded]                [parameter-problem] [echo-request] [echo-reply] [redirect]                 [information-request] [information-reply]"},*/
  34,   /*{"FIREWALL"},*/
  611,  /*fwl_icmpinspect.html*/
  0
 },
 {
  487,  /*{"url filter add <URL-String>"},*/
  34,   /*{"FIREWALL"},*/
  612,  /*fwl_urlfilterconf.html*/
  0
 },
 {
  488,  /*{"show firewall logs"},*/
  38,   /*{"EXEC"},*/
  613,  /*fwl_log.html*/
  0
 },
 {
  489,  /*{"clear global statistics"},*/
  34,   /*{"FIREWALL"},*/
  614,  /*fwl_stats.html*/
  0
 },
 {
  490,  /*{"vpn remote identity {ipv4 | email | fqdn | dn | keyId | ipv6}               <id-value> { psk | cert } <preshared-key>/<key-id>"},*/
  39,   /*{"CONFIGURE"},*/
  615,  /*vpn_globalconf.html*/
  0
 },
 {
  491,  /*{"set vpn {enable | disable}"},*/
  39,   /*{"CONFIGURE"},*/
  616,  /*vpn_policy.html*/
  0
 },
 {
  494,  /*{"crypto map <policy-name>"},*/
  39,   /*{"CONFIGURE"},*/
  617,  /*vpn_ipsec.html*/
  0
 },
 {
  494,  /*{"crypto map <policy-name>"},*/
  39,   /*{"CONFIGURE"},*/
  618,  /*vpn_ikepreshared.html*/
  0
 },
 {
  492,  /*{"ra-vpn username <username> password <password>"},*/
  39,   /*{"CONFIGURE"},*/
  619,  /*vpn_userdb.html*/
  0
 },
 {
  493,  /*{"ip ra-vpn pool <poolname> <start_ip> - <end_ip>"},*/
  39,   /*{"CONFIGURE"},*/
  620,  /*vpn_addresspool.html*/
  0
 },
 {
  494,  /*{"crypto map <policy-name>"},*/
  39,   /*{"CONFIGURE"},*/
  621,  /*vpn_client_term.html*/
  0
 },
 {
  495,  /*{"ip multicast routing"},*/
  39,   /*{"CONFIGURE"},*/
  622,  /*ipv4mc_basicsettings.html*/
  0
 },
 {
  496,  /*{"ip mcast ttl-threshold <ttl-threshold (0-255)>"},*/
  35,   /*{"INTERFACE_ROUTER"},*/
  623,  /*ipv4mc_routeintfconf.html*/
  0
 },
 {
  497,  /*{"shutdown garp"},*/
  39,   /*{"CONFIGURE"},*/
  624,  /*vlan_garpbasicconf.html*/
  0
 },
 {
  498,  /*{"debug garp { global | [{protocol | gmrp | gvrp | redundancy}              [initshut] [mgmt] [data] [ctpl] [dump] [os] [failall] [buffer] [all]] [switch <context_name>] }"},*/
  41,   /*{"USEREXEC"},*/
  625,  /*vlangarp_traces.html*/
  0
 },
 {
  499,  /*{"interface {cpu0 | VlanMgmt | port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name>| ppp <1-10>}"},*/
  39,   /*{"CONFIGURE"},*/
  626,  /*l3context_interfacemapping.html*/
  0
 },
 {
  500,  /*{"mef transmode {provider-bridge | mpls }"},*/
  39,   /*{"CONFIGURE"},*/
  627,  /*transmode.html*/
  0
 },
 {
  501,  /*{"ethernet uni id <string(32)>"},*/
  36,   /*{"REDUNDANCY"},*/
  628,  /*uni.html*/
  0
 },
 {
  502,  /*{"ethernet evc id <string(0-32)>"},*/
  37,   /*{"VLAN_CONFIG"},*/
  629,  /*evc.html*/
  0
 },
 {
  503,  /*{"ethernet evc filter-instance <instance-id> filter-id <filter-no> dest-mac <destination-mac-address> {allow | drop}"},*/
  37,   /*{"VLAN_CONFIG"},*/
  630,  /*evc_filter.html*/
  0
 },
 {
  504,  /*{"ethernet map ce-vlan <ce-vlan> evc <evc>"},*/
  36,   /*{"REDUNDANCY"},*/
  631,  /*UniCVlanEvc.html*/
  0
 },
 {
  505,  /*{"mef e-tree evc <evc> ingress-port <port> egress-port-list <port-list>"},*/
  39,   /*{"CONFIGURE"},*/
  632,  /*mef_etree.html*/
  0
 },
 {
  506,  /*{"mef filter <filter-id> [evc <evc> [ dscp <0-63>]]              direction {in | out}"},*/
  37,   /*{"VLAN_CONFIG"},*/
  633,  /*mef_filter.html*/
  0
 },
 {
  507,  /*{"mef class <class>"},*/
  39,   /*{"CONFIGURE"},*/
  634,  /*class.html*/
  0
 },
 {
  508,  /*{"mef classmap <classmap-id> filter <filter-id> class <class>"},*/
  39,   /*{"CONFIGURE"},*/
  635,  /*classmap.html*/
  0
 },
 {
  509,  /*{"mef meter <meter-id> cir <0-10485760> cbs <0-10485760> eir <0-10485760> ebs <0-10485760> [color-aware] [coupling-flag]"},*/
  39,   /*{"CONFIGURE"},*/
  636,  /*meter.html*/
  0
 },
 {
  510,  /*{"mef policy-map <policy-map-id (1-65535)> class <1-65535>               [meter <1-65535>]"},*/
  39,   /*{"CONFIGURE"},*/
  637,  /*policymap.html*/
  0
 },
 {
  511,  /*{"ethernet evc frame loss {start | stop} md <md-Index> ma <ma-index>             mep <mep-index> {rmep-id <rmep-id> |             rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}             [interval <milliseconds(100-600000)>]             [count <num_of_observations(1-8192)>]             [deadline <seconds(1-172800)>] [switch <context_name>] "},*/
  37,   /*{"VLAN_CONFIG"},*/
  638,  /*mefmepfl.html*/
  0
 },
 {
  512,  /*{"ethernet evc frame delay {start | stop} type {one-way | two-way}             md <md-Index> ma <ma-index>             mep <mep-index> {rmep-id <rmep-id> |             rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}             [interval <milliseconds(100-10000)>]             [count <num_of_observations(1-8192)>]             [deadline <seconds(1-172800)>] [switch <context_name>]"},*/
  37,   /*{"VLAN_CONFIG"},*/
  639,  /*mefmepfd.html*/
  0
 },
 {
  513,  /*{"show ethernet evc frame loss md <md-Index> ma <ma-index> mep <mep-index> [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  640,  /*meffdstats.html*/
  0
 },
 {
  514,  /*{"show ethernet evc frame delay md <md-Index> ma <ma-index> mep <mep-index> [switch <context_name>]"},*/
  38,   /*{"EXEC"},*/
  641,  /*mefflstats.html*/
  0
 },
 {
  515,  /*{"ethernet evc availability measure {start | stop}            md <md-Index> ma <ma-index> mep <mep-index>             {rmep-id <integer(1-8191)> | rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}            [interval <milliseconds(100-3600000)>]            [windowsize <num_of_small_intervals(1-4099680000)>]            [deadline <seconds(1-409968000)>] [static-window | sliding-window]            [lowerthreshold <string(6)>] [upperthreshold <string(6)>]            [modestarea {available | unAvailable}]            [schldDownTime init <integer(1-172800)> end <integer(1-172800)>]            [switch <string(32)>]"},*/
  38,   /*{"EXEC"},*/
  642,  /*mefmepavailability.html*/
  0
 },
 {
  516,  /*{"shutdown aps [linear]"},*/
  39,   /*{"CONFIGURE"},*/
  643,  /*elps_contextinfo.html*/
  0
 },
 {
  521,  /*{"aps group-name <pg_name (32)>"},*/
  40,   /*{"ELPS_CONFIG"},*/
  644,  /*elps_pgcfmconf.html*/
  0
 },
 {
  517,  /*{"aps monitor {y1731 | mplsoam [ version <integer(0-1)> ] | none}"},*/
  40,   /*{"ELPS_CONFIG"},*/
  645,  /*elps_pginfo.html*/
  0
 },
 {
  518,  /*{"switch <name>"},*/
  39,   /*{"CONFIGURE"},*/
  646,  /*elps_pgcmd.html*/
  0
 },
 {
  519,  /*{"aps mpls-pw list <list-id> {working | protect} vc-id <integer>                   [peer-address {<ucast_addr> | <local-map-number>}]"},*/
  40,   /*{"ELPS_CONFIG"},*/
  647,  /*elps_servicelist.html*/
  0
 },
 {
  520,  /*{"show aps [linear] group-list protection port <interface_type> <interface_id>"},*/
  41,   /*{"USEREXEC"},*/
  648,  /*elps_pgshare.html*/
  0
 },
 {
  521,  /*{"aps group-name <pg_name (32)>"},*/
  40,   /*{"ELPS_CONFIG"},*/
  649,  /*elps_pgstats.html*/
  0
 },
 {
  522,  /*{"clear-config[default-config-restore <filename>]"},*/
  41,   /*{"USEREXEC"},*/
  650,  /*gen_infoclearconfig.html*/
  0
 },
 {
  523,  /*{"xconnect vfi <vfi-name (32)> [vlanmode            {other | portbased | nochange | changevlan            <vlan-id (1-4094)> | addvlan <vlan-id (1-4094)> | removevlan            <vlan-id (1-4094)> }] [port-vlan vlan <vlan-id (1-4094)>]"},*/
  42,   /*{"INTERFACE"},*/
  651,  /*mplsenet_conf.html*/
  0
 },
};
#define WEB_PRIV_MAX_LENGTH 658
#endif
