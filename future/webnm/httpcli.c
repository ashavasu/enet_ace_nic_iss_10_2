/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: httpcli.c,v 1.9 2016/04/09 09:57:10 siva Exp $
 * 
 * Description: Action routines for set/get objects in
 *              fshttp.mib              
 ********************************************************************/

#ifndef __HTTPCLI_C__
#define __HTTPCLI_C__

#include "enm.h"
#include "httpcli.h"
#include "httpclipt.h"
#include "fshttplw.h"
#include "fshttpwr.h"
#include "fshttpcli.h"
#include "ip6util.h"

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : httpcli.c                                       |
 * |                                                                          |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                        |
 * |                                                                          |
 * |  SUBSYSTEM NAME        : http                                            |
 * |                                                                          |
 * |  MODULE NAME           : http configuration                              |
 * |                                                                          |
 * |  LANGUAGE              : C                                               |
 * |                                                                          |
 * |  TARGET ENVIRONMENT    :                                                 |
 * |                                                                          |
 * |  DATE OF FIRST RELEASE :                                                 |
 * |                                                                          |
 * |  DESCRIPTION           : Action routines for set/get objects in          | 
 * |                          fshttp.mib                                      |
 * |                                                                          |
 *  ---------------------------------------------------------------------------
 */

INT1
cli_process_http_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[HTTP_MAX_ARGS];
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4RetStatus = HTTP_ZERO;
    UINT4               u4SrvAddr = HTTP_ZERO;
    INT4                i4AuthScheme = HTTP_ZERO;
    UINT4               u4ErrCode = HTTP_ZERO;
    UINT1              *pu1Inst = NULL;
    INT1                argno = HTTP_ZERO;

    MEMSET (au1Addr, HTTP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    va_start (ap, u4Command);

    pu1Inst = va_arg (ap, UINT1 *);    /* This is done to advance the pointer */

    while (HTTP_ONE)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == HTTP_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case HTTP_CLI_SET_AUTH_SCHEME:
            /* args[0] -> contains the HTTP Authentication scheme */
            if (args[HTTP_ZERO] != NULL)
            {
                i4AuthScheme = *(INT4 *) (VOID *) args[HTTP_ZERO];
                i4RetStatus = HttpSetAuthScheme (CliHandle, i4AuthScheme);
            }
            break;
        case HTTP_REDIRECT_ENABLE:
        case HTTP_REDIRECT_DISABLE:
            /* args[0] -> contains the Http Redirection Enable/Disable Status */
            i4RetStatus = HttpSetRedirectionStatus (CliHandle, u4Command);
            break;
        case HTTP_CLI_REDIRECT_V4_SERVERIP:
            /* args[0] -> contains the URL */
            /* args[HTTP_ONE] -> contains the Redirected Server v4 IP */
            u4SrvAddr = *(UINT4 *) (VOID *) (args[HTTP_ONE]);
            u4SrvAddr = OSIX_NTOHL (u4SrvAddr);
            MEMCPY (au1Addr, &u4SrvAddr, IPVX_IPV4_ADDR_LEN);
            i4RetStatus =
                HttpSetRedirectionSrvIp (CliHandle, (args[HTTP_ZERO]),
                                         au1Addr, IPVX_ADDR_FMLY_IPV4);
            break;
        case HTTP_CLI_REDIRECT_V6_SERVERIP:
            /* args[0] -> contains the URL */
            /* args[HTTP_ONE] -> contains the Redirected Server v6 IP */
            if (INET_ATON6 (args[HTTP_ONE], au1Addr) == HTTP_ZERO)
            {
                break;
            }
            i4RetStatus =
                HttpSetRedirectionSrvIp (CliHandle, (args[HTTP_ZERO]),
                                         au1Addr, IPVX_ADDR_FMLY_IPV6);
            break;
        case HTTP_CLI_REDIRECT_DOMAINNAME:
            /* args[0] -> contains the URL */
            /* args[HTTP_ONE] -> contains the Redirected Server Domain name */
            i4RetStatus =
                HttpSetRedirectionSrvDomainName (CliHandle, (args[HTTP_ZERO]),
                                                 (args[HTTP_ONE]));
            break;
        case HTTP_CLI_NO_REDIRECT_ALL:
            i4RetStatus = HttpDelRedirectionEntry (CliHandle, NULL);
            break;
        case HTTP_CLI_NO_REDIRECT_ENTRY:
            /* args[0] -> contains the URL */
            i4RetStatus =
                HttpDelRedirectionEntry (CliHandle, (args[HTTP_ZERO]));
            break;
        case HTTP_CLI_SHOW_AUTH_SCHEME:
            i4RetStatus = HttpShowAuthScheme (CliHandle);
            break;
        case HTTP_CLI_SHOW_HTTP_REDIRECT_URL:
            i4RetStatus =
                HttpShowRedirectionEntry (CliHandle, (args[HTTP_ZERO]));
            break;
        case HTTP_CLI_CLEAR_SRV_STATS:
            i4RetStatus = HttpClearSrvStats (CliHandle);
            break;
        default:
            /* args[0] -> contains the URL */
            i4RetStatus = HttpShowRedirectionEntry (CliHandle, NULL);
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > HTTP_ZERO) && (u4ErrCode < CLI_HTTP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", HttpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (HTTP_ZERO);
    }

    UNUSED_PARAM (pu1Inst);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpSetAuthScheme                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will set/reset the value of the      */
/*                        HTTP Authentication scheme                         */
/*                                                                           */
/*     INPUT            : i4Scheme - Http Authentication Scheme             */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpSetAuthScheme (tCliHandle CliHandle, INT4 i4Scheme)
{
    UINT4               u4ErrorCode = HTTP_ZERO;

    /* Test and Set the value of the Configurable Authentication
     * scheme parameter */
    if (nmhTestv2FsConfigHttpAuthScheme (&u4ErrorCode,
                                         i4Scheme) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    nmhSetFsConfigHttpAuthScheme (i4Scheme);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpSetRedirectionStatus                           */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable the           */
/*                        HTTP Redirection status                            */
/*                                                                           */
/*     INPUT            : i4Status  - Http Redirection stauts                */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpSetRedirectionStatus (tCliHandle CliHandle, UINT4 u4Command)
{
    UINT4               u4ErrorCode = HTTP_ZERO;
    INT4                i4Status = HTTP_ZERO;

    i4Status = (INT4) u4Command;

    if (nmhTestv2FsHttpRedirectionStatus (&u4ErrorCode,
                                          i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectionStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpSetRedirectionSrvIp                            */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT group         */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Group threshold                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpSetRedirectionSrvIp (tCliHandle CliHandle, UINT1 *pu1Url,
                         UINT1 *pu1SrvAddr, UINT1 u1AddrType)
{
    tSNMP_OCTET_STRING_TYPE Url;
    tSNMP_OCTET_STRING_TYPE SrvAddr;
    UINT1               au1SrvAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Url[HTTP_MAX_URL_LEN];
    UINT4               u4ErrorCode = HTTP_ZERO;
    INT4                i4RowStatus = HTTP_CREATE_AND_WAIT;
    UINT1               u1AddrLen = HTTP_ZERO;

    MEMSET (au1Url, HTTP_ZERO, sizeof (au1Url));
    MEMSET (au1SrvAddr, HTTP_ZERO, sizeof (au1SrvAddr));
    MEMSET (&Url, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SrvAddr, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (STRLEN (pu1Url) >= HTTP_MAX_URL_LEN)
    {
        CLI_SET_ERR (CLI_HTTP_URL_LEN_NOT_SUPPORTED);
        return CLI_FAILURE;
    }

    Url.pu1_OctetList = au1Url;
    MEMCPY (Url.pu1_OctetList, pu1Url, STRLEN (pu1Url));
    Url.i4_Length = STRLEN (pu1Url);

    SrvAddr.pu1_OctetList = au1SrvAddr;
    u1AddrLen =
        (u1AddrType ==
         IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    MEMCPY (SrvAddr.pu1_OctetList, pu1SrvAddr, u1AddrLen);
    SrvAddr.i4_Length = u1AddrLen;

    if (nmhValidateIndexInstanceFsHttpRedirectionTable (&Url) == SNMP_SUCCESS)
    {
        i4RowStatus = HTTP_NOT_IN_SERVICE;
    }

    if (nmhTestv2FsHttpRedirectionEntryStatus (&u4ErrorCode, &Url,
                                               i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHttpRedirectionEntryStatus (&Url, i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_HTTP_MAX_REDIRECTION_ENTRIES);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsHttpRedirectedSrvIP (&u4ErrorCode, &Url, &SrvAddr) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectedSrvIP (&Url, &SrvAddr) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsHttpRedirectedSrvAddrType (&u4ErrorCode, &Url,
                                              u1AddrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectedSrvAddrType (&Url, u1AddrType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectionEntryStatus (&Url, HTTP_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpSetRedirectionSrvDomainName                    */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT group         */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Group threshold                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpSetRedirectionSrvDomainName (tCliHandle CliHandle, UINT1 *pu1Url,
                                 UINT1 *pu1DomainName)
{
    tSNMP_OCTET_STRING_TYPE Url;
    tSNMP_OCTET_STRING_TYPE DomainName;
    UINT1               au1DomainName[HTTP_MAX_URL_LEN];
    UINT1               au1Url[HTTP_MAX_URL_LEN];
    UINT4               u4ErrorCode = HTTP_ZERO;
    INT4                i4RowStatus = HTTP_CREATE_AND_WAIT;

    MEMSET (au1Url, HTTP_ZERO, sizeof (au1Url));
    MEMSET (au1DomainName, HTTP_ZERO, sizeof (au1DomainName));
    MEMSET (&Url, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DomainName, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    Url.pu1_OctetList = au1Url;
    MEMCPY (Url.pu1_OctetList, pu1Url, STRLEN (pu1Url));
    Url.i4_Length = STRLEN (pu1Url);

    DomainName.pu1_OctetList = au1DomainName;
    MEMCPY (DomainName.pu1_OctetList, pu1DomainName, STRLEN (pu1DomainName));
    DomainName.i4_Length = STRLEN (pu1DomainName);

    if (nmhValidateIndexInstanceFsHttpRedirectionTable (&Url) == SNMP_SUCCESS)
    {
        i4RowStatus = HTTP_NOT_IN_SERVICE;
    }

    if (nmhTestv2FsHttpRedirectionEntryStatus (&u4ErrorCode, &Url,
                                               i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHttpRedirectionEntryStatus (&Url, i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_HTTP_MAX_REDIRECTION_ENTRIES);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsHttpRedirectedSrvDomainName (&u4ErrorCode, &Url, &DomainName)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectedSrvDomainName (&Url, &DomainName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsHttpRedirectionEntryStatus (&Url, HTTP_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpDelRedirectionEntry                            */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT group         */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Group threshold                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpDelRedirectionEntry (tCliHandle CliHandle, UINT1 *pu1Url)
{
    tSNMP_OCTET_STRING_TYPE Url;
    tSNMP_OCTET_STRING_TYPE NextUrl;
    UINT1               au1Url[HTTP_MAX_URL_LEN];
    UINT1               au1NextUrl[HTTP_MAX_URL_LEN];
    UINT4               u4ErrorCode = HTTP_ZERO;
    INT4                i4RetVal = HTTP_ZERO;

    MEMSET (au1Url, HTTP_ZERO, HTTP_MAX_URL_LEN);
    MEMSET (au1NextUrl, HTTP_ZERO, HTTP_MAX_URL_LEN);
    MEMSET (&Url, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextUrl, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    Url.pu1_OctetList = au1Url;
    NextUrl.pu1_OctetList = au1NextUrl;

    if (pu1Url != NULL)
    {
        MEMCPY (Url.pu1_OctetList, pu1Url, STRLEN (pu1Url));
        Url.i4_Length = STRLEN (pu1Url);

        if (nmhTestv2FsHttpRedirectionEntryStatus (&u4ErrorCode, &Url,
                                                   HTTP_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (u4ErrorCode);
            return CLI_FAILURE;
        }

        if (nmhSetFsHttpRedirectionEntryStatus (&Url, HTTP_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhGetFirstIndexFsHttpRedirectionTable (&Url) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {

        MEMSET (au1NextUrl, HTTP_ZERO, HTTP_MAX_URL_LEN);
        i4RetVal = nmhGetNextIndexFsHttpRedirectionTable (&Url, &NextUrl);

        if (nmhTestv2FsHttpRedirectionEntryStatus (&u4ErrorCode, &Url,
                                                   HTTP_DESTROY) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsHttpRedirectionEntryStatus (&Url, HTTP_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        MEMSET (au1Url, HTTP_ZERO, sizeof (au1Url));
        Url.pu1_OctetList = au1Url;
        MEMCPY (Url.pu1_OctetList, NextUrl.pu1_OctetList, NextUrl.i4_Length);
        Url.i4_Length = NextUrl.i4_Length;
    }
    while (i4RetVal == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : HttpShowAuthScheme                                */
/* Description       : This procedure shows the current status of HTTP   */
/*                     Authentication scheme configured                  */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
HttpShowAuthScheme (tCliHandle CliHandle)
{
    INT4                i4Scheme = HTTP_FOUR;
    INT4                i4RetVal = HTTP_ZERO;

    /* The Operational HTTP Authentication scheme */
    nmhGetFsOperHttpAuthScheme (&i4Scheme);
    if (i4Scheme == CLI_HTTP_DEFAULT)
    {
        CliPrintf (CliHandle,
                   "The Operational HTTP authentication scheme is Default\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else if (i4Scheme == CLI_HTTP_BASIC)
    {
        CliPrintf (CliHandle,
                   "The Operational HTTP authentication scheme is Basic\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else if (i4Scheme == CLI_HTTP_DIGEST)
    {
        CliPrintf (CliHandle,
                   "The Operational HTTP authentication scheme is Digest\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        i4RetVal = CLI_FAILURE;
    }

    /* The Configured HTTP Authentication scheme */
    i4Scheme = HTTP_FOUR;
    nmhGetFsConfigHttpAuthScheme (&i4Scheme);
    if (i4Scheme == CLI_HTTP_DEFAULT)
    {
        CliPrintf (CliHandle,
                   "The Configured HTTP authentication scheme is Default\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else if (i4Scheme == CLI_HTTP_BASIC)
    {
        CliPrintf (CliHandle,
                   "The Configured HTTP authentication scheme is Basic\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else if (i4Scheme == CLI_HTTP_DIGEST)
    {
        CliPrintf (CliHandle,
                   "The Configured HTTP authentication scheme is Digest\r\n");
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        i4RetVal = CLI_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpShowRedirectionEntry                           */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the SPT group         */
/*                        threshold                                          */
/*                                                                           */
/*     INPUT            : i4Threshold   - Group threshold                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
HttpShowRedirectionEntry (tCliHandle CliHandle, UINT1 *pu1Url)
{
    tSNMP_OCTET_STRING_TYPE Url;
    tSNMP_OCTET_STRING_TYPE NextUrl;
    tHttpRedirect      *pRedirectNode;
    UINT1               au1Url[HTTP_MAX_URL_LEN];
    UINT1               au1NextUrl[HTTP_MAX_URL_LEN];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = HTTP_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (au1Url, HTTP_ZERO, HTTP_MAX_URL_LEN);
    MEMSET (au1NextUrl, HTTP_ZERO, HTTP_MAX_URL_LEN);
    MEMSET (&Url, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextUrl, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    CliPrintf (CliHandle, "\r\nHTTP Redirection Entries\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");
    CliPrintf (CliHandle, "\r\n%-34s%-30s\r\n", "URL", "Server IP/DomainName");

    CliPrintf (CliHandle, "%-34s%-30s\r\n", "---", "--------------------");

    Url.pu1_OctetList = au1Url;
    NextUrl.pu1_OctetList = au1NextUrl;

    if (pu1Url != NULL)
    {
        MEMCPY (Url.pu1_OctetList, pu1Url, STRLEN (pu1Url));
        Url.i4_Length = STRLEN (pu1Url);

        pRedirectNode = HttpGetRedirectionEntry (&Url);
        if (pRedirectNode == NULL)
        {
            CLI_SET_ERR (CLI_HTTP_ENTRY_NOT_FOUND);
            return CLI_FAILURE;
        }
        /*PRINT Server IP or domain name */
        CliPrintf (CliHandle, "%-34s", pRedirectNode->ai1RedirectedUrl);

        if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, pRedirectNode->RedirectedSrvAddr.au1Addr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "%-30s", pu1String);
        }
        else if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%-30s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     pRedirectNode->RedirectedSrvAddr.au1Addr));
        }
        else
        {
            CliPrintf (CliHandle, "%-30s",
                       pRedirectNode->ai1RedirectSrvDomainName);
        }
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    if (nmhGetFirstIndexFsHttpRedirectionTable (&Url) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_HTTP_NO_ENTRIES_FOUND);
        return CLI_FAILURE;
    }

    do
    {
        pRedirectNode = HttpGetRedirectionEntry (&Url);

        if (pRedirectNode == NULL)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "%-34s", Url.pu1_OctetList);

        if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, pRedirectNode->RedirectedSrvAddr.au1Addr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "%-30s", pu1String);
        }
        else if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%-30s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     pRedirectNode->RedirectedSrvAddr.au1Addr));
        }
        else
        {
            CliPrintf (CliHandle, "%-30s",
                       pRedirectNode->ai1RedirectSrvDomainName);
        }
        CliPrintf (CliHandle, "\r\n");
        MEMSET (au1NextUrl, HTTP_ZERO, HTTP_MAX_URL_LEN);
        i4RetVal = nmhGetNextIndexFsHttpRedirectionTable (&Url, &NextUrl);

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        MEMSET (au1Url, HTTP_ZERO, sizeof (au1Url));
        MEMCPY (Url.pu1_OctetList, NextUrl.pu1_OctetList, NextUrl.i4_Length);
        Url.i4_Length = NextUrl.i4_Length;
    }
    while (HTTP_ONE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : HttpShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        of HTTP Module                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified module (pim/all), for         */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS /  CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
HttpShowRunningConfig (tCliHandle CliHandle)
{
    
    HttpShowRunningConfigScalar (CliHandle);

    if (HttpShowRunningConfigTable (CliHandle) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : HttpShowRunningConfigScalar                         */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        HTTP scalar objects.                                */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
HttpShowRunningConfigScalar (tCliHandle CliHandle)
{
    INT4                i4RetVal = HTTP_ZERO;
    INT4                i4Scheme = HTTP_FOUR;

    nmhGetFsHttpRedirectionStatus (&i4RetVal);

    if (i4RetVal != HTTP_REDIRECT_DISABLE)
    {
        CliPrintf (CliHandle, "set http redirection enable\r\n");
    }

    /* The configured authentication scheme */

    nmhGetFsConfigHttpAuthScheme (&i4Scheme);

    if (i4Scheme != CLI_HTTP_DEFAULT)
    {
        if (i4Scheme == CLI_HTTP_BASIC)
        {
            CliPrintf (CliHandle, "set http authentication-scheme basic\r\n");
        }
        else if (i4Scheme == CLI_HTTP_DIGEST)
        {
           CliPrintf (CliHandle, "set http authentication-scheme digest\r\n");
        }
    }
    
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : HttpShowRunningConfigTable                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays configuration for specified */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
HttpShowRunningConfigTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE Url;
    tSNMP_OCTET_STRING_TYPE NextUrl;
    tHttpRedirect      *pRedirectNode;
    UINT1               au1Url[HTTP_MAX_URL_LEN];
    CHR1               *pu1String = NULL;
    UINT4               u4Address = HTTP_ZERO;

    
    MEMSET (au1Url, HTTP_ZERO, sizeof (au1Url));
    MEMSET (&Url, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextUrl, HTTP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    Url.pu1_OctetList = au1Url;

    if (nmhGetFirstIndexFsHttpRedirectionTable (&Url) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    NextUrl.pu1_OctetList = au1Url;
    MEMCPY (NextUrl.pu1_OctetList, Url.pu1_OctetList, Url.i4_Length);
    NextUrl.i4_Length = Url.i4_Length;

    
    do
    {
        CliPrintf (CliHandle, "http redirect %s", NextUrl.pu1_OctetList);

        pRedirectNode = HttpGetRedirectionEntry (&NextUrl);

        if (pRedirectNode == NULL)
        {
            return CLI_FAILURE;
        }

        if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, pRedirectNode->RedirectedSrvAddr.au1Addr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, " server %s", pu1String);
        }
        else if (pRedirectNode->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, " server %s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                     pRedirectNode->RedirectedSrvAddr.au1Addr));
        }
        else
        {
            CliPrintf (CliHandle, " server %s",
                       pRedirectNode->ai1RedirectSrvDomainName);
        }
        CliPrintf (CliHandle, "\r\n");
    }
    while (nmhGetNextIndexFsHttpRedirectionTable (&Url, &NextUrl) ==
           SNMP_SUCCESS);
    
    return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name     : HttpClearSrvStats                                 */
/* Description       : This procedure clears the http request received   */
/*                     and http request discarded counter                */
/* Input(s)          : CliHandle - Handle to CLI session                 */
/* Output(s)         : None.                                             */
/* Returns           : CLI_SUCCESS/CLI_FAILURE                           */
/*************************************************************************/
INT4
HttpClearSrvStats (tCliHandle CliHandle)
{
    INT4                i4RstCounter = HTTP_ZERO;

    /* Clear the HTTP Request received, Request discarded counter */
    UNUSED_PARAM (CliHandle);

    if (nmhSetFsHttpRequestCount (i4RstCounter) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHttpRequestDiscards (i4RstCounter) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
