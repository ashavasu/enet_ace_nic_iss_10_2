/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: http.h,v 1.74 2017/12/11 10:02:04 siva Exp $
 *
 * Description: macros, typdefs and proto type for http server 
 *******************************************************************/

#ifndef _HTTP_H
#define _HTTP_H

#include "iss.h"

/* HTTP Global Macros*/
#define HTTP_GET       1
#define HTTP_POST      2
#define HTTP_HEAD      3
#define HTTP_PUT       4

#define WEBNM_HTTP_PORT              gi4HttpPort
#define WEBNM_HTTP_ADDR              gu4HttpSrvrBindAddr 
#define WEBNM_HTTP_TIME_OUT          gu1HttpTimeOutEnable
#define WEBNM_HTTP_LOGIN_REQ         gb1HttpLoginReq

#ifdef WEB_LNXIP_WANTED
#define DEFAULT_HTTP_PORT      6080
#else
#define DEFAULT_HTTP_PORT      80
#endif 
#ifdef WLC_WANTED
#define HTTP_TCP_MAX_SEG_SIZE 1300
#endif
#define HTTP_MAX_SOC_WRITE_LEN 1024
#define HTTP_KEEPALIVE_SIZE    1000
#ifdef ISS_WANTED
#define FLASH_FILE               "flash:"
#endif
#define HTTP_FILE_NAME_SIZE        128

#define HTTP_CONVERSION_FACTOR     10
#define HTTP_TASK_PRIORITY 100
#define HTTP_STACK_SIZE    OSIX_DEFAULT_STACK_SIZE
#define HTTP_PROCESSING_TASK_NAME    "HRT"
#define HTTP_PROCESSING_TASK_NAME_LEN  5
#define HTTP_PROCESSING_QUEUE_NAME   "HRQ"

#define HTTP_REQUEST  1
#define WEBNM_REQUEST 2
#define CHECK_POINTER(x,y)   if(x>=y) return ENM_FAILURE;
#define GET_CHAR_RETURN_CHECK(x) \
{\
    if(x == -1) {\
    return ENM_FAILURE;\
    }\
}

/*macros used for identify the type of linked list*/
#define HTTP_CLIENT_INFO_LIST       1
#define HTTP_CLIENT_TMR_INFO_LIST   2

/*Idle time of the client connection*/
#define HTTP_CONNECTION_IDLE_TIMEOUT IssGetWebMaxSessionTimeOut () 
/* Expiry time of the Global Nonce Array */
#define HTTP_NONCE_EXPIRE_TIME                 105

#define HTTP_REQTYPE  0
#define SSL_REQTYPE   1

/* General Purpose Macros*/
#define HTTP_INVALID_TASK_ID   0
#define HTTP_INVALID_SOCK_FD   -1
#define HTTP_INVALID_Q_ID      -1
#define HTTP_INVALID_SOCK_FLAG -1
#define HTTP_INVALID_SOCK_TYPE 0
#define HTTP_DATA_ON_NEW_CONNECTION       1
#define HTTP_DATA_ON_OLD_CONNECTION       2

/*Macors related to client requests*/
#define ENM_NORMAL_REQ                 1
#define ENM_PIPELINED_REQ              2

/*Macros related to events*/
#define HTTP_CTRL_EVENT                        0x01
#define HTTPSRV_TCP_NEW_CONN_RCVD              0x02
#define HTTPSRV_TCP6_NEW_CONN_RCVD             0x04
#define HTTPSRV_SECURE4_NEW_CONN_RCVD          0x08
#define HTTPSRV_SECURE6_NEW_CONN_RCVD          0x10
#define HTTP_CLIENT_DATA_RCVD_EVENT            0x20
#define HTTP_PROCESSING_COMPLETE_EVENT         0x40
#define HTTP_TIMER_EXPIRY_EVENT                0x80
#define HTTP_WAKEUP_EVENT                      0x100
#define HTTP_DATA_ARRIVAL                      0x200
#define HTTP_DATA_ON_SECURE_SOCK               0x400
#define HTTP_DATA_ON_OLD_SOCK                  0x800
#define HTTPS_CTRL_EVENT                       0x1000
#define HTTP_MSR_RSTR_COMPLETE_EVENT           0x2000 /*Event indicating
                                                        MSR completion*/
#define HTTPSRV_EVENT_WAIT_FLAG           OSIX_WAIT
#define HTTPSRV_EVENT_NO_WAIT_FLAG        OSIX_NO_WAIT 

#define ENM_CLIENT_SERVED                 1
#define ENM_CLIENT_NOT_SERVED             2

#define HTTP_MAX_BOUNDARY_LEN  100
#define HTTP_TCP_MAX_WIN_SIZE  65535
#define HTTP_SCR_BUFFER   120000
#define HTTP_UPGRADE_MAX_RETRY 300000
#define HTTP_TEXT_HTML_RESOURCE               1
#define HTTP_TEXT_STATIC_HTML_RESOURCE        2
#define HTTP_TEXT_JS_RESOURCE                 3
#define HTTP_IMG_JPG_RESOURCE                 4
#define HTTP_IMG_GIF_RESOURCE                 5

#define HTTP_GET                     1
#define HTTP_POST                    2
#define HTTP_VERSION_STRING          "1.1"
#define HTTP_VERSION_NUMERIC_VALUE   11 
#define HTTP_CHUNK_HEADER_SIZE       12
#define ENM_COMPLETE_REQ                       1
#define ENM_INCOMPLETE_REQ                     2
#define ENM_PIPELINED_INCOMPLETE_REQ           3

#define MAX_NUM_TASKS         3

#define HTTP_ERROR_HTML "\r\n<HTML>\n<HEAD><TITLE>Error Observed\
</TITLE></HEAD>\n<BODY BGCOLOR=white>\n<H1>Error Observed</H1>\n\
<P>Error: </BODY></HTML>"

#define HTTP_FLASH_ERROR_HTML "\r\n<HTML>\n<HEAD><TITLE>Error Observed\
</TITLE></HEAD>\n<BODY BGCOLOR=white>\n<P>\n<FONT SIZE=3>\n<H4>Error:Unable to \
locate Treeview scripts, copy the files from code/opensource/html to current \
working directory and Relogin.</H4>\n</FONT>\n</P>\n<P>Error: </BODY></HTML>"

#ifdef WEBNM_TEST_WANTED
extern INT4 WebnmSTSizingMemCreateMemPool (VOID);
extern VOID WebnmSTSizingMemDeleteMemPool (VOID);
extern VOID WebnmInitTestSessionRecord PROTO((VOID));
#define WEBNM_TEST_MEM_CREATE() WebnmSTSizingMemCreateMemPool() 
#define WEBNM_TEST_MEM_DELETE() WebnmSTSizingMemDeleteMemPool()
#define WEBNM_TEST_CREATE_TEST_NODE(x,y,z) WebnmCreateTestUserNode(x,y,z)
#define WEBNM_TEST_INIT_SESSION_RECORD() WebnmInitTestSessionRecord()
#else
#define WEBNM_TEST_MEM_CREATE() OSIX_SUCCESS
#define WEBNM_TEST_MEM_DELETE() ((void)0)
#define WEBNM_TEST_CREATE_TEST_NODE(x,y,z) ((void)0)
#define WEBNM_TEST_INIT_SESSION_RECORD() ((void)0)
#endif /* WEBNM_TEST_WANTED */

typedef struct Ipv4Sec {
   INT4  i4SockFd;
   struct sockaddr_in ClientAddr;
} tIpv4Sec;


typedef struct Ipv6Sec {
   INT4  i4SockFd;
   struct sockaddr_in6 ClientV6Addr;
} tIpv6Sec;


typedef struct HSReasonPhraseEntry {
   const char  *pi1ReasonPhrase;
   UINT4       u4Status;
} tHSReasonPhraseEntry;

#ifdef __HTTP_C__

VOID (*gaHttpTmrExpHdlr[HTTP_MAX_TIMER]) (tHttpTmrNode *) = {NULL};
tHttpInfo  gHttpInfo;
tMemPoolId gHttpMaxEnmPoolId;

extern tMemPoolId gWebAllocBufferPoolId;

tHSReasonPhraseEntry aHSReasonPhrase[ ] = {
       {"Continue", 100},
       {"OK", 200},
       {"Found", 302},
       {"Not Modified", 304},
    {"RM Bulk Update in Progress", 306},
       {"Bad Request", 400},
       {"Unauthorized", 401},
       {"Not Found", 404},
       {"Method Not Allowed", 405},
       {"Response Not Acceptable by client", 406},
       {"Content Length is Too Big", 413},
       {"Request-URI Too Long", 413},
       {"Internal Server Error", 500},
       {"Not Implemented", 501},
       {"HTTP Version Not Supported", 505},
       {NULL,0}
};

UINT1 HttpUrlStr[6][50] = {
        {"/wmi/login"},
        {"/wmi/page/"},
        {"/wmi/submit/"},
#ifdef ISS_WANTED
        {"/wmi/logout"},
#endif
        {"/wmi/relogin"},
        {"NULL"}
};


INT1 gi1Start[MAX_NUM_TASKS];
tHttp gHttp;

INT4 gi4Sockid=0;
UINT1 HTTP_INDEX_URL[]="/";
UINT1 HTTP_INDEX_FILE_NAME[]="index.html";
UINT1 HTTP_POST_METHOD[]="POST";
UINT1 HTTP_GET_METHOD[]="GET";
UINT1 HTTP_PUT_METHOD[]="PUT";
UINT1 HTTP_HEAD_METHOD[]="HEAD";
UINT1 HTTP_REQ_MSG_DELIMITER[]="\r\n\r\n";
UINT1 HTTP_CONTENT_STRING[]="content-length: ";
UINT1 HTTP_CONTENT_TYPE_MULTIPART[]="content-type: multipart";
UINT1 HTTP_CONTENT_TYPE[]="content-type:";
UINT1 HTTP_ACCEPT[]="accept: ";
/* To handle HTTP authentication */
UINT1 gaHTTP_TRUE[]="TRUE";
UINT1 gaHTTP_FALSE[]="FALSE";
UINT1 gaHTTP_AUTHORISATION[]="Authorization: ";
UINT1 gaHTTP_BASIC[]="Basic ";
UINT1 gaHTTP_DIGEST[]="Digest ";
UINT1 gaHTTP_NONCE_SECRET[]="GUESSME";
UINT1 gaHTTP_USERNAME[]="username=";
UINT1 gaHTTP_REALM[]="realm=";
/* The realm strings */
UINT1 gaHTTP_REALM_ISS[]="ISS";
UINT1 gaHTTP_REALM_TIMEOUT[]="ISS as your Session has timed-out";
UINT1 gaHTTP_REALM_WRONG_LOGIN[]=
"ISS as the Login Credentials are either Blank or Incorrect";
UINT1 gaHTTP_URI[]="uri=";
UINT1 gaHTTP_NONCE[]="nonce=";
UINT1 gaHTTP_ALGORITHM[]="algorithm=";
UINT1 gaHTTP_QOP[]="qop=";
UINT1 gaHTTP_NC[]="nc=";
UINT1 gaHTTP_CNONCE[]="cnonce=";
UINT1 gaHTTP_RESPONSE[]="response=";
/* End */
UINT1 HTTP_CONNECTION[]="connection: ";
UINT1 HTTP_CONNECTION_CLOSE[]="close";
UINT1 HTTP_IF_UNMODIFIED_SINCE[]="if-unmodified-since: ";
UINT1 HTTP_IF_MODIFIED_SINCE[]="if-modified-since: ";
UINT1 HTTP_IF_MATCH []="if-match: ";
UINT1 HTTP_IF_NONE_MATCH[]="if-none-match: ";
UINT1 HTTP_COMMON_DELIMITER[]="\r\n";

#ifdef WLC_WANTED
UINT1 WEBNM_HOME_PAGE[]="homepage_wss.html";
#else
UINT1 WEBNM_HOME_PAGE[]="homepage.html";
#endif

INT1 WEBNM_WMI[]="wmi";
UINT1 HTTP_HEADER_STRING[1000];

/* Request files extenstions */
UINT1 HTTP_HTML_EXTENSTION[]=".html"; /*448+278+160*/
UINT1 HTTP_HTM_EXTENSTION[]=".htm";   /*7*/
UINT1 HTTP_CSS_EXTENSTION[]=".css";   /*2*/
UINT1 HTTP_JS_EXTENSTION[]=".js";     /*5*/
UINT1 HTTP_JPG_EXTENSTION[]=".jpg";   /*9*/
UINT1 HTTP_GIF_EXTENSTION[]=".gif";   /*37*/
UINT1 HTTP_SUPP_ALL_ACCEPT[]="*/*";
UINT1 HTTP_SUPP_TEXT_ACCEPT[]="text/";
UINT1 HTTP_SUPP_IMAGE_ACCEPT[]="image/*";
UINT1 HTTP_SUPP_VIDEO_ACCEPT[]="video/*";

PRIVATE INT4 HttpSockInit PROTO((VOID));
PRIVATE INT4 HttpGetProtocol PROTO((tHttp *));
#ifdef IP6_WANTED
INT4 gi4Sock6id=0;
#endif
#endif
INT1 HttpProcessReqLine PROTO((tHttp *));
INT1 HttpGetChar PROTO((tHttp *));
VOID HttpShutDown PROTO((VOID));
VOID HttpInit PROTO((VOID));
INT1 HttpsSockInit (VOID);
VOID ProcessHttpReq (tHttp *pHttp);
VOID *HttpsAcceptNewConnection (INT4 i4Family, INT4 *pi4AcceptSock);
INT4 HttpReadFile (INT1 *, INT1 *, UINT4 *);
UINT4 HttpReadLine (tHttp * , INT1 *, UINT4 *);
VOID
HttpsSrvCtrlSockCallBk (INT4  i4SockId);  
INT1 WebFindPagePriv (tHttp *pHttp);
#ifdef IP6_WANTED
INT4 HttpSock6Init PROTO((VOID));
INT1 HttpsSock6Init PROTO((VOID));
#endif

#define HTTP_GET_CLIENT_TASK_MAP(x)      (gHttpInfo.HttpClientTaskMap[x])
#define HTTP_CONNECTION_COUNT()     (gHttpInfo.u1TotalClientConn)

#define    HTTP_SOCK_Q   "HTQ"
#define    HTTP_Q_ID     gHttpInfo.HttpMainTskQId

/*-------------------------*/
/* macro to Set System Time*/
#define HTTP_SET_SYS_TIME(Time) (Time) * (SYS_TIME_TICKS_IN_A_SEC)

/*------------------------------------------------------------------*/
/* macro to find base address of structure given the member address */
#define HTTP_GET_BASE_PTR(type, memberName,pMember) \
        (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName)) 

#define HTTP_TIMER_FLAG_RESET  0
#define HTTP_TIMER_FLAG_SET    1

/*------------------------------------------------------------------*/
/* Timer related macros macro to start timer other than oif timer */
#define HTTP_START_TIMER(TimerId, pTimerNode,\
            Duration, Status) \
{\
   Status = ENM_FAILURE;\
   if (HTTP_TIMER_FLAG_SET != (pTimerNode)->u1TmrStatus) {\
   (pTimerNode)->u1TimerId     = (TimerId);\
   Status = (INT4 ) TmrStartTimer (gHttpInfo.HttpTmrListId,\
                   (tTmrAppTimer *)(VOID *)(pTimerNode),\
                   HTTP_SET_SYS_TIME(Duration));\
   if (TMR_SUCCESS == Status) {\
       (pTimerNode)->u1TmrStatus   = HTTP_TIMER_FLAG_SET;\
       Status = ENM_SUCCESS;\
   }\
   else {\
       (pTimerNode)->u1TmrStatus   = HTTP_TIMER_FLAG_RESET;\
       Status = ENM_FAILURE;\
   }\
   }\
}

/*---------------------*/
/*---------------------*/
/* macro to stop timer */
#define HTTP_STOP_TIMER(pTimerNode) \
{\
   (pTimerNode)->u1TmrStatus = HTTP_TIMER_FLAG_RESET; \
   TmrStopTimer(gHttpInfo.HttpTmrListId, (tTmrAppTimer *)(VOID *) (pTimerNode) );\
}

#define HTTP_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define HTTPS_SEM_NAME      (const UINT1 *)"HTS"
#define HTTP_SEM_NAME       (const UINT1 *)"HTTP"
#define HTTP_MGMT_SEM_NAME  (const UINT1 *)"HMGT"
#define HTTP_DB_SEM_NAME  (const UINT1 *)"HDB"
#define HTTP_SEM_NODE_ID   SELF
#define HTTP_LOCK()  HttpLock ()
#define HTTP_UNLOCK() HttpUnLock ()
#define HTTP_DB_LOCK()  HttpDbLock ()
#define HTTP_DB_UNLOCK() HttpDbUnLock ()

#define HTTP_PROCESSING_TASK_NAME_ONE "HPT1"
#define TakeHttpsSem()     OsixSemTake (gHttpInfo.HttpCtrlInfo.SemId)
#define GiveHttpsSem()     OsixSemGive (gHttpInfo.HttpCtrlInfo.SemId)

#define SSL_TASK_NAME_LEN             OSIX_NAME_LEN + 4

/* Assuming that there won't be more than 99 tasks at a time */
#define HTTPS_MAX_CONCURRENT_TASKS    99

#ifdef ISS_WANTED

extern VOID  IssProcessSpecificPage PROTO((tHttp *));
extern INT4  IssProcessPost PROTO((tHttp *));
extern VOID  IssProcessGet PROTO((tHttp *));
extern VOID  IssRedirectHtmlPage PROTO((tHttp *));
#define ISS_WEB_REQUEST           3
#endif

#define HTTP_MAX_CHUNK_SIZE              5
#define HTTP_COMMON_DELIMITER_SIZE       4
#define HTTP_MAX_CHUNK_HEADER_LEN        (HTTP_COMMON_DELIMITER_SIZE + HTTP_MAX_CHUNK_SIZE)
#define HTTP_HTML_NAME_COMPARE_LEN       2
#define HTTP_GET_METHOD_SIZE             4
#define ISS_WEB_SPECIFIC_REQUEST         4

#ifdef __HTTP_C__
UINT1 ISS_INDEX_URL[]="/iss/";
UINT1 ISS_SPECIFIC_URL[]="/iss/specific/";

INT1  WEBNM_NO_WRITEACCESS [] = "Modification not allowed for this user";
INT1  WEBNM_NO_READACCESS [] = "User view not permitted";
#endif

VOID HttpPostMultiDataIndicationToMSR (tHttp * pHttp, INT4);
VOID    WebnmSendErr (tHttp *, CONST INT1 *);
UINT4 WebNMPolicyCheck PROTO((UINT4 ,UINT4 ,UINT4 ));
INT4 WebNMGetUserGroupsFromCli PROTO((UINT4 *));
VOID WebNmInitUserRecord PROTO((VOID));

INT4  HttpInitSrvInfo (VOID);
INT4  HttpGlobalTmrInit (VOID);
INT4  HttpGlobalSemInit (VOID);
INT4  HttpGlobalMemInit (VOID);
INT4  HttpAcceptNewConnection (INT4 i4Family);
INT4  HttpHandleDataRcvdOnOldConn (VOID);
tHttpClientInfo *HttpSearchClientTmrNode (INT4  i4SockId);
VOID  HttpCltIdleTmrExpHandler (tHttpTmrNode  *pTimerNode);
INT4  HttpWakeUpProcessingTasks 
          (INT4  i4ClientSockId, UINT1 u1ClientSockType, VOID *pConnID);
INT4  HttpAddNodeToList (VOID *pNode, INT4 i4ListType);
INT4  HttpDelNodeFromList (VOID *pNode, INT4 i4ListType);
INT4  HttpCheckIfClientIsServed (INT4 i4ClientSockId);
INT4  HttpCloseSocket (INT4 i4SockFd);
INT4  HttpGlobalInit (VOID);
VOID  HttpSrvSecureSock6CallBk (INT4 i4SockId);
VOID  HttpSrvSockCallBk (INT4 i4SockId);
VOID  HttpSrvSecureSock4CallBk (INT4 i4SockId);
VOID  HttpDeleteClientTaskEntryFromArray (INT4 i4SockId);
VOID  HttpHandleConnClose (INT4);
VOID  HttpProcessMain (VOID);
VOID  HttpProcHandleWakeUpEvent (INT4  i4ConnAge, tOsixTaskId  SelfTaskId);
VOID  HttpReqDataArrivalCallBk (INT4 i4SockId);
VOID  HttpSrvCtrlSockCallBk (INT4 i4SockId);
VOID  HttpProcHandleReqDataArrival (INT4 i4ReqType);
tHttp *HttpGetReqNode (INT4  i4ClientSockFd);
INT4  HttpFindQueueId (tOsixQId  *QueueId);
INT4  HttpGetContentLength (INT1  *pi1HayStack);
INT4  HttpCheckIsReqPipelined (tHttp *pHttp, INT4  *pi4ComleteReqLen);
INT1  *HttpFindLastOccuranceOfStr (INT1 *pHayStack, INT1 *pNeedle);
INT4  HttpSrvSelAddFd (INT4 i4SockId, VOID (*pCallBk) (INT4));
VOID  HttpReqOnSecureSockCallBk (INT4 i4SockId);
VOID  HttpProcHandleDataOnOldSock (VOID);
VOID  HttpReqDataOnOldSockCallBk (INT4 i4SockId);
INT4  HttpStoreDataInSockBuf (tHttp  *pHttp);
INT4  HttpRecvReq (tHttp  *pHttp);
VOID  HttpTmrExpHandler (VOID);
VOID  HttpStartProcessing (tHttp *pHttp);
VOID  HttpSrvSock6CallBk (INT4 i4SockId);
INT4  HttpDeQueueSockId (VOID);
VOID  HttpMapClientToTask (INT4 i4SockId, tOsixTaskId SelfTaskId, UINT1 u1SockType);
tHttpClientInfo  *HttpGetFirstClientInfoNode (VOID);
INT4  HttpGetSockTypeForConn (INT4 i4SockId);
VOID  HttpGlobalMemDeInit (VOID);
VOID  HttpGlobalSemDeInit (VOID);
VOID  HttpGlobalTmrDeInit (VOID);
INT4  HttpGlobalQInit (VOID);
VOID  HttpGlobalQDeInit (VOID);
INT4    HttpReadPostQuery (tHttp *pHttp);
INT4  HttpProcessReqHeader (tHttp *pHttp, INT1 *pi1Header, INT1 *pi1OrigHeader);
INT4  HttpReadReqHeader (tHttp * pHttp, INT1 *pi1Header, INT1 *pi1OrigHeader);
INT4    HttpValidateReqHeader (tHttp *pHttp);
INT4    HttpSendHeader (tHttp *pHttp , INT4 i4ResType);
INT4    HttpConstructHeader (tHttp *pHttp , UINT1 *au1Header, UINT4 u4ObjType);
INT4    HttpExtractHeaderInfo(INT1 *pHeaderBuf,UINT1 *pu1HeaderFeild,UINT1 *pu1HeaderValue);   
INT4    HttpGetDate (UINT1 *au1FullDate, INT1 i1FutureFlag);
INT4 FlushHttpBuff(tHttp *);
INT4 HttpLock (VOID);
INT4 HttpUnLock (VOID);
INT4 HttpDbLock (VOID);
INT4 HttpDbUnLock (VOID);
INT4    HttpInsertChunkSize (tHttp * pHttp, INT1 *pi1Buffer, INT4 i4Size, BOOL1 bFlag);
VOID    HttpSrvPktRcvd (INT4 i4SockId);
VOID    HttpAddClientToDb (INT4 i4SockFd,struct sockaddr_in  ClientAddr);
VOID    HttpDeleteClientFromDb (INT4  i4SockFd);
VOID    HttpSrvSelAddFdForClients (VOID);
INT4    HttpCheckforPipelinedRequest (tHttp *pHttp);
INT4    HttpSrvSelRemFd (INT4 i4SockId);
/* Function to generate the Request/Response digest */
INT1
HttpGenerateDigest (tHttp * pHttp, UINT1 *pu1Digest, UINT1 u1DigestFlag);
INT4 
HttpFormAuthInfoHeader (tHttp * pHttp, UINT1 * pu1AuthInfoHeader,
                        INT4 * pi4Length);
INT4 
HttpFormWWWAuthHeader  (tHttp * pHttp, UINT1 * pu1WWWAuthHeader,
                        INT4 * pi4Length);
INT4 
HttpProcessAuthHeader  (tHttp * pHttp, INT1 * pi1OrigHeader);
INT4 
HttpFormatExtractedStr (UINT1 * pu1InputString, UINT1 * pu1FormattedString);
VOID 
HttpGarbageNonceCollector (VOID);
VOID HttpSrvUpdateSelFd PROTO ((VOID));
#ifdef WLC_WANTED
UINT4 WebnmGetWebAuthClientInfo(INT4);
UINT4 WebnmWlanIfIndex(UINT1 *);
VOID IssProcessWebAuthRedirect (tHttp *pHttp, UINT2);
UINT4
WebnmGetWebAuthClientMac (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                                  void *pArg, void *pOut);

UINT1 WebnmIsWebAuthEnabled (UINT1 *);

UINT1
WebnmIsStaAuthorized (UINT1 * au1DestMacAddr);

INT4
WebnmStaAssembleWebRespPkts (tHttp *pHttp,INT1 i1Message);

#endif

#endif /* _HTTP_H */
