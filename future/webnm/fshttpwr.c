/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttpwr.c,v 1.4 2013/10/28 09:51:11 siva Exp $
*
* Description: Protocol Wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fshttplw.h"
# include  "fshttpwr.h"
# include  "fshttpdb.h"

VOID
RegisterFSHTTP ()
{
    SNMPRegisterMib (&fshttpOID, &fshttpEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fshttpOID, (const UINT1 *) "fshttp");
}

VOID
UnRegisterFSHTTP ()
{
    SNMPUnRegisterMib (&fshttpOID, &fshttpEntry);
    SNMPDelSysorEntry (&fshttpOID, (const UINT1 *) "fshttp");
}

INT4
FsHttpRedirectionStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsHttpRedirectionStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsOperHttpAuthSchemeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsOperHttpAuthScheme (&(pMultiData->i4_SLongValue)));
}

INT4
FsConfigHttpAuthSchemeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsConfigHttpAuthScheme (&(pMultiData->i4_SLongValue)));
}

INT4
FsHttpRequestCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsHttpRequestCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsHttpRequestDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsHttpRequestDiscards (&(pMultiData->i4_SLongValue)));
}

INT4
FsHttpRedirectionStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsHttpRedirectionStatus (pMultiData->i4_SLongValue));
}

INT4
FsConfigHttpAuthSchemeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsConfigHttpAuthScheme (pMultiData->i4_SLongValue));
}

INT4
FsHttpRequestCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsHttpRequestCount (pMultiData->i4_SLongValue));
}

INT4
FsHttpRequestDiscardsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsHttpRequestDiscards (pMultiData->i4_SLongValue));
}

INT4
FsHttpRedirectionStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsHttpRedirectionStatus (pu4Error,
                                              pMultiData->i4_SLongValue));
}

INT4
FsConfigHttpAuthSchemeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsConfigHttpAuthScheme (pu4Error,
                                             pMultiData->i4_SLongValue));
}

INT4
FsHttpRequestCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsHttpRequestCount (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsHttpRequestDiscardsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsHttpRequestDiscards
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsHttpRedirectionStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsHttpRedirectionStatus (pu4Error,
                                             pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsConfigHttpAuthSchemeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsConfigHttpAuthScheme (pu4Error,
                                            pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsHttpRequestCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsHttpRequestCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsHttpRequestDiscardsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsHttpRequestDiscards
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsHttpRedirectionTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsHttpRedirectionTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsHttpRedirectionTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsHttpRedirectedSrvAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsHttpRedirectedSrvAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsHttpRedirectedSrvIPGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsHttpRedirectedSrvIP (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsHttpRedirectedSrvDomainNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsHttpRedirectedSrvDomainName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsHttpRedirectionEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsHttpRedirectionEntryStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsHttpRedirectedSrvAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsHttpRedirectedSrvAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsHttpRedirectedSrvIPSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsHttpRedirectedSrvIP (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsHttpRedirectedSrvDomainNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsHttpRedirectedSrvDomainName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsHttpRedirectionEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsHttpRedirectionEntryStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsHttpRedirectedSrvAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsHttpRedirectedSrvAddrType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsHttpRedirectedSrvIPTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsHttpRedirectedSrvIP (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsHttpRedirectedSrvDomainNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsHttpRedirectedSrvDomainName (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsHttpRedirectionEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsHttpRedirectionEntryStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsHttpRedirectionTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsHttpRedirectionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
