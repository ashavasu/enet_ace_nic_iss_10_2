/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: http.c,v 1.154 2018/01/25 10:03:25 siva Exp $
 *
 * Description: Routines for HTTP Server Module 
 *******************************************************************/
#ifndef __HTTP_C__
#define __HTTP_C__

#include "enm.h"
#include "webnmutl.h"
#include "htmldata.h"
#include "httpcli.h"
#ifdef ISS_WANTED
#include "trgt_htmldata.h"
#endif /* ISS_WANTED */

#include "http.h"
#include "msr.h"

#ifdef CLI_WANTED
#include "cli.h"
#endif

#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif
#include "fshttpwr.h"
#ifdef SSL_WANTED
#include "httpssl.h"
tHttps              gHttps;
#endif
#include "webpages.h"

#ifdef WLC_WANTED
#include "wsssta.h"
#include "wssifinc.h"
#include "wssstawlcinc.h"
#endif

INT4                gi4HttpPort = DEFAULT_HTTP_PORT;
UINT4               gu4HttpSrvrBindAddr = HTTP_ZERO;
UINT1               gu1HttpTimeOutEnable = OSIX_TRUE;
BOOL1               gb1HttpLoginReq = OSIX_TRUE;

tHttpCtrl           gHttpCtrl;
fd_set              gaHttpFds[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
fd_set              grfds;
INT4                gi4WebSysLogId = HTTP_ZERO;

tIpv4Sec            gIpv4Sec[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
#ifdef IP6_WANTED
tIpv6Sec            gIpv6Sec[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT];
#endif
UINT4               gu4HttpMask = 0;

extern tISSHttpCfg  gIssHttpCfg;
extern INT4         gi4HttpsTrcLevel;
extern tISSGlbCfg   gIssGlbCfg;

#ifdef ISS_WANTED
extern VOID         IssLogout (tHttp *);
#endif
extern UINT4        gu4Stups;
extern INT4         gi4TftpSysLogId;

extern VOID         IssProcessWebAuthLoginPageGet (tHttp * pHttp);
extern VOID         IssProcessWebAuthLoginPageSet (tHttp * pHttp);

INT1 
     
     
     
     
     
     
     
    gai1FileData[HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT][HTTP_MAX_FLASH_FILESIZE];

fd_set             *gpTmpFd = NULL;
/* For Multidata traffic use temporary File descriptor */
UINT4               gu4DestIpAddr;

PUBLIC VOID         RegisterFSSSL (VOID);

/* The global nonce array */
tHttpServerNonceInfo gaHttpServerNonceInfo[ISS_MAX_WEB_SESSIONS] =
    { {HTTP_ZERO, {HTTP_ZERO,}}, };

/* The username and password allowed for ISS. This
 * is used for BASIC and DEFAULT auth schemes */
struct WebnmUserDetails gaWebnmUser[] = {
    {"root", "admin123"},
    {"iss", "iss123"},
    {"", "",}
};

/* The username and the HA1 digest calculated as
 * HEX(MD5(username:realm:password)) for ISS.
 * The realms used are 
 * 1. "ISS",
 * 2. "ISS as your Session has timed-out"
 * 3. "ISS as the Login Credentials are either Blank or Incorrect"
 * RESPECTIVELY & STRICTLY in the same order for each username.
 * This is used for Digest auth scheme */
struct WebnmDigestUserDetails gaWebnmDigestUser[HTTP_MAX_NO_OF_USERS] = {
    {"root", "admin123", {"c2f1fad1d88abdd3a4f950aa1142647d",
                          "1ed4d7b3efdc4a2b617ab3e4cba57389",
                          "b6f524579f7971a6ec3060570a5e6cc4"}},
    {"iss", "iss123", {"df7e3de6e0045a8106cdcf39157a5b93",
                       "a5ef2b300b4e2ef1af2c5de380fa27fb",
                       "7c8b9fad5e1898857060e60d0f940b47"}},
};

#ifdef WEBNM_TEST_WANTED
extern INT1         WebnmCreateTestUserNode (INT4, tOsixTaskId, UINT4);
#endif

#ifdef WLC_WANTED
extern tRBTree      gWssStaWepProcessDB;
UINT1               gu1WebAuthLoginSent = OSIX_FALSE;
UINT4               u4gTaskOneCount = 0;
UINT4               u4gTaskTwoCount = 0;
UINT4               u4gTaskThreeCount = 0;

#endif

/**************************************************************************/
/*   Function Name   : HttpInitSrvInfo                                    */
/*   Description     : This functions is used to intilize the Http Info   */
/*                     data structure                                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpInitSrvInfo (VOID)
{
    INT4                i4Count = HTTP_ZERO;
    UINT1               au1TaskName[HTTP_PROCESSING_TASK_NAME_LEN];
    tIssWebObjTree      HttpObjTree;

    MEMSET (au1TaskName, HTTP_ZERO, sizeof (au1TaskName));

    TMO_DLL_Init (&(gHttpInfo.HttpClientList));
    TMO_DLL_Init (&(gHttpInfo.HttpRedirectList));
    TMO_DLL_Init (&(gHttpInfo.HttpClientTmrList));

    MEMSET (&gHttpInfo.HttpTmrListId, HTTP_ZERO, sizeof (tTimerListId));
    MEMSET (&gHttpInfo.HttpMainTskQId, HTTP_INVALID_Q_ID, sizeof (tOsixQId));

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        gHttpInfo.HttpProcTaskId[i4Count] = HTTP_INVALID_TASK_ID;
        gHttpInfo.pRdTmpFd[i4Count] = NULL;
        gHttpInfo.pWrTmpFd[i4Count] = NULL;
        HTTP_GET_CLIENT_TASK_MAP (i4Count).HttpProcessingTskId =
            HTTP_INVALID_TASK_ID;
        HTTP_GET_CLIENT_TASK_MAP (i4Count).i4ClientSockId =
            HTTP_INVALID_SOCK_FD;
        HTTP_GET_CLIENT_TASK_MAP (i4Count).u1SockType = HTTP_INVALID_SOCK_TYPE;
    }

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_CLIENT_CONNECTIONS; i4Count++)
    {
        gHttpInfo.ai4ConnClients[i4Count] = HTTP_INVALID_SOCK_FD;
        gHttpInfo.au4ConnClientsIp[i4Count] = 0;
    }

    MEMSET (&gHttpInfo.HttpCtrlInfo, HTTP_ZERO, sizeof (tHttpCtrl));
    gHttpInfo.i4SrvSockId = HTTP_INVALID_SOCK_FD;
    gHttpInfo.i4SrvSock6Id = HTTP_INVALID_SOCK_FD;
    gHttpInfo.u1TotalClientConn = HTTP_ZERO;
    gHttpInfo.bRedirectStat = HTTP_REDIRECT_DISABLE;
    /* Assigning default auth schemes */
    gHttpInfo.i4ConfigHttpAuthScheme = HTTP_AUTH_DEFAULT;
    gHttpInfo.i4OperHttpAuthScheme = HTTP_AUTH_DEFAULT;

#ifdef SSL_WANTED
    if (SslArInit () < HTTP_ZERO)
    {
        ENM_TRACE ("Failed to Initialize SSL!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }
    gHttpInfo.HttpCtrlInfo.i4MaxSocketId = SslArGetCtrlSockFd ();
    SslArStart ();
#endif

    if (IssSystemInit () == ISS_FAILURE)
    {
        ENM_TRACE ("Failed to Initialize ISS Socket!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }
    WebNmInitUserRecord ();

    WEBNM_TEST_INIT_SESSION_RECORD ();

    HttpEnable ();
#ifdef SSL_WANTED
    HttpSrvSelAddFd (SslArGetCtrlSockFd (), HttpsSrvCtrlSockCallBk);
#endif

    if (HttpGlobalQInit () == ENM_FAILURE)
    {
        ENM_TRACE ("Failed to create Queue!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
    {
        if (HttpGlobalTmrInit () == ENM_FAILURE)
        {
            HttpGlobalQDeInit ();
            ENM_TRACE ("Failed to create Timer List!\n");
            /* Indicate the status of initialization to the main routine */
            return ENM_FAILURE;
        }
    }

    if (HttpGlobalSemInit () == ENM_FAILURE)
    {
        HttpGlobalQDeInit ();
        if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
        {
            HttpGlobalTmrDeInit ();
        }
        ENM_TRACE ("Failed to create semaphore!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    if (HttpGlobalMemInit () == ENM_FAILURE)
    {
        HttpGlobalQDeInit ();
        if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
        {
            HttpGlobalTmrDeInit ();
        }
        HttpGlobalSemDeInit ();
        ENM_TRACE ("Failed to create memory!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    IssHttpObjTreeInit (&HttpObjTree);
    IssHttpSetObjTree (&HttpObjTree);

    for (i4Count = HTTP_ONE; i4Count <= HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        SPRINTF ((CHR1 *) au1TaskName, "%s%d", HTTP_PROCESSING_TASK_NAME,
                 i4Count);
        if (OsixTskCrt
            (au1TaskName, HTTP_TASK_PRIORITY, HTTP_STACK_SIZE,
             (OsixTskEntry) HttpProcessMain, HTTP_ZERO,
             &(gHttpInfo.HttpProcTaskId[i4Count - HTTP_ONE])) == OSIX_FAILURE)
        {
            ENM_TRACE ("Error During Creation of HTTP Processing Task \n");
            return ENM_FAILURE;
        }
        /*No need to take HTTP lock here because Processing Task ID 
         * are not subject change after Initialization
         * All the Proceesing Task ID's are Initialized at start
         * and do not change so no need to talk lock for this */
        HTTP_GET_CLIENT_TASK_MAP (i4Count - HTTP_ONE).HttpProcessingTskId =
            gHttpInfo.HttpProcTaskId[i4Count - HTTP_ONE];
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGlobalQInit
 *  Description     : This function intilizes the Queue
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpGlobalQInit (VOID)
{

    if (OsixQueCrt ((UINT1 *) HTTP_SOCK_Q, OSIX_MAX_Q_MSG_LEN,
                    MAX_HTTP_Q_DEPTH, &(HTTP_Q_ID)) != OSIX_SUCCESS)
    {
        ENM_TRACE ("Unable to create Http Sock Info Queue \n");
        ENM_TRACE ("Exiting Function HttpMain \n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGlobalQDeInit
 *  Description     : This function DeInitilizes the Queue
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
VOID
HttpGlobalQDeInit (VOID)
{

    OsixQueDel (HTTP_Q_ID);

    return;
}

/************************************************************************
 *  Function Name   : HttpGlobalTmrInit
 *  Description     : This function Intitilizes the Timer
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpGlobalTmrInit (VOID)
{

    if (TmrCreateTimerList
        ((const UINT1 *) HTTP_TASK_NAME, HTTP_TIMER_EXPIRY_EVENT, NULL,
         &(gHttpInfo.HttpTmrListId)) == TMR_FAILURE)
    {
        ENM_TRACE ("Unable to create the TIMER LIST \n");
        return ENM_FAILURE;
    }

    gaHttpTmrExpHdlr[HTTP_CLT_IDLE_TIMER] = HttpCltIdleTmrExpHandler;

    return ENM_SUCCESS;
}

/************************************************************************
 * Function Name   : HttpGlobalTmrDeInit
 * Description     : This function removes the timer list
 * Input           : None
 * Output          : None
 * Returns         : ENM_SUCCESS or ENM_FAILURE
 *************************************************************************/
VOID
HttpGlobalTmrDeInit (VOID)
{
    /* Delete the Timer List */
    if (gHttpInfo.HttpTmrListId != HTTP_ZERO)
    {
        if (TmrDeleteTimerList (gHttpInfo.HttpTmrListId) != HTTP_ZERO)
        {
            ENM_TRACE ("Deleting PIM Timer List - FAILED \n");
        }
    }

    gaHttpTmrExpHdlr[HTTP_CLT_IDLE_TIMER] = NULL;

    return;
}

/************************************************************************
 *  Function Name   : HttpGlobalSemInit
 *  Description     : This function initilizes the Semaphores 
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpGlobalSemInit (VOID)
{

#ifdef SSL_WANTED
    if (OsixCreateSem (HTTPS_SEM_NAME, HTTP_ONE, HTTP_ZERO,
                       (tOsixSemId *) & gHttpInfo.HttpCtrlInfo.SemId) !=
        OSIX_SUCCESS)
    {
        ENM_TRACE ("Failed to create SEM!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }
#endif

    if (OsixCreateSem (HTTP_SEM_NAME, HTTP_ONE, OSIX_WAIT, &gHttpInfo.HttpSemId)
        != OSIX_SUCCESS)
    {
        ENM_TRACE ("Failed to create SEM!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    if (OsixCreateSem (HTTP_MGMT_SEM_NAME, HTTP_ONE, OSIX_WAIT,
                       &gHttpInfo.HttpMgmtSemId) != OSIX_SUCCESS)
    {
        ENM_TRACE ("Failed to create SEM!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    if (OsixCreateSem (HTTP_DB_SEM_NAME, HTTP_ONE, OSIX_WAIT,
                       &gHttpInfo.HttpDbSemId) != OSIX_SUCCESS)
    {
        ENM_TRACE ("Failed to create SEM!\n");
        /* Indicate the status of initialization to the main routine */
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGlobalSemDeInit 
 *  Description     : This function removes all the sempahores
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
VOID
HttpGlobalSemDeInit (VOID)
{

#ifdef SSL_WANTED
    OsixSemDel (gHttpInfo.HttpCtrlInfo.SemId);
#endif
    OsixSemDel (gHttpInfo.HttpSemId);
    OsixSemDel (gHttpInfo.HttpMgmtSemId);
    OsixSemDel (gHttpInfo.HttpDbSemId);

    return;
}

/************************************************************************
 *  Function Name   : HttpGlobalMemInit
 *  Description     : This function create the mem polls required
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpGlobalMemInit (VOID)
{

    INT4                i4Count = HTTP_ZERO;
    /* Dummy pointers for system sizing during run time */
    tHttpRedirectionBlock *pHttpRedirectionBlock = NULL;
    tWebnmResponseBlock *pWebnmResponseBlock = NULL;

    UNUSED_PARAM (pHttpRedirectionBlock);
    UNUSED_PARAM (pWebnmResponseBlock);

    if (WebnmSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return ENM_FAILURE;
    }

    if (WEBNM_TEST_MEM_CREATE () == OSIX_FAILURE)
    {
        return ENM_FAILURE;
    }

    gHttpInfo.HttpReqPoolId =
        WEBNMMemPoolIds[MAX_HTTP_PROCCESSING_TASKS_SIZING_ID];
    gHttpInfo.HttpRedirectMemPoolId =
        WEBNMMemPoolIds[MAX_HTTP_REDIRECTION_ENTRY_BLOCKS_SIZING_ID];
    gHttpInfo.HttpAccCltInfoPoolId =
        WEBNMMemPoolIds[MAX_HTTP_CLT_INFO_BLOCKS_SIZING_ID];
    gHttpInfo.WebnmResponsePoolId =
        WEBNMMemPoolIds[MAX_HTTP_WEBNM_RESP_BLOCKS_SIZING_ID];
    gHttpInfo.HttpSrvPktMsgPoolId = WEBNMMemPoolIds[MAX_HTTP_Q_DEPTH_SIZING_ID];
    gHttpInfo.WebnmUserNodePoolId =
        WEBNMMemPoolIds[MAX_HTTP_WEBNM_USER_NODES_SIZING_ID];
    gHttpInfo.MsrHttpMsgNodePoolId =
        WEBNMMemPoolIds[MAX_HTTP_MSG_NODES_SIZING_ID];
    gHttpInfo.MsrHttpDupMsgPoolId =
        WEBNMMemPoolIds[MAX_HTTP_DUP_MSG_NODES_SIZING_ID];
    gHttpInfo.WebnmTacacsPoolId =
        WEBNMMemPoolIds[MAX_WEBNM_TACACS_BLOCKS_SIZING_ID];
    gHttpMaxEnmPoolId = WEBNMMemPoolIds[MAX_ENM_BLOCKS_SIZING_ID];

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        gi1Start[i4Count] = FALSE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGlobalMemDeInit 
 *  Description     : This function removes the memory polls created
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
VOID
HttpGlobalMemDeInit (VOID)
{

    WebnmSizingMemDeleteMemPools ();

    WEBNM_TEST_MEM_DELETE ();

    return;
}

/************************************************************************
 *  Function Name   : HttpSrvSelAddFd 
 *  Description     : This function Add the socket to select utility
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpSrvSelAddFd (INT4 i4SockId, VOID (*pCallBk) (INT4))
{
    INT4                i4RetVal = HTTP_ZERO;

    i4RetVal = SelAddFd (i4SockId, pCallBk);

    if (i4RetVal == OSIX_FAILURE)
    {
        ENM_TRACE ("!!! select read fd add failed !!!\n");
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpSrvSelRemFd
 *  Description     : This function removes the Socket Id to Select Utility 
 *  Input           : None
 *  Output          : None
 *  Returns         : ENM_SUCCESS or ENM_FAILURE 
 ************************************************************************/
INT4
HttpSrvSelRemFd (INT4 i4SockId)
{
    INT4                i4RetVal = HTTP_ZERO;

    i4RetVal = SelRemoveFd (i4SockId);

    if (i4RetVal == OSIX_FAILURE)
    {
        ENM_TRACE ("!!! select read fd remove failed !!!\n");
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpAcceptNewConnection
 *  Description     : This function accepts the new connection and makes
 *                    socket to non-blocking and adds the Server socket
 *                    to select utility
 *  Input           : None
 *  Output          : None
 *  Returns         : client socket Id / ENM_FAILURE
 ************************************************************************/
INT4
HttpAcceptNewConnection (INT4 i4Family)
{
    struct sockaddr_in  ClientAddr;
#ifdef IP6_WANTED
    struct sockaddr_in6 ClientAddr6;
    INT4                i4Client6Len = sizeof (struct sockaddr_in6);
#endif
    INT4                i4Flags = HTTP_ZERO;
    INT4                i4SockClientId = HTTP_INVALID_SOCK_FD;
    INT4                i4ClientLen = sizeof (struct sockaddr_in);
    INT4                i4RetVal = ENM_FAILURE;
#ifdef WLC_WANTED
    UINT4               u4Counter = 0;
#endif
#ifdef IP6_WANTED
    MEMSET (&ClientAddr6, HTTP_ZERO, i4Client6Len);
#endif
    MEMSET (&ClientAddr, HTTP_ZERO, i4ClientLen);

    if (gHttpInfo.HttpCtrlInfo.bHttpEnable != TRUE)
    {
        ENM_TRACE ("Http: Not Enabled\n");
        return ENM_FAILURE;
    }

    /* Process the HTTP request */
    if (AF_INET == i4Family)
    {
        i4SockClientId = accept (gHttpInfo.i4SrvSockId,
                                 (struct sockaddr *) &ClientAddr,
                                 (socklen_t *) & i4ClientLen);
#ifdef WLC_WANTED
/* Added to store the Client details, which will be used later in the Web Auth
 * functionality to retrieve the Station's MAC address based on the socket ID.
 */
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv4Sec[u4Counter].i4SockFd == -1)
            {
                gIpv4Sec[u4Counter].i4SockFd = i4SockClientId;
                gIpv4Sec[u4Counter].ClientAddr = ClientAddr;
                break;
            }
        }
#endif
    }
#ifdef IP6_WANTED
    else if (AF_INET6 == i4Family)
    {
        i4SockClientId = accept (gHttpInfo.i4SrvSock6Id,
                                 (struct sockaddr *) &ClientAddr6,
                                 (socklen_t *) & i4Client6Len);
    }
#endif
    if (i4SockClientId < HTTP_ZERO)
    {
        HttpSrvSelAddFd (gHttpInfo.i4SrvSockId, HttpSrvSockCallBk);
#ifdef IP6_WANTED
        HttpSrvSelAddFd (gHttpInfo.i4SrvSock6Id, HttpSrvSock6CallBk);
#endif
        ENM_TRACE ("Http: Socket Accept Error\n");
        return ENM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    i4RetVal = fcntl (i4SockClientId, F_SETFL, i4Flags);
    if (i4RetVal < HTTP_ZERO)
    {
        ENM_TRACE ("HTTP: TCP server" " Fcntl SET Failure !!!\r\n");
        HttpSrvSelAddFd (gHttpInfo.i4SrvSockId, HttpSrvSockCallBk);
#ifdef IP6_WANTED
        HttpSrvSelAddFd (gHttpInfo.i4SrvSock6Id, HttpSrvSock6CallBk);
#endif /* IP6_WANTED */

        HttpCloseSocket (i4SockClientId);
        return ENM_FAILURE;
    }

    HttpSrvSelAddFd (gHttpInfo.i4SrvSockId, HttpSrvSockCallBk);
#ifdef IP6_WANTED
    HttpSrvSelAddFd (gHttpInfo.i4SrvSock6Id, HttpSrvSock6CallBk);
#endif /* IP6_WANTED */

    HttpAddClientToDb (i4SockClientId, ClientAddr);
    return i4SockClientId;
}

/************************************************************************
 *  Function Name   : HttpAddClientToDb
 *  Description     : This function adds the newly connected client to 
 *                    the global array
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpAddClientToDb (INT4 i4SockFd, struct sockaddr_in ClientAddr)
{
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_CLIENT_CONNECTIONS; i4Count++)
    {
        if (gHttpInfo.ai4ConnClients[i4Count] == HTTP_INVALID_SOCK_FD)
        {
            gHttpInfo.ai4ConnClients[i4Count] = i4SockFd;
            gHttpInfo.au4ConnClientsIp[i4Count] = ClientAddr.sin_addr.s_addr;
            break;
        }
    }
    return;
}

/************************************************************************
 *  Function Name   : HttpDeleteClientFromDb
 *  Description     : This function deletes the client socket ID from the
 *                    global array
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpDeleteClientFromDb (INT4 i4SockFd)
{
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_CLIENT_CONNECTIONS; i4Count++)
    {
        if (gHttpInfo.ai4ConnClients[i4Count] == i4SockFd)
        {
            gHttpInfo.ai4ConnClients[i4Count] = HTTP_INVALID_SOCK_FD;
            gHttpInfo.au4ConnClientsIp[i4Count] = 0;
            break;
        }
    }
    return;
}

/************************************************************************
 *  Function Name   : HttpSrvSockCallBk 
 *  Description     : This is callback function registered with
 *                    select utility so whenever a client is requesting
 *                    for connection this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvSockCallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTPSRV_TCP_NEW_CONN_RCVD);

    ENM_TRACE ("Http: EXIT \r\n");
}

/************************************************************************
 *  Function Name   : HttpSrvPktRcvd 
 *  Description     : This is callback function registered with
 *                    select utility so whenever server gets the data 
 *                    from the client this callback function is called
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvPktRcvd (INT4 i4SockId)
{
    tHttpSockId        *pMsg = NULL;

    ENM_TRACE ("HttpSrvPktRcvd: ENTRY \r\n");
    /* CRU Buff Allocation */
    pMsg = (tHttpSockId *) MemAllocMemBlk (gHttpInfo.HttpSrvPktMsgPoolId);
    if (pMsg == NULL)
    {
        ENM_TRACE ("CRU Buffer Allocation Failed\n");
        return;
    }
    /* Copying the socket fd to the allocated CRU buff */
    pMsg->i4SockFd = i4SockId;

    /* Send the Data to the queue */
    if (OsixQueSend (HTTP_Q_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gHttpInfo.HttpSrvPktMsgPoolId, (UINT1 *) pMsg);
        ENM_TRACE ("Send To HTTP server Q Failed\n");
        return;
    }

    ENM_TRACE ("HttpPktRcvd: Message sent to Q  \r\n");
    /* Send Event to the task */
    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTP_CLIENT_DATA_RCVD_EVENT);
}

/************************************************************************
 *  Function Name   : HttpSrvSock6CallBk 
 *  Description     : This is callback function registered with
 *                    select utility so whenever a client is requesting
 *                    for connection this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvSock6CallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTPSRV_TCP6_NEW_CONN_RCVD);

    ENM_TRACE ("Http: EXIT \r\n");
}

/************************************************************************
 *  Function Name   : HttpSrvCtrlSockCallBk 
 *  Description     : This is callback function registered with
 *                    select utility so whenever a client is requesting
 *                    for connection this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvCtrlSockCallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTP_CTRL_EVENT);

    ENM_TRACE ("Http: EXIT \r\n");
}

#ifdef SSL_WANTED
/************************************************************************
 *  Function Name   : HttpSrvSecureSock4CallBk
 *  Description     : This is callback function registered with
 *                    select utility so whenever a client is requesting
 *                    for connection this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvSecureSock4CallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTPSRV_SECURE4_NEW_CONN_RCVD);

    ENM_TRACE ("Http: EXIT \r\n");
}

/************************************************************************
 *  Function Name   : HttpsSrvCtrlSockCallBk
 *  Description     : This is callback function registered with
 *                    select utility so whenever a HTTPS receives control 
 *                    packet this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpsSrvCtrlSockCallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTPS_CTRL_EVENT);

    ENM_TRACE ("Http: EXIT \r\n");
}

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : HttpSrvSecureSock6CallBk
 *  Description     : This is callback function registered with
 *                    select utility so whenever a client is requesting
 *                    for connection this API is invoked by select
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSrvSecureSock6CallBk (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    ENM_TRACE ("HTTP: ENTRY \r\n");

    OsixEvtSend (gHttpInfo.HttpMainTskId, HTTPSRV_SECURE6_NEW_CONN_RCVD);

    ENM_TRACE ("Http: EXIT \r\n");
}
#endif /* IP6_WANTED */
#endif /*SSL_WANTED */

/**************************************************************************/
/*   Function Name   : HttpDeQueueSockId                                  */
/*   Description     : This functions Dequeues the socket Fd              */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpDeQueueSockId (VOID)
{
    INT4                i4SockFd = HTTP_INVALID_SOCK_FD;
    tHttpSockId        *pMsg = NULL;

    if (OsixQueRecv (HTTP_Q_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN, HTTP_ZERO) == OSIX_SUCCESS)
    {
        i4SockFd = pMsg->i4SockFd;
        MemReleaseMemBlock (gHttpInfo.HttpSrvPktMsgPoolId, (UINT1 *) pMsg);
        return i4SockFd;
    }

    return HTTP_INVALID_SOCK_FD;
}

/************************************************************************
 *  Function Name   : HttpWebnmConfigurePolicy
 *  Description     : Configure PolicyAccess and PolicyGroup
 *  Input           : None
 *  Output          : None
 *  Returns         : SUCCESS
 ************************************************************************/
INT4
HttpWebnmConfigurePolicy (tHttp * pHttp)
{
    INT4                i4Count = HTTP_ZERO;
    for (i4Count = HTTP_ZERO; PageArray1[i4Count].pi1Name != NULL; i4Count++)
    {
        if (STRCMP (PageArray1[i4Count].pi1Name, pHttp->ai1HtmlName) ==
            HTTP_ZERO)
        {
            pHttp->u4PageAccess = PageArray1[i4Count].u4Access;
            pHttp->u4GroupID = PageArray1[i4Count].u4UserGroup;
            return ENM_SUCCESS;
        }
    }
    return ENM_FAILURE;
}

/************************************************************************
 *  Function Name   : HSLaunch 
 *  Description     : Function to Create Http Task. Called from main
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HSLaunch (VOID)
{
    if (OsixTskCrt ((UINT1 *) HTTP_TASK_NAME, HTTP_TASK_PRIORITY,
                    HTTP_STACK_SIZE, (OsixTskEntry) HttpMain, HTTP_ZERO,
                    &gHttpInfo.HttpMainTskId) != OSIX_SUCCESS)
    {
        ENM_TRACE ("Error During Creation of HTTP Task \n");
    }
}

/************************************************************************
 *  Function Name   : HttpSockInit 
 *  Description     : Function will create the socket adds the server 
 *                    socket to select utility
 *  Input           : None 
 *  Output          : None
 *  Returns         : Socket Descriptor or Failure
 ************************************************************************/

PRIVATE INT4
HttpSockInit (VOID)
{
    INT4                i4Sock = HTTP_ZERO;
    INT4                i4OptVal = TRUE;
    INT4                i4Flags = HTTP_INVALID_SOCK_FLAG;
    INT4                i4WindowSize = HTTP_TCP_MAX_WIN_SIZE;
    INT4                i4WindowLen = sizeof (INT4);
#ifdef WLC_WANTED
    INT4                i4Mss = HTTP_TCP_MAX_SEG_SIZE;
#endif
    struct sockaddr_in  SockAddr;

#ifdef  KERNEL_WANTED
    tSourcePort         HttpPort;
    INT4                i4CommandStatus;

    HttpPort.i4Cmd = IPAUTH_SET_HTTP_PORT;
    HttpPort.i4Port = WEBNM_HTTP_PORT;

    i4CommandStatus = KAPIUpdateInfo (IPAUTH_PORT_IOCTL,
                                      (tKernCmdParam *) & HttpPort);

    if (i4CommandStatus == OSIX_FAILURE)
    {
        return (ENM_FAILURE);
    }
#endif

/*    i4Sock = socket (AF_INET, SOCK_STREAM, HTTP_ZERO);*/
    i4Sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Open Socket\n");
        return ENM_FAILURE;
    }

    if ((setsockopt (i4Sock, SOL_SOCKET, SO_REUSEADDR,
                     (INT4 *) &i4OptVal, sizeof (i4OptVal)) < HTTP_ZERO))
    {
        ENM_TRACE ("Http:Unable to set Socket Options\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags = fcntl (i4Sock, F_GETFL, HTTP_ZERO)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "GET Failure !!!\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }
    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4Sock, F_SETFL, i4Flags) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "SET Failure !!!\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    if ((setsockopt (i4Sock, SOL_SOCKET, SO_REUSEADDR,
                     (INT4 *) &i4OptVal, sizeof (i4OptVal)) < HTTP_ZERO))
    {
        ENM_TRACE ("Http:Unable to set Socket Options\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }
#ifdef WLC_WANTED

    if ((setsockopt (i4Sock, IPPROTO_TCP, TCP_MAXSEG, &i4Mss, sizeof (i4Mss)) <
         HTTP_ZERO))
    {
        /* Not given any return. Will fallthrough even if it fails */
        ENM_TRACE ("Http:Unable to set Socket Options\n");
    }

#endif
    MEMSET (&SockAddr, HTTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (WEBNM_HTTP_PORT);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (WEBNM_HTTP_ADDR);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unabel to Bind Socket\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    if (listen (i4Sock, HTTP_MAX_LISTEN) < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Listen On Socket");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    /* Increase the tcp receive window size for multipart data */
    if (setsockopt (i4Sock, SOL_SOCKET, SO_RCVBUF,
                    (UINT1 *) &i4WindowSize, i4WindowLen) == ENM_FAILURE)
    {
        ENM_TRACE ("Http: Cannot increase tcp buffersize");
    }

    /* If ISS_WANTED switch is defined, DO NOT add the 
     * socket to the SELECT utility at this point of time.
     * The SelAdd should be invoked only after the MSR restoration
     * has been completed, if ISS_WANTED is enabled */
#ifndef ISS_WANTED
    /* Add this SockFd to SELECT library */
    if (HttpSrvSelAddFd (i4Sock, HttpSrvSockCallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }
#endif

    return i4Sock;
}

/******************************************************************************
* Function           : HttpCloseSocket 
* Input(s)           : i4SockFd - Socket descriptor.
* Output(s)          : None.
* Returns            : 0 on success & -1 on failure
* Action             : Routine to close the socket created for HTTP.
******************************************************************************/
INT4
HttpCloseSocket (INT4 i4SockFd)
{
    INT4                i4RetVal = HTTP_ZERO;
    UINT4               u4Counter = 0;

    i4RetVal = close (i4SockFd);
    if (i4RetVal < HTTP_ZERO)
    {
        ENM_TRACE ("!!! socket close Failure !!!\n");
    }
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv4Sec[u4Counter].i4SockFd == i4SockFd)
        {
            gIpv4Sec[u4Counter].i4SockFd = -1;
        }
    }
#ifdef IP6_WANTED
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        if (gIpv6Sec[u4Counter].i4SockFd == i4SockFd)
        {
            gIpv6Sec[u4Counter].i4SockFd = -1;
        }
    }
#endif
    return (i4RetVal);
}

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : HttpSock6Init
 *  Description     : Function will create the ipv6 socket adds the server
 *                    socket to select utility
 *
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket Descriptor or Failure
 ************************************************************************/
INT4
HttpSock6Init (VOID)
{
    INT4                i4Sock = HTTP_ZERO;
    INT4                i4OptVal = TRUE;
    INT4                i4Flags = HTTP_INVALID_SOCK_FLAG;
    struct sockaddr_in6 SockAddr;

    i4Sock = socket (AF_INET6, SOCK_STREAM, HTTP_ZERO);
    if (i4Sock < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Open Socket\n");
        return ENM_FAILURE;
    }

    if (setsockopt (i4Sock, IPPROTO_IPV6, IPV6_V6ONLY, &i4OptVal,
                    sizeof (i4OptVal)))
    {
        ENM_TRACE ("Http:Unable to Set Socket Option IPV6_V6ONLY \n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    if ((setsockopt (i4Sock, SOL_SOCKET, SO_REUSEADDR,
                     (INT4 *) &i4OptVal, sizeof (i4OptVal)) < HTTP_ZERO))
    {
        ENM_TRACE ("Http:Unable to set Socket Options\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags = fcntl (i4Sock, F_GETFL, HTTP_ZERO)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "GET Failure !!!\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4Sock, F_SETFL, i4Flags) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "SET Failure !!!\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    if ((setsockopt (i4Sock, SOL_SOCKET, SO_REUSEADDR,
                     (INT4 *) &i4OptVal, sizeof (i4OptVal)) < HTTP_ZERO))
    {
        ENM_TRACE ("Http:Unable to set Socket Options\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    MEMSET (&SockAddr, HTTP_ZERO, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (WEBNM_HTTP_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "HTTP_ZERO::HTTP_ZERO",
               &SockAddr.sin6_addr.s6_addr);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unabel to Bind Socket\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    if (listen (i4Sock, HTTP_MAX_LISTEN) < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Listen On Socket");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }

    /* If ISS_WANTED switch is defined, DO NOT add the 
     * socket to the SELECT utility at this point of time.
     * The SelAdd should be invoked only after the MSR restoration
     * has been completed, if ISS_WANTED is enabled */
#ifndef ISS_WANTED
    /* Add this SockFd to SELECT library */
    if (HttpSrvSelAddFd (i4Sock, HttpSrvSock6CallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (i4Sock);
        return ENM_FAILURE;
    }
#endif

    return i4Sock;
}
#endif
/************************************************************************
 *  Function Name   : HttpEnable
 *  Description     : The function used to enable HTTP
 *  Input           : None                                         
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpEnable (VOID)
{
    if (gHttpInfo.HttpCtrlInfo.bHttpEnable == TRUE)
    {
        ENM_TRACE ("HttpEnable:HTTP already enabled!\n");
        return;
    }
    gHttpInfo.i4SrvSockId = HttpSockInit ();
#ifdef IP6_WANTED
    gHttpInfo.i4SrvSock6Id = HttpSock6Init ();
#endif
#ifdef ISS_WANTED
    HttpSrvUpdateSelFd ();
#endif
    gHttpInfo.HttpCtrlInfo.bHttpEnable = TRUE;
    return;
}

/************************************************************************
 *  Function Name   : HttpDisable
 *  Description     : The function used to disable HTTP
 *  Input           : None                                         
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpDisable (VOID)
{
    if (gHttpInfo.HttpCtrlInfo.bHttpEnable == FALSE)
    {
        ENM_TRACE ("HttpDisable:HTTP already disabled!\n");
        return;
    }
/*    gpTmpFd = &grfds;*/
    HttpSrvSelRemFd (gHttpInfo.i4SrvSockId);
#ifdef IP6_WANTED
    HttpSrvSelRemFd (gHttpInfo.i4SrvSock6Id);
    HttpCloseSocket (gHttpInfo.i4SrvSock6Id);
    gHttpInfo.i4SrvSock6Id = HTTP_INVALID_SOCK_FD;
#endif
    HttpCloseSocket (gHttpInfo.i4SrvSockId);
    gHttpInfo.i4SrvSockId = HTTP_INVALID_SOCK_FD;
    gHttpInfo.HttpCtrlInfo.bHttpEnable = FALSE;
    return;
}

#ifdef SSL_WANTED
/************************************************************************
 *  Function Name   : HttpsSockInit 
 *  Description     : Function will create https socket and bind to Https port
 *  Input           : None 
 *  Output          : None
 *  Returns         : ENM_SUCCESS/ENM_FAILURE     
 ************************************************************************/

INT1
HttpsSockInit (VOID)
{
    struct sockaddr_in  SockAddr;
    INT4                i4Flags = HTTP_INVALID_SOCK_FLAG;

    MEMSET (&SockAddr, HTTP_ZERO, sizeof (struct sockaddr_in));
    gHttpInfo.HttpCtrlInfo.i4HttpsSockFd =
        socket (AF_INET, SOCK_STREAM, HTTP_ZERO);
    if (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd < HTTP_ZERO)
    {
        ENM_TRACE ("HttpsSockInit:Unable to Open Socket\n");
        return ENM_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags =
         fcntl (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd, F_GETFL,
                HTTP_ZERO)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "GET Failure !!!\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
        return ENM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd, F_SETFL, i4Flags) <
        HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "SET Failure !!!\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
        return ENM_FAILURE;
    }

    MEMSET (&SockAddr, HTTP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (SslArGetHttpsPort ());
    SockAddr.sin_addr.s_addr = OSIX_HTONL (HTTP_ZERO);

    if (bind
        (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd, (struct sockaddr *) &SockAddr,
         sizeof (SockAddr)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpsSockInit:Unable to Bind Socket\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
        return ENM_FAILURE;
    }

    if (listen (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd, HTTPS_MAX_LISTEN) <
        HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Listen On Socket");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
        return ENM_FAILURE;
    }

    /* Add this SockFd to SELECT library */
    if (HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                         HttpSrvSecureSock4CallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : HttpsSock6Init
 *  Description     : Function will create http socket and bind to Http port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket Descriptor or Failure
 ************************************************************************/

INT1
HttpsSock6Init (VOID)
{
    struct sockaddr_in6 Sock6Addr;
    INT4                i4Flags = HTTP_INVALID_SOCK_FLAG;
    INT4                i4OptVal = HTTP_ZERO;

    MEMSET (&Sock6Addr, HTTP_ZERO, sizeof (struct sockaddr_in6));
    gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd =
        socket (AF_INET6, SOCK_STREAM, HTTP_ZERO);
    if (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Open Socket\n");
        return ENM_FAILURE;
    }

    i4OptVal = OSIX_TRUE;
    if (setsockopt
        (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd, IPPROTO_IPV6, IPV6_V6ONLY,
         &i4OptVal, sizeof (i4OptVal)))
    {
        ENM_TRACE ("Http:Unable to Set Socket Option IPV6_V6ONLY \n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags =
         fcntl (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd, F_GETFL,
                HTTP_ZERO)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "GET Failure !!!\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd, F_SETFL, i4Flags) <
        HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "SET Failure !!!\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    MEMSET (&Sock6Addr, HTTP_ZERO, sizeof (Sock6Addr));
    Sock6Addr.sin6_family = AF_INET6;
    Sock6Addr.sin6_port = OSIX_HTONS (SslArGetHttpsPort ());
    inet_pton (AF_INET6, (const CHR1 *) "HTTP_ZERO::HTTP_ZERO",
               &Sock6Addr.sin6_addr.s6_addr);

    if (bind
        (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd, (struct sockaddr *) &Sock6Addr,
         sizeof (Sock6Addr)) < HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unabel to Bind Socket\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    if (listen (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd, HTTP_MAX_LISTEN) <
        HTTP_ZERO)
    {
        ENM_TRACE ("Http:Unable to Listen On Socket");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    /* Add this SockFd to SELECT library */
    if (HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                         HttpSrvSecureSock6CallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}
#endif

/************************************************************************
 *  Function Name   : HttpsEnable
 *  Description     : The function used to enable HTTPS
 *  Input           : None                                         
 *  Output          : None
 *  Returns         : ENM_SUCCESS/ENM_FAILURE
 ************************************************************************/

VOID
HttpsEnable (VOID)
{
    if (gHttpInfo.HttpCtrlInfo.bHttpsEnable == TRUE)
    {
        ENM_TRACE ("HttpsEnable:HTTPS already enabled!\n");
        return;
    }

    if (HttpsSockInit () == ENM_FAILURE)
    {
        ENM_TRACE ("HttpsEnable:HttpsSockInit failed!");
        return;
    }

#ifdef IP6_WANTED
    if (HttpsSock6Init () == ENM_FAILURE)
    {
        ENM_TRACE ("HttpsEnable:HttpsSockInit failed!");
        return;
    }
#endif

    gHttpInfo.HttpCtrlInfo.bHttpsEnable = TRUE;
    return;
}

/************************************************************************
 *  Function Name   : HttpsDisable
 *  Description     : The function used to disable HTTPS
 *  Input           : None                                         
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpsDisable (VOID)
{
    if (gHttpInfo.HttpCtrlInfo.bHttpsEnable == FALSE)
    {
        ENM_TRACE ("HttpsDisable:HTTPS already disabled!\n");
        return;
    }

    /* Clear all the sessions in OpenSSL */
    SslArClearSessions ();
    gpTmpFd = &grfds;
    HttpSrvSelRemFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
#ifdef IP6_WANTED
    HttpSrvSelRemFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
    HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd);
    gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd = HTTP_INVALID_SOCK_FD;
#endif
    HttpCloseSocket (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd);
    gHttpInfo.HttpCtrlInfo.i4HttpsSockFd = HTTP_INVALID_SOCK_FD;
    gHttpInfo.HttpCtrlInfo.bHttpsEnable = FALSE;
    return;
}
#endif
/************************************************************************
 *  Function Name   : HttpGetHttpStatus
 *  Description     : The function used to return the HTTP status - TRUE/FALSE 
 *  Input           : None                                         
 *  Output          : HTTP status 
 *  Returns         : None
 ************************************************************************/

VOID
HttpGetHttpStatus (INT4 *pi4HttpStatus)
{
    *pi4HttpStatus = gHttpInfo.HttpCtrlInfo.bHttpEnable;
}

#ifdef SSL_WANTED
/************************************************************************
 *  Function Name   : HttpGetHttpsStatus
 *  Description     : The function used to return the secure HTTP status - TRUE/FALSE 
 *  Input           : None                                         
 *  Output          : HTTPS status 
 *  Returns         : None
 ************************************************************************/

VOID
HttpGetHttpsStatus (INT4 *pi4HttpsStatus)
{
    *pi4HttpsStatus = gHttpInfo.HttpCtrlInfo.bHttpsEnable;
}

#endif

/************************************************************************
 *  Function Name   : HttpMain 
 *  Description     : Http Task Entry Point Function
 *  Input           : pParam - Parameter given while creating task.
 *                    In http task this parameter is not used.
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpMain (INT1 *pParam)
{
#ifdef SSL_WANTED
    VOID               *pConnId = NULL;
#endif
    INT4                i4SockClientId = HTTP_ZERO;
    UINT4               u4EventReceived = HTTP_ZERO;
    UINT4               u4Counter = 0;

    UNUSED_PARAM (pParam);

    if (OsixTskIdSelf (&gHttpInfo.HttpMainTskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Exiting Function HttpMain \n");
        /* Indicate the status of initialization to the main routine */
        HTTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (HttpInitSrvInfo () == ENM_FAILURE)
    {
        ENM_TRACE ("Exiting Function HttpMain \n");
        /* Indicate the status of initialization to the main routine */
        HTTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        gIpv4Sec[u4Counter].i4SockFd = -1;
    }

#ifdef IP6_WANTED
    for (u4Counter = HTTP_ZERO;
         u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
    {
        gIpv6Sec[u4Counter].i4SockFd = -1;
    }
#endif

#ifdef SYSLOG_WANTED
    gi4WebSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "WEB", SYSLOG_CRITICAL_LEVEL);

    if (gi4WebSysLogId < HTTP_ZERO)
    {
        HttpShutDown ();
        /* Indicate the status of initialization to the main routine */
        HTTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

#ifdef SNMP_2_WANTED
    /* Note:- fswebtst.mib is not registered. If need arises it has
     * to be registered */
    /* fshttp.mib registration */
    RegisterFSHTTP ();
#endif

#ifdef SSL_WANTED
    RegisterFSSSL ();
#endif

    /* Indicate the status of initialization to the main routine */
    HTTP_INIT_COMPLETE (OSIX_SUCCESS);

    for (;;)
    {
        if (OsixEvtRecv (gHttpInfo.HttpMainTskId,
                         (HTTPSRV_TCP_NEW_CONN_RCVD |
                          HTTPSRV_TCP6_NEW_CONN_RCVD |
                          HTTPS_CTRL_EVENT |
                          HTTPSRV_SECURE4_NEW_CONN_RCVD |
                          HTTPSRV_SECURE6_NEW_CONN_RCVD |
                          HTTP_CLIENT_DATA_RCVD_EVENT |
                          HTTP_CTRL_EVENT |
                          HTTP_TIMER_EXPIRY_EVENT |
                          HTTP_MSR_RSTR_COMPLETE_EVENT),
                         HTTPSRV_EVENT_WAIT_FLAG,
                         &u4EventReceived) == OSIX_SUCCESS)
        {

            if (u4EventReceived & HTTP_CTRL_EVENT)
            {
                IssProcessCtrlMsgs ();
            }

            if (u4EventReceived & HTTPSRV_TCP_NEW_CONN_RCVD)
            {
                i4SockClientId = HttpAcceptNewConnection (AF_INET);

                if (i4SockClientId != ENM_FAILURE)
                {
                    HttpWakeUpProcessingTasks (i4SockClientId, HTTP_REQTYPE,
                                               NULL);
                }
            }

#ifdef SSL_WANTED
            if (u4EventReceived & HTTPS_CTRL_EVENT)
            {
                SslArProcessCtrlMsgs ();
                HttpSrvSelAddFd (SslArGetCtrlSockFd (), HttpsSrvCtrlSockCallBk);
            }

            if (u4EventReceived & HTTPSRV_SECURE4_NEW_CONN_RCVD)
            {
                pConnId = HttpsAcceptNewConnection (AF_INET, &i4SockClientId);

                if (pConnId != NULL)
                {
                    HttpWakeUpProcessingTasks (i4SockClientId, SSL_REQTYPE,
                                               pConnId);
                }
            }
#endif

#ifdef IP6_WANTED
            if (u4EventReceived & HTTPSRV_TCP6_NEW_CONN_RCVD)
            {
                i4SockClientId = HttpAcceptNewConnection (AF_INET6);

                if (i4SockClientId != ENM_FAILURE)
                {
                    HttpWakeUpProcessingTasks (i4SockClientId, HTTP_REQTYPE,
                                               NULL);
                }
            }
#ifdef SSL_WANTED
            if (u4EventReceived & HTTPSRV_SECURE6_NEW_CONN_RCVD)
            {
                pConnId = HttpsAcceptNewConnection (AF_INET6, &i4SockClientId);

                if (pConnId != NULL)
                {
                    HttpWakeUpProcessingTasks (i4SockClientId, SSL_REQTYPE,
                                               pConnId);
                }
            }
#endif /* SSL_WANTED */
#endif /* IP6_WANTED */
            if (u4EventReceived & HTTP_CLIENT_DATA_RCVD_EVENT)
            {
                HttpHandleDataRcvdOnOldConn ();
            }

            if (u4EventReceived & HTTP_TIMER_EXPIRY_EVENT)
            {
                HttpTmrExpHandler ();
            }

            /* Add the socket descriptor to select
             * utility after receiving the Restoration complete event
             * from MSR */
            if (u4EventReceived & HTTP_MSR_RSTR_COMPLETE_EVENT)
            {
                /* NOTE:- gb1HttpLoginReq is a global flag which is
                 * initialised to TRUE but can be overridden by 
                 * IssCustomInit function. This flag is introduced 
                 * for customer requirement of by-passing Aricent 
                 * Web authentication*/
                /* If the "customer specific HttpLoginReq" flag
                 * is TRUE, then Store the value of the 
                 * Configurable Auth scheme MIB in the Operational 
                 * Auth scheme MIB. This is a one time operation*/
                if (gb1HttpLoginReq == OSIX_TRUE)
                {
                    gHttpInfo.i4OperHttpAuthScheme =
                        gHttpInfo.i4ConfigHttpAuthScheme;
                }
                /* If the "customer specific HttpLoginReq" flag
                 * is FALSE, then store the Operational authentication
                 * scheme value as DEFAULT as authentication will
                 * be by-passed */
                else
                {
                    gHttpInfo.i4OperHttpAuthScheme = HTTP_AUTH_DEFAULT;
                }

#ifdef ISS_WANTED
                HttpSrvUpdateSelFd ();
#endif
            }
        }
    }
}

/*API's related to Processing Tasks*/
/************************************************************************
 *  Function Name   : HttpProcessMain
 *  Description     : Http Task Entry Point Function
 *  Input           : pParam - Parameter given while creating task.
 *                    In http task this parameter is not used.
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpProcessMain (VOID)
{
    UINT4               u4EventReceived = HTTP_ZERO;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;

    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        return;
    }

    while (HTTP_ONE)
    {
        /* Wait For wakeup event from main task, data arrival event on a 
         * connection*/

        OsixEvtRecv (ProcessingTaskId, (HTTP_WAKEUP_EVENT |
                                        HTTP_DATA_ARRIVAL),
                     HTTPSRV_EVENT_WAIT_FLAG, &u4EventReceived);

        if (u4EventReceived & HTTP_WAKEUP_EVENT)
        {
            HttpProcHandleWakeUpEvent (HTTP_DATA_ON_NEW_CONNECTION,
                                       ProcessingTaskId);
        }

        if (u4EventReceived & HTTP_DATA_ARRIVAL)
        {
            HttpProcHandleWakeUpEvent (HTTP_DATA_ON_OLD_CONNECTION,
                                       ProcessingTaskId);
        }
    }
}

/**************************************************************************/
/*   Function Name   : HttpProcHandleWakeUpEvent                          */
/*   Description     : This function will invoke function to process the  */
/*                     request from the client                            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
VOID
HttpProcHandleWakeUpEvent (INT4 i4ConnAge, tOsixTaskId SelfTaskId)
{
    VOID               *pConnId = NULL;
    tHttp              *pHttp = NULL;
    tHttpClientInfo    *pClientNode = NULL;
    INT4                i4SockId = HTTP_INVALID_SOCK_FD;
    INT4                i4Count = HTTP_ZERO;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;
    INT4                i4TaskCnt = HTTP_ZERO;
    UINT1               u1SockType = HTTP_ZERO;

    UNUSED_PARAM (i4ConnAge);
    HTTP_LOCK ();
    pClientNode = HttpGetFirstClientInfoNode ();
    if (pClientNode == NULL)
    {
        HTTP_UNLOCK ();
        return;
    }

    /*Extract socket Id and socket type from the DLL */
    i4SockId = pClientNode->i4ClientSockId;
    u1SockType = pClientNode->u1SockType;
    pConnId = pClientNode->pSslConnId;

    HttpDelNodeFromList (pClientNode, HTTP_CLIENT_INFO_LIST);
    HttpMapClientToTask (i4SockId, SelfTaskId, u1SockType);

    pHttp = (tHttp *) (MemAllocMemBlk (gHttpInfo.HttpReqPoolId));

    if (pHttp == NULL)
    {
        HTTP_UNLOCK ();
        return;
    }

    HTTP_UNLOCK ();

    MEMSET (pHttp, HTTP_ZERO, sizeof (tHttp));
    pHttp->ProcessingTaskId = SelfTaskId;
    pHttp->i4Sockid = i4SockId;
    pHttp->u1RequestType = u1SockType;
    pHttp->pSslConnId = pConnId;
    pHttp->bIsMultiData = OSIX_FALSE;

    /*Start the processing */
    HttpStartProcessing (pHttp);

    HTTP_LOCK ();

    if (MemReleaseMemBlock (gHttpInfo.HttpReqPoolId,
                            (UINT1 *) (pHttp)) != MEM_SUCCESS)
    {
        ENM_TRACE ("MEM release failure\n");
    }

    HttpMapClientToTask (HTTP_INVALID_SOCK_FD, SelfTaskId,
                         HTTP_INVALID_SOCK_TYPE);
    i4Count = TMO_DLL_Count (&gHttpInfo.HttpClientList);

    HTTP_UNLOCK ();

    /*Send events to the all the processing task if any of 
     *    client is pending to be served*/
    if (i4Count > HTTP_ZERO)
    {
        for (i4TaskCnt = HTTP_ZERO;
             i4TaskCnt < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; i4TaskCnt++)
        {

            ProcessingTaskId = gHttpInfo.HttpProcTaskId[i4TaskCnt];

            if (ProcessingTaskId != HTTP_ZERO)
            {
                OsixEvtSend (ProcessingTaskId, HTTP_DATA_ARRIVAL);
            }
        }
    }

    return;
}

/************************************************************************
 *  Function Name   : HttpMapClientToTask
 *  Description     : This function will map the socket id and sock type
 *                    to the task
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpMapClientToTask (INT4 i4SockId, tOsixTaskId SelfTaskId, UINT1 u1SockType)
{
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        if (HTTP_GET_CLIENT_TASK_MAP (i4Count).HttpProcessingTskId ==
            SelfTaskId)
        {
            HTTP_GET_CLIENT_TASK_MAP (i4Count).i4ClientSockId = i4SockId;
            HTTP_GET_CLIENT_TASK_MAP (i4Count).u1SockType = u1SockType;
        }
    }

    return;
}

/************************************************************************
 *  Function Name   : HttpStartProcessing 
 *  Description     : This function will process the normal and pipelined
 *                    request.
 *  Input           : i4SockId
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpStartProcessing (tHttp * pHttp)
{
    HTTP_LOCK ();
    pHttp->pu1Buff = NULL;

    /*Allocate memory for the buffer */
    pHttp->pu1Buff = (UINT1 *) MemAllocMemBlk (gHttpInfo.WebnmResponsePoolId);

    HTTP_UNLOCK ();

    if (pHttp->pu1Buff == NULL)
    {
        ENM_TRACE ("Cannot allocate memory\n");
        return;
    }

    MEMSET (pHttp->pu1Buff, HTTP_ZERO, WEBNM_MEMBLK_SIZE);

    gHttpInfo.u4HttpRequestCount++;
    do
    {
        pHttp->bIsReqPipelined = FALSE;

        MGMT_LOCK ();
        pHttp->u1IsMgmtLock = OSIX_TRUE;

        ProcessHttpReq (pHttp);

        pHttp->u1IsMgmtLock = OSIX_FALSE;
        MGMT_UNLOCK ();

        if (pHttp->bIsMultiData == OSIX_FALSE)
        {
            if (FlushHttpBuff (pHttp) == ENM_FAILURE)
            {
                ENM_TRACE ("FlushHttpBuff failed\r\n");
            }
            if (pHttp->pSslConnId != NULL)
            {
#ifdef SSL_WANTED
                SslArFlush (pHttp->pSslConnId);
#endif
            }
        }

        if ((pHttp->bIsReqPipelined != TRUE) || (pHttp->bConnClose == TRUE))
        {
            pHttp->pu1BuffCurr = NULL;
            break;
        }
    }
    while (pHttp->bIsReqPipelined == TRUE);

    /* To avoid hang the connetion close flag is set to TRUE always   
     *     once that is solved remove the below line*/
    pHttp->bConnClose = TRUE;

    HTTP_LOCK ();

    if (MemReleaseMemBlock (gHttpInfo.WebnmResponsePoolId,
                            (UINT1 *) (pHttp->pu1Buff)) != MEM_SUCCESS)
    {
        ENM_TRACE ("MEM release failure\n");
    }

    pHttp->pu1Buff = NULL;

    /*Close the connection if we got close indication while processing the
     *    packet*/
    if (pHttp->bConnClose == TRUE)
    {
        /* In case of multidata handling  the socket id will be
         * reset to HTTP_INVALID_SOCK_FD. Ignore this socket
         */

        if (pHttp->i4Sockid != HTTP_INVALID_SOCK_FD)
        {
#ifdef SSL_WANTED
            /*Stop the timer for that client delete the client node */
            if (pHttp->pSslConnId != NULL)
            {
                SslArClose (pHttp->pSslConnId);
            }
#endif

            HttpHandleConnClose (pHttp->i4Sockid);
        }
    }
    else
    {
        if (HttpSrvSelAddFd (pHttp->i4Sockid, HttpSrvPktRcvd) == ENM_FAILURE)
        {
            if (pHttp->i4Sockid != HTTP_INVALID_SOCK_FD)
            {
#ifdef SSL_WANTED
                /*Stop the timer for that client delete the client node */
                if (pHttp->pSslConnId != NULL)
                {
                    SslArClose (pHttp->pSslConnId);
                }
#endif
                HttpHandleConnClose (pHttp->i4Sockid);
            }
        }
    }

    HTTP_UNLOCK ();
#ifdef SSL_WANTED
    if (pHttp->u1RequestType == SSL_REQTYPE)
    {
        pHttp->pSslConnId = NULL;
    }
#endif

    return;
}

/**************************************************************************/
/*   Function Name   : HttpGetFirstClientInfoNode                         */
/*   Description     : This functions will get the first client node      */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
tHttpClientInfo    *
HttpGetFirstClientInfoNode (VOID)
{
    tHttpClientInfo    *pClientNode = NULL;

    pClientNode = (tHttpClientInfo *) TMO_DLL_First (&gHttpInfo.HttpClientList);

    return pClientNode;
}

/**************************************************************************/
/*   Function Name   : HttpHandleConnClose                                */
/*   Description     : This function will close the connection, stops the */
/*                     timer, deletes the mem block allocted for timer    */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
VOID
HttpHandleConnClose (INT4 i4ClientSockId)
{
    /*Remove the client task map, stop the timer */
    tHttpClientInfo    *pClientNode = NULL;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;
    INT4                i4Count = HTTP_ZERO;
    UINT1               u1Flag = HTTP_ZERO;

    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Get Osix Id of current Task failed\n");
    }

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        /*No need to take HTTP lock here because Processing Task ID 
         * are not subject change after Initialization
         * All the Proceesing Task ID's are Initialized at start
         * and do not change so no need to talk lock for this */
        if (HTTP_GET_CLIENT_TASK_MAP (i4Count).HttpProcessingTskId ==
            ProcessingTaskId)
        {
            break;
        }
    }

    if (i4Count >= HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT)
    {
        u1Flag = 1;
    }

    if (HttpCloseSocket (i4ClientSockId) >= HTTP_ZERO)
    {
        if (u1Flag != 1)
        {
            if (gHttpInfo.pRdTmpFd[i4Count] != NULL)
            {
                FD_CLR (i4ClientSockId, gHttpInfo.pRdTmpFd[i4Count]);
            }

            if (gHttpInfo.pWrTmpFd[i4Count] != NULL)
            {
                FD_CLR (i4ClientSockId, gHttpInfo.pWrTmpFd[i4Count]);
            }
        }
    }

    HttpDeleteClientFromDb (i4ClientSockId);
    pClientNode = HttpSearchClientTmrNode (i4ClientSockId);

    if (pClientNode != NULL)
    {
        if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
        {
            HTTP_STOP_TIMER (&(pClientNode->TimeOutTmrLink));
        }
        HttpDelNodeFromList (pClientNode, HTTP_CLIENT_TMR_INFO_LIST);
        if (MemReleaseMemBlock (gHttpInfo.HttpAccCltInfoPoolId,
                                (UINT1 *) (pClientNode)) != MEM_SUCCESS)
        {
            ENM_TRACE ("MEM release failure\n");
        }
        HTTP_CONNECTION_COUNT ()--;
    }
    return;
}

/**************************************************************************/
/*   Function Name   : HttpHandleDataRcvdOnOldConn                        */
/*   Description     : This function will restart the timer and send event*/
/*                     to all the processing tasks for processing the     */
/*                     received request from client                       */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpHandleDataRcvdOnOldConn (VOID)
{
    tHttpClientInfo    *pClientTmrNode = NULL;
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;
    INT4                i4ClientSockId = HTTP_ZERO;
    INT4                i4Status = HTTP_ZERO;
    INT4                i4TaskCnt = HTTP_ZERO;

    MEMSET (&ProcessingTaskId, HTTP_ZERO, sizeof (tOsixTaskId));

    HTTP_LOCK ();
    i4ClientSockId = HttpDeQueueSockId ();
    HTTP_UNLOCK ();
    while (i4ClientSockId != HTTP_INVALID_SOCK_FD)
    {
        HTTP_LOCK ();
        if (i4ClientSockId != ENM_FAILURE)
        {
            pClientTmrNode = HttpSearchClientTmrNode (i4ClientSockId);

            if (pClientTmrNode != NULL)
            {
                if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
                {
                    HTTP_STOP_TIMER (&(pClientTmrNode->TimeOutTmrLink));

                    HTTP_START_TIMER (HTTP_CLT_IDLE_TIMER,
                                      &(pClientTmrNode->TimeOutTmrLink),
                                      HTTP_CONNECTION_IDLE_TIMEOUT, i4Status);

                    if (i4Status != TMR_SUCCESS)
                    {
                        HTTP_UNLOCK ();
                        return ENM_FAILURE;
                    }
                }
                HttpAddNodeToList (pClientTmrNode, HTTP_CLIENT_INFO_LIST);
            }
        }
        HTTP_UNLOCK ();

        for (i4TaskCnt = HTTP_ZERO;
             i4TaskCnt < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; i4TaskCnt++)
        {
            ProcessingTaskId = gHttpInfo.HttpProcTaskId[i4TaskCnt];

            if (ProcessingTaskId != HTTP_ZERO)
            {
                OsixEvtSend (ProcessingTaskId, HTTP_DATA_ARRIVAL);
            }
        }
        HTTP_LOCK ();
        i4ClientSockId = HttpDeQueueSockId ();
        HTTP_UNLOCK ();
    }
    return ENM_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : HttpSearchClientTmrNode                            */
/*   Description     : This functions will search the client timer node   */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
tHttpClientInfo    *
HttpSearchClientTmrNode (INT4 i4SockId)
{
    tTMO_DLL_NODE      *pClientTmrNode = NULL;
    tHttpClientInfo    *pGetNextNode = NULL;

    TMO_DLL_Scan (&(gHttpInfo.HttpClientTmrList),
                  pClientTmrNode, tTMO_DLL_NODE *)
    {

        pGetNextNode = HTTP_GET_BASE_PTR (tHttpClientInfo,
                                          NextSocTmrNode, pClientTmrNode);

        if (pGetNextNode->i4ClientSockId == i4SockId)
        {
            return pGetNextNode;
        }
    }

    return NULL;
}

/**************************************************************************/
/*   Function Name   : HttpCltIdleTmrExpHandler                           */
/*   Description     : This function handles the client timer expiry      */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
VOID
HttpCltIdleTmrExpHandler (tHttpTmrNode * pTimerNode)
{
    VOID               *pConnId = NULL;
    tHttpClientInfo    *pClientTmrNode = NULL;
    INT4                i4SockId = HTTP_ZERO;
    INT4                i4RetStatus = HTTP_ZERO;
    UINT1               u1SockType = HTTP_ZERO;

    pClientTmrNode = HTTP_GET_BASE_PTR (tHttpClientInfo, TimeOutTmrLink,
                                        pTimerNode);

    pClientTmrNode->TimeOutTmrLink.u1TmrStatus = HTTP_TIMER_FLAG_RESET;

    i4SockId = pClientTmrNode->i4ClientSockId;
    u1SockType = pClientTmrNode->u1SockType;
    pConnId = pClientTmrNode->pSslConnId;

    /*It is possible that responding to a request might take more than
     *    5 minutes (ex: HTTP Upgrade downloading of ISS.exe) so in that
     *    case donot stop the timer (this can be verified by knowing
     *    whether the socket is served by any task or not)*/
    HTTP_LOCK ();
    i4RetStatus = HttpCheckIfClientIsServed (i4SockId);
    HTTP_UNLOCK ();

    if (i4RetStatus == ENM_CLIENT_NOT_SERVED)
    {
#ifdef SSL_WANTED
        if (u1SockType == SSL_REQTYPE)
        {
            SslArClose (pConnId);
        }
#endif
        HTTP_LOCK ();
        HttpDelNodeFromList (pClientTmrNode, HTTP_CLIENT_INFO_LIST);
        HttpHandleConnClose (i4SockId);
        HTTP_UNLOCK ();

    }
    else
    {
        HTTP_START_TIMER (HTTP_CLT_IDLE_TIMER,
                          &(pClientTmrNode->TimeOutTmrLink),
                          HTTP_CONNECTION_IDLE_TIMEOUT, i4RetStatus);

        if (i4RetStatus == ENM_FAILURE)
        {
            ENM_TRACE ("Unable to start timer\r\n");
        }
        else
        {
            ENM_TRACE ("Timer is restarted\r\n");
        }
    }
    return;
}

/**************************************************************************/
/*   Function Name   : HttpTmrExpHandler                                  */
/*   Description     : This function is called whenever timer expiry event*/
/*                     is occured, this will the expired the timer id     */
/*                     and calls the corresponding call back function     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
VOID
HttpTmrExpHandler (VOID)
{
    tTimerListId        TimerListId;
    tHttpTmrNode       *pTimer = NULL;
    tTmrAppTimer       *pNextTimer = NULL;
    tTmrAppTimer       *pAppTmr = NULL;
    UINT1               u1TimerId = HTTP_ZERO;

    MEMSET (&TimerListId, HTTP_ZERO, sizeof (tTimerListId));

    TimerListId = gHttpInfo.HttpTmrListId;

    /* Get the First Expired timer */
    if (TmrGetExpiredTimers (TimerListId, &pAppTmr) == TMR_FAILURE)
    {
        ENM_TRACE ("Error in Getting Expired Timers \n");
    }

    pTimer = (tHttpTmrNode *) pAppTmr;
    /* Till all the timer exhaust */
    while (pTimer != NULL)
    {
        u1TimerId = pTimer->u1TimerId;

        if (u1TimerId >= HTTP_MAX_TIMER)
        {
            ENM_TRACE ("Error: TimerId Exceeds max Value.\n");
            continue;
        }

        /* Call the appropriate timer expiry handler */
        (*gaHttpTmrExpHdlr[u1TimerId]) (pTimer);

        /* Get the next timer node */
        pNextTimer = TmrGetNextExpiredTimer (TimerListId);
        pTimer = (tHttpTmrNode *) pNextTimer;

    }

    return;
}

/**************************************************************************/
/*   Function Name   : HttpCheckIfClientIsServed                          */
/*   Description     : This function used to check whether the sockFd is  */
/*                     served by some other client                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpCheckIfClientIsServed (INT4 i4ClientSockId)
{
    INT4                i4Count = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        /*No need to take HTTP lock here because Processing Task ID 
         * are not subject change after Initialization
         * All the Proceesing Task ID's are Initialized at start
         * and do not change so no need to talk lock for this */
        if (HTTP_GET_CLIENT_TASK_MAP (i4Count).i4ClientSockId == i4ClientSockId)
        {
            return ENM_CLIENT_SERVED;
        }
    }

    return ENM_CLIENT_NOT_SERVED;
}

/**************************************************************************/
/*   Function Name   : HttpWakeUpProcessingTasks                          */
/*   Description     : This function will post events to all the          */
/*                     processing task whenever a new connection is       */
/*                     accepted                                           */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpWakeUpProcessingTasks (INT4 i4ClientSockId, UINT1 u1ClientSockType,
                           VOID *pSslConnId)
{

    tOsixTaskId         ProcessingTaskId = HTTP_INVALID_TASK_ID;
    tHttpClientInfo    *pClientNode = NULL;
    INT4                i4Status = HTTP_ZERO;
    INT4                i4TaskCnt = HTTP_ZERO;

    HTTP_LOCK ();
    pClientNode = HttpSearchClientTmrNode (i4ClientSockId);
    if (pClientNode != NULL)
    {
        if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
        {
            HTTP_STOP_TIMER (&(pClientNode->TimeOutTmrLink));
        }
    }
    else
    {
        pClientNode = (tHttpClientInfo *)
            (MemAllocMemBlk (gHttpInfo.HttpAccCltInfoPoolId));
        if (pClientNode == NULL)
        {
            HTTP_UNLOCK ();
            return ENM_FAILURE;
        }
        HTTP_CONNECTION_COUNT ()++;
        pClientNode->i4ClientSockId = i4ClientSockId;
        pClientNode->u1SockType = u1ClientSockType;
        pClientNode->pSslConnId = pSslConnId;
        TMO_DLL_Add (&(gHttpInfo.HttpClientTmrList),
                     &pClientNode->NextSocTmrNode);
    }

    if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
    {
        HTTP_START_TIMER (HTTP_CLT_IDLE_TIMER,
                          &(pClientNode->TimeOutTmrLink),
                          HTTP_CONNECTION_IDLE_TIMEOUT, i4Status);

        if (i4Status != TMR_SUCCESS)
        {
            HTTP_UNLOCK ();
            KW_FALSEPOSITIVE_FIX (pClientNode);
            return ENM_FAILURE;
        }
    }
    TMO_DLL_Add (&(gHttpInfo.HttpClientList), &pClientNode->NextSocInfoNode);
    HTTP_UNLOCK ();

    for (i4TaskCnt = HTTP_ZERO; i4TaskCnt < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4TaskCnt++)
    {
        ProcessingTaskId = gHttpInfo.HttpProcTaskId[i4TaskCnt];

        if (ProcessingTaskId != HTTP_INVALID_TASK_ID)
        {
            OsixEvtSend (ProcessingTaskId, HTTP_WAKEUP_EVENT);
        }
    }

    return ENM_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : HttpAddNodeToList                                  */
/*   Description     : This functions is used to add the client info node */
/*                     in the Client Information list                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpAddNodeToList (VOID *pNode, INT4 i4ListType)
{
    tHttpClientInfo    *pClientNode = NULL;

    pClientNode = (tHttpClientInfo *) pNode;
    switch (i4ListType)
    {
        case HTTP_CLIENT_INFO_LIST:
            TMO_DLL_Add (&(gHttpInfo.HttpClientList),
                         &pClientNode->NextSocInfoNode);
            break;

        case HTTP_CLIENT_TMR_INFO_LIST:
            TMO_DLL_Add (&(gHttpInfo.HttpClientTmrList),
                         &pClientNode->NextSocTmrNode);
            break;

        default:
            return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : HttpDelNodeFromList                                */
/*   Description     : This functions is used to deletes client info node */
/*                     in the Client Information list                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : ENM_SUCCESS or ENM_FAILURE                         */
/**************************************************************************/
INT4
HttpDelNodeFromList (VOID *pNode, INT4 i4ListType)
{
    tHttpClientInfo    *pClientNode = NULL;

    pClientNode = (tHttpClientInfo *) pNode;

    switch (i4ListType)
    {
        case HTTP_CLIENT_INFO_LIST:
            TMO_DLL_Delete (&(gHttpInfo.HttpClientList),
                            &pClientNode->NextSocInfoNode);
            break;

        case HTTP_CLIENT_TMR_INFO_LIST:
            TMO_DLL_Delete (&(gHttpInfo.HttpClientTmrList),
                            &pClientNode->NextSocTmrNode);
            break;

        default:
            return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : ProcessHttpReq 
 *  Description     : Http Processing function
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
ProcessHttpReq (tHttp * pHttp)
{
#ifdef CLI_WANTED
    UINT4               u4UserLevel = HTTP_ZERO;
    UINT4               u4UserAccess = HTTP_ZERO;
#endif
    INT4                i4Index = ENM_FAILURE;
    UINT1               u1FirstTimeFlag = HTTP_ZERO;
#ifdef WLC_WANTED
    HTTP_DBG_TRC1 (HTTP_TRC, "Http: Request Process for %x\n",
                   WebnmGetWebAuthClientInfo (pHttp->i4Sockid));
#endif
    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index)
        == ENM_FAILURE)
    {
        return;
    }

    /* Initialising Realm and Stale to the default value ISS */
    MEMSET (pHttp->au1Realm, '\0', ENM_MAX_REALM_LEN);
    MEMSET (pHttp->au1Stale, '\0', ENM_MAX_STALE_LEN);
    STRNCPY (pHttp->au1Realm, gaHTTP_REALM_ISS, ENM_MAX_REALM_LEN);
    STRNCPY (pHttp->au1Stale, gaHTTP_FALSE, ENM_MAX_STALE_LEN);

    pHttp->pu1BuffCurr = pHttp->pu1Buff;
    /* Process the Request line */
    if (HttpProcessReqLine (pHttp) == ENM_FAILURE)
    {
        gHttpInfo.u4HttpRequestDiscards++;
        return;
    }
    /* Read the complete request and process */
    if (HttpValidateReqHeader (pHttp) == ENM_FAILURE)
    {
        gHttpInfo.u4HttpRequestDiscards++;
        return;
    }

    if (pHttp->bIsMultiData == OSIX_TRUE)
    {
        return;
    }

    /* Read Post Query from the HTTP req *
     * the post query is present only for post requests*/
    if (HttpReadPostQuery (pHttp) == ENM_FAILURE)
    {
        HttpSendError (pHttp, HTTP_BAD_REQUEST);
        return;
    }
    if (WebFindPagePriv (pHttp) == ENM_FAILURE)
    {
        return;
    }

    /* Entered URL = /wmi/login or /wmi/relogin */
    if ((STRCMP (pHttp->ai1Url, HttpUrlStr[HTTP_ZERO]) == HTTP_ZERO) ||
        (STRCMP (pHttp->ai1Url, HttpUrlStr[HTTP_FOUR]) == HTTP_ZERO))
    {
        /* For first login, check if the authorisation header is absent.
         * If the header is absent, then based on the global authentication
         * scheme, initiate the authentication */
        if (pHttp->ai1Authorisation[HTTP_ZERO] == '\0')
        {
            pHttp->i4AuthScheme = gHttpInfo.i4OperHttpAuthScheme;

            if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
                (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST))
            {
                pHttp->u2RespCode = HTTP_UNAUTHORIZED;
                HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
                return;
            }
            else if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
            {
                WebnmSendLogin (pHttp, WEBNM_LOGIN_WANTED);
                gi1Start[i4Index] = HTTP_ONE;
                return;
            }
        }
        /* If the authorisation header is present and if the 
         * credentials are empty, then based on the authentication
         * scheme send the 401 unauthorised response */
        else if ((pHttp->ai1Authorisation[HTTP_ZERO] != '\0') &&
                 ((pHttp->ai1Username[HTTP_ZERO] == '\0') ||
                  (pHttp->ai1Password[HTTP_ZERO] == '\0')))
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            STRNCPY (pHttp->au1Realm,
                     gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);

            pHttp->u2RespCode = HTTP_UNAUTHORIZED;
            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            return;
        }
        /* If the credentials are present, authenticate the user as
         * per the authentication scheme */

        /* NOTE :- If the browser pre-emptively sends the credentials,
         * then the authentication will take place based on the scheme 
         * the browser has used to pass on the credentials. 
         * In this case, the drawback is that the global authentication 
         * scheme cannot override the browser's pre-emptive 
         * authentication scheme.
         * To overcome this, first clear the browser's private data 
         * before accessing the WebUI*/

        else if (pHttp->ai1Authorisation[HTTP_ZERO] != '\0')
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            WebnmInit (pHttp);
            u1FirstTimeFlag = HTTP_ONE;
            if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
            {
                return;
            }

            WebnmSendLogin (pHttp, WEBNM_LOGIN_WANTED);
            gi1Start[i4Index] = HTTP_ONE;
            WEBNM_TEST_CREATE_TEST_NODE (pHttp->i4AuthScheme,
                                         pHttp->ProcessingTaskId, HTTP_ZERO);
            return;
        }

    }

#ifdef ISS_WANTED

    /* Entered URL = '/' */
    if (STRNCMP (pHttp->ai1Url, "/", STRLEN (pHttp->ai1Url)) == HTTP_ZERO)
    {

        /* For first login, check if the authorisation header is absent.
         * If the header is absent, then based on the global authentication
         * scheme, initiate the authentication */
        if (pHttp->ai1Authorisation[HTTP_ZERO] == '\0')
        {
            pHttp->i4AuthScheme = gHttpInfo.i4OperHttpAuthScheme;

            if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
                (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST))
            {
                pHttp->u2RespCode = HTTP_UNAUTHORIZED;
                HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
                return;
            }
            else if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
            {
                WebnmSendLogin (pHttp, WEBNM_TIME_EXPIRED);
                gi1Start[i4Index] = HTTP_ONE;
                return;
            }
        }
        /* If the authorisation header is present and if the 
         * credentials are empty, then based on the authentication
         * scheme send the 401 unauthorised response */
        else if ((pHttp->ai1Authorisation[HTTP_ZERO] != '\0') &&
                 ((pHttp->ai1Username[HTTP_ZERO] == '\0') ||
                  (pHttp->ai1Password[HTTP_ZERO] == '\0')))
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            STRNCPY (pHttp->au1Realm,
                     gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);

            pHttp->u2RespCode = HTTP_UNAUTHORIZED;
            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            return;
        }
        /* If the credentials are present, authenticate the user as
         * per the authentication scheme */

        /* NOTE :- If the browser pre-emptively sends the credentials,
         * then the authentication will take place based on the scheme 
         * the browser has used to pass on the credentials. 
         * In this case, the drawback is that the global authentication 
         * scheme cannot override the browser's pre-emptive 
         * authentication scheme.
         * To overcome this, first clear the browser's private data 
         * before accessing the WebUI*/

        else if (pHttp->ai1Authorisation[HTTP_ZERO] != '\0')
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            WebnmInit (pHttp);
            u1FirstTimeFlag = HTTP_ONE;
            if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
            {
                return;
            }

            WebnmSendLogin (pHttp, WEBNM_TIME_EXPIRED);
            gi1Start[i4Index] = HTTP_ONE;
            WEBNM_TEST_CREATE_TEST_NODE (pHttp->i4AuthScheme,
                                         pHttp->ProcessingTaskId, HTTP_ZERO);
            return;
        }
    }

    /* Received URL = /wmi/logout */
    if ((STRCMP (pHttp->ai1Url, HttpUrlStr[HTTP_THREE])) == HTTP_ZERO)
    {
        IssLogout (pHttp);
        return;
    }
#endif

    /* Entered URL = /wmi */
    if ((gi1Start[i4Index] == HTTP_ZERO)
        && (STRSTR (pHttp->ai1Url, WEBNM_WMI) != NULL))
    {
        /* For first login, check if the authorisation header is absent.
         * If the header is absent, then based on the global authentication
         * scheme, initiate the authentication */
        if (pHttp->ai1Authorisation[HTTP_ZERO] == '\0')
        {
            pHttp->i4AuthScheme = gHttpInfo.i4OperHttpAuthScheme;

            if ((pHttp->i4AuthScheme == HTTP_AUTH_BASIC) ||
                (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST))
            {
                pHttp->u2RespCode = HTTP_UNAUTHORIZED;
                HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
                return;
            }
            else if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
            {
                WebnmSendLogin (pHttp, WEBNM_TIME_EXPIRED);
                gi1Start[i4Index] = HTTP_ONE;
                return;
            }
        }
        /* If the authorisation header is present and if the 
         * credentials are empty, then based on the authentication
         * scheme send the 401 unauthorised response */
        else if ((pHttp->ai1Authorisation[HTTP_ZERO] != '\0') &&
                 ((pHttp->ai1Username[HTTP_ZERO] == '\0') ||
                  (pHttp->ai1Password[HTTP_ZERO] == '\0')))
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            STRNCPY (pHttp->au1Realm,
                     gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);

            pHttp->u2RespCode = HTTP_UNAUTHORIZED;
            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            return;
        }
        /* If the credentials are present, authenticate the user as
         * per the authentication scheme */

        /* NOTE :- If the browser pre-emptively sends the credentials,
         * then the authentication will take place based on the scheme 
         * the browser has used to pass on the credentials. 
         * In this case, the drawback is that the global authentication 
         * scheme cannot override the browser's pre-emptive 
         * authentication scheme.
         * To overcome this, first clear the browser's private data
         * before accessing the WebUI*/

        else if (pHttp->ai1Authorisation[HTTP_ZERO] != '\0')
        {
            if (pHttp->i4AuthScheme != gHttpInfo.i4OperHttpAuthScheme)
            {
                WebnmSendRestartPage (pHttp);
                return;
            }

            WebnmInit (pHttp);
            u1FirstTimeFlag = HTTP_ONE;
            if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
            {
                return;
            }

            WebnmSendLogin (pHttp, WEBNM_TIME_EXPIRED);
            gi1Start[i4Index] = HTTP_ONE;
            WEBNM_TEST_CREATE_TEST_NODE (pHttp->i4AuthScheme,
                                         pHttp->ProcessingTaskId, HTTP_ZERO);
            return;
        }

    }

    if (HttpReadhtml (pHttp) == ENM_FAILURE)
    {
        return;
    }
    if ((STRCMP (pHttp->ai1HtmlName, WEBNM_HOME_PAGE)) == HTTP_ZERO)
    {
        pHttp->i4Method = HTTP_GET;
    }

#ifdef ISS_WANTED
    /* Check whether the configuration is allowed */
    if (IssCustConfControlWebnm ((VOID *) pHttp) == ENM_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Operation not Permitted");
        return;
    }
#endif

    if (pHttp->i4Objreq == WEBNM_REQUEST)
    {
        WebnmInit (pHttp);
        if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
        {
            return;
        }
        pHttp->u2RespCode = HTTP_OK;
        if (HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT) == ENM_FAILURE)
        {
            return;
        }
        if (pHttp->i4Method == HTTP_GET)
        {
            WebnmProcessGet (pHttp);
        }
        else if (pHttp->i4Method == HTTP_POST)
        {
            WebnmProcessPost (pHttp);
        }
    }
#ifdef ISS_WANTED
    else if (pHttp->i4Objreq == ISS_WEB_REQUEST)
    {
        WebnmInit (pHttp);
        if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
        {
            return;
        }
#ifdef CLI_WANTED
        WebNMGetUserGroupsFromCli (&u4UserLevel);
        u4UserAccess = WebNMPolicyCheck (u4UserLevel, pHttp->u4PageAccess,
                                         pHttp->u4GroupID);
#endif
        if (pHttp->i4Method == HTTP_GET)
        {

#ifdef CLI_WANTED
            if (!(u4UserAccess & ENM_READ))
            {
                WebnmSendErr (pHttp, WEBNM_NO_READACCESS);
                return;
            }
#endif
            /* When the request has passsed all the Header validations
             * and is being sent for further processing it means the request is valid
             * and response is being generated. So the Response code is set to 200 */
            /* All the HTML pages in ISS are dynamic pages as the content varies
             * depending on the output of the nmh routines so we set the response
             * as Dynamic. All dynamic objects are sent in Chunked format */
            pHttp->u2RespCode = HTTP_OK;
            if (HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT) == ENM_FAILURE)
            {
                return;
            }
            IssProcessGet (pHttp);
        }
        if (pHttp->i4Method == HTTP_POST)
        {

#ifdef CLI_WANTED
            if (!(u4UserAccess & ENM_WRITE))
            {
                WebnmSendErr (pHttp, WEBNM_NO_WRITEACCESS);
                return;
            }
#endif
            /* When the request has passsed all the Header validations
             * and is being sent for further processing it means the request is valid
             * and response is being generated. So the Response code is set to 200 */
            /* All the HTML pages in ISS are dynamic pages as the content varies
             * depending on the output of the nmh routines so we set the response
             * as Dynamic. All dynamic objects are sent in Chunked format */
            pHttp->u2RespCode = HTTP_OK;
            if (HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT) == ENM_FAILURE)
            {
                return;
            }
            IssProcessPost (pHttp);
        }
    }
    else if (pHttp->i4Objreq == ISS_WEB_SPECIFIC_REQUEST)
    {
        WebnmInit (pHttp);
        if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
        {
            return;
        }
#ifdef CLI_WANTED
        WebNMGetUserGroupsFromCli (&u4UserLevel);
        u4UserAccess = WebNMPolicyCheck (u4UserLevel, pHttp->u4PageAccess,
                                         pHttp->u4GroupID);
        if (pHttp->i4Method == HTTP_GET)
        {
            if (!(u4UserAccess & ENM_READ))
            {
                WebnmSendErr (pHttp, WEBNM_NO_READACCESS);
                return;
            }
        }
        else if (pHttp->i4Method == HTTP_POST)
        {
            if (!(u4UserAccess & ENM_WRITE))
            {
                WebnmSendErr (pHttp, WEBNM_NO_WRITEACCESS);
                return;
            }
        }
#endif
        /* When the request has passsed all the Header validations
         * and is being sent for further processing it means the request is valid
         * and response is being generated. So the Response code is set to 200 */
        /* All the HTML pages in ISS are dynamic pages as the content varies
         * depending on the output of the nmh routines so we set the response
         * as Dynamic. All dynamic objects are sent in Chunked format */
        pHttp->u2RespCode = HTTP_OK;
        if (HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT) == ENM_FAILURE)
        {
            return;
        }
        IssProcessSpecificPage (pHttp);
    }
#endif
    else if (pHttp->i4Objreq == HTTP_REQUEST)
    {
        /* All the static objects used in ISS are of constant length.
         * All the static object requests are classified as HTTP Request
         * So in this case we send a static header. */

        /* To verify the pre-emptive username & pwd */
        WebnmInit (pHttp);
        if (WebnmAuthenticate (pHttp, u1FirstTimeFlag) != ENM_SUCCESS)
        {
            return;
        }

        pHttp->u2RespCode = HTTP_OK;
        if (HttpSendHeader (pHttp, HTTP_STATIC_OBJECT) == ENM_FAILURE)
        {
            return;
        }
        if (SockWrite (pHttp, pHttp->pi1Html, pHttp->i4HtmlSize) == ENM_FAILURE)
        {
            return;
        }
    }

    return;
}

#ifdef SSL_WANTED
/************************************************************************
 *  Function Name   : HttpsAcceptNewConnection 
 *  Description     : Https Task Entry Point Function
 *  Input           : None       
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID               *
HttpsAcceptNewConnection (INT4 i4Family, INT4 *pi4AcceptSock)
{
    INT4                i4ClientLen = HTTP_ZERO;
    VOID               *pConnId = NULL;
    struct sockaddr_in  ClientAddr;
    UINT4               u4Counter;
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
    struct sockaddr_in6 ClientAddr6;
    INT4                i4Client6Len = HTTP_ZERO;
    i4Client6Len = sizeof (ClientAddr6);
#endif

    i4ClientLen = sizeof (ClientAddr);

    MEMSET (&ClientAddr, HTTP_ZERO, i4ClientLen);
#ifdef IP6_WANTED
    MEMSET (&ClientAddr6, HTTP_ZERO, i4Client6Len);
#endif

    if (gHttpInfo.HttpCtrlInfo.bHttpsEnable != TRUE)
    {
        ENM_TRACE ("Https: Not Enabled\n");
        return NULL;
    }

    /* Process the HTTPS request */
    if (AF_INET == i4Family)
    {
        *pi4AcceptSock = accept (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                                 (struct sockaddr *) &ClientAddr,
                                 (socklen_t *) & i4ClientLen);
        IpAddress.u4Addr = ClientAddr.sin_addr.s_addr;
        pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
    }
#ifdef IP6_WANTED
    else if (AF_INET6 == i4Family)
    {
        *pi4AcceptSock = accept (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                                 (struct sockaddr *) &ClientAddr6,
                                 (socklen_t *) & i4Client6Len);
        MEMCPY (In6Addr.u1addr, ClientAddr6.sin6_addr.s6_addr,
                sizeof (ClientAddr6.sin6_addr.s6_addr));
        pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
    }
#endif
    if (*pi4AcceptSock < HTTP_ZERO)
    {
        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                         HttpSrvSecureSock4CallBk);

#ifdef IP6_WANTED
        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                         HttpSrvSecureSock6CallBk);
#endif

        ENM_TRACE ("Https: Socket Accept Error\n");
        WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                            "Https: Socket Accept Error for %s", pu1String);
        return NULL;
    }

    if (fcntl (*pi4AcceptSock, F_SETFL, O_NONBLOCK) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpsAcceptNewConnection: Unable to set non-block!\n");
        WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                            "Https:Unable to set non-block for %s", pu1String);

        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                         HttpSrvSecureSock4CallBk);

#ifdef IP6_WANTED
        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                         HttpSrvSecureSock6CallBk);
#endif

        HttpCloseSocket (*pi4AcceptSock);
        WEB_SESSION_TRACE1 (ALL_FAILURE_TRC, "WEBNM",
                            "Https:Ssl accept failed for %s", pu1String);
        return NULL;
    }

    TakeHttpsSem ();
    pConnId = SslArAccept (*pi4AcceptSock);
    GiveHttpsSem ();

    if (pConnId == NULL)
    {

        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                         HttpSrvSecureSock4CallBk);

#ifdef IP6_WANTED
        HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                         HttpSrvSecureSock6CallBk);
#endif

        HttpCloseSocket (*pi4AcceptSock);
        return NULL;
    }
    if (i4Family == AF_INET)
    {
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv4Sec[u4Counter].i4SockFd == -1)
            {
                gIpv4Sec[u4Counter].i4SockFd = *pi4AcceptSock;
                gIpv4Sec[u4Counter].ClientAddr = ClientAddr;
                break;
            }
        }
    }
#ifdef IP6_WANTED
    else if (i4Family == AF_INET6)
    {
        for (u4Counter = HTTP_ZERO;
             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; u4Counter++)
        {
            if (gIpv6Sec[u4Counter].i4SockFd == -1)
            {
                gIpv6Sec[u4Counter].i4SockFd = *pi4AcceptSock;
                gIpv6Sec[u4Counter].ClientV6Addr = ClientAddr6;
                break;
            }
        }
    }
#endif

    HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSockFd,
                     HttpSrvSecureSock4CallBk);

#ifdef IP6_WANTED
    HttpSrvSelAddFd (gHttpInfo.HttpCtrlInfo.i4HttpsSock6Fd,
                     HttpSrvSecureSock6CallBk);
#endif

    return pConnId;
}
#endif

/************************************************************************
 *  Function Name   : HttpGetChar 
 *  Description     : Function to read data from socket layer. This will
 *                    return a character whenever it is getting called.
 *  Input           : pHttp - Pointer to the Http entry.
 *  Output          : None
 *  Returns         : One single Character or FAILURE
 ************************************************************************/

INT1
HttpGetChar (tHttp * pHttp)
{
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;
    INT4                i4Count = HTTP_ZERO;
    INT4                i4Retval = HTTP_ZERO;
    struct timeval      tv;
    fd_set              rfds;
    INT1                i1Char = HTTP_ZERO;

    if (pHttp->i4Left == HTTP_ZERO)
    {
        MEMSET (pHttp->ai1Sockbuffer, HTTP_ZERO, sizeof (pHttp->ai1Sockbuffer));
#ifdef SSL_WANTED
        if (pHttp->pSslConnId != NULL)
        {
            /* This is an HTTPS connection, use SSLRead */
            INT4                i4NumBytes = ENM_MAX_SOC_BUF_LEN;

            if (SslArRead (pHttp->pSslConnId, (UINT1 *) pHttp->ai1Sockbuffer,
                           (UINT4 *) &i4NumBytes) < HTTP_ZERO)
            {
                ENM_TRACE ("Https: SSLRead failed!\n");
                pHttp->bConnClose = TRUE;
                return ENM_FAILURE;
            }
            pHttp->i4Read = i4NumBytes;
        }
        else
#endif
        {
            if (pHttp->bIsMultiData == OSIX_FALSE)
            {
                if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
                {
                    ENM_TRACE ("Get Osix Id of current Task failed\n");
                }
                for (i4Count = HTTP_ZERO;
                     i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT; i4Count++)
                {
                    /*No need to take HTTP lock here because Processing Task ID 
                     * are not subject change after Initialization
                     * All the Proceesing Task ID's are Initialized at start
                     * and do not change so no need to talk lock for this */
                    if (HTTP_GET_CLIENT_TASK_MAP (i4Count).
                        HttpProcessingTskId == ProcessingTaskId)
                    {
                        break;
                    }
                }

                if (i4Count >= HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT)
                {
                    return ENM_FAILURE;
                }

                /* TmpFD is maintined task wise. Set the TmpFD for each task */
                HTTP_LOCK ();
                MEMSET (&gaHttpFds[i4Count], HTTP_ZERO, sizeof (fd_set));
                gHttpInfo.pRdTmpFd[i4Count] = &gaHttpFds[i4Count];
                FD_SET (pHttp->i4Sockid, (gHttpInfo.pRdTmpFd[i4Count]));
                HTTP_UNLOCK ();

                tv.tv_sec = HTTP_ONE;
                tv.tv_usec = HTTP_ZERO;
                i4Retval =
                    select (pHttp->i4Sockid + HTTP_ONE, &gaHttpFds[i4Count],
                            NULL, NULL, &tv);

                if (i4Retval <= HTTP_ZERO)
                {
                    if (i4Retval == HTTP_ZERO)
                    {
                        ENM_TRACE1 ("Http: Time out occured \n %d", errno);
                    }
                    else
                    {
                        ENM_TRACE1 ("Http: Socket call failed\n %d", errno);
                    }
                    gpTmpFd = NULL;
                    return ENM_FAILURE;
                }

                if (!(FD_ISSET (pHttp->i4Sockid, &gaHttpFds[i4Count])))
                {
                    ENM_TRACE1 ("Http: No Data is available in socket\n %d",
                                errno);
                    gpTmpFd = NULL;
                    return ENM_FAILURE;
                }
            }
            else
            {
                MEMSET (&rfds, HTTP_ZERO, sizeof (fd_set));
                FD_SET (pHttp->i4Sockid, &rfds);

                tv.tv_sec = HTTP_ONE;
                tv.tv_usec = HTTP_ZERO;
                i4Retval =
                    select (pHttp->i4Sockid + HTTP_ONE, &rfds, NULL, NULL, &tv);
                if (i4Retval <= HTTP_ZERO)
                {
                    if (i4Retval == HTTP_ZERO)
                    {
                        ENM_TRACE1 ("Http: Time out occured \n %d", errno);
                    }
                    else
                    {
                        ENM_TRACE1 ("Http: Socket call failed\n %d", errno);
                    }
                    gpTmpFd = NULL;
                    return ENM_FAILURE;
                }

                if (!(FD_ISSET (pHttp->i4Sockid, &rfds)))
                {
                    ENM_TRACE1 ("Http: No Data is available in socket\n %d",
                                errno);
                    gpTmpFd = NULL;
                    return ENM_FAILURE;
                }
            }
            pHttp->i4Read = recv (pHttp->i4Sockid, pHttp->ai1Sockbuffer,
                                  ENM_MAX_SOC_BUF_LEN, HTTP_ZERO);
            if (pHttp->i4Read <= HTTP_ZERO)
            {
                if (pHttp->i4Read == HTTP_ZERO)
                {
                    pHttp->bConnClose = TRUE;
                }
                pHttp->i4Read = HTTP_ZERO;
                /* Other than EWOULDBLOCK OR EAGAIN cases,
                 * critical error occured in the socket*/
                gpTmpFd = NULL;
                if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
                {
                    return ENM_NO_DATA;
                }
                if (errno == ENOTCONN)
                {
                    pHttp->bConnClose = TRUE;
                }
                return ENM_FAILURE;
            }
        }
        pHttp->i4Left = pHttp->i4Read;
    }
    gpTmpFd = NULL;
    i1Char = pHttp->ai1Sockbuffer[pHttp->i4Read - pHttp->i4Left--];
    return i1Char;
}

/************************************************************************
 *  Function Name   : HttpProcessReqLine 
 *  Description     : Process the HTTP Request Received from client.
 *                    This will read Method,Url, Version and Query
 *                    message from the Http request.
 *  Input           : pHttp - Pointer to the Http Entry where the read
 *                    data will be stored.
 *  Output          : None.
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT1
HttpProcessReqLine (tHttp * pHttp)
{
    INT1                ai1VersionChar[HTTP_VERSION_LEN];
    INT1                i1Char = HTTP_ZERO;
    INT1                ai1Line[ENM_MAX_METHOD_LENGTH];
    UINT4               u4Len = HTTP_ZERO;
    UINT4               u4Temp = HTTP_ZERO;
    UINT4               u4Version = HTTP_ZERO;
#ifdef L2RED_WANTED
    INT4                i4BulkUpdtResult = RM_SUCCESS;
#endif

    MEMSET (ai1Line, HTTP_ZERO, ENM_MAX_METHOD_LENGTH);
    MEMSET (ai1VersionChar, HTTP_ZERO, HTTP_VERSION_LEN);

    /* Read HTTP Request Method GET/POST */
    for (u4Len = HTTP_ZERO; u4Len < ENM_MAX_METHOD_LENGTH; u4Len++)
    {
        i1Char = HttpGetChar (pHttp);
        if (i1Char == ENM_NO_DATA)
        {
            u4Len--;
            continue;
        }
        GET_CHAR_RETURN_CHECK (i1Char);
        if (i1Char == ' ')
        {
            ai1Line[u4Len] = '\0';
            break;
        }
        ai1Line[u4Len] = i1Char;
    }
    if ((STRCMP (ai1Line, HTTP_GET_METHOD) == HTTP_ZERO) ||
        (STRCMP (ai1Line, HTTP_PUT_METHOD) == HTTP_ZERO))
    {
        pHttp->i4Method = HTTP_GET;
    }
    else if (STRCMP (ai1Line, HTTP_POST_METHOD) == HTTP_ZERO)
    {
        pHttp->i4Method = HTTP_POST;
    }
    else
    {
        ENM_TRACE ("Http: Unknown HTTP Method\n");
        HttpSendError (pHttp, HTTP_METHOD_NOT_ALLOWED);
        return ENM_FAILURE;
    }

    /* Read HTTP Request URL */
    for (u4Len = HTTP_ZERO; u4Len < ENM_MAX_URL_LEN; u4Len++)
    {
        i1Char = HttpGetChar (pHttp);
        if (i1Char == ENM_NO_DATA)
        {
            u4Len--;
            continue;
        }
        GET_CHAR_RETURN_CHECK (i1Char);
        if ((i1Char == ' ') || (i1Char == '?') || (i1Char == '\r') ||
            (i1Char == '\n') || (i1Char == '\t'))
        {
            pHttp->ai1Url[u4Len] = '\0';
            break;
        }
        pHttp->ai1Url[u4Len] = i1Char;
    }
#ifdef L2RED_WANTED
    if (pHttp->i4Method == HTTP_POST)
    {
        if (RmGetPeerNodeCount () != 0)
        {
            i4BulkUpdtResult = RmIsBulkUpdateComplete ();
            if (i4BulkUpdtResult == RM_FAILURE)
            {
                pHttp->pu1BuffCurr = pHttp->pu1Buff;
                ENM_TRACE ("Http: RM Bulk Update in Progress\n");
                HttpSendError (pHttp, HTTP_UNUSED);
                return ENM_FAILURE;
            }
        }
    }
#endif

    /* Read Gambit Info */
    if (i1Char == '?')
    {
        for (u4Len = HTTP_ZERO; u4Len < ENM_MAX_SOC_BUF_LEN; u4Len++)
        {
            i1Char = HttpGetChar (pHttp);
            if (i1Char == ENM_NO_DATA)
            {
                u4Len--;
                continue;
            }
            GET_CHAR_RETURN_CHECK (i1Char);
            if (i1Char == ' ')
            {
                pHttp->au1PostQuery[u4Len] = '\0';
                break;
            }
            pHttp->au1PostQuery[u4Len] = i1Char;
        }
    }

    /* Read HTTP Request VERSION */
    for (u4Len = HTTP_ZERO; u4Len < HTTP_MAX_VERSION_POS; u4Len++)
    {
        i1Char = HttpGetChar (pHttp);
        if (i1Char == ENM_NO_DATA)
        {
            u4Len--;
            continue;
        }
        GET_CHAR_RETURN_CHECK (i1Char);
        if ((i1Char == '\n') || (i1Char == '\r'))
        {
            pHttp->ai1Version[u4Len] = '\0';
            break;
        }
        pHttp->ai1Version[u4Len] = i1Char;

        /* Read HTTP Request VESRION as Bad Request */
        if ((u4Len == HTTP_MIN_VERSION_POS) && (i1Char != '.'))
        {
            HttpSendError (pHttp, HTTP_BAD_REQUEST);
            return ENM_FAILURE;
        }

        if ((i1Char != '.') && (u4Len >= STRLEN ("HTTP/")))
        {
            STRNCPY (&ai1VersionChar[HTTP_ZERO], &i1Char, HTTP_ONE);
            ai1VersionChar[HTTP_ONE] = '\0';
            u4Temp = ATOI (ai1VersionChar);
            u4Version = u4Version * HTTP_CONVERSION_FACTOR + u4Temp;
        }
    }

    if (u4Version > HTTP_VERSION_NUMERIC_VALUE)
    {
        ENM_TRACE ("Http: HTTP Request version not supported\n");
        HttpSendError (pHttp, HTTP_VERSION_NOT_SUPPORTED);
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGetProtocol 
 *  Description     : From the URL this will decide whether the Request
 *                    is for Http Server or Webnm.
 *  Input           : pHttp- Pointer to Http entry where the URL is stored
 *  Output          : None 
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

PRIVATE INT4
HttpGetProtocol (tHttp * pHttp)
{
    UINT4               u4Count = HTTP_ZERO, u4Len = HTTP_ZERO;
    UINT4               u4Count1 = HTTP_ZERO;

    u4Count1 = HTTP_STRLEN (pHttp->ai1Url);
    for (u4Count = HTTP_ZERO; STRCMP (HttpUrlStr[u4Count], "NULL") != HTTP_ZERO;
         u4Count++)
    {
        u4Len = HTTP_STRLEN (HttpUrlStr[u4Count]);
        if (STRNCMP (pHttp->ai1Url, HttpUrlStr[u4Count], u4Len) == HTTP_ZERO)
        {
            if (HTTP_STRLEN (pHttp->ai1Url) > u4Len)
            {
                if ((u4Count1 - u4Len) >= HTTP_MAX_URL_LEN)
                {
                    HttpSendError (pHttp, HTTP_TOO_BIG);
                    return ENM_FAILURE;
                }
                SNPRINTF ((CHR1 *) pHttp->ai1HtmlName, ENM_MAX_NAME_LEN, "%s",
                          (pHttp->ai1Url + u4Len));
            }
            pHttp->i4Objreq = WEBNM_REQUEST;
            return ENM_SUCCESS;
        }
    }

#ifdef ISS_WANTED
    u4Len = HTTP_STRLEN (ISS_SPECIFIC_URL);
    if (STRNCMP (pHttp->ai1Url, ISS_SPECIFIC_URL, u4Len) == HTTP_ZERO)
    {
        if (HTTP_STRLEN (pHttp->ai1Url) > u4Len)
        {
            if ((u4Count1 - u4Len) >= HTTP_MAX_URL_LEN)
            {
                HttpSendError (pHttp, HTTP_TOO_BIG);
                return ENM_FAILURE;
            }
            SNPRINTF ((CHR1 *) pHttp->ai1HtmlName, ENM_MAX_NAME_LEN, "%s",
                      (pHttp->ai1Url + u4Len));
        }
        pHttp->i4Objreq = ISS_WEB_SPECIFIC_REQUEST;
        return ENM_SUCCESS;
    }

    u4Len = HTTP_STRLEN (ISS_INDEX_URL);
    if (STRNCMP (pHttp->ai1Url, ISS_INDEX_URL, u4Len) == HTTP_ZERO)
    {
        if (HTTP_STRLEN (pHttp->ai1Url) > u4Len)
        {
            if ((u4Count1 - u4Len) >= HTTP_MAX_URL_LEN)
            {
                HttpSendError (pHttp, HTTP_TOO_BIG);
                return ENM_FAILURE;
            }
            SNPRINTF ((CHR1 *) pHttp->ai1HtmlName, ENM_MAX_NAME_LEN, "%s",
                      (pHttp->ai1Url + u4Len));

        }
        pHttp->i4Objreq = ISS_WEB_REQUEST;
        return ENM_SUCCESS;
    }

#endif

    u4Len = HTTP_STRLEN (HTTP_INDEX_URL);
    if (STRNCMP (pHttp->ai1Url, HTTP_INDEX_URL, u4Len) == HTTP_ZERO)
    {
        if (HTTP_STRLEN (pHttp->ai1Url) > u4Len)
        {
            if ((u4Count1 - u4Len) >= HTTP_MAX_URL_LEN)
            {
                HttpSendError (pHttp, HTTP_TOO_BIG);
                return ENM_FAILURE;
            }
            SNPRINTF ((CHR1 *) pHttp->ai1HtmlName, ENM_MAX_NAME_LEN, "%s",
                      (pHttp->ai1Url + u4Len));
        }
        else if (HTTP_STRLEN (pHttp->ai1Url) == u4Len)
        {
            HTTP_STRCPY (pHttp->ai1HtmlName, HTTP_INDEX_FILE_NAME);
        }
        pHttp->i4Objreq = HTTP_REQUEST;
        return ENM_SUCCESS;
    }
    HttpSendError (pHttp, HTTP_BAD_REQUEST);

    return ENM_FAILURE;
}

/************************************************************************
 *  Function Name   : SockWrite 
 *  Description     : Function to Write the Data to the client socket
 *  Input           : pHttp - Pointer to the http entry where the socket
 *                    descriptor is stored
 *                    pi1Buffer - Pointer to the buffer to be written.
 *                    i4Size - Size of the buffer in bytes
 *  Output          : None
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
SockWrite (tHttp * pHttp, INT1 *pi1Buffer, INT4 i4Size)
{
    /*Checking if there is enough space in the pu1BuffCurr.If adequate 
     * space is not there flush the contents.If i4Size is greater than
     * buffer size then copy buffersize number of bytes into buffer
     * and flush it.
     * When i4Size is less than buffersize copy contents into 
     * the buffer*/
    if ((pHttp->i4ResType != HTTP_DYNAMIC_OBJECT))
    {
        if (SockWriteStatic (pHttp, pi1Buffer, i4Size) == ENM_FAILURE)
        {
            return ENM_FAILURE;
        }
    }
    else
    {
        if ((pHttp->pu1BuffCurr + HTTP_MAX_CHUNK_HEADER_LEN
             + i4Size - pHttp->pu1Buff) > BUFFER_SIZE)
        {
            if (FlushHttpBuff (pHttp) == ENM_FAILURE)
            {
                return ENM_FAILURE;
            }
            pHttp->pu1BuffCurr = pHttp->pu1Buff;
            MEMSET (pHttp->pu1BuffCurr, HTTP_ZERO, BUFFER_SIZE);

            while (i4Size > (BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN))
            {
                HttpInsertChunkSize (pHttp, pi1Buffer,
                                     BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN,
                                     FALSE);
                MEMCPY (pHttp->pu1BuffCurr, (UINT1 *) pi1Buffer,
                        BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN);
                pHttp->pu1BuffCurr += (BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN);
                STRNCPY (pHttp->pu1BuffCurr, HTTP_COMMON_DELIMITER,
                         STRLEN (HTTP_COMMON_DELIMITER));
                pHttp->pu1BuffCurr += STRLEN (HTTP_COMMON_DELIMITER);
                pi1Buffer += (BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN);
                i4Size -= (BUFFER_SIZE - HTTP_MAX_CHUNK_HEADER_LEN);
                if (FlushHttpBuff (pHttp) == ENM_FAILURE)
                {
                    return ENM_FAILURE;
                }
                pHttp->pu1BuffCurr = pHttp->pu1Buff;
                MEMSET (pHttp->pu1BuffCurr, HTTP_ZERO, BUFFER_SIZE);
            }
        }
        if (i4Size > HTTP_ZERO)
        {
            HttpInsertChunkSize (pHttp, pi1Buffer, i4Size, FALSE);
            MEMCPY (pHttp->pu1BuffCurr, (UINT1 *) pi1Buffer, i4Size);
            pHttp->pu1BuffCurr += i4Size;
            STRNCPY (pHttp->pu1BuffCurr, HTTP_COMMON_DELIMITER,
                     STRLEN (HTTP_COMMON_DELIMITER));
            pHttp->pu1BuffCurr += STRLEN (HTTP_COMMON_DELIMITER);
            HttpInsertChunkSize (pHttp, pi1Buffer, i4Size, TRUE);
        }
    }
    return ENM_SUCCESS;
}

#ifdef ISS_WANTED
/************************************************************************
 *  Function Name   : HttpReadFlash
 *  Description     : Read file from from flash if present
 *  Input           : FileName, FileContent, FileSize
 *  Output          : None
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
HttpReadFlash (INT1 *pi1FileName, INT1 *pi1FlashData, UINT4 *pu4FileSize)
{
    INT4                i4Flashfp, i4FileSize;
    CHR1                au1FlashFile[HTTP_MAX_URL_LEN];

    if (pi1FlashData == NULL)
    {
        return ENM_FAILURE;
    }
    SNPRINTF (au1FlashFile, HTTP_MAX_URL_LEN, "%s%s", FLASH, pi1FileName);
    i4Flashfp = FileOpen ((UINT1 *) au1FlashFile, 1);
    if (i4Flashfp == -1)
    {
        ENM_TRACE ("File not found in Flash ... \n");
        return ENM_FAILURE;
    }

    MEMSET (pi1FlashData, HTTP_ZERO, HTTP_MAX_FLASH_FILESIZE);
    i4FileSize = FileSize (i4Flashfp);
    if (i4FileSize < 0)
    {
        FileClose (i4Flashfp);
        return ENM_FAILURE;
    }
    *pu4FileSize =
        (UINT4) ((i4FileSize >
                  HTTP_MAX_FLASH_FILESIZE) ? HTTP_MAX_FLASH_FILESIZE :
                 i4FileSize);
    if (FileRead (i4Flashfp, (CHR1 *) pi1FlashData, *pu4FileSize) <= 0)
    {
        ENM_TRACE ("File Read failed or File empty\n");
    }
    FileClose (i4Flashfp);
    return ENM_SUCCESS;
}
#endif /* ISS_WANTED */

/************************************************************************
 *  Function Name   : HttpReadhtml 
 *  Description     : Function copy the html file content and related info
 *                    to http entry pointer from phtml.
 *  Input           : pHttp - Pointer to http entry where the html file
 *                    content to be mapped(copied)
 *  Output          : None
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
HttpReadhtml (tHttp * pHttp)
{
    UINT1               au1TempTag[ENM_MAX_ETAG_LEN];
    INT4                i4Count = HTTP_ZERO;
    INT4                i4Count1 = HTTP_ZERO;
#ifdef ISS_WANTED
    UINT4               u4FileSize = HTTP_ZERO;

    INT1               *pi1Header = NULL;
    INT4                i4Index = ENM_FAILURE;

    MEMSET (au1TempTag, HTTP_ZERO, sizeof (au1TempTag));

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index)
        == ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    if (STRNCMP (FLASH_FILE, pHttp->ai1HtmlName, STRLEN (FLASH_FILE)) ==
        HTTP_ZERO)
    {

        if ((HttpReadFlash (pHttp->ai1HtmlName + STRLEN (FLASH_FILE),
                            gai1FileData[i4Index], &u4FileSize)) == ENM_SUCCESS)
        {
            pHttp->u4GroupID = ENM_WRITE;
            pHttp->u4PageAccess = 0xff;
            pHttp->pi1Html = (INT1 *) gai1FileData[i4Index];
            pHttp->i4MibType = HTTP_ZERO;
            pHttp->i4NoOfSnmpOid = HTTP_ZERO;
            pHttp->pOid = NULL;
            pHttp->i4HtmlSize = (INT4) u4FileSize;
            pHttp->pu1Type = NULL;
            pHttp->pu1Access = NULL;
            return ENM_SUCCESS;
        }
        STRNCPY (pHttp->ai1HtmlName, pHttp->ai1HtmlName + STRLEN (FLASH_FILE),
                 sizeof (pHttp->ai1HtmlName));
    }
#endif /* ISS_WANTED */

#ifdef ISS_WANTED
    /* Check if the HTML page needs to redirected to some other page */
    IssRedirectHtmlPage (pHttp);
#endif /* ISS_WANTED */

    for (i4Count = HTTP_ZERO; ghtml[i4Count].pi1PageIndex != NULL; i4Count++)
    {
        if (STRNCMP
            (ghtml[i4Count].pi1PageIndex, pHttp->ai1HtmlName,
             HTTP_HTML_NAME_COMPARE_LEN) == HTTP_ZERO)
        {
            for (i4Count1 = HTTP_ZERO;
                 ghtml[i4Count].pPageInfoArray[i4Count1].pi1Name != NULL;
                 i4Count1++)
            {
                if (STRCMP
                    (ghtml[i4Count].pPageInfoArray[i4Count1].pi1Name,
                     pHttp->ai1HtmlName) == HTTP_ZERO)
                {
                    pHttp->u4GroupID =
                        ghtml[i4Count].pPageInfoArray[i4Count1].pPageAccess->
                        u4GroupID;
                    pHttp->u4PageAccess =
                        ghtml[i4Count].pPageInfoArray[i4Count1].pPageAccess->
                        u4Access;
                    pHttp->pi1Html =
                        (INT1 *) ghtml[i4Count].pPageInfoArray[i4Count1].pi1Ptr;
                    pHttp->i4MibType =
                        ghtml[i4Count].pPageInfoArray[i4Count1].i4MibType;
                    pHttp->i4NoOfSnmpOid =
                        ghtml[i4Count].pPageInfoArray[i4Count1].i4NoOfSnmpOid;
                    pHttp->pOid = ghtml[i4Count].pPageInfoArray[i4Count1].pOid;
                    pHttp->i4HtmlSize =
                        ghtml[i4Count].pPageInfoArray[i4Count1].i4Size;
                    pHttp->pu1Type =
                        ghtml[i4Count].pPageInfoArray[i4Count1].pu1Type;
                    pHttp->pu1Access =
                        ghtml[i4Count].pPageInfoArray[i4Count1].pu1Access;
                    SPRINTF ((CHR1 *) au1TempTag, "%d-%d",
                             ghtml[i4Count].pPageInfoArray[i4Count1].
                             i4LastModTime,
                             ghtml[i4Count].pPageInfoArray[i4Count1].i4Size);
                    /* When the request consists the Etag then we compare it
                     * ETAG of the partciular entity. If the Entity matches 
                     * we send the response as Not Modified. */
                    if (STRCMP (pHttp->au1Etag, au1TempTag) == HTTP_ZERO)
                    {
                        pHttp->u2RespCode = HTTP_NOT_MODIFIED;
                        HttpSendHeader (pHttp, HTTP_UNMODIFIED_OBJECT);
                        return ENM_FAILURE;
                    }
                    /* If the ETAG does not match then we have to send the
                     * response with proper ETAG*/
                    MEMSET (pHttp->au1Etag, HTTP_ZERO, ENM_MAX_ETAG_LEN);
                    STRCPY (pHttp->au1Etag, au1TempTag);
                    return ENM_SUCCESS;
                }
            }
        }
    }

#ifdef ISS_WANTED

    /*For Processing target specific pages */
    for (i4Count = HTTP_ZERO; ghtmlTarget[i4Count].pi1PageIndex != NULL;
         i4Count++)
    {
        if (STRNCMP
            (ghtmlTarget[i4Count].pi1PageIndex, pHttp->ai1HtmlName,
             HTTP_HTML_NAME_COMPARE_LEN) == HTTP_ZERO)
        {
            for (i4Count1 = HTTP_ZERO;
                 ghtmlTarget[i4Count].pPageInfoArray[i4Count1].pi1Name != NULL;
                 i4Count1++)
            {
                if (STRCMP
                    (ghtmlTarget[i4Count].pPageInfoArray[i4Count1].pi1Name,
                     pHttp->ai1HtmlName) == HTTP_ZERO)
                {
                    pHttp->u4GroupID =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].
                        pPageAccess->u4GroupID;
                    pHttp->u4PageAccess =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].
                        pPageAccess->u4Access;
                    pHttp->pi1Html =
                        (INT1 *) ghtmlTarget[i4Count].pPageInfoArray[i4Count1].
                        pi1Ptr;
                    pHttp->i4MibType =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].i4MibType;
                    pHttp->i4NoOfSnmpOid =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].
                        i4NoOfSnmpOid;
                    pHttp->pOid =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].pOid;
                    pHttp->i4HtmlSize =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].i4Size;
                    pHttp->pu1Type =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].pu1Type;
                    pHttp->pu1Access =
                        ghtmlTarget[i4Count].pPageInfoArray[i4Count1].pu1Access;
                    SPRINTF ((CHR1 *) pHttp->au1Etag, "%d-%d",
                             ghtml[i4Count].pPageInfoArray[i4Count1].
                             i4LastModTime,
                             ghtml[i4Count].pPageInfoArray[i4Count1].i4Size);
                    /* When the request consists the Etag then we compare it
                     * ETAG of the partciular entity. If the Entity matches 
                     * we send the response as Not Modified. */
                    if (STRCMP (pHttp->au1Etag, au1TempTag) == HTTP_ZERO)
                    {
                        pHttp->u2RespCode = HTTP_NOT_MODIFIED;
                        HttpSendHeader (pHttp, HTTP_UNMODIFIED_OBJECT);
                        return ENM_FAILURE;
                    }
                    /* If the ETAG does not match then we have to send the
                     * response with proper ETAG*/
                    MEMSET (pHttp->au1Etag, HTTP_ZERO, ENM_MAX_ETAG_LEN);
                    STRCPY (pHttp->au1Etag, au1TempTag);
                    return ENM_SUCCESS;
                }
            }
        }
    }

    /* Access file system */

    if (STRCMP (pHttp->ai1HtmlName, "iss.conf") == HTTP_ZERO)
    {
        STRCPY (HTTP_HEADER_STRING,
                "HTTP/1.0 200 OK\r\nContent-Type: text/conf\r\nContent-disposition: attachment; filename=iss.conf\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nExpires: -1\r\n\r\n");
        pi1Header = gai1FileData[i4Index];

        MEMSET (pi1Header, HTTP_ZERO,
                (STRLEN (HTTP_HEADER_STRING) + MSR_MIBENTRY_MEMBLK_SIZE));
        MEMCPY (pi1Header, HTTP_HEADER_STRING, STRLEN (HTTP_HEADER_STRING));
        pHttp->pi1Html = pi1Header;
        if ((HttpReadFile
             (pHttp->ai1HtmlName, pi1Header + STRLEN (HTTP_HEADER_STRING),
              &u4FileSize)) == ENM_SUCCESS)
        {
            pHttp->u4GroupID = ENM_WRITE;
            pHttp->u4PageAccess = 0xff;
            pHttp->i4MibType = HTTP_ZERO;
            pHttp->i4NoOfSnmpOid = HTTP_ZERO;
            pHttp->pOid = NULL;
            pHttp->i4HtmlSize =
                (INT4) (u4FileSize + STRLEN (HTTP_HEADER_STRING));
            pHttp->pu1Type = NULL;
            pHttp->pu1Access = NULL;
            return ENM_SUCCESS;
        }
    }
    if ((HttpReadFile
         (pHttp->ai1HtmlName, gai1FileData[i4Index],
          &u4FileSize)) == ENM_SUCCESS)
    {
        pHttp->u4GroupID = ENM_WRITE;
        pHttp->u4PageAccess = 0xff;
        pHttp->pi1Html = (INT1 *) gai1FileData[i4Index];
        pHttp->i4MibType = HTTP_ZERO;
        pHttp->i4NoOfSnmpOid = HTTP_ZERO;
        pHttp->pOid = NULL;
        pHttp->i4HtmlSize = (INT4) u4FileSize;
        pHttp->pu1Type = NULL;
        pHttp->pu1Access = NULL;
        return ENM_SUCCESS;
    }
#endif /* ISS_WANTED */

    HttpSendError (pHttp, HTTP_PAGE_NOT_FOUND);
    ENM_TRACE1 ("Http: No Such HTML Page %s\n", pHttp->ai1HtmlName);
    return ENM_FAILURE;
}

#ifdef ISS_WANTED
/*****************************************************************************/
/* Function Name      : HttpReadFile                                         */
/*                                                                           */
/* Description        : This function is used to read the html directly      */
/*                      from the file when htmldata.h doesnot contain        */
/*                      required html information.                           */
/*                                                                           */
/* Input(s)           : pi1FileName, pi1FlashData & pi4FileSize              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                           */
/*****************************************************************************/
INT4
HttpReadFile (INT1 *pi1FileName, INT1 *pi1FlashData, UINT4 *pu4FileSize)
{
    INT4                i4Flashfp, i4FileSize;
    CHR1                au1FlashFile[HTTP_MAX_URL_LEN];

    MEMSET (au1FlashFile, HTTP_ZERO, HTTP_MAX_URL_LEN);

    SNPRINTF (au1FlashFile, HTTP_MAX_URL_LEN, "%s/%s",
              ISS_DEF_OPENSRC_HTML_PATH, pi1FileName);

    i4Flashfp = FileOpen ((UINT1 *) au1FlashFile, 1);
    if (i4Flashfp == -1)
    {
        ENM_TRACE ("File not found in Flash ... \n");
        return ENM_FAILURE;
    }

    MEMSET (pi1FlashData, HTTP_ZERO, HTTP_MAX_FLASH_FILESIZE);
    i4FileSize = FileSize (i4Flashfp);
    if (i4FileSize < 0)
    {
        FileClose (i4Flashfp);
        return ENM_FAILURE;
    }
    *pu4FileSize =
        (UINT4) ((i4FileSize >
                  HTTP_MAX_FLASH_FILESIZE) ? HTTP_MAX_FLASH_FILESIZE :
                 i4FileSize);
    if (FileRead (i4Flashfp, (CHR1 *) pi1FlashData, *pu4FileSize) <= 0)
    {
        ENM_TRACE ("File Read failed or File empty\n");
    }
    FileClose (i4Flashfp);
    return ENM_SUCCESS;
}

#endif
/************************************************************************
 *  Function Name   : HttpGetNameValue 
 *  Description     : Get the Name Value Pair from the Post Query
 *  Input           : pu1Array - Pointer to the Query String
 *                    u4Index - Nth Name value pair
 *  Output          : pu1FirstPointer - Pointer where the name to be copied 
 *                    pu1NextPointer - Pointer where the Value to be copied
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/

INT4
HttpGetNameValue (UINT1 *pu1FirstPointer,
                  UINT1 *pu1NextPointer, UINT1 *pu1Array, UINT4 u4Index)
{
    UINT4               u4Count = HTTP_ONE;
    UINT1              *pu1Ptr = pu1Array;
    UINT1              *pu1End = pu1Array + ENM_MAX_NAME_LEN;
    UINT1              *pu1Name = pu1FirstPointer;
    UINT1              *pu1Value = pu1NextPointer;
    while (u4Count < u4Index)
    {
        if (*pu1Ptr == '&')
        {
            u4Count++;
        }
        pu1Ptr++;
        CHECK_POINTER (pu1Ptr, pu1End);
    }
    MEMSET (pu1Name, HTTP_ZERO, ENM_MAX_NAME_LEN);
    MEMSET (pu1Value, HTTP_ZERO, ENM_MAX_NAME_LEN);
    while (*pu1Ptr != '=')
    {
        *pu1Name = *pu1Ptr;
        pu1Ptr++;
        pu1Name++;
        CHECK_POINTER (pu1Ptr, pu1End);
    }
    pu1Ptr++;
    while (*pu1Ptr != '&')
    {
        *pu1Value = *pu1Ptr;
        pu1Ptr++;
        pu1Value++;
        CHECK_POINTER (pu1Ptr, pu1End);
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGetValuebyName 
 *  Description     : Get the Value for a Name from Post Query 
 *  Inpu            : pu1FirstPointer - Pointer to a name whos value to
 *                    be fetched from Query Buffer pu1Array
 *  Output          : pu1NextPointer - Pointer where the value will be copied
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
HttpGetValuebyName (UINT1 *pu1FirstPointer,
                    UINT1 *pu1NextPointer, UINT1 *pu1Array)
{
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1End = pu1Array + ENM_MAX_NAME_LEN;
    UINT1              *pu1Value = pu1NextPointer;

    MEMSET (pu1Value, HTTP_ZERO, ENM_MAX_NAME_LEN);
    pu1Ptr = (UINT1 *) STRSTR (pu1Array, pu1FirstPointer);
    if (pu1Ptr == NULL)
    {
        return ENM_FAILURE;
    }
    pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
    if (*pu1Ptr != '=')
    {
        pu1Ptr = (UINT1 *) STRSTR (pu1Ptr, pu1FirstPointer);
        if (pu1Ptr == NULL)
        {
            return ENM_FAILURE;
        }
        pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
    }
    pu1Ptr++;

    CHECK_POINTER (pu1Ptr, pu1End);
    while (*pu1Ptr != '&')
    {
        if (*pu1Ptr == '\0')
        {
            break;
        }
        *pu1Value = *pu1Ptr;
        pu1Ptr++;
        pu1Value++;
        CHECK_POINTER (pu1Ptr, pu1End);
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpGetNonZeroValuebyName 
 *  Description     : Get the Value for a Name from Post Query 
 *  Inpu            : pu1FirstPointer - Pointer to a name whos value to
 *                    be fetched from Query Buffer pu1Array
 *  Output          : pu1NextPointer - Pointer where the value will be copied
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
HttpGetNonZeroValuebyName (UINT1 *pu1FirstPointer,
                           UINT1 *pu1NextPointer, UINT1 *pu1Array)
{
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1End = pu1Array + ENM_MAX_NAME_LEN;
    UINT1              *pu1Value = pu1NextPointer;
    INT4                i4Count = HTTP_ZERO;

    do
    {
        MEMSET (pu1Value, HTTP_ZERO, ENM_MAX_NAME_LEN);
        pu1Ptr = (UINT1 *) STRSTR (pu1Array, pu1FirstPointer);
        i4Count = pu1Ptr - pu1Array;
        pu1Array = pu1Array + i4Count;
        pu1Array = pu1Array + STRLEN (pu1FirstPointer);
        if (pu1Ptr == NULL)
        {
            return ENM_FAILURE;
        }
        pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
        if (*pu1Ptr != '=')
        {
            pu1Ptr = (UINT1 *) STRSTR (pu1Ptr, pu1FirstPointer);
            if (pu1Ptr == NULL)
            {
                return ENM_FAILURE;
            }
            pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
        }
        pu1Ptr++;

        CHECK_POINTER (pu1Ptr, pu1End);
        while (*pu1Ptr != '&')
        {
            if (*pu1Ptr == '\0')
            {
                break;
            }
            *pu1Value = *pu1Ptr;
            pu1Ptr++;
            pu1Value++;
            CHECK_POINTER (pu1Ptr, pu1End);
        }

    }
    while (ATOI (pu1NextPointer) == HTTP_ZERO);
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpNextGetValuebyName 
 *  Description     : Get the next Value for a Name from Post Query 
 *  Inpu            : pu1FirstPointer - Pointer to a name whos value to
 *                    be fetched from Query Buffer pu1Array
 *  Output          : pu1NextPointer - Pointer where the value will be copied
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
HttpNextGetValuebyName (UINT1 *pu1FirstPointer,
                        UINT1 *pu1NextPointer, UINT1 *pu1Array)
{
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1End = pu1Array + ENM_MAX_NAME_LEN;
    UINT1              *pu1Value = pu1NextPointer;
    pu1Ptr = (UINT1 *) STRSTR (pu1Array, pu1FirstPointer);
    if (pu1Ptr == NULL)
    {
        return ENM_FAILURE;
    }
    pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
    pu1Ptr = (UINT1 *) STRSTR (pu1Ptr, pu1FirstPointer);
    if (pu1Ptr == NULL)
    {
        return ENM_FAILURE;
    }
    pu1Ptr += HTTP_STRLEN (pu1FirstPointer);
    pu1Ptr++;
    CHECK_POINTER (pu1Ptr, pu1End);
    MEMSET (pu1Value, HTTP_ZERO, ENM_MAX_NAME_LEN);
    while (*pu1Ptr != '&')
    {
        if (*pu1Ptr == '\0')
        {
            break;
        }
        *pu1Value = *pu1Ptr;
        pu1Ptr++;
        pu1Value++;
        CHECK_POINTER (pu1Ptr, pu1End);
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpSendError 
 *  Description     : Sends a Error Html Page.
 *  Input           : pHttp - Pointer to Http Entry
 *                    u4Error - Http Error Code
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpSendError (tHttp * pHttp, UINT4 u4Error)
{
    UINT1               au1Array[ENM_MAX_HEADER_LENGTH];
    UINT4               u4Count = HTTP_ZERO;

    MEMSET (au1Array, HTTP_ZERO, ENM_MAX_HEADER_LENGTH);
    for (u4Count = HTTP_ZERO; aHSReasonPhrase[u4Count].pi1ReasonPhrase != NULL;
         u4Count++)
    {
        if (aHSReasonPhrase[u4Count].u4Status == u4Error)
        {
            SNPRINTF ((char *) au1Array, ENM_MAX_HA2_LEN, "%s %u %s",
                      HTTP_ERROR_HTML, u4Error,
                      aHSReasonPhrase[u4Count].pi1ReasonPhrase);
            pHttp->u2RespCode = (UINT2) u4Error;
            pHttp->i4HtmlSize = HTTP_STRLEN (au1Array);
            pHttp->u2ResourceType = HTTP_TEXT_HTML_RESOURCE;
            if (HttpSendHeader (pHttp, HTTP_STATIC_OBJECT) == ENM_FAILURE)
            {
                return;
            }
            SockWrite (pHttp, (INT1 *) au1Array, pHttp->i4HtmlSize);
            return;
        }
    }
}

/************************************************************************
 *  Function Name   : HttpSendFlashSpecificError 
 *  Description     : Sends a Error Html Page.
 *  Input           : pHttp - Pointer to Http Entry
 *                    u4Error - Http Error Code
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpSendFlashSpecificError (tHttp * pHttp, UINT4 u4Error)
{
    UINT1               au1Array[ENM_MAX_HEADER_LENGTH];
    UINT4               u4Count = HTTP_ZERO;

    MEMSET (au1Array, HTTP_ZERO, ENM_MAX_HEADER_LENGTH);
    for (u4Count = HTTP_ZERO; aHSReasonPhrase[u4Count].pi1ReasonPhrase != NULL;
         u4Count++)
    {
        if (aHSReasonPhrase[u4Count].u4Status == u4Error)
        {
            SNPRINTF ((char *) au1Array, ENM_MAX_HA2_LEN, "%s %u %s",
                      HTTP_FLASH_ERROR_HTML, u4Error,
                      aHSReasonPhrase[u4Count].pi1ReasonPhrase);
            pHttp->u2RespCode = (UINT2) u4Error;
            pHttp->i4HtmlSize = HTTP_STRLEN (au1Array);
            if (HttpSendHeader (pHttp, HTTP_STATIC_OBJECT) == ENM_FAILURE)
            {
                return;
            }
            SockWrite (pHttp, (INT1 *) au1Array, HTTP_STRLEN (au1Array));
            return;
        }
    }
}

/************************************************************************
 *  Function Name   : Httpstrlen 
 *  Description     : Find the Length of the Given String
 *  Input           : pu1Array - Pointer to a string
 *  Output          : None
 *  Returns         : Length of the given string
 ************************************************************************/

UINT4
Httpstrlen (UINT1 *pu1Array)
{
    UINT4               u4Len = HTTP_ZERO;
    u4Len = STRLEN (pu1Array);
    if (u4Len >= ENM_MAX_NAME_LEN)
    {
        u4Len = HTTP_ZERO;
    }
    return u4Len;
}

/************************************************************************
 *  Function Name   : Httpstrcpy 
 *  Description     : Copy one string to another one
 *  Input           : pu1Dest - Destination string pointer
 *                    pu1Src - Source String Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
Httpstrcpy (UINT1 *pu1Dest, UINT1 *pu1Src)
{
    UINT4               u4Len = HTTP_ZERO;
    u4Len = STRLEN (pu1Src);
    if (u4Len >= ENM_MAX_NAME_LEN)
    {
        u4Len = (ENM_MAX_NAME_LEN - HTTP_ONE);
    }
    STRNCPY (pu1Dest, pu1Src, u4Len);
    pu1Dest[u4Len] = HTTP_ZERO;
}

/************************************************************************
 *  Function Name   : Httpstrcat 
 *  Description     : Concatenate to strings
 *  Input           : pu1Dest - Destiantion string to which the source
 *                    string pu1Src to be copied
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
Httpstrcat (UINT1 *pu1Dest, UINT1 *pu1Src)
{
    UINT4               u4Count = HTTP_ZERO, u4Len = HTTP_ZERO, u4SrcCount =
        HTTP_ZERO;
    u4Len = HTTP_STRLEN (pu1Dest);
    for (u4Count = u4Len; u4Count < ENM_MAX_NAME_LEN; u4Count++)
    {
        if (pu1Src[u4SrcCount] == '\0')
        {
            pu1Dest[u4Count] = '\0';
            return;
        }
        pu1Dest[u4Count] = pu1Src[u4SrcCount];
        u4SrcCount++;
    }
    return;
}

/************************************************************************
 *  Function Name   : Httpstrncat
 *  Description     : Concatenate to strings
 *  Input           : pu1Dest - Destiantion string to which the source
 *                    string pu1Src to be copied upto length bytes
 *  Output          : None
 *  Returns         : None
 *************************************************************************/

VOID
HttpStrnCat (UINT1 *pu1Dest, UINT1 *pu1Src, UINT4 length)
{
    UINT4               u4Count = HTTP_ZERO, u4Len = HTTP_ZERO, u4SrcCount =
        HTTP_ZERO;
    u4Len = HTTP_STRLEN (pu1Dest);
    for (u4Count = u4Len; (u4SrcCount < length) && (u4Count < ENM_MAX_NAME_LEN);
         u4Count++)
    {
        if (pu1Src[u4SrcCount] == '\0')
        {
            pu1Dest[u4Count] = '\0';
            return;
        }
        pu1Dest[u4Count] = pu1Src[u4SrcCount];
        u4SrcCount++;
    }
    return;
}

/************************************************************************
 *  Function Name   : HttpShutDown 
 *  Description     : This will shutdown HTTP server. 
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
HttpShutDown ()
{
    INT4                i4RetVal = HTTP_ZERO;
    INT4                i4Count = HTTP_ZERO;

    if (gHttpInfo.i4SrvSockId != HTTP_INVALID_SOCK_FD)
    {
        HttpSrvSelRemFd (gHttpInfo.i4SrvSockId);
        i4RetVal = HttpCloseSocket (gHttpInfo.i4SrvSockId);
        if (i4RetVal == ENM_FAILURE)
        {
            ENM_TRACE1 ("Http: Unable To Close %d\n", errno);
        }
    }
    if (gHttpInfo.i4SrvSock6Id != HTTP_INVALID_SOCK_FD)
    {
        HttpSrvSelRemFd (gHttpInfo.i4SrvSock6Id);
        i4RetVal = HttpCloseSocket (gHttpInfo.i4SrvSock6Id);
        if (i4RetVal == ENM_FAILURE)
        {
            ENM_TRACE1 ("Http: Unable To Close %d\n", errno);
        }
    }
    HttpGlobalMemDeInit ();
    HttpGlobalSemDeInit ();

    if (WEBNM_HTTP_TIME_OUT == OSIX_TRUE)
    {
        HttpGlobalTmrDeInit ();
    }
    HttpGlobalQDeInit ();
    OsixTskDel (gHttpInfo.HttpMainTskId);
    gHttpInfo.HttpMainTskId = HTTP_ZERO;

    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        gHttpInfo.HttpProcTaskId[i4Count] = HTTP_INVALID_TASK_ID;
        OsixTskDel (gHttpInfo.HttpProcTaskId[i4Count]);
    }

    ENM_TRACE ("Http: ShutDown Success\n");
}

/************************************************************************
 *  Function Name   : HttpInit 
 *  Description     : This will Init  HTTP server. 
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpInit ()
{
    HSLaunch ();
    ENM_TRACE ("Http: Init Success\n");
}

/************************************************************************ 
*  Function Name   : HttpGetSourcePort 
*  Description     : This will get HTTP server port 
*  Input           : None 
*  Output          : None 
*  Returns         : None 
************************************************************************/
INT4
HttpGetSourcePort ()
{
    return (gi4HttpPort);
}

/************************************************************************ 
 *  Function Name   : HttpSetSourcePort 
 *  Description     : This will get HTTP server port 
 *  Input           : None 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/
INT4
HttpSetSourcePort (INT4 i4Port)
{
    gi4HttpPort = i4Port;
    return ENM_SUCCESS;
}

/************************************************************************
 *  Functtion name  : Httpsetsourceport
 *  Description     : This will set Http server bind Address
 *  Input           : none
 *  Output          : none
 *  Returns         : none
 ************************************************************************/
VOID
HttpSetSourceBindAddr (UINT4 u4BindAddr)
{
    gu4HttpSrvrBindAddr = u4BindAddr;
    return;
}

/************************************************************************
 *  Function Name   : HttpSetTimeOutMode
 *  Description     : This will set HTTP Time out Mode - Enable/Disable
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSetTimeOutMode (UINT1 u1TimeOutMode)
{
    gu1HttpTimeOutEnable = u1TimeOutMode;
    return;
}

/************************************************************************
 *  Function Name   : HttpSetLoginReq
 *  Description     : This will set HTTP Login Req. - Enable/Disable
 *  Input           : b1LoginReq - OSIX_TRUE/OSIX_FALSE
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
HttpSetLoginReq (BOOL1 b1LoginReq)
{
    gb1HttpLoginReq = b1LoginReq;
    return;
}

/*********i******************************************************/
/*  Function Name   : HttpsSetTraceLevel                        */
/*                                                              */
/*  Description     : This function is used to set the          */
/*                    trace level for Https sessions            */
/*                                                              */
/*  Input(s)        : i4TraceLevel - Trace level to be set      */
/*                                                              */
/*  Output(s)       : None                                      */
/*                                                              */
/*  Returns         : None                                      */
/****************************************************************/
VOID
HttpsSetTraceLevel (INT4 i4TraceLevel)
{
    gi4HttpsTrcLevel = i4TraceLevel;
    return;
}

/*****************************************************************************/
/* Function Name      : HttpLock                                             */
/*                                                                           */
/* Description        : This function is used to take the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                         */
/*****************************************************************************/
INT4
HttpLock (VOID)
{
    if (OsixSemTake (gHttpInfo.HttpSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                           */
/*****************************************************************************/

INT4
HttpUnLock (VOID)
{
    if (OsixSemGive (gHttpInfo.HttpSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpMgmtLock                                         */
/*                                                                           */
/* Description        : This function is used to take the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                         */
/*****************************************************************************/
INT4
HttpMgmtLock (VOID)
{
    if (OsixSemTake (gHttpInfo.HttpMgmtSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpMgmtUnLock                                       */
/*                                                                           */
/* Description        : This function is used to give the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                           */
/*****************************************************************************/

INT4
HttpMgmtUnLock (VOID)
{
    if (OsixSemGive (gHttpInfo.HttpMgmtSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpDbLock                                           */
/*                                                                           */
/* Description        : This function is used to take the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                         */
/*****************************************************************************/
INT4
HttpDbLock (VOID)
{
    if (OsixSemTake (gHttpInfo.HttpDbSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpDbUnLock                                         */
/*                                                                           */
/* Description        : This function is used to give the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                           */
/*****************************************************************************/
INT4
HttpDbUnLock (VOID)
{
    if (OsixSemGive (gHttpInfo.HttpDbSemId) != OSIX_SUCCESS)
    {
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FlushHttpBuff                                        */
/*                                                                           */
/* Description        : This function is used to take the HTTP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                         */
/*****************************************************************************/
INT4
FlushHttpBuff (tHttp * pHttp)
{
    INT4                i4Size = HTTP_ZERO;
    INT1               *pi1Buffer = NULL;
    INT4                i4Written = HTTP_ZERO;
    INT4                i4WriteLen = HTTP_ZERO;
    INT4                i4RetVal = HTTP_ZERO;
    UINT4               u4ContextId = HTTP_ZERO;
    INT4                i4OutCome = HTTP_ZERO;
    BOOL1               b1SendFailure = FALSE;
#ifdef WLC_WANTED
    tOsixTaskId         ProcessingTaskId = HTTP_ZERO;
#endif

    i4Size = pHttp->pu1BuffCurr - pHttp->pu1Buff;
    pi1Buffer = (INT1 *) pHttp->pu1Buff;

#ifdef WLC_WANTED
    if (OsixTskIdSelf (&ProcessingTaskId) == OSIX_FAILURE)
    {
        ENM_TRACE ("Get Osix Id of current Task failed\n");
    }
    if (HTTP_GET_CLIENT_TASK_MAP (0).HttpProcessingTskId == ProcessingTaskId)
    {
        u4gTaskOneCount = 0;
    }

    if (HTTP_GET_CLIENT_TASK_MAP (1).HttpProcessingTskId == ProcessingTaskId)
    {
        u4gTaskTwoCount = 0;
    }

    if (HTTP_GET_CLIENT_TASK_MAP (2).HttpProcessingTskId == ProcessingTaskId)
    {
        u4gTaskThreeCount = 0;
    }
#endif
    if (i4Size == HTTP_ZERO)
    {
        return ENM_SUCCESS;
    }
    if (pHttp->fpReleaseContext != NULL)
    {
        pHttp->fpReleaseContext ();
    }
    if (pHttp->fpUnLock != NULL)
    {
        pHttp->fpUnLock ();
    }
    if (pHttp->u1IsMgmtLock)
    {
        MGMT_UNLOCK ();
    }
#ifdef SSL_WANTED

    if (pHttp->pSslConnId != NULL)
    {
        /* This is an HTTPS connection, use SSLWrite */
        while (i4Size > HTTP_MAX_SOC_WRITE_LEN)
        {
            i4WriteLen = HTTP_MAX_SOC_WRITE_LEN;
            if (SslArWrite
                (pHttp->pSslConnId, (UINT1 *) pi1Buffer,
                 (UINT4 *) &i4WriteLen) < HTTP_ZERO)
            {
                break;
            }

            pi1Buffer += i4WriteLen;
            i4Size -= i4WriteLen;
            i4Written += i4WriteLen;
        }

        while (i4Size > HTTP_ZERO)
        {
            i4WriteLen = i4Size;
            if (SslArWrite
                (pHttp->pSslConnId, (UINT1 *) pi1Buffer,
                 (UINT4 *) &i4WriteLen) < HTTP_ZERO)
            {
                break;
            }
            pi1Buffer += i4WriteLen;
            i4Size -= i4WriteLen;
            i4Written += i4WriteLen;
        }
        if (pHttp->fpLock != NULL)
        {
            pHttp->fpLock ();

        }
        if (pHttp->fpSelectContext != NULL)
        {
            if (WebnmApiGetContextId (pHttp, &u4ContextId) != ENM_SUCCESS)
            {
                return ENM_FAILURE;
            }
            pHttp->fpSelectContext (u4ContextId);
        }
        return (i4Written);
    }
    else
#else
    UNUSED_PARAM (i4WriteLen);
#endif

    {
        while (i4Size > HTTP_MAX_SOC_WRITE_LEN)
        {
            i4RetVal = send (pHttp->i4Sockid, pi1Buffer,
                             HTTP_MAX_SOC_WRITE_LEN, MSG_NOSIGNAL);

            if (i4RetVal < HTTP_ZERO)
            {
                if ((errno == EINTR) || (errno == EAGAIN))
                {
                    OsixTskDelay (HTTP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                    continue;
                }
                b1SendFailure = TRUE;
                break;
            }
            if ((i4RetVal == HTTP_ZERO) && (errno == ENOTCONN))
            {
                b1SendFailure = TRUE;
                break;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }

        while (i4Size > HTTP_ZERO)
        {
            i4RetVal = send (pHttp->i4Sockid, pi1Buffer, i4Size, MSG_NOSIGNAL);

            if (i4RetVal < HTTP_ZERO)
            {
                if ((errno == EINTR) || (errno == EAGAIN))
                {
                    OsixTskDelay (HTTP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

#ifdef WLC_WANTED
                    if (HTTP_GET_CLIENT_TASK_MAP (0).HttpProcessingTskId ==
                        ProcessingTaskId)
                    {
                        u4gTaskOneCount++;

                        if (u4gTaskOneCount == 10)
                        {
                            u4gTaskOneCount = 0;
                            b1SendFailure = TRUE;
                            break;
                        }

                    }
                    if (HTTP_GET_CLIENT_TASK_MAP (1).HttpProcessingTskId ==
                        ProcessingTaskId)
                    {

                        u4gTaskTwoCount++;

                        if (u4gTaskTwoCount == 10)
                        {
                            u4gTaskTwoCount = 0;
                            b1SendFailure = TRUE;
                            break;
                        }

                    }
                    if (HTTP_GET_CLIENT_TASK_MAP (2).HttpProcessingTskId ==
                        ProcessingTaskId)
                    {

                        u4gTaskThreeCount++;

                        if (u4gTaskThreeCount == 10)
                        {
                            u4gTaskThreeCount = 0;
                            b1SendFailure = TRUE;
                            break;
                        }

                    }
#endif
                    continue;
                }
                b1SendFailure = TRUE;
                break;
            }

            if ((i4RetVal == HTTP_ZERO) && (errno == ENOTCONN))
            {
                b1SendFailure = TRUE;
                break;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }
        if (pHttp->u1IsMgmtLock)
        {
            MGMT_LOCK ();
        }
        if (pHttp->fpLock != NULL)
        {
            pHttp->fpLock ();
        }
        if (pHttp->fpSelectContext != NULL)
        {
            i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
            if (i4OutCome != ENM_SUCCESS)
            {
                /* The Api checked above always returns the *
                 * context Id value stored for the      *
                 * Particular user.             *
                 * And this is also initialised to          *
                 * Default Context              **/
                return ENM_FAILURE;
            }
            pHttp->fpSelectContext (u4ContextId);
        }
        if (b1SendFailure == TRUE)
        {
            pHttp->bConnClose = TRUE;
            return ENM_FAILURE;
        }
        return (i4Written);
    }
}

/*****************************************************************************/
/* Function Name      : HttpPostMultiDataIndicationToMSR                     */
/*                                                                           */
/* Description        : This function will post the upgradde process to      */
/*                      MSR task when we receive multi data from browser     */
/*                                                                           */
/* Input(s)           : pHttp - HTTP Packet                                  */
/*                      i4ContentLen - Size of the Upgrade file including    */
/*                                     boundary and control information      */
/*                      u4Length  - Length of Main string                    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
HttpPostMultiDataIndicationToMSR (tHttp * pHttp, INT4 i4ContentLen)
{
    tHttpMsgNode       *pHttpMsgNode = NULL;
    tHttp              *pDupHttp = NULL;

    /* We dont Require pu1Buff so not allocating the sub field */
    pDupHttp = (tHttp *) MemAllocMemBlk (gHttpInfo.MsrHttpDupMsgPoolId);
    if (pDupHttp == NULL)
    {
        return;
    }

    pHttpMsgNode =
        (tHttpMsgNode *) MemAllocMemBlk (gHttpInfo.MsrHttpMsgNodePoolId);
    if (pHttpMsgNode == NULL)
    {
        MemReleaseMemBlock (gHttpInfo.MsrHttpDupMsgPoolId, (UINT1 *) pDupHttp);
        return;
    }

    MEMCPY (pDupHttp, pHttp, sizeof (tHttp));

    /*Allocate memory for the buffer */
    pDupHttp->pu1Buff =
        (UINT1 *) MemAllocMemBlk (gHttpInfo.WebnmResponsePoolId);
    if (pDupHttp->pu1Buff == NULL)
    {
        MemReleaseMemBlock (gHttpInfo.MsrHttpDupMsgPoolId, (UINT1 *) pDupHttp);
        MemReleaseMemBlock (gHttpInfo.MsrHttpMsgNodePoolId,
                            (UINT1 *) pHttpMsgNode);
        return;
    }

    MEMSET (pDupHttp->pu1Buff, HTTP_ZERO, WEBNM_MEMBLK_SIZE);
    pDupHttp->pu1BuffCurr = pDupHttp->pu1Buff;

    pHttpMsgNode->pHttp = pDupHttp;
    pHttpMsgNode->i4ContentLen = i4ContentLen;
    pHttpMsgNode->u4IpAddr = gu4DestIpAddr;

    pHttp->i4Sockid = HTTP_INVALID_SOCK_FD;

    if (OsixQueSend (MsrHttpQId, (UINT1 *) &pHttpMsgNode,
                     OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        OsixEvtSend (MsrId, HTTP_MULTI_DATA_EVENT);
    }
    else
    {
        MemReleaseMemBlock (gHttpInfo.MsrHttpDupMsgPoolId, (UINT1 *) pDupHttp);
        MemReleaseMemBlock (gHttpInfo.MsrHttpMsgNodePoolId,
                            (UINT1 *) pHttpMsgNode);
        MemReleaseMemBlock (gHttpInfo.WebnmResponsePoolId,
                            (UINT1 *) pDupHttp->pu1Buff);
        ENM_TRACE (" HTTP Firmware Upgrade Queue SendFailed \r\n");
    }
}

/*****************************************************************************/
/* Function Name      : HttpHandleUpgradeFromMSR                             */
/*                                                                           */
/* Description        : This function will process the upgradde process in   */
/*                      MSR task.                          */
/*                                                                           */
/* Input(s)           : pHttpMsgNode                                         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
HttpHandleUpgradeFromMSR (tHttpMsgNode * pHttpMsgNode)
{
    tHttp              *pTempHttp = NULL;

    pTempHttp = (tHttp *) (VOID *) pHttpMsgNode->pHttp;
    HttpHandleMultiPart ((tHttp *) (VOID *) pHttpMsgNode->pHttp,
                         pHttpMsgNode->i4ContentLen);

    IssProcessConfigStatusPage (pTempHttp);

    if (FlushHttpBuff (pTempHttp) == ENM_FAILURE)
    {
        ENM_TRACE ("FlushHttpBuff failed\r\n");
    }
#ifdef SSL_WANTED
    if (pTempHttp->pSslConnId != NULL)
    {
        SslArClose (pTempHttp->pSslConnId);
    }
#endif

    if (MemReleaseMemBlock (gHttpInfo.WebnmResponsePoolId,
                            (UINT1 *) (pTempHttp->pu1Buff)) != MEM_SUCCESS)
    {
        ENM_TRACE ("MEM release failure\n");
    }

    HttpCloseSocket (pTempHttp->i4Sockid);
    MemReleaseMemBlock (gHttpInfo.MsrHttpDupMsgPoolId,
                        (UINT1 *) pHttpMsgNode->pHttp);
    MemReleaseMemBlock (gHttpInfo.MsrHttpMsgNodePoolId, (UINT1 *) pHttpMsgNode);

}

/*****************************************************************************/
/* Function Name      : HttpHandleMultiPart                                  */
/*                                                                           */
/* Description        : This function is used to handle the MIME messages    */
/*                      (which comes for firmware upgrade via http)          */
/*                                                                           */
/* Input(s)           : pHttp - Pointer to Http Entry                        */
/*                      u4ContentLen - Full packet size including            */
/*                      inbetween boundary lines of MIME message             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS/ENM_FAILURE                              */
/*****************************************************************************/
INT4
HttpHandleMultiPart (tHttp * pHttp, UINT4 u4ContentLen)
{
    UINT1               au1Boundary[HTTP_MAX_BOUNDARY_LEN];
    UINT1               au1Exit[HTTP_MAX_BOUNDARY_LEN];
    UINT1              *pu1Boundary = NULL;
    UINT4               u4MultiDataSize = HTTP_ZERO;
    UINT4               u4MultiDataLen = HTTP_ZERO;
    UINT1              *pu1Line = NULL;
    UINT4               u4Count = HTTP_ZERO;
    UINT4               u4BoundaryNotReached = HTTP_ZERO;
    INT4                i4FileFd = HTTP_INVALID_SOCK_FD;
    INT4                i4Len = HTTP_ZERO;
    UINT1              *pu1Query = NULL;
    UINT1              *pu1File = NULL;
    UINT1               au1Temp[HTTP_FILE_NAME_SIZE];
    UINT1               au1RemoteFileName[HTTP_FILE_NAME_SIZE];
    UINT1               au1LocalFileName[HTTP_FILE_NAME_SIZE];
    UINT1              *pu1Location = NULL;
    UINT1              *pu1FileName = NULL;
    INT4                i4RmReturn;
    pu1Line = allocmem_EnmBlk ();
    if (pu1Line == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1Boundary, HTTP_ZERO, HTTP_MAX_BOUNDARY_LEN);
    MEMSET (au1Temp, HTTP_ZERO, HTTP_FILE_NAME_SIZE);
    MEMSET (au1Exit, HTTP_ZERO, HTTP_MAX_BOUNDARY_LEN);
    MEMSET (pu1Line, HTTP_ZERO, ENM_MAX_NAME_LEN);
    MEMSET (au1LocalFileName, HTTP_ZERO, HTTP_FILE_NAME_SIZE);
    MEMSET (au1RemoteFileName, HTTP_ZERO, HTTP_FILE_NAME_SIZE);

    pu1Boundary = (UINT1 *) STRSTR (pHttp->ai1StaticSockbuffer, "boundary=");
    pu1Boundary += STRLEN ("boundary=");
    for (u4Count = HTTP_ZERO; u4Count < STRLEN (pu1Boundary); u4Count++)
    {
        /* Check for standard boundary end point */
        if (u4Count > HTTP_ONE)
        {
            if ((pu1Boundary[u4Count - 1] == '\n')
                && (pu1Boundary[u4Count - 2] == '\r'))
            {
                break;
            }
        }
        au1Boundary[u4Count] = pu1Boundary[u4Count];
    }

    /* Create Exit criteria boundary which is addition of
     * two hyphens instead of \r\n
     */
    STRCPY (au1Exit, au1Boundary);
    au1Exit[STRLEN (au1Exit) - 1] = '-';
    au1Exit[STRLEN (au1Exit) - 2] = '-';
    IssUpdateDownLoadStatus (MIB_DOWNLOAD_IN_PROGRESS);

    do
    {
        MEMSET (pu1Line, HTTP_ZERO, ENM_MAX_NAME_LEN);
        HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);
        /* Fill the au1PostQuery from each line.
         */

        pu1Query = (UINT1 *) STRSTR (pu1Line, "name=");
        if (pu1Query != NULL)
        {
            SSCANF ((CHR1 *) pu1Query, "name=\"%127s", au1Temp);
            STRNCAT (pHttp->au1PostQuery, au1Temp,
                     MEM_MAX_BYTES (STRLEN (au1Temp),
                                    (ENM_MAX_NAME_LEN -
                                     STRLEN (pHttp->au1PostQuery))));

            pHttp->au1PostQuery[ENM_MAX_NAME_LEN - HTTP_ONE] = '\0';
            /* Replace the " symbol with = symbol
             */
            pHttp->au1PostQuery[STRLEN (pHttp->au1PostQuery) - HTTP_ONE] = '=';

            /* Special case in MIME. 
             * "filename" will come in case of http firmware
             * file upload. Note that this will not be in second
             * or third line. It will be in same line of "name"
             * Then straight forward file data transfer
             */
            if ((pu1File = (UINT1 *) STRSTR (pu1Line, "filename=")) != NULL)
            {
                /* Removal of back slash in this special case */
                pHttp->au1PostQuery[STRLEN (pHttp->au1PostQuery) - 2] = '=';
                pHttp->au1PostQuery[STRLEN (pHttp->au1PostQuery) - 1] = '\0';

                /* File name alone comes in same line.
                 * Fill post query from the same line
                 */

                if ((STRLEN (pu1File) == HTTP_ZERO))
                {
                    IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_FAILED);
                    if (i4FileFd > HTTP_INVALID_SOCK_FD)
                    {
                        FileClose (i4FileFd);
                    }
                    free_EnmBlk ((UINT1 *) pu1Line);
                    return ENM_FAILURE;
                }

                if ((pu1FileName = (UINT1 *) STRRCHR (pu1File, '\\')) != NULL)
                {
                    pu1FileName++;
                    STRCPY (pu1File, pu1FileName);
                }
                else if ((pu1FileName =
                          (UINT1 *) STRRCHR (pu1File, ':')) != NULL)
                {
                    pu1FileName++;
                    STRCPY (pu1File, pu1FileName);
                }
                else if ((pu1FileName =
                          (UINT1 *) STRRCHR (pu1File, '/')) != NULL)
                {
                    pu1FileName++;
                    STRCPY (pu1File, pu1FileName);
                }
                else
                {
                    /*some browsers do not send the full path of the file */
                    /*when there is no path then just move the pointer to get 
                     *                   filename */
                    pu1File = pu1File + STRLEN ("filename=\"");
                }
                STRCAT (pHttp->au1PostQuery, pu1File);
                pHttp->au1PostQuery[STRLEN (pHttp->au1PostQuery) - HTTP_ONE] =
                    '\0';

                if ((pu1Location = (UINT1 *) STRCHR (pu1File, '\"')) != NULL)
                {
                    *pu1Location = '\0';
                    STRCPY (au1RemoteFileName, pu1File);
                }
                /* This line contains data stream code */
                HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);

            }
            else
            {
                /* Generally when a name comes, the next line
                 * will be \r\n and the following line contains 
                 * value
                 */

                /* \r\n line. ignore */
                HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);

                /* Data line */
                MEMSET (pu1Line, HTTP_ZERO, ENM_MAX_NAME_LEN);
                HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);

                /* Avoid \r\n in the postquery. Avoid strcat
                 * if the value not present
                 */
                if (STRCMP (pu1Line, "\r\n") != HTTP_ZERO)
                {
                    pu1Line[STRLEN (pu1Line) - STRLEN ("\r\n")] = '\0';
                    STRCAT (pHttp->au1PostQuery, pu1Line);
                }
            }

            /* if name = ACTION, then it means apply/delete/add
             * button is reached, no need to append "&" 
             */

            if (STRSTR (au1Temp, "ACTION") == NULL)
            {
                /* Delimiter for next elements */
                pHttp->au1PostQuery[STRLEN (pHttp->au1PostQuery)] = '&';
            }
        }

        if (STRSTR (pu1Line, "Content-Type:") != NULL)
        {
            STRCPY (au1LocalFileName, au1RemoteFileName);
            if (i4FileFd > HTTP_INVALID_SOCK_FD)
            {
                FileClose (i4FileFd);
            }

            i4RmReturn = remove ((const CHR1 *) au1LocalFileName);
            UNUSED_PARAM (i4RmReturn);
            i4FileFd = FileOpen (au1LocalFileName,
                                 OSIX_FILE_CR | OSIX_FILE_SY |
                                 OSIX_FILE_AP | OSIX_FILE_WO);

            if (i4FileFd < HTTP_ZERO)
            {
                ENM_TRACE ("Unable to open file \r\n");
                IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_FAILED);
                free_EnmBlk ((UINT1 *) pu1Line);
                return ENM_FAILURE;
            }

            /* Data starts from next line
             * so skip the immediate line and
             * quickly write all the data
             * to a file
             */
            HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);
            while (u4MultiDataSize < u4ContentLen)
            {
                MEMSET (pu1Line, HTTP_ZERO, ENM_MAX_NAME_LEN);
                HttpReadLine (pHttp, (INT1 *) pu1Line, &u4MultiDataLen);

                /* Ensure whether boundary is reached
                 * or not.
                 * Generally boundary will contain
                 * 2 hyphens lesser actual end. So 
                 * Instead of strcmp, use strstr
                 */

                /* If File upload field is the last entry in the html page
                 * au1Exit code will hit. If File upload field is in the
                 * middle of the html page, normal au1Boundary code will hit
                 */
                if ((STRSTR (pu1Line, au1Boundary) != NULL) ||
                    (STRSTR (pu1Line, au1Exit) != NULL))
                {
                    /* Boundary reached, data is copied
                     * continue with any other multi-type object if so
                     */
                    /* Remove the last \r\n */
                    i4Len = FileSize (i4FileFd);

                    /* Sanity Check */
                    if (i4Len == STRLEN ("\r\n"))
                    {
                        ENM_TRACE ("HTTP Upgrade done by empty file \r\n");
                        FileClose (i4FileFd);
                        IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_FAILED);
                        free_EnmBlk ((UINT1 *) pu1Line);
                        return ENM_FAILURE;
                    }

                    FileTruncate (i4FileFd, (i4Len - STRLEN ("\r\n")));
                    FileClose (i4FileFd);
                    i4FileFd = HTTP_INVALID_SOCK_FD;
                    ENM_TRACE ("HTTP Upgrade is over !!!\r\n");

                    IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_SUCCESSFUL);
                    SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, gi4TftpSysLogId,
                                  "Firmware upgrade successful ..!!!"));
                    break;
                }
                else
                {
                    u4BoundaryNotReached++;
                    /* Prevention of attack where by any data without
                     * boundary comes for max retries we come out
                     * of loop to save http task
                     */
                    if (u4BoundaryNotReached > HTTP_UPGRADE_MAX_RETRY)
                    {
                        ENM_TRACE ("Boundary not reached \r\n");
                        FileClose (i4FileFd);
                        IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_FAILED);
                        free_EnmBlk ((UINT1 *) pu1Line);
                        return ENM_FAILURE;
                    }
                }
                if ((i4FileFd != HTTP_INVALID_SOCK_FD) && ((i4Len =
                                                            FileWrite (i4FileFd,
                                                                       (CHR1 *)
                                                                       pu1Line,
                                                                       u4MultiDataLen))
                                                           !=
                                                           (INT4)
                                                           u4MultiDataLen))
                {
                    ENM_TRACE2 ("Error in writing to file:"
                                " Written bytes : %ld Failed bytes : %ld\n",
                                i4Len, (u4MultiDataLen - i4Len));
                    FileClose (i4FileFd);
                    IssUpdateDownLoadStatus (LAST_MIB_DOWNLOAD_FAILED);
                    free_EnmBlk ((UINT1 *) pu1Line);
                    return ENM_FAILURE;
                }
                u4MultiDataSize += u4MultiDataLen;
            }
        }

        /* Exit criteria. Two hypens will follow
         * the boundary before \r\n for the last message
         */
        pu1Boundary = (UINT1 *) STRSTR (pu1Line, au1Exit);
        if (pu1Boundary != NULL)
        {
            break;
        }

    }
    while (STRLEN (pHttp->au1PostQuery) < ENM_MAX_SOC_BUF_LEN);

    if ((i4FileFd >= 0))
    {
        FileClose (i4FileFd);
    }
    free_EnmBlk ((UINT1 *) pu1Line);
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpReadLine                                         */
/*                                                                           */
/* Description        : This function is used to read a line from http data  */
/*                      till \r\n. if \r\n is not found it will read upto    */
/*                      ENM_MAX_SOC_BUF_LEN (1024) bytes                     */
/*                                                                           */
/* Input(s)           : pHttp - Pointer to Http Entry                        */
/*                      pi1Start - Pointer to write the line info            */
/*                                                                           */
/* Output(s)          : pu4Length - Length of the line                       */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS or ENM_FAILURE                           */
/*****************************************************************************/
UINT4
HttpReadLine (tHttp * pHttp, INT1 *pi1Start, UINT4 *pu4Length)
{
    UINT4               u4Len = HTTP_ZERO;

    for (u4Len = HTTP_ZERO; u4Len < ENM_MAX_SOC_BUF_LEN; u4Len++)
    {
        pi1Start[u4Len] = HttpGetChar (pHttp);
        if (pi1Start[u4Len] == ENM_NO_DATA)
        {
            if (pHttp->bIsMultiData == OSIX_FALSE)
            {
                u4Len--;
            }
            continue;
        }
        if ((u4Len != HTTP_ZERO) && (pi1Start[u4Len - HTTP_ONE] == '\r') &&
            (pi1Start[u4Len] == '\n'))
        {
            u4Len++;
            break;
        }
    }
    *pu4Length = u4Len;
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpIssCtrlSockInit                                  */
/*                                                                           */
/* Description        : This function initializes the ISS control socket     */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
HttpIssCtrlSockInit (VOID)
{
    struct sockaddr_in  Addr;
    INT4                i4Flags = HTTP_INVALID_SOCK_FLAG;

    gIssGlbCfg.bCliCtrlSockCreated = ISS_FALSE;
    gIssHttpCfg.i4IssCtrlSockFd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (gIssHttpCfg.i4IssCtrlSockFd < HTTP_ZERO)
    {
        ENM_TRACE ("Unable to create Control socket for ISS\n");
        return ENM_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags =
         fcntl (gIssHttpCfg.i4IssCtrlSockFd, F_GETFL, HTTP_ZERO)) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "GET Failure !!!\n");
        HttpCloseSocket (gIssHttpCfg.i4IssCtrlSockFd);
        return ENM_FAILURE;
    }

    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (gIssHttpCfg.i4IssCtrlSockFd, F_SETFL, i4Flags) < HTTP_ZERO)
    {
        ENM_TRACE ("HttpSockInit: TCP server Fcntl " "SET Failure !!!\n");
        HttpCloseSocket (gIssHttpCfg.i4IssCtrlSockFd);
        return ENM_FAILURE;
    }

    MEMSET (&Addr, HTTP_ZERO, sizeof (Addr));
    Addr.sin_family = AF_INET;
    Addr.sin_addr.s_addr = INADDR_ANY;
    Addr.sin_port = OSIX_HTONS (ISS_CTRL_PORT);

    if (bind (gIssHttpCfg.i4IssCtrlSockFd, (struct sockaddr *) &Addr,
              sizeof (Addr)) != HTTP_ZERO)
    {
        ENM_TRACE ("Unable to bind to port\n");
        HttpCloseSocket (gIssHttpCfg.i4IssCtrlSockFd);
        gIssHttpCfg.i4IssCtrlSockFd = HTTP_INVALID_SOCK_FD;
        return ENM_FAILURE;
    }

    /* Add this SockFd to SELECT library */
    if (HttpSrvSelAddFd (gIssHttpCfg.i4IssCtrlSockFd,
                         HttpSrvCtrlSockCallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (gIssHttpCfg.i4IssCtrlSockFd);
        return ENM_FAILURE;
    }

    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpIssSendCtrlMsg                                   */
/*                                                                           */
/* Description        : This function send ISS control message               */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
HttpIssSendCtrlMsg (tISSCtrlMsg * pIssCtrlMsg)
{
    struct sockaddr_in  Addr;

    if (gIssGlbCfg.bCliCtrlSockCreated == ISS_FALSE)
    {
        gIssGlbCfg.i4CliCtrlSockFd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (gIssGlbCfg.i4CliCtrlSockFd < HTTP_ZERO)
        {
            ENM_TRACE ("Unable to create Control socket for CLI\n");
            return ENM_FAILURE;
        }
        gIssGlbCfg.bCliCtrlSockCreated = ISS_TRUE;
    }

    MEMSET (&Addr, HTTP_ZERO, sizeof (Addr));
    Addr.sin_family = AF_INET;
    Addr.sin_addr.s_addr = OSIX_HTONL (ISS_INADDR_LOOPBACK);
    Addr.sin_port = OSIX_HTONS (ISS_CTRL_PORT);
    if (sendto (gIssGlbCfg.i4CliCtrlSockFd, (UINT1 *) pIssCtrlMsg,
                sizeof (tISSCtrlMsg), HTTP_ZERO, (struct sockaddr *) &Addr,
                sizeof (struct sockaddr_in)) != sizeof (tISSCtrlMsg))
    {
        ENM_TRACE ("sendto failure\n");
        return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpIssProcessCtrlMsgs                               */
/*                                                                           */
/* Description        : This function send ISS control message               */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
VOID
HttpIssProcessCtrlMsgs ()
{
    tISSCtrlMsg         IssCtrlMsg;
    struct sockaddr_in  Addr;
    INT4                i4Len = sizeof (Addr);
    INT4                i4Rcv = HTTP_ZERO;

    /* Read from the Control socket */
    Addr.sin_family = AF_INET;
    Addr.sin_addr.s_addr = INADDR_ANY;
    Addr.sin_port = OSIX_HTONS (ISS_CTRL_PORT);

    MEMSET (&IssCtrlMsg, HTTP_ZERO, sizeof (tISSCtrlMsg));

    i4Rcv = recvfrom (gIssHttpCfg.i4IssCtrlSockFd, &IssCtrlMsg,
                      sizeof (tISSCtrlMsg), HTTP_ZERO,
                      (struct sockaddr *) &Addr, (socklen_t *) & i4Len);

    if (HttpSrvSelAddFd (gIssHttpCfg.i4IssCtrlSockFd,
                         HttpSrvCtrlSockCallBk) == ENM_FAILURE)
    {
        ENM_TRACE ("Http:Unable to add ServerFd Socket to select util\n");
        HttpCloseSocket (gIssHttpCfg.i4IssCtrlSockFd);
        return;
    }

    if (i4Rcv <= HTTP_ZERO)
    {
        ENM_TRACE ("Unable to recv from Control socket\n");
        return;
    }
    switch (IssCtrlMsg.u4MsgType)
    {
        case ISS_CTRL_HTTP_STATUS:
        {
            if (IssCtrlMsg.uMsg.bHttpEnable == ISS_TRUE)
            {
                HttpEnable ();
            }
            else
            {
                HttpDisable ();
            }
            break;
        }
        default:
        {
            break;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : HttpConstructHeader                                  */
/*                                                                           */
/* Description        : This function Constructs the header for the Response */
/*                      generated                                            */
/*                                                                           */
/* Input(s)           : pHttp    - Pointer to Http Entry                     */
/*                      au1Header - Header                                   */
/*                                                                           */
/* Output(s)          : -                                                    */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS/ENM_FAILURE                              */
/*****************************************************************************/
INT4
HttpConstructHeader (tHttp * pHttp, UINT1 *pu1Header, UINT4 u4ObjType)
{
    UINT1               au1Date[ISS_MAX_DATE_LEN];
    UINT4               u4Count = HTTP_ZERO;
    INT4                i4OldLen = HTTP_ZERO;
    INT4                i4Len = HTTP_ZERO;
    INT4                i4Length = HTTP_ZERO;

    static UINT1        au1AuthInfoHeader[ENM_MAX_SOC_BUF_LEN] = { HTTP_ZERO };
    static UINT1        au1WWWAuthHeader[ENM_MAX_SOC_BUF_LEN] = { HTTP_ZERO };

    MEMSET (au1Date, HTTP_ZERO, ISS_MAX_DATE_LEN);

    MEMSET (au1AuthInfoHeader, HTTP_ZERO, sizeof (au1AuthInfoHeader));
    MEMSET (au1WWWAuthHeader, HTTP_ZERO, sizeof (au1WWWAuthHeader));

    /* In cases when the HTTP version of the client is not supported by
     * the server, server sends response that version not supported 
     * with the version sent by the client.
     * This indicates that the server does not support the version sent
     * by the client */

    /* HTTP Header format is as follows 
     ****************************************************************
     HTTP 1.1 200 Ok\r\n
     Date : \r\n
     Content-Type : text/html; content-encoding: ISO-8859-1\r\n
     Cache-Control: no-cache\r\n
     ETAG : 12127178-2344\r\n <optional>
     Content-length : 2344\r\n <optional>
     Transfer-Encoding: chunked \r\n <optional> 
     \r\n
     ****************************************************************/
    /* Append the HTTP Version info */
    if (pHttp->u2RespCode == HTTP_VERSION_NOT_SUPPORTED)
    {
        i4Len = STRLEN (pHttp->ai1Version);
        /* Limit the version of HTTP within 3 bytes. Ex "1.1" */
        if (i4Len <= (INT4) STRLEN (HTTP_VERSION_STRING))
        {
            STRNCPY (pu1Header + i4OldLen, pHttp->ai1Version, i4Len);
            i4OldLen += i4Len;
        }
    }
    else
    {
        i4Len = STRLEN ("HTTP/");
        STRNCPY (pu1Header + i4OldLen, "HTTP/", i4Len);
        i4OldLen += i4Len;
        i4Len = STRLEN (HTTP_VERSION_STRING);
        STRNCPY (pu1Header + i4OldLen, HTTP_VERSION_STRING, i4Len);
        i4OldLen += i4Len;
    }

    i4Len = STRLEN (" ");
    STRNCPY (pu1Header + i4OldLen, " ", i4Len);
    i4OldLen += i4Len;

    /* Append the Response code and response string */
    for (u4Count = HTTP_ZERO;
         aHSReasonPhrase[u4Count].pi1ReasonPhrase != NULL; u4Count++)
    {
        if (aHSReasonPhrase[u4Count].u4Status == pHttp->u2RespCode)
        {
            SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%d %s%s",
                     pHttp->u2RespCode,
                     aHSReasonPhrase[u4Count].pi1ReasonPhrase,
                     HTTP_COMMON_DELIMITER);
            i4OldLen = STRLEN (pu1Header);
            break;
        }
    }

    /* Append the system date to the HTTP Header. The value is same as
     * the "show clock" output */
    i4Len = STRLEN ("Date: ");
    STRNCPY (pu1Header + i4OldLen, "Date: ", i4Len);
    i4OldLen += i4Len;
    HttpGetDate (au1Date, FALSE);
    i4Len = STRLEN (au1Date);
    STRNCPY (pu1Header + i4OldLen, au1Date, i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (HTTP_COMMON_DELIMITER);
    STRNCPY (pu1Header + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
    i4OldLen += i4Len;

    /* Construct the Authentication-Info header for
     * DIGEST Auth scheme if Resp Code is 200 */
    if (pHttp->u2RespCode == HTTP_OK)
    {
        if (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST)
        {
            if (HttpFormAuthInfoHeader (pHttp, au1AuthInfoHeader, &i4Length)
                == ENM_SUCCESS)
            {
                STRNCPY (pu1Header + i4OldLen, au1AuthInfoHeader, i4Length);
                i4OldLen += i4Length;
            }
            else
            {
                ENM_TRACE ("Http: Authentication-Info Header \
                           construction failed\n");
            }

        }
    }

    /* Construct the WWW-Authenticate header for
     * BASIC or DIGEST Auth scheme if Resp Code is 401 */
    if (pHttp->u2RespCode == HTTP_UNAUTHORIZED)
    {
        i4Length = HTTP_ZERO;
        if (HttpFormWWWAuthHeader (pHttp, au1WWWAuthHeader, &i4Length)
            == ENM_SUCCESS)
        {
            STRNCPY (pu1Header + i4OldLen, au1WWWAuthHeader, i4Length);
            i4OldLen += i4Length;
        }
        else
        {
            ENM_TRACE ("Http: WWW-Authenticate Header \
                      construction failed\n");
        }
    }

    /* Content Type of the response is set here */
    i4Len = STRLEN (HTTP_CONTENT_TYPE);
    STRNCPY (pu1Header + i4OldLen, HTTP_CONTENT_TYPE, i4Len);
    i4OldLen += i4Len;

    if ((pHttp->u2ResourceType == HTTP_TEXT_HTML_RESOURCE) ||
        (pHttp->u2ResourceType == HTTP_TEXT_STATIC_HTML_RESOURCE))
    {
        i4Len = STRLEN ("text/html; ");
        STRNCPY (pu1Header + i4OldLen, "text/html; ", i4Len);
    }
    else if (pHttp->u2ResourceType == HTTP_TEXT_JS_RESOURCE)
    {
        i4Len = STRLEN ("text/javascript; ");
        STRNCPY (pu1Header + i4OldLen, "text/javascript; ", i4Len);
    }
    else if (pHttp->u2ResourceType == HTTP_IMG_JPG_RESOURCE)
    {
        i4Len = STRLEN ("image/jpg\r\n");
        STRNCPY (pu1Header + i4OldLen, "image/jpg\r\n", i4Len);
    }
    else if (pHttp->u2ResourceType == HTTP_IMG_GIF_RESOURCE)
    {
        i4Len = STRLEN ("image/gif\r\n");
        STRNCPY (pu1Header + i4OldLen, "image/gif\r\n", i4Len);
    }

    i4OldLen += i4Len;

    /* Charset feild is releveant only for resourses containing text */
    if ((pHttp->u2ResourceType == HTTP_TEXT_JS_RESOURCE) ||
        (pHttp->u2ResourceType == HTTP_TEXT_HTML_RESOURCE) ||
        (pHttp->u2ResourceType == HTTP_TEXT_STATIC_HTML_RESOURCE))

    {
        i4Len = STRLEN ("Charset=ISO-8859-1\r\n");
        STRNCPY (pu1Header + i4OldLen, "Charset=ISO-8859-1\r\n", i4Len);
        i4OldLen += i4Len;
    }

    if (pHttp->u2RespCode == HTTP_FOUND)
    {
        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%s%s", "Location: http://",
                 pHttp->au1RedirectLocation, "/");
        i4OldLen = STRLEN (pu1Header);
        i4Len = STRLEN (HTTP_REQ_MSG_DELIMITER);
        STRNCPY (pu1Header + i4OldLen, HTTP_REQ_MSG_DELIMITER, i4Len);
        i4OldLen += i4Len;
        pHttp->bConnClose = TRUE;
        return i4OldLen;
    }

    if (pHttp->i4ResType == HTTP_DYNAMIC_OBJECT)
    {
        STRCPY (pu1Header + i4OldLen, "Cache-Control: no-cache\r\n");
    }
    else
    {
        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%d%s%s",
                 "Cache-Control: maxage=",
                 HTTP_CACHE_TIMEOUT, ",must-revalidate", HTTP_COMMON_DELIMITER);
        i4OldLen = STRLEN (pu1Header);

        HttpGetDate (au1Date, TRUE);
        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%s%s",
                 "Expires: ", au1Date, HTTP_COMMON_DELIMITER);
        i4OldLen = STRLEN (pu1Header);

        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%s%s", "ETag: ",
                 pHttp->au1Etag, HTTP_COMMON_DELIMITER);
        i4OldLen = STRLEN (pu1Header);
    }

    SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%s",
             "Connnection: Close", HTTP_COMMON_DELIMITER);

    i4OldLen = STRLEN (pu1Header);
    /*  For static objects content length is known. So we append the content
     *  length. and also the header end "\r\n\r\n" . */

    if (u4ObjType == HTTP_STATIC_OBJECT)
    {
        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s%d", HTTP_CONTENT_STRING,
                 pHttp->i4HtmlSize);
        i4OldLen = STRLEN (pu1Header);
        i4Len = STRLEN (HTTP_REQ_MSG_DELIMITER);
        STRNCPY (pu1Header + i4OldLen, HTTP_REQ_MSG_DELIMITER, i4Len);
        i4OldLen += i4Len;
    }
    /* For dynamic objects we do no have content length. We use 
     * chunked encoding. Chunked encoding is used to send dynamic
     * pages where server does not know the len of response before hand.
     * So all the individual chunks are sent out */
    else if (u4ObjType == HTTP_DYNAMIC_OBJECT)
    {
        SPRINTF ((CHR1 *) (pu1Header + i4OldLen), "%s",
                 "Transfer-Encoding: chunked");
        i4OldLen = STRLEN (pu1Header);
        i4Len = STRLEN (HTTP_COMMON_DELIMITER);
        STRNCPY (pu1Header + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
        i4OldLen += i4Len;
    }
    /* For Unmodified objects we just need to finish the header and 
     * nothing more to add. Finish the header with "\r\n" */
    else if (u4ObjType == HTTP_UNMODIFIED_OBJECT)
    {
        i4Len = STRLEN (HTTP_COMMON_DELIMITER);
        STRNCPY (pu1Header + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
        i4OldLen += i4Len;
    }
    return i4OldLen;
}

/****************************************************************/
/*  Function Name   : HttpSendHeader                            */
/*  Description     : This function is used to Construct and    */
/*                    Send the Header                           */
/*  Input(s)        : pHttp    - Pointer to Http Entry          */
/*                    i4ResType - Resource type                 */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpSendHeader (tHttp * pHttp, INT4 i4ResType)
{
    INT4                i4HeaderLen = HTTP_ZERO;
    UINT1              *pu1Header = NULL;

    pu1Header = allocmem_EnmBlk ();
    if (pu1Header == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1Header, HTTP_ZERO, ENM_MAX_NAME_LEN);
    /* All the objects except *.html are static object. 
     * The below check is needed to verify the same */
    if ((pHttp->u2ResourceType != HTTP_TEXT_HTML_RESOURCE) &&
        (i4ResType == HTTP_DYNAMIC_OBJECT))
    {
        i4ResType = HTTP_STATIC_OBJECT;
    }
    /* login.html is an exception in ISS as this is the first page
     * All the requests which are in form "/X.Y" are classified
     * as HTTP Request (Static objects). In the above scenario the 
     * initial page is also classified as HTTP request.
     * To change that we have specific condition. This makes it
     * dynamic object and used chunked encoding when sending response*/
#ifdef WLC_WANTED
    if ((STRCMP (pHttp->ai1HtmlName, "redirect_wss.html") == HTTP_ZERO) ||
        (STRCMP (pHttp->ai1HtmlName, "login_wss.html") == HTTP_ZERO) ||
        (STRCMP (pHttp->ai1HtmlName, "wlan_web_auth_login.html") == HTTP_ZERO))
#else
    if ((STRCMP (pHttp->ai1HtmlName, "redirect.html") == HTTP_ZERO) ||
        (STRCMP (pHttp->ai1HtmlName, "login.html") == HTTP_ZERO))
#endif
    {
        i4ResType = HTTP_DYNAMIC_OBJECT;
    }

    pHttp->i4ResType = i4ResType;
    i4HeaderLen = HttpConstructHeader (pHttp, pu1Header, i4ResType);
    if (SockWrite (pHttp, (INT1 *) pu1Header, i4HeaderLen) == ENM_FAILURE)
    {
        free_EnmBlk ((UINT1 *) pu1Header);
        return ENM_FAILURE;
    }
    free_EnmBlk ((UINT1 *) pu1Header);
    return ENM_SUCCESS;
}

/****************************************************************/
/*  Function Name   : HttpValidateReqHeader                     */
/*  Description     : This function is used to Read and Validate*/
/*                    the HTTP request Header read from socket  */
/*  Input(s)        : pHttp    - Pointer to Http Entry          */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpValidateReqHeader (tHttp * pHttp)
{
    UINT1              *pu1Header = NULL;
    static INT1         ai1OrigHeader[ENM_MAX_SOC_BUF_LEN] = { HTTP_ZERO };
    tSNMP_OCTET_STRING_TYPE Url;
    tHttpRedirect      *pRedirectionInfo = NULL;
    UINT4               u4Address = HTTP_ZERO;
    UINT1              *pu1String = NULL;
    UINT1               au1Url[HTTP_MAX_URL_LEN];

    pu1Header = allocmem_EnmBlk ();
    if (pu1Header == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1Url, HTTP_ZERO, HTTP_MAX_URL_LEN);
    MEMSET (pu1Header, HTTP_ZERO, ENM_MAX_NAME_LEN);
    MEMSET (ai1OrigHeader, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);
    /* Read the complete request from the socket */
    if (HttpReadReqHeader (pHttp, (INT1 *) pu1Header, ai1OrigHeader) ==
        ENM_FAILURE)
    {
        HttpSendError (pHttp, HTTP_BAD_REQUEST);
        free_EnmBlk ((UINT1 *) pu1Header);
        return ENM_FAILURE;
    }
    /* Parse and process the request header */
    if (HttpProcessReqHeader (pHttp, (INT1 *) pu1Header, ai1OrigHeader) ==
        ENM_FAILURE)
    {
        free_EnmBlk ((UINT1 *) pu1Header);
        return ENM_FAILURE;
    }

    if (pHttp->bIsMultiData == OSIX_TRUE)
    {
        free_EnmBlk ((UINT1 *) pu1Header);
        return ENM_SUCCESS;
    }

    /*Check if redirection has to be applied for the URL */
    if (gHttpInfo.bRedirectStat == HTTP_REDIRECT_ENABLE)
    {
        Url.pu1_OctetList = &au1Url[HTTP_ZERO];
        MEMSET (Url.pu1_OctetList, HTTP_ZERO, HTTP_MAX_URL_LEN);
        MEMCPY (Url.pu1_OctetList, pHttp->ai1Url, HTTP_MAX_URL_LEN);
        Url.i4_Length = STRLEN (pHttp->ai1Url);
        HTTP_MGMT_LOCK ();
        pRedirectionInfo = HttpGetRedirectionEntry (&Url);
        HTTP_MGMT_UNLOCK ();

        if (pRedirectionInfo == NULL)
        {
            free_EnmBlk ((UINT1 *) pu1Header);
            return ENM_SUCCESS;
        }
        else
        {
            pHttp->u2RespCode = HTTP_FOUND;
            if (pRedirectionInfo->RedirectedSrvAddr.u1Afi ==
                IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4Address,
                            pRedirectionInfo->RedirectedSrvAddr.au1Addr);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                MEMCPY (pHttp->au1RedirectLocation, pu1String,
                        STRLEN (pu1String));
            }
            else if (pRedirectionInfo->RedirectedSrvAddr.u1Afi ==
                     IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pHttp->au1RedirectLocation,
                        pRedirectionInfo->RedirectedSrvAddr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
            }
            else
            {
                MEMCPY (pHttp->au1RedirectLocation,
                        pRedirectionInfo->ai1RedirectSrvDomainName,
                        pRedirectionInfo->i4DomainLen);
            }

            /*Response for this request is sent below as it is a redirection
             *            in this case there is no need for further processing so we return
             *            failure*/
            free_EnmBlk ((UINT1 *) pu1Header);
            HttpSendHeader (pHttp, HTTP_STATIC_OBJECT);
            SockWrite (pHttp, pHttp->pi1Html, pHttp->i4HtmlSize);
            return ENM_FAILURE;
        }
    }
    free_EnmBlk ((UINT1 *) pu1Header);
    return ENM_SUCCESS;
}

/****************************************************************/
/*  Function Name   : HttpReadReqHeader                         */
/*  Description     : This function is used to Read the Request */
/*                    Header from the socket                    */
/*  Input(s)        : pHttp    - Pointer to Http Entry          */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpReadReqHeader (tHttp * pHttp, INT1 *pi1Header, INT1 *pi1OrigHeader)
{
    INT1                i1Char = HTTP_ZERO;
    INT4                i4CopyLen = HTTP_ZERO;
    INT4                i4HeaderLen = HTTP_ZERO;
    INT4                i4Count = HTTP_ZERO;
    INT1               *pi1EndPtr = NULL;
    UINT1              *pu1Header = NULL;

    pu1Header = allocmem_EnmBlk ();
    if (pu1Header == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pu1Header, HTTP_ZERO, ENM_MAX_NAME_LEN);

    do
    {
        /* Read from socket till the end of the header *
         * The end of the header is marked using "\r\n\r\n" */
        /* First we copy the buffer to the the Temporary header
         * then search for the Delimiter "\r\n\r\n" in the copied buffer.
         * Case 1. Sock buffer consists "\r\n" and remaining "\r\n" is yet to be read
         * Case 2. Sock buffer consists "\r" and remaining "\n\r\n" is yet to be read
         * Case 3. Sock buffer consists "\r\n\r" and remaining "\n" is yet to be read
         * Case 4. Sock buffer does not consist the delimiter and remaining 
         * "\r\n\r\n" is yet to be read
         * To exactly find out the buffer end we need to first copy and then serach 
         * the delimiter. */

        /* checks whether the header length is within the max length */
        if (i4HeaderLen > ENM_MAX_SOC_BUF_LEN)
        {
            free_EnmBlk ((UINT1 *) pu1Header);
            return ENM_FAILURE;
        }
        MEMCPY (pu1Header + i4HeaderLen, pHttp->ai1Sockbuffer,
                ENM_MAX_NAME_LEN - i4HeaderLen);
        if ((pi1EndPtr =
             (INT1 *) STRSTR (pu1Header, HTTP_REQ_MSG_DELIMITER)) != NULL)
        {
            /* Once the delimiter is found we have to 
             * find the end of delimiter in the sock buffer.
             * This is needed for moving the i4Left value so that the corresponding
             * HttpGetChar returns the value after the delimiter.
             * The copy len is calculate using the previous len of the Header
             * and the end of the Delimiter in the copied buffer */
            i4CopyLen = pi1EndPtr + STRLEN (HTTP_REQ_MSG_DELIMITER) -
                (INT1 *) pu1Header - i4HeaderLen;
            pHttp->i4Left = pHttp->i4Read - i4CopyLen;
            i4HeaderLen += i4CopyLen;
            /* Only get requests can be pipelined. Check if the request
             * is pipelined when the request Method is get */
            if ((pHttp->i4Method == HTTP_GET) && (pHttp->i4Left != HTTP_ZERO))
            {
                HttpCheckforPipelinedRequest (pHttp);
            }
            break;
        }
        /* If the request is not received fully then we need to read the
         * remaining reqest from the socket. HttpGetChar reads from 
         * socket when i4Left = HTTP_ZERO. */
        pHttp->i4Left = HTTP_ZERO;
        i4HeaderLen += pHttp->i4Read;

        i1Char = HttpGetChar (pHttp);
        if (i1Char == ENM_NO_DATA)
        {
            continue;
        }
        if (i1Char == -1)
        {
            free_EnmBlk ((UINT1 *) pu1Header);
            return ENM_FAILURE;
        }
    }
    while (HTTP_ONE);

    /* Modify the request header content to small case for easier 
     * processing as the header tags are case insensitive and might change
     * from browser to browser */
    for (i4Count = HTTP_ZERO; i4Count < i4HeaderLen; i4Count++)
    {
        /* Keep a back up of the header in its original case
         * as it is required for processing authentication info 
         * which requires the data in its original case */
        pi1OrigHeader[i4Count] = pu1Header[i4Count];
        pi1Header[i4Count] = (INT1) tolower (pu1Header[i4Count]);
    }

    if (STRSTR (pi1Header, HTTP_CONTENT_STRING) != NULL)
    {
        MEMSET (pHttp->ai1StaticSockbuffer, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);
        MEMCPY (pHttp->ai1StaticSockbuffer, pu1Header, ENM_MAX_NAME_LEN);
    }
    free_EnmBlk ((UINT1 *) pu1Header);
    return ENM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HttpExtractHeaderInfo                                */
/*                                                                           */
/* Description        : This function extracts values from the HTTP header   */
/*                      based on the HTTP Header tag passed by the calling   */
/*                      funtion.Returns ENM_FAILURE if the Tag does not exist*/
/*                                                                           */
/* Input(s)           : pHttp - Pointer to Http Entry                        */
/*                      pu1HeaderFeild - HTTP Header Tag                     */
/*                                                                           */
/* Output(s)          : pu1HeadeValue - Value Extracted from the Header      */
/*                                                                           */
/* Return Value(s)    : ENM_SUCCESS/ENM_FAILURE                              */
/*****************************************************************************/
INT4
HttpExtractHeaderInfo (INT1 *pHeaderBuf, UINT1 *pu1HeaderFeild,
                       UINT1 *pu1HeaderValue)
{
    CHR1               *pi1StartPtr = NULL;
    CHR1               *pi1EndPtr = NULL;

    /* Search for the Header Tag in HTTP Req header */
    pi1StartPtr = STRSTR (pHeaderBuf, pu1HeaderFeild);
    if (pi1StartPtr == NULL)
    {
        return ENM_FAILURE;
    }

    MEMSET (pu1HeaderValue, HTTP_ZERO, ENM_MAX_HEADER_LENGTH);
    pi1StartPtr += STRLEN (pu1HeaderFeild);
    /* The value of the particular tag is extracted till "\r\n" */
    pi1EndPtr = STRSTR (pi1StartPtr, HTTP_COMMON_DELIMITER);
    if (pi1EndPtr == NULL)
    {
        return ENM_FAILURE;
    }
    STRNCPY (pu1HeaderValue, pi1StartPtr, pi1EndPtr - pi1StartPtr);
    return ENM_SUCCESS;
}

/****************************************************************/
/*  Function Name   : HttpProcessReqHeader                      */
/*  Description     : This function is used to parse the Request*/
/*                    Header and validate the header fields     */
/*  Input(s)        : pi1Header  - Req Header content           */
/*                    pi1OrigHeader - Header before case conv.  */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpProcessReqHeader (tHttp * pHttp, INT1 *pi1Header, INT1 *pi1OrigHeader)
{
    UINT1              *pu1ProcessHeader = NULL;
    INT4                i4Index = ENM_FAILURE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index)
        == ENM_FAILURE)
    {
        return ENM_FAILURE;
    }

    pu1ProcessHeader = allocmem_EnmBlk ();
    if (pu1ProcessHeader == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1ProcessHeader, HTTP_ZERO, ENM_MAX_NAME_LEN);

    /* Classify the resource requested depending upon the URL */
    if ((STRSTR (pHttp->ai1Url, HTTP_CSS_EXTENSTION)) != NULL)
    {
        pHttp->u2ResourceType = HTTP_TEXT_STATIC_HTML_RESOURCE;
    }
    else if (((STRSTR (pHttp->ai1Url, HTTP_HTM_EXTENSTION)) != NULL) ||
             ((STRSTR (pHttp->ai1Url, HTTP_HTML_EXTENSTION)) != NULL))
    {
        pHttp->u2ResourceType = HTTP_TEXT_HTML_RESOURCE;
    }
    else if ((STRSTR (pHttp->ai1Url, HTTP_JS_EXTENSTION)) != NULL)
    {
        pHttp->u2ResourceType = HTTP_TEXT_JS_RESOURCE;
    }
    else if ((STRSTR (pHttp->ai1Url, HTTP_JPG_EXTENSTION)) != NULL)
    {
        pHttp->u2ResourceType = HTTP_IMG_JPG_RESOURCE;
    }
    else if ((STRSTR (pHttp->ai1Url, HTTP_GIF_EXTENSTION)) != NULL)
    {
        pHttp->u2ResourceType = HTTP_IMG_GIF_RESOURCE;
    }
    else if ((STRSTR (pHttp->ai1Url, HTTP_INDEX_URL)) != NULL)
    {
        pHttp->u2ResourceType = HTTP_TEXT_HTML_RESOURCE;
    }
    /* The accept field in the HTTP Request Header indicates the clinet
     * capability to receive response. verify if the universak acceptor is 
     * present if not then verify the accept feild for individual resource */
    if (HttpExtractHeaderInfo (pi1Header, HTTP_ACCEPT, pu1ProcessHeader) ==
        ENM_SUCCESS)
    {
        if ((STRSTR (pu1ProcessHeader, HTTP_SUPP_ALL_ACCEPT)) == NULL)

        {
            if ((pHttp->u2ResourceType == HTTP_TEXT_HTML_RESOURCE) ||
                (pHttp->u2ResourceType == HTTP_TEXT_JS_RESOURCE))
            {
                if ((STRSTR (pu1ProcessHeader, HTTP_SUPP_TEXT_ACCEPT)) == NULL)
                {
                    pHttp->u2RespCode = HTTP_RESPONSE_NOT_ACCEPTABLE;
                    ENM_TRACE
                        ("Http: HTTP Response to be generated not acceptable by client\n");
                    HttpSendError (pHttp, HTTP_RESPONSE_NOT_ACCEPTABLE);
                    free_EnmBlk ((UINT1 *) pu1ProcessHeader);
                    return ENM_FAILURE;
                }
            }
            else if ((pHttp->u2ResourceType == HTTP_IMG_GIF_RESOURCE) ||
                     (pHttp->u2ResourceType == HTTP_IMG_JPG_RESOURCE))
            {
                if ((STRSTR (pu1ProcessHeader, HTTP_SUPP_IMAGE_ACCEPT)) == NULL)
                {
                    pHttp->u2RespCode = HTTP_RESPONSE_NOT_ACCEPTABLE;
                    ENM_TRACE
                        ("Http: HTTP Response to be generated not acceptable by client\n");
                    HttpSendError (pHttp, HTTP_RESPONSE_NOT_ACCEPTABLE);
                    free_EnmBlk ((UINT1 *) pu1ProcessHeader);
                    return ENM_FAILURE;
                }
            }
        }
    }

    /* Authorisation header is checked for its presence 
     * If present, it contains the authorisation info of the client
     * which has to be extracted and stored in tHttp struct */
    if (HttpProcessAuthHeader (pHttp, pi1OrigHeader) != ENM_SUCCESS)
    {
        ENM_TRACE ("HTTP: Unable to process Authorisation header");

        HttpSendError (pHttp, pHttp->u2RespCode);
        free_EnmBlk ((UINT1 *) pu1ProcessHeader);
        return ENM_FAILURE;
    }

    /* Conditional get is sent to validate the cache. This is done using
     * E-Tag value. Extract Etag for conditional get requests */
    if (HttpExtractHeaderInfo (pi1Header, HTTP_IF_NONE_MATCH, pu1ProcessHeader)
        == ENM_SUCCESS)
    {
        STRNCPY (pHttp->au1Etag, pu1ProcessHeader, ENM_MAX_ETAG_LEN);
    }
    /* verify the connection close header for the updating the flag */
    if (HttpExtractHeaderInfo (pi1Header, HTTP_CONNECTION, pu1ProcessHeader) ==
        ENM_SUCCESS)
    {
        if ((STRSTR (pu1ProcessHeader, HTTP_CONNECTION_CLOSE)) != NULL)
        {
            pHttp->bConnClose = TRUE;
        }
        else
        {
            pHttp->bConnClose = FALSE;
        }
    }
    /* In case of post requests the Content legth feild is sent */
    /* Extract the same, COntent length is also used for Http Upgrade */
    if (pHttp->i4Method == HTTP_POST)
    {
        if (HttpExtractHeaderInfo
            (pi1Header, HTTP_CONTENT_STRING, pu1ProcessHeader) == ENM_SUCCESS)
        {
            pHttp->i4ContentLen = ATOI (pu1ProcessHeader);
            if (STRSTR (pi1Header, HTTP_CONTENT_TYPE_MULTIPART) != NULL)
            {
                pHttp->bIsMultiData = OSIX_TRUE;
                /* When multipart data is received post event to the MSR */
                HttpPostMultiDataIndicationToMSR (pHttp, pHttp->i4ContentLen);
                free_EnmBlk ((UINT1 *) pu1ProcessHeader);
                return ENM_SUCCESS;
            }

        }
    }
    free_EnmBlk ((UINT1 *) pu1ProcessHeader);
    return ENM_SUCCESS;
}

/****************************************************************/
/*  Function Name   : HttpReadPostQuery                         */
/*  Description     : This function is used to read the post    */
/*                    query from the socket. This function reads*/
/*                    till the HTTP header end                  */
/*  Input(s)        : pi1Header  - Req Header content           */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpReadPostQuery (tHttp * pHttp)
{
    UINT4               u4Len = HTTP_ZERO;
    INT1                i1Char = HTTP_ZERO;

    /* Read HTTP Post Query */
    if (pHttp->i4Method == HTTP_POST)
    {
        for (u4Len = HTTP_ZERO; u4Len < (UINT4) pHttp->i4ContentLen; u4Len++)
        {
            i1Char = HttpGetChar (pHttp);
            if (i1Char == ENM_NO_DATA)
            {
                u4Len--;
                continue;
            }
            GET_CHAR_RETURN_CHECK (i1Char);

            /*Checking length of header */
            if (u4Len < ENM_MAX_NAME_LEN)
            {
                pHttp->au1PostQuery[u4Len] = i1Char;
            }
            if (u4Len == ENM_MAX_NAME_LEN - 1)
            {
                break;
            }
        }
        if (u4Len >= ENM_MAX_NAME_LEN)
        {
            pHttp->au1PostQuery[ENM_MAX_NAME_LEN - 1] = '\0';
        }
        else
        {
            pHttp->au1PostQuery[u4Len] = '\0';
        }
    }
    return ((INT1) HttpGetProtocol (pHttp));
}

/****************************************************************/
/*  Function Name   : HttpInsertChunkSize                       */
/*  Description     : This function is used to insert the chunk */
/*                    size in the HTTP packet                   */
/*  Input(s)        : pHttp - Pointer to Http Entry             */
/*  Output(s)       : None                                      */
/*  Returns         : ENM_SUCCESS/ENM_FAILURE                   */
/****************************************************************/
INT4
HttpInsertChunkSize (tHttp * pHttp, INT1 *pi1Buffer, INT4 i4Size, BOOL1 bFlag)
{
    UINT4               u4ChunkLength = HTTP_ZERO;
    UINT1               au1Tag[HTTP_CHUNK_HEADER_SIZE];
    UINT1              *pu1Line = NULL;

    pu1Line = allocmem_EnmBlk ();
    if (pu1Line == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pu1Line, HTTP_ZERO, ENM_MAX_NAME_LEN);
    MEMSET (au1Tag, HTTP_ZERO, HTTP_CHUNK_HEADER_SIZE);

    u4ChunkLength = pHttp->pu1BuffCurr - pHttp->pu1Buff;
    /* Chunk Header format 
     ***************************************
     HTTP Header
     Chunk Size(Hex Value)\r\n
     Data (len of data is above given)\r\n
     0\r\n
     \r\n
     **************************************/
    if (bFlag == TRUE)
    {
        /* The </html> indicates the ending of the HTML page.
         * So after the end of HTML we have o put Zero chunk
         * Zero chunk indicates end of dynamic data */
        if (i4Size > BUFFER_SIZE)
        {
            i4Size = (INT4) BUFFER_SIZE;
        }
        MEMCPY (pu1Line, pi1Buffer, i4Size);
        pu1Line[i4Size] = '\0';

        if (((STRSTR (pu1Line, "</html>")) != NULL) ||
            ((STRSTR (pu1Line, "</HTML>")) != NULL))
        {
            SPRINTF ((CHR1 *) au1Tag, "%s%s", "0", HTTP_REQ_MSG_DELIMITER);
            u4ChunkLength = STRLEN (au1Tag);
            STRNCPY (pHttp->pu1BuffCurr, au1Tag, u4ChunkLength);
            pHttp->pu1BuffCurr += u4ChunkLength;
        }
        free_EnmBlk ((UINT1 *) pu1Line);
        return ENM_SUCCESS;
    }
    /* Chunk size is relevant only for the HTTP Data.
     * When the function is called for the first time we need to verify if
     * the data is HTTP header. In that case no need to insert chunk header */
    if ((u4ChunkLength == HTTP_ZERO) && ((STRSTR (pi1Buffer, "HTTP/")) != NULL))
    {
        free_EnmBlk ((UINT1 *) pu1Line);
        return ENM_SUCCESS;
    }
    /* Insert the chunk size in hex format followed by the "\r\n" */
    SPRINTF ((CHR1 *) au1Tag, "%x%s", i4Size, HTTP_COMMON_DELIMITER);
    u4ChunkLength = STRLEN (au1Tag);
    STRNCPY (pHttp->pu1BuffCurr, au1Tag, u4ChunkLength);
    pHttp->pu1BuffCurr += u4ChunkLength;
    free_EnmBlk ((UINT1 *) pu1Line);
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpCheckforPipelinedRequest 
 *  Description     : Verify if the request is pipelined or not
 *                    Gets the characters and verifies if one more GET is
 *                    there.
 *  Input           : pHttp - Pointer to the http entry where the socket
 *                    descriptor is stored
 *  Output          : None
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
HttpCheckforPipelinedRequest (tHttp * pHttp)
{
    UINT4               u4Len = HTTP_ZERO;
    UINT4               u4Size = HTTP_ZERO;
    INT1                i1Char = HTTP_ZERO;
    INT1                ai1Line[HTTP_GET_METHOD_SIZE];

    MEMSET (ai1Line, HTTP_ZERO, sizeof (ai1Line));
    /* A request can be pipelined if more than ione GET request are in the same
     * packet. */
    u4Size = STRLEN (HTTP_GET_METHOD);
    for (u4Len = HTTP_ZERO; u4Len <= u4Size; u4Len++)
    {
        i1Char = HttpGetChar (pHttp);
        if (i1Char == ENM_NO_DATA)
        {
            return ENM_FAILURE;
        }
        GET_CHAR_RETURN_CHECK (i1Char);
        ai1Line[u4Len] = i1Char;
    }
    pHttp->i4Left += u4Len;
    if (STRCMP (ai1Line, HTTP_GET_METHOD) == HTTP_ZERO)
    {
        pHttp->bIsReqPipelined = TRUE;
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : SockWriteStatic 
 *  Description     : Function to Write the Data to the client socket
 *                    Static Data is Sent using this function
 *  Input           : pHttp - Pointer to the http entry where the socket
 *                    descriptor is stored
 *                    pi1Buffer - Pointer to the buffer to be written.
 *                    i4Size - Size of the buffer in bytes
 *  Output          : None
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SockWriteStatic (tHttp * pHttp, INT1 *pi1Buffer, INT4 i4Size)
{
    INT4                i4RemSize = HTTP_ZERO;
    /*Checking if there is enough space in the pu1BuffCurr.If adequate 
     * space is not there flush the contents.If i4Size is greater than
     * buffer size then copy buffersize number of bytes into buffer
     * and flush it.
     * When i4Size is less than buffersize copy contents into 
     * the buffer*/
    if ((pHttp->pu1BuffCurr + i4Size - pHttp->pu1Buff) > BUFFER_SIZE)
    {
        /* Fill the buffer till the buffer size and flush the buffer */
        i4RemSize = BUFFER_SIZE + pHttp->pu1Buff - pHttp->pu1BuffCurr;
        if (i4RemSize > HTTP_ZERO)
        {
            MEMCPY (pHttp->pu1BuffCurr, (UINT1 *) pi1Buffer, i4RemSize);
            pHttp->pu1BuffCurr += i4RemSize;
            pi1Buffer += i4RemSize;
            i4Size -= i4RemSize;
        }
        if (FlushHttpBuff (pHttp) == ENM_FAILURE)
        {
            return ENM_FAILURE;
        }
        pHttp->pu1BuffCurr = pHttp->pu1Buff;
        MEMSET (pHttp->pu1BuffCurr, HTTP_ZERO, BUFFER_SIZE);

        while (i4Size > BUFFER_SIZE)
        {
            MEMCPY (pHttp->pu1BuffCurr, (UINT1 *) pi1Buffer, BUFFER_SIZE);
            pHttp->pu1BuffCurr += BUFFER_SIZE;
            pi1Buffer += BUFFER_SIZE;
            i4Size -= BUFFER_SIZE;
            if (FlushHttpBuff (pHttp) == ENM_FAILURE)
            {
                return ENM_FAILURE;
            }
            pHttp->pu1BuffCurr = pHttp->pu1Buff;
            MEMSET (pHttp->pu1BuffCurr, HTTP_ZERO, BUFFER_SIZE);
        }
    }
    if (i4Size > HTTP_ZERO)
    {
        MEMCPY (pHttp->pu1BuffCurr, (UINT1 *) pi1Buffer, i4Size);
        pHttp->pu1BuffCurr += i4Size;
    }
    return ENM_SUCCESS;
}

/****************************************************************/
/*  Function Name   : IssGetDate                                */
/*  Description     : This function is used to get the date     */
/*                                                              */
/*  Input(s)        :                                           */
/*  Output(s)       : None                                      */
/*  Returns         : ISS_SUCCESS/ISS_FAILURE                   */
/****************************************************************/
INT4
HttpGetDate (UINT1 *au1FullDate, INT1 i1FutureFlag)
{
    tUtlTm              tm;
    UINT1               au1Hrs[3], au1Min[3], au1Sec[3], au1Day[3], au1Mon[3],
        au1Year[5], au1Month[4], au1Wday[3], au1WeekDay[5];
    UINT4               u4Secs = HTTP_ZERO;
    UINT4               u4Ticks = HTTP_ZERO;

    MEMSET (&tm, HTTP_ZERO, sizeof (tUtlTm));
    MEMSET (au1FullDate, HTTP_ZERO, ISS_MAX_DATE_LEN);
    MEMSET (au1Hrs, HTTP_ZERO, 3);
    MEMSET (au1Min, HTTP_ZERO, 3);
    MEMSET (au1Sec, HTTP_ZERO, 3);
    MEMSET (au1Day, HTTP_ZERO, 3);
    MEMSET (au1Mon, HTTP_ZERO, 3);
    MEMSET (au1Year, HTTP_ZERO, 5);
    MEMSET (au1Month, HTTP_ZERO, 4);
    MEMSET (au1Wday, HTTP_ZERO, 3);
    MEMSET (au1WeekDay, HTTP_ZERO, sizeof (au1WeekDay));

    u4Secs = UtlGetTimeInSecs ();
    if (i1FutureFlag == TRUE)
    {
        u4Secs += HTTP_CACHE_TIMEOUT;
    }
    u4Ticks = u4Secs * gu4Stups;
    UtlGetTimeForTicks (u4Ticks, &tm);
    tm.tm_mon++;
    tm.tm_wday++;
    SNPRINTF ((CHR1 *) au1Wday, 3, "0%u", tm.tm_wday);

    if (tm.tm_mon <= 9)
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "0%u", tm.tm_mon);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "%u", tm.tm_mon);
    }
    if (tm.tm_mday <= 9)
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "0%u", tm.tm_mday);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "%u", tm.tm_mday);
    }

    if (tm.tm_hour <= 9)
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "0%u", tm.tm_hour);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "%u", tm.tm_hour);
    }

    if (tm.tm_min <= 9)
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "0%u", tm.tm_min);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "%u", tm.tm_min);
    }

    if (tm.tm_sec <= 9)
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "0%u", tm.tm_sec);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "%u", tm.tm_sec);
    }
    SNPRINTF ((CHR1 *) au1Year, 5, "%u", tm.tm_year);

    switch (tm.tm_mon)
    {
        case 1:
            STRCPY (au1Month, "Jan");
            break;
        case 2:
            STRCPY (au1Month, "Feb");
            break;
        case 3:
            STRCPY (au1Month, "Mar");
            break;
        case 4:
            STRCPY (au1Month, "Apr");
            break;
        case 5:
            STRCPY (au1Month, "May");
            break;
        case 6:
            STRCPY (au1Month, "Jun");
            break;
        case 7:
            STRCPY (au1Month, "Jul");
            break;
        case 8:
            STRCPY (au1Month, "Aug");
            break;
        case 9:
            STRCPY (au1Month, "Sep");
            break;
        case 10:
            STRCPY (au1Month, "Oct");
            break;
        case 11:
            STRCPY (au1Month, "Nov");
            break;
        case 12:
            STRCPY (au1Month, "Dec");
            break;

        default:
            break;
    }

    switch (tm.tm_wday)
    {
        case 1:
            STRCPY (au1WeekDay, "Sun,");
            break;
        case 2:
            STRCPY (au1WeekDay, "Mon,");
            break;
        case 3:
            STRCPY (au1WeekDay, "Tue,");
            break;
        case 4:
            STRCPY (au1WeekDay, "Wed,");
            break;
        case 5:
            STRCPY (au1WeekDay, "Thu,");
            break;
        case 6:
            STRCPY (au1WeekDay, "Fri,");
            break;
        case 7:
            STRCPY (au1WeekDay, "Sat,");
            break;
        default:
            break;
    }

    SPRINTF ((CHR1 *) au1FullDate, "%s %s %s %s %s:%s:%s",
             au1WeekDay, au1Day, au1Month, au1Year, au1Hrs, au1Min, au1Sec);
    return (ENM_SUCCESS);
}

/*********************************************************************
 *  Function Name : HttpAddRedirectionEntry
 *  Description   : This function adds the Redirection Entry to DLL
 *
 *  Input(s)      : RedirectionURL - Redirection URL Index
 *  Output(s)     : pRedirectLink  - Redirection Entry
 *  Return Values : None
 *
 *********************************************************************/
INT4
HttpAddRedirectionEntry (tSNMP_OCTET_STRING_TYPE * pRedirectionURL)
{
    tHttpRedirect      *pRedirectLink = NULL;
    tHttpRedirect      *pTempRedirectLink = NULL;
    tHttpRedirect      *pNewRedirectLink = NULL;
    UINT1              *pu1MemAlloc = NULL;
    INT4                i4Flag = HTTP_ZERO;

    if (TMO_DLL_Count (&(gHttpInfo.HttpRedirectList)) ==
        MAX_HTTP_REDIRECTION_ENTRY_BLOCKS)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&(gHttpInfo.HttpRedirectList), pRedirectLink, tHttpRedirect *)
    {
        if (MEMCMP (pRedirectLink->ai1RedirectedUrl,
                    pRedirectionURL->pu1_OctetList, pRedirectLink->i4UrlLen)
            > HTTP_ZERO)
        {
            i4Flag = TRUE;
            break;
        }
        pTempRedirectLink = pRedirectLink;
    }

    pu1MemAlloc = MemAllocMemBlk (gHttpInfo.HttpRedirectMemPoolId);

    if (pu1MemAlloc == NULL)
    {
        return SNMP_FAILURE;
    }
    pNewRedirectLink = (tHttpRedirect *) (VOID *) pu1MemAlloc;
    MEMSET (pNewRedirectLink, HTTP_ZERO, sizeof (tHttpRedirect));

    TMO_DLL_Init_Node (&pNewRedirectLink->NextRedirectNode);
    MEMCPY (pNewRedirectLink->ai1RedirectedUrl, pRedirectionURL->pu1_OctetList,
            pRedirectionURL->i4_Length);

    pNewRedirectLink->i4UrlLen = pRedirectionURL->i4_Length;
    pNewRedirectLink->i4RowStatus = HTTP_NOT_IN_SERVICE;

    if ((i4Flag == TRUE) && (pTempRedirectLink != NULL))
    {
        TMO_DLL_Insert_In_Middle (&(gHttpInfo.HttpRedirectList),
                                  &pTempRedirectLink->NextRedirectNode,
                                  &pNewRedirectLink->NextRedirectNode,
                                  &pRedirectLink->NextRedirectNode);
    }
    else
    {
        TMO_DLL_Add (&(gHttpInfo.HttpRedirectList),
                     &pNewRedirectLink->NextRedirectNode);
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : HttpGetRedirectionEntry
 *  Description   : This function gets the Redirection Entry from DLL
 *
 *  Input(s)      : RedirectionURL - Redirection URL Index
 *  Output(s)     : pRedirectLink  - Redirection Entry
 *  Return Values : None
 *
 *********************************************************************/
tHttpRedirect      *
HttpGetRedirectionEntry (tSNMP_OCTET_STRING_TYPE * pRedirectionURL)
{
    tHttpRedirect      *pRedirectLink = NULL;

    TMO_DLL_Scan (&(gHttpInfo.HttpRedirectList), pRedirectLink, tHttpRedirect *)
    {
        if (MEMCMP (pRedirectLink->ai1RedirectedUrl,
                    pRedirectionURL->pu1_OctetList, pRedirectLink->i4UrlLen)
            == HTTP_ZERO)
        {
            return pRedirectLink;
        }
    }
    return NULL;
}

/*********************************************************************
**  Function Name : HttpChildProcessIndex
**  Description   : This function get the index of the given task
**
**  Input(s)      : SelfTaskId
**  Output(s)     : None
**  Return Values : Gives the Index of the dgiven task
**
**********************************************************************/
INT4
HttpChildProcessIndex (tOsixTaskId ProcessingTaskId, INT4 *i4Index)
{
    INT4                i4Count = HTTP_ZERO;
    for (i4Count = HTTP_ZERO; i4Count < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
         i4Count++)
    {
        if (ProcessingTaskId == gHttpInfo.HttpProcTaskId[i4Count])
        {
            *i4Index = i4Count;
            return ENM_SUCCESS;
        }
    }
    return ENM_FAILURE;
}

/************************************************************************
 * Function Name : HttpGenerateDigest
 * Description   : The function generates the Request Digest for 
 *                 the authentication of the client or 
 *                 the Response Digest to be sent in the 200 OK message. 
 *                 These digest generations are required for the 
 *                 DIGEST Authentication scheme.
 * Input(s)      : pHttp
 *                 u1DigestFlag: 
 *                 IF u1DigestFlag == 0 then calculate RequestDigest
                   IF u1DigestFlag == 1 then calculate ResponseDigest
 * Output(s)     : pu1Digest
 * Return Values : Return ENM_SUCCESS / ENM_FAILURE
 *************************************************************************/
INT1
HttpGenerateDigest (tHttp * pHttp, UINT1 *pu1Digest, UINT1 u1DigestFlag)
{
    static UINT1        au1HA1[ENM_MAX_HA1_LEN + HTTP_FOUR + HTTP_ONE] =
        { '\0' };
    static UINT1        au1HA2[ENM_MAX_HA2_LEN + HTTP_FOUR] = { '\0' };
    static UINT1        au1Temp[ENM_MAX_HA2_LEN + HTTP_FOUR] = { '\0' };

    static unArCryptoHash Md5Ctx;
    static UINT1        au1A2[ENM_MAX_URL_LEN +
                              ENM_MAX_METHOD_LEN + HTTP_FOUR] = { '\0' };
    static UINT1        au1Request[sizeof (au1HA1) + sizeof (au1HA2) +
                                   ENM_MAX_NONCE_LEN + ENM_MAX_CNONCE_LEN +
                                   ENM_MAX_NC_LEN + ENM_MAX_QOP_LEN +
                                   HTTP_FOUR] = { '\0' };

    MEMSET (&Md5Ctx, HTTP_ZERO, sizeof (unArCryptoHash));
    MEMSET (au1Temp, HTTP_ZERO, sizeof (au1Temp));
    MEMSET (au1A2, HTTP_ZERO, sizeof (au1A2));
    MEMSET (au1HA1, HTTP_ZERO, sizeof (au1HA1));
    MEMSET (au1HA2, HTTP_ZERO, sizeof (au1HA2));
    MEMSET (au1Request, HTTP_ZERO, sizeof (au1Request));

    if ((pHttp == NULL) || (pu1Digest == NULL))
    {
        ENM_TRACE ("Http: NULL Argument received \n");
        return ENM_FAILURE;
    }

    if (((STRCMP (pHttp->au1Algorithm, "MD5") == HTTP_ZERO) ||
         (STRLEN (pHttp->au1Algorithm) == HTTP_ZERO)) &&
        (pHttp->au1HA1Digest[HTTP_ZERO] != '\0'))
    {
        STRNCPY (au1HA1, pHttp->au1HA1Digest, ENM_MAX_HA1_LEN + HTTP_FOUR);
    }
    else
    {
        ENM_TRACE ("Http: HTTP Algorithm is not supported by server \n");
        return ENM_FAILURE;
    }

    /* Generate A2 value */
    if (u1DigestFlag == ENM_RESPONSE_DIGEST)
    {
        STRNCPY (au1A2, ":", HTTP_ONE);
        STRNCPY (au1A2 + STRLEN (":"),
                 pHttp->au1Uri, (sizeof (au1A2)) - HTTP_ONE);
    }
    else if ((u1DigestFlag == ENM_REQUEST_DIGEST) &&
             (pHttp->i4Method == HTTP_POST))
    {
        STRNCPY (au1A2, "POST", STRLEN ("POST"));
        STRNCPY (au1A2 + STRLEN ("POST"), ":", HTTP_ONE);
        STRNCPY (au1A2 + STRLEN ("POST") + HTTP_ONE,
                 pHttp->au1Uri, STRLEN (pHttp->au1Uri));

    }
    else if ((u1DigestFlag == ENM_REQUEST_DIGEST) &&
             (pHttp->i4Method == HTTP_GET))
    {
        STRNCPY (au1A2, "GET", STRLEN ("GET"));
        STRNCPY (au1A2 + STRLEN ("GET"), ":", HTTP_ONE);
        STRNCPY (au1A2 + STRLEN ("GET") + HTTP_ONE,
                 pHttp->au1Uri, STRLEN (pHttp->au1Uri));
    }
    else
    {
        ENM_TRACE ("Http: The Url method or \
                      the Digest flag's value is incorrect \n");
        return ENM_FAILURE;
    }

    /* Compute HA2 */
    MEMSET (au1Temp, HTTP_ZERO, sizeof (au1Temp));
    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, (UINT1 *) au1A2, STRLEN (au1A2));
    arMD5_finish (&Md5Ctx, au1Temp);
    UtlAuthConvertMd5HashToHex (au1Temp, au1HA2);

    /* Calculation of Request/Response Digest */
    if (STRLEN (pHttp->au1ResponseQop) == HTTP_ZERO)
    {
        STRNCPY (au1Request, au1HA1, (sizeof (au1Request)) - HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) + HTTP_ONE,
                 pHttp->au1Snonce, STRLEN (pHttp->au1Snonce));
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) + HTTP_ONE +
                 STRLEN (pHttp->au1Snonce) + HTTP_ONE, au1HA2, STRLEN (au1HA2));
    }
    else if (STRCMP (pHttp->au1ResponseQop, "auth") == HTTP_ZERO)
    {
        STRNCPY (au1Request, au1HA1, (sizeof (au1Request)) - HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) + HTTP_ONE,
                 pHttp->au1Snonce, STRLEN (pHttp->au1Snonce));
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE,
                 pHttp->au1NonceCount, STRLEN (pHttp->au1NonceCount));
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount) + HTTP_ONE,
                 pHttp->au1Cnonce, STRLEN (pHttp->au1Cnonce));
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount) + HTTP_ONE +
                 STRLEN (pHttp->au1Cnonce), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount) + HTTP_ONE +
                 STRLEN (pHttp->au1Cnonce) + HTTP_ONE,
                 pHttp->au1ResponseQop, STRLEN (pHttp->au1ResponseQop));
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount) + HTTP_ONE +
                 STRLEN (pHttp->au1Cnonce) + HTTP_ONE +
                 STRLEN (pHttp->au1ResponseQop), ":", HTTP_ONE);
        STRNCPY (au1Request + STRLEN (au1HA1) +
                 HTTP_ONE + STRLEN (pHttp->au1Snonce) + HTTP_ONE +
                 STRLEN (pHttp->au1NonceCount) + HTTP_ONE +
                 STRLEN (pHttp->au1Cnonce) + HTTP_ONE +
                 STRLEN (pHttp->au1ResponseQop) + HTTP_ONE,
                 au1HA2, STRLEN (au1HA2));
    }
    else
    {
        ENM_TRACE ("Http: The QoP value is not \
                      auth \n");
        return ENM_FAILURE;
    }

    /*Compute RequestDigest */
    MEMSET (au1Temp, HTTP_ZERO, sizeof (au1Temp));
    arMD5_start (&Md5Ctx);
    arMD5_update (&Md5Ctx, (UINT1 *) au1Request, STRLEN (au1Request));
    arMD5_finish (&Md5Ctx, au1Temp);
    UtlAuthConvertMd5HashToHex (au1Temp, pu1Digest);
    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpGarbageNonceCollector
 * Description   : This function cleans up the global storage of Nonce
 *                 values by setting those values to zero when Nonce
 *                 time expires
 * Input(s)      : None
 * Output(s)     : None
 * Return Values : None
 *********************************************************************/
VOID
HttpGarbageNonceCollector (VOID)
{
    tUtlSysPreciseTime  sysPreciseTime = { HTTP_ZERO, HTTP_ZERO, HTTP_ZERO };
    UINT1               u1Index = HTTP_ZERO;
    UINT4               u4CurrentTimeSecs = HTTP_ZERO;

    MEMSET (&sysPreciseTime, HTTP_ZERO, sizeof (tUtlSysPreciseTime));

    /* Get time since epoch */
    UtlGetPreciseSysTime (&sysPreciseTime);
    u4CurrentTimeSecs = sysPreciseTime.u4Sec;

    HTTP_LOCK ();
    for (u1Index = HTTP_ZERO; u1Index < HTTP_MAX_CLIENT_CONNECTIONS; u1Index++)
    {
        /* Reset the expired Nonce values */
        if ((INT4)
            (u4CurrentTimeSecs - gaHttpServerNonceInfo[u1Index].u4TimeInSecs)
            > HTTP_NONCE_EXPIRE_TIME)
        {
            MEMSET (&gaHttpServerNonceInfo[u1Index], HTTP_ZERO,
                    sizeof (tHttpServerNonceInfo));
        }
    }
    HTTP_UNLOCK ();
}

/*********************************************************************
 * Function Name : HttpSendEventToHttpTask
 * Description   : This function when invoked sends an event to the  
 *                 Http Module.
 * Input(s)      : u4Event - The event to be sent.
 * Output(s)     : None
 * Return Values : ENM_SUCCESS/ENM_FAILURE
 *********************************************************************/
INT4
HttpSendEventToHttpTask (UINT4 u4Event)
{
    OsixEvtSend (gHttpInfo.HttpMainTskId, u4Event);
    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpFormAuthInfoHeader
 * Description   : This function when invoked constructs the   
 *                 authentication-info header for digest
 *                 authentication scheme.
 * Input(s)      : pHttp - Http structure.
 * Output(s)     : pu1AuthInfoHeader - constructed header
 *                 pi4Length - length of the constructed header
 * Return Values : ENM_SUCCESS/ENM_FAILURE
 *********************************************************************/
INT4
HttpFormAuthInfoHeader (tHttp * pHttp, UINT1 *pu1AuthInfoHeader,
                        INT4 *pi4Length)
{
    INT4                i4Len = HTTP_ZERO;
    INT4                i4OldLen = HTTP_ZERO;

    /***************************************************
    * Eg:- Authentication-Info header for DIGEST scheme
    *      ---------------------------------------------
    *    HTTP/1.1 200 OK
    *    Authentication-Info: 
    *    rspauth="5e1200f55411077cc19335979302708a", 
    *    cnonce="c963ae1ee969e9d9", 
    *    nc=00000001, 
    *    qop=auth\r\n                             
    ***************************************************/

    i4Len = STRLEN ("Authentication-Info: ");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "Authentication-Info: ", i4Len);
    i4OldLen += i4Len;

    MEMSET (pHttp->au1Digest, HTTP_ZERO, STRLEN (pHttp->au1Digest));
    if (HttpGenerateDigest (pHttp,
                            pHttp->au1Digest,
                            ENM_RESPONSE_DIGEST) == ENM_FAILURE)
    {
        ENM_TRACE ("Unable to calculate Response Digest\n");
        return ENM_FAILURE;
    }

    i4Len = STRLEN ("rspauth=\"");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "rspauth=\"", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (pHttp->au1Digest);
    STRNCPY (pu1AuthInfoHeader + i4OldLen, pHttp->au1Digest, i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("\",");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "\",", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("cnonce=\"");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "cnonce=\"", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (pHttp->au1Cnonce);
    STRNCPY (pu1AuthInfoHeader + i4OldLen, pHttp->au1Cnonce, i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("\",");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "\",", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("nc=\"");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "nc=\"", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (pHttp->au1NonceCount);
    STRNCPY (pu1AuthInfoHeader + i4OldLen, pHttp->au1NonceCount, i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("\",");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "\",", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("qop=\"");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "qop=\"", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (pHttp->au1ResponseQop);
    STRNCPY (pu1AuthInfoHeader + i4OldLen, pHttp->au1ResponseQop, i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN ("\"");
    STRNCPY (pu1AuthInfoHeader + i4OldLen, "\"", i4Len);
    i4OldLen += i4Len;

    i4Len = STRLEN (HTTP_COMMON_DELIMITER);
    STRNCPY (pu1AuthInfoHeader + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
    i4OldLen += i4Len;

    *pi4Length = STRLEN (pu1AuthInfoHeader);

    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpFormWWWAuthHeader
 * Description   : This function when invoked constructs the   
 *                 WWW-Authenticate header for basic and digest
 *                 authentication schemes.
 * Input(s)      : pHttp - Http structure.
 * Output(s)     : pu1WWWAuthHeader - constructed header
 *                 pi4Length - length of the constructed header
 * Return Values : ENM_SUCCESS/ENM_FAILURE
 *********************************************************************/
INT4
HttpFormWWWAuthHeader (tHttp * pHttp, UINT1 *pu1WWWAuthHeader, INT4 *pi4Length)
{
    INT4                i4Len = HTTP_ZERO;
    INT4                i4OldLen = HTTP_ZERO;
    UINT1               u1Index = HTTP_ZERO;
    UINT4               u4TimeForNonce = HTTP_ZERO;
    UINT1               au1StaleFlag[ENM_MAX_STALEFLG_LEN] = { HTTP_ZERO };
    UINT1               au1Nonce[ENM_MAX_NONCE_LEN] = { HTTP_ZERO };
    CHR1                au1TimeForMD5Calc[ENM_MAX_TIME_LEN] = { HTTP_ZERO };

    static UINT1        au1EncodedString[ENM_MAX_BASE64_LEN] = { HTTP_ZERO };
    static tUtlSysPreciseTime sysPreciseTime =
        { HTTP_ZERO, HTTP_ZERO, HTTP_ZERO };
    static unArCryptoHash Md5Ctx;

    MEMSET (&Md5Ctx, HTTP_ZERO, sizeof (unArCryptoHash));
    MEMSET (au1StaleFlag, HTTP_ZERO, ENM_MAX_STALEFLG_LEN);
    MEMSET (au1TimeForMD5Calc, HTTP_ZERO, ENM_MAX_TIME_LEN);
    MEMSET (&sysPreciseTime, HTTP_ZERO, sizeof (tUtlSysPreciseTime));
    MEMSET (au1Nonce, HTTP_ZERO, ENM_MAX_NONCE_LEN);
    MEMSET (au1EncodedString, HTTP_ZERO, ENM_MAX_BASE64_LEN);

    if (pHttp->i4AuthScheme == HTTP_AUTH_DIGEST)
    {
       /***************************************************
        * Eg:- WWW-Authenticate header for DIGEST scheme
        *      -----------------------------------------
        *      HTTP/1.1 401 Unauthorized
        *      WWW-Authenticate: Digest
        *      realm="ISS",
        *      qop="auth,auth-int",
        *      algorithm=MD5,
        *      nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
        *      stale="FALSE"
        ***************************************************/

        i4Len = STRLEN ("WWW-Authenticate: Digest realm=\"");
        STRNCPY (pu1WWWAuthHeader + i4OldLen,
                 "WWW-Authenticate: Digest realm=\"", i4Len);
        i4OldLen += i4Len;

        /* Add realm */
        i4Len = STRLEN (pHttp->au1Realm);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, pHttp->au1Realm, i4Len);
        i4OldLen += i4Len;

        i4Len = STRLEN ("\",");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "\",", i4Len);
        i4OldLen += i4Len;

        /* Add QoP */
        i4Len = STRLEN ("qop=\"auth\",");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "qop=\"auth\",", i4Len);
        i4OldLen += i4Len;

        /* Add Algorithm */
        i4Len = STRLEN ("algorithm=MD5,");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "algorithm=MD5,", i4Len);
        i4OldLen += i4Len;

        /* Generate Nonce = Hex(MD5(Time:ETag:Secret)) */
        i4Len = STRLEN ("nonce=\"");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "nonce=\"", i4Len);
        i4OldLen += i4Len;

        /* Get time since epoch */
        UtlGetPreciseSysTime (&sysPreciseTime);

        /* Time for nonce computation */
        u4TimeForNonce = sysPreciseTime.u4Sec;
        SPRINTF (au1TimeForMD5Calc, "%u", u4TimeForNonce);

        /*Compute Nonce and convert it to HEX */
        arMD5_start (&Md5Ctx);
        arMD5_update (&Md5Ctx, (UINT1 *) au1TimeForMD5Calc,
                      STRLEN (au1TimeForMD5Calc));
        arMD5_update (&Md5Ctx, (UINT1 *) ":", HTTP_ONE);
        arMD5_update (&Md5Ctx, (UINT1 *) pHttp->au1Etag,
                      STRLEN (pHttp->au1Etag));
        arMD5_update (&Md5Ctx, (UINT1 *) ":", HTTP_ONE);
        arMD5_update (&Md5Ctx, (UINT1 *) gaHTTP_NONCE_SECRET,
                      STRLEN (gaHTTP_NONCE_SECRET));
        arMD5_finish (&Md5Ctx, au1Nonce);

        UtlAuthConvertMd5HashToHex (au1Nonce, au1EncodedString);

        /* Prune the Global Nonce Array */
        HttpGarbageNonceCollector ();

        /* Add this newly generated Nonce to the Global Nonce array */
        HTTP_LOCK ();
        for (u1Index = HTTP_ZERO;
             u1Index < HTTP_MAX_CLIENT_CONNECTIONS; u1Index++)
        {
            if (STRLEN (gaHttpServerNonceInfo[u1Index].au1Nonce) == HTTP_ZERO)
            {
                STRNCPY (gaHttpServerNonceInfo[u1Index].au1Nonce,
                         au1EncodedString, ENM_MAX_NONCE_LEN);
                gaHttpServerNonceInfo[u1Index].u4TimeInSecs = u4TimeForNonce;
                break;
            }
        }
        HTTP_UNLOCK ();

        i4Len = STRLEN (au1EncodedString);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, au1EncodedString, i4Len);
        i4OldLen += i4Len;

        i4Len = STRLEN ("\",");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "\",", i4Len);
        i4OldLen += i4Len;

        /* Add Stale flag */
        SNPRINTF ((CHR1 *) au1StaleFlag, ENM_MAX_STALEFLG_LEN,
                  "stale=\"%s\"", pHttp->au1Stale);
        i4Len = STRLEN (au1StaleFlag);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, au1StaleFlag, i4Len);
        i4OldLen += i4Len;

        i4Len = STRLEN (HTTP_COMMON_DELIMITER);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
        i4OldLen += i4Len;

    }

    else if (pHttp->i4AuthScheme == HTTP_AUTH_BASIC)
    {
       /***************************************************
        *  Eg:- WWW-Authenticate header for BASIC scheme
        *       ----------------------------------------
        *       HTTP/1.1 401 Unauthorized
        *       WWW-Authenticate: Basic realm="ISS"
        ***************************************************/

        i4Len = STRLEN ("WWW-Authenticate: Basic realm=\"");
        STRNCPY (pu1WWWAuthHeader + i4OldLen,
                 "WWW-Authenticate: Basic realm=\"", i4Len);
        i4OldLen += i4Len;

        /* Add realm */
        i4Len = STRLEN (pHttp->au1Realm);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, pHttp->au1Realm, i4Len);
        i4OldLen += i4Len;

        i4Len = STRLEN ("\"");
        STRNCPY (pu1WWWAuthHeader + i4OldLen, "\"", i4Len);
        i4OldLen += i4Len;

        i4Len = STRLEN (HTTP_COMMON_DELIMITER);
        STRNCPY (pu1WWWAuthHeader + i4OldLen, HTTP_COMMON_DELIMITER, i4Len);
        i4OldLen += i4Len;
    }

    *pi4Length = STRLEN (pu1WWWAuthHeader);
    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpProcessAuthHeader
 * Description   : This function when invoked parses, validates and   
 *                 stores the various directives of the authorisation
 *                 header for basic and digest authentication schemes.
 * Input(s)      : pHttp - Http structure.
 *                 pi1OrigHeader - The header which has to be searched
 *                 for extraction.
 * Output(s)     : 
 * Return Values : ENM_SUCCESS/ENM_FAILURE
 *********************************************************************/
INT4
HttpProcessAuthHeader (tHttp * pHttp, INT1 *pi1OrigHeader)
{
    static UINT1        au1HeaderValue[ENM_MAX_SOC_BUF_LEN] = { HTTP_ZERO };
    static UINT1        au1FormattedValue[ENM_MAX_SOC_BUF_LEN] = { HTTP_ZERO };
    static UINT1        au1Base64DecodedStr[ENM_MAX_NAME_LEN] = { HTTP_ZERO };
    static UINT1        au1RequestUri[ENM_MAX_URL_LEN + HTTP_ONE] =
        { HTTP_ZERO };

    CHR1               *pc1RestOfStr = NULL;
    CHR1               *pc1Username = NULL;
    UINT1               u1Index = HTTP_ZERO;
    UINT1               u1RealmNumber = HTTP_ZERO;
    UINT4               u4BasicLength = HTTP_ZERO;
    UINT4               u4DecodeStrlength = HTTP_ZERO;

    MEMSET (au1HeaderValue, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);
    MEMSET (au1FormattedValue, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);
    MEMSET (au1Base64DecodedStr, HTTP_ZERO, ENM_MAX_NAME_LEN);
    MEMSET (au1RequestUri, '\0', ENM_MAX_URL_LEN + HTTP_ONE);

    /* sanity check */
    if ((pHttp == NULL) || (pi1OrigHeader == NULL))
    {
        ENM_TRACE ("Null value received \n");
        return ENM_FAILURE;
    }

    if (HttpExtractHeaderInfo (pi1OrigHeader,
                               gaHTTP_AUTHORISATION,
                               au1HeaderValue) == ENM_SUCCESS)
    {
        STRNCPY (pHttp->ai1Authorisation, au1HeaderValue, ENM_MAX_NAME_LEN);

        /* If Digest authorisation header is received */
        if ((STRSTR (au1HeaderValue, gaHTTP_DIGEST)) != NULL)
        {
            /* Store the authentication scheme used in the incoming
             * request */
            pHttp->i4AuthScheme = HTTP_AUTH_DIGEST;

            /* Extract Username */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_USERNAME,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->ai1Username,
                             au1FormattedValue, ENM_MAX_USERNAME_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of username \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }
            }
            else
            {
                ENM_TRACE ("Http: Missing username directive \n");
                pHttp->u2RespCode = HTTP_BAD_REQUEST;

                return ENM_FAILURE;
            }

            /* Extract Realm */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_REALM,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1Realm,
                             au1FormattedValue, ENM_MAX_REALM_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of realm \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

                /* Check the realm's value and store the corresponding 
                 * enum value to be used to find the HA1. */
                if (STRCMP (pHttp->au1Realm, gaHTTP_REALM_ISS) == HTTP_ZERO)
                {
                    u1RealmNumber = ENM_REALM_ISS;
                }
                else if (STRCMP (pHttp->au1Realm, gaHTTP_REALM_TIMEOUT)
                         == HTTP_ZERO)
                {
                    u1RealmNumber = ENM_REALM_TIMEOUT;
                }
                else if (STRCMP (pHttp->au1Realm, gaHTTP_REALM_WRONG_LOGIN)
                         == HTTP_ZERO)
                {
                    u1RealmNumber = ENM_REALM_WRONG_LOGIN;
                }
                else
                {
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4WebSysLogId,
                                  "WEBNM: Improper realm in HTTP \
                      Digest Authentication scheme"));
                    return ENM_FAILURE;
                }

            }
            else
            {
                ENM_TRACE ("Http: Missing realm directive \n");
                pHttp->u2RespCode = HTTP_BAD_REQUEST;

                return ENM_FAILURE;
            }

            /* Extract server nonce */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_NONCE,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1Snonce,
                             au1FormattedValue, ENM_MAX_NONCE_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of server nonce \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

            }
            else
            {
                ENM_TRACE ("Http: Missing server nonce directive \n");
                pHttp->u2RespCode = HTTP_BAD_REQUEST;

                return ENM_FAILURE;
            }

            /* Extract algorithm */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_ALGORITHM,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1Algorithm,
                             au1FormattedValue, ENM_MAX_ALGO_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of algorithm \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

                if (STRCMP (pHttp->au1Algorithm, "MD5") != HTTP_ZERO)
                {
                    ENM_TRACE ("Http: The algorithm directive \
                       has incorrect value. Expected value is MD5\n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }
            }

            /* Extract uri in incoming Authorisation header */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_URI,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1Uri, au1FormattedValue, ENM_MAX_URL_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of url \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }
            }
            else
            {
                ENM_TRACE ("Http: Missing digest-uri directive \n");
                pHttp->u2RespCode = HTTP_BAD_REQUEST;

                return ENM_FAILURE;
            }

            /* The Request-URI is obtained by combining the 
             * Url and the PostQuery */
            if (pHttp->au1PostQuery[HTTP_ZERO] != '\0')
            {
                SNPRINTF ((CHR1 *) au1RequestUri, ENM_MAX_URL_LEN, "%s?%s",
                          pHttp->ai1Url, pHttp->au1PostQuery);
            }
            else
            {
                STRNCPY (au1RequestUri, pHttp->ai1Url, ENM_MAX_URL_LEN);
            }

            /* NOTE:- This snippet of code will be enabled, when the IE is upgraded
             * to a later version. The current version of IE (IE6) 
             * has a bug */
            /* VALIDATION: Check if the uri is equal to the Request-URI,
             * if not send error 
             if ( STRCMP (pHttp->au1Uri, au1RequestUri) != HTTP_ZERO )
             {
             pHttp->u2RespCode = HTTP_BAD_REQUEST;

             ENM_TRACE ("Http: Mismatch in Request-URI and Digest-URI \
             in HTTP Digest Authentication scheme \n");
             return ENM_FAILURE;
             }*/

            /* Extract qop */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_QOP,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1ResponseQop,
                             au1FormattedValue, ENM_MAX_QOP_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of qop \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

            }
            /* VALIDATION: If the QoP directive is not present,
             * return error as we have supplied QoP in WWW-Authenticate */
            else
            {
                pHttp->u2RespCode = HTTP_BAD_REQUEST;
                ENM_TRACE ("Http: QoP directive is missing\n");

                return ENM_FAILURE;
            }

            /* VALIDATION: Check if the QoP value is equal to auth,
             * if not send error as we have supplied QoP=auth in 
             * WWW-Authenticate header*/
            if (STRCMP (pHttp->au1ResponseQop, "auth") != HTTP_ZERO)
            {
                pHttp->u2RespCode = HTTP_BAD_REQUEST;
                ENM_TRACE
                    ("Http: QoP directive has incorrect value. Expected value \
                 is auth\n");

                return ENM_FAILURE;
            }

            /* Extract nc */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_NC,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1NonceCount,
                             au1FormattedValue, ENM_MAX_NC_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of nc\n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

            }
            /* VALIDATION: If the nc directive is not present,
             * return error as we have supplied QoP in WWW-Authenticate */
            else
            {
                pHttp->u2RespCode = HTTP_BAD_REQUEST;
                ENM_TRACE ("Http: nc directive is missing\n");

                return ENM_FAILURE;
            }

            /* Extract cnonce in incoming Authorisation header */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_CNONCE,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1Cnonce,
                             au1FormattedValue, ENM_MAX_CNONCE_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of cnonce \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

            }
            /* VALIDATION: If the cnonce directive is not present,
             * return error as we have supplied QoP in WWW-Authenticate */
            else
            {
                pHttp->u2RespCode = HTTP_BAD_REQUEST;
                ENM_TRACE ("Http: cnonce directive is missing\n");

                return ENM_FAILURE;
            }

            /* Extract response in incoming Authorisation header */
            if (HttpExtractHeaderInfo (pi1OrigHeader,
                                       gaHTTP_RESPONSE,
                                       au1HeaderValue) == ENM_SUCCESS)
            {
                if (HttpFormatExtractedStr (au1HeaderValue, au1FormattedValue)
                    == ENM_SUCCESS)
                {
                    STRNCPY (pHttp->au1RequestDigest,
                             au1FormattedValue, ENM_MAX_DIGEST_LEN);
                }
                else
                {
                    ENM_TRACE ("Http: Illegal format of response \n");
                    pHttp->u2RespCode = HTTP_BAD_REQUEST;

                    return ENM_FAILURE;
                }

            }
            else
            {
                ENM_TRACE ("Http: Missing response directive \n");
                pHttp->u2RespCode = HTTP_BAD_REQUEST;

                return ENM_FAILURE;
            }

            /* For the Username specified by the client, fetch and store
             * the corresponding HA1 digest from the memory*/
            for (u1Index = HTTP_ZERO; u1Index < HTTP_MAX_NO_OF_USERS; u1Index++)
            {
                if (STRCMP (gaWebnmDigestUser[u1Index].ai1Uname,
                            pHttp->ai1Username) == HTTP_ZERO)
                {
                    STRNCPY (pHttp->au1HA1Digest,
                             gaWebnmDigestUser[u1Index].au1HA1[u1RealmNumber],
                             ENM_MAX_DIGEST_LEN);
                    STRNCPY (pHttp->ai1Password,
                             gaWebnmDigestUser[u1Index].ai1Pwd,
                             ENM_MAX_PASSWORD_LEN);
                    break;
                }
            }

            /* If username is not found in memory,
             * reject the request */
            if (pHttp->au1HA1Digest[HTTP_ONE] == '\0')
            {
                ENM_TRACE ("Http: Incorrect username \n");
                STRNCPY (pHttp->au1Realm,
                         gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);
                pHttp->u2RespCode = HTTP_UNAUTHORIZED;
                return ENM_FAILURE;
            }
        }
        /* If Basic authorisation header is received */
        else if ((STRSTR (au1HeaderValue, gaHTTP_BASIC)) != NULL)
        {
            /* Store the authentication scheme used in the incoming
             * request */
            pHttp->i4AuthScheme = HTTP_AUTH_BASIC;

            u4BasicLength = STRLEN (gaHTTP_BASIC);
            u4DecodeStrlength = STRLEN (au1HeaderValue + u4BasicLength);

            /* Decode the Base64 encoded string */
            UtlBase64DecodeString (au1HeaderValue + u4BasicLength,
                                   au1Base64DecodedStr, &u4DecodeStrlength);

            /* Split the username and password and store */
            pc1Username = STRTOK_R (au1Base64DecodedStr, ":", &pc1RestOfStr);

            if ((pc1Username == NULL) ||
                (pc1RestOfStr == NULL) || (pc1RestOfStr[HTTP_ZERO] == '\0'))
            {
                pHttp->u2RespCode = HTTP_UNAUTHORIZED;
                ENM_TRACE ("Http: Credentials are not entered properly");
                ENM_TRACE ("or Either of the username or password is blank\n");

                STRNCPY (pHttp->au1Realm,
                         gaHTTP_REALM_WRONG_LOGIN, ENM_MAX_REALM_LEN);

                return ENM_FAILURE;
            }
            STRNCPY (pHttp->ai1Username, pc1Username, ENM_MAX_USERNAME_LEN);
            STRNCPY (pHttp->ai1Password, pc1RestOfStr, ENM_MAX_PASSWORD_LEN);
        }
    }
    /* If the authorisation header is not present, 
     * save the value of the flag as Default */
    else
    {
        pHttp->i4AuthScheme = HTTP_AUTH_DEFAULT;
    }

    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpFormatExtractedStr
 * Description   : This function strips the unwanted quotes and comma   
 *                 from the input string.
 * Input(s)      : pu1InputString - The unformatted string
 * Output(s)     : pu1FormattedString - The formatted output string 
 * Return Values : ENM_SUCCESS/ENM_FAILURE
 *********************************************************************/
INT4
HttpFormatExtractedStr (UINT1 *pu1InputString, UINT1 *pu1FormattedString)
{
    static UINT1        au1HeaderValue[ENM_MAX_SOC_BUF_LEN] = { '\0' };
    CHR1               *pc1EndQuotePtr = NULL;

    MEMSET (pu1FormattedString, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);
    MEMSET (au1HeaderValue, HTTP_ZERO, ENM_MAX_SOC_BUF_LEN);

    STRNCPY (au1HeaderValue, pu1InputString, ENM_MAX_SOC_BUF_LEN - HTTP_ONE);

    /* Stripping the " " from "input str" --> input str */
    if (au1HeaderValue[HTTP_ZERO] == '"')
    {
        if ((pc1EndQuotePtr = STRCHR (au1HeaderValue + HTTP_ONE, '"')) != NULL)
        {
            *pc1EndQuotePtr = '\0';
        }
        else if ((pc1EndQuotePtr =
                  STRCHR (au1HeaderValue + HTTP_ONE, ',')) != NULL)
        {
            *pc1EndQuotePtr = '\0';
        }
        else
        {
            ENM_TRACE ("Incorrect data in authorisation header \n");
            return ENM_FAILURE;
        }
        STRNCPY (pu1FormattedString,
                 au1HeaderValue + HTTP_ONE, ENM_MAX_SOC_BUF_LEN);
    }
    /* Stripping the , at the end of input str */
    else
    {
        pc1EndQuotePtr = STRCHR (au1HeaderValue, ',');
        if (pc1EndQuotePtr != NULL)
        {
            *pc1EndQuotePtr = '\0';
        }
        else
        {
            ENM_TRACE ("Incorrect data in authorisation header \n");
            return ENM_FAILURE;
        }
        STRNCPY (pu1FormattedString, au1HeaderValue, ENM_MAX_SOC_BUF_LEN);
    }

    return ENM_SUCCESS;
}

/*********************************************************************
 * Function Name : HttpSrvUpdateSelFd
 * Description   : This function adds the ipv4/ipv6 SockFd to SELECT   
 *                 library.
 * Input(s)      : None.
 * Output(s)     : None.
 * Return Values : None.
 *********************************************************************/
VOID
HttpSrvUpdateSelFd (VOID)
{
    /* Add ipv4 SockFd to SELECT library */
    if (gHttpInfo.i4SrvSockId != HTTP_INVALID_SOCK_FD)
    {
        if (HttpSrvSelAddFd (gHttpInfo.i4SrvSockId,
                             HttpSrvSockCallBk) == ENM_FAILURE)
        {
            ENM_TRACE ("Http:Unable to add ipv4 ServerFd Socket \
                        to select util\n");
            HttpCloseSocket (gHttpInfo.i4SrvSockId);
        }

    }

    /* Add ipv6 SockFd to SELECT library */
    if (gHttpInfo.i4SrvSock6Id != HTTP_INVALID_SOCK_FD)
    {
        if (HttpSrvSelAddFd (gHttpInfo.i4SrvSock6Id,
                             HttpSrvSock6CallBk) == ENM_FAILURE)
        {
            ENM_TRACE ("Http:Unable to add ipv6 ServerFd Socket \
                        to select util\n");
            HttpCloseSocket (gHttpInfo.i4SrvSock6Id);
        }
    }
    return;
}

#ifdef WLC_WANTED
/* Added for web authentication */
/*********************************************************************
 * Function Name : WebnmGetWebAuthClientInfo
 * Description   : Get the client IP address from the socket ID   
 * Input(s)      : Socket ID.
 * Output(s)     : Client MAC address
 * Return Values : None.
 *********************************************************************/
UINT4
WebnmGetWebAuthClientInfo (INT4 i4Sockid)
{

    UINT4               ClientAddr = 0;
    INT4                i4Counter = 0;
    /* Get the ClientAddr based on the scokId */
    for (i4Counter = HTTP_ZERO;
         i4Counter < HTTP_MAX_CLIENT_CONNECTIONS; i4Counter++)
    {
        if (gHttpInfo.ai4ConnClients[i4Counter] == i4Sockid)
        {
            ClientAddr = OSIX_NTOHL (gHttpInfo.au4ConnClientsIp[i4Counter]);
            break;
        }
    }
    if (i4Counter >= HTTP_MAX_CLIENT_CONNECTIONS)
    {
#ifdef WLC_WANTED
        HTTP_DBG_TRC2 (HTTP_TRC,
                       "Http: Max Client Connections reached, Count = %d, Sock =  %d\n",
                       i4Counter, i4Sockid);
#endif
    }
    /* Get the MAC Address based on the IP address */
    return ClientAddr;
}

/*********************************************************************
 * Function Name : WebnmGetWebAuthClientMac
 * Description   : Get the client IP address from the socket ID   
 * Input(s)      : Socket ID.
 * Output(s)     : Client MAC address
 * Return Values : None.
 *********************************************************************/
UINT4
WebnmGetWebAuthClientMac (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                          void *pArg, void *pOut)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaWepProcessDB = (tWssStaWepProcessDB *) pRBElem;

            if (pWssStaWepProcessDB->u4StationIpAddress == *((UINT4 *) pArg))
            {
                MEMCPY ((UINT1 *) pOut, pWssStaWepProcessDB->stationMacAddress,
                        sizeof (tMacAddr));
            }
        }
    }

    return RB_WALK_CONT;
}

/*********************************************************************
 * Function Name : WebnmIsWebAuthEnabled
 * Description   : Checks whether WEB Authentication is enabled for a 
 *                 particular WLAN    
 * Input(s)      : Client MAC address.
 * Output(s)     : Enabled/Disabled. 
 * Return Values : None.
 *********************************************************************/

UINT1
WebnmIsWebAuthEnabled (UINT1 *au1DestMacAddr)
{
    tWssifauthDBMsgStruct WssStaMsg;
    tWssWlanDB          wssWlanDB;
    UINT1               u1WebAuthEnabled = OSIX_FALSE;

    MEMSET (&WssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    MEMCPY (WssStaMsg.WssIfAuthStateDB.stationMacAddress,
            au1DestMacAddr, MAC_ADDR_LEN);

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  &WssStaMsg) != OSIX_FAILURE)
    {
        wssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
            WssStaMsg.WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;
        wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                      &wssWlanDB) != OSIX_FAILURE)
        {
            wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                          &wssWlanDB) != OSIX_FAILURE)
            {
                if (wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus == TRUE)
                {
                    /* WebAuthentication not Enabled for this wlan */
                    u1WebAuthEnabled = OSIX_TRUE;
                }
            }
        }
    }

    return u1WebAuthEnabled;
}

/*********************************************************************
 * Function Name : WebnmWlanIfIndex
 * Description   : Checks whether WEB Authentication is enabled for a 
 *                 particular WLAN    
 * Input(s)      : Client MAC address.
 * Output(s)     : Enabled/Disabled. 
 * Return Values : None.
 *********************************************************************/

UINT4
WebnmWlanIfIndex (UINT1 *au1DestMacAddr)
{
    tWssifauthDBMsgStruct *pWssStaMsg = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4WlanIfId = 0;
    pWssStaMsg =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssStaMsg != NULL)
    {
        pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pwssWlanDB != NULL)
        {
            MEMSET (pWssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));
            MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
            MEMCPY (pWssStaMsg->WssIfAuthStateDB.stationMacAddress,
                    au1DestMacAddr, MAC_ADDR_LEN);

            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                          pWssStaMsg) != OSIX_FAILURE)
            {
                pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                    pWssStaMsg->WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;
                pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              pwssWlanDB) != OSIX_FAILURE)
                {
                    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB)
                        != OSIX_FAILURE)
                    {
                        u4WlanIfId =
                            pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
                    }
                }
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        }
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
    }
    return u4WlanIfId;
}

/*********************************************************************
 * Function Name : WebnmIsStaAuthorized 
 * Description   : Checks whether the STA has got authorized.    
 * Input(s)      : Client MAC address.
 * Output(s)     : Enabled/Disabled. 
 * Return Values : None.
 *********************************************************************/
UINT1
WebnmIsStaAuthorized (UINT1 *au1DestMacAddr)
{
    tWssifauthDBMsgStruct WssStaMsg;
    UINT1               u1StaAuthorized = OSIX_FALSE;

    MEMSET (&WssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));

    MEMCPY (WssStaMsg.WssIfAuthStateDB.stationMacAddress,
            au1DestMacAddr, MAC_ADDR_LEN);

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  &WssStaMsg) != OSIX_FAILURE)
    {
        if (WssStaMsg.WssIfAuthStateDB.u1StaAuthFinished == OSIX_TRUE)
        {
            u1StaAuthorized = OSIX_TRUE;
        }
    }

    return u1StaAuthorized;
}

#endif

#ifdef WLC_WANTED
/*****************************************************************************
 *
 * Function     : WssStaAssembleWebRespPkts
 *
 * Description  : Function to assemble the Web Authentication response pkts.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS, if initialization succeeds
 *                OSIX_FAILURE, otherwise
 *
 *****************************************************************************/
INT4
WebnmStaAssembleWebRespPkts (tHttp * pHttp, INT1 i1Message)
{
#ifdef ISS_WANTED
    INT4                i4Count = HTTP_ZERO;
    tUtlInAddr          IpAddress;
    UINT1              *pu1String = NULL;
    UINT4               u4Counter = 0;
    INT1                i1FirstLogin = OSIX_FALSE;
#endif
#ifdef IP6_WANTED
    tUtlIn6Addr         In6Addr;
#endif

    if (WEBNM_HTTP_LOGIN_REQ == OSIX_TRUE)
    {
#ifdef ISS_WANTED

        if (STRCMP (pHttp->ai1HtmlName, "index.html") == HTTP_ZERO)
        {
            /* ISS Login page can be viewed in multiple scenarios
             * but syslog message should be sent during establishment of 
             * connection to ISS. During this connection establishment 
             * "i1Message" value will be '2' and the flag 1FirstLogin 
             * is introduced to identify the connection establishment */
            if (i1Message == HTTP_TWO)
            {
                i1FirstLogin = OSIX_TRUE;
            }
        }

        /* Send login.html page when the authentication scheme 
         * default */
        if (pHttp->i4AuthScheme == HTTP_AUTH_DEFAULT)
        {
            if (STRCMP (pHttp->ai1HtmlName, "index.html") == HTTP_ZERO)
            {
                for (i4Count = HTTP_ZERO;
                     wlhtml[i4Count].pi1Name != NULL; i4Count++)
                {
                    if (STRCMP (wlhtml[i4Count].pi1Name,
                                "wlan_web_auth_login.html") == HTTP_ZERO)
                    {
                        /* Initially when the client requests for the 
                         * login page, the i1Message is always passed 
                         * as 2 which denotes "Session TimeOut." 
                         * Also the ai1HtmlName is always passed 
                         * as "index.html" which doesnt exist. 
                         * Hence to override this problem we do a 
                         * STRCMP to check whether its the request 
                         * for the login page and change 
                         * the i1Message value to 0. */

                        HTTP_STRCPY (pHttp->ai1HtmlName,
                                     "wlan_web_auth_login.html");
                        pHttp->pi1Html = (INT1 *) wlhtml[i4Count].pi1Ptr;
                        pHttp->i4HtmlSize = wlhtml[i4Count].i4Size;

                        pHttp->u2RespCode = HTTP_OK;
                        HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT);
#ifdef WLC_WANTED
                        IssProcessWebAuthLoginPageGet (pHttp);
#endif
                        /*IssSendLoginPage (pHttp, i1Message); */

                        for (u4Counter = HTTP_ZERO;
                             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                             u4Counter++)
                        {
                            if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                            {
                                IpAddress.u4Addr =
                                    gIpv4Sec[u4Counter].ClientAddr.
                                    sin_addr.s_addr;
                                pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
                            }
                        }
#ifdef IP6_WANTED
                        for (u4Counter = HTTP_ZERO;
                             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                             u4Counter++)
                        {
                            if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                            {
                                MEMCPY (In6Addr.u1addr,
                                        gIpv6Sec[u4Counter].ClientV6Addr.
                                        sin6_addr.s6_addr,
                                        sizeof (In6Addr.u1addr));
                                pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
                            }
                        }
#endif
                        if ((i1FirstLogin == OSIX_TRUE) &&
                            (pHttp->u1RequestType == SSL_REQTYPE))
                        {
                            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                                          (UINT4) gi4WebSysLogId,
                                          "WEBNM: HTTPS session established "
                                          "successfully from %s ", pu1String));
                        }
                        return OSIX_SUCCESS;
                    }
                }
            }
            else
            {

                for (i4Count = HTTP_ZERO;
                     wlhtml[i4Count].pi1Name != NULL; i4Count++)
                {

                    if (STRCMP (wlhtml[i4Count].pi1Name,
                                "wlan_web_auth_login.html") == HTTP_ZERO)
                    {
                        /* Initially when the client requests for the 
                         * login page, the i1Message is always passed 
                         * as 2 which denotes "Session TimeOut." 
                         * Also the ai1HtmlName is always passed 
                         * as "index.html" which doesnt exist. 
                         * Hence to override this problem we do a 
                         * STRCMP to check whether its the request 
                         * for the login page and change 
                         * the i1Message value to 0. */

                        HTTP_STRCPY (pHttp->ai1HtmlName,
                                     "wlan_web_auth_login.html");

                        pHttp->pi1Html = (INT1 *) wlhtml[i4Count].pi1Ptr;
                        pHttp->i4HtmlSize = wlhtml[i4Count].i4Size;

                        pHttp->u2RespCode = HTTP_OK;
                        HttpSendHeader (pHttp, HTTP_DYNAMIC_OBJECT);
#ifdef WLC_WANTED
                        if (pHttp->i4Method == ISS_GET)
                        {
                            IssProcessWebAuthLoginPageGet (pHttp);
                        }
                        else if (pHttp->i4Method == ISS_SET)
                        {
                            IssProcessWebAuthLoginPageSet (pHttp);
                        }
#endif
                        /*IssSendLoginPage (pHttp, i1Message); */

                        for (u4Counter = HTTP_ZERO;
                             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                             u4Counter++)
                        {
                            if (gIpv4Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                            {
                                IpAddress.u4Addr =
                                    gIpv4Sec[u4Counter].ClientAddr.sin_addr.
                                    s_addr;
                                pu1String = (UINT1 *) UtlInetNtoa (IpAddress);
                            }
                        }
#ifdef IP6_WANTED
                        for (u4Counter = HTTP_ZERO;
                             u4Counter < HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT;
                             u4Counter++)
                        {
                            if (gIpv6Sec[u4Counter].i4SockFd == pHttp->i4Sockid)
                            {
                                MEMCPY (In6Addr.u1addr,
                                        gIpv6Sec[u4Counter].ClientV6Addr.
                                        sin6_addr.s6_addr,
                                        sizeof (In6Addr.u1addr));
                                pu1String = (UINT1 *) UtlInetNtoa6 (In6Addr);
                            }
                        }
#endif
                        if ((i1FirstLogin == OSIX_TRUE) &&
                            (pHttp->u1RequestType == SSL_REQTYPE))
                        {
                            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                                          (UINT4) gi4WebSysLogId,
                                          "WEBNM: HTTPS session established "
                                          "successfully from %s ", pu1String));
                        }
                        return OSIX_SUCCESS;
                    }
                }
            }
        }
#endif
    }

    return OSIX_FAILURE;
}
#endif

#endif
