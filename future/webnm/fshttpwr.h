/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttpwr.h,v 1.3 2013/02/12 12:09:38 siva Exp $
*
* Description: Proto types for Wrapper Routines
*********************************************************************/

#ifndef _FSHTTPWR_H
#define _FSHTTPWR_H

VOID RegisterFSHTTP(VOID);

VOID UnRegisterFSHTTP(VOID);
INT4 FsHttpRedirectionStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsOperHttpAuthSchemeGet(tSnmpIndex *, tRetVal *);
INT4 FsConfigHttpAuthSchemeGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestCountGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsConfigHttpAuthSchemeSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestCountSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestDiscardsSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRequestDiscardsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsConfigHttpAuthSchemeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsConfigHttpAuthSchemeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHttpRequestCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHttpRequestDiscardsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsHttpRedirectionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsHttpRedirectedSrvAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvIPGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvDomainNameGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvIPSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvDomainNameSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvIPTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectedSrvDomainNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHttpRedirectionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSHTTPWR_H */
