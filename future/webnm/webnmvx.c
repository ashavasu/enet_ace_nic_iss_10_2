/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: webnmvx.c,v 1.3 2007/02/01 15:09:14 iss Exp $
 *
 * Description: Function releated to struct timeval  
 *******************************************************************/

#include <time.h>
#include "enm.h"

/*********************************************************************
*  Function Name : WebnmGetTime
*  Description   : This will return a Current Time 
*  Input(s)      : None. 
*  Output(s)     : None. 
*  Return Values : None.
*********************************************************************/

UINT4
WebnmGetTime ()
{
    UINT4               u4Time;
    tUtlTm              tm;

    UtlGetTime (&tm);
    u4Time = (tm.tm_mday * 24 * 3600) + (tm.tm_hour * 3600) +
        (tm.tm_min * 60) + tm.tm_sec;

    return u4Time;
}
