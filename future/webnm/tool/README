/*****************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved          *
 *                                                               *
 * $Id: README,v 1.11 2013/06/26 12:14:22 siva Exp $           *
 *                                                               *
 *  Description: typedef for web privilege mapping               *
 ****************************************************************/

Introduction
============

This README file details the usage of
        A) HTMLGEN  TOOL
        B) HTMLSCAN TOOL
        C) GENHTML TOOL
		D) WEB PRIVILEGE TOOL

- Required web pages can be included in ISS by converting them into 
  'htmldata.h' file.
- The htmldata.h file can be generated with the help of 'HTMLSCAN' tool.
  This 'HTMLSCAN' tool takes the htmlpage with 'MIB objects in OIDs format'
  (eg 1.3.6.1.2.1.4.3.0) and generates the 'htmldata.h' file. 
- Incase if the htmlpage is written with 'MIB objects in VARIABLE format'
  (eg. ipInReceives ), such a page can be converted to a htmlpage with 
  'MIB objects in OID format' (eg 1.3.6.1.2.1.4.3.0), using the 'HTMLGEN' tool.
- The 'HTMLGEN' tool does this conversion using the 'db files'(database files)
  of the corresponding MIB.
- The dbfile for a MIB can be generated using the 'MIBCOMPILER' tool.
  Usage of the 'MIBCOMPILER' tool is described in the README file present in
  'future/ISS/common/tool/mibcompiler' directory

The usage of 'htmlgen' ,'htmlscan' and 'genhtml' tools are further explained
below:

A) HTMLGEN TOOL 
===============
        The HTMLGEN tool converts a 'htmlpage with snmp mib variable names'
        into 'htmlpage with objectidentifier(OID)' format.
        
        For Example, is a htmlpage contains MIB variable names as below, 
        <! ipInReceives >
        <! ipInHdrErrors >
        <! ipForwDatagrams >

        the 'htmlgen' tool replaces the mib variables with their corresponding 
        OID ,Data Type and Access Type with the help of db files as below:

        <! 1.3.6.1.2.1.4.3.0:COUNTER:READONLY >
        <! 1.3.6.1.2.1.4.4.0:COUNTER:READONLY >
        <! 1.3.6.1.2.1.4.6.0:COUNTER:READONLY >

        The dbfile for a MIB can be generated using the 'MIBCOMPILER' tool
        present in 'future/ISS/common/tool/mibcompiler'. Refer to the README
        file in that directory for more details on 'how to generate dbfiles?'
         
        If the mibvariable is scalar it will append "0" at the end of the OID
        otherwise the OID will be remain same.

        If the mib variables are which are used in a particular htmlpage is
        not in the database files directory(ie 'dbfiles' directory), the
        'htmlgen' tool will return a error and stops executing the current file.
           
        If the datatype of the mib variable used in the htmlpage design is different
	from the one which is definded in the 'dbfiles', you can modify the following 
	array to get it reflected in the HTMLGEN tool generation.

	For Example:
	===========
	The following OID is definded as ENM_OCTETSTRING in the 'dbfile', but the same
	is required as ENM_IMP_DISPLAYSTRING for web pages. Hence instead of manully 
	updating the changes, it can directly reflected in tool.

                 1.3.6.1.6.3.18.1.1.1.1    ENM_IMP_DISPLAYSTRING

	File needs to be updated:
        =========================
	File : code/future/webnm/too/Htmlgen/src/main.c

        1. api1ChgOidSnmp[], update this array with new OID.
        2. api1ChgTypeSnmp[], update this array with the required datatype. 
        3. Recompile the HTMLGEN TOOL, to effect the changes.
       
           
        HOW TO EXECUTE HTMLGEN TOOL        
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~

        1) Goto the future/webnm/tool/Htmlgen directory.
        2) export MKDIR=mkdir
        3) delete the 'obj' & 'exe' directory
        4) give 'make'
           This will generate 'HTMLGEN' tool in 'exe' directory
        5) Create a directory say, 'htmlpages' and place the required htmlpages
           in that directory.
        6) The htmlpages folder consists of Packagewise ".html" files
        7) The htmlpages for these Packages are present in seperate folders
           carrying their respective Package names.
        8) Each of the directories contain the generated respective "htmldata.h"
           file.
        9) To collect the complete set of package-wise html files, do the 
           following steps -
           i) Copy the ".html" pages from directory "ISS/common/web/htmlpages/workgroup 
              into a new created folder say "htmpages".
           ii)For enterprise package, in addition to (i) above, copy the files from 
              directory "ISS/common/web/htmlpages/enterprise" into the new 
              folder "htmpages".
           iii) Similarly, for metro package, in addition to (i) above, copy the
                files from directory "ISS/common/web/htmlpages/metro" into the
                new folder "htmpages".
       10) Similarly create a directory say, 'dbfiles' and place the 
           corresponding dbfiles in that directory.
       11) ./HTMLGEN <Directory of htmlfiles> <Directory of dbfiles>
           After successful execution, the converted htmlpages will be created in the
           current directory.
       12) After converting the htmlpages , user has to check those htmlpages to 
           verify whether all the 'MIB VARIABLE' names are converted to OID
           format. Need to manually fill-in the OID's incase if there are any misses.
           This might happen if any table refers to objects from some other MIBs.

        
B) HTMLSCAN TOOL
================
         The HTMLSCAN tool is used to convert the html files to a header file
         called htmldata.h. This htmldata.h should be copied to 'future/webnm'
         directory for compiling the html pages to generate AricentEnm.o

         HOW TO EXECUTE HTMLSCAN TOOL
         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        1) Goto the future/webnm/tool/Htmlscan directory.
        2) enter ./mk_HtmlScan.
           HTMLSCAN tool will be created in the current directory.
        3) Copy the 'htmlpages' directory from 
           future/ISS/common/web/htmlpages/workgroup
           into future/webnm/tool/Htmlscan  directory.
        4) The htmlpages folder consists of Packagewise ".html" files
        5) The htmlpages for these Packages are present in seperate folders
           carrying their respective Package names.
        6) The htmlpages for these Packages are present in seperate folders
           carrying their respective Package names.
        7) For WORKGROUP Package just copy the ".html" files from directory
           ISS/common/web/htmlpages/workgroup.
        8) If the Package required is either ENTERPRISE or METRO, in addition to
           (6) above, copy the corresponding ".html" files present in
           ISS/common/web/htmlpages/enterprise OR
           ISS/common/web/htmlpages/metro into the folder 
           future/webnm/tool/Htmlscan.
        9) Enter ->./HTMLSCAN htmpages.
           where 'htmpages' is the name of the <Directory of htmlfiles>
       10) The file htmldata.h will be created in the current directory.
       11) Copy  htmldata.h into future/webnm directory.
       12) The generated htmldata.h for the respective packages is also present
           in the respective package folder in ISS/common/web/htmlpages.

C) GENHTML TOOL 
===============
        The GENHTML tool converts a 'htmlpage with objectidentifier(OID)'
        into 'htmlpage whith snmp mib variable names' format.
        
        For Example, is a htmlpage contains ODIs as below, 
        
        <! 1.3.6.1.2.1.4.3.0:COUNTER:READONLY >       
        <! 1.3.6.1.2.1.4.4.0:COUNTER:READONLY >       
        <! 1.3.6.1.2.1.4.6.0:COUNTER:READONLY >

        the 'genhtml' tool replaces the OIDS and their parameters (Access and
        Data Types) with their corresponding MIB Object Names with the help of
        db files as below:

        <! ipInReceives >
        <! ipInHdrErrors >
        <! ipForwDatagrams >

        The dbfile for a MIB can be generated using the 'MIBCOMPILER' tool
        present in 'future/ISS/common/tool/mibcompiler'. Refer to the README 
        file in that directory for more details on 'how to generate dbfiles?' 
         
        If the OIDS,  which are used in a particular htmlpage is 
        not in the database files directory(ie 'dbfiles' directory), the 
        'genhtml' tool will return a error and stops executing the current file.
           
        HOW TO EXECUTE GENHTML TOOL        
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~

        1) Goto the future/webnm/tool/Htmlgen directory.
        2) export MKDIR=mkdir
        3) delete the 'obj' & 'exe' directory
        4) give 'make'
           This will generate 'GENHTML' tool in 'exe' directory
        5) Create a directory say, 'htmlpages' and place the required htmlpages
           in that directory.
        6) Now, give
                   ./GENHTML <htmlpages> <dbfiles>,
                   where htmlpages --> directory containing we pages to be 
                                       modified, and
                         dbfiles   --> directory containing necessary 
                                       *.db files              

D) WEB PRIVILEGE TOOL
======================
	The WEB PRIVILEGE TOOL is to generate the privilege for the web pages.

	The input file for this tool is webpriv.h that contains the cli commands,
	cli modes and url names. 

	This tool parches and fetches the informations from webpriv.h and generates 
	webprivds.h	and webprivid.h files. 

	The webprivds.h file contains the 1-D array of unique cli commands, unique cli modes 
	and unique url name.

	The webprivid.h file contains the array indices of cli commands, cli modes and urlnames.
	This is the file which is to be used by the webnm.c.

	HOW TO ADD A NEW WEB PAGE
	~~~~~~~~~~~~~~~~~~~~~~~~~~

	1) Goto future/webnm/tool/inc/webpriv.h file.
	2) Add the cli command, cli Privilege mode and webpage at code/future/webnm/tool/inc/webpriv.h. The last parameter should be zero.
		Example : 
		{
			{"show ip mroute"},	
			{"USEREXEC"},
			{"ipv4mc_routeintfconf.html"},
			0
		},
	3) Do the make from code/future/webnm/tool.

                         ------- END OF README -------
