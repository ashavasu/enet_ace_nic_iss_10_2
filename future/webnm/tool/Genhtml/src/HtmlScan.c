/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: HtmlScan.c,v 1.4 2017/06/13 13:14:14 siva Exp $
 *
 * Description: This file has routine to generate Headerfile for HTML files
 *
 ***********************************************************************/
#include "main.h"

INT4
HtmlScan (INT1 *path)
{
    FILE               *fp;
    FILE               *final;
    FILE               *hp;
    FILE               *fpObject;
    INT1               *pi1path;
    INT4                i4Counter = 0;
    UINT1               i1Total;
    INT4                i4HFCount = 0;
    INT4                i4, i4Length;
    INT1                ai1FileName[256];
    INT1                ai1File[50];
    INT1                ai1FileSize[20];

    pi1path = MEM_MALLOC (MAX_PATH_LEN, INT1);
    if (pi1path == NULL)
    {
        printf ("\n Unable to allocate memory\n");
        return FAILURE;
    }
    strcpy (pi1path, "ls -l ");
    strcat (pi1path, path);
    strcat (pi1path, "/*.html >htmlfiles");
    i1Total = system (pi1path);
    if (i1Total == 127)
    {
        printf ("\n Couldn't find HTML Files in this directory");
        exit (1);
    }

    if (ReadHTMLFileList () != SUCCESS)
    {
        return FAILURE;
    }
    unlink (HTML_FILES);
    return 0;
}

INT1
ReadHTMLFileList ()
{
    INT4                i4Count = 0;
    INT1                ai1FileName[256];
    INT1                ai1File[50];
    INT1               *pi1FileName;
    INT4                i4Len;
    INT1                ai1FileSize[20];
    INT4                i4FileSize1;
    FILE               *fp;
    FILE               *SrcFile;

    if ((fp = fopen (HTML_FILES, "r")) == NULL)
    {
        printf ("\n Error html list file open");
        return FAILURE;
    }
    while (fgets (ai1FileName, 256, fp) != NULL)
    {
        sscanf (ai1FileName, "%*s%*s%*s%*s%s%*s%*s%*s%s", ai1FileSize, ai1File);
        i4FileSize1 = atoi (ai1FileSize);
        pi1FileName = (INT1 *) strrchr (ai1File, '/');
        pi1FileName++;
        i4Len = strlen (pi1FileName);
        if ((SrcFile = fopen (ai1File, "r")) == NULL)
        {
            printf ("Error Html files open for %s\n", ai1File);
            continue;
        }
        phtml1[i4Count].pi1Name = MEM_MALLOC (i4Len + 1, INT1);
        if (phtml1[i4Count].pi1Name == NULL)
        {
            printf ("\n Unable to Allocate memory for filename");
            return FAILURE;
        }
        strncpy (phtml1[i4Count].pi1Name, pi1FileName, i4Len);
        phtml1[i4Count].pi1Name[i4Len] = '\0';
        phtml1[i4Count].i4Size = i4FileSize1;
        phtml1[i4Count].pi1Ptr = MEM_MALLOC (i4FileSize1 + 1, INT1);
        if (phtml1[i4Count].pi1Ptr == NULL)
        {
            printf ("Unable to allocate memory for file to copy\n");
            return FAILURE;
        }
        if (fread (phtml1[i4Count].pi1Ptr, 1, i4FileSize1, SrcFile) !=
            (UINT4) i4FileSize1)
        {
            printf ("\n Error in file read\n");
        }
        phtml1[i4Count].pi1Ptr[i4FileSize1] = '\0';
        i4Count++;
        if (i4Count == HTML_MAX_NO_FILES)
        {
            printf ("\n Number of Files exceeds,copies only first % d files ",
                    i4Count);
            return SUCCESS;
        }
        fclose (SrcFile);
    }
    phtml1[i4Count].pi1Name = NULL;
    fclose (fp);
    return SUCCESS;
}

INT4
HSCheckHtmlPage (char *PageName)
{
    INT4                i4Counter;
    for (i4Counter = 0; i4Counter < HTML_MAX_NO_FILES; i4Counter++)
    {
        if (phtml1[i4Counter].pi1Name == 0)
            return FAILURE;
        if (STRCMP (phtml1[i4Counter].pi1Name, PageName) == 0)
            return (phtml1[i4Counter].i4Size);
    }
    return FAILURE;
}

INT1               *
HSReadHTMLPage (char *PageName)
{
    INT4                i4Counter;
    for (i4Counter = 0; i4Counter < HTML_MAX_NO_FILES; i4Counter++)
    {
        if (phtml1[i4Counter].pi1Name == NULL)
        {
            return NULL;
        }
        if (STRCMP (phtml1[i4Counter].pi1Name, PageName) == 0)
        {
            return ((INT1 *) phtml1[i4Counter].pi1Ptr);
        }
    }
    return NULL;
}

INT1
ParseHTMLForSNMPVariables (t_html * pg, INT1 **ppi1MibName)
{
    INT4                i4Length = 0, i4Count = 0;
    INT1                pi1Oid[WEBNM_MAX_OID_LENGTH] = "";
    INT1               *pi1fc1, *pi1fc2;
    INT1               *pi1fcstart, *pi1fcend;
    INT1               *pi1fc = &pg->element[0];
    INT4                i4Result;
    INT1                pi1Name[WEBNM_MAX_NAME_LENGTH];

    ppi1MibName[0] = NULL;

    pi1fcstart =
        (INT1 *) strstr ((const char *) pi1fc,
                         (const char *) "snmpvaluesstart");
    if (pi1fcstart == NULL)
        return FAILURE;

    pi1fcend =
        (INT1 *) strstr ((const char *) pi1fcstart,
                         (const char *) "snmpvaluesend");
    if (pi1fcend == NULL)
        return FAILURE;

    pi1fc1 = pi1fcstart;

    while ((pi1fc1 + 1) < pi1fcend)
    {
        memset (pi1Name, 0, WEBNM_MAX_NAME_LENGTH);
        pi1fc1 = (INT1 *) STRCHR (pi1fc1, '!');
        if (pi1fc1 == NULL)
            break;
        if (pi1fc1 >= pi1fcend)
            break;

        pi1fc2 = (INT1 *) STRCHR (pi1fc1, ':');
        if (pi1fc2 == NULL)
            break;
        if (pi1fc2 >= pi1fcend)
            break;

        i4Length = (pi1fc2 - pi1fc1 - 2);

        if (i4Length >= WEBNM_MAX_OID_LENGTH)
        {
            return MIB_NAME_EXCEEDS_MAX;
        }

        STRNCPY (pi1Oid, (pi1fc1 + 2), i4Length);
        pi1Oid[i4Length] = '\0';

        /* Check done for name and replacing the same by oid
         * Reversing the process ..............*/

        i4Result = CheckAndProcessTypeOfOid (pi1Oid, &i4Length);

        if (i4Result == FAILURE)
        {
            printf ("OID is not Valid");
            return FAILURE;
        }

        if (GetNameFromOid (pi1Oid, i4Length, pi1Name) == FAILURE)
        {
            printf ("\nOID %s not found in the dbfiles", pi1Oid);
            return FAILURE;
        }

        ppi1MibName[i4Count] = MEM_MALLOC (WEBNM_MAX_NAME_LENGTH, INT1);
        if (ppi1MibName == NULL)
        {
            MEM_FREE (ppi1MibName);
            return FAILURE;
        }
        ppi1MibName[i4Count + 1] = NULL;
        STRNCPY (ppi1MibName[i4Count], pi1Name, STRLEN (pi1Name));
        i4Count++;
        pi1fc1 = pi1fc2;
    }
    return SUCCESS;
}

INT1
ParseHTMLForIndexVariables (t_html * pg, INT1 **ppi1Indexvarlist)
{
    INT4                i4Length = 0, i4Count = 0;
    INT1                pi1Oid[WEBNM_MAX_OID_LENGTH] = "";
    INT1               *pi1fc1, *pi1fc2;
    INT1               *pi1Index = NULL;
    INT1               *pi1IndexEnd = NULL;
    INT1               *pi1fcstart, *pi1fcend;
    INT1               *pi1fc = &pg->element[0];
    INT1               *pi1Name[WEBNM_MAX_NAME_LENGTH];
    INT4                i4Result;

    if (pi1fc == NULL)
        return FAILURE;
    pi1fcstart =
        (INT1 *) strstr ((const char *) pi1fc,
                         (const char *) "snmpvaluesstart");
    if (pi1fcstart == NULL)
        return FAILURE;
    pi1fcend =
        (INT1 *) strstr ((const char *) pi1fcstart,
                         (const char *) "snmpvaluesend");
    if (pi1fcend == NULL)
        return FAILURE;

    pi1fc1 = pi1fcstart;
    ppi1Indexvarlist[0] = NULL;

    while ((pi1fc1 + 1) < pi1fcend)
    {
        pi1fc1 = (INT1 *) STRCHR (pi1fc1, '!');
        if (pi1fc1 == NULL)
            break;
        if (pi1fc1 >= pi1fcend)
            break;

        pi1Index = (INT1 *) STRSTR (pi1fc1, ":INDEX");
        if (pi1Index == NULL)
        {
            /* No index specified *
             * Return
             * */
            return SUCCESS;
        }
        pi1IndexEnd = (INT1 *) STRCHR (pi1fc1, '>');

        if (pi1Index > pi1IndexEnd)
        {
            pi1fc1 = STRCHR (pi1fc1, '!');
            pi1IndexEnd = STRCHR (pi1fc1, '>');
        }

        pi1fc2 = (INT1 *) STRCHR (pi1fc1, ':');
        if (pi1fc2 == NULL)
            break;
        if (pi1fc2 >= pi1fcend)
            break;

        i4Length = (pi1fc2 - pi1fc1 - 2);

        if (i4Length >= WEBNM_MAX_OID_LENGTH)
        {
            return MIB_NAME_EXCEEDS_MAX;
        }

        STRNCPY (pi1Oid, (pi1fc1 + 2), i4Length);
        pi1Oid[i4Length] = '\0';

        /* Check done for name and replacing the same by oid
         * Reversing the process ..............*/

        i4Result = CheckAndProcessTypeOfOid (pi1Oid, &i4Length);

        if (i4Result == FAILURE)
        {
            printf ("\nOID is not Valid");
            return FAILURE;
        }

        if (GetNameFromOid (pi1Oid, i4Length, pi1Name) == FAILURE)
        {
            printf ("\nOID %s not found in the dbfiles", pi1Oid);
            return FAILURE;
        }

        ppi1Indexvarlist[i4Count] = MEM_MALLOC (WEBNM_MAX_NAME_LENGTH, INT1);
        if (ppi1Indexvarlist == NULL)
        {
            MEM_FREE (ppi1Indexvarlist);
            return FAILURE;
        }
        ppi1Indexvarlist[i4Count + 1] = NULL;
        STRNCPY (ppi1Indexvarlist[i4Count], pi1Name, STRLEN (pi1Name));
        i4Count++;
        pi1fc1 = pi1fc2;
    }
    return SUCCESS;
}

INT4
CheckAndProcessTypeOfOid (INT1 *pi1Oid, INT4 *i4Len)
{
    if ((pi1Oid[(*i4Len) - 1] == '0') && (pi1Oid[(*i4Len) - 2] == '.'))
    {
        pi1Oid[(*i4Len) - 2] = '\0';
        *i4Len = *i4Len - 2;
        return SUCCESS;
    }
    else
    {
        return SUCCESS;
    }
    return FAILURE;
}
