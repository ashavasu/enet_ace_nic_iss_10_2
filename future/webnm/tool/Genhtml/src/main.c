/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: main.c,v 1.2 2007/08/20 06:50:03 iss Exp $
 *
 * Description: This file has routine to generate HTML files
 *
 ***********************************************************************/
#include "main.h"

INT1               *api1SyntaxSnmp[] = {
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OBJECTIDENTIFIER",
    "ENM_NETWORK_ADDRESS",
    "ENM_IPADDRESS",
    "ENM_TIMETICKS",
    "ENM_GAUGE",
    "ENM_COUNTER",
    "ENM_OPAQUE",
    "ENM_INTEGER",
    "ENM_GAUGE",
    "ENM_COUNTER",
    "ENM_COUNTER64",
    "ENM_UNSIGNED32",
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_TIMETICKS",
    "ENM_OCTETSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_OIDTYPE",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_OIDTYPE",
    "ENM_OIDTYPE",
    "ENM_OCTETSTRING",
    "ENM_INTEGER",
    "ENM_OIDTYPE",
    "ENM_OIDTYPE",
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "SYNTAX_NOT_DEF",
    NULL
};
INT1                api1SyntaxSnmpNumber[] = {
    2,
    3,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    2,
    8,
    9,
    14,
    8,
    2,
    3,
    7,
    3,
    11,
    4,
    2,
    2,
    2,
    2,
    2,
    4,
    4,
    3,
    2,
    4,
    4,
    2,
    3,
    3,
    15,
    NULL
};

UINT1               au1Access[5][20] = {
    "READONLY",
    "READWRITE",
    "READCREATE",
    "NOACCESS",
    "ACCESSBILEFORNOTIFY"
};
INT4
main (argc, argv)
     INT4                argc;
     INT1               *argv[];
{
    UINT1               au1HtmlPath[128];
    UINT1               au1DBPath[128];
    UINT1               au1FileName[128];
    UINT1               au1NewFileName[128];
    UINT1               au1TempName[200];
    UINT1               au1Oid[500];
    INT1                ai1buffer[256];
    INT4                i4Index, i4Count, i4Ret;
    INT1                i1Return;
    INT4                i1IndexRet;
    INT1                i1Flag = 0;
    UINT4               u4MibType;
    INT1               *pi1MibName[WEBNM_MAX_NAME_LENGTH];
    INT1               *pi1IndexName[WEBNM_MAX_NAME_LENGTH];
    t_html              pgcontent;
    FILE               *fp;
    FILE               *fpout, *fpin;
    UINT4               u4Count;
    INT1               *pi1WritePtr = NULL;

    if (argc != 3)
    {
        fprintf (stderr,
                 "\nUsage: HTMLGEN <Directory of Htmlfiles>  <Directory of dbfiles> \n");
        exit (1);
    }
    STRCPY (au1HtmlPath, argv[1]);
    STRCPY (au1DBPath, argv[2]);
    HtmlScan (&au1HtmlPath);
    DBScan (&au1DBPath);
    if (pdb[0] == NULL)
    {
        printf ("Mib is Not Loaded \n");
        exit (1);
    }
    if (phtml1[0].pi1Name == NULL)
    {
        printf ("HTML Pages is Not Loaded \n");
        exit (1);
    }

    printf ("\nProcessing ............\n");

    for (u4Count = 0; phtml1[u4Count].pi1Name != NULL; u4Count++)
    {
        STRCPY (au1FileName, phtml1[u4Count].pi1Name);
        STRCPY (au1NewFileName, phtml1[u4Count].pi1Name);
        pgcontent.element = 0;
        pgcontent.size = 0;
        if (ReadSource (au1FileName, &pgcontent) == FAILURE)
        {
            printf ("Unable TO get the Source\n");
            return FAILURE;
        }
        i1Return = ParseHTMLForIndexVariables (&pgcontent, pi1IndexName);
        i1Return = ParseHTMLForSNMPVariables (&pgcontent, pi1MibName);
        if (i1Return == FAILURE || i1Return == MIB_NAME_EXCEEDS_MAX)
        {
            continue;
        }
        if (i1Return == -2)
        {
            printf ("File \n", au1FileName);
            exit (1);
        }
        sprintf (au1FileName, "new%d.html", u4Count);
        if ((fp = fopen (au1FileName, "w")) == NULL)
        {
            printf ("\n Error html list file open");
            return FAILURE;
        }
        fprintf (fp, "%s", "\n");
        fprintf (fp, "%s", phtml1[u4Count].pi1Ptr);
        fprintf (fp, "%s", "\n");
        fclose (fp);
        if ((fpin = fopen (au1FileName, "r")) == NULL)
        {
            printf ("\n Error html list file open");
            return FAILURE;
        }
        if ((fpout = fopen (au1NewFileName, "w")) == NULL)
        {
            printf ("\n Error html list file open");
            return FAILURE;
        }

        fprintf (fpout, "\n");
        fprintf (fpout, "<HTML>\n");

        if (pi1IndexName[0] != NULL)
        {
            fprintf (fpout, "<! indexstart >\n");

            for (i4Count = 0; pi1IndexName[i4Count] != NULL; i4Count++)
            {
                fprintf (fpout, "<! ");
                memset (au1TempName, 0, WEBNM_MAX_NAME_LENGTH);
                STRCPY (au1TempName, pi1IndexName[i4Count]);
                fprintf (fpout, "%s", au1TempName);
                fprintf (fpout, " >\n");
            }
            fprintf (fpout, "<! indexend >\n");
            /* Make the value as null because if the next page
             * contains only scalar objects this won't be reset
             * */
            pi1IndexName[0] = NULL;
        }

        if (pi1MibName[0] != NULL)
        {
            fprintf (fpout, "\n");
            fprintf (fpout, "<! snmpvaluesstart >\n");

            for (i4Count = 0; pi1MibName[i4Count] != NULL; i4Count++)
            {
                fprintf (fpout, "<! ");
                memset (au1TempName, 0, WEBNM_MAX_NAME_LENGTH);
                STRCPY (au1TempName, pi1MibName[i4Count]);
                fprintf (fpout, "%s", au1TempName);
                fprintf (fpout, " >\n");
            }
            fprintf (fpout, "<! snmpvaluesend >\n");
        }

        while (!feof (fpin))
        {
            fgets (ai1buffer, 80, fpin);
            if (strstr (ai1buffer, "snmpvaluesend >") != NULL)
                break;
        }

        while (!feof (fpin))
        {
            fgets (ai1buffer, 256, fpin);
            if (strstr (ai1buffer, "indexstart") != NULL)
            {
                while (!feof (fpin))
                {
                    fgets (ai1buffer, 80, fpin);
                    if (strstr (ai1buffer, "indexend >") != NULL)
                        break;
                }
            }
            else
            {
                pi1WritePtr = ai1buffer;
                pi1WritePtr = CheckAndWriteName (pi1WritePtr, fpout);
                pi1WritePtr = CheckAndWriteValue (pi1WritePtr, fpout);
                pi1WritePtr = CheckAndWriteKEY (pi1WritePtr, fpout);
                fputs (pi1WritePtr, fpout);
            }
        }
        fclose (fpin);
        unlink (au1FileName);
        fclose (fpout);
    }
    printf ("\n.......DONE\n");
}

INT1               *
CheckAndWriteName (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL;
    INT1               *pi1EndPtr = NULL;
    INT1                ai1MyOid[WEBNM_MAX_OID_LENGTH];
    INT1                ai1Name[WEBNM_MAX_NAME_LENGTH];
    INT4                i4Result = FAILURE;
    INT4                i4Length;
    INT4                i4WriteReqd = FALSE;
    INT1                i1Select = FALSE;

    memset (ai1MyOid, 0, WEBNM_MAX_OID_LENGTH);
    memset (ai1Name, 0, WEBNM_MAX_NAME_LENGTH);

    if ((pi1StartPtr = strstr (pi1WritePtr, "NAME=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "name=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "name =")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "NAME =")) != NULL)
    {
        /* check for the special processing
         * <select name = 1.3.4.5>
         * since this won't get passed in the 
         * strstr (pi1WritePtr, "")
         * */
        if (((pi1EndPtr = strstr (pi1WritePtr, "select")) != NULL) ||
            ((pi1EndPtr = strstr (pi1WritePtr, "Select")) != NULL) ||
            ((pi1EndPtr = strstr (pi1WritePtr, "SELECT")) != NULL))
        {
            i1Select = TRUE;
        }

        pi1StartPtr += strlen ("name");
        fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);
        fwrite ("=", 1, 1, fpout);
        pi1WritePtr += (pi1StartPtr - pi1WritePtr);

        if ((pi1EndPtr = strstr (pi1WritePtr, "==")) != NULL)
        {
            /* This check is done because, in some functions
             * to check the value, we will use the format like
             * 'name == x'.
             * Since an = is already placed, placing one more
             * */
            fwrite ("=", 1, 1, fpout);
        }
        while (*pi1StartPtr == '=' || *pi1StartPtr == ' ')
        {
            pi1StartPtr++;
        }
        pi1WritePtr = pi1StartPtr;

        if (pi1StartPtr[0] == '"')
        {
            pi1StartPtr++;

            /* Find whether "reqd" option is given for this OID */
            if ((pi1EndPtr = strstr (pi1StartPtr, "reqd") != NULL))
            {
                i4WriteReqd = TRUE;
                pi1StartPtr = pi1StartPtr + (strlen ("reqd"));
            }

            if ((pi1EndPtr = strstr (pi1StartPtr, "\"")) != NULL)
            {
                memcpy (ai1MyOid, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyOid[pi1EndPtr - pi1StartPtr] = '\0';

                i4Length = (pi1EndPtr - pi1StartPtr);
                i4Result = CheckAndProcessTypeOfOid (ai1MyOid, &i4Length);

                if (i4Result == FAILURE)
                {
                    printf ("\nOID is not Valid");
                    return FAILURE;
                }
                if (GetNameFromOid (ai1MyOid, (pi1EndPtr - pi1StartPtr),
                                    ai1Name) == SUCCESS)
                {
                    fwrite ("\"", 1, 1, fpout);
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    fprintf (fpout, "%s", ai1Name);
                    fwrite ("\"", 1, 1, fpout);
                    pi1EndPtr++;
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    fwrite ("\"", 1, 1, fpout);
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    pi1WritePtr = pi1StartPtr;
                }
            }
        }
        else
        {
            /* Find whether "reqd" option is given for this OID */
            if ((pi1EndPtr = strstr (pi1StartPtr, "reqd") != NULL))
            {
                i4WriteReqd = TRUE;
                pi1StartPtr = pi1StartPtr + (strlen ("reqd"));
            }
            if ((pi1EndPtr = strstr (pi1StartPtr, " ")) != NULL ||
                (i1Select == TRUE))
            {
                if ((i1Select == TRUE) && (pi1EndPtr == NULL))
                {
                    i1Select = FALSE;
                    pi1EndPtr = strstr (pi1StartPtr, ">");
                    if (pi1EndPtr == NULL)
                    {
                        return FAILURE;
                    }
                }

                memcpy (ai1MyOid, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyOid[pi1EndPtr - pi1StartPtr] = '\0';

                i4Length = (pi1EndPtr - pi1StartPtr);
                i4Result = CheckAndProcessTypeOfOid (ai1MyOid, &i4Length);

                if (i4Result == FAILURE)
                {
                    printf ("OID is not Valid");
                    return FAILURE;
                }
                if (GetNameFromOid (ai1MyOid, (pi1EndPtr - pi1StartPtr),
                                    ai1Name) == SUCCESS)
                {
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    fprintf (fpout, "%s", ai1Name);
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1WritePtr = pi1StartPtr;
                }
            }
        }
    }
    return pi1WritePtr;
}

INT1               *
CheckAndWriteValue (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL;
    INT1               *pi1EndPtr = NULL;
    INT1                ai1MyOid[WEBNM_MAX_OID_LENGTH];
    INT1                ai1Name[WEBNM_MAX_NAME_LENGTH];
    INT4                i4Result = FAILURE;
    INT4                i4Length;

    memset (ai1MyOid, 0, WEBNM_MAX_OID_LENGTH);
    memset (ai1Name, 0, WEBNM_MAX_NAME_LENGTH);

    if ((pi1StartPtr = strstr (pi1WritePtr, "value=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "VALUE=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "VALUE =")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "value =")) != NULL)

    {
        pi1StartPtr += strlen ("value");
        fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);
        fwrite ("=", 1, 1, fpout);
        pi1WritePtr += (pi1StartPtr - pi1WritePtr);

        if ((pi1EndPtr = strstr (pi1WritePtr, "==")) != NULL)
        {
            /* This check is done because, in some functions
             * to check the value, we will use the format like
             * 'value == x'.
             * Since an = is already placed, placing one more
             * */
            fwrite ("=", 1, 1, fpout);
        }

        while (*pi1StartPtr == '=' || *pi1StartPtr == ' ')
        {
            pi1StartPtr++;
        }
        pi1WritePtr = pi1StartPtr;

        if (pi1StartPtr[0] == '"')
        {
            pi1StartPtr++;

            if (((pi1EndPtr = strstr (pi1StartPtr, "_KEY\"")) != NULL) ||
                ((pi1EndPtr = strstr (pi1StartPtr, "_KEY\" ")) != NULL))
            {
                fwrite ("\"", 1, 1, fpout);
                memcpy (ai1MyOid, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyOid[pi1EndPtr - pi1StartPtr] = '\0';

                i4Length = (pi1EndPtr - pi1StartPtr);
                i4Result = CheckAndProcessTypeOfOid (ai1MyOid, &i4Length);

                if (i4Result == FAILURE)
                {
                    printf ("OID is not Valid");
                    return FAILURE;
                }
                if (GetNameFromOid (ai1MyOid, (pi1EndPtr - pi1StartPtr),
                                    ai1Name) == SUCCESS)
                {
                    strcat (ai1Name, "_KEY");
                    fprintf (fpout, "%s", ai1Name);
                    fwrite ("\"", 1, 1, fpout);
                    pi1EndPtr += strlen ("_KEY\"");
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1StartPtr--;
                    pi1WritePtr = pi1StartPtr;
                }

            }
        }
        else
        {
            if ((pi1EndPtr = strstr (pi1StartPtr, "_KEY")) != NULL)
            {
                memcpy (ai1MyOid, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyOid[pi1EndPtr - pi1StartPtr] = '\0';

                i4Length = (pi1EndPtr - pi1StartPtr);
                i4Result = CheckAndProcessTypeOfOid (ai1MyOid, &i4Length);

                if (i4Result == FAILURE)
                {
                    printf ("OID is not Valid");
                    return FAILURE;
                }
                if (GetNameFromOid (ai1MyOid, (pi1EndPtr - pi1StartPtr),
                                    ai1Name) == SUCCESS)
                {
                    strcat (ai1Name, "_KEY");
                    fprintf (fpout, "%s", ai1Name);
                    pi1EndPtr += strlen ("_KEY");
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1WritePtr = pi1StartPtr;
                }

            }
        }
    }
    return pi1WritePtr;
}

INT1               *
CheckAndWriteKEY (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL;
    INT1               *pi1EndPtr = NULL;
    INT1                ai1MyOid[WEBNM_MAX_OID_LENGTH];
    INT1                i1Test = 0;
    INT1                ai1Name[WEBNM_MAX_NAME_LENGTH];
    INT4                i4Result = FAILURE;
    INT4                i4Length;

    memset (ai1MyOid, 0, WEBNM_MAX_OID_LENGTH);
    memset (ai1Name, 0, WEBNM_MAX_NAME_LENGTH);

    if ((pi1EndPtr = strstr (pi1WritePtr, "_KEY")) != NULL)
    {
        pi1StartPtr = pi1EndPtr;
        while (pi1StartPtr >= pi1WritePtr)
        {
            if (*pi1StartPtr == ' ' ||
                *pi1StartPtr == '>' || *pi1StartPtr == '"')
            {
                if (*pi1StartPtr == '"')
                    i1Test = 1;
                else
                    i1Test = 0;
                break;
            }
            pi1StartPtr--;
        }
        pi1StartPtr++;
        if (i1Test == 1)
            fwrite (pi1WritePtr, ((pi1StartPtr - pi1WritePtr) - 1), 1, fpout);
        else
            fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);

        memcpy (ai1MyOid, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
        ai1MyOid[pi1EndPtr - pi1StartPtr] = '\0';

        i4Length = (pi1EndPtr - pi1StartPtr);
        i4Result = CheckAndProcessTypeOfOid (ai1MyOid, &i4Length);

        if (i4Result == FAILURE)
        {
            printf ("OID is not Valid");
            return FAILURE;
        }
        if (GetNameFromOid (ai1MyOid, (pi1EndPtr - pi1StartPtr),
                            ai1Name) == SUCCESS)
        {
            strcat (ai1Name, "_KEY");
            fprintf (fpout, "%s", ai1Name);

            if (i1Test == 1)
                pi1EndPtr += strlen ("_KEY\"");
            else
                pi1EndPtr += strlen ("_KEY");

            pi1WritePtr = pi1EndPtr;
        }
        else
        {
            pi1WritePtr = pi1StartPtr;
        }
    }
    return pi1WritePtr;
}

INT1
ReadSource (INT1 *pgName, t_html * pg)
{
    INT4                i4Size;
    INT1               *pi1Ptr;
    i4Size = HSCheckHtmlPage ((char *) pgName);
    if (i4Size == FAILURE)
    {
        return FAILURE;
    }
    if ((pi1Ptr = HSReadHTMLPage ((char *) pgName)) == NULL)
    {
        return FAILURE;
    }
    pg->element = MEM_MALLOC (i4Size + (i4Size / 2), INT1);
    if (pg->element == NULL)
    {
        printf ("unable to allocate memory for pg->element\n");
        return FAILURE;
    }
    STRNCPY (pg->element, pi1Ptr, i4Size);
    pg->element[i4Size] = '\0';
    pg->size = i4Size + 1;
    return SUCCESS;

}
