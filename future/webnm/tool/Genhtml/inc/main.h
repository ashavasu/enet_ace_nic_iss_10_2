#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define FAILURE -1
#define SUCCESS  0
#define HTML_FILES "htmlfiles"
#define OUTPUTFILE1   "htmldata.h"
#define HTML_MAX_NO_FILES  1500
#define MAX_NO_FILES  1500
#define MAX_PATH_LEN  256
#define FILE_NAME_SIZE 100
#define DB_SIZE 1500
#define NO_OF_FILES 150
#define FILE_NO_OF_CHAR_IN_A_LINE 80
#define DB_FILES "DBfiles"
#define OUTPUTFILE  "DB.h"
#define TRUE   1
#define FALSE  0

#define MEM_MALLOC(Size,Type)              (Type *)malloc(Size)
#define MEM_CALLOC(Size,Count,Type)        (Type *)calloc(Size, Count)
#define MEM_FREE(Ptr)                      free(Ptr)
#define MEM_REALLOC(Ptr,Size,Type)         (Type *)realloc(Ptr, Size)


typedef char INT1;
typedef void VOID;
typedef int INT4;
typedef short int INT2;
typedef unsigned int UINT4;
typedef unsigned char UINT1;

typedef struct
{
    INT1               *pi1Name;
    INT4                i4Size;
    UINT1              *pi1Ptr;
}
t_HTML_PAGE;

typedef struct t_HTML
{
   INT1 *element;
   INT4 size;
}t_html;

t_HTML_PAGE         phtml1[MAX_NO_FILES];
INT1                ReadHTMLFileList ();
INT1                db_init (VOID);
INT1                Readdbfile (INT1 **);
INT1                ReadFileList (INT1 **);

typedef struct t_mib_db
{
    INT1               *pi1name;
    UINT4               u4child;
    UINT4               u4peer;
    UINT1               u1type;
    UINT1               u1access;
    INT1               *pi1oid;
}
t_MIB_DB;

struct t_MMI_DB
{
    const INT1         *pi1name;
    const INT1         *pi1oid;
    t_MIB_DB           *pointer;
};
t_MIB_DB           *pdb[NO_OF_FILES];

typedef struct t_sm_link_list
{
    char               *pi1name;
    char               *pi1oid;
    t_MIB_DB           *pointer;
    struct t_sm_link_list *link;
}
t_MMI_LINK_LIST;

#define  MIB_NOT_IN_DB                 -2
#define  MIB_NAME_EXCEEDS_MAX          -3
#define WEBNM_MAX_NAME_LENGTH         200
#define WEBNM_MAX_VAL_LENGTH          200
#define WEBNM_MAX_INSTANCE_LENGTH     200
#define WEBNM_MAX_VARS_IN_PAGE        200
#define WEBNM_MAX_OID_LENGTH          64 

#define  BRANCH                           0x00
#define  TABLE                            0x01
#define  MIB_READ_ONLY                    0x00
#define  MIB_READ_WRITE                   0x01
#define  MIB_READ_CREATE                  0x02
#define  MIB_NOT_ACCESSIBLE               0x03
#define  MIB_ACCESSIBLE_FOR_NOTIFY        0x04

#define  STRCPY(d,s)       strcpy ((char *)(d),(const char *)(s))
#define  STRNCPY(d,s,n)    strncpy ((char *)(d), (const char *)(s), n)

#define  STRCAT(d,s)       strcat((char *)(d),(const char *)(s))

#define  STRCMP(s1,s2)     strcmp  ((const char *)(s1), (const char *)(s2))
#define  STRNCMP(s1,s2,n)  strncmp ((const char *)(s1), (const char *)(s2), n)

#define  STRCHR(s,c)       strchr((const char *)(s), (int)c)
#define  STRRCHR(s,c)      strrchr((const char *)(s), (int)c)

#define  STRSTR(s,t)       strstr((const char *)(s), (const char *)(t))
#define  STRLEN(s)         strlen((const char *)(s))

#define  STRTOK(s,d)       strtok  ((char *)(s), (const char *)(d))

INT1                GetOidStrFromNameInChildDB (t_MIB_DB *, INT1 *, INT4,
		                                                INT1 *);

VOID                RegisterRootMIB (const INT1 *, const INT1 *, t_MIB_DB *);		                                               
UINT1               MMIGetNodeAccess (UINT4);
UINT1               MMIGetNodeType (UINT4);
INT4                GetIndexFromName (INT1 *);
VOID                RegisterDB (VOID);
VOID                RegisterWithNM (const INT1 *,const INT1 *, t_MIB_DB *);

INT1 CountDotsInOidString (INT1 *);
INT1 *CheckAndWriteName(INT1 *pi1WritePtr,FILE *fpout);
INT1 *CheckAndWriteValue(INT1 *pi1WritePtr,FILE *fpout);
INT1 *CheckAndWriteKEY(INT1 *pi1WritePtr,FILE *fpout);

INT1 ReadSource(INT1 *,t_html *);
INT4 CheckTypeOfOid (INT1 *pi1Oid, INT4 i4Len);
