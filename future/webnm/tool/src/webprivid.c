/*****************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved          *
 *                                                               *
 * $Id: webprivid.c,v 1.2 2013/06/26 12:14:17 siva Exp $       *
 *                                                               *
 *****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <osxstd.h>
#include "webpriv.h"
#include "webprivds.h"

INT4
main (VOID)
{

    INT4                i4GlobalIndex = 0;
    INT4                i4UniqIndex = 0;
    FILE               *fp;

    fp = fopen ("webprivid.h", "w+");
    if (fp == NULL)
    {
        printf ("Failed to create file\n");
        return -1;
    }

    /*The below portion of code is for generating the webprivid.h file which will be using in webnm.c */
    /*Putting global brace */
    fprintf (fp,
             "/*This is an auto-generated file. So please do not modify anything*/\n\n");
    fprintf (fp, "#ifndef _WEBPRIVID_H_\n");
    fprintf (fp, "#define _WEBPRIVID_H_\n");

    /*Generate the final data structure */
    fprintf (fp, "typedef struct PagePrivMapping {\n");
    fprintf (fp, "\tINT2        u2CliCmd;\n");
    fprintf (fp, "\tINT2        u2CliCmdMode;\n\tINT2        u2UrlName;\n");
    fprintf (fp, "\tINT2        i2PrivId;\n");
    fprintf (fp, "} tPagePrivMapping;\n\n");
    fprintf (fp, "tPagePrivMapping gPagePrivMapping[] =\n");
    fprintf (fp, "{\n");

    for (i4GlobalIndex = 0;
         strlen (gPagePrivMapping[i4GlobalIndex].ai1CliCmd) != 0;
         i4GlobalIndex++)
    {
        /*fprintf (fp ,"\t\"%s\",\n",gPagePrivMapping[i4GlobalIndex].ai1CliCmd); */
        fprintf (fp, "\t{\n");
        for (i4UniqIndex = 0; strlen (giCliCommand[i4UniqIndex]) != 0;
             i4UniqIndex++)
        {
            if (!strcmp
                (gPagePrivMapping[i4GlobalIndex].ai1CliCmd,
                 giCliCommand[i4UniqIndex]))
            {
                fprintf (fp, "\t\t%d,", i4UniqIndex);
                fprintf (fp, "\t\t/*{\"%s\"},*/\n",
                         gPagePrivMapping[i4GlobalIndex].ai1CliCmd);
            }
        }
        for (i4UniqIndex = 0; strlen (giCliCmdMode[i4UniqIndex]) != 0;
             i4UniqIndex++)
        {
            if (!strcmp
                (gPagePrivMapping[i4GlobalIndex].ai1CliCmdMode,
                 giCliCmdMode[i4UniqIndex]))
            {
                fprintf (fp, "\t\t%d,\t", i4UniqIndex);
                fprintf (fp, "\t\t/*{\"%s\"},*/\n",
                         gPagePrivMapping[i4GlobalIndex].ai1CliCmdMode);
            }
            else
            {
                continue;
            }
        }
        for (i4UniqIndex = 0; strlen (giUrlName[i4UniqIndex]) != 0;
             i4UniqIndex++)
        {
            if (!strcmp
                ((char *) gPagePrivMapping[i4GlobalIndex].au1UrlName,
                 giUrlName[i4UniqIndex]))
            {
                fprintf (fp, "\t\t%d,\t", i4UniqIndex);
                fprintf (fp, "\t/*%s*/\n",
                         gPagePrivMapping[i4GlobalIndex].au1UrlName);
            }
            else
            {
                continue;
            }
        }
        fprintf (fp, "\t\t0\n");
        fprintf (fp, "\t},\n");
    }
    fprintf (fp, "};\n");
    fprintf (fp, "#define WEB_PRIV_MAX_LENGTH %d\n", i4GlobalIndex);
    fprintf (fp, "#endif");
    fprintf (fp, "\n");
    fclose (fp);
    return 0;
}
