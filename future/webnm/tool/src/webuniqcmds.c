/*****************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved          *
 *                                                               *
 * $Id: webuniqcmds.c,v 1.2 2013/06/26 12:14:17 siva Exp $           *
 *                                                               *
 *  Description: typedef for web privilege mapping               *
 ****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <osxstd.h>
#include "webpriv.h"

#define CLICOMMAND 1
#define CLIMODE    2
#define URLNAME    3
#define CLICOMMAND_COUNT     5
#define CLIMODE_COUNT        6
#define URLNAME_COUNT        7

FILE               *fd;

INT4                StrSearch (char *i1InputCommand, int i4CommandIndex,
                               char i1Mode);
INT4
StrSearch (char *i1InputCommand, int i4CommandIndex, char i1Mode)
{
    INT4                i4Index = 0;
    INT4                i4ArrayCnt = 0;
    char               *i1Tempbuffer = NULL;

    for (i4Index = i4CommandIndex + 1;
         gPagePrivMapping[i4Index].ai1CliCmdMode[0] != '\0'; i4Index++)
    {
        switch (i1Mode)
        {
            case CLICOMMAND:
            case CLICOMMAND_COUNT:
                i1Tempbuffer = gPagePrivMapping[i4Index].ai1CliCmd;
                break;
            case CLIMODE:
            case CLIMODE_COUNT:
                i1Tempbuffer = gPagePrivMapping[i4Index].ai1CliCmdMode;
                break;
            case URLNAME:
            case URLNAME_COUNT:
                i1Tempbuffer = (char *) gPagePrivMapping[i4Index].au1UrlName;
                break;
        }
        if (!strcmp (i1InputCommand, i1Tempbuffer))
        {
            return 0;
        }
    }
    if (i1Mode == CLICOMMAND_COUNT || i1Mode == CLIMODE_COUNT
        || i1Mode == URLNAME_COUNT)
    {
        i4ArrayCnt++;
    }
    else
    {
        fprintf (fd, "\t\"%s\",\n", i1InputCommand);
    }
    return i4ArrayCnt;
}

INT4
main (VOID)
{
    INT4                i4CommandIndex = 0;
    INT4                i4CliCmdCount = 0, i4CliCmdModeCount =
        0, i4UrlNameCount = 0;

    fd = fopen ("webprivds.h", "w+");
    if (fd == NULL)
    {
        printf ("Failed to create file\n");
        return -1;
    }

    fprintf (fd,
             "/*This is an auto-generated file. So please do not modify anything*/\n\n");
    fprintf (fd, "#ifndef _WEBPRIVDS_H_\n");
    fprintf (fd, "#define _WEBPRIVDS_H_\n");
    /*Count of cli commands */
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].ai1CliCmd[0] != '\0';
         i4CommandIndex++)
    {
        i4CliCmdCount +=
            StrSearch (gPagePrivMapping[i4CommandIndex].ai1CliCmd,
                       i4CommandIndex, CLICOMMAND_COUNT);
    }

    /*Generate cli commands */
    fprintf (fd, "CHR1 *giCliCommand[%d] =\n", i4CliCmdCount + 1);
    fprintf (fd, "{\n");
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].ai1CliCmd[0] != '\0';
         i4CommandIndex++)
    {
        StrSearch (gPagePrivMapping[i4CommandIndex].ai1CliCmd, i4CommandIndex,
                   CLICOMMAND);
    }
    fprintf (fd, "\t\"\"\n");
    fprintf (fd, "};\n");

    /*Count of cli modes */
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].ai1CliCmdMode[0] != '\0';
         i4CommandIndex++)
    {
        i4CliCmdModeCount +=
            StrSearch (gPagePrivMapping[i4CommandIndex].ai1CliCmdMode,
                       i4CommandIndex, CLIMODE_COUNT);
    }

    /*Generate CLI Mode */
    fprintf (fd, "CHR1 *giCliCmdMode[%d] =\n", i4CliCmdModeCount + 1);
    fprintf (fd, "{\n");
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].ai1CliCmdMode[0] != '\0';
         i4CommandIndex++)
    {
        StrSearch (gPagePrivMapping[i4CommandIndex].ai1CliCmdMode,
                   i4CommandIndex, CLIMODE);
    }
    fprintf (fd, "\t\"\"\n");
    fprintf (fd, "};\n");

    /*Count of url names */
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].au1UrlName[0] != '\0';
         i4CommandIndex++)
    {
        i4UrlNameCount +=
            StrSearch ((char *) gPagePrivMapping[i4CommandIndex].au1UrlName,
                       i4CommandIndex, URLNAME_COUNT);
    }

    /*Generate url name */
    fprintf (fd, "CHR1 *giUrlName[%d] =\n", i4UrlNameCount + 1);
    fprintf (fd, "{\n");
    for (i4CommandIndex = 0;
         gPagePrivMapping[i4CommandIndex].au1UrlName[0] != '\0';
         i4CommandIndex++)
    {
        StrSearch ((char *) gPagePrivMapping[i4CommandIndex].au1UrlName,
                   i4CommandIndex, URLNAME);
    }
    fprintf (fd, "\t\"\"\n");
    fprintf (fd, "};\n");
    fprintf (fd, "#endif\n");
    fclose (fd);
    return 0;
}
