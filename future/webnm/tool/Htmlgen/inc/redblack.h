/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: redblack.h,v 1.1 2014/12/16 10:46:59 siva Exp $
 *
 * Description: This file has declarations for rbtreee implementation
 *
 ***********************************************************************/

#ifndef _REDBLACK_H
#define _REDBLACK_H

/** Red-black Trees *********************************************************/
typedef struct __ytnode__ YTNODE;
typedef struct __ytree__ YTREE;

struct __ytnode__ {
        YTNODE *child[2];
        YTNODE *parent; 
        unsigned char red;
        UINT1   au1Pad[3];
};

struct  __ytree__ {
        YTNODE
                root;
        INT4
                (*compare)(void *, void *);     
        void
                *(*keyof)(YTREE *T, YTNODE *);
        unsigned int    
                count;
};

enum {
        RB_SUCCESS = 0, 
        RB_FAILURE
};

enum eRBNodeType { 
        RB_EMBD_NODE = 0, 
        RB_NOT_EMBD_NODE 
};

typedef enum {
        RB_WALK_BREAK, 
        RB_WALK_CONT
} eRBWalkReturns;

typedef enum {
        preorder, 
        postorder, 
        endorder, 
        leaf
} eRBVisit;

typedef void           tRBElem;

typedef INT4 (*tRBCompareFn) (tRBElem *e1, tRBElem *e2);
typedef INT4 (*tRBKeyFreeFn) (tRBElem *elem, UINT4 u4Arg);
typedef INT4 (*tRBWalkFn) (tRBElem *e, eRBVisit, UINT4 level, void *arg, void *out);

typedef struct rbnode
{
    YTNODE
            __node__;
    tRBElem
            *key;
} tRBNode;

typedef struct RBNodeEmbd{
        YTNODE __node__;
} tRBNodeEmbd;

typedef struct rbtree
{
    YTREE __tree__;

    enum eRBNodeType
            NodeType;
    UINT4
            u4Offset;

    tRBNode
       *next_cache;
    UINT1  au1Pad[3];
} *tRBTree;

#define RB_OFFSET_SCAN(T,pCurrent,pNext,TypeCast)\
        for(pCurrent = (TypeCast)RBTreeGetFirst(T),\
            pNext = (TypeCast)RBTreeGetNext(T,(tRBElem *)pCurrent,0);\
            pCurrent != 0; \
            pCurrent = pNext,\
            pNext = (pCurrent == 0) ? 0 :\
            (TypeCast)RBTreeGetNext(T,(tRBElem *)pCurrent,0))

/* In Linux kernel assert is not supported, so insmod of FutureKernel.o
 * will fail. Hence, assert function is used only if LINUX_KERN
 * is not defined. */
#ifndef LINUX_KERN
#define RB_ASSERT(s) assert(s)
#else
#define RB_ASSERT(s)
#endif

UINT4        RBTreeLibInit (void);
void         RBTreeLibShut (void);
tRBTree      RBTreeCreate(UINT4 u4NumNodes, tRBCompareFn Cmp);
tRBTree      RBTreeCreateEmbedded(UINT4 u4Offset, tRBCompareFn Cmp);
tRBTree      RBTreeCreateEmbeddedExt(UINT4 u4Offset, tRBCompareFn Cmp); 
void         RBTreeDelete(tRBTree);
void         RBTreeDestroy(tRBTree, tRBKeyFreeFn FreeFn, UINT4 u4Arg);
void         RBTreeDrain(tRBTree, tRBKeyFreeFn FreeFn, UINT4 u4Arg);
UINT4        RBTreeAdd(tRBTree, tRBElem *);
UINT4        RBTreeRemove(tRBTree, tRBElem *);
tRBElem *    RBTreeRem (struct rbtree *rbinfo, tRBElem * key);
tRBElem *    RBTreeGet(tRBTree, tRBElem *);
tRBElem *    RBTreeGetFirst(tRBTree);
tRBElem *    RBTreeGetNext(tRBTree, tRBElem *, tRBCompareFn);
void         RBTreeWalk (tRBTree, tRBWalkFn, void *arg, void *out);
UINT4        RBTreeCount (tRBTree, UINT4 *);

#endif
