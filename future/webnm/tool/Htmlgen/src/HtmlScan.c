/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: HtmlScan.c,v 1.6 2017/06/13 13:14:25 siva Exp $
 *
 * Description: This file has routine to generate Headerfile for HTML files
 *
 ***********************************************************************/
#include "main.h"

INT4
HtmlScan (INT1 *path)
{
    FILE               *fp;
    FILE               *final;
    FILE               *hp;
    FILE               *fpObject;
    INT1               *pi1path;
    INT4                i4Counter = 0;
    UINT1               i1Total;
    INT4                i4HFCount = 0;
    INT4                i4, i4Length;
    INT1                ai1FileName[256];
    INT1                ai1File[50];
    INT1                ai1FileSize[20];

    pi1path = MEM_MALLOC (MAX_PATH_LEN, INT1);
    if (pi1path == NULL)
    {
        printf ("\n Unable to allocate memory\n");
        return FAILURE;
    }
    strcpy (pi1path, "ls -l ");
    strcat (pi1path, path);
    strcat (pi1path, "/*.html >htmlfiles");
    i1Total = system (pi1path);
    if (i1Total == 127)
    {
        printf ("\n Couldn't find HTML Files in this directory");
        exit (1);
    }

    if (ReadHTMLFileList (path) != SUCCESS)
    {
        return FAILURE;
    }
    unlink (HTML_FILES);
    return 0;
}

INT1
ReadHTMLFileList (INT1 *pi1Path)
{
    INT4                i4Count = 0;
    INT1                ai1FileName[256];
    INT1                ai1File[MAX_FILE_NAME];
    INT1               *pi1FileName;
    INT4                i4Len;
    INT1                ai1FileSize[20];
    INT4                i4FileSize1;
    FILE               *fp;
    FILE               *SrcFile;

    if ((fp = fopen (HTML_FILES, "r")) == NULL)
    {
        printf ("\n Error html list file open");
        return FAILURE;
    }
    /* User May enter the path without terminating '/' character 
     * */
    i4Len = strlen (pi1Path);
    if (pi1Path[i4Len - 1] != '/')
    {
        pi1Path[i4Len] = '/';
        pi1Path[i4Len + 1] = 0;
    }
    i4Len = 0;
    while (fgets (ai1FileName, 256, fp) != NULL)
    {
        sscanf (ai1FileName, "%*s%*s%*s%*s%s%*s%*s%*s%*s", ai1FileSize);
        i4FileSize1 = atoi (ai1FileSize);
        pi1FileName = (INT1 *) strrchr (ai1FileName, '/');
        pi1FileName++;
        i4Len = strlen (pi1FileName);
        if (pi1FileName[i4Len - 1] == '\n')
        {
            pi1FileName[i4Len - 1] = '\0';
            i4Len--;
        }

        memset (ai1File, 0, MAX_FILE_NAME);
        STRNCPY (ai1File, pi1Path, MAX_PATH_LEN);
        STRNCAT (ai1File, pi1FileName, (MAX_FILE_NAME - MAX_PATH_LEN));

        if ((SrcFile = fopen (ai1File, "r")) == NULL)
        {
            printf ("Error Html files open for %s\n", ai1File);
            continue;
        }
        phtml1[i4Count].pi1Name = MEM_MALLOC (i4Len + 1, INT1);
        if (phtml1[i4Count].pi1Name == NULL)
        {
            printf ("\n Unable to Allocate memory for filename");
            return FAILURE;
        }
        strncpy (phtml1[i4Count].pi1Name, pi1FileName, i4Len);
        phtml1[i4Count].pi1Name[i4Len] = '\0';
        phtml1[i4Count].i4Size = i4FileSize1;
        phtml1[i4Count].pi1Ptr = MEM_MALLOC (i4FileSize1 + 1, INT1);
        if (phtml1[i4Count].pi1Ptr == NULL)
        {
            printf ("Unable to allocate memory for file to copy\n");
            return FAILURE;
        }
        if (fread (phtml1[i4Count].pi1Ptr, 1, i4FileSize1, SrcFile) !=
            (UINT4) i4FileSize1)
        {
            printf ("\n Error in file read\n");
        }
        phtml1[i4Count].pi1Ptr[i4FileSize1] = '\0';
        i4Count++;
        if (i4Count == HTML_MAX_NO_FILES)
        {
            printf ("\n Number of Files exceeds,copies only first % d files ",
                    i4Count);
            return SUCCESS;
        }
        fclose (SrcFile);
    }
    phtml1[i4Count].pi1Name = NULL;
    fclose (fp);
    return SUCCESS;
}

INT4
HSCheckHtmlPage (char *PageName)
{
    INT4                i4Counter;
    for (i4Counter = 0; i4Counter < HTML_MAX_NO_FILES; i4Counter++)
    {
        if (phtml1[i4Counter].pi1Name == 0)
            return FAILURE;
        if (STRCMP (phtml1[i4Counter].pi1Name, PageName) == 0)
            return (phtml1[i4Counter].i4Size);
    }
    return FAILURE;
}

INT1               *
HSReadHTMLPage (char *PageName)
{
    INT4                i4Counter;
    for (i4Counter = 0; i4Counter < HTML_MAX_NO_FILES; i4Counter++)
    {
        if (phtml1[i4Counter].pi1Name == NULL)
        {
            return NULL;
        }
        if (STRCMP (phtml1[i4Counter].pi1Name, PageName) == 0)
        {
            return ((INT1 *) phtml1[i4Counter].pi1Ptr);
        }
    }
    return NULL;
}

INT1
ParseHTMLForSNMPVariables (t_html * pg, INT1 **ppi1Scalarvarlist,
                           INT1 **ppi1Tablevarlist)
{
    INT4                i4Length = 0, i4ScalarCount = 0, i4TableCount = 0;
    INT1                pi1Name[WEBNM_MAX_NAME_LENGTH] = "";
    INT1               *pi1fc1, *pi1fc2;
    INT1               *pi1fcstart, *pi1fcend;
    INT1               *pi1fc = &pg->element[0];
    INT1                i1Parent = -1;
    INT1                ai1Empty[] = "";
    ppi1Scalarvarlist[0] = NULL;
    ppi1Tablevarlist[0] = NULL;
    pi1fcstart =
        (INT1 *) strstr ((const char *) pi1fc,
                         (const char *) "snmpvaluesstart");
    if (pi1fcstart == NULL)
        return FAILURE;
    pi1fcend =
        (INT1 *) strstr ((const char *) pi1fcstart,
                         (const char *) "snmpvaluesend");
    if (pi1fcend == NULL)
        return FAILURE;
    pi1fc1 = pi1fcstart;
    while ((pi1fc1 + 1) < pi1fcend)
    {
        pi1fc1 = (INT1 *) STRCHR (pi1fc1, '!');
        if (pi1fc1 >= pi1fcend)
            break;
        pi1fc2 = (INT1 *) STRCHR (pi1fc1, '>');
        if (pi1fc2 >= pi1fcend)
            break;
        i4Length = (pi1fc2 - pi1fc1 - 2);
        if (i4Length >= WEBNM_MAX_NAME_LENGTH)
        {
            MEM_FREE (ppi1Scalarvarlist);
            MEM_FREE (ppi1Tablevarlist);
            return MIB_NAME_EXCEEDS_MAX;
        }
        STRNCPY (pi1Name, (pi1fc1 + 2), i4Length);
        pi1Name[i4Length - 1] = '\0';
        i1Parent = CheckParent (pi1Name);
        if (i1Parent == FAILURE)
        {
            printf ("\n%s MIB Variable is not in MIB database ..\n", pi1Name);
            return MIB_NOT_IN_DB;
        }
        if (i1Parent != TABLE)
        {
            ppi1Scalarvarlist[i4ScalarCount] = MEM_MALLOC (i4Length, INT1);
            if (ppi1Scalarvarlist == NULL)
            {
                MEM_FREE (ppi1Scalarvarlist);
                MEM_FREE (ppi1Tablevarlist);
                return FAILURE;
            }
            ppi1Scalarvarlist[i4ScalarCount + 1] = NULL;
            STRNCPY (ppi1Scalarvarlist[i4ScalarCount], pi1Name, i4Length);
            i4ScalarCount++;
        }
        else
        {
            ppi1Tablevarlist[i4TableCount] = MEM_MALLOC (i4Length, INT1);
            if (ppi1Tablevarlist == NULL)
            {
                MEM_FREE (ppi1Scalarvarlist);
                MEM_FREE (ppi1Tablevarlist);
                return FAILURE;
            }

            ppi1Tablevarlist[i4TableCount + 1] = NULL;
            STRNCPY (ppi1Tablevarlist[i4TableCount], pi1Name, i4Length);
            i4TableCount++;
        }
        pi1fc1 = pi1fc2;
    }
    return SUCCESS;
}

INT1
ParseHTMLForIndexVariables (t_html * pg, INT1 **ppi1Indexvarlist)
{
    INT4                i4Length = 0, i4Count = 0;
    INT1                pi1Name[WEBNM_MAX_NAME_LENGTH] = "";
    INT1               *pi1fc1, *pi1fc2;
    INT1               *pi1fcstart, *pi1fcend;
    INT1               *pi1fc = &pg->element[0];

    if (pi1fc == NULL)
        return FAILURE;
    pi1fcstart =
        (INT1 *) strstr ((const char *) pi1fc, (const char *) "indexstart");
    if (pi1fcstart == NULL)
        return FAILURE;
    pi1fcend =
        (INT1 *) strstr ((const char *) pi1fcstart, (const char *) "indexend");
    if (pi1fcend == NULL)
        return FAILURE;
    pi1fc1 = pi1fcstart;
    ppi1Indexvarlist[0] = NULL;
    while ((pi1fc1 + 1) < pi1fcend)
    {
        pi1fc1 = (INT1 *) STRCHR (pi1fc1, '!');
        if (pi1fc1 >= pi1fcend)
            break;
        pi1fc2 = (INT1 *) STRCHR (pi1fc1, '>');
        if (pi1fc2 >= pi1fcend)
            break;
        i4Length = (pi1fc2 - pi1fc1 - 2);
        STRNCPY (pi1Name, (pi1fc1 + 2), i4Length);
        if (pi1Name[i4Length - 2] == ' ')
        {
            pi1Name[i4Length - 2] = '\0';
        }
        else
        {
            pi1Name[i4Length - 1] = '\0';
        }
        ppi1Indexvarlist[i4Count] = MEM_MALLOC (i4Length, INT1);
        if (ppi1Indexvarlist[i4Count] == NULL)
        {
            printf ("unable to allocate memory for ppi1Indexvarlist\n");
            return FAILURE;
        }
        ppi1Indexvarlist[i4Count + 1] = NULL;
        STRNCPY (ppi1Indexvarlist[i4Count], pi1Name, i4Length);
        i4Count++;
        pi1fc1 = pi1fc2;
    }
    return SUCCESS;
}
