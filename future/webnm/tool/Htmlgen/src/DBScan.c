
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: DBScan.c,v 1.10 2017/06/13 13:14:25 siva Exp $
 *
 * Description: This file has routine to generate a header file for DBfiles
 *
 ***********************************************************************/
#include "main.h"

#if !RBTREE
t_MMI_LINK_LIST    *main_list = 0;
t_MMI_LINK_LIST    *FirstNode = 0;
#endif

static int          compare_o_n (void *, void *);

static int          compare_o_n2 (void *, void *);

#if !RBTREE
struct t_MMI_DB     MMI_List[] = {
    {"internet", "1.3.6.1", 0},
    {"mgmt", "1.3.6.1.2", 0},
    {"private", "1.3.6.1.4", 0},
    {"experimental", "1.3.6.1.6", 0},
    {"mib-2", "1.3.6.1.2.1", 0},
    {"transmission", "1.3.6.1.2.1.10", 0},
    {"enterprise", "1.3.6.1.4.1", 0},
    {"futuresoft", "1.3.6.1.4.1.2076", 0},
    {0, 0, 0}
};
#endif

INT4
DBScan (INT1 *path)
{
    INT4                i4Counter = 0;
    INT4                i4Check = 0;
    INT1               *pi1path;
    UINT1               ui1Total;
    UINT4               u4Dummy = 0;

#if RBTREE
    O_N_Tree = RBTreeCreateEmbedded (0, compare_o_n);
    O_N_Tree2 =
        RBTreeCreateEmbedded ((UINT4
                               *) ((&(((struct o_n_ds *) 0)->oid_name_node2))),
                              compare_o_n2);
#endif
    pi1path = MEM_MALLOC (MAX_PATH_LEN, INT1);
    if (pi1path == NULL)
    {
        printf ("\n Unable to allocate memory\n");
        return FAILURE;
    }
    strcpy (pi1path, "ls ");
    strcat (pi1path, path);
    strcat (pi1path, "/*.db >DBfiles");
    ui1Total = system (pi1path);
    if (ui1Total == 127)
    {
        printf ("\n Couldn't find HTML Files in this directory");
        exit (1);
    }

    if (db_init () == FAILURE)
        return FAILURE;

}

INT1
db_init ()
{
    INT1               *pi1dbfiles[NO_OF_FILES];

    if (ReadFileList (pi1dbfiles) == FAILURE)
        return FAILURE;
    if (Readdbfile (pi1dbfiles) == FAILURE)
        return FAILURE;

#if !RBTREE
    RegisterDB ();
#endif
    return SUCCESS;
}

INT1
ReadFileList (INT1 **pi1List)
{
    INT4                i4Count = 0;
    INT1                pi1FileName[FILE_NO_OF_CHAR_IN_A_LINE];
    INT4                i4Len;
    FILE               *fp;

    pi1List[0] = NULL;
    if ((fp = fopen (DB_FILES, "r")) == NULL)
    {
        printf ("\n Error dbfiles open");
        return FAILURE;
    }
    while (fgets (pi1FileName, FILE_NO_OF_CHAR_IN_A_LINE, fp) != NULL)
    {
        i4Len = strlen (pi1FileName);
        pi1FileName[i4Len - 1] = '\0';
        pi1List[i4Count] = MEM_MALLOC (i4Len, INT1);
        if (pi1List[i4Count] == NULL)
        {
            printf ("Memory alloc fails in ReadfileList Routine\n");
            return FAILURE;
        }
        strncpy (pi1List[i4Count], pi1FileName, i4Len);
        i4Count++;
        if (i4Count == NO_OF_FILES)
        {
            printf ("\n Number of Files exceeds,copies only first 1000 files");
            pi1List[i4Count] = NULL;
            return SUCCESS;
        }
        pi1List[i4Count] = NULL;
    }
    fclose (fp);
    unlink (DB_FILES);
    return SUCCESS;
}

INT1
Readdbfile (INT1 **pi1dbfiles)
{
    FILE               *dbfp;
    FILE               *tfp;
    INT4                i4Count = 0, i1Count = 0;
    UINT4               u4FileCount = 0;
    INT1                ai1String[FILE_NO_OF_CHAR_IN_A_LINE] = "";
    INT1                bi1String[FILE_NO_OF_CHAR_IN_A_LINE] = "";
    INT1                ignore[3] = "";
    INT4                i4Len;
    struct o_n_ds      *pds = NULL;

    while (pi1dbfiles[u4FileCount] != NULL)
    {
#if !RBTREE
        if ((dbfp = fopen (pi1dbfiles[u4FileCount], "r")) == NULL)
        {
            printf ("unable to open \n");
            u4FileCount++;
            continue;
        }
        i4Count = 0;
        pdb[i1Count] = 0;
        while (fscanf (dbfp, "%[^:]", ai1String) != EOF)
        {
            pdb[i1Count] =
                MEM_REALLOC (pdb[i1Count],
                             sizeof (t_MIB_DB) * (i4Count + 2), t_MIB_DB);
            if (pdb[i1Count] == NULL)
            {
                printf ("Memory Realloc fails in Readfile Routine\n");
                return FAILURE;
            }
            fseek (dbfp, 1, 1);
            i4Len = strlen (ai1String);
            pdb[i1Count][i4Count].pi1name = MEM_MALLOC (i4Len + 1, INT1);
            if (pdb[i1Count][i4Count].pi1name == NULL)
            {
                printf ("Memory alloc fails in Readfile Routine:1\n");
                return FAILURE;
            }
            strncpy (pdb[i1Count][i4Count].pi1name, ai1String, i4Len + 1);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            sscanf (ai1String, "%u", &pdb[i1Count][i4Count].u4child);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            sscanf (ai1String, "%u", &pdb[i1Count][i4Count].u4peer);

            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            pdb[i1Count][i4Count].u1type = atoi (ai1String);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            pdb[i1Count][i4Count].u1access = atoi (ai1String);
            fscanf (dbfp, "%[^\n]", ai1String);
            fseek (dbfp, 1, 1);
            i4Len = strlen (ai1String);
            pdb[i1Count][i4Count].pi1oid = MEM_MALLOC (i4Len + 1, INT1);
            if (pdb[i1Count][i4Count].pi1oid == NULL)
            {
                printf ("Memory alloc fails in Readfile Routine:2\n");
                return FAILURE;
            }
            strncpy (pdb[i1Count][i4Count].pi1oid, ai1String, i4Len + 1);
#if DEBUG_ENABLE
            printf ("\n <LL Insert> <oid:%s> <name:%s> <type:%d> ",
                    pdb[i1Count][i4Count].pi1oid, pdb[i1Count][i4Count].pi1name,
                    pdb[i1Count][i4Count].u1type);
#endif
            i4Count++;
        }
        fclose (dbfp);

        pdb[i1Count][i4Count].pi1name = MEM_MALLOC (strlen ("NULL") + 1, INT1);
        if (pdb[i1Count][i4Count].pi1name == NULL)
        {
            printf ("Memory alloc fails in Readfile Routine:3\n");
            return FAILURE;
        }
        strncpy (pdb[i1Count][i4Count].pi1name, "NULL", strlen ("NULL"));
        pdb[i1Count][i4Count].pi1name[strlen ("NULL")] = '\0';
        i1Count++;
        u4FileCount++;
#else
        if ((tfp = fopen (pi1dbfiles[u4FileCount], "r")) == NULL)
        {
            printf ("unable to open \n");
            u4FileCount++;
            continue;
        }

        if (fgets (ignore, 3, tfp) != NULL)
        {
            /*IGNORE COMMENT */
            if (STRNCMP (ignore, "/*", 2) == 0)
            {
                ignore[0] = ignore[1];
                while (fgets (&ignore[1], 2, tfp) != NULL)
                {
                    if (STRNCMP (ignore, "*/", 2) == 0)
                    {
                        /*Move to next Line */
                        fscanf (tfp, "%[^\n]", bi1String);
                        fseek (tfp, 1, 1);
                        break;
                    }
                    else
                        ignore[0] = ignore[1];
                }
            }
            else
            {
                rewind (tfp);
            }
        }

        while (fscanf (tfp, "%[^:]", bi1String) != EOF)
        {
            pds = malloc (sizeof (o_n_ds));
            if (pds == NULL)
            {
#if DEBUG_ENABLE
                printf ("\n malloc failed for pds ");
#endif
            }
            fseek (tfp, 1, 1);
            i4Len = strlen (bi1String);
            pds->pi1name = malloc (i4Len + 1);
            if (NULL == pds->pi1name)
            {
#if DEBUG_ENABLE
                printf ("\n malloc failed for pds->pi1name ");
#endif
            }
            memset (pds->pi1name, 0, i4Len + 1);
            strncpy (pds->pi1name, bi1String, i4Len);
            fscanf (tfp, "%[^:]", bi1String);
            fseek (tfp, 1, 1);
            sscanf (bi1String, "%u", &(pds->u4child));
            fscanf (tfp, "%[^:]", bi1String);
            fseek (tfp, 1, 1);
            sscanf (bi1String, "%u", &(pds->u4peer));
            fscanf (tfp, "%[^:]", bi1String);
            fseek (tfp, 1, 1);
            pds->u1type = atoi (bi1String);
            fscanf (tfp, "%[^:]", bi1String);
            fseek (tfp, 1, 1);
            pds->u1access = atoi (bi1String);
            fscanf (tfp, "%[^\n]", bi1String);
            fseek (tfp, 1, 1);
            i4Len = strlen (bi1String);
            pds->pi1oid = malloc (i4Len + 1);
            memset (pds->pi1oid, 0, i4Len + 1);
            if (pds->pi1oid == NULL)
            {
#if DEBUG_ENABLE
                printf ("\n malloc failed for pds->pointer->pi1oid ");
#endif
            }
            strncpy (pds->pi1oid, bi1String, i4Len + 1);
            if (RBTreeAdd (O_N_Tree, (tRBElem *) pds) == RB_FAILURE)
            {
#if DEBUG_MORE_ENABLE
                printf
                    ("\n\r Handle Failure Here <array>   <name: %s> <oid: %s > ",
                     pds->pi1name, pds->pi1oid);
#endif
            }
            else
            {
#if DEBUG_MORE_ENABLE
                printf
                    ("\n\rSuccessfully Added in RBTree <aRRay> <name: %s> <oid: %s > <type: %d> <access: %d>",
                     pds->pi1name, pds->pi1oid, pds->u1type, pds->u1access);
#endif
            }
            if (RBTreeAdd (O_N_Tree2, (tRBElem *) pds) == RB_FAILURE)
            {
#if DEBUG_MORE_ENABLE
                printf ("\n RBTREE 2 Failed ");
#endif
            }
            else
            {
#if DEBUG_MORE_ENABLE
                printf ("\n RBTREE 2 Success");
#endif
            }

        }
        fclose (tfp);
        u4FileCount++;
#endif
    }
#if !RBTREE
    pdb[++i1Count] = 0;
#endif
    return SUCCESS;
}

#if !RBTREE
VOID
RegisterDB ()
{
    INT4                i4MaxCount = 0;
    INT4                i4Count = 0;
    INT1                i1check = 0;

    for (; pdb[i4MaxCount] != 0; i4MaxCount++)
    {
        for (i4Count = 0;
             STRCMP (pdb[i4MaxCount][i4Count].pi1name, "NULL") != 0; i4Count++)
        {
            if (i1check == SUCCESS)
            {
                RegisterWithNM ((const INT1 *) pdb[i4MaxCount][i4Count].pi1name,
                                (const INT1 *) pdb[i4MaxCount][i4Count].pi1oid,
                                pdb[i4MaxCount]);
            }
        }
    }
}

VOID
RegisterWithNM (const INT1 *pi1name, const INT1 *pi1oid, t_MIB_DB * mibptr)
{

    t_MMI_LINK_LIST    *temp, *newNode = NULL, *checkNode;

    checkNode = main_list;
    while (checkNode != NULL)
    {
        if (STRCMP (pi1oid, checkNode->pi1oid) == 0)
            return;
        checkNode = checkNode->link;
    }

    newNode = MEM_MALLOC (sizeof (t_MMI_LINK_LIST), t_MMI_LINK_LIST);
    if (newNode == NULL)
    {
        return;
    }                            /* mem alloc failed */
    newNode->pi1name = (char *) MEM_MALLOC (STRLEN (pi1name) + 1, INT1);
    if (newNode->pi1name == NULL)
    {
        if (newNode != NULL)
        {
            MEM_FREE (newNode);
            newNode = NULL;
        }
        return;
    }
    newNode->pi1oid = (char *) MEM_MALLOC (STRLEN (pi1oid) + 1, INT1);
    if (newNode->pi1oid == NULL)
    {
        if (newNode != NULL)
        {
            MEM_FREE (newNode);
            newNode = NULL;
        }
        if (newNode->pi1name != NULL)
        {
            MEM_FREE (newNode->pi1name);
            newNode->pi1name = NULL;
        }
        return;
    }
    STRNCPY (newNode->pi1name, pi1name, STRLEN (pi1name) + 1);
    STRNCPY (newNode->pi1oid, pi1oid, STRLEN (pi1oid) + 1);
    newNode->pointer = mibptr;
    newNode->link = 0;

    if (main_list == 0)
    {
        main_list = newNode;
    }
    else
    {
        temp = main_list;
        while (temp->link != 0)
            temp = temp->link;
        temp->link = newNode;
    }
}
#endif

INT1
GetOidStrFromName (pi1MibName, pi1OidStr)
     INT1               *pi1MibName, *pi1OidStr;
{
#if RBTREE

#if DEBUG_ENABLE
    printf ("\n Going to search for the name %s", pi1MibName);
#endif
    struct o_n_ds      *search_node = NULL;
    struct o_n_ds      *temp_node = NULL;
    temp_node = malloc (sizeof (struct o_n_ds));
    memset (temp_node, 0, sizeof (struct o_n_ds));
    temp_node->pi1name = malloc (strlen (pi1MibName) + 1);
    memset (temp_node->pi1name, 0, strlen (pi1MibName) + 1);
    strncpy (temp_node->pi1name, pi1MibName, strlen (pi1MibName));
    INT4                i4Count = 0, i4Len = 0;
    while (pi1MibName[i4Count] != '\0')
    {
        if (pi1MibName[i4Count] == '.')
            break;
        i4Count++;
    }
    i4Len = i4Count;
#if DEBUG_ENABLE
    printf ("\n RB-Tree get to be called for < %s >< %s >", temp_node->pi1name,
            pi1MibName);
#endif
    search_node = (struct o_n_ds *) RBTreeGet (O_N_Tree, (tRBElem *) temp_node);

    if (search_node != NULL)
    {
#if DEBUG_ENABLE
        printf ("\n IN  The <Name # Oid> <%s #%s", pi1MibName,
                search_node->pi1oid);
#endif
        STRCPY (pi1OidStr, search_node->pi1oid);
        STRCAT (pi1OidStr, pi1MibName + i4Len);
        return SUCCESS;
    }
    else
    {
#if DEBUG_ENABLE
        printf ("\n Null returned on RBTreeGet for <%s> ", temp_node->pi1name);
#endif
    }
#else
    INT4                i4Count = 0, i4Len = 0;
    t_MMI_LINK_LIST    *MMILocalLinkList;
    while (pi1MibName[i4Count] != '\0')
    {
        if (pi1MibName[i4Count] == '.')
            break;
        i4Count++;
    }
    i4Len = i4Count;
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (STRCMP (MMILocalLinkList->pi1name, pi1MibName) == 0)
        {
            STRCPY (pi1OidStr, MMILocalLinkList->pi1oid);
            STRCAT (pi1OidStr, pi1MibName + i4Len);
            return SUCCESS;
        }
        MMILocalLinkList = MMILocalLinkList->link;
    }
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (MMILocalLinkList->pointer != 0)
        {
            if (GetOidStrFromNameInChildDB
                (MMILocalLinkList->pointer, pi1MibName, i4Len, pi1OidStr) == 0)
            {
                STRCAT (pi1OidStr, pi1MibName + i4Len);
                return SUCCESS;
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
    }

#endif
    return FAILURE;
}

#if !RBTREE
INT1
GetOidStrFromNameInChildDB (MibDBMibPtr, pi1MibName, i4Len, pi1LocalOidStr)
     t_MIB_DB           *MibDBMibPtr;
     INT1               *pi1MibName;
     INT4                i4Len;
     INT1               *pi1LocalOidStr;
{
    INT4                i4Count;
    for (i4Count = 0;
         strncmp ((const char *) MibDBMibPtr[i4Count].pi1name,
                  (const char *) "NULL", STRLEN ("NULL")) != 0; i4Count++)
    {
        if (strncmp
            ((const char *) MibDBMibPtr[i4Count].pi1name, (char *) pi1MibName,
             i4Len) == 0)
        {
            strcpy (pi1LocalOidStr, (const char *) MibDBMibPtr[i4Count].pi1oid);
            return SUCCESS;
        }

    }
    return FAILURE;
}

UINT1
MMIGetNodeAccess (UINT4 u4Index)
{

    INT4                i4MainIndex = 0, i4SubIndex = 0, i4Count = 0;
    t_MIB_DB           *MibDBMibPtr = 0;
    t_MMI_LINK_LIST    *MMILocalLinkList = 0;
    if (u4Index != 0)
    {
        i4MainIndex = (INT4) (u4Index / 1000);
        i4SubIndex = (INT4) (u4Index % 1000);
    }
    if (main_list == NULL)
    {
        return 0;
    }
    MMILocalLinkList = main_list;
    while (i4Count < i4MainIndex)
    {
        MMILocalLinkList = MMILocalLinkList->link;
        if (MMILocalLinkList == NULL)
            return (BRANCH);
        i4Count++;
    }
    if (MMILocalLinkList->pointer != 0)
    {
        MibDBMibPtr = MMILocalLinkList->pointer;
        return (MibDBMibPtr[i4SubIndex].u1access);
    }
    return (MIB_READ_ONLY);

}

UINT1
MMIGetNodeType (UINT4 u4Index)
{
    INT4                i4MainIndex = 0, i4SubIndex = 0, i4Count = 0;
    t_MIB_DB           *MibDBMibPtr;
    t_MMI_LINK_LIST    *MMILocalLinkList;

    if (u4Index == 0)
        return (BRANCH);

    if (u4Index != 0)
    {
        i4MainIndex = (INT4) (u4Index / 1000);
        i4SubIndex = (INT4) (u4Index % 1000);
    }
    if (main_list == NULL)
    {
        return 0;
    }
    MMILocalLinkList = main_list;
    while (i4Count < i4MainIndex)
    {
        MMILocalLinkList = MMILocalLinkList->link;
        if (MMILocalLinkList == NULL)
            return (BRANCH);
        i4Count++;
    }
    if (MMILocalLinkList != 0 && MMILocalLinkList->pointer != 0)
    {
        MibDBMibPtr = MMILocalLinkList->pointer;
        return (MibDBMibPtr[i4SubIndex].u1type);
    }
    return (BRANCH);
}

INT4
GetIndexFromName (INT1 *pi1MibName)
{
    INT4                i4Count = 0, i4Count1;
    t_MIB_DB           *mibptr;
    t_MMI_LINK_LIST    *MMILocalLinkList;

    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (strcmp ((char *) pi1MibName, MMILocalLinkList->pi1name) == 0)
        {
            mibptr = MMILocalLinkList->pointer;
            if (mibptr != NULL)
            {
                for (i4Count1 = 0;
                     STRCMP (mibptr[i4Count1].pi1name, "NULL") != 0; i4Count1++)
                {
                    if (strcmp ((char *) pi1MibName, (const char *)
                                mibptr[i4Count1].pi1name) == 0)
                        return ((i4Count * 1000) + i4Count1);
                }
                return FAILURE;
            }
            else
            {
                return (i4Count * 1000);
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
        i4Count++;
    }
    i4Count = 0;
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (MMILocalLinkList->pointer != 0)
        {
            mibptr = MMILocalLinkList->pointer;
            for (i4Count1 = 0; STRCMP (mibptr[i4Count1].pi1name, "NULL") != 0;
                 i4Count1++)
            {
                if (strcmp
                    ((char *) pi1MibName,
                     (const char *) mibptr[i4Count1].pi1name) == 0)
                    return ((i4Count * 1000) + i4Count1);
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
        i4Count++;
    }
    return FAILURE;
}

VOID
RegisterRootMIB (const INT1 *pi1name, const INT1 *pi1oid, t_MIB_DB * mibptr)
{

    FirstNode = MEM_MALLOC (sizeof (t_MMI_LINK_LIST), t_MMI_LINK_LIST);
    if (FirstNode == NULL)
        return;
    FirstNode->pi1name = (char *) MEM_MALLOC (STRLEN (pi1name) + 1, INT1);
    if (FirstNode->pi1name == NULL)
    {
        if (FirstNode != NULL)
        {
            MEM_FREE (FirstNode);
            FirstNode = NULL;
        }
        return;
    }
    FirstNode->pi1oid = (char *) MEM_MALLOC (STRLEN (pi1oid) + 1, INT1);
    if (FirstNode->pi1oid == NULL)
    {
        if (FirstNode != NULL)
        {
            MEM_FREE (FirstNode);
            FirstNode = NULL;
        }
        if (FirstNode->pi1name != NULL)
        {
            MEM_FREE (FirstNode->pi1name);
            FirstNode->pi1name = NULL;
        }
        return;
    }
    STRNCPY (FirstNode->pi1name, pi1name, STRLEN (pi1name) + 1);
    STRNCPY (FirstNode->pi1oid, pi1oid, STRLEN (pi1oid) + 1);
    FirstNode->pointer = mibptr;
    FirstNode->link = main_list;
    main_list = FirstNode;
}
#endif

INT4
CheckParent (INT1 *pi1Name)
{
    INT4                i4Count = 0, i4DotCount = 0, i4LocalCount = 0, i4Index =
        0;
    INT1                ai1ParentOid[WEBNM_MAX_OID_LENGTH];
    INT1                pi1oidptr[WEBNM_MAX_OID_LENGTH];
    GetOidStrFromName (pi1Name, pi1oidptr);
    i4DotCount = CountDotsInOidString ((char *) pi1oidptr);
    while (1)
    {
        if (i4LocalCount == i4DotCount)
        {
            ai1ParentOid[i4Count] = '\0';
            break;
        }
        ai1ParentOid[i4Count] = pi1oidptr[i4Count];
        i4Count++;
        if (pi1oidptr[i4Count] == '.')
            i4LocalCount++;
    }

#if RBTREE
#if DEBUG_ENABLE
    printf ("\n !@#$   <index: %s > <oid:  %s> <type:  %d>", pi1Name, pi1oidptr,
            get_type_from_name (pi1Name));
#endif
    return (get_type_from_oid (ai1ParentOid));
#else
    i4Index = GetIndexFromOidStr (ai1ParentOid);
    if (i4Index == FAILURE)
        return FAILURE;

#if DEBUG_ENABLE
    printf ("\n  !@#$ <index: %s > <oid:  %s> <type:  %d>", pi1Name, pi1oidptr,
            MMIGetNodeType (i4Index));
#endif
    return (MMIGetNodeType (i4Index));

#endif
}

INT1
CountDotsInOidString (pi1Str)
     char               *pi1Str;
{
    INT4                i4Count, i4Count1 = 0;
    for (i4Count = 0; pi1Str[i4Count] != '\0'; i4Count++)
    {
        if (pi1Str[i4Count] == '.')
            i4Count1++;
    }
    return i4Count1;
}

#if !RBTREE
INT4
GetIndexFromOidStr (INT1 *Oid)
{
    t_MMI_LINK_LIST    *local_list;
    t_MIB_DB           *local_mibptr;
    INT4                i4Count = 0, i4Count1 = 0;
    local_list = main_list;
    while (local_list != 0)
    {
        if (local_list->pointer != 0)
        {
            local_mibptr = local_list->pointer;
            for (i4Count1 = 0;
                 STRCMP (local_mibptr[i4Count1].pi1name, "NULL") != 0;
                 i4Count1++)
            {
                if (STRCMP (local_mibptr[i4Count1].pi1oid, Oid) == 0)
                    return ((i4Count * 1000) + i4Count1);
            }
        }
        local_list = local_list->link;
        i4Count++;
    }
    return FAILURE;
}
#endif

#if RBTREE
static int
compare_o_n (tRBElem * pa, tRBElem * pb)
{
    struct o_n_ds      *a = (struct o_n_ds *) pa;
    struct o_n_ds      *b = (struct o_n_ds *) pb;

    int                 temp = 0;
    temp = strcmp (a->pi1name, b->pi1name);
    if (strcmp (a->pi1name, b->pi1name) == 0)
    {
#if DEBUG_MORE_ENABLE
        printf ("\n Both strings are equal #1 --> %s   #2 --> %s", a->pi1name,
                b->pi1name);
#endif
        return 0;
    }
    else if (strcmp (a->pi1name, b->pi1name) > 0)
    {
#if DEBUG_MORE_ENABLE
        printf ("\n #1 is big  #1 --> %s   #2 --> %s", a->pi1name, b->pi1name);
#endif
        return 1;
    }
    else
    {

#if DEBUG_MORE_ENABLE
        printf ("\n  #2 is big   #1 --> %s   #2 --> %s", a->pi1name,
                b->pi1name);
#endif
        return -1;
    }
}

static int
compare_o_n2 (tRBElem * pa, tRBElem * pb)
{
    struct o_n_ds      *a = (struct o_n_ds *) pa;
    struct o_n_ds      *b = (struct o_n_ds *) pb;
    int                 temp = 0;
    temp = strcmp (a->pi1oid, b->pi1oid);

    if (temp == 0)
    {
#if DEBUG_MORE_ENABLE
        printf ("\n Both strings are equal #1 --> %s   #2 --> %s", a->pi1name,
                b->pi1name);
#endif
        return 0;
    }
    else if (temp > 0)
    {
#if DEBUG_MORE_ENABLE
        printf ("\n #1 is big  #1 --> %s   #2 --> %s", a->pi1name, b->pi1name);
#endif
        return 1;
    }
    else
    {

#if DEBUG_MORE_ENABLE
        printf ("\n  #2 is big   #1 --> %s   #2 --> %s", a->pi1name,
                b->pi1name);
#endif
        return -1;
    }
}

UINT1
get_type_from_name (INT1 *pi1MibName)
{
#if DEBUG_ENABLE
    printf ("\n Going to search for the name %s", pi1MibName);
#endif
    struct o_n_ds      *search_node = NULL;
    struct o_n_ds      *temp_node = NULL;
    temp_node = malloc (sizeof (struct o_n_ds));
    memset (temp_node, 0, sizeof (struct o_n_ds));
    temp_node->pi1name = malloc (strlen (pi1MibName) + 1);
    memset (temp_node->pi1name, 0, strlen (pi1MibName) + 1);
    strncpy (temp_node->pi1name, pi1MibName, strlen (pi1MibName));
#if DEBUG_ENABLE
    printf ("\n RB-Tree get to be called for < %s >< %s >", temp_node->pi1name,
            pi1MibName);
#endif
    search_node = (struct o_n_ds *) RBTreeGet (O_N_Tree, (tRBElem *) temp_node);
    if (search_node != NULL)
    {
#if DEBUG_ENABLE
        printf ("\n IN  The <Name # Oid> <%s #%s", pi1MibName,
                search_node->pi1oid);
        printf ("\n The node<%s> type is %d ", pi1MibName, search_node->u1type);
#endif
        return search_node->u1type;
    }

}

UINT1
get_type_from_oid (INT1 *pi1oid)
{
#if DEBUG_ENABLE
    printf ("\n Going to search for the oid %s", pi1oid);
#endif
    struct o_n_ds      *search_node = NULL;
    struct o_n_ds      *temp_node = NULL;
    temp_node = malloc (sizeof (struct o_n_ds));
    memset (temp_node, 0, sizeof (struct o_n_ds));
    temp_node->pi1oid = malloc (strlen (pi1oid) + 1);
    memset (temp_node->pi1oid, 0, strlen (pi1oid) + 1);
    strncpy (temp_node->pi1oid, pi1oid, strlen (pi1oid));
#if DEBUG_ENABLE
    printf ("\n RB-Tree get to be called for < %s >< %s >", temp_node->pi1oid,
            pi1oid);
#endif
    search_node =
        (struct o_n_ds *) RBTreeGet (O_N_Tree2, (tRBElem *) temp_node);
    if (search_node != NULL)
    {
#if DEBUG_ENABLE
        printf ("\n IN  The <Name # Oid> <%s #%s", pi1oid,
                search_node->pi1name);
        printf ("\n The node<%s> type is %d ", pi1oid, search_node->u1type);
#endif
        return search_node->u1type;
    }

}

UINT1
get_access_from_name (INT1 *pi1MibName)
{
#if DEBUG_ENABLE
    printf ("\n Going to search for the name %s", pi1MibName);
#endif
    struct o_n_ds      *search_node = NULL;
    struct o_n_ds      *temp_node = NULL;
    temp_node = malloc (sizeof (struct o_n_ds));
    memset (temp_node, 0, sizeof (struct o_n_ds));
    temp_node->pi1name = malloc (strlen (pi1MibName) + 1);
    memset (temp_node->pi1name, 0, strlen (pi1MibName) + 1);
    strncpy (temp_node->pi1name, pi1MibName, strlen (pi1MibName));
#if DEBUG_ENABLE
    printf ("\n RB-Tree get to be called for < %s >< %s >", temp_node->pi1name,
            pi1MibName);
#endif
    search_node = (struct o_n_ds *) RBTreeGet (O_N_Tree, (tRBElem *) temp_node);
    if (search_node != NULL)
    {
#if DEBUG_ENABLE
        printf ("\n IN  The <Name # Oid> <%s #%s", pi1MibName,
                search_node->pi1oid);
        printf ("\n The Node <%s>Access is %d", pi1MibName,
                search_node->u1access);
#endif
        return search_node->u1access;
    }

}
#endif
