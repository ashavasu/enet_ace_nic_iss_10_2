/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: main.c,v 1.19 2017/06/13 13:14:26 siva Exp $
 *
 * Description: This file has routine to generate HTML files
 *
 ***********************************************************************/
#include "main.h"
INT1               *api1ChgOidSnmp[] = {
    "1.3.6.1.6.3.18.1.1.1.1",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.18.1.1.1.2",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.4.1.2076.149.1.4.5.0",    /* ENM_OCTETBITS */
    "1.3.6.1.4.1.2076.149.1.4.7.0",    /* ENM_OCTETBITS */
    "1.3.6.1.4.1.2076.149.1.5.6.0",    /* ENM_OCTETBITS */
    "1.3.6.1.4.1.2076.149.1.5.8.0",    /* ENM_OCTETBITS */
    "1.3.6.1.6.3.18.1.1.1.3",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.18.1.1.1.4",    /* ENM_OCTETSTRING */
    "1.3.6.1.6.3.18.1.1.1.5",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.18.1.1.1.6",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.2.1.2",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.2.1.3",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.2.1.3",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.4.1.1",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.4.1.5",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.4.1.6",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.4.1.7",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.13.1.1.1.1",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.13.1.1.1.2",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.12.1.2.1.1",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.12.1.2.1.3",    /* ENM_STR_IPADDRESS */
    "1.3.6.1.6.3.12.1.2.1.6",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.12.1.3.1.1",    /* ENM_IMP_DISPLAYSTRING */
    "1.3.6.1.6.3.15.1.2.2.1.2",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.15.1.2.2.1.6",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.15.1.2.2.1.9",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.16.1.5.2.1.1",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.2076.118.1.3.1.27",    /* ENM_DISPLAYSTRING */
    "1.2.840.10006.300.43.1.2.1.1.20",    /* ENM_BITS */
    "1.3.6.1.4.1.2076.63.3.1.1.4",    /* ENM_BITS */
    "1.3.6.1.2.1.16.3.1.1.11",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.16.9.1.1.4",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.16.9.1.1.6",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.16.2.1.1.6",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.16.1.1.1.20",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.17.7.1.2.2.1.1",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.17.7.1.4.2.1.4",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.2.1.17.7.1.4.2.1.5",    /* ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.2076.28.1.2.7.1.3",    /* ENM_OCTETSTRING */
    "1.3.6.1.4.1.2076.28.1.2.7.1.4",    /* ENM_INTEGER */
    "1.3.6.1.4.1.2076.20.1.2.3.1.5",    /* ENM_INTEGER */
    "1.3.6.1.2.1.158.1.1.1.6",    /* ENM_BITS */
    "1.3.6.1.4.1.2076.62.2.1.2.1.1",    /*ENM_DISPLAYSTRING - 
                                         *FsIsisExtSysAreaRxPasswd */
    "1.3.6.1.4.1.2076.62.2.1.3.1.1",    /*ENM_DISPLAYSTRING - 
                                         *FsIsisExtSysDomainRxPassword */
    "1.3.6.1.4.1.2076.62.2.2.2.1.5",    /* ENM_DISPLAYSTRING -
                                         * FsIsisExtCircLevelTxPassword */
    "1.3.6.1.6.3.14.1.2.1.1",    /*ENM_IMP_DISPLAYSTRING - snmpProxyName */
    "1.3.6.1.6.3.14.1.2.1.3",    /*ENM_OCTETSTRING - snmpProxyContextEngineID */
    "1.3.6.1.6.3.14.1.2.1.4",    /*ENM_IMP_DISPLAYSTRING - snmpProxyContextName */
    "1.3.6.1.6.3.14.1.2.1.5",    /*ENM_IMP_DISPLAYSTRING - snmpProxyTargetParamsIn */
    "1.3.6.1.6.3.14.1.2.1.6",    /*ENM_IMP_DISPLAYSTRING - snmpProxySingleTargetOut */
    "1.3.6.1.6.3.14.1.2.1.7",    /*ENM_IMP_DISPLAYSTRING - snmpProxyMultipleTargetOut */
    "1.3.6.1.4.1.2076.112.18.1.1",    /*ENM_IMP_DISPLAYSTRING - fsSnmpProxyMibName */
    "1.3.6.1.4.1.2076.112.18.1.4",    /*ENM_IMP_DISPLAYSTRING - FsSnmpProxyMibTargetParamsIn */
    "1.3.6.1.4.1.2076.112.18.1.5",    /*ENM_IMP_DISPLAYSTRING - FsSnmpProxyMibSingleTargetOut */
    "1.3.6.1.4.1.2076.112.18.1.6",    /*ENM_IMP_DISPLAYSTRING - FsSnmpProxyMibMultipleTargetOut */
    "1.3.6.1.4.1.29601.2.45.1.16.1.1.5",    /*ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.29601.2.45.1.16.1.1.4",    /*ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.29601.2.45.1.4.1.1.3",    /*ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.29601.2.45.1.4.1.1.2",    /*ENM_DISPLAYSTRING */
    "1.3.6.1.4.1.29601.2.45.1.10.1.1.7",    /*ENM_DISPLAYSTRING */
    "1.3.6.1.6.3.13.1.3.1.1",    /* ENM_IMP_OIDTYPE - snmpNotifyFilterSubtree */
    "1.3.6.1.4.1.2076.16.2.3.1.1",    /* ENM_OCTETSTRING - FwlFilterFilterName */
    "1.3.6.1.4.1.2076.14.1.12.1.3",    /* ENM_OCTETSTRING - natPolicyAclName */
    NULL
};

INT1               *api1ChgTypeSnmp[] = {
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_OCTETBITS",
    "ENM_OCTETBITS",
    "ENM_OCTETBITS",
    "ENM_OCTETBITS",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_OCTETSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_STR_IPADDRESS",
    "ENM_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_BITS",
    "ENM_BITS",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_OCTETSTRING",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_BITS",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_OCTETSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_IMP_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_IMP_OIDTYPE",
    "ENM_DISPLAYSTRING",
    "ENM_DISPLAYSTRING",
    NULL
};

INT1               *api1SyntaxSnmp[] = {
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OBJECTIDENTIFIER",
    "ENM_NETWORK_ADDRESS",
    "ENM_IPADDRESS",
    "ENM_TIMETICKS",
    "ENM_GAUGE",
    "ENM_COUNTER",
    "ENM_OPAQUE",
    "ENM_INTEGER",
    "ENM_GAUGE",
    "ENM_COUNTER",
    "ENM_COUNTER64",
    "ENM_UNSIGNED32",
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_TIMETICKS",
    "ENM_OCTETSTRING",
    "ENM_DISPLAYSTRING",
    "ENM_OIDTYPE",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_INTEGER",
    "ENM_OIDTYPE",
    "ENM_OIDTYPE",
    "ENM_OCTETSTRING",
    "ENM_INTEGER",
    "ENM_OIDTYPE",
    "ENM_OIDTYPE",
    "ENM_INTEGER",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "SYNTAX_NOT_DEF",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OCTETSTRING",
    "ENM_OIDTYPE",
    "ENM_OCTETSTRING",
    NULL
};
INT1                api1SyntaxSnmpNumber[] = {
    2,
    3,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    2,
    8,
    9,
    14,
    8,
    2,
    3,
    7,
    3,
    11,
    4,
    2,
    2,
    2,
    2,
    2,
    4,
    4,
    3,
    2,
    4,
    4,
    2,
    3,
    3,
    15,
    0
};

UINT1               au1Access[5][20] = {
    "READONLY",
    "READWRITE",
    "READCREATE",
    "NOACCESS",
    "ACCESSBILEFORNOTIFY"
};
INT4
main (argc, argv)
     INT4                argc;
     INT1               *argv[];
{
    UINT1               au1HtmlPath[MAX_PATH_LEN];
    UINT1               au1DBPath[128];
    UINT1               au1FileName[128];
    UINT1               au1NewFileName[128];
    UINT1               au1TempName[200];
    UINT1               au1Oid[500];
    INT1               *pi1Type;
    INT1                ai1buffer[256];
    INT4                i4Index, i4Count, i4Ret;
    INT1                i1Return;
    INT4                i1IndexRet;
    INT1                i1Flag = 0;
    UINT4               u4MibType;
    INT1               *pi1ScalarVariableList[WEBNM_MAX_VARS_IN_PAGE];
    INT1               *pi1TableVariableList[WEBNM_MAX_VARS_IN_PAGE];
    INT1               *ai1IndexVariableList[WEBNM_MAX_VARS_IN_PAGE];
    t_html              pgcontent;
    FILE               *fp;
    FILE               *fpout, *fpin;
    UINT4               u4Count;
    INT1               *pi1WritePtr = NULL;
    INT4                i4RetStatus = FAILURE;

    if (argc != 3)
    {
        fprintf (stderr,
                 "\nUsage: HTMLGEN <Directory of Htmlfiles>  <Directory of dbfiles> \n");
        exit (1);
    }

    STRNCPY (au1HtmlPath, argv[1], MAX_PATH_LEN);
    STRCPY (au1DBPath, argv[2]);
    HtmlScan ((INT1 *) &au1HtmlPath);
    DBScan ((INT1 *) &au1DBPath);
#if !RBTREE
    if (pdb[0] == NULL)
    {
        printf ("Mib is Not Loaded \n");
        exit (1);
    }
#else
    RBTreeCount (O_N_Tree, &u4Count);
    if (u4Count == 0)
    {
        printf ("Mib is Not Loaded \n");
        exit (1);
    }
#endif
    if (phtml1[0].pi1Name == NULL)
    {
        printf ("HTML Pages is Not Loaded \n");
        exit (1);
    }

    printf ("\nProcessing ............\n");

    pi1Type = MEM_MALLOC (MAX_DATATYPE_LEN, INT1);
    if (pi1Type == NULL)
    {
        printf ("unable to allocate memory for pi1Type\n");
        return FAILURE;
    }
    memset (pi1Type, 0, MAX_DATATYPE_LEN);

    for (u4Count = 0; phtml1[u4Count].pi1Name != NULL; u4Count++)
    {
        STRCPY (au1FileName, phtml1[u4Count].pi1Name);
        STRCPY (au1NewFileName, phtml1[u4Count].pi1Name);
        pgcontent.element = 0;
        pgcontent.size = 0;
        if (ReadSource (au1FileName, &pgcontent) == FAILURE)
        {
            printf ("Unable TO get the Source\n");
            MEM_FREE (pi1Type);
            return FAILURE;
        }
        i1Return = ParseHTMLForSNMPVariables (&pgcontent, pi1ScalarVariableList,
                                              pi1TableVariableList);
        if (i1Return == FAILURE || i1Return == MIB_NAME_EXCEEDS_MAX)
        {
            continue;
        }
        if (i1Return == -2)
        {
            printf ("File %s\n", au1FileName);
            exit (1);
        }
        i1Flag = ParseHTMLForIndexVariables (&pgcontent, ai1IndexVariableList);
        sprintf (au1FileName, "new%d.html", u4Count);
        if ((fp = fopen (au1FileName, "w")) == NULL)
        {
            printf ("\n Error html list file open");
            MEM_FREE (pi1Type);
            return FAILURE;
        }
        fprintf (fp, "%s", "\n");
        fprintf (fp, "%s", phtml1[u4Count].pi1Ptr);
        fprintf (fp, "%s", "\n");
        fclose (fp);
        if ((fpin = fopen (au1FileName, "r")) == NULL)
        {
            MEM_FREE (pi1Type);
            printf ("\n Error html list file open");
            return FAILURE;
        }
        if ((fpout = fopen (au1NewFileName, "w")) == NULL)
        {
            printf ("\n Error html list file open");
            MEM_FREE (pi1Type);
            return FAILURE;
        }
        /* All the necessary parameters are appended commonly
         * because, the page may contain both Scalar and Tabular
         * Objects
         * */
        if ((pi1ScalarVariableList[0] != NULL) ||
            (pi1TableVariableList[0] != NULL))
        {
            fprintf (fpout, "\n");
            fprintf (fpout, "<HTML>\n");
            fprintf (fpout, "<! snmpvaluesstart >\n");
        }

        if (pi1ScalarVariableList[0] != NULL)
        {
            for (i4Count = 0; pi1ScalarVariableList[i4Count] != NULL; i4Count++)
            {
                fprintf (fpout, "<! ");
                memset (au1TempName, 0, 200);
                STRCPY (au1TempName, pi1ScalarVariableList[i4Count]);
                memset (au1Oid, 0, 500);
                GetOidStrFromName (au1TempName, au1Oid);
#if DEBUG_MORE_ENABLE
                printf ("\n <ooid:%s # name:%s>", au1Oid, au1TempName);
#endif
                strcat (au1Oid, ".0");

#if RBTREE
                u4MibType = get_type_from_name (au1TempName);
#else

                i4Index = GetIndexFromName (au1TempName);
                if (i4Index == FAILURE)
                {
                    printf ("\nGetIndexFromName Failed for this %s\n",
                            au1TempName);
                    fprintf (fpout, "0:0:0 >\n");
                    continue;
                }
                u4MibType = MMIGetNodeType ((UINT4) i4Index);
#endif

                i4RetStatus = get_chg_oid (au1Oid, pi1Type);
                if (i4RetStatus == SUCCESS)
                {
                    /* OID type is changed for au1Oid
                       printf ("CHG OID %s TYPE %s\r\n",au1Oid,pi1Type); */
                }

                if (i4RetStatus == FAILURE)
                {
                    i4RetStatus = sm_get_snmp_type (u4MibType, pi1Type);
                    if (i4RetStatus == FAILURE)
                    {
                        printf ("Data Type wrongly specified for %s\n",
                                au1TempName);
                        return FAILURE;
                    }
                }
                strcat (au1Oid, ":");
                strcat (au1Oid, pi1Type);

#if RBTREE
                i4Ret = get_access_from_name (au1TempName);
#else
                i4Ret = MMIGetNodeAccess (i4Index);
#endif
                strcat (au1Oid, ":");
                strcat (au1Oid, au1Access[i4Ret]);
                fprintf (fpout, "%s", au1Oid);
                fprintf (fpout, " >\n");
            }
        }
        if (pi1TableVariableList[0] != NULL)
        {
            i1IndexRet = 0;
            for (i4Count = 0; pi1TableVariableList[i4Count] != NULL; i4Count++)
            {
#if DEBUG_ENABLE
                printf ("\t <count : %d> ", i4Count);
#endif
                fprintf (fpout, "<! ");
                STRCPY (au1TempName, pi1TableVariableList[i4Count]);
                GetOidStrFromName (au1TempName, au1Oid);
#if DEBUG_MORE_ENABLE
                printf ("\n <ooid:%s # name:%s>", au1Oid, au1TempName);
#endif
#if RBTREE
                u4MibType = get_type_from_name (au1TempName);
#else

                i4Index = GetIndexFromName (au1TempName);

                if (i4Index == FAILURE)
                {
#if DEBUG_MORE_ENABLE
                    printf ("\nGetIndexFromName Failed for this %s\n",
                            au1TempName);
#endif
                    fprintf (fpout, "0:0:0 >\n");
                    continue;
                }
                u4MibType = MMIGetNodeType ((UINT4) i4Index);
#endif

                i4RetStatus = get_chg_oid (au1Oid, pi1Type);
                if (i4RetStatus == SUCCESS)
                {
                    /* OID type is changed for au1Oid
                       printf ("CHG OID %s TYPE %s\r\n",au1Oid,pi1Type); */
                }
                if (i4RetStatus == FAILURE)
                {
                    i4RetStatus = sm_get_snmp_type (u4MibType, pi1Type);
                    if (i4RetStatus == FAILURE)
                    {
                        printf ("Data Type wrongly specified for %s\n",
                                au1TempName);
                        return FAILURE;
                    }
                }
                strcat (au1Oid, ":");
                strcat (au1Oid, pi1Type);

#if RBTREE
                i4Ret = get_access_from_name (au1TempName);
#else
                i4Ret = MMIGetNodeAccess (i4Index);
#endif
                strcat (au1Oid, ":");
                i1Flag = 0;
                for (i1IndexRet = 0; ai1IndexVariableList[i1IndexRet] != NULL;
                     i1IndexRet++)
                {
                    if (STRCMP (pi1TableVariableList[i4Count],
                                ai1IndexVariableList[i1IndexRet]) == 0)
                    {
                        strcat (au1Oid, "INDEX");
                        i1Flag = 1;
                        break;
                    }
                }
                if (i1Flag == 0)
                {
                    strcat (au1Oid, au1Access[i4Ret]);
                }
                fprintf (fpout, "%s", au1Oid);
                fprintf (fpout, " >\n");
            }
        }
        if ((pi1ScalarVariableList[0] != NULL) ||
            (pi1TableVariableList[0] != NULL))
        {
            fprintf (fpout, "<! snmpvaluesend >\n");
        }

        while (!feof (fpin))
        {
            fgets (ai1buffer, 80, fpin);
            if (strstr (ai1buffer, "snmpvaluesend >") != NULL)
                break;
        }
        while (!feof (fpin))
        {
            fgets (ai1buffer, 256, fpin);
            if (strstr (ai1buffer, "indexstart") != NULL)
            {
                while (!feof (fpin))
                {
                    fgets (ai1buffer, 80, fpin);
                    if (strstr (ai1buffer, "indexend >") != NULL)
                        break;
                }
            }
            else
            {
                pi1WritePtr = ai1buffer;
                pi1WritePtr = CheckAndWriteName (pi1WritePtr, fpout);
                pi1WritePtr = CheckAndWriteValue (pi1WritePtr, fpout);
                pi1WritePtr = CheckAndWriteKEY (pi1WritePtr, fpout);
                fputs (pi1WritePtr, fpout);
            }
        }
        fclose (fpin);
        unlink (au1FileName);
        fclose (fpout);
    }
    printf ("\n.......DONE\n");
    MEM_FREE (pi1Type);
}

INT1               *
CheckAndWriteName (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL;
    INT1               *pi1EndPtr = NULL;
    INT1                ai1MyName[1024];
    UINT1               au1Oid[500];
    INT4                i4WriteReqd = FALSE;
    INT1                i1Select = FALSE;

    memset (au1Oid, 0, 500);
    memset (ai1MyName, 0, 1024);

    if ((pi1StartPtr = strstr (pi1WritePtr, "NAME=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "name=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "name =")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "NAME =")) != NULL)
    {
        /* check for the special processing
         * <select name = 1.3.4.5>
         * since this won't get passed in the 
         * strstr (pi1WritePtr, "")
         * */
        if (((pi1EndPtr = strstr (pi1WritePtr, "select")) != NULL) ||
            ((pi1EndPtr = strstr (pi1WritePtr, "Select")) != NULL) ||
            ((pi1EndPtr = strstr (pi1WritePtr, "SELECT")) != NULL))
        {
            i1Select = TRUE;
        }

        pi1StartPtr += strlen ("name");
        fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);
        fwrite ("=", 1, 1, fpout);
        pi1WritePtr += (pi1StartPtr - pi1WritePtr);
        if ((pi1EndPtr = strstr (pi1WritePtr, "==")) != NULL)
        {
            /* This check is done because, in some functions
             * to check the value, we will use the format like
             * 'name == x'.
             * Since an = is already placed, placing one more
             * */
            fwrite ("=", 1, 1, fpout);
        }
        while (*pi1StartPtr == '=' || *pi1StartPtr == ' ')
        {
            pi1StartPtr++;
        }
        pi1WritePtr = pi1StartPtr;

        if (pi1StartPtr[0] == '"')
        {
            pi1StartPtr++;

            /* Find whether "reqd" option is given for this OID */
            if ((pi1EndPtr = strstr (pi1StartPtr, "reqd")) != NULL)
            {
                i4WriteReqd = TRUE;
                pi1StartPtr = pi1StartPtr + (strlen ("reqd"));
            }

            if ((pi1EndPtr = strstr (pi1StartPtr, "\"")) != NULL)
            {
                memcpy (ai1MyName, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyName[pi1EndPtr - pi1StartPtr] = '\0';
                if (GetOidStrFromName (ai1MyName, au1Oid) == SUCCESS)
                {
#if DEBUG_MORE_ENABLE
                    printf ("\n <ooid:%s # name:%s>", au1Oid, ai1MyName);
#endif
                    if (CheckParent (ai1MyName) != TABLE)
                    {
                        strcat (au1Oid, ".0");
                    }
                    fwrite ("\"", 1, 1, fpout);
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    fprintf (fpout, "%s", au1Oid);
                    fwrite ("\"", 1, 1, fpout);
                    pi1EndPtr++;
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    fwrite ("\"", 1, 1, fpout);
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    pi1WritePtr = pi1StartPtr;
                }
            }
        }
        else
        {
            /* Find whether "reqd" option is given for this OID */
            if ((pi1EndPtr = strstr (pi1StartPtr, "reqd")) != NULL)
            {
                i4WriteReqd = TRUE;
                pi1StartPtr = pi1StartPtr + (strlen ("reqd"));
            }

            if ((pi1EndPtr = strstr (pi1StartPtr, " ")) != NULL ||
                (i1Select == TRUE))
            {
                if ((i1Select == TRUE) && (pi1EndPtr == NULL))
                {
                    i1Select = FALSE;
                    pi1EndPtr = strstr (pi1StartPtr, ">");
                    if (pi1EndPtr == NULL)
                    {
                        return NULL;
                    }
                }
                memcpy (ai1MyName, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyName[pi1EndPtr - pi1StartPtr] = '\0';
                if (GetOidStrFromName (ai1MyName, au1Oid) == SUCCESS)
                {
#if DEBUG_MORE_ENABLE
                    printf ("\n <ooid:%s # name:%s>", au1Oid, ai1MyName);
#endif
                    if (CheckParent (ai1MyName) != TABLE)
                    {
                        strcat (au1Oid, ".0");
                    }
                    if (i4WriteReqd == TRUE)
                    {
                        fwrite ("reqd", strlen ("reqd"), 1, fpout);
                    }
                    fprintf (fpout, "%s", au1Oid);
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1WritePtr = pi1StartPtr;
                }
            }
        }
    }
    return pi1WritePtr;
}

INT1               *
CheckAndWriteValue (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL;
    INT1               *pi1EndPtr = NULL;
    INT1                ai1MyName[1024];
    UINT1               au1Oid[100];

    memset (ai1MyName, 0, 1024);
    memset (au1Oid, 0, 100);

    if ((pi1StartPtr = strstr (pi1WritePtr, "value=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "VALUE=")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "VALUE =")) != NULL ||
        (pi1StartPtr = strstr (pi1WritePtr, "value =")) != NULL)

    {
        pi1StartPtr += strlen ("value");
        fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);
        fwrite ("=", 1, 1, fpout);
        pi1WritePtr += (pi1StartPtr - pi1WritePtr);

        if ((pi1EndPtr = strstr (pi1WritePtr, "==")) != NULL)
        {
            /* This check is done because, in some functions
             * to check the value, we will use the format like
             * 'value == x'.
             * Since an = is already placed, placing one more
             * */
            fwrite ("=", 1, 1, fpout);
        }

        while (*pi1StartPtr == '=' || *pi1StartPtr == ' ')
        {
            pi1StartPtr++;
        }
        pi1WritePtr = pi1StartPtr;

        if (pi1StartPtr[0] == '"')
        {
            pi1StartPtr++;

            if (((pi1EndPtr = strstr (pi1StartPtr, "_KEY\"")) != NULL) ||
                ((pi1EndPtr = strstr (pi1StartPtr, "_KEY\" ")) != NULL))
            {
                fwrite ("\"", 1, 1, fpout);
                memcpy (ai1MyName, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyName[pi1EndPtr - pi1StartPtr] = '\0';

                if (GetOidStrFromName (ai1MyName, au1Oid) == SUCCESS)
                {
#if DEBUG_MORE_ENABLE
                    printf ("\n <ooid:%s # name:%s>", au1Oid, ai1MyName);
#endif
                    if (CheckParent (ai1MyName) != TABLE)
                    {
                        strcat (au1Oid, ".0");
                    }
                    strcat (au1Oid, "_KEY");
                    fprintf (fpout, "%s", au1Oid);
                    fwrite ("\"", 1, 1, fpout);
                    pi1EndPtr += strlen ("_KEY\"");
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1StartPtr--;
                    pi1WritePtr = pi1StartPtr;
                }

            }
        }
        else
        {
            if ((pi1EndPtr = strstr (pi1StartPtr, "_KEY")) != NULL)
            {
                memcpy (ai1MyName, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
                ai1MyName[pi1EndPtr - pi1StartPtr] = '\0';

                if (GetOidStrFromName (ai1MyName, au1Oid) == SUCCESS)
                {
#if DEBUG_MORE_ENABLE
                    printf ("\n <ooid:%s # name:%s>", au1Oid, ai1MyName);
#endif
                    if (CheckParent (ai1MyName) != TABLE)
                    {
                        strcat (au1Oid, ".0");
                    }
                    strcat (au1Oid, "_KEY");
                    fprintf (fpout, "%s", au1Oid);
                    pi1EndPtr += strlen ("_KEY");
                    pi1WritePtr = pi1EndPtr;
                }
                else
                {
                    pi1WritePtr = pi1StartPtr;
                }

            }
        }
    }
    return pi1WritePtr;
}

INT1               *
CheckAndWriteKEY (INT1 *pi1WritePtr, FILE * fpout)
{
    INT1               *pi1StartPtr = NULL, *pi1EndPtr = NULL;
    INT1                ai1MyName[1024], i1Test = 0;
    UINT1               au1Oid[100];
    if ((pi1EndPtr = strstr (pi1WritePtr, "_KEY")) != NULL)
    {
        pi1StartPtr = pi1EndPtr;
        while (pi1StartPtr >= pi1WritePtr)
        {
            if (*pi1StartPtr == ' ' ||
                *pi1StartPtr == '>' || *pi1StartPtr == '"')
            {
                if (*pi1StartPtr == '"')
                    i1Test = 1;
                else
                    i1Test = 0;
                break;
            }
            pi1StartPtr--;
        }
        pi1StartPtr++;
        if (i1Test == 1)
            fwrite (pi1WritePtr, ((pi1StartPtr - pi1WritePtr) - 1), 1, fpout);
        else
            fwrite (pi1WritePtr, (pi1StartPtr - pi1WritePtr), 1, fpout);

        memcpy (ai1MyName, pi1StartPtr, (pi1EndPtr - pi1StartPtr));
        ai1MyName[pi1EndPtr - pi1StartPtr] = '\0';
        if (GetOidStrFromName (ai1MyName, au1Oid) == SUCCESS)
        {
#if DEBUG_MORE_ENABLE
            printf ("\n <ooid:%s # name:%s>", au1Oid, ai1MyName);
#endif
            if (CheckParent (ai1MyName) != TABLE)
            {
                strcat (au1Oid, ".0");
            }
            strcat (au1Oid, "_KEY");
            fprintf (fpout, "%s ", au1Oid);
            if (i1Test == 1)
                pi1EndPtr += strlen ("_KEY\"");
            else
                pi1EndPtr += strlen ("_KEY");
            pi1WritePtr = pi1EndPtr;
        }
        else
        {
            pi1WritePtr = pi1StartPtr;
        }
    }
    return pi1WritePtr;
}

INT1
ReadSource (INT1 *pgName, t_html * pg)
{
    INT4                i4Size;
    INT1               *pi1Ptr;
    i4Size = HSCheckHtmlPage ((char *) pgName);
    if (i4Size == FAILURE)
    {
        return FAILURE;
    }
    if ((pi1Ptr = HSReadHTMLPage ((char *) pgName)) == NULL)
    {
        return FAILURE;
    }
    pg->element = MEM_MALLOC (i4Size + (i4Size / 2), INT1);
    if (pg->element == NULL)
    {
        printf ("unable to allocate memory for pg->element\n");
        return FAILURE;
    }
    STRNCPY (pg->element, pi1Ptr, i4Size);
    pg->element[i4Size] = '\0';
    pg->size = i4Size + 1;
    return SUCCESS;

}

INT1
sm_get_snmp_type (INT4 i2sm_type, INT1 *pi1Type)
{

    INT2                i1count = 0;
    while (api1SyntaxSnmpNumber[i1count] != 0)
    {
        if (api1SyntaxSnmpNumber[i1count] == i2sm_type)
        {
            strcpy (pi1Type, api1SyntaxSnmp[i1count]);
            return SUCCESS;
        }
        i1count++;
    }
    return FAILURE;
}

INT1
get_chg_oid (UINT1 *pi1OidStr, INT1 *pi1Type)
{

    INT2                i1count = 0;
    while (api1ChgOidSnmp[i1count] != NULL)
    {
        if (STRCMP (api1ChgOidSnmp[i1count], pi1OidStr) == 0)
        {
            strcpy (pi1Type, api1ChgTypeSnmp[i1count]);
            return SUCCESS;
        }
        i1count++;
    }
    return FAILURE;
}
