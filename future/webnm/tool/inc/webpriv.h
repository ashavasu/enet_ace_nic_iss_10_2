/*****************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved          *
 *                                                               *
 * $Id: webpriv.h,v 1.6 2014/08/08 11:03:51 siva Exp $           *
 *                                                               *
 *  Description: typedef for web privilege mapping               *
 *****************************************************************/
#ifndef _WEBPRIV_H
#define _WEBPRIV_H

#define WEB_URL_MAX_NAME_LEN 50
#define WEB_CLI_MODE_MAX_NAME_LEN 50
#define WEB_CLI_COMMAND_MAX_NAME_LEN 3000 


typedef struct PagePrivMapping {
    char        ai1CliCmd[WEB_CLI_COMMAND_MAX_NAME_LEN];
    char        ai1CliCmdMode[WEB_CLI_MODE_MAX_NAME_LEN];
    unsigned char       au1UrlName[WEB_URL_MAX_NAME_LEN];
    int        i4PrivId;

} tPagePrivMapping;
tPagePrivMapping gPagePrivMapping[] =
{
{
{"shutdown dot1x"},
{"CONFIGURE"},
{"pnac_basicinfo.html"},
0
},
{
{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},
{"USEREXEC"},
{"pnac_macsessionstats.html"},
0
},
{
{"dot1x port-control {auto|force-authorized|force-unauthorized}"},
{"INTERFACE"},
{"pnac_portconfig.html"},
0
},
{
{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},
{"USEREXEC"},
{"pnac_sessionstats.html"},
0
},
{
{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},
{"USEREXEC"},
{"pnac_suppsessionstats.html"},
0
},
{
{"dot1x timeout {quiet-period <value (0-65535)> | {reauth-period  | server-timeout  | supp-timeout  | tx-period  | start-period  | held-period  | auth-period }<value (1-65535)>}"},
{"INTERFACE"},
{"pnac_porttimer.html"},
0
},
{
{"v6access-list <accesslist-index> {<any> |<src-netmask> <src-addr} {<any> | <dest-netmask> <dest-addr>}"},
{"CRYPTO"},
{"secv6_accessconf.html"},
0
},
{
{"access-list <acl name > {in|out} <filter name> {permit|deny} <priority val> [log {brief|detail|none}]  [fragment {permit|deny}]"},
{"FIREWALL"},
{"fwl_aclconf.html"},
0
},
{
{"ip nat pool <global ip> <mask>"},
{"PPP"},
{"nat_dynamic.html"},
0
},
{
{"ipv6 nd prefix {<prefix addr> <prefixlen> | default} [{{<valid lifetime> | infinite | at <var valid lifetime>}{<preferred lifetime> |infinite | at <var preferred lifetime>} | no-advertise}] [off-link] [no-autoconfig] [embedded-rp]"},
{"INTERFACE_ROUTER"},
{"addrprof_conf.html"},
0
},
{
{"ipv6 address <prefix> <prefix Len> [{unicast | anycast | eui64}]"},
{"INTERFACE_ROUTER"},
{"addr_conf.html"},
0
},
{
{"client-layer-level <level-id(0-7)>"},
{"DOMAIN_CONFIG"},
{"ecfm_ais.html"},
0
},
{
{"show entity alias-mapping [index <integer (1..2147483647)>]"},
{"USEREXEC"},
{"ent_AliasMapping.html"},
0
},
{
{"set application-priority  {enable | disable}"},
{"INTERFACE "},
{"ap_adminportconfig.html"},
0
},
{
{"shutdown application-priority"},
{"CONFIGURE"},
{"ap_GlobalInfo.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] application-priority [detail]"},
{"USEREXEC"},
{"ap_localInfo.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] application-priority [detail]"},
{"USEREXEC"},
{"ap_remoteInfo.html"},
0
},
{
{"set selector <selector-field> protocol <protocol-id> priority <priority>"},
{"USEREXEC"},
{"ap_applicationToPriority.html"},
0
},
{
{"show ip arp [vrf <vrf-name>][ { Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-id> | <ipiftype> <ifnum> | <ip-address> | <mac-address> | summary | information | statistics }]"},
{"EXEC"},
{"arpcache_stats.html"},
0
},
{
{"arp [vrf <vrf-name>] <ip address> <hardware address> {Vlan <vlan-id/vfi-id> [switch switch-name] |  <interface-type> <interface-id> | Linuxvlan <interface-name>| Cpu0 | <IP-interface-type> <IP-interface-number>}"},
{"CONFIGURE"},
{"arp_entry.html"},
0
},
{
{"vrrp <vrid(1-255)> ipv4 <ip_addr> [secondary]"},
{"VRRP_IF"},
{"vrrp_assoc.html"},
0
},
{
{"ethernet evc availability measure {start | stop}            md <md-Index> ma <ma-index> mep <mep-index>             {rmep-id <integer(1-8191)> | rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}            [interval <milliseconds(100-3600000)>]            [windowsize <num_of_small_intervals(1-4099680000)>]            [deadline <seconds(1-409968000)>] [static-window | sliding-window]            [lowerthreshold <string(6)>] [upperthreshold <string(6)>]            [modestarea {available | unAvailable}]            [schldDownTime init <integer(1-172800)> end <integer(1-172800)>]            [switch <string(32)>]"},
{"EXEC"},
{"mefmepavailability.html"},
0
},
{
{"ethernet cfm availability measure {start | stop} {domain <string(43)> | level <integer(0-7)>} [service <string(43)> | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)> ] [interface <ifXtype> <ifnum>] [direction {inward | outward}] {mpid <integer(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>} [interval <<milliseconds(100-86400000)>] [windowsize <num_of_small_intervals(1-4099680000)>] [deadline <seconds(1-409968000)>] [static-window | sliding-window] [ lowerthreshold <string(6)> ] [ upperthreshold <string(6)> ] [modestarea { available | unAvailable}] [ schldDownTime init  <integer(1-172800)> end  <integer(1-172800)>  ] [switch <string(32)>]"},
{"EXEC"},
{"ecfm_availability.html"},
0
},
{
{"shutdown qos"},
{"CONFIGURE"},
{"qos_basicsettings.html"},
0
},
{
{"logging-server <short(0-191)> {ipv4 <ucast_addr> | ipv6 <ip6_addr> | <host-name>} [ port <integer(0-65535)>] [{udp | tcp | beep}]"},
{"CONFIGURE"},
{"beep_scalarsconf.html"},
0
},
{
{"do shutdown ip bgp [ vrf <vrf-name> ]"},
{"CONFIGURE"},
{"bgp_globalconf.html"},
0
},
{
{"bgp update-filter <1-100> {permit|deny} remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list-AS1,AS2,...>] direction { in|out }"},
{"BGP"},
{"bgp_filterconf.html"},
0
},
{
{"distance <1-255> [route-map <name(1-20)>]"},
{"BGP"},
{"fltr_bgpconf.html"},
0
},
{
{"bgp graceful-restart [restart-time <(1-4096)<seconds>] [stalepath-time <(90-3600)<seconds>]"},
{"BGP"},
{"bgp_grconf.html"},
0
},
{
{"show ip bgp info"},
{"EXEC"},
{"bgp_grstats.html"},
0
},
{
{"bgp med <1-100> remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list- AS1,AS2,...>] value <value> direction {in | out} [override]"},
{"BGP"},
{"bgp_localprefconf.html"},
0
},
{
{"bgp med <1-100> remote-as <AS no> <ip-address> <prefixlen> [intermediate-as <AS-no list- AS1,AS2,...>] value <value> direction {in | out} [override]"},
{"BGP"},
{"bgp_medconf.html"},
0
},
{
{"show ip bgp summary"},
{"EXEC"},
{"bgp_peerstats.html"},
0
},
{
{"ip bgp dampening [vrf  <vrf-name>] [HalfLife-Time <integer(600-2700)>] [Reuse-Value <integer(100-1999)>] [Suppress-Value <integer(2000-3999)>] [Max-Suppress-Time <integer(1800-10800)>] [-s Decay-Granularity <integer(1-10800)>]  [Reuse-Granularity <integer(15-10800)>] [Reuse-Array-Size <integer(256-65535)>]"},
{"CONFIGURE"},
{"bgp_rfdconf.html"},
0
},
{
{"aggregate-address index <1-100> <ip-address> <prefixlen> [summary-only] [as-set] [suppress-map <map-name>] [advertise-map <map-name>] [attribute-map <map-name>]"},
{"BGP"},
{"bgp_routeaggreconf.html"},
0
},
{
{"neighbor <ip-address|peer-group-name> { route-map <name(1-20)> | prefix-list <ipprefixlist_name(1-20)>} {in | out}"},
{"BGP"},
{"bgp_routemap.html"},
0
},
{
{"bgp cluster-id {cluster id value ip_address/integer}"},
{"BGP"},
{"bgp_globalconfscalars.htm"},
0
},
{
{"neighbor <ip-address|peer-group-name> timers {keepalive <(1-21845)seconds> | holdtime <(3-65535)seconds> |delayopentime <(0-65535)seconds>}"},
{"BGP"},
{"bgp_timerconf.html"},
0
},
{
{"router bgp <AS no> [vrf  <vrf-name>]"},
{"CONFIGURE"},
{"bgp_context_creation.html"},
0
},
{
{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},
{"EXEC"},
{"bridge_info.html"},
0
},
{
{"bridge-mode {customer | provider | provider-core | provider-edge | provider-backbone-icomp |provider-backbone-bcomp}"},
{"VCM"},
{"vcm_bridgemodeselection.html"},
0
},
{
{"spanning-tree mst {instance-id <instance-id(1-64)>} root {primary | secondary} "},
{"VCM"},
{"msti_bridgeconf.html"},
0
},
{
{"logging   { buffered <size (1-200)>  | console  |           facility {local0 | local1 | local2 | local3 | local4 | local5 | local6 | local7| } |          severity  [{ <level (0-7)> | alerts | critical | debugging | emergencies | errors |           informational | notification | warnings }] | on }"},
{"CONFIGURE"},
{"bsdsyslog_loggingconf.html"},
0
},
{
{"logging-file <short(0-191)> <string(32)>"},
{"CONFIGURE"},
{"bsdsyslog_filetable.html"},
0
},
{
{"syslog relay"},
{"CONFIGURE"},
{"bsdsyslog_scalarsconf.html"},
0
},
{
{"ethernet cfm error-log [enable | disable] [size <entries(1-4096)>]"},
{"VCM "},
{"ecfm_miscconf.html"},
0
},
{
{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},
{"CONFIGURE"},
{"pim_crpconf.html"},
0
},
{
{"show ip isis [vrf <contextname>] adjacencies [ { vlan <vlan-id/vfi-id> | <iftype> <ifnum> | <IP-interface-type> <IP-interface-number>} ]"},
{"EXEC"},
{"isis_adjconf.html"},
0
},
{
{"show ip isis [vrf <contextname>] packet-stats [ { vlan <vlan-id/vfi-id> | <iftype> <ifnum> | <IP-interface-type> <IP-interface-number>}]"},
{"EXEC"},
{"isis_pktcountconf.html"},
0
},
{
{"isis circuit-type {level-1 | level1-2 | level-2-only}"},
{"INTERFACE_ROUTER"},
{"isis_circconf.html"},
0
},
{
{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},
{"INTERFACE "},
{"mstp_portconf.html"},
0
},
{
{"mef class <class>"},
{"CONFIGURE"},
{"class.html"},
0
},
{
{"mef classmap <classmap-id> filter <filter-id> class <class>"},
{"CONFIGURE"},
{"classmap.html"},
0
},
{
{"set class <class integer(1-65535)> [pre-color { green | yellow | red | none }] [ regen-priority <integer(0-7)>  group-name <string(31)> ]"},
{"QOSCLSMAP"},
{"qos_classtoprisettings.html"},
0
},
{
{"match access-group { [mac-access-list <integer(0-65535)>] [ ip-access-list <integer(0-65535)>]  | priority-map <integer(0-65535)> }"},
{"QOSCLSMAP"},
{"qos_classmapsettings.html"},
0
},
{
{"clear ip bgp [vrf <string (32)>]  {dampening [<random_str><num_str>] | flap-statistics [<random_str> <num_str>] | { * | <AS no> | external | ipv4 | ipv6 | <random_str> } [soft [{in [prefix-filter]|out}]] }"},
{"EXEC"},
{"bgp_clearbgpconf.html"},
0
},
{
{"clear config[default-config-restore <filename>]"},
{"USEREXEC"},
{"gen_infoclearconfig.html"},
0
},
{
{"clear iss counters [bgp] [ospf] [rip] [rip6] [ospf3] [ipv4] [ipv6]"},
{"USEREXEC"},
{"gen_infoclearcounters.html"},
0
},
{
{"clear interfaces [ <interface-type> <interface-id> ] counters"},
{"USEREXEC"},
{"interface_clearstats.html"},
0
},
{
{"clear { mrp | mvrp | mmrp } statistics { all |               port <interface-type> <interface-id> }"},
{"CONFIGURE"},
{"mrp_clearstat.html"},
0
},
{
{"ip dhcp client client-id {<interface-type> <interface-id> | vlan <vlan-id (1-4094)> | port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | loopback <interface-id (0-100)> | ascii <string> | hex <string> }"},
{"INTERFACE_ROUTER"},
{"dhcpc_confclientId.html"},
0
},
{
{"ptp { mode { ordinary | boundary | e2etransparent | p2ptransparent | forward } | priority1 <value(0-255)> | priority2 <value(0-255)>}"},
{"CLKMODE"},
{"ptp_basicsettings.html"},
0
},
{
{"clock variance <value(0-255)>"},
{"CONFIGURE"},
{"clockiwf_settings.html"},
0
},
{
{"bgp comm-filter <comm-value(4294967041-4294967043,65536-4294901759)> <permit|deny> <in|out>"},
{"BGP"},
{"bgp_commfilterconf.html"},
0
},
{
{"bgp comm-route {additive|delete} <ip-address> <prefixlen> comm-value <4294967041-4294967043,65536-4294901759>"},
{"BGP"},
{"bgp_commrouteconf.html"},
0
},
{
{"bgp confederation peers <AS no(1-65535)>"},
{"BGP"},
{"bgp_confedconf.html"},
0
},
{
{"map switch <name>"},
{"INTERFACE "},
{"context_selection.html"},
0
},
{
{"ethernet cfm cc enable {domain <domain-name> | level <a,b,c-d>}  [service <service-name> | vlan <a,b,c-d> | service-instance <integer(256-16777214)>]"},
{"VCM"},
{"ecfm_ccm.html"},
0
},
{
{"show ptp current [{ vrf | switch } <context-name>] [domain <id (0-127)>]"},
{"USEREXEC"},
{"ptp_current.html"},
0
},
{
{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},
{"EXEC"},
{"pbrstp_cvlanstats.html"},
0
},
{
{"ra-vpn username <username> password <password>"},
{"CONFIGURE"},
{"vpn_userdb.html"},
0
},
{
{"cn cnpv <priority-value(0-7)>"},
{"CONFIGURE"},
{"cn_comp_settings.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"cn_cp_settings.html"},
0
},
{
{"cn trap [error-port-table] [cnm]"},
{"CONFIGURE"},
{"cn_debug.html"},
0
},
{
{"set cn {enable | disable}"},
{"CONFIGURE"},
{"cn_global_settings.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"cn_port_settings.html"},
0
},
{
{"clear cn counters [ { {interface <ifXtype> <ifnum>} | {switch <context name> } } ]"},
{"EXEC"},
{"cn_statistics.html"},
0
},
{
{"shutdown cn"},
{"CONFIGURE"},
{"cn_sys_control.html"},
0
},
{
{"debug dcbx all"},
{"USEREXEC"},
{"dcbx_globaltraces.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"dcbx_adminstatus.html"},
0
},
{
{"qos interface <iftype> <ifnum> def-user-priority <integer(0-7)>"},
{"CONFIGURE"},
{"qos_defaultuserprio.html"},
0
},
{
{"ethernet cfm default-domain vlan <vlan-id/vfi-id> ([level <level (0-7)>] [mip-creation-criteria {none | explicit | defer | default}] [sender-id-permission {none |chassis | manage | chassis-mgt-address | defer }])"},
{"VCM "},
{"ecfm_defmdconf.html"},
0
},
{
{"ipv6 default scope-zone {interfacelocal | linklocal | subnetlocal | adminlocal |  sitelocal | scope6 | scope7 | orglocal | scope9 | scopeA | scopeB | scopeC | scopeD } <zone-index>"},
{"CONFIGURE"},
{"scopezone_conf.html"},
0
},
{
{"service dhcp-server"},
{"CONFIGURE"},
{"dhcp_globalconf.html"},
0
},
{
{"ip dhcp bootfile <bootfile (63)>"},
{"CONFIGURE"},
{"dhcp_globalbootfileconfig.html"},
0
},
{
{"show ip dhcp client stats"},
{"EXEC"},
{"dhcpc_statistics.html"},
0
},
{
{"ip dhcp pool <index (1-2147483647)> [<Pool Name>]"},
{"CONFIGURE"},
{"dhcp_hostip.html"},
0
},
{
{"ip dhcp pool <index (1-2147483647)>"},
{"CONFIGURE"},
{"dhcp_hostopt.html"},
0
},
{
{"ip dhcp client request { tftp-server-name | boot-file-name | sip-server-info | option240}"},
{"INTERFACE_ROUTER"},
{"dhcpc_confopttype.html"},
0
},
{
{"option <code (1-2147483647)> { ascii <string> | hex <Hex String> | ip <address> }"},
{"DHCPPOOL"},
{"dhcp_poolopt.html"},
0
},
{
{"ip dhcp pool <index (1-2147483647)> [<Pool Name>]"},
{"CONFIGURE"},
{"dhcp_poolconf.html"},
0
},
{
{"service dhcp-relay"},
{"CONFIGURE"},
{"dhcpr_globalconf.html"},
0
},
{
{"ip dhcp relay circuit-id <circuit-id>"},
{"INTERFACE_ROUTER"},
{"dhcpr_interfacemap.html"},
0
},
{
{"show ip dhcp relay information [vlan <vlan-id>]"},
{"EXEC"},
{"dhcpr_statistics.html"},
0
},
{
{"ip dhcp pool <index (1-2147483647)>"},
{"CONFIGURE"},
{"dhcp_excl.html"},
0
},
{
{"show ip dhcp server statistics"},
{"EXEC"},
{"dhcpserver_stats.html"},
0
},
{
{"snmp-server enable traps ipv6 dhcp client [invalid-pkt] [auth-fail]"},
{"CONFIGURE"},
{"dhcp6c_globalconf.html"},
0
},
{
{"ipv6 dhcp client-id type {llt | en | ll}"},
{"INTERFACE_ROUTER"},
{"dhcp6c_ifconf.html"},
0
},
{
{"show ipv6 dhcp client statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},
{"EXEC"},
{"dhcp6c_ifstats.html"},
0
},
{
{"ipv6 dhcp client-id type {llt | en | ll}"},
{"INTERFACE_ROUTER"},
{"dhcp6c_option.html"},
0
},
{
{"ipv6 dhcp relay port {listen <value(1-65535)> | client transmit <value(1-65535)> | server transmit <value(1-65535)>}"},
{"CONFIGURE"},
{"dhcp6r_globalconf.html"},
0
},
{
{"ipv6 dhcp relay hop-threshold <count>"},
{"TNL"},
{"dhcp6r_ifconf.html"},
0
},
{
{"show ipv6 dhcp relay statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},
{"EXEC"},
{"dhcp6r_ifstats.html"},
0
},
{
{"ipv6 dhcp relay [destination <prefix> {link-local | <prefix Len> } [interface {Vlan <vlan-id (1-4094)> | <interface-type> <interface-id>}]]"},
{"INTERFACE_ROUTER"},
{"dhcp6r_outifconf.html"},
0
},
{
{"ipv6 dhcp relay [destination <prefix> {link-local | <prefix Len> } [interface {Vlan <vlan-id (1-4094)> | <interface-type> <interface-id>}]]"},
{"INTERFACE_ROUTER"},
{"dhcp6r_serveraddr.html"},
0
},
{
{"ipv6 dhcp server port {listen <value(1-65535)> | client transmit <value(1-65535)> | relay transmit <value(1-65535)>}"},
{"CONFIGURE"},
{"dhcp6s_globalconf.html"},
0
},
{
{"ipv6 dhcp authentication server client-id <string(128)> {llt | en | ll}"},
{"CONFIGURE"},
{"dhcp6s_clientconf.html"},
0
},
{
{"ipv6 dhcp server [<pool-name (1-64)> [preference  <value (0-255)>]]"},
{"INTERFACE_ROUTER"},
{"dhcp6s_ifconf.html"},
0
},
{
{"show ipv6 dhcp server statistics [interface {vlan <VlanId(1-4094)> | <interface-type> <interface-id>} ]"},
{"EXEC"},
{"dhcp6s_ifstats.html"},
0
},
{
{"ipv6 dhcp authentication realm <string(1-128)> key <string(1-64)>"},
{"D6SRVCLNT"},
{"dhcp6s_keyconf.html"},
0
},
{
{"ipv6 dhcp pool <string (1-64)>"},
{"CONFIGURE"},
{"dhcp6s_optionconf.html"},
0
},
{
{"ipv6 dhcp server [<pool-name (1-64)> [preference  <value (0-255)>]]"},
{"TNL"},
{"dhcp6s_poolconf.html"},
0
},
{
{"link-address <IPV6-Prefix>"},
{"D6SRVPOOL"},
{"dhcp6s_prefixconf.html"},
0
},
{
{"ipv6 dhcp authentication realm <string(1-128)> key <string(1-64)>"},
{"D6SRVCLNT"},
{"dhcp6s_realmconf.html"},
0
},
{
{"ipv6 dhcp pool <string (1-64)>"},
{"CONFIGURE"},
{"dhcp6s_suboptionconf.html"},
0
},
{
{"show lldp neighbors [chassis-id <string(255)> port-id <string(255)>] [<interface-type> <interface-id>][detail]"},
{"EXEC"},
{"lldp_neighbors.html"},
0
},
{
{"show d-lag [<port-channel(1-65535)>] {detail}"},
{"USEREXEC"},
{"la_remoteinterfacesettings"},
0
},
{
{"show d-lag [<port-channel(1-65535)>] {detail}"},
{"USEREXEC"},
{"la_remoteportsettings.html"},
0
},
{
{"dmz <DMZ Host IP>"},
{"FIREWALL"},
{"fwl_dmzconf.html"},
0
},
{
{"ipv6 dmz <DMZ IPv6 Host>"},
{"FIREWALL"},
{"fwl_dmzconfv6.html"},
0
},
{
{"clear ip domain cache"},
{"CONFIGURE"},
{"dns_cache.html"},
0
},
{
{"ip domain name <string>"},
{"CONFIGURE"},
{"dns_domainname.html"},
0
},
{
{"domain name-server {ipv4 <ucast_addr> | ipv6 <ip6_addr>}"},
{"CONFIGURE"},
{"dns_nameserver.html"},
0
},
{
{"shutdown dns"},
{"CONFIGURE"},
{"dns_conf.html"},
0
},
{
{"show ip dns statistics"},
{"EXEC"},
{"dns_stats.html"},
0
},
{
{"debug dns {init-shutdown | ctrl | query | response | cache | failure | all}"},
{"EXEC"},
{"dns_traces.html"},
0
},
{
{"dsmon {enable | disable}"},
{"CONFIGURE"},
{"dsmon_globalconf.html"},
0
},
{
{"set ip dvmrp { enable | disable }"},
{"CONFIGURE"},
{"dvmrp_conf.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrpiface_conf.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrp_mcastroute.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrp_neighbor.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrp_nexthop.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrp_prune.html"},
0
},
{
{"show ip dvmrp { routes [{ vlan <vlan-id(1-4094)> | <interface-type> <interface-id> }] | mroutes | nexthop | neighbor | info | prune }"},
{"EXEC"},
{"dvmrp_route.html"},
0
},
{
{"set port gmrp <interface-type> <interface-id> { enable | disable }"},
{"VCM"},
{"vlan_gmrpconf.html"},
0
},
{
{"set port gvrp <interface-type> <interface-id> { enable | disable }"},
{"VCM"},
{"vlan_gvrpconf.html"},
0
},
{
{"set gvrp { enable | disable }"},
{"VCM"},
{"vlan_gvrpbasicconf.html"},
0
},
{
{"set port gvrp <interface-type> <interface-id> { enable | disable }"},
{"VCM"},
{"vlan_gvrpportconf.html"},
0
},
{
{"show ethernet cfm errors [domain <domain-name> | level <level-id(0-7)>][switch <context_name>]"},
{"EXEC"},
{"ecfm_errors.html"},
0
},
{
{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},
{"EXEC"},
{"ecfm_stats.html"},
0
},
{
{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},
{"EXEC"},
{"ecfm_portstats.html"},
0
},
{
{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},
{"EXEC"},
{"ecfm_mepstats.html"},
0
},
{
{"shutdown ethernet lmi"},
{"CONFIGURE"},
{"elmi_basicconf.html"},
0
},
{
{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},
{"EXEC"},
{"elmi_basicstats.html"},
0
},
{
{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},
{"EXEC"},
{"elmi_errorstats.html"},
0
},
{
{"ethernet lmi {n391 <value(1-65000)>|n393 <value(2-10)>|t391 <value(5-30)>|t392 <value(0,5-30)>}"},
{"INTERFACE "},
{"elmi_portconf.html"},
0
},
{
{"show ethernet lmi {{parameters|statistics}{interface <interface-type> <ifnum>}}"},
{"EXEC"},
{"elmi_portstats.html"},
0
},
{
{"shutdown aps [linear]"},
{"CONFIGURE"},
{"elps_contextinfo.html"},
0
},
{
{"show aps [linear] group-list protection port <interface_type> <interface_id>"},
{"USEREXEC"},
{"elps_pgshare.html"},
0
},
{
{"aps group-name <pg_name (32)>"},
{"ELPS_CONFIG"},
{"elps_pgstats.html"},
0
},
{
{"aps group-name <pg_name (32)>"},
{"ELPS_CONFIG"},
{"elps_pgcfmconf.html"},
0
},
{
{"switch <name>"},
{"CONFIGURE"},
{"elps_pgcmd.html"},
0
},
{
{"aps mpls-pw list <list-id> {working | protect} vc-id <integer>                   [peer-address {<ucast_addr> | <local-map-number>}]"},
{"ELPS_CONFIG"},
{"elps_servicelist.html"},
0
},
{
{"aps monitor {y1731 | mplsoam [ version <integer(0-1)> ] | none}"},
{"ELPS_CONFIG"},
{"elps_pginfo.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] event-log"},
{"EXEC"},
{"eoam_eventlog.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] event-log"},
{"EXEC"},
{"eoam_eventlog.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] statistics"},
{"EXEC"},
{"eoam_interfacestats.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] statistics"},
{"EXEC"},
{"eoam_interfacestats.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] neighbor"},
{"EXEC"},
{"eoam_neighborstats.html"},
0
},
{
{"show port ethernet-oam [<interface-type> <interface-id>] neighbor"},
{"EXEC"},
{"eoam_neighborstats.html"},
0
},
{
{"erase  { startup-config | nvram: | flash:filename}"},
{"USEREXEC"},
{"erase.html"},
0
},
{
{"shutdown aps ring [switch]"},
{"VCM"},
{"erps_globalconfig.html"},
0
},
{
{"aps ring map vlan-group <short(0-64)> [{add|remove}] <vlan_list>      "},
{"CONFIGURE"},
{"erps_ringvlan.html"},
0
},
{
{"show ethernet cfm error-log [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}] [switch <context_name>]"},
{"EXEC"},
{"ecfm_errlog.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pb_portconf.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pb_ethertypeswaptable.html"},
0
},
{
{"shutdown ethernet-oam"},
{"CONFIGURE"},
{"eoam_basicsettings.html"},
0
},
{
{"ethernet-oam link-monitor frame-period {enable | disable}"},
{"INTERFACE"},
{"eoam_linkeventsettings.html"},
0
},
{
{"ethernet-oam remote-loopback {disable | enable}"},
{"INTERFACE"},
{"eoam_loopbackconfig.html"},
0
},
{
{"ethernet-oam {disable | enable}"},
{"INTERFACE"},
{"eoam_portsettings.html"},
0
},
{
{"rmon collection stats <index (1-65535)> [owner <ownername (127)>]"},
{"INTERFACE"},
{"rmon_ethstats.html"},
0
},
{
{"set  priority-grouping   {enable | disable}"},
{"INTERFACE "},
{"ets_adminportconfig.html"},
0
},
{
{"shutdown ets"},
{"CONFIGURE"},
{"ets_globalInfo.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] priority-grouping [detail]"},
{"USEREXEC"},
{"ets_localInfo.html"},
0
},
{
{"set priority-group bandwidth <prioritygroup-bw0> <prioritygroup-bw1>                 <prioritygroup-bw2> <prioritygroup-bw3> <prioritygroup-bw4>                   <prioritygroup-bw5> <prioritygroup-bw6> <prioritygroup-bw7> "},
{"INTERFACE "},
{"ets_bandwidthConfiguration.html "},
0
},
{
{"show interfaces priority-grouping  counters [ <ifXtype> <ifnum> ]"},
{"USEREXEC"},
{"ets_portStats.html"},
0
},
{
{"map priority <priority-list> priority-group <integer(0-7) | 15> "},
{"INTERFACE "},
{"ets_priToPGIDMapping.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] priority-grouping [detail]"},
{"USEREXEC"},
{"ets_remoteInfo.html"},
0
},
{
{"set traffic class <tc-group-list> traffic selection algorithm { strict-priority-algorithm | credit-based-shaper-algorithm | enhanced-transmission-selection-algorithm | vendor-specific-algorithm}"},
{"INTERFACE "},
{"ets_tsaConfig.html"},
0
},
{
{"ethernet map ce-vlan <ce-vlan> evc <evc> [uni-evc-id <string(32)>]"},
{"REDUNDANCY"},
{"evc.html"},
0
},
{
{"ethernet evc filter-instance <instance-id> filter-id <filter-no> dest-mac <destination-mac-address> {allow | drop}"},
{"VLAN_CONFIG"},
{"evc_filter.html"},
0
},
{
{"rmon event <number (1-65535)> [description <event-description (127)>] [log] [owner <ownername (127)>] [trap <community (127)>]"},
{"CONFIGURE"},
{"rmon_eventconf.html"},
0
},
{
{"bgp ecomm-filter <ecomm-value(xx:xx:xx:xx:xx:xx:xx:xx)> {permit|deny} {in|out}"},
{"BGP"},
{"bgp_ecommfilterconf.html"},
0
},
{
{"default  mode { manual | dynamic }"},
{"CONFIGURE"},
{"nvram_settings.html"},
0
},
{
{"default  mode { manual | dynamic }"},
{"CONFIGURE"},
{"ip_settings.html"},
0
},
{
{"show env {all | temperature | fan | RAM | CPU | flash | power}"},
{"USEREXEC"},
{"system_resources_fan.html"},
0
},
{
{"show port fault-management ethernet-oam [<interface-type> <interface-id>] remote-loopback {current-session | last-session} [detail]"},
{"EXEC"},
{"fm_lbcurrentstats.html"},
0
},
{
{"show port fault-management ethernet-oam [<interface-type> <interface-id>] remote-loopback {current-session | last-session} [detail]"},
{"EXEC"},
{"fm_lblaststats.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"vlan_fdbflush.html"},
0
},
{
{"copy { tftp://ip-address/filename startup-config | sftp://<user-name>:<pass-word>@ip-address/filename startup-config | flash: filename startup-config }"},
{"USEREXEC"},
{"file_download.html"},
0
},
{
{"copy startup-config {flash: filename | tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename}"},
{"USEREXEC"},
{"file_upload.html"},
0
},
{
{"mef filter <filter-id> [evc <evc> [ dscp <0-63>]]              direction {in | out}"},
{"VLAN_CONFIG"},
{"mef_filter.html"},
0
},
{
{"enable"},
{"FIREWALL"},
{"fwl_basicsettings.html"},
0
},
{
{"filter add <filter name> {<Source IP range>|any} {<Dest IP range>|any} {<tcp|udp|icmp|igmp|ggp|ip|egp|igp|nvp|rsvp|igrp|ospf|any|other <1-255>>} [srcport <range>] [destport <range>]"},
{"FIREWALL"},
{"fwl_filterconf.html"},
0
},
{
{"untrusted port ([<interface-type> <0/a-b, 0/c, ...>] [<interface-type> <0/a-b, 0/c, ...>] [{ppp|multilink} <a,b,c-d>] [vlan <a,b,c-d>]) [trap-threshold <Max Packet Discard Count>]"},
{"FIREWALL"},
{"fwl_interfaceconf.html"},
0
},
{
{"show firewall logs"},
{"EXEC"},
{"fwl_log.html"},
0
},
{
{"clear global statistics"},
{"FIREWALL"},
{"fwl_stats.html"},
0
},
{
{"shutdown fault-management"},
{"CONFIGURE"},
{"fm_basicsettings.html"},
0
},
{
{"fault-management ethernet-oam remote-loopback ([test] [count <no of packets(1-1000)>] [packet <size(64-1500)>] [pattern <hex_string(8)>] [wait-time <integer(1-10)>])"},
{"INTERFACE"},
{"fm_loopbackconfig.html"},
0
},
{
{"fault-management ethernet-oam {critical-event | dying-gasp | link-fault} action {none | warning}"},
{"INTERFACE"},
{"fm_portsettings.html"},
0
},
{
{"show ethernet cfm frame delay buffer [detail] [one-way | two-way] [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]  [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_fdbrief.html"},
0
},
{
{"show ethernet cfm frame delay buffer [detail] [one-way | two-way] [domain <domain-name> | level <level-id(0-7)>] [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]  [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_fddetail.html"},
0
},
{
{"ethernet evc frame delay {start | stop} type {one-way | two-way}             md <md-Index> ma <ma-index>             mep <mep-index> {rmep-id <rmep-id> |             rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}             [interval <milliseconds(100-10000)>]             [count <num_of_observations(1-8192)>]             [deadline <seconds(1-172800)>] [switch <context_name>]"},
{"VLAN_CONFIG"},
{"mefmepfd.html"},
0
},
{
{"ethernet cfm frame delay [{start | stop}] type {one-way | two-way} {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [direction {inward | outward}] {mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)} [interval <milliseconds(10-10000)>] [count <num_of_observations(1-8192)>] [deadline <seconds(1-172800)>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_fd.html"},
0
},
{
{"show ethernet evc frame loss md <md-Index> ma <ma-index> mep <mep-index> [switch <context_name>]"},
{"EXEC"},
{"meffdstats.html"},
0
},
{
{"show ethernet cfm frame loss buffer [detail] [single-ended | dual-ended] [domain <domain-name> | level <level-id(0-7)>]  [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_flbrief.html"},
0
},
{
{"ethernet evc frame loss {start | stop} md <md-Index> ma <ma-index>             mep <mep-index> {rmep-id <rmep-id> |             rmep-mac <rmep-mac(aa:aa:aa:aa:aa:aa)>}             [interval <milliseconds(100-600000)>]             [count <num_of_observations(1-8192)>]             [deadline <seconds(1-172800)>] [switch <context_name>] "},
{"VLAN_CONFIG"},
{"mefmepfl.html"},
0
},
{
{"show ethernet cfm frame loss buffer [detail] [single-ended | dual-ended] [domain <domain-name> | level <level-id(0-7)>]  [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [interface <interface-type> <interface-number>] [mac <peer-mac-address>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_fldetail.html"},
0
},
{
{"ethernet cfm frame loss [{start | stop}] {domain <string(43)> | level <level-id(0-7)>} [{ service <service-name> | vlan <vlan_vfi_id> | service-instance <service-instance (256-16777214)>}] [interface <type> <num>] [direction {inward | outward}] {mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>} [interval <milliseconds(100-600000)>] [count <num_of_observations(1-8192)>] [deadline <seconds(1-172800)>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_fl.html"},
0
},
{
{"show ethernet evc frame delay md <md-Index> ma <ma-index> mep <mep-index> [switch <context_name>]"},
{"EXEC"},
{"mefflstats.html"},
0
},
{
{"mpls static binding ipv4 <prefix> <mask> {input {vlan <in-vlan-id (1-4094)> | <in-interface-type> <in-interface-id>} | output <nexthop>} <label (200001-300000)>"},
{"CONFIGURE"},
{"mplsftn_conf.html"},
0
},
{
{"shutdown garp"},
{"CONFIGURE"},
{"vlan_garpbasicconf.html"},
0
},
{
{"set garp timer {join | leave | leaveall} <time in milli seconds>"},
{"INTERFACE"},
{"vlan_garptimersconf.html"},
0
},
{
{"debug garp { global | [{protocol | gmrp | gvrp | redundancy}              [initshut] [mgmt] [data] [ctpl] [dump] [os] [failall] [buffer] [all]] [switch <context_name>] }"},
{"USEREXEC"},
{"vlangarp_traces.html"},
0
},
{
{"ethernet cfm default-domain global ([level <level(0-7)] [mip-creation-criteria {none | explicit | default}] [sender-id-permission {none | chassis | manage | chassis-mgt-address}])"},
{"VCM "},
{"pvrst_basicconf.html"},
0
},
{
{"shutdown spanning-tree"},
{"CONFIGURE"},
{"ecfm_globalconf.html"},
0
},
{
{"ipsecv6 admin <enable/disable>"},
{"CRYPTO"},
{"secv6_globalconf.html"},
0
},
{
{"set gmrp { enable | disable }"},
{"CONFIGURE"},
{"vlan_gmrpbasicconf.html"},
0
},
{
{"set port gmrp <interface-type> <interface-id> { enable | disable }"},
{"VCM"},
{"vlan_gmrpportconf.html"},
0
},
{
{"nsf ietf [restart-interval <grace period (1-1800)>] [plannedOnly]"},
{"ROUTEROSPF3"},
{"ospfv3gr_conf.html"},
0
},
{
{"nsf ietf { plannedOnly | plannedAndUnplanned }"},
{"ROUTERISIS"},
{"ospf_GRconf.html"},
0
},
{
{"capability opaque"},
{"ROUTEROSPF"},
{"isis_GRconf.html"},
0
},
{
{"sched-hierarchy interface <iftype> <ifnum> hierarchy-level <integer(1-10)> sched-id <integer(1-65535)> {next-level-queue <integer(0-65535)> | next-level-scheduler <integer(0-65535)>} [priority <integer(0-15)>] [weight <integer(0-1000)>]"},
{"CONFIGURE"},
{"qos_hierarchytable.html"},
0
},
{
{"rmon collection history <index (1-65535)> [buckets <bucket-number (1-65535)>] [interval <seconds (1-3600)>] [owner <ownername (127)>]"},
{"INTERFACE"},
{"rmon_historyconf.html"},
0
},
{
{"set hitless-restart enable"},
{"EXEC"},
{"hrconfig.html"},
0
},
{
{"set http authentication-scheme {default | basic | digest}"},
{"CONFIGURE"},
{"http_scalarsconf.html"},
0
},
{
{"ipv6 icmp inspect [destination-unreachable] [time-exceeded]                [parameter-problem] [echo-request] [echo-reply] [redirect]                 [information-request] [information-reply]"},
{"FIREWALL"},
{"fwl_icmpinspect.html"},
0
},
{
{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},
{"EXEC"},
{"icmp_stats.html"},
0
},
{
{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},
{"USEREXEC"},
{"icmp6if_stats.html"},
0
},
{
{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},
{"USEREXEC"},
{"icmp6_stats.html"},
0
},
{
{"set ip igmp { enable | disable }"},
{"CONFIGURE"},
{"igmp_conf.html"},
0
},
{
{"ip igmp static-group <Group Address> [source <Source Address>]"},
{"PPP"},
{"igmp_grp.html"},
0
},
{
{"set ip igmp {enable | disable }"},
{"PPP"},
{"igmp_iface.html"},
0
},
{
{"ip igmp proxy-service"},
{"CONFIGURE"},
{"igp_conf.html"},
0
},
{
{"show ip igmp-proxy forwarding-database [{ Vlan <vlan-id/vfi-id> | group <group-address> | source <source-address> }]"},
{"EXEC"},
{"igp_mroute.html"},
0
},
{
{"show ip igmp-proxy forwarding-database [{ Vlan <vlan-id/vfi-id> | group <group-address> | source <source-address> }]"},
{"EXEC"},
{"igp_nexthop.html"},
0
},
{
{"show ip igmp statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},
{"EXEC"},
{"igp_stats.html"},
0
},
{
{"ip igmp-proxy mrouter-version { 1 | 2 | 3 }"},
{"PPP"},
{"igp_upiface.html"},
0
},
{
{"ip igmp snooping clear counters [Vlan <vlanid/vfi_id>]"},
{"CONFIGURE"},
{"igs_clearstats.html"},
0
},
{
{"shutdown snooping"},
{"CONFIGURE"},
{"igs_conf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"igs_intf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"igs_enhintf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"igs_intfconf.html"},
0
},
{
{"ip igmp snooping static-group <mcast_addr> ports <ifXtype><iface_list>"},
{"VLAN_CONFIG"},
{"igs_static.html"},
0
},
{
{"ip igmp snooping mrouter-time-out <(60 - 600) seconds>"},
{"CONFIGURE"},
{"igs_tmrconf.html"},
0
},
{
{"show ip igmp snooping statistics [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_stats.html"},
0
},
{
{"show ip igmp snooping statistics [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_v3stats.html"},
0
},
{
{"ip igmp snooping fast-leave"},
{"VLAN_CONFIG"},
{"igs_vlan.html"},
0
},
{
{"ip igmp snooping mrouter <interface-type> <0/a-b, 0/c, ...>"},
{"VLAN_CONFIG"},
{"igs_rtrportconf.html"},
0
},
{
{"show ip igmp snooping mrouter [Vlan <vlan-id/vfi-id>] [detail]              [switch <switch_name>]"},
{"USEREXEC"},
{"igs_rtrtable.html"},
0
},
{
{"show ip igmp sources"},
{"EXEC"},
{"igmp_src.html"},
0
},
{
{"show ip igmp statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},
{"EXEC"},
{"igmproute_stats.html"},
0
},
{
{"mpls static crossconnect {{vlan <in-vlan-id (1-4094)> | <in-interface-type> <in-interface-id>} <inlabel> <nexthop> {<outlabel> | explicit-null | implicit-null} | pwxconnect <ingress-PW-Id> <egress-PW-Id>}"},
{"CONFIGURE"},
{"mplsxc_conf.html"},
0
},
{
{"import route ip-address prefixlen nexthop metric ifindex protocol action route-count"},
{"BGP"},
{"bgp_importconf.html"},
0
},
{
{"spanning-tree vlan <vlan-id/vfi_id> {forward-time <seconds(4-30)> | hello-time <seconds(1-10)> | max-age <seconds(6-40)> | hold-count <integer(1-10)> | brg-priority <integer(0-61440)> | root {primary | secondary}}"},
{"VCM"},
{"pvrst_instbrigconf.html"},
0
},
{
{"spanning-tree vlan <vlan-id/vfi_id> status {disable | enable}"},
{"INTERFACE "},
{"pvrst_instportconf.html"},
0
},
{
{"show spanning-tree vlan <vlan-id/vfi-id> interface <ifXtype> <ifnum> [{ cost | detail | priority | rootcost | state | stats }]"},
{"EXEC"},
{"pvrst_instportstatus.html"},
0
},
{
{"show ip isis instances"},
{"EXEC"},
{"isis_aaconf.html"},
0
},
{
{"area-password <password>"},
{"ROUTERISIS"},
{"isis_arearxpwdconf.html"},
0
},
{
{"domain-password <password>"},
{"ROUTERISIS"},
{"isis_domainrxpwdconf.html"},
0
},
{
{"router isis [vrf <contextname>]"},
{"CONFIGURE"},
{"isis_sysinstconf.html"},
0
},
{
{"isis metric < integer (1-63)> [delay-metric <integer (0-63)>] [error-metric < integer (0-63)>] [expense-metric < integer (0-63)>] {level-1 | level-2}"},
{"INTERFACE_ROUTER"},
{"isis_sysinstconf2.html"},
0
},
{
{"interface {cpu0 | VlanMgmt | port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name>| ppp <1-10>}"},
{"CONFIGURE"},
{"vcm_portmap.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"l3context_interfacemapping.html"},
0
},
{
{"interface nat {enable|disable}"},
{"PPP"},
{"nat_conf.html"},
0
},
{
{"lldp {transmit | receive}"},
{"PSW"},
{"lldp_interfaces.html"},
0
},
{
{"ipv6 ospf area <IPv4-Address> [instance <instance-id>]"},
{"INTERFACE_ROUTER"},
{"ospfv3if_conf.html"},
0
},
{
{"show interfaces [{ <interface-type> <interface-id> | vlan <vlan_vfi_id> [switch <switch-name>] | tunnel <tunnel-id(0-128)> | ppp <short(1-4094)> | HC}] counters"},
{"USEREXEC"},
{"interface_stats.html"},
0
},
{
{"ip ra-vpn pool <poolname> <start_ip> - <end_ip>"},
{"CONFIGURE"},
{"vpn_addresspool.html"},
0
},
{
{"authorized-manager ip-source <ip-address>             [{<subnet-mask> | / <prefix-length(0-32)>}]             [interface [<interface-type <0/a-b, 0/c, ...>]             [<interface-type <0/a-b, 0/c, ...>]]             [vlan <a,b or a-b or a,b,c-d>]             [cpu0]             [service [snmp] [telnet] [http] [https] [ssh]]"},
{"CONFIGURE"},
{"ip_auth_mgr.html"},
0
},
{
{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_enhipfwdtable.html"},
0
},
{
{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_enhipfwdtable.html"},
0
},
{
{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_ipfwdtable.html"},
0
},
{
{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_ipfwdtable.html"},
0
},
{
{"ip routing [vrf <vrf-name>]"},
{"CONFIGURE"},
{"rarp_info.html"},
0
},
{
{"ping [vrf <vrf-name>] [ ip ] {IpAddress | hostname } [data (0-65535)] [df-bit] [{repeat|count} packet_count (1-10)] [size packet_size (36-2080)][source <ip-address>] [timeout time_out (1-100)] [validate]"},
{"EXEC"},
{"ip_ping.html"},
0
},
{
{"ip path mtu [vrf <vrf-name>] <dest ip> <tos(0-255)> <mtu(68-65535)>"},
{"CONFIGURE"},
{"rarp_pmtu.html"},
0
},
{
{"ip prefix-list <list-name(1-20)> [seq <seq-num>] {permit | deny } <ipaddr/prefix-len>           [ge <min-len>] [le <max-len>]"},
{"CONFIGURE"},
{"rmap_ipprefix_conf.html"},
0
},
{
{"ip route [vrf <vrf-name>] <prefix> <mask> {<next-hop> | Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-id> | Linuxvlan <interface-name> | Cpu0 | tunnel <tunnel-id (0-128)> | <IP-interface-type> <IP-interface-number>} [<distance (1-254)>] [ private ] [ permanent ] [ name <nexthop-name>]"},
{"CONFIGURE"},
{"ip_routeconf.html"},
0
},
{
{"default  mode { manual | dynamic }"},
{"CONFIGURE"},
{"ip_settings_org.html"},
0
},
{
{"ipra <ipra-idx> {<ip-address> <ip-mask> <next-hop-ip> |<ip6_addr> <prefixlength> <ip6_addr> } [met-type {internal | external}]"},
{"ROUTERISIS"},
{"isis_ipraconf.html"},
0
},
{
{"show ipsecv6 stat <if/ah-esp/intruder>"},
{"USEREXEC"},
{"secv6_stats.html"},
0
},
{
{"traceroute {<ip-address> | ipv6 <prefix>} [vrf <vrf-name>] [min-ttl <value (1-99)>] [max-ttl <value (1-99)>]"},
{"EXEC"},
{"ipv4traceroute.html"},
0
},
{
{"ip address <ip-address> <subnet-mask> [secondary]"},
{"IVR"},
{"ivr_conf.html"},
0
},
{
{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},
{"EXEC"},
{"ipv4ifsp_stats.html"},
0
},
{
{"ip mcast ttl-threshold <ttl-threshold (0-255)>"},
{"INTERFACE_ROUTER"},
{"ipv4mc_routeintfconf.html"},
0
},
{
{"ip multicast routing"},
{"CONFIGURE"},
{"ipv4mc_basicsettings.html"},
0
},
{
{"show ip mroute"},
{"EXEC"},
{"ipv4mcnexthp_stats.html"},
0
},
{
{"show ip mroute"},
{"EXEC"},
{"ipv4mcroute_stats.html"},
0
},
{
{"show ip traffic [vrf <vrf-name>] [ interface { Vlan <vlan-id/vfi-id> [switch  <switch-name>] | tunnel <tunnel-id (1-128)> | <interface-type> <interface-id> | Linuxvlan <interface-name> | <IP-interface-type> <IP-interface-number> } ] [hc]"},
{"EXEC"},
{"ipv4syssp_stats.html"},
0
},
{
{"ipv6 unicast-routing [vrf <vrf-name>]"},
{"CONFIGURE"},
{"ipv6_global_info.html"},
0
},
{
{"ip vrf forwarding <vrf-name>"},
{"INTERFACE_ROUTER"},
{"ipv6_if_Forwarding.html"},
0
},
{
{"ipv6 nd prefix {<prefix addr> <prefixlen> | default} [{{<valid lifetime> | infinite | at <var valid lifetime>  }{<preferred lifetime> |infinite | at <var preferred lifetime>} | no-advertise}]"},
{"CONFIGURE"},
{"ivr6_conf.html"},
0
},
{
{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},
{"USEREXEC"},
{"ipv6ifsp_stats.html"},
0
},
{
{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},
{"USEREXEC"},
{"ipv6if_stats.html"},
0
},
{
{"ping [vrf <vrf-name>] ipv6 <prefix%interface> [data <hex_str>] [repeat <count>] [size <value>] [anycast] [source {vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <id> | <source_prefix>}] [timeout <value (1-100)>] "},
{"EXEC"},
{"ipv6_ping.html"},
0
},
{
{"ipv6 route [vrf <vrf-name>] <prefix> <prefix len> ([<NextHop>] {vlan <vlan-id/vfi-id> [switch <switch-name> [<administrative distance>] [{unicast | anycast}]]|[tunnel <id>][<administrative distance>] [unicast] | [<administrative distance>] [unicast] | [<interface-type> <interface-id>] [<administrative distance>] [unicast] | <IP-interface-type> <IP-interface-number> [<administrative distance>] [unicast]})    "},
{"CONFIGURE"},
{"ip6_routeconf.html"},
0
},
{
{"show ipv6 traffic [vrf <vrf-name>] [interface { vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <tunnel-id> | <interface-type> <if-num> | <IP-interface-type> <IP-interface-number>} ] [hc]"},
{"USEREXEC"},
{"ipv6syssp_stats.html"},
0
},
{
{"isis metric < integer (1-63)> [delay-metric <integer (0-63)>] [error-metric < integer (0-63)>] [expense-metric < integer (0-63)>] {level-1 | level-2}"},
{"IVR"},
{"isis_circlevelconf.html"},
0
},
{
{"distance <1-255> [route-map <name(1-20)>]"},
{"ROUTERISIS"},
{"fltr_isisconf.html"},
0
},
{
{"monitor session <session-id (1-20)> { source              {interface <interface-type> <interface-id> [{rx|tx|both}] |              tunnel <tunnel-id> [{rx|tx|both}]|              vlan <vlan_range> [switch <context_name> ][{rx|tx|both}]              |mac-acl <acl-id> |ip-acl <acl-id>|remote vlan <vlan-id>              [switch <context_name>]}} [ COMP ]"},
{"CONFIGURE"},
{"mirror_ctrlext.html"},
0
},
{
{"private-vlan mapping [{add | remove}] <vlan-list>"},
{"INTERFACE"},
{"ivr_pvlanmapping.html"},
0
},
{
{"mac-address-table static               multicast <aa:aa:aa:aa:aa:aa> vlan <vlan-id/vfi_id>              [recv-port <ifXtype> <ifnum>]               [{recv-port <ifXtype> <ifnum> | service-instance <integer(256-16777214)>}]              interface ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>][pw <a,b,c-d>] [ac <a,b,c-d>]])      [forbidden-ports ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>][pw <a,b,c-d>][ac <a,b,c-d>]]) [status { permanent | deleteOnReset | deleteOnTimeout }]"},
{"CONFIGURE"},
{"l2_mcastfilter.html"},
0
},
{
{"fid <integer(1-4094)> vlan <vlan-range>"},
{"CONFIGURE"},
{"l2_ucastfilter.html"},
0
},
{
{"route-target {import | export | both} <RT-value> [rt-desc description>]"},
{"MPLS_L3VPN_VRF_MODE"},
{"mplsl3vpn_conf.html"},
0
},
{
{"shutdown port-channel"},
{"CONFIGURE"},
{"la_aggreconf.html"},
0
},
{
{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},
{"USEREXEC"},
{"la_interfacestats.html"},
0
},
{
{"port-channel load-balance ([src-mac][dest-mac][src-dest-mac][src-ip][dest-ip][src-dest-ip]              [vlan-id][service-instance][mac-src-vid][mac-dest-vid][mac-src-dest-vid][dest-ip6][src-ip6]              [l3-protocol][dest-l4-port][src-l4-port][mpls-vc-label][mpls-tunnel-label][mpls-vc-tunnel-label])               [ <port-channel-index(1-65535)>]"},
{"CONFIGURE"},
{"la_policyconf.html"},
0
},
{
{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},
{"USEREXEC"},
{"la_neighbourstats.html"},
0
},
{
{"channel-group <channel-group-number(1-65535)> mode                                                               {auto [non-silent] | desirable [non-silent] | on | active | passive }"},
{"INTERFACE "},
{"la_portchannel.html"},
0
},
{
{"lacp port-priority <0-65535>"},
{"INTERFACE "},
{"la_portconf.html"},
0
},
{
{"show interfaces [<interface-type> <interface-id> ] etherchannel "},
{"USEREXEC"},
{"la_portstateinfo.html"},
0
},
{
{"show lacp [<port-channel(1-65535)>] { counters | neighbor [detail] }"},
{"USEREXEC"},
{"la_portlacpstats.html"},
0
},
{
{"layer4 switch <FilterNo (1-20)>  protocol { any | tcp | udp | <Protocol No (1-255)>} port { any | <PortNo (1-65535)>} interface { <interface type> <interface id> }"},
{"CONFIGURE"},
{"l4_switching.html"},
0
},
{
{"traceroute ethernet {mpid <mep-id> | mac <aa:aa:aa:aa:aa:aa>} {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>] [interface <interface-type> <interface-number>] [direction {inward | outward}] [time-to-live <ttl-value(0-255)>] [timeout <milliseconds(10-10000)>] [use-mip-ccm-db] [switch <context_name>]"},
{"EXEC"},
{"ecfm_ltm.html"},
0
},
{
{"lldp tlv-select dot3tlv { [macphy-config] [link-aggregation] [max-framesize] }"},
{"REDUNDANCY"},
{"lldp_agentdetails.html"},
0
},
{
{"lldp dest-mac <mac_addr>"},
{"REDUNDANCY"},
{"lldp_agentinfo.html"},
0
},
{
{"lldp transmit-interval <seconds(5-32768)>"},
{"CONFIGURE"},
{"lldp_basicsettings.html"},
0
},
{
{"show lldp errors"},
{"EXEC"},
{"lldp_errors.html"},
0
},
{
{"shutdown lldp"},
{"CONFIGURE"},
{"lldp_globalconf.html"},
0
},
{
{"show lldp statistics"},
{"EXEC"},
{"lldp_stats.html"},
0
},
{
{"debug lldp [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [neigh-add] [neigh-del] [neigh-updt] [neigh-drop] [neigh-ageout] [critical][tlv {all | [chassis-id][port-id] [ttl] [port-descr] [sys-name] [sys-descr] [sys-capab] [mgmt-addr] [port-vlan] [ppvlan] [vlan-name] [proto-id] [mac-phy] [pwr-mdi] [lagg] [max-frame]}] [redundancy]}]"},
{"EXEC"},
{"lldp_traceconf.html"},
0
},
{
{"clear lldp counters"},
{"CONFIGURE"},
{"lldp_traffic.html"},
0
},
{
{"config-restore {flash | remote ip-addr <ip-address> file <filename> | norestore}"},
{"USEREXEC"},
{"remoterestore.html"},
0
},
{
{"dot1x local-database <username> password <password> permission {allow | deny} [<auth-timeout (value(1-7200))>] [interface <interface-type> <interface-list>]"},
{"CONFIGURE"},
{"pnac_localAS.html"},
0
},
{
{"set out-of-service [enable | disable] [interval {one-sec | one-min}] [period <seconds(0-172800)>] [delay <milliseconds(10-100)>]"},
{"MEP_CONFIG"},
{"ecfm_lck.html"},
0
},
{
{"copy logs {tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename}"},
{"USEREXEC"},
{"log_info.html"},
0
},
{
{"show entity logical [index <integer (1..2147483647)>]"},
{"USEREXEC"},
{"ent_log.html"},
0
},
{
{"ip address  <ip-address> <subnet-mask>"},
{"OOB_CONFIG"},
{"loopback_settings.html"},
0
},
{
{"show ethernet cfm loopback cache [detail] [domain <domain-name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_lbbrief.html"},
0
},
{
{"show ethernet cfm loopback cache [detail] [domain <domain-name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_lbdetail.html"},
0
},
{
{"show ethernet cfm statistics [interface{<interface-type><interface-number>}[domain <domain-name> | level <level-id(0-7)>][{service <service-name> | <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},
{"EXEC"},
{"ecfm_lbrs.html"},
0
},
{
{"ping ethernet [mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>]   {domain <domain-name> | level <level-id(0-7)>} [{service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>}]   [interface <interface-type> <interface-number>]  [direction {inward | outward}][lbm-mode {req-resp | burst}] [data-pattern <string> |  test-pattern null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc] [size <pdu-size(64-9000)> | variable-bytes] [interval <milliseconds(1-600000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>] [stop] [switch <context_name>]"},
{"EXEC"},
{"ecfm_lbm.html"},
0
},
{
{"show entity lp-mapping"},
{"USEREXEC"},
{"ent_LPMapping.html"},
0
},
{
{"show ethernet cfm maintenance-points remote crosscheck [mpid <mep-id(1-8191)>] [domain <domain-name> | level <level-id(0-7)>]  [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance     <service-instance (256-16777214)>}][switch <context_name)>]"},
{"EXEC"},
{"ecfm_mameps.html"},
0
},
{
{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_macfwdtable.html"},
0
},
{
{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_macfwdtable.html"},
0
},
{
{"show dot1x [{ interface <interface-type> <interface-id> | statistics interface <interface-type> <interface-id> | supplicant-statistics interface <interface-type> <interface-id>|local-database | mac-info [address <aa.aa.aa.aa.aa.aa>] | mac-statistics [address <aa.aa.aa.aa.aa.aa>] | all }]"},
{"USEREXEC"},
{"pnac_macsessioninfo.html"},
0
},
{
{"show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_mcasttable.html"},
0
},
{
{"service [format {primary-vid | char-string | unsigned-int16 | rfc2865-vpn-id | icc}] name <service_name> [icc <icc_code> umc <umc_code>] [{vlan <vlan-id/vfi-id> | service-instance <service-instance(256-16777214)>] [mip-creation-criteria {none | default | explicit | defer}] [sender-id-permission {none | chassis | manage | chassis-mgt-address | defer}]"},
{"DOMAIN_CONFIG"},
{"ecfm_maconf.html"},
0
},
{
{"ethernet cfm domain [format {dns-like-name | mac-addr | char-string | none}] name <domain-name> level <level-id(0-7)> "},
{"CONFIGURE"},
{"ecfm_mdconf.html"},
0
},
{
{"net <network-entity-title>"},
{"ROUTERISIS"},
{"isis_maaconf.html"},
0
},
{
{"mef meter <meter-id> cir <0-10485760> cbs <0-10485760> eir <0-10485760> ebs <0-10485760> [color-aware] [coupling-flag]"},
{"CONFIGURE"},
{"meter.html"},
0
},
{
{"mef e-tree evc <evc> ingress-port <port> egress-port-list <port-list>"},
{"CONFIGURE"},
{"mef_etree.html"},
0
},
{
{"ethernet cfm mep {domain <domain-name> | level <0-7>} [inward] mpid <id(1-8191)>  [{service <service-name> | vlan <vlan-id/vfi_id> | service-instance  <integer(256-16777214)>}] [active]"},
{"INTERFACE "},
{"ecfm_mepconf.html"},
0
},
{
{"show ethernet cfm maintenance-points local detail {mpid <mep-id(1-8191)> | mac <aa:aa:aa:aa:aa:aa>} [domain <domain_name> | level <level-id(0-7)>] [service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)>] [switch <context_name>]"},
{"EXEC"},
{"ecfm_mepdetails.html"},
0
},
{
{"ethernet cfm mep {domain <domain-name> | level <0-7>} [inward] mpid <id(1-8191)>  [{service <service-name> | vlan <vlan-id/vfi_id> | service-instance  <integer(256-16777214)>}] [active]"},
{"INTERFACE "},
{"ecfm_mepparamsconf.html"},
0
},
{
{"meter <meter-id(1-65535)>"},
{"CONFIGURE"},
{"qos_meter.html"},
0
},
{
{"meter-type { simpleTokenBucket | avgRate| srTCM | trTCM | tswTCM | mefCoupled | mefDeCoupled } [ color-mode { aware | blind } ] [interval <short(1-10000)>] [cir <integer(0-10485760)>] [cbs <integer(0-10485760)>] [eir <integer(0-10485760)>] [ebs <integer(0-10485760)>] [next-meter <integer(0-65535)>]"},
{"QOSMETER"},
{"qos_metertablesettings.html"},
0
},
{
{"set fault-management ethernet-oam mib-request <branchleaf:branchleaf:...>"},
{"INTERFACE"},
{"fm_mibrequestconfig.html"},
0
},
{
{"show port fault-management ethernet-oam [<interface-type> <interface-id>] mib-variable response"},
{"EXEC"},
{"fm_mibresponse.html"},
0
},
{
{"show ethernet cfm mip-ccm-database [service <service-name> | vlan <vlan-id/vfi-id>] [address <aa:aa:aa:aa:aa:aa>] [interface <interface-type> <interface-id>][switch <context_name>]"},
{"EXEC"},
{"ecfm_mipccmdb.html"},
0
},
{
{"ethernet cfm mip {domain <domain-name> | level <level-id(0-7)>} {service <service-name> | vlan <vlan-id/vfi-id> | service-instance  <integer(256-16777214)>} [active]"},
{"INTERFACE"},
{"ecfm_mipconf.html"},
0
},
{
{"set ipv6 mld { enable | disable }"},
{"CONFIGURE"},
{"mld_conf.html"},
0
},
{
{"ipv6 mld router"},
{"INTERFACE_ROUTER"},
{"mld_iface.html"},
0
},
{
{"show ipv6 mld statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},
{"EXEC"},
{"mldroute_stats.html"},
0
},
{
{"snooping multicast-forwarding-mode {ip | mac}"},
{"CONFIGURE"},
{"mlds_conf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"mlds_intf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"mlds_enhintf.html"},
0
},
{
{"ip igmp snooping leavemode {exp-hosttrack | fastLeave |         normalleave} [InnerVlanId <short (1-4094)>]"},
{"INTERFACE"},
{"mlds_intfconf.html"},
0
},
{
{"ipv6 mld snooping mrouter-time-out <(60 - 600) seconds>"},
{"VCM"},
{"mlds_tmrconf.html"},
0
},
{
{"ipv6 mld snooping version {v1 | v2}"},
{"VCMVLAN_CONFIG"},
{"mlds_vlan.html"},
0
},
{
{"ip igmp snooping mrouter-port <ifXtype> <iface_list> time-out <short(60-600)>"},
{"VLAN_CONFIG"},
{"mlds_rtrportconf.html"},
0
},
{
{"show ipv6 mld snooping mrouter [Vlan <vlan-id/vfi-id>] [detail] [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_rtrtable.html"},
0
},
{
{"show ipv6 mld sources"},
{"EXEC"},
{"mld_src.html"},
0
},
{
{"show ipv6 mld snooping statistics [Vlan <vlan-id/vfi-id>] [switch <string (32)>]"},
{"USEREXEC"},
{"mlds_stats.html"},
0
},
{
{"show ipv6 mld statistics [{ Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>}]"},
{"EXEC"},
{"mlds_v2stats.html"},
0
},
{
{"set { mvrp | mmrp } { enable | disable }"},
{"CONFIGURE"},
{"mrp_mmrpstatus.html"},
0
},
{
{"mpls ip"},
{"INTERFACE_ROUTER"},
{"mplsif_conf.html"},
0
},
{
{"set port mvrp <interface-type> <interface-id> applicant               { normal | non-participant | active }"},
{"CONFIGURE"},
{"mrp_applicantcontrol.html"},
0
},
{
{"show { mrp | mvrp | mmrp } statistics               [{port <interface-type> <interface-id> | switch <context-name>}]"},
{"USEREXEC"},
{"mrp_portrxstats.html"},
0
},
{
{"set port { mrp | mvrp } <interface-type> <interface-id>               periodictimer { enable | disable }"},
{"CONFIGURE"},
{"mrp_portsettings.html"},
0
},
{
{"show { mrp | mvrp | mmrp } statistics               [{port <interface-type> <interface-id> | switch <context-name>}]"},
{"USEREXEC"},
{"mrp_porttxstats.html"},
0
},
{
{"shutdown mrp"},
{"CONFIGURE"},
{"mrp_instance.html"},
0
},
{
{"set port mrp <interface-type> <interface-id> timer { join | leave |             leaveall } <time in centi seconds>"},
{"CONFIGURE"},
{"mrp_timesettings.html"},
0
},
{
{"ip msdp { enable | disable }"},
{"CONFIGURE"},
{"msdp_globalconf.html"},
0
},
{
{"ip msdp mesh-group <mesh Group Name(64)> <Peer IP address>"},
{"CONFIGURE"},
{"msdp_mesh.html"},
0
},
{
{"ip msdp peer-filter {accept-all | deny-all} [{ routemap <route map(20)>}]"},
{"CONFIGURE"},
{"msdp_peerfilter.html"},
0
},
{
{"ip msdp peer <IP address> connect-source {vlan <id>}"},
{"CONFIGURE"},
{"msdp_peertableconf.html"},
0
},
{
{"ip msdp originator-id <IP Address>"},
{"CONFIGURE"},
{"msdp_rp.html"},
0
},
{
{"show ip msdp sa-cache [{ <IP Address> | <Mcast Address>}]"},
{"EXEC"},
{"msdp_sacache.html"},
0
},
{
{"ip msdp redistribute [routemap <routemap name(20)>]"},
{"CONFIGURE"},
{"msdp_saredist.html"},
0
},
{
{"clear spanning-tree [mst <instance-id>] counters  [interface <interface-type> <interface-id>]"},
{"VCM"},
{"mstp_cistportstats.html"},
0
},
{
{"show spanning-tree mst [<instance-id(1-64|4094)>] [detail] [ switch <context_name>]"},
{"EXEC"},
{"mstp_cistportstatus.html"},
0
},
{
{"shutdown spanning-tree"},
{"CONFIGURE"},
{"mstp_globalconf.html"},
0
},
{
{"show spanning-tree detail [ switch <context_name>]"},
{"EXEC"},
{"mstp_info.html"},
0
},
{
{"show spanning-tree mst [<instance-id(1-64|4094)>] interface <interface-type> <interface-id> [{ stats | hello-time | detail }]"},
{"EXEC"},
{"mstp_mstiportstats.html"},
0
},
{
{"spanning-tree mst max-hops <value(6-40)>"},
{"CONFIGURE"},
{"mstp_timersconf.html"},
0
},
{
{"debug spanning-tree { global | { all | errors |init-shut | management | memory | bpdu                | events | timer | state-machine { port-info | port-recieve                          | port-role-selection | role-transition                          | state-transition | protocol-migration                          | topology-change | port-transmit                           | bridge-detection | pseudoInfo} | redundancy | sem-variables} [switch <context_name>]}"},
{"EXEC"},
{"mstp_traces.html"},
0
},
{
{"forward-all ([static-ports ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>] [none])]     [forbidden-ports <interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>]])"},
{"VLAN_CONFIG"},
{"vlan_forwardconf.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"igs_mcastrecv.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"igs_enhmcastrecv.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"igs_recvtable.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_mcastrecv.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_enhmcastrecv.html"},
0
},
{
{"show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]              [switch <switch_name>]"},
{"USEREXEC"},
{"mlds_recvtable.html"},
0
},
{
{"show ip igmp snooping forwarding-database [Vlan <vlan-id/vfi-id>]              [{static | dynamic}] [switch <switch_name>]"},
{"USEREXEC"},
{"igs_mcasttable.html"},
0
},
{
{"set { mvrp | mmrp } { enable | disable }"},
{"CONFIGURE"},
{"mrp_mvrpstatus.html"},
0
},
{
{"ip nat"},
{"CONFIGURE"},
{"nat_basicsettings.html"},
0
},
{
{"static inside <Policy NAT ID (1-65535)> access-list <string>"},
{"CONFIGURE"},
{"nat_policy.html"},
0
},
{
{"ip nat {idle timeout <seconds (60-86400)> |  {tcp | udp } timeout <seconds (300-86400)>}"},
{"CONFIGURE"},
{"nat_timer.html"},
0
},
{
{"ipv6 neighbor [vrf <vrf-name>] <prefix> {vlan <vlan-id/vfi-id> [switch <switch-name>] | tunnel <id>| [<interface-type> <interface-id>] | <IP-interface-type> <IP-interface-number>} <MAC ADDRESS (xx:xx:xx:xx:xx:xx)>"},
{"CONFIGURE"},
{"ND_conf.html"},
0
},
{
{"neighbor <ip-address/peer-group-name> remote-as <AS no> [allow-autostart [idlehold-time <integer(1-65535)>]]"},
{"BGP"},
{"bgp_peerconf.html"},
0
},
{
{"neighbor <ip-address> peer-group <peer-group-name>"},
{"BGP"},
{"bgp_peergroupconf2.html"},
0
},
{
{"show ip bgp [vrf <vrf-name>]{[neighbor [<peer-addr> [received prefix-filter]]]}"},
{"EXEC"},
{"bgp_orffilters.html"},
0
},
{
{"area <AreaId> range <Network> <Mask> {summary | Type7}              [{advertise | not-advertise}] [tag <value>]"},
{"ROUTEROSPF"},
{"ospf_AreaAggregateconf.html"},
0
},
{
{"area <area-id> stub [no-summary]"},
{"ROUTEROSPF"},
{"ospf_areaconf.html"},
0
},
{
{" summary-address <Network> <Mask> <AreaId> [{allowAll | denyAll |  advertise | not-advertise}] [Translation {enabled | disabled}][tag tag-value]"},
{"ROUTEROSPF"},
{"ospf_AsExtAggrconf.html"},
0
},
{
{"set nssa asbr-default-route translator { enable | disable }"},
{"ROUTEROSPF"},
{"ospf_globalconf.html"},
0
},
{
{"distance <1-255> [route-map <name(1-20)>]"},
{"ROUTEROSPF"},
{"fltr_ospfconf.html"},
0
},
{
{"network <Network number> area <area-id> [unnum { Vlan <vlan-id/vfi-id> [switch <switch-name>] | <interface-type> <interface-num> | <IP-interface-type> <IP-interface-number>}]"},
{"ROUTEROSPF"},
{"ospf_ifaceconf.html"},
0
},
{
{"show ip ospf [vrf <name>] [area-id] database [{database-summary | self-originate | adv-router <ip-address>}]"},
{"EXEC"},
{"ospf_linkstdb.html"},
0
},
{
{"neighbor <neighbor-id> [priority  <priority value (0-255)>] [poll-interval seconds] [cost number] [database-filter all]"},
{"ROUTEROSPF"},
{"ospf_nbrconf.html"},
0
},
{
{"show ip ospf redundancy"},
{"EXEC"},
{"ospfred_stats.html"},
0
},
{
{"show ip ospf [vrf <name>] route"},
{"EXEC"},
{"ospfroute_stats.html"},
0
},
{
{"redist-config <Network> <Mask> [metric-value <metric (1 - 16777215)>] [metric-type {asExttype1 | asExttype2}]  [tag <tag-value>}"},
{"ROUTEROSPF"},
{"ospf_RRDrouteconf.html"},
0
},
{
{"area <area-id> virtual-link <router-id> [authentication { simple | message-digest | sha-1 | sha-224 | sha-256 | sha384 | sha-512 | null}] [hello-interval <value (1-65535)>] [retransmit-interval <value (1-3600)>] [transmit-delay <value (1-3600)>] [dead-interval <value>] [{authentication-key <key (8)> | message-digest-key <Key-id (0-255)> {md5 | sha-1 | sha-224 | sha-256 | sha-384 | sha-512} <key (16)>}]"},
{"ROUTEROSPF"},
{"ospf_virtualifaceconf.html"},
0
},
{
{"router ospf [vrf <name>]"},
{"CONFIGURE"},
{"ospf_context_creation.html"},
0
},
{
{"area <area-id> { { stub | nssa } [no-summary] }"},
{"ROUTEROSPF3"},
{"ospfv3area_conf.html"},
0
},
{
{"router-id <IPv4-Address>"},
{"ROUTEROSPF3"},
{"ospfv3basic_conf.html"},
0
},
{
{"distance <1-255> [route-map <name(20)>]"},
{"ROUTEROSPF3"},
{"fltr_ospfv3conf.html"},
0
},
{
{"timers spf <spf-delay> <spf-holdtime>"},
{"ROUTEROSPF3"},
{"ospfv3global_conf.html"},
0
},
{
{"show ipv6 ospf redundany"},
{"USEREXEC"},
{"ospfv3red_stats.html"},
0
},
{
{"show ipv6 ospf [vrf <contextname>] route"},
{"USEREXEC"},
{"ospfv3route_stats.html"},
0
},
{
{"ipv6 router ospf [vrf <contextname>]"},
{"CONFIGURE"},
{"ospfv3context_creation.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbvlan_cvidregi.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbvlan_portinfo.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbvlan_vidtransl.html"},
0
},
{
{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},
{"EXEC"},
{"pbrstp_portstats.html"},
0
},
{
{"show spanning-tree bridge [{ address | forward-time | hello-time | id |    max-age | protocol | priority | detail }] [ switch <context_name>]"},
{"EXEC"},
{"pbrstp_portinfo.html"},
0
},
{
{"pcp-decoding <pcpselectionrow(8P0D|7P1D|6P2D|5P3D)> pcp <recv    priority (0-7)> priority <decoding priority (0-7)> drop-eligible {true|false}"},
{"INTERFACE "},
{"pbvlan_pcpdecoding.html"},
0
},
{
{"pcp-encoding <pcpselectionrow(8P0D|7P1D|6P2D|5P3D)> priority     <decoded priority (0-7)> drop-eligible {true|false} pcp <encoding priority (0-7)>"},
{"INTERFACE "},
{"pbvlan_pcpencoding.html"},
0
},
{
{"neighbor <peer-group-name> peer-group"},
{"BGP"},
{"bgp_peergroupadd.html"},
0
},
{
{"neighbor <peer-group-name> peer-group"},
{"BGP"},
{"bgp_peergroupconf1.html"},
0
},
{
{"neighbor <ip-address|peer-group-name> capability {ipv4-unicast|ipv6-unicast|route-refresh |               orf prefix-list {send | receive | both} | l2vpn-vpls}"},
{"AF_BGP"},
{"bgp_peerorfconf.html"},
0
},
{
{"set priority-flow-control  {enable | disable}"},
{"INTERFACE "},
{"pfc_adminportconfig.html"},
0
},
{
{"shutdown pfc"},
{"CONFIGURE"},
{"pfc_GlobalInfo.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] priority-flow-control [detail]"},
{"USEREXEC"},
{"pfc_localInfo.html"},
0
},
{
{"show interfaces priority-flow-control  counters [ <ifXtype> <ifnum> ]"},
{"USEREXEC"},
{"pfc_portStat.html"},
0
},
{
{"show interfaces [<ifXtype> <ifnum> ] priority-flow-control [detail]"},
{"USEREXEC"},
{"pfc_remoteInfo.html"},
0
},
{
{"set priority <priority-list> flow-control {enable|disable}"},
{"INTERFACE "},
{"pfc_statusPerPriority.html"},
0
},
{
{"show entity phy-containment [index <integer (1..2147483647)>]"},
{"USEREXEC"},
{"ent_ContainsMapping.html"},
0
},
{
{"set entity physical-index <integer (1..2147483647)> {[asset-id <SnmpAdminString (Size (1..32))>] [serial-number  <SnmpAdminString (Size (1..32))>] [alias-name <SnmpAdminString (Size (1..32))>] [uris <OCTET-STRING (Size (1..255))>]}"},
{"CONFIGURE"},
{"ent_phy.html"},
0
},
{
{"set ip pim { enable | disable }"},
{"CONFIGURE"},
{"pim_basicsettings.html"},
0
},
{
{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},
{"CONFIGURE"},
{"pim_component.html"},
0
},
{
{"show ip pim interface [{ Vlan <vlan-id/vfi-id> [df] | <interface-type> <interface-id> [df] | <IP-interface-type> <IP-interface-number> | detail }]"},
{"EXEC"},
{"pim_dfinfo.html"},
0
},
{
{"ip pim state-refresh disable"},
{"CONFIGURE"},
{"pimdm_globconf.html"},
0
},
{
{"show ip pim rp-hash [<multicast_Group_address> <Group_mask>]"},
{"EXEC"},
{"pim_electedrp.html"},
0
},
{
{"ip pim bidir-offer-interval <offer-interval> msecs"},
{"CONFIGURE"},
{"pim_globalconf.html"},
0
},
{
{"show ip pim redundancy state"},
{"EXEC"},
{"pim_ha.html"},
0
},
{
{"ip pim componentId  <value(1-255)>"},
{"INTERFACE_ROUTER"},
{"pim_ifaceconf.html"},
0
},
{
{"show ip pim mroute [bidir] [ {proxy | {compid(1-255) | group-address | source-address } summary } ]"},
{"EXEC"},
{"pim_routeinfo.html"},
0
},
{
{"show ip pim rp-set [rp-address] [bidir]"},
{"EXEC"},
{"pim_rpsetinfo.html"},
0
},
{
{"debug dot1x {all | errors | events | packets |  state-machine | redundancy | registry}"},
{"USEREXEC"},
{"pnac_traces.html"},
0
},
{
{"mef policy-map <policy-map-id (1-65535)> class <1-65535>               [meter <1-65535>]"},
{"CONFIGURE"},
{"policymap.html"},
0
},
{
{"ipv6 policy-prefix  <prefix> <prefix Len> precedence <integer> label <integer> [{unicast| anycast}]"},
{"INTERFACE_ROUTER"},
{"policyprefix_conf.html"},
0
},
{
{"ipsecv6 policy <policyindex> {apply|bypass} {manual|automatic} <sa-index>"},
{"CRYPTO"},
{"secv6_policyconf.html"},
0
},
{
{"set meter <integer(1-65535)> [ conform-action { drop | set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)> | set-port <iftype> <ifnum> | set-inner-vlan-pri <short(0-7)> |set-mpls-exp-transmit <short(0-7)> | set-ip-prec-transmit <short(0-7)> | set-ip-dscp-transmit <short(0-63)> }] [ exceed-action {drop | set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)> | set-inner-vlan-pri <short(0-7)> | set-mpls-exp-transmit <short(0-7)> | set-ip-prec-transmit <short(0-7)> | set-ip-dscp-transmit <short(0-63)> }] [ violate-action {drop | set-cos-transmit <short(0-7)> set-de-transmit <short(0-1)> | set-inner-vlan-pri <short(0-7)> | set-mpls-exp-transmit <short(0-7)> | set-ip-prec-transmit <short(0-7)> | set-ip-dscp-transmit <short(0-63)> }] [ set-conform-newclass <integer(0-65535)> ] [ set-exceed-newclass <integer(0-65535)> ] [ set-violate-newclass <integer(0-65535)> ]"},
{"QOSPLYMAP"},
{"qos_policymapsettings.html"},
0
},
{
{"bridge port-type { providerNetworkPort | customerNetworkPort {port-based | s-tagged| c-tagged} | customerEdgePort | propCustomerEdgePort | propCustomerNetworkPort | propProviderNetworkPort | customerBridgePort| customerBackbonePort }"},
{"INTERFACE"},
{"port_settings.html"},
0
},
{
{"set port-channel { enable | disable }"},
{"CONFIGURE"},
{"la_interfacesettings.html"},
0
},
{
{"ethernet cfm enable"},
{"INTERFACE "},
{"pvrst_portconf.html"},
0
},
{
{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},
{"PO "},
{"ecfm_portconf.html"},
0
},
{
{"negotiation"},
{"INTERFACE "},
{"port_control.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"port_creation.html"},
0
},
{
{"port-isolation in_vlan_ID [{add|remove}] port_list"},
{"INTERFACE"},
{"portisolation_conf.html"},
0
},
{
{"monitor session <session-id (1-20)> { source              {interface <interface-type> <interface-id> [{rx|tx|both}] |              tunnel <tunnel-id> [{rx|tx|both}]|              vlan <vlan_range> [switch <context_name> ][{rx|tx|both}]              |mac-acl <acl-id> |ip-acl <acl-id>|remote vlan <vlan-id>              [switch <context_name>]}} [ COMP ] "},
{"CONFIGURE"},
{"port_monitoring.html"},
0
},
{
{"aaa authentication dot1x default { group {radius | tacacsplus | tacacs+} | local }"},
{"CONFIGURE"},
{"pnac_portsecurity.html"},
0
},
{
{"spanning-tree mst <instance-id(1-64)> { cost <value(1-200000000)>| port-priority <value(0-240)> | disable }"},
{"VCMMST"},
{"msti_portconf.html"},
0
},
{
{"spanning-tree [{cost <value(0-200000000)> | disable | link-type{point-to-point | shared}| portfast | port-priority <value(0-240)>}]"},
{"INTERFACE "},
{"rstp_portconf.html"},
0
},
{
{"switchport map protocols-group <Group id integer(0-2147483647)> vlan <vlan-id/vfi_id>"},
{"INTERFACE"},
{"vlan_portvidset.html"},
0
},
{
{"ipv6 nd prefix {<prefix addr> <prefixlen> | default} [{{<valid lifetime> | infinite | at <var valid lifetime>  }{<preferred lifetime> |infinite | at <var preferred lifetime>} | no-advertise}]"},
{"INTERFACE_ROUTER"},
{"pref_conf.html"},
0
},
{
{"priority-map <priority-map-Id(1-65535)>"},
{"CONFIGURE"},
{"qos_prioritymapsettings.html"},
0
},
{
{"mac-learn-rate {<no of MAC entries(0-2147483647)>} [interval {<milliseconds(1-100000)>}]"},
{"CONFIGURE"},
{"protCPUoveloading.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbtunnel_statusconfig.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbvlan_pepconf.html"},
0
},
{
{"mpls l2transport { manual {<peer-address> |            point-to-multipoint <p2mp_id>}           | pwidfec <peer-address> pwid <pwid>            groupid <groupid> | genfec {<peer-address> |            [global-id <destination global id>]            node-id <destination node id> |           point-to-multipoint <p2mp_id>} {agi <attachment group id> |           { agiType <AgiType> {{ASN <AS Number>} | {< IP Address>} }            id <Assigned number>}} {saii { <ip-addr> | <saii>}           taii { <ip-addr> | <taii> }}           | src-ac-id <source ac id>            dst-ac-id <destination ac id>}}[pwid <integer>]           [locallabel <InBoundLabel>            remotelabel <OutBoundLabel>]            [control-word {disable | enable}] [vccv            disable] mplstype {te <out-tunnel-id>            <in-tunnel-id> | non-te | vconly} [out-tnl-destination            {<out-tunnel-dest-ip> | <out-tunnel-dest-local-map-number>}            out-tnl-source {<out-tunnel-src-ip> |            <out-tunnel-src-local-map-number>}]            [in-tnl-destination {<in-tunnel-dest-ip> |            <in-tunnel-dest-local-map-number>} in-tnl-source {<in-tunnel-src-ip> |           <in-tunnel-src-local-map-number>}][entity <entity-id (1-16)>][vlanmode            {other | portbased | nochange | changevlan            <vlan-id (1-4094)> | addvlan <vlan-id (1-4094)> | removevlan            <vlan-id (1-4094)> }] [port-ifindex <interface-type>            <interface-id>][inactive] PWType {ethtag | eth}           [pseudowire-redundancy class-id <class_id)>] [mtu <size (1-65535)>]"},
{"VCM"},
{"mplspw_conf.html"},
0
},
{
{"xconnect vfi <vfi-name (32)> [vlanmode            {other | portbased | nochange | changevlan            <vlan-id (1-4094)> | addvlan <vlan-id (1-4094)> | removevlan            <vlan-id (1-4094)> }] [port-vlan vlan <vlan-id (1-4094)>]"},
{"INTERFACE"},
{"mplsenet_conf.html"},
0
},
{
{"ptp acceptable-master enable [domain <short(0-127)>]"},
{"INTERFACE"},
{"ptp_accmaster.html"},
0
},
{
{"ptp alternate-time-scale key <value(0-254)> name <string(10)>"},
{"CLKMODE"},
{"ptp_alttime.html"},
0
},
{
{"show ptp foreign-master-record [{ vrf | switch } <context-name>] [domain <id (0-127)>]"},
{"USEREXEC"},
{"ptp_foreignmaster.html"},
0
},
{
{"shutdown ptp"},
{"CONFIGURE"},
{"ptp_globalconf.html"},
0
},
{
{"ptp port [{ ipv4 | ipv6 }] {<iftype> <ifindex> | vlan <vlan-id/vfi-id>}"},
{"CLKMODE"},
{"ptp_if_conf.html"},
0
},
{
{"show ptp parent [{ vrf | switch } <context-name>] [domain <id (0-127)>] [stats]"},
{"USEREXEC"},
{"ptp_parentinfo.html"},
0
},
{
{"show ptp counters [{ vrf | switch } <context-name>][domain <id (0-127)>]"},
{"USEREXEC"},
{"ptp_portstats.html"},
0
},
{
{"show ptp port [{[{ vrf | switch } <string (32)>] | [{ ipv4 | ipv6 }]          {<ifXtype> <ifnum> | vlan <vlan-id/vfi-id> [switch <string(32)>]}}]         [domain <short (0-127)>]"},
{"USEREXEC"},
{"ptp_portinfo.html"},
0
},
{
{"show ptp time-property [{ vrf | switch } <context-name>][domain <id(0-127)>]"},
{"USEREXEC"},
{"ptp_timeinfo.html"},
0
},
{
{"show spanning-tree vlan <vlan-id/vfi-id> bridge [{address | detail | forward-time | hello-time | id | max-age | priority [system-id] | protocol}][ switch <context_name>]"},
{"EXEC"},
{"pvrst_info.html"},
0
},
{
{"show spanning-tree vlan <vlan-id/vfi-id> detail [active] [ switch <context_name>]"},
{"EXEC"},
{"pvrst_instinfo.html"},
0
},
{
{"show spanning-tree interface <ifXtype> <ifnum> [{ cost | encapsulationtype | priority | portfast | rootcost |rootguard | restricted-role | restricted-tcn | state | stats | detail }]"},
{"EXEC"},
{"pvrst_instportinfo.html"},
0
},
{
{"show spanning-tree vlan <vlan-id/vfi-id> interface <ifXtype> <ifnum> [{ cost | detail | priority | rootcost | state | stats }]"},
{"EXEC"},
{"pvrst_portinfo.html"},
0
},
{
{"map pwid <integer> <ifXtype> <ifnum>"},
{"CONFIGURE"},
{"mplsmap_conf.html"},
0
},
{
{"show qos queue-stats [interface <iftype> <ifnum>]"},
{"USEREXEC"},
{"qos_cosstats.html"},
0
},
{
{"show qos meter-stats [<Meter-Id(1-65535)>]"},
{"USEREXEC"},
{"qos_policerstats.html"},
0
},
{
{"cpu rate limit queue <integer(1-65535)> minrate <integer(1-65535)> maxrate <integer(1-65535)>"},
{"CONFIGURE"},
{"qos_stdqueuetable.html"},
0
},
{
{"queue-map { CLASS <integer(1-65535)> | regn-priority { vlanPri | ipTos | ipDscp | mplsExp | vlanDEI } <integer(0-63)> } [interface <iftype> <ifnum>] queue-id <integer(1-65535)>"},
{"CONFIGURE"},
{"qos_queuemap.html"},
0
},
{
{"queue <integer(1-65535)> interface <iftype> <ifnum> [qtype <integer(1-65535)>] [scheduler <integer(1-65535)>] [weight <integer(0-1000)>] [priority <integer(0-15)>] [shaper <integer(0-65535)>] [queue-type {unicast | multicast }]"},
{"CONFIGURE"},
{"qos_queuetable.html"},
0
},
{
{"queue-type <Q-Template-Id(1-65535)>"},
{"CONFIGURE"},
{"qos_queuetemplate.html"},
0
},
{
{"radius-server host {ipv4-address |ipv6-address | host-name} [auth-port <integer(1-65535)>] [acct-port <integer(1-65535)>] [timeout <1-120>] [retransmit <1-254>] [key <secret-key-string>] [primary]"},
{"CONFIGURE"},
{"pnac_radClient.html"},
0
},
{
{"show radius statistics"},
{"USEREXEC"},
{"radius_statistics.html"},
0
},
{
{"debug radius {all | errors | events | packets | responses | timers}"},
{"USEREXEC"},
{"radius_traces.html"},
0
},
{
{"reload"},
{"USEREXEC"},
{"reboot.html"},
0
},
{
{"queue-type <Q-Template-Id(1-65535)>"},
{"CONFIGURE"},
{"qos_redconf.html"},
0
},
{
{"peer-dead-interval <interval(40-20000)>"},
{"REDUNDANCY"},
{"rmconfig.html"},
0
},
{
{"show ethernet cfm maintenance-points remote detail {mpid <mep-id(1-8191)> | mac <aa:aa:aa:aa:aa:aa> }[domain <domain-name> | level <level-id(0-7)> [{service <service-name> | unaware | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>}]][switch <context_name>]"},
{"EXEC"},
{"ecfm_rmeplist.html"},
0
},
{
{"config-restore {flash | remote ip-addr <ip-address> file <filename> | norestore}"},
{"USEREXEC"},
{"restore.html"},
0
},
{
{"aps ring group <group-id>"},
{"CONFIGURE"},
{"erps_ringcfm.html"},
0
},
{
{"aps timers periodic <integer>  {milliseconds | seconds | minutes | hours}  [hold-off <integer> {milliseconds | seconds | minutes | hours}] [guard <integer> {milliseconds | seconds | minutes | hours}]"},
{"VCMERPS_CONFIG"},
{"erps_ringconfig.html"},
0
},
{
{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>] "},
{"USEREXEC"},
{"erps_ringstats.html"},
0
},
{
{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>]"},
{"USEREXEC"},
{"erps_ringstats_rx.html"},
0
},
{
{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>] "},
{"USEREXEC"},
{"erps_ringstats_tx.html"},
0
},
{
{"show aps ring [group <group_id>] [{configuration | statistics | timers }] [switch <context_name>]"},
{"USEREXEC"},
{"erps_ringstatus.html"},
0
},
{
{"aps ring group <group-id>"},
{"CONFIGURE"},
{"erps_ringtable.html"},
0
},
{
{"aps group name <group_name> ring group <group_id>"},
{"CONFIGURE"},
{"erps_ringtcprop.html"},
0
},
{
{"aps group name <group_name> ring group <group_id>"},
{"VCM"},
{"erps_ringtimers.html"},
0
},
{
{"router rip [vrf <name>]"},
{"CONFIGURE"},
{"rip_globalconf.html"},
0
},
{
{"distance <1-255> [route-map <name(1-20)>]"},
{"RIP"},
{"fltr_ripconf.html"},
0
},
{
{"network <ip-address>[unnum {vlan <vlan-id/vfi-id> [switch <switch-name>] | <iftype> <ifnum>}]"},
{"RIP"},
{"rip_ifaceconf.html"},
0
},
{
{"ip rip summary-address <ip-address> <mask>"},
{"PPP"},
{"rip_aggconf.html"},
0
},
{
{"show ip rip [vrf <name>] { database [ <ip-address> <ip-mask> ] | statistics }"},
{"USEREXEC"},
{"rip_interfacestats.html"},
0
},
{
{"neighbor <ip address>"},
{"RIP"},
{"rip_nbrfilter.html"},
0
},
{
{"network <ip-address>[unnum {vlan <vlan-id/vfi-id> [switch <switch-name>] | <iftype> <ifnum>}]"},
{"RIP"},
{"rip_keyconf.html"},
0
},
{
{"ip vrf <vrf-name> [mpls]"},
{"CONFIGURE"},
{"ripcontext_creation.html"},
0
},
{
{"distribute prefix <ip6_addr> {in | out}"},
{"RIP6"},
{"rip6_filt.html"},
0
},
{
{"distance <1-255> [route-map <name(1-20)>]"},
{"RIP6"},
{"fltr_rip6conf.html"},
0
},
{
{"ipv6 rip enable"},
{"INTERFACE_ROUTER"},
{"rip6_IfConf.html"},
0
},
{
{"show ipv6 rip stats"},
{"EXEC"},
{"rip6_interfacestats.html"},
0
},
{
{"show ipv6 rip {database}"},
{"USEREXEC"},
{"rip6_route.html"},
0
},
{
{"rmon alarm <alarm-number> <mib-object-id (255)>   <sample-interval-time (1-65535)> {absolute | delta }   rising-threshold <value (0-2147483647)> [rising-event-number (1-65535)]   falling-threshold <value (0-2147483647)> [falling-event-number (1-65535)]   [owner <ownername (127)>]"},
{"CONFIGURE"},
{"rmon_alarmconf.html"},
0
},
{
{"set rmon { enable | disable }"},
{"CONFIGURE"},
{"rmon_globalconf.html"},
0
},
{
{"show rmon [statistics [<stats-index (1-65535)>]] [alarms] [events] [history [history-index (1-65535)] [overview]]"},
{"EXEC"},
{"rmon_etherstats.html"},
0
},
{
{"rmon2 {enable | disable}"},
{"CONFIGURE"},
{"rmonv2_globalconf.html"},
0
},
{
{"route-map <name(1-20)> [ {permit | deny }] [ <seqnum(1-10)> ]"},
{"CONFIGURE"},
{"rmap_conf.html"},
0
},
{
{"route-map <name(1-20)> [ {permit | deny }] [ <seqnum(1-10)> ]"},
{"CONFIGURE"},
{"rmapmatch_conf.html"},
0
},
{
{"set next-hop ip <next-hop ip address>"},
{"ROUTEMAP"},
{"rmapset_conf.html"},
0
},
{
{"bgp comm-policy <ip-address> <prefixlen> <set-add|set-none|modify>"},
{"BGP"},
{"bgp_commlocpolicyconf.html"},
0
},
{
{"bgp ecomm-policy <ip-address> <prefixlen> <set-add|set-none|modify>"},
{"BGP"},
{"bgp_ecommlocpolicyconf.html"},
0
},
{
{"bgp ecomm-route {additive|delete} <ip-address> <prefixlen> ecomm-value <value(xx:xx:xx:xx:xx:xx:xx:xx)>"},
{"BGP"},
{"bgp_ecommrouteconf.html"},
0
},
{
{"as-num <value(1-65535)> [vrf <vrf-name>]"},
{"CONFIGURE"},
{"rrd_globalconf.html"},
0
},
{
{"no shutdown ip bgp"},
{"CONFIGURE"},
{"rrd_bgpconf.html"},
0
},
{
{"redistribute <static | connected | rip | all> [route-map <string(20)>] [metric <integer>]"},
{"BGP"},
{"rrd6_bgp4conf.html"},
0
},
{
{"router ospf [vrf <name>]"},
{"CONFIGURE"},
{"rrd_ospfconf.html"},
0
},
{
{"router rip [vrf <name>]"},
{"CONFIGURE"},
{"rrd_ripconf.html"},
0
},
{
{"redistribute-policy [vrf <vrf-name>] ipv6 {permit|deny}  <DestIp> <DestRange> {static|local|rip|ospf}  {rip|ospf|all}"},
{"CONFIGURE"},
{"rrd6_filterconf.html"},
0
},
{
{"ipv6 router ospf [vrf <contextname>]"},
{"CONFIGURE"},
{"rrd6_ospfv3conf.html"},
0
},
{
{"ipv6 router rip"},
{"CONFIGURE"},
{"rrd6_rip6conf.html"},
0
},
{
{"spanning-tree [mst <instance-id>] priority <value(0-61440)>"},
{"CONFIGURE"},
{"rstp_timersconf.html"},
0
},
{
{"spanning-tree flush-interval <centi-seconds (0-500)>"},
{"VCM"},
{"rstp_globalconf.html"},
0
},
{
{"show spanning-tree detail [ switch <context_name>]"},
{"EXEC"},
{"rstp_info.html"},
0
},
{
{"show spanning-tree detail [ switch <context_name>]"},
{"EXEC"},
{"rstp_portinfo.html"},
0
},
{
{"clear spanning-tree [mst <instance-id>] counters  [interface <interface-type> <interface-id>]"},
{"VCM"},
{"rstp_portstats.html"},
0
},
{
{"show spanning-tree interface <interface-type> <interface-id> [{ cost | priority | portfast | rootcost | restricted-role | restricted-tcn | state | stats | detail }] "},
{"EXEC"},
{"rstp_portstatus.html"},
0
},
{
{"debug spanning-tree { global | { all | errors |init-shut | management | memory | bpdu                | events | timer | state-machine { port-info | port-recieve                          | port-role-selection | role-transition                          | state-transition | protocol-migration                          | topology-change | port-transmit                           | bridge-detection | pseudoInfo} | redundancy | sem-variables} [switch <context_name>]}"},
{"EXEC"},
{"rstp_traces.html"},
0
},
{
{"copy { tftp://ip-address/filename startup-config | sftp://<user-name>:<pass-word>@ip-address/filename startup-config | flash: filename startup-config }"},
{"USEREXEC"},
{"save.html"},
0
},
{
{"scheduler <integer(1-65535)> interface <iftype> <ifnum> [sched-algo {strict-priority | rr | wrr | wfq | strict-rr | strict-wrr | strict-wfq | deficit-rr}] [shaper <integer(0-65535)>] [hierarchy-level <integer(0-10)>]"},
{"CONFIGURE"},
{"qos_schedulertable.html"},
0
},
{
{"ipsecv6 sa <sa-index> <peeraddress> <spi> {transport|tunnel} [antireplay-enable]"},
{"CRYPTO"},
{"secv6_saconf.html"},
0
},
{
{"ipsecv6 selector {vlan <id>|tunnel <id>|any} {tcp|udp|icmpv6|ah|esp|any} {port-no|any} {inbound|outbound|any} <accesslist-index> <policy-index> {filter|allow} [{LocalTunnelIP}]"},
{"CRYPTO"},
{"secv6_selectorconf.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"pbvlan_priorityregen.html"},
0
},
{
{"vlan <vlan-id/vfi_id>"},
{"CONFIGURE"},
{"pb_servtypeconf.html"},
0
},
{
{"shape-template <integer(1-65535)> [cir <integer(1-10485760)>] [cbs <integer(0-10485760)>] [eir <integer(0-10485760)>] [ebs <integer(0-10485760)>]"},
{"CONFIGURE"},
{"qos_shapetemplate.html"},
0
},
{
{"enable snmpsubagent { master { ip4 <ipv4_address>      | ip6 <ip6_addr>            } [port <number>] [context <context-name>]}"},
{"CONFIGURE"},
{"snmp_homepage.html"},
0
},
{
{"show snmp agentx statistics"},
{"USEREXEC"},
{"snmp_agentx_statistics.html"},
0
},
{
{"show snmp agentx information"},
{"USEREXEC"},
{"snmp_agentx_conf.html"},
0
},
{
{"snmp-server trap tcp-port <port>"},
{"CONFIGURE"},
{"snmp_basicsettings.html"},
0
},
{
{"snmp community index <CommunityIndex> name <CommunityName>              security <SecurityName> [context <name>]              [{volatile | nonvolatile}] [transporttag <TransportTagIdentifier | none>] [contextengineid <ContextEngineID>]"},
{"CONFIGURE"},
{"snmp_communityconf.html"},
0
},
{
{"snmp targetparams <ParamName> user <UserName>              security-model {v1 | v2c | v3 {auth | noauth | priv}}              message-processing {v1 | v2c | v3} [{volatile | nonvolatile}]              [filterprofile-name <profilename> ] [filter-storagetype {volatile                | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_filterprof.html"},
0
},
{
{"snmp filterprofile <profile-name> <OIDTree> [mask <OIDMask>]               {included | excluded} [{volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_filterconf.html"},
0
},
{
{"snmp access <GroupName> {v1 | v2c | v3 {auth | noauth | priv}}              [read <ReadView | none>] [write <WriteView | none>]              [notify <NotifyView | none>] [{volatile | nonvolatile}] [context <string(32)>]"},
{"CONFIGURE"},
{"snmp_accessconf.html"},
0
},
{
{"snmp group <GroupName> user <UserName>               security-model {v1 | v2c | v3 } [{volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_groupconf.html"},
0
},
{
{"snmp mibproxy name <ProxyName> proxytype {read | write | inform | trap} mibid <MibId> targetparamsin <TargetParam> targetout <TargetOut> [storagetype {volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_mibproxy.html"},
0
},
{
{"snmp proxy name <ProxyName> proxytype {read | write | inform | trap} contextengineid <engine ID> targetparamsin <TargetParam> targetout <TargetOut> [contextname <ProxyContextName>] [storagetype {volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_proxy.html"},
0
},
{
{"snmp user <UserName> [auth {md5 | sha} <passwd>              [priv {{{DES | AES_CFB128}  <passwd> } | None}]] [{volatile | nonvolatile}] [EngineId <EngineID>]"},
{"CONFIGURE"},
{"snmp_userconf.html"},
0
},
{
{"show snmp"},
{"USEREXEC"},
{"snmp_statistics.html"},
0
},
{
{"snmp targetaddr <TargetAddressName> param <ParamName>              {<IPAddress> | <IP6Address>} [timeout <Seconds(1-1500)]              [retries <RetryCount(1-3)] [taglist <TagIdentifier | none>]              [{volatile | nonvolatile}] [port <integer (1-65535)>]"},
{"CONFIGURE"},
{"snmp_tgtaddrconf.html"},
0
},
{
{"snmp targetparams <ParamName> user <UserName>              security-model {v1 | v2c | v3 {auth | noauth | priv}}              message-processing {v1 | v2c | v3} [{volatile | nonvolatile}]              [filterprofile-name <profilename> ] [filter-storagetype {volatile                | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_tgtparamconf.html"},
0
},
{
{"snmp notify <NotifyName> tag <TagName> type {Trap | Inform}              [{volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_notifconf.html"},
0
},
{
{"snmp view <ViewName> <OIDTree> [mask <OIDMask>] {included | excluded}              [{volatile | nonvolatile}]"},
{"CONFIGURE"},
{"snmp_viewconf.html"},
0
},
{
{"set sntp broadcast-mode send-request {enabled | disabled}"},
{"SNTP"},
{"sntp_broadcastconf.html"},
0
},
{
{"set sntp manycast-poll-interval [<value (16-16284) seconds>]"},
{"SNTP"},
{"sntp_anycastconf.html"},
0
},
{
{"set sntp multicast-mode send-request {enabled | disabled}"},
{"SNTP"},
{"sntp_multicastconf.html"},
0
},
{
{"set sntp client {enabled | disabled}"},
{"SNTP"},
{"sntp_scalarsconf.html"},
0
},
{
{"set sntp unicast-server {ipv4 <ucast_addr> |ipv6 <ip6_addr> | domain-name <string(64)>} [{primary | secondary}] [version { 3 | 4 }] [port <integer(1025-36564)>]"},
{"SNTP"},
{"sntp_unicastconf.html"},
0
},
{
{"archive  download-sw  /overwrite [ /reload ] { tftp://ip-address/filename | sftp://<user-name>:<pass-word>@ip-address/filename | flash:filename}"},
{"USEREXEC"},
{"upgrade_sw.html"},
0
},
{
{"ssh {enable | disable}"},
{"CONFIGURE"},
{"ssh_globalconf.html"},
0
},
{
{"debug ssh ([all] [shut] [mgmt] [data] [ctrl] [dump] [resource] [buffer] [server])"},
{"USEREXEC"},
{"ssh_traces.html"},
0
},
{
{"ssl gen cert-req algo rsa sn <SubjectName>"},
{"USEREXEC"},
{"ssl_digitalcert.html"},
0
},
{
{"version {all | ssl3 | tls1}"},
{"CONFIGURE"},
{"ssl_globalconf.html"},
0
},
{
{"debug ssl ([all] [shut] [mgmt] [data] [ctrl] [dump] [resource] [buffer])"},
{"USEREXEC"},
{"ssl_traces.html"},
0
},
{
{"show stack { brief | counters | switchid <integer(1-16)> | details}"},
{"BOOTCONFIG"},
{"stackshowbrief.html"},
0
},
{
{"stack {priority {PM | BM | PS}}{switchid <NodeId (1-16)>}{ports <StackPortCount(1-4)>}"},
{"BOOTCONFIG"},
{"stackconfigure.html"},
0
},
{
{"show stack { brief | counters | switchid <integer(1-16)> | details}"},
{"BOOTCONFIG"},
{"stackcounters.html"},
0
},
{
{"show stack { brief | counters | switchid <integer(1-16)> | details}"},
{"BOOTCONFIG"},
{"stackshowdetails.html"},
0
},
{
{"static nat <local ip> <translated local ip>"},
{"PPP"},
{"nat_static.html"},
0
},
{
{"ip pim component <ComponentId (1-255)> [Scope-zone-name(64)]"},
{"CONFIGURE"},
{"pim_srpconf.html"},
0
},
{
{"ports [add] ([<interface-type> <0/a-b,0/c,...>] [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [pw <a,b,c-d>])        [untagged (<interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>] [ac <a,b,c-d>] [all])]         [forbidden <interface-type> <0/a-b,0/c,...> [<interface-type> <0/a-b,0/c,...>] [port-channel <a,b,c-d>] [pw <a,b,c-d>][ac <a,b,c-d>]]       [name <vlan-name>]"},
{"VLAN_CONFIG"},
{"vlan_staticconf.html"},
0
},
{
{"summary-address {<address> <mask> | <ip6_addr> <prefixlength>}{level-1 | level1-2 | level-2-only}"},
{"ROUTERISIS"},
{"isis_summaddrconf.html"},
0
},
{
{"switch <name>"},
{"CONFIGURE"},
{"context_creation.html"},
0
},
{
{"switchport filtering-utility-criteria {default | enhanced}"},
{"INTERFACE"},
{"vlan_switchport_filtering.html"},
0
},
{
{"logging-server <short(0-191)> {ipv4 <ucast_addr> | ipv6 <ip6_addr> | <host-name>} [ port <integer(0-65535)>] [{udp | tcp | beep}]"},
{"CONFIGURE"},
{"bsdsyslog_fwdtable.html"},
0
},
{
{"mail-server <short(0-191)> {ipv4 <ucast_addr> |ipv6 <ip6_addr> | <host-name>} <string(50)> [user <user_name> password <password>]"},
{"CONFIGURE"},
{"bsdsyslog_mailtable.html"},
0
},
{
{"clock set hh:mm:ss <day (1-31)> {january|february|march|april|may|june|july|august|september|october|november|december} <year (2000 - 2035)>"},
{"USEREXEC"},
{"gen_info.html"},
0
},
{
{"audit-logging { enable | disable}"},
{"CONFIGURE"},
{"audit_log.html"},
0
},
{
{"set switch temperature {min|max} threshold <celsius (-14 - 40)>}"},
{"CONFIGURE"},
{"system_resources.html"},
0
},
{
{"ip mcast profile <profile-id> [description (128)]"},
{"CONFIGURE"},
{"tac_profile.html"},
0
},
{
{"ip mcast profile <profile-id> [description (128)]"},
{"CONFIGURE"},
{"tac_prffilter.html"},
0
},
{
{"debug tacm {all | [entry] [exit] [filter] [critical] [init-shut] [mgmt] [ctrl] [resource] [all-fail]}"},
{"EXEC"},
{"tac_traces.html"},
0
},
{
{"tacacs use-server address {<ipv4-address> | <ipv6-address>}"},
{"CONFIGURE"},
{"tacacs_serverconf.html"},
0
},
{
{"tacacs-server host {<ipv4-address> | <ipv6-address> | <host-name>} [single-connection] [port <tcp port (1-65535)>] [timeout <time out in seconds (1-255)>] [key <secret key>]"},
{"CONFIGURE"},
{"tacacs_config.html"},
0
},
{
{"debug tacacs { all | info | errors | dumptx | dumprx }"},
{"USEREXEC"},
{"tacacs_traces.html"},
0
},
{
{"show tcp connections"},
{"USEREXEC"},
{"tcp_conns.html"},
0
},
{
{"show tcp listeners"},
{"USEREXEC"},
{"tcp_listnrs.html"},
0
},
{
{"show tcp statistics"},
{"USEREXEC"},
{"tcp_stats.html"},
0
},
{
{"tcp-ao mkt key-id <Key Id(0-255)>  receive-key-id <Rcv Key Id (0-255)> algorithm {hmac-sha-1 | aes-128-cmac} key <master-key>  [tcp-option-exclude]"},
{"BGP "},
{"bgp_tcpaomktconf.html"},
0
},
{
{"ethernet cfm test [mpid <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>] {domain <domain-name> | level <level-id(0-7)>} [service <service-name> | vlan <vlan-id/vfi-id> | service-instance <service-instance (256-16777214)>]  [interface <interface-type> <interface-number>] [direction {inward | outward}] [pattern null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc] [size <pdu-size(64-9000)> | variable-bytes] [interval <milliseconds(1-600000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>] [stop] [switch <string(32)>]"},
{"EXEC"},
{"ecfm_tst.html"},
0
},
{
{"ethernet cfm throughput type {one-way | two-way } {mpid  <peer-mepid(1-8191)> | mac <peer-mac(aa:aa:aa:aa:aa:aa)>}  {domain  <domain-name> | level <level-id(0-7)>} [{service <service-name> | vlan <vlan-id/vfi-id> | service-instance <integer(256-16777214)> }]  [interface <interface-type> <interface-number>] [direction {inward | outward}]  [test-pattern {null-signal-without-crc | null-signal-with-crc | prbs-without-crc | prbs-with-crc}] [init-size <pdu-size(64-9000)>] [rate <pps(1-100000)>] [count <num_of_msgs(1-8192)>] [deadline <seconds(1-172800)>][{burst-count <integer(1-8192)> | burst-deadline <milli-seconds(1-172800)>}][stop][switch <string(32)>]"},
{"EXEC"},
{"ecfm_th.html"},
0
},
{
{"meter <meter-id(1-65535)>"},
{"CONFIGURE"},
{"qos_tbparammeter.html"},
0
},
{
{"debug ptp { global | { vrf | switch } <string(32)> } { all | [init-shut] [mgmt] [datapath] [ctrl] [pkt-dump] [resource] [all-fail] [buffer] [critical] }"},
{"USEREXEC"},
{"ptp_traceconf.html"},
0
},
{
{"show ethernet cfm traceroute-cache [switch <context_name>]"},
{"EXEC"},
{"ecfm_ltrs.html"},
0
},
{
{"show ethernet cfm traceroute-cache [switch <context_name>]"},
{"EXEC"},
{"ecfm_ltrcache.html"},
0
},
{
{"track <group-index> links <links-to-track(1-255)>"},
{"CONFIGURE"},
{"vrrp_tracksettings.html"},
0
},
{
{"mef transmode {provider-bridge | mpls }"},
{"CONFIGURE"},
{"transmode.html"},
0
},
{
{"show ip nat { global | static | translations | policy}"},
{"EXEC"},
{"nat_translation.html"},
0
},
{
{"tunnel mpls destination {<remote ip> | <remote local-map-number>| point-to-multipoint <p2mp_id>} [source {<src ip> | <src local-map-number>}] [lsp-num <tunnel instance(1-65535)>]"},
{"MPLS_TUNNEL_MODE"},
{"mplstunnel_conf.html"},
0
},
{
{"tunnel mode {gre|sixToFour|isatap|compat|ipv6ip} [config-id <ConfId(1-2147483647)>] source <TnlSrcIP/IfName> [dest <TnlDestIP>]"},
{"TNL"},
{"L3_tnlconf.html"},
0
},
{
{"l2protocol-tunnel cos <cos-value(0-7)>"},
{"CONFIGURE"},
{"pbtunnel_macconfig.html"},
0
},
{
{"show udp connections [vrf <vrf-name>]"},
{"USEREXEC"},
{"udp_conns.html"},
0
},
{
{"show udp statistics [vrf <vrf-name>]"},
{"USEREXEC"},
{"udp_stats.html"},
0
},
{
{"ethernet map ce-vlan <ce-vlan> evc <evc> [uni-evc-id <string(32)>]"},
{"REDUNDANCY"},
{"uni.html"},
0
},
{
{"ethernet map ce-vlan <ce-vlan> evc <evc> [uni-evc-id <string(32)>]"},
{"REDUNDANCY"},
{"UniCVlanEvc.html"},
0
},
{
{"ethernet map ce-vlan <ce-vlan> evc <evc> [uni-evc-id <string(32)>]"},
{"REDUNDANCY"},
{"unilist.html"},
0
},
{
{"url filter add <URL-String>"},
{"FIREWALL"},
{"fwl_urlfilterconf.html"},
0
},
{
{"l2 vfi <vfi-name (32)> { manual            | autodiscover           }"},
{"VCM"},
{"mplsvfi_conf.html"},
0
},
{
{"virtual server <local ip> [<local port number>] { auth | dns | ftp | pop3 | pptp | smtp | telnet | http | nntp | snmp | other [<global port number>] } [<tcp|udp|any>] [<description>]"},
{"PPP"},
{"nat_virtualserver.html"},
0
},
{
{"shutdown garp"},
{"CONFIGURE"},
{"vlan_basicinfo.html"},
0
},
{
{"show vlan device capabilities [ switch <context_name>]"},
{"USEREXEC"},
{"vlan_devicecapa.html"},
0
},
{
{"ethernet cfm associate vlan-id <a,b,c-d> primary-vlan-id <vlan-id/vfi-id>"},
{"VCM"},
{"ecfm_vlan.html"},
0
},
{
{"show vlan counters [vlan <vlan-range>] [ switch <context_name>]"},
{"USEREXEC"},
{"vlan_counterstats.html"},
0
},
{
{"show vlan [brief | id <vlan-range> | summary | ascending] [ switch <context_name>]"},
{"USEREXEC"},
{"vlan_currentdb.html"},
0
},
{
{"show dot1d mac-address-table [address <aa:aa:aa:aa:aa:aa>] [{interface <interface-type> <interface-id> | switch <context_name>}]"},
{"USEREXEC"},
{"vlan_fdbentries.html"},
0
},
{
{"ip [vrf <vrf-name>] path mtu discover"},
{"CONFIGURE"},
{"vlan_interfacesettings.html"},
0
},
{
{"instance <instance-id(1-64|4094)> vlan <vlan-range>"},
{"VCMMST"},
{"msti_vlanmap.html"},
0
},
{
{"show mac-address-table [vlan <vlan-range>] [address <aa:aa:aa:aa:aa:aa>] [{interface <interface-type> <interface-id> | switch <context_name>}]"},
{"USEREXEC"},
{"vlan_dyngrptable.html"},
0
},
{
{"mac-map <aa:aa:aa:aa:aa:aa> vlan <vlan-id/vfi_id> [mcast-bcast {discard | allow}]"},
{"INTERFACE"},
{"vlan_portmacmap.html"},
0
},
{
{"port mac-vlan"},
{"INTERFACE"},
{"vlan_pvidsetting.html"},
0
},
{
{"show vlan statistics [vlan <vlan-range>] [ switch <context_name>]"},
{"USEREXEC"},
{"vlan_portstats.html"},
0
},
{
{"map protocol              {ip | novell | netbios | appletalk | other <aa:aa or aa:aa:aa:aa:aa>}              {enet-v2 | snap | llcOther | snap8021H | snapOther}              protocols-group <Group id integer(0-2147483647)>"},
{"CONFIGURE"},
{"vlan_protgrp.html"},
0
},
{
{"map subnet <ip-subnet-address> vlan <vlan-id/vfi_id>              [arp {suppress | allow}] [mask <subnet-mask>]"},
{"INTERFACE"},
{"vlan_subnetipmap.html"},
0
},
{
{"debug vlan { global | [{fwd | priority | redundancy}              [initshut] [mgmt] [data] [ctpl] [dump] [os] [failall] [buffer] [all]] [switch <context_name>] }"},
{"USEREXEC"},
{"vlan_traces.html"},
0
},
{
{"vlan map-priority <priority value(0-7)>              traffic-class <Traffic class value(0-7)>"},
{"INTERFACE"},
{"vlan_trafficclass.html"},
0
},
{
{"set unicast-mac learning { enable | disable | default}"},
{"VLAN_CONFIG"},
{"vlan_unicastmac.html"},
0
},
{
{"crypto map <policy-name>"},
{"CONFIGURE"},
{"vpn_client_term.html"},
0
},
{
{"vpn remote identity {ipv4 | email | fqdn | dn | keyId | ipv6}               <id-value> { psk | cert } <preshared-key>/<key-id>"},
{"CONFIGURE"},
{"vpn_globalconf.html"},
0
},
{
{"crypto map <policy-name>"},
{"CONFIGURE"},
{"vpn_ikepreshared.html"},
0
},
{
{"crypto map <policy-name>"},
{"CONFIGURE"},
{"vpn_ipsec.html"},
0
},
{
{"set vpn {enable | disable}"},
{"CONFIGURE"},
{"vpn_policy.html"},
0
},
{
{"show vpn global statistics"},
{"EXEC"},
{"vpn_stats.html"},
0
},
{
{"router vrrp"},
{"CONFIGURE"},
{"vrrp_basicsettings.html"},
0
},
{
{"vrrp version { v2 | v2-v3 | v3 }"},
{"VRRP"},
{"vrrp_basic_settings.html"},
0
},
{
{"vrrp <vrid(1-255)> priority <priority(1-254)>"},
{"VRRP_IF"},
{"vrrp_conf.html"},
0
},
{
{"show vrrp [interface  { vlan <VlanId/vfi-id> | <interface-type> <interface-id> | <IP-interface-type> <IP-interface-number>} <VrId(1-255)>] [{brief|detail |statistics}]"},
{"USEREXEC"},
{"vrrp_stats.html"},
0
},
{
{"vrrp <vrid(1-255)> ipv4 <ip_addr> [secondary]"},
{"VRRP_IF"},
{"vrrp_conf.html"},
0
},
{
{"wildcard {mac-adddress <mac_addr> | broadcast} interface                ([<interface-type> <0/a-b, 0/c, ...>]               [<interface-type> <0/a-b, 0/c, ...>] [port-channel <a,b,c-d>]               [pw <a,b,c-d>] [ac <a,b,c-d>])"},
{"CONFIGURE"},
{"vlan_wildcard_settings.html"},
0
},
{
{"interface {cpu0 | vlan <vlan-id/vfi-id> [switch <switch-name>]| port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | <interface-type> <interface-id> | linuxvlan <interface-name> | loopback <interface-id (0-100)> | ppp <1-10> | pw <interface-id (1-65535)> | ac <integer (1-65535)>"},
{"CONFIGURE"},
{"interfacezone_conf.html"},
0
},
{
  {'\0'},
  {'\0'},
  {'\0'},
  0
}
};

#endif
