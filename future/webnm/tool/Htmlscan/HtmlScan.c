/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: HtmlScan.c,v 1.13 2017/06/13 13:14:36 siva Exp $
 *
 * Description: This file has routine to generate Headerfile for HTML files
 *
 ***********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define FAILURE -1
#define SUCCESS  0
#define HTML_FILES "htmlfiles"
#define MEM_FREE(Ptr)         free(Ptr)
#define MEM_MALLOC(Size,Type) (Type *)malloc(Size)
#define OUTPUTFILE1   "htmldata.h"
#define HTML_MAX_NO_FILES  2000
#define MAX_NO_FILES  2000
#define MAX_PATH_LEN  256
#define MAX_FILE_NAME 356
#define MAX_VAR_LENGTH 256

#define ENM_SCALAR      0
#define ENM_TABLE       1

#define  STRNCPY(d,s,n)    strncpy ((char *)(d), (const char *)(s), n)
#define  STRCPY(d,s)       strcpy ((char *)(d), (const char *)(s))
#define  STRCAT(d,s)       strcat ((char *)(d),(const char *)(s))
#define  STRNCAT(d,s,n)      strncat ((char *)(d),(const char *)(s),n)
#define  MEMSET(s,c,n)     memset ((void *)(s),(int) c, n)

#define isdigit(c)  ((c>='0') && (c<= '9'))

typedef char INT1;
typedef void VOID;
typedef int INT4;
typedef unsigned int UINT4;
typedef unsigned char UINT1;
typedef short int INT2;

INT2                gi2TargetFlag = 0;

typedef struct SNMP_OID_TYPE
{
    UINT4               u4_Length;
    UINT4              *pu4_OidList;
}
tSNMP_OID_TYPE;
typedef struct SNMP_OID_LIST
{
    INT4                i4Length;
    INT1               *pi1name;
}
tSNMP_OID_LIST;
tSNMP_OID_LIST     *pList[100];
typedef struct
{
    INT1               *pi1Name;
    INT4                i4Size;
    UINT1              *pi1Ptr;
    INT4                i4MibType;
    INT4                i4NoOfSnmpOid;
    INT4                i4LastModTime;
    UINT1               pOid[1024];
    UINT1               pu1Type[50];
    UINT1               pu1Access[50];
}
t_HTML_PAGE;

t_HTML_PAGE         phtml1[MAX_NO_FILES];

INT1                ReadHTMLFileList (INT1 *pi1Path);
INT1                ParseOid (FILE *, t_HTML_PAGE *);
INT4                SM_SNMP_AGT_ConvSubOIDToLong (UINT1 **);
VOID                SM_SNMP_AGT_FreeOid (tSNMP_OID_TYPE *);
tSNMP_OID_TYPE     *SM_SNMP_AGT_GetOidFromString (INT1 *);

INT4
main (argc, argv)
     INT4                argc;
     INT1               *argv[];
{
    FILE               *fp;
    FILE               *final;
    FILE               *hp;
    FILE               *fpObject;
    UINT1               i1Total;
    UINT1               au1HtmlPath[MAX_PATH_LEN];
    UINT1               au1PolicyAccess[30];
    INT4                i4HFCount = 0;
    INT4                i4, i4Length;
    INT4                i4Len = 0;
    INT4                i4Counter = 0;
    INT1               *pi1path;
    INT1                ai1FileName[256];
    INT1                ai1File[MAX_FILE_NAME];
    INT1                ai1FileSize[20];
    INT1               *pi1FileName = NULL;
    char               *au1SubTok;
    INT1               *pi1ModName = NULL;
    INT1               *pi1TempMod = NULL;
    INT4                i4DotCount = 0;

    pi1ModName = (INT1 *) MEM_MALLOC (2, INT1);
    pi1TempMod = (INT1 *) MEM_MALLOC (2, INT1);

    MEMSET (au1PolicyAccess, 0, 30);

    if ((final = fopen (OUTPUTFILE1, "w")) == NULL)
    {
        printf ("\n Error Html header file open");
        return FAILURE;
    }
    if (argc < 2)
    {
        fprintf (stderr,
                 "\nUsage: HTMLSCAN <Directory of Htmlfiles> [For target specific give input as 1]\n");
        exit (1);
    }
    if (argc == 3)
    {
        if (atoi (argv[2]) == 1)
        {
            printf ("\nGenerating htmldata.h for specific target..\n");
            gi2TargetFlag = 1;
        }
    }
    MEMSET (au1HtmlPath, 0, MAX_PATH_LEN);
    STRNCPY (au1HtmlPath, argv[1], MAX_PATH_LEN);
    pi1path = MEM_MALLOC (MAX_PATH_LEN, INT1);
    if (pi1path == NULL)
    {
        printf ("\n Unable to allocate memory\n");
        return FAILURE;
    }
    STRCPY (pi1path, "ls -l ");
    STRCAT (pi1path, au1HtmlPath);
    STRCAT (pi1path, "/*.* >htmlfiles");
    i1Total = system (pi1path);
    if (i1Total == 127)
    {
        printf ("\n Couldn't find HTML Files in this directory");
        exit (1);
    }

    if (ReadHTMLFileList (au1HtmlPath) != SUCCESS)
    {
        return FAILURE;
    }
    if ((hp = fopen (HTML_FILES, "r")) == NULL)
    {
        printf ("\n Error html list file open");
        return FAILURE;
    }
    while (fgets (ai1FileName, 256, hp) != NULL)
    {
        sscanf (ai1FileName, "%*s%*s%*s%*s%s%*s%*s%*s%*s", ai1FileSize);

        pi1FileName = (INT1 *) strrchr (ai1FileName, '/');
        pi1FileName++;
        i4Len = strlen (pi1FileName);

        if (pi1FileName[i4Len - 1] == '\n')
        {
            pi1FileName[i4Len - 1] = '\0';
            i4Len--;
        }

        MEMSET (ai1File, 0, MAX_FILE_NAME);
        STRNCPY (ai1File, au1HtmlPath, MAX_PATH_LEN);
        STRNCAT (ai1File, pi1FileName, (MAX_FILE_NAME - MAX_PATH_LEN));
        if ((fpObject = fopen (ai1File, "r")) == NULL)
        {
            fprintf (stderr, "\n %s encountered opening %s for read ",
                     strerror (errno), phtml1[i4HFCount].pi1Name);
            return FAILURE;
        }
        else
        {
            if (gi2TargetFlag == 1)
                fprintf (final, "\nUINT1 au1TargetObject%d[] = {\n", i4HFCount);
            else
                fprintf (final, "\nUINT1 au1Object%d[] = {\n", i4HFCount);
            i4Length = 0;
            while ((i4 = fgetc (fpObject)) != EOF)
            {
                fprintf (final, "0x%02x, ", i4);
                if (!(++i4Length % 0xd))
                {
                    fprintf (final, "\n");
                }

            }
            fprintf (final, "\n};\n");
            fclose (fpObject);
            i4HFCount++;
        }
    }
    ParseOid (final, (t_HTML_PAGE *) & phtml1);

    i4Counter = 0;

    while (phtml1[i4Counter].pi1Ptr != NULL)
    {
        strcpy (au1PolicyAccess, phtml1[i4Counter].pi1Name);
        au1SubTok = (char *) strtok (au1PolicyAccess, ".");
        fprintf (final, "\n tPageAccess  %sAccess = {ENM_WRITE,0xff};",
                 au1SubTok);
        i4Counter++;
    }
    if (gi2TargetFlag != 1)
        fprintf (final, "\n tPageAccess  AllAccess = {ENM_ROOT,0xff};");

    fprintf (final, "\n");
    fprintf (final, "\n");

/*    if (gi2TargetFlag == 1)
        fprintf (final, "\ntHtmlPage phtmlTarget[] ={\n");
    else
        fprintf (final, "\ntHtmlPage phtml[] ={\n");*/
    MEMSET (pi1ModName, 0, sizeof (pi1ModName));
    MEMSET (pi1TempMod, 0, sizeof (pi1TempMod));
    i4Counter = 0;
    i4DotCount = 0;

    strncpy (pi1ModName, phtml1[i4Counter].pi1Name, 2);
    while (phtml1[i4Counter].pi1Ptr != NULL)
    {
        if (strcmp (pi1ModName, pi1TempMod))
        {
            if (gi2TargetFlag == 1)
            {
                if (strstr (pi1ModName, ".") != NULL)
                {
                    i4DotCount++;
                    fprintf (final, "\ntHtmlPage dot%dhtmlTarget[] ={\n",
                             i4DotCount);
                }
                else
                {
                    fprintf (final, "\ntHtmlPage %shtmlTarget[] ={\n",
                             pi1ModName);
                }
            }
            else
            {
                if (strstr (pi1ModName, ".") != NULL)
                {
                    i4DotCount++;
                    fprintf (final, "\ntHtmlPage dot%dhtml[] ={\n", i4DotCount);
                }
                else
                {
                    fprintf (final, "\ntHtmlPage %shtml[] ={\n", pi1ModName);
                }
            }
        }
        fprintf (final, "{");
        strcpy (au1PolicyAccess, phtml1[i4Counter].pi1Name);
        au1SubTok = (char *) strtok (au1PolicyAccess, ".");
        fprintf (final, "&%sAccess,", au1SubTok);

        fprintf (final, "\"%s\",", phtml1[i4Counter].pi1Name);
        fprintf (final, "%d,", phtml1[i4Counter].i4Size);
        fprintf (final, "%d,", phtml1[i4Counter].i4LastModTime);
        if (gi2TargetFlag == 1)
            fprintf (final, "au1TargetObject%d,", i4Counter);
        else
            fprintf (final, "au1Object%d,", i4Counter);
        if (phtml1[i4Counter].i4MibType == 0)
        {
            fprintf (final, "ENM_SCALAR,");
        }
        else if (phtml1[i4Counter].i4MibType == 1)
        {
            fprintf (final, "ENM_TABLE,");
        }
        else
        {
            fprintf (final, "0,");
        }
        fprintf (final, "\n%d,", phtml1[i4Counter].i4NoOfSnmpOid);
        fprintf (final, "%s,", phtml1[i4Counter].pOid);
        fprintf (final, "%s,", phtml1[i4Counter].pu1Type);
        fprintf (final, "%s", phtml1[i4Counter].pu1Access);
        fprintf (final, "}");
        strncpy (pi1TempMod, pi1ModName, 2);
        i4Counter++;
        if (phtml1[i4Counter].pi1Ptr != NULL)
        {
            strncpy (pi1ModName, phtml1[i4Counter].pi1Name, 2);
            if (strcmp (pi1ModName, pi1TempMod))
            {
                fprintf (final, ",\n");
                fprintf (final, "{0,0,0,0,0,0,0,0,0,0}");
                fprintf (final, "\n};\n");
            }
            else
            {
                fprintf (final, ",\n");
            }
        }
    }
    fprintf (final, ",\n");
    fprintf (final, "{0,0,0,0,0,0,0,0,0,0}");
    fprintf (final, "\n};\n");
//    fprintf (final, "\n");
    MEMSET (pi1ModName, 0, sizeof (pi1ModName));
    MEMSET (pi1TempMod, 0, sizeof (pi1TempMod));

    if (gi2TargetFlag == 1)
    {
        fprintf (final, "\ntHtmlInfo ghtmlTarget[] ={\n");
    }
    else
    {
        fprintf (final, "\ntHtmlInfo ghtml[] ={\n");
    }

    i4Counter = 0;
    i4DotCount = 0;
    strncpy (pi1ModName, phtml1[i4Counter].pi1Name, 2);
    while (phtml1[i4Counter].pi1Ptr != NULL)
    {
        if (strcmp (pi1ModName, pi1TempMod))
        {
            if (gi2TargetFlag == 1)
            {
                fprintf (final, "{");
                fprintf (final, "\"%s\", ", pi1ModName);
                if (strstr (pi1ModName, ".") != NULL)
                {
                    i4DotCount++;
                    fprintf (final, "dot%dhtmlTarget", i4DotCount);
                }
                else
                {
                    fprintf (final, "%shtmlTarget", pi1ModName);
                }
                fprintf (final, "},\n");
            }
            else
            {
                fprintf (final, "{");
                fprintf (final, "\"%s\", ", pi1ModName);
                if (strstr (pi1ModName, ".") != NULL)
                {
                    i4DotCount++;
                    fprintf (final, "dot%dhtml", i4DotCount);
                }
                else
                {
                    fprintf (final, "%shtml", pi1ModName);
                }
                fprintf (final, "},\n");
            }
        }
        MEM_FREE (phtml1[i4Counter].pi1Ptr);
        MEM_FREE (phtml1[i4Counter].pi1Name);
        i4Counter++;
        strncpy (pi1TempMod, pi1ModName, 2);
        if ((phtml1[i4Counter].pi1Ptr != NULL))
            strncpy (pi1ModName, phtml1[i4Counter].pi1Name, 2);
    }
    fprintf (final, "{0,0}");
    fprintf (final, "\n};\n");

    fclose (final);
    fclose (hp);
    MEM_FREE (pi1ModName);
    MEM_FREE (pi1TempMod);
    MEM_FREE (pi1path);
    unlink (HTML_FILES);
    printf ("\n\n htmldata.h File Generated Successfully...\n");
    return 0;
}

INT1
ReadHTMLFileList (INT1 *pi1Path)
{
    INT4                i4Count = 0;
    INT1                ai1FileName[256];
    INT1                ai1File[MAX_FILE_NAME];
    INT1               *pi1FileName;
    INT4                i4Len;
    INT1                ai1FileSize[20];
    INT4                i4FileSize1;
    INT4                i4lastmodtime;
    FILE               *fp;
    FILE               *SrcFile;
    struct stat         FileInfo;

    memset (&FileInfo, 0, sizeof (struct stat));
    /* User May enter the path without terminating '\' character
     * */

    i4Len = strlen (pi1Path);
    if (pi1Path[i4Len - 1] != '/')
    {
        pi1Path[i4Len] = '/';
        pi1Path[i4Len + 1] = 0;
    }
    i4Len = 0;

    if ((fp = fopen (HTML_FILES, "r")) == NULL)
    {
        printf ("\n Error html list file open");
        return FAILURE;
    }
    while (fgets (ai1FileName, 256, fp) != NULL)
    {
        pi1FileName = (INT1 *) strrchr (ai1FileName, '/');
        pi1FileName++;
        i4Len = strlen (pi1FileName);

        if (pi1FileName[i4Len - 1] == '\n')
        {
            pi1FileName[i4Len - 1] = '\0';
            i4Len--;
        }
        MEMSET (ai1File, 0, MAX_FILE_NAME);
        STRNCPY (ai1File, pi1Path, MAX_PATH_LEN);
        STRNCAT (ai1File, pi1FileName, (MAX_FILE_NAME - MAX_PATH_LEN));

        stat (ai1File, &FileInfo);
        i4FileSize1 = FileInfo.st_size;
        i4lastmodtime = FileInfo.st_mtime;
        if ((SrcFile = fopen (ai1File, "r")) == NULL)
        {
            printf ("Error Html files open for %s\n", ai1File);
            continue;
        }
        phtml1[i4Count].pi1Name = MEM_MALLOC (i4Len + 1, INT1);
        if (phtml1[i4Count].pi1Name == NULL)
        {
            printf ("\n Unable to Allocate memory for filename");
            return FAILURE;
        }
        strncpy (phtml1[i4Count].pi1Name, pi1FileName, i4Len);
        phtml1[i4Count].pi1Name[i4Len] = '\0';
        phtml1[i4Count].i4Size = i4FileSize1;
        phtml1[i4Count].i4LastModTime = i4lastmodtime;
        phtml1[i4Count].pi1Ptr = MEM_MALLOC (i4FileSize1 + 1, INT1);
        if (phtml1[i4Count].pi1Ptr == NULL)
        {
            printf ("Unable to allocate memory for file to copy\n");
            return FAILURE;
        }
        if (fread (phtml1[i4Count].pi1Ptr, 1, i4FileSize1, SrcFile) !=
            (UINT4) i4FileSize1)
        {
            printf ("\n Error in file read\n");
        }
        phtml1[i4Count].pi1Ptr[i4FileSize1] = '\0';
        i4Count++;
        if (i4Count == HTML_MAX_NO_FILES)
        {
            printf ("\n Number of Files exceeds,copies only first % d files ",
                    i4Count);
            return SUCCESS;
        }
        fclose (SrcFile);
    }
    phtml1[i4Count].pi1Name = NULL;
    fclose (fp);
    return SUCCESS;
}

INT1
ParseOid (FILE * fp, t_HTML_PAGE * phtml)
{
    INT4                i4ValCount = 0;
    UINT4               u4TempLen;
    INT4                i4Counter = 0;
    INT1               *pValueList[500];
    INT1                pi1Name[256] = "";
    INT1               *pi1fcstart, *pi1fcend;
    INT1               *pi1fc;
    UINT4               u4Count, u4Count1;
    UINT1               au1OidName[50], au1TypeName[50], au1AccessName[50];
    UINT4               au4Len[50], au1[20], ai1Tmp[20];
    UINT1              *pu1Len[20];
    UINT1               au1Type[2512], au1Type1[100];
    UINT1               au1Access[2512], au1Access1[200];
    UINT1               au1OIDString[64];
    UINT1               au1List[5256];
    INT1               *pi1fc1, *pi1fc2;
    INT4                i4Length, i4Check = 0;
    INT1               *pi1Temp;
    INT1                au1TmpArr[100];
    tSNMP_OID_TYPE     *pOid;
    INT1                ai1Check[100];
    INT1                ai1Var[100][100];
    INT4                i4ValCheckCount = 0;

    while (phtml[i4Counter].pi1Name != NULL)
    {
        pi1fc = phtml[i4Counter].pi1Ptr;
        pi1fcstart = (INT1 *) strstr (pi1fc, "snmpvaluesstart");
        if (pi1fcstart == NULL)
        {
            phtml1[i4Counter].i4MibType = 2;
            phtml1[i4Counter].i4NoOfSnmpOid = 0;
            strcpy (phtml1[i4Counter].pOid, "NULL");
            strcpy (phtml1[i4Counter].pu1Type, "NULL");
            strcpy (phtml1[i4Counter].pu1Access, "NULL");
            i4Counter++;
            continue;
        }
        pi1fcend = (INT1 *) strstr (pi1fc, "snmpvaluesend");
        if (pi1fcend == NULL)
            return FAILURE;
        pi1fc1 = pi1fcstart;
        while ((pi1fc1 + 1) < pi1fcend)
        {
            pi1fc1 = (INT1 *) strchr (pi1fc1, '!');
            if (pi1fc1 >= pi1fcend)
                break;
            pi1fc2 = (INT1 *) strchr (pi1fc1, '>');
            if (pi1fc2 >= pi1fcend)
                break;
            i4Length = (pi1fc2 - pi1fc1 - 2);
            if (i4Length >= MAX_VAR_LENGTH)
            {
                printf ("Max Length Reached\n");
                return FAILURE;
            }
            strncpy (pi1Name, (pi1fc1 + 2), i4Length);
            pi1Name[i4Length - 1] = '\0';
            pValueList[i4ValCount] = MEM_MALLOC (i4Length, INT1);
            if (pValueList[i4ValCount] == NULL)
            {
                printf ("Memory Unavilable\n");
                return FAILURE;
            }
            pValueList[i4ValCount + 1] = NULL;
            strncpy (pValueList[i4ValCount], pi1Name, i4Length);
            i4ValCount++;
            pi1fc1 = pi1fc2;
        }
        strcpy (au1TmpArr, phtml[i4Counter].pi1Name);
        pi1Temp = (char *) strtok (au1TmpArr, ".");
        fprintf (fp, "\n\n/* Start of %s */\n", phtml[i4Counter].pi1Name);
        for (u4Count = 0; u4Count < i4ValCount; u4Count++)
        {
            sscanf (pValueList[u4Count], "%[^:]", au1OIDString);
            pOid =
                (tSNMP_OID_TYPE *) SM_SNMP_AGT_GetOidFromString (au1OIDString);
            if (pOid == NULL)
            {
                printf
                    (" No OID in the %s Page pl Use HTMLGEN to Convert the Page\n",
                     pi1Temp);
                fprintf (fp, "UINT4 au4%s%d[]={0.0};\n", pi1Temp, u4Count);
                au4Len[u4Count] = 0;
                continue;
            }
            for (u4Count1 = 0; u4Count1 < (pOid->u4_Length - 1); u4Count1++)
            {
                sprintf (ai1Check, "%u,", pOid->pu4_OidList[u4Count1]);
                strcat ((char *) ai1Var, ai1Check);
            }
            sprintf (ai1Check, "%u", pOid->pu4_OidList[u4Count1]);
            strcat ((char *) ai1Var, ai1Check);
            au4Len[u4Count] = pOid->u4_Length;
            fprintf (fp, "UINT4 au4%s%d[]={%s};\n", pi1Temp, u4Count,
                     (char *) ai1Var);
            u4TempLen = pOid->u4_Length;
            memset (ai1Var, 0, 100);
        }
        phtml[i4Counter].i4NoOfSnmpOid = i4ValCount;
        if (pOid != NULL)
        {
            if (pOid->pu4_OidList[u4TempLen - 1] == 0)
            {
                phtml[i4Counter].i4MibType = 0;
            }
            else
            {
                phtml[i4Counter].i4MibType = 1;
            }
        }
        else
        {
            phtml[i4Counter].i4MibType = 2;

        }
        memset (au1List, 0, 5256);
        i4ValCheckCount = i4ValCount - 1;
        if (i4ValCheckCount)
        {
            for (u4Count = 0; u4Count < i4ValCount - 1; u4Count++)
            {
                if (!u4Count)
                {
                    strcpy (au1List, "{");
                    sprintf ((char *) ai1Tmp, "%d", au4Len[u4Count]);
                    strcat (au1List, (char *) ai1Tmp);
                    strcat (au1List, ",");
                    strcat (au1List, "au4");
                    strcat (au1List, pi1Temp);
                    sprintf ((char *) au1, "%d", u4Count);
                    strcat (au1List, (char *) au1);
                    strcat (au1List, "},");
                }
                else
                {
                    strcat (au1List, "{");
                    sprintf ((char *) ai1Tmp, "%d", au4Len[u4Count]);
                    strcat (au1List, (char *) ai1Tmp);
                    strcat (au1List, ",");
                    strcat (au1List, "au4");
                    strcat (au1List, pi1Temp);
                    sprintf ((char *) au1, "%d", u4Count);
                    strcat (au1List, (char *) au1);
                    strcat (au1List, "},");
                }
            }
        }
        else
        {

            u4Count = 0;
        }
        strcat (au1List, "{");
        sprintf ((char *) ai1Tmp, "%d", au4Len[u4Count]);
        strcat (au1List, (char *) ai1Tmp);
        strcat (au1List, ",");
        strcat (au1List, "au4");
        strcat (au1List, pi1Temp);
        sprintf ((char *) au1, "%d", u4Count);
        strcat (au1List, (char *) au1);
        strcat (au1List, "}");

        fprintf (fp, "tSNMP_OID_TYPE %sOidList[] = {%s};\n", pi1Temp, au1List);
        strcpy (au1OidName, pi1Temp);
        strcat (au1OidName, "OidList");
        phtml[i4Counter].pOid[0] = '\0';
        strcpy (phtml[i4Counter].pOid, au1OidName);
        /* Type */
        memset (au1Type, 0, 2512);
        for (u4Count = 0; u4Count < i4ValCount; u4Count++)
        {
            sscanf (pValueList[u4Count], "%[^:]", au1OIDString);
            u4TempLen = strlen (au1OIDString);
            pValueList[u4Count] += u4TempLen;
            pValueList[u4Count]++;
            memset (au1Type1, 0, 100);
            sscanf (pValueList[u4Count], "%[^:]", au1Type1);
            if (au1Type1[0] == '\0' || au1Type1[0] == ')' || au1Type1[0] == '!')
            {
                strcpy (au1Type1, "0");
            }
            if (!u4Count)
            {
                strcpy (au1Type, au1Type1);
                strcat (au1Type, ",");
            }
            else
            {
                strcat (au1Type, au1Type1);
                strcat (au1Type, ",");
            }
        }
        u4TempLen = strlen (au1Type);
        au1Type[u4TempLen - 1] = '\0';
        fprintf (fp, "UINT1 au1%sType[] = {%s};\n", pi1Temp, au1Type);
        strcpy (au1TypeName, "au1");
        strcat (au1TypeName, pi1Temp);
        strcat (au1TypeName, "Type");
        strcpy (phtml[i4Counter].pu1Type, au1TypeName);
        /* Access */
        memset (au1Access, 0, 2512);
        for (u4Count = 0; u4Count < i4ValCount; u4Count++)
        {
            memset (au1Type1, 0, 100);
            sscanf (pValueList[u4Count], "%[^:]", au1Type1);
            u4TempLen = strlen (au1Type1);
            pValueList[u4Count] += u4TempLen;
            pValueList[u4Count]++;
            memset (au1Access1, 0, 200);
            sscanf (pValueList[u4Count], "%[^:]", au1Access1);
            if (au1Access1[0] == '\0' || au1Access1[0] == '!'
                || au1Access1[0] == ')')
            {
                strcpy (au1Access1, "0");
            }

            if (!u4Count)
            {
                strcpy (au1Access, au1Access1);
                strcat (au1Access, ",");
            }
            else
            {
                strcat (au1Access, au1Access1);
                strcat (au1Access, ",");
            }
        }
        u4TempLen = strlen (au1Access);
        au1Access[u4TempLen - 1] = '\0';
        fprintf (fp, "UINT1 au1%sAccess[] = {%s};\n", pi1Temp, au1Access);
        strcpy (au1AccessName, "au1");
        strcat (au1AccessName, pi1Temp);
        strcat (au1AccessName, "Access");
        strcpy (phtml[i4Counter].pu1Access, au1AccessName);
        i4Counter++;
        i4ValCount = 0;
    }
}

tSNMP_OID_TYPE     *
SM_SNMP_AGT_GetOidFromString (pi1_str)
     INT1               *pi1_str;
{
    INT4                i4_dot_count;
    INT4                i4_i;
    UINT1              *pu1_temp_ptr;
    tSNMP_OID_TYPE     *p_oid;
    i4_dot_count = 0;

    for (i4_i = 0; pi1_str[i4_i] != '\0'; i4_i++)
    {
        if (pi1_str[i4_i] == '.')
            i4_dot_count++;
    }
    if ((p_oid = MEM_MALLOC (sizeof (tSNMP_OID_TYPE), tSNMP_OID_TYPE)) == NULL)
    {
        return (NULL);
    }
    p_oid->pu4_OidList = NULL;
    p_oid->u4_Length = 0;
    pu1_temp_ptr = (UINT1 *) pi1_str;
    if ((p_oid->pu4_OidList =
         MEM_MALLOC (sizeof (UINT4) * (i4_dot_count + 1), UINT4)) != NULL)
    {
        for (i4_i = 0; i4_i < i4_dot_count + 1; i4_i++)
        {
            if (isdigit ((INT4) *pu1_temp_ptr))
                p_oid->pu4_OidList[i4_i] =
                    SM_SNMP_AGT_ConvSubOIDToLong (&pu1_temp_ptr);
            else
            {
                SM_SNMP_AGT_FreeOid (p_oid);
                return (NULL);
            }
            if (*pu1_temp_ptr == '.')
                pu1_temp_ptr++;
            else if (*pu1_temp_ptr != '\0')
            {
                SM_SNMP_AGT_FreeOid (p_oid);
                return (NULL);
            }
        }
    }
    else
    {
        MEM_FREE (p_oid);
        return (NULL);
    }

    p_oid->u4_Length = (INT2) (++i4_dot_count);
    return (p_oid);
}

VOID
SM_SNMP_AGT_FreeOid (oid_ptr)
     tSNMP_OID_TYPE     *oid_ptr;
{
    if (oid_ptr != NULL)
    {
        if (oid_ptr->pu4_OidList != NULL)
        {
            MEM_FREE (oid_ptr->pu4_OidList);
            oid_ptr->pu4_OidList = NULL;
        }
        MEM_FREE (oid_ptr);
    }
}
INT4
SM_SNMP_AGT_ConvSubOIDToLong (ppu1_temp_ptr)
     UINT1             **ppu1_temp_ptr;
{
    INT2                i2_i;
    UINT4               u4_value;
    INT1                u1_temp_value;
    u4_value = 0;
    for (i2_i = 0; ((i2_i < 11) && (**ppu1_temp_ptr != '.') &&
                    (**ppu1_temp_ptr != '\0')); i2_i++)
    {
        if (!isdigit (**ppu1_temp_ptr))
        {
            return (INT4) (-1);
        }
        if (!(strncpy (&u1_temp_value, *ppu1_temp_ptr, 1)))
        {
            return (INT4) (-1);
        }

        u4_value = (u4_value * (UINT4) 10) + (0x0f & (UINT4) u1_temp_value);
        (*ppu1_temp_ptr)++;
    }
    return (u4_value);
}
