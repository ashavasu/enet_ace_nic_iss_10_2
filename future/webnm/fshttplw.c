/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttplw.c,v 1.5 2013/12/13 11:09:35 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "enm.h"
# include  "fshttplw.h"
# include  "httpcli.h"

/* Low Level GET Routine for All Objects  */

extern tHttpInfo    gHttpInfo;
/****************************************************************************
Function    :  nmhGetFsHttpRedirectionStatus
Input       :  The Indices

               The Object 
               retValFsHttpRedirectionStatus
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsHttpRedirectionStatus (INT4 *pi4RetValFsHttpRedirectionStatus)
{
    HTTP_MGMT_LOCK ();
    *pi4RetValFsHttpRedirectionStatus = gHttpInfo.bRedirectStat;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsOperHttpAuthScheme
 Input       :  The Indices

                The Object 
                retValFsOperHttpAuthScheme
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOperHttpAuthScheme (INT4 *pi4RetValFsOperHttpAuthScheme)
{
    HTTP_MGMT_LOCK ();
    *pi4RetValFsOperHttpAuthScheme = gHttpInfo.i4OperHttpAuthScheme;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsConfigHttpAuthScheme
 Input       :  The Indices

                The Object
                retValFsConfigHttpAuthScheme
 Output      :  The Get Low Lev Routine Take the Indices &                
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsConfigHttpAuthScheme (INT4 *pi4RetValFsConfigHttpAuthScheme)
{
    HTTP_MGMT_LOCK ();
    *pi4RetValFsConfigHttpAuthScheme = gHttpInfo.i4ConfigHttpAuthScheme;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsHttpRequestCount
Input       :  The Indices
                The Object
                retValFsHttpRequestCount
Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsHttpRequestCount (INT4 *pi4RetValFsHttpRequestCount)
{
    *pi4RetValFsHttpRequestCount = gHttpInfo.u4HttpRequestCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsHttpRequestDiscards
Input       :  The Indices
 
                The Object
                retValFsHttpRequestDiscards
Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsHttpRequestDiscards (INT4 *pi4RetValFsHttpRequestDiscards)
{
    *pi4RetValFsHttpRequestDiscards = gHttpInfo.u4HttpRequestDiscards;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsHttpRedirectionStatus
Input       :  The Indices

               The Object 
               setValFsHttpRedirectionStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsHttpRedirectionStatus (INT4 i4SetValFsHttpRedirectionStatus)
{
    HTTP_MGMT_LOCK ();
    gHttpInfo.bRedirectStat = (BOOL1) i4SetValFsHttpRedirectionStatus;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsConfigHttpAuthScheme
 Input       :  The Indices

                The Object 
                setValFsConfigHttpAuthScheme
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsConfigHttpAuthScheme (INT4 i4SetValFsConfigHttpAuthScheme)
{
    HTTP_MGMT_LOCK ();
    gHttpInfo.i4ConfigHttpAuthScheme = i4SetValFsConfigHttpAuthScheme;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsHttpRequestCount
Input       :  The Indices
 
                The Object
                setValFsHttpRequestCount
Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsHttpRequestCount (INT4 i4SetValFsHttpRequestCount)
{
    gHttpInfo.u4HttpRequestCount = i4SetValFsHttpRequestCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsHttpRequestDiscards
Input       :  The Indices
 
                The Object
                setValFsHttpRequestDiscards
Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsHttpRequestDiscards (INT4 i4SetValFsHttpRequestDiscards)
{
    gHttpInfo.u4HttpRequestDiscards = i4SetValFsHttpRequestDiscards;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsHttpRedirectionStatus
Input       :  The Indices

               The Object 
               testValFsHttpRedirectionStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsHttpRedirectionStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsHttpRedirectionStatus)
{
    if ((i4TestValFsHttpRedirectionStatus == HTTP_REDIRECT_ENABLE) ||
        (i4TestValFsHttpRedirectionStatus == HTTP_REDIRECT_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsConfigHttpAuthScheme
 Input       :  The Indices

                The Object 
                testValFsConfigHttpAuthScheme
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsConfigHttpAuthScheme (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsConfigHttpAuthScheme)
{
    if ((i4TestValFsConfigHttpAuthScheme == HTTP_AUTH_DEFAULT) ||
        (i4TestValFsConfigHttpAuthScheme == HTTP_AUTH_BASIC) ||
        (i4TestValFsConfigHttpAuthScheme == HTTP_AUTH_DIGEST))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsHttpRequestCount
Input       :  The Indices
 
                The Object
                testValFsHttpRequestCount
Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsHttpRequestCount (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsHttpRequestCount)
{
    UNUSED_PARAM (pu4ErrorCode);
/*  Purpose of  making FsHttpRequestCount as read-write is to reset the counter value to zero
*  No other value  than zero is not acceptable */

    if (i4TestValFsHttpRequestCount != HTTP_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsHttpRequestDiscards
Input       :  The Indices
 
                The Object
                testValFsHttpRequestDiscards
Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsHttpRequestDiscards (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsHttpRequestDiscards)
{
    UNUSED_PARAM (pu4ErrorCode);
/*  Purpose of  making FsHttpRequestDiscards as read-write is to reset the counter value to zero
*  No other value  than zero is not acceptable */

    if (i4TestValFsHttpRequestDiscards != HTTP_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsHttpRedirectionStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsHttpRedirectionStatus (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsConfigHttpAuthScheme
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsConfigHttpAuthScheme (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsHttpRequestCount
Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsHttpRequestCount (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsHttpRequestDiscards
Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsHttpRequestDiscards (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsHttpRedirectionTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsHttpRedirectionTable
Input       :  The Indices
               FsHttpRedirectionURL
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsHttpRedirectionTable (tSNMP_OCTET_STRING_TYPE *
                                                pFsHttpRedirectionURL)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);
    HTTP_MGMT_UNLOCK ();

    if (pRedirectLink == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsHttpRedirectionTable
Input       :  The Indices
               FsHttpRedirectionURL
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsHttpRedirectionTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsHttpRedirectionURL)
{
    tHttpRedirect      *pRedirectGetNextLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectGetNextLink = (tHttpRedirect *)
        TMO_DLL_First (&gHttpInfo.HttpRedirectList);
    if (pRedirectGetNextLink != NULL)
    {
        MEMSET (pFsHttpRedirectionURL->pu1_OctetList, HTTP_ZERO,
                pFsHttpRedirectionURL->i4_Length);
        MEMCPY (pFsHttpRedirectionURL->pu1_OctetList,
                pRedirectGetNextLink->ai1RedirectedUrl,
                pRedirectGetNextLink->i4UrlLen);

        pFsHttpRedirectionURL->i4_Length = pRedirectGetNextLink->i4UrlLen;
        HTTP_MGMT_UNLOCK ();
        return SNMP_SUCCESS;
    }

    HTTP_MGMT_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsHttpRedirectionTable
Input       :  The Indices
               FsHttpRedirectionURL
               nextFsHttpRedirectionURL
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsHttpRedirectionTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsHttpRedirectionURL,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsHttpRedirectionURL)
{
    tHttpRedirect      *pRedirectLink = NULL;
    tHttpRedirect      *pRedirectNextLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    pRedirectNextLink = (tHttpRedirect *)
        TMO_DLL_Next (&(gHttpInfo.HttpRedirectList),
                      &(pRedirectLink->NextRedirectNode));

    if (pRedirectNextLink != NULL)
    {
        MEMCPY (pNextFsHttpRedirectionURL->pu1_OctetList,
                pRedirectNextLink->ai1RedirectedUrl,
                pRedirectNextLink->i4UrlLen);
        pNextFsHttpRedirectionURL->i4_Length = pRedirectNextLink->i4UrlLen;
        HTTP_MGMT_UNLOCK ();
        return SNMP_SUCCESS;
    }

    HTTP_MGMT_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsHttpRedirectedSrvAddrType
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               retValFsHttpRedirectedSrvAddrType
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsHttpRedirectedSrvAddrType (tSNMP_OCTET_STRING_TYPE *
                                   pFsHttpRedirectionURL,
                                   INT4 *pi4RetValFsHttpRedirectedSrvAddrType)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsHttpRedirectedSrvAddrType =
        pRedirectLink->RedirectedSrvAddr.u1Afi;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsHttpRedirectedSrvIP
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               retValFsHttpRedirectedSrvIP
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsHttpRedirectedSrvIP (tSNMP_OCTET_STRING_TYPE * pFsHttpRedirectionURL,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsHttpRedirectedSrvIP)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pRedirectLink->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsHttpRedirectedSrvIP->pu1_OctetList,
                pRedirectLink->RedirectedSrvAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
    }
    else if (pRedirectLink->RedirectedSrvAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pRetValFsHttpRedirectedSrvIP->pu1_OctetList,
                pRedirectLink->RedirectedSrvAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
    }

    pRetValFsHttpRedirectedSrvIP->i4_Length =
        pRedirectLink->RedirectedSrvAddr.u1AddrLen;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsHttpRedirectedSrvDomainName
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               retValFsHttpRedirectedSrvDomainName
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsHttpRedirectedSrvDomainName (tSNMP_OCTET_STRING_TYPE *
                                     pFsHttpRedirectionURL,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsHttpRedirectedSrvDomainName)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsHttpRedirectedSrvDomainName->pu1_OctetList,
            pRedirectLink->ai1RedirectSrvDomainName,
            pRedirectLink->i4DomainLen);
    pRetValFsHttpRedirectedSrvDomainName->i4_Length =
        pRedirectLink->i4DomainLen;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsHttpRedirectionEntryStatus
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               retValFsHttpRedirectionEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsHttpRedirectionEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                    pFsHttpRedirectionURL,
                                    INT4 *pi4RetValFsHttpRedirectionEntryStatus)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsHttpRedirectionEntryStatus = pRedirectLink->i4RowStatus;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsHttpRedirectedSrvAddrType
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               setValFsHttpRedirectedSrvAddrType
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsHttpRedirectedSrvAddrType (tSNMP_OCTET_STRING_TYPE *
                                   pFsHttpRedirectionURL,
                                   INT4 i4SetValFsHttpRedirectedSrvAddrType)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* Remove the Domain name for redirection as we are configuring the IP */
    MEMSET (pRedirectLink->ai1RedirectSrvDomainName, HTTP_ZERO,
            HTTP_MAX_URL_LEN);
    pRedirectLink->i4DomainLen = HTTP_ZERO;

    pRedirectLink->RedirectedSrvAddr.u1Afi = (UINT1)
        i4SetValFsHttpRedirectedSrvAddrType;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsHttpRedirectedSrvIP
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               setValFsHttpRedirectedSrvIP
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsHttpRedirectedSrvIP (tSNMP_OCTET_STRING_TYPE * pFsHttpRedirectionURL,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsHttpRedirectedSrvIP)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pSetValFsHttpRedirectedSrvIP->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        pRedirectLink->RedirectedSrvAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        pRedirectLink->RedirectedSrvAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
    }

    MEMCPY (pRedirectLink->RedirectedSrvAddr.au1Addr,
            pSetValFsHttpRedirectedSrvIP->pu1_OctetList,
            pSetValFsHttpRedirectedSrvIP->i4_Length);

    pRedirectLink->RedirectedSrvAddr.u1AddrLen = (UINT1)
        pSetValFsHttpRedirectedSrvIP->i4_Length;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsHttpRedirectedSrvDomainName
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               setValFsHttpRedirectedSrvDomainName
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsHttpRedirectedSrvDomainName (tSNMP_OCTET_STRING_TYPE *
                                     pFsHttpRedirectionURL,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsHttpRedirectedSrvDomainName)
{
    tHttpRedirect      *pRedirectLink = NULL;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    if (pRedirectLink == NULL)
    {
        HTTP_MGMT_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Remove the IP address for redirection as we are configuring the Domain */
    if (pSetValFsHttpRedirectedSrvDomainName->i4_Length != 0)
    {
        MEMSET (pRedirectLink->RedirectedSrvAddr.au1Addr, HTTP_ZERO,
                IPVX_MAX_INET_ADDR_LEN);
        pRedirectLink->RedirectedSrvAddr.u1AddrLen = HTTP_ZERO;
        pRedirectLink->RedirectedSrvAddr.u1Afi = HTTP_ZERO;
    }

    MEMCPY (pRedirectLink->ai1RedirectSrvDomainName,
            pSetValFsHttpRedirectedSrvDomainName->pu1_OctetList,
            pSetValFsHttpRedirectedSrvDomainName->i4_Length);

    pRedirectLink->i4DomainLen =
        pSetValFsHttpRedirectedSrvDomainName->i4_Length;
    HTTP_MGMT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsHttpRedirectionEntryStatus
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               setValFsHttpRedirectionEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsHttpRedirectionEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                    pFsHttpRedirectionURL,
                                    INT4 i4SetValFsHttpRedirectionEntryStatus)
{
    tHttpRedirect      *pRedirectLink = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    HTTP_MGMT_LOCK ();
    pRedirectLink = HttpGetRedirectionEntry (pFsHttpRedirectionURL);

    switch (i4SetValFsHttpRedirectionEntryStatus)
    {
        case HTTP_ACTIVE:
            if (pRedirectLink == NULL)
            {
                HTTP_MGMT_UNLOCK ();
                return SNMP_FAILURE;
            }
            pRedirectLink->i4RowStatus = HTTP_ACTIVE;
            i1RetVal = SNMP_SUCCESS;
            break;

        case HTTP_NOT_IN_SERVICE:
            if (pRedirectLink == NULL)
            {
                HTTP_MGMT_UNLOCK ();
                return SNMP_FAILURE;
            }
            pRedirectLink->i4RowStatus = HTTP_NOT_IN_SERVICE;
            i1RetVal = SNMP_SUCCESS;
            break;

        case HTTP_CREATE_AND_WAIT:
            if (pRedirectLink != NULL)
            {
                HTTP_MGMT_UNLOCK ();
                return SNMP_FAILURE;
            }
            i1RetVal = (INT1) HttpAddRedirectionEntry (pFsHttpRedirectionURL);
            break;

        default:
            if (pRedirectLink == NULL)
            {
                HTTP_MGMT_UNLOCK ();
                return SNMP_FAILURE;
            }
            TMO_DLL_Delete (&(gHttpInfo.HttpRedirectList),
                            &(pRedirectLink->NextRedirectNode));
            MemReleaseMemBlock (gHttpInfo.HttpRedirectMemPoolId,
                                (UINT1 *) (pRedirectLink));

            i1RetVal = SNMP_SUCCESS;
            break;
    }

    HTTP_MGMT_UNLOCK ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsHttpRedirectedSrvAddrType
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               testValFsHttpRedirectedSrvAddrType
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsHttpRedirectedSrvAddrType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsHttpRedirectionURL,
                                      INT4 i4TestValFsHttpRedirectedSrvAddrType)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable (pFsHttpRedirectionURL)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsHttpRedirectedSrvAddrType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsHttpRedirectedSrvAddrType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsHttpRedirectedSrvIP
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               testValFsHttpRedirectedSrvIP
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsHttpRedirectedSrvIP (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsHttpRedirectionURL,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsHttpRedirectedSrvIP)
{
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];

    if (nmhValidateIndexInstanceFsHttpRedirectionTable (pFsHttpRedirectionURL)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    MEMSET (au1TmpArray, HTTP_ZERO, IPVX_IPV6_ADDR_LEN);
    if (MEMCMP (pTestValFsHttpRedirectedSrvIP->pu1_OctetList, au1TmpArray,
                pTestValFsHttpRedirectedSrvIP->i4_Length) == HTTP_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsHttpRedirectedSrvIP->i4_Length > IPVX_IPV6_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsHttpRedirectedSrvDomainName
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               testValFsHttpRedirectedSrvDomainName
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsHttpRedirectedSrvDomainName (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsHttpRedirectionURL,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsHttpRedirectedSrvDomainName)
{
    if (nmhValidateIndexInstanceFsHttpRedirectionTable (pFsHttpRedirectionURL)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((pTestValFsHttpRedirectedSrvDomainName->i4_Length == HTTP_ZERO) ||
        (pTestValFsHttpRedirectedSrvDomainName->i4_Length > HTTP_MAX_URL_LEN))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsHttpRedirectionEntryStatus
Input       :  The Indices
               FsHttpRedirectionURL

               The Object 
               testValFsHttpRedirectionEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsHttpRedirectionEntryStatus (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsHttpRedirectionURL,
                                       INT4
                                       i4TestValFsHttpRedirectionEntryStatus)
{
    INT1                i1Status = SNMP_SUCCESS;
    if (pFsHttpRedirectionURL->i4_Length >= HTTP_MAX_URL_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsHttpRedirectionEntryStatus)
    {
        case HTTP_CREATE_AND_WAIT:
        case HTTP_NOT_IN_SERVICE:
        case HTTP_ACTIVE:
            break;
        case HTTP_DESTROY:
            if (HttpGetRedirectionEntry (pFsHttpRedirectionURL) == NULL)
            {
                *pu4ErrorCode = CLI_HTTP_ENTRY_NOT_FOUND;
                i1Status = SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1Status = SNMP_FAILURE;
            break;
    }
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsHttpRedirectionTable
Input       :  The Indices
               FsHttpRedirectionURL
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsHttpRedirectionTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
