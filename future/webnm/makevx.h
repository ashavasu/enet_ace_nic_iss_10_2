##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                                         #
# ------------------------------------------                             #
# $Id: makevx.h,v 1.2 2007/02/01 15:09:14 iss Exp $                                                             #
# DESCRIPTION            : The VxWorks makefile for builing Future       #
#                           Embedded Network Manager                     #
##########################################################################

# Top level make include files
# ----------------------------

include ../LR/makevx.h
include ../LR/make.rule

# Compilation switches
# --------------------

ENM_FINAL_COMPILATION_SWITCHES = ${GENERAL_COMPILATION_SWITCHES}\
                                 ${SYSTEM_COMPILATION_SWITCHES}

# Directories
# -----------
                            
ENM_DIR       = ${BASE_DIR}\webnm
SNMP_DIR      = ${BASE_DIR}\snmp_2
WEBNM_UTL_DIR = ${BASE_DIR}\util\webnm

# Include directories
# -------------------

ENM_INCLUDE_FILES = ${ENM_BASE_DIR}/enm.h\
                    ${ENM_BASE_DIR}/http.h\
                    ${WEBNM_INC_DIR}/webnm.h
                       
ENM_FINAL_INCLUDE_FILES = ${ENM_INCLUDE_FILES}


# Include directories
# -------------------

ENM_INCLUDE_DIRS = -I${ENM_DIR} -I${SNMP_DIR} -I${WEBNM_UTL_DIR}

ENM_FINAL_INCLUDES = ${ENM_INCLUDE_DIRS}\
                     ${COMMON_INCLUDE_DIRS}

# Project dependencies
# --------------------

ENM_DEPENDENCIES = $(COMMON_DEPENDENCIES)\
                   $(ENM_FINAL_INCLUDE_FILES)\
                   $(ENM_BASE_DIR)/makefile.vx\
                   $(ENM_BASE_DIR)/makevx.h

# -----------------------------  END OF FILE  -------------------------------
