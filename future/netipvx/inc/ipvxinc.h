 /*********************************************************************
 * copyright (C) 2007 Aricent Inc. All Rights Reserved.
 *
 * $Id: ipvxinc.h,v 1.8 2013/09/02 14:05:12 siva Exp $
 *
 * Description : This file contains header files included in
 *               IPVX module.
 *********************************************************************/

#ifndef _IPVX_INC_H_
#define _IPVX_INC_H_

#include "lr.h"
#include "cfa.h"
#include "arp.h"
#include "vcm.h"
#include "iss.h"
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) 
#include "ip.h"
#endif 
#ifdef IP6_WANTED 
#include "ipv6.h"
#endif /* IP6_WANTED */
#include "ipvx.h"
#include "stdtcpwr.h"
#include "stdudpwr.h"
#include "stdipvlw.h"
#include "stdipvwr.h"
#include "fsmsiplw.h"
#include "fsmsipwr.h"
#include "fsmpipvxlw.h"
#include "fsmpipvxwr.h"
#include "ipvxext.h"
#include "fsipvxwr.h"
#include "fsipvxlw.h"
#include "fsmsudwr.h"
#endif /* _IPVXINC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  ipvxinc.h                       */
/*-----------------------------------------------------------------------*/

