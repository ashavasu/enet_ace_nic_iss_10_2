/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: trcrtsz.h,v 1.1 2010/10/29 12:58:57 prabuc Exp $
 *
 * Description: This File contains the prototypes & data structures related to
 *              sizing feature of Trace Route Module
 *
 ******************************************************************************/
enum {
    MAX_TRACE_INFO_ENTRIES_SIZING_ID,
    MAX_TRACE_MAX_INSTANCES_SIZING_ID,
    TRCRT_MAX_SIZING_ID
};


#ifdef  _TRCRTSZ_C
tMemPoolId TRCRTMemPoolIds[ TRCRT_MAX_SIZING_ID];
INT4  TrcrtSizingMemCreateMemPools(VOID);
VOID  TrcrtSizingMemDeleteMemPools(VOID);
INT4  TrcrtSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TRCRTSZ_C  */
extern tMemPoolId TRCRTMemPoolIds[ ];
extern INT4  TrcrtSizingMemCreateMemPools(VOID);
extern VOID  TrcrtSizingMemDeleteMemPools(VOID);
extern INT4  TrcrtSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _TRCRTSZ_C  */


#ifdef  _TRCRTSZ_C
tFsModSizingParams FsTRCRTSizingParams [] = {
{ "tTraceInfo", "MAX_TRACE_INFO_ENTRIES", sizeof(tTraceInfo),MAX_TRACE_INFO_ENTRIES, MAX_TRACE_INFO_ENTRIES,0 },
{ "tTraceEntry", "MAX_TRACE_MAX_INSTANCES", sizeof(tTraceEntry),MAX_TRACE_MAX_INSTANCES, MAX_TRACE_MAX_INSTANCES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TRCRTSZ_C  */
extern tFsModSizingParams FsTRCRTSizingParams [];
#endif /*  _TRCRTSZ_C  */


