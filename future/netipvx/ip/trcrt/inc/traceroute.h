/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: traceroute.h,v 1.8 2014/11/15 12:32:34 siva Exp $
 *
 * Description: This file contains the Data Structures, Macros and Constants of
 *              the Trace Route Implementation
 *
 ******************************************************************************/
#ifndef _TRACEROUTE_H_
#define _TRACEROUTE_H_

#include "ipvxinc.h"
#include "utilipvx.h"
#include "fssocket.h"
#include "ipvxdefs.h"
/* Constant Declaration */

#define TRACE_MAX_PROBES                             3
#define TRACE_MAX_INSTANCES                         11
#define TRACE_NODE_UNREACH                          -1

#define TRACE_INITIAL_PORT                       33434
#define TRACE_DEF_TIME_TO_WAIT                       5
#define TRACE_MIN_RECV_LEN                         100

#define TRACE_DEF_MIN_TTL                            1
#define TRACE_DEF_MAX_TTL                           15
#define TRACE_MIN_MTU                                1
#define TRACE_MAX_MTU                              100
#define TRACE_DEF_MTU                    TRACE_MIN_MTU
#define TRACE_MIN_TTL                                1
#define TRACE_MAX_TTL                               99

#define TRACE_INVALID_INSTANCE                      -1
#define TRACE_TIMEDOUT                              -2

#define TRACE_MIN_TIMEOUT                            1
#define TRACE_MAX_TIMEOUT                          100

#define TRACE_ADMIN_ENABLE                           1
#define TRACE_ADMIN_DISABLE                          2

#define TRACE_OPER_IN_PROGRESS                       1
#define TRACE_OPER_NOT_IN_PROGRESS                   2

#define TRACE_CLI_ERR_STR_LEN                        3

#define TRACE_INADDR_ANY                           "0::0"


/* Data Structure Declaration */

typedef struct
{
    tRBNodeEmbd rbConfigNode;  /* RBTree Based on 3 tuples (Instance ID, 
                                  Destination IP and its type (V4/V6) */
    tRBNodeEmbd rbTraceHopNode;/* RBTree Based on Destination IP and its
                                  type (V4/V6) */
    tTMO_SLL    IntrmHopList;  /* Intermediate hop information on the way to the 
                                  a particular destination */
    tIPvXAddr   DestAddr;      /* IP addr to be traceroute */
    UINT4       u4CxtId;       /* Context Identifier */
    INT4        i4Instance;    /* Instance of a trace route entry */
    UINT2       u2Seq;         /* Timeout to be used for tracing */
    INT2        i2Timeout;     /* Timeout to be used for tracing */
    INT2        i2Max_ttl;     /* Number of hops to look for */
    INT2        i2Min_ttl;     /* Number of hops to start with */
    INT4        i4CurHops;     /* Current attempt number for a ttl */
    INT2        i2Mtu;         /* Size of the data to be sent with trace */
    INT1        i1Admin;       /* Start or Stop the Trace operation */
    INT1        i1Oper;        /* Indicates if the trace is in progress */
    INT1        i1ErrCode;     /* ICMP Error Code */
    INT1        i1Reserved[3]; /* For Alignment purpose */
} tTraceEntry;

typedef struct
{
   tTMO_SLL_NODE  Link;
   tIPvXAddr      GatewayAddr;
   tOsixSysTime   ReachTime[TRACE_MAX_PROBES];
} tTraceInfo;

typedef struct _UDP_HEADER
{
    UINT2  u2SrcPort;  /* Port number from which application 
                        * data is being sent */
    UINT2  u2DstPort;  /* Port number at destination on which 
                        * application data is to be sent */
    UINT2  u2Len;      /* Length of UDP header+ data */
    UINT2  u2Chksum;   /* Checksum of datagram prepended with 
                        * pseudo header */
}
tUdp6Hdr;
   
/* Macro Declaration */

#define TRACE_INSTANCE(pTrace)      (pTrace)->i4Instance
#define TRACE_CONTEXT(pTrace)       pTrace->u4CxtId
#define TRACE_DEST_IP(pTrace)       pTrace->DestAddr
#define TRACE_ADMIN_STATUS(pTrace)  pTrace->i1Admin
#define TRACE_TIMEOUT(pTrace)       pTrace->i2Timeout
#define TRACE_TTL_MAX(pTrace)       pTrace->i2Max_ttl
#define TRACE_TTL_MIN(pTrace)       pTrace->i2Min_ttl
#define TRACE_OPER_STATUS(pTrace)   pTrace->i1Oper
#define TRACE_MTU(pTrace)           pTrace->i2Mtu
#define TRACE_CUR_PROBE(pTrace)     pTrace->i2Cur_probe
#define TRACE_IPVX_ADDR(pTrace)     pTrace->DestAddr.au1Addr
#define TRACE_ADDR_TYPE(pTrace)     pTrace->DestAddr.u1Afi
#define TRACE_ADDR_LEN(pTrace)      pTrace->DestAddr.u1AddrLen
#define TRACE_ERR_CODE(pTrace)      pTrace->i1ErrCode

#define TRACE_LOG                   UtlTrcPrint

#ifdef BSDCOMP_SLI_WANTED
#define SLI_EWOULDBLOCK               EWOULDBLOCK
#define SLI_EMSGSIZE                  EMSGSIZE
#endif

#define OCTETSTR_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
    MEMCPY((UINT1 *)&u4Index,pOctetString, IPV4_ADD_LEN);\
    u4Index = OSIX_NTOHL(u4Index);}

#define TRACE_ENTRY_INIT(pTraceEntry)    \
    pTraceEntry->i2Max_ttl = TRACE_DEF_MAX_TTL;    \
    pTraceEntry->i2Min_ttl = TRACE_DEF_MIN_TTL;    \
    pTraceEntry->u2Seq = TRACE_INITIAL_PORT;  \
    pTraceEntry->i2Timeout = TRACE_DEF_TIME_TO_WAIT;   \
    pTraceEntry->i2Mtu = TRACE_DEF_MTU;   \
    pTraceEntry->i4CurHops = TRACE_DEF_MIN_TTL;   \
    pTraceEntry->i1Oper = TRACE_OPER_IN_PROGRESS ;\
    pTraceEntry->i1Admin = TRACE_ADMIN_ENABLE ;\
    TMO_SLL_Init (&pTraceEntry->IntrmHopList);

/* Prototype Declaration */

INT4 TraceRouteMemInit PROTO ((VOID));
VOID TraceRouteMemDeInit PROTO ((VOID));
INT4 TraceRouteInit PROTO ((tTraceEntry *pTraceEntry));
VOID TraceRouteDeleteInstance PROTO ((tTraceEntry * pTraceEntry));
INT4 TraceRouteProcessPkt PROTO ((INT4 i4SockSend, INT4 i4SockRecv,
                               tTraceEntry *pTraceEntry));
tTraceEntry *
TraceRouteCreateInstance PROTO ((INT4 i4Instance , UINT1 *pDestAddr,
                            INT4 i4AddrType));
INT4 TraceRoute6Init PROTO ((tTraceEntry *pTraceEntry));
INT4 TraceRoute4Init PROTO ((tTraceEntry *pTraceEntry));
INT4 TraceRouteSendPkt PROTO ((INT4 i4SockId, tTraceEntry * pTraceEntry));
INT4 TraceRouteRecvPkt PROTO ((INT4 i4SockId, UINT1 * pIpAddr,
                       tOsixSysTime * pTime, tTraceEntry * pTraceEntry));
tTraceEntry * 
TraceRouteGetConfigEntry PROTO ((INT4 i4ConfigIndex, UINT1 *pDestAddr,
                            INT4 i4AddrType));
tTraceEntry * 
TraceRouteGetNextConfigEntry PROTO ((INT4 i4ConfigIndex, UINT1 *pDestAddr,
                                INT4 i4AddrType));
tTraceEntry *
TraceRouteGetHopTableEntry PROTO ((UINT1 *pDestAddr, INT4 i4AddrType));
INT4 
TraceRouteGetNextHopEntry PROTO ((UINT1 *pDestAddr, INT4 i4AddrType,
                             INT4 i4HopCount, UINT1 *pNextDest,
                             INT4 *pNextDstLen, INT4 *pHopCount));

#endif /* _TRACEROUTE_H_ */
