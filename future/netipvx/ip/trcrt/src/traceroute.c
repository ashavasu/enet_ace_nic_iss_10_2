/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: traceroute.c,v 1.11 2014/11/15 12:36:13 siva Exp $
 *
 * Description: This File contains the routines required for Supporting
 *              Trace Route Application in both IPV4 and IPV6
 *
 ******************************************************************************/

#include "traceroute.h"
#include "trcrtsz.h"

/************************** PRIVATE DECLARATIONS *****************************/

PRIVATE tMemPoolId  gTraceInstPoolId;
PRIVATE tMemPoolId  gTraceInfoPoolId;
PRIVATE tRBTree     grbTraceConfigTable;
PRIVATE tRBTree     grbTraceHopTable;

VOID TraceRouteAssignMemPoolIds PROTO ((VOID));
INT4                TraceRouteConfigTableCompFunc
PROTO ((tRBElem * prbElem1, tRBElem * prbElem2));
INT4                TraceRouteHopTableCompFunc
PROTO ((tRBElem * prbElem1, tRBElem * prbElem2));

/********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteMemInit
 *                                                                          
 *    DESCRIPTION      : Function to Create the Memory Pool and RB Tree for 
 *                       maintaing the Trace Configuration Table
 *
 *    INPUT            : None
 *                      
 *    OUTPUT           : None
 *                            
 *    RETURNS          : OSIX_SUCCESS on Successful Memory and Table creation
 *                       else OSIX_FAILURE on failure
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteMemInit (VOID)
{
    /* Create memory pool for Trace Configuration Table */

    if (TrcrtSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Assign the Mem Pool IDs */
    TraceRouteAssignMemPoolIds ();

    /* Create RB Tree for maintaining the Trace Configuration Table */

    grbTraceConfigTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tTraceEntry, rbConfigNode),
                              TraceRouteConfigTableCompFunc);

    if (NULL == grbTraceConfigTable)
    {
        TraceRouteMemDeInit ();
        return OSIX_FAILURE;
    }

    grbTraceHopTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tTraceEntry, rbTraceHopNode),
                              TraceRouteHopTableCompFunc);

    if (NULL == grbTraceHopTable)
    {
        RBTreeDelete (grbTraceConfigTable);
        TraceRouteMemDeInit ();
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteMemDeInit
 *                                                                          
 *    DESCRIPTION      : Function to Create the Memory Pool and RB Tree for 
 *                       maintaing the Trace Configuration Table
 *
 *    INPUT            : None
 *                      
 *    OUTPUT           : None
 *                            
 *    RETURNS          : OSIX_SUCCESS on Successful Memory and Table creation
 *                       else OSIX_FAILURE on failure
 *                                                                          
 ****************************************************************************/
VOID
TraceRouteMemDeInit (VOID)
{
    TrcrtSizingMemDeleteMemPools ();
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteInit
 *                                                                          
 *    DESCRIPTION      : Function to Start the Trace Route Operation depending 
 *                       on the Destination Address Type (V4/V6)
 *
 *    INPUT            : pTraceEntry - Pointer to the Trace Entry
 *                                                                                               
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteInit (tTraceEntry * pTraceEntry)
{
    if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV4)
    {
        return (TraceRoute4Init (pTraceEntry));
    }
    else
    {
        return (TraceRoute6Init (pTraceEntry));
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteDeleteInstance
 *                                                                          
 *    DESCRIPTION      : Function to delete the Trace Configuration Entry and
 *                       its associated Trace Information
 *
 *    INPUT            : pTraceEntry - Pointer to the Trace Entry
 *                                                                                               
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
TraceRouteDeleteInstance (tTraceEntry * pTraceEntry)
{
    tTraceInfo         *pTraceInfo = NULL;

    /* Delete the Hop Information List and initialize the Trace Entry */
    while ((pTraceInfo = TMO_SLL_First (&pTraceEntry->IntrmHopList)) != NULL)
    {
        TMO_SLL_Delete (&pTraceEntry->IntrmHopList, &(pTraceInfo->Link));
        MemReleaseMemBlock (gTraceInfoPoolId, (UINT1 *) pTraceInfo);
    }

    RBTreeRem (grbTraceHopTable, pTraceEntry);
    RBTreeRem (grbTraceConfigTable, pTraceEntry);
    MemReleaseMemBlock (gTraceInstPoolId, (UINT1 *) pTraceEntry);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteCreateInstance
 *                                                                          
 *    DESCRIPTION      : Function to Creates an new entry in the Trace 
 *                       Configuraion Table.
 *                      
 *    INPUT            : i4Instance  - Instance Number
 *                       pDestAddr   - Destination IP(VX) Address
 *                       i4AddrType  - Dest. Address Type
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : Pointer to Trace Entry just created or NULL on failure
 *                                                                          
 ****************************************************************************/
tTraceEntry        *
TraceRouteCreateInstance (INT4 i4Instance, UINT1 *pDestAddr, INT4 i4AddrType)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceEntry        *pPrevTraceEntry = NULL;
    UINT4               u4DestIp = 0;

    if (MemAllocateMemBlock (gTraceInstPoolId, (UINT1 **) (VOID *) &pTraceEntry)
        == MEM_FAILURE)
    {
        return NULL;
    }

    TRACE_INSTANCE (pTraceEntry) = i4Instance;
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        OCTETSTR_TO_INTEGER (pDestAddr, u4DestIp);
        IPVX_ADDR_INIT_FROMV4 (pTraceEntry->DestAddr, u4DestIp);
    }
    else
    {
        IPVX_ADDR_INIT_FROMV6 (pTraceEntry->DestAddr, pDestAddr);
    }

    TRACE_ENTRY_INIT (pTraceEntry);

    if (RBTreeAdd (grbTraceConfigTable, pTraceEntry) == RB_SUCCESS)
    {
        if ((pPrevTraceEntry =
             RBTreeGet (grbTraceHopTable, pTraceEntry)) != NULL)
        {
            /* Trace route can be done for multiple times for a same destination 
             * So update the Trace Hop table with new information (applicable for
             * only SNMP since trace route trigger from CLI will clean up the
             * route and hop information completly) */
            RBTreeRemove (grbTraceHopTable, pPrevTraceEntry);
        }
        RBTreeAdd (grbTraceHopTable, pTraceEntry);
        return pTraceEntry;
    }
    else
    {
        MemReleaseMemBlock (gTraceInstPoolId, (UINT1 *) pTraceEntry);
        return NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRoute6Init
 *                                                                          
 *    DESCRIPTION      : Function to Setup Socket (IPV6) for transmitting and 
 *                       receiving packets for performing Trace Route Operation
 *                      
 *    INPUT            : pTraceEntry - Pointer to the Trace Entry
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRoute6Init (tTraceEntry * pTraceEntry)
{
    INT4                i4SockSend = -1, i4SockRecv = -1;

    /* UDP socket created for sending packets */
    i4SockSend = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if (i4SockSend < 0)
    {
        TRACE_LOG ("Error in creating UDP socket\n");
        return OSIX_FAILURE;
    }

    /* RAW Socket for receving packets */
    i4SockRecv = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if (i4SockRecv < 0)
    {
        TRACE_LOG ("Error in creating RAW socket\n");
        close (i4SockSend);
        return OSIX_FAILURE;
    }
    /* Setting the socket to non-blocking mode */
    if (fcntl (i4SockRecv, F_SETFL, O_NONBLOCK) < 0)
    {
        TRACE_LOG ("Error in setting non-blocking mode!!!\n");
        close (i4SockSend);
        close (i4SockRecv);
        return OSIX_FAILURE;
    }

    return (TraceRouteProcessPkt (i4SockSend, i4SockRecv, pTraceEntry));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRoute4Init
 *                                                                          
 *    DESCRIPTION      : Function to Setup Socket (IPV4) for transmitting and 
 *                       receiving packets for performing Trace Route Operation
 *                      
 *    INPUT            : pTraceEntry - Pointer to the Trace Entry
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRoute4Init (tTraceEntry * pTraceEntry)
{
    INT4                i4SockSend = -1, i4SockRecv = -1;

    /* UDP socket created for sending packets */
    i4SockSend = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (i4SockSend < 0)
    {
        TRACE_LOG ("Error in creating UDP socket\n");
        return OSIX_FAILURE;
    }

    /* RAW Socket for receving packets */
    i4SockRecv = socket (AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (i4SockRecv < 0)
    {
        TRACE_LOG ("Error in creating RAW socket\n");
        close (i4SockSend);
        return OSIX_FAILURE;
    }
    /* Setting the socket to non-blocking mode */
    if (fcntl (i4SockRecv, F_SETFL, O_NONBLOCK) < 0)
    {
        TRACE_LOG ("Error in setting non-blocking mode!!!\n");
        close (i4SockSend);
        close (i4SockRecv);
        return OSIX_FAILURE;
    }

    return (TraceRouteProcessPkt (i4SockSend, i4SockRecv, pTraceEntry));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteProcessPkt
 *                                                                          
 *    DESCRIPTION      : Function to sends & receives the trace packets(V4/V6),
 *                       process them and update the Trace Information hop by 
 *                       hop until it reaches the destination or until the 
 *                       Max. Configured Hops (TTL) is reached
 *                      
 *    INPUT            : i4SockSend - Socket ID to be used for Sending UDP 
 *                                    Packets
 *                       i4SockRecv - Socket ID to be used for receiving ICMP
 *                                    Packets
 *                       pTraceEntry - Pointer to the Trace Entry
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteProcessPkt (INT4 i4SockSend, INT4 i4SockRecv,
                      tTraceEntry * pTraceEntry)
{
    tTraceInfo         *pTraceInfo = NULL;
    tOsixSysTime        Time1, Time2;
    UINT1               RecvAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4RetVal;
    INT2                i2Count = 0, i2ProbeCount = 0;

    MEMSET (RecvAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    TRACE_OPER_STATUS (pTraceEntry) = TRACE_OPER_IN_PROGRESS;

    for (i2Count = pTraceEntry->i4CurHops; i2Count <= pTraceEntry->i2Max_ttl;
         i2Count++)
    {
        for (i2ProbeCount = 0; i2ProbeCount < TRACE_MAX_PROBES; i2ProbeCount++)
        {
            if (TraceRouteSendPkt (i4SockSend, pTraceEntry) == OSIX_FAILURE)
            {
                TRACE_LOG ("Error in sending packet\n");
                close (i4SockSend);
                close (i4SockRecv);
                return OSIX_FAILURE;
            }

            OsixGetSysTime (&Time1);
            i4RetVal =
                TraceRouteRecvPkt (i4SockRecv, RecvAddr, &Time2, pTraceEntry);
            if (i4RetVal == OSIX_FAILURE)
            {
                TRACE_LOG ("Error in receiving packet\n");
                close (i4SockSend);
                close (i4SockRecv);
                return OSIX_FAILURE;
            }

            if (i2ProbeCount == 0)
            {
                if (MemAllocateMemBlock (gTraceInfoPoolId,
                                         (UINT1 **) (VOID *) &pTraceInfo) ==
                    MEM_FAILURE)
                {
                    TRACE_LOG ("Error in Allocating Memory\n");
                    close (i4SockSend);
                    close (i4SockRecv);
                    return OSIX_FAILURE;
                }

                TMO_SLL_Add (&pTraceEntry->IntrmHopList, &(pTraceInfo->Link));
            }

            IPVX_ADDR_INIT (pTraceInfo->GatewayAddr,
                            TRACE_ADDR_TYPE (pTraceEntry), (VOID *) &RecvAddr);

            if (i4RetVal == TRACE_TIMEDOUT)
            {
                pTraceInfo->ReachTime[i2ProbeCount] =
                    (UINT4) TRACE_NODE_UNREACH;
            }
            else
            {
                pTraceInfo->ReachTime[i2ProbeCount] = (Time2 - Time1);
            }
            pTraceEntry->u2Seq += 1;
        }
        pTraceEntry->i4CurHops += 1;

        if (TRACE_OPER_STATUS (pTraceEntry) == TRACE_OPER_NOT_IN_PROGRESS)
        {
            close (i4SockSend);
            close (i4SockRecv);
            return OSIX_SUCCESS;
        }
    }
    close (i4SockSend);
    close (i4SockRecv);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteSendPkt
 *                                                                          
 *    DESCRIPTION      : Function to sends the trace packets(V4/V6) to the
 *                       destination IP
 *                      
 *    INPUT            : i4SockSend - Socket ID to be used for Sending UDP 
 *                                    Packets
 *                       pTraceEntry - Pointer to the Trace Entry
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : OSIX_FAILURE on Failure or OSIX_FAILURE on Success 
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteSendPkt (INT4 i4SockId, tTraceEntry * pTraceEntry)
{
    struct sockaddr_in6 Ipv6SendAddr;
    struct sockaddr_in  Ipv4SendAddr;
    struct sockaddr    *pIpvxSendAddr = NULL;
    UINT1               au1msg[TRACE_MAX_MTU];
    INT4                i4NewTTL = pTraceEntry->i4CurHops;
    INT4                i4IpvxAddrLen = 0;
    UINT4               u4Index = 0;

    MEMSET (au1msg, 0, TRACE_MAX_MTU);

    if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV6)
    {
        if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                        &i4NewTTL, sizeof (i4NewTTL)) < 0)
        {
            return OSIX_FAILURE;
        }

        MEMSET (&Ipv6SendAddr, 0, sizeof (struct sockaddr_in6));
        Ipv6SendAddr.sin6_family = AF_INET6;
        Ipv6SendAddr.sin6_port = OSIX_HTONS (pTraceEntry->u2Seq);
        MEMCPY (&Ipv6SendAddr.sin6_addr.s6_addr, &TRACE_IPVX_ADDR (pTraceEntry),
                IPVX_IPV6_ADDR_LEN);
        pIpvxSendAddr = (struct sockaddr *) &Ipv6SendAddr;
        i4IpvxAddrLen = sizeof (struct sockaddr_in6);
    }
    else
    {
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TTL,
                        &i4NewTTL, sizeof (i4NewTTL)) < 0)
        {
            return OSIX_FAILURE;
        }

        MEMSET (&Ipv4SendAddr, 0, sizeof (struct sockaddr_in));
        Ipv4SendAddr.sin_family = AF_INET;
        Ipv4SendAddr.sin_port = OSIX_HTONS (pTraceEntry->u2Seq);
        MEMCPY (&Ipv4SendAddr.sin_addr.s_addr, TRACE_IPVX_ADDR (pTraceEntry),
                IPVX_IPV4_ADDR_LEN);
        Ipv4SendAddr.sin_addr.s_addr =
            OSIX_HTONL (Ipv4SendAddr.sin_addr.s_addr);
        pIpvxSendAddr = (struct sockaddr *) &Ipv4SendAddr;
        i4IpvxAddrLen = sizeof (struct sockaddr_in);
    }
    while (u4Index < IP_MAX_OPT_LEN)
    {
        au1msg[u4Index] = u4Index + IP_MAX_OPT_LEN;
        u4Index++;
    }
    if (sendto (i4SockId, au1msg, IP_MAX_OPT_LEN, 0,
                pIpvxSendAddr, i4IpvxAddrLen) != IP_MAX_OPT_LEN)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteRecvPkt
 *                                                                          
 *    DESCRIPTION      : Function to receives the responses for the trace 
 *                       packets (sent earlier) to the destination IP
 *                      
 *    INPUT            : i4SockSend - Socket ID to be used for Sending UDP 
 *                                    Packets
 *                       pTraceEntry - Pointer to the Trace Entry
 *        
 *    OUTPUT           : pIpAddr     - Originator IP (Gateway/Destination)
 *                       pTime       - Time in which the response is been
 *                                     received
 *           
 *    RETURNS          : OSIX_FAILURE on Failure or OSIX_FAILURE on Success 
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteRecvPkt (INT4 i4SockId, UINT1 *pIpAddr, tOsixSysTime * pTime,
                   tTraceEntry * pTraceEntry)
{

    INT4                i4Count, i4RetVal = 0;
    UINT1               u1End = FALSE;
    struct sockaddr_in6 Ipv6RecvAddr;
    struct sockaddr_in  Ipv4RecvAddr;
    struct sockaddr    *pIpvxRecvAddr = NULL;
    INT4                i4IpvxAddrLen;
    UINT1               au1recvmsg[TRACE_MIN_RECV_LEN];
    tIcmp6PktHdr       *pIcmpHdr = NULL;
    tUdp6Hdr           *pUdpHdr = NULL;

    if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV6)
    {
        MEMSET (&Ipv6RecvAddr, 0, sizeof (struct sockaddr_in6));
        Ipv6RecvAddr.sin6_family = AF_INET6;
        pIpvxRecvAddr = (struct sockaddr *) &Ipv6RecvAddr;
        i4IpvxAddrLen = sizeof (Ipv6RecvAddr);
    }
    else
    {
        MEMSET (&Ipv4RecvAddr, 0, sizeof (struct sockaddr_in));
        Ipv4RecvAddr.sin_family = AF_INET;
        pIpvxRecvAddr = (struct sockaddr *) &Ipv4RecvAddr;
        i4IpvxAddrLen = sizeof (Ipv4RecvAddr);
    }
    for (i4Count = 0; i4Count < pTraceEntry->i2Timeout; i4Count++)
    {
        i4RetVal = recvfrom (i4SockId, au1recvmsg, TRACE_MIN_RECV_LEN, 0,
                             pIpvxRecvAddr, &i4IpvxAddrLen);

        if (i4RetVal <= 0)
        {
            /* recvfrom () might have returned error as no packet was 
             * available */
            if ((errno == SLI_EWOULDBLOCK) ||
                (errno == SLI_EMSGSIZE))
            {
                OsixDelayTask ((SYS_NUM_OF_TIME_UNITS_IN_A_SEC * 100) /
                               IPVX_THOUSAND);
                continue;
            }
            return OSIX_FAILURE;
        }

        OsixGetSysTime (pTime);
        if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV6)
        {
            pIcmpHdr = (tIcmp6PktHdr *) (VOID *) au1recvmsg;
            switch (pIcmpHdr->u1Type)
            {
                case ICMP6_DEST_UNREACHABLE:
                {
                    switch (pIcmpHdr->u1Code)
                    {
                        case ICMP6_NO_ROUTE_TO_DEST:
                        case ICMP6_ADDRESS_UNREACHABLE:
                        case ICMP6_PORT_UNREACHABLE:
                            pUdpHdr = (tUdp6Hdr *) (VOID *) (au1recvmsg +
                                                             sizeof
                                                             (tIcmp6PktHdr) +
                                                             44);
                            if (OSIX_NTOHS (pUdpHdr->u2DstPort) ==
                                pTraceEntry->u2Seq)
                            {
                                /* Marking that the destination is reached by
                                 * setting Oper Status to "not in progress" and
                                 * Admin Status to "off" */
                                TRACE_OPER_STATUS (pTraceEntry) =
                                    TRACE_OPER_NOT_IN_PROGRESS;
                                TRACE_ADMIN_STATUS (pTraceEntry) =
                                    TRACE_ADMIN_DISABLE;
                                u1End = TRUE;
                            }
                            break;
                        default:
                            break;
                    }
                }
                    break;
                case ICMP6_PKT_TOO_BIG:
                    break;
                case ICMP6_TIME_EXCEEDED:
                    if (pIcmpHdr->u1Code == 0)
                        u1End = TRUE;
                    break;
                default:
                    break;
            }
        }
        else
        {
            pIcmpHdr = (tIcmp6PktHdr *) (VOID *) (au1recvmsg + 20);
            switch (pIcmpHdr->u1Type)
            {
                case ICMP_DEST_UNREACH:
                    switch (pIcmpHdr->u1Code)
                    {
                        case ICMP_NET_UNREACH:
                        case ICMP_HOST_UNREACH:
                        case ICMP_PROTO_UNREACH:
                        case ICMP_PORT_UNREACH:
                            pUdpHdr = (tUdp6Hdr *) (VOID *) (au1recvmsg + 48);
                            if (OSIX_NTOHS (pUdpHdr->u2DstPort) ==
                                pTraceEntry->u2Seq)
                            {
                                u1End = TRUE;
                                /* Marking that the destination is reached by
                                 * setting Oper Status to "not in progress" and
                                 * Admin Status to "off" */
                                TRACE_OPER_STATUS (pTraceEntry) =
                                    TRACE_OPER_NOT_IN_PROGRESS;
                                TRACE_ADMIN_STATUS (pTraceEntry) =
                                    TRACE_ADMIN_DISABLE;
                            }
                            break;
                        default:
                            break;

                    }
                    break;
                case ICMP_TIME_EXCEED:
                    if (pIcmpHdr->u1Code == 0)
                        u1End = TRUE;
                    break;
                default:
                    break;
            }                    /* end of switch case */
        }

        if (u1End == TRUE)
            break;
        OsixDelayTask ((SYS_NUM_OF_TIME_UNITS_IN_A_SEC * 100) / IPVX_THOUSAND);
    }
    if (i4Count == pTraceEntry->i2Timeout)
        return TRACE_TIMEDOUT;
    else
    {
        if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV6)
        {
            Ipv6RecvAddr = *((struct sockaddr_in6 *) (VOID *) pIpvxRecvAddr);
            MEMCPY (pIpAddr, &Ipv6RecvAddr.sin6_addr.s6_addr,
                    sizeof (tIp6Addr));
        }
        else
        {
            Ipv4RecvAddr = *((struct sockaddr_in *) (VOID *) pIpvxRecvAddr);
            MEMCPY (pIpAddr, &(Ipv4RecvAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);
        }
        /* Set the Error Code from the node for logging them along with
         * the response time (Specific for CLI) */
        if (pIcmpHdr != NULL)
        {
            TRACE_ERR_CODE (pTraceEntry) = pIcmpHdr->u1Code;
        }
        return OSIX_SUCCESS;
    }

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteGetConfigEntry
 *                                                                          
 *    DESCRIPTION      : Function to get the matching Trace Entry by indexing 
 *                       the Trace Config Table with the Input Arguments
 *
 *    INPUT            : i4ConfigIndex  - Instance Number
 *                       pDestAddr   - Destination IP(VX) Address
 *                       i4AddrType  - Dest. Address Type
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : Pointer to the Trace Config Entry on Success else NULL
 *                                                                          
 ****************************************************************************/
tTraceEntry        *
TraceRouteGetConfigEntry (INT4 i4ConfigIndex, UINT1 *pDestAddr, INT4 i4AddrType)
{
    tTraceEntry         TraceEntry;
    UINT4               u4DestIp = 0;

    if (i4ConfigIndex > TRACE_MAX_INSTANCES)
    {
        return NULL;
    }

    MEMSET (&TraceEntry, 0, sizeof (tTraceEntry));

    TraceEntry.i4Instance = i4ConfigIndex;

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        OCTETSTR_TO_INTEGER (pDestAddr, u4DestIp);
        IPVX_ADDR_INIT_FROMV4 (TraceEntry.DestAddr, u4DestIp);
    }
    else
    {
        IPVX_ADDR_INIT_FROMV6 (TraceEntry.DestAddr, pDestAddr);
    }

    return (RBTreeGet (grbTraceConfigTable, &TraceEntry));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteGetHopTableEntry
 *                                                                          
 *    DESCRIPTION      : Function to get the matching Trace Entry by indexing 
 *                       the Trace Hop Table with the Input Arguments
 *
 *    INPUT            : pDestAddr   - Destination IP(VX) Address
 *                       i4AddrType  - Dest. Address Type
 *        
 *    OUTPUT           : None
 *           
 *    RETURNS          : Pointer to the Trace Hop table entry on Success else
 *                       NULL
 *                                                                          
 ****************************************************************************/
tTraceEntry        *
TraceRouteGetHopTableEntry (UINT1 *pDestAddr, INT4 i4AddrType)
{
    tTraceEntry         TraceEntry;
    UINT4               u4DestIp = 0;

    if (pDestAddr == NULL)
    {
        return NULL;
    }

    MEMSET (&TraceEntry, 0, sizeof (tTraceEntry));

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        OCTETSTR_TO_INTEGER (pDestAddr, u4DestIp);
        IPVX_ADDR_INIT_FROMV4 (TraceEntry.DestAddr, u4DestIp);
    }
    else
    {
        IPVX_ADDR_INIT_FROMV6 (TraceEntry.DestAddr, pDestAddr);
    }

    return (RBTreeGet (grbTraceHopTable, &TraceEntry));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteGetNextConfigEntry
 *                                                                          
 *    DESCRIPTION      : Function to get the next successive Trace Entry by 
 *                       passing Input Arguments
 *
 *    INPUT            : i4ConfigIndex  - Instance Number
 *                       pDestAddr   - Destination IP(VX) Address
 *                       i4AddrType  - Dest. Address Type
 *        
 *    OUTPUT           : None
 *
 *    RETURNS          : Pointer to the Trace Entry on Success else NULL
 *                                                                          
 ****************************************************************************/
tTraceEntry        *
TraceRouteGetNextConfigEntry (INT4 i4ConfigIndex, UINT1 *pDestAddr,
                              INT4 i4AddrType)
{
    tTraceEntry         TraceEntry;
    UINT4               u4DestIp;

    if (i4ConfigIndex + 1 > TRACE_MAX_INSTANCES)
    {
        return NULL;
    }

    if ((i4ConfigIndex == 0) && (pDestAddr == NULL))
    {
        return (RBTreeGetFirst (grbTraceConfigTable));
    }
    else
    {
        MEMSET (&TraceEntry, 0, sizeof (tTraceEntry));
        TraceEntry.i4Instance = i4ConfigIndex;
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            OCTETSTR_TO_INTEGER (pDestAddr, u4DestIp);
            IPVX_ADDR_INIT_FROMV4 (TraceEntry.DestAddr, u4DestIp);
        }
        else
        {
            IPVX_ADDR_INIT_FROMV6 (TraceEntry.DestAddr, pDestAddr);
        }

        return (RBTreeGetNext (grbTraceConfigTable, &(TraceEntry.rbConfigNode),
                               NULL));
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteGetNextHopEntry
 *                                                                          
 *    DESCRIPTION      : Function to Get the Next Hop Entry by indexing the
 *                       Config Table with the Input Arguments
 *
 *    INPUT            : pDestAddr   - Destination IP(VX) Address
 *                       i4AddrType  - Dest. Address Type
 *                       i4HopCount  - Hop Count
 *        
 *    OUTPUT           : pNextDest   - Next Destination IP(VX) Address
 *                       pNextDstLen - Next Dest. IP's Length
 *                       pHopCount   - Next Hop Count
 *           
 *    RETURNS          : OSIX_FAILURE on Failure or OSIX_FAILURE on Success 
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteGetNextHopEntry (UINT1 *pDestAddr, INT4 i4AddrType,
                           INT4 i4HopCount, UINT1 *pNextDest,
                           INT4 *pNextDstLen, INT4 *pHopCount)
{
    tTraceEntry         TraceEntry;
    tTraceEntry        *pTraceEntry = NULL;
    UINT4               u4DestIP = 0;

    if (pDestAddr == NULL)
    {
        pTraceEntry = RBTreeGetFirst (grbTraceHopTable);
    }
    else
    {
        MEMSET (&TraceEntry, 0, sizeof (tTraceEntry));
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            OCTETSTR_TO_INTEGER (pDestAddr, u4DestIP);
            IPVX_ADDR_INIT_FROMV4 (TraceEntry.DestAddr, u4DestIP);
        }
        else
        {
            IPVX_ADDR_INIT_FROMV6 (TraceEntry.DestAddr, pDestAddr);
        }

        pTraceEntry = RBTreeGet (grbTraceHopTable, &TraceEntry);
    }

    if (pTraceEntry != NULL)
    {
        if (i4HopCount < (INT4) TMO_SLL_Count (&pTraceEntry->IntrmHopList))
        {
            *pHopCount = i4HopCount + 1;
        }
        else
        {
            MEMSET (&TraceEntry, 0, sizeof (tTraceEntry));
            IPVX_ADDR_INIT (TraceEntry.DestAddr, TRACE_ADDR_TYPE (pTraceEntry),
                            TRACE_IPVX_ADDR (pTraceEntry));

            pTraceEntry = RBTreeGetNext (grbTraceHopTable, &TraceEntry, NULL);
            if (pTraceEntry == NULL)
            {
                return OSIX_FAILURE;
            }

            *pHopCount = 1;
        }

        if (TRACE_ADDR_TYPE (pTraceEntry) == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4DestIP, TRACE_IPVX_ADDR (pTraceEntry),
                    IPVX_IPV4_ADDR_LEN);
            u4DestIP = OSIX_HTONL (u4DestIP);
            MEMSET (pNextDest, 0, IPVX_IPV6_ADDR_LEN);
            MEMCPY (pNextDest, (UINT1 *) &u4DestIP, IPVX_IPV4_ADDR_LEN);
            *pNextDstLen = IPVX_IPV4_ADDR_LEN;
        }
        else
        {
            MEMCPY (pNextDest, TRACE_IPVX_ADDR (pTraceEntry),
                    IPVX_IPV6_ADDR_LEN);
            *pNextDstLen = IPVX_IPV6_ADDR_LEN;
        }
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteConfigTableCompFunc
 *                                                                          
 *    DESCRIPTION      : Function to compare the input keys (Instance, IPVX 
 *                       Address and its type) and returns the comparision
 *                       output using which the Trace Table gets traversed 
 *                       or modified (add/delete)
 *
 *    INPUT            : prbElem1 - Input Key 1
 *                       prbElem2 - Input Key 2
 *                                                                                               
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteConfigTableCompFunc (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tTraceEntry        *pConfigEntry1 = (tTraceEntry *) prbElem1;
    tTraceEntry        *pConfigEntry2 = (tTraceEntry *) prbElem2;
    INT4                i4CmpVal = 0;

    if ((TRACE_INSTANCE (pConfigEntry1) == TRACE_INVALID_INSTANCE) ||
        (TRACE_INSTANCE (pConfigEntry2) == TRACE_INVALID_INSTANCE))
    {
        return (MEMCMP (&pConfigEntry1->DestAddr, &pConfigEntry2->DestAddr,
                        sizeof (tIPvXAddr)));
    }
    /* Skip the Instance ID as a Key while Comparing if Input Instance is INVALID
     * (making the key as `dont care`) */
    if (TRACE_INSTANCE (pConfigEntry1) < TRACE_INSTANCE (pConfigEntry2))
    {
        return -1;
    }
    else if (TRACE_INSTANCE (pConfigEntry1) > TRACE_INSTANCE (pConfigEntry2))
    {
        return 1;
    }
    else
    {
        i4CmpVal = MEMCMP (&pConfigEntry1->DestAddr, &pConfigEntry2->DestAddr,
                           sizeof (tIPvXAddr));
        return i4CmpVal;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteHopTableCompFunc
 *                                                                          
 *    DESCRIPTION      : Function to compare the input keys (IPVX Address and
 *                       its type) and returns the comparision
 *                       output using which the Trace Hop Table gets traversed 
 *                       for Trace Hop table walk purpose
 *
 *    INPUT            : prbElem1 - Input Key 1
 *                       prbElem2 - Input Key 2
 *                                                                                               
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
INT4
TraceRouteHopTableCompFunc (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tIPvXAddr          *pDestAddr1 =
        (tIPvXAddr *) & ((tTraceEntry *) prbElem1)->DestAddr;
    tIPvXAddr          *pDestAddr2 =
        (tIPvXAddr *) & ((tTraceEntry *) prbElem2)->DestAddr;

    return (MEMCMP (pDestAddr1, pDestAddr2, sizeof (tIPvXAddr)));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceRouteAssignMemPoolIds
 *                                                                          
 *    DESCRIPTION      : Function to assign Memory Pool IDs to the respective
 *                       globals
 *
 *    INPUT            : None
 *                      
 *    OUTPUT           : None
 *                            
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
TraceRouteAssignMemPoolIds (VOID)
{
    gTraceInstPoolId = TRCRTMemPoolIds[MAX_TRACE_MAX_INSTANCES_SIZING_ID];
    gTraceInfoPoolId = TRCRTMemPoolIds[MAX_TRACE_INFO_ENTRIES_SIZING_ID];
}

/******************************** END OF FILE *********************************/
