/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: tracecli.c,v 1.14 2015/01/14 12:27:54 siva Exp $
 *
 * Description: This File contains the routines required for Supporting
 *              Trace Route Application in both IPV4 and IPV6
 *
 ******************************************************************************/

#ifndef __TRACECLI_C__
#define __TRACECLI_C__

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "ip6util.h"
#include "ipvx.h"

#include "tracecli.h"
#include "vcm.h"
#include "traceroute.h"

/************************** PRIVATE DECLARATIONS *****************************/

PRIVATE INT4 TraceCliInit PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                                  tIPvXAddr * pIpVxAddr, INT4 i4MinTtl,
                                  INT4 i4MaxTtl));

PRIVATE VOID TraceCliDeInit PROTO ((tCliHandle CliHandle, INT4 i4TraceInst,
                                    INT4 i4AddrType,
                                    tSNMP_OCTET_STRING_TYPE * pDestIpvxAddr));
/********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : cli_process_trace_cmd
 *                                                                          
 *    DESCRIPTION      : This function processes the command entered by User 
 *                       and calls their respective routines (SNMP) for 
 *                       servicing that command
 *
 *    INPUT            : CliHandle   -  CLI Handle  
 *                       u4Command   -  Command number
 *
 *    OUTPUT           : None
 *                            
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/

INT4
cli_process_trace_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tIPvXAddr           IpVxAddr;
    UINT1               Ip6Addr[IPVX_IPV6_ADDR_LEN];
    UINT1              *args[CLI_TRACE_MAX_ARGS];
    UINT1              *pu1IpCxtName = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4VcId = 0;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4MinTtl = 0;
    INT4                i4MaxTtl = 0;
    INT1                argno = IPVX_ZERO;

    CLI_SET_CMD_STATUS (CLI_SUCCESS);

    va_start (ap, u4Command);

    /* Third arguement is always InterfaceName/Index */
    va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_TRACE_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    /* Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        u4VcId = VCM_DEFAULT_CONTEXT;
    }

    switch (u4Command)
    {
        case CLI_TRACE_INIT:

            if (CLI_PTR_TO_I4 (args[0]) == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&u4IpAddr, args[1], sizeof (INT4));
                IPVX_ADDR_INIT (IpVxAddr, IPVX_ADDR_FMLY_IPV4,
                                (UINT1 *) &u4IpAddr);
            }
            else
            {
                INET_ATON6 (args[1], &Ip6Addr);
                IPVX_ADDR_INIT (IpVxAddr, IPVX_ADDR_FMLY_IPV6,
                                (UINT1 *) &Ip6Addr);
            }

            if (args[2] == NULL)

                i4MinTtl = TRACE_DEF_MIN_TTL;
            else
                MEMCPY (&i4MinTtl, args[2], sizeof (INT4));

            if (args[3] == NULL)

                i4MaxTtl = TRACE_DEF_MAX_TTL;

            else
                MEMCPY (&i4MaxTtl, args[3], sizeof (INT4));

            i4RetVal = TraceCliInit (CliHandle, u4VcId, &IpVxAddr, i4MinTtl,
                                     i4MaxTtl);
            break;

        default:
            CliPrintf (CliHandle, "%%Command Not Supported\r\n");
            break;
    }

    CLI_SET_CMD_STATUS (i4RetVal);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceCliInit
 *                                                                          
 *    DESCRIPTION      : This function does the operations required for tracing
 *                       the route to the destination IP  
 *
 *    INPUT            : CliHandle      -   CLI Handle
 *                       u4ContextId    -   Virtual Context ID
 *                       pIpVxAddr      -   IP Address (V4/V6)
 *                       i4MinTtl       -   Minimum TTL
 *                       i4MaxTtl       -   Maximum TTL
 *                         
 *    OUTPUT           : None
 *                          
 *    RETURNS          : CLI_FAILURE on failure cases else returns CLI_SUCCESS
 *                                                                          
 ****************************************************************************/
INT4
TraceCliInit (tCliHandle CliHandle, UINT4 u4ContextId, tIPvXAddr * pIpVxAddr,
              INT4 i4MinTtl, INT4 i4MaxTtl)
{
    tSNMP_OCTET_STRING_TYPE DestIpvxAddr;
    tSNMP_OCTET_STRING_TYPE GatewayAddr;
    INT4                i4Time[TRACE_MAX_PROBES];
    UINT1               au1ErrChar[TRACE_CLI_ERR_STR_LEN];
    UINT1               au1DestIpvxAddr[IP6_ADDR_SIZE];
    UINT1               au1GatewayIpvxAddr[IP6_ADDR_SIZE];
    CHR1               *pu1DestAddr = NULL;
    CHR1               *pu1GatewayAddr = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4TempAddr;
    UINT4               u4DestIp;
    INT4                i4Mtu = 0;
    INT4                i4Ttl = 0;
    INT4                i4ProbeNo = 0;
    INT4                i4OperStatus = 0;
    INT4                i4TraceInst = TRACE_INVALID_INSTANCE;
    INT4                i4AddrType = 0;
    INT4		i4TraceProg = IPVX_ZERO;
    BOOL1               b1EmptyIPAddr = OSIX_FALSE;
    INT1                i1Flag = FALSE;

    MEMSET (au1DestIpvxAddr, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1GatewayIpvxAddr, IP6_ZERO, IP6_ADDR_SIZE);
    MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);

    i4AddrType = pIpVxAddr->u1Afi;
    DestIpvxAddr.pu1_OctetList = au1DestIpvxAddr;
    GatewayAddr.pu1_OctetList = au1GatewayIpvxAddr;

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4DestIp, &pIpVxAddr->au1Addr, IPV4_ADD_LEN);
        u4DestIp = OSIX_NTOHL (u4DestIp);

        MEMCPY (DestIpvxAddr.pu1_OctetList, &u4DestIp, pIpVxAddr->u1AddrLen);
        DestIpvxAddr.i4_Length = pIpVxAddr->u1AddrLen;
    }
    else

    {
        MEMCPY (DestIpvxAddr.pu1_OctetList, &pIpVxAddr->au1Addr,
                pIpVxAddr->u1AddrLen);
        DestIpvxAddr.i4_Length = pIpVxAddr->u1AddrLen;
    }

    /* While doing Trace route operation from CLI use arbitary value (0) as the
     * Trace Config Index */
    i4TraceInst = 0;

    if (TraceRouteGetConfigEntry (i4TraceInst, 
                                  DestIpvxAddr.pu1_OctetList, i4AddrType)
        != NULL)
    {
        CliPrintf (CliHandle, "%% Traceroute is already in progess for this ip \n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMIIpvxTraceConfigAdminStatus (&u4ErrorCode, i4TraceInst,
                                                 i4AddrType, &DestIpvxAddr,
                                                 TRACE_ADMIN_DISABLE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Failed while validating AdminStatus \n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMIIpvxTraceConfigAdminStatus (i4TraceInst, i4AddrType,
                                              &DestIpvxAddr,
                                              TRACE_ADMIN_DISABLE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Failed while setting AdminStatus \n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMIIpvxTraceConfigCxtId (&u4ErrorCode, i4TraceInst,
                                           i4AddrType, &DestIpvxAddr,
                                           u4ContextId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Failed while validating Context ID \n");
        TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
        return CLI_FAILURE;
    }

    if (nmhSetFsMIIpvxTraceConfigCxtId (i4TraceInst, i4AddrType,
                                        &DestIpvxAddr,
                                        u4ContextId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Failed while setting Context ID \n");
        TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
        return CLI_FAILURE;
    }
    if (i4MaxTtl != 0)
    {
        if (nmhTestv2FsMIIpvxTraceConfigMaxTTL (&u4ErrorCode, i4TraceInst,
                                                i4AddrType, &DestIpvxAddr,
                                                i4MaxTtl) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Failed while validating Max TTL \n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIIpvxTraceConfigMaxTTL (i4TraceInst, i4AddrType,
                                             &DestIpvxAddr, i4MaxTtl) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Failed while setting Max TTL \n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }
    }

    if (i4MinTtl != 0)
    {
        if (nmhTestv2FsMIIpvxTraceConfigMinTTL (&u4ErrorCode, i4TraceInst,
                                                i4AddrType, &DestIpvxAddr,
                                                i4MinTtl) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%  Failed while validating Min TTL \n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIIpvxTraceConfigMinTTL (i4TraceInst, i4AddrType,
                                             &DestIpvxAddr, i4MinTtl) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Failed while setting Min TTL \n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }
    }

    i4Mtu = (i4Mtu == 0 ? TRACE_DEF_MTU : i4Mtu);
    i4MaxTtl = (i4MaxTtl == 0 ? TRACE_DEF_MAX_TTL : i4MaxTtl);

    CLI_SET_ERR (IPVX_ZERO);

    CliModifyMoreFlag (CliHandle, &i1Flag);
    for (i4Ttl = i4MinTtl; i4Ttl <= i4MaxTtl; i4Ttl++)
    {
        if (nmhSetFsMIIpvxTraceConfigMaxTTL (i4TraceInst,
                                             i4AddrType, &DestIpvxAddr,
                                             i4Ttl) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Failed while setting Max TTL to %d \r\n", i4Ttl);
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIIpvxTraceConfigAdminStatus (i4TraceInst,
                                                  i4AddrType, &DestIpvxAddr,
                                                  TRACE_ADMIN_ENABLE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed to Initiate Trace Route Operation \r\n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }

        nmhGetFsMIIpvxTraceConfigOperStatus (i4TraceInst,
                                             i4AddrType, &DestIpvxAddr,
                                             &i4OperStatus);

        if (i4OperStatus == TRACE_OPER_NOT_IN_PROGRESS)
        {
            CLI_GET_ERR (&u4ErrorCode);

            if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                switch (u4ErrorCode)
                {
                    case ICMP_NET_UNREACH:
                        MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);
                        SNPRINTF ((CHR1 *) au1ErrChar, TRACE_CLI_ERR_STR_LEN,
                                 "!N");
                        i4TraceProg=IPVX_ONE;
                        break;
                    case ICMP_HOST_UNREACH:
                        MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);
                        SNPRINTF ((CHR1 *) au1ErrChar, TRACE_CLI_ERR_STR_LEN,
                                  "!H");
                        i4TraceProg=IPVX_ONE;
                        break;
                    case ICMP_PROTO_UNREACH:
                        MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);
                        SNPRINTF ((CHR1 *) au1ErrChar, TRACE_CLI_ERR_STR_LEN,
                                  "!P");
                        i4TraceProg=IPVX_ONE;
                        break;
                    default:
                        /* TRACE_INVALID_ERR (ICMP_TIME_EXCEED) or
                         * ICMP_PORT_UNREACH -- Empty String is used */
                        break;
                }
            }
            else                /* IP v6 Error code handling */
            {
                switch (u4ErrorCode)
                {
                    case ICMP6_NO_ROUTE_TO_DEST:
                        MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);
                        SNPRINTF ((CHR1 *) au1ErrChar, TRACE_CLI_ERR_STR_LEN,
                                  "!N");
                        i4TraceProg=IPVX_ONE;
                        break;
                    case ICMP6_ADDRESS_UNREACHABLE:
                        MEMSET (au1ErrChar, 0, TRACE_CLI_ERR_STR_LEN);
                        SNPRINTF ((CHR1 *) au1ErrChar, TRACE_CLI_ERR_STR_LEN,
                                  "!H");
                        i4TraceProg=IPVX_ONE;
                        break;
                    default:
                        /* ICMP6_TIME_EXCEEDED or ICMP6_PORT_UNREACHABLE -- Empty
                         * String is used */
                        break;
                }
            }
        }

        MEMSET (GatewayAddr.pu1_OctetList, 0, sizeof (tIp6Addr));

        if (nmhGetFsMIIpvxTraceIntermHop (i4AddrType,
                                          &DestIpvxAddr, i4Ttl, &GatewayAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed to reach destination with configured TTL"
                       " values \r\n");
            TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
            return CLI_FAILURE;
        }

        nmhGetFsMIIpvxTraceReachTime1 (i4AddrType,
                                       &DestIpvxAddr, i4Ttl, &i4Time[0]);

        nmhGetFsMIIpvxTraceReachTime2 (i4AddrType,
                                       &DestIpvxAddr, i4Ttl, &i4Time[1]);

        nmhGetFsMIIpvxTraceReachTime3 (i4AddrType,
                                       &DestIpvxAddr, i4Ttl, &i4Time[2]);

        if (i4Ttl == i4MinTtl)
        {
            if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                CliPrintf (CliHandle, "Tracing Route to %s with %d hops max and"
                           " %d byte packets \n\r",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) pIpVxAddr->
                                         au1Addr), i4MaxTtl, i4Mtu);
            }
            else
            {
                MEMCPY (&u4TempAddr, pIpVxAddr->au1Addr, IPVX_IPV4_ADDR_LEN);
                CLI_CONVERT_IPADDR_TO_STR (pu1DestAddr, u4TempAddr);
                CliPrintf (CliHandle, "Tracing Route to %s with %d hops max and"
                           " %d byte packets \n\r", pu1DestAddr, i4MaxTtl,
                           i4Mtu);
            }
             if(i4TraceProg==IPVX_ONE)
	     {
		     CliPrintf (CliHandle, "\r[!N - Network Unreachable "
				     "!H - Host Unreachable !P - Protocol Unreachable]\n\r");
	     }
           }
        
       
        CliPrintf (CliHandle, "\n%3d", i4Ttl);

        if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "\t%s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) GatewayAddr.
                                     pu1_OctetList));
            if (IS_ADDR_UNSPECIFIED
                (*(tIp6Addr *) (VOID *) GatewayAddr.pu1_OctetList))
            {
                b1EmptyIPAddr = OSIX_TRUE;
            }
        }
        else
        {
            MEMCPY (&u4TempAddr, GatewayAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);
            u4TempAddr = OSIX_HTONL (u4TempAddr);
            CLI_CONVERT_IPADDR_TO_STR (pu1GatewayAddr, u4TempAddr);
            CliPrintf (CliHandle, "\t%s", pu1GatewayAddr);
            if (u4TempAddr == 0)
            {
                b1EmptyIPAddr = OSIX_TRUE;
            }
        }

        for (i4ProbeNo = 0; i4ProbeNo < TRACE_MAX_PROBES; i4ProbeNo++)
        {
            if (i4Time[i4ProbeNo] == TRACE_NODE_UNREACH)
            {
                /* Costume changes for neat display incase out 3 some replies are
                 * not received */
                if ((i4ProbeNo == 0) && (b1EmptyIPAddr == OSIX_TRUE))
                    CliPrintf (CliHandle, "\t\t");
                else
                    CliPrintf (CliHandle, "\t");

                CliPrintf (CliHandle, "      *    %s", au1ErrChar);
            }
            else
            {
                if (i4Time[i4ProbeNo] > IPVX_THOUSAND)
                {
                    CliPrintf (CliHandle, "\t%6ld s  %s",
                               i4Time[i4ProbeNo], au1ErrChar);
                }
                else
                {
                    CliPrintf (CliHandle, "\t%6ld ms %s",
                               (i4Time[i4ProbeNo]), au1ErrChar);
                }
            }
        }

        CliPrintf (CliHandle, "\r\n");

        if (i4OperStatus == TRACE_OPER_NOT_IN_PROGRESS)
        {
            /* If the tracing process completes, the Admin Status and Oper Status
             * are updated to Disable and Not in Progress resp. So check the State
             * and quit the trace operation */
            break;
        }
        CliFlush (CliHandle);
        b1EmptyIPAddr = OSIX_FALSE;
    }
    CliModifyMoreFlag (CliHandle, &i1Flag);
    TraceCliDeInit (CliHandle, i4TraceInst, i4AddrType, &DestIpvxAddr);
    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : TraceCliDeInit
 *                                                                          
 *    DESCRIPTION      : Function to delete the Trace Configuration information
 *                       This gets called on trace route process completion
 *
 *    INPUT            : CliHandle      -   CLI Handle
 *                       i4TraceInst    -   Trace Instance ID 
 *                       i4AddrType     -   IP Address Type (V4/V6)
 *                       pDestIpvxAddr  -   Destination IP (V4/V6)
 *                         
 *    OUTPUT           : None
 *                          
 *    RETURNS          : OSIX_FAILURE for failure or ICMP Error Code from the 
 *                       last hop while tracing to the Destination IP
 *                                                                          
 ****************************************************************************/
VOID
TraceCliDeInit (tCliHandle CliHandle, INT4 i4TraceInst,
                INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pDestIpvxAddr)
{
    UNUSED_PARAM (CliHandle);

    nmhSetFsMIIpvxTraceConfigAdminStatus (i4TraceInst, i4AddrType,
                                          pDestIpvxAddr, TRACE_ADMIN_DISABLE);
    return;
}

/***********************  END OF FILE **************************************/
#endif /* __TRACECLI_C__ */
