/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trcrtsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _TRCRTSZ_C
#include "traceroute.h"
#include "trcrtsz.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TrcrtSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRCRT_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsTRCRTSizingParams[i4SizingId].u4StructSize,
                              FsTRCRTSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(TRCRTMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            TrcrtSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TrcrtSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTRCRTSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TRCRTMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TrcrtSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TRCRT_MAX_SIZING_ID; i4SizingId++)
    {
        if (TRCRTMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TRCRTMemPoolIds[i4SizingId]);
            TRCRTMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
