/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipvxlw.c,v 1.8 2014/02/24 11:26:33 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "cli.h"
# include  "fssnmp.h"
# include  "ipvx.h"
# include  "ipvxdefs.h"
# include  "vcm.h"
# include  "traceroute.h"

/* LOW LEVEL Routines for Table : FsMIIpvxAddrPrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpvxAddrPrefixTable
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpvxAddrPrefixTable (INT4
                                                 i4FsMIIpvxAddrPrefixIfIndex,
                                                 INT4
                                                 i4FsMIIpvxAddrPrefixAddrType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIIpvxAddrPrefix,
                                                 UINT4 u4FsMIIpvxAddrPrefixLen)
{
    return (nmhValidateIndexInstanceFsIpvxAddrPrefixTable
            (i4FsMIIpvxAddrPrefixIfIndex, i4FsMIIpvxAddrPrefixAddrType,
             pFsMIIpvxAddrPrefix, u4FsMIIpvxAddrPrefixLen));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpvxAddrPrefixTable
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpvxAddrPrefixTable (INT4 *pi4FsMIIpvxAddrPrefixIfIndex,
                                         INT4 *pi4FsMIIpvxAddrPrefixAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpvxAddrPrefix,
                                         UINT4 *pu4FsMIIpvxAddrPrefixLen)
{
    return (nmhGetFirstIndexFsIpvxAddrPrefixTable
            (pi4FsMIIpvxAddrPrefixIfIndex, pi4FsMIIpvxAddrPrefixAddrType,
             pFsMIIpvxAddrPrefix, pu4FsMIIpvxAddrPrefixLen));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpvxAddrPrefixTable
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                nextFsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                nextFsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                nextFsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen
                nextFsMIIpvxAddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpvxAddrPrefixTable (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                        INT4 *pi4NextFsMIIpvxAddrPrefixIfIndex,
                                        INT4 i4FsMIIpvxAddrPrefixAddrType,
                                        INT4 *pi4NextFsMIIpvxAddrPrefixAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIIpvxAddrPrefix,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsMIIpvxAddrPrefix,
                                        UINT4 u4FsMIIpvxAddrPrefixLen,
                                        UINT4 *pu4NextFsMIIpvxAddrPrefixLen)
{
    return (nmhGetNextIndexFsIpvxAddrPrefixTable
            (i4FsMIIpvxAddrPrefixIfIndex, pi4NextFsMIIpvxAddrPrefixIfIndex,
             i4FsMIIpvxAddrPrefixAddrType, pi4NextFsMIIpvxAddrPrefixAddrType,
             pFsMIIpvxAddrPrefix, pNextFsMIIpvxAddrPrefix,
             u4FsMIIpvxAddrPrefixLen, pu4NextFsMIIpvxAddrPrefixLen));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpvxAddrPrefixContextId
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                retValFsMIIpvxAddrPrefixContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxAddrPrefixContextId (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                   INT4 i4FsMIIpvxAddrPrefixAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpvxAddrPrefix,
                                   UINT4 u4FsMIIpvxAddrPrefixLen,
                                   INT4 *pi4RetValFsMIIpvxAddrPrefixContextId)
{
    UNUSED_PARAM (i4FsMIIpvxAddrPrefixAddrType);
    UNUSED_PARAM (pFsMIIpvxAddrPrefix);
    UNUSED_PARAM (u4FsMIIpvxAddrPrefixLen);

    if (VcmGetContextIdFromCfaIfIndex
        (i4FsMIIpvxAddrPrefixIfIndex,
         (UINT4 *) pi4RetValFsMIIpvxAddrPrefixContextId) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                retValFsMIIpvxAddrPrefixProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxAddrPrefixProfileIndex (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                      INT4 i4FsMIIpvxAddrPrefixAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpvxAddrPrefix,
                                      UINT4 u4FsMIIpvxAddrPrefixLen,
                                      UINT4
                                      *pu4RetValFsMIIpvxAddrPrefixProfileIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpvxAddrPrefixProfileIndex (i4FsMIIpvxAddrPrefixIfIndex,
                                            i4FsMIIpvxAddrPrefixAddrType,
                                            pFsMIIpvxAddrPrefix,
                                            u4FsMIIpvxAddrPrefixLen,
                                            pu4RetValFsMIIpvxAddrPrefixProfileIndex);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                retValFsMIIpvxAddrPrefixSecAddrFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxAddrPrefixSecAddrFlag (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                     INT4 i4FsMIIpvxAddrPrefixAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpvxAddrPrefix,
                                     UINT4 u4FsMIIpvxAddrPrefixLen,
                                     INT4
                                     *pi4RetValFsMIIpvxAddrPrefixSecAddrFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpvxAddrPrefixSecAddrFlag (i4FsMIIpvxAddrPrefixIfIndex,
                                           i4FsMIIpvxAddrPrefixAddrType,
                                           pFsMIIpvxAddrPrefix,
                                           u4FsMIIpvxAddrPrefixLen,
                                           pi4RetValFsMIIpvxAddrPrefixSecAddrFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                retValFsMIIpvxAddrPrefixRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxAddrPrefixRowStatus (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                   INT4 i4FsMIIpvxAddrPrefixAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpvxAddrPrefix,
                                   UINT4 u4FsMIIpvxAddrPrefixLen,
                                   INT4 *pi4RetValFsMIIpvxAddrPrefixRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpvxAddrPrefixRowStatus (i4FsMIIpvxAddrPrefixIfIndex,
                                         i4FsMIIpvxAddrPrefixAddrType,
                                         pFsMIIpvxAddrPrefix,
                                         u4FsMIIpvxAddrPrefixLen,
                                         pi4RetValFsMIIpvxAddrPrefixRowStatus);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                setValFsMIIpvxAddrPrefixProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxAddrPrefixProfileIndex (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                      INT4 i4FsMIIpvxAddrPrefixAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpvxAddrPrefix,
                                      UINT4 u4FsMIIpvxAddrPrefixLen,
                                      UINT4
                                      u4SetValFsMIIpvxAddrPrefixProfileIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpvxAddrPrefixProfileIndex (i4FsMIIpvxAddrPrefixIfIndex,
                                            i4FsMIIpvxAddrPrefixAddrType,
                                            pFsMIIpvxAddrPrefix,
                                            u4FsMIIpvxAddrPrefixLen,
                                            u4SetValFsMIIpvxAddrPrefixProfileIndex);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                setValFsMIIpvxAddrPrefixSecAddrFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxAddrPrefixSecAddrFlag (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                     INT4 i4FsMIIpvxAddrPrefixAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpvxAddrPrefix,
                                     UINT4 u4FsMIIpvxAddrPrefixLen,
                                     INT4 i4SetValFsMIIpvxAddrPrefixSecAddrFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpvxAddrPrefixSecAddrFlag (i4FsMIIpvxAddrPrefixIfIndex,
                                           i4FsMIIpvxAddrPrefixAddrType,
                                           pFsMIIpvxAddrPrefix,
                                           u4FsMIIpvxAddrPrefixLen,
                                           i4SetValFsMIIpvxAddrPrefixSecAddrFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                setValFsMIIpvxAddrPrefixRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxAddrPrefixRowStatus (INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                   INT4 i4FsMIIpvxAddrPrefixAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpvxAddrPrefix,
                                   UINT4 u4FsMIIpvxAddrPrefixLen,
                                   INT4 i4SetValFsMIIpvxAddrPrefixRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpvxAddrPrefixRowStatus (i4FsMIIpvxAddrPrefixIfIndex,
                                         i4FsMIIpvxAddrPrefixAddrType,
                                         pFsMIIpvxAddrPrefix,
                                         u4FsMIIpvxAddrPrefixLen,
                                         i4SetValFsMIIpvxAddrPrefixRowStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                testValFsMIIpvxAddrPrefixProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxAddrPrefixProfileIndex (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                         INT4 i4FsMIIpvxAddrPrefixAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpvxAddrPrefix,
                                         UINT4 u4FsMIIpvxAddrPrefixLen,
                                         UINT4
                                         u4TestValFsMIIpvxAddrPrefixProfileIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpvxAddrPrefixProfileIndex (pu4ErrorCode,
                                               i4FsMIIpvxAddrPrefixIfIndex,
                                               i4FsMIIpvxAddrPrefixAddrType,
                                               pFsMIIpvxAddrPrefix,
                                               u4FsMIIpvxAddrPrefixLen,
                                               u4TestValFsMIIpvxAddrPrefixProfileIndex);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                testValFsMIIpvxAddrPrefixSecAddrFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxAddrPrefixSecAddrFlag (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                        INT4 i4FsMIIpvxAddrPrefixAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIIpvxAddrPrefix,
                                        UINT4 u4FsMIIpvxAddrPrefixLen,
                                        INT4
                                        i4TestValFsMIIpvxAddrPrefixSecAddrFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpvxAddrPrefixSecAddrFlag (pu4ErrorCode,
                                              i4FsMIIpvxAddrPrefixIfIndex,
                                              i4FsMIIpvxAddrPrefixAddrType,
                                              pFsMIIpvxAddrPrefix,
                                              u4FsMIIpvxAddrPrefixLen,
                                              i4TestValFsMIIpvxAddrPrefixSecAddrFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen

                The Object 
                testValFsMIIpvxAddrPrefixRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxAddrPrefixRowStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIIpvxAddrPrefixIfIndex,
                                      INT4 i4FsMIIpvxAddrPrefixAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpvxAddrPrefix,
                                      UINT4 u4FsMIIpvxAddrPrefixLen,
                                      INT4 i4TestValFsMIIpvxAddrPrefixRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpvxAddrPrefixRowStatus (pu4ErrorCode,
                                            i4FsMIIpvxAddrPrefixIfIndex,
                                            i4FsMIIpvxAddrPrefixAddrType,
                                            pFsMIIpvxAddrPrefix,
                                            u4FsMIIpvxAddrPrefixLen,
                                            i4TestValFsMIIpvxAddrPrefixRowStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpvxAddrPrefixTable
 Input       :  The Indices
                FsMIIpvxAddrPrefixIfIndex
                FsMIIpvxAddrPrefixAddrType
                FsMIIpvxAddrPrefix
                FsMIIpvxAddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpvxAddrPrefixTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpvxTraceConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpvxTraceConfigTable
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpvxTraceConfigTable (INT4
                                                  i4FsMIIpvxTraceConfigIndex,
                                                  INT4
                                                  i4FsMIIpvxTraceConfigAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIIpvxTraceConfigDest)
{
    UINT4               u4IpAddress = IPVX_ZERO;
    UINT1               au1NullStr[IPVX_V4_EXCESS_ADDR_LEN];

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIIpvxTraceConfigAddrType == IPV4_ADD_TYPE)
    {
        MEMSET (au1NullStr, 0, IPVX_V4_EXCESS_ADDR_LEN);
        if (MEMCMP (&(pFsMIIpvxTraceConfigDest->pu1_OctetList[4]), au1NullStr,
                    IPVX_V4_EXCESS_ADDR_LEN) != 0)
        {
            return SNMP_FAILURE;
        }
        OCTETSTRING_TO_INTEGER (pFsMIIpvxTraceConfigDest, u4IpAddress);

        if (u4IpAddress == 0 || IP_IS_ADDR_CLASS_D (u4IpAddress) ||
            IP_IS_ADDR_CLASS_E (u4IpAddress))
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4FsMIIpvxTraceConfigAddrType == IPV6_ADD_TYPE)
    {
#ifdef IP6_WANTED
        if ((IS_ADDR_MULTI (*(tIp6Addr *) (VOID *)
                            pFsMIIpvxTraceConfigDest->pu1_OctetList)) ||
            (IS_ADDR_UNSPECIFIED (*(tIp6Addr *) (VOID *)
                                  pFsMIIpvxTraceConfigDest->pu1_OctetList)))
        {
            return SNMP_FAILURE;
        }
#endif
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpvxTraceConfigTable
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpvxTraceConfigTable (INT4 *pi4FsMIIpvxTraceConfigIndex,
                                          INT4 *pi4FsMIIpvxTraceConfigAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIIpvxTraceConfigDest)
{
    UINT4               u4DestIP = 0;
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry = TraceRouteGetNextConfigEntry (0, NULL, 0)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIIpvxTraceConfigIndex = pTraceEntry->i4Instance;
    *pi4FsMIIpvxTraceConfigAddrType = TRACE_ADDR_TYPE (pTraceEntry);

    if (TRACE_ADDR_TYPE (pTraceEntry) == IPV4_ADD_TYPE)
    {
        MEMCPY (&u4DestIP, TRACE_IPVX_ADDR (pTraceEntry), IPV4_ADD_LEN);
        INTEGER_TO_OCTETSTRING (u4DestIP, pFsMIIpvxTraceConfigDest);
    }
    else
    {
        MEMCPY (pFsMIIpvxTraceConfigDest->pu1_OctetList,
                TRACE_IPVX_ADDR (pTraceEntry), IPV6_ADD_LEN);
        pFsMIIpvxTraceConfigDest->i4_Length = IPV6_ADD_LEN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpvxTraceConfigTable
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                nextFsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                nextFsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest
                nextFsMIIpvxTraceConfigDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpvxTraceConfigTable (INT4 i4FsMIIpvxTraceConfigIndex,
                                         INT4 *pi4NextFsMIIpvxTraceConfigIndex,
                                         INT4 i4FsMIIpvxTraceConfigAddrType,
                                         INT4
                                         *pi4NextFsMIIpvxTraceConfigAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpvxTraceConfigDest,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMIIpvxTraceConfigDest)
{
    tTraceEntry        *pTraceEntry = NULL;
    UINT4               u4DestIP = 0;

    if ((pTraceEntry =
         TraceRouteGetNextConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                       pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                       i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIIpvxTraceConfigIndex = pTraceEntry->i4Instance;
    *pi4NextFsMIIpvxTraceConfigAddrType = TRACE_ADDR_TYPE (pTraceEntry);

    if (TRACE_ADDR_TYPE (pTraceEntry) == IPV4_ADD_TYPE)
    {
        MEMCPY (&u4DestIP, TRACE_IPVX_ADDR (pTraceEntry), IPV4_ADD_LEN);
        INTEGER_TO_OCTETSTRING (u4DestIP, pNextFsMIIpvxTraceConfigDest);
    }
    else
    {
        MEMCPY (pNextFsMIIpvxTraceConfigDest->pu1_OctetList,
                TRACE_IPVX_ADDR (pTraceEntry), IPV6_ADD_LEN);
        pNextFsMIIpvxTraceConfigDest->i4_Length = IPV6_ADD_LEN;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigAdminStatus
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigAdminStatus (INT4 i4FsMIIpvxTraceConfigIndex,
                                      INT4 i4FsMIIpvxTraceConfigAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpvxTraceConfigDest,
                                      INT4
                                      *pi4RetValFsMIIpvxTraceConfigAdminStatus)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigAdminStatus = TRACE_ADMIN_STATUS (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigMaxTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigMaxTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigMaxTTL (INT4 i4FsMIIpvxTraceConfigIndex,
                                 INT4 i4FsMIIpvxTraceConfigAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIIpvxTraceConfigDest,
                                 INT4 *pi4RetValFsMIIpvxTraceConfigMaxTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigMaxTTL = TRACE_TTL_MAX (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigMinTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigMinTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigMinTTL (INT4 i4FsMIIpvxTraceConfigIndex,
                                 INT4 i4FsMIIpvxTraceConfigAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIIpvxTraceConfigDest,
                                 INT4 *pi4RetValFsMIIpvxTraceConfigMinTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigMinTTL = TRACE_TTL_MIN (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigOperStatus
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigOperStatus (INT4 i4FsMIIpvxTraceConfigIndex,
                                     INT4 i4FsMIIpvxTraceConfigAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpvxTraceConfigDest,
                                     INT4
                                     *pi4RetValFsMIIpvxTraceConfigOperStatus)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigOperStatus = TRACE_OPER_STATUS (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigTimeout
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigTimeout (INT4 i4FsMIIpvxTraceConfigIndex,
                                  INT4 i4FsMIIpvxTraceConfigAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpvxTraceConfigDest,
                                  INT4 *pi4RetValFsMIIpvxTraceConfigTimeout)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigTimeout = TRACE_TIMEOUT (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigMtu
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigMtu (INT4 i4FsMIIpvxTraceConfigIndex,
                              INT4 i4FsMIIpvxTraceConfigAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIIpvxTraceConfigDest,
                              INT4 *pi4RetValFsMIIpvxTraceConfigMtu)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIIpvxTraceConfigMtu = TRACE_MTU (pTraceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceConfigCxtId
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                retValFsMIIpvxTraceConfigCxtId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceConfigCxtId (INT4 i4FsMIIpvxTraceConfigIndex,
                                INT4 i4FsMIIpvxTraceConfigAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIIpvxTraceConfigDest,
                                INT4 *pi4RetValFsMIIpvxTraceConfigCxtId)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIIpvxTraceConfigCxtId = VCM_DEFAULT_CONTEXT;

    UNUSED_PARAM (pTraceEntry);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigAdminStatus
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigAdminStatus (INT4 i4FsMIIpvxTraceConfigIndex,
                                      INT4 i4FsMIIpvxTraceConfigAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIIpvxTraceConfigDest,
                                      INT4
                                      i4SetValFsMIIpvxTraceConfigAdminStatus)
{
    tTraceEntry        *pTraceEntry = NULL;
    INT4                i4RetVal = 0;

    pTraceEntry = TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                            pFsMIIpvxTraceConfigDest->
                                            pu1_OctetList,
                                            i4FsMIIpvxTraceConfigAddrType);

    if (i4SetValFsMIIpvxTraceConfigAdminStatus == TRACE_ADMIN_DISABLE)
    {
        if (pTraceEntry != NULL)
        {
            /* Deleting an existing entry (which can be either in "progress" (in
             * middle of trace route processing) or "not in progress" (trace route
             * is completed or operation stopped due to some failure's.  */

            TraceRouteDeleteInstance (pTraceEntry);
            return SNMP_SUCCESS;
        }
    }

    if (pTraceEntry == NULL)
    {
        /* Create a new Trace route instance and initialize it to the defaults */
        if ((pTraceEntry =
             TraceRouteCreateInstance (i4FsMIIpvxTraceConfigIndex,
                                       pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                       i4FsMIIpvxTraceConfigAddrType)) == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    if (i4SetValFsMIIpvxTraceConfigAdminStatus == TRACE_ADMIN_ENABLE)
    {
        if ((i4RetVal = TraceRouteInit (pTraceEntry)) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            if (TRACE_ADMIN_STATUS (pTraceEntry) == TRACE_ADMIN_DISABLE)
            {
                /* Setting the ICMP Error Code received from the Last Hop 
                 * (for displaying along with the TimeStamp & Gateway IP) */
                CLI_SET_ERR (TRACE_ERR_CODE (pTraceEntry));
            }
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TRACE_ADMIN_STATUS (pTraceEntry) =
            (INT1) i4SetValFsMIIpvxTraceConfigAdminStatus;
        TRACE_OPER_STATUS (pTraceEntry) = TRACE_OPER_NOT_IN_PROGRESS;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigMaxTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigMaxTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigMaxTTL (INT4 i4FsMIIpvxTraceConfigIndex,
                                 INT4 i4FsMIIpvxTraceConfigAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIIpvxTraceConfigDest,
                                 INT4 i4SetValFsMIIpvxTraceConfigMaxTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    TRACE_TTL_MAX (pTraceEntry) = (INT2) i4SetValFsMIIpvxTraceConfigMaxTTL;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigMinTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigMinTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigMinTTL (INT4 i4FsMIIpvxTraceConfigIndex,
                                 INT4 i4FsMIIpvxTraceConfigAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIIpvxTraceConfigDest,
                                 INT4 i4SetValFsMIIpvxTraceConfigMinTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    TRACE_TTL_MIN (pTraceEntry) = (INT2) i4SetValFsMIIpvxTraceConfigMinTTL;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigTimeout
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigTimeout (INT4 i4FsMIIpvxTraceConfigIndex,
                                  INT4 i4FsMIIpvxTraceConfigAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIIpvxTraceConfigDest,
                                  INT4 i4SetValFsMIIpvxTraceConfigTimeout)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    TRACE_TIMEOUT (pTraceEntry) = (INT2) i4SetValFsMIIpvxTraceConfigTimeout;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigMtu
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigMtu (INT4 i4FsMIIpvxTraceConfigIndex,
                              INT4 i4FsMIIpvxTraceConfigAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIIpvxTraceConfigDest,
                              INT4 i4SetValFsMIIpvxTraceConfigMtu)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    TRACE_MTU (pTraceEntry) = (INT2) i4SetValFsMIIpvxTraceConfigMtu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpvxTraceConfigCxtId
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                setValFsMIIpvxTraceConfigCxtId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpvxTraceConfigCxtId (INT4 i4FsMIIpvxTraceConfigIndex,
                                INT4 i4FsMIIpvxTraceConfigAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIIpvxTraceConfigDest,
                                INT4 i4SetValFsMIIpvxTraceConfigCxtId)
{
    tTraceEntry        *pTraceEntry = NULL;

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    TRACE_CONTEXT (pTraceEntry) = i4SetValFsMIIpvxTraceConfigCxtId;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigAdminStatus
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigAdminStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIIpvxTraceConfigIndex,
                                         INT4 i4FsMIIpvxTraceConfigAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIIpvxTraceConfigDest,
                                         INT4
                                         i4TestValFsMIIpvxTraceConfigAdminStatus)
{
    tTraceEntry        *pTraceEntry = NULL;
    UINT1               au1NullStr[IPVX_V4_EXCESS_ADDR_LEN];
    UINT4               u4IpAddress = 0;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4FsMIIpvxTraceConfigAddrType == IPV4_ADD_TYPE)
    {
        MEMSET (au1NullStr, 0, IPVX_V4_EXCESS_ADDR_LEN);
        if (MEMCMP (&(pFsMIIpvxTraceConfigDest->pu1_OctetList[4]), au1NullStr,
                    IPVX_V4_EXCESS_ADDR_LEN) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        OCTETSTRING_TO_INTEGER (pFsMIIpvxTraceConfigDest, u4IpAddress);

        if (IP_IS_ADDR_CLASS_D (u4IpAddress) ||
            IP_IS_ADDR_CLASS_E (u4IpAddress))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (i4FsMIIpvxTraceConfigAddrType == IPV6_ADD_TYPE)
    {
#ifdef IP6_WANTED
        if ((IS_ADDR_MULTI (*(tIp6Addr *) (VOID *)
                            pFsMIIpvxTraceConfigDest->pu1_OctetList)) ||
            (IS_ADDR_UNSPECIFIED (*(tIp6Addr *) (VOID *)
                                  pFsMIIpvxTraceConfigDest->pu1_OctetList)))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
#endif
    }

    if (!((i4TestValFsMIIpvxTraceConfigAdminStatus == TRACE_ADMIN_DISABLE) ||
          (i4TestValFsMIIpvxTraceConfigAdminStatus == TRACE_ADMIN_ENABLE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) != NULL)
    {
        /* Do not allow to set this object if the entry is already Admin UP */
        /* Dest IP is Checked here because this will used for
         * sending probes with different TTL's */

        if ((i4TestValFsMIIpvxTraceConfigAdminStatus == TRACE_ADMIN_ENABLE) &&
            ((TRACE_OPER_STATUS (pTraceEntry) == TRACE_OPER_IN_PROGRESS) ||
             TRACE_ADDR_TYPE (pTraceEntry) == 0))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (TRACE_ADDR_TYPE (pTraceEntry) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigMaxTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigMaxTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigMaxTTL (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIIpvxTraceConfigIndex,
                                    INT4 i4FsMIIpvxTraceConfigAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIIpvxTraceConfigDest,
                                    INT4 i4TestValFsMIIpvxTraceConfigMaxTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIIpvxTraceConfigMaxTTL < TRACE_MIN_TTL) ||
        (i4TestValFsMIIpvxTraceConfigMaxTTL > TRACE_MAX_TTL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        /* Invalid Index - No Such Entry */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Do not allow to set any values if the entry is already UP */
    if (TRACE_ADMIN_STATUS (pTraceEntry) == TRACE_ADMIN_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMIIpvxTraceConfigMaxTTL < TRACE_TTL_MIN (pTraceEntry))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigMinTTL
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigMinTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigMinTTL (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIIpvxTraceConfigIndex,
                                    INT4 i4FsMIIpvxTraceConfigAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIIpvxTraceConfigDest,
                                    INT4 i4TestValFsMIIpvxTraceConfigMinTTL)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIIpvxTraceConfigMinTTL < TRACE_MIN_TTL) ||
        (i4TestValFsMIIpvxTraceConfigMinTTL > TRACE_MAX_TTL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        /* Invalid Index - No Such Entry */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Do not allow to set any values if the entry is already UP */
    if (TRACE_ADMIN_STATUS (pTraceEntry) == TRACE_ADMIN_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMIIpvxTraceConfigMinTTL > TRACE_TTL_MAX (pTraceEntry))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigTimeout
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigTimeout (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIIpvxTraceConfigIndex,
                                     INT4 i4FsMIIpvxTraceConfigAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIIpvxTraceConfigDest,
                                     INT4 i4TestValFsMIIpvxTraceConfigTimeout)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIIpvxTraceConfigTimeout < TRACE_MIN_TIMEOUT) ||
        (i4TestValFsMIIpvxTraceConfigTimeout > TRACE_MAX_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        /* Invalid Index - No Such Entry */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Do not allow to set any values if the entry is already UP */
    if (TRACE_ADMIN_STATUS (pTraceEntry) == TRACE_ADMIN_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigMtu
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigMtu (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIIpvxTraceConfigIndex,
                                 INT4 i4FsMIIpvxTraceConfigAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIIpvxTraceConfigDest,
                                 INT4 i4TestValFsMIIpvxTraceConfigMtu)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIIpvxTraceConfigMtu < TRACE_MIN_MTU) ||
        (i4TestValFsMIIpvxTraceConfigMtu > TRACE_MAX_MTU))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        /* Invalid Index - No Such Entry */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Do not allow to set any values if the entry is already UP */
    if (TRACE_ADMIN_STATUS (pTraceEntry) == TRACE_ADMIN_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpvxTraceConfigCxtId
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest

                The Object 
                testValFsMIIpvxTraceConfigCxtId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpvxTraceConfigCxtId (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIIpvxTraceConfigIndex,
                                   INT4 i4FsMIIpvxTraceConfigAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIIpvxTraceConfigDest,
                                   INT4 i4TestValFsMIIpvxTraceConfigCxtId)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (!((i4FsMIIpvxTraceConfigIndex >= 0) &&
          (i4FsMIIpvxTraceConfigIndex < TRACE_MAX_INSTANCES)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTraceEntry =
         TraceRouteGetConfigEntry (i4FsMIIpvxTraceConfigIndex,
                                   pFsMIIpvxTraceConfigDest->pu1_OctetList,
                                   i4FsMIIpvxTraceConfigAddrType)) == NULL)
    {
        /* Invalid Index - No Such Entry */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMIIpvxTraceConfigCxtId == VCM_DEFAULT_CONTEXT)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (pTraceEntry);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpvxTraceConfigTable
 Input       :  The Indices
                FsMIIpvxTraceConfigIndex
                FsMIIpvxTraceConfigAddrType
                FsMIIpvxTraceConfigDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpvxTraceConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpvxTraceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpvxTraceTable
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpvxTraceTable (INT4 i4FsMIIpvxTraceAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIIpvxTraceAddr,
                                            INT4 i4FsMIIpvxTraceHopCount)
{
    tTraceEntry        *pTraceEntry = NULL;

    if (((pTraceEntry =
          TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                      i4FsMIIpvxTraceAddrType)) == NULL) ||
        (TMO_SLL_Nth
         (&pTraceEntry->IntrmHopList, (UINT4) i4FsMIIpvxTraceHopCount) == NULL))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpvxTraceTable
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpvxTraceTable (INT4 *pi4FsMIIpvxTraceAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIIpvxTraceAddr,
                                    INT4 *pi4FsMIIpvxTraceHopCount)
{
    if (TraceRouteGetNextHopEntry
        (NULL, 0, 0, pFsMIIpvxTraceAddr->pu1_OctetList,
         &pFsMIIpvxTraceAddr->i4_Length,
         pi4FsMIIpvxTraceHopCount) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pFsMIIpvxTraceAddr->i4_Length == IPV4_ADD_LEN)
    {
        *pi4FsMIIpvxTraceAddrType = IPV4_ADD_TYPE;
    }
    else
    {
        *pi4FsMIIpvxTraceAddrType = IPV6_ADD_TYPE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpvxTraceTable
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                nextFsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                nextFsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount
                nextFsMIIpvxTraceHopCount
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpvxTraceTable (INT4 i4FsMIIpvxTraceAddrType,
                                   INT4 *pi4NextFsMIIpvxTraceAddrType,
                                   tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMIIpvxTraceAddr,
                                   INT4 i4FsMIIpvxTraceHopCount,
                                   INT4 *pi4NextFsMIIpvxTraceHopCount)
{
    if (TraceRouteGetNextHopEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                   i4FsMIIpvxTraceAddrType,
                                   i4FsMIIpvxTraceHopCount,
                                   pNextFsMIIpvxTraceAddr->pu1_OctetList,
                                   &pNextFsMIIpvxTraceAddr->i4_Length,
                                   pi4NextFsMIIpvxTraceHopCount) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pNextFsMIIpvxTraceAddr->i4_Length == IPV4_ADD_LEN)
    {
        *pi4NextFsMIIpvxTraceAddrType = IPV4_ADD_TYPE;
    }
    else
    {
        *pi4NextFsMIIpvxTraceAddrType = IPV6_ADD_TYPE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceIntermHop
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount

                The Object 
                retValFsMIIpvxTraceIntermHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceIntermHop (INT4 i4FsMIIpvxTraceAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                              INT4 i4FsMIIpvxTraceHopCount,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMIIpvxTraceIntermHop)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceInfo         *pTraceInfo = NULL;

    if ((pTraceEntry =
         TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                     i4FsMIIpvxTraceAddrType)) != NULL)
    {
        if ((pTraceInfo =
             (tTraceInfo *) TMO_SLL_Nth (&pTraceEntry->IntrmHopList,
                                         i4FsMIIpvxTraceHopCount)) != NULL)
        {
            if (pTraceInfo->GatewayAddr.u1Afi == IPV4_ADD_TYPE)
            {
                MEMCPY (pRetValFsMIIpvxTraceIntermHop->pu1_OctetList,
                        pTraceInfo->GatewayAddr.au1Addr, IPV4_ADD_LEN);
                pRetValFsMIIpvxTraceIntermHop->i4_Length = IPV4_ADD_LEN;
            }
            else
            {
                MEMCPY (pRetValFsMIIpvxTraceIntermHop->pu1_OctetList,
                        pTraceInfo->GatewayAddr.au1Addr, IPV6_ADD_LEN);
                pRetValFsMIIpvxTraceIntermHop->i4_Length = IPV6_ADD_LEN;
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceReachTime1
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount

                The Object 
                retValFsMIIpvxTraceReachTime1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceReachTime1 (INT4 i4FsMIIpvxTraceAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                               INT4 i4FsMIIpvxTraceHopCount,
                               INT4 *pi4RetValFsMIIpvxTraceReachTime1)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceInfo         *pTraceInfo = NULL;

    if ((pTraceEntry =
         TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                     i4FsMIIpvxTraceAddrType)) != NULL)
    {
        if ((pTraceInfo =
             (tTraceInfo *) TMO_SLL_Nth (&pTraceEntry->IntrmHopList,
                                         i4FsMIIpvxTraceHopCount)) != NULL)
        {
            *pi4RetValFsMIIpvxTraceReachTime1 = pTraceInfo->ReachTime[0];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceReachTime2
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount

                The Object 
                retValFsMIIpvxTraceReachTime2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceReachTime2 (INT4 i4FsMIIpvxTraceAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                               INT4 i4FsMIIpvxTraceHopCount,
                               INT4 *pi4RetValFsMIIpvxTraceReachTime2)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceInfo         *pTraceInfo = NULL;

    if ((pTraceEntry =
         TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                     i4FsMIIpvxTraceAddrType)) != NULL)
    {
        if ((pTraceInfo =
             (tTraceInfo *) TMO_SLL_Nth (&pTraceEntry->IntrmHopList,
                                         i4FsMIIpvxTraceHopCount)) != NULL)
        {
            *pi4RetValFsMIIpvxTraceReachTime2 = pTraceInfo->ReachTime[1];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceReachTime3
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount

                The Object 
                retValFsMIIpvxTraceReachTime3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceReachTime3 (INT4 i4FsMIIpvxTraceAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                               INT4 i4FsMIIpvxTraceHopCount,
                               INT4 *pi4RetValFsMIIpvxTraceReachTime3)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceInfo         *pTraceInfo = NULL;

    if ((pTraceEntry =
         TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                     i4FsMIIpvxTraceAddrType)) != NULL)
    {
        if ((pTraceInfo =
             (tTraceInfo *) TMO_SLL_Nth (&pTraceEntry->IntrmHopList,
                                         i4FsMIIpvxTraceHopCount)) != NULL)
        {
            *pi4RetValFsMIIpvxTraceReachTime3 = pTraceInfo->ReachTime[2];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpvxTraceCxtId
 Input       :  The Indices
                FsMIIpvxTraceAddrType
                FsMIIpvxTraceAddr
                FsMIIpvxTraceHopCount

                The Object 
                retValFsMIIpvxTraceCxtId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpvxTraceCxtId (INT4 i4FsMIIpvxTraceAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFsMIIpvxTraceAddr,
                          INT4 i4FsMIIpvxTraceHopCount,
                          INT4 *pi4RetValFsMIIpvxTraceCxtId)
{
    tTraceEntry        *pTraceEntry = NULL;
    tTraceInfo         *pTraceInfo = NULL;

    if ((pTraceEntry =
         TraceRouteGetHopTableEntry (pFsMIIpvxTraceAddr->pu1_OctetList,
                                     i4FsMIIpvxTraceAddrType)) != NULL)
    {
        if ((pTraceInfo =
             (tTraceInfo *) TMO_SLL_Nth (&pTraceEntry->IntrmHopList,
                                         i4FsMIIpvxTraceHopCount)) != NULL)
        {
            *pi4RetValFsMIIpvxTraceCxtId = TRACE_CONTEXT (pTraceEntry);
            return SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (pTraceInfo);
    return SNMP_FAILURE;
}
