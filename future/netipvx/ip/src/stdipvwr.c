/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* *
* * $Id: stdipvwr.c,v 1.4 2011/03/08 11:58:44 siva Exp $
* *
* * Description: Protocol Low Level Routines
* *********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "stdipvlw.h"
# include  "stdipvwr.h"
# include  "stdipvdb.h"
# include  "ipvxext.h"
# include  "ipvx.h"

VOID
RegisterSTDIPV ()
{
    SNMPRegisterMibWithContextIdAndLock (&stdipvOID, &stdipvEntry,
                                         IpvxLock, IpvxUnLock,
                                         IpvxSetContext, IpvxResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdipvOID, (const UINT1 *) "stdipvx");
}

VOID
UnRegisterSTDIPV ()
{
    SNMPUnRegisterMib (&stdipvOID, &stdipvEntry);
    SNMPDelSysorEntry (&stdipvOID, (const UINT1 *) "stdipvx");
}

INT4
IpForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpForwarding (&(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
IpDefaultTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpDefaultTTL (&(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
IpReasmTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpReasmTimeout (&(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
Ipv6IpForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6IpForwarding (&(pMultiData->i4_SLongValue)));
}

INT4
Ipv6IpDefaultHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6IpDefaultHopLimit (&(pMultiData->i4_SLongValue)));
}

INT4
Ipv4InterfaceTableLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv4InterfaceTableLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
IpForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpForwarding (pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpDefaultTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpDefaultTTL (pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
Ipv6IpForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpv6IpForwarding (pMultiData->i4_SLongValue));
}

INT4
Ipv6IpDefaultHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpv6IpDefaultHopLimit (pMultiData->i4_SLongValue));
}

INT4
IpForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpForwarding (pu4Error, pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
IpDefaultTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpDefaultTTL (pu4Error, pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
Ipv6IpForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Ipv6IpForwarding (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Ipv6IpDefaultHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Ipv6IpDefaultHopLimit
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IpForwardingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhDepv2IpForwarding (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
    return SNMP_FAILURE;
#endif
}

INT4
IpDefaultTTLDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhDepv2IpDefaultTTL (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
    return SNMP_FAILURE;
#endif
}

INT4
Ipv6IpForwardingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6IpForwarding (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6IpDefaultHopLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6IpDefaultHopLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpv4InterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv4InterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv4InterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ipv4InterfaceReasmMaxSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv4InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv4InterfaceReasmMaxSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv4InterfaceEnableStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv4InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv4InterfaceEnableStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv4InterfaceRetransmitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv4InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv4InterfaceRetransmitTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv4InterfaceEnableStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv4InterfaceEnableStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv4InterfaceEnableStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Ipv4InterfaceEnableStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Ipv4InterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv4InterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ipv6InterfaceTableLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6InterfaceTableLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexIpv6InterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6InterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6InterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ipv6InterfaceReasmMaxSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceReasmMaxSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6InterfaceIdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceIdentifier (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Ipv6InterfaceEnableStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceEnableStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6InterfaceReachableTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceReachableTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6InterfaceRetransmitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceRetransmitTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6InterfaceForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6InterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6InterfaceForwarding (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6InterfaceEnableStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6InterfaceEnableStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv6InterfaceForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6InterfaceForwarding (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Ipv6InterfaceEnableStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6InterfaceEnableStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Ipv6InterfaceForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6InterfaceForwarding (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Ipv6InterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6InterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpSystemStatsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpSystemStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpSystemStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpSystemStatsInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInReceives (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInReceives
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInHdrErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInNoRoutes (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInAddrErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInUnknownProtos
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInTruncatedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInTruncatedPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInForwDatagramsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsReasmReqds (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsReasmOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsReasmFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInDelivers (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInDelivers
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutRequests
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutRequests
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutNoRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutForwDatagramsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutForwDatagramsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutFragReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutFragReqds
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutFragOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutFragFails
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutFragCreates
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsOutTransmitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutTransmits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutTransmitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutTransmits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsInMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutMcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutMcastOctetsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsInBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsInBcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCInBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCInBcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsOutBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsOutBcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsHCOutBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsHCOutBcastPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpSystemStatsDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsDiscontinuityTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpSystemStatsRefreshRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpSystemStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpSystemStatsRefreshRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsTableLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpIfStatsTableLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexIpIfStatsTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpIfStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpIfStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpIfStatsInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInReceives (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInReceives (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInHdrErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInNoRoutes (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInAddrErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInUnknownProtos
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInTruncatedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInTruncatedPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsReasmReqds (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsReasmOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsReasmFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInDelivers (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInDelivers (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutRequests (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutRequests (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutForwDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsOutFragReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutFragReqds (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsOutFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutFragOKs (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsOutFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutFragFails (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsOutFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutFragCreates (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsOutTransmitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutTransmits (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutTransmitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutTransmits (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsInMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInMcastOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutMcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutMcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutMcastOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutMcastOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutMcastOctets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsInBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsInBcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCInBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCInBcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsOutBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsOutBcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsHCOutBcastPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsHCOutBcastPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
IpIfStatsDiscontinuityTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsDiscontinuityTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpIfStatsRefreshRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpIfStatsRefreshRate (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIpAddressPrefixTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpAddressPrefixTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpAddressPrefixTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpAddressPrefixOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefixOrigin (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressPrefixOnLinkFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefixOnLinkFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressPrefixAutonomousFlagGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefixAutonomousFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressPrefixAdvPreferredLifetimeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefixAdvPreferredLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpAddressPrefixAdvValidLifetimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefixAdvValidLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpAddressSpinLockGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpAddressSpinLock (&(pMultiData->i4_SLongValue)));
}

INT4
IpAddressSpinLockSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpAddressSpinLock (pMultiData->i4_SLongValue));
}

INT4
IpAddressSpinLockTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IpAddressSpinLock (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IpAddressSpinLockDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IpAddressSpinLock
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpAddressTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpAddressTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpAddressTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpAddressIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressType (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressPrefix (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOidValue));

}

INT4
IpAddressOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressOrigin (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressCreatedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressCreated (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
IpAddressLastChangedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressLastChanged (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IpAddressRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAddressStorageType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IpAddressIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpAddressIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
IpAddressTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpAddressType (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
IpAddressStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpAddressStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
IpAddressRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpAddressRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IpAddressStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpAddressStorageType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IpAddressIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2IpAddressIfIndex (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IpAddressTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2IpAddressType (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
IpAddressStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2IpAddressStatus (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IpAddressRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2IpAddressRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
IpAddressStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IpAddressStorageType (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IpAddressTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IpAddressTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpNetToPhysicalTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpNetToPhysicalTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpNetToPhysicalTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpNetToPhysicalPhysAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpNetToPhysicalTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToPhysicalPhysAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
IpNetToPhysicalLastUpdatedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpNetToPhysicalTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToPhysicalLastUpdated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IpNetToPhysicalTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpNetToPhysicalTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToPhysicalType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IpNetToPhysicalStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpNetToPhysicalTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToPhysicalState (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IpNetToPhysicalRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpNetToPhysicalTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToPhysicalRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IpNetToPhysicalPhysAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpNetToPhysicalPhysAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
IpNetToPhysicalTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpNetToPhysicalType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IpNetToPhysicalRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpNetToPhysicalRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
IpNetToPhysicalPhysAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IpNetToPhysicalPhysAddress (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[2].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
IpNetToPhysicalTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IpNetToPhysicalType (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IpNetToPhysicalRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IpNetToPhysicalRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IpNetToPhysicalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IpNetToPhysicalTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpv6ScopeZoneIndexTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6ScopeZoneIndexTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6ScopeZoneIndexTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ipv6ScopeZoneIndexLinkLocalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexLinkLocal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndex3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndex3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexAdminLocalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexAdminLocal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexSiteLocalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexSiteLocal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndex6Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndex6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndex7Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndex7 (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexOrganizationLocalGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexOrganizationLocal
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndex9Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndex9 (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexAGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexA (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexB (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexC (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6ScopeZoneIndexDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6ScopeZoneIndexD (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIpDefaultRouterTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpDefaultRouterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpDefaultRouterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IpDefaultRouterLifetimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpDefaultRouterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpDefaultRouterLifetime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IpDefaultRouterPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpDefaultRouterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpDefaultRouterPreference
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6RouterAdvertSpinLockGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIpv6RouterAdvertSpinLock (&(pMultiData->i4_SLongValue)));
}

INT4
Ipv6RouterAdvertSpinLockSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIpv6RouterAdvertSpinLock (pMultiData->i4_SLongValue));
}

INT4
Ipv6RouterAdvertSpinLockTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Ipv6RouterAdvertSpinLock
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Ipv6RouterAdvertSpinLockDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6RouterAdvertSpinLock
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIpv6RouterAdvertTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpv6RouterAdvertTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpv6RouterAdvertTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ipv6RouterAdvertSendAdvertsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertSendAdverts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6RouterAdvertMaxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertMaxInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertMinIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertMinInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertManagedFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertManagedFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6RouterAdvertOtherConfigFlagGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertOtherConfigFlag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6RouterAdvertLinkMTUGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertLinkMTU (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertReachableTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertReachableTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertRetransmitTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertRetransmitTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertCurHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertCurHopLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertDefaultLifetimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertDefaultLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ipv6RouterAdvertRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIpv6RouterAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpv6RouterAdvertRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ipv6RouterAdvertSendAdvertsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertSendAdverts
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertMaxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertMaxInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertMinIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertMinInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertManagedFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertManagedFlag
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertOtherConfigFlagSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertOtherConfigFlag
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertLinkMTUSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertLinkMTU (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertReachableTimeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertReachableTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertRetransmitTimeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertRetransmitTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertCurHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertCurHopLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertDefaultLifetimeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertDefaultLifetime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpv6RouterAdvertRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertSendAdvertsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertSendAdverts (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertMaxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertMaxInterval (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertMinIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertMinInterval (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertManagedFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertManagedFlag (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertOtherConfigFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertOtherConfigFlag (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Ipv6RouterAdvertLinkMTUTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertLinkMTU (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertReachableTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertReachableTime (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertRetransmitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertRetransmitTime (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
Ipv6RouterAdvertCurHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertCurHopLimit (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Ipv6RouterAdvertDefaultLifetimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertDefaultLifetime (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
Ipv6RouterAdvertRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Ipv6RouterAdvertRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Ipv6RouterAdvertTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ipv6RouterAdvertTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIcmpStatsTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIcmpStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIcmpStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IcmpStatsInMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpStatsInMsgs (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
IcmpStatsInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpStatsInErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
IcmpStatsOutMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpStatsOutMsgs (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
IcmpStatsOutErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpStatsOutErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIcmpMsgStatsTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIcmpMsgStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIcmpMsgStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IcmpMsgStatsInPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpMsgStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpMsgStatsInPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
IcmpMsgStatsOutPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIcmpMsgStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIcmpMsgStatsOutPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IpInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInReceives (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInHdrErrors (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInAddrErrors (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpForwDatagrams (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInUnknownProtos (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInDiscards (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpInDelivers (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpOutRequests (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpOutDiscards (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpOutNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpOutNoRoutes (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpReasmReqds (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpReasmOKs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpReasmFails (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpFragOKs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpFragFails (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpFragCreates (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IpRoutingDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpRoutingDiscards (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
GetNextIndexIpAddrTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpAddrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpAddrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pNextMultiIndex);
    UNUSED_PARAM (pFirstMultiIndex);
    return SNMP_SUCCESS;
#endif

}

INT4
IpAdEntAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpAdEntIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAdEntIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpAdEntNetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAdEntNetMask (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpAdEntBcastAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAdEntBcastAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpAdEntReasmMaxSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpAdEntReasmMaxSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
GetNextIndexIpNetToMediaTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpNetToMediaTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpNetToMediaTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return (SNMP_SUCCESS);
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaPhysAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToMediaPhysAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->pOctetStrValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaNetAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;
    return (SNMP_SUCCESS);
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpNetToMediaType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpNetToMediaIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IpNetToMediaPhysAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpNetToMediaPhysAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->pOctetStrValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaNetAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIpNetToMediaNetAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
IpNetToMediaTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpNetToMediaType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IpNetToMediaIfIndex (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IpNetToMediaPhysAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpNetToMediaPhysAddress (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->pOctetStrValue));
#else

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaNetAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IpNetToMediaNetAddress (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));
}

INT4
IpNetToMediaTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpNetToMediaType (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));
#else

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpNetToMediaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhDepv2IpNetToMediaTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
    return SNMP_SUCCESS;
#endif
}

INT4
IcmpInMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInMsgs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInErrors (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInDestUnreachs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInTimeExcds (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInParmProbs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInSrcQuenchsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInSrcQuenchs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInRedirects (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInEchos (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInEchoReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInTimestampsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInTimestamps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInTimestampRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInTimestampReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInAddrMasksGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInAddrMasks (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpInAddrMaskRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpInAddrMaskReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutMsgs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutErrors (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutDestUnreachs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutTimeExcds (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutParmProbs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutSrcQuenchsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutSrcQuenchs (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutRedirects (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutEchos (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutEchoReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutTimestampsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutTimestamps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutTimestampRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutTimestampReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutAddrMasksGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutAddrMasks (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
IcmpOutAddrMaskRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIcmpOutAddrMaskReps (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif
}

INT4
InetCidrRouteNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetInetCidrRouteNumber (&(pMultiData->u4_ULongValue)));
}

INT4
InetCidrRouteDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetInetCidrRouteDiscards (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexInetCidrRouteTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexInetCidrRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pNextMultiIndex->pIndex[3].pOidValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexInetCidrRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].pOidValue,
             pNextMultiIndex->pIndex[3].pOidValue,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
InetCidrRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].pOidValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteProto (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].pOidValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].pOidValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiIndex->pIndex[5].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
InetCidrRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteNextHopAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].pOidValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
InetCidrRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteMetric1 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteMetric2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteMetric2 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteMetric3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteMetric3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteMetric4Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteMetric4 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteMetric5Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteMetric5 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceInetCidrRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].pOidValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetInetCidrRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].pOidValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
InetCidrRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].pOidValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteNextHopAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].pOidValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
InetCidrRouteMetric1Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteMetric1 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric2Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteMetric2 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric3Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteMetric3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric4Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteMetric4 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric5Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteMetric5 (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetInetCidrRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].pOidValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteIfIndex (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteType (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].pOidValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteNextHopAS (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].pOidValue,
                                             pMultiIndex->pIndex[4].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[5].
                                             pOctetStrValue,
                                             pMultiData->u4_ULongValue));

}

INT4
InetCidrRouteMetric1Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteMetric1 (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric2Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteMetric2 (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric3Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteMetric3 (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric4Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteMetric4 (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteMetric5Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteMetric5 (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].pOidValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2InetCidrRouteStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].pOidValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
InetCidrRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2InetCidrRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IpCidrRouteNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhGetIpCidrRouteNumber (&(pMultiData->u4_ULongValue)));
#else
    UNUSED_PARAM (pMultiData);
    return SNMP_FAILURE;
#endif

}

INT4
GetNextIndexIpCidrRouteTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpCidrRouteTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpCidrRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFirstMultiIndex);
    UNUSED_PARAM (pNextMultiIndex);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteAge (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiIndex->pIndex[3].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->pOidValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteMetric2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteMetric3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric4Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteMetric4 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric5Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteMetric5 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIpCidrRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteInfoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->pOidValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric1Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric2Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteMetric2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric3Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteMetric3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric4Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteMetric4 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric5Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteMetric5 (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhSetIpCidrRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteIfIndex (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteType (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteInfoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteInfo (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->pOidValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteNextHopAS (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric1Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteMetric1 (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric2Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteMetric2 (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric3Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteMetric3 (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric4Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteMetric4 (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteMetric5Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteMetric5 (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhTestv2IpCidrRouteStatus (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
#endif

}

INT4
IpCidrRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    return (nmhDepv2IpCidrRouteTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
    return SNMP_SUCCESS;
#endif
}
