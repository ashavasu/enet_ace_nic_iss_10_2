/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipvxlw.c,v 1.32 2013/04/22 12:55:26 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "ipvxinc.h"
#ifdef IP6_WANTED
# include  "fsip6low.h"
#endif
# include  "ipvxdefs.h"
# include  "ipvxext.h"
# include  "cfa.h"
# include  "ipv6.h"
# include  "ip.h"
# include   "cli.h"
# include  "fsipvxcli.h"
# include  "fsmpipvxcli.h"
# include  "ip6util.h"

/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               gau4IpCidrSubnetMask[IP_CFA_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};

/* LOW LEVEL Routines for Table : FsIpvxAddrPrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpvxAddrPrefixTable
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsIpvxAddrPrefixTable
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix, UINT4 u4FsIpvxAddrPrefixLen)
{
    UINT4               u4IpAddress = IPVX_ZERO;

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        /*Validating the interface index */
        if (nmhValidateIndexInstanceIfIpTable
            (i4FsIpvxAddrPrefixIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
#endif
    }
    else if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifdef IP6_WANTED
        /*Validating the interface index */
        if (nmhValidateIndexInstanceFsipv6AddrTable
            (i4FsIpvxAddrPrefixIfIndex,
             pFsIpvxAddrPrefix, u4FsIpvxAddrPrefixLen) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
#endif
    }
    else
    {
        return SNMP_FAILURE;
    }
    /*Validating the IP address and prefix length */
    if ((pFsIpvxAddrPrefix == NULL) || (u4FsIpvxAddrPrefixLen > MAX_MASK_LEN))
    {
        return SNMP_FAILURE;
    }
    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAddress, pFsIpvxAddrPrefix);

        if (u4IpAddress != IPVX_ZERO)
        {
            if (!((CFA_IS_ADDR_CLASS_A (u4IpAddress) ?
                   (IP_IS_ZERO_NETWORK (u4IpAddress) ?
                    IPVX_ZERO : IPVX_ONE) : IPVX_ONE) ||
                  (CFA_IS_ADDR_CLASS_B (u4IpAddress)) ||
                  (CFA_IS_ADDR_CLASS_C (u4IpAddress))))
            {
                return SNMP_FAILURE;
            }
        }
    }
    else if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifdef IP6_WANTED
        if ((IS_ADDR_MULTI (*(tIp6Addr *) (VOID *)
                            pFsIpvxAddrPrefix->pu1_OctetList)) ||
            (IS_ADDR_UNSPECIFIED (*(tIp6Addr *) (VOID *)
                                  pFsIpvxAddrPrefix->pu1_OctetList)))
        {
            return SNMP_FAILURE;
        }
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpvxAddrPrefixTable
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsIpvxAddrPrefixTable
    (INT4 *pi4FsIpvxAddrPrefixIfIndex,
     INT4 *pi4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix, UINT4 *pu4FsIpvxAddrPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1V4RetVal = SNMP_FAILURE;
    INT1                i1V6RetVal = SNMP_FAILURE;
    INT4                i4Ifv4Index = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord         IpIntfInfo;
#endif
#ifdef IP6_WANTED
    INT4                i4Ifv6Index = IPVX_ZERO;
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pIp6AddrInfo = NULL;
#else
    tIp6Addr            Ip6Addr;
    UINT1               u1Prefix;
#endif
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
#if defined IP_WANTED
    i1V4RetVal = nmhGetNextIndexFsIpifTable (IPVX_ZERO, &i4Ifv4Index);
#endif
#if defined LNXIP4_WANTED

    i1V4RetVal = IPvxGetNextIndexIpv4InterfaceTable (IPVX_ZERO, &i4Ifv4Index);
#endif
    if ((i4Ifv4Index != IPVX_ZERO) && (i1V4RetVal != SNMP_FAILURE))
    {
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
        IpIntfInfo.u4IfIndex = (UINT4) i4Ifv4Index;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        if (pIpIntf == NULL)
        {
            i1V4RetVal = SNMP_FAILURE;
        }
        CFA_IPIFTBL_UNLOCK ();
    }

#endif
#ifdef IP6_WANTED
    i1V6RetVal = nmhGetFirstIndexFsipv6IfTable (&i4Ifv6Index);
    if ((i4Ifv6Index != IPVX_ZERO) && (i1V6RetVal != SNMP_FAILURE))
    {
        IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
        pIp6AddrInfo = Ip6AddrTblGetAddrInfo (i4Ifv6Index);
        if (pIp6AddrInfo == NULL)
        {
            i1V6RetVal = SNMP_FAILURE;
        }
#else
        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        if (Ip6AddrTblGetAddrInfo (i4Ifv6Index, &Ip6Addr, &u1Prefix) ==
            SNMP_FAILURE)
        {
            i1V6RetVal = SNMP_FAILURE;
        }
#endif
        IP6_TASK_UNLOCK ();
    }
#endif

    if ((i1V4RetVal == SNMP_FAILURE) && (i1V6RetVal == SNMP_FAILURE))
    {
        i1RetVal = SNMP_FAILURE;
    }
    else if (pIpIntf != NULL)
    {
#ifdef IP6_WANTED
        if (i4Ifv4Index <= i4Ifv6Index)
        {
#endif
            if (i1V4RetVal == SNMP_SUCCESS)
            {
                CFA_IPIFTBL_LOCK ();
                *pi4FsIpvxAddrPrefixIfIndex = i4Ifv4Index;
                INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr, pFsIpvxAddrPrefix);
                *pi4FsIpvxAddrPrefixAddrType = IPV4_ADD_TYPE;
                *pu4FsIpvxAddrPrefixLen = pIpIntf->u1SubnetMask;
                CFA_IPIFTBL_UNLOCK ();
                i1RetVal = SNMP_SUCCESS;
            }
#ifdef IP6_WANTED
        }
#endif
#ifdef IP6_WANTED
        else if ((i4Ifv6Index < i4Ifv4Index) && (i1V6RetVal == SNMP_SUCCESS))
        {
#ifndef LNXIP6_WANTED
            if (pIp6AddrInfo != NULL)
            {
                IP6_TASK_LOCK ();
                *pi4FsIpvxAddrPrefixIfIndex = i4Ifv6Index;
                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pFsIpvxAddrPrefix->pu1_OctetList,
                             &pIp6AddrInfo->ip6Addr);
                pFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                *pu4FsIpvxAddrPrefixLen = pIp6AddrInfo->u1PrefLen;
                *pi4FsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                IP6_TASK_UNLOCK ();
                i1RetVal = SNMP_SUCCESS;
            }
#else
            *pi4FsIpvxAddrPrefixIfIndex = i4Ifv6Index;
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pFsIpvxAddrPrefix->pu1_OctetList,
                         &Ip6Addr);
            pFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
            *pu4FsIpvxAddrPrefixLen = u1Prefix;
            *pi4FsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
            i1RetVal = SNMP_SUCCESS;
#endif

        }
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpvxAddrPrefixTable
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                nextFsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                nextFsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                nextFsIpvxAddrPrefix
                FsIpvxAddrPrefixLen
                nextFsIpvxAddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsIpvxAddrPrefixTable
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 *pi4NextFsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     INT4 *pi4NextFsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     tSNMP_OCTET_STRING_TYPE * pNextFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, UINT4 *pu4NextFsIpvxAddrPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4TmpNextIfIndex = IPVX_ZERO;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pIp6AddrInfo = NULL;
#else
    tIp6Addr            Ip6Addr;
    UINT1               u1Prefix;
#endif
#endif

    UNUSED_PARAM (pFsIpvxAddrPrefix);
    UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        i1RetVal = nmhGetNextIndexIfIpTable
            (i4FsIpvxAddrPrefixIfIndex, pi4NextFsIpvxAddrPrefixIfIndex);
        if ((*pi4NextFsIpvxAddrPrefixIfIndex != IPVX_ZERO) &&
            (i1RetVal != SNMP_FAILURE))
        {
            CFA_IPIFTBL_LOCK ();
            /* Get the IP interface node from RBTree */
            IpIntfInfo.u4IfIndex = *pi4NextFsIpvxAddrPrefixIfIndex;

            pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
            CFA_IPIFTBL_UNLOCK ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            if (i4FsIpvxAddrPrefixIfIndex == *pi4NextFsIpvxAddrPrefixIfIndex)
            {
                if (pIpIntf != NULL)
                {
                    CFA_IPIFTBL_LOCK ();
                    INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr,
                                            pNextFsIpvxAddrPrefix);
                    *pi4NextFsIpvxAddrPrefixAddrType = IPV4_ADD_TYPE;
                    *pu4NextFsIpvxAddrPrefixLen = pIpIntf->u1SubnetMask;
                    CFA_IPIFTBL_UNLOCK ();
                    return SNMP_SUCCESS;

                }

            }
            else
            {
                i4TmpNextIfIndex = *pi4NextFsIpvxAddrPrefixIfIndex;
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                if ((pIp6AddrInfo = Ip6AddrTblGetAddrInfo
                     (i4FsIpvxAddrPrefixIfIndex)) != NULL)
                {
                    IP6_TASK_LOCK ();
                    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                                 pNextFsIpvxAddrPrefix->pu1_OctetList,
                                 &pIp6AddrInfo->ip6Addr);
                    pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                    *pu4NextFsIpvxAddrPrefixLen = pIp6AddrInfo->u1PrefLen;
                    *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                    *pi4NextFsIpvxAddrPrefixIfIndex = i4FsIpvxAddrPrefixIfIndex;
                    IP6_TASK_UNLOCK ();
                    return SNMP_SUCCESS;
                }
#else
                MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                IP6_TASK_LOCK ();
                if (Ip6AddrTblGetAddrInfo (i4FsIpvxAddrPrefixIfIndex,
                                           &Ip6Addr, &u1Prefix) == SNMP_SUCCESS)
                {
                    IP6_TASK_UNLOCK ();
                    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                                 pNextFsIpvxAddrPrefix->pu1_OctetList,
                                 &Ip6Addr);
                    pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                    *pu4NextFsIpvxAddrPrefixLen = u1Prefix;
                    *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                    *pi4NextFsIpvxAddrPrefixIfIndex = i4FsIpvxAddrPrefixIfIndex;
                    return SNMP_SUCCESS;
                }
                IP6_TASK_UNLOCK ();
#endif /* IP_WANTED */
#else
                i1RetVal = SNMP_FAILURE;
#endif
                if ((i4TmpNextIfIndex <=
                     *pi4NextFsIpvxAddrPrefixIfIndex) || (pIpIntf != NULL))
                {
                    if (pIpIntf != NULL)
                    {
                        CFA_IPIFTBL_LOCK ();
                        INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr,
                                                pNextFsIpvxAddrPrefix);
                        *pi4NextFsIpvxAddrPrefixAddrType = IPV4_ADD_TYPE;
                        *pu4NextFsIpvxAddrPrefixLen = pIpIntf->u1SubnetMask;
                        *pi4NextFsIpvxAddrPrefixIfIndex = i4TmpNextIfIndex;
                        CFA_IPIFTBL_UNLOCK ();
                        return SNMP_SUCCESS;

                    }
                }
                else
                {
                    i1RetVal = SNMP_FAILURE;

                }
            }
        }
        else
        {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
            IP6_TASK_LOCK ();
            pIp6AddrInfo = Ip6AddrTblGetAddrInfo (i4FsIpvxAddrPrefixIfIndex);
            IP6_TASK_UNLOCK ();
            if (pIp6AddrInfo != NULL)
            {
                IP6_TASK_LOCK ();
                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsIpvxAddrPrefix->pu1_OctetList,
                             &pIp6AddrInfo->ip6Addr);
                pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                *pu4NextFsIpvxAddrPrefixLen = pIp6AddrInfo->u1PrefLen;
                *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                IP6_TASK_UNLOCK ();
                return (SNMP_SUCCESS);
            }
            else
#else
            MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
            IP6_TASK_LOCK ();
            if (Ip6AddrTblGetAddrInfo (i4FsIpvxAddrPrefixIfIndex,
                                       &Ip6Addr, &u1Prefix) == SNMP_SUCCESS)
            {
                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsIpvxAddrPrefix->pu1_OctetList, &Ip6Addr);
                pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                *pu4NextFsIpvxAddrPrefixLen = u1Prefix;
                *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                *pi4NextFsIpvxAddrPrefixIfIndex = i4FsIpvxAddrPrefixIfIndex;
                IP6_TASK_UNLOCK ();
                return SNMP_SUCCESS;
            }
            IP6_TASK_UNLOCK ();
#endif
            {
                i1RetVal = SNMP_FAILURE;
            }
#endif
        }
#endif
    }
    else if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifdef IP6_WANTED
        i1RetVal = nmhGetNextIndexFsipv6IfTable
            (i4FsIpvxAddrPrefixIfIndex, pi4NextFsIpvxAddrPrefixIfIndex);
#ifndef LNXIP6_WANTED
        IP6_TASK_LOCK ();
        pIp6AddrInfo = Ip6AddrTblGetAddrInfo (*pi4NextFsIpvxAddrPrefixIfIndex);
        if (pIp6AddrInfo == NULL)
        {
            i1RetVal = SNMP_FAILURE;
        }

        IP6_TASK_UNLOCK ();
#else
        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        IP6_TASK_LOCK ();
        if (i1RetVal != SNMP_FAILURE)
        {
            i1RetVal = Ip6AddrTblGetAddrInfo (*pi4NextFsIpvxAddrPrefixIfIndex,
                                              &Ip6Addr, &u1Prefix);
        }
        IP6_TASK_UNLOCK ();
#endif /* IP_WANTED */
#else
        i1RetVal = SNMP_FAILURE;
#endif

        if (i1RetVal == SNMP_SUCCESS)
        {
            if (i4FsIpvxAddrPrefixIfIndex == *pi4NextFsIpvxAddrPrefixIfIndex)
            {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                IP6_TASK_LOCK ();
                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsIpvxAddrPrefix->pu1_OctetList,
                             &pIp6AddrInfo->ip6Addr);
                pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                *pu4NextFsIpvxAddrPrefixLen = pIp6AddrInfo->u1PrefLen;
                *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                IP6_TASK_UNLOCK ();
                return (SNMP_SUCCESS);
#else
                Ip6AddrCopy ((tIp6Addr *) (VOID *)
                             pNextFsIpvxAddrPrefix->pu1_OctetList, &Ip6Addr);
                pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                *pu4NextFsIpvxAddrPrefixLen = u1Prefix;
                *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                *pi4NextFsIpvxAddrPrefixIfIndex = i4FsIpvxAddrPrefixIfIndex;
                return SNMP_SUCCESS;
#endif /* IP_WANTED */
#endif

            }
            else
            {
                i4TmpNextIfIndex = *pi4NextFsIpvxAddrPrefixIfIndex;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
                i1RetVal = nmhGetNextIndexIfIpTable
                    (i4FsIpvxAddrPrefixIfIndex, pi4NextFsIpvxAddrPrefixIfIndex);
                CFA_IPIFTBL_LOCK ();
                /* Get the IP interface node from RBTree */
                IpIntfInfo.u4IfIndex = *pi4NextFsIpvxAddrPrefixIfIndex;

                pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
                CFA_IPIFTBL_UNLOCK ();

#else
                i1RetVal = SNMP_FAILURE;
#endif
                if (pIpIntf != NULL)
                {
                    CFA_IPIFTBL_LOCK ();
                    INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr,
                                            pNextFsIpvxAddrPrefix);
                    *pi4NextFsIpvxAddrPrefixAddrType = IPV4_ADD_TYPE;
                    *pu4NextFsIpvxAddrPrefixLen = pIpIntf->u1SubnetMask;
                    *pi4NextFsIpvxAddrPrefixIfIndex = i4TmpNextIfIndex;
                    CFA_IPIFTBL_UNLOCK ();
                    return SNMP_SUCCESS;

                }
#ifdef IP6_WANTED
                else if ((i1RetVal == SNMP_FAILURE) ||
                         ((i1RetVal == SNMP_SUCCESS) &&
                          (i4TmpNextIfIndex < *pi4NextFsIpvxAddrPrefixIfIndex)))
                {
#ifndef LNXIP6_WANTED
                    if (pIp6AddrInfo != NULL)
                    {
                        IP6_TASK_LOCK ();
                        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                                     pNextFsIpvxAddrPrefix->pu1_OctetList,
                                     &pIp6AddrInfo->ip6Addr);
                        pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                        *pu4NextFsIpvxAddrPrefixLen = pIp6AddrInfo->u1PrefLen;
                        *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                        IP6_TASK_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
#else
                    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                                 pNextFsIpvxAddrPrefix->pu1_OctetList,
                                 &Ip6Addr);
                    pNextFsIpvxAddrPrefix->i4_Length = IPV6_ADD_LEN;
                    *pu4NextFsIpvxAddrPrefixLen = u1Prefix;
                    *pi4NextFsIpvxAddrPrefixAddrType = IPV6_ADD_TYPE;
                    *pi4NextFsIpvxAddrPrefixIfIndex = i4FsIpvxAddrPrefixIfIndex;
                    return SNMP_SUCCESS;
#endif /* IP_WANTED */
                }
#endif
                else
                {
                    i1RetVal = SNMP_FAILURE;
                }
            }
        }
        else
        {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
            i1RetVal = nmhGetNextIndexIfIpTable
                (i4FsIpvxAddrPrefixIfIndex, pi4NextFsIpvxAddrPrefixIfIndex);
            CFA_IPIFTBL_LOCK ();
            /* Get the IP interface node from RBTree */
            IpIntfInfo.u4IfIndex = *pi4NextFsIpvxAddrPrefixIfIndex;

            pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
            CFA_IPIFTBL_UNLOCK ();

#else
            i1RetVal = SNMP_FAILURE;
#endif
            if (i1RetVal == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
            if (pIpIntf != NULL)
            {
                CFA_IPIFTBL_LOCK ();
                INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr,
                                        pNextFsIpvxAddrPrefix);
                *pi4NextFsIpvxAddrPrefixAddrType = IPV4_ADD_TYPE;
                *pu4NextFsIpvxAddrPrefixLen = pIpIntf->u1SubnetMask;
                CFA_IPIFTBL_UNLOCK ();
                return SNMP_SUCCESS;

            }
            else
            {
                i1RetVal = SNMP_FAILURE;
            }
        }
    }
    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                retValFsIpvxAddrPrefixProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsIpvxAddrPrefixProfileIndex
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, UINT4 *pu4RetValFsIpvxAddrPrefixProfileIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#endif
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        /*Profile Index will be always zero for
         *IPv4 address*/
        *pu4RetValFsIpvxAddrPrefixProfileIndex = IPVX_ZERO;
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        i1RetVal = SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifndef LNXIP6_WANTED
        IP6_TASK_LOCK ();
        pAddrInfo = Ip6AddrTblGetEntry
            ((INT2) i4FsIpvxAddrPrefixIfIndex,
             (tIp6Addr *) (VOID *) pFsIpvxAddrPrefix->pu1_OctetList,
             (UINT1) u4FsIpvxAddrPrefixLen);
        if (pAddrInfo != NULL)
        {
            *pu4RetValFsIpvxAddrPrefixProfileIndex = pAddrInfo->u1ProfileIndex;
            i1RetVal = SNMP_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
#else
        /* Profile Index not supported in Linux IP */
        *pu4RetValFsIpvxAddrPrefixProfileIndex = 0;
        UNUSED_PARAM (pAddrInfo);
        i1RetVal = SNMP_SUCCESS;
#endif
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetFsIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                retValFsIpvxAddrPrefixSecAddrFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsIpvxAddrPrefixSecAddrFlag
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 *pi4RetValFsIpvxAddrPrefixSecAddrFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);

        IpIntfInfo.u4IfIndex = i4FsIpvxAddrPrefixIfIndex;
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf != NULL)
        {
            *pi4RetValFsIpvxAddrPrefixSecAddrFlag = pIpIntf->u1SecAddrFlag;
            i1RetVal = SNMP_SUCCESS;
        }
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        /*Secondary address flag is not used by IPv6, Hence, the address
         * is always the primary address */
        *pi4RetValFsIpvxAddrPrefixSecAddrFlag = IPVX_TWO;
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        i1RetVal = SNMP_SUCCESS;

    }
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFsIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                retValFsIpvxAddrPrefixRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsIpvxAddrPrefixRowStatus
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 *pi4RetValFsIpvxAddrPrefixRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#endif
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);

        IpIntfInfo.u4IfIndex = i4FsIpvxAddrPrefixIfIndex;
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Checks whether the entry exists and
         *also check whether the interface index is equal to 33*/
        if (pIpIntf != NULL)
        {
            *pi4RetValFsIpvxAddrPrefixRowStatus =
                (INT4) pIpIntf->u1PrefixRowStatus;
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsIpvxAddrPrefixRowStatus = IPVX_ZERO;
            i1RetVal = SNMP_SUCCESS;

        }
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifndef LNXIP6_WANTED
        IP6_TASK_LOCK ();
        pAddrInfo = Ip6AddrTblGetEntry
            ((INT2) i4FsIpvxAddrPrefixIfIndex,
             (tIp6Addr *) (VOID *) pFsIpvxAddrPrefix->pu1_OctetList,
             (UINT1) u4FsIpvxAddrPrefixLen);
        if (pAddrInfo != NULL)
        {
            *pi4RetValFsIpvxAddrPrefixRowStatus =
                (INT4) pAddrInfo->u1PrefixRowStatus;
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsIpvxAddrPrefixRowStatus = IPVX_ZERO;
            i1RetVal = SNMP_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
#else
        /* Profile Index not supported in Linux IP */
        *pi4RetValFsIpvxAddrPrefixRowStatus = IPVX_ZERO;
        i1RetVal = SNMP_SUCCESS;
#endif /* IP_WANTED */
    }
#endif
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                setValFsIpvxAddrPrefixProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsIpvxAddrPrefixProfileIndex
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, UINT4 u4SetValFsIpvxAddrPrefixProfileIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#endif
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        /*Profile index will not be used for IPv4 entry */
        UNUSED_PARAM (u4SetValFsIpvxAddrPrefixProfileIndex);
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        i1RetVal = SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
#ifndef LNXIP6_WANTED
        IP6_TASK_LOCK ();
        pAddrInfo = Ip6AddrTblGetAddrInfo ((UINT4) i4FsIpvxAddrPrefixIfIndex);
        if (pAddrInfo != NULL)
        {
            pAddrInfo->u1ProfileIndex =
                (UINT1) u4SetValFsIpvxAddrPrefixProfileIndex;
            i1RetVal = SNMP_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
#else
        /* Profile Index not supported for Linux IP */
        i1RetVal = SNMP_SUCCESS;
#endif /* IP_WANTED */
    }
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForAddrPrefixTable (i4FsIpvxAddrPrefixIfIndex,
                                  i4FsIpvxAddrPrefixAddrType,
                                  pFsIpvxAddrPrefix,
                                  u4FsIpvxAddrPrefixLen,
                                  u4SetValFsIpvxAddrPrefixProfileIndex,
                                  FsMIIpvxAddrPrefixProfileIndex,
                                  (sizeof (FsMIIpvxAddrPrefixProfileIndex) /
                                   sizeof (UINT4)), FALSE);
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                setValFsIpvxAddrPrefixSecAddrFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsIpvxAddrPrefixSecAddrFlag
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 i4SetValFsIpvxAddrPrefixSecAddrFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        IpIntfInfo.u4IfIndex = i4FsIpvxAddrPrefixIfIndex;
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf != NULL)
        {
            pIpIntf->u1SecAddrFlag =
                (UINT1) i4SetValFsIpvxAddrPrefixSecAddrFlag;
            i1RetVal = SNMP_SUCCESS;
        }
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        /*Secondary flag is not used for IPv6 entry */
        i1RetVal = SNMP_SUCCESS;
    }
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForAddrPrefixTable (i4FsIpvxAddrPrefixIfIndex,
                                  i4FsIpvxAddrPrefixAddrType,
                                  pFsIpvxAddrPrefix,
                                  u4FsIpvxAddrPrefixLen,
                                  i4SetValFsIpvxAddrPrefixSecAddrFlag,
                                  FsMIIpvxAddrPrefixSecAddrFlag,
                                  (sizeof (FsMIIpvxAddrPrefixSecAddrFlag) /
                                   sizeof (UINT4)), FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetFsIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                setValFsIpvxAddrPrefixRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsIpvxAddrPrefixRowStatus
    (INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 i4SetValFsIpvxAddrPrefixRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    INT1                i1AddrVal = IPVX_ZERO;
    UINT1               u1AddrRowStatus = IPVX_ZERO;
    UINT1               u1PrefixRowStatus = IPVX_ZERO;
    UINT4               u4IpAddr = IPVX_ZERO;
    UINT4               u4IfIpAddr = IPVX_ZERO;
    UINT4               u4BcastAddr = IPVX_ZERO;
    UINT4               u4SubnetMask = IPVX_ZERO;
    UINT4               u4PrefixLen = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
    tIpIfRecord         IpIntfInfo;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        /* We should allow the creation of the row in 
         * prefix table only when the interafce is present*/
        IpIntfInfo.u4IfIndex = i4FsIpvxAddrPrefixIfIndex;

        CFA_IPIFTBL_LOCK ();
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the interface exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        u4IfIpAddr = pIpIntf->u4IpAddr;
        u1AddrRowStatus = pIpIntf->u1AddrRowStatus;
        u1PrefixRowStatus = pIpIntf->u1PrefixRowStatus;
        CFA_IPIFTBL_UNLOCK ();

        switch (i4SetValFsIpvxAddrPrefixRowStatus)
        {
            case IPVX_CREATE_AND_GO:
            case IPVX_CREATE_AND_WAIT:
                IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAddr, pFsIpvxAddrPrefix);

                u4PrefixLen = gau4IpCidrSubnetMask[u4FsIpvxAddrPrefixLen];
                /*If IP address is not configured via IP address table
                 *i.e if the Address table is not present,dont allow the
                 *creation of the corresponding Prefix table*/
                if (u1AddrRowStatus == IPVX_ZERO)
                {
                    return SNMP_FAILURE;
                }
                /*If IP address is already configured by the IP address
                 *table configure the Subnet Mask and Broadcast address*/
                else if (u4IfIpAddr == u4IpAddr)
                {
                    if (CfaIPVXSetIfIpSubnetMask
                        (i4FsIpvxAddrPrefixIfIndex, u4PrefixLen) == CFA_FAILURE)
                    {
                        return (SNMP_FAILURE);
                    }

                    u4BcastAddr = u4IpAddr | (~(u4PrefixLen));

                    if (CfaIPVXSetIfIpBroadcastAddr
                        (i4FsIpvxAddrPrefixIfIndex, u4BcastAddr) == CFA_FAILURE)
                    {
                        return (SNMP_FAILURE);
                    }
                }
                else
                {
                    return SNMP_FAILURE;
                }
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     IPVX_NOT_IN_SERVICE,
                     (UINT1) i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            case IPVX_ACTIVE:

                /*Allow the Rowstatus to make active only when rowstatus
                   is in not-in-service state */
                if (u1PrefixRowStatus != IPVX_NOT_IN_SERVICE)
                {
                    return SNMP_FAILURE;
                }

                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) & pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            case IPVX_NOT_IN_SERVICE:
            case IPVX_NOT_READY:
                /*Allow the Rowstatus to make not-in-service only when it
                   is in active state */
                if (u1PrefixRowStatus != IPVX_ACTIVE)
                {
                    return SNMP_FAILURE;
                }

                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;
            case IPVX_DESTROY:

                /*If the Address table entry is not present dont proceed as the
                 *prefix table is already deleted when the Address table is
                 *deleted*/
                if (u1AddrRowStatus == IPVX_ZERO)
                {
                    return SNMP_FAILURE;
                }
                else
                {
                    IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAddr,
                                                      pFsIpvxAddrPrefix);
                    i1AddrVal = CFA_IS_ADDR_CLASS_A (u4IpAddr) ?
                        IPVX_ONE : IPVX_ZERO;
                    if (i1AddrVal)
                    {
                        i1AddrVal = IP_IS_ZERO_NETWORK (u4IpAddr) ?
                            IPVX_ZERO : IPVX_ONE;
                    }
                    if (i1AddrVal)
                    {
                        u4SubnetMask = CLASS_A_DEF_MASK;

                    }
                    else if (CFA_IS_ADDR_CLASS_B (u4IpAddr))
                    {
                        u4SubnetMask = CLASS_B_DEF_MASK;
                    }
                    else if (CFA_IS_ADDR_CLASS_B (u4IpAddr))
                    {
                        u4SubnetMask = CLASS_C_DEF_MASK;
                    }
                    u4SubnetMask = gau4IpCidrSubnetMask[u4SubnetMask];
                    if (CfaIPVXSetIpAddress
                        (i4FsIpvxAddrPrefixIfIndex,
                         u4IpAddr, u4SubnetMask) != CFA_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    u4BcastAddr = u4IpAddr | (~(u4SubnetMask));

                    if (CfaIPVXSetIfIpBroadcastAddr
                        (i4FsIpvxAddrPrefixIfIndex, u4BcastAddr) == CFA_FAILURE)
                    {
                        return (SNMP_FAILURE);
                    }
                }
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            default:
                return SNMP_FAILURE;
        }
        IncMsrForAddrPrefixTable (i4FsIpvxAddrPrefixIfIndex,
                                  i4FsIpvxAddrPrefixAddrType,
                                  pFsIpvxAddrPrefix,
                                  u4FsIpvxAddrPrefixLen,
                                  i4SetValFsIpvxAddrPrefixRowStatus,
                                  FsMIIpvxAddrPrefixRowStatus,
                                  (sizeof (FsMIIpvxAddrPrefixRowStatus) /
                                   sizeof (UINT4)), TRUE);
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        switch (i4SetValFsIpvxAddrPrefixRowStatus)
        {
            case IPVX_CREATE_AND_GO:
            case IPVX_CREATE_AND_WAIT:
                IPvxSetIPv6Addr (i4FsIpvxAddrPrefixIfIndex,
                                 *(tIp6Addr *) (VOID *)
                                 pFsIpvxAddrPrefix->pu1_OctetList,
                                 u4FsIpvxAddrPrefixLen, ADDR6_UNICAST);
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            case IPVX_ACTIVE:
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            case IPVX_NOT_IN_SERVICE:
            case IPVX_NOT_READY:
                /*Support is not provided in IPv6 module for NOT_IN_SERVICE 
                 *and NOT_READY*/
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) & pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                break;

            case IPVX_DESTROY:
                if (SetPrefixTableRowStatus
                    (i4FsIpvxAddrPrefixIfIndex,
                     (UINT1) i4SetValFsIpvxAddrPrefixRowStatus,
                     i4FsIpvxAddrPrefixAddrType,
                     *(tIp6Addr *) & pFsIpvxAddrPrefix,
                     (UINT1) u4FsIpvxAddrPrefixLen) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                IPvxDeleteIPv6Add ((UINT4) i4FsIpvxAddrPrefixIfIndex,
                                   *(tIp6Addr *) (VOID *)
                                   pFsIpvxAddrPrefix->pu1_OctetList,
                                   (UINT1) u4FsIpvxAddrPrefixLen,
                                   ADDR6_UNICAST);
                break;

            default:
                return SNMP_FAILURE;
        }

    }
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpvxAddrPrefixProfileIndex
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                testValFsIpvxAddrPrefixProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsIpvxAddrPrefixProfileIndex
    (UINT4 *pu4ErrorCode,
     INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, UINT4 u4TestValFsIpvxAddrPrefixProfileIndex)
{
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#else
    tIp6Addr            Ip6Addr;
    UINT1               u1Prefix;
#endif
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        /*Profile index will not be used for IPv4 entry */
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (u4TestValFsIpvxAddrPrefixProfileIndex);
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    UNUSED_PARAM (u4TestValFsIpvxAddrPrefixProfileIndex);
    UNUSED_PARAM (pFsIpvxAddrPrefix);
    UNUSED_PARAM (u4FsIpvxAddrPrefixLen);

    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
        pAddrInfo = Ip6AddrTblGetAddrInfo ((UINT4) i4FsIpvxAddrPrefixIfIndex);
        if (pAddrInfo == NULL)
#else
        if (Ip6AddrTblGetAddrInfo (i4FsIpvxAddrPrefixIfIndex,
                                   &Ip6Addr, &u1Prefix) == SNMP_FAILURE)
#endif
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            IP6_TASK_UNLOCK ();
            return SNMP_FAILURE;
        }
        IP6_TASK_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpvxAddrPrefixSecAddrFlag
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                testValFsIpvxAddrPrefixSecAddrFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsIpvxAddrPrefixSecAddrFlag
    (UINT4 *pu4ErrorCode,
     INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 i4TestValFsIpvxAddrPrefixSecAddrFlag)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))

    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);

        if ((i4TestValFsIpvxAddrPrefixSecAddrFlag != IPVX_ONE) &&
            (i4TestValFsIpvxAddrPrefixSecAddrFlag != IPVX_TWO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        /*Secondary flag is not used for IPv6 entry */
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (i4TestValFsIpvxAddrPrefixSecAddrFlag);
        UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
        UNUSED_PARAM (pFsIpvxAddrPrefix);
        UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpvxAddrPrefixRowStatus
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen

                The Object 
                testValFsIpvxAddrPrefixRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsIpvxAddrPrefixRowStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsIpvxAddrPrefixIfIndex,
     INT4 i4FsIpvxAddrPrefixAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
     UINT4 u4FsIpvxAddrPrefixLen, INT4 i4TestValFsIpvxAddrPrefixRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4IPAddr = IPVX_ZERO;
#endif

#ifdef IP6_WANTED
    tIp6Addr            ip6addr;
    MEMSET (&ip6addr, IPVX_ZERO, sizeof (tIp6Addr));
#endif

    UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
    UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);

    if (i4FsIpvxAddrPrefixIfIndex < IPVX_STARTING_IFINDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsIpvxAddrPrefixRowStatus <= IPVX_ZERO) ||
        (i4TestValFsIpvxAddrPrefixRowStatus > IPVX_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4FsIpvxAddrPrefixAddrType == IPV4_ADD_TYPE)
    {
        /*Check for the Valid IPv4 Address */
        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IPAddr, pFsIpvxAddrPrefix);

        if (u4IPAddr != IPVX_ZERO)
        {
            if (!((CFA_IS_ADDR_CLASS_A (u4IPAddr) ?
                   (IP_IS_ZERO_NETWORK (u4IPAddr) ?
                    IPVX_ZERO : IPVX_ONE) : IPVX_ZERO) ||
                  (CFA_IS_ADDR_CLASS_B (u4IPAddr)) ||
                  (CFA_IS_ADDR_CLASS_C (u4IPAddr))))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4FsIpvxAddrPrefixAddrType == IPV6_ADD_TYPE)
    {
        MEMSET (&ip6addr, IPVX_ZERO, sizeof (tIp6Addr));
        Ip6AddrCopy (&ip6addr, (tIp6Addr *) (VOID *)
                     pFsIpvxAddrPrefix->pu1_OctetList);
        /* Validate the Address. */
        if ((IS_ADDR_MULTI (ip6addr)) ||
            (IS_ADDR_UNSPECIFIED (ip6addr)) ||
            (IS_ADDR_LOOPBACK (ip6addr)) || (IS_ADDR_V4_COMPAT (ip6addr)))
        {
            /* Invalid Address. */
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        /* Validate the Address Prefix Length */
        if (u4FsIpvxAddrPrefixLen > IP6_ADDR_MAX_PREFIX)
        {
            /* Invalid Address Prefix Length */
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpvxAddrPrefixTable
 Input       :  The Indices
                FsIpvxAddrPrefixIfIndex
                FsIpvxAddrPrefixAddrType
                FsIpvxAddrPrefix
                FsIpvxAddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpvxAddrPrefixTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpv6IfIcmpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpv6IfIcmpTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIpv6IfIcmpTable (INT4 i4Ipv6InterfaceIfIndex)
{
#ifdef IP6_WANTED
    if (Ip6ifEntryExists ((UINT4) i4Ipv6InterfaceIfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpv6IfIcmpTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIpv6IfIcmpTable (INT4 *pi4Ipv6InterfaceIfIndex)
{
#ifdef IP6_WANTED
    *pi4Ipv6InterfaceIfIndex = 0;

    if (Ip6ifEntryExists (PTR_TO_U4 (pi4Ipv6InterfaceIfIndex) == IP6_SUCCESS))
    {
        if (Ip6ifGetNextIndex ((UINT4 *) pi4Ipv6InterfaceIfIndex) ==
            IP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pi4Ipv6InterfaceIfIndex);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpv6IfIcmpTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
                nextIpv6InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpv6IfIcmpTable (INT4 i4Ipv6InterfaceIfIndex,
                                  INT4 *pi4NextIpv6InterfaceIfIndex)
{
#ifdef IP6_WANTED
    if (Ip6ifGetNextIndex ((UINT4 *) &i4Ipv6InterfaceIfIndex) == IP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIpv6InterfaceIfIndex = i4Ipv6InterfaceIfIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pi4NextIpv6InterfaceIfIndex);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInMsgs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInMsgs (INT4 i4Ipv6InterfaceIfIndex,
                          UINT4 *pu4RetValFsIpv6IfIcmpInMsgs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInMsgs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInMsgs = pIf6->pIfIcmp6Stats->u4InMsgs;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInMsgs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInErrors
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInErrors (INT4 i4Ipv6InterfaceIfIndex,
                            UINT4 *pu4RetValFsIpv6IfIcmpInErrors)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInErrors = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInErrors = pIf6->pIfIcmp6Stats->u4InErrs;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInErrors);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInDestUnreachs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInDestUnreachs (INT4 i4Ipv6InterfaceIfIndex,
                                  UINT4 *pu4RetValFsIpv6IfIcmpInDestUnreachs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInDestUnreachs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInDestUnreachs =
                pIf6->pIfIcmp6Stats->u4InDstUnreach;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInDestUnreachs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInAdminProhibs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInAdminProhibs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInAdminProhibs (INT4 i4Ipv6InterfaceIfIndex,
                                  UINT4 *pu4RetValFsIpv6IfIcmpInAdminProhibs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInAdminProhibs =
                pIf6->pIfIcmp6Stats->u4InDstUnreach;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInAdminProhibs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInTimeExcds
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInTimeExcds (INT4 i4Ipv6InterfaceIfIndex,
                               UINT4 *pu4RetValFsIpv6IfIcmpInTimeExcds)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInTimeExcds = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInTimeExcds =
                pIf6->pIfIcmp6Stats->u4InTmexceeded;
            return (SNMP_SUCCESS);
        }

    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInTimeExcds);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInParmProblems
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInParmProblems
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInParmProblems (INT4 i4Ipv6InterfaceIfIndex,
                                  UINT4 *pu4RetValFsIpv6IfIcmpInParmProblems)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInParmProblems = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInParmProblems =
                pIf6->pIfIcmp6Stats->u4InParamprob;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInParmProblems);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInPktTooBigs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInPktTooBigs (INT4 i4Ipv6InterfaceIfIndex,
                                UINT4 *pu4RetValFsIpv6IfIcmpInPktTooBigs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInPktTooBigs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInPktTooBigs =
                pIf6->pIfIcmp6Stats->u4InToobig;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInPktTooBigs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInEchos
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInEchos (INT4 i4Ipv6InterfaceIfIndex,
                           UINT4 *pu4RetValFsIpv6IfIcmpInEchos)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInEchos = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInEchos = pIf6->pIfIcmp6Stats->u4InEchoReq;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInEchos);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInEchoReplies
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInEchoReplies
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInEchoReplies (INT4 i4Ipv6InterfaceIfIndex,
                                 UINT4 *pu4RetValFsIpv6IfIcmpInEchoReplies)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInEchoReplies = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInEchoReplies =
                pIf6->pIfIcmp6Stats->u4InEchoResp;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInEchoReplies);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInRouterSolicits
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInRouterSolicits (INT4 i4Ipv6InterfaceIfIndex,
                                    UINT4
                                    *pu4RetValFsIpv6IfIcmpInRouterSolicits)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInRouterSolicits = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInRouterSolicits =
                pIf6->pIfIcmp6Stats->u4InRouterSol;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInRouterSolicits);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInRouterAdvertisements
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInRouterAdvertisements (INT4 i4Ipv6InterfaceIfIndex,
                                          UINT4
                                          *pu4RetValFsIpv6IfIcmpInRouterAdvertisements)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInRouterAdvertisements = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInRouterAdvertisements =
                pIf6->pIfIcmp6Stats->u4InRouterAdv;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInRouterAdvertisements);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInNeighborSolicits
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInNeighborSolicits (INT4 i4Ipv6InterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIpv6IfIcmpInNeighborSolicits)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInNeighborSolicits = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInNeighborSolicits =
                pIf6->pIfIcmp6Stats->u4InNeighSol;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInNeighborSolicits);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInNeighborAdvertisements
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInNeighborAdvertisements (INT4 i4Ipv6InterfaceIfIndex,
                                            UINT4
                                            *pu4RetValFsIpv6IfIcmpInNeighborAdvertisements)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInNeighborAdvertisements = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInNeighborAdvertisements =
                pIf6->pIfIcmp6Stats->u4InNeighAdv;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInNeighborAdvertisements);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInRedirects
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInRedirects (INT4 i4Ipv6InterfaceIfIndex,
                               UINT4 *pu4RetValFsIpv6IfIcmpInRedirects)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInRedirects = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInRedirects = pIf6->pIfIcmp6Stats->u4InRedir;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInRedirects);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInGroupMembQueries
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInGroupMembQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInGroupMembQueries (INT4 i4Ipv6InterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIpv6IfIcmpInGroupMembQueries)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInGroupMembQueries = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInGroupMembQueries =
                pIf6->pIfIcmp6Stats->u4InMLDQuery;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInGroupMembQueries);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInGroupMembResponses
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInGroupMembResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInGroupMembResponses (INT4 i4Ipv6InterfaceIfIndex,
                                        UINT4
                                        *pu4RetValFsIpv6IfIcmpInGroupMembResponses)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInGroupMembResponses = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInGroupMembResponses =
                pIf6->pIfIcmp6Stats->u4InMLDReport;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInGroupMembResponses);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpInGroupMembReductions
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpInGroupMembReductions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpInGroupMembReductions (INT4 i4Ipv6InterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIpv6IfIcmpInGroupMembReductions)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpInGroupMembReductions = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpInGroupMembReductions =
                pIf6->pIfIcmp6Stats->u4InMLDDone;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpInGroupMembReductions);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutMsgs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutMsgs (INT4 i4Ipv6InterfaceIfIndex,
                           UINT4 *pu4RetValFsIpv6IfIcmpOutMsgs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutMsgs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutMsgs = pIf6->pIfIcmp6Stats->u4OutMsgs;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutMsgs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutErrors
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutErrors (INT4 i4Ipv6InterfaceIfIndex,
                             UINT4 *pu4RetValFsIpv6IfIcmpOutErrors)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutErrors = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutErrors = pIf6->pIfIcmp6Stats->u4OutErrs;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutErrors);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutDestUnreachs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutDestUnreachs (INT4 i4Ipv6InterfaceIfIndex,
                                   UINT4 *pu4RetValFsIpv6IfIcmpOutDestUnreachs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutDestUnreachs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutDestUnreachs =
                pIf6->pIfIcmp6Stats->u4OutDstUnreach;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutDestUnreachs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutAdminProhibs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutAdminProhibs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutAdminProhibs (INT4 i4Ipv6InterfaceIfIndex,
                                   UINT4 *pu4RetValFsIpv6IfIcmpOutAdminProhibs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutAdminProhibs =
                pIf6->pIfIcmp6Stats->u4OutDstUnreach;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutAdminProhibs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutTimeExcds
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutTimeExcds (INT4 i4Ipv6InterfaceIfIndex,
                                UINT4 *pu4RetValFsIpv6IfIcmpOutTimeExcds)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutTimeExcds = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutTimeExcds =
                pIf6->pIfIcmp6Stats->u4OutTmexceeded;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutTimeExcds);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutParmProblems
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutParmProblems
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutParmProblems (INT4 i4Ipv6InterfaceIfIndex,
                                   UINT4 *pu4RetValFsIpv6IfIcmpOutParmProblems)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutParmProblems = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutParmProblems =
                pIf6->pIfIcmp6Stats->u4OutParamprob;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutParmProblems);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutPktTooBigs
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutPktTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutPktTooBigs (INT4 i4Ipv6InterfaceIfIndex,
                                 UINT4 *pu4RetValFsIpv6IfIcmpOutPktTooBigs)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutPktTooBigs = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutPktTooBigs =
                pIf6->pIfIcmp6Stats->u4OutToobig;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutPktTooBigs);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutEchos
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutEchos (INT4 i4Ipv6InterfaceIfIndex,
                            UINT4 *pu4RetValFsIpv6IfIcmpOutEchos)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutEchos = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutEchos = pIf6->pIfIcmp6Stats->u4OutEchoReq;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutEchos);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutEchoReplies
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutEchoReplies
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutEchoReplies (INT4 i4Ipv6InterfaceIfIndex,
                                  UINT4 *pu4RetValFsIpv6IfIcmpOutEchoReplies)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutEchoReplies = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutEchoReplies =
                pIf6->pIfIcmp6Stats->u4OutEchoResp;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutEchoReplies);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutRouterSolicits
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutRouterSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutRouterSolicits (INT4 i4Ipv6InterfaceIfIndex,
                                     UINT4
                                     *pu4RetValFsIpv6IfIcmpOutRouterSolicits)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutRouterSolicits = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutRouterSolicits =
                pIf6->pIfIcmp6Stats->u4OutRouterSol;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutRouterSolicits);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutRouterAdvertisements
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutRouterAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutRouterAdvertisements (INT4 i4Ipv6InterfaceIfIndex,
                                           UINT4
                                           *pu4RetValFsIpv6IfIcmpOutRouterAdvertisements)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutRouterAdvertisements = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutRouterAdvertisements =
                pIf6->pIfIcmp6Stats->u4OutRouterAdv;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutRouterAdvertisements);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutNeighborSolicits
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutNeighborSolicits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutNeighborSolicits (INT4 i4Ipv6InterfaceIfIndex,
                                       UINT4
                                       *pu4RetValFsIpv6IfIcmpOutNeighborSolicits)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutNeighborSolicits = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutNeighborSolicits =
                pIf6->pIfIcmp6Stats->u4OutNeighSol;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutNeighborSolicits);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutNeighborAdvertisements
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutNeighborAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutNeighborAdvertisements (INT4 i4Ipv6InterfaceIfIndex,
                                             UINT4
                                             *pu4RetValFsIpv6IfIcmpOutNeighborAdvertisements)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutNeighborAdvertisements = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutNeighborAdvertisements =
                pIf6->pIfIcmp6Stats->u4OutNeighAdv;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutNeighborAdvertisements);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutRedirects
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutRedirects (INT4 i4Ipv6InterfaceIfIndex,
                                UINT4 *pu4RetValFsIpv6IfIcmpOutRedirects)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutRedirects = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutRedirects =
                pIf6->pIfIcmp6Stats->u4OutRedir;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutRedirects);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutGroupMembQueries
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutGroupMembQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutGroupMembQueries (INT4 i4Ipv6InterfaceIfIndex,
                                       UINT4
                                       *pu4RetValFsIpv6IfIcmpOutGroupMembQueries)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutGroupMembQueries = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutGroupMembQueries =
                pIf6->pIfIcmp6Stats->u4OutMLDQuery;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutGroupMembQueries);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutGroupMembResponses
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutGroupMembResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutGroupMembResponses (INT4 i4Ipv6InterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIpv6IfIcmpOutGroupMembResponses)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutGroupMembResponses = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutGroupMembResponses =
                pIf6->pIfIcmp6Stats->u4OutMLDReport;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutGroupMembResponses);
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIpv6IfIcmpOutGroupMembReductions
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValFsIpv6IfIcmpOutGroupMembReductions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpv6IfIcmpOutGroupMembReductions (INT4 i4Ipv6InterfaceIfIndex,
                                          UINT4
                                          *pu4RetValFsIpv6IfIcmpOutGroupMembReductions)
{
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    *pu4RetValFsIpv6IfIcmpOutGroupMembReductions = 0;

    if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
    {
        pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
        if (pIf6 == NULL)
        {
            return (SNMP_FAILURE);
        }
        if (pIf6->pIfIcmp6Stats != NULL)
        {
            *pu4RetValFsIpv6IfIcmpOutGroupMembReductions =
                pIf6->pIfIcmp6Stats->u4OutMLDDone;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValFsIpv6IfIcmpOutGroupMembReductions);
    return (SNMP_SUCCESS);
#endif
}
