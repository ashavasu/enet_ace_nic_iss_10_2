/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: ipvxutil.c,v 1.22 2016/02/27 10:14:54 siva Exp $
*
* Description: IPvx utility functions are present in this file.
*********************************************************************/
#ifndef __IPVX_UTIL_C
#define __IPVX_UTIL_C

# include  "ipvxinc.h"
# include  "ipvxdefs.h"
# include  "ipvxext.h"
# include  "trie.h"
# include  "ipv6.h"
# include  "rtm6.h"
# include  "fsipvxcli.h"
# include  "rmgr.h"
# include  "vcm.h"
# include  "utilipvx.h"
# include  "ip6util.h"

extern tOsixSemId   gNetIpvxSem;
PRIVATE INT4 IpvxVcmIsVRExists PROTO ((UINT4));
/*
PRIVATE VOID IpvxClearAddressTable (UINT4 u4ContextId);
*/
PRIVATE VOID        IpvxClearNetToPhysicalTable (UINT4 u4IfIndex);
PRIVATE VOID        IpvxClearIp6RouterAdvtTable (UINT4 u4IfIndex);
PRIVATE VOID        IpvxClearIpv4InterfaceTable (UINT4 u4IfIndex);
PRIVATE VOID        IpvxClearIpv6InterfaceTable (UINT4 u4IfIndex);
PRIVATE VOID        IpvxClearAddrPrefixTable (UINT4 u4IfIndex);

#ifdef IP6_WANTED
extern INT1 Ip6UtilClearIpv6InterfaceTable PROTO ((INT4));
#endif

/****************************************************************************
 Function    :  SetPrefixTableRowStatus
 Input       :  pi4IfIndex   - Pointer to Interface index
                i1RowStatus  - RowStatus of the prefix table
                i4AddType    - IPv4 or IPv6 address type.
 Description :  This function sets the rowstatus of the prefix table.
 Output      :  None.
 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
SetPrefixTableRowStatus (INT4 i4PrefixIfIndex, UINT1 i1RowStatus,
                         INT4 i4AddType, tIp6Addr IpvxAddrPrefix,
                         UINT1 u1IpvxAddrPrefixLen)
{
    INT1                i1RetVal = IPVX_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#endif
#endif

    UNUSED_PARAM (IpvxAddrPrefix);
    UNUSED_PARAM (u1IpvxAddrPrefixLen);

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4AddType == IPV4_ADD_TYPE)
    {
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4PrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf != NULL)
        {
            pIpIntf->u1PrefixRowStatus = i1RowStatus;
            /*Set the IP address origin type as manual
             *only when rowstatus is active*/
            if (i1RowStatus == IPVX_ACTIVE)
            {
                pIpIntf->u1Origin = IPVX_MANUAL;
            }
            /*If the rowstatus is destroy set the
             * profile table rowstatus as zero*/
            if (i1RowStatus == IPVX_DESTROY)
            {
                pIpIntf->u1PrefixRowStatus = IPVX_ZERO;
                pIpIntf->u1SecAddrFlag = IPVX_ZERO;
            }
            i1RetVal = IPVX_SUCCESS;
        }
        CFA_IPIFTBL_UNLOCK ();
    }
#endif

#ifdef IP6_WANTED
    if (i4AddType == IPV6_ADD_TYPE)
    {
#ifndef LNXIP6_WANTED
        IP6_TASK_LOCK ();
        pAddrInfo = Ip6AddrTblGetAddrInfo ((UINT4) i4PrefixIfIndex);
        if (pAddrInfo != NULL)
        {
            pAddrInfo->u1PrefixRowStatus = i1RowStatus;
            /*Set the IP address origin type as manual
             * only when rowstatus is active*/
            if (i1RowStatus == IPVX_ACTIVE)
            {
                pAddrInfo->u1Origin = IPVX_MANUAL;
            }
            if (i1RowStatus == IPVX_DESTROY)
            {
                pAddrInfo->u1ProfileIndex = IPVX_ZERO;
                pAddrInfo->u1PrefixRowStatus = IPVX_ZERO;
            }
            i1RetVal = IPVX_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
#else
        i1RetVal = IPVX_SUCCESS;
#endif /* IP_WANTED */
    }
#endif
    return (i1RetVal);;
}

/****************************************************************************
 Function    :  SetAddrTableRowStatus
 Input       :  pi4IfIndex   - Pointer to Interface index
                i1RowStatus  - RowStatus of the prefix table
                i4AddType    - IPv4 or IPv6 address type.
 Description :  This function sets the rowstatus of the prefix table.
 Output      :  None.
 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
SetAddrTableRowStatusInCxt (UINT4 u4ContextId, INT1 i1RowStatus, INT4 i4AddType,
                            tSNMP_OCTET_STRING_TYPE * pIpvxAddrPrefix)
{
    UINT4               u4IpLastChanged = IPVX_ZERO;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4AddType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4ContextId, pIpvxAddrPrefix);
        CFA_IPIFTBL_LOCK ();
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (IPVX_FAILURE);
        }
        MEMCPY (&pIpIntf->u1AddrRowStatus, (UINT1 *) &i1RowStatus,
                sizeof (UINT1));
        if (i1RowStatus == IPVX_ACTIVE)
        {
            pIpIntf->u1Origin = IPVX_MANUAL;
        }
        if (i1RowStatus == IPVX_NOT_IN_SERVICE)
        {
            u4IpLastChanged = OsixGetSysUpTime ();
            pIpIntf->u4IpLastChanged = u4IpLastChanged;
        }
        CFA_IPIFTBL_UNLOCK ();
        return (IPVX_SUCCESS);
    }
#endif
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    if (i4AddType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        pInfo = Ip6AddrAllIfScanInCxt (u4ContextId, (tIp6Addr *) (VOID *)
                                       pIpvxAddrPrefix->pu1_OctetList,
                                       IPVX_ZERO);
        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (IPVX_FAILURE);
        }
        MEMCPY (&pInfo->u1AddrRowStatus, (UINT1 *) &i1RowStatus,
                sizeof (UINT1));
        /*Set the IP address origin type as manual
         * only when rowstatus is active*/
        if (i1RowStatus == IPVX_ACTIVE)
        {
            pInfo->u1Origin = IPVX_MANUAL;
        }
        if (i1RowStatus == IPVX_NOT_IN_SERVICE)
        {
            u4IpLastChanged = OsixGetSysUpTime ();
            pInfo->u4IpLastChanged = u4IpLastChanged;
        }
        IP6_TASK_UNLOCK ();
        return (IPVX_SUCCESS);

    }
#endif
    return IPVX_FAILURE;
}

/****************************************************************************
 Function    :  FindIPv4AddrTableRowInCxt
 Input       :  u4ContextId - The context identifier
                pIpAddr   - Pointer to IP address
 Description :  This function gets the row for the corresponding IPv4 address
                in the specified context.
 Output      :  None.
 Returns     :  NULL or Pointer to the tIpIfRecord block
****************************************************************************/
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
tIpIfRecord        *
FindIPv4AddrTableRowInCxt (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pIpAddr)
{
    tIpIfRecord        *pIpIntf = NULL;
    UINT4               u4IpAdd = IPVX_ZERO;
    UINT4               u4CxtId = 0;

    CFA_IPIFTBL_LOCK ();
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    do
    {
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (NULL);
        }

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAdd, pIpAddr);

        if (VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4CxtId)
            == VCM_FAILURE)
        {
            CFA_IPIFTBL_UNLOCK ();
            return NULL;
        }
        if ((pIpIntf->u4IpAddr == u4IpAdd) && (u4ContextId == u4CxtId))
        {
            CFA_IPIFTBL_UNLOCK ();
            return (pIpIntf);
        }
    }
    while ((pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf,
                                                     NULL)) != NULL);
    CFA_IPIFTBL_UNLOCK ();
    return (pIpIntf);
}
#endif
#if defined IP6_WANTED && !defined LNXIP6_WANTED
/****************************************************************************
 Function    :  FindIPv6AddrTableRowInCxt
 Input       :  u4ContextId - Context identifier
                pIpAddr   - Pointer to IP address
 Description :  This function gets the row for the corresponding IPv6 address.
 Output      :  None.
 Returns     :  NULL or Pointer to the tIp6AddrInfo block
****************************************************************************/
tIp6AddrInfo       *
FindIPv6AddrTableRowInCxt (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                           UINT4 *pIfIndex)
{
    tIp6AddrInfo       *pInfo = NULL;

    IP6_TASK_LOCK ();
    pInfo = Ip6AddrAllIfScanInCxt (u4ContextId, (tIp6Addr *) (VOID *)
                                   pIpAddr->pu1_OctetList, IPVX_ZERO);
    if (pInfo == NULL)
    {
        IP6_TASK_UNLOCK ();
        return (NULL);
    }
    *pIfIndex = pInfo->pIf6->u4Index;
    IP6_TASK_UNLOCK ();
    return (pInfo);
}
#endif

/****************************************************************************
 Function    :  IncMsrForAddrPrefixTable

 Input       :  i4FsIpvxAddrPrefixIfIndex - Interface index.
                i4FsIpvxAddrPrefixAddrType - Address type.
                pFsIpvxAddrPrefix - IP address.
                u4FsIpvxAddrPrefixLen - IP address length.
                i4SetValFsIpvxAddrPrefix - Value which should be stored.
                pu4ObjectId - Object ID
                IsRowStatus - Rowstatus

 Description :  This function performs Inc Msr functionality for 
                fsipvx Prefix table.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForAddrPrefixTable (INT4 i4FsIpvxAddrPrefixIfIndex,
                          INT4 i4FsIpvxAddrPrefixAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
                          UINT4 u4FsIpvxAddrPrefixLen,
                          INT4 i4SetValFsIpvxAddrPrefix,
                          UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_FOUR;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %i",
                      i4FsIpvxAddrPrefixIfIndex, i4FsIpvxAddrPrefixAddrType,
                      pFsIpvxAddrPrefix, u4FsIpvxAddrPrefixLen,
                      i4SetValFsIpvxAddrPrefix));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4FsIpvxAddrPrefixIfIndex);
    UNUSED_PARAM (i4FsIpvxAddrPrefixAddrType);
    UNUSED_PARAM (pFsIpvxAddrPrefix);
    UNUSED_PARAM (u4FsIpvxAddrPrefixLen);
    UNUSED_PARAM (i4SetValFsIpvxAddrPrefix);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForIPVXScalars

 Input       :  i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID

 Description :  This function performs Inc Msr functionality for 
                IPVX scalars.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForIpvxGlbTable (UINT4 u4ContextId, INT4 i4SetVal,
                       UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4ContextId, i4SetVal));

#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4ContextId);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForIPVXIfTable

 Input       :  i4Ipv6InterfaceIfIndex - IPv6 IfIndex
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for 
                IPVX Interface table.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForIPVXIfTable (INT4 i4Ipv6InterfaceIfIndex,
                      INT4 i4SetVal, UINT4 *pu4ObjectId,
                      UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      i4Ipv6InterfaceIfIndex, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (i4SetVal);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForIPVXAddrTable

 Input       :  u4ContextId - Context Information
                i4IpAddressAddrType - IP address type.
                pIpAddressAddr - IP address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for 
                IPVX address table.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForIPVXAddrTable (UINT4 u4ContextId, INT4 i4IpAddressAddrType,
                        tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                        INT4 i4SetVal, UINT4 *pu4ObjectId,
                        UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", (INT4) u4ContextId,
                      i4IpAddressAddrType, pIpAddressAddr, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IpAddressAddrType);
    UNUSED_PARAM (pIpAddressAddr);
    UNUSED_PARAM (i4SetVal);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForIPVXNetToPhyTable

 Input       :  i4IpAddressAddrType - IP address type.
                pIpAddressAddr - IP address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for 
                IPVX address table.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForIPVXNetToPhyTable (INT4 i4IpNetToPhysicalIfIndex,
                            INT4 i4IpNetToPhysicalNetAddressType,
                            tSNMP_OCTET_STRING_TYPE * pIpNetToPhyNetAddr,
                            CHR1 cDatatype, VOID *pSetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                              i4IpNetToPhysicalIfIndex,
                              i4IpNetToPhysicalNetAddressType,
                              pIpNetToPhyNetAddr, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s",
                              i4IpNetToPhysicalIfIndex,
                              i4IpNetToPhysicalNetAddressType,
                              pIpNetToPhyNetAddr, pSetString));
            break;
    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4IpNetToPhysicalIfIndex);
    UNUSED_PARAM (i4IpNetToPhysicalNetAddressType);
    UNUSED_PARAM (pIpNetToPhyNetAddr);
    UNUSED_PARAM (pSetString);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForIPVXRtrAdverTable

 Input       :  i4IpAddressAddrType - IP address type.
                pIpAddressAddr - IP address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for 
                IPVX address table.

 Output      :  None.

 Returns     :  IPVX_SUCCESS or IPVX_FAILURE
****************************************************************************/
INT1
IncMsrForIPVXRtrAdverTable (INT4 i4Ipv6RouterAdvertIfIndex,
                            INT4 i4SetVal, UINT4 u4SetVal,
                            UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                            UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    if (i4SetVal != IPVX_MINUS_ONE)
    {
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          i4Ipv6RouterAdvertIfIndex, i4SetVal));
    }
    else
    {
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                          i4Ipv6RouterAdvertIfIndex, u4SetVal));
    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetVal);
    UNUSED_PARAM (i4SetVal);
#endif
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpvxSetCxt                                             */
/*                                                                           */
/* Description  : This function sets the global context variable             */
/*                                                                           */
/* Input        :  u4ContextId : The ip context identifier.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
INT4
UtilIpvxSetCxt (UINT4 u4ContextId)
{
#ifdef VRF_WANTED
    if (u4ContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return SNMP_FAILURE;
    }
    gu4CurrContextId = u4ContextId;
#else
    UNUSED_PARAM (u4ContextId);
    gu4CurrContextId = VCM_DEFAULT_CONTEXT;
#endif
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpvxReleaseCxt                                         */
/*                                                                           */
/* Description  : This function clear the global context variable            */
/*                                                                           */
/* Input        :  u4ContextId : The context identifier.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
INT4
UtilIpvxReleaseCxt (VOID)
{
    gu4CurrContextId = 0;
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpvxGetCurrContext                                         */
/*                                                                           */
/* Description  : This function returns the global context variable          */
/*                                                                           */
/* Input        :  None                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returs the global context variable.                        */
/*                                                                           */
/*****************************************************************************/
INT4
IpvxGetCurrContext ()
{
    return gu4CurrContextId;
}
#endif
/*****************************************************************************/
/* Function     : IpvxLock                                                   */
/*                                                                           */
/*  Description : Takes the Protocol Semaphore                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
IpvxLock (VOID)
{
    if (OsixSemTake (gNetIpvxSem) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : IpvxUnLock                                                 */
/*                                                                           */
/* Description : Releases the Protcol Semaphore                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
IpvxUnLock (VOID)
{
    OsixSemGive (gNetIpvxSem);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : IpvxVcmIsVRExists                                          */
/*                                                                           */
/* Description  : This function verifies the existence of the given context  */
/*                with the VCM module.                                       */
/*                                                                           */
/* Input        : u4ContextId - Context Id that has to be verified.          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IPVX_SUCCESS/IPVX_FAILURE                                  */
/*****************************************************************************/
PRIVATE INT4
IpvxVcmIsVRExists (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
    {
        return IPVX_FAILURE;
    }
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/* Function     : IpvxSetContext                                             */
/*                                                                           */
/* Description : Saves the current context Id in the global variable         */
/*                gu4CurrContextId                                           */
/*                                                                           */
/* Input        : u4ContextId - Current Context Id                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
IpvxSetContext (UINT4 u4ContextId)
{
    if (IpvxVcmIsVRExists (u4ContextId) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    gu4CurrContextId = u4ContextId;
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : IpvxResetContext                                           */
/*                                                                           */
/* Description  : Resets the saved context Id in the global variable         */
/*                gu4CurrContextId                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
IpvxResetContext (VOID)
{
    gu4CurrContextId = 0;
    return;
}

#ifdef IP6_WANTED
/****************************************************************************
 *  Function    :  IncMsrForIpv6RouteTable 
 *
 *   Input      :   u4ContextId - Context Identifier value
 *                  pDestIp - Destination  IP address
 *                  i4PrefixLen - Length of Destination IP
 *                  pNextHop - NextHop value
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM
 *                 for the Route Table.
 *
 * Output      :  None.
 *
 * Returns     :  None.                         
 *
 * ****************************************************************************/

VOID
IncMsrForIpv6RouteTable (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pDestIp,
                         INT4 i4PrefixLen,
                         tSNMP_OCTET_STRING_TYPE * pNextHop,
                         CHR1 cDatatype, VOID *pSetVal,
                         UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    UINT4               u4Policy[IPVX_TWO];
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OID_TYPE      RoutePolicy, *pRoutePolicy = NULL;
    tIp6Addr            Dest;
    tIp6Addr            DestRcvd;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RoutePolicy.pu4_OidList = u4Policy;
    pRoutePolicy = &RoutePolicy;
    IPVX_GET_NULL_ROUTE_POLICY (pRoutePolicy);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = Rtm6Lock;
    SnmpNotifyInfo.pUnLockPointer = Rtm6UnLock;
    SnmpNotifyInfo.u4Indices = IPVX_SEVEN;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    MEMCPY (&DestRcvd, pDestIp->pu1_OctetList, sizeof (tIp6Addr));
    Ip6CopyAddrBits (&Dest, &DestRcvd, i4PrefixLen);
    MEMCPY (pDestIp->pu1_OctetList, &Dest, sizeof (tIp6Addr));
    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %o %i %s %i",
                              (INT4) u4ContextId, IPVX_ADDR_FMLY_IPV6,
                              pDestIp, i4PrefixLen, pRoutePolicy,
                              IPVX_ADDR_FMLY_IPV6, pNextHop, i4SetValue));
            break;
        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %o %i %s %i",
                              (INT4) u4ContextId, IPVX_ADDR_FMLY_IPV6,
                              pDestIp, i4PrefixLen, pRoutePolicy,
                              IPVX_ADDR_FMLY_IPV6, pNextHop, u4SetValue));

            break;

    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pDestIp);
    UNUSED_PARAM (i4PrefixLen);
    UNUSED_PARAM (pRoutePolicy);
    UNUSED_PARAM (pNextHop);
#endif
    return;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearIpvxInterfaceEntries                        */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP interface based        */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex  - Interface index.                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IPVX_SUCCESS /IPVX_FAILURE;                        */
/*                                                                           */
/*****************************************************************************/

INT4
IpvxClearIpInterfaceEntries (UINT4 u4IfIndex)
{
    IpvxClearNetToPhysicalTable (u4IfIndex);
    IpvxClearAddrPrefixTable (u4IfIndex);
    IpvxClearIpv4InterfaceTable (u4IfIndex);
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearIpvxInterfaceEntries                       */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP6 interface based       */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex  - Interface index.                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IPVX_SUCCESS /IPVX_FAILURE;                        */
/*                                                                           */
/*****************************************************************************/

INT4
IpvxClearIp6InterfaceEntries (UINT4 u4IfIndex)
{
    /* When IP is not defined, clear the IP6 cache entries here */
#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED))))
    IpvxClearNetToPhysicalTable (u4IfIndex);
    /*
       IpvxClearAddrPrefixTable (u4IfIndex);
     */
#endif
    IpvxClearIpv6InterfaceTable (u4IfIndex);
    IpvxClearIp6RouterAdvtTable (u4IfIndex);
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearIpvxContextEntries                          */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP context based          */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IPVX_SUCCESS /IPVX_FAILURE;                        */
/*                                                                           */
/*****************************************************************************/

INT4
IpvxClearIpContextEntries (UINT4 u4ContextId)
{

    if (nmhValidateIndexInstanceFsMIStdIpGlobalTable (u4ContextId)
        == SNMP_FAILURE)
    {
        return IPVX_FAILURE;
    }

    nmhSetFsMIStdIpDefaultTTL ((INT4) u4ContextId, IP_DEF_TTL);

    /* Ipv4 address configurations will be cleared as a part of the 
     * fscfa ipAddrTable */
    /*
       IpvxClearAddressTable (u4ContextId);
     */
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Ip6ClearIpvxContextEntries                         */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP6 context based         */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IPVX_SUCCESS /IPVX_FAILURE;                        */
/*                                                                           */
/*****************************************************************************/

INT4
IpvxClearIp6ContextEntries (UINT4 u4ContextId)
{
    if (nmhValidateIndexInstanceFsMIStdIpGlobalTable (u4ContextId)
        == SNMP_FAILURE)
    {
        return IPVX_FAILURE;
    }

    nmhSetFsMIStdIpv6IpForwarding ((INT4) u4ContextId, IPVX_IP6_FORW_ENABLE);
    nmhSetFsMIStdIpv6IpDefaultHopLimit ((INT4) u4ContextId,
                                        IPVX_IP6_IF_DEF_HOPLMT);

    /*
     * Ipv6 Address table configurations will be cleared as a part of the 
     * fsmiipv6 address table 
     */
    /*
       #if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED))))
       IpvxClearAddressTable (u4ContextId);
       #endif
     */
    return IPVX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpvxClearNetToPhysicalTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Static cache entry        */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpvxClearNetToPhysicalTable (UINT4 u4IfIndex)
{
    INT4                i4IfIndex = 0;
    INT4                i4NxtIfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NxtAddrType = 0;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE NxtAddr;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Addr, IPVX_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtAddr, IPVX_ZERO, IPVX_MAX_INET_ADDR_LEN);
    Addr.pu1_OctetList = au1Addr;
    NxtAddr.pu1_OctetList = au1NxtAddr;
    Addr.i4_Length = IPVX_ZERO;
    NxtAddr.i4_Length = IPVX_ZERO;

    /*
       if (nmhGetFirstIndexFsMIStdIpNetToPhysicalTable
       (&i4NxtIfIndex, &i4NxtAddrType, &NxtAddr) == SNMP_FAILURE)
       {
       return;
       }
     */
    if (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
        ((INT4) u4IfIndex, &i4NxtIfIndex, i4AddrType, &i4NxtAddrType,
         &Addr, &NxtAddr) == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        if (i4NxtIfIndex > (INT4) u4IfIndex)
        {
            break;
        }

        i4IfIndex = i4NxtIfIndex;
        MEMCPY (Addr.pu1_OctetList, NxtAddr.pu1_OctetList,
                IPVX_MAX_INET_ADDR_LEN);
        Addr.i4_Length = NxtAddr.i4_Length;
        i4AddrType = i4NxtAddrType;
        if (i4NxtIfIndex == (INT4) u4IfIndex)
        {
            IpvxUtilClearNetToPhysicalTable (i4NxtIfIndex, i4NxtAddrType,
                                             &NxtAddr);
        }
    }
    while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
           (i4IfIndex, &i4NxtIfIndex, i4AddrType,
            &i4NxtAddrType, &Addr, &NxtAddr) != SNMP_FAILURE);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpvxClearIp6RouterAdvtTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP6 Router advertisment   */
/*                        table configurations done for the given interface. */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpvxClearIp6RouterAdvtTable (UINT4 u4IfIndex)
{
    if (nmhValidateIndexInstanceFsMIStdIpv6RouterAdvertTable ((INT4) u4IfIndex)
        == SNMP_SUCCESS)
    {
        IpvxUtilClearIp6RouterAdvtTable ((INT4) u4IfIndex);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpvxClearIpv4InterfaceTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Ipv4 interface table      */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpvxClearIpv4InterfaceTable (UINT4 u4IfIndex)
{
    if (nmhValidateIndexInstanceFsMIStdIpv4InterfaceTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return;
    }

    nmhSetFsMIStdIpv4InterfaceEnableStatus ((INT4) u4IfIndex,
                                            IPVX_IPV4_ENABLE_STATUS_UP);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpvxClearIpv6InterfaceTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Ipv6 interface table      */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpvxClearIpv6InterfaceTable (UINT4 u4IfIndex)
{
    if (nmhValidateIndexInstanceFsMIStdIpv6InterfaceTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return;
    }

#ifdef IP6_WANTED
    Ip6UtilClearIpv6InterfaceTable ((INT4) u4IfIndex);
#endif
    nmhSetFsMIStdIpv6InterfaceForwarding ((INT4) u4IfIndex,
                                          IPVX_IP6_FORW_ENABLE);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpvxClearAddrPrefixTable                           */
/*                                                                           */
/*     DESCRIPTION      : This function clears the Address prefix table      */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpvxClearAddrPrefixTable (UINT4 u4IfIndex)
{
    INT4                i4NxtIfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NxtAddrType = 0;
    UINT4               u4PrefLen = 0;
    UINT4               u4NxtPrefLen = 0;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE NxtAddr;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NxtAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Addr, IPVX_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NxtAddr, IPVX_ZERO, IPVX_MAX_INET_ADDR_LEN);
    Addr.pu1_OctetList = au1Addr;
    NxtAddr.pu1_OctetList = au1NxtAddr;
    Addr.i4_Length = IPVX_ZERO;
    NxtAddr.i4_Length = IPVX_ZERO;

    while (nmhGetNextIndexFsMIIpvxAddrPrefixTable
           ((INT4) u4IfIndex, &i4NxtIfIndex, i4AddrType,
            &i4NxtAddrType, &Addr, &NxtAddr, u4PrefLen, &u4NxtPrefLen)
           != SNMP_FAILURE)
    {
        /* Only the ipv4 prefix entry configurations are cleared here.
         * The ipv6 prefix configurations will be cleared as a part 
         * of the ipv6 address table */
        if ((i4NxtIfIndex > (INT4) u4IfIndex) ||
            (i4NxtAddrType == IPV6_ADD_TYPE))
        {
            break;
        }

        MEMCPY (Addr.pu1_OctetList, NxtAddr.pu1_OctetList,
                IPVX_MAX_INET_ADDR_LEN);
        Addr.i4_Length = NxtAddr.i4_Length;
        i4AddrType = i4NxtAddrType;
        u4PrefLen = u4NxtPrefLen;
        IpvxUtilClearAddrPrefixTable (i4NxtIfIndex, i4NxtAddrType, &NxtAddr,
                                      u4NxtPrefLen);
    }
}

/****************************************************************************
 Function    :  Ipv4HealthChkClearCtr
 Description :  Clears the IP statistics of (Ipv4/Ipv6)
 Returns     :  NONE
****************************************************************************/
VOID
Ipv4HealthChkClearCtr ()
{

    UINT4               u4ContextId = 0;
    INT4                i4IpSystemStatsIPVersion = 0;
    INT4                i4IpIfStatsIPVersion = 0;
    INT4                i4IpIfStatsPrevIPVersion = 0;
    INT4                i4IcmpStatsIPVersion = 0;
    INT4                i4IcmpStatsPrevIPVersion = 0;
    INT4                i4IcmpMsgStatsIPVersion = 0;
    INT4                i4IcmpMsgStatsPrevIPVersion = 0;
    INT4                i4IpIfStatsPrevIfIndex = 0;
    INT4                i4IpIfStatsIfIndex = 0;
    INT4                i4IcmpMsgStatsType = 0;
    INT4                i4IcmpMsgStatsPrevType = 0;

    for (u4ContextId = IPVX_DEFAULT_CONTEXT;
         u4ContextId <= SYS_DEF_MAX_NUM_CONTEXTS; u4ContextId++)
    {

        if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
        {
            continue;
        }
        /*IpSystemStatsTable */
        if (nmhGetFirstIndexIpSystemStatsTable (&i4IpSystemStatsIPVersion) !=
            SNMP_FAILURE)
        {

            do
            {
                if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxClearIpv4SystemStats (u4ContextId, IPVX_INVALID_IFIDX);
#endif
                }
                else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxClearIpv6SystemStats (u4ContextId, IPVX_INVALID_IFIDX);
#endif
#endif

                }
                i4IpIfStatsPrevIPVersion = i4IpSystemStatsIPVersion;
            }
            while (nmhGetNextIndexIpSystemStatsTable (i4IpIfStatsPrevIPVersion,
                                                      &i4IpSystemStatsIPVersion)
                   != SNMP_FAILURE);

        }
        /*IpIfStatsEntry */

        if (nmhGetFirstIndexIpIfStatsTable (&i4IpIfStatsIPVersion,
                                            &i4IpIfStatsIfIndex) !=
            SNMP_FAILURE)
        {

            do
            {
                if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxClearIpv4SystemStats (u4ContextId, i4IpIfStatsIfIndex);
#endif
                }
                else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxClearIpv6SystemStats (u4ContextId, i4IpIfStatsIfIndex);
#endif
#endif
                }
                i4IpIfStatsPrevIPVersion = i4IpIfStatsIPVersion;
                i4IpIfStatsPrevIfIndex = i4IpIfStatsIfIndex;

            }
            while (nmhGetNextIndexIpIfStatsTable (i4IpIfStatsPrevIPVersion,
                                                  &i4IpIfStatsIPVersion,
                                                  i4IpIfStatsPrevIfIndex,
                                                  &i4IpIfStatsIfIndex) !=
                   SNMP_FAILURE);

        }

        /*IcmpStatsEntry */

        if (nmhGetFirstIndexIcmpStatsTable (&i4IcmpStatsIPVersion) !=
            SNMP_FAILURE)
        {
            do
            {
                if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxClearIpv4IcmpStats (u4ContextId);
#endif
                }
                else if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxClearIpv6IcmpStats (u4ContextId);
#endif
#endif
                }
                i4IcmpStatsPrevIPVersion = i4IcmpStatsIPVersion;

            }
            while (nmhGetNextIndexIcmpStatsTable (i4IcmpStatsPrevIPVersion,
                                                  &i4IcmpStatsIPVersion) !=
                   SNMP_FAILURE);
        }

        /*IcmpMsgStatsEntry */
        if (nmhGetFirstIndexIcmpMsgStatsTable (&i4IcmpMsgStatsIPVersion,
                                               &i4IcmpMsgStatsType) !=
            SNMP_FAILURE)
        {
            do
            {
                if (i4IcmpMsgStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED
                    IPvxClearIpv4IcmpStats (u4ContextId);
#endif
                }
                else if (i4IcmpMsgStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxClearIpv6IcmpStats (u4ContextId);
#endif
#endif
                }
                i4IcmpMsgStatsPrevIPVersion = i4IcmpMsgStatsIPVersion;
                i4IcmpMsgStatsPrevType = i4IcmpMsgStatsType;
            }
            while (nmhGetNextIndexIcmpMsgStatsTable
                   (i4IcmpMsgStatsPrevIPVersion, &i4IcmpMsgStatsIPVersion,
                    i4IcmpMsgStatsPrevType,
                    &i4IcmpMsgStatsType) != SNMP_FAILURE);

        }

        /*inetCidrRoute */
#ifdef IP_WANTED
        ClearInetCidrRoute (u4ContextId);
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
        ClearInetCidrIpv6Route (u4ContextId);
#endif
#endif

        /*UDPDataGrams */
#ifdef IP_WANTED
        ClearUdpDatagrams (u4ContextId);
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
        ClearUDPIpv6Datagrams (u4ContextId);
#endif
#endif
    }
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    IP6_TASK_LOCK ();
    ClearFsIpv6IfIcmpTable ();
    IP6_TASK_UNLOCK ();
#endif
#endif
    return;
}

#if ((defined IP6_WANTED) && (!defined LNXIP6_WANTED))
/****************************************************************************
 *
 *    FUNCTION NAME    : ClearFsIpv6IfIcmpTable
 *
 *    DESCRIPTION      : Clears the fsIpv6IfIcmpTable 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 **************************************************************************/
VOID
ClearFsIpv6IfIcmpTable (VOID)
{
    INT4                i4Ipv6InterfaceIfIndex = 0;
    INT4                i4NextIpv6InterfaceIfIndex = 0;
    tIp6If             *pIf6 = NULL;

    if (SNMP_SUCCESS ==
        nmhGetFirstIndexFsIpv6IfIcmpTable (&i4NextIpv6InterfaceIfIndex))
    {
        do
        {
            i4Ipv6InterfaceIfIndex = i4NextIpv6InterfaceIfIndex;
            if (i4Ipv6InterfaceIfIndex != IPVX_INVALID_IFIDX)
            {
                pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6InterfaceIfIndex);
                if (pIf6 == NULL)
                {
                    return;
                }
                if (pIf6->pIfIcmp6Stats != NULL)
                {
                    pIf6->pIfIcmp6Stats->u4InMsgs = 0;
                    pIf6->pIfIcmp6Stats->u4InMsgs = 0;
                    pIf6->pIfIcmp6Stats->u4InErrs = 0;
                    pIf6->pIfIcmp6Stats->u4InBadcode = 0;
                    pIf6->pIfIcmp6Stats->u4InToobig = 0;
                    pIf6->pIfIcmp6Stats->u4InDstUnreach = 0;
                    pIf6->pIfIcmp6Stats->u4InTmexceeded = 0;
                    pIf6->pIfIcmp6Stats->u4InParamprob = 0;
                    pIf6->pIfIcmp6Stats->u4InEchoReq = 0;
                    pIf6->pIfIcmp6Stats->u4InEchoResp = 0;
                    pIf6->pIfIcmp6Stats->u4OutMsgs = 0;
                    pIf6->pIfIcmp6Stats->u4OutErrs = 0;
                    pIf6->pIfIcmp6Stats->u4OutDstUnreach = 0;
                    pIf6->pIfIcmp6Stats->u4OutToobig = 0;
                    pIf6->pIfIcmp6Stats->u4OutTmexceeded = 0;
                    pIf6->pIfIcmp6Stats->u4OutParamprob = 0;
                    pIf6->pIfIcmp6Stats->u4OutEchoReq = 0;
                    pIf6->pIfIcmp6Stats->u4OutEchoResp = 0;
                    pIf6->pIfIcmp6Stats->u4InRouterSol = 0;
                    pIf6->pIfIcmp6Stats->u4InRouterAdv = 0;
                    pIf6->pIfIcmp6Stats->u4InNeighSol = 0;
                    pIf6->pIfIcmp6Stats->u4InNeighAdv = 0;
                    pIf6->pIfIcmp6Stats->u4InRedir = 0;
                    pIf6->pIfIcmp6Stats->u4OutRouterSol = 0;
                    pIf6->pIfIcmp6Stats->u4OutRouterAdv = 0;
                    pIf6->pIfIcmp6Stats->u4OutNeighSol = 0;
                    pIf6->pIfIcmp6Stats->u4OutNeighAdv = 0;
                    pIf6->pIfIcmp6Stats->u4OutRedir = 0;
                    pIf6->pIfIcmp6Stats->u4InMLDQuery = 0;
                    pIf6->pIfIcmp6Stats->u4InMLDReport = 0;
                    pIf6->pIfIcmp6Stats->u4InMLDDone = 0;
                    pIf6->pIfIcmp6Stats->u4OutMLDQuery = 0;
                    pIf6->pIfIcmp6Stats->u4OutMLDReport = 0;
                    pIf6->pIfIcmp6Stats->u4OutMLDDone = 0;
                }
            }                    /*if i4Ipv6InterfaceIfIndex */
        }
        while (SNMP_SUCCESS ==
               nmhGetNextIndexFsIpv6IfIcmpTable (i4Ipv6InterfaceIfIndex,
                                                 &i4NextIpv6InterfaceIfIndex));
    }                            /*if getFirstIndex */

}
#endif

/*PopulateIpv4Counters is test code.This function is invoked from ISS.
 *It populates the Ipvx(v4/v6) counters/statistics for verfying the 
 *Ipvx(v4/v6) clear counters*/

/****************************************************************************
 Function    :  PopulateIpv4Counters
 Description :  Clears the IP statistics of (Ipv4/Ipv6)
 Returns     :  NONE
****************************************************************************/
VOID
PopulateIpv4Counters ()
{

    UINT4               u4ContextId = 0;
    INT4                i4IpSystemStatsIPVersion = 0;
    INT4                i4IpIfStatsIPVersion = 0;
    INT4                i4IpIfStatsPrevIPVersion = 0;
    INT4                i4IcmpStatsIPVersion = 0;
    INT4                i4IcmpStatsPrevIPVersion = 0;
    INT4                i4IcmpMsgStatsIPVersion = 0;
    INT4                i4IcmpMsgStatsPrevIPVersion = 0;
    INT4                i4IpIfStatsPrevIfIndex = 0;
    INT4                i4IpIfStatsIfIndex = 0;
    INT4                i4IcmpMsgStatsType = 0;
    INT4                i4IcmpMsgStatsPrevType = 0;

    for (u4ContextId = IPVX_DEFAULT_CONTEXT;
         u4ContextId <= SYS_DEF_MAX_NUM_CONTEXTS; u4ContextId++)
    {

        if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
        {
            continue;
        }
        /*IpSystemStatsTable */
        if (nmhGetFirstIndexIpSystemStatsTable (&i4IpSystemStatsIPVersion) !=
            SNMP_FAILURE)
        {

            do
            {
                if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxTstPopulateIpv4SystemStats (u4ContextId,
                                                    IPVX_INVALID_IFIDX);
#endif
                }
                else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxTstPopulateIpv6SystemStats (u4ContextId,
                                                    IPVX_INVALID_IFIDX);
#endif
#endif

                }
                i4IpIfStatsPrevIPVersion = i4IpSystemStatsIPVersion;
            }
            while (nmhGetNextIndexIpSystemStatsTable (i4IpIfStatsPrevIPVersion,
                                                      &i4IpSystemStatsIPVersion)
                   != SNMP_FAILURE);

        }
        /*IpIfStatsEntry */

        if (nmhGetFirstIndexIpIfStatsTable (&i4IpIfStatsIPVersion,
                                            &i4IpIfStatsIfIndex) !=
            SNMP_FAILURE)
        {

            do
            {
                if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxTstPopulateIpv4SystemStats (u4ContextId,
                                                    i4IpIfStatsIfIndex);
#endif
                }
                else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxTstPopulateIpv6SystemStats (u4ContextId,
                                                    i4IpIfStatsIfIndex);
#endif
#endif
                }
                i4IpIfStatsPrevIPVersion = i4IpIfStatsIPVersion;
                i4IpIfStatsPrevIfIndex = i4IpIfStatsIfIndex;

            }
            while (nmhGetNextIndexIpIfStatsTable (i4IpIfStatsPrevIPVersion,
                                                  &i4IpIfStatsIPVersion,
                                                  i4IpIfStatsPrevIfIndex,
                                                  &i4IpIfStatsIfIndex) !=
                   SNMP_FAILURE);

        }

        /*IcmpStatsEntry */

        if (nmhGetFirstIndexIcmpStatsTable (&i4IcmpStatsIPVersion) !=
            SNMP_FAILURE)
        {
            do
            {
                if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxTstPopulateIpv4IcmpStats (u4ContextId);
#endif
                }
                else if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxTstPopulateIpv6IcmpStats (u4ContextId);
#endif
#endif
                }
                i4IcmpStatsPrevIPVersion = i4IcmpStatsIPVersion;

            }
            while (nmhGetNextIndexIcmpStatsTable (i4IcmpStatsPrevIPVersion,
                                                  &i4IcmpStatsIPVersion) !=
                   SNMP_FAILURE);
        }

        /*IcmpMsgStatsEntry */
        if (nmhGetFirstIndexIcmpMsgStatsTable (&i4IcmpMsgStatsIPVersion,
                                               &i4IcmpMsgStatsType) !=
            SNMP_FAILURE)
        {
            do
            {
                if (i4IcmpMsgStatsIPVersion == INET_ADDR_TYPE_IPV4)
                {
#ifdef IP_WANTED

                    IPvxTstPopulateIpv4IcmpStats (u4ContextId);
#endif
                }
                else if (i4IcmpMsgStatsIPVersion == INET_ADDR_TYPE_IPV6)
                {
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    IPvxTstPopulateIpv6IcmpStats (u4ContextId);
#endif
#endif
                }
                i4IcmpMsgStatsPrevIPVersion = i4IcmpMsgStatsIPVersion;
                i4IcmpMsgStatsPrevType = i4IcmpMsgStatsType;
            }
            while (nmhGetNextIndexIcmpMsgStatsTable
                   (i4IcmpMsgStatsPrevIPVersion, &i4IcmpMsgStatsIPVersion,
                    i4IcmpMsgStatsPrevType,
                    &i4IcmpMsgStatsType) != SNMP_FAILURE);

        }

        /*inetCidrRoute */
#ifdef IP_WANTED
        RtmTstPopulateInetCidrRoute (u4ContextId);
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
        Rtm6TstPopulateInetCidrIpv6Route (u4ContextId);
#endif
#endif

        /*UDPDataGrams */
#ifdef IP_WANTED
        UdpTstPopulateDatagrams (u4ContextId);
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
        UdpTstPopulateIpv6Datagrams (u4ContextId);
#endif
#endif

    }
    return;
}

/****************************************************************************
 Function    :  IpvxUtilClearNetToPhysicalTable
 Description :  Clears the NetToPhysicalTable of (Ipv4/Ipv6)
 INPUT       :  i4NxtIfIndex, i4NxtAddrType, NxtAddr
 Returns     :  IPVX_SUCCESS/IPVX_FAILURE
****************************************************************************/
INT1
IpvxUtilClearNetToPhysicalTable (INT4 i4NxtIfIndex, INT4 i4NxtAddrType,
                                 tSNMP_OCTET_STRING_TYPE * NxtAddr)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    if (i4NxtAddrType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr, NxtAddr);

        if (IPvxSetIpv4NetToPhyRowStatus (i4NxtIfIndex,
                                          u4Ipv4Addr,
                                          IPVX_DESTROY) == IPVX_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return IPVX_FAILURE;
        }
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4NxtAddrType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED

        if (IPvxSetIpv6NetToPhyRowStatus (i4NxtIfIndex,
                                          NxtAddr,
                                          IPVX_DESTROY) == IPVX_FAILURE)
        {
            return IPVX_FAILURE;
        }
#endif
    }
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IpvxUtilClearNetToPhysicalTable
 Description :  Clears the RouterAdvtTable of (Ipv4/Ipv6)
 INPUT       :  i4IfIndex 
 Returns     :  IPVX_SUCCESS/IPVX_FAILURE
****************************************************************************/
INT1
IpvxUtilClearIp6RouterAdvtTable (INT4 i4IfIndex)
{
#ifdef IP6_WANTED
    if (Ip6SetRtrAdvtRowStatus (i4IfIndex, IPVX_DESTROY) == IPVX_FAILURE)
    {
        return IPVX_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
 Function    :  IpvxUtilClearNetToPhysicalTable
 Description :  Clears the AddrPrefixTable of Ipv4
 INPUT       :  i4NxtIfIndex, i4NxtAddrType, NxtAddr, u4NxtPrefLen
 Returns     :  IPVX_SUCCESS/IPVX_FAILURE
****************************************************************************/
INT1
IpvxUtilClearAddrPrefixTable (INT4 i4NxtIfIndex, INT4 i4NxtAddrType,
                              tSNMP_OCTET_STRING_TYPE * NxtAddr,
                              UINT4 u4NxtPrefLen)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    INT1                i1AddrVal = IPVX_ZERO;
    UINT1               u1AddrRowStatus = IPVX_ZERO;
    UINT4               u4IpAddr = IPVX_ZERO;
    UINT4               u4BcastAddr = IPVX_ZERO;
    UINT4               u4SubnetMask = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
    tIpIfRecord         IpIntfInfo;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    if (i4NxtAddrType == IPV4_ADD_TYPE)
    {
        /* We should allow the creation of the row in
         * prefix table only when the interafce is present*/
        IpIntfInfo.u4IfIndex = (UINT4) i4NxtIfIndex;

        CFA_IPIFTBL_LOCK ();
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the interface exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return IPVX_FAILURE;
        }
        u1AddrRowStatus = pIpIntf->u1AddrRowStatus;
        CFA_IPIFTBL_UNLOCK ();
        /*If the Address table entry is not present dont proceed as the
         *prefix table is already deleted when the Address table is
         *deleted*/
        if (u1AddrRowStatus == IPVX_ZERO)
        {
            return IPVX_FAILURE;
        }
        else
        {
            IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAddr, NxtAddr);
            i1AddrVal = CFA_IS_ADDR_CLASS_A (u4IpAddr) ? IPVX_ONE : IPVX_ZERO;
            if (i1AddrVal)
            {
                i1AddrVal = IP_IS_ZERO_NETWORK (u4IpAddr) ?
                    IPVX_ZERO : IPVX_ONE;
            }
            if (i1AddrVal)
            {
                u4SubnetMask = 0xFF000000;

            }
            else if (CFA_IS_ADDR_CLASS_B (u4IpAddr))
            {
                u4SubnetMask = 0xFFFF0000;
            }
            else if (CFA_IS_ADDR_CLASS_B (u4IpAddr))
            {
                u4SubnetMask = 0xFFFFFF00;
            }
            if (CfaIPVXSetIpAddress
                ((UINT4) i4NxtIfIndex, u4IpAddr, u4SubnetMask) != CFA_SUCCESS)
            {
                return IPVX_FAILURE;
            }
            u4BcastAddr = u4IpAddr | (~(u4SubnetMask));

            if (CfaIPVXSetIfIpBroadcastAddr
                (i4NxtIfIndex, u4BcastAddr) == CFA_FAILURE)
            {
                return (IPVX_FAILURE);
            }
        }
        if (SetPrefixTableRowStatus
            (i4NxtIfIndex,
             (UINT1) IPVX_DESTROY,
             i4NxtAddrType,
             *(tIp6Addr *) NxtAddr, (UINT1) u4NxtPrefLen) != IPVX_SUCCESS)
        {
            return IPVX_FAILURE;
        }
    }
#endif
#ifdef IP6_WANTED
    if (i4NxtAddrType == IPV6_ADD_TYPE)
    {
        if (SetPrefixTableRowStatus
            (i4NxtIfIndex,
             (UINT1) IPVX_DESTROY,
             i4NxtAddrType,
             *(tIp6Addr *) NxtAddr, (UINT1) u4NxtPrefLen) != IPVX_SUCCESS)
        {
            return IPVX_FAILURE;
        }
        IPvxDeleteIPv6Add ((UINT4) i4NxtIfIndex,
                           *(tIp6Addr *) (VOID *)
                           NxtAddr->pu1_OctetList,
                           (UINT1) u4NxtPrefLen, ADDR6_UNICAST);
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilGetFirstIndexFsMIStdIpifTable 
Description :   Gets the first Index of the Table
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/
    INT1 
IpvxUtilGetFirstIndexFsMIStdIpifTable(INT4 *pi4FsMIStdIpIndex)
{
#ifdef LNXIP4_WANTED
    INT4 i4FsMIStdIpIndex = 0;
    if (LnxGetNextIndexFsMIStdIpifTable(0, &i4FsMIStdIpIndex) == NETIPV4_FAILURE)
    {
        return IPVX_FAILURE;
    }
    *pi4FsMIStdIpIndex = i4FsMIStdIpIndex;
#else
    if (IpGetFirstIndexFsIpifTable(pi4FsMIStdIpIndex)== IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;

}

/****************************************************************************
Function    :  IpvxUtilGetNextIndexFsMIStdIpifTable 
Description :  To get the next index from the Table. 
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


    INT1 IpvxUtilGetNextIndexFsMIStdIpifTable
(INT4 i4FsMIStdIpIndex ,INT4 *pi4NextFsMIStdIpIndex)
{
#ifdef LNXIP4_WANTED
    if (LnxGetNextIndexFsMIStdIpifTable 
        (i4FsMIStdIpIndex , pi4NextFsMIStdIpIndex) == NETIPV4_FAILURE)
    {
        return IPVX_FAILURE;
    }
#else
    if (IpGetNextIndexFsIpifTable (i4FsMIStdIpIndex , pi4NextFsMIStdIpIndex)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilGetFsMIStdIpProxyArpAdminStatus 
Inputs      :  i4FsMIStdIpIndex 
Outputs     :  pi4RetValFsMIStdIpProxyArpAdminStatus
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


    INT1 IpvxUtilGetFsMIStdIpProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 *pi4RetValFsMIStdIpProxyArpAdminStatus)
{
#ifdef LNXIP4_WANTED
    if (LnxIpGetProxyArpAdminStatus (i4FsMIStdIpIndex, 
                                     pi4RetValFsMIStdIpProxyArpAdminStatus)
        == NETIPV4_FAILURE)
    {
        return IPVX_FAILURE;
    }
#else
    if (IpGetFsIpifProxyArpAdminStatus 
        (i4FsMIStdIpIndex, pi4RetValFsMIStdIpProxyArpAdminStatus)== IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilGetFsMIStdIpProxyArpAdminStatus
Inputs      :  i4FsMIStdIpIndex
Outputs     :  pi4RetValFsMIStdIpProxyArpAdminStatus
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


    INT1 IpvxUtilGetFsMIStdIpLocalProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 *pi4RetValFsMIStdIpLocalProxyArpAdminStatus)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM(i4FsMIStdIpIndex);
    UNUSED_PARAM(pi4RetValFsMIStdIpLocalProxyArpAdminStatus);
#else
    if (IpGetFsIpifLocalProxyArpAdminStatus 
        (i4FsMIStdIpIndex, pi4RetValFsMIStdIpLocalProxyArpAdminStatus)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;

}

/****************************************************************************
Function    :  IpvxUtilSetFsMIStdIpProxyArpAdminStatus 
Inputs      :  i4FsMIStdIpIndex, i4SetValFsMIStdIpProxyArpAdminStatus
Outputs     :  None
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


    INT1 IpvxUtilSetFsMIStdIpProxyArpAdminStatus 
(INT4 i4FsMIStdIpIndex , INT4 i4SetValFsMIStdIpProxyArpAdminStatus)
{
#ifdef LNXIP4_WANTED
    if (LnxIpSetProxyArpAdminStatus
        (i4FsMIStdIpIndex, i4SetValFsMIStdIpProxyArpAdminStatus)
        == NETIPV4_FAILURE)
    {
        return IPVX_FAILURE;
    }
#else
    if (IpSetFsIpifProxyArpAdminStatus
        (i4FsMIStdIpIndex, i4SetValFsMIStdIpProxyArpAdminStatus)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;

}

/****************************************************************************
Function    :  IpvxUtilSetFsMIStdIpLocalProxyArpAdminStatus
Inputs      :  i4FsMIStdIpIndex,i4SetValFsMIStdIpLocalProxyArpAdminStatus 
Outputs     :  None
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


INT1
    IpvxUtilSetFsMIStdIpLocalProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 i4SetValFsMIStdIpLocalProxyArpAdminStatus)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM(i4FsMIStdIpIndex);
    UNUSED_PARAM(i4SetValFsMIStdIpLocalProxyArpAdminStatus);
#else
    if (IpSetFsIpifLocalProxyArpAdminStatus
        (i4FsMIStdIpIndex, i4SetValFsMIStdIpLocalProxyArpAdminStatus)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilTestv2FsMIStdIpProxyArpAdminStatus
Inputs      :  i4FsMIStdIpIndex,i4TestValFsMIStdIpProxyArpAdminStatus
Outputs     :  pu4ErrorCode
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


INT1
    IpvxUtilTestv2FsMIStdIpProxyArpAdminStatus
(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpIndex ,
 INT4 i4TestValFsMIStdIpProxyArpAdminStatus)
{

#ifdef LNXIP4_WANTED
    if ( LnxIpTestProxyArpAdminStatus ( pu4ErrorCode, i4FsMIStdIpIndex,
                                        i4TestValFsMIStdIpProxyArpAdminStatus)
         == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IPVX_FAILURE;
    }
#else
    if (IpTestv2FsIpifProxyArpAdminStatus
        (pu4ErrorCode, i4FsMIStdIpIndex, i4TestValFsMIStdIpProxyArpAdminStatus)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilTestv2FsMIStdIpLocalProxyArpAdminStatus 
Inputs      :  i4FsMIStdIpIndex,i4TestValFsMIStdIpLocalProxyArpAdminStatus
Outputs     :  pu4ErrorCode
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/


INT1
    IpvxUtilTestv2FsMIStdIpLocalProxyArpAdminStatus
(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpIndex ,
 INT4 i4TestValFsMIStdIpLocalProxyArpAdminStatus)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIStdIpIndex);
    UNUSED_PARAM (i4TestValFsMIStdIpLocalProxyArpAdminStatus);
#else
    if (IpTestv2FsIpifLocalProxyArpAdminStatus
        (pu4ErrorCode, i4FsMIStdIpIndex,
         i4TestValFsMIStdIpLocalProxyArpAdminStatus)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilGetFsMIStdIpProxyArpSubnetOption 
Inputs      :  
Outputs     :  pi4RetValFsMIStdIpProxyArpSubnetOption 
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/

    INT1 IpvxUtilGetFsMIStdIpProxyArpSubnetOption
(INT4 *pi4RetValFsMIStdIpProxyArpSubnetOption)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pi4RetValFsMIStdIpProxyArpSubnetOption);
#else
    if (IpGetFsIpProxyArpSubnetOption (pi4RetValFsMIStdIpProxyArpSubnetOption) 
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif

    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilSetFsMIStdIpProxyArpSubnetOption 
Inputs      :  i4SetValFsMIStdIpProxyArpSubnetOption
Outputs     :  None
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/

    INT1
IpvxUtilSetFsMIStdIpProxyArpSubnetOption(INT4 i4SetValFsMIStdIpProxyArpSubnetOption)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4SetValFsMIStdIpProxyArpSubnetOption);
#else
    if (IpSetFsIpProxyArpSubnetOption
        (i4SetValFsMIStdIpProxyArpSubnetOption) == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}

/****************************************************************************
Function    :  IpvxUtilTestv2FsMIStdIpProxyArpSubnetOption 
Inputs      :  i4TestValFsMIStdIpProxyArpSubnetOption 
Outputs     :  pu4ErrorCode
Returns     :  IPVX_SUCCESS/IPVX_FAILURE
 ****************************************************************************/

    INT1 IpvxUtilTestv2FsMIStdIpProxyArpSubnetOption
(UINT4 *pu4ErrorCode ,INT4 i4TestValFsMIStdIpProxyArpSubnetOption)
{
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMIStdIpProxyArpSubnetOption);
#else
    if (IpTestv2FsIpProxyArpSubnetOption(pu4ErrorCode,
                                         i4TestValFsMIStdIpProxyArpSubnetOption)
        == IP_FAILURE)
    {
        return IPVX_FAILURE;
    }
#endif
    return IPVX_SUCCESS;
}



