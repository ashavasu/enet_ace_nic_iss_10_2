/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipvlw.c,v 1.63 2016/03/09 11:45:16 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "trieinc.h"
# include  "fssnmp.h"
# include  "ipvxinc.h"
# include  "ipvxdefs.h"
# include  "ipvxext.h"
# include  "cfa.h"
# include  "ipv6.h"
# include  "ip.h"
# include   "cli.h"
# include  "ip6util.h"
# include  "stdipvcli.h"
# include  "fsmsipcli.h"
# include  "rmgr.h"
# include  "rtm6.h"
#ifdef IP6_WANTED
# include "ip6snmp.h"
# include "fsip6low.h"
#endif

INT4                gi4IPvxIpAddrTblSpinLock = IPVX_ZERO;
INT4                gi4IPvxIpv6RtrAdvtSpinLock = IPVX_ZERO;
UINT4               gu4IpvxIfStatsTblLstChange = IPVX_ZERO;
extern UINT4        gau4IpCidrSubnetMask[IP_CFA_MAX_CIDR + 1];
/* The nmh Functions for the following Objects are present in the 
 * netip module , so the nmh functions are removed form here 
 * 
 * IP Config Obj 
 * -------------
 * 1. ipForwarding   2. ipDefaultTTL  3. ipReasmTimeout
 *
 * IP Stats Obj
 * ------------
 * 4. ipInReceives   5. ipInHdrErrors  6. ipInAddrErrors  7. ipForwDatagrams
 * 8. ipInUnknownProtos  9. ipInDiscards  10. ipInDelivers  11. ipOutRequests
 * 12. ipOutDiscards  13. ipOutNoRoutes  14. ipReasmReqds  15. ipReasmOKs
 * 16. ipReasmFails  17. ipFragOKs  18. ipFragFails  19. ipFragCreates 
 * 20. ipRoutingDiscards 
 *
 * IP Tables
 * ---------
 * 21. ipAddrTable  22. ipNetToMediaTable 
 * 
 * ICMP Stats Obj
 * --------------
 * 23. icmpInMsgs  24. icmpInErrors  25. icmpInDestUnreachs  
 * 26. icmpInTimeExcds  27. icmpInParmProbs  28. icmpInSrcQuenchs 
 * 29. icmpInRedirects  30. icmpInEchos  31. icmpInEchoReps
 * 32. icmpInTimestamps  33. icmpInTimestampReps  34. icmpInAddrMasks 
 * 35. icmpInAddrMaskReps  36. icmpOutMsgs  37. icmpOutErrors
 * 38. icmpOutDestUnreachs  39. icmpOutTimeExcds  40. icmpOutParmProbs
 * 41. icmpOutSrcQuenchs  42. icmpOutRedirects 43. icmpOutEchos
 * 44. icmpOutEchoReps  45. icmpOutTimestamps  46. icmpOutTimestampReps
 * 47. icmpOutAddrMasks  48. icmpOutAddrMaskReps  
 *
 * UDP Stats Obj
 * --------------
 * 49. udpInDatagrams  50. udpNoPorts  51. udpInErrors  51. udpOutDatagrams
 *  
 * UDP Tables
 * --------------
 * 52. udpTable
 *
 * */

/****************************************************************************
 Function    :  nmhGetIpv6IpForwarding
 Input       :  The Indices

                The Object 
                retValIpv6IpForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IpForwarding (INT4 *pi4RetValIpv6IpForwarding)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhGetIpv6Forwarding (pi4RetValIpv6IpForwarding);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#else
    *pi4RetValIpv6IpForwarding = IP6_FORW_DISABLE;
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6IpDefaultHopLimit
 Input       :  The Indices

                The Object 
                retValIpv6IpDefaultHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6IpDefaultHopLimit (INT4 *pi4RetValIpv6IpDefaultHopLimit)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhGetIpv6DefaultHopLimit (pi4RetValIpv6IpDefaultHopLimit);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pi4RetValIpv6IpDefaultHopLimit);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv4InterfaceTableLastChange
 Input       :  The Indices

                The Object 
                retValIpv4InterfaceTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv4InterfaceTableLastChange (UINT4
                                    *pu4RetValIpv4InterfaceTableLastChange)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    *pu4RetValIpv4InterfaceTableLastChange = IPvxGetIpv4IfTblLastChange ();
    IPFWD_PROT_UNLOCK ();
    i1RetVal = SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4RetValIpv4InterfaceTableLastChange);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6IpForwarding
 Input       :  The Indices

                The Object 
                setValIpv6IpForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IpForwarding (INT4 i4SetValIpv6IpForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhSetIpv6Forwarding (i4SetValIpv6IpForwarding);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4SetValIpv6IpForwarding);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6IpDefaultHopLimit
 Input       :  The Indices

                The Object 
                setValIpv6IpDefaultHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6IpDefaultHopLimit (INT4 i4SetValIpv6IpDefaultHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhSetIpv6DefaultHopLimit (i4SetValIpv6IpDefaultHopLimit);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4SetValIpv6IpDefaultHopLimit);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IpForwarding
 Input       :  The Indices

                The Object 
                testValIpv6IpForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IpForwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpv6IpForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6VRExists (gu4CurrContextId) == IP6_FAILURE)
    {
        IP6_TASK_UNLOCK ();
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Ipv6Forwarding (pu4ErrorCode,
                                        i4TestValIpv6IpForwarding);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValIpv6IpForwarding);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6IpDefaultHopLimit
 Input       :  The Indices

                The Object 
                testValIpv6IpDefaultHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6IpDefaultHopLimit (UINT4 *pu4ErrorCode,
                                INT4 i4TestValIpv6IpDefaultHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6VRExists (gu4CurrContextId) == IP6_FAILURE)
    {
        IP6_TASK_UNLOCK ();
        return SNMP_FAILURE;
    }
    i1RetVal = nmhTestv2Ipv6DefaultHopLimit (pu4ErrorCode,
                                             i4TestValIpv6IpDefaultHopLimit);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValIpv6IpDefaultHopLimit);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhDepv2Ipv6IpForwarding
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6IpForwarding (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ipv6IpDefaultHopLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6IpDefaultHopLimit (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv4InterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv4InterfaceTable
 Input       :  The Indices
                Ipv4InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv4InterfaceTable (INT4 i4Ipv4InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined IP_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = nmhValidateIndexInstanceFsIpifTable (i4Ipv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();
#endif

#if defined LNXIP4_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = IPvxValidateIdxInsIpv4IfTable (i4Ipv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();

#endif

#if !(defined (LNXIP4_WANTED) && defined (IP_WANTED))
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv4InterfaceTable
 Input       :  The Indices
                Ipv4InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv4InterfaceTable (INT4 *pi4Ipv4InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    i1RetVal = nmhGetNextIndexIpv4InterfaceTable (IPVX_ZERO,
                                                  pi4Ipv4InterfaceIfIndex);
#else
    UNUSED_PARAM (pi4Ipv4InterfaceIfIndex);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv4InterfaceTable
 Input       :  The Indices
                Ipv4InterfaceIfIndex
                nextIpv4InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv4InterfaceTable (INT4 i4Ipv4InterfaceIfIndex,
                                   INT4 *pi4NextIpv4InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined IP_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = nmhGetNextIndexFsIpifTable (i4Ipv4InterfaceIfIndex,
                                           pi4NextIpv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();
#endif

#if defined LNXIP4_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = IPvxGetNextIndexIpv4InterfaceTable (i4Ipv4InterfaceIfIndex,
                                                   pi4NextIpv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();
#endif

#if !(defined (LNXIP4_WANTED) && defined (IP_WANTED))
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    UNUSED_PARAM (pi4NextIpv4InterfaceIfIndex);
#endif
    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv4InterfaceReasmMaxSize
 Input       :  The Indices
                Ipv4InterfaceIfIndex

                The Object 
                retValIpv4InterfaceReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv4InterfaceReasmMaxSize (INT4 i4Ipv4InterfaceIfIndex,
                                 INT4 *pi4RetValIpv4InterfaceReasmMaxSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if defined IP_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = nmhGetFsIpifMaxReasmSize (i4Ipv4InterfaceIfIndex,
                                         pi4RetValIpv4InterfaceReasmMaxSize);
    IPFWD_PROT_UNLOCK ();
#endif

#if defined LNXIP4_WANTED
    IPFWD_PROT_LOCK ();
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    *pi4RetValIpv4InterfaceReasmMaxSize = IPVX_ZERO;
    /* No API to get Linux IP ReasmMaxSize */
    i1RetVal = SNMP_SUCCESS;
    IPFWD_PROT_UNLOCK ();
#endif

#if !(defined (LNXIP4_WANTED) && defined (IP_WANTED))
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValIpv4InterfaceReasmMaxSize);
#endif

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv4InterfaceEnableStatus
 Input       :  The Indices
                Ipv4InterfaceIfIndex

                The Object 
                retValIpv4InterfaceEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv4InterfaceEnableStatus (INT4 i4Ipv4InterfaceIfIndex,
                                 INT4 *pi4RetValIpv4InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1RetVal = IPvxGetIpv4IfEnableStatus (i4Ipv4InterfaceIfIndex,
                                          pi4RetValIpv4InterfaceEnableStatus);
    IPFWD_PROT_UNLOCK ();
#else
    i1RetVal = SNMP_SUCCESS;
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValIpv4InterfaceEnableStatus);
#endif

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv4InterfaceRetransmitTime
 Input       :  The Indices
                Ipv4InterfaceIfIndex

                The Object 
                retValIpv4InterfaceRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv4InterfaceRetransmitTime (INT4 i4Ipv4InterfaceIfIndex,
                                   UINT4 *pu4RetValIpv4InterfaceRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();

    /* ARP Retransmittion time in Milliseconds. */
    *pu4RetValIpv4InterfaceRetransmitTime =
        (ARP_REQUEST_RETRY_TIMEOUT * IPVX_THOUSAND);

    IPFWD_PROT_UNLOCK ();
    i1RetVal = SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4RetValIpv4InterfaceRetransmitTime);
#endif

    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpv4InterfaceEnableStatus
 Input       :  The Indices
                Ipv4InterfaceIfIndex

                The Object 
                setValIpv4InterfaceEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv4InterfaceEnableStatus (INT4 i4Ipv4InterfaceIfIndex,
                                 INT4 i4SetValIpv4InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if defined (IP_WANTED)

    if( (IPvxHandleIpv4IfEnableStatus (i4Ipv4InterfaceIfIndex,
	                               i4SetValIpv4InterfaceEnableStatus) == IP_SUCCESS))
    {
    	i1RetVal = SNMP_SUCCESS;
    }

#elif defined (LNXIP4_WANTED)
    IPFWD_PROT_LOCK ();
    i1RetVal = IPvxSetIpv4IfEnableStatus (i4Ipv4InterfaceIfIndex,
                                          i4SetValIpv4InterfaceEnableStatus);
    IPFWD_PROT_UNLOCK ();

#else
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    UNUSED_PARAM (i4SetValIpv4InterfaceEnableStatus);
#endif

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXIfTable (i4Ipv4InterfaceIfIndex,
                              i4SetValIpv4InterfaceEnableStatus,
                              FsMIStdIpv4InterfaceEnableStatus,
                              (sizeof (FsMIStdIpv4InterfaceEnableStatus) /
                               sizeof (UINT4)), FALSE);
    }
    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Ipv4InterfaceEnableStatus
 Input       :  The Indices
                Ipv4InterfaceIfIndex

                The Object 
                testValIpv4InterfaceEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv4InterfaceEnableStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Ipv4InterfaceIfIndex,
                                    INT4 i4TestValIpv4InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4TestValIpv4InterfaceEnableStatus != IPIF_OPER_DISABLE) &&
        (i4TestValIpv4InterfaceEnableStatus != IPIF_OPER_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (i1RetVal);
    }

#if defined IP_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = nmhValidateIndexInstanceFsIpifTable (i4Ipv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();

#endif

#if defined LNXIP4_WANTED
    IPFWD_PROT_LOCK ();
    i1RetVal = IPvxValidateIdxInsIpv4IfTable (i4Ipv4InterfaceIfIndex);
    IPFWD_PROT_UNLOCK ();
#endif

#if !(defined (LNXIP4_WANTED) && defined (IP_WANTED))
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Ipv4InterfaceIfIndex);
    UNUSED_PARAM (i4TestValIpv4InterfaceEnableStatus);
#endif
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Ipv4InterfaceTable
 Input       :  The Indices
                Ipv4InterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv4InterfaceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv6InterfaceTableLastChange
 Input       :  The Indices

                The Object 
                retValIpv6InterfaceTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceTableLastChange (UINT4
                                    *pu4RetValIpv6InterfaceTableLastChange)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal =
        nmhGetIpv6IfTableLastChange (pu4RetValIpv6InterfaceTableLastChange);

    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pu4RetValIpv6InterfaceTableLastChange);
#endif

    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : Ipv6InterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6InterfaceTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6InterfaceTable (INT4 i4Ipv6InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED

    i1RetVal = nmhValidateIndexInstanceIpv6IfTable (i4Ipv6InterfaceIfIndex);

#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6InterfaceTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6InterfaceTable (INT4 *pi4Ipv6InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexIpv6InterfaceTable (IPVX_ZERO,
                                                  pi4Ipv6InterfaceIfIndex);

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6InterfaceTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
                nextIpv6InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6InterfaceTable (INT4 i4Ipv6InterfaceIfIndex,
                                   INT4 *pi4NextIpv6InterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal = nmhGetNextIndexIpv6IfTable (i4Ipv6InterfaceIfIndex,
                                           pi4NextIpv6InterfaceIfIndex);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pi4NextIpv6InterfaceIfIndex);
#endif

    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv6InterfaceReasmMaxSize
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceReasmMaxSize (INT4 i4Ipv6InterfaceIfIndex,
                                 UINT4 *pu4RetValIpv6InterfaceReasmMaxSize)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal = nmhGetIpv6IfReasmMaxSize (i4Ipv6InterfaceIfIndex,
                                         pu4RetValIpv6InterfaceReasmMaxSize);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValIpv6InterfaceReasmMaxSize);
#endif

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6InterfaceIdentifier
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceIdentifier (INT4 i4Ipv6InterfaceIfIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValIpv6InterfaceIdentifier)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal = nmhGetIpv6IfIdentifier (i4Ipv6InterfaceIfIndex,
                                       pRetValIpv6InterfaceIdentifier);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pRetValIpv6InterfaceIdentifier);
#endif

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6InterfaceEnableStatus
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceEnableStatus (INT4 i4Ipv6InterfaceIfIndex,
                                 INT4 *pi4RetValIpv6InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal = nmhGetFsipv6IfAdminStatus (i4Ipv6InterfaceIfIndex,
                                          pi4RetValIpv6InterfaceEnableStatus);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValIpv6InterfaceEnableStatus);
#endif

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6InterfaceReachableTime
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceReachableTime (INT4 i4Ipv6InterfaceIfIndex,
                                  UINT4 *pu4RetValIpv6InterfaceReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    *pu4RetValIpv6InterfaceReachableTime =
        Ip6GetIfReachTime ((UINT4) i4Ipv6InterfaceIfIndex);
    i1RetVal = SNMP_SUCCESS;

    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValIpv6InterfaceReachableTime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6InterfaceRetransmitTime
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceRetransmitTime (INT4 i4Ipv6InterfaceIfIndex,
                                   UINT4 *pu4RetValIpv6InterfaceRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    *pu4RetValIpv6InterfaceRetransmitTime =
        Ip6GetIfReTransTime ((UINT4) i4Ipv6InterfaceIfIndex);
    i1RetVal = SNMP_SUCCESS;

    IP6_TASK_UNLOCK ();

#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pu4RetValIpv6InterfaceRetransmitTime);
#endif
    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpv6InterfaceForwarding
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                retValIpv6InterfaceForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6InterfaceForwarding (INT4 i4Ipv6InterfaceIfIndex,
                               INT4 *pi4RetValIpv6InterfaceForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    /* Aricent Ipv6 Stack is not supporting Interface level Ipv6 Forwarding
     * So it is always 'IPVX_IP6_FORW_ENABLE' only.
     */

    /* RFC 4293 says: 
     * This object is constrained by ipv6IpForwarding and is
     * ignored if ipv6IpForwarding is set to notForwarding. Those
     * systems that do not provide per-interface control of the
     * forwarding function should set this object to forwarding for
     * all interfaces and allow the ipv6IpForwarding object to
     * control the forwarding capability.
     */

    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
#ifdef IP6_WANTED
    *pi4RetValIpv6InterfaceForwarding = IPVX_IP6_FORW_ENABLE;
    i1RetVal = SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (pi4RetValIpv6InterfaceForwarding);
#endif
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpv6InterfaceEnableStatus
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                setValIpv6InterfaceEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6InterfaceEnableStatus (INT4 i4Ipv6InterfaceIfIndex,
                                 INT4 i4SetValIpv6InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED

    i1RetVal = nmhSetFsipv6IfAdminStatus (i4Ipv6InterfaceIfIndex,
                                          i4SetValIpv6InterfaceEnableStatus);

#else
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (i4SetValIpv6InterfaceEnableStatus);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6InterfaceForwarding
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                setValIpv6InterfaceForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6InterfaceForwarding (INT4 i4Ipv6InterfaceIfIndex,
                               INT4 i4SetValIpv6InterfaceForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    /* Aricent Ipv6 Stack is not supporting Interface level Ipv6 Forwarding
     * So it is always 'IPVX_IP6_FORW_ENABLE' only.
     */

    /* RFC 4293 says: 
     * This object is constrained by ipv6IpForwarding and is
     * ignored if ipv6IpForwarding is set to notForwarding. Those
     * systems that do not provide per-interface control of the
     * forwarding function should set this object to forwarding for
     * all interfaces and allow the ipv6IpForwarding object to
     * control the forwarding capability.
     */

    UNUSED_PARAM (i4SetValIpv6InterfaceForwarding);
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
#ifdef IP6_WANTED
    IncMsrForIPVXIfTable (i4Ipv6InterfaceIfIndex,
                          i4SetValIpv6InterfaceForwarding,
                          FsMIStdIpv6InterfaceForwarding,
                          (sizeof (FsMIStdIpv6InterfaceForwarding) /
                           sizeof (UINT4)), FALSE);
    i1RetVal = SNMP_SUCCESS;
#endif

    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Ipv6InterfaceEnableStatus
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                testValIpv6InterfaceEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6InterfaceEnableStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Ipv6InterfaceIfIndex,
                                    INT4 i4TestValIpv6InterfaceEnableStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal =
        nmhTestv2Fsipv6IfAdminStatus (pu4ErrorCode,
                                      i4Ipv6InterfaceIfIndex,
                                      i4TestValIpv6InterfaceEnableStatus);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
    UNUSED_PARAM (i4TestValIpv6InterfaceEnableStatus);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6InterfaceForwarding
 Input       :  The Indices
                Ipv6InterfaceIfIndex

                The Object 
                testValIpv6InterfaceForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6InterfaceForwarding (UINT4 *pu4ErrorCode,
                                  INT4 i4Ipv6InterfaceIfIndex,
                                  INT4 i4TestValIpv6InterfaceForwarding)
{
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    /* Aricent Ipv6 Stack is not supporting Interface level Ipv6 Forwarding
     * So it is always 'IPVX_IP6_FORW_ENABLE' only.
     */

    /* RFC 4293 says: 
     * This object is constrained by ipv6IpForwarding and is
     * ignored if ipv6IpForwarding is set to notForwarding. Those
     * systems that do not provide per-interface control of the
     * forwarding function should set this object to forwarding for
     * all interfaces and allow the ipv6IpForwarding object to
     * control the forwarding capability.
     */
    if ((i4TestValIpv6InterfaceForwarding != IPVX_IP6_FORW_ENABLE) &&
        (i4TestValIpv6InterfaceForwarding != IPVX_IP6_FORW_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    UNUSED_PARAM (i4Ipv6InterfaceIfIndex);
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();

    i1RetVal = nmhValidateIndexInstanceIpv6IfTable (i4Ipv6InterfaceIfIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValIpv6InterfaceForwarding == IPVX_IP6_FORW_ENABLE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    IP6_TASK_UNLOCK ();

#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValIpv6InterfaceForwarding);
#endif
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Ipv6InterfaceTable
 Input       :  The Indices
                Ipv6InterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6InterfaceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpSystemStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpSystemStatsTable
 Input       :  The Indices
                IpSystemStatsIPVersion
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpSystemStatsTable (INT4 i4IpSystemStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        if (Ip6VRExists (gu4CurrContextId) == IP6_FAILURE)
        {
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpSystemStatsTable
 Input       :  The Indices
                IpSystemStatsIPVersion
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpSystemStatsTable (INT4 *pi4IpSystemStatsIPVersion)
{

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    *pi4IpSystemStatsIPVersion = INET_ADDR_TYPE_IPV4;
    IPFWD_PROT_UNLOCK ();
    return (SNMP_SUCCESS);
#endif

#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED))) && (defined (IP6_WANTED)))
    IP6_TASK_LOCK ();
    *pi4IpSystemStatsIPVersion = INET_ADDR_TYPE_IPV6;
    IP6_TASK_UNLOCK ();
    return (SNMP_SUCCESS);
#endif
#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)) || (defined (IP6_WANTED))))
    return (SNMP_FAILURE);
#endif

}

/****************************************************************************
 Function    :  nmhGetNextIndexIpSystemStatsTable
 Input       :  The Indices
                IpSystemStatsIPVersion
                nextIpSystemStatsIPVersion
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpSystemStatsTable (INT4 i4IpSystemStatsIPVersion,
                                   INT4 *pi4NextIpSystemStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_UNKNOWN)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pi4NextIpSystemStatsIPVersion = INET_ADDR_TYPE_IPV4;
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pi4NextIpSystemStatsIPVersion = INET_ADDR_TYPE_IPV6;
        i1RetVal = SNMP_SUCCESS;
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpSystemStatsInReceives
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInReceives (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsInReceives)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpInReceives (pu4RetValIpSystemStatsInReceives);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_RECEIVES,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInReceives);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInReceives
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInReceives (INT4 i4IpSystemStatsIPVersion,
                                 tSNMP_COUNTER64_TYPE
                                 * pu8RetValIpSystemStatsHCInReceives)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_RECEIVES,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInReceives);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_RECEIVES,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInReceives);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInOctets (INT4 i4IpSystemStatsIPVersion,
                             UINT4 *pu4RetValIpSystemStatsInOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_OCTETS,
                                     (UINT1 *) pu4RetValIpSystemStatsInOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_OCTETS,
                                     (UINT1 *) pu4RetValIpSystemStatsInOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInOctets (INT4 i4IpSystemStatsIPVersion,
                               tSNMP_COUNTER64_TYPE
                               * pu8RetValIpSystemStatsHCInOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInHdrErrors
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInHdrErrors (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsInHdrErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpInHdrErrors (pu4RetValIpSystemStatsInHdrErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_HDR_ERRS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInHdrErrors);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInNoRoutes
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInNoRoutes (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsInNoRoutes)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_NO_ROUTE,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInNoRoutes);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_NO_ROUTE,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInNoRoutes);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInAddrErrors
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInAddrErrors (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsInAddrErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpInAddrErrors (pu4RetValIpSystemStatsInAddrErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_ADDR_ERRS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInAddrErrors);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInUnknownProtos
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInUnknownProtos (INT4 i4IpSystemStatsIPVersion,
                                    UINT4
                                    *pu4RetValIpSystemStatsInUnknownProtos)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal =
            nmhGetIpInUnknownProtos (pu4RetValIpSystemStatsInUnknownProtos);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_UNKNOWN_PROTOS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInUnknownProtos);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInTruncatedPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInTruncatedPkts (INT4 i4IpSystemStatsIPVersion,
                                    UINT4
                                    *pu4RetValIpSystemStatsInTruncatedPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_TRUN_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInTruncatedPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_TRUN_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInTruncatedPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInForwDatagrams
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInForwDatagrams (INT4 i4IpSystemStatsIPVersion,
                                    UINT4
                                    *pu4RetValIpSystemStatsInForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal =
            nmhGetIpForwDatagrams (pu4RetValIpSystemStatsInForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInForwDatagrams
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInForwDatagrams (INT4 i4IpSystemStatsIPVersion,
                                      tSNMP_COUNTER64_TYPE
                                      * pu8RetValIpSystemStatsHCInForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsReasmReqds
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsReasmReqds (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsReasmReqds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpReasmReqds (pu4RetValIpSystemStatsReasmReqds);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_REASM_REQDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsReasmReqds);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsReasmOKs
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsReasmOKs (INT4 i4IpSystemStatsIPVersion,
                             UINT4 *pu4RetValIpSystemStatsReasmOKs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpReasmOKs (pu4RetValIpSystemStatsReasmOKs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_REASM_OK,
                                     (UINT1 *) pu4RetValIpSystemStatsReasmOKs);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsReasmFails
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsReasmFails (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsReasmFails)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpReasmFails (pu4RetValIpSystemStatsReasmFails);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_REASM_FAILS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsReasmFails);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInDiscards
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInDiscards (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsInDiscards)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_DISCARDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInDiscards);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_DISCARDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInDiscards);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInDelivers
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInDelivers (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsInDelivers)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpInDelivers (pu4RetValIpSystemStatsInDelivers);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_DELIVERS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInDelivers);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInDelivers
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInDelivers (INT4 i4IpSystemStatsIPVersion,
                                 tSNMP_COUNTER64_TYPE
                                 * pu8RetValIpSystemStatsHCInDelivers)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_DELIVERS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInDelivers);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_DELIVERS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInDelivers);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutRequests
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutRequests (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsOutRequests)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpOutRequests (pu4RetValIpSystemStatsOutRequests);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_REQUESTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutRequests);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutRequests
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutRequests (INT4 i4IpSystemStatsIPVersion,
                                  tSNMP_COUNTER64_TYPE
                                  * pu8RetValIpSystemStatsHCOutRequests)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_REQUESTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutRequests);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_REQUESTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutRequests);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutNoRoutes
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutNoRoutes (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsOutNoRoutes)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_NO_ROUTES,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutNoRoutes);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_NO_ROUTES,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutNoRoutes);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutForwDatagrams
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutForwDatagrams (INT4 i4IpSystemStatsIPVersion,
                                     UINT4
                                     *pu4RetValIpSystemStatsOutForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutForwDatagrams
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutForwDatagrams (INT4 i4IpSystemStatsIPVersion,
                                       tSNMP_COUNTER64_TYPE
                                       *
                                       pu8RetValIpSystemStatsHCOutForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutDiscards
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutDiscards (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsOutDiscards)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_DISCARDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutDiscards);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_DISCARDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutDiscards);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutFragReqds
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutFragReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutFragReqds (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsOutFragReqds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_FRAG_REQDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragReqds);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_FRAG_REQDS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragReqds);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutFragOKs
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutFragOKs (INT4 i4IpSystemStatsIPVersion,
                               UINT4 *pu4RetValIpSystemStatsOutFragOKs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_FRAG_OK,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragOKs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_FRAG_OK,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragOKs);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutFragFails
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutFragFails (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsOutFragFails)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpFragFails (pu4RetValIpSystemStatsOutFragFails);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_FRAG_FAILS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragFails);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutFragCreates
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutFragCreates (INT4 i4IpSystemStatsIPVersion,
                                   UINT4 *pu4RetValIpSystemStatsOutFragCreates)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIpFragCreates (pu4RetValIpSystemStatsOutFragCreates);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_FRAG_CRET,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutFragCreates);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutTransmits
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutTransmits (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsOutTransmits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_TRANS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutTransmits);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_TRANS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutTransmits);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutTransmits
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutTransmits (INT4 i4IpSystemStatsIPVersion,
                                   tSNMP_COUNTER64_TYPE
                                   * pu8RetValIpSystemStatsHCOutTransmits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_TRANS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutTransmits);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_TRANS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutTransmits);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutOctets (INT4 i4IpSystemStatsIPVersion,
                              UINT4 *pu4RetValIpSystemStatsOutOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_OCTETS,
                                     (UINT1 *) pu4RetValIpSystemStatsOutOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_OCTETS,
                                     (UINT1 *) pu4RetValIpSystemStatsOutOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutOctets (INT4 i4IpSystemStatsIPVersion,
                                tSNMP_COUNTER64_TYPE
                                * pu8RetValIpSystemStatsHCOutOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInMcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInMcastPkts (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsInMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_MCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_MCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInMcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInMcastPkts (INT4 i4IpSystemStatsIPVersion,
                                  tSNMP_COUNTER64_TYPE
                                  * pu8RetValIpSystemStatsHCInMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInMcastOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInMcastOctets (INT4 i4IpSystemStatsIPVersion,
                                  UINT4 *pu4RetValIpSystemStatsInMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInMcastOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInMcastOctets (INT4 i4IpSystemStatsIPVersion,
                                    tSNMP_COUNTER64_TYPE
                                    * pu8RetValIpSystemStatsHCInMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutMcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutMcastPkts (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsOutMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutMcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutMcastPkts (INT4 i4IpSystemStatsIPVersion,
                                   tSNMP_COUNTER64_TYPE
                                   * pu8RetValIpSystemStatsHCOutMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutMcastOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutMcastOctets (INT4 i4IpSystemStatsIPVersion,
                                   UINT4 *pu4RetValIpSystemStatsOutMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutMcastOctets
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutMcastOctets (INT4 i4IpSystemStatsIPVersion,
                                     tSNMP_COUNTER64_TYPE
                                     * pu8RetValIpSystemStatsHCOutMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsInBcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsInBcastPkts (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsInBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_BCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_IN_BCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsInBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCInBcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCInBcastPkts (INT4 i4IpSystemStatsIPVersion,
                                  tSNMP_COUNTER64_TYPE
                                  * pu8RetValIpSystemStatsHCInBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_IN_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCInBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsOutBcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsOutBcastPkts (INT4 i4IpSystemStatsIPVersion,
                                 UINT4 *pu4RetValIpSystemStatsOutBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpSystemStatsOutBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsHCOutBcastPkts
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsHCOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsHCOutBcastPkts (INT4 i4IpSystemStatsIPVersion,
                                   tSNMP_COUNTER64_TYPE
                                   * pu8RetValIpSystemStatsHCOutBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (IPVX_INVALID_IFIDX, gu4CurrContextId,
                                     IPVX_STATS_HC_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpSystemStatsHCOutBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsDiscontinuityTime
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsDiscontinuityTime (INT4 i4IpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValIpSystemStatsDiscontinuityTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pu4RetValIpSystemStatsDiscontinuityTime = IPVX_IPV4_STATS_DISCON_TIME;
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pu4RetValIpSystemStatsDiscontinuityTime = IPVX_IPV6_STATS_DISCON_TIME;
        i1RetVal = SNMP_SUCCESS;
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpSystemStatsRefreshRate
 Input       :  The Indices
                IpSystemStatsIPVersion

                The Object 
                retValIpSystemStatsRefreshRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpSystemStatsRefreshRate (INT4 i4IpSystemStatsIPVersion,
                                UINT4 *pu4RetValIpSystemStatsRefreshRate)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pu4RetValIpSystemStatsRefreshRate = IPVX_IPV4_STATS_REFRESH_RATE;
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpSystemStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pu4RetValIpSystemStatsRefreshRate = IPVX_IPV6_STATS_REFRESH_RATE;
        i1RetVal = SNMP_SUCCESS;
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpIfStatsTableLastChange
 Input       :  The Indices

                The Object 
                retValIpIfStatsTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsTableLastChange (UINT4 *pu4RetValIpIfStatsTableLastChange)
{
    *pu4RetValIpIfStatsTableLastChange = gu4IpvxIfStatsTblLstChange;
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : IpIfStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpIfStatsTable
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpIfStatsTable (INT4 i4IpIfStatsIPVersion,
                                        INT4 i4IpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if defined IP_WANTED
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhValidateIndexInstanceFsIpifTable (i4IpIfStatsIfIndex);
        IPFWD_PROT_UNLOCK ();
#endif

#if defined LNXIP4_WANTED
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxValidateIdxInsIpv4IfTable (i4IpIfStatsIfIndex);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhValidateIndexInstanceIpv6IfTable (i4IpIfStatsIfIndex);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpIfStatsTable
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpIfStatsTable (INT4 *pi4IpIfStatsIPVersion,
                                INT4 *pi4IpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexIpIfStatsTable (IPVX_ZERO, pi4IpIfStatsIPVersion,
                                              IPVX_ZERO, pi4IpIfStatsIfIndex);

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpIfStatsTable
 Input       :  The Indices
                IpIfStatsIPVersion
                nextIpIfStatsIPVersion
                IpIfStatsIfIndex
                nextIpIfStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpIfStatsTable (INT4 i4IpIfStatsIPVersion,
                               INT4 *pi4NextIpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               INT4 *pi4NextIpIfStatsIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Ipv4RtTblEnd = FALSE;

    if ((i4IpIfStatsIPVersion == INET_ADDR_TYPE_UNKNOWN) ||
        (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
#if defined IP_WANTED
        i1RetVal = nmhGetNextIndexFsIpifTable (i4IpIfStatsIfIndex,
                                               pi4NextIpIfStatsIfIndex);
#endif
#if defined LNXIP4_WANTED

        i1RetVal = IPvxGetNextIndexIpv4InterfaceTable (i4IpIfStatsIfIndex,
                                                       pi4NextIpIfStatsIfIndex);
#endif
        if (i1RetVal == SNMP_SUCCESS)
        {
            /* Fill the Data */
            *pi4NextIpIfStatsIPVersion = INET_ADDR_TYPE_IPV4;
            IPFWD_PROT_UNLOCK ();
            return (SNMP_SUCCESS);
        }

        u1Ipv4RtTblEnd = TRUE;
        IPFWD_PROT_UNLOCK ();
#endif
    }

    if ((i1RetVal == SNMP_FAILURE) ||
        (i4IpIfStatsIPVersion == INET_ADDR_TYPE_UNKNOWN) ||
        (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        if (u1Ipv4RtTblEnd == TRUE)
        {
            i4IpIfStatsIfIndex = IPVX_ZERO;
        }

        i1RetVal = nmhGetNextIndexIpv6IfTable (i4IpIfStatsIfIndex,
                                               pi4NextIpIfStatsIfIndex);
        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextIpIfStatsIPVersion = INET_ADDR_TYPE_IPV6;
            IP6_TASK_UNLOCK ();
            return (SNMP_SUCCESS);
        }

        IP6_TASK_UNLOCK ();
#else
        UNUSED_PARAM (u1Ipv4RtTblEnd);
#endif
    }

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpIfStatsInReceives
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInReceives (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsInReceives)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_RECEIVES,
                                     (UINT1 *) pu4RetValIpIfStatsInReceives);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsInReceives (i4IpIfStatsIfIndex,
                                                  pu4RetValIpIfStatsInReceives);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInReceives
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInReceives (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             tSNMP_COUNTER64_TYPE
                             * pu8RetValIpIfStatsHCInReceives)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_RECEIVES,
                                     (UINT1 *) pu8RetValIpIfStatsHCInReceives);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_RECEIVES,
                                     (UINT1 *) pu8RetValIpIfStatsHCInReceives);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInOctets (INT4 i4IpIfStatsIPVersion,
                         INT4 i4IpIfStatsIfIndex,
                         UINT4 *pu4RetValIpIfStatsInOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsInOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsInOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInOctets (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValIpIfStatsHCInOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_OCTETS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_OCTETS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInHdrErrors
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInHdrErrors (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsInHdrErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_HDR_ERRS,
                                     (UINT1 *) pu4RetValIpIfStatsInHdrErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsInHdrErrors (i4IpIfStatsIfIndex,
                                            pu4RetValIpIfStatsInHdrErrors);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInNoRoutes
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInNoRoutes (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsInNoRoutes)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_NO_ROUTE,
                                     (UINT1 *) pu4RetValIpIfStatsInNoRoutes);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetIpv6IfStatsInNoRoutes (i4IpIfStatsIfIndex,
                                                pu4RetValIpIfStatsInNoRoutes);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInAddrErrors
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInAddrErrors (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsInAddrErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_ADDR_ERRS,
                                     (UINT1 *) pu4RetValIpIfStatsInAddrErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsInAddrErrors (i4IpIfStatsIfIndex,
                                             pu4RetValIpIfStatsInAddrErrors);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInUnknownProtos
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInUnknownProtos (INT4 i4IpIfStatsIPVersion,
                                INT4 i4IpIfStatsIfIndex,
                                UINT4 *pu4RetValIpIfStatsInUnknownProtos)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_UNKNOWN_PROTOS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsInUnknownProtos);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsInUnknownProtos (i4IpIfStatsIfIndex,
                                                       pu4RetValIpIfStatsInUnknownProtos);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInTruncatedPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInTruncatedPkts (INT4 i4IpIfStatsIPVersion,
                                INT4 i4IpIfStatsIfIndex,
                                UINT4 *pu4RetValIpIfStatsInTruncatedPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_TRUN_PKTS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsInTruncatedPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsInTruncatedPkts (i4IpIfStatsIfIndex,
                                                       pu4RetValIpIfStatsInTruncatedPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInForwDatagrams
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInForwDatagrams (INT4 i4IpIfStatsIPVersion,
                                INT4 i4IpIfStatsIfIndex,
                                UINT4 *pu4RetValIpIfStatsInForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsInForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsForwDatagrams (i4IpIfStatsIfIndex,
                                              pu4RetValIpIfStatsInForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInForwDatagrams
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInForwDatagrams (INT4 i4IpIfStatsIPVersion,
                                  INT4 i4IpIfStatsIfIndex,
                                  tSNMP_COUNTER64_TYPE
                                  * pu8RetValIpIfStatsHCInForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCInForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCInForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpIfStatsReasmReqds
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsReasmReqds (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsReasmReqds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_REASM_REQDS,
                                     (UINT1 *) pu4RetValIpIfStatsReasmReqds);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsReasmReqds (i4IpIfStatsIfIndex,
                                                  pu4RetValIpIfStatsReasmReqds);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsReasmOKs
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsReasmOKs (INT4 i4IpIfStatsIPVersion,
                         INT4 i4IpIfStatsIfIndex,
                         UINT4 *pu4RetValIpIfStatsReasmOKs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_REASM_OK,
                                     (UINT1 *) pu4RetValIpIfStatsReasmOKs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsReasmOKs (i4IpIfStatsIfIndex,
                                                pu4RetValIpIfStatsReasmOKs);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsReasmFails
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsReasmFails (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsReasmFails)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_REASM_FAILS,
                                     (UINT1 *) pu4RetValIpIfStatsReasmFails);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsReasmFails (i4IpIfStatsIfIndex,
                                                  pu4RetValIpIfStatsReasmFails);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInDiscards
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInDiscards (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsInDiscards)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_DISCARDS,
                                     (UINT1 *) pu4RetValIpIfStatsInDiscards);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsInDiscards (i4IpIfStatsIfIndex,
                                                  pu4RetValIpIfStatsInDiscards);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInDelivers
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInDelivers (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsInDelivers)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_DELIVERS,
                                     (UINT1 *) pu4RetValIpIfStatsInDelivers);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsInDelivers (i4IpIfStatsIfIndex,
                                                  pu4RetValIpIfStatsInDelivers);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInDelivers
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInDelivers (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             tSNMP_COUNTER64_TYPE
                             * pu8RetValIpIfStatsHCInDelivers)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_DELIVERS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInDelivers);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_DELIVERS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInDelivers);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutRequests
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutRequests (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsOutRequests)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_REQUESTS,
                                     (UINT1 *) pu4RetValIpIfStatsOutRequests);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsOutRequests (i4IpIfStatsIfIndex,
                                            pu4RetValIpIfStatsOutRequests);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutRequests
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutRequests (INT4 i4IpIfStatsIPVersion,
                              INT4 i4IpIfStatsIfIndex,
                              tSNMP_COUNTER64_TYPE
                              * pu8RetValIpIfStatsHCOutRequests)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_REQUESTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCOutRequests);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_REQUESTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCOutRequests);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutForwDatagrams
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutForwDatagrams (INT4 i4IpIfStatsIPVersion,
                                 INT4 i4IpIfStatsIfIndex,
                                 UINT4 *pu4RetValIpIfStatsOutForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsOutForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsOutForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutForwDatagrams
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutForwDatagrams (INT4 i4IpIfStatsIPVersion,
                                   INT4 i4IpIfStatsIfIndex,
                                   tSNMP_COUNTER64_TYPE
                                   * pu8RetValIpIfStatsHCOutForwDatagrams)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutForwDatagrams);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_FWD_DGRAMS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutForwDatagrams);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutDiscards
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutDiscards (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsOutDiscards)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_DISCARDS,
                                     (UINT1 *) pu4RetValIpIfStatsOutDiscards);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsOutDiscards (i4IpIfStatsIfIndex,
                                            pu4RetValIpIfStatsOutDiscards);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutFragReqds
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutFragReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutFragReqds (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsOutFragReqds)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_FRAG_REQDS,
                                     (UINT1 *) pu4RetValIpIfStatsOutFragReqds);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_FRAG_REQDS,
                                     (UINT1 *) pu4RetValIpIfStatsOutFragReqds);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutFragOKs
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutFragOKs (INT4 i4IpIfStatsIPVersion,
                           INT4 i4IpIfStatsIfIndex,
                           UINT4 *pu4RetValIpIfStatsOutFragOKs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_FRAG_OK,
                                     (UINT1 *) pu4RetValIpIfStatsOutFragOKs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsFragOKs (i4IpIfStatsIfIndex,
                                               pu4RetValIpIfStatsOutFragOKs);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutFragFails
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutFragFails (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsOutFragFails)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_FRAG_FAILS,
                                     (UINT1 *) pu4RetValIpIfStatsOutFragFails);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = nmhGetFsipv6IfStatsFragFails (i4IpIfStatsIfIndex,
                                                 pu4RetValIpIfStatsOutFragFails);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutFragCreates
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutFragCreates (INT4 i4IpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               UINT4 *pu4RetValIpIfStatsOutFragCreates)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_FRAG_CRET,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsOutFragCreates);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsFragCreates (i4IpIfStatsIfIndex,
                                            pu4RetValIpIfStatsOutFragCreates);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutTransmits
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutTransmits (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsOutTransmits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_TRANS,
                                     (UINT1 *) pu4RetValIpIfStatsOutTransmits);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_TRANS,
                                     (UINT1 *) pu4RetValIpIfStatsOutTransmits);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutTransmits
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutTransmits (INT4 i4IpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               tSNMP_COUNTER64_TYPE
                               * pu8RetValIpIfStatsHCOutTransmits)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_TRANS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutTransmits);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_TRANS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutTransmits);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutOctets (INT4 i4IpIfStatsIPVersion,
                          INT4 i4IpIfStatsIfIndex,
                          UINT4 *pu4RetValIpIfStatsOutOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsOutOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsOutOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutOctets (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValIpIfStatsHCOutOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_OCTETS,
                                     (UINT1 *) pu8RetValIpIfStatsHCOutOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_OCTETS,
                                     (UINT1 *) pu8RetValIpIfStatsHCOutOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInMcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInMcastPkts (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsInMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_MCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsInMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsInMcastPkts (i4IpIfStatsIfIndex,
                                            pu4RetValIpIfStatsInMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInMcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInMcastPkts (INT4 i4IpIfStatsIPVersion,
                              INT4 i4IpIfStatsIfIndex,
                              tSNMP_COUNTER64_TYPE
                              * pu8RetValIpIfStatsHCInMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_MCAST_PKTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_MCAST_PKTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInMcastOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInMcastOctets (INT4 i4IpIfStatsIPVersion,
                              INT4 i4IpIfStatsIfIndex,
                              UINT4 *pu4RetValIpIfStatsInMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_MCAST_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsInMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_MCAST_OCTETS,
                                     (UINT1 *) pu4RetValIpIfStatsInMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInMcastOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInMcastOctets (INT4 i4IpIfStatsIPVersion,
                                INT4 i4IpIfStatsIfIndex,
                                tSNMP_COUNTER64_TYPE
                                * pu8RetValIpIfStatsHCInMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCInMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCInMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutMcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutMcastPkts (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsOutMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_MCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsOutMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal =
            nmhGetFsipv6IfStatsOutMcastPkts (i4IpIfStatsIfIndex,
                                             pu4RetValIpIfStatsOutMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutMcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutMcastPkts (INT4 i4IpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               tSNMP_COUNTER64_TYPE
                               * pu8RetValIpIfStatsHCOutMcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutMcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_MCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutMcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutMcastOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutMcastOctets (INT4 i4IpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               UINT4 *pu4RetValIpIfStatsOutMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsOutMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu4RetValIpIfStatsOutMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutMcastOctets
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutMcastOctets (INT4 i4IpIfStatsIPVersion,
                                 INT4 i4IpIfStatsIfIndex,
                                 tSNMP_COUNTER64_TYPE
                                 * pu8RetValIpIfStatsHCOutMcastOctets)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutMcastOctets);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_MCAST_OCTETS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutMcastOctets);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsInBcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsInBcastPkts (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsInBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_IN_BCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsInBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_IN_BCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsInBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCInBcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCInBcastPkts (INT4 i4IpIfStatsIPVersion,
                              INT4 i4IpIfStatsIfIndex,
                              tSNMP_COUNTER64_TYPE
                              * pu8RetValIpIfStatsHCInBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_IN_BCAST_PKTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_IN_BCAST_PKTS,
                                     (UINT1 *) pu8RetValIpIfStatsHCInBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsOutBcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsOutBcastPkts (INT4 i4IpIfStatsIPVersion,
                             INT4 i4IpIfStatsIfIndex,
                             UINT4 *pu4RetValIpIfStatsOutBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_OUT_BCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsOutBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_OUT_BCAST_PKTS,
                                     (UINT1 *) pu4RetValIpIfStatsOutBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsHCOutBcastPkts
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsHCOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsHCOutBcastPkts (INT4 i4IpIfStatsIPVersion,
                               INT4 i4IpIfStatsIfIndex,
                               tSNMP_COUNTER64_TYPE
                               * pu8RetValIpIfStatsHCOutBcastPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = IPvxGetIpv4Stats (i4IpIfStatsIfIndex,
                                     IPVX_STATS_HC_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutBcastPkts);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        i1RetVal = IPvxGetIpv6Stats (i4IpIfStatsIfIndex, IPVX_INVALID_IFIDX,
                                     IPVX_STATS_HC_OUT_BCAST_PKTS,
                                     (UINT1 *)
                                     pu8RetValIpIfStatsHCOutBcastPkts);
        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsDiscontinuityTime
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsDiscontinuityTime (INT4 i4IpIfStatsIPVersion,
                                  INT4 i4IpIfStatsIfIndex,
                                  UINT4 *pu4RetValIpIfStatsDiscontinuityTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4IpIfStatsIfIndex);

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pu4RetValIpIfStatsDiscontinuityTime = IPVX_IPV4_STATS_DISCON_TIME;
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pu4RetValIpIfStatsDiscontinuityTime = IPVX_IPV6_STATS_DISCON_TIME;
        i1RetVal = SNMP_SUCCESS;
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpIfStatsRefreshRate
 Input       :  The Indices
                IpIfStatsIPVersion
                IpIfStatsIfIndex

                The Object 
                retValIpIfStatsRefreshRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpIfStatsRefreshRate (INT4 i4IpIfStatsIPVersion,
                            INT4 i4IpIfStatsIfIndex,
                            UINT4 *pu4RetValIpIfStatsRefreshRate)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4IpIfStatsIfIndex);

    if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pu4RetValIpIfStatsRefreshRate = IPVX_IPV4_STATS_REFRESH_RATE;
        i1RetVal = SNMP_SUCCESS;
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IpIfStatsIPVersion == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pu4RetValIpIfStatsRefreshRate = IPVX_IPV6_STATS_REFRESH_RATE;
        i1RetVal = SNMP_SUCCESS;
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : IpAddressPrefixTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpAddressPrefixTable
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpAddressPrefixTable (INT4 i4IpAddressPrefixIfIndex,
                                              INT4 i4IpAddressPrefixType,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pIpAddressPrefixPrefix,
                                              UINT4 u4IpAddressPrefixLength)
{
    if (nmhValidateIndexInstanceFsIpvxAddrPrefixTable
        (i4IpAddressPrefixIfIndex,
         i4IpAddressPrefixType,
         pIpAddressPrefixPrefix, u4IpAddressPrefixLength) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpAddressPrefixTable
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpAddressPrefixTable (INT4 *pi4IpAddressPrefixIfIndex,
                                      INT4 *pi4IpAddressPrefixType,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pIpAddressPrefixPrefix,
                                      UINT4 *pu4IpAddressPrefixLength)
{
    if (nmhGetFirstIndexFsIpvxAddrPrefixTable
        (pi4IpAddressPrefixIfIndex,
         pi4IpAddressPrefixType,
         pIpAddressPrefixPrefix, pu4IpAddressPrefixLength) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpAddressPrefixTable
 Input       :  The Indices
                IpAddressPrefixIfIndex
                nextIpAddressPrefixIfIndex
                IpAddressPrefixType
                nextIpAddressPrefixType
                IpAddressPrefixPrefix
                nextIpAddressPrefixPrefix
                IpAddressPrefixLength
                nextIpAddressPrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpAddressPrefixTable (INT4 i4IpAddressPrefixIfIndex,
                                     INT4 *pi4NextIpAddressPrefixIfIndex,
                                     INT4 i4IpAddressPrefixType,
                                     INT4 *pi4NextIpAddressPrefixType,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pIpAddressPrefixPrefix,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextIpAddressPrefixPrefix,
                                     UINT4 u4IpAddressPrefixLength,
                                     UINT4 *pu4NextIpAddressPrefixLength)
{
    if (nmhGetNextIndexFsIpvxAddrPrefixTable
        (i4IpAddressPrefixIfIndex,
         pi4NextIpAddressPrefixIfIndex,
         i4IpAddressPrefixType,
         pi4NextIpAddressPrefixType,
         pIpAddressPrefixPrefix,
         pNextIpAddressPrefixPrefix,
         u4IpAddressPrefixLength, pu4NextIpAddressPrefixLength) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpAddressPrefixOrigin
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength

                The Object 
                retValIpAddressPrefixOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressPrefixOrigin (INT4 i4IpAddressPrefixIfIndex,
                             INT4 i4IpAddressPrefixType,
                             tSNMP_OCTET_STRING_TYPE * pIpAddressPrefixPrefix,
                             UINT4 u4IpAddressPrefixLength,
                             INT4 *pi4RetValIpAddressPrefixOrigin)
{
    tIpIfRecord         IpIntfInfo;

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#if defined IP6_WANTED && !defined LNXIP6_WANTED
    tIp6AddrInfo       *pAddrInfo = NULL;
#endif

    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressPrefixType == IPV4_ADD_TYPE)
    {

        UNUSED_PARAM (pIpAddressPrefixPrefix);
        UNUSED_PARAM (u4IpAddressPrefixLength);

        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4IpAddressPrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        *pi4RetValIpAddressPrefixOrigin = pIpIntf->u1Origin;
        CFA_IPIFTBL_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    if (i4IpAddressPrefixType == IPV6_ADD_TYPE)
    {
        pAddrInfo = Ip6AddrTblGetEntry
            ((INT2) i4IpAddressPrefixIfIndex,
             (tIp6Addr *) (VOID *) pIpAddressPrefixPrefix->pu1_OctetList,
             (UINT1) u4IpAddressPrefixLength);
        if (pAddrInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValIpAddressPrefixOrigin = pAddrInfo->u1Origin;
        return SNMP_SUCCESS;
    }
#else
    *pi4RetValIpAddressPrefixOrigin = IPVX_MANUAL;
    return SNMP_SUCCESS;
#endif /* IP_WANTED */
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressPrefixOnLinkFlag
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength

                The Object 
                retValIpAddressPrefixOnLinkFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressPrefixOnLinkFlag (INT4 i4IpAddressPrefixIfIndex,
                                 INT4 i4IpAddressPrefixType,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pIpAddressPrefixPrefix,
                                 UINT4 u4IpAddressPrefixLength,
                                 INT4 *pi4RetValIpAddressPrefixOnLinkFlag)
{
    tIpIfRecord         IpIntfInfo;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressPrefixType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pIpAddressPrefixPrefix);
        UNUSED_PARAM (u4IpAddressPrefixLength);

        CFA_IPIFTBL_LOCK ();

        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4IpAddressPrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        /*Onlink flag is always true for IPv4 address */
        *pi4RetValIpAddressPrefixOnLinkFlag = TRUE;
        CFA_IPIFTBL_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressPrefixType == IPV6_ADD_TYPE)
    {
        if (nmhGetIpv6AddrPrefixOnLinkFlag (i4IpAddressPrefixIfIndex,
                                            pIpAddressPrefixPrefix,
                                            (INT4) u4IpAddressPrefixLength,
                                            pi4RetValIpAddressPrefixOnLinkFlag)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressPrefixAutonomousFlag
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength

                The Object 
                retValIpAddressPrefixAutonomousFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressPrefixAutonomousFlag (INT4 i4IpAddressPrefixIfIndex,
                                     INT4 i4IpAddressPrefixType,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pIpAddressPrefixPrefix,
                                     UINT4 u4IpAddressPrefixLength,
                                     INT4
                                     *pi4RetValIpAddressPrefixAutonomousFlag)
{
    tIpIfRecord         IpIntfInfo;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressPrefixType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pIpAddressPrefixPrefix);
        UNUSED_PARAM (u4IpAddressPrefixLength);

        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4IpAddressPrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        /*Onlink flag is always false for IPv4 address */
        *pi4RetValIpAddressPrefixAutonomousFlag = IPV4_AUTONOMOUS_FLAG;
        CFA_IPIFTBL_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressPrefixType == IPV6_ADD_TYPE)
    {
        if (nmhGetIpv6AddrPrefixAutonomousFlag
            (i4IpAddressPrefixIfIndex,
             pIpAddressPrefixPrefix,
             (INT4) u4IpAddressPrefixLength,
             pi4RetValIpAddressPrefixAutonomousFlag) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressPrefixAdvPreferredLifetime
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength

                The Object 
                retValIpAddressPrefixAdvPreferredLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIpAddressPrefixAdvPreferredLifetime
    (INT4 i4IpAddressPrefixIfIndex,
     INT4 i4IpAddressPrefixType,
     tSNMP_OCTET_STRING_TYPE * pIpAddressPrefixPrefix,
     UINT4 u4IpAddressPrefixLength,
     UINT4 *pu4RetValIpAddressPrefixAdvPreferredLifetime)
{
    tIpIfRecord         IpIntfInfo;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressPrefixType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pIpAddressPrefixPrefix);
        UNUSED_PARAM (u4IpAddressPrefixLength);

        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4IpAddressPrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        /*Preferred lifetime is always equal to default value 0xffffffff */
        *pu4RetValIpAddressPrefixAdvPreferredLifetime = DEFAULT_LIFETIME;
        CFA_IPIFTBL_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressPrefixType == IPV6_ADD_TYPE)
    {
        if (nmhGetIpv6AddrPrefixAdvPreferredLifetime
            (i4IpAddressPrefixIfIndex,
             pIpAddressPrefixPrefix,
             (INT4) u4IpAddressPrefixLength,
             pu4RetValIpAddressPrefixAdvPreferredLifetime) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressPrefixAdvValidLifetime
 Input       :  The Indices
                IpAddressPrefixIfIndex
                IpAddressPrefixType
                IpAddressPrefixPrefix
                IpAddressPrefixLength

                The Object 
                retValIpAddressPrefixAdvValidLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIpAddressPrefixAdvValidLifetime
    (INT4 i4IpAddressPrefixIfIndex,
     INT4 i4IpAddressPrefixType,
     tSNMP_OCTET_STRING_TYPE * pIpAddressPrefixPrefix,
     UINT4 u4IpAddressPrefixLength,
     UINT4 *pu4RetValIpAddressPrefixAdvValidLifetime)
{
    tIpIfRecord         IpIntfInfo;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressPrefixType == IPV4_ADD_TYPE)
    {
        UNUSED_PARAM (pIpAddressPrefixPrefix);
        UNUSED_PARAM (u4IpAddressPrefixLength);

        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4IpAddressPrefixIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        /*Preferred lifetime is always equal to default value 0xffffffff */
        *pu4RetValIpAddressPrefixAdvValidLifetime = DEFAULT_LIFETIME;
        CFA_IPIFTBL_UNLOCK ();
        return SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressPrefixType == IPV6_ADD_TYPE)
    {
        if (nmhGetIpv6AddrPrefixAdvValidLifetime
            (i4IpAddressPrefixIfIndex,
             pIpAddressPrefixPrefix,
             (INT4) u4IpAddressPrefixLength,
             pu4RetValIpAddressPrefixAdvValidLifetime) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpAddressSpinLock
 Input       :  The Indices

                The Object 
                retValIpAddressSpinLock
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressSpinLock (INT4 *pi4RetValIpAddressSpinLock)
{
    *pi4RetValIpAddressSpinLock = gi4IPvxIpAddrTblSpinLock;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpAddressSpinLock
 Input       :  The Indices

                The Object 
                setValIpAddressSpinLock
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressSpinLock (INT4 i4SetValIpAddressSpinLock)
{
    UNUSED_PARAM (i4SetValIpAddressSpinLock);
    gi4IPvxIpAddrTblSpinLock++;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IpAddressSpinLock
 Input       :  The Indices

                The Object 
                testValIpAddressSpinLock
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressSpinLock (UINT4 *pu4ErrorCode,
                            INT4 i4TestValIpAddressSpinLock)
{
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    if (i4TestValIpAddressSpinLock == gi4IPvxIpAddrTblSpinLock)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2IpAddressSpinLock
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpAddressSpinLock (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpAddressTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpAddressTable
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpAddressTable (INT4 i4IpAddressAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIpAddressAddr)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4IPAddr = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
#endif

    if ((i4IpAddressAddrType != IPV4_ADD_TYPE) &&
        (i4IpAddressAddrType != IPV6_ADD_TYPE))
    {
        return (SNMP_FAILURE);
    }
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))

    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IPAddr, pIpAddressAddr);
        if (!((CFA_IS_ADDR_CLASS_A (u4IPAddr) ?
               (IP_IS_ZERO_NETWORK (u4IPAddr) ?
                IPVX_ZERO : IPVX_ONE) : IPVX_ZERO) ||
              (CFA_IS_ADDR_CLASS_B (u4IPAddr)) ||
              (CFA_IS_ADDR_CLASS_C (u4IPAddr))))
        {
            return (SNMP_FAILURE);
        }
    }
#endif
#ifdef IP6_WANTED
    MEMSET (&Ip6Addr, IPVX_ZERO, sizeof (tIp6Addr));
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        if (Ip6VRExists (gu4CurrContextId) == IP6_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return SNMP_FAILURE;
        }

        MEMSET (&Ip6Addr, IPVX_ZERO, sizeof (tIp6Addr));
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

        /* Validate the Address. */
        if (IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_UNSPECIFIED (Ip6Addr) ||
            IS_ADDR_LOOPBACK (Ip6Addr) || IS_ADDR_V4_COMPAT (Ip6Addr))
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);
        if (pInfo == NULL)
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
#endif
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        IP6_TASK_UNLOCK ();
    }
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpAddressTable
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpAddressTable (INT4 *pi4IpAddressAddrType,
                                tSNMP_OCTET_STRING_TYPE * pIpAddressAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1V4RetVal = SNMP_FAILURE;
    INT1                i1V6RetVal = SNMP_FAILURE;
/*    INT4                i4Ifv4Index = IPVX_ZERO;  */
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    INT4                i4Ifv4Index = IPVX_ZERO;
#endif
#if (defined (LNXIP4_WANTED))
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
    INT4                i4Ifv6Index = IPVX_ZERO;
    tIp6Addr            Ip6Addr;
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pIp6AddrInfo = NULL;
#else
    tIp6Addr            NextAddr;
#endif
#endif

#ifdef IP_WANTED
    i1V4RetVal = nmhGetNextIndexFsIpifTable (IPVX_ZERO, &i4Ifv4Index);
#endif
#if defined LNXIP4_WANTED

    UINT4               u4CxtId = 0;
    UINT4               au4Ip4Ifaces[IPIF_MAX_LOGICAL_IFACES + 1];
    u4CxtId = IpvxGetCurrContext ();

    MEMSET (au4Ip4Ifaces, IP_ZERO,
            (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));

    if ((VcmGetCxtIpIfaceList (u4CxtId, au4Ip4Ifaces) == VCM_SUCCESS)
        && (au4Ip4Ifaces[0] > 0))
    {
        i4Ifv4Index = au4Ip4Ifaces[1];
        i1V4RetVal = SNMP_SUCCESS;
    }

    /*Check that interface index starts from 33 */
    if (i4Ifv4Index < IPVX_STARTING_IFINDEX)
    {
        return SNMP_FAILURE;
    }
    if ((i4Ifv4Index != IPVX_ZERO) && (i1V4RetVal != SNMP_FAILURE))
    {
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
        IpIntfInfo.u4IfIndex = (UINT4) i4Ifv4Index;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

        if ((pIpIntf == NULL) || (pIpIntf->u4IpAddr == 0))
        {
            i1V4RetVal = SNMP_FAILURE;
        }

        CFA_IPIFTBL_UNLOCK ();
    }

#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#ifndef LNXIP6_WANTED
    pIp6AddrInfo = Ip6GetNextAddressInfo (gu4CurrContextId, Ip6Addr);
    if ((pIp6AddrInfo == NULL) ||
        (pIp6AddrInfo->pIf6->pIp6Cxt->u4ContextId != gu4CurrContextId))
    {
        i1V6RetVal = SNMP_FAILURE;
    }
    else
    {
        i4Ifv6Index = pIp6AddrInfo->pIf6->u4Index;
        i1V6RetVal = SNMP_SUCCESS;
    }
#else
    if (Ip6GetNextAddressInfo (gu4CurrContextId, &Ip6Addr, &NextAddr,
                               (UINT4 *) &i4Ifv6Index) == SNMP_FAILURE)
    {
        i1V6RetVal = SNMP_FAILURE;
    }
    else
    {
        i1V6RetVal = SNMP_SUCCESS;
    }
#endif
    IP6_TASK_UNLOCK ();
#endif

    /* When both ipv4 get first address and ipv6 get first address 
     * returns failure, return failure. Else if a valid ipv4 address is
     * obtained return ipv4 address. else return ipv6 address. */

    if ((i1V4RetVal == SNMP_FAILURE) && (i1V6RetVal == SNMP_FAILURE))
    {
        i1RetVal = SNMP_FAILURE;
    }
#if defined LNXIP4_WANTED
    else if ((pIpIntf != NULL) && (i1V4RetVal == SNMP_SUCCESS))
    {
        CFA_IPIFTBL_LOCK ();
        INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr, pIpAddressAddr);
        *pi4IpAddressAddrType = IPV4_ADD_TYPE;
        CFA_IPIFTBL_UNLOCK ();
        i1RetVal = SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    if (pIp6AddrInfo != NULL)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpAddressAddr->pu1_OctetList,
                     &pIp6AddrInfo->ip6Addr);
        pIpAddressAddr->i4_Length = IPV6_ADD_LEN;
        *pi4IpAddressAddrType = IPV6_ADD_TYPE;
        IP6_TASK_UNLOCK ();
        i1RetVal = SNMP_SUCCESS;
    }
#else
    else
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpAddressAddr->pu1_OctetList,
                     &NextAddr);
        pIpAddressAddr->i4_Length = IPV6_ADD_LEN;
        *pi4IpAddressAddrType = IPV6_ADD_TYPE;
        i1RetVal = SNMP_SUCCESS;
    }
#endif
#endif
#if ((defined (IP6_WANTED)) && (!defined (LNXIP6_WANTED)))
    UNUSED_PARAM (i4Ifv6Index);
#endif
    UNUSED_PARAM (pi4IpAddressAddrType);
    UNUSED_PARAM (pIpAddressAddr);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpAddressTable
 Input       :  The Indices
                IpAddressAddrType
                nextIpAddressAddrType
                IpAddressAddr
                nextIpAddressAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpAddressTable (INT4 i4IpAddressAddrType,
                               INT4 *pi4NextIpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                               tSNMP_OCTET_STRING_TYPE * pNextIpAddressAddr)
{
    UINT4               u4CxtId = 0;
    UINT4               u4IfIndex = IPVX_ZERO;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4NextIfIndex = IPVX_ZERO;
    UINT4               u4CurCxtId = 0;
    tIpIfRecord        *pIpIntf = NULL;
#endif
    tIpIfRecord         IpIntfInfo;
#ifdef IP6_WANTED
    UINT4               u4FirstIpv6Entry = IPVX_ZERO;
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pIp6AddrInfo = NULL;
#else
    tIp6Addr            NextAddr;
    tIp6Addr            AddrOne;
    tIp6Addr            AddrTwo;
#endif
    tIp6Addr            Ipv6Add;
    INT1                i1V6RetVal = SNMP_FAILURE;

    MEMSET (&Ipv6Add, IPVX_ZERO, sizeof (tIp6Addr));
#endif
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));

    UNUSED_PARAM (u4IfIndex);
    u4CxtId = IpvxGetCurrContext ();

    /*Get the next index from IPv4 table based on the
     *address type*/
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        if (pIpIntf != NULL)
        {
            u4IfIndex = pIpIntf->u4IfIndex;
            while (u4IfIndex < IP_DEV_MAX_L3VLAN_INTF)
            {
                if (nmhGetNextIndexIfIpTable
                    ((INT4) u4IfIndex, (INT4 *) &u4NextIfIndex) != SNMP_FAILURE)
                {
                    VcmGetContextIdFromCfaIfIndex (u4NextIfIndex, &u4CurCxtId);

                    if (u4CurCxtId != u4CxtId)
                    {
                        break;
                    }

                    CFA_IPIFTBL_LOCK ();
                    IpIntfInfo.u4IfIndex = u4NextIfIndex;
                    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
                    /*Check whether the entry exists */
                    if ((pIpIntf != NULL) && (pIpIntf->u4IpAddr != IPVX_ZERO))
                    {
                        INTEGER_TO_OCTETSTRING (pIpIntf->u4IpAddr,
                                                pNextIpAddressAddr);
                        *pi4NextIpAddressAddrType = IPV4_ADD_TYPE;
                        CFA_IPIFTBL_UNLOCK ();
                        return (SNMP_SUCCESS);
                    }
                    CFA_IPIFTBL_UNLOCK ();
                }
                else
                {
                    break;
                }
                u4IfIndex = u4NextIfIndex;
            }
        }
#ifdef IP6_WANTED
        /*As all the entries in IPv4 table are walked
         *search for the IPv6 entries*/
        u4FirstIpv6Entry = IPVX_ONE;
        i4IpAddressAddrType = IPV6_ADD_TYPE;
#endif
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        if (u4FirstIpv6Entry == IPVX_ONE)
        {
            MEMSET (&Ipv6Add, IPVX_ZERO, sizeof (tIp6Addr));
        }
        else
        {
            Ip6AddrCopy (&Ipv6Add, (tIp6Addr *) (VOID *)
                         pIpAddressAddr->pu1_OctetList);
        }
        IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
        pIp6AddrInfo = Ip6GetNextAddressInfo (gu4CurrContextId, Ipv6Add);
        if ((pIp6AddrInfo == NULL) ||
            (pIp6AddrInfo->pIf6->pIp6Cxt->u4ContextId != gu4CurrContextId))
        {
            i1V6RetVal = SNMP_FAILURE;
        }
        else
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextIpAddressAddr->
                         pu1_OctetList, &pIp6AddrInfo->ip6Addr);
            pNextIpAddressAddr->i4_Length = IPV6_ADD_LEN;
            *pi4NextIpAddressAddrType = IPV6_ADD_TYPE;
            i1V6RetVal = SNMP_SUCCESS;
        }
#else
        MEMSET (&AddrOne, IPVX_ZERO, sizeof (tIp6Addr));
        MEMSET (&AddrTwo, IPVX_ZERO, sizeof (tIp6Addr));
        if (Ip6GetNextAddressInfo (gu4CurrContextId, &Ipv6Add, &NextAddr,
                                   &u4IfIndex) == SNMP_FAILURE)
        {
            i1V6RetVal = SNMP_FAILURE;
        }
        else
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextIpAddressAddr->
                         pu1_OctetList, &NextAddr);
            pNextIpAddressAddr->i4_Length = IPV6_ADD_LEN;
            *pi4NextIpAddressAddrType = IPV6_ADD_TYPE;
            Ip6AddrCopy (&AddrOne, (tIp6Addr *) (VOID *)
                         pIpAddressAddr->pu1_OctetList);
            Ip6AddrCopy (&AddrTwo, (tIp6Addr *) (VOID *)
                         pNextIpAddressAddr->pu1_OctetList);
            if ((i4IpAddressAddrType == *pi4NextIpAddressAddrType) &&
                (Ip6AddrCompare (AddrOne, AddrTwo) == 0))
            {
                i1V6RetVal = SNMP_FAILURE;
            }
            else
            {
                i1V6RetVal = SNMP_SUCCESS;
            }
        }
#endif
        IP6_TASK_UNLOCK ();
        return i1V6RetVal;
    }
#endif

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpAddressIfIndex
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressIfIndex (INT4 i4IpAddressAddrType,
                        tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                        INT4 *pi4RetValIpAddressIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressIfIndex = ((INT4) (pIpIntf->u4IfIndex));

        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);

    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressIfIndex = ((INT4) (pInfo->pIf6->u4Index));
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressIfIndex = (INT4) u4IfIndex;
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressType (INT4 i4IpAddressAddrType,
                     tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                     INT4 *pi4RetValIpAddressType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        MEMCPY ((UINT1 *) pi4RetValIpAddressType,
                &pIpIntf->u1AddrType, sizeof (UINT1));
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressType = pInfo->u1AddrType;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressType = u1AddrType;
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressPrefix
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressPrefix
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressPrefix (INT4 i4IpAddressAddrType,
                       tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                       tSNMP_OID_TYPE * pRetValIpAddressPrefix)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (pRetValIpAddressPrefix);

    u4CxtId = IpvxGetCurrContext ();
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressOrigin
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressOrigin (INT4 i4IpAddressAddrType,
                       tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                       INT4 *pi4RetValIpAddressOrigin)
{
    UINT4               u4IfIndex = IPVX_ZERO;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT1               u1AllocMethod = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (u4IfIndex);
    u4CxtId = IpvxGetCurrContext ();
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        u4IfIndex = pIpIntf->u4IfIndex;
        CFA_IPIFTBL_UNLOCK ();
        if (CfaIfGetIpAllocMethod (u4IfIndex, &u1AllocMethod) == CFA_SUCCESS)
        {
            *pi4RetValIpAddressOrigin = (INT4) u1AllocMethod;
        }
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        if (pInfo->u1Origin == IPVX_ZERO)
        {
            *pi4RetValIpAddressOrigin = IPVX_MANUAL;
        }
        else
        {
            *pi4RetValIpAddressOrigin = pInfo->u1Origin;
        }
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif /* IP_WANTED */
        *pi4RetValIpAddressOrigin = IPVX_MANUAL;
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressStatus (INT4 i4IpAddressAddrType,
                       tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                       INT4 *pi4RetValIpAddressStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        MEMCPY ((UINT1 *) pi4RetValIpAddressStatus,
                &pIpIntf->u1IpStatus, sizeof (UINT1));
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressStatus = pInfo->u1IpStatus;
#else
        if (Ip6GetAddressStatus (u4IfIndex, &Ip6Addr,
                                 pi4RetValIpAddressStatus) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressCreated
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressCreated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressCreated (INT4 i4IpAddressAddrType,
                        tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                        UINT4 *pu4RetValIpAddressCreated)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressCreated = pIpIntf->u4IpCreated;
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressCreated = pInfo->u4IpCreated;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressCreated = 0;
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressLastChanged
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressLastChanged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressLastChanged (INT4 i4IpAddressAddrType,
                            tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                            UINT4 *pu4RetValIpAddressLastChanged)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressLastChanged = pIpIntf->u4IpLastChanged;
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressLastChanged = pInfo->u4IpLastChanged;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pu4RetValIpAddressLastChanged = 0;
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressRowStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressRowStatus (INT4 i4IpAddressAddrType,
                          tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                          INT4 *pi4RetValIpAddressRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf != NULL)
        {
            MEMCPY ((UINT1 *) pi4RetValIpAddressRowStatus,
                    &pIpIntf->u1AddrRowStatus, sizeof (UINT1));
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_SUCCESS);
        }
        CFA_IPIFTBL_UNLOCK ();
        /*As the row in the table is created only when the interface is
         *configured , the get routine for the rowstatus is returned as
         *create and wait, when "pIpIntf" is NULL,so set it not-in-service
         */
        if (pIpIntf == NULL)
        {
            *pi4RetValIpAddressRowStatus = IPVX_CREATE_AND_WAIT;
            return (SNMP_SUCCESS);
        }
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        /*As the row in the table is created only when the interface is
         *configured , the get routine for the rowstaus is returned as
         *create and wait, when "pInfo" is NULL,else returns the rowstatus
         *of the address table*/

        if (pInfo == NULL)
        {
            *pi4RetValIpAddressRowStatus = IPVX_CREATE_AND_WAIT;
            IP6_TASK_UNLOCK ();
            return (SNMP_SUCCESS);
        }
        *pi4RetValIpAddressRowStatus = pInfo->u1AddrRowStatus;
#else
        if (Ip6GetAddrAdminStatus (gu4CurrContextId, &Ip6Addr,
                                   pi4RetValIpAddressRowStatus) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_SUCCESS);
        }
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetIpAddressStorageType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                retValIpAddressStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAddressStorageType (INT4 i4IpAddressAddrType,
                            tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                            INT4 *pi4RetValIpAddressStorageType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        MEMCPY ((UINT1 *) pi4RetValIpAddressStorageType,
                &pIpIntf->u1IpStorageType, sizeof (UINT1));
        CFA_IPIFTBL_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        MEMCPY ((UINT1 *) pi4RetValIpAddressStorageType,
                &pInfo->u1IpStorageType, sizeof (UINT1));
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        *pi4RetValIpAddressStorageType = IPVX_PERMANENT;
#endif
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
#endif
    return (SNMP_FAILURE);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpAddressIfIndex
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                setValIpAddressIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressIfIndex (INT4 i4IpAddressAddrType,
                        tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                        INT4 i4SetValIpAddressIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4IfCreateTime = IPVX_ZERO;
    UINT4               u4CxtId = 0;
    UINT4               u4ContextId = 0;
    tIpIfRecord         IpIntfInfo;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4IpAdd = IPVX_ZERO;
    UINT4               u4SubnetMask = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
    INT1                i1AddrVal = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED            /* Not supported in Linux IP6 implementation */
    tIp6AddrInfo       *pInfo = NULL;
#endif
#endif

    u4CxtId = IpvxGetCurrContext ();
    MEMSET (&IpIntfInfo, IPVX_ZERO, sizeof (tIpIfRecord));
    VcmGetContextIdFromCfaIfIndex (i4SetValIpAddressIfIndex, &u4ContextId);
    if (u4ContextId != u4CxtId)
    {
        return SNMP_FAILURE;
    }

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        CFA_IPIFTBL_LOCK ();
        /* Get the IP interface node from RBTree */
        IpIntfInfo.u4IfIndex = i4SetValIpAddressIfIndex;
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
        /*Check whether the entry exists */
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpAdd, pIpAddressAddr);

        /*If the IP address is configured through the Prefix
         *table,dont allow to configure the IP address through
         *the IP adress table*/
        if (pIpIntf->u4IpAddr != IPVX_ZERO)
        {
            CFA_IPIFTBL_UNLOCK ();
            return SNMP_FAILURE;
        }
        u4IfCreateTime = OsixGetSysUpTime ();
        pIpIntf->u4IpCreated = u4IfCreateTime;
        /*Set the default values of address table */
        pIpIntf->u1AddrType = IPVX_UNICAST;
        pIpIntf->u1IpStatus = IPVX_PREFERRED;
        pIpIntf->u1IpStorageType = IPVX_VOLATILE;
        CFA_IPIFTBL_UNLOCK ();

        /*When the fsprefix table is not created before creating
         *this entry, then the default mask should be 
         *used for the IP address*/
        if (pIpIntf->u1PrefixRowStatus == IPVX_ZERO)
        {
            i1AddrVal = CFA_IS_ADDR_CLASS_A (u4IpAdd) ? IPVX_ONE : IPVX_ZERO;
            if (i1AddrVal)
            {
                i1AddrVal = IP_IS_ZERO_NETWORK (u4IpAdd) ? IPVX_ZERO : IPVX_ONE;
            }
            if (i1AddrVal)
            {
                u4SubnetMask = CLASS_A_DEF_MASK;

            }
            else if (CFA_IS_ADDR_CLASS_B (u4IpAdd))
            {
                u4SubnetMask = CLASS_B_DEF_MASK;
            }
            else if (CFA_IS_ADDR_CLASS_B (u4IpAdd))
            {
                u4SubnetMask = CLASS_C_DEF_MASK;
            }
        }
        u4SubnetMask = gau4IpCidrSubnetMask[u4SubnetMask];

        if (CfaIPVXSetIpAddress
            (i4SetValIpAddressIfIndex, u4IpAdd, u4SubnetMask) != CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        i1RetVal = SNMP_SUCCESS;
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
#ifndef LNXIP6_WANTED            /* Not supported in Linux IP6 implementation */
        IP6_TASK_LOCK ();

        IPvxSetIPv6Addr (i4SetValIpAddressIfIndex,
                         *(tIp6Addr *) (VOID *) pIpAddressAddr->pu1_OctetList,
                         IPVX_ZERO, ADDR6_UNICAST);
        IP6_TASK_UNLOCK ();
        pInfo = FindIPv6AddrTableRowInCxt (u4ContextId, pIpAddressAddr,
                                           (UINT4 *) &i4SetValIpAddressIfIndex);
        IP6_TASK_LOCK ();
        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }

        u4IfCreateTime = OsixGetSysUpTime ();
        pInfo->u4IpCreated = u4IfCreateTime;
        /*Set the default values of address table */
        pInfo->u1AddrType = IPVX_UNICAST;
        pInfo->u1IpStatus = IPVX_PREFERRED;
        pInfo->u1IpStorageType = IPVX_VOLATILE;
        IP6_TASK_UNLOCK ();
#endif /* IP_WANTED */
        i1RetVal = SNMP_SUCCESS;

    }
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpAddressType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                setValIpAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressType (INT4 i4IpAddressAddrType,
                     tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                     INT4 i4SetValIpAddressType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();
    /*Storage type can be permanent or volatile
     *By default it is volatile*/
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        CFA_IPIFTBL_LOCK ();
        if (pIpIntf == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return (SNMP_FAILURE);
        }
        pIpIntf->u1AddrType = (UINT1) i4SetValIpAddressType;
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        pInfo->u1AddrType = (UINT1) i4SetValIpAddressType;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif
        IP6_TASK_UNLOCK ();
    }
#endif
    IncMsrForIPVXAddrTable (u4CxtId, i4IpAddressAddrType, pIpAddressAddr,
                            i4SetValIpAddressType, FsMIStdIpAddressType,
                            (sizeof (FsMIStdIpAddressType) / sizeof (UINT4)),
                            FALSE);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpAddressStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                setValIpAddressStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressStatus (INT4 i4IpAddressAddrType,
                       tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                       INT4 i4SetValIpAddressStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();
    /*Address status is always "preferred" */
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        if (pIpIntf == NULL)
        {
            return (SNMP_FAILURE);
        }
        CFA_IPIFTBL_LOCK ();
        pIpIntf->u1IpStatus = (UINT1) i4SetValIpAddressStatus;
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        pInfo->u1IpStatus = (UINT1) i4SetValIpAddressStatus;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif
        IP6_TASK_UNLOCK ();
    }
#endif
    IncMsrForIPVXAddrTable (u4CxtId, i4IpAddressAddrType, pIpAddressAddr,
                            i4SetValIpAddressStatus, FsMIStdIpAddressStatus,
                            (sizeof (FsMIStdIpAddressStatus) / sizeof (UINT4)),
                            FALSE);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpAddressRowStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                setValIpAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressRowStatus (INT4 i4IpAddressAddrType,
                          tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                          INT4 i4SetValIpAddressRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT1               u1AddrRowStatus = IPVX_ZERO;
    UINT4               u4IpAdd = IPVX_ZERO;
    tIpIfRecord        *pIpIntf = NULL;
#endif
    UINT4               u4CxtId = 0;
#ifdef IP6_WANTED
#ifdef IP_WANTED
    UINT4               u4PrefLen = IPVX_ZERO;
    UINT4               u4IfIndex = IPVX_ZERO;
    tIp6AddrInfo       *pInfo = NULL;
#endif
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, IPVX_ZERO, sizeof (tIp6Addr));
#endif

    u4CxtId = IpvxGetCurrContext ();

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        if ((i4SetValIpAddressRowStatus != IPVX_CREATE_AND_WAIT) &&
            (i4SetValIpAddressRowStatus != IPVX_CREATE_AND_GO))
        {
            pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
            /*Check's whether interface is created in the table
             *If the rowstatus is made active without creating the
             *interface in the table,return failure*/
            if (pIpIntf == NULL)
            {
                return SNMP_FAILURE;
            }
            CFA_IPIFTBL_LOCK ();
            u1AddrRowStatus = pIpIntf->u1AddrRowStatus;
            CFA_IPIFTBL_UNLOCK ();
        }

        switch (i4SetValIpAddressRowStatus)
        {

            case IPVX_CREATE_AND_GO:
            case IPVX_CREATE_AND_WAIT:
                break;

            case IPVX_ACTIVE:
                if (SetAddrTableRowStatusInCxt
                    (u4CxtId, (INT1) i4SetValIpAddressRowStatus,
                     i4IpAddressAddrType, pIpAddressAddr) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                break;

            case IPVX_NOT_IN_SERVICE:
            case IPVX_NOT_READY:
                if (u1AddrRowStatus != IPVX_ACTIVE)
                {
                    return SNMP_FAILURE;
                }
                if (SetAddrTableRowStatusInCxt
                    (u4CxtId, (INT1) i4SetValIpAddressRowStatus,
                     i4IpAddressAddrType, pIpAddressAddr) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                break;

            case IPVX_DESTROY:
                if (CfaIPVXSetIpAddress
                    (pIpIntf->u4IfIndex,
                     u4IpAdd, gau4IpCidrSubnetMask[IPVX_ZERO]) != CFA_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                /*Since we are deleting the Address table the corresponding
                 *Prefix table should also be deleted*/
                CFA_IPIFTBL_LOCK ();
                pIpIntf->u4IpCreated = IPVX_ZERO;
                pIpIntf->u4IpLastChanged = IPVX_ZERO;
                pIpIntf->u1IpStatus = IPVX_ZERO;
                pIpIntf->u1Origin = IPVX_ZERO;
                pIpIntf->u1IpStorageType = IPVX_ZERO;
                pIpIntf->u1AddrRowStatus = IPVX_ZERO;
                pIpIntf->u1PrefixRowStatus = IPVX_ZERO;
                pIpIntf->u4IpAddr = IPVX_ZERO;
                pIpIntf->u1SubnetMask = IPVX_ZERO;
                pIpIntf->u1SecAddrFlag = IPVX_ZERO;
                CFA_IPIFTBL_UNLOCK ();
                return (SNMP_SUCCESS);

            default:
                return SNMP_FAILURE;
        }

    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        MEMSET (&Ip6Addr, IPVX_ZERO, sizeof (tIp6Addr));
        switch (i4SetValIpAddressRowStatus)
        {
            case IPVX_CREATE_AND_GO:
            case IPVX_CREATE_AND_WAIT:
                return SNMP_SUCCESS;

            case IPVX_ACTIVE:
                if (SetAddrTableRowStatusInCxt
                    (u4CxtId, (INT1) i4SetValIpAddressRowStatus,
                     i4IpAddressAddrType, pIpAddressAddr) != IPVX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                break;

            case IPVX_NOT_IN_SERVICE:
            case IPVX_NOT_READY:
                if (SetAddrTableRowStatusInCxt
                    (u4CxtId, (INT1) i4SetValIpAddressRowStatus,
                     i4IpAddressAddrType, pIpAddressAddr) != IPVX_SUCCESS)
                {
                    return (SNMP_FAILURE);
                }

                break;

            case IPVX_DESTROY:
#ifdef IP_WANTED                /* Not supported in Linux IP6 implementation */
                /*Check's whether interface is created in the table
                 *If the rowstatus is made active without creating the
                 *interface in the table,return failure*/
                pInfo = FindIPv6AddrTableRowInCxt (u4CxtId, pIpAddressAddr,
                                                   &u4IfIndex);
                if (pInfo == NULL)
                {
                    return (SNMP_FAILURE);
                }
                IP6_TASK_LOCK ();
                Ip6AddrCopy (&Ip6Addr,
                             (tIp6Addr *) (VOID *)
                             pIpAddressAddr->pu1_OctetList);
                u4PrefLen = (UINT4) (pInfo->u1PrefLen);
                IPvxDeleteIPv6Add (u4IfIndex, Ip6Addr,
                                   u4PrefLen, ADDR6_UNICAST);

                pInfo->u4IpCreated = IPVX_ZERO;
                pInfo->u4IpLastChanged = IPVX_ZERO;
                pInfo->u1IpStatus = IPVX_ZERO;
                pInfo->u1Origin = IPVX_ZERO;
                pInfo->u1IpStorageType = IPVX_ZERO;
                if (pInfo->u1PrefixRowStatus == IPVX_ZERO)
                {
                    MEMSET (&pInfo->ip6Addr, IPVX_ZERO, sizeof (tIp6Addr));
                    pInfo->u1PrefLen = IPVX_ZERO;
                }
                pInfo->u1AddrRowStatus = IPVX_ZERO;
                IP6_TASK_UNLOCK ();
#endif /* IP_WANTED */
                break;

            default:
                return SNMP_FAILURE;
        }
    }
#endif
    IncMsrForIPVXAddrTable (u4CxtId, i4IpAddressAddrType,
                            pIpAddressAddr,
                            i4SetValIpAddressRowStatus,
                            FsMIStdIpAddressRowStatus,
                            (sizeof (FsMIStdIpAddressRowStatus) /
                             sizeof (UINT4)), TRUE);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpAddressStorageType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                setValIpAddressStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpAddressStorageType (INT4 i4IpAddressAddrType,
                            tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                            INT4 i4SetValIpAddressStorageType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    tIpIfRecord        *pIpIntf = NULL;
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6AddrInfo       *pInfo = NULL;
#else
    UINT4               u4IfIndex = 0;
    INT4                i4Prefix = 0;
    UINT1               u1AddrType = 0;
#endif
    tIp6Addr            Ip6Addr;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = IpvxGetCurrContext ();
    /*Storage type can be permanent or volatile
     *By default it is volatile*/
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        pIpIntf = FindIPv4AddrTableRowInCxt (u4CxtId, pIpAddressAddr);
        if (pIpIntf == NULL)
        {
            return (SNMP_FAILURE);
        }
        CFA_IPIFTBL_LOCK ();
        pIpIntf->u1IpStorageType = (UINT1) i4SetValIpAddressStorageType;
        CFA_IPIFTBL_UNLOCK ();
    }
#endif
#ifdef IP6_WANTED
    if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        IP6_TASK_LOCK ();
        Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                     pIpAddressAddr->pu1_OctetList);

#ifndef LNXIP6_WANTED
        pInfo = Ip6GetAddressInfo (gu4CurrContextId, Ip6Addr);

        if (pInfo == NULL)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
        pInfo->u1IpStorageType = (UINT1) i4SetValIpAddressStorageType;
#else
        if (Ip6GetAddressInfo (gu4CurrContextId, &Ip6Addr, &u4IfIndex,
                               &i4Prefix, &u1AddrType) == SNMP_FAILURE)
        {
            IP6_TASK_UNLOCK ();
            return (SNMP_FAILURE);
        }
#endif
        IP6_TASK_UNLOCK ();
    }
#endif
    IncMsrForIPVXAddrTable (u4CxtId, i4IpAddressAddrType,
                            pIpAddressAddr,
                            i4SetValIpAddressStorageType,
                            FsMIStdIpAddressStorageType,
                            (sizeof (FsMIStdIpAddressStorageType) /
                             sizeof (UINT4)), FALSE);
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IpAddressIfIndex
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                testValIpAddressIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressIfIndex (UINT4 *pu4ErrorCode,
                           INT4 i4IpAddressAddrType,
                           tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                           INT4 i4TestValIpAddressIfIndex)
{
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (i4IpAddressAddrType);
    UNUSED_PARAM (pIpAddressAddr);

    u4CxtId = IpvxGetCurrContext ();

    if (u4CxtId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist (u4CxtId) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Interface Index zero should return Wrong Vlaue.
     * */
    if (i4TestValIpAddressIfIndex == IPVX_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*Checks whether the interface is of type vlan, physical or tunnel */
    if ((CfaIsL3IpVlanInterface ((UINT4) i4TestValIpAddressIfIndex)
         != CFA_SUCCESS) &&
        (CfaIsPhysicalInterface ((UINT4) i4TestValIpAddressIfIndex)
         != CFA_TRUE) &&
        (CfaIsTunnelInterface ((UINT4) i4TestValIpAddressIfIndex) != CFA_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2IpAddressType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                testValIpAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressType (UINT4 *pu4ErrorCode,
                        INT4 i4IpAddressAddrType,
                        tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                        INT4 i4TestValIpAddressType)
{
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (i4IpAddressAddrType);
    UNUSED_PARAM (pIpAddressAddr);

    u4CxtId = IpvxGetCurrContext ();
    if (u4CxtId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIpAddressType != IPVX_UNICAST) &&
        (i4TestValIpAddressType != IPVX_ANYCAST) &&
        (i4TestValIpAddressType != IPVX_BROADCAST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (VcmIsL3VcExist (u4CxtId) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2IpAddressStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                testValIpAddressStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressStatus (UINT4 *pu4ErrorCode,
                          INT4 i4IpAddressAddrType,
                          tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                          INT4 i4TestValIpAddressStatus)
{
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (i4IpAddressAddrType);
    UNUSED_PARAM (pIpAddressAddr);

    u4CxtId = IpvxGetCurrContext ();
    if (u4CxtId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist (u4CxtId) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4IpAddressAddrType == IPV4_ADD_TYPE)
    {
        if (i4TestValIpAddressStatus == IPVX_STATUS_PREFERRED)
        {
            return (SNMP_SUCCESS);
        }
    }
#ifdef IP6_WANTED
    else if (i4IpAddressAddrType == IPV6_ADD_TYPE)
    {
        return (SNMP_SUCCESS);
    }
#endif
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2IpAddressRowStatus
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                testValIpAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressRowStatus (UINT4 *pu4ErrorCode,
                             INT4 i4IpAddressAddrType,
                             tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                             INT4 i4TestValIpAddressRowStatus)
{
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (pIpAddressAddr);
    UNUSED_PARAM (i4IpAddressAddrType);

    u4CxtId = IpvxGetCurrContext ();
    if (u4CxtId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4IpAddressAddrType != IPV4_ADD_TYPE) &&
        (i4IpAddressAddrType != IPV6_ADD_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if ((i4TestValIpAddressRowStatus < IPVX_ACTIVE) ||
        (i4TestValIpAddressRowStatus > IPVX_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    u4CxtId = IpvxGetCurrContext ();
    if (VcmIsL3VcExist (u4CxtId) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2IpAddressStorageType
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr

                The Object 
                testValIpAddressStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpAddressStorageType (UINT4 *pu4ErrorCode,
                               INT4 i4IpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                               INT4 i4TestValIpAddressStorageType)
{
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (pIpAddressAddr);
    UNUSED_PARAM (i4IpAddressAddrType);

    u4CxtId = IpvxGetCurrContext ();
    if (u4CxtId >= SYS_DEF_MAX_NUM_CONTEXTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIpAddressStorageType != IPVX_VOLATILE) &&
        (i4TestValIpAddressStorageType != IPVX_PERMANENT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (VcmIsL3VcExist (u4CxtId) == VCM_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2IpAddressTable
 Input       :  The Indices
                IpAddressAddrType
                IpAddressAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpAddressTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpNetToPhysicalTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpNetToPhysicalTable
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpNetToPhysicalTable (INT4 i4IpNetToPhysicalIfIndex,
                                              INT4
                                              i4IpNetToPhysicalNetAddressType,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pIpNetToPhysicalNetAddress)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);
        i1RetVal =
            nmhValidateIndexInstanceIpNetToMediaTable (i4IpNetToPhysicalIfIndex,
                                                       u4Ipv4Addr);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            nmhValidateIndexInstanceIpv6NetToMediaTable
            (i4IpNetToPhysicalIfIndex, pIpNetToPhysicalNetAddress);

        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpNetToPhysicalTable
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpNetToPhysicalTable (INT4 *pi4IpNetToPhysicalIfIndex,
                                      INT4 *pi4IpNetToPhysicalNetAddressType,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pIpNetToPhysicalNetAddress)
{
    UINT4               u4IpNetToMediaNetAddress = IPVX_ZERO;
    INT4                i4Ipv4NetToPhysicalIfIndex = IPVX_IPV4_INVALID_IF_IDX;
    INT1                i1v4RetVal = SNMP_FAILURE;
    INT4                i4Ipv6NetToPhysicalIfIndex = IPVX_IPV6_INVALID_IF_IDX;
    INT1                i1v6RetVal = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_FAILURE;

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    ARP_PROT_LOCK ();

    i1v4RetVal =
        nmhGetFirstIndexIpNetToMediaTable (&i4Ipv4NetToPhysicalIfIndex,
                                           &u4IpNetToMediaNetAddress);
    ARP_PROT_UNLOCK ();
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1v6RetVal =
        nmhGetFirstIndexIpv6NetToMediaTable (&i4Ipv6NetToPhysicalIfIndex,
                                             pIpNetToPhysicalNetAddress);
    IP6_TASK_UNLOCK ();
#endif

    /* Return the Lesser ifIndex Value */
    if ((i1v4RetVal == SNMP_FAILURE) && (i1v6RetVal == SNMP_FAILURE))
    {
        i1RetVal = SNMP_FAILURE;
    }
    else
    {
        if ((i4Ipv4NetToPhysicalIfIndex <= i4Ipv6NetToPhysicalIfIndex) ||
            ((i1v6RetVal == SNMP_FAILURE) && (i1v4RetVal == SNMP_SUCCESS)))
        {
            *pi4IpNetToPhysicalIfIndex = i4Ipv4NetToPhysicalIfIndex;
            *pi4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
            IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pIpNetToPhysicalNetAddress,
                                              u4IpNetToMediaNetAddress);
            i1RetVal = SNMP_SUCCESS;
        }
        else if ((i4Ipv6NetToPhysicalIfIndex < i4Ipv4NetToPhysicalIfIndex) ||
                 ((i1v4RetVal == SNMP_FAILURE) && (i1v6RetVal == SNMP_SUCCESS)))
        {
            *pi4IpNetToPhysicalIfIndex = i4Ipv6NetToPhysicalIfIndex;
            *pi4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV6;
            i1RetVal = SNMP_SUCCESS;
        }
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpNetToPhysicalTable
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                nextIpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                nextIpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress
                nextIpNetToPhysicalNetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpNetToPhysicalTable (INT4 i4IpNetToPhysicalIfIndex,
                                     INT4 *pi4NextIpNetToPhysicalIfIndex,
                                     INT4 i4IpNetToPhysicalNetAddressType,
                                     INT4 *pi4NextIpNetToPhysicalNetAddressType,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pIpNetToPhysicalNetAddress,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextIpNetToPhysicalNetAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    ARP_PROT_LOCK ();
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#endif

    i1RetVal =
        IpvxGetNextIndexIpNetToPhyTable (i4IpNetToPhysicalIfIndex,
                                         pi4NextIpNetToPhysicalIfIndex,
                                         i4IpNetToPhysicalNetAddressType,
                                         pi4NextIpNetToPhysicalNetAddressType,
                                         pIpNetToPhysicalNetAddress,
                                         pNextIpNetToPhysicalNetAddress);
#ifdef IP6_WANTED
    IP6_TASK_UNLOCK ();
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    ARP_PROT_UNLOCK ();
#endif

    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpNetToPhysicalPhysAddress
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                retValIpNetToPhysicalPhysAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpNetToPhysicalPhysAddress (INT4 i4IpNetToPhysicalIfIndex,
                                  INT4 i4IpNetToPhysicalNetAddressType,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pIpNetToPhysicalNetAddress,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pRetValIpNetToPhysicalPhysAddress)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            nmhGetIpNetToMediaPhysAddress (i4IpNetToPhysicalIfIndex,
                                           u4Ipv4Addr,
                                           pRetValIpNetToPhysicalPhysAddress);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            nmhGetIpv6NetToMediaPhysAddress (i4IpNetToPhysicalIfIndex,
                                             pIpNetToPhysicalNetAddress,
                                             pRetValIpNetToPhysicalPhysAddress);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpNetToPhysicalLastUpdated
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                retValIpNetToPhysicalLastUpdated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpNetToPhysicalLastUpdated (INT4 i4IpNetToPhysicalIfIndex,
                                  INT4 i4IpNetToPhysicalNetAddressType,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pIpNetToPhysicalNetAddress,
                                  UINT4 *pu4RetValIpNetToPhysicalLastUpdated)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            IPvxGetIPv4NetToPhyLastUpdated (i4IpNetToPhysicalIfIndex,
                                            u4Ipv4Addr,
                                            pu4RetValIpNetToPhysicalLastUpdated);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxGetIPv6NetToPhyLastUpdated (i4IpNetToPhysicalIfIndex,
                                            pIpNetToPhysicalNetAddress,
                                            pu4RetValIpNetToPhysicalLastUpdated);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpNetToPhysicalType
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                retValIpNetToPhysicalType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpNetToPhysicalType (INT4 i4IpNetToPhysicalIfIndex,
                           INT4 i4IpNetToPhysicalNetAddressType,
                           tSNMP_OCTET_STRING_TYPE * pIpNetToPhysicalNetAddress,
                           INT4 *pi4RetValIpNetToPhysicalType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal = nmhGetIpNetToMediaType (i4IpNetToPhysicalIfIndex,
                                           u4Ipv4Addr,
                                           pi4RetValIpNetToPhysicalType);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxGetIPv6NetToPhyType (i4IpNetToPhysicalIfIndex,
                                            pIpNetToPhysicalNetAddress,
                                            pi4RetValIpNetToPhysicalType);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpNetToPhysicalState
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                retValIpNetToPhysicalState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpNetToPhysicalState (INT4 i4IpNetToPhysicalIfIndex,
                            INT4 i4IpNetToPhysicalNetAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pIpNetToPhysicalNetAddress,
                            INT4 *pi4RetValIpNetToPhysicalState)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        UNUSED_PARAM (i4IpNetToPhysicalIfIndex);
        UNUSED_PARAM (pIpNetToPhysicalNetAddress);

        /* RFC:4293 - If Neighbor Unreachability Detection is not in use 
         * (e.g. for IPv4), this object is always unknown(6)."
         */
        *pi4RetValIpNetToPhysicalState = IPVX_NET_TO_PHY_STATE_UNKNOWN;
        i1RetVal = SNMP_SUCCESS;

        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxGetIpv6NetToPhyState (i4IpNetToPhysicalIfIndex,
                                             pIpNetToPhysicalNetAddress,
                                             pi4RetValIpNetToPhysicalState);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpNetToPhysicalRowStatus
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                retValIpNetToPhysicalRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpNetToPhysicalRowStatus (INT4 i4IpNetToPhysicalIfIndex,
                                INT4 i4IpNetToPhysicalNetAddressType,
                                tSNMP_OCTET_STRING_TYPE
                                * pIpNetToPhysicalNetAddress,
                                INT4 *pi4RetValIpNetToPhysicalRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            IPvxGetIpv4NetToPhyRowStatus (i4IpNetToPhysicalIfIndex,
                                          u4Ipv4Addr,
                                          pi4RetValIpNetToPhysicalRowStatus);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxGetIpv6NetToPhyRowStatus (i4IpNetToPhysicalIfIndex,
                                          pIpNetToPhysicalNetAddress,
                                          pi4RetValIpNetToPhysicalRowStatus);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpNetToPhysicalPhysAddress
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                setValIpNetToPhysicalPhysAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpNetToPhysicalPhysAddress (INT4 i4IpNetToPhysicalIfIndex,
                                  INT4 i4IpNetToPhysicalNetAddressType,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pIpNetToPhysicalNetAddress,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pSetValIpNetToPhysicalPhysAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            IPvxSetIPv4NetToPhyPhyAddr (i4IpNetToPhysicalIfIndex,
                                        u4Ipv4Addr,
                                        pSetValIpNetToPhysicalPhysAddress);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxSetIPv6NetToPhyPhyAddr (i4IpNetToPhysicalIfIndex,
                                        pIpNetToPhysicalNetAddress,
                                        pSetValIpNetToPhysicalPhysAddress);
        IP6_TASK_UNLOCK ();
#endif
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXNetToPhyTable (i4IpNetToPhysicalIfIndex,
                                    i4IpNetToPhysicalNetAddressType,
                                    pIpNetToPhysicalNetAddress,
                                    's', pSetValIpNetToPhysicalPhysAddress,
                                    FsMIStdIpNetToPhysicalPhysAddress,
                                    (sizeof (FsMIStdIpNetToPhysicalPhysAddress)
                                     / sizeof (UINT4)), FALSE);
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpNetToPhysicalType
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                setValIpNetToPhysicalType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpNetToPhysicalType (INT4 i4IpNetToPhysicalIfIndex,
                           INT4 i4IpNetToPhysicalNetAddressType,
                           tSNMP_OCTET_STRING_TYPE * pIpNetToPhysicalNetAddress,
                           INT4 i4SetValIpNetToPhysicalType)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal = IPvxSetIPv4NetToPhyType (i4IpNetToPhysicalIfIndex,
                                            u4Ipv4Addr,
                                            i4SetValIpNetToPhysicalType);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxSetIPv6NetToPhyType (i4IpNetToPhysicalIfIndex,
                                            pIpNetToPhysicalNetAddress,
                                            i4SetValIpNetToPhysicalType);
        IP6_TASK_UNLOCK ();
#endif
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXNetToPhyTable (i4IpNetToPhysicalIfIndex,
                                    i4IpNetToPhysicalNetAddressType,
                                    pIpNetToPhysicalNetAddress,
                                    'i', &i4SetValIpNetToPhysicalType,
                                    FsMIStdIpNetToPhysicalType,
                                    (sizeof (FsMIStdIpNetToPhysicalType) /
                                     sizeof (UINT4)), FALSE);

    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpNetToPhysicalRowStatus
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                setValIpNetToPhysicalRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpNetToPhysicalRowStatus (INT4 i4IpNetToPhysicalIfIndex,
                                INT4 i4IpNetToPhysicalNetAddressType,
                                tSNMP_OCTET_STRING_TYPE
                                * pIpNetToPhysicalNetAddress,
                                INT4 i4SetValIpNetToPhysicalRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            IPvxSetIpv4NetToPhyRowStatus (i4IpNetToPhysicalIfIndex,
                                          u4Ipv4Addr,
                                          i4SetValIpNetToPhysicalRowStatus);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED

        i1RetVal =
            IPvxSetIpv6NetToPhyRowStatus (i4IpNetToPhysicalIfIndex,
                                          pIpNetToPhysicalNetAddress,
                                          i4SetValIpNetToPhysicalRowStatus);
#endif
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXNetToPhyTable (i4IpNetToPhysicalIfIndex,
                                    i4IpNetToPhysicalNetAddressType,
                                    pIpNetToPhysicalNetAddress,
                                    'i', &i4SetValIpNetToPhysicalRowStatus,
                                    FsMIStdIpNetToPhysicalRowStatus,
                                    (sizeof (FsMIStdIpNetToPhysicalRowStatus) /
                                     sizeof (UINT4)), TRUE);
    }

    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IpNetToPhysicalPhysAddress
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                testValIpNetToPhysicalPhysAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpNetToPhysicalPhysAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4IpNetToPhysicalIfIndex,
                                     INT4 i4IpNetToPhysicalNetAddressType,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pIpNetToPhysicalNetAddress,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pTestValIpNetToPhysicalPhysAddress)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal =
            nmhTestv2IpNetToMediaPhysAddress (pu4ErrorCode,
                                              i4IpNetToPhysicalIfIndex,
                                              u4Ipv4Addr,
                                              pTestValIpNetToPhysicalPhysAddress);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            nmhTestv2Fsipv6NdLanCachePhysAddr (pu4ErrorCode,
                                               i4IpNetToPhysicalIfIndex,
                                               pIpNetToPhysicalNetAddress,
                                               pTestValIpNetToPhysicalPhysAddress);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IpNetToPhysicalType
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                testValIpNetToPhysicalType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpNetToPhysicalType (UINT4 *pu4ErrorCode,
                              INT4 i4IpNetToPhysicalIfIndex,
                              INT4 i4IpNetToPhysicalNetAddressType,
                              tSNMP_OCTET_STRING_TYPE
                              * pIpNetToPhysicalNetAddress,
                              INT4 i4TestValIpNetToPhysicalType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();
        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);

        i1RetVal = IPvxTestIPv4NetToPhyType (pu4ErrorCode,
                                             i4IpNetToPhysicalIfIndex,
                                             u4Ipv4Addr,
                                             i4TestValIpNetToPhysicalType);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxTestIPv6NetToPhyType (pu4ErrorCode,
                                             i4IpNetToPhysicalIfIndex,
                                             pIpNetToPhysicalNetAddress,
                                             i4TestValIpNetToPhysicalType);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IpNetToPhysicalRowStatus
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress

                The Object 
                testValIpNetToPhysicalRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpNetToPhysicalRowStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4IpNetToPhysicalIfIndex,
                                   INT4 i4IpNetToPhysicalNetAddressType,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pIpNetToPhysicalNetAddress,
                                   INT4 i4TestValIpNetToPhysicalRowStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4Ipv4Addr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        ARP_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4Ipv4Addr,
                                          pIpNetToPhysicalNetAddress);
        i1RetVal =
            IPvxTestIpv4NetToPhyRowStatus (pu4ErrorCode,
                                           i4IpNetToPhysicalIfIndex,
                                           u4Ipv4Addr,
                                           i4TestValIpNetToPhysicalRowStatus);
        ARP_PROT_UNLOCK ();
#endif
    }
    else if (i4IpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxTestIpv6NetToPhyRowStatus (pu4ErrorCode,
                                           i4IpNetToPhysicalIfIndex,
                                           pIpNetToPhysicalNetAddress,
                                           i4TestValIpNetToPhysicalRowStatus);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2IpNetToPhysicalTable
 Input       :  The Indices
                IpNetToPhysicalIfIndex
                IpNetToPhysicalNetAddressType
                IpNetToPhysicalNetAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpNetToPhysicalTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6ScopeZoneIndexTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6ScopeZoneIndexTable (INT4
                                                 i4Ipv6ScopeZoneIndexIfIndex)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    if (SNMP_SUCCESS ==
        nmhValidateIndexInstanceFsipv6IfScopeZoneMapTable
        (i4Ipv6ScopeZoneIndexIfIndex))
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6ScopeZoneIndexTable
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6ScopeZoneIndexTable (INT4 *pi4Ipv6ScopeZoneIndexIfIndex)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))

    if (SNMP_SUCCESS ==
        nmhGetFirstIndexFsipv6IfScopeZoneMapTable
        (pi4Ipv6ScopeZoneIndexIfIndex))
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (pi4Ipv6ScopeZoneIndexIfIndex);
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6ScopeZoneIndexTable
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex
                nextIpv6ScopeZoneIndexIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6ScopeZoneIndexTable (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                                        INT4 *pi4NextIpv6ScopeZoneIndexIfIndex)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    if (SNMP_SUCCESS ==
        nmhGetNextIndexFsipv6IfScopeZoneMapTable (i4Ipv6ScopeZoneIndexIfIndex,
                                                  pi4NextIpv6ScopeZoneIndexIfIndex))
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pi4NextIpv6ScopeZoneIndexIfIndex);
#endif
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexLinkLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexLinkLocal (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                                   UINT4 *pu4RetValIpv6ScopeZoneIndexLinkLocal)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_LLOCAL;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexLinkLocal = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;

    }
    *pu4RetValIpv6ScopeZoneIndexLinkLocal = i4ZoneIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexLinkLocal);
    return (SNMP_FAILURE);
#endif

}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndex3
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndex3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndex3 (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndex3)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_SUBNETLOCAL;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndex3 = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }

    *pu4RetValIpv6ScopeZoneIndex3 = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndex3);
    return (SNMP_FAILURE);
#endif

}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexAdminLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexAdminLocal (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                                    UINT4
                                    *pu4RetValIpv6ScopeZoneIndexAdminLocal)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_SUBNETLOCAL;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexAdminLocal = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }

    *pu4RetValIpv6ScopeZoneIndexAdminLocal = i4ZoneIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexAdminLocal);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexSiteLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexSiteLocal (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                                   UINT4 *pu4RetValIpv6ScopeZoneIndexSiteLocal)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_SITELOCAL;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexSiteLocal = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }

    *pu4RetValIpv6ScopeZoneIndexSiteLocal = i4ZoneIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexSiteLocal);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndex6
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndex6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndex6 (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndex6)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN6;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndex6 = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndex6 = i4ZoneIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndex6);
    return (SNMP_FAILURE);
#endif

}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndex7
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndex7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndex7 (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndex7)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN7;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndex7 = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }

    *pu4RetValIpv6ScopeZoneIndex7 = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndex7);
    return (SNMP_FAILURE);
#endif

}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexOrganizationLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexOrganizationLocal (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                                           UINT4
                                           *pu4RetValIpv6ScopeZoneIndexOrganizationLocal)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_ORGLOCAL;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexOrganizationLocal =
            IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndexOrganizationLocal = i4ZoneIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexOrganizationLocal);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndex9
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndex9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndex9 (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndex9)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGN9;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndex9 = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndex9 = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndex9);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexA
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexA (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndexA)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNA;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexA = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndexA = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexA);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexB
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexB (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndexB)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNB;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexB = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndexB = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexB);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexC
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexC (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndexC)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGNC;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexC = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    *pu4RetValIpv6ScopeZoneIndexC = i4ZoneIndex;
    return SNMP_SUCCESS;

#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexC);
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetIpv6ScopeZoneIndexD
 Input       :  The Indices
                Ipv6ScopeZoneIndexIfIndex

                The Object 
                retValIpv6ScopeZoneIndexD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6ScopeZoneIndexD (INT4 i4Ipv6ScopeZoneIndexIfIndex,
                           UINT4 *pu4RetValIpv6ScopeZoneIndexD)
{
#if ((defined (IP6_WANTED)) || (defined (LNXIP6_WANTED)))
    INT4                i4ZoneIndex = IP6_ZONE_INVALID;
    UINT1               u1Scope = ADDR6_SCOPE_UNASSIGND;

    i4ZoneIndex =
        NetIpv6GetIfScopeZoneIndex (u1Scope, i4Ipv6ScopeZoneIndexIfIndex);

    if (i4ZoneIndex == IP6_ZONE_INVALID)
    {
        *pu4RetValIpv6ScopeZoneIndexD = IP6_DEFAULT_SCOPE_ZONE_INDEX;
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4RetValIpv6ScopeZoneIndexD = i4ZoneIndex;
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4Ipv6ScopeZoneIndexIfIndex);
    UNUSED_PARAM (pu4RetValIpv6ScopeZoneIndexD);
    return (SNMP_FAILURE);
#endif
}

/* LOW LEVEL Routines for Table : IpDefaultRouterTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpDefaultRouterTable
 Input       :  The Indices
                IpDefaultRouterAddressType
                IpDefaultRouterAddress
                IpDefaultRouterIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpDefaultRouterTable (INT4 i4IpDefaultRouterAddressType,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pIpDefaultRouterAddress,
                                              INT4 i4IpDefaultRouterIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4DefRtrAddr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4DefRtrAddr,
                                          pIpDefaultRouterAddress);
        i1RetVal = IPvxValIdxInstIpv4DefRtrTbl (u4DefRtrAddr,
                                                i4IpDefaultRouterIfIndex);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6) ||
             (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxValIdxInstIpv6DefRtrTbl (pIpDefaultRouterAddress,
                                                i4IpDefaultRouterIfIndex);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpDefaultRouterTable
 Input       :  The Indices
                IpDefaultRouterAddressType
                IpDefaultRouterAddress
                IpDefaultRouterIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpDefaultRouterTable (INT4 *pi4IpDefaultRouterAddressType,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pIpDefaultRouterAddress,
                                      INT4 *pi4IpDefaultRouterIfIndex)
{

    return (nmhGetNextIndexIpDefaultRouterTable (INET_ADDR_TYPE_UNKNOWN,
                                                 pi4IpDefaultRouterAddressType,
                                                 NULL,
                                                 pIpDefaultRouterAddress,
                                                 IPVX_ZERO,
                                                 pi4IpDefaultRouterIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpDefaultRouterTable
 Input       :  The Indices
                IpDefaultRouterAddressType
                nextIpDefaultRouterAddressType
                IpDefaultRouterAddress
                nextIpDefaultRouterAddress
                IpDefaultRouterIfIndex
                nextIpDefaultRouterIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpDefaultRouterTable (INT4 i4IpDefaultRouterAddressType,
                                     INT4 *pi4NextIpDefaultRouterAddressType,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pIpDefaultRouterAddress,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextIpDefaultRouterAddress,
                                     INT4 i4IpDefaultRouterIfIndex,
                                     INT4 *pi4NextIpDefaultRouterIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4DefRtrAddr = IPVX_ZERO;
    UINT4               u4NxtDefRtrAddr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Ipv4RtTblEnd = FALSE;

    if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_UNKNOWN) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4DefRtrAddr,
                                          pIpDefaultRouterAddress);
        i1RetVal =
            IPvxGetNxtIdxIpv4DefRtrTbl (u4DefRtrAddr, &u4NxtDefRtrAddr,
                                        i4IpDefaultRouterIfIndex,
                                        pi4NextIpDefaultRouterIfIndex);

        if (i1RetVal == SNMP_SUCCESS)
        {
            /* Fill the Data */
            *pi4NextIpDefaultRouterAddressType = INET_ADDR_TYPE_IPV4;
            IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pNextIpDefaultRouterAddress,
                                              u4NxtDefRtrAddr);
            IPFWD_PROT_UNLOCK ();
            return (SNMP_SUCCESS);
        }

        u1Ipv4RtTblEnd = TRUE;
        IPFWD_PROT_UNLOCK ();
#endif
    }

    if ((i1RetVal == SNMP_FAILURE) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_UNKNOWN) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        if (u1Ipv4RtTblEnd == TRUE)
        {
            i4IpDefaultRouterIfIndex = IPVX_ZERO;
            if (pIpDefaultRouterAddress != NULL)
            {
                MEMSET (pIpDefaultRouterAddress->pu1_OctetList, IPVX_ZERO,
                        pIpDefaultRouterAddress->i4_Length);
            }
            if (pNextIpDefaultRouterAddress != NULL)
            {
                MEMSET (pNextIpDefaultRouterAddress->pu1_OctetList, IPVX_ZERO,
                        pNextIpDefaultRouterAddress->i4_Length);
            }

        }

        i1RetVal = IPvxGetNxtIdxIpv6DefRtrTbl (pIpDefaultRouterAddress,
                                               pNextIpDefaultRouterAddress,
                                               i4IpDefaultRouterIfIndex,
                                               pi4NextIpDefaultRouterIfIndex);
        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextIpDefaultRouterAddressType = INET_ADDR_TYPE_IPV6;
            IP6_TASK_UNLOCK ();
            return (SNMP_SUCCESS);
        }
        IP6_TASK_UNLOCK ();
#else
        UNUSED_PARAM (u1Ipv4RtTblEnd);
#endif
    }

    return (SNMP_FAILURE);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpDefaultRouterLifetime
 Input       :  The Indices
                IpDefaultRouterAddressType
                IpDefaultRouterAddress
                IpDefaultRouterIfIndex

                The Object 
                retValIpDefaultRouterLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpDefaultRouterLifetime (INT4 i4IpDefaultRouterAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpDefaultRouterAddress,
                               INT4 i4IpDefaultRouterIfIndex,
                               UINT4 *pu4RetValIpDefaultRouterLifetime)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4DefRtrAddr = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4DefRtrAddr,
                                          pIpDefaultRouterAddress);
        i1RetVal =
            IPvxGetIpv4DefRtrLifetime (u4DefRtrAddr,
                                       i4IpDefaultRouterIfIndex,
                                       pu4RetValIpDefaultRouterLifetime);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6) ||
             (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxGetIpv6DefRtrLifetime (pIpDefaultRouterAddress,
                                       i4IpDefaultRouterIfIndex,
                                       pu4RetValIpDefaultRouterLifetime);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIpDefaultRouterPreference
 Input       :  The Indices
                IpDefaultRouterAddressType
                IpDefaultRouterAddress
                IpDefaultRouterIfIndex

                The Object 
                retValIpDefaultRouterPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpDefaultRouterPreference (INT4 i4IpDefaultRouterAddressType,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pIpDefaultRouterAddress,
                                 INT4 i4IpDefaultRouterIfIndex,
                                 INT4 *pi4RetValIpDefaultRouterPreference)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4DefRtrAddr = IPVX_ZERO;
#endif

    if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4) ||
        (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4DefRtrAddr,
                                          pIpDefaultRouterAddress);
        i1RetVal =
            IPvxGetIpv4DefRtrPreference (u4DefRtrAddr,
                                         i4IpDefaultRouterIfIndex,
                                         (UINT4 *)
                                         pi4RetValIpDefaultRouterPreference);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if ((i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6) ||
             (i4IpDefaultRouterAddressType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal =
            IPvxGetIpv6DefRtrPreference (pIpDefaultRouterAddress,
                                         i4IpDefaultRouterIfIndex,
                                         (UINT4 *)
                                         pi4RetValIpDefaultRouterPreference);
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertSpinLock
 Input       :  The Indices

                The Object 
                retValIpv6RouterAdvertSpinLock
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertSpinLock (INT4 *pi4RetValIpv6RouterAdvertSpinLock)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    *pi4RetValIpv6RouterAdvertSpinLock = gi4IPvxIpv6RtrAdvtSpinLock;
    i1RetVal = SNMP_SUCCESS;
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pi4RetValIpv6RouterAdvertSpinLock);
#endif
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertSpinLock
 Input       :  The Indices

                The Object 
                setValIpv6RouterAdvertSpinLock
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertSpinLock (INT4 i4SetValIpv6RouterAdvertSpinLock)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UNUSED_PARAM (i4SetValIpv6RouterAdvertSpinLock);
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    gi4IPvxIpv6RtrAdvtSpinLock++;
    i1RetVal = SNMP_SUCCESS;
    IP6_TASK_UNLOCK ();
#endif
    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertSpinLock
 Input       :  The Indices

                The Object 
                testValIpv6RouterAdvertSpinLock
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertSpinLock (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValIpv6RouterAdvertSpinLock)
{
    INT1                i1RetVal = SNMP_FAILURE;

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (i4TestValIpv6RouterAdvertSpinLock == gi4IPvxIpv6RtrAdvtSpinLock)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4TestValIpv6RouterAdvertSpinLock);
#endif

    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Ipv6RouterAdvertSpinLock
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6RouterAdvertSpinLock (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6RouterAdvertTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpv6RouterAdvertTable
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpv6RouterAdvertTable (INT4 i4Ipv6RouterAdvertIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    i1RetVal =
        nmhValidateIndexInstanceFsipv6IfTable (i4Ipv6RouterAdvertIfIndex);
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpv6RouterAdvertTable
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpv6RouterAdvertTable (INT4 *pi4Ipv6RouterAdvertIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal =
        Ip6GetNextIndexRtrAdvtTable (IPVX_ZERO, pi4Ipv6RouterAdvertIfIndex);
#else
    i1RetVal = nmhGetFirstIndexFsipv6IfTable (pi4Ipv6RouterAdvertIfIndex);
#endif /* LNXIP6_WANTED */
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (pi4Ipv6RouterAdvertIfIndex);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpv6RouterAdvertTable
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex
                nextIpv6RouterAdvertIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpv6RouterAdvertTable (INT4 i4Ipv6RouterAdvertIfIndex,
                                      INT4 *pi4NextIpv6RouterAdvertIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal = Ip6GetNextIndexRtrAdvtTable (i4Ipv6RouterAdvertIfIndex,
                                            pi4NextIpv6RouterAdvertIfIndex);
#else
    i1RetVal = nmhGetNextIndexFsipv6IfTable (i4Ipv6RouterAdvertIfIndex,
                                             pi4NextIpv6RouterAdvertIfIndex);
#endif /* LNXIP6_WANTED */
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pi4NextIpv6RouterAdvertIfIndex);
#endif
    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertSendAdverts
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertSendAdverts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertSendAdverts (INT4 i4Ipv6RouterAdvertIfIndex,
                                   INT4 *pi4RetValIpv6RouterAdvertSendAdverts)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal =
        Ip6GetRtrAdvtSendAdverts (i4Ipv6RouterAdvertIfIndex,
                                  pi4RetValIpv6RouterAdvertSendAdverts);
#else
    i1RetVal = nmhGetFsipv6IfRouterAdvStatus (i4Ipv6RouterAdvertIfIndex,
                                              pi4RetValIpv6RouterAdvertSendAdverts);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pi4RetValIpv6RouterAdvertSendAdverts);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertMaxInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertMaxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertMaxInterval (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 *pu4RetValIpv6RouterAdvertMaxInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal = Ip6GetRtrAdvtMaxInterval (i4Ipv6RouterAdvertIfIndex,
                                         pu4RetValIpv6RouterAdvertMaxInterval);
#else
    i1RetVal =
        nmhGetFsipv6IfMaxRouterAdvTime (i4Ipv6RouterAdvertIfIndex,
                                        (INT4 *)
                                        pu4RetValIpv6RouterAdvertMaxInterval);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertMaxInterval);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertMinInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertMinInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertMinInterval (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 *pu4RetValIpv6RouterAdvertMinInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal =
        Ip6GetRtrAdvtMinInterval (i4Ipv6RouterAdvertIfIndex,
                                  pu4RetValIpv6RouterAdvertMinInterval);
#else
    i1RetVal = nmhGetFsipv6IfMinRouterAdvTime (i4Ipv6RouterAdvertIfIndex,
                                               (INT4 *)
                                               pu4RetValIpv6RouterAdvertMinInterval);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertMinInterval);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertManagedFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertManagedFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertManagedFlag (INT4 i4Ipv6RouterAdvertIfIndex,
                                   INT4 *pi4RetValIpv6RouterAdvertManagedFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined IP6_WANTED
    INT4                i4AdvFlag = 0;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal = Ip6GetRtrAdvtMFlag (i4Ipv6RouterAdvertIfIndex, &i4AdvFlag);
    *pi4RetValIpv6RouterAdvertManagedFlag = i4AdvFlag;
#else
    i1RetVal =
        nmhGetFsipv6IfRouterAdvFlags (i4Ipv6RouterAdvertIfIndex, &i4AdvFlag);
    if (i4AdvFlag & IP6_IF_ROUT_ADV_M_BIT)
    {
        *pi4RetValIpv6RouterAdvertManagedFlag = IPVX_TRUTH_VALUE_TRUE;
    }
    else
    {
        if ((i4AdvFlag & IP6_IF_ROUT_ADV_BOTH_BIT) == IP6_IF_ROUT_ADV_BOTH_BIT)
        {
            *pi4RetValIpv6RouterAdvertManagedFlag = IPVX_TRUTH_VALUE_TRUE;
        }
        else
        {
            *pi4RetValIpv6RouterAdvertManagedFlag = IPVX_TRUTH_VALUE_FALSE;
        }
    }
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pi4RetValIpv6RouterAdvertManagedFlag);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertOtherConfigFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertOtherConfigFlag (INT4 i4Ipv6RouterAdvertIfIndex,
                                       INT4
                                       *pi4RetValIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined IP6_WANTED
    INT4                i4AdvFlag = 0;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal = Ip6GetRtrAdvtOFlag (i4Ipv6RouterAdvertIfIndex, &i4AdvFlag);
    *pi4RetValIpv6RouterAdvertOtherConfigFlag = i4AdvFlag;
#else
    i1RetVal =
        nmhGetFsipv6IfRouterAdvFlags (i4Ipv6RouterAdvertIfIndex, &i4AdvFlag);
    if ((i4AdvFlag & IP6_IF_ROUT_ADV_NO_BIT) == IP6_IF_ROUT_ADV_NO_BIT)
    {
        *pi4RetValIpv6RouterAdvertOtherConfigFlag = IPVX_TRUTH_VALUE_FALSE;
    }
    else if ((i4AdvFlag & IP6_IF_ROUT_ADV_O_BIT) == IP6_IF_ROUT_ADV_O_BIT)
    {
        *pi4RetValIpv6RouterAdvertOtherConfigFlag = IPVX_TRUTH_VALUE_TRUE;
    }
    else
    {
        if (i4AdvFlag == IP6_IF_ROUT_ADV_BOTH_BIT)
        {
            *pi4RetValIpv6RouterAdvertOtherConfigFlag = IPVX_TRUTH_VALUE_TRUE;
        }
        else
        {
            *pi4RetValIpv6RouterAdvertOtherConfigFlag = IPVX_TRUTH_VALUE_FALSE;
        }
    }
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pi4RetValIpv6RouterAdvertOtherConfigFlag);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertLinkMTU
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertLinkMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertLinkMTU (INT4 i4Ipv6RouterAdvertIfIndex,
                               UINT4 *pu4RetValIpv6RouterAdvertLinkMTU)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6GetRtrAdvtLinkMTU (i4Ipv6RouterAdvertIfIndex,
                              pu4RetValIpv6RouterAdvertLinkMTU);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertLinkMTU);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertReachableTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertReachableTime (INT4 i4Ipv6RouterAdvertIfIndex,
                                     UINT4
                                     *pu4RetValIpv6RouterAdvertReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal =
        Ip6GetRtrAdvtReachTime (i4Ipv6RouterAdvertIfIndex,
                                pu4RetValIpv6RouterAdvertReachableTime);
#else
    i1RetVal =
        nmhGetFsipv6IfReachableTime (i4Ipv6RouterAdvertIfIndex,
                                     (INT4 *)
                                     pu4RetValIpv6RouterAdvertReachableTime);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertReachableTime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertRetransmitTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertRetransmitTime (INT4 i4Ipv6RouterAdvertIfIndex,
                                      UINT4
                                      *pu4RetValIpv6RouterAdvertRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal =
        Ip6GetRtrAdvtTxTime (i4Ipv6RouterAdvertIfIndex,
                             pu4RetValIpv6RouterAdvertRetransmitTime);
#else
    i1RetVal =
        nmhGetFsipv6IfRetransmitTime (i4Ipv6RouterAdvertIfIndex,
                                      (INT4 *)
                                      pu4RetValIpv6RouterAdvertRetransmitTime);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertRetransmitTime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertCurHopLimit
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertCurHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertCurHopLimit (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 *pu4RetValIpv6RouterAdvertCurHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
#ifndef LNXIP6_WANTED
    i1RetVal = Ip6GetRtrAdvtCurHopLimit (i4Ipv6RouterAdvertIfIndex,
                                         pu4RetValIpv6RouterAdvertCurHopLimit);
#else
    i1RetVal = nmhGetFsipv6IfHopLimit (i4Ipv6RouterAdvertIfIndex,
                                       (INT4 *)
                                       pu4RetValIpv6RouterAdvertCurHopLimit);
#endif
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertCurHopLimit);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertDefaultLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertDefaultLifetime (INT4 i4Ipv6RouterAdvertIfIndex,
                                       UINT4
                                       *pu4RetValIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6GetRtrAdvtDefLifeTime (i4Ipv6RouterAdvertIfIndex,
                                  pu4RetValIpv6RouterAdvertDefaultLifetime);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pu4RetValIpv6RouterAdvertDefaultLifetime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpv6RouterAdvertRowStatus
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                retValIpv6RouterAdvertRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpv6RouterAdvertRowStatus (INT4 i4Ipv6RouterAdvertIfIndex,
                                 INT4 *pi4RetValIpv6RouterAdvertRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6GetRtrAdvtRowStatus (i4Ipv6RouterAdvertIfIndex,
                                       pi4RetValIpv6RouterAdvertRowStatus);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (pi4RetValIpv6RouterAdvertRowStatus);
#endif
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertSendAdverts
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertSendAdverts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertSendAdverts (INT4 i4Ipv6RouterAdvertIfIndex,
                                   INT4 i4SetValIpv6RouterAdvertSendAdverts)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6SetRtrAdvtSendAdverts (i4Ipv6RouterAdvertIfIndex,
                                  i4SetValIpv6RouterAdvertSendAdverts);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4SetValIpv6RouterAdvertSendAdverts);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, i4SetValIpv6RouterAdvertSendAdverts,
             IPVX_ZERO, FsMIStdIpv6RouterAdvertSendAdverts,
             (sizeof (FsMIStdIpv6RouterAdvertSendAdverts) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertMaxInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertMaxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertMaxInterval (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 u4SetValIpv6RouterAdvertMaxInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6SetRtrAdvtMaxInterval (i4Ipv6RouterAdvertIfIndex,
                                  u4SetValIpv6RouterAdvertMaxInterval);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertMaxInterval);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable (i4Ipv6RouterAdvertIfIndex,
                                    IPVX_MINUS_ONE,
                                    u4SetValIpv6RouterAdvertMaxInterval,
                                    FsMIStdIpv6RouterAdvertMaxInterval,
                                    (sizeof (FsMIStdIpv6RouterAdvertMaxInterval)
                                     / sizeof (UINT4)), FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertMinInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertMinInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertMinInterval (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 u4SetValIpv6RouterAdvertMinInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6SetRtrAdvtMinInterval (i4Ipv6RouterAdvertIfIndex,
                                  u4SetValIpv6RouterAdvertMinInterval);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertMinInterval);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable (i4Ipv6RouterAdvertIfIndex,
                                    IPVX_MINUS_ONE,
                                    u4SetValIpv6RouterAdvertMinInterval,
                                    FsMIStdIpv6RouterAdvertMinInterval,
                                    (sizeof (FsMIStdIpv6RouterAdvertMinInterval)
                                     / sizeof (UINT4)), FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertManagedFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertManagedFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertManagedFlag (INT4 i4Ipv6RouterAdvertIfIndex,
                                   INT4 i4SetValIpv6RouterAdvertManagedFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtMFlag (i4Ipv6RouterAdvertIfIndex,
                                   i4SetValIpv6RouterAdvertManagedFlag);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4SetValIpv6RouterAdvertManagedFlag);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable (i4Ipv6RouterAdvertIfIndex,
                                    i4SetValIpv6RouterAdvertManagedFlag,
                                    IPVX_ZERO,
                                    FsMIStdIpv6RouterAdvertManagedFlag,
                                    (sizeof (FsMIStdIpv6RouterAdvertManagedFlag)
                                     / sizeof (UINT4)), FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertOtherConfigFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertOtherConfigFlag (INT4 i4Ipv6RouterAdvertIfIndex,
                                       INT4
                                       i4SetValIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtOFlag (i4Ipv6RouterAdvertIfIndex,
                                   i4SetValIpv6RouterAdvertOtherConfigFlag);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4SetValIpv6RouterAdvertOtherConfigFlag);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, i4SetValIpv6RouterAdvertOtherConfigFlag,
             IPVX_ZERO, FsMIStdIpv6RouterAdvertOtherConfigFlag,
             (sizeof (FsMIStdIpv6RouterAdvertOtherConfigFlag) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertLinkMTU
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertLinkMTU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertLinkMTU (INT4 i4Ipv6RouterAdvertIfIndex,
                               UINT4 u4SetValIpv6RouterAdvertLinkMTU)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6RouterAdvertIfIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtLinkMTU (i4Ipv6RouterAdvertIfIndex,
                                     u4SetValIpv6RouterAdvertLinkMTU);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertLinkMTU);
#endif

#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    if ((pIf6->u1RaCnf & IP6_IF_RA_ADV) == 0)
    {
        /* Indicate to ND New reachable time of the RA */
        Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
    }
#endif


    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, IPVX_MINUS_ONE,
             u4SetValIpv6RouterAdvertLinkMTU,
             FsMIStdIpv6RouterAdvertLinkMTU,
             (sizeof (FsMIStdIpv6RouterAdvertLinkMTU) / sizeof (UINT4)), FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertReachableTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertReachableTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertReachableTime (INT4 i4Ipv6RouterAdvertIfIndex,
                                     UINT4
                                     u4SetValIpv6RouterAdvertReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6RouterAdvertIfIndex);

    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtReachTime (i4Ipv6RouterAdvertIfIndex,
                                       u4SetValIpv6RouterAdvertReachableTime);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertReachableTime);
#endif
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    if ((pIf6->u1RaCnf & IP6_IF_RA_ADV) == 0)
    {
        /* Indicate to ND New reachable time of the RA */
        Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
    }
#endif

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex,
             IPVX_MINUS_ONE,
             u4SetValIpv6RouterAdvertReachableTime,
             FsMIStdIpv6RouterAdvertReachableTime,
             (sizeof (FsMIStdIpv6RouterAdvertReachableTime) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertRetransmitTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertRetransmitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertRetransmitTime (INT4 i4Ipv6RouterAdvertIfIndex,
                                      UINT4
                                      u4SetValIpv6RouterAdvertRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    tIp6If             *pIf6 = NULL;
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtTxTime (i4Ipv6RouterAdvertIfIndex,
                                    u4SetValIpv6RouterAdvertRetransmitTime);
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
            if ((Ip6CheckRAStatus (i4Ipv6RouterAdvertIfIndex)) == TRUE )
            {
                pIf6 = Ip6ifGetEntry (i4Ipv6RouterAdvertIfIndex);
                Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
            }
#endif

    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertRetransmitTime);
#endif
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
   if ((pIf6->u1RaCnf & IP6_IF_RA_ADV) == 0)
    {
        /* Indicate to ND New reachable time of the RA */
        Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
    }
#endif


    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, IPVX_MINUS_ONE,
             u4SetValIpv6RouterAdvertRetransmitTime,
             FsMIStdIpv6RouterAdvertRetransmitTime,
             (sizeof (FsMIStdIpv6RouterAdvertRetransmitTime) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertCurHopLimit
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertCurHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertCurHopLimit (INT4 i4Ipv6RouterAdvertIfIndex,
                                   UINT4 u4SetValIpv6RouterAdvertCurHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6SetRtrAdvtCurHopLimit (i4Ipv6RouterAdvertIfIndex,
                                         u4SetValIpv6RouterAdvertCurHopLimit);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertCurHopLimit);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, IPVX_MINUS_ONE,
             u4SetValIpv6RouterAdvertCurHopLimit,
             FsMIStdIpv6RouterAdvertCurHopLimit,
             (sizeof (FsMIStdIpv6RouterAdvertCurHopLimit) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertDefaultLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertDefaultLifetime (INT4 i4Ipv6RouterAdvertIfIndex,
                                       UINT4
                                       u4SetValIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;
#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    tIp6If             *pIf6 = Ip6ifGetEntry ((UINT4) i4Ipv6RouterAdvertIfIndex);
    if (pIf6 == NULL)
    {
        return IP6_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6SetRtrAdvtDefLifeTime (i4Ipv6RouterAdvertIfIndex,
                                  u4SetValIpv6RouterAdvertDefaultLifetime);
    IP6_TASK_UNLOCK ();
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4SetValIpv6RouterAdvertDefaultLifetime);
#endif

#if defined (FSIP6_WANTED) && defined(IP6_WANTED)
    if ((pIf6->u1RaCnf & IP6_IF_RA_ADV) == 0)
    {
        /* Indicate to ND New reachable time of the RA */
        Nd6ActOnRaCnf (pIf6, IP6_IF_RA_ADV);
    }
#endif

    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable
            (i4Ipv6RouterAdvertIfIndex, IPVX_MINUS_ONE,
             u4SetValIpv6RouterAdvertDefaultLifetime,
             FsMIStdIpv6RouterAdvertDefaultLifetime,
             (sizeof (FsMIStdIpv6RouterAdvertDefaultLifetime) / sizeof (UINT4)),
             FALSE);
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetIpv6RouterAdvertRowStatus
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                setValIpv6RouterAdvertRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpv6RouterAdvertRowStatus (INT4 i4Ipv6RouterAdvertIfIndex,
                                 INT4 i4SetValIpv6RouterAdvertRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    i1RetVal = Ip6SetRtrAdvtRowStatus (i4Ipv6RouterAdvertIfIndex,
                                       i4SetValIpv6RouterAdvertRowStatus);
#else
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4SetValIpv6RouterAdvertRowStatus);
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        IncMsrForIPVXRtrAdverTable (i4Ipv6RouterAdvertIfIndex,
                                    i4SetValIpv6RouterAdvertRowStatus,
                                    IPVX_ZERO, FsMIStdIpv6RouterAdvertRowStatus,
                                    (sizeof (FsMIStdIpv6RouterAdvertRowStatus) /
                                     sizeof (UINT4)), TRUE);
    }

    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertSendAdverts
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertSendAdverts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertSendAdverts (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RouterAdvertIfIndex,
                                      INT4 i4TestValIpv6RouterAdvertSendAdverts)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtSendAdverts (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                   i4TestValIpv6RouterAdvertSendAdverts);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4TestValIpv6RouterAdvertSendAdverts);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertMaxInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertMaxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertMaxInterval (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RouterAdvertIfIndex,
                                      UINT4
                                      u4TestValIpv6RouterAdvertMaxInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtMaxInterval (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                   u4TestValIpv6RouterAdvertMaxInterval);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertMaxInterval);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertMinInterval
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertMinInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertMinInterval (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RouterAdvertIfIndex,
                                      UINT4
                                      u4TestValIpv6RouterAdvertMinInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtMinInterval (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                   u4TestValIpv6RouterAdvertMinInterval);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertMinInterval);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertManagedFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertManagedFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertManagedFlag (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RouterAdvertIfIndex,
                                      INT4 i4TestValIpv6RouterAdvertManagedFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtMFlag (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                             i4TestValIpv6RouterAdvertManagedFlag);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4TestValIpv6RouterAdvertManagedFlag);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertOtherConfigFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertOtherConfigFlag (UINT4 *pu4ErrorCode,
                                          INT4 i4Ipv6RouterAdvertIfIndex,
                                          INT4
                                          i4TestValIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtOFlag (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                             i4TestValIpv6RouterAdvertOtherConfigFlag);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4TestValIpv6RouterAdvertOtherConfigFlag);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertLinkMTU
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertLinkMTU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertLinkMTU (UINT4 *pu4ErrorCode,
                                  INT4 i4Ipv6RouterAdvertIfIndex,
                                  UINT4 u4TestValIpv6RouterAdvertLinkMTU)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtLinkMTU (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                               u4TestValIpv6RouterAdvertLinkMTU);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertLinkMTU);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertReachableTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertReachableTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertReachableTime (UINT4 *pu4ErrorCode,
                                        INT4 i4Ipv6RouterAdvertIfIndex,
                                        UINT4
                                        u4TestValIpv6RouterAdvertReachableTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtReachTime (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                 u4TestValIpv6RouterAdvertReachableTime);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertReachableTime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertRetransmitTime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertRetransmitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertRetransmitTime (UINT4 *pu4ErrorCode,
                                         INT4 i4Ipv6RouterAdvertIfIndex,
                                         UINT4
                                         u4TestValIpv6RouterAdvertRetransmitTime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtTxTime (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                              u4TestValIpv6RouterAdvertRetransmitTime);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertRetransmitTime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertCurHopLimit
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertCurHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertCurHopLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RouterAdvertIfIndex,
                                      UINT4
                                      u4TestValIpv6RouterAdvertCurHopLimit)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtCurHopLimit (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                   u4TestValIpv6RouterAdvertCurHopLimit);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertCurHopLimit);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertDefaultLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertDefaultLifetime (UINT4 *pu4ErrorCode,
                                          INT4 i4Ipv6RouterAdvertIfIndex,
                                          UINT4
                                          u4TestValIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef LNXIP6_WANTED
    if (CheckIp6IsFwdEnabled (i4Ipv6RouterAdvertIfIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal =
        Ip6TestRtrAdvtDefLifeTime (pu4ErrorCode, i4Ipv6RouterAdvertIfIndex,
                                   u4TestValIpv6RouterAdvertDefaultLifetime);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (u4TestValIpv6RouterAdvertDefaultLifetime);
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2Ipv6RouterAdvertRowStatus
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex

                The Object 
                testValIpv6RouterAdvertRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ipv6RouterAdvertRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Ipv6RouterAdvertIfIndex,
                                    INT4 i4TestValIpv6RouterAdvertRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i1RetVal = Ip6TestRtrAdvtRowStatus (pu4ErrorCode,
                                        i4Ipv6RouterAdvertIfIndex,
                                        i4TestValIpv6RouterAdvertRowStatus);
    IP6_TASK_UNLOCK ();
#else
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Ipv6RouterAdvertIfIndex);
    UNUSED_PARAM (i4TestValIpv6RouterAdvertRowStatus);
#endif
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Ipv6RouterAdvertTable
 Input       :  The Indices
                Ipv6RouterAdvertIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ipv6RouterAdvertTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IcmpStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIcmpStatsTable
 Input       :  The Indices
                IcmpStatsIPVersion
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIcmpStatsTable (INT4 i4IcmpStatsIPVersion)
{
    if (i4IcmpStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        return (SNMP_SUCCESS);
#endif
    }
    if (i4IcmpStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        if (Ip6VRExists (gu4CurrContextId) == IP6_SUCCESS)
        {
            return (SNMP_SUCCESS);
        }
#endif
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIcmpStatsTable
 Input       :  The Indices
                IcmpStatsIPVersion
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIcmpStatsTable (INT4 *pi4IcmpStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pi4IcmpStatsIPVersion);
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    *pi4IcmpStatsIPVersion = INET_VERSION_IPV4;
    IPFWD_PROT_UNLOCK ();
    i1RetVal = SNMP_SUCCESS;
#endif

#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)) || (defined (IP6_WANTED))))
    IP6_TASK_LOCK ();
    *pi4IcmpStatsIPVersion = INET_VERSION_IPV6;
    IP6_TASK_UNLOCK ();
    i1RetVal = SNMP_SUCCESS;
#endif
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIcmpStatsTable
 Input       :  The Indices
                IcmpStatsIPVersion
                nextIcmpStatsIPVersion
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIcmpStatsTable (INT4 i4IcmpStatsIPVersion,
                               INT4 *pi4NextIcmpStatsIPVersion)
{
    if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_UNKNOWN)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        *pi4NextIcmpStatsIPVersion = INET_ADDR_TYPE_IPV4;
        IPFWD_PROT_UNLOCK ();
        return (SNMP_SUCCESS);
#endif
    }
    else if (i4IcmpStatsIPVersion == INET_ADDR_TYPE_IPV4)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        *pi4NextIcmpStatsIPVersion = INET_VERSION_IPV6;
        IP6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
#endif
    }

    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIcmpStatsInMsgs
 Input       :  The Indices
                IcmpStatsIPVersion

                The Object 
                retValIcmpStatsInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpStatsInMsgs (INT4 i4IcmpStatsIPVersion,
                       UINT4 *pu4RetValIcmpStatsInMsgs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIcmpInMsgs (pu4RetValIcmpStatsInMsgs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext (gu4CurrContextId) == SNMP_FAILURE)
        {
            return SNMP_SUCCESS;
        }
        i1RetVal = nmhGetFsipv6IcmpInMsgs (pu4RetValIcmpStatsInMsgs);
        Ip6ReleaseContext ();
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpStatsInErrors
 Input       :  The Indices
                IcmpStatsIPVersion

                The Object 
                retValIcmpStatsInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpStatsInErrors (INT4 i4IcmpStatsIPVersion,
                         UINT4 *pu4RetValIcmpStatsInErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIcmpInErrors (pu4RetValIcmpStatsInErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext (gu4CurrContextId) == SNMP_FAILURE)
        {
            return SNMP_SUCCESS;
        }
        i1RetVal = nmhGetFsipv6IcmpInErrors (pu4RetValIcmpStatsInErrors);
        Ip6ReleaseContext ();
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpStatsOutMsgs
 Input       :  The Indices
                IcmpStatsIPVersion

                The Object 
                retValIcmpStatsOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpStatsOutMsgs (INT4 i4IcmpStatsIPVersion,
                        UINT4 *pu4RetValIcmpStatsOutMsgs)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIcmpOutMsgs (pu4RetValIcmpStatsOutMsgs);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext (gu4CurrContextId) == SNMP_FAILURE)
        {
            return SNMP_SUCCESS;
        }
        i1RetVal = nmhGetFsipv6IcmpOutMsgs (pu4RetValIcmpStatsOutMsgs);
        Ip6ReleaseContext ();
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpStatsOutErrors
 Input       :  The Indices
                IcmpStatsIPVersion

                The Object 
                retValIcmpStatsOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpStatsOutErrors (INT4 i4IcmpStatsIPVersion,
                          UINT4 *pu4RetValIcmpStatsOutErrors)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        i1RetVal = nmhGetIcmpOutErrors (pu4RetValIcmpStatsOutErrors);
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext (gu4CurrContextId) == SNMP_FAILURE)
        {
            return SNMP_SUCCESS;
        }
        i1RetVal = nmhGetFsipv6IcmpOutErrors (pu4RetValIcmpStatsOutErrors);
        Ip6ReleaseContext ();
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : IcmpMsgStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIcmpMsgStatsTable
 Input       :  The Indices
                IcmpMsgStatsIPVersion
                IcmpMsgStatsType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIcmpMsgStatsTable (INT4 i4IcmpMsgStatsIPVersion,
                                           INT4 i4IcmpMsgStatsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();
        if ((i4IcmpMsgStatsType >= ICMP_TYPE_MIN_VAL) &&
            (i4IcmpMsgStatsType < ICMP_TYPE_MAX_VAL))
        {
            i1RetVal = SNMP_SUCCESS;
        }
        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6VRExists (gu4CurrContextId) == IP6_SUCCESS)
        {
            i1RetVal = IPvxValidateIpv6IcmpMsgType (i4IcmpMsgStatsType);
        }
        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIcmpMsgStatsTable
 Input       :  The Indices
                IcmpMsgStatsIPVersion
                IcmpMsgStatsType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIcmpMsgStatsTable (INT4 *pi4IcmpMsgStatsIPVersion,
                                   INT4 *pi4IcmpMsgStatsType)
{

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    *pi4IcmpMsgStatsIPVersion = INET_VERSION_IPV4;
    *pi4IcmpMsgStatsType = ICMP_TYPE_MIN_VAL;
    IPFWD_PROT_UNLOCK ();
    return (SNMP_SUCCESS);
#endif

#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED))) && (defined (IP6_WANTED)))
    IP6_TASK_LOCK ();
    *pi4IcmpMsgStatsIPVersion = INET_VERSION_IPV6;
    *pi4IcmpMsgStatsType = ICMP6_DEST_UNREACHABLE;
    IP6_TASK_UNLOCK ();
    return (SNMP_SUCCESS);
#endif
#if (!((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)) || (defined (IP6_WANTED))))
    return (SNMP_FAILURE);
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexIcmpMsgStatsTable
 Input       :  The Indices
                IcmpMsgStatsIPVersion
                nextIcmpMsgStatsIPVersion
                IcmpMsgStatsType
                nextIcmpMsgStatsType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIcmpMsgStatsTable (INT4 i4IcmpMsgStatsIPVersion,
                                  INT4 *pi4NextIcmpMsgStatsIPVersion,
                                  INT4 i4IcmpMsgStatsType,
                                  INT4 *pi4NextIcmpMsgStatsType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Ipv4IcmpTblEnd = FALSE;

    if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        if ((i4IcmpMsgStatsType + IPVX_ONE) < ICMP_TYPE_MAX_VAL)
        {
            *pi4NextIcmpMsgStatsType = i4IcmpMsgStatsType + IPVX_ONE;
            *pi4NextIcmpMsgStatsIPVersion = INET_VERSION_IPV4;
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            u1Ipv4IcmpTblEnd = TRUE;
        }

        IPFWD_PROT_UNLOCK ();
#endif
    }

    if ((i4IcmpMsgStatsIPVersion == INET_VERSION_IPV6) ||
        (u1Ipv4IcmpTblEnd == TRUE))
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        *pi4NextIcmpMsgStatsIPVersion = INET_VERSION_IPV6;

        if (u1Ipv4IcmpTblEnd == TRUE)
        {
            *pi4NextIcmpMsgStatsType = ICMP6_DEST_UNREACHABLE;
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = IPvxGetNxtIpv6IcmpMsgType (i4IcmpMsgStatsType,
                                                  pi4NextIcmpMsgStatsType);
        }

        IP6_TASK_UNLOCK ();
#endif
    }

    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIcmpMsgStatsInPkts
 Input       :  The Indices
                IcmpMsgStatsIPVersion
                IcmpMsgStatsType

                The Object 
                retValIcmpMsgStatsInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpMsgStatsInPkts (INT4 i4IcmpMsgStatsIPVersion,
                          INT4 i4IcmpMsgStatsType,
                          UINT4 *pu4RetValIcmpMsgStatsInPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        i1RetVal = IPvxIpv4GetIcmpMsgStatsInPkts (i4IcmpMsgStatsType,
                                                  pu4RetValIcmpMsgStatsInPkts);

        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxIpv6GetIcmpMsgStatsPkts (gu4CurrContextId,
                                                i4IcmpMsgStatsType,
                                                IPVX_STATS_DIR_IN,
                                                pu4RetValIcmpMsgStatsInPkts);

        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);

}

/****************************************************************************
 Function    :  nmhGetIcmpMsgStatsOutPkts
 Input       :  The Indices
                IcmpMsgStatsIPVersion
                IcmpMsgStatsType

                The Object 
                retValIcmpMsgStatsOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIcmpMsgStatsOutPkts (INT4 i4IcmpMsgStatsIPVersion,
                           INT4 i4IcmpMsgStatsType,
                           UINT4 *pu4RetValIcmpMsgStatsOutPkts)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV4)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPFWD_PROT_LOCK ();

        i1RetVal =
            IPvxIpv4GetIcmpMsgStatsOutPkts (i4IcmpMsgStatsType,
                                            pu4RetValIcmpMsgStatsOutPkts);

        IPFWD_PROT_UNLOCK ();
#endif
    }
    else if (i4IcmpMsgStatsIPVersion == INET_VERSION_IPV6)
    {
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();

        i1RetVal = IPvxIpv6GetIcmpMsgStatsPkts (gu4CurrContextId,
                                                i4IcmpMsgStatsType,
                                                IPVX_STATS_DIR_OUT,
                                                pu4RetValIcmpMsgStatsOutPkts);

        IP6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpNetToMediaIfIndex
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object 
                setValIpNetToMediaIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpNetToMediaIfIndex (INT4 i4IpNetToMediaIfIndex,
                           UINT4 u4IpNetToMediaNetAddress,
                           INT4 i4SetValIpNetToMediaIfIndex)
{
    /* Index member as  Read Create  is not supported */
    UNUSED_PARAM (i4IpNetToMediaIfIndex);
    UNUSED_PARAM (u4IpNetToMediaNetAddress);
    UNUSED_PARAM (i4SetValIpNetToMediaIfIndex);

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpNetToMediaNetAddress
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object 
                setValIpNetToMediaNetAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpNetToMediaNetAddress (INT4 i4IpNetToMediaIfIndex,
                              UINT4 u4IpNetToMediaNetAddress,
                              UINT4 u4SetValIpNetToMediaNetAddress)
{
    /* Index member as  Read Create  is not supported */
    UNUSED_PARAM (i4IpNetToMediaIfIndex);
    UNUSED_PARAM (u4IpNetToMediaNetAddress);
    UNUSED_PARAM (u4SetValIpNetToMediaNetAddress);

    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IpNetToMediaIfIndex
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object 
                testValIpNetToMediaIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpNetToMediaIfIndex (UINT4 *pu4ErrorCode,
                              INT4 i4IpNetToMediaIfIndex,
                              UINT4 u4IpNetToMediaNetAddress,
                              INT4 i4TestValIpNetToMediaIfIndex)
{
    /* Index member as  Read Create  is not supported */
    UNUSED_PARAM (i4IpNetToMediaIfIndex);
    UNUSED_PARAM (u4IpNetToMediaNetAddress);
    UNUSED_PARAM (i4TestValIpNetToMediaIfIndex);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2IpNetToMediaNetAddress
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object 
                testValIpNetToMediaNetAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpNetToMediaNetAddress (UINT4 *pu4ErrorCode,
                                 INT4 i4IpNetToMediaIfIndex,
                                 UINT4 u4IpNetToMediaNetAddress,
                                 UINT4 u4TestValIpNetToMediaNetAddress)
{
    /* Index member as  Read Create  is not supported */
    UNUSED_PARAM (i4IpNetToMediaIfIndex);
    UNUSED_PARAM (u4IpNetToMediaNetAddress);
    UNUSED_PARAM (u4TestValIpNetToMediaNetAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetInetCidrRouteNumber
 Input       :  The Indices

                The Object 
                retValInetCidrRouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteNumber (UINT4 *pu4RetValInetCidrRouteNumber)
{
    UINT4               u4NoIPv4Rt = IPVX_ZERO;
    UINT4               u4NoIPv6Rt = IPVX_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    RTM_PROT_LOCK ();
    i1RetVal = nmhGetIpCidrRouteNumber (&u4NoIPv4Rt);
    RTM_PROT_UNLOCK ();
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhGetIpv6RouteNumber (&u4NoIPv6Rt);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif

    *pu4RetValInetCidrRouteNumber = u4NoIPv4Rt + u4NoIPv6Rt;

    return (i1RetVal);
}

/****************************************************************************
Function    :  nmhGetInetCidrRouteDiscards
Input       :  The Indices

               The Object 
               retValInetCidrRouteDiscards
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteDiscards (UINT4 *pu4RetValInetCidrRouteDiscards)
{
    UINT4               u4NoIPv4RtDisc = IPVX_ZERO;
    UINT4               u4NoIPv6RtDisc = IPVX_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    RTM_PROT_LOCK ();
    i1RetVal = nmhGetIpRoutingDiscards (&u4NoIPv4RtDisc);
    RTM_PROT_UNLOCK ();
#endif

#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    i4RetVal = Ip6SelectContext (gu4CurrContextId);
    UNUSED_PARAM (i4RetVal);
    i1RetVal = nmhGetIpv6DiscardedRoutes (&u4NoIPv6RtDisc);
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif

    *pu4RetValInetCidrRouteDiscards = u4NoIPv4RtDisc + u4NoIPv6RtDisc;

    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : InetCidrRouteTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceInetCidrRouteTable
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceInetCidrRouteTable (INT4 i4InetCidrRouteDestType,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pInetCidrRouteDest,
                                            UINT4 u4InetCidrRoutePfxLen,
                                            tSNMP_OID_TYPE
                                            * pInetCidrRoutePolicy,
                                            INT4 i4InetCidrRouteNextHopType,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pInetCidrRouteNextHop)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4InetCidrRouteNextHopType != i4InetCidrRouteDestType) ||
        (pInetCidrRoutePolicy->u4_Length != IPVX_TWO) ||
        (pInetCidrRoutePolicy->pu4_OidList[IPVX_ZERO] != IPVX_ZERO) ||
        (pInetCidrRoutePolicy->pu4_OidList[IPVX_ONE] != IPVX_ZERO))
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal =
            nmhValidateIndexInstanceIpCidrRouteTable (u4RtDest, u4RtMask,
                                                      i4RtTos, u4RtNextHop);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal = Rtm6ApiValIdxInstIpv6RouteTableInCxt (gu4CurrContextId,
                                                         pInetCidrRouteDest,
                                                         u4InetCidrRoutePfxLen,
                                                         pInetCidrRouteNextHop);
        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
        return i1RetVal;
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexInetCidrRouteTable
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexInetCidrRouteTable (INT4 *pi4InetCidrRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pInetCidrRouteDest,
                                    UINT4 *pu4InetCidrRoutePfxLen,
                                    tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                                    INT4 *pi4InetCidrRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pInetCidrRouteNextHop)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4NxtRtDest = IPVX_ZERO;
    UINT4               u4NxtRtMask = IPVX_ZERO;
    INT4                i4NxtRtTos = IPVX_ZERO;
    UINT4               u4NxtRtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    tNetIpv6RtInfo      NetIpv6RtInfo;
#endif

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    RTM_PROT_LOCK ();
    i1RetVal = nmhGetFirstIndexIpCidrRouteTable (&u4NxtRtDest,
                                                 &u4NxtRtMask,
                                                 &i4NxtRtTos, &u4NxtRtNextHop);

    if (i1RetVal == SNMP_SUCCESS)
    {
        /* Fill the Data */
        *pi4InetCidrRouteDestType = INET_ADDR_TYPE_IPV4;
        IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pInetCidrRouteDest, u4NxtRtDest);
        *pu4InetCidrRoutePfxLen = (CfaGetCidrSubnetMaskIndex (u4NxtRtMask));
        IPVX_GET_NULL_ROUTE_POLICY (pInetCidrRoutePolicy);
        *pi4InetCidrRouteNextHopType = INET_ADDR_TYPE_IPV4;
        IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pInetCidrRouteNextHop,
                                          u4NxtRtNextHop);
        RTM_PROT_UNLOCK ();
        return (SNMP_SUCCESS);
    }
    RTM_PROT_UNLOCK ();
#endif
#ifdef IP6_WANTED

    RTM6_TASK_LOCK ();

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    /* Get the first route from the TRIE. */
    i1RetVal = (INT1) Rtm6ApiTrieGetFirstBestRtInCxt (gu4CurrContextId,
                                                      &NetIpv6RtInfo);
    if (i1RetVal == RTM6_SUCCESS)
    {

        Ip6AddrCopy ((tIp6Addr *) (VOID *) pInetCidrRouteDest->pu1_OctetList,
                     &NetIpv6RtInfo.Ip6Dst);
        pInetCidrRouteDest->i4_Length = IP6_ADDR_SIZE;
        *pu4InetCidrRoutePfxLen = NetIpv6RtInfo.u1Prefixlen;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pInetCidrRouteNextHop->pu1_OctetList,
                     &NetIpv6RtInfo.NextHop);
        pInetCidrRouteNextHop->i4_Length = IP6_ADDR_SIZE;

        *pi4InetCidrRouteDestType = INET_ADDR_TYPE_IPV6;
        *pi4InetCidrRouteNextHopType = INET_ADDR_TYPE_IPV6;

        IPVX_GET_NULL_ROUTE_POLICY (pInetCidrRoutePolicy);
        RTM6_TASK_UNLOCK ();
        return (SNMP_SUCCESS);
    }
    RTM6_TASK_UNLOCK ();
#endif
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexInetCidrRouteTable
 Input       :  The Indices
                InetCidrRouteDestType
                nextInetCidrRouteDestType
                InetCidrRouteDest
                nextInetCidrRouteDest
                InetCidrRoutePfxLen
                nextInetCidrRoutePfxLen
                InetCidrRoutePolicy
                nextInetCidrRoutePolicy
                InetCidrRouteNextHopType
                nextInetCidrRouteNextHopType
                InetCidrRouteNextHop
                nextInetCidrRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexInetCidrRouteTable (INT4 i4InetCidrRouteDestType,
                                   INT4 *pi4NextInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pInetCidrRouteDest,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNextInetCidrRouteDest,
                                   UINT4 u4InetCidrRoutePfxLen,
                                   UINT4 *pu4NextInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                                   tSNMP_OID_TYPE * pNextInetCidrRoutePolicy,
                                   INT4 i4InetCidrRouteNextHopType,
                                   INT4 *pi4NextInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pInetCidrRouteNextHop,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNextInetCidrRouteNextHop)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4NxtRtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    UINT4               u4NxtRtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    INT4                i4NxtRtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
    UINT4               u4NxtRtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    UINT1               u1IsFirst = FALSE;
#endif
    UINT1               u1Ipv4RtTblEnd = FALSE;

    UNUSED_PARAM (pInetCidrRoutePolicy);
    UNUSED_PARAM (i4InetCidrRouteNextHopType);

    if (u4InetCidrRoutePfxLen > IP6_ADDR_MAX_PREFIX)
    {
        u4InetCidrRoutePfxLen = IP6_ADDR_MIN_PREFIX;
    }
    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_UNKNOWN) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        if (u4InetCidrRoutePfxLen < (CFA_MAX_CIDR + 1))
        {
            IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                    u4RtMask, u4InetCidrRoutePfxLen,
                                    u4RtNextHop, pInetCidrRouteNextHop);
        }
        else
        {
            RTM_PROT_UNLOCK ();
            return (SNMP_FAILURE);
        }

        i1RetVal = nmhGetNextIndexIpCidrRouteTable (u4RtDest, &u4NxtRtDest,
                                                    u4RtMask, &u4NxtRtMask,
                                                    i4RtTos, &i4NxtRtTos,
                                                    u4RtNextHop,
                                                    &u4NxtRtNextHop);

        if (i1RetVal == SNMP_SUCCESS)
        {
            /* Fill the Data */
            *pi4NextInetCidrRouteDestType = INET_ADDR_TYPE_IPV4;
            IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pNextInetCidrRouteDest,
                                              u4NxtRtDest);
            *pu4NextInetCidrRoutePfxLen =
                (CfaGetCidrSubnetMaskIndex (u4NxtRtMask));

            IPVX_GET_NULL_ROUTE_POLICY (pNextInetCidrRoutePolicy);
            *pi4NextInetCidrRouteNextHopType = INET_ADDR_TYPE_IPV4;
            IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pNextInetCidrRouteNextHop,
                                              u4NxtRtNextHop);
            RTM_PROT_UNLOCK ();
            return (SNMP_SUCCESS);
        }

        u1Ipv4RtTblEnd = TRUE;

        RTM_PROT_UNLOCK ();
#endif
    }

    if ((i1RetVal == SNMP_FAILURE) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_UNKNOWN) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        if (u1Ipv4RtTblEnd == TRUE)
        {
            u4InetCidrRoutePfxLen = IPVX_ZERO;
            if (pInetCidrRouteDest != NULL)
            {
                MEMSET (pInetCidrRouteDest->pu1_OctetList, IPVX_ZERO,
                        IP6_ADDR_SIZE);
                pInetCidrRouteDest->i4_Length = IPVX_ZERO;
            }
            if (pInetCidrRouteNextHop != NULL)
            {
                MEMSET (pInetCidrRouteNextHop->pu1_OctetList, IPVX_ZERO,
                        IP6_ADDR_SIZE);
                pInetCidrRouteNextHop->i4_Length = IPVX_ZERO;
            }

            u1IsFirst = TRUE;
        }

        i1RetVal =
            Rtm6ApiGetNextIpv6RouteTableEntryInCxt (gu4CurrContextId,
                                                    pInetCidrRouteDest,
                                                    pNextInetCidrRouteDest,
                                                    u4InetCidrRoutePfxLen,
                                                    pu4NextInetCidrRoutePfxLen,
                                                    pInetCidrRouteNextHop,
                                                    pNextInetCidrRouteNextHop,
                                                    u1IsFirst);

        if (i1RetVal == RTM6_SUCCESS)
        {
            *pi4NextInetCidrRouteDestType = INET_ADDR_TYPE_IPV6;
            *pi4NextInetCidrRouteNextHopType = INET_ADDR_TYPE_IPV6;

            IPVX_GET_NULL_ROUTE_POLICY (pNextInetCidrRoutePolicy);

            RTM6_TASK_UNLOCK ();
            return (SNMP_SUCCESS);
        }
        RTM6_TASK_UNLOCK ();
#else
        UNUSED_PARAM (u1Ipv4RtTblEnd);
#endif
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetInetCidrRouteIfIndex
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteIfIndex (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteIfIndex (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteIfIndex);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_IF_INDEX,
                                                   (UINT1 *)
                                                   pi4RetValInetCidrRouteIfIndex);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteType
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteType (INT4 i4InetCidrRouteDestType,
                         tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                         UINT4 u4InetCidrRoutePfxLen,
                         tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                         INT4 i4InetCidrRouteNextHopType,
                         tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                         INT4 *pi4RetValInetCidrRouteType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteType (u4RtDest, u4RtMask, i4RtTos,
                                          u4RtNextHop,
                                          pi4RetValInetCidrRouteType);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal = Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                          pInetCidrRouteDest,
                                                          u4InetCidrRoutePfxLen,
                                                          pInetCidrRouteNextHop,
                                                          IPVX_IPV6_RT_ROUTE_TYPE,
                                                          (UINT1 *)
                                                          pi4RetValInetCidrRouteType);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteProto
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteProto (INT4 i4InetCidrRouteDestType,
                          tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                          UINT4 u4InetCidrRoutePfxLen,
                          tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                          INT4 i4InetCidrRouteNextHopType,
                          tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                          INT4 *pi4RetValInetCidrRouteProto)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteProto (u4RtDest, u4RtMask, i4RtTos,
                                           u4RtNextHop,
                                           pi4RetValInetCidrRouteProto);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal = Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                          pInetCidrRouteDest,
                                                          u4InetCidrRoutePfxLen,
                                                          pInetCidrRouteNextHop,
                                                          IPVX_IPV6_RT_ROUTE_PROTO,
                                                          (UINT1 *)
                                                          pi4RetValInetCidrRouteProto);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteAge
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteAge (INT4 i4InetCidrRouteDestType,
                        tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                        UINT4 u4InetCidrRoutePfxLen,
                        tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                        INT4 i4InetCidrRouteNextHopType,
                        tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                        UINT4 *pu4RetValInetCidrRouteAge)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteAge (u4RtDest, u4RtMask, i4RtTos,
                                         u4RtNextHop,
                                         (INT4 *) pu4RetValInetCidrRouteAge);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal = Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                          pInetCidrRouteDest,
                                                          u4InetCidrRoutePfxLen,
                                                          pInetCidrRouteNextHop,
                                                          IPVX_IPV6_RT_ROUTE_AGE,
                                                          (UINT1 *)
                                                          pu4RetValInetCidrRouteAge);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteNextHopAS
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteNextHopAS (INT4 i4InetCidrRouteDestType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                              UINT4 u4InetCidrRoutePfxLen,
                              tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                              INT4 i4InetCidrRouteNextHopType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                              UINT4 *pu4RetValInetCidrRouteNextHopAS)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal =
            nmhGetIpCidrRouteNextHopAS (u4RtDest, u4RtMask, i4RtTos,
                                        u4RtNextHop,
                                        (INT4 *)
                                        pu4RetValInetCidrRouteNextHopAS);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_ROUTE_TAG,
                                                   (UINT1 *)
                                                   pu4RetValInetCidrRouteNextHopAS);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteHWStatus 
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object
                retValInetCidrRouteHWStatus 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteHWStatus (INT4 i4InetCidrRouteDestType,
                             tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                             UINT4 u4InetCidrRoutePfxLen,
                             tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                             INT4 i4InetCidrRouteNextHopType,
                             tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                             INT4 *pRetValFsMIStdInetCidrRouteHWStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteHWStatus (u4RtDest, u4RtMask, i4RtTos,
                                              u4RtNextHop,
                                              pRetValFsMIStdInetCidrRouteHWStatus);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_METRIC_1,
                                                   (UINT1 *)
                                                   pRetValFsMIStdInetCidrRouteHWStatus);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteMetric1
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteMetric1 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteMetric1)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteMetric1 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteMetric1);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_METRIC_1,
                                                   (UINT1 *)
                                                   pi4RetValInetCidrRouteMetric1);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteMetric2
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteMetric2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteMetric2 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteMetric2)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteMetric2 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteMetric2);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pi4RetValInetCidrRouteMetric2 = IPVX_METRIC_NOT_USED;
        i1RetVal = SNMP_SUCCESS;

        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteMetric3
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteMetric3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteMetric3 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteMetric3)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteMetric3 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteMetric3);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pi4RetValInetCidrRouteMetric3 = IPVX_METRIC_NOT_USED;
        i1RetVal = SNMP_SUCCESS;

        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteMetric4
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteMetric4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteMetric4 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteMetric4)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteMetric4 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteMetric4);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pi4RetValInetCidrRouteMetric4 = IPVX_METRIC_NOT_USED;
        i1RetVal = SNMP_SUCCESS;

        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteMetric5
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteMetric5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteMetric5 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 *pi4RetValInetCidrRouteMetric5)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            Ipv6Dest;
    tIp6Addr            NextHop;
    INT4                i4ApiRetVal = 0;
#endif
    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteMetric5 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             pi4RetValInetCidrRouteMetric5);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        UNUSED_PARAM (i4InetCidrRouteNextHopType);

        /* it is applicable only for static routes added as part of DHCPv6 PD feature */

        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&Ipv6Dest, 0, sizeof (tIp6Addr));
        MEMSET (&NextHop, 0, sizeof (tIp6Addr));

        MEMCPY (&(Ipv6Dest), (pInetCidrRouteDest->pu1_OctetList),
                pInetCidrRouteDest->i4_Length);
        MEMCPY (&(NextHop), (pInetCidrRouteNextHop->pu1_OctetList),
                pInetCidrRouteNextHop->i4_Length);

        i4ApiRetVal =
            Rtm6ApiFindRtEntryInCxt (VCM_DEFAULT_CONTEXT, &Ipv6Dest,
                                     (UINT1) u4InetCidrRoutePfxLen,
                                     (INT1) IP6_NETMGMT_PROTOID, &NextHop,
                                     &NetIpv6RtInfo);

        if (i4ApiRetVal == RTM6_SUCCESS)
        {
            *pi4RetValInetCidrRouteMetric5 = (INT4) NetIpv6RtInfo.i1MetricType5;
        }
        else
        {
            *pi4RetValInetCidrRouteMetric5 = -1;
        }

        i1RetVal = SNMP_SUCCESS;

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetInetCidrRouteStatus
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                retValInetCidrRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetInetCidrRouteStatus (INT4 i4InetCidrRouteDestType,
                           tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                           UINT4 u4InetCidrRoutePfxLen,
                           tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                           INT4 i4InetCidrRouteNextHopType,
                           tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                           INT4 *pi4RetValInetCidrRouteStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhGetIpCidrRouteStatus (u4RtDest, u4RtMask, i4RtTos,
                                            u4RtNextHop,
                                            pi4RetValInetCidrRouteStatus);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_ROWSTATUS,
                                                   (UINT1 *)
                                                   pi4RetValInetCidrRouteStatus);

        if (i1RetVal == RTM6_SUCCESS)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetInetCidrRouteIfIndex
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteIfIndex (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteIfIndex (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteIfIndex);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhSetFsipv6RouteIfIndex (pInetCidrRouteDest,
                                             u4InetCidrRoutePfxLen,
                                             IP6_NETMGMT_PROTOID,
                                             pInetCidrRouteNextHop,
                                             i4SetValInetCidrRouteIfIndex);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteType
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteType (INT4 i4InetCidrRouteDestType,
                         tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                         UINT4 u4InetCidrRoutePfxLen,
                         tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                         INT4 i4InetCidrRouteNextHopType,
                         tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                         INT4 i4SetValInetCidrRouteType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);
    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteType (u4RtDest, u4RtMask, i4RtTos,
                                          u4RtNextHop,
                                          i4SetValInetCidrRouteType);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhSetFsipv6RouteType (pInetCidrRouteDest,
                                          u4InetCidrRoutePfxLen,
                                          IP6_NETMGMT_PROTOID,
                                          pInetCidrRouteNextHop,
                                          i4SetValInetCidrRouteType);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteNextHopAS
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteNextHopAS (INT4 i4InetCidrRouteDestType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                              UINT4 u4InetCidrRoutePfxLen,
                              tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                              INT4 i4InetCidrRouteNextHopType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                              UINT4 u4SetValInetCidrRouteNextHopAS)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    UINT4               u4Ipv6RouteTag = IPVX_ZERO;
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteNextHopAS (u4RtDest, u4RtMask, i4RtTos,
                                               u4RtNextHop,
                                               u4SetValInetCidrRouteNextHopAS);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_ROUTE_TAG,
                                                   (UINT1 *) &u4Ipv6RouteTag);

        if (i1RetVal == RTM6_SUCCESS)
        {
            /*
             * The higher order 2 bytes refers to the tag value and the 
             * lower order 2 byter refers to the next-hop AS number.
             */
            u4Ipv6RouteTag &= IPVX_IPV6_RT_TAG_MASK;
            u4SetValInetCidrRouteNextHopAS |= u4Ipv6RouteTag;

            i1RetVal = nmhSetFsipv6RouteTag (pInetCidrRouteDest,
                                             u4InetCidrRoutePfxLen,
                                             IP6_NETMGMT_PROTOID,
                                             pInetCidrRouteNextHop,
                                             u4SetValInetCidrRouteNextHopAS);

            if (i1RetVal == RTM6_SUCCESS)
            {
                i1RetVal = SNMP_SUCCESS;
            }
            else if (i1RetVal == RTM6_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
            }

        }
        else
        {
            i1RetVal = SNMP_FAILURE;
        }
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteMetric1
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteMetric1 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteMetric1)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteMetric1 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteMetric1);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhSetFsipv6RouteMetric (pInetCidrRouteDest,
                                            u4InetCidrRoutePfxLen,
                                            IP6_NETMGMT_PROTOID,
                                            pInetCidrRouteNextHop,
                                            i4SetValInetCidrRouteMetric1);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteMetric2
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteMetric2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteMetric2 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteMetric2)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteMetric2 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteMetric2);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        /* Ipv6 not supporting more than one Metric so it is unused */
        UNUSED_PARAM (i4SetValInetCidrRouteMetric2);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteMetric3
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteMetric3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteMetric3 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteMetric3)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteMetric3 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteMetric3);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        /* Ipv6 not supporting more than one Metric so it is unused */
        UNUSED_PARAM (i4SetValInetCidrRouteMetric3);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteMetric4
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteMetric4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteMetric4 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteMetric4)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))

        RTM_PROT_LOCK ();
        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteMetric4 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteMetric4);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        /* Ipv6 not supporting more than one Metric so it is unused */
        UNUSED_PARAM (i4SetValInetCidrRouteMetric4);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteMetric5
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteMetric5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteMetric5 (INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4SetValInetCidrRouteMetric5)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteMetric5 (u4RtDest, u4RtMask, i4RtTos,
                                             u4RtNextHop,
                                             i4SetValInetCidrRouteMetric5);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        /* Ipv6 not supporting more than one Metric so it is unused */
        UNUSED_PARAM (i4SetValInetCidrRouteMetric5);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhSetInetCidrRouteStatus
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                setValInetCidrRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetInetCidrRouteStatus (INT4 i4InetCidrRouteDestType,
                           tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                           UINT4 u4InetCidrRoutePfxLen,
                           tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                           INT4 i4InetCidrRouteNextHopType,
                           tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                           INT4 i4SetValInetCidrRouteStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhSetIpCidrRouteStatus (u4RtDest, u4RtMask, i4RtTos,
                                            u4RtNextHop,
                                            i4SetValInetCidrRouteStatus);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhSetFsipv6RouteAdminStatus (pInetCidrRouteDest,
                                                 u4InetCidrRoutePfxLen,
                                                 IP6_NETMGMT_PROTOID,
                                                 pInetCidrRouteNextHop,
                                                 i4SetValInetCidrRouteStatus);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteIfIndex
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteIfIndex (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteIfIndex)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteIfIndex (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos,
                                                u4RtNextHop,
                                                i4TestValInetCidrRouteIfIndex);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhTestv2Fsipv6RouteIfIndex (pu4ErrorCode,
                                                pInetCidrRouteDest,
                                                u4InetCidrRoutePfxLen,
                                                IP6_NETMGMT_PROTOID,
                                                pInetCidrRouteNextHop,
                                                i4TestValInetCidrRouteIfIndex);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteType
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteType (UINT4 *pu4ErrorCode,
                            INT4 i4InetCidrRouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                            UINT4 u4InetCidrRoutePfxLen,
                            tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                            INT4 i4InetCidrRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                            INT4 i4TestValInetCidrRouteType)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteType (pu4ErrorCode, u4RtDest, u4RtMask,
                                             i4RtTos, u4RtNextHop,
                                             i4TestValInetCidrRouteType);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhTestv2Fsipv6RouteType (pu4ErrorCode, pInetCidrRouteDest,
                                             u4InetCidrRoutePfxLen,
                                             IP6_NETMGMT_PROTOID,
                                             pInetCidrRouteNextHop,
                                             i4TestValInetCidrRouteType);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteNextHopAS
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteNextHopAS (UINT4 *pu4ErrorCode,
                                 INT4 i4InetCidrRouteDestType,
                                 tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                                 UINT4 u4InetCidrRoutePfxLen,
                                 tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                                 INT4 i4InetCidrRouteNextHopType,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pInetCidrRouteNextHop,
                                 UINT4 u4TestValInetCidrRouteNextHopAS)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    UINT4               u4Ipv6RouteTag = IPVX_ZERO;
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);
        i1RetVal =
            nmhTestv2IpCidrRouteNextHopAS (pu4ErrorCode, u4RtDest, u4RtMask,
                                           i4RtTos, u4RtNextHop,
                                           u4TestValInetCidrRouteNextHopAS);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED

        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pInetCidrRouteDest,
                                                   u4InetCidrRoutePfxLen,
                                                   pInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_ROUTE_TAG,
                                                   (UINT1 *) &u4Ipv6RouteTag);

        if (i1RetVal == RTM6_SUCCESS)
        {
            /*
             * The higher order 2 bytes refers to the tag value and the 
             * lower order 2 byter refers to the next-hop AS number.
             */
            u4Ipv6RouteTag &= IPVX_IPV6_RT_TAG_MASK;
            u4TestValInetCidrRouteNextHopAS |= u4Ipv6RouteTag;

            i1RetVal =
                nmhTestv2Fsipv6RouteTag (pu4ErrorCode, pInetCidrRouteDest,
                                         u4InetCidrRoutePfxLen,
                                         IP6_NETMGMT_PROTOID,
                                         pInetCidrRouteNextHop,
                                         u4TestValInetCidrRouteNextHopAS);
        }
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();

#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteMetric1
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteMetric1 (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteMetric1)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteMetric1 (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos,
                                                u4RtNextHop,
                                                i4TestValInetCidrRouteMetric1);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal = nmhTestv2Fsipv6RouteMetric (pu4ErrorCode, pInetCidrRouteDest,
                                               u4InetCidrRoutePfxLen,
                                               IP6_NETMGMT_PROTOID,
                                               pInetCidrRouteNextHop,
                                               i4TestValInetCidrRouteMetric1);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteMetric2
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteMetric2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteMetric2 (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteMetric2)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteMetric2 (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos, u4RtNextHop,
                                                i4TestValInetCidrRouteMetric2);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = SNMP_FAILURE;

        UNUSED_PARAM (i4TestValInetCidrRouteMetric2);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteMetric3
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteMetric3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteMetric3 (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteMetric3)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteMetric3 (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos, u4RtNextHop,
                                                i4TestValInetCidrRouteMetric3);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = SNMP_FAILURE;

        UNUSED_PARAM (i4TestValInetCidrRouteMetric3);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteMetric4
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteMetric4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteMetric4 (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteMetric4)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteMetric4 (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos, u4RtNextHop,
                                                i4TestValInetCidrRouteMetric4);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = SNMP_FAILURE;

        UNUSED_PARAM (i4TestValInetCidrRouteMetric4);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteMetric5
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteMetric5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteMetric5 (UINT4 *pu4ErrorCode,
                               INT4 i4InetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                               UINT4 u4InetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                               INT4 i4InetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                               INT4 i4TestValInetCidrRouteMetric5)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteMetric5 (pu4ErrorCode, u4RtDest,
                                                u4RtMask, i4RtTos, u4RtNextHop,
                                                i4TestValInetCidrRouteMetric5);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = SNMP_FAILURE;

        UNUSED_PARAM (i4TestValInetCidrRouteMetric5);
        UNUSED_PARAM (pInetCidrRouteDest);
        UNUSED_PARAM (u4InetCidrRoutePfxLen);
        UNUSED_PARAM (pInetCidrRoutePolicy);
        UNUSED_PARAM (i4InetCidrRouteNextHopType);
        UNUSED_PARAM (pInetCidrRouteNextHop);
#endif
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2InetCidrRouteStatus
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop

                The Object 
                testValInetCidrRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2InetCidrRouteStatus (UINT4 *pu4ErrorCode,
                              INT4 i4InetCidrRouteDestType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteDest,
                              UINT4 u4InetCidrRoutePfxLen,
                              tSNMP_OID_TYPE * pInetCidrRoutePolicy,
                              INT4 i4InetCidrRouteNextHopType,
                              tSNMP_OCTET_STRING_TYPE * pInetCidrRouteNextHop,
                              INT4 i4TestValInetCidrRouteStatus)
{
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4RtDest = IPVX_ZERO;
    UINT4               u4RtMask = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
#endif
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
#endif
    UNUSED_PARAM (pInetCidrRoutePolicy);

    if (i4InetCidrRouteDestType != i4InetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4) ||
        (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV4Z))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        RTM_PROT_LOCK ();

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pInetCidrRouteDest,
                                u4RtMask, u4InetCidrRoutePfxLen,
                                u4RtNextHop, pInetCidrRouteNextHop);

        i1RetVal = nmhTestv2IpCidrRouteStatus (pu4ErrorCode, u4RtDest,
                                               u4RtMask, i4RtTos, u4RtNextHop,
                                               i4TestValInetCidrRouteStatus);
        RTM_PROT_UNLOCK ();
#endif
    }
    else if ((i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4InetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i4RetVal);
        i1RetVal =
            nmhTestv2Fsipv6RouteAdminStatus (pu4ErrorCode, pInetCidrRouteDest,
                                             u4InetCidrRoutePfxLen,
                                             IP6_NETMGMT_PROTOID,
                                             pInetCidrRouteNextHop,
                                             i4TestValInetCidrRouteStatus);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#endif
    }
    return (i1RetVal);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2InetCidrRouteTable
 Input       :  The Indices
                InetCidrRouteDestType
                InetCidrRouteDest
                InetCidrRoutePfxLen
                InetCidrRoutePolicy
                InetCidrRouteNextHopType
                InetCidrRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2InetCidrRouteTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
