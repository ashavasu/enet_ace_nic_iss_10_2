/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsiplw.c,v 1.14 2016/02/27 10:14:53 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "ipvxinc.h"
# include  "ipvxdefs.h"
# include  "ipvxext.h"
# include  "cfa.h"
# include  "ipv6.h"
# include  "ip.h"
# include   "cli.h"
# include  "stdipvcli.h"
# include  "rmgr.h"
# include  "trieinc.h"
# include  "rtm6.h"
#include "fsmsipcli.h"
# include  "ip6util.h"

extern INT1
nmhSetFsipv6RoutePreference (tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,
                                 tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1
nmhTestv2Fsipv6RoutePreference (UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,
                                 INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 );

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv4InterfaceTableLastChange
 Input       :  The Indices

                The Object 
                retValFsMIStdIpv4InterfaceTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv4InterfaceTableLastChange (UINT4
                                           *pu4RetValFsMIStdIpv4InterfaceTableLastChange)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv4InterfaceTableLastChange
        (pu4RetValFsMIStdIpv4InterfaceTableLastChange);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceTableLastChange
 Input       :  The Indices

                The Object 
                retValFsMIStdIpv6InterfaceTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceTableLastChange (UINT4
                                           *pu4RetValFsMIStdIpv6InterfaceTableLastChange)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceTableLastChange
        (pu4RetValFsMIStdIpv6InterfaceTableLastChange);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsTableLastChange
 Input       :  The Indices

                The Object 
                retValFsMIStdIpIfStatsTableLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsTableLastChange (UINT4
                                       *pu4RetValFsMIStdIpIfStatsTableLastChange)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsTableLastChange
        (pu4RetValFsMIStdIpIfStatsTableLastChange);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdIpGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpGlobalTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT) ||
        (i4FsMIStdIpContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpGlobalTable (INT4 *pi4FsMIStdIpContextId)
{
    INT4                i4RetVal = VCM_FAILURE;
    INT4                i4ContextId = 0;

    i4RetVal = VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4RetVal == VCM_SUCCESS)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        if (IpIsCxtExist (*pi4FsMIStdIpContextId) == IP_TRUE)
        {
            return SNMP_SUCCESS;
        }
#endif
#ifdef IP6_WANTED
        if (Ip6VRExists (*pi4FsMIStdIpContextId) == IP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
#endif
        i4ContextId = *pi4FsMIStdIpContextId;
        i4RetVal = VcmGetNextActiveL3Context ((UINT4) i4ContextId,
                                              (UINT4 *) pi4FsMIStdIpContextId);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpGlobalTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId)
{
    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        if (IpIsCxtExist (*pi4NextFsMIStdIpContextId) == IP_TRUE)
        {
            return SNMP_SUCCESS;
        }
#endif
#ifdef IP6_WANTED
        if (Ip6VRExists (*pi4NextFsMIStdIpContextId) == IP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
#endif
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdIpForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpForwarding (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIStdIpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return = nmhGetIpForwarding (pi4RetValFsMIStdIpForwarding);
    IPFWD_PROT_UNLOCK ();
#else
    *pi4RetValFsMIStdIpForwarding = 0;
    i1Return = SNMP_SUCCESS;
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpDefaultTTL
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdIpDefaultTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpDefaultTTL (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIStdIpDefaultTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return = nmhGetIpDefaultTTL (pi4RetValFsMIStdIpDefaultTTL);
    IPFWD_PROT_UNLOCK ();
#else
    *pi4RetValFsMIStdIpDefaultTTL = 0;
    i1Return = SNMP_SUCCESS;
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpReasmTimeout
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdIpReasmTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpReasmTimeout (INT4 i4FsMIStdIpContextId,
                             INT4 *pi4RetValFsMIStdIpReasmTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return = nmhGetIpReasmTimeout (pi4RetValFsMIStdIpReasmTimeout);
    IPFWD_PROT_UNLOCK ();
#else
    *pi4RetValFsMIStdIpReasmTimeout = 0;
    i1Return = SNMP_SUCCESS;
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6IpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdIpv6IpForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6IpForwarding (INT4 i4FsMIStdIpContextId,
                               INT4 *pi4RetValFsMIStdIpv6IpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
#ifdef IP6_WANTED
    i1Return = nmhGetIpv6IpForwarding (pi4RetValFsMIStdIpv6IpForwarding);
#else
    *pi4RetValFsMIStdIpv6IpForwarding = IP6_FORW_DISABLE;
    i1Return = SNMP_SUCCESS;
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6IpDefaultHopLimit
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdIpv6IpDefaultHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6IpDefaultHopLimit (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4RetValFsMIStdIpv6IpDefaultHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
#ifdef IP6_WANTED
    i1Return =
        nmhGetIpv6IpDefaultHopLimit (pi4RetValFsMIStdIpv6IpDefaultHopLimit);
#else
    *pi4RetValFsMIStdIpv6IpDefaultHopLimit = 0;
    i1Return = SNMP_SUCCESS;
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteNumber
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdInetCidrRouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteNumber (INT4 i4FsMIStdIpContextId,
                                  UINT4 *pu4RetValFsMIStdInetCidrRouteNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetInetCidrRouteNumber (pu4RetValFsMIStdInetCidrRouteNumber);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteDiscards
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIStdInetCidrRouteDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteDiscards (INT4 i4FsMIStdIpContextId,
                                    UINT4
                                    *pu4RetValFsMIStdInetCidrRouteDiscards)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteDiscards (pu4RetValFsMIStdInetCidrRouteDiscards);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIStdIpForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpForwarding (INT4 i4FsMIStdIpContextId,
                           INT4 i4SetValFsMIStdIpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return = nmhSetIpForwarding (i4SetValFsMIStdIpForwarding);
    IPFWD_PROT_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (i4SetValFsMIStdIpForwarding);
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpDefaultTTL
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIStdIpDefaultTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpDefaultTTL (INT4 i4FsMIStdIpContextId,
                           INT4 i4SetValFsMIStdIpDefaultTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return = nmhSetIpDefaultTTL (i4SetValFsMIStdIpDefaultTTL);
    IPFWD_PROT_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (i4SetValFsMIStdIpDefaultTTL);
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6IpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIStdIpv6IpForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6IpForwarding (INT4 i4FsMIStdIpContextId,
                               INT4 i4SetValFsMIStdIpv6IpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetIpv6IpForwarding (i4SetValFsMIStdIpv6IpForwarding);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6IpDefaultHopLimit
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIStdIpv6IpDefaultHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6IpDefaultHopLimit (INT4 i4FsMIStdIpContextId,
                                    INT4 i4SetValFsMIStdIpv6IpDefaultHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpv6IpDefaultHopLimit (i4SetValFsMIStdIpv6IpDefaultHopLimit);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIStdIpForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpForwarding (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4TestValFsMIStdIpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return =
        nmhTestv2IpForwarding (pu4ErrorCode, i4TestValFsMIStdIpForwarding);
    IPFWD_PROT_UNLOCK ();
#else
    UNUSED_PARAM (i4TestValFsMIStdIpForwarding);
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpDefaultTTL
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIStdIpDefaultTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpDefaultTTL (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4TestValFsMIStdIpDefaultTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    IPFWD_PROT_LOCK ();
    i1Return =
        nmhTestv2IpDefaultTTL (pu4ErrorCode, i4TestValFsMIStdIpDefaultTTL);
    IPFWD_PROT_UNLOCK ();
#else
    UNUSED_PARAM (i4TestValFsMIStdIpDefaultTTL);
#endif
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6IpForwarding
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIStdIpv6IpForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6IpForwarding (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  INT4 i4TestValFsMIStdIpv6IpForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ipv6IpForwarding (pu4ErrorCode,
                                   i4TestValFsMIStdIpv6IpForwarding);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6IpDefaultHopLimit
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIStdIpv6IpDefaultHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6IpDefaultHopLimit (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4TestValFsMIStdIpv6IpDefaultHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ipv6IpDefaultHopLimit (pu4ErrorCode,
                                        i4TestValFsMIStdIpv6IpDefaultHopLimit);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpGlobalTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpv4InterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpv4InterfaceTable
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpv4InterfaceTable (INT4
                                                   i4FsMIStdIpv4InterfaceIfIndex)
{
    if (nmhValidateIndexInstanceIpv4InterfaceTable
        (i4FsMIStdIpv4InterfaceIfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpv4InterfaceTable
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpv4InterfaceTable (INT4 *pi4FsMIStdIpv4InterfaceIfIndex)
{
    if (nmhGetFirstIndexIpv4InterfaceTable (pi4FsMIStdIpv4InterfaceIfIndex) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpv4InterfaceTable
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex
                nextFsMIStdIpv4InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpv4InterfaceTable (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                          INT4
                                          *pi4NextFsMIStdIpv4InterfaceIfIndex)
{
    if (nmhGetNextIndexIpv4InterfaceTable (i4FsMIStdIpv4InterfaceIfIndex,
                                           pi4NextFsMIStdIpv4InterfaceIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv4InterfaceReasmMaxSize
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                retValFsMIStdIpv4InterfaceReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv4InterfaceReasmMaxSize (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpv4InterfaceReasmMaxSize)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return =
        nmhGetIpv4InterfaceReasmMaxSize (i4FsMIStdIpv4InterfaceIfIndex,
                                         pi4RetValFsMIStdIpv4InterfaceReasmMaxSize);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv4InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                retValFsMIStdIpv4InterfaceEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv4InterfaceEnableStatus (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpv4InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv4InterfaceEnableStatus (i4FsMIStdIpv4InterfaceIfIndex,
                                         pi4RetValFsMIStdIpv4InterfaceEnableStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv4InterfaceRetransmitTime
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                retValFsMIStdIpv4InterfaceRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv4InterfaceRetransmitTime (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv4InterfaceRetransmitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv4InterfaceRetransmitTime (i4FsMIStdIpv4InterfaceIfIndex,
                                           pu4RetValFsMIStdIpv4InterfaceRetransmitTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv4IfContextId
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                retValFsMIStdIpv4IfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv4IfContextId (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                              INT4 *pi4RetValFsMIStdIpv4IfContextId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (VcmGetContextIdFromCfaIfIndex (i4FsMIStdIpv4InterfaceIfIndex,
                                       (UINT4 *)
                                       pi4RetValFsMIStdIpv4IfContextId) ==
        VCM_FAILURE)
    {
        return i1Return;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv4InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                setValFsMIStdIpv4InterfaceEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv4InterfaceEnableStatus (INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                        INT4
                                        i4SetValFsMIStdIpv4InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv4InterfaceEnableStatus (i4FsMIStdIpv4InterfaceIfIndex,
                                         i4SetValFsMIStdIpv4InterfaceEnableStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv4InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex

                The Object 
                testValFsMIStdIpv4InterfaceEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv4InterfaceEnableStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdIpv4InterfaceIfIndex,
                                           INT4
                                           i4TestValFsMIStdIpv4InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv4InterfaceEnableStatus (pu4ErrorCode,
                                            i4FsMIStdIpv4InterfaceIfIndex,
                                            i4TestValFsMIStdIpv4InterfaceEnableStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpv4InterfaceTable
 Input       :  The Indices
                FsMIStdIpv4InterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpv4InterfaceTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpv6InterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpv6InterfaceTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpv6InterfaceTable (INT4
                                                   i4FsMIStdIpv6InterfaceIfIndex)
{
    return (nmhValidateIndexInstanceIpv6InterfaceTable
            (i4FsMIStdIpv6InterfaceIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpv6InterfaceTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpv6InterfaceTable (INT4 *pi4FsMIStdIpv6InterfaceIfIndex)
{
    return (nmhGetFirstIndexIpv6InterfaceTable
            (pi4FsMIStdIpv6InterfaceIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpv6InterfaceTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
                nextFsMIStdIpv6InterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpv6InterfaceTable (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                          INT4
                                          *pi4NextFsMIStdIpv6InterfaceIfIndex)
{
    return (nmhGetNextIndexIpv6InterfaceTable (i4FsMIStdIpv6InterfaceIfIndex,
                                               pi4NextFsMIStdIpv6InterfaceIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceReasmMaxSize
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceReasmMaxSize (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                        UINT4
                                        *pu4RetValFsMIStdIpv6InterfaceReasmMaxSize)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceReasmMaxSize (i4FsMIStdIpv6InterfaceIfIndex,
                                         pu4RetValFsMIStdIpv6InterfaceReasmMaxSize);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceIdentifier
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceIdentifier (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIStdIpv6InterfaceIdentifier)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceIdentifier (i4FsMIStdIpv6InterfaceIfIndex,
                                       pRetValFsMIStdIpv6InterfaceIdentifier);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceEnableStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpv6InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceEnableStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                         pi4RetValFsMIStdIpv6InterfaceEnableStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceReachableTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceReachableTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsMIStdIpv6InterfaceReachableTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceReachableTime (i4FsMIStdIpv6InterfaceIfIndex,
                                          pu4RetValFsMIStdIpv6InterfaceReachableTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceRetransmitTime (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6InterfaceRetransmitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceRetransmitTime (i4FsMIStdIpv6InterfaceIfIndex,
                                           pu4RetValFsMIStdIpv6InterfaceRetransmitTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6InterfaceForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6InterfaceForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6InterfaceForwarding (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                      INT4
                                      *pi4RetValFsMIStdIpv6InterfaceForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6InterfaceForwarding (i4FsMIStdIpv6InterfaceIfIndex,
                                       pi4RetValFsMIStdIpv6InterfaceForwarding);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6IfContextId
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                retValFsMIStdIpv6IfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6IfContextId (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                              INT4 *pi4RetValFsMIStdIpv6IfContextId)
{
    if (VcmGetContextIdFromCfaIfIndex (i4FsMIStdIpv6InterfaceIfIndex,
                                       (UINT4 *)
                                       pi4RetValFsMIStdIpv6IfContextId) ==
        VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIStdIpv6InterfaceEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6InterfaceEnableStatus (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                        INT4
                                        i4SetValFsMIStdIpv6InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6InterfaceEnableStatus (i4FsMIStdIpv6InterfaceIfIndex,
                                         i4SetValFsMIStdIpv6InterfaceEnableStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6InterfaceForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                setValFsMIStdIpv6InterfaceForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6InterfaceForwarding (INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                      INT4
                                      i4SetValFsMIStdIpv6InterfaceForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6InterfaceForwarding (i4FsMIStdIpv6InterfaceIfIndex,
                                       i4SetValFsMIStdIpv6InterfaceForwarding);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6InterfaceEnableStatus
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIStdIpv6InterfaceEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6InterfaceEnableStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                           INT4
                                           i4TestValFsMIStdIpv6InterfaceEnableStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6InterfaceEnableStatus (pu4ErrorCode,
                                            i4FsMIStdIpv6InterfaceIfIndex,
                                            i4TestValFsMIStdIpv6InterfaceEnableStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6InterfaceForwarding
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex

                The Object 
                testValFsMIStdIpv6InterfaceForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6InterfaceForwarding (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdIpv6InterfaceIfIndex,
                                         INT4
                                         i4TestValFsMIStdIpv6InterfaceForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6InterfaceForwarding (pu4ErrorCode,
                                          i4FsMIStdIpv6InterfaceIfIndex,
                                          i4TestValFsMIStdIpv6InterfaceForwarding);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpv6InterfaceTable
 Input       :  The Indices
                FsMIStdIpv6InterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpv6InterfaceTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpSystemStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpSystemStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpSystemStatsTable (INT4 i4FsMIStdIpContextId,
                                                   INT4
                                                   i4FsMIStdIpSystemStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT) ||
        (i4FsMIStdIpContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceIpSystemStatsTable
        (i4FsMIStdIpSystemStatsIPVersion);

    UtilIpvxReleaseCxt ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpSystemStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpSystemStatsTable (INT4 *pi4FsMIStdIpContextId,
                                           INT4
                                           *pi4FsMIStdIpSystemStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = VCM_FAILURE;
    INT4                i4ContextId = 0;

    i4Value = VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == VCM_SUCCESS)
    {
        if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIpSystemStatsTable
                (pi4FsMIStdIpSystemStatsIPVersion);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        i4ContextId = *pi4FsMIStdIpContextId;
        i4Value = VcmGetNextActiveL3Context ((UINT4) i4ContextId,
                                             (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpSystemStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion
                nextFsMIStdIpSystemStatsIPVersion
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpSystemStatsTable (INT4 i4FsMIStdIpContextId,
                                          INT4 *pi4NextFsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          INT4
                                          *pi4NextFsMIStdIpSystemStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            if (nmhGetNextIndexIpSystemStatsTable
                (i4FsMIStdIpSystemStatsIPVersion,
                 pi4NextFsMIStdIpSystemStatsIPVersion) == SNMP_SUCCESS)
            {
                *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
                UtilIpvxReleaseCxt ();
                return SNMP_SUCCESS;
            }
        }
    }

    /* Current context is invalid or both address types are already obtained
     * for the current session, hence retrive the next context id */

    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           != VCM_FAILURE)
    {
        if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIpSystemStatsTable
                (pi4NextFsMIStdIpSystemStatsIPVersion);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInReceives
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInReceives (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsInReceives)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInReceives (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsInReceives);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInReceives
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInReceives (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsMIStdIpSystemStatsHCInReceives)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInReceives (i4FsMIStdIpSystemStatsIPVersion,
                                         pu8RetValFsMIStdIpSystemStatsHCInReceives);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInOctets (INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdIpSystemStatsIPVersion,
                                    UINT4
                                    *pu4RetValFsMIStdIpSystemStatsInOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInOctets (i4FsMIStdIpSystemStatsIPVersion,
                                     pu4RetValFsMIStdIpSystemStatsInOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInOctets (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdIpSystemStatsHCInOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInOctets (i4FsMIStdIpSystemStatsIPVersion,
                                       pu8RetValFsMIStdIpSystemStatsHCInOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInHdrErrors
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInHdrErrors (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsInHdrErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInHdrErrors (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsInHdrErrors);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInNoRoutes
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInNoRoutes (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsInNoRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInNoRoutes (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsInNoRoutes);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInAddrErrors
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInAddrErrors (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsInAddrErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInAddrErrors (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsInAddrErrors);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInUnknownProtos
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInUnknownProtos (INT4 i4FsMIStdIpContextId,
                                           INT4 i4FsMIStdIpSystemStatsIPVersion,
                                           UINT4
                                           *pu4RetValFsMIStdIpSystemStatsInUnknownProtos)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInUnknownProtos (i4FsMIStdIpSystemStatsIPVersion,
                                            pu4RetValFsMIStdIpSystemStatsInUnknownProtos);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInTruncatedPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInTruncatedPkts (INT4 i4FsMIStdIpContextId,
                                           INT4 i4FsMIStdIpSystemStatsIPVersion,
                                           UINT4
                                           *pu4RetValFsMIStdIpSystemStatsInTruncatedPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInTruncatedPkts (i4FsMIStdIpSystemStatsIPVersion,
                                            pu4RetValFsMIStdIpSystemStatsInTruncatedPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInForwDatagrams
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInForwDatagrams (INT4 i4FsMIStdIpContextId,
                                           INT4 i4FsMIStdIpSystemStatsIPVersion,
                                           UINT4
                                           *pu4RetValFsMIStdIpSystemStatsInForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInForwDatagrams (i4FsMIStdIpSystemStatsIPVersion,
                                            pu4RetValFsMIStdIpSystemStatsInForwDatagrams);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInForwDatagrams
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInForwDatagrams (INT4 i4FsMIStdIpContextId,
                                             INT4
                                             i4FsMIStdIpSystemStatsIPVersion,
                                             tSNMP_COUNTER64_TYPE *
                                             pu8RetValFsMIStdIpSystemStatsHCInForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInForwDatagrams (i4FsMIStdIpSystemStatsIPVersion,
                                              pu8RetValFsMIStdIpSystemStatsHCInForwDatagrams);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsReasmReqds
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsReasmReqds (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsReasmReqds)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsReasmReqds (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsReasmReqds);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsReasmOKs
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsReasmOKs (INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdIpSystemStatsIPVersion,
                                    UINT4
                                    *pu4RetValFsMIStdIpSystemStatsReasmOKs)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsReasmOKs (i4FsMIStdIpSystemStatsIPVersion,
                                     pu4RetValFsMIStdIpSystemStatsReasmOKs);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsReasmFails
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsReasmFails (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsReasmFails)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsReasmFails (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsReasmFails);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInDiscards
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInDiscards (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsInDiscards)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInDiscards (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsInDiscards);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInDelivers
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInDelivers (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsInDelivers)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInDelivers (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsInDelivers);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInDelivers
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInDelivers (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsMIStdIpSystemStatsHCInDelivers)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInDelivers (i4FsMIStdIpSystemStatsIPVersion,
                                         pu8RetValFsMIStdIpSystemStatsHCInDelivers);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutRequests
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutRequests (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsOutRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutRequests (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsOutRequests);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutRequests
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutRequests (INT4 i4FsMIStdIpContextId,
                                         INT4 i4FsMIStdIpSystemStatsIPVersion,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsMIStdIpSystemStatsHCOutRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutRequests (i4FsMIStdIpSystemStatsIPVersion,
                                          pu8RetValFsMIStdIpSystemStatsHCOutRequests);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutNoRoutes
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutNoRoutes (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsOutNoRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutNoRoutes (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsOutNoRoutes);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutForwDatagrams
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutForwDatagrams (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            i4FsMIStdIpSystemStatsIPVersion,
                                            UINT4
                                            *pu4RetValFsMIStdIpSystemStatsOutForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutForwDatagrams (i4FsMIStdIpSystemStatsIPVersion,
                                             pu4RetValFsMIStdIpSystemStatsOutForwDatagrams);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams (INT4 i4FsMIStdIpContextId,
                                              INT4
                                              i4FsMIStdIpSystemStatsIPVersion,
                                              tSNMP_COUNTER64_TYPE *
                                              pu8RetValFsMIStdIpSystemStatsHCOutForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutForwDatagrams (i4FsMIStdIpSystemStatsIPVersion,
                                               pu8RetValFsMIStdIpSystemStatsHCOutForwDatagrams);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutDiscards
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutDiscards (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsOutDiscards)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutDiscards (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsOutDiscards);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutFragReqds
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutFragReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutFragReqds (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsOutFragReqds)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutFragReqds (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsOutFragReqds);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutFragOKs
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutFragOKs (INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpSystemStatsIPVersion,
                                      UINT4
                                      *pu4RetValFsMIStdIpSystemStatsOutFragOKs)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutFragOKs (i4FsMIStdIpSystemStatsIPVersion,
                                       pu4RetValFsMIStdIpSystemStatsOutFragOKs);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutFragFails
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutFragFails (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsOutFragFails)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutFragFails (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsOutFragFails);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutFragCreates
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutFragCreates (INT4 i4FsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          UINT4
                                          *pu4RetValFsMIStdIpSystemStatsOutFragCreates)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutFragCreates (i4FsMIStdIpSystemStatsIPVersion,
                                           pu4RetValFsMIStdIpSystemStatsOutFragCreates);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutTransmits
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutTransmits (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsOutTransmits)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutTransmits (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsOutTransmits);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutTransmits
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutTransmits (INT4 i4FsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValFsMIStdIpSystemStatsHCOutTransmits)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutTransmits (i4FsMIStdIpSystemStatsIPVersion,
                                           pu8RetValFsMIStdIpSystemStatsHCOutTransmits);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutOctets (INT4 i4FsMIStdIpContextId,
                                     INT4 i4FsMIStdIpSystemStatsIPVersion,
                                     UINT4
                                     *pu4RetValFsMIStdIpSystemStatsOutOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutOctets (i4FsMIStdIpSystemStatsIPVersion,
                                      pu4RetValFsMIStdIpSystemStatsOutOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutOctets (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFsMIStdIpSystemStatsHCOutOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutOctets (i4FsMIStdIpSystemStatsIPVersion,
                                        pu8RetValFsMIStdIpSystemStatsHCOutOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInMcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInMcastPkts (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsInMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInMcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsInMcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInMcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInMcastPkts (INT4 i4FsMIStdIpContextId,
                                         INT4 i4FsMIStdIpSystemStatsIPVersion,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsMIStdIpSystemStatsHCInMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInMcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                          pu8RetValFsMIStdIpSystemStatsHCInMcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInMcastOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInMcastOctets (INT4 i4FsMIStdIpContextId,
                                         INT4 i4FsMIStdIpSystemStatsIPVersion,
                                         UINT4
                                         *pu4RetValFsMIStdIpSystemStatsInMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInMcastOctets (i4FsMIStdIpSystemStatsIPVersion,
                                          pu4RetValFsMIStdIpSystemStatsInMcastOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInMcastOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInMcastOctets (INT4 i4FsMIStdIpContextId,
                                           INT4 i4FsMIStdIpSystemStatsIPVersion,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValFsMIStdIpSystemStatsHCInMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInMcastOctets (i4FsMIStdIpSystemStatsIPVersion,
                                            pu8RetValFsMIStdIpSystemStatsHCInMcastOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutMcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutMcastPkts (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsOutMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutMcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsOutMcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutMcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastPkts (INT4 i4FsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValFsMIStdIpSystemStatsHCOutMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutMcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                           pu8RetValFsMIStdIpSystemStatsHCOutMcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutMcastOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutMcastOctets (INT4 i4FsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          UINT4
                                          *pu4RetValFsMIStdIpSystemStatsOutMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutMcastOctets (i4FsMIStdIpSystemStatsIPVersion,
                                           pu4RetValFsMIStdIpSystemStatsOutMcastOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutMcastOctets
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastOctets (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            i4FsMIStdIpSystemStatsIPVersion,
                                            tSNMP_COUNTER64_TYPE *
                                            pu8RetValFsMIStdIpSystemStatsHCOutMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutMcastOctets (i4FsMIStdIpSystemStatsIPVersion,
                                             pu8RetValFsMIStdIpSystemStatsHCOutMcastOctets);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsInBcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsInBcastPkts (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsInBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsInBcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsInBcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCInBcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCInBcastPkts (INT4 i4FsMIStdIpContextId,
                                         INT4 i4FsMIStdIpSystemStatsIPVersion,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsMIStdIpSystemStatsHCInBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCInBcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                          pu8RetValFsMIStdIpSystemStatsHCInBcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsOutBcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsOutBcastPkts (INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdIpSystemStatsIPVersion,
                                        UINT4
                                        *pu4RetValFsMIStdIpSystemStatsOutBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsOutBcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                         pu4RetValFsMIStdIpSystemStatsOutBcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsHCOutBcastPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsHCOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsHCOutBcastPkts (INT4 i4FsMIStdIpContextId,
                                          INT4 i4FsMIStdIpSystemStatsIPVersion,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValFsMIStdIpSystemStatsHCOutBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsHCOutBcastPkts (i4FsMIStdIpSystemStatsIPVersion,
                                           pu8RetValFsMIStdIpSystemStatsHCOutBcastPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsDiscontinuityTime
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsDiscontinuityTime (INT4 i4FsMIStdIpContextId,
                                             INT4
                                             i4FsMIStdIpSystemStatsIPVersion,
                                             UINT4
                                             *pu4RetValFsMIStdIpSystemStatsDiscontinuityTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsDiscontinuityTime (i4FsMIStdIpSystemStatsIPVersion,
                                              pu4RetValFsMIStdIpSystemStatsDiscontinuityTime);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpSystemStatsRefreshRate
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpSystemStatsIPVersion

                The Object 
                retValFsMIStdIpSystemStatsRefreshRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpSystemStatsRefreshRate (INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdIpSystemStatsIPVersion,
                                       UINT4
                                       *pu4RetValFsMIStdIpSystemStatsRefreshRate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpSystemStatsRefreshRate (i4FsMIStdIpSystemStatsIPVersion,
                                        pu4RetValFsMIStdIpSystemStatsRefreshRate);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdIpIfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpIfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpIfStatsTable (INT4 i4FsMIStdIpIfStatsIPVersion,
                                               INT4 i4FsMIStdIpIfStatsIfIndex)
{
    if (nmhValidateIndexInstanceIpIfStatsTable
        (i4FsMIStdIpIfStatsIPVersion,
         i4FsMIStdIpIfStatsIfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpIfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpIfStatsTable (INT4 *pi4FsMIStdIpIfStatsIPVersion,
                                       INT4 *pi4FsMIStdIpIfStatsIfIndex)
{
    if (nmhGetFirstIndexIpIfStatsTable (pi4FsMIStdIpIfStatsIPVersion,
                                        pi4FsMIStdIpIfStatsIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpIfStatsTable
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                nextFsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex
                nextFsMIStdIpIfStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpIfStatsTable (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 *pi4NextFsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      INT4 *pi4NextFsMIStdIpIfStatsIfIndex)
{
    if (nmhGetNextIndexIpIfStatsTable
        (i4FsMIStdIpIfStatsIPVersion, pi4NextFsMIStdIpIfStatsIPVersion,
         i4FsMIStdIpIfStatsIfIndex, pi4NextFsMIStdIpIfStatsIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInReceives
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInReceives (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsInReceives)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInReceives (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsInReceives);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInReceives
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInReceives (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValFsMIStdIpIfStatsHCInReceives)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInReceives (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu8RetValFsMIStdIpIfStatsHCInReceives);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                INT4 i4FsMIStdIpIfStatsIfIndex,
                                UINT4 *pu4RetValFsMIStdIpIfStatsInOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInOctets (i4FsMIStdIpIfStatsIPVersion,
                                 i4FsMIStdIpIfStatsIfIndex,
                                 pu4RetValFsMIStdIpIfStatsInOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValFsMIStdIpIfStatsHCInOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInOctets (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu8RetValFsMIStdIpIfStatsHCInOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInHdrErrors
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInHdrErrors (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsInHdrErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInHdrErrors (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsInHdrErrors);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInNoRoutes
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInNoRoutes (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsInNoRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInNoRoutes (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsInNoRoutes);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInAddrErrors
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInAddrErrors (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsInAddrErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInAddrErrors (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsInAddrErrors);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInUnknownProtos
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInUnknownProtos (INT4 i4FsMIStdIpIfStatsIPVersion,
                                       INT4 i4FsMIStdIpIfStatsIfIndex,
                                       UINT4
                                       *pu4RetValFsMIStdIpIfStatsInUnknownProtos)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInUnknownProtos (i4FsMIStdIpIfStatsIPVersion,
                                        i4FsMIStdIpIfStatsIfIndex,
                                        pu4RetValFsMIStdIpIfStatsInUnknownProtos);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInTruncatedPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInTruncatedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInTruncatedPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                       INT4 i4FsMIStdIpIfStatsIfIndex,
                                       UINT4
                                       *pu4RetValFsMIStdIpIfStatsInTruncatedPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInTruncatedPkts (i4FsMIStdIpIfStatsIPVersion,
                                        i4FsMIStdIpIfStatsIfIndex,
                                        pu4RetValFsMIStdIpIfStatsInTruncatedPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInForwDatagrams
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInForwDatagrams (INT4 i4FsMIStdIpIfStatsIPVersion,
                                       INT4 i4FsMIStdIpIfStatsIfIndex,
                                       UINT4
                                       *pu4RetValFsMIStdIpIfStatsInForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInForwDatagrams (i4FsMIStdIpIfStatsIPVersion,
                                        i4FsMIStdIpIfStatsIfIndex,
                                        pu4RetValFsMIStdIpIfStatsInForwDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInForwDatagrams
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInForwDatagrams (INT4 i4FsMIStdIpIfStatsIPVersion,
                                         INT4 i4FsMIStdIpIfStatsIfIndex,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsMIStdIpIfStatsHCInForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInForwDatagrams (i4FsMIStdIpIfStatsIPVersion,
                                          i4FsMIStdIpIfStatsIfIndex,
                                          pu8RetValFsMIStdIpIfStatsHCInForwDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsReasmReqds
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsReasmReqds (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsReasmReqds)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsReasmReqds (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsReasmReqds);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsReasmOKs
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsReasmOKs (INT4 i4FsMIStdIpIfStatsIPVersion,
                                INT4 i4FsMIStdIpIfStatsIfIndex,
                                UINT4 *pu4RetValFsMIStdIpIfStatsReasmOKs)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsReasmOKs (i4FsMIStdIpIfStatsIPVersion,
                                 i4FsMIStdIpIfStatsIfIndex,
                                 pu4RetValFsMIStdIpIfStatsReasmOKs);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsReasmFails
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsReasmFails (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsReasmFails)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsReasmFails (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsReasmFails);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInDiscards
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInDiscards (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsInDiscards)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInDiscards (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsInDiscards);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInDelivers
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInDelivers (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsInDelivers)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInDelivers (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsInDelivers);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInDelivers
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInDelivers (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValFsMIStdIpIfStatsHCInDelivers)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInDelivers (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu8RetValFsMIStdIpIfStatsHCInDelivers);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutRequests
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutRequests (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsOutRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutRequests (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsOutRequests);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutRequests
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutRequests (INT4 i4FsMIStdIpIfStatsIPVersion,
                                     INT4 i4FsMIStdIpIfStatsIfIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMIStdIpIfStatsHCOutRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutRequests (i4FsMIStdIpIfStatsIPVersion,
                                      i4FsMIStdIpIfStatsIfIndex,
                                      pu8RetValFsMIStdIpIfStatsHCOutRequests);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutForwDatagrams
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutForwDatagrams (INT4 i4FsMIStdIpIfStatsIPVersion,
                                        INT4 i4FsMIStdIpIfStatsIfIndex,
                                        UINT4
                                        *pu4RetValFsMIStdIpIfStatsOutForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutForwDatagrams (i4FsMIStdIpIfStatsIPVersion,
                                         i4FsMIStdIpIfStatsIfIndex,
                                         pu4RetValFsMIStdIpIfStatsOutForwDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutForwDatagrams
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutForwDatagrams (INT4 i4FsMIStdIpIfStatsIPVersion,
                                          INT4 i4FsMIStdIpIfStatsIfIndex,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValFsMIStdIpIfStatsHCOutForwDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutForwDatagrams (i4FsMIStdIpIfStatsIPVersion,
                                           i4FsMIStdIpIfStatsIfIndex,
                                           pu8RetValFsMIStdIpIfStatsHCOutForwDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutDiscards
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutDiscards (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsOutDiscards)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutDiscards (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsOutDiscards);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutFragReqds
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutFragReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutFragReqds (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsOutFragReqds)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutFragReqds (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsOutFragReqds);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutFragOKs
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutFragOKs (INT4 i4FsMIStdIpIfStatsIPVersion,
                                  INT4 i4FsMIStdIpIfStatsIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpIfStatsOutFragOKs)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutFragOKs (i4FsMIStdIpIfStatsIPVersion,
                                   i4FsMIStdIpIfStatsIfIndex,
                                   pu4RetValFsMIStdIpIfStatsOutFragOKs);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutFragFails
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutFragFails (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsOutFragFails)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutFragFails (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsOutFragFails);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutFragCreates
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutFragCreates (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      UINT4
                                      *pu4RetValFsMIStdIpIfStatsOutFragCreates)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutFragCreates (i4FsMIStdIpIfStatsIPVersion,
                                       i4FsMIStdIpIfStatsIfIndex,
                                       pu4RetValFsMIStdIpIfStatsOutFragCreates);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutTransmits
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutTransmits (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsOutTransmits)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutTransmits (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsOutTransmits);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutTransmits
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutTransmits (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdIpIfStatsHCOutTransmits)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutTransmits (i4FsMIStdIpIfStatsIPVersion,
                                       i4FsMIStdIpIfStatsIfIndex,
                                       pu8RetValFsMIStdIpIfStatsHCOutTransmits);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                 INT4 i4FsMIStdIpIfStatsIfIndex,
                                 UINT4 *pu4RetValFsMIStdIpIfStatsOutOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutOctets (i4FsMIStdIpIfStatsIPVersion,
                                  i4FsMIStdIpIfStatsIfIndex,
                                  pu4RetValFsMIStdIpIfStatsOutOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsMIStdIpIfStatsHCOutOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutOctets (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu8RetValFsMIStdIpIfStatsHCOutOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInMcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInMcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsInMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInMcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsInMcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInMcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInMcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                     INT4 i4FsMIStdIpIfStatsIfIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMIStdIpIfStatsHCInMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInMcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                      i4FsMIStdIpIfStatsIfIndex,
                                      pu8RetValFsMIStdIpIfStatsHCInMcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInMcastOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInMcastOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                     INT4 i4FsMIStdIpIfStatsIfIndex,
                                     UINT4
                                     *pu4RetValFsMIStdIpIfStatsInMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInMcastOctets (i4FsMIStdIpIfStatsIPVersion,
                                      i4FsMIStdIpIfStatsIfIndex,
                                      pu4RetValFsMIStdIpIfStatsInMcastOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInMcastOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInMcastOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                       INT4 i4FsMIStdIpIfStatsIfIndex,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFsMIStdIpIfStatsHCInMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInMcastOctets (i4FsMIStdIpIfStatsIPVersion,
                                        i4FsMIStdIpIfStatsIfIndex,
                                        pu8RetValFsMIStdIpIfStatsHCInMcastOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutMcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutMcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsOutMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutMcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsOutMcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutMcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutMcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutMcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdIpIfStatsHCOutMcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutMcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                       i4FsMIStdIpIfStatsIfIndex,
                                       pu8RetValFsMIStdIpIfStatsHCOutMcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutMcastOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutMcastOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      UINT4
                                      *pu4RetValFsMIStdIpIfStatsOutMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutMcastOctets (i4FsMIStdIpIfStatsIPVersion,
                                       i4FsMIStdIpIfStatsIfIndex,
                                       pu4RetValFsMIStdIpIfStatsOutMcastOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutMcastOctets
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutMcastOctets (INT4 i4FsMIStdIpIfStatsIPVersion,
                                        INT4 i4FsMIStdIpIfStatsIfIndex,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsMIStdIpIfStatsHCOutMcastOctets)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutMcastOctets (i4FsMIStdIpIfStatsIPVersion,
                                         i4FsMIStdIpIfStatsIfIndex,
                                         pu8RetValFsMIStdIpIfStatsHCOutMcastOctets);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsInBcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsInBcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsInBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsInBcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsInBcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCInBcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCInBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCInBcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                     INT4 i4FsMIStdIpIfStatsIfIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMIStdIpIfStatsHCInBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCInBcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                      i4FsMIStdIpIfStatsIfIndex,
                                      pu8RetValFsMIStdIpIfStatsHCInBcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsOutBcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsOutBcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                    INT4 i4FsMIStdIpIfStatsIfIndex,
                                    UINT4
                                    *pu4RetValFsMIStdIpIfStatsOutBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsOutBcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                     i4FsMIStdIpIfStatsIfIndex,
                                     pu4RetValFsMIStdIpIfStatsOutBcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsHCOutBcastPkts
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsHCOutBcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsHCOutBcastPkts (INT4 i4FsMIStdIpIfStatsIPVersion,
                                      INT4 i4FsMIStdIpIfStatsIfIndex,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdIpIfStatsHCOutBcastPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsHCOutBcastPkts (i4FsMIStdIpIfStatsIPVersion,
                                       i4FsMIStdIpIfStatsIfIndex,
                                       pu8RetValFsMIStdIpIfStatsHCOutBcastPkts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsDiscontinuityTime
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsDiscontinuityTime (INT4 i4FsMIStdIpIfStatsIPVersion,
                                         INT4 i4FsMIStdIpIfStatsIfIndex,
                                         UINT4
                                         *pu4RetValFsMIStdIpIfStatsDiscontinuityTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsDiscontinuityTime (i4FsMIStdIpIfStatsIPVersion,
                                          i4FsMIStdIpIfStatsIfIndex,
                                          pu4RetValFsMIStdIpIfStatsDiscontinuityTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsRefreshRate
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsRefreshRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsRefreshRate (INT4 i4FsMIStdIpIfStatsIPVersion,
                                   INT4 i4FsMIStdIpIfStatsIfIndex,
                                   UINT4 *pu4RetValFsMIStdIpIfStatsRefreshRate)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpIfStatsRefreshRate (i4FsMIStdIpIfStatsIPVersion,
                                    i4FsMIStdIpIfStatsIfIndex,
                                    pu4RetValFsMIStdIpIfStatsRefreshRate);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpIfStatsContextId
 Input       :  The Indices
                FsMIStdIpIfStatsIPVersion
                FsMIStdIpIfStatsIfIndex

                The Object 
                retValFsMIStdIpIfStatsContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpIfStatsContextId (INT4 i4FsMIStdIpIfStatsIPVersion,
                                 INT4 i4FsMIStdIpIfStatsIfIndex,
                                 INT4 *pi4RetValFsMIStdIpIfStatsContextId)
{
    UNUSED_PARAM (i4FsMIStdIpIfStatsIPVersion);

    if (VcmGetContextIdFromCfaIfIndex
        (i4FsMIStdIpIfStatsIfIndex,
         (UINT4 *) pi4RetValFsMIStdIpIfStatsContextId) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpAddressPrefixTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpAddressPrefixTable
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpAddressPrefixTable (INT4
                                                     i4FsMIStdIpAddressPrefixIfIndex,
                                                     INT4
                                                     i4FsMIStdIpAddressPrefixType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsMIStdIpAddressPrefixPrefix,
                                                     UINT4
                                                     u4FsMIStdIpAddressPrefixLength)
{
    if (nmhValidateIndexInstanceIpAddressPrefixTable
        (i4FsMIStdIpAddressPrefixIfIndex, i4FsMIStdIpAddressPrefixType,
         pFsMIStdIpAddressPrefixPrefix, u4FsMIStdIpAddressPrefixLength)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpAddressPrefixTable
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpAddressPrefixTable (INT4
                                             *pi4FsMIStdIpAddressPrefixIfIndex,
                                             INT4
                                             *pi4FsMIStdIpAddressPrefixType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdIpAddressPrefixPrefix,
                                             UINT4
                                             *pu4FsMIStdIpAddressPrefixLength)
{
    if (nmhGetFirstIndexFsIpvxAddrPrefixTable (pi4FsMIStdIpAddressPrefixIfIndex,
                                               pi4FsMIStdIpAddressPrefixType,
                                               pFsMIStdIpAddressPrefixPrefix,
                                               pu4FsMIStdIpAddressPrefixLength)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpAddressPrefixTable
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                nextFsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                nextFsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                nextFsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength
                nextFsMIStdIpAddressPrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpAddressPrefixTable (INT4
                                            i4FsMIStdIpAddressPrefixIfIndex,
                                            INT4
                                            *pi4NextFsMIStdIpAddressPrefixIfIndex,
                                            INT4 i4FsMIStdIpAddressPrefixType,
                                            INT4
                                            *pi4NextFsMIStdIpAddressPrefixType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIStdIpAddressPrefixPrefix,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsMIStdIpAddressPrefixPrefix,
                                            UINT4
                                            u4FsMIStdIpAddressPrefixLength,
                                            UINT4
                                            *pu4NextFsMIStdIpAddressPrefixLength)
{
    if (nmhGetNextIndexIpAddressPrefixTable
        (i4FsMIStdIpAddressPrefixIfIndex, pi4NextFsMIStdIpAddressPrefixIfIndex,
         i4FsMIStdIpAddressPrefixType, pi4NextFsMIStdIpAddressPrefixType,
         pFsMIStdIpAddressPrefixPrefix, pNextFsMIStdIpAddressPrefixPrefix,
         u4FsMIStdIpAddressPrefixLength, pu4NextFsMIStdIpAddressPrefixLength)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefixOrigin
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressPrefixOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefixOrigin (INT4 i4FsMIStdIpAddressPrefixIfIndex,
                                    INT4 i4FsMIStdIpAddressPrefixType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdIpAddressPrefixPrefix,
                                    UINT4 u4FsMIStdIpAddressPrefixLength,
                                    INT4 *pi4RetValFsMIStdIpAddressPrefixOrigin)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpAddressPrefixOrigin (i4FsMIStdIpAddressPrefixIfIndex,
                                     i4FsMIStdIpAddressPrefixType,
                                     pFsMIStdIpAddressPrefixPrefix,
                                     u4FsMIStdIpAddressPrefixLength,
                                     pi4RetValFsMIStdIpAddressPrefixOrigin);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefixOnLinkFlag
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressPrefixOnLinkFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefixOnLinkFlag (INT4 i4FsMIStdIpAddressPrefixIfIndex,
                                        INT4 i4FsMIStdIpAddressPrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdIpAddressPrefixPrefix,
                                        UINT4 u4FsMIStdIpAddressPrefixLength,
                                        INT4
                                        *pi4RetValFsMIStdIpAddressPrefixOnLinkFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpAddressPrefixOnLinkFlag (i4FsMIStdIpAddressPrefixIfIndex,
                                         i4FsMIStdIpAddressPrefixType,
                                         pFsMIStdIpAddressPrefixPrefix,
                                         u4FsMIStdIpAddressPrefixLength,
                                         pi4RetValFsMIStdIpAddressPrefixOnLinkFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefixAutonomousFlag
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressPrefixAutonomousFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefixAutonomousFlag (INT4
                                            i4FsMIStdIpAddressPrefixIfIndex,
                                            INT4 i4FsMIStdIpAddressPrefixType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIStdIpAddressPrefixPrefix,
                                            UINT4
                                            u4FsMIStdIpAddressPrefixLength,
                                            INT4
                                            *pi4RetValFsMIStdIpAddressPrefixAutonomousFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpAddressPrefixAutonomousFlag (i4FsMIStdIpAddressPrefixIfIndex,
                                             i4FsMIStdIpAddressPrefixType,
                                             pFsMIStdIpAddressPrefixPrefix,
                                             u4FsMIStdIpAddressPrefixLength,
                                             pi4RetValFsMIStdIpAddressPrefixAutonomousFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefixAdvPreferredLifetime
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressPrefixAdvPreferredLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefixAdvPreferredLifetime (INT4
                                                  i4FsMIStdIpAddressPrefixIfIndex,
                                                  INT4
                                                  i4FsMIStdIpAddressPrefixType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIStdIpAddressPrefixPrefix,
                                                  UINT4
                                                  u4FsMIStdIpAddressPrefixLength,
                                                  UINT4
                                                  *pu4RetValFsMIStdIpAddressPrefixAdvPreferredLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpAddressPrefixAdvPreferredLifetime
        (i4FsMIStdIpAddressPrefixIfIndex, i4FsMIStdIpAddressPrefixType,
         pFsMIStdIpAddressPrefixPrefix, u4FsMIStdIpAddressPrefixLength,
         pu4RetValFsMIStdIpAddressPrefixAdvPreferredLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefixAdvValidLifetime
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressPrefixAdvValidLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefixAdvValidLifetime (INT4
                                              i4FsMIStdIpAddressPrefixIfIndex,
                                              INT4 i4FsMIStdIpAddressPrefixType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMIStdIpAddressPrefixPrefix,
                                              UINT4
                                              u4FsMIStdIpAddressPrefixLength,
                                              UINT4
                                              *pu4RetValFsMIStdIpAddressPrefixAdvValidLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpAddressPrefixAdvValidLifetime (i4FsMIStdIpAddressPrefixIfIndex,
                                               i4FsMIStdIpAddressPrefixType,
                                               pFsMIStdIpAddressPrefixPrefix,
                                               u4FsMIStdIpAddressPrefixLength,
                                               pu4RetValFsMIStdIpAddressPrefixAdvValidLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressContextId
 Input       :  The Indices
                FsMIStdIpAddressPrefixIfIndex
                FsMIStdIpAddressPrefixType
                FsMIStdIpAddressPrefixPrefix
                FsMIStdIpAddressPrefixLength

                The Object 
                retValFsMIStdIpAddressContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressContextId (INT4 i4FsMIStdIpAddressPrefixIfIndex,
                                 INT4 i4FsMIStdIpAddressPrefixType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdIpAddressPrefixPrefix,
                                 UINT4 u4FsMIStdIpAddressPrefixLength,
                                 INT4 *pi4RetValFsMIStdIpAddressContextId)
{
    UNUSED_PARAM (i4FsMIStdIpAddressPrefixType);
    UNUSED_PARAM (pFsMIStdIpAddressPrefixPrefix);
    UNUSED_PARAM (u4FsMIStdIpAddressPrefixLength);

    if (VcmGetContextIdFromCfaIfIndex (i4FsMIStdIpAddressPrefixIfIndex,
                                       (UINT4 *)
                                       pi4RetValFsMIStdIpAddressContextId) ==
        VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpAddressTable (INT4 i4FsMIStdIpContextId,
                                               INT4 i4FsMIStdIpAddressAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsMIStdIpAddressAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceIpAddressTable
        (i4FsMIStdIpAddressAddrType, pFsMIStdIpAddressAddr);

    UtilIpvxReleaseCxt ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpAddressTable (INT4 *pi4FsMIStdIpContextId,
                                       INT4 *pi4FsMIStdIpAddressAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdIpAddressAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = VCM_FAILURE;
    INT4                i4ContextId = 0;

    i4Value = VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == VCM_SUCCESS)
    {
        if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        i1RetVal = nmhGetFirstIndexIpAddressTable
            (pi4FsMIStdIpAddressAddrType, pFsMIStdIpAddressAddr);
        UtilIpvxReleaseCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        i4ContextId = *pi4FsMIStdIpContextId;
        i4Value = VcmGetNextActiveL3Context ((UINT4) i4ContextId,
                                             (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIStdIpAddressAddrType
                nextFsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr
                nextFsMIStdIpAddressAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpAddressTable (INT4 i4FsMIStdIpContextId,
                                      INT4 *pi4NextFsMIStdIpContextId,
                                      INT4 i4FsMIStdIpAddressAddrType,
                                      INT4 *pi4NextFsMIStdIpAddressAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdIpAddressAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsMIStdIpAddressAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            if (nmhGetNextIndexIpAddressTable (i4FsMIStdIpAddressAddrType,
                                               pi4NextFsMIStdIpAddressAddrType,
                                               pFsMIStdIpAddressAddr,
                                               pNextFsMIStdIpAddressAddr)
                == SNMP_SUCCESS)
            {
                *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
                UtilIpvxReleaseCxt ();
                return SNMP_SUCCESS;
            }
        }
    }

    /* Current context is invalid or both address types are already obtained
     * for the current session, hence retrive the next context id */

    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           != VCM_FAILURE)
    {
        if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIpAddressTable
                (pi4NextFsMIStdIpAddressAddrType, pNextFsMIStdIpAddressAddr);
            UtilIpvxReleaseCxt ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                return i1RetVal;
            }
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressIfIndex (INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdIpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                               INT4 *pi4RetValFsMIStdIpAddressIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressIfIndex (i4FsMIStdIpAddressAddrType,
                                pFsMIStdIpAddressAddr,
                                pi4RetValFsMIStdIpAddressIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressType (INT4 i4FsMIStdIpContextId,
                            INT4 i4FsMIStdIpAddressAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                            INT4 *pi4RetValFsMIStdIpAddressType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressType (i4FsMIStdIpAddressAddrType, pFsMIStdIpAddressAddr,
                             pi4RetValFsMIStdIpAddressType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressPrefix
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressPrefix
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressPrefix (INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMIStdIpAddressAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                              tSNMP_OID_TYPE * pRetValFsMIStdIpAddressPrefix)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressPrefix (i4FsMIStdIpAddressAddrType,
                               pFsMIStdIpAddressAddr,
                               pRetValFsMIStdIpAddressPrefix);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressOrigin
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressOrigin (INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMIStdIpAddressAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                              INT4 *pi4RetValFsMIStdIpAddressOrigin)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressOrigin (i4FsMIStdIpAddressAddrType,
                               pFsMIStdIpAddressAddr,
                               pi4RetValFsMIStdIpAddressOrigin);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressStatus (INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMIStdIpAddressAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                              INT4 *pi4RetValFsMIStdIpAddressStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressStatus (i4FsMIStdIpAddressAddrType,
                               pFsMIStdIpAddressAddr,
                               pi4RetValFsMIStdIpAddressStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressCreated
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressCreated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressCreated (INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdIpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                               UINT4 *pu4RetValFsMIStdIpAddressCreated)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressCreated (i4FsMIStdIpAddressAddrType,
                                pFsMIStdIpAddressAddr,
                                pu4RetValFsMIStdIpAddressCreated);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressLastChanged
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressLastChanged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressLastChanged (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdIpAddressAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdIpAddressAddr,
                                   UINT4 *pu4RetValFsMIStdIpAddressLastChanged)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressLastChanged (i4FsMIStdIpAddressAddrType,
                                    pFsMIStdIpAddressAddr,
                                    pu4RetValFsMIStdIpAddressLastChanged);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressRowStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressRowStatus (INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdIpAddressAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdIpAddressAddr,
                                 INT4 *pi4RetValFsMIStdIpAddressRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressRowStatus (i4FsMIStdIpAddressAddrType,
                                  pFsMIStdIpAddressAddr,
                                  pi4RetValFsMIStdIpAddressRowStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpAddressStorageType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                retValFsMIStdIpAddressStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpAddressStorageType (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdIpAddressAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdIpAddressAddr,
                                   INT4 *pi4RetValFsMIStdIpAddressStorageType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIpAddressStorageType (i4FsMIStdIpAddressAddrType,
                                    pFsMIStdIpAddressAddr,
                                    pi4RetValFsMIStdIpAddressStorageType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpAddressIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                setValFsMIStdIpAddressIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpAddressIfIndex (INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdIpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                               INT4 i4SetValFsMIStdIpAddressIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpAddressIfIndex (i4FsMIStdIpAddressAddrType,
                                pFsMIStdIpAddressAddr,
                                i4SetValFsMIStdIpAddressIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpAddressType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                setValFsMIStdIpAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpAddressType (INT4 i4FsMIStdIpContextId,
                            INT4 i4FsMIStdIpAddressAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                            INT4 i4SetValFsMIStdIpAddressType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpAddressType (i4FsMIStdIpAddressAddrType, pFsMIStdIpAddressAddr,
                             i4SetValFsMIStdIpAddressType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpAddressStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                setValFsMIStdIpAddressStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpAddressStatus (INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMIStdIpAddressAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                              INT4 i4SetValFsMIStdIpAddressStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpAddressStatus (i4FsMIStdIpAddressAddrType,
                               pFsMIStdIpAddressAddr,
                               i4SetValFsMIStdIpAddressStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpAddressRowStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                setValFsMIStdIpAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpAddressRowStatus (INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdIpAddressAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdIpAddressAddr,
                                 INT4 i4SetValFsMIStdIpAddressRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpAddressRowStatus (i4FsMIStdIpAddressAddrType,
                                  pFsMIStdIpAddressAddr,
                                  i4SetValFsMIStdIpAddressRowStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpAddressStorageType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                setValFsMIStdIpAddressStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpAddressStorageType (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdIpAddressAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdIpAddressAddr,
                                   INT4 i4SetValFsMIStdIpAddressStorageType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetIpAddressStorageType (i4FsMIStdIpAddressAddrType,
                                    pFsMIStdIpAddressAddr,
                                    i4SetValFsMIStdIpAddressStorageType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpAddressIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                testValFsMIStdIpAddressIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpAddressIfIndex (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  INT4 i4FsMIStdIpAddressAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdIpAddressAddr,
                                  INT4 i4TestValFsMIStdIpAddressIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2IpAddressIfIndex (pu4ErrorCode, i4FsMIStdIpAddressAddrType,
                                   pFsMIStdIpAddressAddr,
                                   i4TestValFsMIStdIpAddressIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpAddressType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                testValFsMIStdIpAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpAddressType (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdIpAddressAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsMIStdIpAddressAddr,
                               INT4 i4TestValFsMIStdIpAddressType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2IpAddressType (pu4ErrorCode, i4FsMIStdIpAddressAddrType,
                                pFsMIStdIpAddressAddr,
                                i4TestValFsMIStdIpAddressType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpAddressStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                testValFsMIStdIpAddressStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpAddressStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdIpAddressAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdIpAddressAddr,
                                 INT4 i4TestValFsMIStdIpAddressStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2IpAddressStatus (pu4ErrorCode, i4FsMIStdIpAddressAddrType,
                                  pFsMIStdIpAddressAddr,
                                  i4TestValFsMIStdIpAddressStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpAddressRowStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                testValFsMIStdIpAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpAddressRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdIpAddressAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdIpAddressAddr,
                                    INT4 i4TestValFsMIStdIpAddressRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2IpAddressRowStatus (pu4ErrorCode, i4FsMIStdIpAddressAddrType,
                                     pFsMIStdIpAddressAddr,
                                     i4TestValFsMIStdIpAddressRowStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpAddressStorageType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr

                The Object 
                testValFsMIStdIpAddressStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpAddressStorageType (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdIpAddressAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdIpAddressAddr,
                                      INT4 i4TestValFsMIStdIpAddressStorageType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2IpAddressStorageType (pu4ErrorCode, i4FsMIStdIpAddressAddrType,
                                       pFsMIStdIpAddressAddr,
                                       i4TestValFsMIStdIpAddressStorageType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIpAddressAddrType
                FsMIStdIpAddressAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpAddressTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpNetToPhysicalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpNetToPhysicalTable
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpNetToPhysicalTable (INT4
                                                     i4FsMIStdIpNetToPhysicalIfIndex,
                                                     INT4
                                                     i4FsMIStdIpNetToPhysicalNetAddressType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsMIStdIpNetToPhysicalNetAddress)
{
    return (nmhValidateIndexInstanceIpNetToPhysicalTable
            (i4FsMIStdIpNetToPhysicalIfIndex,
             i4FsMIStdIpNetToPhysicalNetAddressType,
             pFsMIStdIpNetToPhysicalNetAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpNetToPhysicalTable
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpNetToPhysicalTable (INT4
                                             *pi4FsMIStdIpNetToPhysicalIfIndex,
                                             INT4
                                             *pi4FsMIStdIpNetToPhysicalNetAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdIpNetToPhysicalNetAddress)
{
    return (nmhGetFirstIndexIpNetToPhysicalTable
            (pi4FsMIStdIpNetToPhysicalIfIndex,
             pi4FsMIStdIpNetToPhysicalNetAddressType,
             pFsMIStdIpNetToPhysicalNetAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpNetToPhysicalTable
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                nextFsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                nextFsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress
                nextFsMIStdIpNetToPhysicalNetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpNetToPhysicalTable (INT4
                                            i4FsMIStdIpNetToPhysicalIfIndex,
                                            INT4
                                            *pi4NextFsMIStdIpNetToPhysicalIfIndex,
                                            INT4
                                            i4FsMIStdIpNetToPhysicalNetAddressType,
                                            INT4
                                            *pi4NextFsMIStdIpNetToPhysicalNetAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIStdIpNetToPhysicalNetAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsMIStdIpNetToPhysicalNetAddress)
{
    return (nmhGetNextIndexIpNetToPhysicalTable
            (i4FsMIStdIpNetToPhysicalIfIndex,
             pi4NextFsMIStdIpNetToPhysicalIfIndex,
             i4FsMIStdIpNetToPhysicalNetAddressType,
             pi4NextFsMIStdIpNetToPhysicalNetAddressType,
             pFsMIStdIpNetToPhysicalNetAddress,
             pNextFsMIStdIpNetToPhysicalNetAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalPhysAddress
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalPhysAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalPhysAddress (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                         INT4
                                         i4FsMIStdIpNetToPhysicalNetAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdIpNetToPhysicalNetAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsMIStdIpNetToPhysicalPhysAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpNetToPhysicalPhysAddress (i4FsMIStdIpNetToPhysicalIfIndex,
                                          i4FsMIStdIpNetToPhysicalNetAddressType,
                                          pFsMIStdIpNetToPhysicalNetAddress,
                                          pRetValFsMIStdIpNetToPhysicalPhysAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalLastUpdated
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalLastUpdated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalLastUpdated (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                         INT4
                                         i4FsMIStdIpNetToPhysicalNetAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdIpNetToPhysicalNetAddress,
                                         UINT4
                                         *pu4RetValFsMIStdIpNetToPhysicalLastUpdated)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpNetToPhysicalLastUpdated (i4FsMIStdIpNetToPhysicalIfIndex,
                                          i4FsMIStdIpNetToPhysicalNetAddressType,
                                          pFsMIStdIpNetToPhysicalNetAddress,
                                          pu4RetValFsMIStdIpNetToPhysicalLastUpdated);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalType
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalType (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                  INT4 i4FsMIStdIpNetToPhysicalNetAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdIpNetToPhysicalNetAddress,
                                  INT4 *pi4RetValFsMIStdIpNetToPhysicalType)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpNetToPhysicalType (i4FsMIStdIpNetToPhysicalIfIndex,
                                   i4FsMIStdIpNetToPhysicalNetAddressType,
                                   pFsMIStdIpNetToPhysicalNetAddress,
                                   pi4RetValFsMIStdIpNetToPhysicalType);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalState
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalState (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                   INT4 i4FsMIStdIpNetToPhysicalNetAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdIpNetToPhysicalNetAddress,
                                   INT4 *pi4RetValFsMIStdIpNetToPhysicalState)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpNetToPhysicalState (i4FsMIStdIpNetToPhysicalIfIndex,
                                    i4FsMIStdIpNetToPhysicalNetAddressType,
                                    pFsMIStdIpNetToPhysicalNetAddress,
                                    pi4RetValFsMIStdIpNetToPhysicalState);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalRowStatus
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalRowStatus (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                       INT4
                                       i4FsMIStdIpNetToPhysicalNetAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdIpNetToPhysicalNetAddress,
                                       INT4
                                       *pi4RetValFsMIStdIpNetToPhysicalRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpNetToPhysicalRowStatus (i4FsMIStdIpNetToPhysicalIfIndex,
                                        i4FsMIStdIpNetToPhysicalNetAddressType,
                                        pFsMIStdIpNetToPhysicalNetAddress,
                                        pi4RetValFsMIStdIpNetToPhysicalRowStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpNetToPhysicalContextId
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                retValFsMIStdIpNetToPhysicalContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpNetToPhysicalContextId (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                       INT4
                                       i4FsMIStdIpNetToPhysicalNetAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdIpNetToPhysicalNetAddress,
                                       INT4
                                       *pi4RetValFsMIStdIpNetToPhysicalContextId)
{
    UNUSED_PARAM (i4FsMIStdIpNetToPhysicalNetAddressType);
    UNUSED_PARAM (pFsMIStdIpNetToPhysicalNetAddress);
    UNUSED_PARAM (pi4RetValFsMIStdIpNetToPhysicalContextId);

    if (VcmGetContextIdFromCfaIfIndex (i4FsMIStdIpNetToPhysicalIfIndex,
                                       (UINT4 *)
                                       pi4RetValFsMIStdIpNetToPhysicalContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpNetToPhysicalPhysAddress
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                setValFsMIStdIpNetToPhysicalPhysAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpNetToPhysicalPhysAddress (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                         INT4
                                         i4FsMIStdIpNetToPhysicalNetAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdIpNetToPhysicalNetAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValFsMIStdIpNetToPhysicalPhysAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpNetToPhysicalPhysAddress (i4FsMIStdIpNetToPhysicalIfIndex,
                                          i4FsMIStdIpNetToPhysicalNetAddressType,
                                          pFsMIStdIpNetToPhysicalNetAddress,
                                          pSetValFsMIStdIpNetToPhysicalPhysAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpNetToPhysicalType
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                setValFsMIStdIpNetToPhysicalType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpNetToPhysicalType (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                  INT4 i4FsMIStdIpNetToPhysicalNetAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdIpNetToPhysicalNetAddress,
                                  INT4 i4SetValFsMIStdIpNetToPhysicalType)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpNetToPhysicalType (i4FsMIStdIpNetToPhysicalIfIndex,
                                   i4FsMIStdIpNetToPhysicalNetAddressType,
                                   pFsMIStdIpNetToPhysicalNetAddress,
                                   i4SetValFsMIStdIpNetToPhysicalType);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpNetToPhysicalRowStatus
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                setValFsMIStdIpNetToPhysicalRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpNetToPhysicalRowStatus (INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                       INT4
                                       i4FsMIStdIpNetToPhysicalNetAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdIpNetToPhysicalNetAddress,
                                       INT4
                                       i4SetValFsMIStdIpNetToPhysicalRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpNetToPhysicalRowStatus (i4FsMIStdIpNetToPhysicalIfIndex,
                                        i4FsMIStdIpNetToPhysicalNetAddressType,
                                        pFsMIStdIpNetToPhysicalNetAddress,
                                        i4SetValFsMIStdIpNetToPhysicalRowStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpNetToPhysicalPhysAddress
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                testValFsMIStdIpNetToPhysicalPhysAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpNetToPhysicalPhysAddress (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4FsMIStdIpNetToPhysicalIfIndex,
                                            INT4
                                            i4FsMIStdIpNetToPhysicalNetAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIStdIpNetToPhysicalNetAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFsMIStdIpNetToPhysicalPhysAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2IpNetToPhysicalPhysAddress (pu4ErrorCode,
                                             i4FsMIStdIpNetToPhysicalIfIndex,
                                             i4FsMIStdIpNetToPhysicalNetAddressType,
                                             pFsMIStdIpNetToPhysicalNetAddress,
                                             pTestValFsMIStdIpNetToPhysicalPhysAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpNetToPhysicalType
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                testValFsMIStdIpNetToPhysicalType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpNetToPhysicalType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                     INT4
                                     i4FsMIStdIpNetToPhysicalNetAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdIpNetToPhysicalNetAddress,
                                     INT4 i4TestValFsMIStdIpNetToPhysicalType)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2IpNetToPhysicalType (pu4ErrorCode,
                                      i4FsMIStdIpNetToPhysicalIfIndex,
                                      i4FsMIStdIpNetToPhysicalNetAddressType,
                                      pFsMIStdIpNetToPhysicalNetAddress,
                                      i4TestValFsMIStdIpNetToPhysicalType);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpNetToPhysicalRowStatus
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress

                The Object 
                testValFsMIStdIpNetToPhysicalRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpNetToPhysicalRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdIpNetToPhysicalIfIndex,
                                          INT4
                                          i4FsMIStdIpNetToPhysicalNetAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdIpNetToPhysicalNetAddress,
                                          INT4
                                          i4TestValFsMIStdIpNetToPhysicalRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2IpNetToPhysicalRowStatus (pu4ErrorCode,
                                           i4FsMIStdIpNetToPhysicalIfIndex,
                                           i4FsMIStdIpNetToPhysicalNetAddressType,
                                           pFsMIStdIpNetToPhysicalNetAddress,
                                           i4TestValFsMIStdIpNetToPhysicalRowStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpNetToPhysicalTable
 Input       :  The Indices
                FsMIStdIpNetToPhysicalIfIndex
                FsMIStdIpNetToPhysicalNetAddressType
                FsMIStdIpNetToPhysicalNetAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpNetToPhysicalTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpv6ScopeZoneIndexTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpv6ScopeZoneIndexTable
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpv6ScopeZoneIndexTable (INT4
                                                        i4FsMIStdIpv6ScopeZoneIndexIfIndex)
{
    return (nmhValidateIndexInstanceIpv6ScopeZoneIndexTable
            (i4FsMIStdIpv6ScopeZoneIndexIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpv6ScopeZoneIndexTable
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpv6ScopeZoneIndexTable (INT4
                                                *pi4FsMIStdIpv6ScopeZoneIndexIfIndex)
{
    return (nmhGetFirstIndexIpv6ScopeZoneIndexTable
            (pi4FsMIStdIpv6ScopeZoneIndexIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpv6ScopeZoneIndexTable
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex
                nextFsMIStdIpv6ScopeZoneIndexIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpv6ScopeZoneIndexTable (INT4
                                               i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                               INT4
                                               *pi4NextFsMIStdIpv6ScopeZoneIndexIfIndex)
{
    return (nmhGetNextIndexIpv6ScopeZoneIndexTable
            (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
             pi4NextFsMIStdIpv6ScopeZoneIndexIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexLinkLocal
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexLinkLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexLinkLocal (INT4
                                          i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6ScopeZoneIndexLinkLocal)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexLinkLocal (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                           pu4RetValFsMIStdIpv6ScopeZoneIndexLinkLocal);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndex3
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndex3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndex3 (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndex3)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndex3 (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndex3);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexAdminLocal
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexAdminLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexAdminLocal (INT4
                                           i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                           UINT4
                                           *pu4RetValFsMIStdIpv6ScopeZoneIndexAdminLocal)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexAdminLocal (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                            pu4RetValFsMIStdIpv6ScopeZoneIndexAdminLocal);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexSiteLocal
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexSiteLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexSiteLocal (INT4
                                          i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6ScopeZoneIndexSiteLocal)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexSiteLocal (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                           pu4RetValFsMIStdIpv6ScopeZoneIndexSiteLocal);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndex6
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndex6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndex6 (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndex6)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndex6 (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndex6);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndex7
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndex7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndex7 (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndex7)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndex7 (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndex7);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexOrganizationLocal
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexOrganizationLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexOrganizationLocal (INT4
                                                  i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                                  UINT4
                                                  *pu4RetValFsMIStdIpv6ScopeZoneIndexOrganizationLocal)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexOrganizationLocal
        (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
         pu4RetValFsMIStdIpv6ScopeZoneIndexOrganizationLocal);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndex9
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndex9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndex9 (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndex9)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndex9 (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndex9);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexA
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexA (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndexA)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexA (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndexA);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexB
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexB (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndexB)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexB (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndexB);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexC
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexC (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndexC)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexC (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndexC);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneIndexD
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneIndexD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneIndexD (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                  UINT4 *pu4RetValFsMIStdIpv6ScopeZoneIndexD)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpv6ScopeZoneIndexD (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                   pu4RetValFsMIStdIpv6ScopeZoneIndexD);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6ScopeZoneContextId
 Input       :  The Indices
                FsMIStdIpv6ScopeZoneIndexIfIndex

                The Object 
                retValFsMIStdIpv6ScopeZoneContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6ScopeZoneContextId (INT4 i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                     INT4
                                     *pi4RetValFsMIStdIpv6ScopeZoneContextId)
{
    if (VcmGetContextIdFromCfaIfIndex (i4FsMIStdIpv6ScopeZoneIndexIfIndex,
                                       (UINT4 *)
                                       pi4RetValFsMIStdIpv6ScopeZoneContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIpDefaultRouterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpDefaultRouterTable
 Input       :  The Indices
                FsMIStdIpDefaultRouterAddressType
                FsMIStdIpDefaultRouterAddress
                FsMIStdIpDefaultRouterIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpDefaultRouterTable (INT4
                                                     i4FsMIStdIpDefaultRouterAddressType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsMIStdIpDefaultRouterAddress,
                                                     INT4
                                                     i4FsMIStdIpDefaultRouterIfIndex)
{
    return (nmhValidateIndexInstanceIpDefaultRouterTable
            (i4FsMIStdIpDefaultRouterAddressType,
             pFsMIStdIpDefaultRouterAddress, i4FsMIStdIpDefaultRouterIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpDefaultRouterTable
 Input       :  The Indices
                FsMIStdIpDefaultRouterAddressType
                FsMIStdIpDefaultRouterAddress
                FsMIStdIpDefaultRouterIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpDefaultRouterTable (INT4
                                             *pi4FsMIStdIpDefaultRouterAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdIpDefaultRouterAddress,
                                             INT4
                                             *pi4FsMIStdIpDefaultRouterIfIndex)
{
    return (nmhGetFirstIndexIpDefaultRouterTable
            (pi4FsMIStdIpDefaultRouterAddressType,
             pFsMIStdIpDefaultRouterAddress, pi4FsMIStdIpDefaultRouterIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpDefaultRouterTable
 Input       :  The Indices
                FsMIStdIpDefaultRouterAddressType
                nextFsMIStdIpDefaultRouterAddressType
                FsMIStdIpDefaultRouterAddress
                nextFsMIStdIpDefaultRouterAddress
                FsMIStdIpDefaultRouterIfIndex
                nextFsMIStdIpDefaultRouterIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpDefaultRouterTable (INT4
                                            i4FsMIStdIpDefaultRouterAddressType,
                                            INT4
                                            *pi4NextFsMIStdIpDefaultRouterAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIStdIpDefaultRouterAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsMIStdIpDefaultRouterAddress,
                                            INT4
                                            i4FsMIStdIpDefaultRouterIfIndex,
                                            INT4
                                            *pi4NextFsMIStdIpDefaultRouterIfIndex)
{
    return (nmhGetNextIndexIpDefaultRouterTable
            (i4FsMIStdIpDefaultRouterAddressType,
             pi4NextFsMIStdIpDefaultRouterAddressType,
             pFsMIStdIpDefaultRouterAddress, pNextFsMIStdIpDefaultRouterAddress,
             i4FsMIStdIpDefaultRouterIfIndex,
             pi4NextFsMIStdIpDefaultRouterIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpDefaultRouterLifetime
 Input       :  The Indices
                FsMIStdIpDefaultRouterAddressType
                FsMIStdIpDefaultRouterAddress
                FsMIStdIpDefaultRouterIfIndex

                The Object 
                retValFsMIStdIpDefaultRouterLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpDefaultRouterLifetime (INT4 i4FsMIStdIpDefaultRouterAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdIpDefaultRouterAddress,
                                      INT4 i4FsMIStdIpDefaultRouterIfIndex,
                                      UINT4
                                      *pu4RetValFsMIStdIpDefaultRouterLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpDefaultRouterLifetime (i4FsMIStdIpDefaultRouterAddressType,
                                       pFsMIStdIpDefaultRouterAddress,
                                       i4FsMIStdIpDefaultRouterIfIndex,
                                       pu4RetValFsMIStdIpDefaultRouterLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpDefaultRouterPreference
 Input       :  The Indices
                FsMIStdIpDefaultRouterAddressType
                FsMIStdIpDefaultRouterAddress
                FsMIStdIpDefaultRouterIfIndex

                The Object 
                retValFsMIStdIpDefaultRouterPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpDefaultRouterPreference (INT4
                                        i4FsMIStdIpDefaultRouterAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdIpDefaultRouterAddress,
                                        INT4 i4FsMIStdIpDefaultRouterIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpDefaultRouterPreference)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetIpDefaultRouterPreference (i4FsMIStdIpDefaultRouterAddressType,
                                         pFsMIStdIpDefaultRouterAddress,
                                         i4FsMIStdIpDefaultRouterIfIndex,
                                         pi4RetValFsMIStdIpDefaultRouterPreference);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdIpv6RouterAdvertTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIpv6RouterAdvertTable
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIpv6RouterAdvertTable (INT4
                                                      i4FsMIStdIpv6RouterAdvertIfIndex)
{
    return (nmhValidateIndexInstanceIpv6RouterAdvertTable
            (i4FsMIStdIpv6RouterAdvertIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIpv6RouterAdvertTable
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIpv6RouterAdvertTable (INT4
                                              *pi4FsMIStdIpv6RouterAdvertIfIndex)
{
    return (nmhGetFirstIndexIpv6RouterAdvertTable
            (pi4FsMIStdIpv6RouterAdvertIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIpv6RouterAdvertTable
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex
                nextFsMIStdIpv6RouterAdvertIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIpv6RouterAdvertTable (INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             INT4
                                             *pi4NextFsMIStdIpv6RouterAdvertIfIndex)
{
    return (nmhGetNextIndexIpv6RouterAdvertTable
            (i4FsMIStdIpv6RouterAdvertIfIndex,
             pi4NextFsMIStdIpv6RouterAdvertIfIndex));

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertSendAdverts
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertSendAdverts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertSendAdverts (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          INT4
                                          *pi4RetValFsMIStdIpv6RouterAdvertSendAdverts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertSendAdverts
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pi4RetValFsMIStdIpv6RouterAdvertSendAdverts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertMaxInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertMaxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertMaxInterval (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6RouterAdvertMaxInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertMaxInterval
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertMaxInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertMinInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertMinInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertMinInterval (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6RouterAdvertMinInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertMinInterval
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertMinInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertManagedFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertManagedFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertManagedFlag (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          INT4
                                          *pi4RetValFsMIStdIpv6RouterAdvertManagedFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertManagedFlag
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pi4RetValFsMIStdIpv6RouterAdvertManagedFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertOtherConfigFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertOtherConfigFlag (INT4
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              INT4
                                              *pi4RetValFsMIStdIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertOtherConfigFlag
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pi4RetValFsMIStdIpv6RouterAdvertOtherConfigFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertLinkMTU
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertLinkMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertLinkMTU (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                      UINT4
                                      *pu4RetValFsMIStdIpv6RouterAdvertLinkMTU)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertLinkMTU
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertLinkMTU);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertReachableTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertReachableTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertReachableTime (INT4
                                            i4FsMIStdIpv6RouterAdvertIfIndex,
                                            UINT4
                                            *pu4RetValFsMIStdIpv6RouterAdvertReachableTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertReachableTime
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertReachableTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertRetransmitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertRetransmitTime (INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             UINT4
                                             *pu4RetValFsMIStdIpv6RouterAdvertRetransmitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertRetransmitTime
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertRetransmitTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertCurHopLimit
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertCurHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertCurHopLimit (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          *pu4RetValFsMIStdIpv6RouterAdvertCurHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetIpv6RouterAdvertCurHopLimit
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertCurHopLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertDefaultLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertDefaultLifetime (INT4
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              UINT4
                                              *pu4RetValFsMIStdIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetIpv6RouterAdvertDefaultLifetime
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pu4RetValFsMIStdIpv6RouterAdvertDefaultLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertRowStatus
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertRowStatus (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpv6RouterAdvertRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetIpv6RouterAdvertRowStatus
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         pi4RetValFsMIStdIpv6RouterAdvertRowStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIpv6RouterAdvertContextId
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                retValFsMIStdIpv6RouterAdvertContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIpv6RouterAdvertContextId (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                        INT4
                                        *pi4RetValFsMIStdIpv6RouterAdvertContextId)
{
    if (VcmGetContextIdFromCfaIfIndex
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         (UINT4 *) pi4RetValFsMIStdIpv6RouterAdvertContextId) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertSendAdverts
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertSendAdverts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertSendAdverts (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          INT4
                                          i4SetValFsMIStdIpv6RouterAdvertSendAdverts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetIpv6RouterAdvertSendAdverts
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         i4SetValFsMIStdIpv6RouterAdvertSendAdverts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertMaxInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertMaxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertMaxInterval (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          u4SetValFsMIStdIpv6RouterAdvertMaxInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetIpv6RouterAdvertMaxInterval
        (i4FsMIStdIpv6RouterAdvertIfIndex,
         u4SetValFsMIStdIpv6RouterAdvertMaxInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertMinInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertMinInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertMinInterval (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          u4SetValFsMIStdIpv6RouterAdvertMinInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertMinInterval (i4FsMIStdIpv6RouterAdvertIfIndex,
                                           u4SetValFsMIStdIpv6RouterAdvertMinInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertManagedFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertManagedFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertManagedFlag (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          INT4
                                          i4SetValFsMIStdIpv6RouterAdvertManagedFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertManagedFlag (i4FsMIStdIpv6RouterAdvertIfIndex,
                                           i4SetValFsMIStdIpv6RouterAdvertManagedFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertOtherConfigFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertOtherConfigFlag (INT4
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              INT4
                                              i4SetValFsMIStdIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertOtherConfigFlag (i4FsMIStdIpv6RouterAdvertIfIndex,
                                               i4SetValFsMIStdIpv6RouterAdvertOtherConfigFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertLinkMTU
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertLinkMTU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertLinkMTU (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                      UINT4
                                      u4SetValFsMIStdIpv6RouterAdvertLinkMTU)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertLinkMTU (i4FsMIStdIpv6RouterAdvertIfIndex,
                                       u4SetValFsMIStdIpv6RouterAdvertLinkMTU);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertReachableTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertReachableTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertReachableTime (INT4
                                            i4FsMIStdIpv6RouterAdvertIfIndex,
                                            UINT4
                                            u4SetValFsMIStdIpv6RouterAdvertReachableTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertReachableTime (i4FsMIStdIpv6RouterAdvertIfIndex,
                                             u4SetValFsMIStdIpv6RouterAdvertReachableTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertRetransmitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertRetransmitTime (INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             UINT4
                                             u4SetValFsMIStdIpv6RouterAdvertRetransmitTime)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return =
        nmhSetIpv6RouterAdvertRetransmitTime (i4FsMIStdIpv6RouterAdvertIfIndex,
                                              u4SetValFsMIStdIpv6RouterAdvertRetransmitTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertCurHopLimit
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertCurHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertCurHopLimit (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                          UINT4
                                          u4SetValFsMIStdIpv6RouterAdvertCurHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertCurHopLimit (i4FsMIStdIpv6RouterAdvertIfIndex,
                                           u4SetValFsMIStdIpv6RouterAdvertCurHopLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertDefaultLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertDefaultLifetime (INT4
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              UINT4
                                              u4SetValFsMIStdIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertDefaultLifetime (i4FsMIStdIpv6RouterAdvertIfIndex,
                                               u4SetValFsMIStdIpv6RouterAdvertDefaultLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdIpv6RouterAdvertRowStatus
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                setValFsMIStdIpv6RouterAdvertRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdIpv6RouterAdvertRowStatus (INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                        INT4
                                        i4SetValFsMIStdIpv6RouterAdvertRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetIpv6RouterAdvertRowStatus (i4FsMIStdIpv6RouterAdvertIfIndex,
                                         i4SetValFsMIStdIpv6RouterAdvertRowStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertSendAdverts
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertSendAdverts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertSendAdverts (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             INT4
                                             i4TestValFsMIStdIpv6RouterAdvertSendAdverts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertSendAdverts (pu4ErrorCode,
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              i4TestValFsMIStdIpv6RouterAdvertSendAdverts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertMaxInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertMaxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertMaxInterval (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             UINT4
                                             u4TestValFsMIStdIpv6RouterAdvertMaxInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertMaxInterval (pu4ErrorCode,
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              u4TestValFsMIStdIpv6RouterAdvertMaxInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertMinInterval
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertMinInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertMinInterval (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             UINT4
                                             u4TestValFsMIStdIpv6RouterAdvertMinInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertMinInterval (pu4ErrorCode,
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              u4TestValFsMIStdIpv6RouterAdvertMinInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertManagedFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertManagedFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertManagedFlag (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             INT4
                                             i4TestValFsMIStdIpv6RouterAdvertManagedFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertManagedFlag (pu4ErrorCode,
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              i4TestValFsMIStdIpv6RouterAdvertManagedFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertOtherConfigFlag
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertOtherConfigFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertOtherConfigFlag (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4FsMIStdIpv6RouterAdvertIfIndex,
                                                 INT4
                                                 i4TestValFsMIStdIpv6RouterAdvertOtherConfigFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertOtherConfigFlag (pu4ErrorCode,
                                                  i4FsMIStdIpv6RouterAdvertIfIndex,
                                                  i4TestValFsMIStdIpv6RouterAdvertOtherConfigFlag);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertLinkMTU
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertLinkMTU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertLinkMTU (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdIpv6RouterAdvertIfIndex,
                                         UINT4
                                         u4TestValFsMIStdIpv6RouterAdvertLinkMTU)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertLinkMTU (pu4ErrorCode,
                                          i4FsMIStdIpv6RouterAdvertIfIndex,
                                          u4TestValFsMIStdIpv6RouterAdvertLinkMTU);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertReachableTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertReachableTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertReachableTime (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FsMIStdIpv6RouterAdvertIfIndex,
                                               UINT4
                                               u4TestValFsMIStdIpv6RouterAdvertReachableTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertReachableTime (pu4ErrorCode,
                                                i4FsMIStdIpv6RouterAdvertIfIndex,
                                                u4TestValFsMIStdIpv6RouterAdvertReachableTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertRetransmitTime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertRetransmitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertRetransmitTime (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsMIStdIpv6RouterAdvertIfIndex,
                                                UINT4
                                                u4TestValFsMIStdIpv6RouterAdvertRetransmitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertRetransmitTime (pu4ErrorCode,
                                                 i4FsMIStdIpv6RouterAdvertIfIndex,
                                                 u4TestValFsMIStdIpv6RouterAdvertRetransmitTime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertCurHopLimit
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertCurHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertCurHopLimit (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIStdIpv6RouterAdvertIfIndex,
                                             UINT4
                                             u4TestValFsMIStdIpv6RouterAdvertCurHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertCurHopLimit (pu4ErrorCode,
                                              i4FsMIStdIpv6RouterAdvertIfIndex,
                                              u4TestValFsMIStdIpv6RouterAdvertCurHopLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertDefaultLifetime
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertDefaultLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertDefaultLifetime (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4FsMIStdIpv6RouterAdvertIfIndex,
                                                 UINT4
                                                 u4TestValFsMIStdIpv6RouterAdvertDefaultLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertDefaultLifetime (pu4ErrorCode,
                                                  i4FsMIStdIpv6RouterAdvertIfIndex,
                                                  u4TestValFsMIStdIpv6RouterAdvertDefaultLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdIpv6RouterAdvertRowStatus
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex

                The Object 
                testValFsMIStdIpv6RouterAdvertRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdIpv6RouterAdvertRowStatus (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4FsMIStdIpv6RouterAdvertIfIndex,
                                           INT4
                                           i4TestValFsMIStdIpv6RouterAdvertRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2Ipv6RouterAdvertRowStatus (pu4ErrorCode,
                                            i4FsMIStdIpv6RouterAdvertIfIndex,
                                            i4TestValFsMIStdIpv6RouterAdvertRowStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdIpv6RouterAdvertTable
 Input       :  The Indices
                FsMIStdIpv6RouterAdvertIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdIpv6RouterAdvertTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdIcmpStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIcmpStatsTable (INT4 i4FsMIStdIpContextId,
                                               INT4 i4FsMIStdIcmpStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceIcmpStatsTable (i4FsMIStdIcmpStatsIPVersion);
    UtilIpvxReleaseCxt ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIcmpStatsTable (INT4 *pi4FsMIStdIpContextId,
                                       INT4 *pi4FsMIStdIcmpStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = VCM_FAILURE;
    INT4                i4ContextId = 0;

    i4Value = VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == VCM_SUCCESS)
    {
        if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIcmpStatsTable
                (pi4FsMIStdIcmpStatsIPVersion);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
        i4ContextId = *pi4FsMIStdIpContextId;
        i4Value = VcmGetNextActiveL3Context ((UINT4) i4ContextId,
                                             (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIcmpStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion
                nextFsMIStdIcmpStatsIPVersion
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIcmpStatsTable (INT4 i4FsMIStdIpContextId,
                                      INT4 *pi4NextFsMIStdIpContextId,
                                      INT4 i4FsMIStdIcmpStatsIPVersion,
                                      INT4 *pi4NextFsMIStdIcmpStatsIPVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            if (nmhGetNextIndexIcmpStatsTable (i4FsMIStdIcmpStatsIPVersion,
                                               pi4NextFsMIStdIcmpStatsIPVersion)
                == SNMP_SUCCESS)
            {
                UtilIpvxReleaseCxt ();
                *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
                return SNMP_SUCCESS;
            }
        }
    }

    /* Current context is invalid or both address types are already obtained
     * for the current session, hence retrive the next context id */

    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           != VCM_FAILURE)
    {
        if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal =
                nmhGetFirstIndexIcmpStatsTable
                (pi4NextFsMIStdIcmpStatsIPVersion);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpStatsInMsgs
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion

                The Object 
                retValFsMIStdIcmpStatsInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpStatsInMsgs (INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMIStdIcmpStatsIPVersion,
                              UINT4 *pu4RetValFsMIStdIcmpStatsInMsgs)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpStatsInMsgs (i4FsMIStdIcmpStatsIPVersion,
                               pu4RetValFsMIStdIcmpStatsInMsgs);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpStatsInErrors
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion

                The Object 
                retValFsMIStdIcmpStatsInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpStatsInErrors (INT4 i4FsMIStdIpContextId,
                                INT4 i4FsMIStdIcmpStatsIPVersion,
                                UINT4 *pu4RetValFsMIStdIcmpStatsInErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpStatsInErrors (i4FsMIStdIcmpStatsIPVersion,
                                 pu4RetValFsMIStdIcmpStatsInErrors);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpStatsOutMsgs
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion

                The Object 
                retValFsMIStdIcmpStatsOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpStatsOutMsgs (INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdIcmpStatsIPVersion,
                               UINT4 *pu4RetValFsMIStdIcmpStatsOutMsgs)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpStatsOutMsgs (i4FsMIStdIcmpStatsIPVersion,
                                pu4RetValFsMIStdIcmpStatsOutMsgs);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpStatsOutErrors
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpStatsIPVersion

                The Object 
                retValFsMIStdIcmpStatsOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpStatsOutErrors (INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdIcmpStatsIPVersion,
                                 UINT4 *pu4RetValFsMIStdIcmpStatsOutErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpStatsOutErrors (i4FsMIStdIcmpStatsIPVersion,
                                  pu4RetValFsMIStdIcmpStatsOutErrors);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdIcmpMsgStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdIcmpMsgStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpMsgStatsIPVersion
                FsMIStdIcmpMsgStatsType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdIcmpMsgStatsTable (INT4 i4FsMIStdIpContextId,
                                                  INT4
                                                  i4FsMIStdIcmpMsgStatsIPVersion,
                                                  INT4
                                                  i4FsMIStdIcmpMsgStatsType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceIcmpMsgStatsTable
        (i4FsMIStdIcmpMsgStatsIPVersion, i4FsMIStdIcmpMsgStatsType);
    UtilIpvxReleaseCxt ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdIcmpMsgStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpMsgStatsIPVersion
                FsMIStdIcmpMsgStatsType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdIcmpMsgStatsTable (INT4 *pi4FsMIStdIpContextId,
                                          INT4 *pi4FsMIStdIcmpMsgStatsIPVersion,
                                          INT4 *pi4FsMIStdIcmpMsgStatsType)
{

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Value = VCM_FAILURE;
    INT4                i4ContextId = 0;

    i4Value = VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i4Value == VCM_SUCCESS)
    {
        if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIcmpMsgStatsTable
                (pi4FsMIStdIcmpMsgStatsIPVersion, pi4FsMIStdIcmpMsgStatsType);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        i4ContextId = *pi4FsMIStdIpContextId;
        i4Value = VcmGetNextActiveL3Context ((UINT4) i4ContextId,
                                             (UINT4 *) pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdIcmpMsgStatsTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIStdIcmpMsgStatsIPVersion
                nextFsMIStdIcmpMsgStatsIPVersion
                FsMIStdIcmpMsgStatsType
                nextFsMIStdIcmpMsgStatsType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdIcmpMsgStatsTable (INT4 i4FsMIStdIpContextId,
                                         INT4 *pi4NextFsMIStdIpContextId,
                                         INT4 i4FsMIStdIcmpMsgStatsIPVersion,
                                         INT4
                                         *pi4NextFsMIStdIcmpMsgStatsIPVersion,
                                         INT4 i4FsMIStdIcmpMsgStatsType,
                                         INT4 *pi4NextFsMIStdIcmpMsgStatsType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_SUCCESS)
        {
            if (nmhGetNextIndexIcmpMsgStatsTable
                (i4FsMIStdIcmpMsgStatsIPVersion,
                 pi4NextFsMIStdIcmpMsgStatsIPVersion,
                 i4FsMIStdIcmpMsgStatsType,
                 pi4NextFsMIStdIcmpMsgStatsType) == SNMP_SUCCESS)
            {
                *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
                UtilIpvxReleaseCxt ();
                return SNMP_SUCCESS;
            }
        }
    }

    /* Current context is invalid or both address types are already obtained
     * for the current session, hence retrive the next context id */

    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           != VCM_FAILURE)
    {
        if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_SUCCESS)
        {
            i1RetVal = nmhGetFirstIndexIcmpMsgStatsTable
                (pi4NextFsMIStdIcmpMsgStatsIPVersion,
                 pi4NextFsMIStdIcmpMsgStatsType);
            UtilIpvxReleaseCxt ();
        }
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpMsgStatsInPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpMsgStatsIPVersion
                FsMIStdIcmpMsgStatsType

                The Object 
                retValFsMIStdIcmpMsgStatsInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpMsgStatsInPkts (INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdIcmpMsgStatsIPVersion,
                                 INT4 i4FsMIStdIcmpMsgStatsType,
                                 UINT4 *pu4RetValFsMIStdIcmpMsgStatsInPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpMsgStatsInPkts (i4FsMIStdIcmpMsgStatsIPVersion,
                                  i4FsMIStdIcmpMsgStatsType,
                                  pu4RetValFsMIStdIcmpMsgStatsInPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdIcmpMsgStatsOutPkts
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdIcmpMsgStatsIPVersion
                FsMIStdIcmpMsgStatsType

                The Object 
                retValFsMIStdIcmpMsgStatsOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdIcmpMsgStatsOutPkts (INT4 i4FsMIStdIpContextId,
                                  INT4 i4FsMIStdIcmpMsgStatsIPVersion,
                                  INT4 i4FsMIStdIcmpMsgStatsType,
                                  UINT4 *pu4RetValFsMIStdIcmpMsgStatsOutPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetIcmpMsgStatsOutPkts (i4FsMIStdIcmpMsgStatsIPVersion,
                                   i4FsMIStdIcmpMsgStatsType,
                                   pu4RetValFsMIStdIcmpMsgStatsOutPkts);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdInetCidrRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdInetCidrRouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdInetCidrRouteTable (INT4 i4FsMIStdIpContextId,
                                                   INT4
                                                   i4FsMIStdInetCidrRouteDestType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMIStdInetCidrRouteDest,
                                                   UINT4
                                                   u4FsMIStdInetCidrRoutePfxLen,
                                                   tSNMP_OID_TYPE *
                                                   pFsMIStdInetCidrRoutePolicy,
                                                   INT4
                                                   i4FsMIStdInetCidrRouteNextHopType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMIStdInetCidrRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIStdIpContextId < IPVX_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceInetCidrRouteTable
        (i4FsMIStdInetCidrRouteDestType,
         pFsMIStdInetCidrRouteDest,
         u4FsMIStdInetCidrRoutePfxLen,
         pFsMIStdInetCidrRoutePolicy,
         i4FsMIStdInetCidrRouteNextHopType, pFsMIStdInetCidrRouteNextHop);
    UtilIpvxReleaseCxt ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdInetCidrRouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdInetCidrRouteTable (INT4 *pi4FsMIStdIpContextId,
                                           INT4
                                           *pi4FsMIStdInetCidrRouteDestType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdInetCidrRouteDest,
                                           UINT4 *pu4FsMIStdInetCidrRoutePfxLen,
                                           tSNMP_OID_TYPE *
                                           pFsMIStdInetCidrRoutePolicy,
                                           INT4
                                           *pi4FsMIStdInetCidrRouteNextHopType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdInetCidrRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4NextContextId;

    if (VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFirstIndexInetCidrRouteTable (pi4FsMIStdInetCidrRouteDestType,
                                            pFsMIStdInetCidrRouteDest,
                                            pu4FsMIStdInetCidrRoutePfxLen,
                                            pFsMIStdInetCidrRoutePolicy,
                                            pi4FsMIStdInetCidrRouteNextHopType,
                                            pFsMIStdInetCidrRouteNextHop);
    while ((i1RetVal == SNMP_FAILURE)
           &&
           (VcmGetNextActiveL3Context (*pi4FsMIStdIpContextId, &u4NextContextId)
            == VCM_SUCCESS))
    {
        *pi4FsMIStdIpContextId = u4NextContextId;
        if (UtilIpvxSetCxt (*pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        i1RetVal =
            nmhGetFirstIndexInetCidrRouteTable (pi4FsMIStdInetCidrRouteDestType,
                                                pFsMIStdInetCidrRouteDest,
                                                pu4FsMIStdInetCidrRoutePfxLen,
                                                pFsMIStdInetCidrRoutePolicy,
                                                pi4FsMIStdInetCidrRouteNextHopType,
                                                pFsMIStdInetCidrRouteNextHop);

    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdInetCidrRouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                nextFsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                nextFsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                nextFsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                nextFsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                nextFsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop
                nextFsMIStdInetCidrRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdInetCidrRouteTable (INT4 i4FsMIStdIpContextId,
                                          INT4 *pi4NextFsMIStdIpContextId,
                                          INT4 i4FsMIStdInetCidrRouteDestType,
                                          INT4
                                          *pi4NextFsMIStdInetCidrRouteDestType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdInetCidrRouteDest,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMIStdInetCidrRouteDest,
                                          UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                          UINT4
                                          *pu4NextFsMIStdInetCidrRoutePfxLen,
                                          tSNMP_OID_TYPE *
                                          pFsMIStdInetCidrRoutePolicy,
                                          tSNMP_OID_TYPE *
                                          pNextFsMIStdInetCidrRoutePolicy,
                                          INT4
                                          i4FsMIStdInetCidrRouteNextHopType,
                                          INT4
                                          *pi4NextFsMIStdInetCidrRouteNextHopType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdInetCidrRouteNextHop,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMIStdInetCidrRouteNextHop)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (VcmIsL3VcExist (i4FsMIStdIpContextId) == VCM_FALSE)
    {
        while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                          (UINT4 *) pi4NextFsMIStdIpContextId)
               == VCM_SUCCESS)
        {
            if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i4RetVal = nmhGetFirstIndexInetCidrRouteTable
                (pi4NextFsMIStdInetCidrRouteDestType,
                 pNextFsMIStdInetCidrRouteDest,
                 pu4NextFsMIStdInetCidrRoutePfxLen,
                 pNextFsMIStdInetCidrRoutePolicy,
                 pi4NextFsMIStdInetCidrRouteNextHopType,
                 pNextFsMIStdInetCidrRouteNextHop);

            if (i4RetVal == SNMP_SUCCESS)
            {
                UtilIpvxReleaseCxt ();
                return SNMP_SUCCESS;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
            UtilIpvxReleaseCxt ();
        }
        return SNMP_FAILURE;
    }

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexInetCidrRouteTable
        (i4FsMIStdInetCidrRouteDestType, pi4NextFsMIStdInetCidrRouteDestType,
         pFsMIStdInetCidrRouteDest, pNextFsMIStdInetCidrRouteDest,
         u4FsMIStdInetCidrRoutePfxLen, pu4NextFsMIStdInetCidrRoutePfxLen,
         pFsMIStdInetCidrRoutePolicy, pNextFsMIStdInetCidrRoutePolicy,
         i4FsMIStdInetCidrRouteNextHopType,
         pi4NextFsMIStdInetCidrRouteNextHopType, pFsMIStdInetCidrRouteNextHop,
         pNextFsMIStdInetCidrRouteNextHop) == SNMP_SUCCESS)
    {
        *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
        UtilIpvxReleaseCxt ();
        return SNMP_SUCCESS;
    }

    UtilIpvxReleaseCxt ();

    while (VcmGetNextActiveL3Context (i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
        if (UtilIpvxSetCxt (*pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetFirstIndexInetCidrRouteTable
            (pi4NextFsMIStdInetCidrRouteDestType,
             pNextFsMIStdInetCidrRouteDest, pu4NextFsMIStdInetCidrRoutePfxLen,
             pNextFsMIStdInetCidrRoutePolicy,
             pi4NextFsMIStdInetCidrRouteNextHopType,
             pNextFsMIStdInetCidrRouteNextHop) == SNMP_SUCCESS)
        {
            UtilIpvxReleaseCxt ();
            return SNMP_SUCCESS;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    UtilIpvxReleaseCxt ();
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteIfIndex (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteIfIndex (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteType (INT4 i4FsMIStdIpContextId,
                                INT4 i4FsMIStdInetCidrRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdInetCidrRouteDest,
                                UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                INT4 i4FsMIStdInetCidrRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdInetCidrRouteNextHop,
                                INT4 *pi4RetValFsMIStdInetCidrRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteType (i4FsMIStdInetCidrRouteDestType,
                                 pFsMIStdInetCidrRouteDest,
                                 u4FsMIStdInetCidrRoutePfxLen,
                                 pFsMIStdInetCidrRoutePolicy,
                                 i4FsMIStdInetCidrRouteNextHopType,
                                 pFsMIStdInetCidrRouteNextHop,
                                 pi4RetValFsMIStdInetCidrRouteType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteProto
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteProto (INT4 i4FsMIStdIpContextId,
                                 INT4 i4FsMIStdInetCidrRouteDestType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdInetCidrRouteDest,
                                 UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                 tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                 INT4 i4FsMIStdInetCidrRouteNextHopType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdInetCidrRouteNextHop,
                                 INT4 *pi4RetValFsMIStdInetCidrRouteProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteProto (i4FsMIStdInetCidrRouteDestType,
                                  pFsMIStdInetCidrRouteDest,
                                  u4FsMIStdInetCidrRoutePfxLen,
                                  pFsMIStdInetCidrRoutePolicy,
                                  i4FsMIStdInetCidrRouteNextHopType,
                                  pFsMIStdInetCidrRouteNextHop,
                                  pi4RetValFsMIStdInetCidrRouteProto);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteAge
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteAge (INT4 i4FsMIStdIpContextId,
                               INT4 i4FsMIStdInetCidrRouteDestType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdInetCidrRouteDest,
                               UINT4 u4FsMIStdInetCidrRoutePfxLen,
                               tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                               INT4 i4FsMIStdInetCidrRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdInetCidrRouteNextHop,
                               UINT4 *pu4RetValFsMIStdInetCidrRouteAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteAge (i4FsMIStdInetCidrRouteDestType,
                                pFsMIStdInetCidrRouteDest,
                                u4FsMIStdInetCidrRoutePfxLen,
                                pFsMIStdInetCidrRoutePolicy,
                                i4FsMIStdInetCidrRouteNextHopType,
                                pFsMIStdInetCidrRouteNextHop,
                                pu4RetValFsMIStdInetCidrRouteAge);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteNextHopAS (INT4 i4FsMIStdIpContextId,
                                     INT4 i4FsMIStdInetCidrRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteDest,
                                     UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                     tSNMP_OID_TYPE *
                                     pFsMIStdInetCidrRoutePolicy,
                                     INT4 i4FsMIStdInetCidrRouteNextHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteNextHop,
                                     UINT4
                                     *pu4RetValFsMIStdInetCidrRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteNextHopAS (i4FsMIStdInetCidrRouteDestType,
                                      pFsMIStdInetCidrRouteDest,
                                      u4FsMIStdInetCidrRoutePfxLen,
                                      pFsMIStdInetCidrRoutePolicy,
                                      i4FsMIStdInetCidrRouteNextHopType,
                                      pFsMIStdInetCidrRouteNextHop,
                                      pu4RetValFsMIStdInetCidrRouteNextHopAS);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteMetric1
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteMetric1 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteMetric1 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteMetric1);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteMetric2
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteMetric2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteMetric2 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteMetric2)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteMetric2 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteMetric2);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteMetric3
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteMetric3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteMetric3 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteMetric3)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteMetric3 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteMetric3);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteMetric4
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteMetric4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteMetric4 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteMetric4)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteMetric4 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteMetric4);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteMetric5
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteMetric5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteMetric5 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 *pi4RetValFsMIStdInetCidrRouteMetric5)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteMetric5 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    pi4RetValFsMIStdInetCidrRouteMetric5);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteStatus (INT4 i4FsMIStdIpContextId,
                                  INT4 i4FsMIStdInetCidrRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdInetCidrRouteDest,
                                  UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                  tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                  INT4 i4FsMIStdInetCidrRouteNextHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdInetCidrRouteNextHop,
                                  INT4 *pi4RetValFsMIStdInetCidrRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetInetCidrRouteStatus (i4FsMIStdInetCidrRouteDestType,
                                   pFsMIStdInetCidrRouteDest,
                                   u4FsMIStdInetCidrRoutePfxLen,
                                   pFsMIStdInetCidrRoutePolicy,
                                   i4FsMIStdInetCidrRouteNextHopType,
                                   pFsMIStdInetCidrRouteNextHop,
                                   pi4RetValFsMIStdInetCidrRouteStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteAddrType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                retValFsMIStdInetCidrRouteAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteAddrType (INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdInetCidrRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteDest,
                                    UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                    tSNMP_OID_TYPE *
                                    pFsMIStdInetCidrRoutePolicy,
                                    INT4 i4FsMIStdInetCidrRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteNextHop,
                                    INT4 *pi4RetValFsMIStdInetCidrRouteAddrType)
{

    UNUSED_PARAM (pFsMIStdInetCidrRoutePolicy);
    UNUSED_PARAM (pFsMIStdInetCidrRouteDest);
    UNUSED_PARAM (u4FsMIStdInetCidrRoutePfxLen);
    UNUSED_PARAM (pFsMIStdInetCidrRouteNextHop);

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        UtilIpvxReleaseCxt ();
        return (SNMP_FAILURE);
    }
    *pi4RetValFsMIStdInetCidrRouteAddrType = ADDR6_UNICAST;
    if ((i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
        (i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        if (Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pFsMIStdInetCidrRouteDest,
                                                   u4FsMIStdInetCidrRoutePfxLen,
                                                   pFsMIStdInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_ADDR_TYPE,
                                                   (UINT1 *)
                                                   pi4RetValFsMIStdInetCidrRouteAddrType)
            == RTM6_FAILURE)
        {
            RTM6_TASK_UNLOCK ();
            UtilIpvxReleaseCxt ();
            return SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#endif
    }
    UtilIpvxReleaseCxt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object
                retValFsMIStdInetCidrRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIStdInetCidrRoutePreference(INT4 i4FsMIStdIpContextId , 
                                          INT4 i4FsMIStdInetCidrRouteDestType , 
                                          tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteDest , 
                                          UINT4 u4FsMIStdInetCidrRoutePfxLen , 
                                          tSNMP_OID_TYPE *pFsMIStdInetCidrRoutePolicy , 
                                          INT4 i4FsMIStdInetCidrRouteNextHopType , 
                                          tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteNextHop , 
                                          INT4 *pi4RetValFsMIStdInetCidrRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt ((UINT4)i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    UNUSED_PARAM(pFsMIStdInetCidrRoutePolicy);
    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();

        i1Return =
            Rtm6ApiGetIpv6RouteTableEntryObjInCxt (gu4CurrContextId,
                                                   pFsMIStdInetCidrRouteDest,
                                                   u4FsMIStdInetCidrRoutePfxLen,
                                                   pFsMIStdInetCidrRouteNextHop,
                                                   IPVX_IPV6_RT_PREFERENCE,
                                                   (UINT1 *)
                                                   pi4RetValFsMIStdInetCidrRoutePreference);

        if (i1Return == RTM6_SUCCESS)
        {
            i1Return = SNMP_SUCCESS;
        }
        else
        {
            i1Return = SNMP_FAILURE;
        }

        RTM6_TASK_UNLOCK ();
#else
        UNUSED_PARAM(pFsMIStdInetCidrRouteDest);
        UNUSED_PARAM(u4FsMIStdInetCidrRoutePfxLen);
        UNUSED_PARAM(i4FsMIStdInetCidrRouteNextHopType);
        UNUSED_PARAM(pFsMIStdInetCidrRouteNextHop);
        UNUSED_PARAM(pi4RetValFsMIStdInetCidrRoutePreference);
#endif
    }
    else
    {
        i1Return = SNMP_SUCCESS;
    }
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteIfIndex (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteIfIndex (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteType (INT4 i4FsMIStdIpContextId,
                                INT4 i4FsMIStdInetCidrRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdInetCidrRouteDest,
                                UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                INT4 i4FsMIStdInetCidrRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdInetCidrRouteNextHop,
                                INT4 i4SetValFsMIStdInetCidrRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteType (i4FsMIStdInetCidrRouteDestType,
                                 pFsMIStdInetCidrRouteDest,
                                 u4FsMIStdInetCidrRoutePfxLen,
                                 pFsMIStdInetCidrRoutePolicy,
                                 i4FsMIStdInetCidrRouteNextHopType,
                                 pFsMIStdInetCidrRouteNextHop,
                                 i4SetValFsMIStdInetCidrRouteType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteNextHopAS (INT4 i4FsMIStdIpContextId,
                                     INT4 i4FsMIStdInetCidrRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteDest,
                                     UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                     tSNMP_OID_TYPE *
                                     pFsMIStdInetCidrRoutePolicy,
                                     INT4 i4FsMIStdInetCidrRouteNextHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteNextHop,
                                     UINT4
                                     u4SetValFsMIStdInetCidrRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteNextHopAS (i4FsMIStdInetCidrRouteDestType,
                                      pFsMIStdInetCidrRouteDest,
                                      u4FsMIStdInetCidrRoutePfxLen,
                                      pFsMIStdInetCidrRoutePolicy,
                                      i4FsMIStdInetCidrRouteNextHopType,
                                      pFsMIStdInetCidrRouteNextHop,
                                      u4SetValFsMIStdInetCidrRouteNextHopAS);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteMetric1
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteMetric1 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteMetric1 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteMetric1);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteMetric2
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteMetric2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteMetric2 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteMetric2)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteMetric2 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteMetric2);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteMetric3
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteMetric3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteMetric3 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteMetric3)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteMetric3 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteMetric3);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteMetric4
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteMetric4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteMetric4 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteMetric4)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteMetric4 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteMetric4);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteMetric5
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteMetric5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteMetric5 (INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4SetValFsMIStdInetCidrRouteMetric5)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteMetric5 (i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4SetValFsMIStdInetCidrRouteMetric5);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteStatus (INT4 i4FsMIStdIpContextId,
                                  INT4 i4FsMIStdInetCidrRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdInetCidrRouteDest,
                                  UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                  tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                  INT4 i4FsMIStdInetCidrRouteNextHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdInetCidrRouteNextHop,
                                  INT4 i4SetValFsMIStdInetCidrRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetInetCidrRouteStatus (i4FsMIStdInetCidrRouteDestType,
                                   pFsMIStdInetCidrRouteDest,
                                   u4FsMIStdInetCidrRoutePfxLen,
                                   pFsMIStdInetCidrRoutePolicy,
                                   i4FsMIStdInetCidrRouteNextHopType,
                                   pFsMIStdInetCidrRouteNextHop,
                                   i4SetValFsMIStdInetCidrRouteStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRouteAddrType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                setValFsMIStdInetCidrRouteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdInetCidrRouteAddrType (INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdInetCidrRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteDest,
                                    UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                    tSNMP_OID_TYPE *
                                    pFsMIStdInetCidrRoutePolicy,
                                    INT4 i4FsMIStdInetCidrRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteNextHop,
                                    INT4 i4SetValFsMIStdInetCidrRouteAddrType)
{
#ifdef IP6_WANTED
    tIp6Addr            ip6Addr;
    tIp6Addr            NextHop;
    INT4                i4RetVal = 0;
#else
    UNUSED_PARAM (pFsMIStdInetCidrRouteDest);
    UNUSED_PARAM (u4FsMIStdInetCidrRoutePfxLen);
    UNUSED_PARAM (pFsMIStdInetCidrRouteNextHop);
    UNUSED_PARAM (i4SetValFsMIStdInetCidrRouteAddrType);

#endif

    UNUSED_PARAM (pFsMIStdInetCidrRoutePolicy);
    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        UtilIpvxReleaseCxt ();
        return (SNMP_FAILURE);
    }

    if ((i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
        (i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i4RetVal = UtilRtm6SetContext (gu4CurrContextId);

        MEMSET (&ip6Addr, IP6_ZERO, sizeof (tIp6Addr));
        MEMSET (&NextHop, IP6_ZERO, sizeof (tIp6Addr));
        Ip6AddrCopy (&ip6Addr,
                     (tIp6Addr *) (VOID *) pFsMIStdInetCidrRouteDest->
                     pu1_OctetList);
        Ip6AddrCopy (&NextHop,
                     (tIp6Addr *) (VOID *) pFsMIStdInetCidrRouteNextHop->
                     pu1_OctetList);

        if (Rtm6ApiSetRtParamsInCxt (gu4CurrContextId,
                                     &ip6Addr,
                                     (UINT1) u4FsMIStdInetCidrRoutePfxLen,
                                     IP6_NETMGMT_PROTOID, &NextHop,
                                     RTM6_ROUTE_ADDR_TYPE,
                                     (UINT1)
                                     i4SetValFsMIStdInetCidrRouteAddrType) ==
            RTM6_FAILURE)
        {
            IncMsrForIpv6RouteTable (i4FsMIStdIpContextId,
                                     pFsMIStdInetCidrRouteDest,
                                     (INT4) u4FsMIStdInetCidrRoutePfxLen,
                                     pFsMIStdInetCidrRouteNextHop, 'i',
                                     &i4SetValFsMIStdInetCidrRouteAddrType,
                                     FsMIStdInetCidrRouteAddrType,
                                     sizeof (FsMIStdInetCidrRouteAddrType) /
                                     sizeof (UINT4), FALSE);
            UtilRtm6ResetContext ();
            RTM6_TASK_UNLOCK ();
            UtilIpvxReleaseCxt ();
            return SNMP_FAILURE;
        }
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
        UNUSED_PARAM (i4RetVal);
#endif
    }
    UtilIpvxReleaseCxt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdInetCidrRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object
                setValFsMIStdInetCidrRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIStdInetCidrRoutePreference(INT4 i4FsMIStdIpContextId , INT4 i4FsMIStdInetCidrRouteDestType , tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteDest , UINT4 u4FsMIStdInetCidrRoutePfxLen , tSNMP_OID_TYPE *pFsMIStdInetCidrRoutePolicy , INT4 i4FsMIStdInetCidrRouteNextHopType , tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteNextHop , INT4 i4SetValFsMIStdInetCidrRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt ((UINT4)i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    UNUSED_PARAM(pFsMIStdInetCidrRoutePolicy);
    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }
    if ((i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i1Return = (INT1)UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i1Return);
        i1Return = nmhSetFsipv6RoutePreference (pFsMIStdInetCidrRouteDest,
                                               (INT4)u4FsMIStdInetCidrRoutePfxLen,
                                                IP6_NETMGMT_PROTOID,
                                                pFsMIStdInetCidrRouteNextHop,
                                                i4SetValFsMIStdInetCidrRoutePreference);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#else
        UNUSED_PARAM(pFsMIStdInetCidrRouteDest);
        UNUSED_PARAM(u4FsMIStdInetCidrRoutePfxLen);
        UNUSED_PARAM(i4FsMIStdInetCidrRouteNextHopType);
        UNUSED_PARAM(pFsMIStdInetCidrRouteNextHop);
        UNUSED_PARAM(i4SetValFsMIStdInetCidrRoutePreference);
#endif
    }

    UtilIpvxReleaseCxt ();
    return i1Return;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteIfIndex (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteIfIndex (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteIfIndex);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteType (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpContextId,
                                   INT4 i4FsMIStdInetCidrRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteDest,
                                   UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                   tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
                                   INT4 i4FsMIStdInetCidrRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdInetCidrRouteNextHop,
                                   INT4 i4TestValFsMIStdInetCidrRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteType (pu4ErrorCode,
                                    i4FsMIStdInetCidrRouteDestType,
                                    pFsMIStdInetCidrRouteDest,
                                    u4FsMIStdInetCidrRoutePfxLen,
                                    pFsMIStdInetCidrRoutePolicy,
                                    i4FsMIStdInetCidrRouteNextHopType,
                                    pFsMIStdInetCidrRouteNextHop,
                                    i4TestValFsMIStdInetCidrRouteType);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteNextHopAS (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdIpContextId,
                                        INT4 i4FsMIStdInetCidrRouteDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdInetCidrRouteDest,
                                        UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                        tSNMP_OID_TYPE *
                                        pFsMIStdInetCidrRoutePolicy,
                                        INT4 i4FsMIStdInetCidrRouteNextHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdInetCidrRouteNextHop,
                                        UINT4
                                        u4TestValFsMIStdInetCidrRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteNextHopAS (pu4ErrorCode,
                                         i4FsMIStdInetCidrRouteDestType,
                                         pFsMIStdInetCidrRouteDest,
                                         u4FsMIStdInetCidrRoutePfxLen,
                                         pFsMIStdInetCidrRoutePolicy,
                                         i4FsMIStdInetCidrRouteNextHopType,
                                         pFsMIStdInetCidrRouteNextHop,
                                         u4TestValFsMIStdInetCidrRouteNextHopAS);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteMetric1
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteMetric1 (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteMetric1 (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteMetric1);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteMetric2
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteMetric2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteMetric2 (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteMetric2)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteMetric2 (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteMetric2);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteMetric3
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteMetric3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteMetric3 (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteMetric3)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteMetric3 (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteMetric3);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteMetric4
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteMetric4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteMetric4 (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteMetric4)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteMetric4 (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteMetric4);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteMetric5
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteMetric5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteMetric5 (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4FsMIStdInetCidrRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteDest,
                                      UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                      tSNMP_OID_TYPE *
                                      pFsMIStdInetCidrRoutePolicy,
                                      INT4 i4FsMIStdInetCidrRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdInetCidrRouteNextHop,
                                      INT4 i4TestValFsMIStdInetCidrRouteMetric5)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteMetric5 (pu4ErrorCode,
                                       i4FsMIStdInetCidrRouteDestType,
                                       pFsMIStdInetCidrRouteDest,
                                       u4FsMIStdInetCidrRoutePfxLen,
                                       pFsMIStdInetCidrRoutePolicy,
                                       i4FsMIStdInetCidrRouteNextHopType,
                                       pFsMIStdInetCidrRouteNextHop,
                                       i4TestValFsMIStdInetCidrRouteMetric5);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdIpContextId,
                                     INT4 i4FsMIStdInetCidrRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteDest,
                                     UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                     tSNMP_OID_TYPE *
                                     pFsMIStdInetCidrRoutePolicy,
                                     INT4 i4FsMIStdInetCidrRouteNextHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdInetCidrRouteNextHop,
                                     INT4 i4TestValFsMIStdInetCidrRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2InetCidrRouteStatus (pu4ErrorCode,
                                      i4FsMIStdInetCidrRouteDestType,
                                      pFsMIStdInetCidrRouteDest,
                                      u4FsMIStdInetCidrRoutePfxLen,
                                      pFsMIStdInetCidrRoutePolicy,
                                      i4FsMIStdInetCidrRouteNextHopType,
                                      pFsMIStdInetCidrRouteNextHop,
                                      i4TestValFsMIStdInetCidrRouteStatus);
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRouteAddrType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object 
                testValFsMIStdInetCidrRouteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdInetCidrRouteAddrType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4 i4FsMIStdInetCidrRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdInetCidrRouteDest,
                                       UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                       tSNMP_OID_TYPE *
                                       pFsMIStdInetCidrRoutePolicy,
                                       INT4 i4FsMIStdInetCidrRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdInetCidrRouteNextHop,
                                       INT4
                                       i4TestValFsMIStdInetCidrRouteAddrType)
{
    tIp6Addr            ip6addr;

    UNUSED_PARAM (pFsMIStdInetCidrRoutePolicy);

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        UtilIpvxReleaseCxt ();
        return (SNMP_FAILURE);
    }
    if (i4TestValFsMIStdInetCidrRouteAddrType != ADDR6_ANYCAST &&
        i4TestValFsMIStdInetCidrRouteAddrType != ADDR6_UNICAST)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        UtilIpvxReleaseCxt ();
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    if (pFsMIStdInetCidrRouteDest == NULL)
    {
        UtilIpvxReleaseCxt ();
        return SNMP_FAILURE;
    }

    if (u4FsMIStdInetCidrRoutePfxLen > IP6_ADDR_MAX_PREFIX)
    {
        UtilIpvxReleaseCxt ();
        return SNMP_FAILURE;
    }

    MEMSET (&ip6addr, IP6_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&ip6addr,
                 (tIp6Addr *) (VOID *) pFsMIStdInetCidrRouteNextHop->
                 pu1_OctetList);

    if (IS_ADDR_MULTI (ip6addr))
    {
        UtilIpvxReleaseCxt ();
        return SNMP_FAILURE;
    }

    UtilIpvxReleaseCxt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdInetCidrRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object
                testValFsMIStdInetCidrRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIStdInetCidrRoutePreference(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4FsMIStdInetCidrRouteDestType , tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteDest , UINT4 u4FsMIStdInetCidrRoutePfxLen , tSNMP_OID_TYPE *pFsMIStdInetCidrRoutePolicy , INT4 i4FsMIStdInetCidrRouteNextHopType , tSNMP_OCTET_STRING_TYPE *pFsMIStdInetCidrRouteNextHop , INT4 i4TestValFsMIStdInetCidrRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpvxSetCxt ((UINT4)i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    UNUSED_PARAM (pFsMIStdInetCidrRoutePolicy);

    if (i4FsMIStdInetCidrRouteDestType != i4FsMIStdInetCidrRouteNextHopType)
    {
        return (SNMP_FAILURE);
    }

    if ((i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6) ||
             (i4FsMIStdInetCidrRouteDestType == INET_ADDR_TYPE_IPV6Z))
    {
#ifdef IP6_WANTED
        RTM6_TASK_LOCK ();
        i1Return = (INT1)UtilRtm6SetContext (gu4CurrContextId);
        UNUSED_PARAM (i1Return);
        i1Return = nmhTestv2Fsipv6RoutePreference(pu4ErrorCode, pFsMIStdInetCidrRouteDest,
                                                  (INT4)u4FsMIStdInetCidrRoutePfxLen,
                                                  IP6_NETMGMT_PROTOID,
                                                  pFsMIStdInetCidrRouteNextHop,
                                                  i4TestValFsMIStdInetCidrRoutePreference);
        UtilRtm6ResetContext ();
        RTM6_TASK_UNLOCK ();
#else
        UNUSED_PARAM(pFsMIStdInetCidrRouteDest);
        UNUSED_PARAM(u4FsMIStdInetCidrRoutePfxLen);
        UNUSED_PARAM(i4FsMIStdInetCidrRouteNextHopType);
        UNUSED_PARAM(pFsMIStdInetCidrRouteNextHop);
        UNUSED_PARAM(i4TestValFsMIStdInetCidrRoutePreference);
#endif
    }

    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdInetCidrRouteTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIStdInetCidrRouteDestType
                FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdInetCidrRouteTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdInetCidrRouteHWStatus
 Input       :  The Indices                                                                                                                  FsMIStdIpContextId                                                                                                           FsMIStdInetCidrRouteDestType                                                                                                 FsMIStdInetCidrRouteDest
                FsMIStdInetCidrRoutePfxLen
                FsMIStdInetCidrRoutePolicy
                FsMIStdInetCidrRouteNextHopType
                FsMIStdInetCidrRouteNextHop

                The Object
                retValFsMIStdInetCidrRouteHWStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdInetCidrRouteHWStatus (INT4 i4FsMIStdIpContextId,
                                    INT4 i4FsMIStdInetCidrRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteDest,
                                    UINT4 u4FsMIStdInetCidrRoutePfxLen,
                                    tSNMP_OID_TYPE *
                                    pFsMIStdInetCidrRoutePolicy,
                                    INT4 i4FsMIStdInetCidrRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdInetCidrRouteNextHop,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMIStdInetCidrRouteHWStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4CidrRouteHWStatus = 0;
    INT4                i4LocalCidrRouteHWStatus = 0;

    if (UtilIpvxSetCxt (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    MEMSET (pRetValFsMIStdInetCidrRouteHWStatus->pu1_OctetList, IP_ZERO,
            sizeof (IP_FOUR));
    if (nmhGetInetCidrRouteHWStatus
        (i4FsMIStdInetCidrRouteDestType, pFsMIStdInetCidrRouteDest,
         u4FsMIStdInetCidrRoutePfxLen, pFsMIStdInetCidrRoutePolicy,
         i4FsMIStdInetCidrRouteNextHopType, pFsMIStdInetCidrRouteNextHop,
         &i4CidrRouteHWStatus) == SNMP_SUCCESS)
    {
        if ((i4CidrRouteHWStatus & RTM_RT_REACHABLE) == RTM_RT_REACHABLE)
        {
            i4LocalCidrRouteHWStatus |= RTM_REACHABLE;
        }
        if ((i4CidrRouteHWStatus & RTM_BEST_RT) == RTM_BEST_RT)
        {
            i4LocalCidrRouteHWStatus |= RTM_BEST_RT;
        }
        if ((i4CidrRouteHWStatus & RTM_RT_HWSTATUS) == RTM_RT_HWSTATUS)
        {
            i4LocalCidrRouteHWStatus |= RTM_RT_HWSTATUS;
        }
        MEMCPY (pRetValFsMIStdInetCidrRouteHWStatus->pu1_OctetList,
                &i4LocalCidrRouteHWStatus, IP_FOUR);
        pRetValFsMIStdInetCidrRouteHWStatus->i4_Length = IP_FOUR;
        UtilIpvxReleaseCxt ();
        return SNMP_SUCCESS;
    }

    UtilIpvxReleaseCxt ();
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : FsMIStdIpifTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsMIStdIpifTable
Input       :  The Indices
FsMIStdIpIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsMIStdIpifTable(INT4 i4FsMIStdIpIndex)
{
    UNUSED_PARAM (i4FsMIStdIpIndex);
    return SNMP_SUCCESS;

}
/****************************************************************************
Function    :  nmhGetFirstIndexFsMIStdIpifTable
Input       :  The Indices
FsMIStdIpIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsMIStdIpifTable(INT4 *pi4FsMIStdIpIndex)
{
    if (IpvxUtilGetFirstIndexFsMIStdIpifTable (pi4FsMIStdIpIndex) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetNextIndexFsMIStdIpifTable
Input       :  The Indices
FsMIStdIpIndex
nextFsMIStdIpIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsMIStdIpifTable(INT4 i4FsMIStdIpIndex 
                                     ,INT4 *pi4NextFsMIStdIpIndex )
{

    if (IpvxUtilGetNextIndexFsMIStdIpifTable (i4FsMIStdIpIndex , 
                                              pi4NextFsMIStdIpIndex) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsMIStdIpProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
retValFsMIStdIpProxyArpAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
    nmhGetFsMIStdIpProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 *pi4RetValFsMIStdIpProxyArpAdminStatus)
{
    if (IpvxUtilGetFsMIStdIpProxyArpAdminStatus
        (i4FsMIStdIpIndex, pi4RetValFsMIStdIpProxyArpAdminStatus)
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
Function    :  nmhGetFsMIStdIpLocalProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
retValFsMIStdIpLocalProxyArpAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
    INT1 nmhGetFsMIStdIpLocalProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 *pi4RetValFsMIStdIpLocalProxyArpAdminStatus)
{
    if (IpvxUtilGetFsMIStdIpLocalProxyArpAdminStatus 
        (i4FsMIStdIpIndex, 
         pi4RetValFsMIStdIpLocalProxyArpAdminStatus) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsMIStdIpProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
setValFsMIStdIpProxyArpAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
    INT1 nmhSetFsMIStdIpProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 i4SetValFsMIStdIpProxyArpAdminStatus)
{
    if (IpvxUtilSetFsMIStdIpProxyArpAdminStatus 
        (i4FsMIStdIpIndex , i4SetValFsMIStdIpProxyArpAdminStatus)
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMIStdIpLocalProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
setValFsMIStdIpLocalProxyArpAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
    nmhSetFsMIStdIpLocalProxyArpAdminStatus
(INT4 i4FsMIStdIpIndex , INT4 i4SetValFsMIStdIpLocalProxyArpAdminStatus)
{
    if (IpvxUtilSetFsMIStdIpLocalProxyArpAdminStatus
        (i4FsMIStdIpIndex, i4SetValFsMIStdIpLocalProxyArpAdminStatus) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsMIStdIpProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
testValFsMIStdIpProxyArpAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
    nmhTestv2FsMIStdIpProxyArpAdminStatus
(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpIndex ,
 INT4 i4TestValFsMIStdIpProxyArpAdminStatus)
{
    if (IpvxUtilTestv2FsMIStdIpProxyArpAdminStatus 
        (pu4ErrorCode, i4FsMIStdIpIndex , i4TestValFsMIStdIpProxyArpAdminStatus)
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMIStdIpLocalProxyArpAdminStatus
Input       :  The Indices
FsMIStdIpIndex

The Object
testValFsMIStdIpLocalProxyArpAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
    nmhTestv2FsMIStdIpLocalProxyArpAdminStatus
(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpIndex ,
 INT4 i4TestValFsMIStdIpLocalProxyArpAdminStatus)
{
    if (IpvxUtilTestv2FsMIStdIpLocalProxyArpAdminStatus
        (pu4ErrorCode, i4FsMIStdIpIndex, 
         i4TestValFsMIStdIpLocalProxyArpAdminStatus) 
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsMIStdIpifTable
Input       :  The Indices
FsMIStdIpIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhDepv2FsMIStdIpifTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList,
                              tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsMIStdIpProxyArpSubnetOption
Input       :  The Indices

The Object
retValFsMIStdIpProxyArpSubnetOption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMIStdIpProxyArpSubnetOption(INT4 *pi4RetValFsMIStdIpProxyArpSubnetOption)
{
    if (IpvxUtilGetFsMIStdIpProxyArpSubnetOption 
        (pi4RetValFsMIStdIpProxyArpSubnetOption) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsMIStdIpProxyArpSubnetOption
Input       :  The Indices

The Object
setValFsMIStdIpProxyArpSubnetOption
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
    INT1 
nmhSetFsMIStdIpProxyArpSubnetOption(INT4 i4SetValFsMIStdIpProxyArpSubnetOption)
{
    if (IpvxUtilSetFsMIStdIpProxyArpSubnetOption
        (i4SetValFsMIStdIpProxyArpSubnetOption) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsMIStdIpProxyArpSubnetOption
Input       :  The Indices

The Object
testValFsMIStdIpProxyArpSubnetOption
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
    nmhTestv2FsMIStdIpProxyArpSubnetOption
(UINT4 *pu4ErrorCode ,INT4 i4TestValFsMIStdIpProxyArpSubnetOption)
{
    if (IpvxUtilTestv2FsMIStdIpProxyArpSubnetOption
        (pu4ErrorCode, i4TestValFsMIStdIpProxyArpSubnetOption) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsMIStdIpProxyArpSubnetOption
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
    INT1 nmhDepv2FsMIStdIpProxyArpSubnetOption
(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

