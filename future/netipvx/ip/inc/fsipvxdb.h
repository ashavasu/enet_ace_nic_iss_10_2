/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipvxdb.h,v 1.2 2009/10/05 11:54:45 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIPVXDB_H
#define _FSIPVXDB_H

UINT1 FsIpvxAddrPrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsIpv6IfIcmpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsipvx [] ={1,3,6,1,4,1,29601,2,16};
tSNMP_OID_TYPE fsipvxOID = {9, fsipvx};


UINT4 FsIpvxAddrPrefixIfIndex [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,1};
UINT4 FsIpvxAddrPrefixAddrType [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,2};
UINT4 FsIpvxAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,3};
UINT4 FsIpvxAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,4};
UINT4 FsIpvxAddrPrefixProfileIndex [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,5};
UINT4 FsIpvxAddrPrefixSecAddrFlag [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,6};
UINT4 FsIpvxAddrPrefixRowStatus [ ] ={1,3,6,1,4,1,29601,2,16,1,1,1,7};
UINT4 FsIpv6IfIcmpInMsgs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,1};
UINT4 FsIpv6IfIcmpInErrors [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,2};
UINT4 FsIpv6IfIcmpInDestUnreachs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,3};
UINT4 FsIpv6IfIcmpInAdminProhibs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,4};
UINT4 FsIpv6IfIcmpInTimeExcds [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,5};
UINT4 FsIpv6IfIcmpInParmProblems [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,6};
UINT4 FsIpv6IfIcmpInPktTooBigs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,7};
UINT4 FsIpv6IfIcmpInEchos [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,8};
UINT4 FsIpv6IfIcmpInEchoReplies [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,9};
UINT4 FsIpv6IfIcmpInRouterSolicits [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,10};
UINT4 FsIpv6IfIcmpInRouterAdvertisements [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,11};
UINT4 FsIpv6IfIcmpInNeighborSolicits [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,12};
UINT4 FsIpv6IfIcmpInNeighborAdvertisements [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,13};
UINT4 FsIpv6IfIcmpInRedirects [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,14};
UINT4 FsIpv6IfIcmpInGroupMembQueries [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,15};
UINT4 FsIpv6IfIcmpInGroupMembResponses [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,16};
UINT4 FsIpv6IfIcmpInGroupMembReductions [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,17};
UINT4 FsIpv6IfIcmpOutMsgs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,18};
UINT4 FsIpv6IfIcmpOutErrors [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,19};
UINT4 FsIpv6IfIcmpOutDestUnreachs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,20};
UINT4 FsIpv6IfIcmpOutAdminProhibs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,21};
UINT4 FsIpv6IfIcmpOutTimeExcds [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,22};
UINT4 FsIpv6IfIcmpOutParmProblems [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,23};
UINT4 FsIpv6IfIcmpOutPktTooBigs [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,24};
UINT4 FsIpv6IfIcmpOutEchos [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,25};
UINT4 FsIpv6IfIcmpOutEchoReplies [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,26};
UINT4 FsIpv6IfIcmpOutRouterSolicits [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,27};
UINT4 FsIpv6IfIcmpOutRouterAdvertisements [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,28};
UINT4 FsIpv6IfIcmpOutNeighborSolicits [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,29};
UINT4 FsIpv6IfIcmpOutNeighborAdvertisements [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,30};
UINT4 FsIpv6IfIcmpOutRedirects [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,31};
UINT4 FsIpv6IfIcmpOutGroupMembQueries [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,32};
UINT4 FsIpv6IfIcmpOutGroupMembResponses [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,33};
UINT4 FsIpv6IfIcmpOutGroupMembReductions [ ] ={1,3,6,1,4,1,29601,2,16,2,1,1,34};


tMbDbEntry fsipvxMibEntry[]= {

{{13,FsIpvxAddrPrefixIfIndex}, GetNextIndexFsIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{13,FsIpvxAddrPrefixAddrType}, GetNextIndexFsIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{13,FsIpvxAddrPrefix}, GetNextIndexFsIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{13,FsIpvxAddrPrefixLen}, GetNextIndexFsIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{13,FsIpvxAddrPrefixProfileIndex}, GetNextIndexFsIpvxAddrPrefixTable, FsIpvxAddrPrefixProfileIndexGet, FsIpvxAddrPrefixProfileIndexSet, FsIpvxAddrPrefixProfileIndexTest, FsIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{13,FsIpvxAddrPrefixSecAddrFlag}, GetNextIndexFsIpvxAddrPrefixTable, FsIpvxAddrPrefixSecAddrFlagGet, FsIpvxAddrPrefixSecAddrFlagSet, FsIpvxAddrPrefixSecAddrFlagTest, FsIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpvxAddrPrefixTableINDEX, 4, 0, 0, "1"},

{{13,FsIpvxAddrPrefixRowStatus}, GetNextIndexFsIpvxAddrPrefixTable, FsIpvxAddrPrefixRowStatusGet, FsIpvxAddrPrefixRowStatusSet, FsIpvxAddrPrefixRowStatusTest, FsIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpvxAddrPrefixTableINDEX, 4, 0, 1, NULL},

{{13,FsIpv6IfIcmpInMsgs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInErrors}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInDestUnreachs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInAdminProhibs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInAdminProhibsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInTimeExcds}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInParmProblems}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInParmProblemsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInPktTooBigs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInEchos}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInEchoReplies}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInEchoRepliesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInRouterSolicits}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInRouterAdvertisements}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInNeighborSolicits}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInNeighborAdvertisements}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInRedirects}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInGroupMembQueries}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInGroupMembQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInGroupMembResponses}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInGroupMembResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpInGroupMembReductions}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpInGroupMembReductionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutMsgs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutErrors}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutDestUnreachs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutAdminProhibs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutAdminProhibsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutTimeExcds}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutParmProblems}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutParmProblemsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutPktTooBigs}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutPktTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutEchos}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutEchoReplies}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutEchoRepliesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutRouterSolicits}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutRouterSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutRouterAdvertisements}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutRouterAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutNeighborSolicits}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutNeighborSolicitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutNeighborAdvertisements}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutNeighborAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutRedirects}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutGroupMembQueries}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutGroupMembQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutGroupMembResponses}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutGroupMembResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},

{{13,FsIpv6IfIcmpOutGroupMembReductions}, GetNextIndexFsIpv6IfIcmpTable, FsIpv6IfIcmpOutGroupMembReductionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpv6IfIcmpTableINDEX, 1, 0, 0, NULL},
};
tMibData fsipvxEntry = { 41, fsipvxMibEntry };
#endif /* _FSIPVXDB_H */

