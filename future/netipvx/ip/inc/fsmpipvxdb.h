/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipvxdb.h,v 1.4 2010/10/29 13:30:26 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPIPVXDB_H
#define _FSMPIPVXDB_H

UINT1 FsMIIpvxAddrPrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIIpvxTraceConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIIpvxTraceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmpipvx [] ={1,3,6,1,4,1,29601,2,34};
tSNMP_OID_TYPE fsmpipvxOID = {9, fsmpipvx};


UINT4 FsMIIpvxAddrPrefixIfIndex [ ] ={1,3,6,1,4,1,29601,2,34,1,1,1};
UINT4 FsMIIpvxAddrPrefixAddrType [ ] ={1,3,6,1,4,1,29601,2,34,1,1,2};
UINT4 FsMIIpvxAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,34,1,1,3};
UINT4 FsMIIpvxAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,34,1,1,4};
UINT4 FsMIIpvxAddrPrefixContextId [ ] ={1,3,6,1,4,1,29601,2,34,1,1,5};
UINT4 FsMIIpvxAddrPrefixProfileIndex [ ] ={1,3,6,1,4,1,29601,2,34,1,1,6};
UINT4 FsMIIpvxAddrPrefixSecAddrFlag [ ] ={1,3,6,1,4,1,29601,2,34,1,1,7};
UINT4 FsMIIpvxAddrPrefixRowStatus [ ] ={1,3,6,1,4,1,29601,2,34,1,1,8};
UINT4 FsMIIpvxTraceConfigIndex [ ] ={1,3,6,1,4,1,29601,2,34,2,1,1};
UINT4 FsMIIpvxTraceConfigAddrType [ ] ={1,3,6,1,4,1,29601,2,34,2,1,2};
UINT4 FsMIIpvxTraceConfigDest [ ] ={1,3,6,1,4,1,29601,2,34,2,1,3};
UINT4 FsMIIpvxTraceConfigAdminStatus [ ] ={1,3,6,1,4,1,29601,2,34,2,1,4};
UINT4 FsMIIpvxTraceConfigMaxTTL [ ] ={1,3,6,1,4,1,29601,2,34,2,1,5};
UINT4 FsMIIpvxTraceConfigMinTTL [ ] ={1,3,6,1,4,1,29601,2,34,2,1,6};
UINT4 FsMIIpvxTraceConfigOperStatus [ ] ={1,3,6,1,4,1,29601,2,34,2,1,7};
UINT4 FsMIIpvxTraceConfigTimeout [ ] ={1,3,6,1,4,1,29601,2,34,2,1,8};
UINT4 FsMIIpvxTraceConfigMtu [ ] ={1,3,6,1,4,1,29601,2,34,2,1,9};
UINT4 FsMIIpvxTraceConfigCxtId [ ] ={1,3,6,1,4,1,29601,2,34,2,1,10};
UINT4 FsMIIpvxTraceIndex [ ] ={1,3,6,1,4,1,29601,2,34,3,1,1};
UINT4 FsMIIpvxTraceAddrType [ ] ={1,3,6,1,4,1,29601,2,34,3,1,2};
UINT4 FsMIIpvxTraceAddr [ ] ={1,3,6,1,4,1,29601,2,34,3,1,3};
UINT4 FsMIIpvxTraceHopCount [ ] ={1,3,6,1,4,1,29601,2,34,3,1,4};
UINT4 FsMIIpvxTraceIntermHop [ ] ={1,3,6,1,4,1,29601,2,34,3,1,5};
UINT4 FsMIIpvxTraceReachTime1 [ ] ={1,3,6,1,4,1,29601,2,34,3,1,6};
UINT4 FsMIIpvxTraceReachTime2 [ ] ={1,3,6,1,4,1,29601,2,34,3,1,7};
UINT4 FsMIIpvxTraceReachTime3 [ ] ={1,3,6,1,4,1,29601,2,34,3,1,8};
UINT4 FsMIIpvxTraceCxtId [ ] ={1,3,6,1,4,1,29601,2,34,3,1,9};


tMbDbEntry fsmpipvxMibEntry[]= {

{{12,FsMIIpvxAddrPrefixIfIndex}, GetNextIndexFsMIIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefixAddrType}, GetNextIndexFsMIIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefix}, GetNextIndexFsMIIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefixLen}, GetNextIndexFsMIIpvxAddrPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefixContextId}, GetNextIndexFsMIIpvxAddrPrefixTable, FsMIIpvxAddrPrefixContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefixProfileIndex}, GetNextIndexFsMIIpvxAddrPrefixTable, FsMIIpvxAddrPrefixProfileIndexGet, FsMIIpvxAddrPrefixProfileIndexSet, FsMIIpvxAddrPrefixProfileIndexTest, FsMIIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIIpvxAddrPrefixSecAddrFlag}, GetNextIndexFsMIIpvxAddrPrefixTable, FsMIIpvxAddrPrefixSecAddrFlagGet, FsMIIpvxAddrPrefixSecAddrFlagSet, FsMIIpvxAddrPrefixSecAddrFlagTest, FsMIIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 0, "1"},

{{12,FsMIIpvxAddrPrefixRowStatus}, GetNextIndexFsMIIpvxAddrPrefixTable, FsMIIpvxAddrPrefixRowStatusGet, FsMIIpvxAddrPrefixRowStatusSet, FsMIIpvxAddrPrefixRowStatusTest, FsMIIpvxAddrPrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpvxAddrPrefixTableINDEX, 4, 0, 1, NULL},

{{12,FsMIIpvxTraceConfigIndex}, GetNextIndexFsMIIpvxTraceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigAddrType}, GetNextIndexFsMIIpvxTraceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigDest}, GetNextIndexFsMIIpvxTraceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigAdminStatus}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigAdminStatusGet, FsMIIpvxTraceConfigAdminStatusSet, FsMIIpvxTraceConfigAdminStatusTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, "1"},

{{12,FsMIIpvxTraceConfigMaxTTL}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigMaxTTLGet, FsMIIpvxTraceConfigMaxTTLSet, FsMIIpvxTraceConfigMaxTTLTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, "15"},

{{12,FsMIIpvxTraceConfigMinTTL}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigMinTTLGet, FsMIIpvxTraceConfigMinTTLSet, FsMIIpvxTraceConfigMinTTLTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, "1"},

{{12,FsMIIpvxTraceConfigOperStatus}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigTimeout}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigTimeoutGet, FsMIIpvxTraceConfigTimeoutSet, FsMIIpvxTraceConfigTimeoutTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigMtu}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigMtuGet, FsMIIpvxTraceConfigMtuSet, FsMIIpvxTraceConfigMtuTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceConfigCxtId}, GetNextIndexFsMIIpvxTraceConfigTable, FsMIIpvxTraceConfigCxtIdGet, FsMIIpvxTraceConfigCxtIdSet, FsMIIpvxTraceConfigCxtIdTest, FsMIIpvxTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpvxTraceConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceIndex}, GetNextIndexFsMIIpvxTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceAddrType}, GetNextIndexFsMIIpvxTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceAddr}, GetNextIndexFsMIIpvxTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceHopCount}, GetNextIndexFsMIIpvxTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceIntermHop}, GetNextIndexFsMIIpvxTraceTable, FsMIIpvxTraceIntermHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceReachTime1}, GetNextIndexFsMIIpvxTraceTable, FsMIIpvxTraceReachTime1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceReachTime2}, GetNextIndexFsMIIpvxTraceTable, FsMIIpvxTraceReachTime2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceReachTime3}, GetNextIndexFsMIIpvxTraceTable, FsMIIpvxTraceReachTime3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIIpvxTraceCxtId}, GetNextIndexFsMIIpvxTraceTable, FsMIIpvxTraceCxtIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpvxTraceTableINDEX, 3, 0, 0, NULL},
};
tMibData fsmpipvxEntry = { 27, fsmpipvxMibEntry };
#endif /* _FSMPIPVXDB_H */

