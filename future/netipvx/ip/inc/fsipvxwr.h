#ifndef _FSIPVXWR_H
#define _FSIPVXWR_H
INT4 GetNextIndexFsIpvxAddrPrefixTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSIPVX(VOID);

VOID UnRegisterFSIPVX(VOID);
INT4 FsIpvxAddrPrefixProfileIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixSecAddrFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixProfileIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixSecAddrFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixProfileIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixSecAddrFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpvxAddrPrefixTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsIpv6IfIcmpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpv6IfIcmpInMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInDestUnreachsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInAdminProhibsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInTimeExcdsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInParmProblemsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInPktTooBigsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInEchosGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInEchoRepliesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInRouterSolicitsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInRouterAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInNeighborSolicitsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInNeighborAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInRedirectsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInGroupMembQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInGroupMembResponsesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpInGroupMembReductionsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutDestUnreachsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutAdminProhibsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutTimeExcdsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutParmProblemsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutPktTooBigsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutEchosGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutEchoRepliesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutRouterSolicitsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutRouterAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutNeighborSolicitsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutNeighborAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutRedirectsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutGroupMembQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutGroupMembResponsesGet(tSnmpIndex *, tRetVal *);
INT4 FsIpv6IfIcmpOutGroupMembReductionsGet(tSnmpIndex *, tRetVal *);
#endif /* _FSIPVXWR_H */
