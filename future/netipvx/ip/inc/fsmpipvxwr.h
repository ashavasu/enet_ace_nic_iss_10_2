/*$Id: fsmpipvxwr.h,v 1.4 2010/10/29 13:48:32 prabuc Exp $ */
#ifndef _FSMPIPWR_H
#define _FSMPIPWR_H
INT4 GetNextIndexFsMIIpvxAddrPrefixTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPIPVX (VOID);

VOID UnRegisterFSMPIPVX (VOID);
INT4 FsMIIpvxAddrPrefixContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixProfileIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixSecAddrFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixProfileIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixSecAddrFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixProfileIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixSecAddrFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxAddrPrefixTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIIpvxTraceConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpvxTraceConfigAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMaxTTLGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMinTTLGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMtuGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigCxtIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMaxTTLSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMinTTLSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMtuSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigCxtIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMaxTTLTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMinTTLTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigMtuTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigCxtIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIIpvxTraceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpvxTraceIntermHopGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceReachTime1Get(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceReachTime2Get(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceReachTime3Get(tSnmpIndex *, tRetVal *);
INT4 FsMIIpvxTraceCxtIdGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMPIPWR_H */
