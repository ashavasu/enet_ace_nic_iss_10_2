/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsipdb.h,v 1.8 2016/02/27 10:14:52 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSIPDB_H
#define _FSMSIPDB_H

UINT1 FsMIStdIpGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdIpv4InterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpv6InterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpSystemStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpIfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpAddressPrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdIpAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIStdIpNetToPhysicalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIStdIpv6ScopeZoneIndexTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpDefaultRouterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIpv6RouterAdvertTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIcmpStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdIcmpMsgStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdInetCidrRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OBJECT_ID ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIStdIpifTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmsip [] ={1,3,6,1,4,1,29601,2,37};
tSNMP_OID_TYPE fsmsipOID = {9, fsmsip};


UINT4 FsMIStdIpv4InterfaceTableLastChange [ ] ={1,3,6,1,4,1,29601,2,37,1};
UINT4 FsMIStdIpv6InterfaceTableLastChange [ ] ={1,3,6,1,4,1,29601,2,37,2};
UINT4 FsMIStdIpIfStatsTableLastChange [ ] ={1,3,6,1,4,1,29601,2,37,3};
UINT4 FsMIStdIpContextId [ ] ={1,3,6,1,4,1,29601,2,37,4,1,1};
UINT4 FsMIStdIpForwarding [ ] ={1,3,6,1,4,1,29601,2,37,4,1,2};
UINT4 FsMIStdIpDefaultTTL [ ] ={1,3,6,1,4,1,29601,2,37,4,1,3};
UINT4 FsMIStdIpReasmTimeout [ ] ={1,3,6,1,4,1,29601,2,37,4,1,4};
UINT4 FsMIStdIpv6IpForwarding [ ] ={1,3,6,1,4,1,29601,2,37,4,1,5};
UINT4 FsMIStdIpv6IpDefaultHopLimit [ ] ={1,3,6,1,4,1,29601,2,37,4,1,6};
UINT4 FsMIStdInetCidrRouteNumber [ ] ={1,3,6,1,4,1,29601,2,37,4,1,7};
UINT4 FsMIStdInetCidrRouteDiscards [ ] ={1,3,6,1,4,1,29601,2,37,4,1,8};
UINT4 FsMIStdIpv4InterfaceIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,5,1,1};
UINT4 FsMIStdIpv4InterfaceReasmMaxSize [ ] ={1,3,6,1,4,1,29601,2,37,5,1,2};
UINT4 FsMIStdIpv4InterfaceEnableStatus [ ] ={1,3,6,1,4,1,29601,2,37,5,1,3};
UINT4 FsMIStdIpv4InterfaceRetransmitTime [ ] ={1,3,6,1,4,1,29601,2,37,5,1,4};
UINT4 FsMIStdIpv4IfContextId [ ] ={1,3,6,1,4,1,29601,2,37,5,1,5};
UINT4 FsMIStdIpv6InterfaceIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,6,1,1};
UINT4 FsMIStdIpv6InterfaceReasmMaxSize [ ] ={1,3,6,1,4,1,29601,2,37,6,1,2};
UINT4 FsMIStdIpv6InterfaceIdentifier [ ] ={1,3,6,1,4,1,29601,2,37,6,1,3};
UINT4 FsMIStdIpv6InterfaceEnableStatus [ ] ={1,3,6,1,4,1,29601,2,37,6,1,4};
UINT4 FsMIStdIpv6InterfaceReachableTime [ ] ={1,3,6,1,4,1,29601,2,37,6,1,5};
UINT4 FsMIStdIpv6InterfaceRetransmitTime [ ] ={1,3,6,1,4,1,29601,2,37,6,1,6};
UINT4 FsMIStdIpv6InterfaceForwarding [ ] ={1,3,6,1,4,1,29601,2,37,6,1,7};
UINT4 FsMIStdIpv6IfContextId [ ] ={1,3,6,1,4,1,29601,2,37,6,1,8};
UINT4 FsMIStdIpSystemStatsIPVersion [ ] ={1,3,6,1,4,1,29601,2,37,7,1,1};
UINT4 FsMIStdIpSystemStatsInReceives [ ] ={1,3,6,1,4,1,29601,2,37,7,1,2};
UINT4 FsMIStdIpSystemStatsHCInReceives [ ] ={1,3,6,1,4,1,29601,2,37,7,1,3};
UINT4 FsMIStdIpSystemStatsInOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,4};
UINT4 FsMIStdIpSystemStatsHCInOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,5};
UINT4 FsMIStdIpSystemStatsInHdrErrors [ ] ={1,3,6,1,4,1,29601,2,37,7,1,6};
UINT4 FsMIStdIpSystemStatsInNoRoutes [ ] ={1,3,6,1,4,1,29601,2,37,7,1,7};
UINT4 FsMIStdIpSystemStatsInAddrErrors [ ] ={1,3,6,1,4,1,29601,2,37,7,1,8};
UINT4 FsMIStdIpSystemStatsInUnknownProtos [ ] ={1,3,6,1,4,1,29601,2,37,7,1,9};
UINT4 FsMIStdIpSystemStatsInTruncatedPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,10};
UINT4 FsMIStdIpSystemStatsInForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,7,1,11};
UINT4 FsMIStdIpSystemStatsHCInForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,7,1,12};
UINT4 FsMIStdIpSystemStatsReasmReqds [ ] ={1,3,6,1,4,1,29601,2,37,7,1,13};
UINT4 FsMIStdIpSystemStatsReasmOKs [ ] ={1,3,6,1,4,1,29601,2,37,7,1,14};
UINT4 FsMIStdIpSystemStatsReasmFails [ ] ={1,3,6,1,4,1,29601,2,37,7,1,15};
UINT4 FsMIStdIpSystemStatsInDiscards [ ] ={1,3,6,1,4,1,29601,2,37,7,1,16};
UINT4 FsMIStdIpSystemStatsInDelivers [ ] ={1,3,6,1,4,1,29601,2,37,7,1,17};
UINT4 FsMIStdIpSystemStatsHCInDelivers [ ] ={1,3,6,1,4,1,29601,2,37,7,1,18};
UINT4 FsMIStdIpSystemStatsOutRequests [ ] ={1,3,6,1,4,1,29601,2,37,7,1,19};
UINT4 FsMIStdIpSystemStatsHCOutRequests [ ] ={1,3,6,1,4,1,29601,2,37,7,1,20};
UINT4 FsMIStdIpSystemStatsOutNoRoutes [ ] ={1,3,6,1,4,1,29601,2,37,7,1,21};
UINT4 FsMIStdIpSystemStatsOutForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,7,1,22};
UINT4 FsMIStdIpSystemStatsHCOutForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,7,1,23};
UINT4 FsMIStdIpSystemStatsOutDiscards [ ] ={1,3,6,1,4,1,29601,2,37,7,1,24};
UINT4 FsMIStdIpSystemStatsOutFragReqds [ ] ={1,3,6,1,4,1,29601,2,37,7,1,25};
UINT4 FsMIStdIpSystemStatsOutFragOKs [ ] ={1,3,6,1,4,1,29601,2,37,7,1,26};
UINT4 FsMIStdIpSystemStatsOutFragFails [ ] ={1,3,6,1,4,1,29601,2,37,7,1,27};
UINT4 FsMIStdIpSystemStatsOutFragCreates [ ] ={1,3,6,1,4,1,29601,2,37,7,1,28};
UINT4 FsMIStdIpSystemStatsOutTransmits [ ] ={1,3,6,1,4,1,29601,2,37,7,1,29};
UINT4 FsMIStdIpSystemStatsHCOutTransmits [ ] ={1,3,6,1,4,1,29601,2,37,7,1,30};
UINT4 FsMIStdIpSystemStatsOutOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,31};
UINT4 FsMIStdIpSystemStatsHCOutOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,32};
UINT4 FsMIStdIpSystemStatsInMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,33};
UINT4 FsMIStdIpSystemStatsHCInMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,34};
UINT4 FsMIStdIpSystemStatsInMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,35};
UINT4 FsMIStdIpSystemStatsHCInMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,36};
UINT4 FsMIStdIpSystemStatsOutMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,37};
UINT4 FsMIStdIpSystemStatsHCOutMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,38};
UINT4 FsMIStdIpSystemStatsOutMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,39};
UINT4 FsMIStdIpSystemStatsHCOutMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,7,1,40};
UINT4 FsMIStdIpSystemStatsInBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,41};
UINT4 FsMIStdIpSystemStatsHCInBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,42};
UINT4 FsMIStdIpSystemStatsOutBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,43};
UINT4 FsMIStdIpSystemStatsHCOutBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,7,1,44};
UINT4 FsMIStdIpSystemStatsDiscontinuityTime [ ] ={1,3,6,1,4,1,29601,2,37,7,1,45};
UINT4 FsMIStdIpSystemStatsRefreshRate [ ] ={1,3,6,1,4,1,29601,2,37,7,1,46};
UINT4 FsMIStdIpIfStatsIPVersion [ ] ={1,3,6,1,4,1,29601,2,37,8,1,1};
UINT4 FsMIStdIpIfStatsIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,8,1,2};
UINT4 FsMIStdIpIfStatsInReceives [ ] ={1,3,6,1,4,1,29601,2,37,8,1,3};
UINT4 FsMIStdIpIfStatsHCInReceives [ ] ={1,3,6,1,4,1,29601,2,37,8,1,4};
UINT4 FsMIStdIpIfStatsInOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,5};
UINT4 FsMIStdIpIfStatsHCInOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,6};
UINT4 FsMIStdIpIfStatsInHdrErrors [ ] ={1,3,6,1,4,1,29601,2,37,8,1,7};
UINT4 FsMIStdIpIfStatsInNoRoutes [ ] ={1,3,6,1,4,1,29601,2,37,8,1,8};
UINT4 FsMIStdIpIfStatsInAddrErrors [ ] ={1,3,6,1,4,1,29601,2,37,8,1,9};
UINT4 FsMIStdIpIfStatsInUnknownProtos [ ] ={1,3,6,1,4,1,29601,2,37,8,1,10};
UINT4 FsMIStdIpIfStatsInTruncatedPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,11};
UINT4 FsMIStdIpIfStatsInForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,8,1,12};
UINT4 FsMIStdIpIfStatsHCInForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,8,1,13};
UINT4 FsMIStdIpIfStatsReasmReqds [ ] ={1,3,6,1,4,1,29601,2,37,8,1,14};
UINT4 FsMIStdIpIfStatsReasmOKs [ ] ={1,3,6,1,4,1,29601,2,37,8,1,15};
UINT4 FsMIStdIpIfStatsReasmFails [ ] ={1,3,6,1,4,1,29601,2,37,8,1,16};
UINT4 FsMIStdIpIfStatsInDiscards [ ] ={1,3,6,1,4,1,29601,2,37,8,1,17};
UINT4 FsMIStdIpIfStatsInDelivers [ ] ={1,3,6,1,4,1,29601,2,37,8,1,18};
UINT4 FsMIStdIpIfStatsHCInDelivers [ ] ={1,3,6,1,4,1,29601,2,37,8,1,19};
UINT4 FsMIStdIpIfStatsOutRequests [ ] ={1,3,6,1,4,1,29601,2,37,8,1,20};
UINT4 FsMIStdIpIfStatsHCOutRequests [ ] ={1,3,6,1,4,1,29601,2,37,8,1,21};
UINT4 FsMIStdIpIfStatsOutForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,8,1,22};
UINT4 FsMIStdIpIfStatsHCOutForwDatagrams [ ] ={1,3,6,1,4,1,29601,2,37,8,1,23};
UINT4 FsMIStdIpIfStatsOutDiscards [ ] ={1,3,6,1,4,1,29601,2,37,8,1,24};
UINT4 FsMIStdIpIfStatsOutFragReqds [ ] ={1,3,6,1,4,1,29601,2,37,8,1,25};
UINT4 FsMIStdIpIfStatsOutFragOKs [ ] ={1,3,6,1,4,1,29601,2,37,8,1,26};
UINT4 FsMIStdIpIfStatsOutFragFails [ ] ={1,3,6,1,4,1,29601,2,37,8,1,27};
UINT4 FsMIStdIpIfStatsOutFragCreates [ ] ={1,3,6,1,4,1,29601,2,37,8,1,28};
UINT4 FsMIStdIpIfStatsOutTransmits [ ] ={1,3,6,1,4,1,29601,2,37,8,1,29};
UINT4 FsMIStdIpIfStatsHCOutTransmits [ ] ={1,3,6,1,4,1,29601,2,37,8,1,30};
UINT4 FsMIStdIpIfStatsOutOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,31};
UINT4 FsMIStdIpIfStatsHCOutOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,32};
UINT4 FsMIStdIpIfStatsInMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,33};
UINT4 FsMIStdIpIfStatsHCInMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,34};
UINT4 FsMIStdIpIfStatsInMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,35};
UINT4 FsMIStdIpIfStatsHCInMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,36};
UINT4 FsMIStdIpIfStatsOutMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,37};
UINT4 FsMIStdIpIfStatsHCOutMcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,38};
UINT4 FsMIStdIpIfStatsOutMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,39};
UINT4 FsMIStdIpIfStatsHCOutMcastOctets [ ] ={1,3,6,1,4,1,29601,2,37,8,1,40};
UINT4 FsMIStdIpIfStatsInBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,41};
UINT4 FsMIStdIpIfStatsHCInBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,42};
UINT4 FsMIStdIpIfStatsOutBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,43};
UINT4 FsMIStdIpIfStatsHCOutBcastPkts [ ] ={1,3,6,1,4,1,29601,2,37,8,1,44};
UINT4 FsMIStdIpIfStatsDiscontinuityTime [ ] ={1,3,6,1,4,1,29601,2,37,8,1,45};
UINT4 FsMIStdIpIfStatsRefreshRate [ ] ={1,3,6,1,4,1,29601,2,37,8,1,47};
UINT4 FsMIStdIpIfStatsContextId [ ] ={1,3,6,1,4,1,29601,2,37,8,1,48};
UINT4 FsMIStdIpAddressPrefixIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,9,1,1};
UINT4 FsMIStdIpAddressPrefixType [ ] ={1,3,6,1,4,1,29601,2,37,9,1,2};
UINT4 FsMIStdIpAddressPrefixPrefix [ ] ={1,3,6,1,4,1,29601,2,37,9,1,3};
UINT4 FsMIStdIpAddressPrefixLength [ ] ={1,3,6,1,4,1,29601,2,37,9,1,4};
UINT4 FsMIStdIpAddressPrefixOrigin [ ] ={1,3,6,1,4,1,29601,2,37,9,1,5};
UINT4 FsMIStdIpAddressPrefixOnLinkFlag [ ] ={1,3,6,1,4,1,29601,2,37,9,1,6};
UINT4 FsMIStdIpAddressPrefixAutonomousFlag [ ] ={1,3,6,1,4,1,29601,2,37,9,1,7};
UINT4 FsMIStdIpAddressPrefixAdvPreferredLifetime [ ] ={1,3,6,1,4,1,29601,2,37,9,1,8};
UINT4 FsMIStdIpAddressPrefixAdvValidLifetime [ ] ={1,3,6,1,4,1,29601,2,37,9,1,9};
UINT4 FsMIStdIpAddressContextId [ ] ={1,3,6,1,4,1,29601,2,37,9,1,10};
UINT4 FsMIStdIpAddressAddrType [ ] ={1,3,6,1,4,1,29601,2,37,10,1,2};
UINT4 FsMIStdIpAddressAddr [ ] ={1,3,6,1,4,1,29601,2,37,10,1,3};
UINT4 FsMIStdIpAddressIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,10,1,4};
UINT4 FsMIStdIpAddressType [ ] ={1,3,6,1,4,1,29601,2,37,10,1,5};
UINT4 FsMIStdIpAddressPrefix [ ] ={1,3,6,1,4,1,29601,2,37,10,1,6};
UINT4 FsMIStdIpAddressOrigin [ ] ={1,3,6,1,4,1,29601,2,37,10,1,7};
UINT4 FsMIStdIpAddressStatus [ ] ={1,3,6,1,4,1,29601,2,37,10,1,8};
UINT4 FsMIStdIpAddressCreated [ ] ={1,3,6,1,4,1,29601,2,37,10,1,9};
UINT4 FsMIStdIpAddressLastChanged [ ] ={1,3,6,1,4,1,29601,2,37,10,1,10};
UINT4 FsMIStdIpAddressRowStatus [ ] ={1,3,6,1,4,1,29601,2,37,10,1,11};
UINT4 FsMIStdIpAddressStorageType [ ] ={1,3,6,1,4,1,29601,2,37,10,1,12};
UINT4 FsMIStdIpNetToPhysicalIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,11,1,1};
UINT4 FsMIStdIpNetToPhysicalNetAddressType [ ] ={1,3,6,1,4,1,29601,2,37,11,1,2};
UINT4 FsMIStdIpNetToPhysicalNetAddress [ ] ={1,3,6,1,4,1,29601,2,37,11,1,3};
UINT4 FsMIStdIpNetToPhysicalPhysAddress [ ] ={1,3,6,1,4,1,29601,2,37,11,1,4};
UINT4 FsMIStdIpNetToPhysicalLastUpdated [ ] ={1,3,6,1,4,1,29601,2,37,11,1,5};
UINT4 FsMIStdIpNetToPhysicalType [ ] ={1,3,6,1,4,1,29601,2,37,11,1,6};
UINT4 FsMIStdIpNetToPhysicalState [ ] ={1,3,6,1,4,1,29601,2,37,11,1,7};
UINT4 FsMIStdIpNetToPhysicalRowStatus [ ] ={1,3,6,1,4,1,29601,2,37,11,1,8};
UINT4 FsMIStdIpNetToPhysicalContextId [ ] ={1,3,6,1,4,1,29601,2,37,11,1,9};
UINT4 FsMIStdIpv6ScopeZoneIndexIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,12,1,1};
UINT4 FsMIStdIpv6ScopeZoneIndexLinkLocal [ ] ={1,3,6,1,4,1,29601,2,37,12,1,2};
UINT4 FsMIStdIpv6ScopeZoneIndex3 [ ] ={1,3,6,1,4,1,29601,2,37,12,1,3};
UINT4 FsMIStdIpv6ScopeZoneIndexAdminLocal [ ] ={1,3,6,1,4,1,29601,2,37,12,1,4};
UINT4 FsMIStdIpv6ScopeZoneIndexSiteLocal [ ] ={1,3,6,1,4,1,29601,2,37,12,1,5};
UINT4 FsMIStdIpv6ScopeZoneIndex6 [ ] ={1,3,6,1,4,1,29601,2,37,12,1,6};
UINT4 FsMIStdIpv6ScopeZoneIndex7 [ ] ={1,3,6,1,4,1,29601,2,37,12,1,7};
UINT4 FsMIStdIpv6ScopeZoneIndexOrganizationLocal [ ] ={1,3,6,1,4,1,29601,2,37,12,1,8};
UINT4 FsMIStdIpv6ScopeZoneIndex9 [ ] ={1,3,6,1,4,1,29601,2,37,12,1,9};
UINT4 FsMIStdIpv6ScopeZoneIndexA [ ] ={1,3,6,1,4,1,29601,2,37,12,1,10};
UINT4 FsMIStdIpv6ScopeZoneIndexB [ ] ={1,3,6,1,4,1,29601,2,37,12,1,11};
UINT4 FsMIStdIpv6ScopeZoneIndexC [ ] ={1,3,6,1,4,1,29601,2,37,12,1,12};
UINT4 FsMIStdIpv6ScopeZoneIndexD [ ] ={1,3,6,1,4,1,29601,2,37,12,1,13};
UINT4 FsMIStdIpv6ScopeZoneContextId [ ] ={1,3,6,1,4,1,29601,2,37,12,1,14};
UINT4 FsMIStdIpDefaultRouterAddressType [ ] ={1,3,6,1,4,1,29601,2,37,13,1,1};
UINT4 FsMIStdIpDefaultRouterAddress [ ] ={1,3,6,1,4,1,29601,2,37,13,1,2};
UINT4 FsMIStdIpDefaultRouterIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,13,1,3};
UINT4 FsMIStdIpDefaultRouterLifetime [ ] ={1,3,6,1,4,1,29601,2,37,13,1,4};
UINT4 FsMIStdIpDefaultRouterPreference [ ] ={1,3,6,1,4,1,29601,2,37,13,1,5};
UINT4 FsMIStdIpv6RouterAdvertIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,14,1,1};
UINT4 FsMIStdIpv6RouterAdvertSendAdverts [ ] ={1,3,6,1,4,1,29601,2,37,14,1,2};
UINT4 FsMIStdIpv6RouterAdvertMaxInterval [ ] ={1,3,6,1,4,1,29601,2,37,14,1,3};
UINT4 FsMIStdIpv6RouterAdvertMinInterval [ ] ={1,3,6,1,4,1,29601,2,37,14,1,4};
UINT4 FsMIStdIpv6RouterAdvertManagedFlag [ ] ={1,3,6,1,4,1,29601,2,37,14,1,5};
UINT4 FsMIStdIpv6RouterAdvertOtherConfigFlag [ ] ={1,3,6,1,4,1,29601,2,37,14,1,6};
UINT4 FsMIStdIpv6RouterAdvertLinkMTU [ ] ={1,3,6,1,4,1,29601,2,37,14,1,7};
UINT4 FsMIStdIpv6RouterAdvertReachableTime [ ] ={1,3,6,1,4,1,29601,2,37,14,1,8};
UINT4 FsMIStdIpv6RouterAdvertRetransmitTime [ ] ={1,3,6,1,4,1,29601,2,37,14,1,9};
UINT4 FsMIStdIpv6RouterAdvertCurHopLimit [ ] ={1,3,6,1,4,1,29601,2,37,14,1,10};
UINT4 FsMIStdIpv6RouterAdvertDefaultLifetime [ ] ={1,3,6,1,4,1,29601,2,37,14,1,11};
UINT4 FsMIStdIpv6RouterAdvertRowStatus [ ] ={1,3,6,1,4,1,29601,2,37,14,1,12};
UINT4 FsMIStdIpv6RouterAdvertContextId [ ] ={1,3,6,1,4,1,29601,2,37,14,1,13};
UINT4 FsMIStdIcmpStatsIPVersion [ ] ={1,3,6,1,4,1,29601,2,37,15,1,1};
UINT4 FsMIStdIcmpStatsInMsgs [ ] ={1,3,6,1,4,1,29601,2,37,15,1,2};
UINT4 FsMIStdIcmpStatsInErrors [ ] ={1,3,6,1,4,1,29601,2,37,15,1,3};
UINT4 FsMIStdIcmpStatsOutMsgs [ ] ={1,3,6,1,4,1,29601,2,37,15,1,4};
UINT4 FsMIStdIcmpStatsOutErrors [ ] ={1,3,6,1,4,1,29601,2,37,15,1,5};
UINT4 FsMIStdIcmpMsgStatsIPVersion [ ] ={1,3,6,1,4,1,29601,2,37,16,1,1};
UINT4 FsMIStdIcmpMsgStatsType [ ] ={1,3,6,1,4,1,29601,2,37,16,1,2};
UINT4 FsMIStdIcmpMsgStatsInPkts [ ] ={1,3,6,1,4,1,29601,2,37,16,1,3};
UINT4 FsMIStdIcmpMsgStatsOutPkts [ ] ={1,3,6,1,4,1,29601,2,37,16,1,4};
UINT4 FsMIStdInetCidrRouteDestType [ ] ={1,3,6,1,4,1,29601,2,37,17,1,1};
UINT4 FsMIStdInetCidrRouteDest [ ] ={1,3,6,1,4,1,29601,2,37,17,1,2};
UINT4 FsMIStdInetCidrRoutePfxLen [ ] ={1,3,6,1,4,1,29601,2,37,17,1,3};
UINT4 FsMIStdInetCidrRoutePolicy [ ] ={1,3,6,1,4,1,29601,2,37,17,1,4};
UINT4 FsMIStdInetCidrRouteNextHopType [ ] ={1,3,6,1,4,1,29601,2,37,17,1,5};
UINT4 FsMIStdInetCidrRouteNextHop [ ] ={1,3,6,1,4,1,29601,2,37,17,1,6};
UINT4 FsMIStdInetCidrRouteIfIndex [ ] ={1,3,6,1,4,1,29601,2,37,17,1,7};
UINT4 FsMIStdInetCidrRouteType [ ] ={1,3,6,1,4,1,29601,2,37,17,1,8};
UINT4 FsMIStdInetCidrRouteProto [ ] ={1,3,6,1,4,1,29601,2,37,17,1,9};
UINT4 FsMIStdInetCidrRouteAge [ ] ={1,3,6,1,4,1,29601,2,37,17,1,10};
UINT4 FsMIStdInetCidrRouteNextHopAS [ ] ={1,3,6,1,4,1,29601,2,37,17,1,11};
UINT4 FsMIStdInetCidrRouteMetric1 [ ] ={1,3,6,1,4,1,29601,2,37,17,1,12};
UINT4 FsMIStdInetCidrRouteMetric2 [ ] ={1,3,6,1,4,1,29601,2,37,17,1,13};
UINT4 FsMIStdInetCidrRouteMetric3 [ ] ={1,3,6,1,4,1,29601,2,37,17,1,14};
UINT4 FsMIStdInetCidrRouteMetric4 [ ] ={1,3,6,1,4,1,29601,2,37,17,1,15};
UINT4 FsMIStdInetCidrRouteMetric5 [ ] ={1,3,6,1,4,1,29601,2,37,17,1,16};
UINT4 FsMIStdInetCidrRouteStatus [ ] ={1,3,6,1,4,1,29601,2,37,17,1,17};
UINT4 FsMIStdInetCidrRouteAddrType [ ] ={1,3,6,1,4,1,29601,2,37,17,1,18};
UINT4 FsMIStdInetCidrRouteHWStatus [ ] ={1,3,6,1,4,1,29601,2,37,17,1,19};
UINT4 FsMIStdInetCidrRoutePreference [ ] ={1,3,6,1,4,1,29601,2,37,17,1,20};
UINT4 FsMIStdIpIndex [ ] ={1,3,6,1,4,1,29601,2,37,18,1,1};
UINT4 FsMIStdIpProxyArpAdminStatus [ ] ={1,3,6,1,4,1,29601,2,37,18,1,2};
UINT4 FsMIStdIpLocalProxyArpAdminStatus [ ] ={1,3,6,1,4,1,29601,2,37,18,1,3};
UINT4 FsMIStdIpProxyArpSubnetOption [ ] ={1,3,6,1,4,1,29601,2,37,19};




tMbDbEntry fsmsipMibEntry[]= {

{{10,FsMIStdIpv4InterfaceTableLastChange}, NULL, FsMIStdIpv4InterfaceTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIStdIpv6InterfaceTableLastChange}, NULL, FsMIStdIpv6InterfaceTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIStdIpIfStatsTableLastChange}, NULL, FsMIStdIpIfStatsTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMIStdIpContextId}, GetNextIndexFsMIStdIpGlobalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpForwarding}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdIpForwardingGet, FsMIStdIpForwardingSet, FsMIStdIpForwardingTest, FsMIStdIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpDefaultTTL}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdIpDefaultTTLGet, FsMIStdIpDefaultTTLSet, FsMIStdIpDefaultTTLTest, FsMIStdIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpReasmTimeout}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdIpReasmTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6IpForwarding}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdIpv6IpForwardingGet, FsMIStdIpv6IpForwardingSet, FsMIStdIpv6IpForwardingTest, FsMIStdIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6IpDefaultHopLimit}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdIpv6IpDefaultHopLimitGet, FsMIStdIpv6IpDefaultHopLimitSet, FsMIStdIpv6IpDefaultHopLimitTest, FsMIStdIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteNumber}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdInetCidrRouteNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteDiscards}, GetNextIndexFsMIStdIpGlobalTable, FsMIStdInetCidrRouteDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv4InterfaceIfIndex}, GetNextIndexFsMIStdIpv4InterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv4InterfaceReasmMaxSize}, GetNextIndexFsMIStdIpv4InterfaceTable, FsMIStdIpv4InterfaceReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv4InterfaceEnableStatus}, GetNextIndexFsMIStdIpv4InterfaceTable, FsMIStdIpv4InterfaceEnableStatusGet, FsMIStdIpv4InterfaceEnableStatusSet, FsMIStdIpv4InterfaceEnableStatusTest, FsMIStdIpv4InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv4InterfaceRetransmitTime}, GetNextIndexFsMIStdIpv4InterfaceTable, FsMIStdIpv4InterfaceRetransmitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv4InterfaceTableINDEX, 1, 0, 0, "1000"},

{{12,FsMIStdIpv4IfContextId}, GetNextIndexFsMIStdIpv4InterfaceTable, FsMIStdIpv4IfContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceIfIndex}, GetNextIndexFsMIStdIpv6InterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceReasmMaxSize}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceIdentifier}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceEnableStatus}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceEnableStatusGet, FsMIStdIpv6InterfaceEnableStatusSet, FsMIStdIpv6InterfaceEnableStatusTest, FsMIStdIpv6InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceReachableTime}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceReachableTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceRetransmitTime}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceRetransmitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6InterfaceForwarding}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6InterfaceForwardingGet, FsMIStdIpv6InterfaceForwardingSet, FsMIStdIpv6InterfaceForwardingTest, FsMIStdIpv6InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6IfContextId}, GetNextIndexFsMIStdIpv6InterfaceTable, FsMIStdIpv6IfContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsIPVersion}, GetNextIndexFsMIStdIpSystemStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInReceives}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInReceives}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInHdrErrors}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInNoRoutes}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInAddrErrors}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInUnknownProtos}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInTruncatedPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInForwDatagrams}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInForwDatagrams}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsReasmReqds}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsReasmOKs}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsReasmFails}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInDiscards}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInDelivers}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInDelivers}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutRequests}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutRequests}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutNoRoutes}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutForwDatagrams}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutForwDatagrams}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutDiscards}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutFragReqds}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutFragReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutFragOKs}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutFragFails}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutFragCreates}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutTransmits}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutTransmits}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInMcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInMcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInMcastOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInMcastOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutMcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutMcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutMcastOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutMcastOctets}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsInBcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCInBcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsOutBcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsHCOutBcastPkts}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsHCOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsDiscontinuityTime}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpSystemStatsRefreshRate}, GetNextIndexFsMIStdIpSystemStatsTable, FsMIStdIpSystemStatsRefreshRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpSystemStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsIPVersion}, GetNextIndexFsMIStdIpIfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsIfIndex}, GetNextIndexFsMIStdIpIfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInReceives}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInReceives}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInHdrErrors}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInNoRoutes}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInAddrErrors}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInUnknownProtos}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInTruncatedPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInForwDatagrams}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInForwDatagrams}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsReasmReqds}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsReasmOKs}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsReasmFails}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInDiscards}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInDelivers}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInDelivers}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutRequests}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutRequests}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutForwDatagrams}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutForwDatagrams}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutDiscards}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutFragReqds}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutFragReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutFragOKs}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutFragFails}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutFragCreates}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutTransmits}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutTransmits}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInMcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInMcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInMcastOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInMcastOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutMcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutMcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutMcastOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutMcastOctets}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsInBcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCInBcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsOutBcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsHCOutBcastPkts}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsHCOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsDiscontinuityTime}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsRefreshRate}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsRefreshRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpIfStatsContextId}, GetNextIndexFsMIStdIpIfStatsTable, FsMIStdIpIfStatsContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpIfStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixIfIndex}, GetNextIndexFsMIStdIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixType}, GetNextIndexFsMIStdIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixPrefix}, GetNextIndexFsMIStdIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixLength}, GetNextIndexFsMIStdIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixOrigin}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressPrefixOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixOnLinkFlag}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressPrefixOnLinkFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixAutonomousFlag}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressPrefixAutonomousFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixAdvPreferredLifetime}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressPrefixAdvPreferredLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressPrefixAdvValidLifetime}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressPrefixAdvValidLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressContextId}, GetNextIndexFsMIStdIpAddressPrefixTable, FsMIStdIpAddressContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdIpAddressAddrType}, GetNextIndexFsMIStdIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressAddr}, GetNextIndexFsMIStdIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressIfIndex}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressIfIndexGet, FsMIStdIpAddressIfIndexSet, FsMIStdIpAddressIfIndexTest, FsMIStdIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressType}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressTypeGet, FsMIStdIpAddressTypeSet, FsMIStdIpAddressTypeTest, FsMIStdIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpAddressTableINDEX, 3, 0, 0, "1"},

{{12,FsMIStdIpAddressPrefix}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressPrefixGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressOrigin}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressStatus}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressStatusGet, FsMIStdIpAddressStatusSet, FsMIStdIpAddressStatusTest, FsMIStdIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressCreated}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressCreatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressLastChanged}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdIpAddressTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpAddressRowStatus}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressRowStatusGet, FsMIStdIpAddressRowStatusSet, FsMIStdIpAddressRowStatusTest, FsMIStdIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpAddressTableINDEX, 3, 0, 1, NULL},

{{12,FsMIStdIpAddressStorageType}, GetNextIndexFsMIStdIpAddressTable, FsMIStdIpAddressStorageTypeGet, FsMIStdIpAddressStorageTypeSet, FsMIStdIpAddressStorageTypeTest, FsMIStdIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpAddressTableINDEX, 3, 0, 0, "2"},

{{12,FsMIStdIpNetToPhysicalIfIndex}, GetNextIndexFsMIStdIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalNetAddressType}, GetNextIndexFsMIStdIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalNetAddress}, GetNextIndexFsMIStdIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalPhysAddress}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalPhysAddressGet, FsMIStdIpNetToPhysicalPhysAddressSet, FsMIStdIpNetToPhysicalPhysAddressTest, FsMIStdIpNetToPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalLastUpdated}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalLastUpdatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalType}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalTypeGet, FsMIStdIpNetToPhysicalTypeSet, FsMIStdIpNetToPhysicalTypeTest, FsMIStdIpNetToPhysicalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, "4"},

{{12,FsMIStdIpNetToPhysicalState}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpNetToPhysicalRowStatus}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalRowStatusGet, FsMIStdIpNetToPhysicalRowStatusSet, FsMIStdIpNetToPhysicalRowStatusTest, FsMIStdIpNetToPhysicalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 1, NULL},

{{12,FsMIStdIpNetToPhysicalContextId}, GetNextIndexFsMIStdIpNetToPhysicalTable, FsMIStdIpNetToPhysicalContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexIfIndex}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexLinkLocal}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexLinkLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndex3}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndex3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexAdminLocal}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexAdminLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexSiteLocal}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexSiteLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndex6}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndex6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndex7}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndex7Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexOrganizationLocal}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexOrganizationLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndex9}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndex9Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexA}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexAGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexB}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexBGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexC}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneIndexD}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneIndexDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6ScopeZoneContextId}, GetNextIndexFsMIStdIpv6ScopeZoneIndexTable, FsMIStdIpv6ScopeZoneContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpDefaultRouterAddressType}, GetNextIndexFsMIStdIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpDefaultRouterAddress}, GetNextIndexFsMIStdIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdIpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpDefaultRouterIfIndex}, GetNextIndexFsMIStdIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpDefaultRouterLifetime}, GetNextIndexFsMIStdIpDefaultRouterTable, FsMIStdIpDefaultRouterLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdIpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpDefaultRouterPreference}, GetNextIndexFsMIStdIpDefaultRouterTable, FsMIStdIpDefaultRouterPreferenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdIpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIpv6RouterAdvertIfIndex}, GetNextIndexFsMIStdIpv6RouterAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6RouterAdvertSendAdverts}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertSendAdvertsGet, FsMIStdIpv6RouterAdvertSendAdvertsSet, FsMIStdIpv6RouterAdvertSendAdvertsTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{12,FsMIStdIpv6RouterAdvertMaxInterval}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertMaxIntervalGet, FsMIStdIpv6RouterAdvertMaxIntervalSet, FsMIStdIpv6RouterAdvertMaxIntervalTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "600"},

{{12,FsMIStdIpv6RouterAdvertMinInterval}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertMinIntervalGet, FsMIStdIpv6RouterAdvertMinIntervalSet, FsMIStdIpv6RouterAdvertMinIntervalTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6RouterAdvertManagedFlag}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertManagedFlagGet, FsMIStdIpv6RouterAdvertManagedFlagSet, FsMIStdIpv6RouterAdvertManagedFlagTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{12,FsMIStdIpv6RouterAdvertOtherConfigFlag}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertOtherConfigFlagGet, FsMIStdIpv6RouterAdvertOtherConfigFlagSet, FsMIStdIpv6RouterAdvertOtherConfigFlagTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{12,FsMIStdIpv6RouterAdvertLinkMTU}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertLinkMTUGet, FsMIStdIpv6RouterAdvertLinkMTUSet, FsMIStdIpv6RouterAdvertLinkMTUTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdIpv6RouterAdvertReachableTime}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertReachableTimeGet, FsMIStdIpv6RouterAdvertReachableTimeSet, FsMIStdIpv6RouterAdvertReachableTimeTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdIpv6RouterAdvertRetransmitTime}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertRetransmitTimeGet, FsMIStdIpv6RouterAdvertRetransmitTimeSet, FsMIStdIpv6RouterAdvertRetransmitTimeTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdIpv6RouterAdvertCurHopLimit}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertCurHopLimitGet, FsMIStdIpv6RouterAdvertCurHopLimitSet, FsMIStdIpv6RouterAdvertCurHopLimitTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6RouterAdvertDefaultLifetime}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertDefaultLifetimeGet, FsMIStdIpv6RouterAdvertDefaultLifetimeSet, FsMIStdIpv6RouterAdvertDefaultLifetimeTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpv6RouterAdvertRowStatus}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertRowStatusGet, FsMIStdIpv6RouterAdvertRowStatusSet, FsMIStdIpv6RouterAdvertRowStatusTest, FsMIStdIpv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 1, NULL},

{{12,FsMIStdIpv6RouterAdvertContextId}, GetNextIndexFsMIStdIpv6RouterAdvertTable, FsMIStdIpv6RouterAdvertContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdIpv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIcmpStatsIPVersion}, GetNextIndexFsMIStdIcmpStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIcmpStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIcmpStatsInMsgs}, GetNextIndexFsMIStdIcmpStatsTable, FsMIStdIcmpStatsInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIcmpStatsInErrors}, GetNextIndexFsMIStdIcmpStatsTable, FsMIStdIcmpStatsInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIcmpStatsOutMsgs}, GetNextIndexFsMIStdIcmpStatsTable, FsMIStdIcmpStatsOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIcmpStatsOutErrors}, GetNextIndexFsMIStdIcmpStatsTable, FsMIStdIcmpStatsOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIStdIcmpMsgStatsIPVersion}, GetNextIndexFsMIStdIcmpMsgStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdIcmpMsgStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIcmpMsgStatsType}, GetNextIndexFsMIStdIcmpMsgStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdIcmpMsgStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIcmpMsgStatsInPkts}, GetNextIndexFsMIStdIcmpMsgStatsTable, FsMIStdIcmpMsgStatsInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpMsgStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdIcmpMsgStatsOutPkts}, GetNextIndexFsMIStdIcmpMsgStatsTable, FsMIStdIcmpMsgStatsOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdIcmpMsgStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteDestType}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteDest}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRoutePfxLen}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRoutePolicy}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteNextHopType}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteNextHop}, GetNextIndexFsMIStdInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteIfIndex}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteIfIndexGet, FsMIStdInetCidrRouteIfIndexSet, FsMIStdInetCidrRouteIfIndexTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteType}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteTypeGet, FsMIStdInetCidrRouteTypeSet, FsMIStdInetCidrRouteTypeTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteProto}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteAge}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRouteNextHopAS}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteNextHopASGet, FsMIStdInetCidrRouteNextHopASSet, FsMIStdInetCidrRouteNextHopASTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "0"},

{{12,FsMIStdInetCidrRouteMetric1}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteMetric1Get, FsMIStdInetCidrRouteMetric1Set, FsMIStdInetCidrRouteMetric1Test, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "-1"},

{{12,FsMIStdInetCidrRouteMetric2}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteMetric2Get, FsMIStdInetCidrRouteMetric2Set, FsMIStdInetCidrRouteMetric2Test, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "-1"},

{{12,FsMIStdInetCidrRouteMetric3}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteMetric3Get, FsMIStdInetCidrRouteMetric3Set, FsMIStdInetCidrRouteMetric3Test, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "-1"},

{{12,FsMIStdInetCidrRouteMetric4}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteMetric4Get, FsMIStdInetCidrRouteMetric4Set, FsMIStdInetCidrRouteMetric4Test, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "-1"},

{{12,FsMIStdInetCidrRouteMetric5}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteMetric5Get, FsMIStdInetCidrRouteMetric5Set, FsMIStdInetCidrRouteMetric5Test, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "-1"},

{{12,FsMIStdInetCidrRouteStatus}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteStatusGet, FsMIStdInetCidrRouteStatusSet, FsMIStdInetCidrRouteStatusTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 1, NULL},

{{12,FsMIStdInetCidrRouteAddrType}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteAddrTypeGet, FsMIStdInetCidrRouteAddrTypeSet, FsMIStdInetCidrRouteAddrTypeTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, "1"},

{{12,FsMIStdInetCidrRouteHWStatus}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRouteHWStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdInetCidrRoutePreference}, GetNextIndexFsMIStdInetCidrRouteTable, FsMIStdInetCidrRoutePreferenceGet, FsMIStdInetCidrRoutePreferenceSet, FsMIStdInetCidrRoutePreferenceTest, FsMIStdInetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdInetCidrRouteTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdIpIndex}, GetNextIndexFsMIStdIpifTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdIpProxyArpAdminStatus}, GetNextIndexFsMIStdIpifTable, FsMIStdIpProxyArpAdminStatusGet, FsMIStdIpProxyArpAdminStatusSet, FsMIStdIpProxyArpAdminStatusTest, FsMIStdIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpifTableINDEX, 1, 0, 0, "2"},

{{12,FsMIStdIpLocalProxyArpAdminStatus}, GetNextIndexFsMIStdIpifTable, FsMIStdIpLocalProxyArpAdminStatusGet, FsMIStdIpLocalProxyArpAdminStatusSet, FsMIStdIpLocalProxyArpAdminStatusTest, FsMIStdIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdIpifTableINDEX, 1, 0, 0, "2"},

{{10,FsMIStdIpProxyArpSubnetOption}, NULL, FsMIStdIpProxyArpSubnetOptionGet, FsMIStdIpProxyArpSubnetOptionSet, FsMIStdIpProxyArpSubnetOptionTest, FsMIStdIpProxyArpSubnetOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

};

tMibData fsmsipEntry = { sizeof (fsmsipMibEntry) / sizeof (fsmsipMibEntry[0]), fsmsipMibEntry };

#endif /* _FSMSIPDB_H */

