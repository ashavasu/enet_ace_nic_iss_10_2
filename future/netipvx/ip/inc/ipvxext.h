
 /*********************************************************************
 * copyright (C) 2007 Aricent Inc. All Rights Reserved.
 *
* $Id: ipvxext.h,v 1.14 2016/02/27 10:14:52 siva Exp $
 *
 * Description : This file contains header files included in
 *               IPVX module.
 *********************************************************************/

#ifndef _IPVX_EXT_H_
#define _IPVX_EXT_H_

/* IPv4 */
#include "ipv6.h"
#include "cfa.h"

extern  UINT4  gu4CurrContextId;

/* Proto Validate Index Instance for FsIpAddressTable. */
INT1
nmhValidateIndexInstanceFsIpAddressTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpAddressTable  */

INT1
nmhGetFirstIndexFsIpAddressTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpAddressTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpAddrTabIfaceId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabAdvertise ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabPreflevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpAddrTabAdvertise ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpAddrTabPreflevel ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpAddrTabAdvertise ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpAddrTabPreflevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpifTable. */
INT1
nmhValidateIndexInstanceFsIpifTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpifTable  */

INT1
nmhGetFirstIndexFsIpifTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpifTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpifMaxReasmSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIpifIcmpRedirectEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIpifDrtBcastFwdingEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpifMaxReasmSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIpifIcmpRedirectEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIpifDrtBcastFwdingEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpifMaxReasmSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIpifIcmpRedirectEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIpifDrtBcastFwdingEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpifTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIcmpSendRedirectEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpSendUnreachableEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpSendEchoReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpNetMaskReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpTimeStampReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpInDomainNameRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpInDomainNameReply ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutDomainNameRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutDomainNameReply ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpDirectQueryEnable ARG_LIST((INT4 *));

INT1
nmhGetFsDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTimeToLive ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpInSecurityFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutSecurityFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIcmpSendRedirectEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendUnreachableEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendEchoReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpNetMaskReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpTimeStampReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpDirectQueryEnable ARG_LIST((INT4 ));

INT1
nmhSetFsDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTimeToLive ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIcmpSendRedirectEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendUnreachableEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendEchoReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpNetMaskReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpTimeStampReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpDirectQueryEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDomainName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTimeToLive ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpRecvSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIcmpSendRedirectEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendUnreachableEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendEchoReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpNetMaskReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpTimeStampReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpDirectQueryEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDomainName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTimeToLive ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendSecurityFailuresEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpRecvSecurityFailuresEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsUdpInNoCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInIcmpErr ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInErrCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInBcast ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsCidrAggTable. */
INT1
nmhValidateIndexInstanceFsCidrAggTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCidrAggTable  */

INT1
nmhGetFirstIndexFsCidrAggTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCidrAggTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCidrAggStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCidrAggStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCidrAggStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCidrAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsCidrAdvertTable. */
INT1
nmhValidateIndexInstanceFsCidrAdvertTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCidrAdvertTable  */

INT1
nmhGetFirstIndexFsCidrAdvertTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCidrAdvertTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCidrAdvertStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCidrAdvertStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCidrAdvertStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCidrAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsHardwareAddrLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsProtocolAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsEntryStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNoOfStaticRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfAggregatedRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfReassemblyLists ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfFragmentsPerList ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNoOfStaticRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfAggregatedRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfReassemblyLists ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfFragmentsPerList ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNoOfStaticRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfAggregatedRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfReassemblyLists ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfFragmentsPerList ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNoOfStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfAggregatedRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfReassemblyLists ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfFragmentsPerList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/*extern definition of functions declared in stdiplow.h*/

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for Fsipv6UdpTable. */
INT1
nmhValidateIndexInstanceFsipv6UdpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6UdpTable  */

INT1
nmhGetFirstIndexFsipv6UdpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6UdpTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6Forwarding ARG_LIST((INT4 *));

INT1
nmhGetIpv6DefaultHopLimit ARG_LIST((INT4 *));

INT1
nmhGetIpv6Interfaces ARG_LIST((UINT4 *));

INT1
nmhGetIpv6IfTableLastChange ARG_LIST((UINT4 *));

INT1
nmhGetIpv6RouteNumber ARG_LIST((UINT4 *));

INT1
nmhGetIpv6DiscardedRoutes ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6Forwarding ARG_LIST((INT4 ));

INT1
nmhSetIpv6DefaultHopLimit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6Forwarding ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ipv6DefaultHopLimit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6Forwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ipv6DefaultHopLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6IfTable. */
INT1
nmhValidateIndexInstanceIpv6IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6IfTable  */

INT1
nmhGetFirstIndexIpv6IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6IfDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfLowerLayer ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpv6IfEffectiveMtu ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfReasmMaxSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfIdentifierLength ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfPhysicalAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6IfAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6IfLastChange ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6IfDescr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpv6IfIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpv6IfIdentifierLength ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6IfAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6IfDescr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ipv6IfIdentifier ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ipv6IfIdentifierLength ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6IfStatsTable. */
INT1
nmhValidateIndexInstanceIpv6IfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6IfStatsTable  */

INT1
nmhGetFirstIndexIpv6IfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6IfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6IfStatsInReceives ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInHdrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInTooBigErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInNoRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInAddrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInTruncatedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInDelivers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutForwDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutFragCreates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmReqds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsReasmFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsInMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6IfStatsOutMcastPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ipv6AddrPrefixTable. */
INT1
nmhValidateIndexInstanceIpv6AddrPrefixTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6AddrPrefixTable  */

INT1
nmhGetFirstIndexIpv6AddrPrefixTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6AddrPrefixTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6AddrPrefixOnLinkFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpv6AddrPrefixAutonomousFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetIpv6AddrPrefixAdvPreferredLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpv6AddrPrefixAdvValidLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ipv6AddrTable. */
INT1
nmhValidateIndexInstanceIpv6AddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ipv6AddrTable  */

INT1
nmhGetFirstIndexIpv6AddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6AddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6AddrPfxLength ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrAnycastFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6AddrStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for Ipv6RouteTable. */
INT1
nmhValidateIndexInstanceIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6RouteTable  */

INT1
nmhGetFirstIndexIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteNextHop ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6RouteType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RoutePolicy ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpv6RouteAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteNextHopRDI ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteWeight ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIpv6RouteInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpv6RouteValid ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6RouteValid ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6RouteValid ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6RouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6NetToMediaTable. */
INT1
nmhValidateIndexInstanceIpv6NetToMediaTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ipv6NetToMediaTable  */

INT1
nmhGetFirstIndexIpv6NetToMediaTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6NetToMediaTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6NetToMediaPhysAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6NetToMediaType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6IfNetToMediaState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpv6IfNetToMediaLastUpdated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIpv6NetToMediaValid ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6NetToMediaValid ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6NetToMediaValid ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6NetToMediaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhValidateIndexInstanceIfIpTable ARG_LIST ((INT4));

INT1
nmhGetFirstIndexIfIpTable ARG_LIST ((INT4 *));

INT1
nmhGetNextIndexIfIpTable ARG_LIST ((INT4, INT4 *));

INT4
CfaIpIfTblLock ARG_LIST ((VOID));

INT4
CfaIpIfTblUnlock  ARG_LIST ((VOID));

INT1
nmhSetIfMainRowStatus ARG_LIST ((INT4 , INT4));

INT1
nmhSetIfMainType ARG_LIST ((INT4 , INT4));

INT1
nmhSetIfIpAddrAllocMethod ARG_LIST ((INT4 , INT4));

INT1
nmhSetIfIpAddr ARG_LIST ((INT4 , UINT4));

INT1
nmhSetIfIpSubnetMask ARG_LIST ((INT4 , UINT4));

INT1
nmhSetIfIpBroadcastAddr ARG_LIST ((INT4 , UINT4));

INT1
nmhSetIfIpAddrAllocProtocol ARG_LIST ((INT4 , INT4));

INT1
nmhTestv2IfMainRowStatus ARG_LIST ((UINT4 *, INT4 , INT4));

tIp6AddrInfo *
Ip6AddrTblGetEntry ARG_LIST ((UINT4 , tIp6Addr *, UINT1));

INT1
CfaIpIfCreateIpInterface ARG_LIST ((UINT4));

INT4
CfaCreateInterface ARG_LIST ((INT4, UINT4, UINT4, UINT1 *));

INT4
CfaIvrSetSecondaryIpAddress ARG_LIST ((INT4, UINT4, UINT4, UINT4));

INT4
CfaDeleteInterface ARG_LIST ((INT4, UINT4));

INT1
SetPrefixTableRowStatus ARG_LIST ((INT4, UINT1, INT4, tIp6Addr , UINT1));

INT1
CfaSetInterfaceAdminStatus ARG_LIST ((INT4, UINT4, UINT4));

VOID
Ip6SetInterfaceAdminStatus  ARG_LIST ((INT4, UINT4, UINT4));

INT4
Ip6AddrTblNextAddr ARG_LIST ((UINT4 *, tIp6Addr *, UINT1 *, UINT1));



tIp6AddrInfo    *
Ip6AddrAllIfScanInCxt ARG_LIST ((UINT4, tIp6Addr *, UINT1));

INT1
SetAddrTableRowStatusInCxt ARG_LIST ((UINT4, INT1, INT4, 
                                      tSNMP_OCTET_STRING_TYPE *));

tIp6AddrInfo *
FindIPv6AddrTableRowInCxt ARG_LIST ((UINT4, tSNMP_OCTET_STRING_TYPE *,UINT4 *));

tIpIfRecord *
FindIPv4AddrTableRowInCxt ARG_LIST ((UINT4 u4CxtId, 
                                     tSNMP_OCTET_STRING_TYPE *pIpAddr));

tIp6If   *
Ip6AddrScanForInterface ARG_LIST ((tIp6Addr * pAddr6));

INT1
CfaIvrDeleteIpAddress ARG_LIST ((INT4, UINT4, UINT4));

INT4
CfaTestIfIpAddrAndIfIpSubnetMask ARG_LIST ((INT4, UINT4, UINT4));

UINT4
Ip6GetIfReTransTime (UINT4 u4IfIndex);

UINT4
Ip6GetIfReachTime (UINT4 u4IfIndex);

INT1
IncMsrForAddrPrefixTable  ARG_LIST 
                        ((INT4 i4FsIpvxAddrPrefixIfIndex,
                          INT4 i4FsIpvxAddrPrefixAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFsIpvxAddrPrefix,
                          UINT4 u4FsIpvxAddrPrefixLen,
                          INT4 i4SetValFsIpvxAddrPrefixRowStatus,
                          UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                          UINT1 IsRowStatus));

INT1
IncMsrForIPVXIfTable ARG_LIST ((INT4 i4Ipv6InterfaceIfIndex,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OIdLen, UINT1 IsRowStatus));

INT1
IncMsrForIPVXAddrTable ARG_LIST ((UINT4 u4ContextId, INT4 i4IpAddressAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pIpAddressAddr,
                                  INT4 i4SetVal, UINT4 *pu4ObjectId, 
                                  UINT4 u4OIdLen, UINT1 IsRowStatus));
INT1
IncMsrForIPVXNetToPhyTable ARG_LIST ((INT4 i4IpNetToPhysicalIfIndex,
                            INT4 i4IpNetToPhysicalNetAddressType,
                            tSNMP_OCTET_STRING_TYPE * pIpNetToPhyNetAddr,
                            CHR1 cDatatype, VOID *pSetVal,
                            UINT4 *pu4ObjectId, UINT4 u4OIdLen, 
                            UINT1 IsRowStatus));
INT1
IncMsrForIPVXRtrAdverTable  ARG_LIST ((INT4 i4Ipv6RouterAdvertIfIndex,
                            INT4 i4SetVal, UINT4 u4SetVal,
                            UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                            UINT1 IsRowStatus));
    
INT1
IncMsrForIpvxGlbTable ARG_LIST ((UINT4 u4ContextId, INT4 i4SetVal,
                                 UINT4 *pu4ObjectId, UINT4 u4OIdLen));

VOID
IncMsrForIpv6RouteTable ARG_LIST ((UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pDestIp,
                                   INT4 i4PrefixLen, 
                                   tSNMP_OCTET_STRING_TYPE * pNextHop,
                                   CHR1 cDatatype, VOID *pSetVal,
                                   UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                                   UINT1 IsRowStatus));

INT4  IpvxSetContext ARG_LIST ((UINT4));
VOID  IpvxResetContext ARG_LIST ((VOID));


extern INT1
IpSetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
IpSetFsIpifProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
IpTestv2FsIpifLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
IpTestv2FsIpifProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
IpGetFirstIndexFsIpifTable ARG_LIST((INT4 *));

extern INT1
IpGetNextIndexFsIpifTable ARG_LIST((INT4 , INT4 *));

extern INT1
IpGetFsIpifProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
IpSetFsIpProxyArpSubnetOption ARG_LIST((INT4 ));

extern INT1
IpTestv2FsIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
IpGetFsIpProxyArpSubnetOption ARG_LIST((INT4 *));

extern INT1
IpGetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));





#ifndef LNXIP6_WANTED
tIp6AddrInfo *  Ip6GetAddressInfo ARG_LIST ((UINT4, tIp6Addr));
tIp6AddrInfo *  Ip6GetNextAddressInfo  ARG_LIST ((UINT4, tIp6Addr));
tIp6AddrInfo       *
Ip6AddrTblGetAddrInfo ARG_LIST ((UINT4 u4Index));
extern VOID    Nd6ActOnRaCnf           PROTO ((tIp6If * pIf6, UINT1 u1Status));
#else /* LNXIP6_WANTED */
UINT4
Ip6GetAddressInfo (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                   UINT4 *pu4IfIndex, INT4 *pi4Prefix, UINT1 *pu1AddrType);
UINT4
Ip6GetAddressStatus (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                     INT4 *pi4AddrStatus);
UINT4
Ip6GetAddrAdminStatus (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                       INT4 *pi4AddrRowStatus);
UINT4
Ip6GetNextAddressInfo (UINT4 u4ContextId, tIp6Addr *pIp6Addr,
                       tIp6Addr *pNextAddr, UINT4 *pu4NextIndex);
UINT4
Ip6AddrTblGetAddrInfo (UINT4 u4IfIndex, tIp6Addr *pIp6Addr, UINT1 *pu1Prefix);
#endif /* LNXIP6_WANTED */

extern INT4  Ip6VRExists (UINT4 u4ContextId);

extern INT4  UtilRtm6SetContext PROTO ((UINT4 u4Rtm6CxtId));
extern VOID  UtilRtm6ResetContext PROTO ((VOID));

#endif /* _IPVX_EXT_H_ */

