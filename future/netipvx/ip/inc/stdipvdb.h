/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipvdb.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDIPVDB_H
#define _STDIPVDB_H

UINT1 Ipv4InterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Ipv6InterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IpSystemStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IpIfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 IpAddressPrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 IpAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IpNetToPhysicalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Ipv6ScopeZoneIndexTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IpDefaultRouterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ipv6RouterAdvertTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IcmpStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IcmpMsgStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IpAddrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpNetToMediaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 InetCidrRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OBJECT_ID ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IpCidrRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpForwardTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 stdipv [] ={1,3,6,1,2,1,4};
tSNMP_OID_TYPE stdipvOID = {7, stdipv};


UINT4 IpForwarding [ ] ={1,3,6,1,2,1,4,1};
UINT4 IpDefaultTTL [ ] ={1,3,6,1,2,1,4,2};
UINT4 IpReasmTimeout [ ] ={1,3,6,1,2,1,4,13};
UINT4 Ipv6IpForwarding [ ] ={1,3,6,1,2,1,4,25};
UINT4 Ipv6IpDefaultHopLimit [ ] ={1,3,6,1,2,1,4,26};
UINT4 Ipv4InterfaceTableLastChange [ ] ={1,3,6,1,2,1,4,27};
UINT4 Ipv4InterfaceIfIndex [ ] ={1,3,6,1,2,1,4,28,1,1};
UINT4 Ipv4InterfaceReasmMaxSize [ ] ={1,3,6,1,2,1,4,28,1,2};
UINT4 Ipv4InterfaceEnableStatus [ ] ={1,3,6,1,2,1,4,28,1,3};
UINT4 Ipv4InterfaceRetransmitTime [ ] ={1,3,6,1,2,1,4,28,1,4};
UINT4 Ipv6InterfaceTableLastChange [ ] ={1,3,6,1,2,1,4,29};
UINT4 Ipv6InterfaceIfIndex [ ] ={1,3,6,1,2,1,4,30,1,1};
UINT4 Ipv6InterfaceReasmMaxSize [ ] ={1,3,6,1,2,1,4,30,1,2};
UINT4 Ipv6InterfaceIdentifier [ ] ={1,3,6,1,2,1,4,30,1,3};
UINT4 Ipv6InterfaceEnableStatus [ ] ={1,3,6,1,2,1,4,30,1,5};
UINT4 Ipv6InterfaceReachableTime [ ] ={1,3,6,1,2,1,4,30,1,6};
UINT4 Ipv6InterfaceRetransmitTime [ ] ={1,3,6,1,2,1,4,30,1,7};
UINT4 Ipv6InterfaceForwarding [ ] ={1,3,6,1,2,1,4,30,1,8};
UINT4 IpSystemStatsIPVersion [ ] ={1,3,6,1,2,1,4,31,1,1,1};
UINT4 IpSystemStatsInReceives [ ] ={1,3,6,1,2,1,4,31,1,1,3};
UINT4 IpSystemStatsHCInReceives [ ] ={1,3,6,1,2,1,4,31,1,1,4};
UINT4 IpSystemStatsInOctets [ ] ={1,3,6,1,2,1,4,31,1,1,5};
UINT4 IpSystemStatsHCInOctets [ ] ={1,3,6,1,2,1,4,31,1,1,6};
UINT4 IpSystemStatsInHdrErrors [ ] ={1,3,6,1,2,1,4,31,1,1,7};
UINT4 IpSystemStatsInNoRoutes [ ] ={1,3,6,1,2,1,4,31,1,1,8};
UINT4 IpSystemStatsInAddrErrors [ ] ={1,3,6,1,2,1,4,31,1,1,9};
UINT4 IpSystemStatsInUnknownProtos [ ] ={1,3,6,1,2,1,4,31,1,1,10};
UINT4 IpSystemStatsInTruncatedPkts [ ] ={1,3,6,1,2,1,4,31,1,1,11};
UINT4 IpSystemStatsInForwDatagrams [ ] ={1,3,6,1,2,1,4,31,1,1,12};
UINT4 IpSystemStatsHCInForwDatagrams [ ] ={1,3,6,1,2,1,4,31,1,1,13};
UINT4 IpSystemStatsReasmReqds [ ] ={1,3,6,1,2,1,4,31,1,1,14};
UINT4 IpSystemStatsReasmOKs [ ] ={1,3,6,1,2,1,4,31,1,1,15};
UINT4 IpSystemStatsReasmFails [ ] ={1,3,6,1,2,1,4,31,1,1,16};
UINT4 IpSystemStatsInDiscards [ ] ={1,3,6,1,2,1,4,31,1,1,17};
UINT4 IpSystemStatsInDelivers [ ] ={1,3,6,1,2,1,4,31,1,1,18};
UINT4 IpSystemStatsHCInDelivers [ ] ={1,3,6,1,2,1,4,31,1,1,19};
UINT4 IpSystemStatsOutRequests [ ] ={1,3,6,1,2,1,4,31,1,1,20};
UINT4 IpSystemStatsHCOutRequests [ ] ={1,3,6,1,2,1,4,31,1,1,21};
UINT4 IpSystemStatsOutNoRoutes [ ] ={1,3,6,1,2,1,4,31,1,1,22};
UINT4 IpSystemStatsOutForwDatagrams [ ] ={1,3,6,1,2,1,4,31,1,1,23};
UINT4 IpSystemStatsHCOutForwDatagrams [ ] ={1,3,6,1,2,1,4,31,1,1,24};
UINT4 IpSystemStatsOutDiscards [ ] ={1,3,6,1,2,1,4,31,1,1,25};
UINT4 IpSystemStatsOutFragReqds [ ] ={1,3,6,1,2,1,4,31,1,1,26};
UINT4 IpSystemStatsOutFragOKs [ ] ={1,3,6,1,2,1,4,31,1,1,27};
UINT4 IpSystemStatsOutFragFails [ ] ={1,3,6,1,2,1,4,31,1,1,28};
UINT4 IpSystemStatsOutFragCreates [ ] ={1,3,6,1,2,1,4,31,1,1,29};
UINT4 IpSystemStatsOutTransmits [ ] ={1,3,6,1,2,1,4,31,1,1,30};
UINT4 IpSystemStatsHCOutTransmits [ ] ={1,3,6,1,2,1,4,31,1,1,31};
UINT4 IpSystemStatsOutOctets [ ] ={1,3,6,1,2,1,4,31,1,1,32};
UINT4 IpSystemStatsHCOutOctets [ ] ={1,3,6,1,2,1,4,31,1,1,33};
UINT4 IpSystemStatsInMcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,34};
UINT4 IpSystemStatsHCInMcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,35};
UINT4 IpSystemStatsInMcastOctets [ ] ={1,3,6,1,2,1,4,31,1,1,36};
UINT4 IpSystemStatsHCInMcastOctets [ ] ={1,3,6,1,2,1,4,31,1,1,37};
UINT4 IpSystemStatsOutMcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,38};
UINT4 IpSystemStatsHCOutMcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,39};
UINT4 IpSystemStatsOutMcastOctets [ ] ={1,3,6,1,2,1,4,31,1,1,40};
UINT4 IpSystemStatsHCOutMcastOctets [ ] ={1,3,6,1,2,1,4,31,1,1,41};
UINT4 IpSystemStatsInBcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,42};
UINT4 IpSystemStatsHCInBcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,43};
UINT4 IpSystemStatsOutBcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,44};
UINT4 IpSystemStatsHCOutBcastPkts [ ] ={1,3,6,1,2,1,4,31,1,1,45};
UINT4 IpSystemStatsDiscontinuityTime [ ] ={1,3,6,1,2,1,4,31,1,1,46};
UINT4 IpSystemStatsRefreshRate [ ] ={1,3,6,1,2,1,4,31,1,1,47};
UINT4 IpIfStatsTableLastChange [ ] ={1,3,6,1,2,1,4,31,2};
UINT4 IpIfStatsIPVersion [ ] ={1,3,6,1,2,1,4,31,3,1,1};
UINT4 IpIfStatsIfIndex [ ] ={1,3,6,1,2,1,4,31,3,1,2};
UINT4 IpIfStatsInReceives [ ] ={1,3,6,1,2,1,4,31,3,1,3};
UINT4 IpIfStatsHCInReceives [ ] ={1,3,6,1,2,1,4,31,3,1,4};
UINT4 IpIfStatsInOctets [ ] ={1,3,6,1,2,1,4,31,3,1,5};
UINT4 IpIfStatsHCInOctets [ ] ={1,3,6,1,2,1,4,31,3,1,6};
UINT4 IpIfStatsInHdrErrors [ ] ={1,3,6,1,2,1,4,31,3,1,7};
UINT4 IpIfStatsInNoRoutes [ ] ={1,3,6,1,2,1,4,31,3,1,8};
UINT4 IpIfStatsInAddrErrors [ ] ={1,3,6,1,2,1,4,31,3,1,9};
UINT4 IpIfStatsInUnknownProtos [ ] ={1,3,6,1,2,1,4,31,3,1,10};
UINT4 IpIfStatsInTruncatedPkts [ ] ={1,3,6,1,2,1,4,31,3,1,11};
UINT4 IpIfStatsInForwDatagrams [ ] ={1,3,6,1,2,1,4,31,3,1,12};
UINT4 IpIfStatsHCInForwDatagrams [ ] ={1,3,6,1,2,1,4,31,3,1,13};
UINT4 IpIfStatsReasmReqds [ ] ={1,3,6,1,2,1,4,31,3,1,14};
UINT4 IpIfStatsReasmOKs [ ] ={1,3,6,1,2,1,4,31,3,1,15};
UINT4 IpIfStatsReasmFails [ ] ={1,3,6,1,2,1,4,31,3,1,16};
UINT4 IpIfStatsInDiscards [ ] ={1,3,6,1,2,1,4,31,3,1,17};
UINT4 IpIfStatsInDelivers [ ] ={1,3,6,1,2,1,4,31,3,1,18};
UINT4 IpIfStatsHCInDelivers [ ] ={1,3,6,1,2,1,4,31,3,1,19};
UINT4 IpIfStatsOutRequests [ ] ={1,3,6,1,2,1,4,31,3,1,20};
UINT4 IpIfStatsHCOutRequests [ ] ={1,3,6,1,2,1,4,31,3,1,21};
UINT4 IpIfStatsOutForwDatagrams [ ] ={1,3,6,1,2,1,4,31,3,1,23};
UINT4 IpIfStatsHCOutForwDatagrams [ ] ={1,3,6,1,2,1,4,31,3,1,24};
UINT4 IpIfStatsOutDiscards [ ] ={1,3,6,1,2,1,4,31,3,1,25};
UINT4 IpIfStatsOutFragReqds [ ] ={1,3,6,1,2,1,4,31,3,1,26};
UINT4 IpIfStatsOutFragOKs [ ] ={1,3,6,1,2,1,4,31,3,1,27};
UINT4 IpIfStatsOutFragFails [ ] ={1,3,6,1,2,1,4,31,3,1,28};
UINT4 IpIfStatsOutFragCreates [ ] ={1,3,6,1,2,1,4,31,3,1,29};
UINT4 IpIfStatsOutTransmits [ ] ={1,3,6,1,2,1,4,31,3,1,30};
UINT4 IpIfStatsHCOutTransmits [ ] ={1,3,6,1,2,1,4,31,3,1,31};
UINT4 IpIfStatsOutOctets [ ] ={1,3,6,1,2,1,4,31,3,1,32};
UINT4 IpIfStatsHCOutOctets [ ] ={1,3,6,1,2,1,4,31,3,1,33};
UINT4 IpIfStatsInMcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,34};
UINT4 IpIfStatsHCInMcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,35};
UINT4 IpIfStatsInMcastOctets [ ] ={1,3,6,1,2,1,4,31,3,1,36};
UINT4 IpIfStatsHCInMcastOctets [ ] ={1,3,6,1,2,1,4,31,3,1,37};
UINT4 IpIfStatsOutMcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,38};
UINT4 IpIfStatsHCOutMcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,39};
UINT4 IpIfStatsOutMcastOctets [ ] ={1,3,6,1,2,1,4,31,3,1,40};
UINT4 IpIfStatsHCOutMcastOctets [ ] ={1,3,6,1,2,1,4,31,3,1,41};
UINT4 IpIfStatsInBcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,42};
UINT4 IpIfStatsHCInBcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,43};
UINT4 IpIfStatsOutBcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,44};
UINT4 IpIfStatsHCOutBcastPkts [ ] ={1,3,6,1,2,1,4,31,3,1,45};
UINT4 IpIfStatsDiscontinuityTime [ ] ={1,3,6,1,2,1,4,31,3,1,46};
UINT4 IpIfStatsRefreshRate [ ] ={1,3,6,1,2,1,4,31,3,1,47};
UINT4 IpAddressPrefixIfIndex [ ] ={1,3,6,1,2,1,4,32,1,1};
UINT4 IpAddressPrefixType [ ] ={1,3,6,1,2,1,4,32,1,2};
UINT4 IpAddressPrefixPrefix [ ] ={1,3,6,1,2,1,4,32,1,3};
UINT4 IpAddressPrefixLength [ ] ={1,3,6,1,2,1,4,32,1,4};
UINT4 IpAddressPrefixOrigin [ ] ={1,3,6,1,2,1,4,32,1,5};
UINT4 IpAddressPrefixOnLinkFlag [ ] ={1,3,6,1,2,1,4,32,1,6};
UINT4 IpAddressPrefixAutonomousFlag [ ] ={1,3,6,1,2,1,4,32,1,7};
UINT4 IpAddressPrefixAdvPreferredLifetime [ ] ={1,3,6,1,2,1,4,32,1,8};
UINT4 IpAddressPrefixAdvValidLifetime [ ] ={1,3,6,1,2,1,4,32,1,9};
UINT4 IpAddressSpinLock [ ] ={1,3,6,1,2,1,4,33};
UINT4 IpAddressAddrType [ ] ={1,3,6,1,2,1,4,34,1,1};
UINT4 IpAddressAddr [ ] ={1,3,6,1,2,1,4,34,1,2};
UINT4 IpAddressIfIndex [ ] ={1,3,6,1,2,1,4,34,1,3};
UINT4 IpAddressType [ ] ={1,3,6,1,2,1,4,34,1,4};
UINT4 IpAddressPrefix [ ] ={1,3,6,1,2,1,4,34,1,5};
UINT4 IpAddressOrigin [ ] ={1,3,6,1,2,1,4,34,1,6};
UINT4 IpAddressStatus [ ] ={1,3,6,1,2,1,4,34,1,7};
UINT4 IpAddressCreated [ ] ={1,3,6,1,2,1,4,34,1,8};
UINT4 IpAddressLastChanged [ ] ={1,3,6,1,2,1,4,34,1,9};
UINT4 IpAddressRowStatus [ ] ={1,3,6,1,2,1,4,34,1,10};
UINT4 IpAddressStorageType [ ] ={1,3,6,1,2,1,4,34,1,11};
UINT4 IpNetToPhysicalIfIndex [ ] ={1,3,6,1,2,1,4,35,1,1};
UINT4 IpNetToPhysicalNetAddressType [ ] ={1,3,6,1,2,1,4,35,1,2};
UINT4 IpNetToPhysicalNetAddress [ ] ={1,3,6,1,2,1,4,35,1,3};
UINT4 IpNetToPhysicalPhysAddress [ ] ={1,3,6,1,2,1,4,35,1,4};
UINT4 IpNetToPhysicalLastUpdated [ ] ={1,3,6,1,2,1,4,35,1,5};
UINT4 IpNetToPhysicalType [ ] ={1,3,6,1,2,1,4,35,1,6};
UINT4 IpNetToPhysicalState [ ] ={1,3,6,1,2,1,4,35,1,7};
UINT4 IpNetToPhysicalRowStatus [ ] ={1,3,6,1,2,1,4,35,1,8};
UINT4 Ipv6ScopeZoneIndexIfIndex [ ] ={1,3,6,1,2,1,4,36,1,1};
UINT4 Ipv6ScopeZoneIndexLinkLocal [ ] ={1,3,6,1,2,1,4,36,1,2};
UINT4 Ipv6ScopeZoneIndex3 [ ] ={1,3,6,1,2,1,4,36,1,3};
UINT4 Ipv6ScopeZoneIndexAdminLocal [ ] ={1,3,6,1,2,1,4,36,1,4};
UINT4 Ipv6ScopeZoneIndexSiteLocal [ ] ={1,3,6,1,2,1,4,36,1,5};
UINT4 Ipv6ScopeZoneIndex6 [ ] ={1,3,6,1,2,1,4,36,1,6};
UINT4 Ipv6ScopeZoneIndex7 [ ] ={1,3,6,1,2,1,4,36,1,7};
UINT4 Ipv6ScopeZoneIndexOrganizationLocal [ ] ={1,3,6,1,2,1,4,36,1,8};
UINT4 Ipv6ScopeZoneIndex9 [ ] ={1,3,6,1,2,1,4,36,1,9};
UINT4 Ipv6ScopeZoneIndexA [ ] ={1,3,6,1,2,1,4,36,1,10};
UINT4 Ipv6ScopeZoneIndexB [ ] ={1,3,6,1,2,1,4,36,1,11};
UINT4 Ipv6ScopeZoneIndexC [ ] ={1,3,6,1,2,1,4,36,1,12};
UINT4 Ipv6ScopeZoneIndexD [ ] ={1,3,6,1,2,1,4,36,1,13};
UINT4 IpDefaultRouterAddressType [ ] ={1,3,6,1,2,1,4,37,1,1};
UINT4 IpDefaultRouterAddress [ ] ={1,3,6,1,2,1,4,37,1,2};
UINT4 IpDefaultRouterIfIndex [ ] ={1,3,6,1,2,1,4,37,1,3};
UINT4 IpDefaultRouterLifetime [ ] ={1,3,6,1,2,1,4,37,1,4};
UINT4 IpDefaultRouterPreference [ ] ={1,3,6,1,2,1,4,37,1,5};
UINT4 Ipv6RouterAdvertSpinLock [ ] ={1,3,6,1,2,1,4,38};
UINT4 Ipv6RouterAdvertIfIndex [ ] ={1,3,6,1,2,1,4,39,1,1};
UINT4 Ipv6RouterAdvertSendAdverts [ ] ={1,3,6,1,2,1,4,39,1,2};
UINT4 Ipv6RouterAdvertMaxInterval [ ] ={1,3,6,1,2,1,4,39,1,3};
UINT4 Ipv6RouterAdvertMinInterval [ ] ={1,3,6,1,2,1,4,39,1,4};
UINT4 Ipv6RouterAdvertManagedFlag [ ] ={1,3,6,1,2,1,4,39,1,5};
UINT4 Ipv6RouterAdvertOtherConfigFlag [ ] ={1,3,6,1,2,1,4,39,1,6};
UINT4 Ipv6RouterAdvertLinkMTU [ ] ={1,3,6,1,2,1,4,39,1,7};
UINT4 Ipv6RouterAdvertReachableTime [ ] ={1,3,6,1,2,1,4,39,1,8};
UINT4 Ipv6RouterAdvertRetransmitTime [ ] ={1,3,6,1,2,1,4,39,1,9};
UINT4 Ipv6RouterAdvertCurHopLimit [ ] ={1,3,6,1,2,1,4,39,1,10};
UINT4 Ipv6RouterAdvertDefaultLifetime [ ] ={1,3,6,1,2,1,4,39,1,11};
UINT4 Ipv6RouterAdvertRowStatus [ ] ={1,3,6,1,2,1,4,39,1,12};
UINT4 IcmpStatsIPVersion [ ] ={1,3,6,1,2,1,5,29,1,1};
UINT4 IcmpStatsInMsgs [ ] ={1,3,6,1,2,1,5,29,1,2};
UINT4 IcmpStatsInErrors [ ] ={1,3,6,1,2,1,5,29,1,3};
UINT4 IcmpStatsOutMsgs [ ] ={1,3,6,1,2,1,5,29,1,4};
UINT4 IcmpStatsOutErrors [ ] ={1,3,6,1,2,1,5,29,1,5};
UINT4 IcmpMsgStatsIPVersion [ ] ={1,3,6,1,2,1,5,30,1,1};
UINT4 IcmpMsgStatsType [ ] ={1,3,6,1,2,1,5,30,1,2};
UINT4 IcmpMsgStatsInPkts [ ] ={1,3,6,1,2,1,5,30,1,3};
UINT4 IcmpMsgStatsOutPkts [ ] ={1,3,6,1,2,1,5,30,1,4};
UINT4 IpInReceives [ ] ={1,3,6,1,2,1,4,3};
UINT4 IpInHdrErrors [ ] ={1,3,6,1,2,1,4,4};
UINT4 IpInAddrErrors [ ] ={1,3,6,1,2,1,4,5};
UINT4 IpForwDatagrams [ ] ={1,3,6,1,2,1,4,6};
UINT4 IpInUnknownProtos [ ] ={1,3,6,1,2,1,4,7};
UINT4 IpInDiscards [ ] ={1,3,6,1,2,1,4,8};
UINT4 IpInDelivers [ ] ={1,3,6,1,2,1,4,9};
UINT4 IpOutRequests [ ] ={1,3,6,1,2,1,4,10};
UINT4 IpOutDiscards [ ] ={1,3,6,1,2,1,4,11};
UINT4 IpOutNoRoutes [ ] ={1,3,6,1,2,1,4,12};
UINT4 IpReasmReqds [ ] ={1,3,6,1,2,1,4,14};
UINT4 IpReasmOKs [ ] ={1,3,6,1,2,1,4,15};
UINT4 IpReasmFails [ ] ={1,3,6,1,2,1,4,16};
UINT4 IpFragOKs [ ] ={1,3,6,1,2,1,4,17};
UINT4 IpFragFails [ ] ={1,3,6,1,2,1,4,18};
UINT4 IpFragCreates [ ] ={1,3,6,1,2,1,4,19};
UINT4 IpRoutingDiscards [ ] ={1,3,6,1,2,1,4,23};
UINT4 IpAdEntAddr [ ] ={1,3,6,1,2,1,4,20,1,1};
UINT4 IpAdEntIfIndex [ ] ={1,3,6,1,2,1,4,20,1,2};
UINT4 IpAdEntNetMask [ ] ={1,3,6,1,2,1,4,20,1,3};
UINT4 IpAdEntBcastAddr [ ] ={1,3,6,1,2,1,4,20,1,4};
UINT4 IpAdEntReasmMaxSize [ ] ={1,3,6,1,2,1,4,20,1,5};
UINT4 IpNetToMediaIfIndex [ ] ={1,3,6,1,2,1,4,22,1,1};
UINT4 IpNetToMediaPhysAddress [ ] ={1,3,6,1,2,1,4,22,1,2};
UINT4 IpNetToMediaNetAddress [ ] ={1,3,6,1,2,1,4,22,1,3};
UINT4 IpNetToMediaType [ ] ={1,3,6,1,2,1,4,22,1,4};
UINT4 IcmpInMsgs [ ] ={1,3,6,1,2,1,5,1};
UINT4 IcmpInErrors [ ] ={1,3,6,1,2,1,5,2};
UINT4 IcmpInDestUnreachs [ ] ={1,3,6,1,2,1,5,3};
UINT4 IcmpInTimeExcds [ ] ={1,3,6,1,2,1,5,4};
UINT4 IcmpInParmProbs [ ] ={1,3,6,1,2,1,5,5};
UINT4 IcmpInSrcQuenchs [ ] ={1,3,6,1,2,1,5,6};
UINT4 IcmpInRedirects [ ] ={1,3,6,1,2,1,5,7};
UINT4 IcmpInEchos [ ] ={1,3,6,1,2,1,5,8};
UINT4 IcmpInEchoReps [ ] ={1,3,6,1,2,1,5,9};
UINT4 IcmpInTimestamps [ ] ={1,3,6,1,2,1,5,10};
UINT4 IcmpInTimestampReps [ ] ={1,3,6,1,2,1,5,11};
UINT4 IcmpInAddrMasks [ ] ={1,3,6,1,2,1,5,12};
UINT4 IcmpInAddrMaskReps [ ] ={1,3,6,1,2,1,5,13};
UINT4 IcmpOutMsgs [ ] ={1,3,6,1,2,1,5,14};
UINT4 IcmpOutErrors [ ] ={1,3,6,1,2,1,5,15};
UINT4 IcmpOutDestUnreachs [ ] ={1,3,6,1,2,1,5,16};
UINT4 IcmpOutTimeExcds [ ] ={1,3,6,1,2,1,5,17};
UINT4 IcmpOutParmProbs [ ] ={1,3,6,1,2,1,5,18};
UINT4 IcmpOutSrcQuenchs [ ] ={1,3,6,1,2,1,5,19};
UINT4 IcmpOutRedirects [ ] ={1,3,6,1,2,1,5,20};
UINT4 IcmpOutEchos [ ] ={1,3,6,1,2,1,5,21};
UINT4 IcmpOutEchoReps [ ] ={1,3,6,1,2,1,5,22};
UINT4 IcmpOutTimestamps [ ] ={1,3,6,1,2,1,5,23};
UINT4 IcmpOutTimestampReps [ ] ={1,3,6,1,2,1,5,24};
UINT4 IcmpOutAddrMasks [ ] ={1,3,6,1,2,1,5,25};
UINT4 IcmpOutAddrMaskReps [ ] ={1,3,6,1,2,1,5,26};
UINT4 InetCidrRouteNumber [ ] ={1,3,6,1,2,1,4,24,6};
UINT4 InetCidrRouteDiscards [ ] ={1,3,6,1,2,1,4,24,8};
UINT4 InetCidrRouteDestType [ ] ={1,3,6,1,2,1,4,24,7,1,1};
UINT4 InetCidrRouteDest [ ] ={1,3,6,1,2,1,4,24,7,1,2};
UINT4 InetCidrRoutePfxLen [ ] ={1,3,6,1,2,1,4,24,7,1,3};
UINT4 InetCidrRoutePolicy [ ] ={1,3,6,1,2,1,4,24,7,1,4};
UINT4 InetCidrRouteNextHopType [ ] ={1,3,6,1,2,1,4,24,7,1,5};
UINT4 InetCidrRouteNextHop [ ] ={1,3,6,1,2,1,4,24,7,1,6};
UINT4 InetCidrRouteIfIndex [ ] ={1,3,6,1,2,1,4,24,7,1,7};
UINT4 InetCidrRouteType [ ] ={1,3,6,1,2,1,4,24,7,1,8};
UINT4 InetCidrRouteProto [ ] ={1,3,6,1,2,1,4,24,7,1,9};
UINT4 InetCidrRouteAge [ ] ={1,3,6,1,2,1,4,24,7,1,10};
UINT4 InetCidrRouteNextHopAS [ ] ={1,3,6,1,2,1,4,24,7,1,11};
UINT4 InetCidrRouteMetric1 [ ] ={1,3,6,1,2,1,4,24,7,1,12};
UINT4 InetCidrRouteMetric2 [ ] ={1,3,6,1,2,1,4,24,7,1,13};
UINT4 InetCidrRouteMetric3 [ ] ={1,3,6,1,2,1,4,24,7,1,14};
UINT4 InetCidrRouteMetric4 [ ] ={1,3,6,1,2,1,4,24,7,1,15};
UINT4 InetCidrRouteMetric5 [ ] ={1,3,6,1,2,1,4,24,7,1,16};
UINT4 InetCidrRouteStatus [ ] ={1,3,6,1,2,1,4,24,7,1,17};
UINT4 IpCidrRouteNumber [ ] ={1,3,6,1,2,1,4,24,3};
UINT4 IpCidrRouteDest [ ] ={1,3,6,1,2,1,4,24,4,1,1};
UINT4 IpCidrRouteMask [ ] ={1,3,6,1,2,1,4,24,4,1,2};
UINT4 IpCidrRouteTos [ ] ={1,3,6,1,2,1,4,24,4,1,3};
UINT4 IpCidrRouteNextHop [ ] ={1,3,6,1,2,1,4,24,4,1,4};
UINT4 IpCidrRouteIfIndex [ ] ={1,3,6,1,2,1,4,24,4,1,5};
UINT4 IpCidrRouteType [ ] ={1,3,6,1,2,1,4,24,4,1,6};
UINT4 IpCidrRouteProto [ ] ={1,3,6,1,2,1,4,24,4,1,7};
UINT4 IpCidrRouteAge [ ] ={1,3,6,1,2,1,4,24,4,1,8};
UINT4 IpCidrRouteInfo [ ] ={1,3,6,1,2,1,4,24,4,1,9};
UINT4 IpCidrRouteNextHopAS [ ] ={1,3,6,1,2,1,4,24,4,1,10};
UINT4 IpCidrRouteMetric1 [ ] ={1,3,6,1,2,1,4,24,4,1,11};
UINT4 IpCidrRouteMetric2 [ ] ={1,3,6,1,2,1,4,24,4,1,12};
UINT4 IpCidrRouteMetric3 [ ] ={1,3,6,1,2,1,4,24,4,1,13};
UINT4 IpCidrRouteMetric4 [ ] ={1,3,6,1,2,1,4,24,4,1,14};
UINT4 IpCidrRouteMetric5 [ ] ={1,3,6,1,2,1,4,24,4,1,15};
UINT4 IpCidrRouteStatus [ ] ={1,3,6,1,2,1,4,24,4,1,16};

tMbDbEntry stdipvMibEntry[]= {

{{8,IpForwarding}, NULL, IpForwardingGet, IpForwardingSet, IpForwardingTest, IpForwardingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,IpDefaultTTL}, NULL, IpDefaultTTLGet, IpDefaultTTLSet, IpDefaultTTLTest, IpDefaultTTLDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,IpInReceives}, NULL, IpInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInHdrErrors}, NULL, IpInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInAddrErrors}, NULL, IpInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpForwDatagrams}, NULL, IpForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInUnknownProtos}, NULL, IpInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInDiscards}, NULL, IpInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInDelivers}, NULL, IpInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutRequests}, NULL, IpOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutDiscards}, NULL, IpOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutNoRoutes}, NULL, IpOutNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmTimeout}, NULL, IpReasmTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmReqds}, NULL, IpReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmOKs}, NULL, IpReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmFails}, NULL, IpReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragOKs}, NULL, IpFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragFails}, NULL, IpFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragCreates}, NULL, IpFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,IpAdEntAddr}, GetNextIndexIpAddrTable, IpAdEntAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpAddrTableINDEX, 1, 1, 0, NULL},

{{10,IpAdEntIfIndex}, GetNextIndexIpAddrTable, IpAdEntIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddrTableINDEX, 1, 1, 0, NULL},

{{10,IpAdEntNetMask}, GetNextIndexIpAddrTable, IpAdEntNetMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpAddrTableINDEX, 1, 1, 0, NULL},

{{10,IpAdEntBcastAddr}, GetNextIndexIpAddrTable, IpAdEntBcastAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddrTableINDEX, 1, 1, 0, NULL},

{{10,IpAdEntReasmMaxSize}, GetNextIndexIpAddrTable, IpAdEntReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddrTableINDEX, 1, 1, 0, NULL},

{{10,IpNetToMediaIfIndex}, GetNextIndexIpNetToMediaTable, IpNetToMediaIfIndexGet, IpNetToMediaIfIndexSet, IpNetToMediaIfIndexTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 1, 0, NULL},

{{10,IpNetToMediaPhysAddress}, GetNextIndexIpNetToMediaTable, IpNetToMediaPhysAddressGet, IpNetToMediaPhysAddressSet, IpNetToMediaPhysAddressTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 1, 0, NULL},

{{10,IpNetToMediaNetAddress}, GetNextIndexIpNetToMediaTable, IpNetToMediaNetAddressGet, IpNetToMediaNetAddressSet, IpNetToMediaNetAddressTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 1, 0, NULL},

{{10,IpNetToMediaType}, GetNextIndexIpNetToMediaTable, IpNetToMediaTypeGet, IpNetToMediaTypeSet, IpNetToMediaTypeTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 1, 0, NULL},

{{8,IpRoutingDiscards}, NULL, IpRoutingDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,IpCidrRouteNumber}, NULL, IpCidrRouteNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,IpCidrRouteDest}, GetNextIndexIpCidrRouteTable, IpCidrRouteDestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteMask}, GetNextIndexIpCidrRouteTable, IpCidrRouteMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteTos}, GetNextIndexIpCidrRouteTable, IpCidrRouteTosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteNextHop}, GetNextIndexIpCidrRouteTable, IpCidrRouteNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteIfIndex}, GetNextIndexIpCidrRouteTable, IpCidrRouteIfIndexGet, IpCidrRouteIfIndexSet, IpCidrRouteIfIndexTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "0"},

{{11,IpCidrRouteType}, GetNextIndexIpCidrRouteTable, IpCidrRouteTypeGet, IpCidrRouteTypeSet, IpCidrRouteTypeTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteProto}, GetNextIndexIpCidrRouteTable, IpCidrRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteAge}, GetNextIndexIpCidrRouteTable, IpCidrRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 1, 0, "0"},

{{11,IpCidrRouteInfo}, GetNextIndexIpCidrRouteTable, IpCidrRouteInfoGet, IpCidrRouteInfoSet, IpCidrRouteInfoTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, NULL},

{{11,IpCidrRouteNextHopAS}, GetNextIndexIpCidrRouteTable, IpCidrRouteNextHopASGet, IpCidrRouteNextHopASSet, IpCidrRouteNextHopASTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "0"},

{{11,IpCidrRouteMetric1}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric1Get, IpCidrRouteMetric1Set, IpCidrRouteMetric1Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "-1"},

{{11,IpCidrRouteMetric2}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric2Get, IpCidrRouteMetric2Set, IpCidrRouteMetric2Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "-1"},

{{11,IpCidrRouteMetric3}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric3Get, IpCidrRouteMetric3Set, IpCidrRouteMetric3Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "-1"},

{{11,IpCidrRouteMetric4}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric4Get, IpCidrRouteMetric4Set, IpCidrRouteMetric4Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "-1"},

{{11,IpCidrRouteMetric5}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric5Get, IpCidrRouteMetric5Set, IpCidrRouteMetric5Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 0, "-1"},

{{11,IpCidrRouteStatus}, GetNextIndexIpCidrRouteTable, IpCidrRouteStatusGet, IpCidrRouteStatusSet, IpCidrRouteStatusTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 1, 1, NULL},

{{9,InetCidrRouteNumber}, NULL, InetCidrRouteNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,InetCidrRouteDestType}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteDest}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRoutePfxLen}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRoutePolicy}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteNextHopType}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteNextHop}, GetNextIndexInetCidrRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteIfIndex}, GetNextIndexInetCidrRouteTable, InetCidrRouteIfIndexGet, InetCidrRouteIfIndexSet, InetCidrRouteIfIndexTest, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteType}, GetNextIndexInetCidrRouteTable, InetCidrRouteTypeGet, InetCidrRouteTypeSet, InetCidrRouteTypeTest, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteProto}, GetNextIndexInetCidrRouteTable, InetCidrRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteAge}, GetNextIndexInetCidrRouteTable, InetCidrRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, InetCidrRouteTableINDEX, 6, 0, 0, NULL},

{{11,InetCidrRouteNextHopAS}, GetNextIndexInetCidrRouteTable, InetCidrRouteNextHopASGet, InetCidrRouteNextHopASSet, InetCidrRouteNextHopASTest, InetCidrRouteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "0"},

{{11,InetCidrRouteMetric1}, GetNextIndexInetCidrRouteTable, InetCidrRouteMetric1Get, InetCidrRouteMetric1Set, InetCidrRouteMetric1Test, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "-1"},

{{11,InetCidrRouteMetric2}, GetNextIndexInetCidrRouteTable, InetCidrRouteMetric2Get, InetCidrRouteMetric2Set, InetCidrRouteMetric2Test, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "-1"},

{{11,InetCidrRouteMetric3}, GetNextIndexInetCidrRouteTable, InetCidrRouteMetric3Get, InetCidrRouteMetric3Set, InetCidrRouteMetric3Test, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "-1"},

{{11,InetCidrRouteMetric4}, GetNextIndexInetCidrRouteTable, InetCidrRouteMetric4Get, InetCidrRouteMetric4Set, InetCidrRouteMetric4Test, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "-1"},

{{11,InetCidrRouteMetric5}, GetNextIndexInetCidrRouteTable, InetCidrRouteMetric5Get, InetCidrRouteMetric5Set, InetCidrRouteMetric5Test, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 0, "-1"},

{{11,InetCidrRouteStatus}, GetNextIndexInetCidrRouteTable, InetCidrRouteStatusGet, InetCidrRouteStatusSet, InetCidrRouteStatusTest, InetCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, InetCidrRouteTableINDEX, 6, 0, 1, NULL},

{{9,InetCidrRouteDiscards}, NULL, InetCidrRouteDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,Ipv6IpForwarding}, NULL, Ipv6IpForwardingGet, Ipv6IpForwardingSet, Ipv6IpForwardingTest, Ipv6IpForwardingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,Ipv6IpDefaultHopLimit}, NULL, Ipv6IpDefaultHopLimitGet, Ipv6IpDefaultHopLimitSet, Ipv6IpDefaultHopLimitTest, Ipv6IpDefaultHopLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,Ipv4InterfaceTableLastChange}, NULL, Ipv4InterfaceTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Ipv4InterfaceIfIndex}, GetNextIndexIpv4InterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv4InterfaceReasmMaxSize}, GetNextIndexIpv4InterfaceTable, Ipv4InterfaceReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ipv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv4InterfaceEnableStatus}, GetNextIndexIpv4InterfaceTable, Ipv4InterfaceEnableStatusGet, Ipv4InterfaceEnableStatusSet, Ipv4InterfaceEnableStatusTest, Ipv4InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv4InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv4InterfaceRetransmitTime}, GetNextIndexIpv4InterfaceTable, Ipv4InterfaceRetransmitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv4InterfaceTableINDEX, 1, 0, 0, "1000"},

{{8,Ipv6InterfaceTableLastChange}, NULL, Ipv6InterfaceTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Ipv6InterfaceIfIndex}, GetNextIndexIpv6InterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceReasmMaxSize}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceIdentifier}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceEnableStatus}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceEnableStatusGet, Ipv6InterfaceEnableStatusSet, Ipv6InterfaceEnableStatusTest, Ipv6InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceReachableTime}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceReachableTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceRetransmitTime}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceRetransmitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6InterfaceForwarding}, GetNextIndexIpv6InterfaceTable, Ipv6InterfaceForwardingGet, Ipv6InterfaceForwardingSet, Ipv6InterfaceForwardingTest, Ipv6InterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6InterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsIPVersion}, GetNextIndexIpSystemStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInReceives}, GetNextIndexIpSystemStatsTable, IpSystemStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInReceives}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInHdrErrors}, GetNextIndexIpSystemStatsTable, IpSystemStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInNoRoutes}, GetNextIndexIpSystemStatsTable, IpSystemStatsInNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInAddrErrors}, GetNextIndexIpSystemStatsTable, IpSystemStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInUnknownProtos}, GetNextIndexIpSystemStatsTable, IpSystemStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInTruncatedPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInForwDatagrams}, GetNextIndexIpSystemStatsTable, IpSystemStatsInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInForwDatagrams}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsReasmReqds}, GetNextIndexIpSystemStatsTable, IpSystemStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsReasmOKs}, GetNextIndexIpSystemStatsTable, IpSystemStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsReasmFails}, GetNextIndexIpSystemStatsTable, IpSystemStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInDiscards}, GetNextIndexIpSystemStatsTable, IpSystemStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInDelivers}, GetNextIndexIpSystemStatsTable, IpSystemStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInDelivers}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutRequests}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutRequests}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutNoRoutes}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutForwDatagrams}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutForwDatagrams}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutDiscards}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutFragReqds}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutFragReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutFragOKs}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutFragFails}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutFragCreates}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutTransmits}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutTransmits}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInMcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInMcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInMcastOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInMcastOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutMcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutMcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutMcastOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutMcastOctets}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsInBcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCInBcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsOutBcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsHCOutBcastPkts}, GetNextIndexIpSystemStatsTable, IpSystemStatsHCOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsDiscontinuityTime}, GetNextIndexIpSystemStatsTable, IpSystemStatsDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{11,IpSystemStatsRefreshRate}, GetNextIndexIpSystemStatsTable, IpSystemStatsRefreshRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IpSystemStatsTableINDEX, 1, 0, 0, NULL},

{{9,IpIfStatsTableLastChange}, NULL, IpIfStatsTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,IpIfStatsIPVersion}, GetNextIndexIpIfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsIfIndex}, GetNextIndexIpIfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInReceives}, GetNextIndexIpIfStatsTable, IpIfStatsInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInReceives}, GetNextIndexIpIfStatsTable, IpIfStatsHCInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInOctets}, GetNextIndexIpIfStatsTable, IpIfStatsInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInOctets}, GetNextIndexIpIfStatsTable, IpIfStatsHCInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInHdrErrors}, GetNextIndexIpIfStatsTable, IpIfStatsInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInNoRoutes}, GetNextIndexIpIfStatsTable, IpIfStatsInNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInAddrErrors}, GetNextIndexIpIfStatsTable, IpIfStatsInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInUnknownProtos}, GetNextIndexIpIfStatsTable, IpIfStatsInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInTruncatedPkts}, GetNextIndexIpIfStatsTable, IpIfStatsInTruncatedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInForwDatagrams}, GetNextIndexIpIfStatsTable, IpIfStatsInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInForwDatagrams}, GetNextIndexIpIfStatsTable, IpIfStatsHCInForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsReasmReqds}, GetNextIndexIpIfStatsTable, IpIfStatsReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsReasmOKs}, GetNextIndexIpIfStatsTable, IpIfStatsReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsReasmFails}, GetNextIndexIpIfStatsTable, IpIfStatsReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInDiscards}, GetNextIndexIpIfStatsTable, IpIfStatsInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInDelivers}, GetNextIndexIpIfStatsTable, IpIfStatsInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInDelivers}, GetNextIndexIpIfStatsTable, IpIfStatsHCInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutRequests}, GetNextIndexIpIfStatsTable, IpIfStatsOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutRequests}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutForwDatagrams}, GetNextIndexIpIfStatsTable, IpIfStatsOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutForwDatagrams}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutDiscards}, GetNextIndexIpIfStatsTable, IpIfStatsOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutFragReqds}, GetNextIndexIpIfStatsTable, IpIfStatsOutFragReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutFragOKs}, GetNextIndexIpIfStatsTable, IpIfStatsOutFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutFragFails}, GetNextIndexIpIfStatsTable, IpIfStatsOutFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutFragCreates}, GetNextIndexIpIfStatsTable, IpIfStatsOutFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutTransmits}, GetNextIndexIpIfStatsTable, IpIfStatsOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutTransmits}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutTransmitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutOctets}, GetNextIndexIpIfStatsTable, IpIfStatsOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutOctets}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInMcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInMcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsHCInMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInMcastOctets}, GetNextIndexIpIfStatsTable, IpIfStatsInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInMcastOctets}, GetNextIndexIpIfStatsTable, IpIfStatsHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutMcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutMcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutMcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutMcastOctets}, GetNextIndexIpIfStatsTable, IpIfStatsOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutMcastOctets}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsInBcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCInBcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsHCInBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsOutBcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsHCOutBcastPkts}, GetNextIndexIpIfStatsTable, IpIfStatsHCOutBcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsDiscontinuityTime}, GetNextIndexIpIfStatsTable, IpIfStatsDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{11,IpIfStatsRefreshRate}, GetNextIndexIpIfStatsTable, IpIfStatsRefreshRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IpIfStatsTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressPrefixIfIndex}, GetNextIndexIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixType}, GetNextIndexIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixPrefix}, GetNextIndexIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixLength}, GetNextIndexIpAddressPrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixOrigin}, GetNextIndexIpAddressPrefixTable, IpAddressPrefixOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixOnLinkFlag}, GetNextIndexIpAddressPrefixTable, IpAddressPrefixOnLinkFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixAutonomousFlag}, GetNextIndexIpAddressPrefixTable, IpAddressPrefixAutonomousFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixAdvPreferredLifetime}, GetNextIndexIpAddressPrefixTable, IpAddressPrefixAdvPreferredLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{10,IpAddressPrefixAdvValidLifetime}, GetNextIndexIpAddressPrefixTable, IpAddressPrefixAdvValidLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IpAddressPrefixTableINDEX, 4, 0, 0, NULL},

{{8,IpAddressSpinLock}, NULL, IpAddressSpinLockGet, IpAddressSpinLockSet, IpAddressSpinLockTest, IpAddressSpinLockDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,IpAddressAddrType}, GetNextIndexIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressAddr}, GetNextIndexIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressIfIndex}, GetNextIndexIpAddressTable, IpAddressIfIndexGet, IpAddressIfIndexSet, IpAddressIfIndexTest, IpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressType}, GetNextIndexIpAddressTable, IpAddressTypeGet, IpAddressTypeSet, IpAddressTypeTest, IpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpAddressTableINDEX, 2, 0, 0, "1"},

{{10,IpAddressPrefix}, GetNextIndexIpAddressTable, IpAddressPrefixGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressOrigin}, GetNextIndexIpAddressTable, IpAddressOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressStatus}, GetNextIndexIpAddressTable, IpAddressStatusGet, IpAddressStatusSet, IpAddressStatusTest, IpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpAddressTableINDEX, 2, 0, 0, "1"},

{{10,IpAddressCreated}, GetNextIndexIpAddressTable, IpAddressCreatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressLastChanged}, GetNextIndexIpAddressTable, IpAddressLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpAddressTableINDEX, 2, 0, 0, NULL},

{{10,IpAddressRowStatus}, GetNextIndexIpAddressTable, IpAddressRowStatusGet, IpAddressRowStatusSet, IpAddressRowStatusTest, IpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpAddressTableINDEX, 2, 0, 1, NULL},

{{10,IpAddressStorageType}, GetNextIndexIpAddressTable, IpAddressStorageTypeGet, IpAddressStorageTypeSet, IpAddressStorageTypeTest, IpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpAddressTableINDEX, 2, 0, 0, "2"},

{{10,IpNetToPhysicalIfIndex}, GetNextIndexIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalNetAddressType}, GetNextIndexIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalNetAddress}, GetNextIndexIpNetToPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalPhysAddress}, GetNextIndexIpNetToPhysicalTable, IpNetToPhysicalPhysAddressGet, IpNetToPhysicalPhysAddressSet, IpNetToPhysicalPhysAddressTest, IpNetToPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalLastUpdated}, GetNextIndexIpNetToPhysicalTable, IpNetToPhysicalLastUpdatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalType}, GetNextIndexIpNetToPhysicalTable, IpNetToPhysicalTypeGet, IpNetToPhysicalTypeSet, IpNetToPhysicalTypeTest, IpNetToPhysicalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpNetToPhysicalTableINDEX, 3, 0, 0, "4"},

{{10,IpNetToPhysicalState}, GetNextIndexIpNetToPhysicalTable, IpNetToPhysicalStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpNetToPhysicalTableINDEX, 3, 0, 0, NULL},

{{10,IpNetToPhysicalRowStatus}, GetNextIndexIpNetToPhysicalTable, IpNetToPhysicalRowStatusGet, IpNetToPhysicalRowStatusSet, IpNetToPhysicalRowStatusTest, IpNetToPhysicalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpNetToPhysicalTableINDEX, 3, 0, 1, NULL},

{{10,Ipv6ScopeZoneIndexIfIndex}, GetNextIndexIpv6ScopeZoneIndexTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexLinkLocal}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexLinkLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndex3}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndex3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexAdminLocal}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexAdminLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexSiteLocal}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexSiteLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndex6}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndex6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndex7}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndex7Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexOrganizationLocal}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexOrganizationLocalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndex9}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndex9Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexA}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexAGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexB}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexBGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexC}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6ScopeZoneIndexD}, GetNextIndexIpv6ScopeZoneIndexTable, Ipv6ScopeZoneIndexDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ipv6ScopeZoneIndexTableINDEX, 1, 0, 0, NULL},

{{10,IpDefaultRouterAddressType}, GetNextIndexIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{10,IpDefaultRouterAddress}, GetNextIndexIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{10,IpDefaultRouterIfIndex}, GetNextIndexIpDefaultRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{10,IpDefaultRouterLifetime}, GetNextIndexIpDefaultRouterTable, IpDefaultRouterLifetimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{10,IpDefaultRouterPreference}, GetNextIndexIpDefaultRouterTable, IpDefaultRouterPreferenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpDefaultRouterTableINDEX, 3, 0, 0, NULL},

{{8,Ipv6RouterAdvertSpinLock}, NULL, Ipv6RouterAdvertSpinLockGet, Ipv6RouterAdvertSpinLockSet, Ipv6RouterAdvertSpinLockTest, Ipv6RouterAdvertSpinLockDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Ipv6RouterAdvertIfIndex}, GetNextIndexIpv6RouterAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ipv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6RouterAdvertSendAdverts}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertSendAdvertsGet, Ipv6RouterAdvertSendAdvertsSet, Ipv6RouterAdvertSendAdvertsTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{10,Ipv6RouterAdvertMaxInterval}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertMaxIntervalGet, Ipv6RouterAdvertMaxIntervalSet, Ipv6RouterAdvertMaxIntervalTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "600"},

{{10,Ipv6RouterAdvertMinInterval}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertMinIntervalGet, Ipv6RouterAdvertMinIntervalSet, Ipv6RouterAdvertMinIntervalTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6RouterAdvertManagedFlag}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertManagedFlagGet, Ipv6RouterAdvertManagedFlagSet, Ipv6RouterAdvertManagedFlagTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{10,Ipv6RouterAdvertOtherConfigFlag}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertOtherConfigFlagGet, Ipv6RouterAdvertOtherConfigFlagSet, Ipv6RouterAdvertOtherConfigFlagTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "2"},

{{10,Ipv6RouterAdvertLinkMTU}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertLinkMTUGet, Ipv6RouterAdvertLinkMTUSet, Ipv6RouterAdvertLinkMTUTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{10,Ipv6RouterAdvertReachableTime}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertReachableTimeGet, Ipv6RouterAdvertReachableTimeSet, Ipv6RouterAdvertReachableTimeTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{10,Ipv6RouterAdvertRetransmitTime}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertRetransmitTimeGet, Ipv6RouterAdvertRetransmitTimeSet, Ipv6RouterAdvertRetransmitTimeTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, "0"},

{{10,Ipv6RouterAdvertCurHopLimit}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertCurHopLimitGet, Ipv6RouterAdvertCurHopLimitSet, Ipv6RouterAdvertCurHopLimitTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6RouterAdvertDefaultLifetime}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertDefaultLifetimeGet, Ipv6RouterAdvertDefaultLifetimeSet, Ipv6RouterAdvertDefaultLifetimeTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 0, NULL},

{{10,Ipv6RouterAdvertRowStatus}, GetNextIndexIpv6RouterAdvertTable, Ipv6RouterAdvertRowStatusGet, Ipv6RouterAdvertRowStatusSet, Ipv6RouterAdvertRowStatusTest, Ipv6RouterAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ipv6RouterAdvertTableINDEX, 1, 0, 1, NULL},

{{8,IcmpInMsgs}, NULL, IcmpInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInErrors}, NULL, IcmpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInDestUnreachs}, NULL, IcmpInDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimeExcds}, NULL, IcmpInTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInParmProbs}, NULL, IcmpInParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInSrcQuenchs}, NULL, IcmpInSrcQuenchsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInRedirects}, NULL, IcmpInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInEchos}, NULL, IcmpInEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInEchoReps}, NULL, IcmpInEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimestamps}, NULL, IcmpInTimestampsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimestampReps}, NULL, IcmpInTimestampRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInAddrMasks}, NULL, IcmpInAddrMasksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInAddrMaskReps}, NULL, IcmpInAddrMaskRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutMsgs}, NULL, IcmpOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutErrors}, NULL, IcmpOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutDestUnreachs}, NULL, IcmpOutDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimeExcds}, NULL, IcmpOutTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutParmProbs}, NULL, IcmpOutParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutSrcQuenchs}, NULL, IcmpOutSrcQuenchsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutRedirects}, NULL, IcmpOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutEchos}, NULL, IcmpOutEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutEchoReps}, NULL, IcmpOutEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimestamps}, NULL, IcmpOutTimestampsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimestampReps}, NULL, IcmpOutTimestampRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutAddrMasks}, NULL, IcmpOutAddrMasksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutAddrMaskReps}, NULL, IcmpOutAddrMaskRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,IcmpStatsIPVersion}, GetNextIndexIcmpStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{10,IcmpStatsInMsgs}, GetNextIndexIcmpStatsTable, IcmpStatsInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{10,IcmpStatsInErrors}, GetNextIndexIcmpStatsTable, IcmpStatsInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{10,IcmpStatsOutMsgs}, GetNextIndexIcmpStatsTable, IcmpStatsOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{10,IcmpStatsOutErrors}, GetNextIndexIcmpStatsTable, IcmpStatsOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpStatsTableINDEX, 1, 0, 0, NULL},

{{10,IcmpMsgStatsIPVersion}, GetNextIndexIcmpMsgStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IcmpMsgStatsTableINDEX, 2, 0, 0, NULL},

{{10,IcmpMsgStatsType}, GetNextIndexIcmpMsgStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IcmpMsgStatsTableINDEX, 2, 0, 0, NULL},

{{10,IcmpMsgStatsInPkts}, GetNextIndexIcmpMsgStatsTable, IcmpMsgStatsInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpMsgStatsTableINDEX, 2, 0, 0, NULL},

{{10,IcmpMsgStatsOutPkts}, GetNextIndexIcmpMsgStatsTable, IcmpMsgStatsOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IcmpMsgStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData stdipvEntry = { 268, stdipvMibEntry };
#endif /* _STDIPVDB_H */

