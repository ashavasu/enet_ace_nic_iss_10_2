/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipvxlw.h,v 1.2 2009/10/05 11:54:45 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsIpvxAddrPrefixTable. */
INT1
nmhValidateIndexInstanceFsIpvxAddrPrefixTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpvxAddrPrefixTable  */

INT1
nmhGetFirstIndexFsIpvxAddrPrefixTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpvxAddrPrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpvxAddrPrefixProfileIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsIpvxAddrPrefixSecAddrFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsIpvxAddrPrefixRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpvxAddrPrefixProfileIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsIpvxAddrPrefixSecAddrFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsIpvxAddrPrefixRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpvxAddrPrefixProfileIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsIpvxAddrPrefixSecAddrFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsIpvxAddrPrefixRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpvxAddrPrefixTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpv6IfIcmpTable. */
INT1
nmhValidateIndexInstanceFsIpv6IfIcmpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpv6IfIcmpTable  */

INT1
nmhGetFirstIndexFsIpv6IfIcmpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpv6IfIcmpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpv6IfIcmpInMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInDestUnreachs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInAdminProhibs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInTimeExcds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInParmProblems ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInPktTooBigs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInEchos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInEchoReplies ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInRouterSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInGroupMembQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInGroupMembResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpInGroupMembReductions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutDestUnreachs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutAdminProhibs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutTimeExcds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutParmProblems ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutPktTooBigs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutEchos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutEchoReplies ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutRouterSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutRedirects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutGroupMembQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutGroupMembResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpv6IfIcmpOutGroupMembReductions ARG_LIST((INT4 ,UINT4 *));
