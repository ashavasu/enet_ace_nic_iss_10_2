/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsiplw.h,v 1.6 2016/02/27 10:14:52 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpv4InterfaceTableLastChange ARG_LIST((UINT4 *));

INT1
nmhGetFsMIStdIpv6InterfaceTableLastChange ARG_LIST((UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsTableLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsMIStdIpGlobalTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpGlobalTable  */

INT1
nmhGetFirstIndexFsMIStdIpGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpDefaultTTL ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpReasmTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6IpForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6IpDefaultHopLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteNumber ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdInetCidrRouteDiscards ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpDefaultTTL ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6IpForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6IpDefaultHopLimit ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpDefaultTTL ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6IpForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6IpDefaultHopLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpv4InterfaceTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpv4InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv4InterfaceTable  */

INT1
nmhGetFirstIndexFsMIStdIpv4InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpv4InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpv4InterfaceReasmMaxSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv4InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv4InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv4IfContextId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpv4InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpv4InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpv4InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpv6InterfaceTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpv6InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv6InterfaceTable  */

INT1
nmhGetFirstIndexFsMIStdIpv6InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpv6InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpv6InterfaceReasmMaxSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6InterfaceIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdIpv6InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6InterfaceReachableTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6InterfaceForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6IfContextId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpv6InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6InterfaceForwarding ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpv6InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6InterfaceForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpv6InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpSystemStatsTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpSystemStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpSystemStatsTable  */

INT1
nmhGetFirstIndexFsMIStdIpSystemStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpSystemStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpSystemStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpSystemStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpSystemStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdIpIfStatsTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpIfStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpIfStatsTable  */

INT1
nmhGetFirstIndexFsMIStdIpIfStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpIfStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpIfStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdIpIfStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpIfStatsContextId ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpAddressPrefixTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpAddressPrefixTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpAddressPrefixTable  */

INT1
nmhGetFirstIndexFsMIStdIpAddressPrefixTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpAddressPrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpAddressPrefixOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdIpAddressPrefixOnLinkFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdIpAddressPrefixAutonomousFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdIpAddressPrefixAdvPreferredLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpAddressPrefixAdvValidLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpAddressContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpAddressTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpAddressTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpAddressTable  */

INT1
nmhGetFirstIndexFsMIStdIpAddressTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpAddressTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpAddressIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpAddressType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpAddressPrefix ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIStdIpAddressOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpAddressStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpAddressCreated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdIpAddressLastChanged ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdIpAddressRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpAddressStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpAddressIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdIpAddressType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdIpAddressStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdIpAddressRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdIpAddressStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpAddressIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdIpAddressType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdIpAddressStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdIpAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdIpAddressStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpNetToPhysicalTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpNetToPhysicalTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpNetToPhysicalTable  */

INT1
nmhGetFirstIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdIpNetToPhysicalLastUpdated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpNetToPhysicalState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdIpNetToPhysicalContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpNetToPhysicalPhysAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdIpNetToPhysicalType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdIpNetToPhysicalRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpNetToPhysicalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpv6ScopeZoneIndexTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv6ScopeZoneIndexTable  */

INT1
nmhGetFirstIndexFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndex3 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndex6 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndex7 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndex9 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexA ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexB ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexC ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneIndexD ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6ScopeZoneContextId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpDefaultRouterTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpDefaultRouterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpDefaultRouterTable  */

INT1
nmhGetFirstIndexFsMIStdIpDefaultRouterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpDefaultRouterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpDefaultRouterLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpDefaultRouterPreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpv6RouterAdvertTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv6RouterAdvertTable  */

INT1
nmhGetFirstIndexFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertMinInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertReachableTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpv6RouterAdvertContextId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertMinInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertReachableTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdIpv6RouterAdvertRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertReachableTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdIpv6RouterAdvertRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpv6RouterAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIcmpStatsTable. */
INT1
nmhValidateIndexInstanceFsMIStdIcmpStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIcmpStatsTable  */

INT1
nmhGetFirstIndexFsMIStdIcmpStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIcmpStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIcmpStatsInMsgs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIcmpStatsInErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIcmpStatsOutMsgs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIcmpStatsOutErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdIcmpMsgStatsTable. */
INT1
nmhValidateIndexInstanceFsMIStdIcmpMsgStatsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIcmpMsgStatsTable  */

INT1
nmhGetFirstIndexFsMIStdIcmpMsgStatsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIcmpMsgStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIcmpMsgStatsInPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdIcmpMsgStatsOutPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdInetCidrRouteTable. */
INT1
nmhValidateIndexInstanceFsMIStdInetCidrRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdInetCidrRouteTable  */

INT1
nmhGetFirstIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OID_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteAge ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdInetCidrRouteNextHopAS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteMetric2 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteMetric3 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteMetric4 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRouteAddrType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdInetCidrRoutePreference ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteNextHopAS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteMetric2 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteMetric3 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteMetric4 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRouteAddrType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdInetCidrRoutePreference ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdInetCidrRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhGetFsMIStdInetCidrRouteHWStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIStdInetCidrRouteMetric2 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteMetric3 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteMetric4 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteMetric5 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRouteAddrType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdInetCidrRoutePreference ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdInetCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpifTable. */
INT1
nmhValidateIndexInstanceFsMIStdIpifTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpifTable  */

INT1
nmhGetFirstIndexFsMIStdIpifTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdIpifTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdIpLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdIpLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdIpLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpifTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdIpProxyArpSubnetOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdIpProxyArpSubnetOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdIpProxyArpSubnetOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
