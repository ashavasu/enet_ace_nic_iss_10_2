/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipvlw.h,v 1.3 2013/03/30 12:13:12 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpForwarding ARG_LIST((INT4 *));

INT1
nmhGetIpDefaultTTL ARG_LIST((INT4 *));

INT1
nmhGetIpReasmTimeout ARG_LIST((INT4 *));

INT1
nmhGetIpv6IpForwarding ARG_LIST((INT4 *));

INT1
nmhGetIpv6IpDefaultHopLimit ARG_LIST((INT4 *));

INT1
nmhGetIpv4InterfaceTableLastChange ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpForwarding ARG_LIST((INT4 ));

INT1
nmhSetIpDefaultTTL ARG_LIST((INT4 ));

INT1
nmhSetIpv6IpForwarding ARG_LIST((INT4 ));

INT1
nmhSetIpv6IpDefaultHopLimit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpForwarding ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IpDefaultTTL ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ipv6IpForwarding ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ipv6IpDefaultHopLimit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpForwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IpDefaultTTL ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ipv6IpForwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ipv6IpDefaultHopLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv4InterfaceTable. */
INT1
nmhValidateIndexInstanceIpv4InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv4InterfaceTable  */

INT1
nmhGetFirstIndexIpv4InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv4InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv4InterfaceReasmMaxSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv4InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv4InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv4InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv4InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv4InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6InterfaceTableLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Ipv6InterfaceTable. */
INT1
nmhValidateIndexInstanceIpv6InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6InterfaceTable  */

INT1
nmhGetFirstIndexIpv6InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6InterfaceReasmMaxSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6InterfaceIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpv6InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6InterfaceReachableTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6InterfaceForwarding ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6InterfaceForwarding ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6InterfaceForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpSystemStatsTable. */
INT1
nmhValidateIndexInstanceIpSystemStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpSystemStatsTable  */

INT1
nmhGetFirstIndexIpSystemStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpSystemStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpSystemStatsInReceives ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInReceives ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsInOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsInHdrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInNoRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInAddrErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInTruncatedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInForwDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInForwDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsReasmReqds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsReasmOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsReasmFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsInDelivers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInDelivers ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutRequests ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutNoRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutForwDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutForwDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutFragReqds ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutFragOKs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutFragFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutFragCreates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsOutTransmits ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutTransmits ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsInMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInMcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsInMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutMcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutMcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsInBcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCInBcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsOutBcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsHCOutBcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpSystemStatsDiscontinuityTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpSystemStatsRefreshRate ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpIfStatsTableLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpIfStatsTable. */
INT1
nmhValidateIndexInstanceIpIfStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpIfStatsTable  */

INT1
nmhGetFirstIndexIpIfStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpIfStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpIfStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIpIfStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIpIfStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IpAddressPrefixTable. */
INT1
nmhValidateIndexInstanceIpAddressPrefixTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpAddressPrefixTable  */

INT1
nmhGetFirstIndexIpAddressPrefixTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpAddressPrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpAddressPrefixOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetIpAddressPrefixOnLinkFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetIpAddressPrefixAutonomousFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetIpAddressPrefixAdvPreferredLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetIpAddressPrefixAdvValidLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpAddressSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpAddressSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpAddressSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpAddressSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpAddressTable. */
INT1
nmhValidateIndexInstanceIpAddressTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IpAddressTable  */

INT1
nmhGetFirstIndexIpAddressTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpAddressTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpAddressIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpAddressPrefix ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetIpAddressOrigin ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpAddressCreated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIpAddressLastChanged ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIpAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpAddressStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpAddressIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIpAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIpAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIpAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIpAddressStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpAddressIfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IpAddressType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IpAddressStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IpAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IpAddressStorageType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpNetToPhysicalTable. */
INT1
nmhValidateIndexInstanceIpNetToPhysicalTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IpNetToPhysicalTable  */

INT1
nmhGetFirstIndexIpNetToPhysicalTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpNetToPhysicalTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpNetToPhysicalLastUpdated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpNetToPhysicalState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpNetToPhysicalPhysAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IpNetToPhysicalType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IpNetToPhysicalRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpNetToPhysicalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6ScopeZoneIndexTable. */
INT1
nmhValidateIndexInstanceIpv6ScopeZoneIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6ScopeZoneIndexTable  */

INT1
nmhGetFirstIndexIpv6ScopeZoneIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6ScopeZoneIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndex3 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndex6 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndex7 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndex9 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexA ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexB ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexC ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6ScopeZoneIndexD ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for IpDefaultRouterTable. */
INT1
nmhValidateIndexInstanceIpDefaultRouterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpDefaultRouterTable  */

INT1
nmhGetFirstIndexIpDefaultRouterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpDefaultRouterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpDefaultRouterLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetIpDefaultRouterPreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6RouterAdvertSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6RouterAdvertSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6RouterAdvertSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6RouterAdvertSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6RouterAdvertTable. */
INT1
nmhValidateIndexInstanceIpv6RouterAdvertTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6RouterAdvertTable  */

INT1
nmhGetFirstIndexIpv6RouterAdvertTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpv6RouterAdvertTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpv6RouterAdvertSendAdverts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6RouterAdvertMaxInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertMinInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertManagedFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIpv6RouterAdvertLinkMTU ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertReachableTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertRetransmitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertCurHopLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIpv6RouterAdvertRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpv6RouterAdvertSendAdverts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6RouterAdvertMaxInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertMinInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertManagedFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIpv6RouterAdvertLinkMTU ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertReachableTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertRetransmitTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertCurHopLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIpv6RouterAdvertRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ipv6RouterAdvertSendAdverts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6RouterAdvertMaxInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertManagedFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6RouterAdvertOtherConfigFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ipv6RouterAdvertLinkMTU ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertReachableTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertCurHopLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertDefaultLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ipv6RouterAdvertRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ipv6RouterAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IcmpStatsTable. */
INT1
nmhValidateIndexInstanceIcmpStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IcmpStatsTable  */

INT1
nmhGetFirstIndexIcmpStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIcmpStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIcmpStatsInMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIcmpStatsInErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIcmpStatsOutMsgs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIcmpStatsOutErrors ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for IcmpMsgStatsTable. */
INT1
nmhValidateIndexInstanceIcmpMsgStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IcmpMsgStatsTable  */

INT1
nmhGetFirstIndexIcmpMsgStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIcmpMsgStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIcmpMsgStatsInPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIcmpMsgStatsOutPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpInReceives ARG_LIST((UINT4 *));

INT1
nmhGetIpInHdrErrors ARG_LIST((UINT4 *));

INT1
nmhGetIpInAddrErrors ARG_LIST((UINT4 *));

INT1
nmhGetIpForwDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetIpInUnknownProtos ARG_LIST((UINT4 *));

INT1
nmhGetIpInDiscards ARG_LIST((UINT4 *));

INT1
nmhGetIpInDelivers ARG_LIST((UINT4 *));

INT1
nmhGetIpOutRequests ARG_LIST((UINT4 *));

INT1
nmhGetIpOutDiscards ARG_LIST((UINT4 *));

INT1
nmhGetIpOutNoRoutes ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmReqds ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmOKs ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmFails ARG_LIST((UINT4 *));

INT1
nmhGetIpFragOKs ARG_LIST((UINT4 *));

INT1
nmhGetIpFragFails ARG_LIST((UINT4 *));

INT1
nmhGetIpFragCreates ARG_LIST((UINT4 *));

INT1
nmhGetIpRoutingDiscards ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpAddrTable. */
INT1
nmhValidateIndexInstanceIpAddrTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpAddrTable  */

INT1
nmhGetFirstIndexIpAddrTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpAddrTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpAdEntIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIpAdEntNetMask ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIpAdEntBcastAddr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIpAdEntReasmMaxSize ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for IpNetToMediaTable. */
INT1
nmhValidateIndexInstanceIpNetToMediaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpNetToMediaTable  */

INT1
nmhGetFirstIndexIpNetToMediaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpNetToMediaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpNetToMediaType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpNetToMediaIfIndex ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpNetToMediaNetAddress ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIpNetToMediaType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpNetToMediaIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpNetToMediaPhysAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IpNetToMediaNetAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2IpNetToMediaType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpNetToMediaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIcmpInMsgs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInSrcQuenchs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInRedirects ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInEchos ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimestamps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimestampReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInAddrMasks ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInAddrMaskReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutMsgs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutErrors ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutSrcQuenchs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutRedirects ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutEchos ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimestamps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimestampReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutAddrMasks ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutAddrMaskReps ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetInetCidrRouteNumber ARG_LIST((UINT4 *));

INT1
nmhGetInetCidrRouteDiscards ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for InetCidrRouteTable. */
INT1
nmhValidateIndexInstanceInetCidrRouteTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for InetCidrRouteTable  */

INT1
nmhGetFirstIndexInetCidrRouteTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OID_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexInetCidrRouteTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetInetCidrRouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteAge ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetInetCidrRouteNextHopAS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetInetCidrRouteMetric1 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteMetric2 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteMetric3 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteMetric4 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteMetric5 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetInetCidrRouteHWStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetInetCidrRouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteNextHopAS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetInetCidrRouteMetric1 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteMetric2 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteMetric3 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteMetric4 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteMetric5 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetInetCidrRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2InetCidrRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2InetCidrRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteMetric2 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteMetric3 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteMetric4 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteMetric5 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2InetCidrRouteStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2InetCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCidrRouteNumber ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpCidrRouteTable. */
INT1
nmhValidateIndexInstanceIpCidrRouteTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpCidrRouteTable  */

INT1
nmhGetFirstIndexIpCidrRouteTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpCidrRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteProto ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteAge ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteHWStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpCidrRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteInfo ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2IpCidrRouteNextHopAS ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric3 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric4 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric5 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUdpInDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetUdpNoPorts ARG_LIST((UINT4 *));

INT1
nmhGetUdpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetUdpOutDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetUdpHCInDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetUdpHCOutDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for UdpEndpointTable. */
INT1
nmhValidateIndexInstanceUdpEndpointTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpEndpointTable  */

INT1
nmhGetFirstIndexUdpEndpointTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUdpEndpointTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUdpEndpointProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for UdpTable. */
INT1
nmhValidateIndexInstanceUdpTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpTable  */

INT1
nmhGetFirstIndexUdpTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUdpTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
