/* Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: ipvxdefs.h,v 1.9 2010/05/19 07:13:32 prabuc Exp $
 *
 * Description: File containing the macros and constants used in 
                IPVX module.
 *
 *******************************************************************/

#ifndef __IPVX_DEFS_H
#define __IPVX_DEFS_H

#define IPVX_ZERO     0
#define IPVX_ONE      1
#define IPVX_TWO      2
#define IPVX_FOUR     4
#define IPVX_SIX      6

#define IPVX_MINUS_ONE    (-1)

#define IPVX_SUCCESS   1
#define IPVX_FAILURE   0
#define MAX_MASK_LEN   128

#define IPV4_ADD_LEN   4
#define IPV6_ADD_LEN   16
#define IPVX_DEFAULT_CONTEXT 0
/* Definition for the RowStatus values */
#define   IPVX_ACTIVE                 1
#define   IPVX_NOT_IN_SERVICE         2
#define   IPVX_NOT_READY              3
#define   IPVX_CREATE_AND_GO          4
#define   IPVX_CREATE_AND_WAIT        5
#define   IPVX_DESTROY                6
#define   IPVX_NOT_APPLICABLE         0

/*IP address types*/
#define   IPVX_UNICAST    1
#define   IPVX_ANYCAST    2
#define   IPVX_BROADCAST  3

/*IP address status*/
#define   IPVX_STATUS_PREFERRED    1

/*IP address storage type*/
#define IPVX_PERMANENT   1
#define IPVX_VOLATILE    2

/*IP address origin*/
#define IPVX_MANUAL         1
#define IPVX_NEGOTIATION    2
#define IPVX_DHCP           3
#define IPVX_NONE           4

#define IPVX_PREFERRED      1

#define  ADMIN_UP       NETIPV6_ADMIN_UP
#define  ADMIN_DOWN     NETIPV6_ADMIN_DOWN

#define DEFAULT_LIFETIME    0xffffffff
#define  CLASS_A_DEF_MASK    8
#define  CLASS_B_DEF_MASK    16
#define  CLASS_C_DEF_MASK    24

#define  IPVX_STARTING_IFINDEX    SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG + 1
#define  IPVX_IFINDEX             SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG
#define  IPV4_AUTONOMOUS_FLAG     2
#define OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
        if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
            MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}

#define INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
    MEMSET (pOctetString->pu1_OctetList, 0, IPV6_ADD_LEN); \
    MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
           sizeof(UINT4));\
    pOctetString->i4_Length=sizeof(UINT4);}

/* MAX valid CIDR subnet mask number */
#define   IP_CFA_MAX_CIDR                   32
#define   IPVX_OFFSET(x,y)  ((UINT4)(&(((x *)0)->y)))

/* Excess address length for the ipv4 address */
#define IPVX_V4_EXCESS_ADDR_LEN        12
#endif
