/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsipwr.h,v 1.2
*
* Description: Protocol Low Level Routines
*********************************************************************/
#ifndef _FSMSIPWR_H
#define _FSMSIPWR_H

VOID RegisterFSMSIP(VOID);

VOID UnRegisterFSMSIP(VOID);
INT4 FsMIStdIpv4InterfaceTableLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceTableLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsTableLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpGlobalTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpForwardingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpDefaultTTLGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpReasmTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpForwardingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpDefaultHopLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpForwardingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpDefaultTTLSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpForwardingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpDefaultHopLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpForwardingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpDefaultTTLTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpForwardingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IpDefaultHopLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpGlobalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIpv4InterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpv4InterfaceReasmMaxSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4InterfaceEnableStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4InterfaceRetransmitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4IfContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4InterfaceEnableStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4InterfaceEnableStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv4InterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIpv6InterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpv6InterfaceReasmMaxSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceEnableStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceReachableTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceRetransmitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceForwardingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6IfContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceEnableStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceForwardingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceEnableStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceForwardingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6InterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIpSystemStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpSystemStatsInReceivesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInReceivesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInHdrErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInNoRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInAddrErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInUnknownProtosGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInTruncatedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsReasmReqdsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsReasmOKsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsReasmFailsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInDeliversGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInDeliversGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutNoRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutFragReqdsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutFragOKsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutFragFailsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutFragCreatesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutTransmitsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutTransmitsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsInBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCInBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsOutBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsHCOutBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpSystemStatsRefreshRateGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpIfStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpIfStatsInReceivesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInReceivesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInHdrErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInNoRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInAddrErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInUnknownProtosGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInTruncatedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsReasmReqdsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsReasmOKsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsReasmFailsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInDeliversGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInDeliversGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutForwDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutFragReqdsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutFragOKsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutFragFailsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutFragCreatesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutTransmitsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutTransmitsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutMcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutMcastOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsInBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCInBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsOutBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsHCOutBcastPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsRefreshRateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpIfStatsContextIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpAddressPrefixTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpAddressPrefixOriginGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressPrefixOnLinkFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressPrefixAutonomousFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressPrefixAdvPreferredLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressPrefixAdvValidLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressContextIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpAddressIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressPrefixGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressOriginGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressCreatedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressLastChangedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIpNetToPhysicalTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpNetToPhysicalPhysAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalLastUpdatedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalPhysAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalPhysAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpNetToPhysicalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIpv6ScopeZoneIndexTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpv6ScopeZoneIndexLinkLocalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndex3Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexAdminLocalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexSiteLocalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndex6Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndex7Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexOrganizationLocalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndex9Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexAGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexBGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexCGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneIndexDGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6ScopeZoneContextIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpDefaultRouterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpDefaultRouterLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpDefaultRouterPreferenceGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpv6RouterAdvertTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpv6RouterAdvertSendAdvertsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMaxIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMinIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertManagedFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertOtherConfigFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertLinkMTUGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertReachableTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRetransmitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertCurHopLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertDefaultLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertSendAdvertsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMaxIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMinIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertManagedFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertOtherConfigFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertLinkMTUSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertReachableTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRetransmitTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertCurHopLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertDefaultLifetimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertSendAdvertsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMaxIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertMinIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertManagedFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertOtherConfigFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertLinkMTUTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertReachableTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRetransmitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertCurHopLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertDefaultLifetimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpv6RouterAdvertTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdIcmpStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIcmpStatsInMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIcmpStatsInErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIcmpStatsOutMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIcmpStatsOutErrorsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIcmpMsgStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIcmpMsgStatsInPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIcmpMsgStatsOutPktsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdInetCidrRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdInetCidrRouteIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteProtoGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteNextHopASGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric1Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric2Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric3Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric4Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric5Get(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRoutePreferenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteNextHopASSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric1Set(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric2Set(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric3Set(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric4Set(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric5Set(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRoutePreferenceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteNextHopASTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric3Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric4Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteMetric5Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRoutePreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdInetCidrRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIStdInetCidrRouteHWStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdIpifTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdIpProxyArpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpLocalProxyArpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpProxyArpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpLocalProxyArpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpProxyArpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpLocalProxyArpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpifTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIStdIpProxyArpSubnetOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpProxyArpSubnetOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpProxyArpSubnetOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdIpProxyArpSubnetOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif /* _FSMSIPWR_H */
