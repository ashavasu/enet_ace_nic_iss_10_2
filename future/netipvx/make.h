
# (C) 2007 Future Software Pvt. Ltd.
# $Id: make.h,v 1.6 2015/10/13 13:01:32 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software Pvt. Ltd.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
############################################################################
#                         Directories                                      #
############################################################################

IPVX_IP_BASE_DIR = ${BASE_DIR}/netipvx/ip
IPVX_IP_INC_DIR  = ${IPVX_IP_BASE_DIR}/inc
IPVX_IP_SRC_DIR  = ${IPVX_IP_BASE_DIR}/src
IPVX_IP_OBJ_DIR  = ${IPVX_IP_BASE_DIR}/obj
    
IPVX_RTMX_BASE_DIR = ${BASE_DIR}/netipvx/rtmx
IPVX_RTMX_INC_DIR  = ${IPVX_RTMX_BASE_DIR}/inc
IPVX_RTMX_SRC_DIR  = ${IPVX_RTMX_BASE_DIR}/src
IPVX_RTMX_OBJ_DIR  = ${IPVX_RTMX_BASE_DIR}/obj

IPVX_TCP_BASE_DIR = ${BASE_DIR}/netipvx/tcp
IPVX_TCP_INC_DIR  = ${IPVX_TCP_BASE_DIR}/inc
IPVX_TCP_SRC_DIR  = ${IPVX_TCP_BASE_DIR}/src
IPVX_TCP_OBJ_DIR  = ${IPVX_TCP_BASE_DIR}/obj

IPVX_UDP_BASE_DIR = ${BASE_DIR}/netipvx/udp
IPVX_UDP_INC_DIR  = ${IPVX_UDP_BASE_DIR}/inc
IPVX_UDP_SRC_DIR  = ${IPVX_UDP_BASE_DIR}/src
IPVX_UDP_OBJ_DIR  = ${IPVX_UDP_BASE_DIR}/obj

IPVX_BASE_DIR = ${BASE_DIR}/netipvx
IPVX_INC_DIR  = ${IPVX_BASE_DIR}/inc
IPVX_SRC_DIR  = ${IPVX_BASE_DIR}/src
IPVX_OBJ_DIR  = ${IPVX_BASE_DIR}/obj

IPVX_TCP_DIR = ${BASE_DIR}/tcp
IPVX_UDP4_DIR = ${BASE_DIR}/netip/fsip/udp
IPVX_UDP4_INC_DIR = ${IPVX_UDP4_DIR}/inc
IPVX_UDP4_SRC_DIR = ${IPVX_UDP4_DIR}/src
IPVX_UDP6_DIR = ${BASE_DIR}/netip6/fsip6/ip6core

IPVX_TRCRT_BASE_DIR = ${BASE_DIR}/netipvx/ip/trcrt
IPVX_TRCRT_INC_DIR  = ${IPVX_TRCRT_BASE_DIR}/inc
IPVX_TRCRT_SRC_DIR  = ${IPVX_TRCRT_BASE_DIR}/src
IPVX_TRCRT_OBJ_DIR  = ${IPVX_TRCRT_BASE_DIR}/obj

NETIP_BASE_DIR = ${BASE_DIR}/netip
IPMGMT_BASE_DIR  = ${NETIP_BASE_DIR}/ipmgmt
IPMGMT_SRCD      = ${IPMGMT_BASE_DIR}/src
IPMGMT_INCD      = ${IPMGMT_BASE_DIR}/inc
IPMGMT_OBJD      = ${IPMGMT_BASE_DIR}/obj

NETIP6_BASE_DIR = ${BASE_DIR}/netip6
IP6MGMT_BASE_DIR  = ${NETIP6_BASE_DIR}/ip6mgmt
IP6MGMT_SRCD      = ${IP6MGMT_BASE_DIR}/src
IP6MGMT_INCD      = ${IP6MGMT_BASE_DIR}/inc
IP6MGMT_OBJD      = ${IP6MGMT_BASE_DIR}/obj
IP6HOST_BASE_DIR = ${NETIP6_BASE_DIR}/fsip6/ip6host


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${IPVX_TCP_INC_DIR} -I${IPVX_INC_DIR} -I${IPVX_TCP_DIR} -I${IPVX_UDP_INC_DIR} -I${IPVX_UDP4_INC_DIR} -I${IPVX_TRCRT_INC_DIR} -I${IPVX_UDP6_DIR} -I${IPMGMT_INCD} -I${IP6MGMT_INCD} -I${IPVX_IP_INC_DIR} -I${IPVX_RTMX_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

ifeq (${IP6HOST}, YES)
INCLUDES   += -I ${IP6HOST_BASE_DIR}
endif

#############################################################################

