/*******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmxinc.h,v 1.1.1.1 2015/10/13 13:07:50 siva Exp $
 *
 * Description: Function declarations for RTMx functions
 *******************************************************************/

#ifndef _RTMX_INC_H_
#define _RTMX_INC_H_

#include "lr.h"
#include "iss.h"
#include "trie.h"
#include "ip.h"
#include "rtm.h"
#include "rtmx.h"

#endif /* _RTMX_INC_H_ */
