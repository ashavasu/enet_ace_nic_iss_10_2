/*******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmxapi.c,v 1.1.1.1 2015/10/13 13:07:50 siva Exp $
 *
 * Description: Common RTM APIs for Best Route Selection 
 *******************************************************************/

#include <rtmxinc.h>

/*******************************************************************
 * Function    :  RtmxGetBestRouteInCxt 
 * Description :  This routine gets the best route  
 * Input       :  
 * Output      :  pRt -  
 * Returns     :  IP_SUCCESS/IP_FAILURE 
 ********************************************************************/
INT4
RtmxGetBestRouteInCxt (void *pInputRt, void **ppOutputRt,
                       UINT1 u1QueryFlag, UINT1 u1Protocol)
{
    tRtInfo   *pCurRt = NULL;
    tRtInfo   *pTmpRt = NULL;
    tRtInfo   *pBestRt = NULL;

    if (u1Protocol == IP_VERSION_4)
    {
        pCurRt = (tRtInfo *) pInputRt;

        while (pCurRt != NULL)
        {
            /* get the first active entry from the alternate paths */
            pTmpRt = pCurRt;
            while (pTmpRt != NULL)
            {
                /* only active routes will be returned */
                if (pTmpRt->u4RowStatus == IPFWD_ACTIVE)
                {                
                    pBestRt = pTmpRt;
                    break;
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }

            /* break only if an active route is found */
            if (pBestRt != NULL)
            {
                break;
            }
            pCurRt = (tRtInfo *) (void *)(pCurRt->NextRtInfo.pNext);
        }

        /* If any route is having reachable next hop, then retun it */
        if (u1QueryFlag == IP_ONE /*RTM_GET_REACHABLE_BEST*/)
        {
            pTmpRt = pBestRt;
            while ((pTmpRt != NULL))
            {
                if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
                {
                    pBestRt = pTmpRt;
                    break;
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
        }

        if (pBestRt == NULL)
        {
            return IP_FAILURE;
        }

        *ppOutputRt = (void *)pBestRt;
    }
    else
    {
        /* IPv6 */
    }
    return IP_SUCCESS;
}
