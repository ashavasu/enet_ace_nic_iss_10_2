/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtcpdb.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDTCPDB_H
#define _STDTCPDB_H

UINT1 TcpConnectionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 TcpListenerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdtcp [] ={1,3,6,1,2,1,6};
tSNMP_OID_TYPE stdtcpOID = {7, stdtcp};


UINT4 TcpRtoAlgorithm [ ] ={1,3,6,1,2,1,6,1};
UINT4 TcpRtoMin [ ] ={1,3,6,1,2,1,6,2};
UINT4 TcpRtoMax [ ] ={1,3,6,1,2,1,6,3};
UINT4 TcpMaxConn [ ] ={1,3,6,1,2,1,6,4};
UINT4 TcpActiveOpens [ ] ={1,3,6,1,2,1,6,5};
UINT4 TcpPassiveOpens [ ] ={1,3,6,1,2,1,6,6};
UINT4 TcpAttemptFails [ ] ={1,3,6,1,2,1,6,7};
UINT4 TcpEstabResets [ ] ={1,3,6,1,2,1,6,8};
UINT4 TcpCurrEstab [ ] ={1,3,6,1,2,1,6,9};
UINT4 TcpInSegs [ ] ={1,3,6,1,2,1,6,10};
UINT4 TcpOutSegs [ ] ={1,3,6,1,2,1,6,11};
UINT4 TcpRetransSegs [ ] ={1,3,6,1,2,1,6,12};
UINT4 TcpInErrs [ ] ={1,3,6,1,2,1,6,14};
UINT4 TcpOutRsts [ ] ={1,3,6,1,2,1,6,15};
UINT4 TcpHCInSegs [ ] ={1,3,6,1,2,1,6,17};
UINT4 TcpHCOutSegs [ ] ={1,3,6,1,2,1,6,18};
UINT4 TcpConnectionLocalAddressType [ ] ={1,3,6,1,2,1,6,19,1,1};
UINT4 TcpConnectionLocalAddress [ ] ={1,3,6,1,2,1,6,19,1,2};
UINT4 TcpConnectionLocalPort [ ] ={1,3,6,1,2,1,6,19,1,3};
UINT4 TcpConnectionRemAddressType [ ] ={1,3,6,1,2,1,6,19,1,4};
UINT4 TcpConnectionRemAddress [ ] ={1,3,6,1,2,1,6,19,1,5};
UINT4 TcpConnectionRemPort [ ] ={1,3,6,1,2,1,6,19,1,6};
UINT4 TcpConnectionState [ ] ={1,3,6,1,2,1,6,19,1,7};
UINT4 TcpConnectionProcess [ ] ={1,3,6,1,2,1,6,19,1,8};
UINT4 TcpListenerLocalAddressType [ ] ={1,3,6,1,2,1,6,20,1,1};
UINT4 TcpListenerLocalAddress [ ] ={1,3,6,1,2,1,6,20,1,2};
UINT4 TcpListenerLocalPort [ ] ={1,3,6,1,2,1,6,20,1,3};
UINT4 TcpListenerProcess [ ] ={1,3,6,1,2,1,6,20,1,4};


tMbDbEntry stdtcpMibEntry[]= {

{{8,TcpRtoAlgorithm}, NULL, TcpRtoAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRtoMin}, NULL, TcpRtoMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRtoMax}, NULL, TcpRtoMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpMaxConn}, NULL, TcpMaxConnGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpActiveOpens}, NULL, TcpActiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpPassiveOpens}, NULL, TcpPassiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpAttemptFails}, NULL, TcpAttemptFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpEstabResets}, NULL, TcpEstabResetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpCurrEstab}, NULL, TcpCurrEstabGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpInSegs}, NULL, TcpInSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpOutSegs}, NULL, TcpOutSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRetransSegs}, NULL, TcpRetransSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpInErrs}, NULL, TcpInErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpOutRsts}, NULL, TcpOutRstsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpHCInSegs}, NULL, TcpHCInSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpHCOutSegs}, NULL, TcpHCOutSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,TcpConnectionLocalAddressType}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionLocalAddress}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionLocalPort}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionRemAddressType}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionRemAddress}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionRemPort}, GetNextIndexTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionState}, GetNextIndexTcpConnectionTable, TcpConnectionStateGet, TcpConnectionStateSet, TcpConnectionStateTest, TcpConnectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpConnectionProcess}, GetNextIndexTcpConnectionTable, TcpConnectionProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, TcpConnectionTableINDEX, 6, 0, 0, NULL},

{{10,TcpListenerLocalAddressType}, GetNextIndexTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, TcpListenerTableINDEX, 3, 0, 0, NULL},

{{10,TcpListenerLocalAddress}, GetNextIndexTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, TcpListenerTableINDEX, 3, 0, 0, NULL},

{{10,TcpListenerLocalPort}, GetNextIndexTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TcpListenerTableINDEX, 3, 0, 0, NULL},

{{10,TcpListenerProcess}, GetNextIndexTcpListenerTable, TcpListenerProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, TcpListenerTableINDEX, 3, 0, 0, NULL},
};
tMibData stdtcpEntry = { 28, stdtcpMibEntry };
#endif /* _STDTCPDB_H */

