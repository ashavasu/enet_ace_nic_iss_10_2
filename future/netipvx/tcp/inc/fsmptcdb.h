/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmptcdb.h,v 1.3 2013/06/07 13:29:58 siva Exp $
*********************************************************************/
#ifndef _FSMPTCDB_H
#define _FSMPTCDB_H

UINT1 FsMIContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMITcpConnTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMITcpExtConnTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMITcpAoConnTestTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmptc [] ={1,3,6,1,4,1,29601,2,76};
tSNMP_OID_TYPE fsmptcOID = {9, fsmptc};


UINT4 FsMITcpGlobalTraceDebug [ ] ={1,3,6,1,4,1,29601,2,76,1};
UINT4 FsMITcpContextId [ ] ={1,3,6,1,4,1,29601,2,76,2,1,1};
UINT4 FsMITcpAckOption [ ] ={1,3,6,1,4,1,29601,2,76,2,1,2};
UINT4 FsMITcpTimeStampOption [ ] ={1,3,6,1,4,1,29601,2,76,2,1,3};
UINT4 FsMITcpBigWndOption [ ] ={1,3,6,1,4,1,29601,2,76,2,1,4};
UINT4 FsMITcpIncrIniWnd [ ] ={1,3,6,1,4,1,29601,2,76,2,1,5};
UINT4 FsMITcpMaxNumOfTCB [ ] ={1,3,6,1,4,1,29601,2,76,2,1,6};
UINT4 FsMITcpTraceDebug [ ] ={1,3,6,1,4,1,29601,2,76,2,1,7};
UINT4 FsMITcpMaxReTries [ ] ={1,3,6,1,4,1,29601,2,76,2,1,8};
UINT4 FsMITcpClearStatistics [ ] ={1,3,6,1,4,1,29601,2,76,2,1,9};
UINT4 FsMITcpTrapAdminStatus [ ] ={1,3,6,1,4,1,29601,2,76,2,1,10};
UINT4 FsMITcpConnLocalAddress [ ] ={1,3,6,1,4,1,29601,2,76,3,1,2};
UINT4 FsMITcpConnLocalPort [ ] ={1,3,6,1,4,1,29601,2,76,3,1,3};
UINT4 FsMITcpConnRemAddress [ ] ={1,3,6,1,4,1,29601,2,76,3,1,4};
UINT4 FsMITcpConnRemPort [ ] ={1,3,6,1,4,1,29601,2,76,3,1,5};
UINT4 FsMITcpConnOutState [ ] ={1,3,6,1,4,1,29601,2,76,3,1,6};
UINT4 FsMITcpConnSWindow [ ] ={1,3,6,1,4,1,29601,2,76,3,1,7};
UINT4 FsMITcpConnRWindow [ ] ={1,3,6,1,4,1,29601,2,76,3,1,8};
UINT4 FsMITcpConnCWindow [ ] ={1,3,6,1,4,1,29601,2,76,3,1,9};
UINT4 FsMITcpConnSSThresh [ ] ={1,3,6,1,4,1,29601,2,76,3,1,10};
UINT4 FsMITcpConnSMSS [ ] ={1,3,6,1,4,1,29601,2,76,3,1,11};
UINT4 FsMITcpConnRMSS [ ] ={1,3,6,1,4,1,29601,2,76,3,1,12};
UINT4 FsMITcpConnSRT [ ] ={1,3,6,1,4,1,29601,2,76,3,1,13};
UINT4 FsMITcpConnRTDE [ ] ={1,3,6,1,4,1,29601,2,76,3,1,14};
UINT4 FsMITcpConnPersist [ ] ={1,3,6,1,4,1,29601,2,76,3,1,15};
UINT4 FsMITcpConnRexmt [ ] ={1,3,6,1,4,1,29601,2,76,3,1,16};
UINT4 FsMITcpConnRexmtCnt [ ] ={1,3,6,1,4,1,29601,2,76,3,1,17};
UINT4 FsMITcpConnSBCount [ ] ={1,3,6,1,4,1,29601,2,76,3,1,18};
UINT4 FsMITcpConnSBSize [ ] ={1,3,6,1,4,1,29601,2,76,3,1,19};
UINT4 FsMITcpConnRBCount [ ] ={1,3,6,1,4,1,29601,2,76,3,1,20};
UINT4 FsMITcpConnRBSize [ ] ={1,3,6,1,4,1,29601,2,76,3,1,21};
UINT4 FsMITcpKaMainTmr [ ] ={1,3,6,1,4,1,29601,2,76,3,1,22};
UINT4 FsMITcpKaRetransTmr [ ] ={1,3,6,1,4,1,29601,2,76,3,1,23};
UINT4 FsMITcpKaRetransCnt [ ] ={1,3,6,1,4,1,29601,2,76,3,1,24};
UINT4 FsMITcpConnMD5Option [ ] ={1,3,6,1,4,1,29601,2,76,4,1,1};
UINT4 FsMITcpConnMD5ErrCtr [ ] ={1,3,6,1,4,1,29601,2,76,4,1,2};
UINT4 FsMITcpConnTcpAOOption [ ] ={1,3,6,1,4,1,29601,2,76,4,1,3};
UINT4 FsMITcpConTcpAOCurKeyId [ ] ={1,3,6,1,4,1,29601,2,76,4,1,4};
UINT4 FsMITcpConTcpAORnextKeyId [ ] ={1,3,6,1,4,1,29601,2,76,4,1,5};
UINT4 FsMITcpConTcpAORcvKeyId [ ] ={1,3,6,1,4,1,29601,2,76,4,1,6};
UINT4 FsMITcpConTcpAORcvRnextKeyId [ ] ={1,3,6,1,4,1,29601,2,76,4,1,7};
UINT4 FsMITcpConTcpAOConnErrCtr [ ] ={1,3,6,1,4,1,29601,2,76,4,1,8};
UINT4 FsMITcpConTcpAOSndSne [ ] ={1,3,6,1,4,1,29601,2,76,4,1,9};
UINT4 FsMITcpConTcpAORcvSne [ ] ={1,3,6,1,4,1,29601,2,76,4,1,10};
UINT4 FsMITcpAoLocalAddressType [ ] ={1,3,6,1,4,1,29601,2,76,5,1,1};
UINT4 FsMITcpAoLocalAddress [ ] ={1,3,6,1,4,1,29601,2,76,5,1,2};
UINT4 FsMITcpAoLocalPort [ ] ={1,3,6,1,4,1,29601,2,76,5,1,3};
UINT4 FsMITcpAoRemAddressType [ ] ={1,3,6,1,4,1,29601,2,76,5,1,4};
UINT4 FsMITcpAoRemAddress [ ] ={1,3,6,1,4,1,29601,2,76,5,1,5};
UINT4 FsMITcpAoRemPort [ ] ={1,3,6,1,4,1,29601,2,76,5,1,6};
UINT4 FsMITcpAoContextId [ ] ={1,3,6,1,4,1,29601,2,76,5,1,7};
UINT4 FsMITcpAoConnTestLclAdrType [ ] ={1,3,6,1,4,1,29601,2,76,6,1,2};
UINT4 FsMITcpAoConnTestLclAdress [ ] ={1,3,6,1,4,1,29601,2,76,6,1,3};
UINT4 FsMITcpAoConnTestLclPort [ ] ={1,3,6,1,4,1,29601,2,76,6,1,4};
UINT4 FsMITcpAoConnTestRmtAdrType [ ] ={1,3,6,1,4,1,29601,2,76,6,1,5};
UINT4 FsMITcpAoConnTestRmtAdress [ ] ={1,3,6,1,4,1,29601,2,76,6,1,6};
UINT4 FsMITcpAoConnTestRmtPort [ ] ={1,3,6,1,4,1,29601,2,76,6,1,7};
UINT4 FsMITcpConTcpAOIcmpIgnCtr [ ] ={1,3,6,1,4,1,29601,2,76,6,1,8};
UINT4 FsMITcpConTcpAOSilentAccptCtr [ ] ={1,3,6,1,4,1,29601,2,76,6,1,9};




tMbDbEntry fsmptcMibEntry[]= {

{{10,FsMITcpGlobalTraceDebug}, NULL, FsMITcpGlobalTraceDebugGet, FsMITcpGlobalTraceDebugSet, FsMITcpGlobalTraceDebugTest, FsMITcpGlobalTraceDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMITcpContextId}, GetNextIndexFsMIContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpAckOption}, GetNextIndexFsMIContextTable, FsMITcpAckOptionGet, FsMITcpAckOptionSet, FsMITcpAckOptionTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpTimeStampOption}, GetNextIndexFsMIContextTable, FsMITcpTimeStampOptionGet, FsMITcpTimeStampOptionSet, FsMITcpTimeStampOptionTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpBigWndOption}, GetNextIndexFsMIContextTable, FsMITcpBigWndOptionGet, FsMITcpBigWndOptionSet, FsMITcpBigWndOptionTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpIncrIniWnd}, GetNextIndexFsMIContextTable, FsMITcpIncrIniWndGet, FsMITcpIncrIniWndSet, FsMITcpIncrIniWndTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpMaxNumOfTCB}, GetNextIndexFsMIContextTable, FsMITcpMaxNumOfTCBGet, FsMITcpMaxNumOfTCBSet, FsMITcpMaxNumOfTCBTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpTraceDebug}, GetNextIndexFsMIContextTable, FsMITcpTraceDebugGet, FsMITcpTraceDebugSet, FsMITcpTraceDebugTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpMaxReTries}, GetNextIndexFsMIContextTable, FsMITcpMaxReTriesGet, FsMITcpMaxReTriesSet, FsMITcpMaxReTriesTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMITcpClearStatistics}, GetNextIndexFsMIContextTable, FsMITcpClearStatisticsGet, FsMITcpClearStatisticsSet, FsMITcpClearStatisticsTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, "0"},
{{12,FsMITcpTrapAdminStatus}, GetNextIndexFsMIContextTable, FsMITcpTrapAdminStatusGet, FsMITcpTrapAdminStatusSet, FsMITcpTrapAdminStatusTest, FsMIContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMITcpConnLocalAddress}, GetNextIndexFsMITcpConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnLocalPort}, GetNextIndexFsMITcpConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRemAddress}, GetNextIndexFsMITcpConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRemPort}, GetNextIndexFsMITcpConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnOutState}, GetNextIndexFsMITcpConnTable, FsMITcpConnOutStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSWindow}, GetNextIndexFsMITcpConnTable, FsMITcpConnSWindowGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRWindow}, GetNextIndexFsMITcpConnTable, FsMITcpConnRWindowGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnCWindow}, GetNextIndexFsMITcpConnTable, FsMITcpConnCWindowGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSSThresh}, GetNextIndexFsMITcpConnTable, FsMITcpConnSSThreshGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSMSS}, GetNextIndexFsMITcpConnTable, FsMITcpConnSMSSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRMSS}, GetNextIndexFsMITcpConnTable, FsMITcpConnRMSSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSRT}, GetNextIndexFsMITcpConnTable, FsMITcpConnSRTGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRTDE}, GetNextIndexFsMITcpConnTable, FsMITcpConnRTDEGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnPersist}, GetNextIndexFsMITcpConnTable, FsMITcpConnPersistGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRexmt}, GetNextIndexFsMITcpConnTable, FsMITcpConnRexmtGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRexmtCnt}, GetNextIndexFsMITcpConnTable, FsMITcpConnRexmtCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSBCount}, GetNextIndexFsMITcpConnTable, FsMITcpConnSBCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnSBSize}, GetNextIndexFsMITcpConnTable, FsMITcpConnSBSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRBCount}, GetNextIndexFsMITcpConnTable, FsMITcpConnRBCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnRBSize}, GetNextIndexFsMITcpConnTable, FsMITcpConnRBSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpKaMainTmr}, GetNextIndexFsMITcpConnTable, FsMITcpKaMainTmrGet, FsMITcpKaMainTmrSet, FsMITcpKaMainTmrTest, FsMITcpConnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpKaRetransTmr}, GetNextIndexFsMITcpConnTable, FsMITcpKaRetransTmrGet, FsMITcpKaRetransTmrSet, FsMITcpKaRetransTmrTest, FsMITcpConnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpKaRetransCnt}, GetNextIndexFsMITcpConnTable, FsMITcpKaRetransCntGet, FsMITcpKaRetransCntSet, FsMITcpKaRetransCntTest, FsMITcpConnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITcpConnTableINDEX, 5, 0, 0, NULL},

{{12,FsMITcpConnMD5Option}, GetNextIndexFsMITcpExtConnTable, FsMITcpConnMD5OptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},

{{12,FsMITcpConnMD5ErrCtr}, GetNextIndexFsMITcpExtConnTable, FsMITcpConnMD5ErrCtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConnTcpAOOption}, GetNextIndexFsMITcpExtConnTable, FsMITcpConnTcpAOOptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAOCurKeyId}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAOCurKeyIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAORnextKeyId}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAORnextKeyIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAORcvKeyId}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAORcvKeyIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAORcvRnextKeyId}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAORcvRnextKeyIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAOConnErrCtr}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAOConnErrCtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAOSndSne}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAOSndSneGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAORcvSne}, GetNextIndexFsMITcpExtConnTable, FsMITcpConTcpAORcvSneGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITcpExtConnTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoLocalAddressType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoLocalAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoLocalPort}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoRemAddressType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoRemAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoRemPort}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoContextId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
{{12,FsMITcpAoConnTestLclAdrType}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoConnTestLclAdress}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoConnTestLclPort}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoConnTestRmtAdrType}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoConnTestRmtAdress}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpAoConnTestRmtPort}, GetNextIndexFsMITcpAoConnTestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAOIcmpIgnCtr}, GetNextIndexFsMITcpAoConnTestTable, FsMITcpConTcpAOIcmpIgnCtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
{{12,FsMITcpConTcpAOSilentAccptCtr}, GetNextIndexFsMITcpAoConnTestTable, FsMITcpConTcpAOSilentAccptCtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMITcpAoConnTestTableINDEX, 7, 0, 0, NULL},
};
tMibData fsmptcEntry = { 59, fsmptcMibEntry };

#endif /* _FSMPTCDB_H */

