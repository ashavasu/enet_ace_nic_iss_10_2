/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpipvxdefs.h,v 1.3 2014/03/11 14:13:22 siva Exp $
 *
 * Description: File containing the macros and constants used in TCP
 *
 *******************************************************************/
#ifndef TCPDEFS_H
#define TCPDEFS_H

#define    TCP_ONE                   1
#define    TCP_TWO                   2
#define    TCP_THREE                 3
#define    TCP_FOUR                  4
#define    TCP_IPV4_ADDR_TYPE        TCP_ONE
#define    TCP_IPV6_ADDR_TYPE        TCP_TWO
#define    IPVX_MAX_INET_ADDR_LEN    IPVX_IPV6_ADDR_LEN
#define    TCP_SUCCESS               TCP_ONE
#define    TCP_FAILURE              -1
#define    TCP_MIN_STATE             1
#define    TCP_MAX_STATE             12
#endif

