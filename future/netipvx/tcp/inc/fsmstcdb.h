/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmstcdb.h,v 1.1 2013/01/23 11:18:41 siva Exp $
*********************************************************************/
#ifndef _FSMSTCDB_H
#define _FSMSTCDB_H

UINT1 FsMIStdContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdTcpConnectionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdTcpListenerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmstc [] ={1,3,6,1,4,1,29601,2,75};
tSNMP_OID_TYPE fsmstcOID = {9, fsmstc};


UINT4 FsMIStdContextId [ ] ={1,3,6,1,4,1,29601,2,75,1,1,1};
UINT4 FsMIStdTcpRtoAlgorithm [ ] ={1,3,6,1,4,1,29601,2,75,1,1,2};
UINT4 FsMIStdTcpRtoMin [ ] ={1,3,6,1,4,1,29601,2,75,1,1,3};
UINT4 FsMIStdTcpRtoMax [ ] ={1,3,6,1,4,1,29601,2,75,1,1,4};
UINT4 FsMIStdTcpMaxConn [ ] ={1,3,6,1,4,1,29601,2,75,1,1,5};
UINT4 FsMIStdTcpActiveOpens [ ] ={1,3,6,1,4,1,29601,2,75,1,1,6};
UINT4 FsMIStdTcpPassiveOpens [ ] ={1,3,6,1,4,1,29601,2,75,1,1,7};
UINT4 FsMIStdTcpAttemptFails [ ] ={1,3,6,1,4,1,29601,2,75,1,1,8};
UINT4 FsMIStdTcpEstabResets [ ] ={1,3,6,1,4,1,29601,2,75,1,1,9};
UINT4 FsMIStdTcpCurrEstab [ ] ={1,3,6,1,4,1,29601,2,75,1,1,10};
UINT4 FsMIStdTcpInSegs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,11};
UINT4 FsMIStdTcpOutSegs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,12};
UINT4 FsMIStdTcpRetransSegs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,13};
UINT4 FsMIStdTcpInErrs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,14};
UINT4 FsMIStdTcpOutRsts [ ] ={1,3,6,1,4,1,29601,2,75,1,1,15};
UINT4 FsMIStdTcpHCInSegs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,16};
UINT4 FsMIStdTcpHCOutSegs [ ] ={1,3,6,1,4,1,29601,2,75,1,1,17};
UINT4 FsMIStdTcpConnectionLocalAddressType [ ] ={1,3,6,1,4,1,29601,2,75,2,1,2};
UINT4 FsMIStdTcpConnectionLocalAddress [ ] ={1,3,6,1,4,1,29601,2,75,2,1,3};
UINT4 FsMIStdTcpConnectionLocalPort [ ] ={1,3,6,1,4,1,29601,2,75,2,1,4};
UINT4 FsMIStdTcpConnectionRemAddressType [ ] ={1,3,6,1,4,1,29601,2,75,2,1,5};
UINT4 FsMIStdTcpConnectionRemAddress [ ] ={1,3,6,1,4,1,29601,2,75,2,1,6};
UINT4 FsMIStdTcpConnectionRemPort [ ] ={1,3,6,1,4,1,29601,2,75,2,1,7};
UINT4 FsMIStdTcpConnectionState [ ] ={1,3,6,1,4,1,29601,2,75,2,1,8};
UINT4 FsMIStdTcpConnectionProcess [ ] ={1,3,6,1,4,1,29601,2,75,2,1,9};
UINT4 FsMIStdTcpListenerLocalAddressType [ ] ={1,3,6,1,4,1,29601,2,75,3,1,2};
UINT4 FsMIStdTcpListenerLocalAddress [ ] ={1,3,6,1,4,1,29601,2,75,3,1,3};
UINT4 FsMIStdTcpListenerLocalPort [ ] ={1,3,6,1,4,1,29601,2,75,3,1,4};
UINT4 FsMIStdTcpListenerProcess [ ] ={1,3,6,1,4,1,29601,2,75,3,1,5};




tMbDbEntry fsmstcMibEntry[]= {

{{12,FsMIStdContextId}, GetNextIndexFsMIStdContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpRtoAlgorithm}, GetNextIndexFsMIStdContextTable, FsMIStdTcpRtoAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpRtoMin}, GetNextIndexFsMIStdContextTable, FsMIStdTcpRtoMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpRtoMax}, GetNextIndexFsMIStdContextTable, FsMIStdTcpRtoMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpMaxConn}, GetNextIndexFsMIStdContextTable, FsMIStdTcpMaxConnGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpActiveOpens}, GetNextIndexFsMIStdContextTable, FsMIStdTcpActiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpPassiveOpens}, GetNextIndexFsMIStdContextTable, FsMIStdTcpPassiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpAttemptFails}, GetNextIndexFsMIStdContextTable, FsMIStdTcpAttemptFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpEstabResets}, GetNextIndexFsMIStdContextTable, FsMIStdTcpEstabResetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpCurrEstab}, GetNextIndexFsMIStdContextTable, FsMIStdTcpCurrEstabGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpInSegs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpInSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpOutSegs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpOutSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpRetransSegs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpRetransSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpInErrs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpInErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpOutRsts}, GetNextIndexFsMIStdContextTable, FsMIStdTcpOutRstsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpHCInSegs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpHCInSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpHCOutSegs}, GetNextIndexFsMIStdContextTable, FsMIStdTcpHCOutSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdTcpConnectionLocalAddressType}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionLocalAddress}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionLocalPort}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionRemAddressType}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionRemAddress}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionRemPort}, GetNextIndexFsMIStdTcpConnectionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionState}, GetNextIndexFsMIStdTcpConnectionTable, FsMIStdTcpConnectionStateGet, FsMIStdTcpConnectionStateSet, FsMIStdTcpConnectionStateTest, FsMIStdTcpConnectionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpConnectionProcess}, GetNextIndexFsMIStdTcpConnectionTable, FsMIStdTcpConnectionProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdTcpConnectionTableINDEX, 7, 0, 0, NULL},

{{12,FsMIStdTcpListenerLocalAddressType}, GetNextIndexFsMIStdTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdTcpListenerTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdTcpListenerLocalAddress}, GetNextIndexFsMIStdTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdTcpListenerTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdTcpListenerLocalPort}, GetNextIndexFsMIStdTcpListenerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdTcpListenerTableINDEX, 4, 0, 0, NULL},

{{12,FsMIStdTcpListenerProcess}, GetNextIndexFsMIStdTcpListenerTable, FsMIStdTcpListenerProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdTcpListenerTableINDEX, 4, 0, 0, NULL},
};
tMibData fsmstcEntry = { 29, fsmstcMibEntry };

#endif /* _FSMSTCDB_H */

