#ifndef _STDTCPWR_H
#define _STDTCPWR_H

VOID RegisterSTDTCP(VOID);

VOID UnRegisterSTDTCP(VOID);
INT4 TcpRtoAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 TcpRtoMinGet(tSnmpIndex *, tRetVal *);
INT4 TcpRtoMaxGet(tSnmpIndex *, tRetVal *);
INT4 TcpMaxConnGet(tSnmpIndex *, tRetVal *);
INT4 TcpActiveOpensGet(tSnmpIndex *, tRetVal *);
INT4 TcpPassiveOpensGet(tSnmpIndex *, tRetVal *);
INT4 TcpAttemptFailsGet(tSnmpIndex *, tRetVal *);
INT4 TcpEstabResetsGet(tSnmpIndex *, tRetVal *);
INT4 TcpCurrEstabGet(tSnmpIndex *, tRetVal *);
INT4 TcpInSegsGet(tSnmpIndex *, tRetVal *);
INT4 TcpOutSegsGet(tSnmpIndex *, tRetVal *);
INT4 TcpRetransSegsGet(tSnmpIndex *, tRetVal *);
INT4 TcpInErrsGet(tSnmpIndex *, tRetVal *);
INT4 TcpOutRstsGet(tSnmpIndex *, tRetVal *);
INT4 TcpHCInSegsGet(tSnmpIndex *, tRetVal *);
INT4 TcpHCOutSegsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexTcpConnectionTable(tSnmpIndex *, tSnmpIndex *);
INT4 TcpConnectionStateGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnectionProcessGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnectionStateSet(tSnmpIndex *, tRetVal *);
INT4 TcpConnectionStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 TcpConnectionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexTcpListenerTable(tSnmpIndex *, tSnmpIndex *);
INT4 TcpListenerProcessGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexTcpConnTable(tSnmpIndex *, tSnmpIndex *);
INT4 TcpConnStateGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnLocalAddressGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnLocalPortGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnRemAddressGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnRemPortGet(tSnmpIndex *, tRetVal *);
INT4 TcpConnStateSet(tSnmpIndex *, tRetVal *);
INT4 TcpConnStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 TcpConnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDTCPWR_H */
