/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmptcwr.h,v 1.3 2013/06/07 13:29:59 siva Exp $
*
* Description: Proto types for Wrapper Routines
*********************************************************************/
#ifndef _FSMPTCWR_H
#define _FSMPTCWR_H

VOID RegisterFSMPTC(VOID);

VOID UnRegisterFSMPTC(VOID);
INT4 FsMITcpGlobalTraceDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpGlobalTraceDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpGlobalTraceDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpGlobalTraceDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMIContextTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMITcpAckOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTimeStampOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpBigWndOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpIncrIniWndGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxNumOfTCBGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTraceDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxReTriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpClearStatisticsGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTrapAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpAckOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTimeStampOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpBigWndOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpIncrIniWndSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxNumOfTCBSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTraceDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxReTriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpClearStatisticsSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpTrapAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpAckOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpTimeStampOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpBigWndOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpIncrIniWndTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxNumOfTCBTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpTraceDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpMaxReTriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpClearStatisticsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpTrapAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMITcpConnTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMITcpConnOutStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSWindowGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRWindowGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnCWindowGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSSThreshGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSMSSGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRMSSGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSRTGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRTDEGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnPersistGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRexmtGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRexmtCntGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSBCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnSBSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRBCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnRBSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaMainTmrGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransTmrGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransCntGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaMainTmrSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransTmrSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransCntSet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaMainTmrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransTmrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpKaRetransCntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMITcpExtConnTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMITcpConnMD5OptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnMD5ErrCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConnTcpAOOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAOCurKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAORnextKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAORcvKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAORcvRnextKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAOConnErrCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAOSndSneGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAORcvSneGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMITcpAoConnTestTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMITcpConTcpAOIcmpIgnCtrGet(tSnmpIndex *, tRetVal *);
INT4 FsMITcpConTcpAOSilentAccptCtrGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMPTCWR_H */
