/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmstclw.h,v 1.2 2013/03/16 12:10:51 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
/* Proto Type for Low Level GET FIRST fn for FsMIStdContextTable  */

INT1
nmhGetFirstIndexFsMIStdContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdContextTable ARG_LIST((INT4 , INT4 *));

#ifndef LNXIP_WANTED
/* Proto Validate Index Instance for FsMIStdContextTable. */
INT1
nmhValidateIndexInstanceFsMIStdContextTable ARG_LIST((INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdTcpRtoAlgorithm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdTcpRtoMin ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdTcpRtoMax ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdTcpMaxConn ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdTcpActiveOpens ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpPassiveOpens ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpAttemptFails ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpEstabResets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpCurrEstab ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpInSegs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpOutSegs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpRetransSegs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpInErrs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpOutRsts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdTcpHCInSegs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdTcpHCOutSegs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsMIStdTcpConnectionTable. */
INT1
nmhValidateIndexInstanceFsMIStdTcpConnectionTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdTcpConnectionTable  */

INT1
nmhGetFirstIndexFsMIStdTcpConnectionTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdTcpConnectionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdTcpConnectionState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdTcpConnectionProcess ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdTcpConnectionState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdTcpConnectionState ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdTcpConnectionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdTcpListenerTable. */
INT1
nmhValidateIndexInstanceFsMIStdTcpListenerTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdTcpListenerTable  */

INT1
nmhGetFirstIndexFsMIStdTcpListenerTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdTcpListenerTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdTcpListenerProcess ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

#endif
