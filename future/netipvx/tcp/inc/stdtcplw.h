/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtcplw.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpRtoAlgorithm ARG_LIST((INT4 *));

INT1
nmhGetTcpRtoMin ARG_LIST((INT4 *));

INT1
nmhGetTcpRtoMax ARG_LIST((INT4 *));

INT1
nmhGetTcpMaxConn ARG_LIST((INT4 *));

INT1
nmhGetTcpActiveOpens ARG_LIST((UINT4 *));

INT1
nmhGetTcpPassiveOpens ARG_LIST((UINT4 *));

INT1
nmhGetTcpAttemptFails ARG_LIST((UINT4 *));

INT1
nmhGetTcpEstabResets ARG_LIST((UINT4 *));

INT1
nmhGetTcpCurrEstab ARG_LIST((UINT4 *));

INT1
nmhGetTcpInSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpOutSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpRetransSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpInErrs ARG_LIST((UINT4 *));

INT1
nmhGetTcpOutRsts ARG_LIST((UINT4 *));

INT1
nmhGetTcpHCInSegs ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetTcpHCOutSegs ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for TcpConnectionTable. */
INT1
nmhValidateIndexInstanceTcpConnectionTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for TcpConnectionTable  */

INT1
nmhGetFirstIndexTcpConnectionTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTcpConnectionTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpConnectionState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetTcpConnectionProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTcpConnectionState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TcpConnectionState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TcpConnectionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for TcpListenerTable. */
INT1
nmhValidateIndexInstanceTcpListenerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for TcpListenerTable  */

INT1
nmhGetFirstIndexTcpListenerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTcpListenerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpListenerProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for TcpConnTable. */
INT1
nmhValidateIndexInstanceTcpConnTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for TcpConnTable  */

INT1
nmhGetFirstIndexTcpConnTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTcpConnTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpConnState ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTcpConnState ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TcpConnState ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TcpConnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
