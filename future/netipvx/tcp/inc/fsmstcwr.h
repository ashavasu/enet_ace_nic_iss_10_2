/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmstcwr.h,v 1.1 2013/01/23 11:18:41 siva Exp $
*
* Description: Proto types for Wrapper  Routines
*********************************************************************/
#ifndef _FSMSTCWR_H
#define _FSMSTCWR_H
INT4 GetNextIndexFsMIStdContextTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMSTC(VOID);

VOID UnRegisterFSMSTC(VOID);
INT4 FsMIStdTcpRtoAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpRtoMinGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpRtoMaxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpMaxConnGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpActiveOpensGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpPassiveOpensGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpAttemptFailsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpEstabResetsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpCurrEstabGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpInSegsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpOutSegsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpRetransSegsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpInErrsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpOutRstsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpHCInSegsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpHCOutSegsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdTcpConnectionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdTcpConnectionStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpConnectionProcessGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpConnectionStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpConnectionStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdTcpConnectionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdTcpListenerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdTcpListenerProcessGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMSTCWR_H */
