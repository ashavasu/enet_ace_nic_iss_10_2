/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: tcputils.h,v 1.6 2013/03/12 12:13:11 siva Exp $
 *
 * Description: This file contains tcp utility function proto types
 *
 *******************************************************************/
#ifndef __TCPCLIUTIL_H__
#define __TCPCLIUTIL_H__

INT1  TcpGetNextActiveContextID (UINT4 u4ContextID, UINT4* pu4NextContextID);
INT1  TcpGetFirstActiveContextID (UINT4* pu4RetValContextID);

#define TCP_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
        if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
            MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}

#define TCP_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
        MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
               sizeof(UINT4));\
        pOctetString->i4_Length=sizeof(UINT4);}
        

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)       
INT4 tcpGetConnID (UINT4, INT4, UINT4, INT4);
VOID IpvxAddrCopy (tIpAddr *pAddrdst, tIpAddr *pAddrsrc);
INT4 IpvxAddrCmp (tIpAddr *pAddrsrc, tIpAddr *pAddrdst);

INT4
CheckForIpv6Entry (UINT4 *pu4TcpListenerLocalPort);
INT4 CheckForIpv4Entry (UINT4 *pu4TcpListenerLocalPort);
INT4 CheckForAddrTypeZeroEntry
           (UINT4 u4Context,
         INT4 i4TcpListenerLocalAddressType ,
            INT4 *pi4NextTcpListenerLocalAddressType ,
            tSNMP_OCTET_STRING_TYPE *pTcpListenerLocalAddress ,
            tSNMP_OCTET_STRING_TYPE * pNextTcpListenerLocalAddress ,
            UINT4 u4TcpListenerLocalPort ,
            UINT4 *pu4NextTcpListenerLocalPort);
INT4 CheckForAddrTypeOneAndTwoEntry
           (UINT4 u4Context,
         INT4 i4TcpListenerLocalAddressType ,
            INT4 *pi4NextTcpListenerLocalAddressType ,
            tSNMP_OCTET_STRING_TYPE *pTcpListenerLocalAddress ,
            tSNMP_OCTET_STRING_TYPE * pNextTcpListenerLocalAddress ,
            UINT4 u4TcpListenerLocalPort ,
            UINT4 *pu4NextTcpListenerLocalPort);
INT4
FillLengthOfIp (INT4 i4AddrType,
                tSNMP_OCTET_STRING_TYPE *pIpAddress);
#endif
#endif
#endif
