/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmptclw.h,v 1.3 2013/06/07 13:29:59 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITcpGlobalTraceDebug ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMITcpGlobalTraceDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMITcpGlobalTraceDebug ARG_LIST((UINT4 *, INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMITcpGlobalTraceDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIContextTable. */
INT1
nmhValidateIndexInstanceFsMIContextTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIContextTable  */

INT1
nmhGetFirstIndexFsMIContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIContextTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITcpAckOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpTimeStampOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpBigWndOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpIncrIniWnd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpMaxNumOfTCB ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpTraceDebug ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMITcpMaxReTries ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMITcpClearStatistics ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMITcpTrapAdminStatus ARG_LIST((INT4 ,INT4 *));
INT1
nmhSetFsMITcpAckOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpTimeStampOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpBigWndOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpIncrIniWnd ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpMaxNumOfTCB ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpTraceDebug ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpMaxReTries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMITcpClearStatistics ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMITcpTrapAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMITcpAckOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpTimeStampOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpBigWndOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpIncrIniWnd ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpMaxNumOfTCB ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpTraceDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpMaxReTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpClearStatistics ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpTrapAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMITcpConnTable. */
INT1
nmhValidateIndexInstanceFsMITcpConnTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMITcpConnTable  */

INT1
nmhGetFirstIndexFsMITcpConnTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMITcpConnTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITcpConnOutState ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSWindow ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRWindow ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnCWindow ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSSThresh ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSMSS ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRMSS ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSRT ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRTDE ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnPersist ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRexmt ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRexmtCnt ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSBCount ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnSBSize ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRBCount ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpConnRBSize ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpKaMainTmr ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpKaRetransTmr ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITcpKaRetransCnt ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMITcpKaMainTmr ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITcpKaRetransTmr ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITcpKaRetransCnt ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMITcpKaMainTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpKaRetransTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITcpKaRetransCnt ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMITcpConnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMITcpExtConnTable. */
INT1
nmhValidateIndexInstanceFsMITcpExtConnTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMITcpExtConnTable  */

INT1
nmhGetFirstIndexFsMITcpExtConnTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMITcpExtConnTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITcpConnMD5Option ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConnMD5ErrCtr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
INT1
nmhGetFsMITcpConnTcpAOOption ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAOCurKeyId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAORnextKeyId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAORcvKeyId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAORcvRnextKeyId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAOConnErrCtr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMITcpConTcpAOSndSne ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMITcpConTcpAORcvSne ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMITcpAoConnTestTable. */
INT1
nmhValidateIndexInstanceFsMITcpAoConnTestTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMITcpAoConnTestTable  */

INT1
nmhGetFirstIndexFsMITcpAoConnTestTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMITcpAoConnTestTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITcpConTcpAOIcmpIgnCtr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMITcpConTcpAOSilentAccptCtr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));
