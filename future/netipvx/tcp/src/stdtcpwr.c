/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: stdtcpwr.c,v 1.2 2013/01/23 11:45:32 siva Exp $
 *
 * Description: Protocol Wrapper Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdtcplw.h"
# include  "stdtcpwr.h"
# include  "stdtcpdb.h"

# include  "tcpinc.h"
# include  "tcputils.h"

VOID
RegisterSTDTCP ()
{
    SNMPRegisterMibWithContextIdAndLock (&stdtcpOID, &stdtcpEntry,
                                         TcpLock, TcpUnLock,
                                         TcpSetContext, TcpReleaseContext,
                                         SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdtcpOID, (const UINT1 *) "stdtcpipvx");
}

VOID
UnRegisterSTDTCP ()
{
    SNMPUnRegisterMib (&stdtcpOID, &stdtcpEntry);
    SNMPDelSysorEntry (&stdtcpOID, (const UINT1 *) "stdtcpipvx");
}

INT4
TcpRtoAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoAlgorithm (&(pMultiData->i4_SLongValue)));
}

INT4
TcpRtoMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoMin (&(pMultiData->i4_SLongValue)));
}

INT4
TcpRtoMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoMax (&(pMultiData->i4_SLongValue)));
}

INT4
TcpMaxConnGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpMaxConn (&(pMultiData->i4_SLongValue)));
}

INT4
TcpActiveOpensGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpActiveOpens (&(pMultiData->u4_ULongValue)));
}

INT4
TcpPassiveOpensGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpPassiveOpens (&(pMultiData->u4_ULongValue)));
}

INT4
TcpAttemptFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpAttemptFails (&(pMultiData->u4_ULongValue)));
}

INT4
TcpEstabResetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpEstabResets (&(pMultiData->u4_ULongValue)));
}

INT4
TcpCurrEstabGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpCurrEstab (&(pMultiData->u4_ULongValue)));
}

INT4
TcpInSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpInSegs (&(pMultiData->u4_ULongValue)));
}

INT4
TcpOutSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpOutSegs (&(pMultiData->u4_ULongValue)));
}

INT4
TcpRetransSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRetransSegs (&(pMultiData->u4_ULongValue)));
}

INT4
TcpInErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpInErrs (&(pMultiData->u4_ULongValue)));
}

INT4
TcpOutRstsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpOutRsts (&(pMultiData->u4_ULongValue)));
}

INT4
TcpHCInSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpHCInSegs (&(pMultiData->u8_Counter64Value)));
}

INT4
TcpHCOutSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpHCOutSegs (&(pMultiData->u8_Counter64Value)));
}

INT4
GetNextIndexTcpConnectionTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTcpConnectionTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTcpConnectionTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TcpConnectionStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTcpConnectionState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      pMultiIndex->pIndex[5].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
TcpConnectionProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnectionTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTcpConnectionProcess (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].pOctetStrValue,
                                        pMultiIndex->pIndex[5].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
TcpConnectionStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTcpConnectionState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      pMultiIndex->pIndex[5].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
TcpConnectionStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2TcpConnectionState (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         pMultiIndex->pIndex[4].pOctetStrValue,
                                         pMultiIndex->pIndex[5].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
TcpConnectionTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TcpConnectionTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexTcpListenerTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTcpListenerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTcpListenerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TcpListenerProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpListenerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTcpListenerProcess (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexTcpConnTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTcpConnTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTcpConnTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
TcpConnStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTcpConnState (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
TcpConnLocalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
TcpConnLocalPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
TcpConnRemAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[2].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
TcpConnRemPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[3].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
TcpConnStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTcpConnState (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
TcpConnStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2TcpConnState (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
TcpConnTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TcpConnTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
