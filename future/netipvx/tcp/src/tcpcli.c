/** $Id: tcpcli.c,v 1.11 2013/06/07 13:30:00 siva Exp $ */

#ifndef __TCPCLI_C__
#define __TCPCLI_C__

# include "tcpinc.h"
# include "tcpcli.h"
# include "stdtcplw.h"
# include "tcputils.h"
# include "tcpipvxdefs.h"
# include "fsmptclw.h"
/* 
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : tcpcli.c                                        |
 * |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                      |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : TE                                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    : Linux (Portable)                                 |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for TCP CLI commands             | * |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

INT1
cli_process_tcp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS];
    INT1                argno = TCP_ZERO;
    INT4                i4RetStatus = TCP_ZERO;
    INT4                i4ShowRetStatus = TCP_ZERO;    /* Return status of show cmds */
    UINT1              *pu1CxtName = NULL;
    UINT4               u4ContextID = VCM_INVALID_VC;
    UINT4               u4PrevCxt = VCM_INVALID_VC;
#ifndef LNXIP4_WANTED
    INT4                i4RetClearStatus = TCP_ZERO;
    UINT4               u4ErrorCode = 0;
#endif
    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 5 arguements at the max. This is because igmp commands do not
     * take more than 5 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == TCP_MAX_ARGS)
            break;
    }

    va_end (ap);
    CLI_SET_ERR (TCP_ZERO);

    pu1CxtName = (UINT1 *) args[1];
    if (pu1CxtName == NULL)
    {
        u4ContextID = VCM_INVALID_VC;
    }
    else
    {
        i4RetStatus = VcmIsVrfExist (pu1CxtName, &u4ContextID);
        if (VCM_FALSE == i4RetStatus)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF name\r\n");
            CLI_SET_CMD_STATUS (i4RetStatus);
            return CLI_SUCCESS;
        }

    }

    CliRegisterLock (CliHandle, TcpLock, TcpUnLock);
    if (SNMP_FAILURE == TcpLock ())
    {
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case TCP_CLI_SHOW_STATS:
            if (VCM_INVALID_VC != u4ContextID)    /* Display for given Context */
            {
                if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                {
                    break;
                }
                i4ShowRetStatus = TcpShowStats (CliHandle);
                TcpReleaseContext ();
            }
            else                /* No VR name input by user. Display all VRs */
            {
                i4RetStatus = TcpGetFirstActiveContextID (&u4ContextID);
                while (i4RetStatus != SNMP_FAILURE)
                {
                    if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                    {
                        break;
                    }
                    i4ShowRetStatus = TcpShowStats (CliHandle);
                    TcpReleaseContext ();
                    u4PrevCxt = u4ContextID;
                    i4RetStatus =
                        TcpGetNextActiveContextID (u4PrevCxt, &u4ContextID);
                }
            }
            break;

        case TCP_CLI_SHOW_CONN:
            if (VCM_INVALID_VC != u4ContextID)    /* Display for given Context */
            {
                if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                {
                    break;
                }
                i4ShowRetStatus = TcpShowConnections (CliHandle);
                TcpReleaseContext ();
            }
            else                /* No VR name input by user. Display all VRs */
            {
                i4RetStatus = TcpGetFirstActiveContextID (&u4ContextID);
                while (i4RetStatus != SNMP_FAILURE)
                {
                    if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                    {
                        break;
                    }
                    i4ShowRetStatus = TcpShowConnections (CliHandle);
                    TcpReleaseContext ();
                    u4PrevCxt = u4ContextID;
                    i4RetStatus =
                        TcpGetNextActiveContextID (u4PrevCxt, &u4ContextID);
                }
            }
            break;

        case TCP_CLI_SHOW_LIST:
            CliPrintf (CliHandle, "\nTCP Listeners\r\n");
            CliPrintf (CliHandle, " ===============\r\n");
            if (VCM_INVALID_VC != u4ContextID)    /* Display for given Context */
            {
                if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                {
                    break;
                }
                i4RetStatus = TcpShowListener (CliHandle);
                TcpReleaseContext ();
            }
            else                /* No VR name input by user. Display all VRs */
            {
                i4RetStatus = TcpGetFirstActiveContextID (&u4ContextID);
                while (i4RetStatus != SNMP_FAILURE)
                {
                    if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                    {
                        break;
                    }
                    i4RetStatus = TcpShowListener (CliHandle);
                    TcpReleaseContext ();
                    u4PrevCxt = u4ContextID;
                    i4RetStatus =
                        TcpGetNextActiveContextID (u4PrevCxt, &u4ContextID);
                }
            }
            break;

        case TCP_CLI_SHOW_RTO:
            if (VCM_INVALID_VC != u4ContextID)    /* Display for given Context */
            {
                if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                {
                    break;
                }
                i4RetStatus = TcpShowRetransmissionDetails (CliHandle);
                TcpReleaseContext ();
            }
            else                /* No VR name input by user. Display all VRs */
            {
                i4RetStatus = TcpGetFirstActiveContextID (&u4ContextID);
                while (i4RetStatus != SNMP_FAILURE)
                {
                    if (SNMP_FAILURE == TcpSetContext (u4ContextID))
                    {
                        break;
                    }
                    i4RetStatus = TcpShowRetransmissionDetails (CliHandle);
                    TcpReleaseContext ();
                    u4PrevCxt = u4ContextID;
                    i4RetStatus =
                        TcpGetNextActiveContextID (u4PrevCxt, &u4ContextID);
                }
            }
            break;
        case TCP_CLI_CLEAR_STATS:
#ifndef LNXIP4_WANTED
            if (VCM_INVALID_VC != u4ContextID)    /* Display for given Context */
            {
                i4RetClearStatus = SNMP_SUCCESS;
                if (SNMP_FAILURE ==
                    nmhTestv2FsMITcpClearStatistics (&u4ErrorCode, u4ContextID,
                                                     i4RetClearStatus))
                {
                    break;
                }
                else
                {
                    i4RetStatus =
                        nmhSetFsMITcpClearStatistics (u4ContextID,
                                                      i4RetClearStatus);
                }
            }
            else                /* No VR name input by user. Display all VRs */
            {
                i4RetStatus = TcpGetFirstActiveContextID (&u4ContextID);
                i4RetClearStatus = SNMP_SUCCESS;
                while (i4RetStatus != SNMP_FAILURE)
                {
                    if (SNMP_FAILURE ==
                        nmhTestv2FsMITcpClearStatistics (&u4ErrorCode,
                                                         u4ContextID,
                                                         i4RetClearStatus))
                    {
                        break;
                    }
                    else
                    {
                        i4RetStatus =
                            nmhSetFsMITcpClearStatistics (u4ContextID,
                                                          i4RetClearStatus);
                    }
                    u4PrevCxt = u4ContextID;
                    i4RetStatus =
                        TcpGetNextActiveContextID (u4PrevCxt, &u4ContextID);
                }
            }
#else
            CliPrintf (CliHandle,
                       "Clear TCP statistics option not available\r\n");
#endif
            break;
        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            return CLI_ERROR;
    }

    CLI_SET_CMD_STATUS (i4ShowRetStatus);
    TcpUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * Name               : TcpShowStats
 *
 * Description        : This function displays TCP statistics
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
TcpShowStats (tCliHandle CliHandle)
{
    INT4                i4TcpMaxConn = TCP_ZERO;
    UINT4               u4TcpActiveOpens = TCP_ZERO;
    UINT4               u4TcpPassiveOpens = TCP_ZERO;
    UINT4               u4TcpAttemptFails = TCP_ZERO;
    UINT4               u4TcpEstabResets = TCP_ZERO;
    UINT4               u4TcpCurrEstab = TCP_ZERO;
    UINT4               u4TcpInSegs = TCP_ZERO;
    UINT4               u4TcpOutSegs = TCP_ZERO;
    UINT4               u4TcpRetransSegs = TCP_ZERO;
    UINT4               u4TcpInErrs = TCP_ZERO;
    UINT4               u4TcpOutRsts = TCP_ZERO;
    UINT1               au1Str[CFA_CLI_U8_STR_LENGTH];
    FS_UINT8            u8TcpHCInSegs;
    FS_UINT8            u8TcpHCOutSegs;
    tSNMP_COUNTER64_TYPE RetVal64HCInSegs;
    tSNMP_COUNTER64_TYPE RetVal64HCOutSegs;
#ifndef LNXIP4_WANTED
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    MEMSET (au1CxtName, TCP_ZERO, VCMALIAS_MAX_ARRAY_LEN);
    VcmGetAliasName (gpTcpCurrContext->u4ContextId, au1CxtName);
    CliPrintf (CliHandle, "\n Context Name : %s\r\n", au1CxtName);
#endif

    FSAP_U8_CLR (&(u8TcpHCInSegs));
    FSAP_U8_CLR (&(u8TcpHCOutSegs));
    MEMSET (&RetVal64HCInSegs, TCP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetVal64HCOutSegs, TCP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (au1Str, TCP_ZERO, CFA_CLI_U8_STR_LENGTH);

    nmhGetTcpMaxConn (&i4TcpMaxConn);
    nmhGetTcpActiveOpens (&u4TcpActiveOpens);
    nmhGetTcpPassiveOpens (&u4TcpPassiveOpens);
    nmhGetTcpAttemptFails (&u4TcpAttemptFails);
    nmhGetTcpEstabResets (&u4TcpEstabResets);
    nmhGetTcpCurrEstab (&u4TcpCurrEstab);
    nmhGetTcpInSegs (&u4TcpInSegs);
    nmhGetTcpOutSegs (&u4TcpOutSegs);
    nmhGetTcpRetransSegs (&u4TcpRetransSegs);
    nmhGetTcpInErrs (&u4TcpInErrs);
    nmhGetTcpOutRsts (&u4TcpOutRsts);
    nmhGetTcpHCInSegs (&RetVal64HCInSegs);
    nmhGetTcpHCOutSegs (&RetVal64HCOutSegs);

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, " Max Connections : %d\r\n", i4TcpMaxConn);
    CliPrintf (CliHandle, " Active Opens : %u\r\n", u4TcpActiveOpens);
    CliPrintf (CliHandle, " Passive Opens : %u\r\n", u4TcpPassiveOpens);
    CliPrintf (CliHandle, " Attempts Fail : %u\r\n", u4TcpAttemptFails);
    CliPrintf (CliHandle, " Estab Resets : %u\r\n", u4TcpEstabResets);
    CliPrintf (CliHandle, " Current Estab : %u\r\n", u4TcpCurrEstab);
    CliPrintf (CliHandle, " Input Segments : %u\r\n", u4TcpInSegs);
    CliPrintf (CliHandle, " Output Segments : %u\r\n", u4TcpOutSegs);
    CliPrintf (CliHandle, " Retransmitted Segments : %u\r\n", u4TcpRetransSegs);
    CliPrintf (CliHandle, " Input Errors : %u\r\n", u4TcpInErrs);
    CliPrintf (CliHandle,
               " TCP Segments with RST flag Set: %u\r\n", u4TcpOutRsts);

    u8TcpHCInSegs.u4Hi = RetVal64HCInSegs.msn;
    u8TcpHCInSegs.u4Lo = RetVal64HCInSegs.lsn;
    FSAP_U8_2STR (&u8TcpHCInSegs, (CHR1 *) au1Str);
    CliPrintf (CliHandle, " HC Input Segments : %s\r\n", au1Str);

    MEMSET (au1Str, 0, CFA_CLI_U8_STR_LENGTH);
    u8TcpHCOutSegs.u4Hi = RetVal64HCOutSegs.msn;
    u8TcpHCOutSegs.u4Lo = RetVal64HCOutSegs.lsn;
    FSAP_U8_2STR (&u8TcpHCOutSegs, (CHR1 *) au1Str);
    CliPrintf (CliHandle, " HC Output Segments : %s\r\n", au1Str);
    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/***************************************************************************
 * Name               : TcpShowConnections
 *
 * Description        : This function displays TCP Connections
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
TcpShowConnections (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    INT1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4TcpCurrConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpCurrConnRemAddressType = TCP_ZERO;
    INT4                i4TcpNextConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnRemAddressType = TCP_ZERO;
    INT4                i4ConnState = TCP_ZERO;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TcpCurrConnLocalPort = TCP_ZERO;
    UINT4               u4TcpCurrConnRemPort = TCP_ZERO;
    UINT4               u4TcpNextConnLocalPort = TCP_ZERO;
    UINT4               u4TcpNextConnRemPort = TCP_ZERO;
    UINT4               u4LocalIp = TCP_ZERO;
    UINT4               u4RemoteIp = TCP_ZERO;
    UINT4               u1IsShow = TCP_TRUE;
#ifndef LNXIP4_WANTED
    INT4                i4Md5Authenticated = TCP_ZERO;
    INT4                i4Md5InErrCtr = TCP_ZERO;
    INT4                i4TcpAoAuth = TCP_ZERO;
    INT4                i4TcpAoCurKeyId = TCP_ZERO;
    INT4                i4RNextKeyId = TCP_ZERO;
    INT4                i4RcvKeyId = TCP_ZERO;
    INT4                i4RcvRNextKeyId = TCP_ZERO;
    UINT4               u4TcpAoAuthErr = TCP_ZERO;
    INT4                i4TcpAoSndSne = TCP_ZERO;
    INT4                i4TcpAoRcvSne = TCP_ZERO;
#endif

    tSNMP_OCTET_STRING_TYPE TcpNextConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnRemAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    CONST CHR1         *tcpstate[] = {
        "Closed",
        "Listen",
        "SynSent",
        "SynReceived",
        "Established",
        "FinWait1",
        "FinWait2",
        "CloseWait",
        "LastAck",
        "Closing",
        "TimeWait",
        "DeleteTCB"
    };

#ifndef LNXIP4_WANTED
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    MEMSET (au1CxtName, TCP_ZERO, VCMALIAS_MAX_ARRAY_LEN);
    VcmGetAliasName (gpTcpCurrContext->u4ContextId, au1CxtName);
    CliPrintf (CliHandle, " Context Name : %s\r\n", au1CxtName);
#endif

    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;
    TcpCurrConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnLocalAddress.pu1_OctetList = au1NextLocalIp;
    TcpNextConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnRemAddress.pu1_OctetList = au1NextRemoteIp;
    TcpNextConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (nmhGetFirstIndexTcpConnectionTable
        (&i4TcpNextConnLocalAddressType,
         &TcpNextConnLocalAddress,
         &u4TcpNextConnLocalPort,
         &i4TcpNextConnRemAddressType,
         &TcpNextConnRemAddress, &u4TcpNextConnRemPort) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (SNMP_FAILURE ==
            nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4ConnState))
        {

            break;
        }
#ifndef LNXIP4_WANTED
        nmhGetFsTcpConnMD5Option (i4TcpNextConnLocalAddressType,
                                  &TcpNextConnLocalAddress,
                                  u4TcpNextConnLocalPort,
                                  i4TcpNextConnRemAddressType,
                                  &TcpNextConnRemAddress,
                                  u4TcpNextConnRemPort, &i4Md5Authenticated);
        nmhGetFsTcpConnTcpAOOption (i4TcpNextConnLocalAddressType,
                                    &TcpNextConnLocalAddress,
                                    u4TcpNextConnLocalPort,
                                    i4TcpNextConnRemAddressType,
                                    &TcpNextConnRemAddress,
                                    u4TcpNextConnRemPort, &i4TcpAoAuth);
#endif
        CliPrintf (CliHandle, "\n TCP Connections\r\n");
        CliPrintf (CliHandle, " ===============\r\n\n");

        if (i4TcpNextConnLocalAddressType == IPV4_ADD_TYPE)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv4\r\n");
            MEMCPY ((UINT1 *) &u4LocalIp,
                    TcpNextConnLocalAddress.pu1_OctetList,
                    TcpNextConnLocalAddress.i4_Length);
            u4LocalIp = OSIX_NTOHL (u4LocalIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocalIp);
            CliPrintf (CliHandle, "Local IP               : %s\r\n", pu1String);
        }
        else
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv6\r\n");
            MEMCPY (au1Ip6Addr, TcpNextConnLocalAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Local IP               : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        CliPrintf (CliHandle, "Local Port             : %d\r\n",
                   u4TcpNextConnLocalPort);

        if (i4TcpNextConnRemAddressType == IPV4_ADD_TYPE)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv4\r\n");
            MEMCPY ((UINT1 *) &u4RemoteIp,
                    TcpNextConnRemAddress.pu1_OctetList,
                    TcpNextConnRemAddress.i4_Length);
            u4RemoteIp = OSIX_NTOHL (u4RemoteIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RemoteIp);
            CliPrintf (CliHandle, "Remote IP              : %s\r\n", pu1String);
        }
        else
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv6\r\n");
            MEMCPY (au1Ip6Addr, TcpNextConnRemAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Remote IP              : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        CliPrintf (CliHandle, "Remote Port            : %d\r\n",
                   u4TcpNextConnRemPort);
        if (i4ConnState > TCP_MIN_STATE && i4ConnState < TCP_MAX_STATE)
        {
            CliPrintf (CliHandle, "TCP State              : %s\r\n",
                       tcpstate[i4ConnState - 1]);
        }
#ifndef LNXIP4_WANTED
        if (i4Md5Authenticated == TCP_MD5CONF)
        {
            nmhGetFsTcpConnMD5ErrCtr (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4Md5InErrCtr);

            CliPrintf (CliHandle, "MD5 Authenticated      : Yes\r\n");
            CliPrintf (CliHandle, "In MD5 Auth Failures   : %d\r\n",
                       i4Md5InErrCtr);
        }
        else
        {
            CliPrintf (CliHandle, "MD5 Authenticated      : No\r\n");
        }
        if (i4TcpAoAuth == TRUE)
        {
            CliPrintf (CliHandle, "TCP-AO Authenticated   : Yes\r\n");
            nmhGetFsTcpConTcpAOCurKeyId (i4TcpNextConnLocalAddressType,
                                         &TcpNextConnLocalAddress,
                                         u4TcpNextConnLocalPort,
                                         i4TcpNextConnRemAddressType,
                                         &TcpNextConnRemAddress,
                                         u4TcpNextConnRemPort,
                                         &i4TcpAoCurKeyId);
            nmhGetFsTcpConTcpAORnextKeyId (i4TcpNextConnLocalAddressType,
                                           &TcpNextConnLocalAddress,
                                           u4TcpNextConnLocalPort,
                                           i4TcpNextConnRemAddressType,
                                           &TcpNextConnRemAddress,
                                           u4TcpNextConnRemPort, &i4RNextKeyId);
            nmhGetFsTcpConTcpAORcvKeyId (i4TcpNextConnLocalAddressType,
                                         &TcpNextConnLocalAddress,
                                         u4TcpNextConnLocalPort,
                                         i4TcpNextConnRemAddressType,
                                         &TcpNextConnRemAddress,
                                         u4TcpNextConnRemPort, &i4RcvKeyId);
            nmhGetFsTcpConTcpAORcvRnextKeyId (i4TcpNextConnLocalAddressType,
                                              &TcpNextConnLocalAddress,
                                              u4TcpNextConnLocalPort,
                                              i4TcpNextConnRemAddressType,
                                              &TcpNextConnRemAddress,
                                              u4TcpNextConnRemPort,
                                              &i4RcvRNextKeyId);
            nmhGetFsTcpConTcpAOConnErrCtr (i4TcpNextConnLocalAddressType,
                                           &TcpNextConnLocalAddress,
                                           u4TcpNextConnLocalPort,
                                           i4TcpNextConnRemAddressType,
                                           &TcpNextConnRemAddress,
                                           u4TcpNextConnRemPort,
                                           &u4TcpAoAuthErr);
            nmhGetFsTcpConTcpAOSndSne (i4TcpNextConnLocalAddressType,
                                       &TcpNextConnLocalAddress,
                                       u4TcpNextConnLocalPort,
                                       i4TcpNextConnRemAddressType,
                                       &TcpNextConnRemAddress,
                                       u4TcpNextConnRemPort, &i4TcpAoSndSne);
            nmhGetFsTcpConTcpAORcvSne (i4TcpNextConnLocalAddressType,
                                       &TcpNextConnLocalAddress,
                                       u4TcpNextConnLocalPort,
                                       i4TcpNextConnRemAddressType,
                                       &TcpNextConnRemAddress,
                                       u4TcpNextConnRemPort, &i4TcpAoRcvSne);
            CliPrintf (CliHandle, "TCP-AO Current KeyId   : %d\r\n",
                       i4TcpAoCurKeyId);
            CliPrintf (CliHandle, "TCP-AO RNextKeyId      : %d\r\n",
                       i4RNextKeyId);
            CliPrintf (CliHandle, "Received KeyId         : %d\r\n",
                       i4RcvKeyId);
            CliPrintf (CliHandle, "Received  RNextKeyId   : %d\r\n",
                       i4RcvRNextKeyId);
            CliPrintf (CliHandle, "TCP-AO Auth Failures   : %u\r\n",
                       u4TcpAoAuthErr);
            CliPrintf (CliHandle, "TCP-AO Send SNE        : %d\r\n",
                       i4TcpAoSndSne);
            CliPrintf (CliHandle, "TCP-AO Receive SNE     : %d\r\n",
                       i4TcpAoRcvSne);
        }
        else
        {
            CliPrintf (CliHandle, "TCP-AO Authenticated   : No\r\n");
        }
#endif
        CliPrintf (CliHandle, "\r\n");

        i4TcpCurrConnLocalAddressType = i4TcpNextConnLocalAddressType;
        i4TcpCurrConnRemAddressType = i4TcpNextConnRemAddressType;
        u4TcpCurrConnLocalPort = u4TcpNextConnLocalPort;
        u4TcpCurrConnRemPort = u4TcpNextConnRemPort;
        MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.i4_Length);
        MEMCPY (TcpCurrConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.i4_Length);

        if (nmhGetNextIndexTcpConnectionTable
            (i4TcpCurrConnLocalAddressType, &i4TcpNextConnLocalAddressType,
             &TcpCurrConnLocalAddress, &TcpNextConnLocalAddress,
             u4TcpCurrConnLocalPort, &u4TcpNextConnLocalPort,
             i4TcpCurrConnRemAddressType, &i4TcpNextConnRemAddressType,
             &TcpCurrConnRemAddress, &TcpNextConnRemAddress,
             u4TcpCurrConnRemPort, &u4TcpNextConnRemPort) == SNMP_FAILURE)
        {
            u1IsShow = TCP_FALSE;
        }
    }
    while (u1IsShow);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************
 * Name               : TcpShowListener
 *
 * Description        : This function displays TCP Connections which are in
 *                      Listen state.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
TcpShowListener (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    INT1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4TcpCurrConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnLocalAddressType = TCP_ZERO;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TcpCurrConnLocalPort = TCP_ZERO;
    UINT4               u4TcpNextConnLocalPort = TCP_ZERO;
    UINT4               u4LocalIp = TCP_ZERO;
    UINT4               u1IsShow = TCP_TRUE;
    tSNMP_OCTET_STRING_TYPE TcpNextConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
#ifndef LNXIP4_WANTED
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    MEMSET (au1CxtName, TCP_ZERO, VCMALIAS_MAX_ARRAY_LEN);
    VcmGetAliasName (gpTcpCurrContext->u4ContextId, au1CxtName);
#endif

    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnLocalAddress.pu1_OctetList = au1NextLocalIp;
    TcpNextConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (nmhGetFirstIndexTcpListenerTable
        (&i4TcpNextConnLocalAddressType,
         &TcpNextConnLocalAddress, &u4TcpNextConnLocalPort) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
#ifndef LNXIP4_WANTED
    CliPrintf (CliHandle, " Context Name : %s\r\n", au1CxtName);
    CliPrintf (CliHandle, " --------------\r\n", au1CxtName);
#endif
    do
    {
        if (i4TcpNextConnLocalAddressType == IPV4_ADD_TYPE)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv4\r\n");
            MEMCPY ((UINT1 *) &u4LocalIp,
                    TcpNextConnLocalAddress.pu1_OctetList,
                    TcpNextConnLocalAddress.i4_Length);
            u4LocalIp = OSIX_NTOHL (u4LocalIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocalIp);
            CliPrintf (CliHandle, "Local IP               : %s\r\n", pu1String);
        }
        else if (i4TcpNextConnLocalAddressType == IPV6_ADD_TYPE)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv6\r\n");
            MEMCPY (au1Ip6Addr, TcpNextConnLocalAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Local IP               : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        else if (i4TcpNextConnLocalAddressType == TCP_ZERO)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : 0\r\n");
            MEMCPY ((UINT1 *) &u4LocalIp,
                    TcpNextConnLocalAddress.pu1_OctetList,
                    TcpNextConnLocalAddress.i4_Length);
            u4LocalIp = OSIX_NTOHL (u4LocalIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocalIp);
            CliPrintf (CliHandle, "Local IP               : %s\r\n", pu1String);

        }
        CliPrintf (CliHandle, "Local Port             : %d\r\n",
                   u4TcpNextConnLocalPort);
        CliPrintf (CliHandle, "\r\n");

        i4TcpCurrConnLocalAddressType = i4TcpNextConnLocalAddressType;
        u4TcpCurrConnLocalPort = u4TcpNextConnLocalPort;
        MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.i4_Length);
        if (nmhGetNextIndexTcpListenerTable
            (i4TcpCurrConnLocalAddressType, &i4TcpNextConnLocalAddressType,
             &TcpCurrConnLocalAddress, &TcpNextConnLocalAddress,
             u4TcpCurrConnLocalPort, &u4TcpNextConnLocalPort) == SNMP_FAILURE)
        {
            u1IsShow = TCP_FALSE;
        }
    }
    while (u1IsShow);
    /*HDC-49226 */
    CliPrintf (CliHandle, "Address Type [0 - IPv4 and IPv6] "
               "[1 - IPv4] [2 - IPv6]\r\n\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * Name               : TcpShowRetransmissionDetails
 *
 * Description        : This function displays TCP Retransmission details
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
TcpShowRetransmissionDetails (tCliHandle CliHandle)
{
    INT4                i4TcpRtoAlgorithm = TCP_ZERO;
    INT4                i4TcpRtoMin = TCP_ZERO;
    INT4                i4TcpRtoMax = TCP_ZERO;

#ifndef LNXIP4_WANTED
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    MEMSET (au1CxtName, TCP_ZERO, VCMALIAS_MAX_ARRAY_LEN);
    VcmGetAliasName (gpTcpCurrContext->u4ContextId, au1CxtName);
    CliPrintf (CliHandle, "\n Context Name : %s\r\n", au1CxtName);
#endif

    CliPrintf (CliHandle, "\n");
    nmhGetTcpRtoAlgorithm (&i4TcpRtoAlgorithm);
    nmhGetTcpRtoMin (&i4TcpRtoMin);
    nmhGetTcpRtoMax (&i4TcpRtoMax);
    if (i4TcpRtoAlgorithm == VAN_JACOBSON)
    {
        CliPrintf (CliHandle, " RTO Algorithm Used : VAN JACOBSON\r\n");
    }
    else if (i4TcpRtoAlgorithm == RFC2988)
    {
        CliPrintf (CliHandle, " RTO Algorithm Used : RFC 2988\r\n");
    }
    else if (i4TcpRtoAlgorithm == TCP_ZERO)
    {
        CliPrintf (CliHandle, " RTO Algorithm Used : None\r\n");
    }
    CliPrintf (CliHandle,
               " Min Retransmission Timeout : %d msec\r\n", i4TcpRtoMin);

    CliPrintf (CliHandle,
               " Max Retransmission Timeout : %d msec\r\n", i4TcpRtoMax);
    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

#endif
