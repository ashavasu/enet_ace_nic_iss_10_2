/*******************************************************************
 * copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: tcputils.c,v 1.12 2014/07/27 10:54:56 siva Exp $
 *
 * Description:TCP utility functions are present in the file.
 *
 *******************************************************************/

#ifndef __TCP_UTIL_C
#define __TCP_UTIL_C

#include "tcpinc.h"
#include "tcputils.h"
#include "tcpipvxdefs.h"
#include "stdtcplw.h"
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
/***********************************************************************
 * FUNCTION      :  tcpGetConnID
 *
 * DESCRIPTION   :  Gets the connection index specified bythe indices exist.
 *                  It is done by a simple scan through the TCB table. 
 * INPUT         :  i4LocalAddrType -- Local Address type
 *                  u4LocalAddress  -- Local IP address
 *                  u4LocalPort     -- Local Port
 *                  
 * OUTPUT        :  None
 *
 * RETURN        :  TCP_SUCCESS/TCP_FAILURE
 ******************************************************************************/
INT4
tcpGetConnID (UINT4 u4Context, INT4 i4LocalAddrType, UINT4 u4LocalPort,
              INT4 i4RemoteAddrType)
{
    UINT4               u4TcbIndex = TCP_SUCCESS;
    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if ((u4Context != GetTCBContext (u4TcbIndex)) ||
            (i4LocalAddrType != GetTCBLipAddType (u4TcbIndex)) ||
            (i4RemoteAddrType != GetTCBRipAddType (u4TcbIndex)) ||
            (u4LocalPort != GetTCBlport (u4TcbIndex)))
        {
            continue;
        }
        return u4TcbIndex;
    }
    return (TCP_FAILURE);
}

/***********************************************************************
 * FUNCTION      :  tcpGetConnTableID
 *
 * DESCRIPTION   :  Gets the connection index specified by all the indices.
 *                  It is done by a simple scan through the TCB table. 
 * INPUT         :  TcpConnectionLocalAddressType
 *                  TcpConnectionLocalAddress
 *                  TcpConnectionLocalPort
 *                  TcpConnectionRemAddress
 *                  TcpConnectionRemPort
 *
 *                  
 * OUTPUT        :  None
 *
 * RETURN        :  TCP_SUCCESS/TCP_FAILURE
 ******************************************************************************/
INT4
tcpGetConnTableID (UINT4 u4Context,
                   INT4 i4TcpConnAddrType,
                   tSNMP_OCTET_STRING_TYPE * pTcpConnLocalAddr,
                   UINT4 u4TcpConnLocalPort,
                   tSNMP_OCTET_STRING_TYPE * pTcpConnRemAddr,
                   UINT4 u4TcpConnRemPort)
{
    UINT4               u4TcbIndex = TCP_SUCCESS;
    UINT4               u4TcpCurLocalAddrType = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    tIpAddr             TempRemoteIP;
    tIpAddr             TempLocalIP;
    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;

    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));

    if (pTcpConnLocalAddr->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnLocalAddr->pu1_OctetList,
                pTcpConnLocalAddr->i4_Length);
    }
    if (pTcpConnRemAddr->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnRemAddr->pu1_OctetList,
                pTcpConnRemAddr->i4_Length);
    }

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        u4TcpCurLocalAddrType = GetTCBLipAddType (u4TcbIndex);

        MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
        MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

        if (u4TcpCurLocalAddrType == TCP_IPV4_ADDR_TYPE)
        {
            u4TempLocalTcpAddr = GetTCBv4lip (u4TcbIndex);
            MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
            u4TempRemTcpAddr = GetTCBv4rip (u4TcbIndex);
            MEMCPY (&TcpCurRemoteAddr, &u4TempRemTcpAddr, sizeof (UINT4));
        }
        else if (u4TcpCurLocalAddrType == TCP_IPV6_ADDR_TYPE)
        {
            MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4TcbIndex),
                    sizeof (tIpAddr));
            MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4TcbIndex),
                    sizeof (tIpAddr));
        }

        if ((u4Context != GetTCBContext (u4TcbIndex)) ||
            (i4TcpConnAddrType != (INT4) u4TcpCurLocalAddrType) ||
            (IpvxAddrCmp (&TempLocalIP, &TcpCurLocalAddr) != TCP_ZERO) ||
            (u4TcpConnLocalPort != GetTCBlport (u4TcbIndex)) ||
            (IpvxAddrCmp (&TempRemoteIP, &TcpCurRemoteAddr) != TCP_ZERO)
            || (u4TcpConnRemPort != GetTCBrport (u4TcbIndex)))
        {
            continue;
        }
        return u4TcbIndex;
    }
    return (TCP_FAILURE);
}

/******************************************************************************
 * DESCRIPTION : Copies One address pointer to another.
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to copied address.
 *
 * NOTES       :
 ******************************************************************************/
VOID
IpvxAddrCopy (tIpAddr * pAddrdst, tIpAddr * pAddrsrc)
{
    MEMCPY (pAddrdst, pAddrsrc, sizeof (tIpAddr));
}

/******************************************************************************
 * DESCRIPTION : Compare One address pointer to another.
 *
 * INPUTS      : The two address (pA1,pA2)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to copied address.
 *
 * NOTES       :
 ******************************************************************************/
INT4
IpvxAddrCmp (tIpAddr * pAddrsrc, tIpAddr * pAddrdst)
{
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pAddrsrc, pAddrdst, sizeof (tIpAddr));
    return (i4RetVal);
}

/******************************************************************************
 * FUNCTION    : CheckForIpv6Entry 
 *
 * DESCRIPTION : Check's for a TCP IPV6 entry which uses the same port as TCP 
 *               IPV4 entry.
 *               
 * INPUTS      : pu4TcpListenerLocalPort - Port number.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT4
CheckForIpv6Entry (UINT4 *pu4TcpListenerLocalPort)
{
    UINT4               u4TcbIndex = TCP_ZERO;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) != TCPS_LISTEN ||
            GetTCBLipAddType (u4TcbIndex) != TCP_IPV6_ADDR_TYPE ||
            GetTCBv4lip (u4TcbIndex) != TCP_ZERO ||
            GetTCBContext (u4TcbIndex) != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }

        if (*pu4TcpListenerLocalPort == GetTCBlport (u4TcbIndex))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            continue;
        }
    }
    return SNMP_FAILURE;
}

/******************************************************************************
 * FUNCTION    : CheckForIpv4Entry 
 *
 * DESCRIPTION : Check's for a TCP IPV4 entry which uses the same port as TCP 
 *               IPV6 entry.
 *               
 * INPUTS      : pu4TcpListenerLocalPort - Port number.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT4
CheckForIpv4Entry (UINT4 *pu4TcpListenerLocalPort)
{
    UINT4               u4TcbIndex = TCP_ZERO;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) != TCPS_LISTEN ||
            GetTCBLipAddType (u4TcbIndex) != TCP_IPV4_ADDR_TYPE ||
            GetTCBv4lip (u4TcbIndex) != TCP_ZERO ||
            GetTCBContext (u4TcbIndex) != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }

        if (*pu4TcpListenerLocalPort == GetTCBlport (u4TcbIndex))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            continue;
        }
    }
    return SNMP_FAILURE;
}

/******************************************************************************
 * FUNCTION    : CheckForAddrTypeZeroEntry 
 *
 * DESCRIPTION : Check's for a TCP IPV4 entry whose local port is used by the 
 *               TCP IPV6 entry also.
 *               
 * INPUTS      : i4TcpListenerLocalAddressType - Local address type
 *               pi4NextTcpListenerLocalAddressType - Local address type of 
 *                                                    Next Conn
 *               pTcpListenerLocalAddress - Local Ip of the Conn
 *               pNextTcpListenerLocalAddress - Local Ip of Next Conn
 *               u4TcpListenerLocalPort - Local Port of the Conn
 *               pu4NextTcpListenerLocalPort - Local Port of Next Conn
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT4                CheckForAddrTypeZeroEntry
    (UINT4 u4Context,
     INT4 i4TcpListenerLocalAddressType,
     INT4 *pi4NextTcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextTcpListenerLocalAddress,
     UINT4 u4TcpListenerLocalPort, UINT4 *pu4NextTcpListenerLocalPort)
{
    INT4                i4TcpCurrConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpCurrConnRemAddressType = TCP_ZERO;
    INT4                i4TcpNextConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnRemAddressType = TCP_ZERO;
    INT4                i4ConnState = TCP_ZERO;
    INT4                i4TcbIndex = TCP_ZERO;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TcpNextConnLocalPort = TCP_ZERO;
    UINT4               u4TcpNextConnRemPort = TCP_ZERO;
    UINT4               u4TcpCurrConnLocalPort = TCP_ZERO;
    UINT4               u4TcpCurrConnRemPort = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnRemAddress;

    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemAddr;

    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemAddr, TCP_ZERO, sizeof (tIpAddr));

    UNUSED_PARAM (pTcpListenerLocalAddress);
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;
    TcpNextConnLocalAddress.pu1_OctetList = au1NextLocalIp;
    TcpNextConnRemAddress.pu1_OctetList = au1NextRemoteIp;

    if (i4TcpListenerLocalAddressType == TCP_ZERO &&
        u4TcpListenerLocalPort != TCP_ZERO)
    {
        i4TcpListenerLocalAddressType = TCP_IPV4_ADDR_TYPE;
    }

    if (i4TcpListenerLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        TcpCurrConnLocalAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
        TcpCurrConnRemAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
        TcpNextConnLocalAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
        TcpNextConnRemAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        TcpCurrConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;
        TcpCurrConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;
        TcpNextConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;
        TcpNextConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    }

    /*Get the Conn ID for the given indices */
    i4TcbIndex = tcpGetConnID (u4Context,
                               i4TcpListenerLocalAddressType,
                               u4TcpListenerLocalPort,
                               i4TcpListenerLocalAddressType);

    /*If the Conn exists fill the entries */
    if (i4TcbIndex != TCP_FAILURE)
    {
        i4TcpNextConnLocalAddressType = GetTCBLipAddType (i4TcbIndex);
        i4TcpNextConnRemAddressType = GetTCBRipAddType (i4TcbIndex);
        u4TcpNextConnLocalPort = GetTCBlport (i4TcbIndex);
        u4TcpNextConnRemPort = GetTCBrport (i4TcbIndex);
        if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
        {
            u4TempLocalTcpAddr = GetTCBv4lip (i4TcbIndex);
            MEMCPY ((tIpAddr *) (VOID *) TcpNextConnLocalAddress.pu1_OctetList,
                    &u4TempLocalTcpAddr, sizeof (UINT4));
            u4TempRemTcpAddr = GetTCBv4rip (i4TcbIndex);
            MEMCPY ((tIpAddr *) (VOID *) TcpNextConnRemAddress.pu1_OctetList,
                    &u4TempRemTcpAddr, sizeof (UINT4));
        }
        else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
        {
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnLocalAddress.pu1_OctetList,
                          &GetTCBlip (i4TcbIndex));
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnRemAddress.pu1_OctetList,
                          &GetTCBrip (i4TcbIndex));
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    do
    {

        i4TcpCurrConnLocalAddressType = i4TcpNextConnLocalAddressType;
        i4TcpCurrConnRemAddressType = i4TcpNextConnRemAddressType;
        u4TcpCurrConnLocalPort = u4TcpNextConnLocalPort;
        u4TcpCurrConnRemPort = u4TcpNextConnRemPort;
        MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.i4_Length);
        MEMCPY (TcpCurrConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.i4_Length);

        if (nmhGetNextIndexTcpConnectionTable
            (i4TcpCurrConnLocalAddressType,
             &i4TcpNextConnLocalAddressType,
             &TcpCurrConnLocalAddress, &TcpNextConnLocalAddress,
             u4TcpCurrConnLocalPort, &u4TcpNextConnLocalPort,
             i4TcpCurrConnRemAddressType, &i4TcpNextConnRemAddressType,
             &TcpCurrConnRemAddress, &TcpNextConnRemAddress,
             u4TcpCurrConnRemPort, &u4TcpNextConnRemPort) == SNMP_SUCCESS)
        {
            nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4ConnState);
            /*If the Conn state is Listen and the address type is IPv4
             * check for a Corresponding IPv6 entry having 
             * same Local Ip and Local Port,
             * if IPv6 Conn exists set Local add type as zero and return 
             * the entry, else continue
             * If the Address type of the Conn is IPv6 come out of the loop*/
            if (i4ConnState == TCPS_LISTEN)
            {
                if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
                {
                    if (CheckForIpv6Entry (&u4TcpNextConnLocalPort)
                        == SNMP_SUCCESS)
                    {
                        *pi4NextTcpListenerLocalAddressType = TCP_ZERO;
                        MEMCPY (pNextTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (TCP_ZERO, pNextTcpListenerLocalAddress);
                        *pu4NextTcpListenerLocalPort = u4TcpNextConnLocalPort;
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
                {
                    break;
                }

            }

        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    while (TCP_TRUE);
    return SNMP_FAILURE;
}

/******************************************************************************
 * FUNCTION    : CheckForAddrTypeOneAndTwoEntry 
 *
 * DESCRIPTION : Check's for a TCP IPv4 and IPv6 entries whose local ports 
 *               are not used by any other Connection.
 *               
 * INPUTS      : i4TcpListenerLocalAddressType - Local address type
 *               pi4NextTcpListenerLocalAddressType - Local address type of 
 *                                                    Next Conn
 *               pTcpListenerLocalAddress - Local Ip of the Conn
 *               pNextTcpListenerLocalAddress - Local Ip of Next Conn
 *               u4TcpListenerLocalPort - Local Port of the Conn
 *               pu4NextTcpListenerLocalPort - Local Port of Next Conn
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       : SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT4                CheckForAddrTypeOneAndTwoEntry
    (UINT4 u4Context,
     INT4 i4TcpListenerLocalAddressType,
     INT4 *pi4NextTcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextTcpListenerLocalAddress,
     UINT4 u4TcpListenerLocalPort, UINT4 *pu4NextTcpListenerLocalPort)
{
    INT4                i4TcpCurrConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpCurrConnRemAddressType = TCP_ZERO;
    INT4                i4TcpNextConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnRemAddressType = TCP_ZERO;
    INT4                i4ConnState = TCP_ZERO;
    INT4                i4TcbIndex = TCP_ZERO;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TcpNextConnLocalPort = TCP_ZERO;
    UINT4               u4TcpNextConnRemPort = TCP_ZERO;
    UINT4               u4TcpCurrConnLocalPort = TCP_ZERO;
    UINT4               u4TcpCurrConnRemPort = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;

    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnRemAddress;

    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemAddr;

    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemAddr, TCP_ZERO, sizeof (tIpAddr));

    UNUSED_PARAM (pTcpListenerLocalAddress);
    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;
    TcpCurrConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnLocalAddress.pu1_OctetList = au1NextLocalIp;
    TcpNextConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnRemAddress.pu1_OctetList = au1NextRemoteIp;
    TcpNextConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    /*This routine will give the first entry whose local port
     * is not used by any other Connection*/
    if (i4TcpListenerLocalAddressType == TCP_ZERO)
    {
        if (nmhGetFirstIndexTcpConnectionTable
            (&i4TcpNextConnLocalAddressType,
             &TcpNextConnLocalAddress,
             &u4TcpNextConnLocalPort,
             &i4TcpNextConnRemAddressType,
             &TcpNextConnRemAddress, &u4TcpNextConnRemPort) == SNMP_SUCCESS)
        {
            nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4ConnState);
            /*If the Conn state is Listen and the address type is IPv4
             * check for a Corresponding IPv6 entry having 
             * same Local Ip and Local Port,
             * if IPv6 Conn exists skip the entry,
             * else return the entry
             * If the Address type of the Conn is IPv6 check for IPv4 entry
             * if IPv4 Conn present skip the entry
             * else return the entry*/
            if (i4ConnState == TCPS_LISTEN)
            {
                if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
                {
                    i4TcpListenerLocalAddressType = TCP_IPV4_ADDR_TYPE;
                    u4TcpListenerLocalPort = u4TcpNextConnLocalPort;
                    if (CheckForIpv6Entry (&u4TcpNextConnLocalPort)
                        == SNMP_FAILURE)
                    {
                        *pi4NextTcpListenerLocalAddressType
                            = i4TcpNextConnLocalAddressType;
                        MEMCPY (pNextTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (i4TcpNextConnLocalAddressType,
                                        pNextTcpListenerLocalAddress);
                        *pu4NextTcpListenerLocalPort = u4TcpNextConnLocalPort;

                        return SNMP_SUCCESS;
                    }
                }
                else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
                {
                    i4TcpListenerLocalAddressType = TCP_IPV6_ADDR_TYPE;
                    u4TcpListenerLocalPort = u4TcpNextConnLocalPort;
                    if (CheckForIpv4Entry (&u4TcpNextConnLocalPort)
                        == SNMP_FAILURE)
                    {
                        *pi4NextTcpListenerLocalAddressType
                            = i4TcpNextConnLocalAddressType;
                        MEMCPY (pNextTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (i4TcpNextConnLocalAddressType,
                                        pNextTcpListenerLocalAddress);
                        *pu4NextTcpListenerLocalPort = u4TcpNextConnLocalPort;

                        return SNMP_SUCCESS;
                    }
                }

            }
        }
    }
    i4TcbIndex = tcpGetConnID (u4Context,
                               i4TcpListenerLocalAddressType,
                               u4TcpListenerLocalPort,
                               i4TcpListenerLocalAddressType);

    if (i4TcbIndex != TCP_FAILURE)
    {
        i4TcpNextConnLocalAddressType = GetTCBLipAddType (i4TcbIndex);
        i4TcpNextConnRemAddressType = GetTCBRipAddType (i4TcbIndex);
        u4TcpNextConnLocalPort = GetTCBlport (i4TcbIndex);
        u4TcpNextConnRemPort = GetTCBrport (i4TcbIndex);
        if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
        {
            u4TempLocalTcpAddr = GetTCBv4lip (i4TcbIndex);
            MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnLocalAddress.pu1_OctetList,
                          &TcpCurLocalAddr);

        }
        else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
        {
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnLocalAddress.pu1_OctetList,
                          &GetTCBlip (i4TcbIndex));
        }
        if (i4TcpNextConnRemAddressType == TCP_IPV4_ADDR_TYPE)
        {
            MEMCPY (&TcpCurRemAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnRemAddress.pu1_OctetList, &TcpCurRemAddr);

        }
        else if (i4TcpNextConnRemAddressType == TCP_IPV6_ADDR_TYPE)
        {
            IpvxAddrCopy ((tIpAddr *) (VOID *)
                          TcpNextConnRemAddress.pu1_OctetList,
                          &GetTCBrip (i4TcbIndex));
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    /*If the Local IP address type is not zero get the next
     * entry whose local Port is not used by any other 
     * application of different type
     * i.e If IPv4 application uses Loval IP as 0 and local port as 2000 and
     * if there is another IPv6 application using Local IP as zero and Local
     * port as 200o in this case skip the entry.Viceversa is also applicable*/
    do
    {

        i4TcpCurrConnLocalAddressType = i4TcpNextConnLocalAddressType;
        i4TcpCurrConnRemAddressType = i4TcpNextConnRemAddressType;
        u4TcpCurrConnLocalPort = u4TcpNextConnLocalPort;
        u4TcpCurrConnRemPort = u4TcpNextConnRemPort;
        MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.i4_Length);
        MEMCPY (TcpCurrConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.i4_Length);

        if (nmhGetNextIndexTcpConnectionTable
            (i4TcpCurrConnLocalAddressType,
             &i4TcpNextConnLocalAddressType,
             &TcpCurrConnLocalAddress, &TcpNextConnLocalAddress,
             u4TcpCurrConnLocalPort, &u4TcpNextConnLocalPort,
             i4TcpCurrConnRemAddressType, &i4TcpNextConnRemAddressType,
             &TcpCurrConnRemAddress, &TcpNextConnRemAddress,
             u4TcpCurrConnRemPort, &u4TcpNextConnRemPort) == SNMP_SUCCESS)
        {
            nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4ConnState);
            if (i4ConnState == TCPS_LISTEN)
            {
                if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
                {
                    if (CheckForIpv6Entry (&u4TcpNextConnLocalPort)
                        == SNMP_FAILURE)
                    {
                        *pi4NextTcpListenerLocalAddressType
                            = i4TcpNextConnLocalAddressType;
                        MEMCPY (pNextTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (i4TcpNextConnLocalAddressType,
                                        pNextTcpListenerLocalAddress);
                        *pu4NextTcpListenerLocalPort = u4TcpNextConnLocalPort;
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
                {
                    if (CheckForIpv4Entry (&u4TcpNextConnLocalPort)
                        == SNMP_FAILURE)
                    {
                        *pi4NextTcpListenerLocalAddressType
                            = i4TcpNextConnLocalAddressType;
                        MEMCPY (pNextTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (i4TcpNextConnLocalAddressType,
                                        pNextTcpListenerLocalAddress);
                        *pu4NextTcpListenerLocalPort = u4TcpNextConnLocalPort;
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        continue;
                    }

                }

            }
            else
            {
                continue;
            }

        }
        else
        {
            break;
        }
    }
    while (TCP_TRUE);
    return SNMP_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Fills the length of the IP address based on the address type.
 *
 * INPUTS      : i4AddrType - Address type
 *               pIpAddress - IP address
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None.
 *
 * NOTES       : SNMP_SUCCESS/SNMP_FAILURE
 ******************************************************************************/
INT4
FillLengthOfIp (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddress)
{
    if (i4AddrType == TCP_IPV4_ADDR_TYPE || i4AddrType == TCP_ZERO)
    {
        pIpAddress->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (i4AddrType == TCP_IPV6_ADDR_TYPE)
    {
        pIpAddress->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Set the current context pointer to the given context ID
 *
 * INPUTS      : pu4SetValFsMIContextID - Context ID
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *
 ******************************************************************************/

INT4
TcpSetContext (UINT4 u4SetValFsMIContextID)
{
    /*  Check boundary condition */
    if (u4SetValFsMIContextID >= MAX_TCP_NUM_CONTEXT)
    {
        gpTcpCurrContext = NULL;
        return SNMP_FAILURE;
    }
    if (NULL == gpTcpContext[u4SetValFsMIContextID])
    {
        gpTcpCurrContext = NULL;
        return SNMP_FAILURE;
    }
    gpTcpCurrContext = gpTcpContext[u4SetValFsMIContextID];
    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Set the current context pointer to NULL
 *
 * INPUTS      : pu4SetValFsMIContextID - Context ID
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 ******************************************************************************/
VOID
TcpReleaseContext (VOID)
{
    gpTcpCurrContext = NULL;
}

/******************************************************************************
 * DESCRIPTION : Gets the first active context ID
 *
 * INPUTS      : None
 *
 * OUTPUTS     : pu4RetValContextID - Context ID
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *
 ******************************************************************************/

INT1
TcpGetFirstActiveContextID (UINT4 *pu4RetValContextID)
{
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (NULL != gpTcpContext[i4TmpContext])
        {
            *pu4RetValContextID = i4TmpContext;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Gets the next active context ID
 *
 * INPUTS      : u4ContextID - Context ID
 *
 * OUTPUTS     : pu4NextContextID - Next active Context ID
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *
 ******************************************************************************/

INT1
TcpGetNextActiveContextID (UINT4 u4ContextID, UINT4 *pu4NextContextID)
{
    INT4                i4TmpContext = 0;

    if (u4ContextID >= MAX_TCP_NUM_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    for (i4TmpContext = u4ContextID + 1; i4TmpContext < MAX_TCP_NUM_CONTEXT;
         i4TmpContext++)
    {
        if (NULL != gpTcpContext[i4TmpContext])
        {
            *pu4NextContextID = i4TmpContext;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
#endif /* defined (IP_WANTED) || defined (IP6_WANTED) */
#else /* LNXIP4_WANTED */

/******************************************************************************
 * DESCRIPTION : Stub for LXNIP
 *
 * INPUTS      : pu4SetValFsMIContextID - Context ID
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SNMP_SUCCESS
 *
 ******************************************************************************/

INT4
TcpSetContext (UINT4 u4SetValFsMIContextID)
{
    UNUSED_PARAM (u4SetValFsMIContextID);
    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Stub for LXNIP
 *
 * INPUTS      : pu4SetValFsMIContextID - Context ID
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 ******************************************************************************/
VOID
TcpReleaseContext (VOID)
{
    return;
}

/******************************************************************************
 * DESCRIPTION : Stub for LXNIP
 *
 * INPUTS      : None
 *
 * OUTPUTS     : pu4RetValContextID - Context ID
 *
 * RETURNS     : SNMP_SUCCESS
 *
 ******************************************************************************/

INT1
TcpGetFirstActiveContextID (UINT4 *pu4RetValContextID)
{
    *pu4RetValContextID = VCM_DEFAULT_CONTEXT;
    return SNMP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Stub for LXNIP
 *
 * INPUTS      : u4ContextID - Context ID
 *
 * OUTPUTS     : pu4NextContextID - Next active Context ID
 *
 * RETURNS     : SNMP_FAILURE
 *
 ******************************************************************************/
INT1
TcpGetNextActiveContextID (UINT4 u4ContextID, UINT4 *pu4NextContextID)
{
    UNUSED_PARAM (u4ContextID);
    UNUSED_PARAM (pu4NextContextID);
    return SNMP_FAILURE;
}

/********************************************************************
* Function Name   : TcpLock                                        
* Description     : Stub for LXNIP
* Input(s)        : None                                           
* Output(s)       : None                                           
* Returns         : SNMP_SUCCESS or SNMP_FAILURE                   
********************************************************************/
INT4
TcpLock ()
{
    return SNMP_SUCCESS;
}

/********************************************************************
* Function Name   : TcpUnLock                                      
* Description     : Stub for LXNIP
* Input(s)        : None                                           
* Output(s)       : None                                          
* Returns         : OSIX_SUCCESS                                   
********************************************************************/
INT4
TcpUnLock ()
{
    return SNMP_SUCCESS;
}

#endif /* LNXIP4_WANTED */
#endif /* __TCP_UTIL_C  */
