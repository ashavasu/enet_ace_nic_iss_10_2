/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmptclw.c,v 1.5 2014/07/27 10:54:56 siva Exp $
*
* Description: Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmptclw.h"

# include  "tcpinc.h"
# include  "stdtcplw.h"
# include  "tcputils.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITcpGlobalTraceDebug
 Input       :  The Indices

                The Object 
                retValFsMITcpGlobalTraceDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMITcpGlobalTraceDebug (INT4 *pi4RetValFsMITcpGlobalTraceDebug)
{
    *pi4RetValFsMITcpGlobalTraceDebug = gi4TcpGlobalTraceDebug;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMITcpGlobalTraceDebug
 Input       :  The Indices

                The Object 
                setValFsMITcpGlobalTraceDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpGlobalTraceDebug (INT4 i4SetValFsMITcpGlobalTraceDebug)
{
    gi4TcpGlobalTraceDebug = i4SetValFsMITcpGlobalTraceDebug;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMITcpGlobalTraceDebug
 Input       :  The Indices

                The Object 
                testValFsMITcpGlobalTraceDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpGlobalTraceDebug (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsMITcpGlobalTraceDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMITcpGlobalTraceDebug);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMITcpGlobalTraceDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMITcpGlobalTraceDebug (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIContextTable
 Input       :  The Indices
                FsMITcpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIContextTable (INT4 i4FsMITcpContextId)
{
    if (i4FsMITcpContextId >= MAX_TCP_NUM_CONTEXT)
    {
        return SNMP_FAILURE;
    }
    if (gpTcpContext[i4FsMITcpContextId] != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIContextTable
 Input       :  The Indices
                FsMITcpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIContextTable (INT4 *pi4FsMITcpContextId)
{

    if (SNMP_SUCCESS ==
        TcpGetFirstActiveContextID ((UINT4 *) pi4FsMITcpContextId))
    {
        return SNMP_SUCCESS;
    }
    /* No context is created */
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIContextTable
 Input       :  The Indices
                FsMITcpContextId
                nextFsMITcpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIContextTable (INT4 i4FsMITcpContextId,
                                 INT4 *pi4NextFsMITcpContextId)
{

    if (SNMP_SUCCESS ==
        TcpGetNextActiveContextID ((UINT4) i4FsMITcpContextId,
                                   (UINT4 *) pi4NextFsMITcpContextId))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITcpAckOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpAckOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpAckOption (INT4 i4FsMITcpContextId,
                        INT4 *pi4RetValFsMITcpAckOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpAckOption (pi4RetValFsMITcpAckOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpTimeStampOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpTimeStampOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpTimeStampOption (INT4 i4FsMITcpContextId,
                              INT4 *pi4RetValFsMITcpTimeStampOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpTimeStampOption (pi4RetValFsMITcpTimeStampOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpBigWndOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpBigWndOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpBigWndOption (INT4 i4FsMITcpContextId,
                           INT4 *pi4RetValFsMITcpBigWndOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpBigWndOption (pi4RetValFsMITcpBigWndOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpIncrIniWnd
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpIncrIniWnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpIncrIniWnd (INT4 i4FsMITcpContextId,
                         INT4 *pi4RetValFsMITcpIncrIniWnd)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpIncrIniWnd (pi4RetValFsMITcpIncrIniWnd);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpMaxNumOfTCB
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpMaxNumOfTCB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpMaxNumOfTCB (INT4 i4FsMITcpContextId,
                          INT4 *pi4RetValFsMITcpMaxNumOfTCB)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpMaxNumOfTCB (pi4RetValFsMITcpMaxNumOfTCB);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpTraceDebug
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpTraceDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpTraceDebug (INT4 i4FsMITcpContextId,
                         INT4 *pi4RetValFsMITcpTraceDebug)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpTraceDebug (pi4RetValFsMITcpTraceDebug);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpMaxReTries
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpMaxReTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpMaxReTries (INT4 i4FsMITcpContextId,
                         INT4 *pi4RetValFsMITcpMaxReTries)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetFsTcpMaxReTries (pi4RetValFsMITcpMaxReTries);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpClearStatistics
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpClearStatistics
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpClearStatistics (INT4 i4FsMITcpContextId,
                              INT4 *pi4RetValFsMITcpClearStatistics)
{
    UNUSED_PARAM (i4FsMITcpContextId);
    pi4RetValFsMITcpClearStatistics = 0;
    UNUSED_PARAM (pi4RetValFsMITcpClearStatistics);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpTrapAdminStatus
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                retValFsMITcpTrapAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpTrapAdminStatus (INT4 i4FsMITcpContextId,
                              INT4 *pi4RetValFsMITcpTrapAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsTcpTrapAdminStatus (pi4RetValFsMITcpTrapAdminStatus);
    TcpReleaseContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMITcpAckOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpAckOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpAckOption (INT4 i4FsMITcpContextId, INT4 i4SetValFsMITcpAckOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpAckOption (i4SetValFsMITcpAckOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpTimeStampOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpTimeStampOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpTimeStampOption (INT4 i4FsMITcpContextId,
                              INT4 i4SetValFsMITcpTimeStampOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpTimeStampOption (i4SetValFsMITcpTimeStampOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpBigWndOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpBigWndOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpBigWndOption (INT4 i4FsMITcpContextId,
                           INT4 i4SetValFsMITcpBigWndOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpBigWndOption (i4SetValFsMITcpBigWndOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpIncrIniWnd
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpIncrIniWnd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpIncrIniWnd (INT4 i4FsMITcpContextId,
                         INT4 i4SetValFsMITcpIncrIniWnd)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpIncrIniWnd (i4SetValFsMITcpIncrIniWnd);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpMaxNumOfTCB
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpMaxNumOfTCB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpMaxNumOfTCB (INT4 i4FsMITcpContextId,
                          INT4 i4SetValFsMITcpMaxNumOfTCB)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpMaxNumOfTCB (i4SetValFsMITcpMaxNumOfTCB);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpTraceDebug
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpTraceDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpTraceDebug (INT4 i4FsMITcpContextId,
                         INT4 i4SetValFsMITcpTraceDebug)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpTraceDebug (i4SetValFsMITcpTraceDebug);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpMaxReTries
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpMaxReTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpMaxReTries (INT4 i4FsMITcpContextId,
                         INT4 i4SetValFsMITcpMaxReTries)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhSetFsTcpMaxReTries (i4SetValFsMITcpMaxReTries);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpClearStatistics
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpClearStatistics
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpClearStatistics (INT4 i4FsMITcpContextId,
                              INT4 i4SetValFsMITcpClearStatistics)
{

    if (i4SetValFsMITcpClearStatistics == 0
        || gpTcpContext[i4FsMITcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4ActiveOpens = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4PassiveOpens = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4AttemptFails = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4EstabResets = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4CurrEstab = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4InSegs = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4OutSegs = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4RetransSegs = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4InErrs = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u4OutRsts = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u8HcInSegs.u4Lo = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u8HcInSegs.u4Hi = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u8HcOutSegs.u4Lo = 0;
        gpTcpContext[i4FsMITcpContextId]->TcpStatParms.u8HcOutSegs.u4Hi = 0;

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMITcpTrapAdminStatus
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                setValFsMITcpTrapAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpTrapAdminStatus (INT4 i4FsMITcpContextId,
                              INT4 i4SetValFsMITcpTrapAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsTcpTrapAdminStatus (i4SetValFsMITcpTrapAdminStatus);
    TcpReleaseContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMITcpAckOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpAckOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpAckOption (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                           INT4 i4TestValFsMITcpAckOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpAckOption (pu4ErrorCode, i4TestValFsMITcpAckOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpTimeStampOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpTimeStampOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpTimeStampOption (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                                 INT4 i4TestValFsMITcpTimeStampOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpTimeStampOption (pu4ErrorCode,
                                       i4TestValFsMITcpTimeStampOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpBigWndOption
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpBigWndOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpBigWndOption (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                              INT4 i4TestValFsMITcpBigWndOption)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpBigWndOption (pu4ErrorCode, i4TestValFsMITcpBigWndOption);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpIncrIniWnd
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpIncrIniWnd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpIncrIniWnd (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                            INT4 i4TestValFsMITcpIncrIniWnd)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpIncrIniWnd (pu4ErrorCode, i4TestValFsMITcpIncrIniWnd);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpMaxNumOfTCB
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpMaxNumOfTCB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpMaxNumOfTCB (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                             INT4 i4TestValFsMITcpMaxNumOfTCB)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpMaxNumOfTCB (pu4ErrorCode, i4TestValFsMITcpMaxNumOfTCB);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpTraceDebug
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpTraceDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpTraceDebug (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                            INT4 i4TestValFsMITcpTraceDebug)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpTraceDebug (pu4ErrorCode, i4TestValFsMITcpTraceDebug);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpMaxReTries
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpMaxReTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpMaxReTries (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                            INT4 i4TestValFsMITcpMaxReTries)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpMaxReTries (pu4ErrorCode, i4TestValFsMITcpMaxReTries);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpClearStatistics
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpClearStatistics
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpClearStatistics (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                                 INT4 i4TestValFsMITcpClearStatistics)
{

    if (i4TestValFsMITcpClearStatistics == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (gpTcpContext[i4FsMITcpContextId] == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpTrapAdminStatus
 Input       :  The Indices
                FsMITcpContextId

                The Object 
                testValFsMITcpTrapAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpTrapAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                                 INT4 i4TestValFsMITcpTrapAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsTcpTrapAdminStatus (pu4ErrorCode,
                                       i4TestValFsMITcpTrapAdminStatus);
    TcpReleaseContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIContextTable
 Input       :  The Indices
                FsMITcpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIContextTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMITcpConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMITcpConnTable
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMITcpConnTable (INT4 i4FsMITcpContextId,
                                          UINT4 u4FsMITcpConnLocalAddress,
                                          INT4 i4FsMITcpConnLocalPort,
                                          UINT4 u4FsMITcpConnRemAddress,
                                          INT4 i4FsMITcpConnRemPort)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhValidateIndexInstanceFsTcpConnTable (u4FsMITcpConnLocalAddress,
                                                i4FsMITcpConnLocalPort,
                                                u4FsMITcpConnRemAddress,
                                                i4FsMITcpConnRemPort);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMITcpConnTable
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMITcpConnTable (INT4 *pi4FsMITcpContextId,
                                  UINT4 *pu4FsMITcpConnLocalAddress,
                                  INT4 *pi4FsMITcpConnLocalPort,
                                  UINT4 *pu4FsMITcpConnRemAddress,
                                  INT4 *pi4FsMITcpConnRemPort)
{
    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return = nmhGetFirstIndexFsTcpConnTable (pu4FsMITcpConnLocalAddress,
                                                   pi4FsMITcpConnLocalPort,
                                                   pu4FsMITcpConnRemAddress,
                                                   pi4FsMITcpConnRemPort);
        if (SNMP_SUCCESS == i4Return)
        {
            *pi4FsMITcpContextId = i4TmpContext;
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMITcpConnTable
 Input       :  The Indices
                FsMITcpContextId
                nextFsMITcpContextId
                FsMITcpConnLocalAddress
                nextFsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                nextFsMITcpConnLocalPort
                FsMITcpConnRemAddress
                nextFsMITcpConnRemAddress
                FsMITcpConnRemPort
                nextFsMITcpConnRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMITcpConnTable (INT4 i4FsMITcpContextId,
                                 INT4 *pi4NextFsMITcpContextId,
                                 UINT4 u4FsMITcpConnLocalAddress,
                                 UINT4 *pu4NextFsMITcpConnLocalAddress,
                                 INT4 i4FsMITcpConnLocalPort,
                                 INT4 *pi4NextFsMITcpConnLocalPort,
                                 UINT4 u4FsMITcpConnRemAddress,
                                 UINT4 *pu4NextFsMITcpConnRemAddress,
                                 INT4 i4FsMITcpConnRemPort,
                                 INT4 *pi4NextFsMITcpConnRemPort)
{

    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    /* Get next in current context */
    if (TcpSetContext (i4FsMITcpContextId) == SNMP_SUCCESS)
    {
        i4Return = nmhGetNextIndexTcpConnTable (u4FsMITcpConnLocalAddress,
                                                pu4NextFsMITcpConnLocalAddress,
                                                i4FsMITcpConnLocalPort,
                                                pi4NextFsMITcpConnLocalPort,
                                                u4FsMITcpConnRemAddress,
                                                pu4NextFsMITcpConnRemAddress,
                                                i4FsMITcpConnRemPort,
                                                pi4NextFsMITcpConnRemPort);
    }
    if (SNMP_SUCCESS == i4Return)
    {
        *pi4NextFsMITcpContextId = i4FsMITcpContextId;
        TcpReleaseContext ();
        return SNMP_SUCCESS;
    }

    /* If that fails, GetFirst  in Next context */
    for (i4TmpContext = i4FsMITcpContextId + 1;
         i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return = nmhGetFirstIndexTcpConnTable (pu4NextFsMITcpConnLocalAddress,
                                                 pi4NextFsMITcpConnLocalPort,
                                                 pu4NextFsMITcpConnRemAddress,
                                                 pi4NextFsMITcpConnRemPort);
        if (SNMP_SUCCESS == i4Return)
        {
            *pi4NextFsMITcpContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    TcpReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITcpConnOutState
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnOutState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnOutState (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 *pi4RetValFsMITcpConnOutState)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnOutState (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 pi4RetValFsMITcpConnOutState);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSWindow
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSWindow (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnSWindow)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSWindow (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnSWindow);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRWindow
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRWindow (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnRWindow)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRWindow (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnRWindow);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnCWindow
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnCWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnCWindow (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnCWindow)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnCWindow (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnCWindow);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSSThresh
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSSThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSSThresh (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 *pi4RetValFsMITcpConnSSThresh)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSSThresh (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 pi4RetValFsMITcpConnSSThresh);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSMSS
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSMSS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSMSS (INT4 i4FsMITcpContextId, UINT4 u4FsMITcpConnLocalAddress,
                       INT4 i4FsMITcpConnLocalPort,
                       UINT4 u4FsMITcpConnRemAddress, INT4 i4FsMITcpConnRemPort,
                       INT4 *pi4RetValFsMITcpConnSMSS)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSMSS (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                             u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                             pi4RetValFsMITcpConnSMSS);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRMSS
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRMSS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRMSS (INT4 i4FsMITcpContextId, UINT4 u4FsMITcpConnLocalAddress,
                       INT4 i4FsMITcpConnLocalPort,
                       UINT4 u4FsMITcpConnRemAddress, INT4 i4FsMITcpConnRemPort,
                       INT4 *pi4RetValFsMITcpConnRMSS)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRMSS (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                             u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                             pi4RetValFsMITcpConnRMSS);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSRT
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSRT (INT4 i4FsMITcpContextId, UINT4 u4FsMITcpConnLocalAddress,
                      INT4 i4FsMITcpConnLocalPort,
                      UINT4 u4FsMITcpConnRemAddress, INT4 i4FsMITcpConnRemPort,
                      INT4 *pi4RetValFsMITcpConnSRT)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSRT (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                            u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                            pi4RetValFsMITcpConnSRT);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRTDE
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRTDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRTDE (INT4 i4FsMITcpContextId, UINT4 u4FsMITcpConnLocalAddress,
                       INT4 i4FsMITcpConnLocalPort,
                       UINT4 u4FsMITcpConnRemAddress, INT4 i4FsMITcpConnRemPort,
                       INT4 *pi4RetValFsMITcpConnRTDE)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRTDE (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                             u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                             pi4RetValFsMITcpConnRTDE);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnPersist
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnPersist
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnPersist (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnPersist)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnPersist (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnPersist);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRexmt
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRexmt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRexmt (INT4 i4FsMITcpContextId,
                        UINT4 u4FsMITcpConnLocalAddress,
                        INT4 i4FsMITcpConnLocalPort,
                        UINT4 u4FsMITcpConnRemAddress,
                        INT4 i4FsMITcpConnRemPort,
                        INT4 *pi4RetValFsMITcpConnRexmt)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRexmt (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                              u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                              pi4RetValFsMITcpConnRexmt);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRexmtCnt
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRexmtCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRexmtCnt (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 *pi4RetValFsMITcpConnRexmtCnt)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRexmtCnt (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 pi4RetValFsMITcpConnRexmtCnt);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSBCount
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSBCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSBCount (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnSBCount)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSBCount (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnSBCount);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnSBSize
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnSBSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnSBSize (INT4 i4FsMITcpContextId,
                         UINT4 u4FsMITcpConnLocalAddress,
                         INT4 i4FsMITcpConnLocalPort,
                         UINT4 u4FsMITcpConnRemAddress,
                         INT4 i4FsMITcpConnRemPort,
                         INT4 *pi4RetValFsMITcpConnSBSize)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnSBSize (u4FsMITcpConnLocalAddress,
                               i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                               i4FsMITcpConnRemPort,
                               pi4RetValFsMITcpConnSBSize);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRBCount
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRBCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRBCount (INT4 i4FsMITcpContextId,
                          UINT4 u4FsMITcpConnLocalAddress,
                          INT4 i4FsMITcpConnLocalPort,
                          UINT4 u4FsMITcpConnRemAddress,
                          INT4 i4FsMITcpConnRemPort,
                          INT4 *pi4RetValFsMITcpConnRBCount)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRBCount (u4FsMITcpConnLocalAddress,
                                i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                                i4FsMITcpConnRemPort,
                                pi4RetValFsMITcpConnRBCount);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnRBSize
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpConnRBSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnRBSize (INT4 i4FsMITcpContextId,
                         UINT4 u4FsMITcpConnLocalAddress,
                         INT4 i4FsMITcpConnLocalPort,
                         UINT4 u4FsMITcpConnRemAddress,
                         INT4 i4FsMITcpConnRemPort,
                         INT4 *pi4RetValFsMITcpConnRBSize)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnRBSize (u4FsMITcpConnLocalAddress,
                               i4FsMITcpConnLocalPort, u4FsMITcpConnRemAddress,
                               i4FsMITcpConnRemPort,
                               pi4RetValFsMITcpConnRBSize);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpKaMainTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpKaMainTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpKaMainTmr (INT4 i4FsMITcpContextId,
                        UINT4 u4FsMITcpConnLocalAddress,
                        INT4 i4FsMITcpConnLocalPort,
                        UINT4 u4FsMITcpConnRemAddress,
                        INT4 i4FsMITcpConnRemPort,
                        INT4 *pi4RetValFsMITcpKaMainTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpKaMainTmr (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                              u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                              pi4RetValFsMITcpKaMainTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpKaRetransTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpKaRetransTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpKaRetransTmr (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 *pi4RetValFsMITcpKaRetransTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpKaRetransTmr (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 pi4RetValFsMITcpKaRetransTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpKaRetransCnt
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                retValFsMITcpKaRetransCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpKaRetransCnt (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 *pi4RetValFsMITcpKaRetransCnt)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpKaRetransCnt (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 pi4RetValFsMITcpKaRetransCnt);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMITcpKaMainTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                setValFsMITcpKaMainTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpKaMainTmr (INT4 i4FsMITcpContextId,
                        UINT4 u4FsMITcpConnLocalAddress,
                        INT4 i4FsMITcpConnLocalPort,
                        UINT4 u4FsMITcpConnRemAddress,
                        INT4 i4FsMITcpConnRemPort,
                        INT4 i4SetValFsMITcpKaMainTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhSetFsTcpKaMainTmr (u4FsMITcpConnLocalAddress, i4FsMITcpConnLocalPort,
                              u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                              i4SetValFsMITcpKaMainTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpKaRetransTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                setValFsMITcpKaRetransTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpKaRetransTmr (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 i4SetValFsMITcpKaRetransTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhSetFsTcpKaRetransTmr (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 i4SetValFsMITcpKaRetransTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsMITcpKaRetransCnt
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                setValFsMITcpKaRetransCnt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITcpKaRetransCnt (INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 i4SetValFsMITcpKaRetransCnt)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhSetFsTcpKaRetransCnt (u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 i4SetValFsMITcpKaRetransCnt);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMITcpKaMainTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                testValFsMITcpKaMainTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpKaMainTmr (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                           UINT4 u4FsMITcpConnLocalAddress,
                           INT4 i4FsMITcpConnLocalPort,
                           UINT4 u4FsMITcpConnRemAddress,
                           INT4 i4FsMITcpConnRemPort,
                           INT4 i4TestValFsMITcpKaMainTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpKaMainTmr (pu4ErrorCode, u4FsMITcpConnLocalAddress,
                                 i4FsMITcpConnLocalPort,
                                 u4FsMITcpConnRemAddress, i4FsMITcpConnRemPort,
                                 i4TestValFsMITcpKaMainTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpKaRetransTmr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                testValFsMITcpKaRetransTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpKaRetransTmr (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                              UINT4 u4FsMITcpConnLocalAddress,
                              INT4 i4FsMITcpConnLocalPort,
                              UINT4 u4FsMITcpConnRemAddress,
                              INT4 i4FsMITcpConnRemPort,
                              INT4 i4TestValFsMITcpKaRetransTmr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpKaRetransTmr (pu4ErrorCode, u4FsMITcpConnLocalAddress,
                                    i4FsMITcpConnLocalPort,
                                    u4FsMITcpConnRemAddress,
                                    i4FsMITcpConnRemPort,
                                    i4TestValFsMITcpKaRetransTmr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITcpKaRetransCnt
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort

                The Object 
                testValFsMITcpKaRetransCnt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITcpKaRetransCnt (UINT4 *pu4ErrorCode, INT4 i4FsMITcpContextId,
                              UINT4 u4FsMITcpConnLocalAddress,
                              INT4 i4FsMITcpConnLocalPort,
                              UINT4 u4FsMITcpConnRemAddress,
                              INT4 i4FsMITcpConnRemPort,
                              INT4 i4TestValFsMITcpKaRetransCnt)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMITcpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2FsTcpKaRetransCnt (pu4ErrorCode, u4FsMITcpConnLocalAddress,
                                    i4FsMITcpConnLocalPort,
                                    u4FsMITcpConnRemAddress,
                                    i4FsMITcpConnRemPort,
                                    i4TestValFsMITcpKaRetransCnt);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMITcpConnTable
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpConnLocalAddress
                FsMITcpConnLocalPort
                FsMITcpConnRemAddress
                FsMITcpConnRemPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMITcpConnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMITcpExtConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMITcpExtConnTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMITcpExtConnTable (INT4 i4FsMIStdContextId,
                                             INT4
                                             i4FsMIStdTcpConnectionLocalAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdTcpConnectionLocalAddress,
                                             UINT4
                                             u4FsMIStdTcpConnectionLocalPort,
                                             INT4
                                             i4FsMIStdTcpConnectionRemAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdTcpConnectionRemAddress,
                                             UINT4
                                             u4FsMIStdTcpConnectionRemPort)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhValidateIndexInstanceFsTcpExtConnTable
        (i4FsMIStdTcpConnectionLocalAddressType,
         pFsMIStdTcpConnectionLocalAddress, u4FsMIStdTcpConnectionLocalPort,
         i4FsMIStdTcpConnectionRemAddressType, pFsMIStdTcpConnectionRemAddress,
         u4FsMIStdTcpConnectionRemPort);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMITcpExtConnTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMITcpExtConnTable (INT4 *pi4FsMIStdContextId,
                                     INT4
                                     *pi4FsMIStdTcpConnectionLocalAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdTcpConnectionLocalAddress,
                                     UINT4 *pu4FsMIStdTcpConnectionLocalPort,
                                     INT4
                                     *pi4FsMIStdTcpConnectionRemAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdTcpConnectionRemAddress,
                                     UINT4 *pu4FsMIStdTcpConnectionRemPort)
{
    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return = nmhGetFirstIndexFsTcpExtConnTable
            (pi4FsMIStdTcpConnectionLocalAddressType,
             pFsMIStdTcpConnectionLocalAddress,
             pu4FsMIStdTcpConnectionLocalPort,
             pi4FsMIStdTcpConnectionRemAddressType,
             pFsMIStdTcpConnectionRemAddress, pu4FsMIStdTcpConnectionRemPort);
        if (SNMP_SUCCESS == i4Return)
        {
            *pi4FsMIStdContextId = i4TmpContext;
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMITcpExtConnTable
 Input       :  The Indices
                FsMIStdContextId
                nextFsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                nextFsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                nextFsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                nextFsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                nextFsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                nextFsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
                nextFsMIStdTcpConnectionRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMITcpExtConnTable (INT4 i4FsMIStdContextId,
                                    INT4 *pi4NextFsMIStdContextId,
                                    INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                    INT4
                                    *pi4NextFsMIStdTcpConnectionLocalAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionLocalAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsMIStdTcpConnectionLocalAddress,
                                    UINT4 u4FsMIStdTcpConnectionLocalPort,
                                    UINT4 *pu4NextFsMIStdTcpConnectionLocalPort,
                                    INT4 i4FsMIStdTcpConnectionRemAddressType,
                                    INT4
                                    *pi4NextFsMIStdTcpConnectionRemAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionRemAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsMIStdTcpConnectionRemAddress,
                                    UINT4 u4FsMIStdTcpConnectionRemPort,
                                    UINT4 *pu4NextFsMIStdTcpConnectionRemPort)
{
    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    /* Get next in current context */
    if (TcpSetContext (i4FsMIStdContextId) == SNMP_SUCCESS)
    {
        i4Return =
            nmhGetNextIndexFsTcpExtConnTable
            (i4FsMIStdTcpConnectionLocalAddressType,
             pi4NextFsMIStdTcpConnectionLocalAddressType,
             pFsMIStdTcpConnectionLocalAddress,
             pNextFsMIStdTcpConnectionLocalAddress,
             u4FsMIStdTcpConnectionLocalPort,
             pu4NextFsMIStdTcpConnectionLocalPort,
             i4FsMIStdTcpConnectionRemAddressType,
             pi4NextFsMIStdTcpConnectionRemAddressType,
             pFsMIStdTcpConnectionRemAddress,
             pNextFsMIStdTcpConnectionRemAddress, u4FsMIStdTcpConnectionRemPort,
             pu4NextFsMIStdTcpConnectionRemPort);
    }
    if (SNMP_SUCCESS == i4Return)
    {
        *pi4NextFsMIStdContextId = i4FsMIStdContextId;
        TcpReleaseContext ();
        return SNMP_SUCCESS;
    }

    /* If that fails, GetFirst  in Next context */
    for (i4TmpContext = i4FsMIStdContextId + 1;
         i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return =
            nmhGetFirstIndexFsTcpExtConnTable
            (pi4NextFsMIStdTcpConnectionLocalAddressType,
             pNextFsMIStdTcpConnectionLocalAddress,
             pu4NextFsMIStdTcpConnectionLocalPort,
             pi4NextFsMIStdTcpConnectionRemAddressType,
             pNextFsMIStdTcpConnectionRemAddress,
             pu4NextFsMIStdTcpConnectionRemPort);
        if (SNMP_SUCCESS == i4Return)
        {
            *pi4NextFsMIStdContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    TcpReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITcpConnMD5Option
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConnMD5Option
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnMD5Option (INT4 i4FsMIStdContextId,
                            INT4 i4FsMIStdTcpConnectionLocalAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMIStdTcpConnectionLocalAddress,
                            UINT4 u4FsMIStdTcpConnectionLocalPort,
                            INT4 i4FsMIStdTcpConnectionRemAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMIStdTcpConnectionRemAddress,
                            UINT4 u4FsMIStdTcpConnectionRemPort,
                            INT4 *pi4RetValFsMITcpConnMD5Option)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnMD5Option (i4FsMIStdTcpConnectionLocalAddressType,
                                  pFsMIStdTcpConnectionLocalAddress,
                                  u4FsMIStdTcpConnectionLocalPort,
                                  i4FsMIStdTcpConnectionRemAddressType,
                                  pFsMIStdTcpConnectionRemAddress,
                                  u4FsMIStdTcpConnectionRemPort,
                                  pi4RetValFsMITcpConnMD5Option);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnMD5ErrCtr
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConnMD5ErrCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnMD5ErrCtr (INT4 i4FsMIStdContextId,
                            INT4 i4FsMIStdTcpConnectionLocalAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMIStdTcpConnectionLocalAddress,
                            UINT4 u4FsMIStdTcpConnectionLocalPort,
                            INT4 i4FsMIStdTcpConnectionRemAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMIStdTcpConnectionRemAddress,
                            UINT4 u4FsMIStdTcpConnectionRemPort,
                            INT4 *pi4RetValFsMITcpConnMD5ErrCtr)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetFsTcpConnMD5ErrCtr (i4FsMIStdTcpConnectionLocalAddressType,
                                  pFsMIStdTcpConnectionLocalAddress,
                                  u4FsMIStdTcpConnectionLocalPort,
                                  i4FsMIStdTcpConnectionRemAddressType,
                                  pFsMIStdTcpConnectionRemAddress,
                                  u4FsMIStdTcpConnectionRemPort,
                                  pi4RetValFsMITcpConnMD5ErrCtr);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConnTcpAOOption
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConnTcpAOOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConnTcpAOOption (INT4 i4FsMIStdContextId,
                              INT4 i4FsMIStdTcpConnectionLocalAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIStdTcpConnectionLocalAddress,
                              UINT4 u4FsMIStdTcpConnectionLocalPort,
                              INT4 i4FsMIStdTcpConnectionRemAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIStdTcpConnectionRemAddress,
                              UINT4 u4FsMIStdTcpConnectionRemPort,
                              INT4 *pi4RetValFsMITcpConnTcpAOOption)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConnTcpAOOption (i4FsMIStdTcpConnectionLocalAddressType,
                                    pFsMIStdTcpConnectionLocalAddress,
                                    u4FsMIStdTcpConnectionLocalPort,
                                    i4FsMIStdTcpConnectionRemAddressType,
                                    pFsMIStdTcpConnectionRemAddress,
                                    u4FsMIStdTcpConnectionRemPort,
                                    pi4RetValFsMITcpConnTcpAOOption);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAOCurKeyId
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAOCurKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAOCurKeyId (INT4 i4FsMIStdContextId,
                               INT4 i4FsMIStdTcpConnectionLocalAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdTcpConnectionLocalAddress,
                               UINT4 u4FsMIStdTcpConnectionLocalPort,
                               INT4 i4FsMIStdTcpConnectionRemAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdTcpConnectionRemAddress,
                               UINT4 u4FsMIStdTcpConnectionRemPort,
                               INT4 *pi4RetValFsMITcpConTcpAOCurKeyId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAOCurKeyId (i4FsMIStdTcpConnectionLocalAddressType,
                                     pFsMIStdTcpConnectionLocalAddress,
                                     u4FsMIStdTcpConnectionLocalPort,
                                     i4FsMIStdTcpConnectionRemAddressType,
                                     pFsMIStdTcpConnectionRemAddress,
                                     u4FsMIStdTcpConnectionRemPort,
                                     pi4RetValFsMITcpConTcpAOCurKeyId);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAORnextKeyId
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAORnextKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAORnextKeyId (INT4 i4FsMIStdContextId,
                                 INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionLocalAddress,
                                 UINT4 u4FsMIStdTcpConnectionLocalPort,
                                 INT4 i4FsMIStdTcpConnectionRemAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionRemAddress,
                                 UINT4 u4FsMIStdTcpConnectionRemPort,
                                 INT4 *pi4RetValFsMITcpConTcpAORnextKeyId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAORnextKeyId (i4FsMIStdTcpConnectionLocalAddressType,
                                       pFsMIStdTcpConnectionLocalAddress,
                                       u4FsMIStdTcpConnectionLocalPort,
                                       i4FsMIStdTcpConnectionRemAddressType,
                                       pFsMIStdTcpConnectionRemAddress,
                                       u4FsMIStdTcpConnectionRemPort,
                                       pi4RetValFsMITcpConTcpAORnextKeyId);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAORcvKeyId
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAORcvKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAORcvKeyId (INT4 i4FsMIStdContextId,
                               INT4 i4FsMIStdTcpConnectionLocalAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdTcpConnectionLocalAddress,
                               UINT4 u4FsMIStdTcpConnectionLocalPort,
                               INT4 i4FsMIStdTcpConnectionRemAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdTcpConnectionRemAddress,
                               UINT4 u4FsMIStdTcpConnectionRemPort,
                               INT4 *pi4RetValFsMITcpConTcpAORcvKeyId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAORcvKeyId (i4FsMIStdTcpConnectionLocalAddressType,
                                     pFsMIStdTcpConnectionLocalAddress,
                                     u4FsMIStdTcpConnectionLocalPort,
                                     i4FsMIStdTcpConnectionRemAddressType,
                                     pFsMIStdTcpConnectionRemAddress,
                                     u4FsMIStdTcpConnectionRemPort,
                                     pi4RetValFsMITcpConTcpAORcvKeyId);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAORcvRnextKeyId
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAORcvRnextKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAORcvRnextKeyId (INT4 i4FsMIStdContextId,
                                    INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionLocalAddress,
                                    UINT4 u4FsMIStdTcpConnectionLocalPort,
                                    INT4 i4FsMIStdTcpConnectionRemAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionRemAddress,
                                    UINT4 u4FsMIStdTcpConnectionRemPort,
                                    INT4 *pi4RetValFsMITcpConTcpAORcvRnextKeyId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAORcvRnextKeyId
        (i4FsMIStdTcpConnectionLocalAddressType,
         pFsMIStdTcpConnectionLocalAddress, u4FsMIStdTcpConnectionLocalPort,
         i4FsMIStdTcpConnectionRemAddressType, pFsMIStdTcpConnectionRemAddress,
         u4FsMIStdTcpConnectionRemPort, pi4RetValFsMITcpConTcpAORcvRnextKeyId);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAOConnErrCtr
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAOConnErrCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAOConnErrCtr (INT4 i4FsMIStdContextId,
                                 INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionLocalAddress,
                                 UINT4 u4FsMIStdTcpConnectionLocalPort,
                                 INT4 i4FsMIStdTcpConnectionRemAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionRemAddress,
                                 UINT4 u4FsMIStdTcpConnectionRemPort,
                                 UINT4 *pu4RetValFsMITcpConTcpAOConnErrCtr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAOConnErrCtr (i4FsMIStdTcpConnectionLocalAddressType,
                                       pFsMIStdTcpConnectionLocalAddress,
                                       u4FsMIStdTcpConnectionLocalPort,
                                       i4FsMIStdTcpConnectionRemAddressType,
                                       pFsMIStdTcpConnectionRemAddress,
                                       u4FsMIStdTcpConnectionRemPort,
                                       pu4RetValFsMITcpConTcpAOConnErrCtr);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAOSndSne
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAOSndSne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAOSndSne (INT4 i4FsMIStdContextId,
                             INT4 i4FsMIStdTcpConnectionLocalAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIStdTcpConnectionLocalAddress,
                             UINT4 u4FsMIStdTcpConnectionLocalPort,
                             INT4 i4FsMIStdTcpConnectionRemAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIStdTcpConnectionRemAddress,
                             UINT4 u4FsMIStdTcpConnectionRemPort,
                             INT4 *pi4RetValFsMITcpConTcpAOSndSne)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAOSndSne (i4FsMIStdTcpConnectionLocalAddressType,
                                   pFsMIStdTcpConnectionLocalAddress,
                                   u4FsMIStdTcpConnectionLocalPort,
                                   i4FsMIStdTcpConnectionRemAddressType,
                                   pFsMIStdTcpConnectionRemAddress,
                                   u4FsMIStdTcpConnectionRemPort,
                                   pi4RetValFsMITcpConTcpAOSndSne);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAORcvSne
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMITcpConTcpAORcvSne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAORcvSne (INT4 i4FsMIStdContextId,
                             INT4 i4FsMIStdTcpConnectionLocalAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIStdTcpConnectionLocalAddress,
                             UINT4 u4FsMIStdTcpConnectionLocalPort,
                             INT4 i4FsMIStdTcpConnectionRemAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIStdTcpConnectionRemAddress,
                             UINT4 u4FsMIStdTcpConnectionRemPort,
                             INT4 *pi4RetValFsMITcpConTcpAORcvSne)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAORcvSne (i4FsMIStdTcpConnectionLocalAddressType,
                                   pFsMIStdTcpConnectionLocalAddress,
                                   u4FsMIStdTcpConnectionLocalPort,
                                   i4FsMIStdTcpConnectionRemAddressType,
                                   pFsMIStdTcpConnectionRemAddress,
                                   u4FsMIStdTcpConnectionRemPort,
                                   pi4RetValFsMITcpConTcpAORcvSne);
    TcpReleaseContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMITcpAoConnTestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMITcpAoConnTestTable
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpAoConnTestLclAdrType
                FsMITcpAoConnTestLclAdress
                FsMITcpAoConnTestLclPort
                FsMITcpAoConnTestRmtAdrType
                FsMITcpAoConnTestRmtAdress
                FsMITcpAoConnTestRmtPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMITcpAoConnTestTable (INT4 i4FsMITcpContextId,
                                                INT4
                                                i4FsMITcpAoConnTestLclAdrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMITcpAoConnTestLclAdress,
                                                UINT4
                                                u4FsMITcpAoConnTestLclPort,
                                                INT4
                                                i4FsMITcpAoConnTestRmtAdrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMITcpAoConnTestRmtAdress,
                                                UINT4
                                                u4FsMITcpAoConnTestRmtPort)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhValidateIndexInstanceFsTcpAoConnTestTable
        (i4FsMITcpAoConnTestLclAdrType, pFsMITcpAoConnTestLclAdress,
         u4FsMITcpAoConnTestLclPort, i4FsMITcpAoConnTestRmtAdrType,
         pFsMITcpAoConnTestRmtAdress, u4FsMITcpAoConnTestRmtPort);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMITcpAoConnTestTable
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpAoConnTestLclAdrType
                FsMITcpAoConnTestLclAdress
                FsMITcpAoConnTestLclPort
                FsMITcpAoConnTestRmtAdrType
                FsMITcpAoConnTestRmtAdress
                FsMITcpAoConnTestRmtPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMITcpAoConnTestTable (INT4 *pi4FsMITcpContextId,
                                        INT4 *pi4FsMITcpAoConnTestLclAdrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMITcpAoConnTestLclAdress,
                                        UINT4 *pu4FsMITcpAoConnTestLclPort,
                                        INT4 *pi4FsMITcpAoConnTestRmtAdrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMITcpAoConnTestRmtAdress,
                                        UINT4 *pu4FsMITcpAoConnTestRmtPort)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext ((UINT4) i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i1Return = nmhGetFirstIndexFsTcpAoConnTestTable
            (pi4FsMITcpAoConnTestLclAdrType,
             pFsMITcpAoConnTestLclAdress,
             pu4FsMITcpAoConnTestLclPort,
             pi4FsMITcpAoConnTestRmtAdrType,
             pFsMITcpAoConnTestRmtAdress, pu4FsMITcpAoConnTestRmtPort);
        if (SNMP_SUCCESS == i1Return)
        {
            *pi4FsMITcpContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMITcpAoConnTestTable
 Input       :  The Indices
                FsMITcpContextId
                nextFsMITcpContextId
                FsMITcpAoConnTestLclAdrType
                nextFsMITcpAoConnTestLclAdrType
                FsMITcpAoConnTestLclAdress
                nextFsMITcpAoConnTestLclAdress
                FsMITcpAoConnTestLclPort
                nextFsMITcpAoConnTestLclPort
                FsMITcpAoConnTestRmtAdrType
                nextFsMITcpAoConnTestRmtAdrType
                FsMITcpAoConnTestRmtAdress
                nextFsMITcpAoConnTestRmtAdress
                FsMITcpAoConnTestRmtPort
                nextFsMITcpAoConnTestRmtPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMITcpAoConnTestTable (INT4 i4FsMITcpContextId,
                                       INT4 *pi4NextFsMITcpContextId,
                                       INT4 i4FsMITcpAoConnTestLclAdrType,
                                       INT4 *pi4NextFsMITcpAoConnTestLclAdrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMITcpAoConnTestLclAdress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMITcpAoConnTestLclAdress,
                                       UINT4 u4FsMITcpAoConnTestLclPort,
                                       UINT4 *pu4NextFsMITcpAoConnTestLclPort,
                                       INT4 i4FsMITcpAoConnTestRmtAdrType,
                                       INT4 *pi4NextFsMITcpAoConnTestRmtAdrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMITcpAoConnTestRmtAdress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMITcpAoConnTestRmtAdress,
                                       UINT4 u4FsMITcpAoConnTestRmtPort,
                                       UINT4 *pu4NextFsMITcpAoConnTestRmtPort)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    /* Get next in current context */
    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_SUCCESS)
    {
        i1Return =
            nmhGetNextIndexFsTcpAoConnTestTable (i4FsMITcpAoConnTestLclAdrType,
                                                 pi4NextFsMITcpAoConnTestLclAdrType,
                                                 pFsMITcpAoConnTestLclAdress,
                                                 pNextFsMITcpAoConnTestLclAdress,
                                                 u4FsMITcpAoConnTestLclPort,
                                                 pu4NextFsMITcpAoConnTestLclPort,
                                                 i4FsMITcpAoConnTestRmtAdrType,
                                                 pi4NextFsMITcpAoConnTestRmtAdrType,
                                                 pFsMITcpAoConnTestRmtAdress,
                                                 pNextFsMITcpAoConnTestRmtAdress,
                                                 u4FsMITcpAoConnTestRmtPort,
                                                 pu4NextFsMITcpAoConnTestRmtPort);
    }
    if (SNMP_SUCCESS == i1Return)
    {
        *pi4NextFsMITcpContextId = i4FsMITcpContextId;
        TcpReleaseContext ();
        return SNMP_SUCCESS;
    }

    /* If that fails, GetFirst  in Next context */
    for (i4TmpContext = i4FsMITcpContextId + 1;
         i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext ((UINT4) i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i1Return =
            nmhGetFirstIndexFsTcpAoConnTestTable
            (pi4NextFsMITcpAoConnTestLclAdrType,
             pNextFsMITcpAoConnTestLclAdress, pu4NextFsMITcpAoConnTestLclPort,
             pi4NextFsMITcpAoConnTestRmtAdrType,
             pNextFsMITcpAoConnTestRmtAdress, pu4NextFsMITcpAoConnTestRmtPort);
        if (SNMP_SUCCESS == i1Return)
        {
            *pi4NextFsMITcpContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAOIcmpIgnCtr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpAoConnTestLclAdrType
                FsMITcpAoConnTestLclAdress
                FsMITcpAoConnTestLclPort
                FsMITcpAoConnTestRmtAdrType
                FsMITcpAoConnTestRmtAdress
                FsMITcpAoConnTestRmtPort

                The Object 
                retValFsMITcpConTcpAOIcmpIgnCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAOIcmpIgnCtr (INT4 i4FsMITcpContextId,
                                 INT4 i4FsMITcpAoConnTestLclAdrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMITcpAoConnTestLclAdress,
                                 UINT4 u4FsMITcpAoConnTestLclPort,
                                 INT4 i4FsMITcpAoConnTestRmtAdrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMITcpAoConnTestRmtAdress,
                                 UINT4 u4FsMITcpAoConnTestRmtPort,
                                 UINT4 *pu4RetValFsMITcpConTcpAOIcmpIgnCtr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAOIcmpIgnCtr (i4FsMITcpAoConnTestLclAdrType,
                                       pFsMITcpAoConnTestLclAdress,
                                       u4FsMITcpAoConnTestLclPort,
                                       i4FsMITcpAoConnTestRmtAdrType,
                                       pFsMITcpAoConnTestRmtAdress,
                                       u4FsMITcpAoConnTestRmtPort,
                                       pu4RetValFsMITcpConTcpAOIcmpIgnCtr);
    TcpReleaseContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMITcpConTcpAOSilentAccptCtr
 Input       :  The Indices
                FsMITcpContextId
                FsMITcpAoConnTestLclAdrType
                FsMITcpAoConnTestLclAdress
                FsMITcpAoConnTestLclPort
                FsMITcpAoConnTestRmtAdrType
                FsMITcpAoConnTestRmtAdress
                FsMITcpAoConnTestRmtPort

                The Object 
                retValFsMITcpConTcpAOSilentAccptCtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITcpConTcpAOSilentAccptCtr (INT4 i4FsMITcpContextId,
                                     INT4 i4FsMITcpAoConnTestLclAdrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMITcpAoConnTestLclAdress,
                                     UINT4 u4FsMITcpAoConnTestLclPort,
                                     INT4 i4FsMITcpAoConnTestRmtAdrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMITcpAoConnTestRmtAdress,
                                     UINT4 u4FsMITcpAoConnTestRmtPort,
                                     UINT4
                                     *pu4RetValFsMITcpConTcpAOSilentAccptCtr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (TcpSetContext ((UINT4) i4FsMITcpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsTcpConTcpAOSilentAccptCtr (i4FsMITcpAoConnTestLclAdrType,
                                           pFsMITcpAoConnTestLclAdress,
                                           u4FsMITcpAoConnTestLclPort,
                                           i4FsMITcpAoConnTestRmtAdrType,
                                           pFsMITcpAoConnTestRmtAdress,
                                           u4FsMITcpAoConnTestRmtPort,
                                           pu4RetValFsMITcpConTcpAOSilentAccptCtr);
    TcpReleaseContext ();
    return i1Return;
}
