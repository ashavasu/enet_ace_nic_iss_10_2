/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmstclw.c,v 1.2 2013/03/16 12:10:52 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmstclw.h"

#ifndef LNXIP4_WANTED
# include  "tcpinc.h"
# include  "fsmptclw.h"
# include  "stdtcplw.h"
# include  "tcputils.h"

/* LOW LEVEL Routines for Table : FsMIStdContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdContextTable
 Input       :  The Indices
                FsMIStdContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdContextTable (INT4 i4FsMIStdContextId)
{
    return (nmhValidateIndexInstanceFsMIContextTable (i4FsMIStdContextId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdContextTable
 Input       :  The Indices
                FsMIStdContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdContextTable (INT4 *pi4FsMIStdContextId)
{
    return (nmhGetFirstIndexFsMIContextTable (pi4FsMIStdContextId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdContextTable
 Input       :  The Indices
                FsMIStdContextId
                nextFsMIStdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdContextTable (INT4 i4FsMIStdContextId,
                                    INT4 *pi4NextFsMIStdContextId)
{
    return (nmhGetNextIndexFsMIContextTable
            (i4FsMIStdContextId, pi4NextFsMIStdContextId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpRtoAlgorithm
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpRtoAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpRtoAlgorithm (INT4 i4FsMIStdContextId,
                              INT4 *pi4RetValFsMIStdTcpRtoAlgorithm)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpRtoAlgorithm (pi4RetValFsMIStdTcpRtoAlgorithm);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpRtoMin
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpRtoMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpRtoMin (INT4 i4FsMIStdContextId,
                        INT4 *pi4RetValFsMIStdTcpRtoMin)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpRtoMin (pi4RetValFsMIStdTcpRtoMin);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpRtoMax
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpRtoMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpRtoMax (INT4 i4FsMIStdContextId,
                        INT4 *pi4RetValFsMIStdTcpRtoMax)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpRtoMax (pi4RetValFsMIStdTcpRtoMax);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpMaxConn
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpMaxConn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpMaxConn (INT4 i4FsMIStdContextId,
                         INT4 *pi4RetValFsMIStdTcpMaxConn)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpMaxConn (pi4RetValFsMIStdTcpMaxConn);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpActiveOpens
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpActiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpActiveOpens (INT4 i4FsMIStdContextId,
                             UINT4 *pu4RetValFsMIStdTcpActiveOpens)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpActiveOpens (pu4RetValFsMIStdTcpActiveOpens);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpPassiveOpens
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpPassiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpPassiveOpens (INT4 i4FsMIStdContextId,
                              UINT4 *pu4RetValFsMIStdTcpPassiveOpens)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpPassiveOpens (pu4RetValFsMIStdTcpPassiveOpens);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpAttemptFails
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpAttemptFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpAttemptFails (INT4 i4FsMIStdContextId,
                              UINT4 *pu4RetValFsMIStdTcpAttemptFails)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpAttemptFails (pu4RetValFsMIStdTcpAttemptFails);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpEstabResets
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpEstabResets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpEstabResets (INT4 i4FsMIStdContextId,
                             UINT4 *pu4RetValFsMIStdTcpEstabResets)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpEstabResets (pu4RetValFsMIStdTcpEstabResets);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpCurrEstab
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpCurrEstab
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpCurrEstab (INT4 i4FsMIStdContextId,
                           UINT4 *pu4RetValFsMIStdTcpCurrEstab)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpCurrEstab (pu4RetValFsMIStdTcpCurrEstab);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpInSegs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpInSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpInSegs (INT4 i4FsMIStdContextId,
                        UINT4 *pu4RetValFsMIStdTcpInSegs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpInSegs (pu4RetValFsMIStdTcpInSegs);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpOutSegs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpOutSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpOutSegs (INT4 i4FsMIStdContextId,
                         UINT4 *pu4RetValFsMIStdTcpOutSegs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpOutSegs (pu4RetValFsMIStdTcpOutSegs);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpRetransSegs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpRetransSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpRetransSegs (INT4 i4FsMIStdContextId,
                             UINT4 *pu4RetValFsMIStdTcpRetransSegs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpRetransSegs (pu4RetValFsMIStdTcpRetransSegs);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpInErrs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpInErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpInErrs (INT4 i4FsMIStdContextId,
                        UINT4 *pu4RetValFsMIStdTcpInErrs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpInErrs (pu4RetValFsMIStdTcpInErrs);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpOutRsts
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpOutRsts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpOutRsts (INT4 i4FsMIStdContextId,
                         UINT4 *pu4RetValFsMIStdTcpOutRsts)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpOutRsts (pu4RetValFsMIStdTcpOutRsts);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpHCInSegs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpHCInSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpHCInSegs (INT4 i4FsMIStdContextId,
                          tSNMP_COUNTER64_TYPE * pu8RetValFsMIStdTcpHCInSegs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpHCInSegs (pu8RetValFsMIStdTcpHCInSegs);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpHCOutSegs
 Input       :  The Indices
                FsMIStdContextId

                The Object 
                retValFsMIStdTcpHCOutSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpHCOutSegs (INT4 i4FsMIStdContextId,
                           tSNMP_COUNTER64_TYPE * pu8RetValFsMIStdTcpHCOutSegs)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return = nmhGetTcpHCOutSegs (pu8RetValFsMIStdTcpHCOutSegs);
    TcpReleaseContext ();
    return i4Return;
}

/* LOW LEVEL Routines for Table : FsMIStdTcpConnectionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdTcpConnectionTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdTcpConnectionTable (INT4 i4FsMIStdContextId,
                                                   INT4
                                                   i4FsMIStdTcpConnectionLocalAddressType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMIStdTcpConnectionLocalAddress,
                                                   UINT4
                                                   u4FsMIStdTcpConnectionLocalPort,
                                                   INT4
                                                   i4FsMIStdTcpConnectionRemAddressType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMIStdTcpConnectionRemAddress,
                                                   UINT4
                                                   u4FsMIStdTcpConnectionRemPort)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhValidateIndexInstanceTcpConnectionTable
        (i4FsMIStdTcpConnectionLocalAddressType,
         pFsMIStdTcpConnectionLocalAddress, u4FsMIStdTcpConnectionLocalPort,
         i4FsMIStdTcpConnectionRemAddressType, pFsMIStdTcpConnectionRemAddress,
         u4FsMIStdTcpConnectionRemPort);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdTcpConnectionTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdTcpConnectionTable (INT4 *pi4FsMIStdContextId,
                                           INT4
                                           *pi4FsMIStdTcpConnectionLocalAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdTcpConnectionLocalAddress,
                                           UINT4
                                           *pu4FsMIStdTcpConnectionLocalPort,
                                           INT4
                                           *pi4FsMIStdTcpConnectionRemAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdTcpConnectionRemAddress,
                                           UINT4
                                           *pu4FsMIStdTcpConnectionRemPort)
{
    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return =
            nmhGetFirstIndexTcpConnectionTable
            (pi4FsMIStdTcpConnectionLocalAddressType,
             pFsMIStdTcpConnectionLocalAddress,
             pu4FsMIStdTcpConnectionLocalPort,
             pi4FsMIStdTcpConnectionRemAddressType,
             pFsMIStdTcpConnectionRemAddress, pu4FsMIStdTcpConnectionRemPort);

        if (SNMP_SUCCESS == i4Return)
        {
            *pi4FsMIStdContextId = i4TmpContext;
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdTcpConnectionTable
 Input       :  The Indices
                FsMIStdContextId
                nextFsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                nextFsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                nextFsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                nextFsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                nextFsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                nextFsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
                nextFsMIStdTcpConnectionRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdTcpConnectionTable (INT4 i4FsMIStdContextId,
                                          INT4 *pi4NextFsMIStdContextId,
                                          INT4
                                          i4FsMIStdTcpConnectionLocalAddressType,
                                          INT4
                                          *pi4NextFsMIStdTcpConnectionLocalAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdTcpConnectionLocalAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMIStdTcpConnectionLocalAddress,
                                          UINT4 u4FsMIStdTcpConnectionLocalPort,
                                          UINT4
                                          *pu4NextFsMIStdTcpConnectionLocalPort,
                                          INT4
                                          i4FsMIStdTcpConnectionRemAddressType,
                                          INT4
                                          *pi4NextFsMIStdTcpConnectionRemAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdTcpConnectionRemAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMIStdTcpConnectionRemAddress,
                                          UINT4 u4FsMIStdTcpConnectionRemPort,
                                          UINT4
                                          *pu4NextFsMIStdTcpConnectionRemPort)
{

    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;
    /* Get next in current context */
    if (TcpSetContext (i4FsMIStdContextId) == SNMP_SUCCESS)
    {
        i4Return = nmhGetNextIndexTcpConnectionTable
            (i4FsMIStdTcpConnectionLocalAddressType,
             pi4NextFsMIStdTcpConnectionLocalAddressType,
             pFsMIStdTcpConnectionLocalAddress,
             pNextFsMIStdTcpConnectionLocalAddress,
             u4FsMIStdTcpConnectionLocalPort,
             pu4NextFsMIStdTcpConnectionLocalPort,
             i4FsMIStdTcpConnectionRemAddressType,
             pi4NextFsMIStdTcpConnectionRemAddressType,
             pFsMIStdTcpConnectionRemAddress,
             pNextFsMIStdTcpConnectionRemAddress,
             u4FsMIStdTcpConnectionRemPort, pu4NextFsMIStdTcpConnectionRemPort);
    }

    if (SNMP_SUCCESS == i4Return)
    {
        *pi4NextFsMIStdContextId = i4FsMIStdContextId;
        TcpReleaseContext ();
        return SNMP_SUCCESS;
    }

    /* If that fails, GetFirst  in Next context */
    for (i4TmpContext = i4FsMIStdContextId + 1;
         i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return = nmhGetFirstIndexTcpConnectionTable
            (pi4NextFsMIStdTcpConnectionLocalAddressType,
             pNextFsMIStdTcpConnectionLocalAddress,
             pu4NextFsMIStdTcpConnectionLocalPort,
             pi4NextFsMIStdTcpConnectionRemAddressType,
             pNextFsMIStdTcpConnectionRemAddress,
             pu4NextFsMIStdTcpConnectionRemPort);

        if (SNMP_SUCCESS == i4Return)
        {
            *pi4NextFsMIStdContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    TcpReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpConnectionState
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMIStdTcpConnectionState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpConnectionState (INT4 i4FsMIStdContextId,
                                 INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionLocalAddress,
                                 UINT4 u4FsMIStdTcpConnectionLocalPort,
                                 INT4 i4FsMIStdTcpConnectionRemAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionRemAddress,
                                 UINT4 u4FsMIStdTcpConnectionRemPort,
                                 INT4 *pi4RetValFsMIStdTcpConnectionState)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetTcpConnectionState (i4FsMIStdTcpConnectionLocalAddressType,
                                  pFsMIStdTcpConnectionLocalAddress,
                                  u4FsMIStdTcpConnectionLocalPort,
                                  i4FsMIStdTcpConnectionRemAddressType,
                                  pFsMIStdTcpConnectionRemAddress,
                                  u4FsMIStdTcpConnectionRemPort,
                                  pi4RetValFsMIStdTcpConnectionState);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpConnectionProcess
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                retValFsMIStdTcpConnectionProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpConnectionProcess (INT4 i4FsMIStdContextId,
                                   INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdTcpConnectionLocalAddress,
                                   UINT4 u4FsMIStdTcpConnectionLocalPort,
                                   INT4 i4FsMIStdTcpConnectionRemAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdTcpConnectionRemAddress,
                                   UINT4 u4FsMIStdTcpConnectionRemPort,
                                   UINT4 *pu4RetValFsMIStdTcpConnectionProcess)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetTcpConnectionProcess (i4FsMIStdTcpConnectionLocalAddressType,
                                    pFsMIStdTcpConnectionLocalAddress,
                                    u4FsMIStdTcpConnectionLocalPort,
                                    i4FsMIStdTcpConnectionRemAddressType,
                                    pFsMIStdTcpConnectionRemAddress,
                                    u4FsMIStdTcpConnectionRemPort,
                                    pu4RetValFsMIStdTcpConnectionProcess);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdTcpConnectionState
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                setValFsMIStdTcpConnectionState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdTcpConnectionState (INT4 i4FsMIStdContextId,
                                 INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionLocalAddress,
                                 UINT4 u4FsMIStdTcpConnectionLocalPort,
                                 INT4 i4FsMIStdTcpConnectionRemAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpConnectionRemAddress,
                                 UINT4 u4FsMIStdTcpConnectionRemPort,
                                 INT4 i4SetValFsMIStdTcpConnectionState)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhSetTcpConnectionState (i4FsMIStdTcpConnectionLocalAddressType,
                                  pFsMIStdTcpConnectionLocalAddress,
                                  u4FsMIStdTcpConnectionLocalPort,
                                  i4FsMIStdTcpConnectionRemAddressType,
                                  pFsMIStdTcpConnectionRemAddress,
                                  u4FsMIStdTcpConnectionRemPort,
                                  i4SetValFsMIStdTcpConnectionState);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdTcpConnectionState
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort

                The Object 
                testValFsMIStdTcpConnectionState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdTcpConnectionState (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdContextId,
                                    INT4 i4FsMIStdTcpConnectionLocalAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionLocalAddress,
                                    UINT4 u4FsMIStdTcpConnectionLocalPort,
                                    INT4 i4FsMIStdTcpConnectionRemAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdTcpConnectionRemAddress,
                                    UINT4 u4FsMIStdTcpConnectionRemPort,
                                    INT4 i4TestValFsMIStdTcpConnectionState)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i4Return;
    }

    i4Return =
        nmhTestv2TcpConnectionState (pu4ErrorCode,
                                     i4FsMIStdTcpConnectionLocalAddressType,
                                     pFsMIStdTcpConnectionLocalAddress,
                                     u4FsMIStdTcpConnectionLocalPort,
                                     i4FsMIStdTcpConnectionRemAddressType,
                                     pFsMIStdTcpConnectionRemAddress,
                                     u4FsMIStdTcpConnectionRemPort,
                                     i4TestValFsMIStdTcpConnectionState);
    TcpReleaseContext ();
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdTcpConnectionTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpConnectionLocalAddressType
                FsMIStdTcpConnectionLocalAddress
                FsMIStdTcpConnectionLocalPort
                FsMIStdTcpConnectionRemAddressType
                FsMIStdTcpConnectionRemAddress
                FsMIStdTcpConnectionRemPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdTcpConnectionTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdTcpListenerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdTcpListenerTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpListenerLocalAddressType
                FsMIStdTcpListenerLocalAddress
                FsMIStdTcpListenerLocalPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdTcpListenerTable (INT4 i4FsMIStdContextId,
                                                 INT4
                                                 i4FsMIStdTcpListenerLocalAddressType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIStdTcpListenerLocalAddress,
                                                 UINT4
                                                 u4FsMIStdTcpListenerLocalPort)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhValidateIndexInstanceTcpListenerTable
        (i4FsMIStdTcpListenerLocalAddressType, pFsMIStdTcpListenerLocalAddress,
         u4FsMIStdTcpListenerLocalPort);
    TcpReleaseContext ();
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdTcpListenerTable
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpListenerLocalAddressType
                FsMIStdTcpListenerLocalAddress
                FsMIStdTcpListenerLocalPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdTcpListenerTable (INT4 *pi4FsMIStdContextId,
                                         INT4
                                         *pi4FsMIStdTcpListenerLocalAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdTcpListenerLocalAddress,
                                         UINT4 *pu4FsMIStdTcpListenerLocalPort)
{
    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    for (i4TmpContext = 0; i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return =
            nmhGetFirstIndexTcpListenerTable
            (pi4FsMIStdTcpListenerLocalAddressType,
             pFsMIStdTcpListenerLocalAddress, pu4FsMIStdTcpListenerLocalPort);

        if (SNMP_SUCCESS == i4Return)
        {
            *pi4FsMIStdContextId = i4TmpContext;
            return SNMP_SUCCESS;
        }
        TcpReleaseContext ();
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdTcpListenerTable
 Input       :  The Indices
                FsMIStdContextId
                nextFsMIStdContextId
                FsMIStdTcpListenerLocalAddressType
                nextFsMIStdTcpListenerLocalAddressType
                FsMIStdTcpListenerLocalAddress
                nextFsMIStdTcpListenerLocalAddress
                FsMIStdTcpListenerLocalPort
                nextFsMIStdTcpListenerLocalPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdTcpListenerTable (INT4 i4FsMIStdContextId,
                                        INT4 *pi4NextFsMIStdContextId,
                                        INT4
                                        i4FsMIStdTcpListenerLocalAddressType,
                                        INT4
                                        *pi4NextFsMIStdTcpListenerLocalAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdTcpListenerLocalAddress,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsMIStdTcpListenerLocalAddress,
                                        UINT4 u4FsMIStdTcpListenerLocalPort,
                                        UINT4
                                        *pu4NextFsMIStdTcpListenerLocalPort)
{

    INT4                i4Return = SNMP_FAILURE;
    INT4                i4TmpContext = 0;

    /* Get next in current context */
    if (TcpSetContext (i4FsMIStdContextId) == SNMP_SUCCESS)
    {
        i4Return =
            nmhGetNextIndexTcpListenerTable
            (i4FsMIStdTcpListenerLocalAddressType,
             pi4NextFsMIStdTcpListenerLocalAddressType,
             pFsMIStdTcpListenerLocalAddress,
             pNextFsMIStdTcpListenerLocalAddress, u4FsMIStdTcpListenerLocalPort,
             pu4NextFsMIStdTcpListenerLocalPort);
    }

    if (SNMP_SUCCESS == i4Return)
    {
        *pi4NextFsMIStdContextId = i4FsMIStdContextId;
        TcpReleaseContext ();
        return SNMP_SUCCESS;
    }

    /* If that fails, GetFirst  in Next context */
    for (i4TmpContext = i4FsMIStdContextId + 1;
         i4TmpContext < MAX_TCP_NUM_CONTEXT; i4TmpContext++)
    {
        if (TcpSetContext (i4TmpContext) == SNMP_FAILURE)
        {
            continue;
        }
        i4Return =
            nmhGetFirstIndexTcpListenerTable
            (pi4NextFsMIStdTcpListenerLocalAddressType,
             pNextFsMIStdTcpListenerLocalAddress,
             pu4NextFsMIStdTcpListenerLocalPort);

        if (SNMP_SUCCESS == i4Return)
        {
            *pi4NextFsMIStdContextId = i4TmpContext;
            TcpReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    TcpReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdTcpListenerProcess
 Input       :  The Indices
                FsMIStdContextId
                FsMIStdTcpListenerLocalAddressType
                FsMIStdTcpListenerLocalAddress
                FsMIStdTcpListenerLocalPort

                The Object 
                retValFsMIStdTcpListenerProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdTcpListenerProcess (INT4 i4FsMIStdContextId,
                                 INT4 i4FsMIStdTcpListenerLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdTcpListenerLocalAddress,
                                 UINT4 u4FsMIStdTcpListenerLocalPort,
                                 UINT4 *pu4RetValFsMIStdTcpListenerProcess)
{
    INT4                i4Return = SNMP_FAILURE;

    if (TcpSetContext (i4FsMIStdContextId) == SNMP_FAILURE)
    {
        return i4Return;
    }

    i4Return =
        nmhGetTcpListenerProcess (i4FsMIStdTcpListenerLocalAddressType,
                                  pFsMIStdTcpListenerLocalAddress,
                                  u4FsMIStdTcpListenerLocalPort,
                                  pu4RetValFsMIStdTcpListenerProcess);
    TcpReleaseContext ();
    return i4Return;
}
#else
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdContextTable
 Input       :  The Indices
                FsMIStdContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdContextTable (INT4 *pi4FsMIStdContextId)
{
    *pi4FsMIStdContextId = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdContextTable
 Input       :  The Indices
                FsMIStdContextId
                nextFsMIStdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdContextTable (INT4 i4FsMIStdContextId,
                                    INT4 *pi4NextFsMIStdContextId)
{
    UNUSED_PARAM (i4FsMIStdContextId);
    UNUSED_PARAM (pi4NextFsMIStdContextId);
    return SNMP_FAILURE;
}

#endif
