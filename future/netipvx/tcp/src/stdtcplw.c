/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: stdtcplw.c,v 1.30 2015/05/14 12:53:32 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#ifndef __TCPLW_C__
#define __TCPLW_C__

# include  "fssnmp.h"
#if defined (IP_WANTED) || defined (IP6_WANTED)
#ifndef LNXIP4_WANTED
# include  "tcpinc.h"
# include  "tcputils.h"
# include  "stdtcplw.h"
#include "tcpipvxdefs.h"
#endif
#endif
#ifdef LNXIP4_WANTED
#include "lr.h"
#include  "stdtcplw.h"
#include "lnxiputl.h"
#include "tcp.h"
#include  "tcputils.h"
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetTcpRtoAlgorithm
Input       :  The Indices

The Object 
retValTcpRtoAlgorithm
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpRtoAlgorithm (INT4 *pi4RetValTcpRtoAlgorithm)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    *pi4RetValTcpRtoAlgorithm =
        (INT4) GetTCPstat (gpTcpCurrContext->u4ContextId, RtoAlgorithm);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpRtoAlogorithm returned %d\n",
                 *pi4RetValTcpRtoAlgorithm);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_RTO_ALGM, (UINT4 *) pi4RetValTcpRtoAlgorithm)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);
}

/****************************************************************************
Function    :  nmhGetTcpRtoMin
Input       :  The Indices

The Object 
retValTcpRtoMin
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpRtoMin (INT4 *pi4RetValTcpRtoMin)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pi4RetValTcpRtoMin = GetTCPstat (gpTcpCurrContext->u4ContextId, RtoMin);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpRtoMin returned %d\n", *pi4RetValTcpRtoMin);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_RTO_MIN, (UINT4 *) pi4RetValTcpRtoMin)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpRtoMax
Input       :  The Indices

The Object 
retValTcpRtoMax
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpRtoMax (INT4 *pi4RetValTcpRtoMax)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pi4RetValTcpRtoMax = GetTCPstat (gpTcpCurrContext->u4ContextId, RtoMax);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpRtoMax returned %d\n", *pi4RetValTcpRtoMax);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_RTO_MAX, (UINT4 *) pi4RetValTcpRtoMax)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpMaxConn
Input       :  The Indices

The Object 
retValTcpMaxConn
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpMaxConn (INT4 *pi4RetValTcpMaxConn)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pi4RetValTcpMaxConn = GetTCPstat (gpTcpCurrContext->u4ContextId, MaxConn);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpMaxConn returned %d\n", *pi4RetValTcpMaxConn);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_MAX_CONN, (UINT4 *) pi4RetValTcpMaxConn)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpActiveOpens
Input       :  The Indices

The Object 
retValTcpActiveOpens
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpActiveOpens (UINT4 *pu4RetValTcpActiveOpens)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpActiveOpens =
        GetTCPstat (gpTcpCurrContext->u4ContextId, ActiveOpens);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpActiveOpens returned %d\n",
                 *pu4RetValTcpActiveOpens);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_ACTV_OPENS, (UINT4 *) pu4RetValTcpActiveOpens)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpPassiveOpens
Input       :  The Indices

The Object 
retValTcpPassiveOpens
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpPassiveOpens (UINT4 *pu4RetValTcpPassiveOpens)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpPassiveOpens =
        GetTCPstat (gpTcpCurrContext->u4ContextId, PassiveOpens);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpPassiveOpens returned %d\n",
                 *pu4RetValTcpPassiveOpens);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_PASSIVE_OPENS,
                        (UINT4 *) pu4RetValTcpPassiveOpens) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpAttemptFails
Input       :  The Indices

The Object 
retValTcpAttemptFails
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpAttemptFails (UINT4 *pu4RetValTcpAttemptFails)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpAttemptFails =
        GetTCPstat (gpTcpCurrContext->u4ContextId, AttemptFails);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpAttemptFails returned %d\n",
                 *pu4RetValTcpAttemptFails);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_ATTEMPT_FAILS,
                        (UINT4 *) pu4RetValTcpAttemptFails) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpEstabResets
Input       :  The Indices

The Object 
retValTcpEstabResets
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpEstabResets (UINT4 *pu4RetValTcpEstabResets)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpEstabResets =
        GetTCPstat (gpTcpCurrContext->u4ContextId, EstabResets);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpEstabResets returned %d\n",
                 *pu4RetValTcpEstabResets);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_ESTAB_RESETS,
                        (UINT4 *) pu4RetValTcpEstabResets) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpCurrEstab
Input       :  The Indices

The Object 
retValTcpCurrEstab
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpCurrEstab (UINT4 *pu4RetValTcpCurrEstab)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    INT4                i4Count = TCP_ZERO;
    UINT4               u4TcbIndex = TCP_ZERO;
    for (i4Count = u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB;
         u4TcbIndex++)
    {
        if (GetTCBContext (u4TcbIndex) != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }
        if (GetTCBstate (u4TcbIndex) == TCPS_ESTABLISHED ||
            GetTCBstate (u4TcbIndex) == TCPS_CLOSEWAIT)
        {
            i4Count++;
        }
    }
    *pu4RetValTcpCurrEstab = i4Count;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpCurrEstab returned %d\n", *pu4RetValTcpCurrEstab);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_CURR_ESTAB,
                        (UINT4 *) pu4RetValTcpCurrEstab) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpInSegs
Input       :  The Indices

The Object 
retValTcpInSegs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpInSegs (UINT4 *pu4RetValTcpInSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpInSegs = GetTCPstat (gpTcpCurrContext->u4ContextId, InSegs);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpInSegs returned %d\n", *pu4RetValTcpInSegs);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_IN_SEGS,
                        (UINT4 *) pu4RetValTcpInSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpOutSegs
Input       :  The Indices

The Object 
retValTcpOutSegs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpOutSegs (UINT4 *pu4RetValTcpOutSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined IP_WANTED || defined (IP6_WANTED)
    *pu4RetValTcpOutSegs = GetTCPstat (gpTcpCurrContext->u4ContextId, OutSegs);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpOutSegs returned %d\n", *pu4RetValTcpOutSegs);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_OUT_SEGS,
                        (UINT4 *) pu4RetValTcpOutSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpRetransSegs
Input       :  The Indices

The Object 
retValTcpRetransSegs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpRetransSegs (UINT4 *pu4RetValTcpRetransSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    *pu4RetValTcpRetransSegs =
        GetTCPstat (gpTcpCurrContext->u4ContextId, RetransSegs);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpRetransSegs returned %d\n",
                 *pu4RetValTcpRetransSegs);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_RETANS_SEGS,
                        (UINT4 *) pu4RetValTcpRetransSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpInErrs
Input       :  The Indices

The Object 
retValTcpInErrs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpInErrs (UINT4 *pu4RetValTcpInErrs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    *pu4RetValTcpInErrs = GetTCPstat (gpTcpCurrContext->u4ContextId, InErrs);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpInErrs returned %d\n", *pu4RetValTcpInErrs);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_IN_ERRS,
                        (UINT4 *) pu4RetValTcpInErrs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpOutRsts
Input       :  The Indices

The Object 
retValTcpOutRsts
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpOutRsts (UINT4 *pu4RetValTcpOutRsts)
{
    INT1                i1RetVal = SNMP_SUCCESS;

#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    *pu4RetValTcpOutRsts = GetTCPstat (gpTcpCurrContext->u4ContextId, OutRsts);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpOutRsts returned %d\n", *pu4RetValTcpOutRsts);
#endif
#endif

#ifdef LNXIP4_WANTED
    if (LnxIpGetObject (LNX_TCP_OUT_RSTS,
                        (UINT4 *) pu4RetValTcpOutRsts) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
#endif
    return (i1RetVal);

}

/****************************************************************************
Function    :  nmhGetTcpHCInSegs
Input       :  The Indices

The Object 
retValTcpHCInSegs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpHCInSegs (tSNMP_COUNTER64_TYPE * pu8RetValTcpHCInSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    pu8RetValTcpHCInSegs->lsn = gpTcpCurrContext->TcpStatParms.u8HcInSegs.u4Lo;
    pu8RetValTcpHCInSegs->msn = gpTcpCurrContext->TcpStatParms.u8HcInSegs.u4Hi;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpHCInSegs returned %ld\n", *pu8RetValTcpHCInSegs);
#endif
#endif

#ifdef LNXIP4_WANTED
    UINT4               u4RetValTcpInSegs;
    if (LnxIpGetObject (LNX_TCP_IN_SEGS, &u4RetValTcpInSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    pu8RetValTcpHCInSegs->lsn = u4RetValTcpInSegs;
    pu8RetValTcpHCInSegs->msn = LNX_ZERO;
#endif
    return (i1RetVal);
}

/****************************************************************************
Function    :  nmhGetTcpHCOutSegs
Input       :  The Indices

The Object 
retValTcpHCOutSegs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpHCOutSegs (tSNMP_COUNTER64_TYPE * pu8RetValTcpHCOutSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    pu8RetValTcpHCOutSegs->lsn =
        gpTcpCurrContext->TcpStatParms.u8HcOutSegs.u4Lo;
    pu8RetValTcpHCOutSegs->msn =
        gpTcpCurrContext->TcpStatParms.u8HcOutSegs.u4Hi;
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of TcpHcOutSegs returned %ld\n", *pu8RetValTcpHCOutSegs);
#endif
#endif

#ifdef LNXIP4_WANTED
    UINT4               u8RetValTcpHCOut;
    if (LnxIpGetObject (LNX_TCP_OUT_SEGS, &u8RetValTcpHCOut) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    pu8RetValTcpHCOutSegs->lsn = u8RetValTcpHCOut;
    pu8RetValTcpHCOutSegs->msn = LNX_ZERO;
#endif
    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : TcpConnectionTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceTcpConnectionTable
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceTcpConnectionTable
    (INT4 i4TcpConnectionLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
     UINT4 u4TcpConnectionLocalPort,
     INT4 i4TcpConnectionRemAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
     UINT4 u4TcpConnectionRemPort)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UINT4               u4LocalAddress;
    UINT4               u4RemoteAddress;
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4TcpCurLocalAddrType = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    UINT4               u4TcbContext;

    INT4                i4Count = TCP_ZERO;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;
    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;

    u4LocalAddress = TCP_ZERO;
    u4RemoteAddress = TCP_ZERO;
    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));

    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_IPV6_ADDR_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }
    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {

        u4TcpCurLocalAddrType = GetTCBLipAddType (u4TcbIndex);
        u4TcbContext = GetTCBContext (u4TcbIndex);

        MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
        MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

        if (u4TcbContext == gpTcpCurrContext->u4ContextId)
        {
            if (u4TcpCurLocalAddrType == TCP_IPV4_ADDR_TYPE)
            {
                u4TempLocalTcpAddr = GetTCBv4lip (u4TcbIndex);
                MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
                u4TempRemTcpAddr = GetTCBv4rip (u4TcbIndex);
                MEMCPY (&TcpCurRemoteAddr, &u4TempRemTcpAddr, sizeof (UINT4));
            }
            else if (u4TcpCurLocalAddrType == TCP_IPV6_ADDR_TYPE)
            {
                MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4TcbIndex),
                        sizeof (tIpAddr));
                MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4TcbIndex),
                        sizeof (tIpAddr));
            }
            if ((GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
                (GetTCBstate (u4TcbIndex) == TCPS_ABORT) ||
                (u4TcpConnectionLocalPort != GetTCBlport (u4TcbIndex)) ||
                (u4TcpConnectionRemPort != GetTCBrport (u4TcbIndex)) ||
                (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) != TCP_ZERO) ||
                (IpvxAddrCmp (&TcpCurRemoteAddr, &TempRemoteIP) != TCP_ZERO))
            {
                continue;
            }
            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "Validate index called with \n");

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "local address type = %d \n,"
                         "remote address type = %d \n",
                         i4TcpConnectionLocalAddressType,
                         i4TcpConnectionRemAddressType);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "local port = %d \nremote port = %d \n",
                         u4TcpConnectionLocalPort, u4TcpConnectionRemPort);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "remote ip addr = %x \nlocal ip addr = %x\n",
                         u4LocalAddress, u4RemoteAddress);

            TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                         "returned success. \n ");

        /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
            return SNMP_SUCCESS;
        }
    }
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Validate index called with \n");

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "local address type = %d \n,"
                 "remote address type = %d \n",
                 i4TcpConnectionLocalAddressType,
                 i4TcpConnectionRemAddressType);

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "local port = %d \nremote port = %d \n",
                 u4TcpConnectionLocalPort, u4TcpConnectionRemPort);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "remote ip addr = %x \nlocal ip addr = %x\n",
                 u4LocalAddress, u4RemoteAddress);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "returned failure. \n ");

    return SNMP_FAILURE;
    /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
#endif
#endif

#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
Function    :  nmhGetFirstIndexTcpConnectionTable
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexTcpConnectionTable
    (INT4 *pi4TcpConnectionLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
     UINT4 *pu4TcpConnectionLocalPort,
     INT4 *pi4TcpConnectionRemAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
     UINT4 *pu4TcpConnectionRemPort)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4FirstTcbIndex = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemoteTcpAddr = TCP_ZERO;
    UINT1               u1GotOne = FALSE;
    UINT4               u4TcbContext;
    INT4                i4Count = 0;

    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;

    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        u4TcbContext = GetTCBContext (u4TcbIndex);
        if (u4TcbContext != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        if (u1GotOne == FALSE)
        {
            u4FirstTcbIndex = u4TcbIndex;
            u1GotOne = TRUE;
        }

        if (u1GotOne == TRUE)
        {
            if (GetTCBLipAddType (u4TcbIndex) <
                GetTCBLipAddType (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBLipAddType (u4TcbIndex) >
                GetTCBLipAddType (u4FirstTcbIndex))
            {
                continue;
            }
            if (IpvxAddrCmp (&GetTCBlip (u4TcbIndex),
                             &GetTCBlip (u4FirstTcbIndex)) < TCP_ZERO)
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (IpvxAddrCmp (&GetTCBlip (u4TcbIndex),
                             &GetTCBlip (u4FirstTcbIndex)) > TCP_ZERO)
            {
                continue;
            }
            if (GetTCBlport (u4TcbIndex) < GetTCBlport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBlport (u4TcbIndex) > GetTCBlport (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBRipAddType (u4TcbIndex) <
                GetTCBRipAddType (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBRipAddType (u4TcbIndex) >
                GetTCBRipAddType (u4FirstTcbIndex))
            {
                continue;
            }
            if (IpvxAddrCmp (&GetTCBrip (u4TcbIndex),
                             &GetTCBrip (u4FirstTcbIndex)) < TCP_ZERO)
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (IpvxAddrCmp (&GetTCBrip (u4TcbIndex),
                             &GetTCBrip (u4FirstTcbIndex)) > TCP_ZERO)
            {
                continue;
            }
            if (GetTCBrport (u4TcbIndex) < GetTCBrport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBrport (u4TcbIndex) > GetTCBrport (u4FirstTcbIndex))
            {
                continue;
            }
        }
    }

    if (u1GotOne == FALSE)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Get of first index in TCP ConnTable failed.\n");

        return SNMP_FAILURE;
    }

    *pi4TcpConnectionLocalAddressType = GetTCBLipAddType (u4FirstTcbIndex);

    if (*pi4TcpConnectionLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        u4TempLocalTcpAddr = GetTCBv4lip (u4FirstTcbIndex);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList,
                &u4TempLocalTcpAddr, sizeof (UINT4));
        u4TempRemoteTcpAddr = GetTCBv4rip (u4FirstTcbIndex);
        u4TempRemoteTcpAddr = OSIX_HTONL (u4TempRemoteTcpAddr);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList,
                &u4TempRemoteTcpAddr, sizeof (UINT4));
    }
    else if (*pi4TcpConnectionLocalAddressType == TCP_IPV6_ADDR_TYPE)
    {

        IpvxAddrCopy (&TcpCurLocalAddr, &GetTCBlip (u4FirstTcbIndex));
        IpvxAddrCopy (&TcpCurRemoteAddr, &GetTCBrip (u4FirstTcbIndex));

        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TcpCurLocalAddr.u4_addr[i4Count] =
                OSIX_HTONL (TcpCurLocalAddr.u4_addr[i4Count]);
            TcpCurRemoteAddr.u4_addr[i4Count] =
                OSIX_HTONL (TcpCurRemoteAddr.u4_addr[i4Count]);
        }

        IpvxAddrCopy ((tIpAddr *) (VOID *)
                      pTcpConnectionLocalAddress->pu1_OctetList,
                      &TcpCurLocalAddr);
        IpvxAddrCopy ((tIpAddr *) (VOID *)
                      pTcpConnectionRemAddress->pu1_OctetList,
                      &TcpCurRemoteAddr);
    }
    FillLengthOfIp (GetTCBLipAddType (u4FirstTcbIndex),
                    pTcpConnectionLocalAddress);
    *pu4TcpConnectionLocalPort = GetTCBlport (u4FirstTcbIndex);
    *pi4TcpConnectionRemAddressType = GetTCBRipAddType (u4FirstTcbIndex);
    FillLengthOfIp (GetTCBRipAddType (u4FirstTcbIndex),
                    pTcpConnectionRemAddress);
    *pu4TcpConnectionRemPort = GetTCBrport (u4FirstTcbIndex);
    return SNMP_SUCCESS;

#endif
#endif

#ifdef LNXIP4_WANTED
    return (nmhGetNextIndexTcpConnectionTable
            (LNX_ZERO, pi4TcpConnectionLocalAddressType,
             LNX_ZERO, pTcpConnectionLocalAddress,
             LNX_ZERO, pu4TcpConnectionLocalPort,
             LNX_ZERO, pi4TcpConnectionRemAddressType,
             LNX_ZERO, pTcpConnectionRemAddress,
             LNX_ZERO, pu4TcpConnectionRemPort));
#endif
}

/****************************************************************************
Function    :  nmhGetNextIndexTcpConnectionTable
Input       :  The Indices
TcpConnectionLocalAddressType
nextTcpConnectionLocalAddressType
TcpConnectionLocalAddress
nextTcpConnectionLocalAddress
TcpConnectionLocalPort
nextTcpConnectionLocalPort
TcpConnectionRemAddressType
nextTcpConnectionRemAddressType
TcpConnectionRemAddress
nextTcpConnectionRemAddress
TcpConnectionRemPort
nextTcpConnectionRemPort
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexTcpConnectionTable
    (INT4 i4TcpConnectionLocalAddressType,
     INT4 *pi4NextTcpConnectionLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextTcpConnectionLocalAddress,
     UINT4 u4TcpConnectionLocalPort,
     UINT4 *pu4NextTcpConnectionLocalPort,
     INT4 i4TcpConnectionRemAddressType,
     INT4 *pi4NextTcpConnectionRemAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpConnectionRemAddress,
     tSNMP_OCTET_STRING_TYPE * pNextTcpConnectionRemAddress,
     UINT4 u4TcpConnectionRemPort, UINT4 *pu4NextTcpConnectionRemPort)
{

#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    INT4                i4NextLocalAddrType = TCP_ZERO;
    INT4                i4NextRemoteAddrType = TCP_ZERO;
    INT4                i4TcpCurLocalAddrType = TCP_ZERO;
    INT4                i4TcpCurRemoteAddrType = TCP_ZERO;
    INT4                i4Found = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4NextTcpConnLocalPort = TCP_ZERO;
    UINT4               u4NextTcpConnRemPort = TCP_ZERO;
    UINT4               u4TcpCurRemotePort = TCP_ZERO;
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4TcpCurLocalPort = TCP_ZERO;
    tIpAddr             LocalIP;
    tIpAddr             RemoteIP;
    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;
    tIpAddr             TcpCurLocalAddr;
    tIpAddr             TcpCurRemoteAddr;
    tIpAddr             NextTcpConnLocalAddress;
    tIpAddr             NextTcpConnRemAddress;

    UINT4               u4TempLocalTcpAddr;
    UINT4               u4TempRemTcpAddr;

    UINT4               u4TcbContext;

    MEMSET (&LocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&RemoteIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempLocalIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TempRemoteIP, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
    MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));

    if ((pTcpConnectionLocalAddress->i4_Length <= 0) ||
        (pTcpConnectionRemAddress->i4_Length <= 0))
    {
        return SNMP_FAILURE;
    }

    if (i4TcpConnectionLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_IPV6_ADDR_TYPE)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }
    }

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {

        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        i4TcpCurLocalAddrType = GetTCBLipAddType (u4TcbIndex);
        i4TcpCurRemoteAddrType = GetTCBRipAddType (u4TcbIndex);
        u4TcbContext = GetTCBContext (u4TcbIndex);

        if (u4TcbContext != gpTcpCurrContext->u4ContextId)
        {
            continue;
        }

        if (i4TcpCurLocalAddrType == TCP_IPV4_ADDR_TYPE)
        {
            u4TempLocalTcpAddr = GetTCBv4lip (u4TcbIndex);
            MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurLocalAddr, &u4TempLocalTcpAddr, sizeof (UINT4));
            u4TempRemTcpAddr = GetTCBv4rip (u4TcbIndex);
            MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurRemoteAddr, &u4TempRemTcpAddr, sizeof (UINT4));
        }
        else if (i4TcpCurLocalAddrType == TCP_IPV6_ADDR_TYPE)
        {
            MEMSET (&TcpCurLocalAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurLocalAddr, &GetTCBlip (u4TcbIndex),
                    sizeof (tIpAddr));
            MEMSET (&TcpCurRemoteAddr, TCP_ZERO, sizeof (tIpAddr));
            MEMCPY (&TcpCurRemoteAddr, &GetTCBrip (u4TcbIndex),
                    sizeof (tIpAddr));
        }
        u4TcpCurLocalPort = GetTCBlport (u4TcbIndex);
        u4TcpCurRemotePort = GetTCBrport (u4TcbIndex);

        /*As same application of same address type cannot 
         * use the same port,skip the entry when such entry is found*/
        /*As the Next port is always greater than the current one 
         *skip the entry when the address type of current entry
         *is less than are equal to next entry*/

        if ((u4TcpCurLocalPort <= u4TcpConnectionLocalPort) &&
            (i4TcpCurLocalAddrType <= i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) <= TCP_ZERO) &&
            (u4TcpCurRemotePort <= u4TcpConnectionRemPort) &&
            (IpvxAddrCmp (&TcpCurRemoteAddr, &TempRemoteIP) <= TCP_ZERO))
        {
            continue;
        }
        /*Check whether this entry's indexes are less than the requested
         * indexes. If so, skip this entry*/
        if (i4TcpCurLocalAddrType < i4TcpConnectionLocalAddressType)
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) < TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) == TCP_ZERO) &&
            (u4TcpCurLocalPort < u4TcpConnectionLocalPort))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4TcpConnectionLocalPort) &&
            (i4TcpCurRemoteAddrType < i4TcpConnectionRemAddressType))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4TcpConnectionLocalPort) &&
            (i4TcpCurRemoteAddrType == i4TcpConnectionRemAddressType) &&
            (IpvxAddrCmp (&TcpCurRemoteAddr, &TempRemoteIP) < TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4TcpConnectionLocalAddressType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr, &TempLocalIP) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4TcpConnectionLocalPort) &&
            (i4TcpCurRemoteAddrType == i4TcpConnectionRemAddressType) &&
            (IpvxAddrCmp (&TcpCurRemoteAddr, &TempRemoteIP) == TCP_ZERO) &&
            (u4TcpCurRemotePort <= u4TcpConnectionRemPort))
        {
            /*Last condition in if clause is <=, to avoid returning 
             * the incoming index!*/
            continue;
        }

        /* Now it is sure that current index is greater than the requested 
         * Index. If we didn't identify a getnext so far, we can initialise
         * the getnext value with current index.*/

        if (i4Found == 0)
        {
            i4NextLocalAddrType = i4TcpCurLocalAddrType;
            NextTcpConnLocalAddress = TcpCurLocalAddr;
            u4NextTcpConnLocalPort = u4TcpCurLocalPort;
            i4NextRemoteAddrType = i4TcpCurRemoteAddrType;
            NextTcpConnRemAddress = TcpCurRemoteAddr;
            u4NextTcpConnRemPort = u4TcpCurRemotePort;
            i4Found = 1;
            continue;
        }

        /*Check whether this entry is greater than the currently 
         * identified getnext index. If so, skip this entry*/
        if (i4TcpCurLocalAddrType > i4NextLocalAddrType)
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr,
                          &NextTcpConnLocalAddress) > TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr,
                          &NextTcpConnLocalAddress) == TCP_ZERO) &&
            (u4TcpCurLocalPort > u4NextTcpConnLocalPort))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr,
                          &NextTcpConnLocalAddress) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType > i4NextRemoteAddrType))
        {
            continue;
        }

        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr,
                          &NextTcpConnLocalAddress) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType == i4NextRemoteAddrType) &&
            (IpvxAddrCmp (&TcpCurRemoteAddr,
                          &NextTcpConnRemAddress) > TCP_ZERO))
        {
            continue;
        }
        if ((i4TcpCurLocalAddrType == i4NextLocalAddrType) &&
            (IpvxAddrCmp (&TcpCurLocalAddr,
                          &NextTcpConnLocalAddress) == TCP_ZERO) &&
            (u4TcpCurLocalPort == u4NextTcpConnLocalPort) &&
            (i4TcpCurRemoteAddrType == i4NextRemoteAddrType) &&
            (IpvxAddrCmp (&TcpCurRemoteAddr,
                          &NextTcpConnRemAddress) == TCP_ZERO) &&
            (u4TcpCurRemotePort > u4NextTcpConnRemPort))
        {
            continue;
        }
        /*Now it is sure that Current index is not greater than the
         * present get next value. So we need to take the new value 
         * for get next :-)*/
        i4NextLocalAddrType = i4TcpCurLocalAddrType;
        NextTcpConnLocalAddress = TcpCurLocalAddr;
        u4NextTcpConnLocalPort = u4TcpCurLocalPort;
        i4NextRemoteAddrType = i4TcpCurRemoteAddrType;
        NextTcpConnRemAddress = TcpCurRemoteAddr;
        u4NextTcpConnRemPort = u4TcpCurRemotePort;
    }
    if (i4Found == 0)
    {
        TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                     "Getnext of index returned failure.\n");
        return SNMP_FAILURE;
    }

    *pi4NextTcpConnectionLocalAddressType = i4NextLocalAddrType;
    if (i4NextLocalAddrType == TCP_IPV4_ADDR_TYPE)
    {
        MEMCPY (&u4TempLocalTcpAddr, &NextTcpConnLocalAddress,
                IPVX_IPV4_ADDR_LEN);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (&NextTcpConnLocalAddress, &u4TempLocalTcpAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4TempRemTcpAddr, &NextTcpConnRemAddress, IPVX_IPV4_ADDR_LEN);
        u4TempRemTcpAddr = OSIX_HTONL (u4TempRemTcpAddr);
        MEMCPY (&NextTcpConnRemAddress, &u4TempRemTcpAddr, IPVX_IPV4_ADDR_LEN);

    }
    else if (i4NextLocalAddrType == TCP_IPV6_ADDR_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            NextTcpConnLocalAddress.u4_addr[i4Count] =
                OSIX_HTONL (NextTcpConnLocalAddress.u4_addr[i4Count]);
            NextTcpConnRemAddress.u4_addr[i4Count] =
                OSIX_HTONL (NextTcpConnRemAddress.u4_addr[i4Count]);
        }
    }
    IpvxAddrCopy ((tIpAddr *) (VOID *)
                  pNextTcpConnectionLocalAddress->pu1_OctetList,
                  &NextTcpConnLocalAddress);
    FillLengthOfIp (i4NextLocalAddrType, pNextTcpConnectionLocalAddress);
    *pu4NextTcpConnectionLocalPort = u4NextTcpConnLocalPort;
    *pi4NextTcpConnectionRemAddressType = i4NextRemoteAddrType;

    IpvxAddrCopy ((tIpAddr *) (VOID *)
                  pNextTcpConnectionRemAddress->pu1_OctetList,
                  &NextTcpConnRemAddress);
    FillLengthOfIp (i4NextRemoteAddrType, pNextTcpConnectionRemAddress);
    *pu4NextTcpConnectionRemPort = u4NextTcpConnRemPort;

    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "GetNext in TCP ConnTable returned\n");

    return SNMP_SUCCESS;

#endif
#endif

#ifdef LNXIP4_WANTED
    tTcpConnEntry       tcpConnEntry;
    tTcpConnEntry       nextTcpConnEntry;

    MEMSET (&tcpConnEntry, 0, sizeof (tTcpConnEntry));
    tcpConnEntry.i4LocalAddrType = i4TcpConnectionLocalAddressType;
    if (i4TcpConnectionLocalAddressType == LNX_ZERO)
    {
        tcpConnEntry.u4LocalIp = LNX_ZERO;
    }
    else
    {
        pTcpConnectionLocalAddress->i4_Length = LNX_IPV4_LEN;
        TCP_OCTETSTRING_TO_INTEGER (pTcpConnectionLocalAddress,
                                    tcpConnEntry.u4LocalIp);
    }
    tcpConnEntry.u2LocalPort = (UINT2) u4TcpConnectionLocalPort;
    tcpConnEntry.i4RemoteAddrType = i4TcpConnectionRemAddressType;
    if (i4TcpConnectionRemAddressType == LNX_ZERO)
    {
        tcpConnEntry.u4RemoteIp = LNX_ZERO;
    }
    else
    {
        pTcpConnectionRemAddress->i4_Length = LNX_IPV4_LEN;
        TCP_OCTETSTRING_TO_INTEGER (pTcpConnectionRemAddress,
                                    tcpConnEntry.u4RemoteIp);
    }
    tcpConnEntry.u2RemotePort = (UINT2) u4TcpConnectionRemPort;
    if (TcpConnTableGetNextEntry (&tcpConnEntry, &nextTcpConnEntry) ==
        SNMP_SUCCESS)
    {
        *pi4NextTcpConnectionLocalAddressType =
            nextTcpConnEntry.i4LocalAddrType;
        TCP_INTEGER_TO_OCTETSTRING (nextTcpConnEntry.u4LocalIp,
                                    pNextTcpConnectionLocalAddress);

        *pu4NextTcpConnectionLocalPort = nextTcpConnEntry.u2LocalPort;
        *pi4NextTcpConnectionRemAddressType = nextTcpConnEntry.i4RemoteAddrType;
        TCP_INTEGER_TO_OCTETSTRING (nextTcpConnEntry.u4RemoteIp,
                                    pNextTcpConnectionRemAddress);
        *pu4NextTcpConnectionRemPort = nextTcpConnEntry.u2RemotePort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetTcpConnectionState
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort

The Object 
retValTcpConnectionState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpConnectionState (INT4 i4TcpConnectionLocalAddressType,
                          tSNMP_OCTET_STRING_TYPE
                          * pTcpConnectionLocalAddress,
                          UINT4 u4TcpConnectionLocalPort,
                          INT4 i4TcpConnectionRemAddressType,
                          tSNMP_OCTET_STRING_TYPE
                          * pTcpConnectionRemAddress,
                          UINT4 u4TcpConnectionRemPort,
                          INT4 *pi4RetValTcpConnectionState)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    INT4                i4TcbIndex = TCP_ZERO;
    INT4                i4Count = TCP_ZERO;
    UINT4               u4TempLocalTcpAddr = TCP_ZERO;
    UINT4               u4TempRemTcpAddr = TCP_ZERO;
    UINT4               u4Context;

    tIpAddr             TempLocalIP;
    tIpAddr             TempRemoteIP;
#endif
#endif

#ifdef LNXIP4_WANTED
    FILE               *pFd = NULL;
    INT1                au1TcpConnEntry[LNX_MAX_ENTRY_SIZE];
    INT1                FirstLn = FALSE;
    INT1               *token = NULL;

    UINT1               au1Index[LNX_MAX_INDEX_SIZE];
    UINT1               au1Local[LNX_MAX_IP_PORT_SIZE];
    UINT1               au1Remote[LNX_MAX_IP_PORT_SIZE];
    UINT4               u4TcpConnLocalAddress = LNX_ZERO;
    UINT4               u4TcpConnRemAddress = LNX_ZERO;
    tTcpConnEntry       tempTcpConnEntry;

    MEMSET (&tempTcpConnEntry, LNX_ZERO, sizeof (tTcpConnEntry));
#endif
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)

    if (pTcpConnectionLocalAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempLocalIP,
                (tIpAddr *) (VOID *) pTcpConnectionLocalAddress->pu1_OctetList,
                pTcpConnectionLocalAddress->i4_Length);
    }
    if (pTcpConnectionRemAddress->i4_Length != TCP_ZERO)
    {
        MEMCPY (&TempRemoteIP,
                (tIpAddr *) (VOID *) pTcpConnectionRemAddress->pu1_OctetList,
                pTcpConnectionRemAddress->i4_Length);
    }
    if (i4TcpConnectionLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        TempLocalIP.u4_addr[0] = OSIX_NTOHL (TempLocalIP.u4_addr[0]);
        TempRemoteIP.u4_addr[0] = OSIX_NTOHL (TempRemoteIP.u4_addr[0]);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_IPV6_ADDR_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_NTOHL (TempRemoteIP.u4_addr[i4Count]);
        }

    }

    MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
            pTcpConnectionRemAddress->i4_Length);
    MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
            pTcpConnectionLocalAddress->i4_Length);

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnTableID (u4Context,
                                    i4TcpConnectionLocalAddressType,
                                    pTcpConnectionLocalAddress,
                                    u4TcpConnectionLocalPort,
                                    pTcpConnectionRemAddress,
                                    u4TcpConnectionRemPort);

    if (i4TcpConnectionLocalAddressType == TCP_IPV4_ADDR_TYPE)
    {
        MEMCPY (&u4TempLocalTcpAddr, pTcpConnectionLocalAddress->pu1_OctetList,
                IPVX_IPV4_ADDR_LEN);
        u4TempLocalTcpAddr = OSIX_HTONL (u4TempLocalTcpAddr);
        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &u4TempLocalTcpAddr,
                pTcpConnectionLocalAddress->i4_Length);

        MEMCPY (&u4TempRemTcpAddr, pTcpConnectionRemAddress->pu1_OctetList,
                IPVX_IPV4_ADDR_LEN);
        u4TempRemTcpAddr = OSIX_HTONL (u4TempRemTcpAddr);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &u4TempRemTcpAddr,
                pTcpConnectionRemAddress->i4_Length);
    }
    else if (i4TcpConnectionLocalAddressType == TCP_IPV6_ADDR_TYPE)
    {
        for (i4Count = 0; i4Count <= 3; i4Count++)
        {
            TempLocalIP.u4_addr[i4Count] =
                OSIX_HTONL (TempLocalIP.u4_addr[i4Count]);
            TempRemoteIP.u4_addr[i4Count] =
                OSIX_HTONL (TempRemoteIP.u4_addr[i4Count]);
        }

        MEMCPY (pTcpConnectionLocalAddress->pu1_OctetList, &TempLocalIP,
                pTcpConnectionLocalAddress->i4_Length);
        MEMCPY (pTcpConnectionRemAddress->pu1_OctetList, &TempRemoteIP,
                pTcpConnectionRemAddress->i4_Length);
    }

    if (i4TcbIndex == TCP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValTcpConnectionState = GetTCBstate (i4TcbIndex);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Get of tcpConnState returned %d.\n",
                 *pi4RetValTcpConnectionState);

    return SNMP_SUCCESS;
#endif
#endif
#ifdef LNXIP4_WANTED

    TCP_OCTETSTRING_TO_INTEGER (pTcpConnectionLocalAddress,
                                u4TcpConnLocalAddress);
    TCP_OCTETSTRING_TO_INTEGER (pTcpConnectionRemAddress, u4TcpConnRemAddress);

    if ((pFd = FOPEN ("/proc/net/tcp", "r")) == NULL)
    {
        perror ("Cannot open file /proc/net/tcp ...\n");
        return 1;
    }

    /* Skipping the first line */
    FirstLn = TRUE;
    while ((fgets ((CHR1 *) au1TcpConnEntry, LNX_MAX_ENTRY_SIZE, pFd)) != NULL)
    {
        if (FirstLn == TRUE)
        {
            FirstLn = FALSE;
            continue;
        }

        /* Extracting LocalIp LocalPort RemoteIp RemotePort and TcpState
         * from the file "/proc/net/tcp" */

        sscanf ((CHR1 *) au1TcpConnEntry, "%s%s%s%d", au1Index, au1Local,
                au1Remote, &tempTcpConnEntry.i4TcpState);
        token = (INT1 *) STRTOK (au1Local, (VOID *) ":");
        tempTcpConnEntry.u4LocalIp = OSIX_NTOHL ((UINT4) strtoll ((CHR1 *) token, NULL,
                                                          LNX_HEX_BASE));
        token = (INT1 *) STRTOK (NULL, ":");
        if (token != NULL)
        {
            tempTcpConnEntry.u2LocalPort
                = (UINT2) strtol ((CHR1 *) token, (CHR1 **) NULL, LNX_HEX_BASE);
        }
        else
        {
            tempTcpConnEntry.u2LocalPort = LNX_ZERO;
        }

        token = (INT1 *) STRTOK (au1Remote, ":");
        tempTcpConnEntry.u4RemoteIp = OSIX_NTOHL ((UINT4) strtoll ((CHR1 *) token, NULL,
                                                           LNX_HEX_BASE));
        token = (INT1 *) STRTOK (NULL, ":");
        if (token != NULL)
        {
            tempTcpConnEntry.u2RemotePort
                = (UINT2) strtol ((CHR1 *) token, NULL, LNX_HEX_BASE);
        }
        else
        {
            tempTcpConnEntry.u2RemotePort = LNX_ZERO;
        }
        tempTcpConnEntry.i4LocalAddrType = IPV4_ADD_TYPE;
        tempTcpConnEntry.i4RemoteAddrType = IPV4_ADD_TYPE;

        if ((tempTcpConnEntry.i4LocalAddrType ==
             i4TcpConnectionLocalAddressType) &&
            (tempTcpConnEntry.u4LocalIp == u4TcpConnLocalAddress) &&
            (tempTcpConnEntry.u2LocalPort == u4TcpConnectionLocalPort) &&
            (tempTcpConnEntry.i4RemoteAddrType ==
             i4TcpConnectionRemAddressType) &&
            (tempTcpConnEntry.u4RemoteIp == u4TcpConnRemAddress) &&
            (tempTcpConnEntry.u2RemotePort == u4TcpConnectionRemPort))
        {
            *pi4RetValTcpConnectionState = tempTcpConnEntry.i4TcpState;
            fclose (pFd);
            return SNMP_SUCCESS;
        }
    }

    fclose (pFd);
    return SNMP_FAILURE;

#endif
}

/****************************************************************************
Function    :  nmhGetTcpConnectionProcess
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort

The Object 
retValTcpConnectionProcess
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpConnectionProcess (INT4 i4TcpConnectionLocalAddressType,
                            tSNMP_OCTET_STRING_TYPE
                            * pTcpConnectionLocalAddress,
                            UINT4 u4TcpConnectionLocalPort,
                            INT4 i4TcpConnectionRemAddressType,
                            tSNMP_OCTET_STRING_TYPE
                            * pTcpConnectionRemAddress,
                            UINT4 u4TcpConnectionRemPort,
                            UINT4 *pu4RetValTcpConnectionProcess)
{
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    UNUSED_PARAM (pu4RetValTcpConnectionProcess);
    return SNMP_SUCCESS;
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    UNUSED_PARAM (pu4RetValTcpConnectionProcess);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetTcpConnectionState
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort

The Object 
setValTcpConnectionState
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetTcpConnectionState (INT4 i4TcpConnectionLocalAddressType,
                          tSNMP_OCTET_STRING_TYPE
                          * pTcpConnectionLocalAddress,
                          UINT4 u4TcpConnectionLocalPort,
                          INT4 i4TcpConnectionRemAddressType,
                          tSNMP_OCTET_STRING_TYPE
                          * pTcpConnectionRemAddress,
                          UINT4 u4TcpConnectionRemPort,
                          INT4 i4SetValTcpConnectionState)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    INT4                i4TcbIndex = TCP_ZERO;
    UINT4               u4Context;

    UNUSED_PARAM (i4SetValTcpConnectionState);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);

    u4Context = gpTcpCurrContext->u4ContextId;

    i4TcbIndex = tcpGetConnID (u4Context,
                               i4TcpConnectionLocalAddressType,
                               u4TcpConnectionLocalPort,
                               i4TcpConnectionRemAddressType);

    if (i4TcbIndex == TCP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((UINT4) i4TcbIndex > MAX_NUM_OF_TCB)
    {
        TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                     "In set tcpConnState, out of bounds tcb index\n");
        return SNMP_FAILURE;
    }
    OsmResetConn (i4TcbIndex);    /* Send RESET to the remote guy */
    /* Indicate connection aborted */
    FsmAbortConn (&TCBtable[i4TcbIndex], TCPE_MGMTRESET);
    TCP_MOD_TRC (u4Context, TCP_MGMT_TRC, "TCP",
                 "In set tcpConnState, connection reset\n");
    return SNMP_SUCCESS;
#endif
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    UNUSED_PARAM (i4SetValTcpConnectionState);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2TcpConnectionState
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort

The Object 
testValTcpConnectionState
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2TcpConnectionState (UINT4 *pu4ErrorCode,
                             INT4 i4TcpConnectionLocalAddressType,
                             tSNMP_OCTET_STRING_TYPE
                             * pTcpConnectionLocalAddress,
                             UINT4 u4TcpConnectionLocalPort,
                             INT4 i4TcpConnectionRemAddressType,
                             tSNMP_OCTET_STRING_TYPE
                             * pTcpConnectionRemAddress,
                             UINT4 u4TcpConnectionRemPort,
                             INT4 i4TestValTcpConnectionState)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    TCP_MOD_TRC (gpTcpCurrContext->u4ContextId, TCP_MGMT_TRC, "TCP",
                 "Connection state tested for value %d\n",
                 i4TestValTcpConnectionState);
    if (i4TestValTcpConnectionState == TCPS_ABORT)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4TcpConnectionLocalAddressType);
    UNUSED_PARAM (pTcpConnectionLocalAddress);
    UNUSED_PARAM (u4TcpConnectionLocalPort);
    UNUSED_PARAM (i4TcpConnectionRemAddressType);
    UNUSED_PARAM (pTcpConnectionRemAddress);
    UNUSED_PARAM (u4TcpConnectionRemPort);
    if (i4TestValTcpConnectionState == LNX_DELETE_TCB)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2TcpConnectionTable
Input       :  The Indices
TcpConnectionLocalAddressType
TcpConnectionLocalAddress
TcpConnectionLocalPort
TcpConnectionRemAddressType
TcpConnectionRemAddress
TcpConnectionRemPort
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2TcpConnectionTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TcpListenerTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceTcpListenerTable
Input       :  The Indices
TcpListenerLocalAddressType
TcpListenerLocalAddress
TcpListenerLocalPort
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceTcpListenerTable
    (INT4 i4TcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     UINT4 u4TcpListenerLocalPort)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UINT4               u4LocalAddress = TCP_ZERO;
    UINT4               u4TcbIndex = TCP_ZERO;
    UINT4               u4Context;

    u4Context = gpTcpCurrContext->u4ContextId;

    TCP_OCTETSTRING_TO_INTEGER (pTcpListenerLocalAddress, u4LocalAddress);
    if (i4TcpListenerLocalAddressType == TCP_ZERO)
    {
        i4TcpListenerLocalAddressType = TCP_IPV4_ADDR_TYPE;
    }

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_LISTEN)
        {
            if ((u4Context != GetTCBContext (u4TcbIndex)) ||
                (i4TcpListenerLocalAddressType != GetTCBLipAddType (u4TcbIndex))
                || (u4LocalAddress != GetTCBv4lip (u4TcbIndex))
                || (u4TcpListenerLocalPort != GetTCBlport (u4TcbIndex)))
            {
                continue;
            }
            return SNMP_SUCCESS;
        }
        else
        {
            continue;
        }
    }
    return SNMP_FAILURE;
#endif
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4TcpListenerLocalAddressType);
    UNUSED_PARAM (pTcpListenerLocalAddress);
    UNUSED_PARAM (u4TcpListenerLocalPort);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
Function    :  nmhGetFirstIndexTcpListenerTable
Input       :  The Indices
TcpListenerLocalAddressType
TcpListenerLocalAddress
TcpListenerLocalPort
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexTcpListenerTable
    (INT4 *pi4TcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     UINT4 *pu4TcpListenerLocalPort)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    INT4                i4TcpCurrConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnLocalAddressType = TCP_ZERO;
    INT4                i4TcpNextConnRemAddressType = TCP_ZERO;
    INT4                i4TcpCurrConnRemAddressType = TCP_ZERO;
    INT4                i4ConnState = TCP_ZERO;
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TcpCurrConnLocalPort = TCP_ZERO;
    UINT4               u4TcpCurrConnRemPort = TCP_ZERO;
    UINT4               u4TcpNextConnLocalPort = TCP_ZERO;
    UINT4               u4TcpNextConnRemPort = TCP_ZERO;
    UINT1               u1IsPresent = TCP_TRUE;
    UINT1               u1IsType1Present = TCP_FALSE;
    tSNMP_OCTET_STRING_TYPE TcpNextConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnLocalAddress;
    tSNMP_OCTET_STRING_TYPE TcpCurrConnRemAddress;
    tSNMP_OCTET_STRING_TYPE TcpNextConnRemAddress;

    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    TcpCurrConnLocalAddress.pu1_OctetList = au1CurrLocalIp;
    TcpCurrConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpCurrConnRemAddress.pu1_OctetList = au1CurrRemoteIp;
    TcpCurrConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnLocalAddress.pu1_OctetList = au1NextLocalIp;
    TcpNextConnLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    TcpNextConnRemAddress.pu1_OctetList = au1NextRemoteIp;
    TcpNextConnRemAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    /*Get the first entry from the conn table */
    if (nmhGetFirstIndexTcpConnectionTable
        (&i4TcpNextConnLocalAddressType,
         &TcpNextConnLocalAddress,
         &u4TcpNextConnLocalPort,
         &i4TcpNextConnRemAddressType,
         &TcpNextConnRemAddress, &u4TcpNextConnRemPort) == SNMP_SUCCESS)
    {
        nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                  &TcpNextConnLocalAddress,
                                  u4TcpNextConnLocalPort,
                                  i4TcpNextConnRemAddressType,
                                  &TcpNextConnRemAddress,
                                  u4TcpNextConnRemPort, &i4ConnState);
        /*If the Conn state is Listen and the address type is IPv4
         * check for a Corresponding IPv6 entry having 
         * same Local Ip (=zero) and Local Port,
         * if IPv6 Conn exists set Local add type as zero and return 
         * the entry, else continue
         * If the Address type of the Conn is IPv6 return the entry as 
         * there are no IPv4 entries in the TCB block*/
        if (i4ConnState == TCPS_LISTEN)
        {
            if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
            {
                *pi4TcpListenerLocalAddressType = i4TcpNextConnLocalAddressType;
                MEMCPY (pTcpListenerLocalAddress->pu1_OctetList,
                        TcpNextConnLocalAddress.pu1_OctetList,
                        TcpNextConnLocalAddress.i4_Length);
                FillLengthOfIp (TCP_ZERO, pTcpListenerLocalAddress);
                *pu4TcpListenerLocalPort = u4TcpNextConnLocalPort;
                /* If Type 0 entry is never found, return this Type 1 entry finally */
                u1IsType1Present = TCP_TRUE;
                if (CheckForIpv6Entry (&u4TcpNextConnLocalPort) == SNMP_SUCCESS)
                {
                    *pi4TcpListenerLocalAddressType = TCP_ZERO;
                    return SNMP_SUCCESS;
                }
            }
            else if (i4TcpNextConnLocalAddressType == TCP_IPV6_ADDR_TYPE)
            {
                *pi4TcpListenerLocalAddressType = i4TcpNextConnLocalAddressType;
                MEMCPY (pTcpListenerLocalAddress->pu1_OctetList,
                        TcpNextConnLocalAddress.pu1_OctetList,
                        TcpNextConnLocalAddress.i4_Length);
                FillLengthOfIp (i4TcpNextConnLocalAddressType,
                                pTcpListenerLocalAddress);
                *pu4TcpListenerLocalPort = u4TcpNextConnLocalPort;

                return SNMP_SUCCESS;
            }

        }
    }
    else                        /*  No connections present in table */
    {
        return SNMP_FAILURE;
    }
    do
    {
        i4TcpCurrConnLocalAddressType = i4TcpNextConnLocalAddressType;
        i4TcpCurrConnRemAddressType = i4TcpNextConnRemAddressType;
        u4TcpCurrConnLocalPort = u4TcpNextConnLocalPort;
        u4TcpCurrConnRemPort = u4TcpNextConnRemPort;
        MEMCPY (TcpCurrConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.pu1_OctetList,
                TcpNextConnLocalAddress.i4_Length);
        MEMCPY (TcpCurrConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.pu1_OctetList,
                TcpNextConnRemAddress.i4_Length);

        if (nmhGetNextIndexTcpConnectionTable
            (i4TcpCurrConnLocalAddressType,
             &i4TcpNextConnLocalAddressType,
             &TcpCurrConnLocalAddress, &TcpNextConnLocalAddress,
             u4TcpCurrConnLocalPort, &u4TcpNextConnLocalPort,
             i4TcpCurrConnRemAddressType, &i4TcpNextConnRemAddressType,
             &TcpCurrConnRemAddress, &TcpNextConnRemAddress,
             u4TcpCurrConnRemPort, &u4TcpNextConnRemPort) == SNMP_SUCCESS)
        {
            nmhGetTcpConnectionState (i4TcpNextConnLocalAddressType,
                                      &TcpNextConnLocalAddress,
                                      u4TcpNextConnLocalPort,
                                      i4TcpNextConnRemAddressType,
                                      &TcpNextConnRemAddress,
                                      u4TcpNextConnRemPort, &i4ConnState);
            if (i4ConnState == TCPS_LISTEN)
            {
                if (i4TcpNextConnLocalAddressType == TCP_IPV4_ADDR_TYPE)
                {
                    if (CheckForIpv6Entry (&u4TcpNextConnLocalPort)
                        == SNMP_SUCCESS)
                    {
                        *pi4TcpListenerLocalAddressType = TCP_ZERO;
                        MEMCPY (pTcpListenerLocalAddress->pu1_OctetList,
                                TcpNextConnLocalAddress.pu1_OctetList,
                                TcpNextConnLocalAddress.i4_Length);
                        FillLengthOfIp (TCP_ZERO, pTcpListenerLocalAddress);
                        *pu4TcpListenerLocalPort = u4TcpNextConnLocalPort;

                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            else
            {
                continue;
            }

        }
        /* Get next failed. No more entries availble */
        else
        {
            u1IsPresent = TCP_ZERO;
        }
    }
    while (u1IsPresent);

    if (TCP_TRUE == u1IsType1Present)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#endif
#endif
#ifdef LNXIP4_WANTED
    return (nmhGetNextIndexTcpListenerTable
            (0, pi4TcpListenerLocalAddressType,
             0, pTcpListenerLocalAddress, 0, pu4TcpListenerLocalPort));
#endif
}

/****************************************************************************
Function    :  nmhGetNextIndexTcpListenerTable
Input       :  The Indices
TcpListenerLocalAddressType
nextTcpListenerLocalAddressType
TcpListenerLocalAddress
nextTcpListenerLocalAddress
TcpListenerLocalPort
nextTcpListenerLocalPort
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexTcpListenerTable
    (INT4 i4TcpListenerLocalAddressType,
     INT4 *pi4NextTcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextTcpListenerLocalAddress,
     UINT4 u4TcpListenerLocalPort, UINT4 *pu4NextTcpListenerLocalPort)
{
#ifndef LNXIP4_WANTED
#if defined (IP_WANTED) || defined (IP6_WANTED)
    UINT1               au1CurrLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4Context;

    UNUSED_PARAM (pTcpListenerLocalAddress);

    u4Context = gpTcpCurrContext->u4ContextId;

    MEMSET (au1CurrLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CurrRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, TCP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    /*If the address type is zero check for the next entry 
     * having adress type as zero,
     * if fails check for the address type one and two entries*/
    if (i4TcpListenerLocalAddressType == TCP_ZERO &&
        u4TcpListenerLocalPort != TCP_ZERO)
    {
        if ((CheckForAddrTypeZeroEntry
             (u4Context,
              i4TcpListenerLocalAddressType,
              pi4NextTcpListenerLocalAddressType,
              pTcpListenerLocalAddress,
              pNextTcpListenerLocalAddress,
              u4TcpListenerLocalPort,
              pu4NextTcpListenerLocalPort)) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if ((CheckForAddrTypeOneAndTwoEntry
                 (u4Context,
                  i4TcpListenerLocalAddressType,
                  pi4NextTcpListenerLocalAddressType,
                  pTcpListenerLocalAddress,
                  pNextTcpListenerLocalAddress,
                  u4TcpListenerLocalPort,
                  pu4NextTcpListenerLocalPort)) == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

        }

    }
    else
    {
        if ((CheckForAddrTypeOneAndTwoEntry
             (u4Context,
              i4TcpListenerLocalAddressType,
              pi4NextTcpListenerLocalAddressType,
              pTcpListenerLocalAddress,
              pNextTcpListenerLocalAddress,
              u4TcpListenerLocalPort,
              pu4NextTcpListenerLocalPort)) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

    }
    return SNMP_FAILURE;
#endif
#endif

#ifdef LNXIP4_WANTED
    tTcpConnEntry       tcpConnEntry;
    tTcpConnEntry       nextTcpConnEntry;

    MEMSET (&tcpConnEntry, 0, sizeof (tTcpConnEntry));
    tcpConnEntry.i4LocalAddrType = i4TcpListenerLocalAddressType;
    if (i4TcpListenerLocalAddressType == LNX_ZERO)
    {
        tcpConnEntry.u4LocalIp = LNX_ZERO;
    }
    else
    {
        pTcpListenerLocalAddress->i4_Length = LNX_IPV4_LEN;
        TCP_OCTETSTRING_TO_INTEGER (pTcpListenerLocalAddress,
                                    tcpConnEntry.u4LocalIp);
    }
    tcpConnEntry.u2LocalPort = (UINT2) u4TcpListenerLocalPort;

    if (TcpListTableGetNextEntry (&tcpConnEntry, &nextTcpConnEntry) ==
        SNMP_SUCCESS)
    {
        *pi4NextTcpListenerLocalAddressType = nextTcpConnEntry.i4LocalAddrType;
        TCP_INTEGER_TO_OCTETSTRING (nextTcpConnEntry.u4LocalIp,
                                    pNextTcpListenerLocalAddress);
        *pu4NextTcpListenerLocalPort = nextTcpConnEntry.u2LocalPort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetTcpListenerProcess
Input       :  The Indices
TcpListenerLocalAddressType
TcpListenerLocalAddress
TcpListenerLocalPort

The Object 
retValTcpListenerProcess
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1                nmhGetTcpListenerProcess
    (INT4 i4TcpListenerLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pTcpListenerLocalAddress,
     UINT4 u4TcpListenerLocalPort, UINT4 *pu4RetValTcpListenerProcess)
{
    UNUSED_PARAM (i4TcpListenerLocalAddressType);
    UNUSED_PARAM (pTcpListenerLocalAddress);
    UNUSED_PARAM (u4TcpListenerLocalPort);
    UNUSED_PARAM (pu4RetValTcpListenerProcess);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TcpConnTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceTcpConnTable
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTcpConnTable (UINT4 u4TcpConnLocalAddress,
                                      INT4 i4TcpConnLocalPort,
                                      UINT4 u4TcpConnRemAddress,
                                      INT4 i4TcpConnRemPort)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexTcpConnTable
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTcpConnTable (UINT4 *pu4TcpConnLocalAddress,
                              INT4 *pi4TcpConnLocalPort,
                              UINT4 *pu4TcpConnRemAddress,
                              INT4 *pi4TcpConnRemPort)
{
    UNUSED_PARAM (pu4TcpConnLocalAddress);
    UNUSED_PARAM (pi4TcpConnLocalPort);
    UNUSED_PARAM (pu4TcpConnRemAddress);
    UNUSED_PARAM (pi4TcpConnRemPort);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexTcpConnTable
Input       :  The Indices
TcpConnLocalAddress
nextTcpConnLocalAddress
TcpConnLocalPort
nextTcpConnLocalPort
TcpConnRemAddress
nextTcpConnRemAddress
TcpConnRemPort
nextTcpConnRemPort
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTcpConnTable (UINT4 u4TcpConnLocalAddress,
                             UINT4 *pu4NextTcpConnLocalAddress,
                             INT4 i4TcpConnLocalPort,
                             INT4 *pi4NextTcpConnLocalPort,
                             UINT4 u4TcpConnRemAddress,
                             UINT4 *pu4NextTcpConnRemAddress,
                             INT4 i4TcpConnRemPort, INT4 *pi4NextTcpConnRemPort)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (pu4NextTcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (pi4NextTcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (pu4NextTcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (pi4NextTcpConnRemPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetTcpConnState
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort

The Object 
retValTcpConnState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetTcpConnState (UINT4 u4TcpConnLocalAddress,
                    INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress,
                    INT4 i4TcpConnRemPort, INT4 *pi4RetValTcpConnState)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (pi4RetValTcpConnState);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetTcpConnState
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort

The Object 
setValTcpConnState
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetTcpConnState (UINT4 u4TcpConnLocalAddress,
                    INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress,
                    INT4 i4TcpConnRemPort, INT4 i4SetValTcpConnState)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (i4SetValTcpConnState);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2TcpConnState
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort

The Object 
testValTcpConnState
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2TcpConnState (UINT4 *pu4ErrorCode,
                       UINT4 u4TcpConnLocalAddress,
                       INT4 i4TcpConnLocalPort,
                       UINT4 u4TcpConnRemAddress,
                       INT4 i4TcpConnRemPort, INT4 i4TestValTcpConnState)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (i4TestValTcpConnState);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2TcpConnTable
Input       :  The Indices
TcpConnLocalAddress
TcpConnLocalPort
TcpConnRemAddress
TcpConnRemPort
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2TcpConnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif
