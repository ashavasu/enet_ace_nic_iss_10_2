/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsmstcwr.c,v 1.1 2013/01/23 11:18:43 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsmstclw.h"
# include  "fsmstcwr.h"
# include  "fsmstcdb.h"

# include "tcpinc.h"
# include "tcputils.h"

INT4 GetNextIndexFsMIStdContextTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIStdContextTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIStdContextTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterFSMSTC ()
{
	SNMPRegisterMib (&fsmstcOID, &fsmstcEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fsmstcOID, (const UINT1 *) "fsmstcpipvx");
}



VOID UnRegisterFSMSTC ()
{
	SNMPUnRegisterMib (&fsmstcOID, &fsmstcEntry);
	SNMPDelSysorEntry (&fsmstcOID, (const UINT1 *) "fsmstcpipvx");
}

INT4 FsMIStdTcpRtoAlgorithmGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpRtoAlgorithm(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIStdTcpRtoMinGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpRtoMin(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIStdTcpRtoMaxGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpRtoMax(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIStdTcpMaxConnGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpMaxConn(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIStdTcpActiveOpensGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpActiveOpens(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpPassiveOpensGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpPassiveOpens(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpAttemptFailsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpAttemptFails(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpEstabResetsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpEstabResets(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpCurrEstabGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpCurrEstab(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpInSegsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpInSegs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpOutSegsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpOutSegs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpRetransSegsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpRetransSegs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpInErrsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpInErrs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpOutRstsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpOutRsts(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpHCInSegsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpHCInSegs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 FsMIStdTcpHCOutSegsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdContextTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpHCOutSegs(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}

INT4 GetNextIndexFsMIStdTcpConnectionTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIStdTcpConnectionTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue),
			&(pNextMultiIndex->pIndex[4].i4_SLongValue),
			pNextMultiIndex->pIndex[5].pOctetStrValue,
			&(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIStdTcpConnectionTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].pOctetStrValue,
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			pFirstMultiIndex->pIndex[3].u4_ULongValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue),
			pFirstMultiIndex->pIndex[4].i4_SLongValue,
			&(pNextMultiIndex->pIndex[4].i4_SLongValue),
			pFirstMultiIndex->pIndex[5].pOctetStrValue,
			pNextMultiIndex->pIndex[5].pOctetStrValue,
			pFirstMultiIndex->pIndex[6].u4_ULongValue,
			&(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIStdTcpConnectionStateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdTcpConnectionTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpConnectionState(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIStdTcpConnectionProcessGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdTcpConnectionTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpConnectionProcess(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIStdTcpConnectionStateSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIStdTcpConnectionState(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIStdTcpConnectionStateTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIStdTcpConnectionState(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].pOctetStrValue,
		pMultiIndex->pIndex[6].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIStdTcpConnectionTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIStdTcpConnectionTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIStdTcpListenerTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIStdTcpListenerTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIStdTcpListenerTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].pOctetStrValue,
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			pFirstMultiIndex->pIndex[3].u4_ULongValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIStdTcpListenerProcessGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIStdTcpListenerTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIStdTcpListenerProcess(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
