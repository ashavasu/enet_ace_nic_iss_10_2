#ifndef _STDUDPWR_H
#define _STDUDPWR_H

VOID RegisterSTDUDP(VOID);

VOID UnRegisterSTDUDP(VOID);
INT4 UdpInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 UdpNoPortsGet(tSnmpIndex *, tRetVal *);
INT4 UdpInErrorsGet(tSnmpIndex *, tRetVal *);
INT4 UdpOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 UdpHCInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 UdpHCOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexUdpEndpointTable(tSnmpIndex *, tSnmpIndex *);
INT4 UdpEndpointProcessGet(tSnmpIndex *, tRetVal *);
#endif /* _STDUDPWR_H */
