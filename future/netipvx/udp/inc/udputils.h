/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: udputils.h,v 1.2 2009/10/13 11:07:57 prabuc Exp $
 *
 * Description: This file contains udp utility function proto types
 *
 *******************************************************************/
#ifndef __UDPCLIUTIL_H__
#define __UDPCLIUTIL_H__

#define UDP_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
            MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   sizeof(UINT4));\
                u4Index = OSIX_NTOHL(u4Index);}

#define UDP_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
        MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
               sizeof(UINT4));\
        pOctetString->i4_Length=sizeof(UINT4);}
#ifdef IP6_WANTED
INT4 UdpGetMatchingIPv6Entry (UINT2 u2Ipv4LocalPort);
INT4 UdpCheckForZeroIPv6Add (tIp6Addr  Ip6UdpAddr);
#endif
#endif

