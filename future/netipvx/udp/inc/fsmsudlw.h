/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsudlw.h,v 1.2 2009/08/24 13:12:57 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMiUdpInDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpNoPorts ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpOutDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpInNoCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpInIcmpErr ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpInErrCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpInBcast ARG_LIST((UINT4 *));

INT1
nmhGetFsMiUdpHCInDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMiUdpHCOutDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsMIUdpStatTable. */
INT1
nmhValidateIndexInstanceFsMIUdpStatTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIUdpStatTable  */

INT1
nmhGetFirstIndexFsMIUdpStatTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIUdpStatTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMiUdpIpvxInDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxNoPorts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxInErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxOutDatagrams ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxInNoCksum ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxInIcmpErr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxInErrCksum ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxInBcast ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMiUdpIpvxHCInDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMiUdpIpvxHCOutDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsMiUdpEndpointTable. */
INT1
nmhValidateIndexInstanceFsMiUdpEndpointTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMiUdpEndpointTable  */

INT1
nmhGetFirstIndexFsMiUdpEndpointTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMiUdpEndpointTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMiUdpEndpointProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMiUdpIpvxEndpointTable. */
INT1
nmhValidateIndexInstanceFsMiUdpIpvxEndpointTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMiUdpIpvxEndpointTable  */

INT1
nmhGetFirstIndexFsMiUdpIpvxEndpointTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMiUdpIpvxEndpointTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMiUdpIpvxEndpointProcess ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,UINT4 *));
