/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: udpipvxinc.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
 *
 * Description: Contains common includes used by UDP submodule.
 *
 *******************************************************************/
#ifndef _UDP_INC_H
#define _UDP_INC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "udpport.h"
#include "udppdudf.h"
#include "udpprtno.h"
#include "udpproto.h"
#include "snmctdfs.h"
#include "fssocket.h"

#endif /* _UDP_INC_H */

