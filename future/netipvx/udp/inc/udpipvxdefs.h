/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: udpipvxdefs.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
 *
 * Description: File containing the macros and constants used in UDP
 *
 *******************************************************************/
#ifndef _UDPDEFS_H_
#define _UDPDEFS_H_

#define    UDP_ZERO      0
#define    UDP_ONE       1
#define    UDP_TWO       2
#define    UDP_THREE     3
#define    UDP_FOUR      4
#define    UDP_FAILURE  UDP_ZERO
#define    UDP_SUCCESS  UDP_ONE
#define    UDP_TRUE     UDP_ONE
#define    UDP_FALSE    UDP_ZERO

#define UDP_IPV4_ADDR  UDP_ONE
#define UDP_IPV6_ADDR  UDP_TWO

#define IPVX_IPV4_ADDR_LEN   4
#define IPVX_IPV6_ADDR_LEN   16
#define IPVX_MAX_INET_ADDR_LEN  IPVX_IPV6_ADDR_LEN
#endif
