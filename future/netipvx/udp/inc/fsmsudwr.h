#ifndef _FSMSUDWR_H
#define _FSMSUDWR_H

VOID RegisterFSMSUD(VOID);

VOID UnRegisterFSMSUD(VOID);
INT4 FsMiUdpInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpNoPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpInErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpInNoCksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpInIcmpErrGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpInErrCksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpInBcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpHCInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpHCOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIUdpStatTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMiUdpIpvxInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxNoPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxInErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxInNoCksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxInIcmpErrGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxInErrCksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxInBcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxHCInDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 FsMiUdpIpvxHCOutDatagramsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMiUdpEndpointTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMiUdpEndpointProcessGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMiUdpIpvxEndpointTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMiUdpIpvxEndpointProcessGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMSUDWR_H */
