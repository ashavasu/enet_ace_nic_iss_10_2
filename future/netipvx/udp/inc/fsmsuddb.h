/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsuddb.h,v 1.3 2009/10/06 10:27:22 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSUDDB_H
#define _FSMSUDDB_H

UINT1 FsMIUdpStatTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMiUdpEndpointTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMiUdpIpvxEndpointTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmsud [] ={1,3,6,1,4,1,29601,2,28};
tSNMP_OID_TYPE fsmsudOID = {9, fsmsud};


UINT4 FsMiUdpInDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,1};
UINT4 FsMiUdpNoPorts [ ] ={1,3,6,1,4,1,29601,2,28,1,2};
UINT4 FsMiUdpInErrors [ ] ={1,3,6,1,4,1,29601,2,28,1,3};
UINT4 FsMiUdpOutDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,4};
UINT4 FsMiUdpInNoCksum [ ] ={1,3,6,1,4,1,29601,2,28,1,5};
UINT4 FsMiUdpInIcmpErr [ ] ={1,3,6,1,4,1,29601,2,28,1,6};
UINT4 FsMiUdpInErrCksum [ ] ={1,3,6,1,4,1,29601,2,28,1,7};
UINT4 FsMiUdpInBcast [ ] ={1,3,6,1,4,1,29601,2,28,1,8};
UINT4 FsMiUdpHCInDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,9};
UINT4 FsMiUdpHCOutDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,10};
UINT4 FsMiUdpIpvxContextId [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,1};
UINT4 FsMiUdpIpvxInDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,2};
UINT4 FsMiUdpIpvxNoPorts [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,3};
UINT4 FsMiUdpIpvxInErrors [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,4};
UINT4 FsMiUdpIpvxOutDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,5};
UINT4 FsMiUdpIpvxInNoCksum [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,6};
UINT4 FsMiUdpIpvxInIcmpErr [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,7};
UINT4 FsMiUdpIpvxInErrCksum [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,8};
UINT4 FsMiUdpIpvxInBcast [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,9};
UINT4 FsMiUdpIpvxHCInDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,10};
UINT4 FsMiUdpIpvxHCOutDatagrams [ ] ={1,3,6,1,4,1,29601,2,28,1,11,1,11};
UINT4 FsMiUdpEndpointLocalAddressType [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,1};
UINT4 FsMiUdpEndpointLocalAddress [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,2};
UINT4 FsMiUdpEndpointLocalPort [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,3};
UINT4 FsMiUdpEndpointRemoteAddressType [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,4};
UINT4 FsMiUdpEndpointRemoteAddress [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,5};
UINT4 FsMiUdpEndpointRemotePort [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,6};
UINT4 FsMiUdpEndpointInstance [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,7};
UINT4 FsMiUdpEndpointProcess [ ] ={1,3,6,1,4,1,29601,2,28,1,12,1,8};
UINT4 FsMiUdpIpvxEndpointLocalAddressType [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,1};
UINT4 FsMiUdpIpvxEndpointLocalAddress [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,2};
UINT4 FsMiUdpIpvxEndpointLocalPort [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,3};
UINT4 FsMiUdpIpvxEndpointRemoteAddressType [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,4};
UINT4 FsMiUdpIpvxEndpointRemoteAddress [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,5};
UINT4 FsMiUdpIpvxEndpointRemotePort [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,6};
UINT4 FsMiUdpIpvxEndpointInstance [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,7};
UINT4 FsMiUdpIpvxEndpointProcess [ ] ={1,3,6,1,4,1,29601,2,28,1,13,1,8};


tMbDbEntry fsmsudMibEntry[]= {

{{11,FsMiUdpInDatagrams}, NULL, FsMiUdpInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpNoPorts}, NULL, FsMiUdpNoPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpInErrors}, NULL, FsMiUdpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpOutDatagrams}, NULL, FsMiUdpOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpInNoCksum}, NULL, FsMiUdpInNoCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpInIcmpErr}, NULL, FsMiUdpInIcmpErrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpInErrCksum}, NULL, FsMiUdpInErrCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpInBcast}, NULL, FsMiUdpInBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpHCInDatagrams}, NULL, FsMiUdpHCInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMiUdpHCOutDatagrams}, NULL, FsMiUdpHCOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsMiUdpIpvxContextId}, GetNextIndexFsMIUdpStatTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInDatagrams}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxNoPorts}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxNoPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInErrors}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxOutDatagrams}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInNoCksum}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInNoCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInIcmpErr}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInIcmpErrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInErrCksum}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInErrCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxInBcast}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxInBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxHCInDatagrams}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxHCInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpIpvxHCOutDatagrams}, GetNextIndexFsMIUdpStatTable, FsMiUdpIpvxHCOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIUdpStatTableINDEX, 1, 0, 0, NULL},

{{13,FsMiUdpEndpointLocalAddressType}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointLocalAddress}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointLocalPort}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointRemoteAddressType}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointRemoteAddress}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointRemotePort}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointInstance}, GetNextIndexFsMiUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpEndpointProcess}, GetNextIndexFsMiUdpEndpointTable, FsMiUdpEndpointProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMiUdpEndpointTableINDEX, 7, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointLocalAddressType}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointLocalAddress}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointLocalPort}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointRemoteAddressType}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointRemoteAddress}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointRemotePort}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointInstance}, GetNextIndexFsMiUdpIpvxEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},

{{13,FsMiUdpIpvxEndpointProcess}, GetNextIndexFsMiUdpIpvxEndpointTable, FsMiUdpIpvxEndpointProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMiUdpIpvxEndpointTableINDEX, 8, 0, 0, NULL},
};
tMibData fsmsudEntry = { 37, fsmsudMibEntry };
#endif /* _FSMSUDDB_H */

