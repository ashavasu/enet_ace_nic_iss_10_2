/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdudpdb.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDUDPDB_H
#define _STDUDPDB_H

UINT1 UdpEndpointTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT4 stdudp [] ={1,3,6,1,2,1,7};
tSNMP_OID_TYPE stdudpOID = {7, stdudp};


UINT4 UdpInDatagrams [ ] ={1,3,6,1,2,1,7,1};
UINT4 UdpNoPorts [ ] ={1,3,6,1,2,1,7,2};
UINT4 UdpInErrors [ ] ={1,3,6,1,2,1,7,3};
UINT4 UdpOutDatagrams [ ] ={1,3,6,1,2,1,7,4};
UINT4 UdpHCInDatagrams [ ] ={1,3,6,1,2,1,7,8};
UINT4 UdpHCOutDatagrams [ ] ={1,3,6,1,2,1,7,9};
UINT4 UdpEndpointLocalAddressType [ ] ={1,3,6,1,2,1,7,7,1,1};
UINT4 UdpEndpointLocalAddress [ ] ={1,3,6,1,2,1,7,7,1,2};
UINT4 UdpEndpointLocalPort [ ] ={1,3,6,1,2,1,7,7,1,3};
UINT4 UdpEndpointRemoteAddressType [ ] ={1,3,6,1,2,1,7,7,1,4};
UINT4 UdpEndpointRemoteAddress [ ] ={1,3,6,1,2,1,7,7,1,5};
UINT4 UdpEndpointRemotePort [ ] ={1,3,6,1,2,1,7,7,1,6};
UINT4 UdpEndpointInstance [ ] ={1,3,6,1,2,1,7,7,1,7};
UINT4 UdpEndpointProcess [ ] ={1,3,6,1,2,1,7,7,1,8};


tMbDbEntry stdudpMibEntry[]= {

{{8,UdpInDatagrams}, NULL, UdpInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpNoPorts}, NULL, UdpNoPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpInErrors}, NULL, UdpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpOutDatagrams}, NULL, UdpOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UdpEndpointLocalAddressType}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointLocalAddress}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointLocalPort}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointRemoteAddressType}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointRemoteAddress}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointRemotePort}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointInstance}, GetNextIndexUdpEndpointTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{10,UdpEndpointProcess}, GetNextIndexUdpEndpointTable, UdpEndpointProcessGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, UdpEndpointTableINDEX, 7, 0, 0, NULL},

{{8,UdpHCInDatagrams}, NULL, UdpHCInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpHCOutDatagrams}, NULL, UdpHCOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdudpEntry = { 14, stdudpMibEntry };
#endif /* _STDUDPDB_H */

