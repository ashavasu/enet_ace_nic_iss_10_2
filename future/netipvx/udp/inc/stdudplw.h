/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdudplw.h,v 1.1.1.2 2015/10/13 13:07:50 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUdpInDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetUdpNoPorts ARG_LIST((UINT4 *));

INT1
nmhGetUdpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetUdpOutDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetUdpHCInDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetUdpHCOutDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for UdpEndpointTable. */
INT1
nmhValidateIndexInstanceUdpEndpointTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpEndpointTable  */

INT1
nmhGetFirstIndexUdpEndpointTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUdpEndpointTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUdpEndpointProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for UdpTable. */
INT1
nmhValidateIndexInstanceUdpTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpTable  */

INT1
nmhGetFirstIndexUdpTable ARG_LIST((UINT4 * , INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */
