#ifndef __UDPUTIL_C__
#define __UDPUTIL_C__

/* 
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : udputil.c                                        |
 * |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                      |
 * |  MODULE NAME           : UDP                                               |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    : Linux (Portable)                                 |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for UDP utilities commands       | * |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED            /* FSIP */
# include "ip6inc.h"
#else
#include "lr.h"
#include "ipv6.h"
#include "ip6util.h"
#endif
# include "udputils.h"
#else
# include "ipvxinc.h"
#endif
#include "udpipvxdefs.h"

/****************************************************************************
 Function    :  UdpGetMatchingIPv6Entry
 Description :  This function checks whether a IPV6 entry exists with
                the given port
 Input       :  u2Ipv4LocalPort - Local port of the Connection
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef IP6_WANTED

INT4
UdpGetMatchingIPv6Entry (UINT2 u2Ipv4LocalPort)
{
#ifndef LNXIP6_WANTED            /* FSIP */
    tUdp6Port          *pUdp6CBNode = NULL;
    INT4                i4Found = UDP_ZERO;

    /* Assumption: IP6_TASK_LOCK is taken before
     * calling this function */
    TMO_SLL_Scan (&(gIp6GblInfo.Udp6CbEntry), pUdp6CBNode, tUdp6Port *)
    {
        if ((pUdp6CBNode->u2UPort == u2Ipv4LocalPort) &&
            (UdpCheckForZeroIPv6Add (pUdp6CBNode->addr) == i4Found))
        {
            return SNMP_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (u2Ipv4LocalPort);
#endif
    return SNMP_FAILURE;
}
#endif

/****************************************************************************
 Function    :  UdpCheckForZeroIPv6Add
 Description :  This function checks whether a IPV6 address is zero.
 Input       :  The Indices
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef IP6_WANTED

INT4
UdpCheckForZeroIPv6Add (tIp6Addr Ip6UdpAddr)
{
    INT4                i4Found = UDP_ZERO;
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, UDP_ZERO, sizeof (tIp6Addr));
    if ((Ip6AddrDiff (&Ip6UdpAddr, &Ip6Addr)) == i4Found)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
#endif
#if defined IP_WANTED
/****************************************************************************
 Function    :  UdpMiGetFirstIndexEndpointTable
 Description :  This function gets first indices of Context
                specific end point table
 Input       :  None
 Output      :  pi4UdpEndpointLocalAddressType - Type of Local Address 
                pUdpEndpointLocalAddress - Reference to Local address
                pu4UdpEndpointLocalPort - Local UDP port number
                pi4UdpEndpointRemoteAddressType - Type of Remote Address
                pUdpEndpointRemoteAddress - Reference to Remote address
                pu4UdpEndpointRemotePort - Remote Udp port number
                pu4UdpEndpointInstance - Instance value
                
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 Note        :  Assumption: LOCK is taken before calling this function 
****************************************************************************/
INT4
UdpMiGetFirstIndexEndpointTable (INT4 *pi4UdpEndpointLocalAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pUdpEndpointLocalAddress,
                                 UINT4 *pu4UdpEndpointLocalPort,
                                 INT4 *pi4UdpEndpointRemoteAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pUdpEndpointRemoteAddress,
                                 UINT4 *pu4UdpEndpointRemotePort,
                                 UINT4 *pu4UdpEndpointInstance)
{

    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = udpCxtGetFirstIndexEndpointTable
        (pUdpEndpointLocalAddress,
         pu4UdpEndpointLocalPort,
         pUdpEndpointRemoteAddress,
         pu4UdpEndpointRemotePort, pu4UdpEndpointInstance);

    *pi4UdpEndpointLocalAddressType = INET_ADDR_TYPE_IPV4;
    *pi4UdpEndpointRemoteAddressType = INET_ADDR_TYPE_IPV4;
#if defined IP6_WANTED
    if (i4RetVal == SNMP_FAILURE)
    {
        i4RetVal = udp6CxtGetFirstIndexIpvxUdp6Table
            (pUdpEndpointLocalAddress,
             pu4UdpEndpointLocalPort,
             pUdpEndpointRemoteAddress,
             pu4UdpEndpointRemotePort, pu4UdpEndpointInstance);
        *pi4UdpEndpointLocalAddressType = INET_ADDR_TYPE_IPV6;
        *pi4UdpEndpointRemoteAddressType = INET_ADDR_TYPE_IPV6;

    }
#endif
    return i4RetVal;
}

/****************************************************************************
 Function    :  UdpMiGetNextIndexEndpointTable
 Description :  This function gets next index to the given index in Context
                 specific end point table
 Input       :  i4UdpEndpointLocalAddressType, pUdpEndpointLocalAddress,
                u4UdpEndpointLocalPort, i4UdpEndpointRemoteAddressType,
                pUdpEndpointRemoteAddress, u4UdpEndpointRemotePort,
                u4UdpEndpointInstance
 Output      :  pi4NextUdpEndpointLocalAddressType, pNextUdpEndpointLocalAddress,
                pu4NextUdpEndpointLocalPort, pi4NextUdpEndpointRemoteAddressType,
                pNextUdpEndpointRemoteAddress, pu4NextUdpEndpointRemotePort
                pu4NextUdpEndpointInstance
 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 Note        :  Assumption: LOCK is taken before calling this function 
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    UdpMiGetNextIndexEndpointTable
    (INT4 i4UdpEndpointLocalAddressType,
     INT4 *pi4NextUdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointLocalAddress,
     UINT4 u4UdpEndpointLocalPort,
     UINT4 *pu4NextUdpEndpointLocalPort,
     INT4 i4UdpEndpointRemoteAddressType,
     INT4 *pi4NextUdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointRemoteAddress,
     UINT4 u4UdpEndpointRemotePort,
     UINT4 *pu4NextUdpEndpointRemotePort,
     UINT4 u4UdpEndpointInstance, UINT4 *pu4NextUdpEndpointInstance)
{
    /* Assumption: LOCK is taken before
     * calling this function */
    INT4                i4RetVal = SNMP_FAILURE;

    if ((i4UdpEndpointLocalAddressType != INET_ADDR_TYPE_IPV4) &&
        (i4UdpEndpointLocalAddressType != INET_ADDR_TYPE_IPV6) &&
        (i4UdpEndpointRemoteAddressType != INET_ADDR_TYPE_IPV4) &&
        (i4UdpEndpointRemoteAddressType != INET_ADDR_TYPE_IPV6))
    {
        return i4RetVal;
    }

    if (i4UdpEndpointLocalAddressType == INET_ADDR_TYPE_IPV4)
    {
        i4RetVal = udpCxtGetNextIndexEndpointTable
            (pUdpEndpointLocalAddress,
             pNextUdpEndpointLocalAddress,
             u4UdpEndpointLocalPort,
             pu4NextUdpEndpointLocalPort,
             pUdpEndpointRemoteAddress,
             pNextUdpEndpointRemoteAddress,
             u4UdpEndpointRemotePort,
             pu4NextUdpEndpointRemotePort,
             u4UdpEndpointInstance, pu4NextUdpEndpointInstance);

        *pi4NextUdpEndpointLocalAddressType = INET_ADDR_TYPE_IPV4;
        *pi4NextUdpEndpointRemoteAddressType = INET_ADDR_TYPE_IPV4;
    }
#if defined IP6_WANTED
    if (i4RetVal == SNMP_FAILURE)
    {
        if (i4UdpEndpointLocalAddressType == INET_ADDR_TYPE_IPV6)
        {
            i4RetVal = udp6CxtGetNextIndexIpvxUdp6Table
                (pUdpEndpointLocalAddress,
                 pNextUdpEndpointLocalAddress,
                 u4UdpEndpointLocalPort,
                 pu4NextUdpEndpointLocalPort,
                 pUdpEndpointRemoteAddress,
                 pNextUdpEndpointRemoteAddress,
                 u4UdpEndpointRemotePort,
                 pu4NextUdpEndpointRemotePort,
                 u4UdpEndpointInstance, pu4NextUdpEndpointInstance);
        }
        else
        {
            i4RetVal = udp6CxtGetFirstIndexIpvxUdp6Table
                (pNextUdpEndpointLocalAddress,
                 pu4NextUdpEndpointLocalPort,
                 pNextUdpEndpointRemoteAddress,
                 pu4NextUdpEndpointRemotePort, pu4NextUdpEndpointInstance);

        }
        *pi4NextUdpEndpointLocalAddressType = INET_ADDR_TYPE_IPV6;
        *pi4NextUdpEndpointRemoteAddressType = INET_ADDR_TYPE_IPV6;
    }
#endif
    return i4RetVal;

}
#endif
#endif
