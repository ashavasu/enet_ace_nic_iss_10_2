# include  "lr.h"
# include  "fssnmp.h"
# include  "stdudplw.h"
# include  "stdudpwr.h"
# include  "stdudpdb.h"

VOID
RegisterSTDUDP ()
{
    SNMPRegisterMib (&stdudpOID, &stdudpEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdudpOID, (const UINT1 *) "stdudpipvx");
}

VOID
UnRegisterSTDUDP ()
{
    SNMPUnRegisterMib (&stdudpOID, &stdudpEntry);
    SNMPDelSysorEntry (&stdudpOID, (const UINT1 *) "stdudpipvx");
}

INT4
UdpInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpInDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
UdpNoPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpNoPorts (&(pMultiData->u4_ULongValue)));
}

INT4
UdpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpInErrors (&(pMultiData->u4_ULongValue)));
}

INT4
UdpOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpOutDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
UdpHCInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpHCInDatagrams (&(pMultiData->u8_Counter64Value)));
}

INT4
UdpHCOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUdpHCOutDatagrams (&(pMultiData->u8_Counter64Value)));
}

INT4
GetNextIndexUdpEndpointTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUdpEndpointTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             &(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUdpEndpointTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             pFirstMultiIndex->pIndex[6].u4_ULongValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
UdpEndpointProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUdpEndpointTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUdpEndpointProcess (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      pMultiIndex->pIndex[5].u4_ULongValue,
                                      pMultiIndex->pIndex[6].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}
