/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsudlw.c,v 1.6 2010/08/02 06:56:01 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "ip.h"
# include  "vcm.h"
# include  "fssnmp.h"
# include  "fsmsudlw.h"

# include  "stdudplw.h"
# include  "udpipvxdefs.h"
# include  "stdudpwr.h"
#if defined IP_WANTED
# include  "udpipvxinc.h"
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED            /* FSIP */
# include "ip6inc.h"
#else
# include "ipvx.h"
#endif
# include "ipv6.h"
#endif
#ifdef LNXIP4_WANTED
#include "lnxiputl.h"
#endif
#include "udputils.h"

extern INT1 nmhGetFsUdpInNoCksum ARG_LIST ((UINT4 *));

extern INT1 nmhGetFsUdpInIcmpErr ARG_LIST ((UINT4 *));

extern INT1 nmhGetFsUdpInErrCksum ARG_LIST ((UINT4 *));

extern INT1 nmhGetFsUdpInBcast ARG_LIST ((UINT4 *));

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMiUdpInDatagrams
 Input       :  The Indices

                The Object 
                retValFsMiUdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInDatagrams (UINT4 *pu4RetValFsMiUdpInDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpInDatagrams (pu4RetValFsMiUdpInDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMiUdpNoPorts
 Input       :  The Indices

                The Object 
                retValFsMiUdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpNoPorts (UINT4 *pu4RetValFsMiUdpNoPorts)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpNoPorts (pu4RetValFsMiUdpNoPorts);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMiUdpInErrors
 Input       :  The Indices

                The Object 
                retValFsMiUdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInErrors (UINT4 *pu4RetValFsMiUdpInErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpInErrors (pu4RetValFsMiUdpInErrors);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpOutDatagrams
 Input       :  The Indices

                The Object 
                retValFsMiUdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpOutDatagrams (UINT4 *pu4RetValFsMiUdpOutDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpOutDatagrams (pu4RetValFsMiUdpOutDatagrams);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMiUdpInNoCksum
 Input       :  The Indices

                The Object 
                retValFsMiUdpInNoCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInNoCksum (UINT4 *pu4RetValFsMiUdpInNoCksum)
{
#ifdef IP_WANTED
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsUdpInNoCksum (pu4RetValFsMiUdpInNoCksum);
    return i1Return;
#else
    UNUSED_PARAM (pu4RetValFsMiUdpInNoCksum);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsMiUdpInIcmpErr
 Input       :  The Indices

                The Object 
                retValFsMiUdpInIcmpErr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInIcmpErr (UINT4 *pu4RetValFsMiUdpInIcmpErr)
{
#ifdef IP_WANTED
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsUdpInIcmpErr (pu4RetValFsMiUdpInIcmpErr);
    return i1Return;
#else
    UNUSED_PARAM (pu4RetValFsMiUdpInIcmpErr);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpInErrCksum
 Input       :  The Indices

                The Object 
                retValFsMiUdpInErrCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInErrCksum (UINT4 *pu4RetValFsMiUdpInErrCksum)
{
#ifdef IP_WANTED
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsUdpInErrCksum (pu4RetValFsMiUdpInErrCksum);
    return i1Return;
#else
    UNUSED_PARAM (pu4RetValFsMiUdpInErrCksum);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpInBcast
 Input       :  The Indices

                The Object 
                retValFsMiUdpInBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpInBcast (UINT4 *pu4RetValFsMiUdpInBcast)
{
#ifdef IP_WANTED
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsUdpInBcast (pu4RetValFsMiUdpInBcast);
    return i1Return;
#else
    UNUSED_PARAM (pu4RetValFsMiUdpInBcast);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpHCInDatagrams
 Input       :  The Indices

                The Object 
                retValFsMiUdpHCInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpHCInDatagrams (tSNMP_COUNTER64_TYPE *
                            pu8RetValFsMiUdpHCInDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpHCInDatagrams (pu8RetValFsMiUdpHCInDatagrams);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpHCOutDatagrams
 Input       :  The Indices

                The Object 
                retValFsMiUdpHCOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpHCOutDatagrams (tSNMP_COUNTER64_TYPE *
                             pu8RetValFsMiUdpHCOutDatagrams)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpHCOutDatagrams (pu8RetValFsMiUdpHCOutDatagrams);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIUdpStatTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIUdpStatTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIUdpStatTable (INT4 i4FsMiUdpIpvxContextId)
{

#ifdef IP_WANTED
    if ((i4FsMiUdpIpvxContextId < (INT4) VCM_DEFAULT_CONTEXT) ||
        (i4FsMiUdpIpvxContextId >= (INT4) SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist (i4FsMiUdpIpvxContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }
    UDP_DS_LOCK ();
    if (UdpIsValidCxtId ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_FAILURE)
    {
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIUdpStatTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIUdpStatTable (INT4 *pi4FsMiUdpIpvxContextId)
{
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    if (UdpGetFirstCxtId ((UINT4 *) pi4FsMiUdpIpvxContextId) == SNMP_FAILURE)
    {
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (pi4FsMiUdpIpvxContextId);
    return SNMP_FAILURE;
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIUdpStatTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
                nextFsMiUdpIpvxContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIUdpStatTable (INT4 i4FsMiUdpIpvxContextId,
                                 INT4 *pi4NextFsMiUdpIpvxContextId)
{
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    if (UdpGetNextCxtId ((UINT4) i4FsMiUdpIpvxContextId,
                         (UINT4 *) pi4NextFsMiUdpIpvxContextId) == SNMP_FAILURE)
    {
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pi4NextFsMiUdpIpvxContextId);
    return SNMP_FAILURE;
#endif
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInDatagrams
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInDatagrams (INT4 i4FsMiUdpIpvxContextId,
                              UINT4 *pu4RetValFsMiUdpIpvxInDatagrams)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValFsMiUdpIpvxInDatagrams = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInDatagrams = pUdpStatEntry->u4InPkts;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInDatagrams += pUdp6StatEntry->u4InDgrams;
        }
    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInDatagrams);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxNoPorts
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxNoPorts (INT4 i4FsMiUdpIpvxContextId,
                          UINT4 *pu4RetValFsMiUdpIpvxNoPorts)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValFsMiUdpIpvxNoPorts = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxNoPorts = pUdpStatEntry->u4InErrNoport;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxNoPorts += pUdp6StatEntry->u4NumPorts;
        }
    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxNoPorts);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInErrors
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInErrors (INT4 i4FsMiUdpIpvxContextId,
                           UINT4 *pu4RetValFsMiUdpIpvxInErrors)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValFsMiUdpIpvxInErrors = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInErrors = pUdpStatEntry->u4InErrNoport +
                pUdpStatEntry->u4InErrCksum + pUdpStatEntry->u4InTotalErr;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInErrors += pUdp6StatEntry->u4InErrs;
        }
    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInErrors);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxOutDatagrams
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxOutDatagrams (INT4 i4FsMiUdpIpvxContextId,
                               UINT4 *pu4RetValFsMiUdpIpvxOutDatagrams)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValFsMiUdpIpvxOutDatagrams = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxOutDatagrams = pUdpStatEntry->u4OutPkts;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxOutDatagrams += pUdp6StatEntry->u4OutDgrams;
        }
    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxOutDatagrams);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInNoCksum
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInNoCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInNoCksum (INT4 i4FsMiUdpIpvxContextId,
                            UINT4 *pu4RetValFsMiUdpIpvxInNoCksum)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;

    *pu4RetValFsMiUdpIpvxInNoCksum = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInNoCksum = pUdpStatEntry->u4InNoCksum;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInNoCksum);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInIcmpErr
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInIcmpErr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInIcmpErr (INT4 i4FsMiUdpIpvxContextId,
                            UINT4 *pu4RetValFsMiUdpIpvxInIcmpErr)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;

    *pu4RetValFsMiUdpIpvxInIcmpErr = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInIcmpErr = pUdpStatEntry->u4InIcmpErr;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInIcmpErr);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInErrCksum
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInErrCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInErrCksum (INT4 i4FsMiUdpIpvxContextId,
                             UINT4 *pu4RetValFsMiUdpIpvxInErrCksum)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;

    *pu4RetValFsMiUdpIpvxInErrCksum = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInErrCksum = pUdpStatEntry->u4InErrCksum;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInErrCksum);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxInBcast
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxInBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxInBcast (INT4 i4FsMiUdpIpvxContextId,
                          UINT4 *pu4RetValFsMiUdpIpvxInBcast)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;

    *pu4RetValFsMiUdpIpvxInBcast = UDP_ZERO;

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            *pu4RetValFsMiUdpIpvxInBcast = pUdpStatEntry->u4InBcast;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxInBcast);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxHCInDatagrams
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxHCInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxHCInDatagrams (INT4 i4FsMiUdpIpvxContextId,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValFsMiUdpIpvxHCInDatagrams)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            pu8RetValFsMiUdpIpvxHCInDatagrams->lsn =
                pUdpStatEntry->u8HcInPkts.u4Lo;
            pu8RetValFsMiUdpIpvxHCInDatagrams->msn =
                pUdpStatEntry->u8HcInPkts.u4Hi;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry != NULL)
        {
            if ((pUdp6StatEntry->u8HcInDgrams.u4Lo) >
                (pu8RetValFsMiUdpIpvxHCInDatagrams->lsn +
                 pUdp6StatEntry->u8HcInDgrams.u4Lo))
            {
                pu8RetValFsMiUdpIpvxHCInDatagrams->msn += IPVX_ONE;
            }
            pu8RetValFsMiUdpIpvxHCInDatagrams->lsn +=
                pUdp6StatEntry->u8HcInDgrams.u4Lo;
            pu8RetValFsMiUdpIpvxHCInDatagrams->msn +=
                pUdp6StatEntry->u8HcInDgrams.u4Hi;
        }
    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu8RetValFsMiUdpIpvxHCInDatagrams);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxHCOutDatagrams
 Input       :  The Indices
                FsMiUdpIpvxContextId

                The Object 
                retValFsMiUdpIpvxHCOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxHCOutDatagrams (INT4 i4FsMiUdpIpvxContextId,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValFsMiUdpIpvxHCOutDatagrams)
{
#ifdef IP_WANTED
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    pu8RetValFsMiUdpIpvxHCOutDatagrams->lsn = UDP_ZERO;
    pu8RetValFsMiUdpIpvxHCOutDatagrams->msn = UDP_ZERO;
    UDP_DS_LOCK ();
    if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdpStatEntry = UdpGetCurrCxtStatEntry ();
        if (pUdpStatEntry != NULL)
        {
            pu8RetValFsMiUdpIpvxHCOutDatagrams->lsn =
                pUdpStatEntry->u8HcOutPkts.u4Lo;
            pu8RetValFsMiUdpIpvxHCOutDatagrams->msn =
                pUdpStatEntry->u8HcOutPkts.u4Hi;
        }
    }
    UdpReleaseContext ();
    UDP_DS_UNLOCK ();
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        pUdp6StatEntry = Udp6GetCurrCxtStatEntry ();
        if (pUdp6StatEntry == NULL)
        {
            Ip6ReleaseContext ();
            IP6_TASK_UNLOCK ();
            return SNMP_FAILURE;
        }

        pu8RetValFsMiUdpIpvxHCOutDatagrams->lsn +=
            pUdp6StatEntry->u8HcOutDgrams.u4Lo;
        pu8RetValFsMiUdpIpvxHCOutDatagrams->msn +=
            pUdp6StatEntry->u8HcOutDgrams.u4Hi;

        if ((pUdp6StatEntry->u8HcOutDgrams.u4Lo) >
            (pu8RetValFsMiUdpIpvxHCOutDatagrams->lsn +
             pUdp6StatEntry->u8HcOutDgrams.u4Lo))
        {
            pu8RetValFsMiUdpIpvxHCOutDatagrams->msn += IPVX_ONE;
        }
        pu8RetValFsMiUdpIpvxHCOutDatagrams->lsn +=
            pUdp6StatEntry->u8HcOutDgrams.u4Lo;
        pu8RetValFsMiUdpIpvxHCOutDatagrams->msn +=
            pUdp6StatEntry->u8HcOutDgrams.u4Hi;

    }
    Ip6ReleaseContext ();
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pu8RetValFsMiUdpIpvxHCOutDatagrams);
#endif
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsUdpIpvxGlobalEndpointTable. */

/****************************************************************************
 Function    : nmhValidateIndexInstanceFsMiUdpEndpoint 
 Input       :  The Indices
                FsUdpIpvxEndpointLocalAddressType
                FsUdpIpvxEndpointLocalAddress
                FsUdpIpvxEndpointLocalPort
                FsUdpIpvxEndpointRemoteAddressType
                FsUdpIpvxEndpointRemoteAddress
                FsUdpIpvxEndpointRemotePort
                FsUdpIpvxEndpointInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMiUdpEndpointTable (INT4
                                              i4FsMiUdpEndpointLocalAddressType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMiUdpEndpointLocalAddress,
                                              UINT4 u4FsMiUdpEndpointLocalPort,
                                              INT4
                                              i4FsMiUdpEndpointRemoteAddressType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMiUdpEndpointRemoteAddress,
                                              UINT4 u4FsMiUdpEndpointRemotePort,
                                              UINT4 u4FsMiUdpEndpointInstance)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhValidateIndexInstanceUdpEndpointTable
        (i4FsMiUdpEndpointLocalAddressType, pFsMiUdpEndpointLocalAddress,
         u4FsMiUdpEndpointLocalPort, i4FsMiUdpEndpointRemoteAddressType,
         pFsMiUdpEndpointRemoteAddress, u4FsMiUdpEndpointRemotePort,
         u4FsMiUdpEndpointInstance);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMiUdpEndpointTable
 Input       :  The Indices
                FsMiUdpEndpointLocalAddressType
                FsMiUdpEndpointLocalAddress
                FsMiUdpEndpointLocalPort
                FsMiUdpEndpointRemoteAddressType
                FsMiUdpEndpointRemoteAddress
                FsMiUdpEndpointRemotePort
                FsMiUdpEndpointInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMiUdpEndpointTable (INT4 *pi4FsMiUdpEndpointLocalAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMiUdpEndpointLocalAddress,
                                      UINT4 *pu4FsMiUdpEndpointLocalPort,
                                      INT4 *pi4FsMiUdpEndpointRemoteAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMiUdpEndpointRemoteAddress,
                                      UINT4 *pu4FsMiUdpEndpointRemotePort,
                                      UINT4 *pu4FsMiUdpEndpointInstance)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFirstIndexUdpEndpointTable (pi4FsMiUdpEndpointLocalAddressType,
                                          pFsMiUdpEndpointLocalAddress,
                                          pu4FsMiUdpEndpointLocalPort,
                                          pi4FsMiUdpEndpointRemoteAddressType,
                                          pFsMiUdpEndpointRemoteAddress,
                                          pu4FsMiUdpEndpointRemotePort,
                                          pu4FsMiUdpEndpointInstance);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMiUdpEndpointTable
 Input       :  The Indices
                FsMiUdpEndpointLocalAddressType
                nextFsMiUdpEndpointLocalAddressType
                FsMiUdpEndpointLocalAddress
                nextFsMiUdpEndpointLocalAddress
                FsMiUdpEndpointLocalPort
                nextFsMiUdpEndpointLocalPort
                FsMiUdpEndpointRemoteAddressType
                nextFsMiUdpEndpointRemoteAddressType
                FsMiUdpEndpointRemoteAddress
                nextFsMiUdpEndpointRemoteAddress
                FsMiUdpEndpointRemotePort
                nextFsMiUdpEndpointRemotePort
                FsMiUdpEndpointInstance
                nextFsMiUdpEndpointInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMiUdpEndpointTable (INT4 i4FsMiUdpEndpointLocalAddressType,
                                     INT4
                                     *pi4NextFsMiUdpEndpointLocalAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMiUdpEndpointLocalAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMiUdpEndpointLocalAddress,
                                     UINT4 u4FsMiUdpEndpointLocalPort,
                                     UINT4 *pu4NextFsMiUdpEndpointLocalPort,
                                     INT4 i4FsMiUdpEndpointRemoteAddressType,
                                     INT4
                                     *pi4NextFsMiUdpEndpointRemoteAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMiUdpEndpointRemoteAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMiUdpEndpointRemoteAddress,
                                     UINT4 u4FsMiUdpEndpointRemotePort,
                                     UINT4 *pu4NextFsMiUdpEndpointRemotePort,
                                     UINT4 u4FsMiUdpEndpointInstance,
                                     UINT4 *pu4NextFsMiUdpEndpointInstance)
{

    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetNextIndexUdpEndpointTable (i4FsMiUdpEndpointLocalAddressType,
                                         pi4NextFsMiUdpEndpointLocalAddressType,
                                         pFsMiUdpEndpointLocalAddress,
                                         pNextFsMiUdpEndpointLocalAddress,
                                         u4FsMiUdpEndpointLocalPort,
                                         pu4NextFsMiUdpEndpointLocalPort,
                                         i4FsMiUdpEndpointRemoteAddressType,
                                         pi4NextFsMiUdpEndpointRemoteAddressType,
                                         pFsMiUdpEndpointRemoteAddress,
                                         pNextFsMiUdpEndpointRemoteAddress,
                                         u4FsMiUdpEndpointRemotePort,
                                         pu4NextFsMiUdpEndpointRemotePort,
                                         u4FsMiUdpEndpointInstance,
                                         pu4NextFsMiUdpEndpointInstance);
    return i1Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMiUdpEndpointProcess
 Input       :  The Indices
                FsMiUdpEndpointLocalAddressType
                FsMiUdpEndpointLocalAddress
                FsMiUdpEndpointLocalPort
                FsMiUdpEndpointRemoteAddressType
                FsMiUdpEndpointRemoteAddress
                FsMiUdpEndpointRemotePort
                FsMiUdpEndpointInstance

                The Object 
                retValFsMiUdpEndpointProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpEndpointProcess (INT4 i4FsMiUdpEndpointLocalAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMiUdpEndpointLocalAddress,
                              UINT4 u4FsMiUdpEndpointLocalPort,
                              INT4 i4FsMiUdpEndpointRemoteAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMiUdpEndpointRemoteAddress,
                              UINT4 u4FsMiUdpEndpointRemotePort,
                              UINT4 u4FsMiUdpEndpointInstance,
                              UINT4 *pu4RetValFsMiUdpEndpointProcess)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetUdpEndpointProcess (i4FsMiUdpEndpointLocalAddressType,
                                         pFsMiUdpEndpointLocalAddress,
                                         u4FsMiUdpEndpointLocalPort,
                                         i4FsMiUdpEndpointRemoteAddressType,
                                         pFsMiUdpEndpointRemoteAddress,
                                         u4FsMiUdpEndpointRemotePort,
                                         u4FsMiUdpEndpointInstance,
                                         pu4RetValFsMiUdpEndpointProcess);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMiUdpIpvxEndpointTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMiUdpIpvxEndpointTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
                FsMiUdpIpvxEndpointLocalAddressType
                FsMiUdpIpvxEndpointLocalAddress
                FsMiUdpIpvxEndpointLocalPort
                FsMiUdpIpvxEndpointRemoteAddressType
                FsMiUdpIpvxEndpointRemoteAddress
                FsMiUdpIpvxEndpointRemotePort
                FsMiUdpIpvxEndpointInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMiUdpIpvxEndpointTable (INT4 i4FsMiUdpIpvxContextId,
                                                  INT4
                                                  i4FsMiUdpIpvxEndpointLocalAddressType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMiUdpIpvxEndpointLocalAddress,
                                                  UINT4
                                                  u4FsMiUdpIpvxEndpointLocalPort,
                                                  INT4
                                                  i4FsMiUdpIpvxEndpointRemoteAddressType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMiUdpIpvxEndpointRemoteAddress,
                                                  UINT4
                                                  u4FsMiUdpIpvxEndpointRemotePort,
                                                  UINT4
                                                  u4FsMiUdpIpvxEndpointInstance)
{
#ifdef IP_WANTED
    INT1                i1Return = SNMP_FAILURE;

    if ((i4FsMiUdpIpvxContextId < (INT4) VCM_DEFAULT_CONTEXT)
        || (i4FsMiUdpIpvxContextId >= (INT4) SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist (i4FsMiUdpIpvxContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    UDP_DS_LOCK ();
    if (UdpIsValidCxtId ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_FAILURE)
    {
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    UDP_DS_UNLOCK ();

    i1Return =
        nmhValidateIndexInstanceUdpEndpointTable
        (i4FsMiUdpIpvxEndpointLocalAddressType,
         pFsMiUdpIpvxEndpointLocalAddress, u4FsMiUdpIpvxEndpointLocalPort,
         i4FsMiUdpIpvxEndpointRemoteAddressType,
         pFsMiUdpIpvxEndpointRemoteAddress, u4FsMiUdpIpvxEndpointRemotePort,
         u4FsMiUdpIpvxEndpointInstance);
    return i1Return;
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointLocalAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointLocalAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointLocalPort);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointRemoteAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointRemoteAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointRemotePort);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointInstance);
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMiUdpIpvxEndpointTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
                FsMiUdpIpvxEndpointLocalAddressType
                FsMiUdpIpvxEndpointLocalAddress
                FsMiUdpIpvxEndpointLocalPort
                FsMiUdpIpvxEndpointRemoteAddressType
                FsMiUdpIpvxEndpointRemoteAddress
                FsMiUdpIpvxEndpointRemotePort
                FsMiUdpIpvxEndpointInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMiUdpIpvxEndpointTable (INT4 *pi4FsMiUdpIpvxContextId,
                                          INT4
                                          *pi4FsMiUdpIpvxEndpointLocalAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMiUdpIpvxEndpointLocalAddress,
                                          UINT4
                                          *pu4FsMiUdpIpvxEndpointLocalPort,
                                          INT4
                                          *pi4FsMiUdpIpvxEndpointRemoteAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMiUdpIpvxEndpointRemoteAddress,
                                          UINT4
                                          *pu4FsMiUdpIpvxEndpointRemotePort,
                                          UINT4 *pu4FsMiUdpIpvxEndpointInstance)
{
#ifdef IP_WANTED
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4CxtId = VCM_INVALID_VC;
    UINT4               u4NextCxtId = VCM_INVALID_VC;
    UDP_DS_LOCK ();
    if (UdpGetFirstCxtId (&u4NextCxtId) == SNMP_FAILURE)
    {
        UDP_DS_UNLOCK ();
        return i1RetVal;
    }
    do
    {
        u4CxtId = u4NextCxtId;

        if (UdpSelectContext (u4CxtId) == SNMP_FAILURE)
        {
            UDP_DS_UNLOCK ();
            return i1RetVal;
        }
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext (u4CxtId) == SNMP_SUCCESS)
        {
#endif
            i1RetVal = (INT1) UdpMiGetFirstIndexEndpointTable
                (pi4FsMiUdpIpvxEndpointLocalAddressType,
                 pFsMiUdpIpvxEndpointLocalAddress,
                 pu4FsMiUdpIpvxEndpointLocalPort,
                 pi4FsMiUdpIpvxEndpointRemoteAddressType,
                 pFsMiUdpIpvxEndpointRemoteAddress,
                 pu4FsMiUdpIpvxEndpointRemotePort,
                 pu4FsMiUdpIpvxEndpointInstance);
#ifdef IP6_WANTED
            Ip6ReleaseContext ();
        }
        IP6_TASK_UNLOCK ();
#endif
        UdpReleaseContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4FsMiUdpIpvxContextId = (INT4) u4CxtId;
            UDP_DS_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    while (UdpGetNextCxtId (u4CxtId, &u4NextCxtId) != SNMP_FAILURE);
    UDP_DS_UNLOCK ();
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pi4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pi4FsMiUdpIpvxEndpointLocalAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointLocalAddress);
    UNUSED_PARAM (pu4FsMiUdpIpvxEndpointLocalPort);
    UNUSED_PARAM (pi4FsMiUdpIpvxEndpointRemoteAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointRemoteAddress);
    UNUSED_PARAM (pu4FsMiUdpIpvxEndpointRemotePort);
    UNUSED_PARAM (pu4FsMiUdpIpvxEndpointInstance);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMiUdpIpvxEndpointTable
 Input       :  The Indices
                FsMiUdpIpvxContextId
                nextFsMiUdpIpvxContextId
                FsMiUdpIpvxEndpointLocalAddressType
                nextFsMiUdpIpvxEndpointLocalAddressType
                FsMiUdpIpvxEndpointLocalAddress
                nextFsMiUdpIpvxEndpointLocalAddress
                FsMiUdpIpvxEndpointLocalPort
                nextFsMiUdpIpvxEndpointLocalPort
                FsMiUdpIpvxEndpointRemoteAddressType
                nextFsMiUdpIpvxEndpointRemoteAddressType
                FsMiUdpIpvxEndpointRemoteAddress
                nextFsMiUdpIpvxEndpointRemoteAddress
                FsMiUdpIpvxEndpointRemotePort
                nextFsMiUdpIpvxEndpointRemotePort
                FsMiUdpIpvxEndpointInstance
                nextFsMiUdpIpvxEndpointInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMiUdpIpvxEndpointTable (INT4 i4FsMiUdpIpvxContextId,
                                         INT4 *pi4NextFsMiUdpIpvxContextId,
                                         INT4
                                         i4FsMiUdpIpvxEndpointLocalAddressType,
                                         INT4
                                         *pi4NextFsMiUdpIpvxEndpointLocalAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMiUdpIpvxEndpointLocalAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMiUdpIpvxEndpointLocalAddress,
                                         UINT4 u4FsMiUdpIpvxEndpointLocalPort,
                                         UINT4
                                         *pu4NextFsMiUdpIpvxEndpointLocalPort,
                                         INT4
                                         i4FsMiUdpIpvxEndpointRemoteAddressType,
                                         INT4
                                         *pi4NextFsMiUdpIpvxEndpointRemoteAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMiUdpIpvxEndpointRemoteAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMiUdpIpvxEndpointRemoteAddress,
                                         UINT4 u4FsMiUdpIpvxEndpointRemotePort,
                                         UINT4
                                         *pu4NextFsMiUdpIpvxEndpointRemotePort,
                                         UINT4 u4FsMiUdpIpvxEndpointInstance,
                                         UINT4
                                         *pu4NextFsMiUdpIpvxEndpointInstance)
{
#ifdef IP_WANTED
    INT1                i1RetVal = SNMP_FAILURE;

    UDP_DS_LOCK ();
    if (UdpIsValidCxtId ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
    {
        if (UdpSelectContext (i4FsMiUdpIpvxContextId) == SNMP_FAILURE)
        {
            UDP_DS_UNLOCK ();
            return i1RetVal;
        }
#ifdef IP6_WANTED
        IP6_TASK_LOCK ();
        if (Ip6SelectContext ((UINT4) i4FsMiUdpIpvxContextId) == SNMP_SUCCESS)
        {
#endif
            i1RetVal = (INT1) UdpMiGetNextIndexEndpointTable
                (i4FsMiUdpIpvxEndpointLocalAddressType,
                 pi4NextFsMiUdpIpvxEndpointLocalAddressType,
                 pFsMiUdpIpvxEndpointLocalAddress,
                 pNextFsMiUdpIpvxEndpointLocalAddress,
                 u4FsMiUdpIpvxEndpointLocalPort,
                 pu4NextFsMiUdpIpvxEndpointLocalPort,
                 i4FsMiUdpIpvxEndpointRemoteAddressType,
                 pi4NextFsMiUdpIpvxEndpointRemoteAddressType,
                 pFsMiUdpIpvxEndpointRemoteAddress,
                 pNextFsMiUdpIpvxEndpointRemoteAddress,
                 u4FsMiUdpIpvxEndpointRemotePort,
                 pu4NextFsMiUdpIpvxEndpointRemotePort,
                 u4FsMiUdpIpvxEndpointInstance,
                 pu4NextFsMiUdpIpvxEndpointInstance);
#ifdef IP6_WANTED
            Ip6ReleaseContext ();
        }
        IP6_TASK_UNLOCK ();
#endif
        UdpReleaseContext ();

    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UdpGetNextCxtId ((UINT4) i4FsMiUdpIpvxContextId,
                                 (UINT4 *) pi4NextFsMiUdpIpvxContextId))
               == SNMP_SUCCESS)
        {
            if (UdpSelectContext (*pi4NextFsMiUdpIpvxContextId) == SNMP_FAILURE)
            {
                break;
            }
#ifdef IP6_WANTED
            IP6_TASK_LOCK ();
            if (Ip6SelectContext (PTR_TO_U4 (pi4NextFsMiUdpIpvxContextId)
                                  == SNMP_SUCCESS))
            {
#endif
                i1RetVal =
                    (INT1)
                    UdpMiGetFirstIndexEndpointTable
                    (&i4FsMiUdpIpvxEndpointLocalAddressType,
                     pFsMiUdpIpvxEndpointLocalAddress,
                     &u4FsMiUdpIpvxEndpointLocalPort,
                     &i4FsMiUdpIpvxEndpointRemoteAddressType,
                     pFsMiUdpIpvxEndpointRemoteAddress,
                     &u4FsMiUdpIpvxEndpointRemotePort,
                     &u4FsMiUdpIpvxEndpointInstance);
#ifdef IP6_WANTED
                Ip6ReleaseContext ();
            }
            IP6_TASK_UNLOCK ();
#endif
            UdpReleaseContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMiUdpIpvxContextId = *pi4NextFsMiUdpIpvxContextId;
        }
    }
    else
    {
        *pi4NextFsMiUdpIpvxContextId = i4FsMiUdpIpvxContextId;
    }
    UDP_DS_UNLOCK ();
    return i1RetVal;
#else
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (pi4NextFsMiUdpIpvxContextId);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointLocalAddressType);
    UNUSED_PARAM (pi4NextFsMiUdpIpvxEndpointLocalAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointLocalAddress);
    UNUSED_PARAM (pNextFsMiUdpIpvxEndpointLocalAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointLocalPort);
    UNUSED_PARAM (pu4NextFsMiUdpIpvxEndpointLocalPort);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointRemoteAddressType);
    UNUSED_PARAM (pi4NextFsMiUdpIpvxEndpointRemoteAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointRemoteAddress);
    UNUSED_PARAM (pNextFsMiUdpIpvxEndpointRemoteAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointRemotePort);
    UNUSED_PARAM (pu4NextFsMiUdpIpvxEndpointRemotePort);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointInstance);
    UNUSED_PARAM (pu4NextFsMiUdpIpvxEndpointInstance);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMiUdpIpvxEndpointProcess
 Input       :  The Indices
                FsMiUdpIpvxContextId
                FsMiUdpIpvxEndpointLocalAddressType
                FsMiUdpIpvxEndpointLocalAddress
                FsMiUdpIpvxEndpointLocalPort
                FsMiUdpIpvxEndpointRemoteAddressType
                FsMiUdpIpvxEndpointRemoteAddress
                FsMiUdpIpvxEndpointRemotePort
                FsMiUdpIpvxEndpointInstance

                The Object 
                retValFsMiUdpIpvxEndpointProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMiUdpIpvxEndpointProcess (INT4 i4FsMiUdpIpvxContextId,
                                  INT4 i4FsMiUdpIpvxEndpointLocalAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMiUdpIpvxEndpointLocalAddress,
                                  UINT4 u4FsMiUdpIpvxEndpointLocalPort,
                                  INT4 i4FsMiUdpIpvxEndpointRemoteAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMiUdpIpvxEndpointRemoteAddress,
                                  UINT4 u4FsMiUdpIpvxEndpointRemotePort,
                                  UINT4 u4FsMiUdpIpvxEndpointInstance,
                                  UINT4 *pu4RetValFsMiUdpIpvxEndpointProcess)
{
    UNUSED_PARAM (i4FsMiUdpIpvxContextId);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointLocalAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointLocalAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointLocalPort);
    UNUSED_PARAM (i4FsMiUdpIpvxEndpointRemoteAddressType);
    UNUSED_PARAM (pFsMiUdpIpvxEndpointRemoteAddress);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointRemotePort);
    UNUSED_PARAM (u4FsMiUdpIpvxEndpointInstance);
    UNUSED_PARAM (pu4RetValFsMiUdpIpvxEndpointProcess);
    return SNMP_SUCCESS;

}
