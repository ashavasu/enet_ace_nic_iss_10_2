# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmsudlw.h"
# include  "fsmsudwr.h"
# include  "fsmsuddb.h"

VOID
RegisterFSMSUD ()
{
    SNMPRegisterMibWithLock (&fsmsudOID, &fsmsudEntry, NULL, NULL,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmsudOID, (const UINT1 *) "fsmsudpipvx");
}

VOID
UnRegisterFSMSUD ()
{
    SNMPUnRegisterMib (&fsmsudOID, &fsmsudEntry);
    SNMPDelSysorEntry (&fsmsudOID, (const UINT1 *) "fsmsudpipvx");
}

INT4
FsMiUdpInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpNoPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpNoPorts (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInErrors (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpOutDatagrams (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpInNoCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInNoCksum (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpInIcmpErrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInIcmpErr (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpInErrCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInErrCksum (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpInBcastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpInBcast (&(pMultiData->u4_ULongValue)));
}

INT4
FsMiUdpHCInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpHCInDatagrams (&(pMultiData->u8_Counter64Value)));
}

INT4
FsMiUdpHCOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMiUdpHCOutDatagrams (&(pMultiData->u8_Counter64Value)));
}

INT4
GetNextIndexFsMIUdpStatTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIUdpStatTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIUdpStatTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMiUdpIpvxInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInDatagrams (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxNoPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxNoPorts (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxOutDatagrams (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxInNoCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInNoCksum (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxInIcmpErrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInIcmpErr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxInErrCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInErrCksum (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxInBcastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxInBcast (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMiUdpIpvxHCInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxHCInDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsMiUdpIpvxHCOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIUdpStatTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxHCOutDatagrams
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
GetNextIndexFsMiUdpEndpointTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMiUdpEndpointTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             &(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMiUdpEndpointTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             pFirstMultiIndex->pIndex[6].u4_ULongValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMiUdpEndpointProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMiUdpEndpointTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpEndpointProcess (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].pOctetStrValue,
                                          pMultiIndex->pIndex[5].u4_ULongValue,
                                          pMultiIndex->pIndex[6].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsMiUdpIpvxEndpointTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMiUdpIpvxEndpointTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue),
             &(pNextMultiIndex->pIndex[7].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMiUdpIpvxEndpointTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             pFirstMultiIndex->pIndex[6].u4_ULongValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue),
             pFirstMultiIndex->pIndex[7].u4_ULongValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMiUdpIpvxEndpointProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMiUdpIpvxEndpointTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMiUdpIpvxEndpointProcess
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}
