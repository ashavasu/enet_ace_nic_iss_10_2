/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdudplw.c,v 1.8 2010/08/02 06:56:01 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#ifndef __UDP_LW_
#define __UDP_LW_

# include  "lr.h"
# include  "ip.h"
# include  "fssnmp.h"
# include  "stdudplw.h"
# include  "udpipvxdefs.h"
# include  "stdudpwr.h"
#if defined IP_WANTED
# include  "udpipvxinc.h"
#endif
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED            /* FSIP */
# include "ip6inc.h"
#else
#include "ipvx.h"
#endif
# include "ipv6.h"
#endif
#ifdef LNXIP4_WANTED
#include "lnxiputl.h"
#endif
# include  "udputils.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUdpInDatagrams
 Input       :  The Indices

                The Object 
                retValUdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpInDatagrams (UINT4 *pu4RetValUdpInDatagrams)
{
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValUdpInDatagrams = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
        *pu4RetValUdpInDatagrams = pUdpStatEntry->u4InPkts;
    UDP_DS_UNLOCK ();
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pUdpStatEntry);
    if (LnxIpGetObject (LNX_UDP_IN_DATAGRAMS,
                        pu4RetValUdpInDatagrams) == SNMP_FAILURE)
    {
        *pu4RetValUdpInDatagrams = UDP_ZERO;
    }
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
        *pu4RetValUdpInDatagrams += pUdp6StatEntry->u4InDgrams;
    IP6_TASK_UNLOCK ();
#endif
#if (!defined (IP_WANTED)) && (!defined (LNXIP4_WANTED))
    UNUSED_PARAM (pUdpStatEntry);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUdpNoPorts
 Input       :  The Indices

                The Object 
                retValUdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpNoPorts (UINT4 *pu4RetValUdpNoPorts)
{
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValUdpNoPorts = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
        *pu4RetValUdpNoPorts = pUdpStatEntry->u4InErrNoport;
    UDP_DS_UNLOCK ();
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pUdpStatEntry);
    if (LnxIpGetObject (LNX_UDP_NO_PORTS, pu4RetValUdpNoPorts) == SNMP_FAILURE)
    {
        *pu4RetValUdpNoPorts = UDP_ZERO;
    }
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
        *pu4RetValUdpNoPorts += pUdp6StatEntry->u4NumPorts;
    IP6_TASK_UNLOCK ();
#endif
#if (!defined (IP_WANTED)) && (!defined (LNXIP4_WANTED))
    UNUSED_PARAM (pUdpStatEntry);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUdpInErrors
 Input       :  The Indices

                The Object 
                retValUdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpInErrors (UINT4 *pu4RetValUdpInErrors)
{
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValUdpInErrors = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
    {
        *pu4RetValUdpInErrors = pUdpStatEntry->u4InErrNoport +
            pUdpStatEntry->u4InErrCksum + pUdpStatEntry->u4InTotalErr;
    }
    UDP_DS_UNLOCK ();
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pUdpStatEntry);
    if (LnxIpGetObject (LNX_UDP_IN_ERRS, pu4RetValUdpInErrors) == SNMP_FAILURE)
    {
        *pu4RetValUdpInErrors = UDP_ZERO;
    }
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
        *pu4RetValUdpInErrors += pUdp6StatEntry->u4InErrs;
    IP6_TASK_UNLOCK ();
#endif
#if (!defined (IP_WANTED)) && (!defined (LNXIP4_WANTED))
    UNUSED_PARAM (pUdpStatEntry);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUdpOutDatagrams
 Input       :  The Indices

                The Object 
                retValUdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpOutDatagrams (UINT4 *pu4RetValUdpOutDatagrams)
{
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    *pu4RetValUdpOutDatagrams = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
        *pu4RetValUdpOutDatagrams = pUdpStatEntry->u4OutPkts;
    UDP_DS_UNLOCK ();
#endif

#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pUdpStatEntry);
    if (LnxIpGetObject (LNX_UDP_OUT_DATAGRAMS,
                        pu4RetValUdpOutDatagrams) == SNMP_FAILURE)
    {
        *pu4RetValUdpOutDatagrams = UDP_ZERO;
    }
#endif

#if defined IP_WANTED && defined (IP6_WANTED)
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
        *pu4RetValUdpOutDatagrams += pUdp6StatEntry->u4OutDgrams;
    IP6_TASK_UNLOCK ();
#endif
#if !(defined IP_WANTED) && defined (IP6_WANTED)
    UNUSED_PARAM (pUdp6StatEntry);
#endif
#else
    UNUSED_PARAM (pu4RetValUdpOutDatagrams);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUdpHCInDatagrams
 Input       :  The Indices

                The Object 
                retValUdpHCInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpHCInDatagrams (tSNMP_COUNTER64_TYPE * pu8RetValUdpHCInDatagrams)
{
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    pu8RetValUdpHCInDatagrams->lsn = UDP_ZERO;
    pu8RetValUdpHCInDatagrams->msn = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
    {
        pu8RetValUdpHCInDatagrams->lsn = pUdpStatEntry->u8HcInPkts.u4Lo;
        pu8RetValUdpHCInDatagrams->msn = pUdpStatEntry->u8HcInPkts.u4Hi;
    }
    UDP_DS_UNLOCK ();
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (pUdpStatEntry);
    UINT4               u8RetValUdpHCInData;
    if (LnxIpGetObject (LNX_UDP_IN_DATAGRAMS,
                        &u8RetValUdpHCInData) == SNMP_FAILURE)
    {
        pu8RetValUdpHCInDatagrams->lsn = UDP_ZERO;
        pu8RetValUdpHCInDatagrams->msn = UDP_ZERO;
    }
    pu8RetValUdpHCInDatagrams->lsn = u8RetValUdpHCInData;
    pu8RetValUdpHCInDatagrams->msn = LNX_ZERO;
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
    {
        if ((pUdp6StatEntry->u8HcInDgrams.u4Lo) >
            (pu8RetValUdpHCInDatagrams->lsn +
             pUdp6StatEntry->u8HcInDgrams.u4Lo))
        {
            pu8RetValUdpHCInDatagrams->msn += IPVX_ONE;
        }
        pu8RetValUdpHCInDatagrams->lsn += pUdp6StatEntry->u8HcInDgrams.u4Lo;
        pu8RetValUdpHCInDatagrams->msn += pUdp6StatEntry->u8HcInDgrams.u4Hi;
    }
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (pu8RetValUdpHCInDatagrams);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUdpHCOutDatagrams
 Input       :  The Indices

                The Object 
                retValUdpHCOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUdpHCOutDatagrams (tSNMP_COUNTER64_TYPE * pu8RetValUdpHCOutDatagrams)
{
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    t_UDP_STAT         *pUdpStatEntry = NULL;
#ifdef IP6_WANTED
    tIp6Udp6Stats      *pUdp6StatEntry = NULL;
#endif

    pu8RetValUdpHCOutDatagrams->lsn = UDP_ZERO;
    pu8RetValUdpHCOutDatagrams->msn = UDP_ZERO;
#ifdef IP_WANTED
    UDP_DS_LOCK ();
    pUdpStatEntry = UdpGetStatEntry ();
    if (pUdpStatEntry != NULL)
    {
        pu8RetValUdpHCOutDatagrams->lsn = pUdpStatEntry->u8HcOutPkts.u4Lo;
        pu8RetValUdpHCOutDatagrams->msn = pUdpStatEntry->u8HcOutPkts.u4Hi;
    }
    UDP_DS_UNLOCK ();
#endif
#ifdef LNXIP4_WANTED
    UINT4               u8RetValUdpHCOutData;
    UNUSED_PARAM (pUdpStatEntry);
    if (LnxIpGetObject (LNX_UDP_OUT_DATAGRAMS,
                        &u8RetValUdpHCOutData) == SNMP_FAILURE)
    {
        pu8RetValUdpHCOutDatagrams->lsn = UDP_ZERO;
        pu8RetValUdpHCOutDatagrams->msn = UDP_ZERO;
    }
    pu8RetValUdpHCOutDatagrams->lsn = u8RetValUdpHCOutData;
    pu8RetValUdpHCOutDatagrams->msn = LNX_ZERO;
#endif
#ifdef IP6_WANTED
    IP6_TASK_LOCK ();
    pUdp6StatEntry = Udp6GetStatEntry ();
    if (pUdp6StatEntry != NULL)
    {
        pu8RetValUdpHCOutDatagrams->lsn += pUdp6StatEntry->u8HcOutDgrams.u4Lo;
        pu8RetValUdpHCOutDatagrams->msn += pUdp6StatEntry->u8HcOutDgrams.u4Hi;

        if ((pUdp6StatEntry->u8HcOutDgrams.u4Lo) >
            (pu8RetValUdpHCOutDatagrams->lsn +
             pUdp6StatEntry->u8HcOutDgrams.u4Lo))
        {
            pu8RetValUdpHCOutDatagrams->msn += IPVX_ONE;
        }
        pu8RetValUdpHCOutDatagrams->lsn += pUdp6StatEntry->u8HcOutDgrams.u4Lo;
        pu8RetValUdpHCOutDatagrams->msn += pUdp6StatEntry->u8HcOutDgrams.u4Hi;
    }
    IP6_TASK_UNLOCK ();
#endif
#else
    UNUSED_PARAM (pu8RetValUdpHCOutDatagrams);
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : UdpEndpointTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUdpEndpointTable
 Input       :  The Indices
                UdpEndpointLocalAddressType
                UdpEndpointLocalAddress
                UdpEndpointLocalPort
                UdpEndpointRemoteAddressType
                UdpEndpointRemoteAddress
                UdpEndpointRemotePort
                UdpEndpointInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceUdpEndpointTable
    (INT4 i4UdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     UINT4 u4UdpEndpointLocalPort,
     INT4 i4UdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     UINT4 u4UdpEndpointRemotePort, UINT4 u4UdpEndpointInstance)
{
    /* This validation is common for both SI and MI
     * low level routines */

#if defined IP_WANTED && defined (IP6_WANTED)
    tUdp6Port          *pUdp6PortEntry = NULL;
    UINT1               u1Udp6CBMode = UDP_ZERO;
#endif
#ifdef IP_WANTED
    UINT4               u4LocalAddress = UDP_ZERO;
    UINT4               u4RemoteAddress = UDP_ZERO;
    t_UDP_CB            tUdp_cb;
    if (i4UdpEndpointLocalAddressType == UDP_ZERO)
    {
        i4UdpEndpointLocalAddressType = UDP_IPV4_ADDR;
        i4UdpEndpointRemoteAddressType = UDP_IPV4_ADDR;
    }
#endif
#if defined IP_WANTED && !defined (IP6_WANTED)
    UNUSED_PARAM (u4UdpEndpointInstance);
    if (i4UdpEndpointLocalAddressType == UDP_IPV4_ADDR &&
        i4UdpEndpointRemoteAddressType == UDP_IPV4_ADDR)
    {
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress, u4LocalAddress);
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress, u4RemoteAddress);
        UDP_DS_LOCK ();
        if (udp_get_ipv4_cb ((UINT2) u4UdpEndpointLocalPort,
                             u4LocalAddress,
                             (UINT2) u4UdpEndpointRemotePort,
                             u4RemoteAddress, &tUdp_cb) == SUCCESS)
        {
            UDP_DS_UNLOCK ();
            return SNMP_SUCCESS;
        }
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
#endif
#if defined IP_WANTED && defined (IP6_WANTED)

    if (i4UdpEndpointLocalAddressType == UDP_IPV4_ADDR &&
        i4UdpEndpointRemoteAddressType == UDP_IPV4_ADDR)
    {
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress, u4LocalAddress);
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress, u4RemoteAddress);
        UDP_DS_LOCK ();
        if (udp_get_ipv4_cb ((UINT2) u4UdpEndpointLocalPort,
                             u4LocalAddress,
                             (UINT2) u4UdpEndpointRemotePort,
                             u4RemoteAddress, &tUdp_cb) == SUCCESS)
        {
            UDP_DS_UNLOCK ();
            return SNMP_SUCCESS;
        }
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    else if (i4UdpEndpointLocalAddressType == UDP_IPV6_ADDR &&
             i4UdpEndpointRemoteAddressType == UDP_IPV6_ADDR)
    {
        UNUSED_PARAM (u4UdpEndpointRemotePort);
        UNUSED_PARAM (u4UdpEndpointInstance);
        IP6_TASK_LOCK ();
        if (UdpIpv6GetPort ((UINT2) u4UdpEndpointLocalPort,
                            (tIp6Addr *) (VOID *)
                            pUdpEndpointLocalAddress->pu1_OctetList,
                            (UINT2) u4UdpEndpointRemotePort,
                            (tIp6Addr *)
                            pUdpEndpointRemoteAddress,
                            &pUdp6PortEntry, &u1Udp6CBMode) == IP6_SUCCESS)
        {
            IP6_TASK_UNLOCK ();
            return SNMP_SUCCESS;
        }
        IP6_TASK_UNLOCK ();
        return SNMP_FAILURE;
    }
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (pUdpEndpointLocalAddress);
    UNUSED_PARAM (u4UdpEndpointLocalPort);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (pUdpEndpointRemoteAddress);
    UNUSED_PARAM (u4UdpEndpointRemotePort);
    UNUSED_PARAM (u4UdpEndpointInstance);
    return SNMP_SUCCESS;
#endif

#if !(defined IP_WANTED) && defined (IP6_WANTED)
    /* IPv6 Stack only Defined This block needs chnaged to be get the
     * Valid vlaues from the Udp6 Tables */
    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (pUdpEndpointLocalAddress);
    UNUSED_PARAM (u4UdpEndpointLocalPort);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (pUdpEndpointRemoteAddress);
    UNUSED_PARAM (u4UdpEndpointRemotePort);
    UNUSED_PARAM (u4UdpEndpointInstance);
    return SNMP_SUCCESS;
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexUdpEndpointTable
 Input       :  The Indices
                UdpEndpointLocalAddressType
                UdpEndpointLocalAddress
                UdpEndpointLocalPort
                UdpEndpointRemoteAddressType
                UdpEndpointRemoteAddress
                UdpEndpointRemotePort
                UdpEndpointInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexUdpEndpointTable
    (INT4 *pi4UdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     UINT4 *pu4UdpEndpointLocalPort,
     INT4 *pi4UdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     UINT4 *pu4UdpEndpointRemotePort, UINT4 *pu4UdpEndpointInstance)
{
    return (nmhGetNextIndexUdpEndpointTable
            (UDP_ZERO, pi4UdpEndpointLocalAddressType,
             UDP_ZERO, pUdpEndpointLocalAddress,
             UDP_ZERO, pu4UdpEndpointLocalPort,
             UDP_ZERO, pi4UdpEndpointRemoteAddressType,
             UDP_ZERO, pUdpEndpointRemoteAddress,
             UDP_ZERO, pu4UdpEndpointRemotePort,
             UDP_ZERO, pu4UdpEndpointInstance));

}

/****************************************************************************
 Function    :  nmhGetNextIndexUdpEndpointTable
 Input       :  The Indices
                UdpEndpointLocalAddressType
                nextUdpEndpointLocalAddressType
                UdpEndpointLocalAddress
                nextUdpEndpointLocalAddress
                UdpEndpointLocalPort
                nextUdpEndpointLocalPort
                UdpEndpointRemoteAddressType
                nextUdpEndpointRemoteAddressType
                UdpEndpointRemoteAddress
                nextUdpEndpointRemoteAddress
                UdpEndpointRemotePort
                nextUdpEndpointRemotePort
                UdpEndpointInstance
                nextUdpEndpointInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexUdpEndpointTable
    (INT4 i4UdpEndpointLocalAddressType,
     INT4 *pi4NextUdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointLocalAddress,
     UINT4 u4UdpEndpointLocalPort,
     UINT4 *pu4NextUdpEndpointLocalPort,
     INT4 i4UdpEndpointRemoteAddressType,
     INT4 *pi4NextUdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointRemoteAddress,
     UINT4 u4UdpEndpointRemotePort,
     UINT4 *pu4NextUdpEndpointRemotePort,
     UINT4 u4UdpEndpointInstance, UINT4 *pu4NextUdpEndpointInstance)
{
#ifdef LNXIP4_WANTED
    UINT4               u4LocalIp = UDP_ZERO;
    UINT4               u4RemoteIp = UDP_ZERO;
    tUdpEntry           udpEntry;
    tUdpEntry           nextUdpEntry;

    if (i4UdpEndpointLocalAddressType != LNX_ZERO)
    {
        pUdpEndpointLocalAddress->i4_Length = LNX_IPV4_LEN;
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress, u4LocalIp);
    }
    else
    {
        u4LocalIp = LNX_ZERO;
    }
    if (i4UdpEndpointRemoteAddressType != LNX_ZERO)
    {
        pUdpEndpointRemoteAddress->i4_Length = LNX_IPV4_LEN;
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress, u4RemoteIp);
    }
    else
    {
        u4RemoteIp = LNX_ZERO;
    }

    udpEntry.u4LocalIp = u4LocalIp;
    udpEntry.u2LocalPort = (UINT2) u4UdpEndpointLocalPort;
    udpEntry.i4LocalAddrType = i4UdpEndpointLocalAddressType;
    udpEntry.i4RemoteAddrType = i4UdpEndpointRemoteAddressType;
    udpEntry.u4RemoteIp = u4RemoteIp;
    udpEntry.u2Remoteport = (UINT2) u4UdpEndpointRemotePort;
    udpEntry.u4Instance = u4UdpEndpointInstance;

    if (UdpTableGetNextEntry (&udpEntry, &nextUdpEntry) == SNMP_SUCCESS)
    {
        *pi4NextUdpEndpointLocalAddressType = nextUdpEntry.i4LocalAddrType;
        UDP_INTEGER_TO_OCTETSTRING (nextUdpEntry.u4LocalIp,
                                    pNextUdpEndpointLocalAddress);
        *pu4NextUdpEndpointLocalPort = nextUdpEntry.u2LocalPort;
        *pi4NextUdpEndpointRemoteAddressType = nextUdpEntry.i4RemoteAddrType;
        UDP_INTEGER_TO_OCTETSTRING (nextUdpEntry.u4RemoteIp,
                                    pNextUdpEndpointRemoteAddress);
        *pu4NextUdpEndpointRemotePort = nextUdpEntry.u2Remoteport;
        *pu4NextUdpEndpointInstance = nextUdpEntry.u4Instance;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

#endif

#if defined IP_WANTED && !defined (IP6_WANTED)
    UINT2               u2LPort = UDP_ZERO;
    UINT2               u2NextLPort = UDP_ZERO;
    UINT2               u2RPort = UDP_ZERO;
    UINT2               u2NextRPort = UDP_ZERO;
    UINT4               u4LocalIp = UDP_ZERO;
    UINT4               u4RemoteIp = UDP_ZERO;
    UINT4               u4NextLocalIp = UDP_ZERO;
    UINT4               u4NextRemoteIp = UDP_ZERO;

    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (u4UdpEndpointInstance);
    if (i4UdpEndpointLocalAddressType == UDP_ZERO)
    {
        u4LocalIp = UDP_ZERO;
    }
    else
    {
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress, u4LocalIp);
    }
    if (i4UdpEndpointRemoteAddressType == UDP_ZERO)
    {
        u4RemoteIp = UDP_ZERO;
    }
    else
    {
        UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress, u4RemoteIp);
    }

    u2LPort = (UINT2) u4UdpEndpointLocalPort;
    u2RPort = (UINT2) u4UdpEndpointRemotePort;
    UDP_DS_LOCK ();
    if (udpGetNextIndexIpvxUdpTable (u4LocalIp, &u4NextLocalIp,
                                     u2LPort, &u2NextLPort,
                                     u4RemoteIp, &u4NextRemoteIp,
                                     u2RPort, &u2NextRPort) == FAILURE)
    {
        UDP_DS_UNLOCK ();
        return SNMP_FAILURE;
    }
    UDP_INTEGER_TO_OCTETSTRING (u4NextLocalIp, pNextUdpEndpointLocalAddress);
    *pu4NextUdpEndpointLocalPort = u2NextLPort;
    UDP_INTEGER_TO_OCTETSTRING (u4NextRemoteIp, pNextUdpEndpointRemoteAddress);
    *pu4NextUdpEndpointRemotePort = u2NextRPort;
    *pi4NextUdpEndpointLocalAddressType = UDP_IPV4_ADDR;
    *pi4NextUdpEndpointRemoteAddressType = UDP_IPV4_ADDR;
    *pu4NextUdpEndpointInstance = UDP_ONE;
    UDP_DS_UNLOCK ();
    return SNMP_SUCCESS;
#endif
#if defined IP_WANTED && defined (IP6_WANTED)
    INT4                i4Flag = UDP_ZERO;
    INT4                i4Count = UDP_ZERO;
    UINT2               u2LPort = UDP_ZERO;
    UINT2               u2NextLPort = UDP_ZERO;
    UINT2               u2RPort = UDP_ZERO;
    UINT2               u2NextRPort = UDP_ZERO;
    UINT4               u4LocalIp = UDP_ZERO;
    UINT4               u4RemoteIp = UDP_ZERO;
    UINT4               u4NextLocalIp = UDP_ZERO;
    UINT4               u4NextRemoteIp = UDP_ZERO;
    UINT4               i4RetVal = UDP_ONE;
    t_UDP_CB            tUdp_cb;
    UINT1               u1UdpCBMode = UDP_ZERO;

    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (u4UdpEndpointInstance);

    /*Check whether Local and Remote IP address type are zero
     * if zero,set them to IPV4 address type and set the flag*/
    if (i4UdpEndpointLocalAddressType == UDP_ZERO ||
        i4UdpEndpointRemoteAddressType == UDP_ZERO)
    {
        i4UdpEndpointLocalAddressType = UDP_IPV4_ADDR;
        i4UdpEndpointRemoteAddressType = UDP_IPV4_ADDR;
        i4Flag = UDP_ONE;
    }
    if (i4UdpEndpointLocalAddressType == UDP_IPV4_ADDR ||
        i4UdpEndpointRemoteAddressType == UDP_IPV4_ADDR)
    {
        /*Check whether the entry is first one,
         * if true,set Local and Remote IP address to zero*/
        if (i4Flag != UDP_ONE)
        {
            UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress,
                                        u4NextLocalIp);
            UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress,
                                        u4NextRemoteIp);
        }
        else
        {
            u4NextLocalIp = UDP_ZERO;
            u4NextRemoteIp = UDP_ZERO;
        }

        u2NextLPort = (UINT2) u4UdpEndpointLocalPort;
        u2NextRPort = (UINT2) u4UdpEndpointRemotePort;

        /*This loop scan for IPV4 and IPV6 connection having 
         * Local IP as zero and same local port*/
        UDP_DS_LOCK ();
        IP6_TASK_LOCK ();
        do
        {
            u4LocalIp = u4NextLocalIp;
            u2LPort = u2NextLPort;
            u4RemoteIp = u4NextRemoteIp;
            u2RPort = u2NextRPort;

            /*Get the first entry from the UDP IPV4 table */
            if (udpGetNextIndexIpvxUdpTable (u4LocalIp,
                                             &u4NextLocalIp,
                                             u2LPort,
                                             &u2NextLPort,
                                             u4RemoteIp,
                                             &u4NextRemoteIp,
                                             u2RPort, &u2NextRPort) == FAILURE)
            {
                break;
            }
            i4Count++;
            /*Check whether a IPV6 entry exists with Local IP as zero 
             * and Local Port same as IPV4 entries local port
             * if present,exit from the loop
             * else, continue*/
            if (u4NextLocalIp == UDP_ZERO)
            {
                if (UdpGetMatchingIPv6Entry (u2NextLPort) == SNMP_SUCCESS)
                {
                    i4RetVal = UDP_ZERO;
                    break;
                }
            }
            else
            {
                continue;
            }
        }
        while (i4Count < UDP_CB_TAB_SIZE);
        UDP_DS_UNLOCK ();
        IP6_TASK_UNLOCK ();

        if (i4RetVal == UDP_ZERO)
        {
            *pi4NextUdpEndpointLocalAddressType = UDP_ZERO;
            *pi4NextUdpEndpointRemoteAddressType = UDP_ZERO;
            *pu4NextUdpEndpointLocalPort = u2NextLPort;
            *pu4NextUdpEndpointRemotePort = u2NextRPort;
            UDP_INTEGER_TO_OCTETSTRING (u4NextLocalIp,
                                        pNextUdpEndpointLocalAddress);
            UDP_INTEGER_TO_OCTETSTRING (u4NextRemoteIp,
                                        pNextUdpEndpointRemoteAddress);
            *pu4NextUdpEndpointInstance = UDP_ONE;
            return SNMP_SUCCESS;
        }
    }

    u2NextLPort = (UINT2) u4UdpEndpointLocalPort;
    u2NextRPort = (UINT2) u4UdpEndpointRemotePort;

    if (i4UdpEndpointLocalAddressType == UDP_IPV4_ADDR ||
        i4UdpEndpointRemoteAddressType == UDP_IPV4_ADDR)
    {
        if (i4Flag != UDP_ONE)
        {
            UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointLocalAddress,
                                        u4NextLocalIp);
            UDP_OCTETSTRING_TO_INTEGER (pUdpEndpointRemoteAddress,
                                        u4NextRemoteIp);
        }
        else
        {
            u4NextLocalIp = UDP_ZERO;
            u4NextRemoteIp = UDP_ZERO;
        }
    }
    else
    {
        *pu4NextUdpEndpointLocalPort = u2LPort;
        pNextUdpEndpointLocalAddress = pUdpEndpointLocalAddress;
        *pu4NextUdpEndpointRemotePort = u2RPort;
        pNextUdpEndpointRemoteAddress = pUdpEndpointRemoteAddress;

    }
    /*In this loop we will scan the IPV4 and IPV6 tables inorder to
     * retrive the entries which dont have same local port and local IP 
     * address (i.e Local IP address as zero).*/
    UDP_DS_LOCK ();
    IP6_TASK_LOCK ();
    do
    {
        /*Here we will sacn the tables based on the address types.
         * If adress type is IPV4 we will get the IPV4 entries check
         * whether IPV6 entry exists with same port and Local IP
         * if exists,skip the entry
         * else,return the indices*/
        if (i4UdpEndpointLocalAddressType == UDP_IPV4_ADDR ||
            i4UdpEndpointRemoteAddressType == UDP_IPV4_ADDR)
        {
            u4LocalIp = u4NextLocalIp;
            u2LPort = u2NextLPort;
            u4RemoteIp = u4NextRemoteIp;
            u2RPort = u2NextRPort;

            /*Get the entry from the UDP IPV4 table
             *If routine fails get the entries from UDP6 table*/
            if (udpGetNextIndexIpvxUdpTable (u4LocalIp,
                                             &u4NextLocalIp,
                                             u2LPort,
                                             &u2NextLPort,
                                             u4RemoteIp,
                                             &u4NextRemoteIp,
                                             u2RPort, &u2NextRPort) != FAILURE)

            {
                if (u4NextLocalIp == UDP_ZERO)
                {
                    /*Check for matching IPV6 entry */
                    if (UdpGetMatchingIPv6Entry (u2NextLPort) == SNMP_SUCCESS)
                    {
                        continue;
                    }
                    else
                    {
                        *pi4NextUdpEndpointLocalAddressType = UDP_IPV4_ADDR;
                        *pi4NextUdpEndpointRemoteAddressType = UDP_IPV4_ADDR;
                        *pu4NextUdpEndpointLocalPort = u2NextLPort;
                        *pu4NextUdpEndpointRemotePort = u2NextRPort;
                        UDP_INTEGER_TO_OCTETSTRING (u4NextLocalIp,
                                                    pNextUdpEndpointLocalAddress);
                        UDP_INTEGER_TO_OCTETSTRING (u4NextRemoteIp,
                                                    pNextUdpEndpointRemoteAddress);
                        *pu4NextUdpEndpointInstance = UDP_ONE;
                        UDP_DS_UNLOCK ();
                        IP6_TASK_UNLOCK ();
                        return SNMP_SUCCESS;

                    }
                }
                else
                {
                    *pi4NextUdpEndpointLocalAddressType = UDP_IPV4_ADDR;
                    *pi4NextUdpEndpointRemoteAddressType = UDP_IPV4_ADDR;
                    *pu4NextUdpEndpointLocalPort = u2NextLPort;
                    *pu4NextUdpEndpointRemotePort = u2NextRPort;
                    UDP_INTEGER_TO_OCTETSTRING (u4NextLocalIp,
                                                pNextUdpEndpointLocalAddress);
                    UDP_INTEGER_TO_OCTETSTRING (u4NextRemoteIp,
                                                pNextUdpEndpointRemoteAddress);
                    *pu4NextUdpEndpointInstance = UDP_ONE;
                    UDP_DS_UNLOCK ();
                    IP6_TASK_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                do
                {
                    u2LPort = *((UINT2 *) pu4NextUdpEndpointLocalPort);
                    pUdpEndpointLocalAddress = pNextUdpEndpointLocalAddress;
                    u2RPort = *((UINT2 *) pu4NextUdpEndpointRemotePort);
                    pUdpEndpointRemoteAddress = pNextUdpEndpointRemoteAddress;

                    if (udp6GetNextIndexIpvxUdp6Table
                        (u2LPort,
                         (UINT2 *) pu4NextUdpEndpointLocalPort,
                         pUdpEndpointLocalAddress,
                         pNextUdpEndpointLocalAddress,
                         u2RPort,
                         (UINT2 *) pu4NextUdpEndpointRemotePort,
                         pUdpEndpointRemoteAddress,
                         pNextUdpEndpointRemoteAddress) == SNMP_FAILURE)
                    {
                        UDP_DS_UNLOCK ();
                        IP6_TASK_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                    if (UdpCheckForZeroIPv6Add
                        (*(tIp6Addr *) (VOID *) pNextUdpEndpointLocalAddress->
                         pu1_OctetList) == UDP_ZERO)
                    {
                        if (udp_get_cb (*(UINT2 *) pu4NextUdpEndpointLocalPort,
                                        UDP_ZERO, &tUdp_cb,
                                        &u1UdpCBMode) == SUCCESS)
                        {
                            continue;
                        }
                    }
                    *pi4NextUdpEndpointLocalAddressType = UDP_IPV6_ADDR;
                    *pi4NextUdpEndpointRemoteAddressType = UDP_IPV6_ADDR;
                    *pu4NextUdpEndpointInstance = UDP_ONE;
                    UDP_DS_UNLOCK ();
                    IP6_TASK_UNLOCK ();
                    return SNMP_SUCCESS;
                }
                while (UDP_ONE);
            }
        }
        /* If adress type is IPV6 we will get the IPV6 entries check
         * whether IPV4 entry exists with same port and Local IP
         * if exists,skip the entry
         * else,return the indices*/
        else
        {
            do
            {
                u2LPort = *((UINT2 *) pu4NextUdpEndpointLocalPort);
                pUdpEndpointLocalAddress = pNextUdpEndpointLocalAddress;
                u2RPort = *((UINT2 *) pu4NextUdpEndpointRemotePort);
                pUdpEndpointRemoteAddress = pNextUdpEndpointRemoteAddress;

                if (udp6GetNextIndexIpvxUdp6Table
                    (u2LPort,
                     (UINT2 *) pu4NextUdpEndpointLocalPort,
                     pUdpEndpointLocalAddress,
                     pNextUdpEndpointLocalAddress,
                     u2RPort,
                     (UINT2 *) pu4NextUdpEndpointRemotePort,
                     pUdpEndpointRemoteAddress,
                     pNextUdpEndpointRemoteAddress) == SNMP_FAILURE)
                {
                    UDP_DS_UNLOCK ();
                    IP6_TASK_UNLOCK ();
                    return SNMP_FAILURE;
                }
                if (UdpCheckForZeroIPv6Add
                    (*(tIp6Addr *) (VOID *)
                     pNextUdpEndpointLocalAddress->pu1_OctetList) == UDP_ZERO)
                {
                    if (udp_get_cb (*(UINT2 *) pu4NextUdpEndpointLocalPort,
                                    UDP_ZERO, &tUdp_cb,
                                    &u1UdpCBMode) == SUCCESS)
                    {
                        continue;
                    }
                }
                *pi4NextUdpEndpointLocalAddressType = UDP_IPV6_ADDR;
                *pi4NextUdpEndpointRemoteAddressType = UDP_IPV6_ADDR;
                *pu4NextUdpEndpointInstance = UDP_ONE;
                UDP_DS_UNLOCK ();
                IP6_TASK_UNLOCK ();
                return SNMP_SUCCESS;
            }
            while (UDP_ONE);
        }
    }
    while (UDP_ONE);
#endif

#if !(defined IP_WANTED) && defined (IP6_WANTED)
    /* IPv6 Stack only Defined This block needs chnaged to be get the
     * Valid vlaues from the Udp6 Tables */
    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (pi4NextUdpEndpointLocalAddressType);
    UNUSED_PARAM (pUdpEndpointLocalAddress);
    UNUSED_PARAM (pNextUdpEndpointLocalAddress);
    UNUSED_PARAM (u4UdpEndpointLocalPort);
    UNUSED_PARAM (pu4NextUdpEndpointLocalPort);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (pi4NextUdpEndpointRemoteAddressType);
    UNUSED_PARAM (pUdpEndpointRemoteAddress);
    UNUSED_PARAM (pNextUdpEndpointRemoteAddress);
    UNUSED_PARAM (u4UdpEndpointRemotePort);
    UNUSED_PARAM (pu4NextUdpEndpointRemotePort);
    UNUSED_PARAM (u4UdpEndpointInstance);
    UNUSED_PARAM (pu4NextUdpEndpointInstance);
    return SNMP_FAILURE;
#endif

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUdpEndpointProcess
 Input       :  The Indices
                UdpEndpointLocalAddressType
                UdpEndpointLocalAddress
                UdpEndpointLocalPort
                UdpEndpointRemoteAddressType
                UdpEndpointRemoteAddress
                UdpEndpointRemotePort
                UdpEndpointInstance

                The Object 
                retValUdpEndpointProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetUdpEndpointProcess
    (INT4 i4UdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     UINT4 u4UdpEndpointLocalPort,
     INT4 i4UdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     UINT4 u4UdpEndpointRemotePort,
     UINT4 u4UdpEndpointInstance, UINT4 *pu4RetValUdpEndpointProcess)
{
    UNUSED_PARAM (i4UdpEndpointLocalAddressType);
    UNUSED_PARAM (pUdpEndpointLocalAddress);
    UNUSED_PARAM (u4UdpEndpointLocalPort);
    UNUSED_PARAM (i4UdpEndpointRemoteAddressType);
    UNUSED_PARAM (pUdpEndpointRemoteAddress);
    UNUSED_PARAM (u4UdpEndpointRemotePort);
    UNUSED_PARAM (u4UdpEndpointInstance);
    UNUSED_PARAM (pu4RetValUdpEndpointProcess);
    return SNMP_SUCCESS;
}
#endif
