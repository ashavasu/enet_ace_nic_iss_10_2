 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udpcli.c,v 1.11 2014/02/24 11:26:37 siva Exp $
 *
 * Description: This file contains the CLI routine for UDP
 *
 *******************************************************************/

#ifndef __UDPCLI_C__
#define __UDPCLI_C__

# include "udpcli.h"
# include "udpipvxdefs.h"
# include "cfa.h"
# include "vcm.h"
# include "ip.h"
# include "ipvx.h"
# include "stdudplw.h"
#include "ip6util.h"
# include  "fssnmp.h"
# include  "udputils.h"
# include  "fsmsudlw.h"
/* 
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : udpcli.c                                        |
 * |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                      |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : TE                                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    : Linux (Portable)                                 |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for CLI TE commands              | * |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

/*Function prototypes defined in this file*/
INT4                UdpShowConnections (tCliHandle CliHandle, UINT4 u4VcId);
INT4                UdpShowStats (tCliHandle CliHandle, UINT4 u4VcId);
INT4                UdpCliGetCxtIdFromCxtName (tCliHandle CliHandle,
                                               UINT1 *pu1UdpCxtName,
                                               UINT4 *pu4VcId);

INT1
cli_process_udp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4VcId = VCM_INVALID_VC;
    UINT1              *args[CLI_MAX_ARGS];
    UINT1              *pu1UdpCxtName = NULL;
    INT1                argno = UDP_ZERO;
    INT4                i4RetStatus = UDP_ZERO;

    UNUSED_PARAM (args);

    va_start (ap, u4Command);

    /* third argument is usually an InterfaceName/Index, but unused here */
    va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1UdpCxtName = va_arg (ap, UINT1 *);
    if (pu1UdpCxtName != NULL)
    {
        i4RetStatus = UdpCliGetCxtIdFromCxtName (CliHandle,
                                                 pu1UdpCxtName, &u4VcId);
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
        va_end (ap);
        return CLI_FAILURE;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 5 arguements at the max. This is because igmp commands do not
     * take more than 5 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == UDP_MAX_ARGS)
            break;
    }

    va_end (ap);
    CLI_SET_ERR (UDP_ZERO);
    switch (u4Command)
    {
        case UDP_CLI_SHOW_STATS:
            i4RetStatus = UdpShowStats (CliHandle, u4VcId);
            break;

        case UDP_CLI_SHOW_CONN:
            i4RetStatus = UdpShowConnections (CliHandle, u4VcId);
            break;

        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            return CLI_ERROR;
    }

    CLI_SET_CMD_STATUS (i4RetStatus);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  UdpShowStats
 Description :  Displays UDP stats
 Input       :  None
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
UdpShowStats (tCliHandle CliHandle, UINT4 u4VcId)
{
    UINT1               au1Str[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT4               u4UdpInDatagrams = UDP_ZERO;
    UINT4               u4UdpNoPorts = UDP_ZERO;
    UINT4               u4UdpInErrors = UDP_ZERO;
    UINT4               u4UdpOutDatagrams = UDP_ZERO;
    UINT4               u4UdpInNoCksum = UDP_ZERO;
    UINT4               u4UdpInIcmpErr = UDP_ZERO;
    UINT4               u4UdpInErrCksum = UDP_ZERO;
    UINT4               u4UdpInBcast = UDP_ZERO;
    UINT4               u4ShowAllCxt = UDP_FALSE;
    UINT4               u4PrevVcId = VCM_INVALID_VC;
    UINT4               u4FsMiUdpInDatagrams = UDP_ZERO;
    UINT4               u4FsMiUdpInErrors = UDP_ZERO;
    UINT4               u4FsMiUdpOutDatagrams = UDP_ZERO;
    UINT4               u4FsMiUdpInNoCksum = UDP_ZERO;
    UINT4               u4FsMiUdpInIcmpErr = UDP_ZERO;
    UINT4               u4FsMiUdpInErrCksum = UDP_ZERO;
    UINT4               u4FsMiUdpInBcast = UDP_ZERO;
    UINT4               u4FsMiUdpNoPorts = UDP_ZERO;
    FS_UINT8            u8UdpHCInDatagrams;
    FS_UINT8            u8UdpHCOutDatagrams;
    FS_UINT8            u8FsMiUdpHCInDatagrams;
    FS_UINT8            u8FsMiUdpHCOutDatagrams;
    tSNMP_COUNTER64_TYPE RetVal64HCInSegs;
    tSNMP_COUNTER64_TYPE RetVal64HCOutSegs;
    tSNMP_COUNTER64_TYPE RetValFsMiUdpHCInDatagrams;
    tSNMP_COUNTER64_TYPE RetValFsMiUdpHCOutDatagrams;

    FSAP_U8_CLR (&u8UdpHCInDatagrams);
    FSAP_U8_CLR (&u8UdpHCOutDatagrams);
    FSAP_U8_CLR (&u8FsMiUdpHCInDatagrams);
    FSAP_U8_CLR (&u8FsMiUdpHCOutDatagrams);
    MEMSET (&RetVal64HCInSegs, UDP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetVal64HCOutSegs, UDP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValFsMiUdpHCInDatagrams, UDP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValFsMiUdpHCOutDatagrams, UDP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (au1Str, UDP_ZERO, CFA_CLI_U8_STR_LENGTH);

    /*Retrieving Global scalar data */
    nmhGetFsMiUdpInDatagrams (&u4UdpInDatagrams);
    nmhGetFsMiUdpNoPorts (&u4UdpNoPorts);
    nmhGetFsMiUdpInErrors (&u4UdpInErrors);
    nmhGetFsMiUdpOutDatagrams (&u4UdpOutDatagrams);
    nmhGetFsMiUdpInNoCksum (&u4UdpInNoCksum);
    nmhGetFsMiUdpInIcmpErr (&u4UdpInIcmpErr);
    nmhGetFsMiUdpInErrCksum (&u4UdpInErrCksum);
    nmhGetFsMiUdpInBcast (&u4UdpInBcast);
    nmhGetFsMiUdpHCInDatagrams (&RetVal64HCInSegs);
    nmhGetFsMiUdpHCOutDatagrams (&RetVal64HCOutSegs);

    u8UdpHCInDatagrams.u4Hi = RetVal64HCInSegs.msn;
    u8UdpHCInDatagrams.u4Lo = RetVal64HCInSegs.lsn;
    u8UdpHCOutDatagrams.u4Hi = RetVal64HCOutSegs.msn;
    u8UdpHCOutDatagrams.u4Lo = RetVal64HCOutSegs.lsn;

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "\n Global UDP Statistics\r\n");
    CliPrintf (CliHandle, " =========================\r\n\n");
    CliPrintf (CliHandle, " InDatagrams             : %u\r\n",
               u4UdpInDatagrams);
    CliPrintf (CliHandle, " OutDatagrams            : %u\r\n",
               u4UdpOutDatagrams);
    FSAP_U8_2STR (&u8UdpHCInDatagrams, (CHR1 *) au1Str);
    CliPrintf (CliHandle, " HC InDatagrams          : %s\r\n", au1Str);
    MEMSET (au1Str, UDP_ZERO, CFA_CLI_U8_STR_LENGTH);
    FSAP_U8_2STR (&u8UdpHCOutDatagrams, (CHR1 *) au1Str);
    CliPrintf (CliHandle, " HC OutDatagrams         : %s\r\n", au1Str);

    CliPrintf (CliHandle, " UDP No Ports            : %u\r\n", u4UdpNoPorts);
    CliPrintf (CliHandle, " UDP In Errors           : %u\r\n", u4UdpInErrors);
    CliPrintf (CliHandle, " UDP with no Checksum    : %u\r\n", u4UdpInNoCksum);
    CliPrintf (CliHandle, " No. ICMP error packets  : %u\r\n", u4UdpInIcmpErr);
    CliPrintf (CliHandle, " UDP with wrong Checksum : %u\r\n", u4UdpInErrCksum);
    CliPrintf (CliHandle, " UDP In Broadcast Mode   : %u\r\n", u4UdpInBcast);
    CliPrintf (CliHandle, "\n");

    /*Retrieving context specific data */
    if (u4VcId == VCM_INVALID_VC)
    {
        if (UdpGetFirstCxtId (&u4VcId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4ShowAllCxt = UDP_TRUE;
    }

    do
    {
        u4PrevVcId = u4VcId;
        (VOID) nmhGetFsMiUdpIpvxInDatagrams (u4VcId, &u4FsMiUdpInDatagrams);
        (VOID) nmhGetFsMiUdpIpvxNoPorts (u4VcId, &u4FsMiUdpNoPorts);
        (VOID) nmhGetFsMiUdpIpvxInErrors (u4VcId, &u4FsMiUdpInErrors);
        (VOID) nmhGetFsMiUdpIpvxOutDatagrams (u4VcId, &u4FsMiUdpOutDatagrams);
        (VOID) nmhGetFsMiUdpIpvxInNoCksum (u4VcId, &u4FsMiUdpInNoCksum);
        (VOID) nmhGetFsMiUdpIpvxInIcmpErr (u4VcId, &u4FsMiUdpInIcmpErr);
        (VOID) nmhGetFsMiUdpIpvxInErrCksum (u4VcId, &u4FsMiUdpInErrCksum);
        (VOID) nmhGetFsMiUdpIpvxInBcast (u4VcId, &u4FsMiUdpInBcast);
        (VOID) nmhGetFsMiUdpIpvxHCInDatagrams (u4VcId,
                                               &RetValFsMiUdpHCInDatagrams);
        (VOID) nmhGetFsMiUdpIpvxHCOutDatagrams (u4VcId,
                                                &RetValFsMiUdpHCOutDatagrams);

        u8FsMiUdpHCInDatagrams.u4Hi = RetValFsMiUdpHCInDatagrams.msn;
        u8FsMiUdpHCInDatagrams.u4Lo = RetValFsMiUdpHCInDatagrams.lsn;
        u8FsMiUdpHCOutDatagrams.u4Hi = RetValFsMiUdpHCOutDatagrams.msn;
        u8FsMiUdpHCOutDatagrams.u4Lo = RetValFsMiUdpHCOutDatagrams.lsn;

        /* To display the context specific UDP socket statistics only
         * incase of it has any non-zero values */
        if ((u4FsMiUdpInDatagrams != 0) || (u4FsMiUdpNoPorts != 0)
            || (u4FsMiUdpInErrors != 0) || (u4FsMiUdpOutDatagrams != 0)
            || (u4FsMiUdpInNoCksum != 0) || (u4FsMiUdpInIcmpErr != 0)
            || (u4FsMiUdpInBcast != 0) || (u4FsMiUdpInErrCksum != 0)
            || (u8FsMiUdpHCInDatagrams.u4Hi != 0)
            || (u8FsMiUdpHCInDatagrams.u4Lo != 0)
            || (u8FsMiUdpHCOutDatagrams.u4Hi != 0)
            || (u8FsMiUdpHCOutDatagrams.u4Lo != 0))
        {
            VcmGetAliasName (u4VcId, au1ContextName);
            CliPrintf (CliHandle, "\nUDP Statistics for VRF: %s\r\n",
                       au1ContextName);
            CliPrintf (CliHandle, "=======================\n");
            CliPrintf (CliHandle, " InDatagrams             :   %u\r\n",
                       u4FsMiUdpInDatagrams);
            CliPrintf (CliHandle, " OutDatagrams            :   %u\r\n",
                       u4FsMiUdpOutDatagrams);
            FSAP_U8_2STR (&u8FsMiUdpHCInDatagrams, (CHR1 *) au1Str);
            CliPrintf (CliHandle, " HC InDatagrams          :  %s\r\n", au1Str);
            MEMSET (au1Str, UDP_ZERO, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_2STR (&u8FsMiUdpHCOutDatagrams, (CHR1 *) au1Str);
            CliPrintf (CliHandle, " HC OutDatagrams         : %s\r\n", au1Str);

            CliPrintf (CliHandle, " UDP No Ports            : %u\r\n",
                       u4FsMiUdpNoPorts);
            CliPrintf (CliHandle, " UDP In Errors           : %u\r\n",
                       u4FsMiUdpInErrors);
            CliPrintf (CliHandle, " UDP with no Checksum    : %u\r\n",
                       u4FsMiUdpInNoCksum);
            CliPrintf (CliHandle, " No. ICMP error packets  : %u\r\n",
                       u4FsMiUdpInIcmpErr);
            CliPrintf (CliHandle, " UDP with wrong Checksum : %u\r\n",
                       u4FsMiUdpInErrCksum);
            CliPrintf (CliHandle, " UDP In Broadcast Mode   : %u\r\n",
                       u4FsMiUdpInBcast);

            CliPrintf (CliHandle, "\n");
            CliPrintf (CliHandle, "\n");
        }
    }
    while ((u4ShowAllCxt == UDP_TRUE) &&
           (UdpGetNextCxtId (u4PrevVcId, &u4VcId) != SNMP_FAILURE));
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  UdpShowConnections
 Description :  Displays UDP Connections
 Input       :  None
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
UdpShowConnections (tCliHandle CliHandle, UINT4 u4L3ContextId)
{
    CHR1               *pu1String = NULL;
    INT1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4UdpLocalAddressType = UDP_ZERO;
    INT4                i4UdpRemoteAddressType = UDP_ZERO;
    INT4                i4UdpNextLocalAddressType = UDP_ZERO;
    INT4                i4UdpNextRemoteAddressType = UDP_ZERO;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1LocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextLocalIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRemoteIp[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4UdpNextRemotePort = UDP_ZERO;
    UINT4               u4UdpInstance = UDP_ZERO;
    UINT4               u4UdpRemotePort = UDP_ZERO;
    UINT4               u4UdpNextLocalPort = UDP_ZERO;
    UINT4               u4LocalIp = UDP_ZERO;
    UINT4               u4RemoteIp = UDP_ZERO;
    UINT4               u1IsShow = UDP_ONE;
    UINT4               u4UdpNextInstance = UDP_ZERO;
    UINT4               u4UdpLocalPort = UDP_ZERO;
    UINT4               u4ShowAllCxt = UDP_FALSE;
    UINT4               u4VcId = VCM_INVALID_VC;
    INT4                i4NextVcId = (INT4) VCM_INVALID_VC;
    tSNMP_OCTET_STRING_TYPE UdpLocalAddress;
    tSNMP_OCTET_STRING_TYPE UdpRemoteAddress;
    tSNMP_OCTET_STRING_TYPE UdpNextLocalAddress;
    tSNMP_OCTET_STRING_TYPE UdpNextRemoteAddress;
    UINT1               u1Len = UDP_ZERO;

    MEMSET (au1LocalIp, UDP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RemoteIp, UDP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextLocalIp, UDP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextRemoteIp, UDP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1Ip6Addr, UDP_ZERO, IPVX_MAX_INET_ADDR_LEN);

    i4NextVcId = (INT4) u4L3ContextId;

    UdpLocalAddress.pu1_OctetList = au1LocalIp;
    UdpLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpRemoteAddress.pu1_OctetList = au1RemoteIp;
    UdpRemoteAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpNextLocalAddress.pu1_OctetList = au1NextLocalIp;
    UdpNextLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpNextRemoteAddress.pu1_OctetList = au1NextRemoteIp;
    UdpNextRemoteAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    /*Retrieving Global data */
    if (nmhGetFirstIndexFsMiUdpEndpointTable (&i4UdpNextLocalAddressType,
                                              &UdpNextLocalAddress,
                                              &u4UdpNextLocalPort,
                                              &i4UdpNextRemoteAddressType,
                                              &UdpNextRemoteAddress,
                                              &u4UdpNextRemotePort,
                                              &u4UdpNextInstance) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n Global UDP Connections\r\n");
    CliPrintf (CliHandle, " ========================\r\n\n");
    do
    {
        if (i4UdpNextLocalAddressType == UDP_IPV4_ADDR)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv4\r\n");
            MEMCPY ((UINT1 *) &u4LocalIp,
                    UdpNextLocalAddress.pu1_OctetList,
                    UdpNextLocalAddress.i4_Length);
            u4LocalIp = OSIX_NTOHL (u4LocalIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocalIp);
            CliPrintf (CliHandle, "Local IP               : %s\r\n", pu1String);
        }
        else if (i4UdpNextLocalAddressType == UDP_IPV6_ADDR)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv6\r\n");
            MEMCPY (au1Ip6Addr, UdpNextLocalAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Local IP               : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        else if (i4UdpNextLocalAddressType == UDP_ZERO)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : 0\r\n");
            CliPrintf (CliHandle, "Local IP               : 0.0.0.0\r\n");
        }
        CliPrintf (CliHandle, "Local Port             : %d\r\n",
                   u4UdpNextLocalPort);

        if (i4UdpNextRemoteAddressType == UDP_IPV4_ADDR)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv4\r\n");
            MEMCPY ((UINT1 *) &u4RemoteIp,
                    UdpNextRemoteAddress.pu1_OctetList,
                    UdpNextRemoteAddress.i4_Length);
            u4RemoteIp = OSIX_NTOHL (u4RemoteIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RemoteIp);
            CliPrintf (CliHandle, "Remote IP              : %s\r\n", pu1String);
        }
        else if (i4UdpNextRemoteAddressType == UDP_IPV6_ADDR)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv6\r\n");
            MEMCPY (au1Ip6Addr, UdpNextRemoteAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Remote IP              : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        else if (i4UdpNextRemoteAddressType == UDP_ZERO)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : 0\r\n");
            CliPrintf (CliHandle, "Remote IP              : 0.0.0.0\r\n");
        }

        CliPrintf (CliHandle, "Remote Port            : %d\r\n",
                   u4UdpNextRemotePort);
        CliPrintf (CliHandle, "\r\n");

        i4UdpLocalAddressType = i4UdpNextLocalAddressType;
        u4UdpLocalPort = u4UdpNextLocalPort;
        i4UdpRemoteAddressType = i4UdpNextRemoteAddressType;
        u4UdpRemotePort = u4UdpNextRemotePort;
        u4UdpInstance = u4UdpNextInstance;

        MEMCPY (UdpLocalAddress.pu1_OctetList,
                UdpNextLocalAddress.pu1_OctetList,
                UdpNextLocalAddress.i4_Length);
        MEMCPY (UdpRemoteAddress.pu1_OctetList,
                UdpNextRemoteAddress.pu1_OctetList,
                UdpNextRemoteAddress.i4_Length);
        if (nmhGetNextIndexFsMiUdpEndpointTable
            (i4UdpLocalAddressType, &i4UdpNextLocalAddressType,
             &UdpLocalAddress, &UdpNextLocalAddress,
             u4UdpLocalPort, &u4UdpNextLocalPort,
             i4UdpRemoteAddressType, &i4UdpNextRemoteAddressType,
             &UdpRemoteAddress, &UdpNextRemoteAddress,
             u4UdpRemotePort, &u4UdpNextRemotePort,
             u4UdpInstance, &u4UdpNextInstance) == SNMP_FAILURE)
        {
            u1IsShow = UDP_ZERO;
        }
    }
    while (u1IsShow);
    CliPrintf (CliHandle, "\r\n");
    /* Reset all variables */
    u4VcId = UDP_ZERO;
    u1IsShow = UDP_ONE;
    u4UdpLocalPort = UDP_ZERO;
    u4UdpNextLocalPort = UDP_ZERO;
    u4UdpRemotePort = UDP_ZERO;
    u4UdpNextRemotePort = UDP_ZERO;
    i4UdpLocalAddressType = UDP_ONE;
    i4UdpRemoteAddressType = UDP_ONE;
    i4UdpNextLocalAddressType = UDP_ONE;
    i4UdpNextRemoteAddressType = UDP_ONE;

    UdpLocalAddress.pu1_OctetList = au1LocalIp;
    UdpLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpRemoteAddress.pu1_OctetList = au1RemoteIp;
    UdpRemoteAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpNextLocalAddress.pu1_OctetList = au1NextLocalIp;
    UdpNextLocalAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    UdpNextRemoteAddress.pu1_OctetList = au1NextRemoteIp;
    UdpNextRemoteAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    /*Retrieving VRF specific data */

    if (i4NextVcId == (INT4) VCM_INVALID_VC)
    {
        u4ShowAllCxt = UDP_TRUE;
    }

    if (u4ShowAllCxt == UDP_TRUE)
    {
        if (nmhGetFirstIndexFsMiUdpIpvxEndpointTable (&i4NextVcId,
                                                      &i4UdpNextLocalAddressType,
                                                      &UdpNextLocalAddress,
                                                      &u4UdpNextLocalPort,
                                                      &i4UdpNextRemoteAddressType,
                                                      &UdpNextRemoteAddress,
                                                      &u4UdpNextRemotePort,
                                                      &u4UdpNextInstance) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    CliPrintf (CliHandle, "\n Virtual Context - UDP Connections\r\n");
    CliPrintf (CliHandle, " ====================================\r\n\n");
    do
    {
        if (u4VcId != (UINT4) i4NextVcId)
        {
            VcmGetAliasName ((UINT4) i4NextVcId, au1ContextName);
            CliPrintf (CliHandle, "VRF   Name:  %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "---------------\r\n\n");
        }
        if (i4UdpNextLocalAddressType == UDP_IPV4_ADDR)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv4\r\n");
            u1Len = MEM_MAX_BYTES ((UINT4) UdpNextLocalAddress.i4_Length,
                                   sizeof (UINT4));
            MEMCPY ((UINT1 *) &u4LocalIp,
                    UdpNextLocalAddress.pu1_OctetList, u1Len);
            u4LocalIp = OSIX_NTOHL (u4LocalIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocalIp);
            CliPrintf (CliHandle, "Local IP               : %s\r\n", pu1String);
        }
        else if (i4UdpNextLocalAddressType == UDP_IPV6_ADDR)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : IPv6\r\n");
            MEMCPY (au1Ip6Addr, UdpNextLocalAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Local IP               : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        else if (i4UdpNextLocalAddressType == UDP_ZERO)
        {
            CliPrintf (CliHandle, "Local IP Address Type  : 0\r\n");
            CliPrintf (CliHandle, "Local IP               : 0.0.0.0\r\n");
        }
        CliPrintf (CliHandle, "Local Port             : %d\r\n",
                   u4UdpNextLocalPort);

        if (i4UdpNextRemoteAddressType == UDP_IPV4_ADDR)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv4\r\n");
            u1Len = MEM_MAX_BYTES ((UINT4) UdpNextRemoteAddress.i4_Length,
                                   sizeof (UINT4));
            MEMCPY ((UINT1 *) &u4RemoteIp,
                    UdpNextRemoteAddress.pu1_OctetList, u1Len);
            u4RemoteIp = OSIX_NTOHL (u4RemoteIp);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RemoteIp);
            CliPrintf (CliHandle, "Remote IP              : %s\r\n", pu1String);
        }
        else if (i4UdpNextRemoteAddressType == UDP_IPV6_ADDR)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : IPv6\r\n");
            MEMCPY (au1Ip6Addr, UdpNextRemoteAddress.pu1_OctetList,
                    IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "Remote IP              : %s\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
        else if (i4UdpNextRemoteAddressType == UDP_ZERO)
        {
            CliPrintf (CliHandle, "Remote IP Address Type : 0\r\n");
            CliPrintf (CliHandle, "Remote IP              : 0.0.0.0\r\n");
        }

        CliPrintf (CliHandle, "Remote Port            : %d\r\n",
                   u4UdpNextRemotePort);
        CliPrintf (CliHandle, "\r\n");

        u4VcId = (UINT4) i4NextVcId;
        i4UdpLocalAddressType = i4UdpNextLocalAddressType;
        u4UdpLocalPort = u4UdpNextLocalPort;
        i4UdpRemoteAddressType = i4UdpNextRemoteAddressType;
        u4UdpRemotePort = u4UdpNextRemotePort;
        u4UdpInstance = u4UdpNextInstance;

        MEMCPY (UdpLocalAddress.pu1_OctetList,
                UdpNextLocalAddress.pu1_OctetList,
                UdpNextLocalAddress.i4_Length);
        MEMCPY (UdpRemoteAddress.pu1_OctetList,
                UdpNextRemoteAddress.pu1_OctetList,
                UdpNextRemoteAddress.i4_Length);
        UdpLocalAddress.i4_Length = UdpNextLocalAddress.i4_Length;
        UdpRemoteAddress.i4_Length = UdpNextRemoteAddress.i4_Length;

        if (nmhGetNextIndexFsMiUdpIpvxEndpointTable
            (u4VcId, &i4NextVcId,
             i4UdpLocalAddressType, &i4UdpNextLocalAddressType,
             &UdpLocalAddress, &UdpNextLocalAddress,
             u4UdpLocalPort, &u4UdpNextLocalPort,
             i4UdpRemoteAddressType, &i4UdpNextRemoteAddressType,
             &UdpRemoteAddress, &UdpNextRemoteAddress,
             u4UdpRemotePort, &u4UdpNextRemotePort,
             u4UdpInstance, &u4UdpNextInstance) == SNMP_FAILURE)
        {
            u1IsShow = UDP_ZERO;
        }
    }
    while (u1IsShow && (u4ShowAllCxt == UDP_TRUE));
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  UdpCliGetCxtIdFromCxtName
 Description :  Get VRF name from VRF ID
 Input       :  CliHandle - Pointer to CLI handle
                pu1UdpCxtName - ContextName of VRF
 Output      :  pu4VcId - Corresponding VRF ID
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
UdpCliGetCxtIdFromCxtName (tCliHandle CliHandle, UINT1 *pu1UdpCxtName,
                           UINT4 *pu4VcId)
{
    UNUSED_PARAM (CliHandle);
    if (VcmIsVrfExist (pu1UdpCxtName, pu4VcId) == VCM_TRUE)
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}
#endif
