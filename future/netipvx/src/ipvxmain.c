/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ipvxmain.c,v 1.15 2017/12/01 13:08:39 siva Exp $
 *
 * Description: This file contains IPvx Module initialization routines.
 *********************************************************************/
#include "ipvxinc.h"
#include "traceroute.h"

PUBLIC VOID         IpvxMain (INT1 *pArg);

extern UINT4        gu4IpvxIfStatsTblLstChange;

UINT4               gu4CurrContextId;
tOsixSemId          gNetIpvxSem;

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : IpvxMain
 *                                                                          
 *    DESCRIPTION      : Entry point function of Ipvx module.
 *
 *    INPUT            : pArg - Pointer to the arguments 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
IpvxMain (INT1 *pArg)
{
    UNUSED_PARAM (pArg);

    /* Register the protocol MIBS with SNMP */
    RegisterFSMSIP ();
    RegisterFSMPIPVX ();
    RegisterSTDIPV ();
#ifdef TCP_WANTED
    RegisterSTDTCP ();
#endif
    RegisterSTDUDP ();
    RegisterFSIPVX ();
    RegisterFSMSUD ();

    /* Create a semaphore for the NetIpvx mibs */
    if (OsixCreateSem ((const UINT1 *) "IPVX", 1, 0, &gNetIpvxSem)
        != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
    }
    else
    {
        if (TraceRouteMemInit () == OSIX_FAILURE)
        {
            lrInitComplete (OSIX_FAILURE);
            return;
        }
    }

    lrInitComplete (OSIX_SUCCESS);

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : IPvxUpdateIfStatsTblLstChange
 *                                                                          
 *    DESCRIPTION      : This functions Update the time on  Ipv4 or Ipv6 
 *                       statistics are updated
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
IPvxUpdateIfStatsTblLstChange ()
{
    OsixGetSysTime ((tOsixSysTime *) & gu4IpvxIfStatsTblLstChange);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : IpvxGetNextIndexIpNetToPhyTable
 *                                                                          
 *    DESCRIPTION      : This functions get the next entry for the  Arp table 
 *                       or ND Cache table according to the IfIndex and
 *                        i4IpNetToPhyNetAddrType. 
 *
 *    INPUT            : i4IpNetToPhyIfIndex     - IfIndex 
 *                     : pi4NxtIpNetToPhyIfIndex - Next IfIndex 
 *                     : i4IpNetToPhyNetAddrType -  IPv4 or IPv6 
 *                     : pi4NxtIpNetToPhyNetAddrType -  NextType (IPv4 or IPv6) 
 *                     : pIpNetToPhyNetAddr      -  IP addres of 
 *                         'i4IpNetToPhyNetAddrType'
 *                     : pi4NxtIpNetToPhyNetAddrType -  Next IP address of 
 *                        'pi4NxtIpNetToPhyNetAddrType'  
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE - End of Both ARP 
 *                       and ND Cache Table.
 *                                                                          
 ****************************************************************************/
INT1
IpvxGetNextIndexIpNetToPhyTable (INT4 i4IpNetToPhyIfIndex,
                                 INT4 *pi4NxtIpNetToPhyIfIndex,
                                 INT4 i4IpNetToPhyNetAddrType,
                                 INT4 *pi4NxtIpNetToPhyNetAddrType,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pIpNetToPhyNetAddr,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pNxtIpNetToPhyNetAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4NxtIpNetToPhyNetAddr = IPVX_ZERO;
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
    UINT4               u4IpNetToPhyNetAddr = IPVX_ZERO;
#endif

    if ((i4IpNetToPhyNetAddrType == INET_ADDR_TYPE_IPV4)
        || (i4IpNetToPhyNetAddrType == INET_ADDR_TYPE_UNKNOWN))
    {
#if ((defined (IP_WANTED)) || (defined (LNXIP4_WANTED)))
        IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4IpNetToPhyNetAddr,
                                          pIpNetToPhyNetAddr);
        i1RetVal =
            nmhGetNextIndexIpNetToMediaTable (i4IpNetToPhyIfIndex,
                                              pi4NxtIpNetToPhyIfIndex,
                                              u4IpNetToPhyNetAddr,
                                              &u4NxtIpNetToPhyNetAddr);
#else
        i1RetVal = SNMP_FAILURE;
#endif
        if (i1RetVal == SNMP_SUCCESS)
        {
            /* Copy V4 values */
            *pi4NxtIpNetToPhyNetAddrType = INET_ADDR_TYPE_IPV4;
            IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE (pNxtIpNetToPhyNetAddr,
                                              u4NxtIpNetToPhyNetAddr);
            return (SNMP_SUCCESS);
        }
    }
    /* Always IPv4 will be added first in RB Tree. If Getting IPv4 is failed
     *  IPv6 Entry has to be fetched even i4IpNetToPhyNetAddrType is IPV4.
     */
#if IP6_WANTED
    i1RetVal =
        nmhGetNextIndexIpv6NetToMediaTable (i4IpNetToPhyIfIndex,
                                            pi4NxtIpNetToPhyIfIndex,
                                            pIpNetToPhyNetAddr,
                                            pNxtIpNetToPhyNetAddr);
#else
    i1RetVal = SNMP_FAILURE;
#endif
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pi4NxtIpNetToPhyNetAddrType = INET_ADDR_TYPE_IPV6;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}
