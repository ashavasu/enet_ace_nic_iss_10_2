###########################################################################
#  Copyright (C) 2006 Aricent Inc . All Rights Reserved
#  $Id: make.h,v 1.9 2014/05/28 12:16:01 siva Exp $
###########################################################################

#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07th Mar 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

RSTP_SWITCHES = -DRSTP_DEBUG -DRSTP_TRAP_WANTED -UIEEE_8021Y_Z12 


TOTAL_OPNS = ${RSTP_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

RSTP_BASE_DIR      = ${BASE_DIR}/stp/rstp
RSTP_SRC_DIR       = ${RSTP_BASE_DIR}/src
RSTP_INC_DIR       = ${RSTP_BASE_DIR}/inc
RSTP_OBJ_DIR       = ${RSTP_BASE_DIR}/obj
SNMP_INC_DIR      = ${BASE_DIR}/inc/snmp
CLI_INC_DIR        = ${BASE_DIR}/inc/cli
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
FSAP_INC_DIR= ${BASE_DIR}/fsap2/linux
MSTP_INC_DIR=${BASE_DIR}/stp/mstp/inc
PVRST_INC_DIR=${BASE_DIR}/stp/pvrst/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${RSTP_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${SNMP_INC_DIR} -I${MSTP_INC_DIR} -I${PVRST_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
