/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbprot.h,v 1.18 2017/11/27 13:39:31 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the RSTP and MSTP Modules for Provider Bridging
 *              functionality.
 *
 *******************************************************************/


#ifndef _ASTPBPROT_H_
#define _ASTPBPROT_H_


#ifdef NPAPI_WANTED
INT4  AstMiStpNpHwInit (VOID);

INT4  AstMiRstpNpInitHw (VOID);

INT4  AstMiNpDeleteAllFdbEntries(VOID);

INT4  AstMiRstpNpSetPortState ( UINT2 u2PortNum, UINT1 u1PortState);

INT4  AstMiMstpNpSetInstancePortState (UINT2 u2PortNum, 
                                 UINT2 u2MstInst, UINT1 u1PortState);
INT4 AstMiPvrstNpSetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 u1PortState);
INT4 AstMiRstpNpGetPortState (UINT2 u2PortNum, UINT1 *pu1PortState);
INT4 AstMiMstpNpGetInstancePortState (UINT2 u2PortNum,
                                      UINT2 u2MstInst, UINT1 *pu1PortState);
INT4 AstMiPvrstNpGetVlanPortState (UINT2 u2PortNum,
                              tVlanId VlanId, UINT1 *pu1PortState);
#endif /*NPAPI_WANTED*/


INT4  AstCreateSpanningTreeInstanceInL2Iwf(UINT2 u2MstInst);

INT4  AstDeleteMstInstanceFromL2Iwf (UINT2 u2MstInst);

INT4  AstGetPortOperStatusFromL2Iwf(UINT1 u1Module, UINT2 u2PortNum, 
                              UINT1 *pu1OperStatus);

UINT1  AstGetInstPortStateFromL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum);

INT4  AstSetInstPortStateToL2Iwf (UINT2 u2MstInst, UINT2 u2PortNum, UINT1
                                 u1PortState);

INT4  AstSetPortOperEdgeStatusToL2Iwf (UINT2 u2PortNum, BOOL1 bOperEdge);

INT4  AstGetPortOperEdgeStatusFromL2Iwf (UINT2 u2PortNum, BOOL1 *pbOperEdge);

INT4  AstPbHandleOperStatus (tAstPortEntry *pPortEntry, UINT1 u1OperStatus);

INT4  AstPbAllCVlanCompShutdown(VOID);

INT4 AstPbCheckAllPortTypes (VOID);

INT4  AstPbCVlanCompShutdown (tAstPortEntry *pAstPortEntry);

INT4  AstPbAllCVlanCompDisable(VOID);

INT4  AstPbAllCVlanCompEnable(VOID);

INT4 AstPbCVlanCompEnable(tAstPortEntry *pAstPortEntry);

INT4  AstPbCVlanCompDisable(tAstPortEntry *pAstPortEntry);

INT4 AstPbEnableCepPort (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType);
VOID  AstUpdatePortType (tAstPortEntry *pPortEntry, UINT1 u1BrgPortType);
VOID AstUpdateCompType (VOID );

INT4 AstPbHandleCcompCepPortStatus (UINT2 u2Port, UINT1 u1TrigType);

INT4  AstVlanResetShortAgeoutTime (tAstPortEntry *pPortInfo);

INT4  AstIsVlanEnabledInContext (VOID);

INT4 AstVlanSetShortAgeoutTime (tAstPortEntry *pPortInfo);

INT4 AstBrgDelFwdEntryForPort (tAstPortEntry *pPortInfo);

INT4  AstPbVlanDeleteFdbEntries (tAstPortEntry *pPortInfo, UINT1 u1Optimize);

INT4  AstHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER *pBuf,
                           tAstPortEntry *pAstPortInfo, UINT4 u4PktSize, UINT2
                           u2Protocol, UINT1 u1EncapType);

INT4  AstHandleCreatePepPort (tAstMsgNode *pMsgNode);

INT4  AstHandleDeletePepPort (tAstMsgNode *pNode);

INT4  AstHandlePepOperStatus (tAstMsgNode *pNode);

INT4 AstPbSwitchCVlanContext (tAstInterface *pIfaceId , UINT2 *pu2PortNum);

INT4  AstHandleSVlanModuleStatus (tAstMsgNode *pNode);

INT4  AstCVlanSetPortPathCost(UINT2 u2Port, UINT2 u2Inst, UINT4 u4PathCost);

INT4  AstPbCVlanSetCalculatedPtoP (UINT2 u2Port, UINT1 u1AdminPToP,
                                   tAstBoolean bOperPToP);

INT4  AstCVlanSetAutoEdge (UINT2 u2Port, tAstBoolean bAutoEdge);


/*rstpbproc.c*/


INT4 RstUpdatePepOperPtoP (UINT2 u2PortNum, UINT1 u1TrigType);

INT4 AstPbHandleCustomerEdgePort (tAstPortEntry *pAstPortInfo, 
                                  UINT1 u1TrigType);

INT4 MstConfigureRestrictedRoleAndTCN (tAstPortEntry *pPortEntry, 
                                       UINT1 u1NewRole, UINT1 u1NewTCN);

INT4 RstConfigureRestrictedRoleAndTCN (tAstPortEntry *pPortEntry, 
                                       UINT1 u1NewRole, UINT1 u1NewTCN);

INT4 RstPbCVlanComponentInit (tAstPortEntry *pAstPortInfo);

VOID AstPbCVlanFillBrgMacAddr (tAstMacAddr pAstBridgeMacAddr);

INT4 RstPbDeInitCVlanComponent (VOID);

INT4 RstPbCreateCvlanPort (UINT4 u4CepIfIndex, UINT2 u2ProtocolPort,
                         tAstPortEntry *pParentPortEntry, 
                         tAstPerStPortInfo *pParentPerStPortInfo);

INT4 RstPbDeleteCvlanPort (UINT4 u4CepIfIndex, UINT2 u2PortNum, 
                           UINT2 u2ProtocolPort, 
                           tAstPortEntry *pParentPortEntry);
                         
INT4 AstPbSelectCvlanContext(tAstPortEntry *pPortEntry);


VOID AstPbRestoreContext (VOID);
INT4 AstPbSelOnlyCvlanContext (tAstPortEntry * pPortEntry);
VOID AstPbRestoreOnlyCvlanContext (VOID);


INT4 AstPbCreateCVlanPorts (VOID);

INT4 AstPbGenerateCvlanLocalPort (UINT2 u2ProtocolPort, UINT2 *pu2HLPortId);

INT4 AstPbCVlanAddPort2IndexMap (UINT2 u2PortNum, UINT2 u2ProtocolPort);

INT4 AstPbCVlanRemovePort2IndexMapBasedOnProtPort (UINT2 u2ProtocolPort);

tAstPort2IndexMap *
AstPbCVlanGetPort2IndexMapBasedOnProtoPort (UINT2 u2ProtocolPort);

tAstPort2IndexMap * AstPbCVlanGetPort2IndexMapBasedOnHLPort (UINT2 u2PortNum);

INT4 RstPbAllocPortInfoBlocks (UINT2 u2HlPortId);

INT4 RstPbDeAllocPortInfoBlocks (tAstPortEntry *pAstPortInfo, UINT2 u2HlPortId);

INT4 AstPbAllocCVlanPortTblAndPerStInfo (VOID);

INT4  AstPbDeAllocCVlanPortTblAndPerStInfo (VOID);

INT4 RstPbAllocCVlanMemblockForPB (tAstPortEntry *pAstPortInfo);

INT4 RstPbDeAllocCVlanMemblockForPB(tAstPortEntry *pAstPortInfo);

INT4  AstPbCvlanMemPoolInit (VOID);

INT4  AstPbCvlanMemPoolDeInit (VOID);

INT4 AstPbCreateCVlanPortArrayPools (UINT4 u4BlockSize, tAstMemPoolId *pPortArrayPool,
                              tAstMemPoolId *pPerStPortArrayPool);
                              
INT4 AstPbDeleteCVlanPortArrayPools (tAstMemPoolId *pPortArrayPool,
                              tAstMemPoolId *pPerStPortArrayPool);

                              
INT4 AstPbIncrCvlanTablesSize (VOID);

INT4 AstPbCmpPort2IndexMapTable (tRBElem *pNodeA, tRBElem *pNodeB);

tAstBoolean  RstPbCvlanIsTreeAllSynced (UINT2 u2PortNum);

INT4  RstPbCvlanIsReRooted (VOID);

INT4 RstPbCvlanHdlSyncedChangeForPort (UINT2 u2Port);

/*fspblw.c*/

tAstPortEntry * 
AstGetPepPortEntryWithCtxtSwitch (tAstPortEntry *pCepPortEntry, INT4 i4VlanId);

INT4 AstGetNextPepPortEntry (INT4 i4VlanId);

tAstPortEntry *
AstGetNextCepPortEntry (UINT2 u2InPort);

INT4 AstGetNextCVlanPort (tAstPortEntry *pPortEntry, INT4 i4Svid, 
                          INT4  *pi4NextSvid);

INT4
AstPbGetNextCepPortInCurrContext(UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 *pu4NextCepIfIndex);

/*RSTP clear statistics feature for CEP */
INT4
RstResetCepPortCounters (tAstPortEntry *pCepPortEntry);

/* APIs used by both PBB and PB modules */
VOID RstProvCheckAndAlterLogicalIfPortRole (UINT2 u2PortNum, UINT2 u2RootPort);

INT4 RstProviderSetSyncTree (UINT2 u2Port);

INT4 RstProviderSetReRootTree (UINT2 u2Port);

INT4 RstProviderSetTcPropTree (UINT2 u2Port);
    
#endif  /*_ASTPBPROT_H_*/

