/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: asttrc.h,v 1.21 2017/10/30 14:29:18 siva Exp $
 *
 * Description: This file contains declarations of traces used by  
 *              the RSTP and MSTP modules.
 *
 *******************************************************************/


#ifndef _ASTTRC_H_
#define _ASTTRC_H_

/* Trace and debug flags */
#define   AST_TRC_FLAG     (AST_CURR_CONTEXT_INFO ()==NULL)? (0): (AST_CURR_CONTEXT_INFO ())->u4TraceOption 


#define   AST_GLB_TRC_FLAG    (gAstGlobalInfo.u4GlobalTraceOption)
#define   AST_SYS_TRC_FLAG      0xffff
#define   AST_SYS_TRC_VALUE      0xffff


#define   AST_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   AST_MGMT_TRC           MGMT_TRC          
#define   AST_DATA_PATH_TRC      DATA_PATH_TRC     
#define   AST_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   AST_DUMP_TRC           DUMP_TRC          
#define   AST_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   AST_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   AST_BUFFER_TRC         BUFFER_TRC        

#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
#define AST_GET_IFINDEX_STR(u2PortNum) \
        AST_GET_PORTENTRY(u2PortNum)->au1StrIfIndex
#else 
#define AST_GET_IFINDEX_STR(u2PortNum) ""
#endif

/* Trace definitions */
#ifdef  TRACE_WANTED

#define RSTP_TRACE_WANTED

#define AST_PKT_DUMP(TraceType, pBuf, Length, Str) \
    if ((AST_TRC_FLAG & TraceType) && (NULL != pBuf))\
      UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, Str); \
   if (AST_TRC_FLAG & AST_DUMP_TRC) \
   {                                \
      RstPktDump(pBuf, Length);     \
   }

#define AST_GLOBAL_TRC  AstGlobalTrace


#define FORM_CVLAN_COMP_STR(u4IfIndex)  \
    SNPRINTF ((CHR1 *)AST_CONTEXT_STR(), AST_CONTEXT_STR_LEN, "CVLAN COMP- %s-,CEP- %u:", \
             AST_PREV_CONTEXT_INFO()->au1StrContext, u4IfIndex);

#define PRINT_CONTEXT(Str) "%s:" Str,  (AST_CURR_CONTEXT_INFO() == NULL) ? "" : (CHR1 *)(AST_CURR_CONTEXT_INFO())->au1StrContext


#define AST_TRC(TraceType, Str)                                \
    if ((AST_TRC_FLAG & (TraceType)))\
       UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, PRINT_CONTEXT(Str))

#define AST_TRC_ARG1(TraceType, Str, Arg1)                     \
    if ((AST_TRC_FLAG & (TraceType)))\
        UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1)

#define AST_TRC_ARG2(TraceType, Str, Arg1, Arg2)                \
    if ((AST_TRC_FLAG & (TraceType)))\
        UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1, Arg2)

#define AST_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)         \
    if ((AST_TRC_FLAG & (TraceType)))\
        UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1, Arg2, Arg3)

#define AST_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)   \
    if ((AST_TRC_FLAG & (TraceType)))\
        UtlTrcLog(AST_TRC_FLAG, TraceType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4)

#define AST_SYS_TRC(TraceType, Str)                             \
 SYS_LOG_MSG((AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str))

#define AST_SYS_TRC_ARG1(TraceType, Str, Arg1)                  \
 SYS_LOG_MSG((AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str,Arg1))

#define AST_SYS_TRC_ARG2(TraceType, Str, Arg1, Arg2)            \
        SYS_LOG_MSG((AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str, Arg1, Arg2))

#define AST_SYS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)      \
        SYS_LOG_MSG(((UINT4)AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str, Arg1, Arg2, Arg3))  

#define AST_SYS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
        SYS_LOG_MSG((AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str, Arg1, Arg2, Arg3 , Arg4))

#define AST_SYS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
        SYS_LOG_MSG((AstGetSyslogLevel (TraceType),(UINT4)AST_SYSLOG_ID,Str, Arg1, Arg2 ,Arg3 , Arg4 , Arg5))

#define   AST_DEBUG(x) {x}

#ifndef RSTP_DEBUG
#define AST_PRINT  UtlTrcLog
#endif  /* RSTP_DEBUG */

#else  /* TRACE_WANTED */

#define FORM_CVLAN_COMP_STR(u4IfIndex)
#define AST_GLOBAL_TRC  AstGlobalTrace
#define AST_PKT_DUMP(TraceType, pBuf, Length, Str)
#define AST_TRC(TraceType, Str)

#define AST_TRC_ARG1(TraceType, Str, Arg1)\
{\
  UNUSED_PARAM(Arg1);\
}
#define AST_TRC_ARG2(TraceType, Str, Arg1, Arg2)\
{\
  UNUSED_PARAM(Arg1);\
  UNUSED_PARAM(Arg2);\
}
#define AST_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)\
{\
  UNUSED_PARAM(Arg1);\
  UNUSED_PARAM(Arg2);\
  UNUSED_PARAM(Arg3);\
}
#define AST_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)\
{\
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
}
#define   AST_DEBUG(x)

#define AST_SYS_TRC(TraceType, Str)                             

#define AST_SYS_TRC_ARG1(TraceType, Str, Arg1)                  \
{\
  UNUSED_PARAM(Arg1);\
}

#define AST_SYS_TRC_ARG2(TraceType, Str, Arg1, Arg2)            \
{\
  UNUSED_PARAM(Arg1);\
  UNUSED_PARAM(Arg2);\
}

#define AST_SYS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)      \
{\
  UNUSED_PARAM(Arg1);\
  UNUSED_PARAM(Arg2);\
  UNUSED_PARAM(Arg3);\
}

#define AST_SYS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
{\
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
}

#define AST_SYS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
{\
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
  UNUSED_PARAM (Arg5);\
}

#endif /* TRACE_WANTED */
VOID AstGlobalTrace (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...);
VOID AstGlobalDebug (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...);

#endif/* _ASTTRC_H_ */

