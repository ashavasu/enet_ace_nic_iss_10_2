/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsrswr.h,v 1.4 2011/03/21 12:38:09 siva Exp $
*
* Description: Protocol wrapper file 
*********************************************************************/
#ifndef _FSMSRSWR_H
#define _FSMSRSWR_H
INT4 GetNextIndexFsDot1dStpExtTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMSRS(VOID);

VOID UnRegisterFSMSRS(VOID);
INT4 FsDot1dStpVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTxHoldCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTxHoldCountSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTxHoldCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsDot1dStpExtPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dStpPortProtocolMigrationGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortOperEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortOperPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortProtocolMigrationSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminEdgePortSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPointToPointSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortProtocolMigrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminEdgePortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPointToPointTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortAdminPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpExtPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMSRSWR_H */
