
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbrswr.h,v 1.6 2011/08/24 06:13:51 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/



#ifndef _FSPBRSWR_H
#define _FSPBRSWR_H

VOID RegisterFSPBRS(VOID);

VOID UnRegisterFSPBRS(VOID);
INT4 FsPbProviderStpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbProviderStpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbProviderStpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbProviderStpStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsPbRstCVlanBridgeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbRstPortGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanBridgeTxHoldCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanStpHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanStpMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanStpForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanStpTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanStpTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPbRstCVlanPortInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbRstCepSvidGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortAdminEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortOperEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortAdminPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortOperPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortAutoEdgeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPbRstCVlanPortSmTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbRstCVlanPortInfoSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortMigSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortRoleTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortStateTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortTopoChSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortTxSmStateGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPbRstCVlanPortStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbRstCVlanPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRstCVlanPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);

#endif /* _FSPBRSWR_H */
