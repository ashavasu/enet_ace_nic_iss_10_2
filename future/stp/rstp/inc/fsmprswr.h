
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmprswr.h,v 1.26 2017/09/12 14:08:12 siva Exp $
*
* Description:  Function Prototype for SNMP wrappers
*********************************************************************/
#ifndef _FSMPRSWR_H
#define _FSMPRSWR_H

VOID RegisterFSMPRS(VOID);

VOID UnRegisterFSMPRS(VOID);
INT4 FsMIRstGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRstGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFsMIDot1wFutureRstTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRstSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRstpUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRstpDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstBufferFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstMemAllocFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstNewRootIdCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRoleSelSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstOldDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstDynamicPathcostCalculationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstContextNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstCalcPortPathCostOnSpeedChgGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstClearBridgeStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRcvdPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIndicationThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstTotalFlushCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFwdDelayAltPortRoleTrOptimizationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstStpPerfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstDynamicPathcostCalculationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstCalcPortPathCostOnSpeedChgSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstClearBridgeStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIndicationThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstFwdDelayAltPortRoleTrOptimizationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstStpPerfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstDynamicPathcostCalculationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstCalcPortPathCostOnSpeedChgTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstClearBridgeStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstFlushIndicationThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstFwdDelayAltPortRoleTrOptimizationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1wFutureRstTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsMIRstPortExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRstPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortInfoSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortMigSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRoleTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortStateTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTopoChSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTxSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAutoEdgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedTCNGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDURxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDUTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortIsL2GpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortLoopGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRootGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortErrorRecoveryGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortStpModeDot1wEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardActionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRcvInfoWhileExpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRcvInfoWhileExpTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortImpStateOccurCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortImpStateOccurTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAutoEdgeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedTCNSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDURxSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDUTxSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortIsL2GpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortLoopGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRootGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortErrorRecoverySet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortStpModeDot1wEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardActionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortAutoEdgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRestrictedTCNTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDURxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortEnableBPDUTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortIsL2GpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortLoopGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstStpPerfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRootGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortErrorRecoveryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortStpModeDot1wEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortBpduGuardActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 FsMIRstSetGlobalTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGlobalErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSetGlobalTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSetGlobalTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRstSetGlobalTrapsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMIDot1wFsRstTrapsControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRstSetTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstGenTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSetTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstSetTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1wFsRstTrapsControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIRstPortTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRstPortMigrationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPktErrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPktErrValGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstPortRoleTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRstOldRoleTypeGet(tSnmpIndex *, tRetVal *);

#endif /* _FSMPRSWR_H */
