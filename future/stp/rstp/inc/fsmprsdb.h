/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmprsdb.h,v 1.27 2017/09/12 14:08:12 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPRSDB_H
#define _FSMPRSDB_H

UINT1 FsMIDot1wFutureRstTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRstPortExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1wFsRstTrapsControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmprs [] ={1,3,6,1,4,1,2076,119};
tSNMP_OID_TYPE fsmprsOID = {8, fsmprs};


UINT4 FsMIRstGlobalTrace [ ] ={1,3,6,1,4,1,2076,119,1,1};
UINT4 FsMIRstGlobalDebug [ ] ={1,3,6,1,4,1,2076,119,1,2};
UINT4 FsMIDot1wFutureRstContextId [ ] ={1,3,6,1,4,1,2076,119,1,3,1,1};
UINT4 FsMIRstSystemControl [ ] ={1,3,6,1,4,1,2076,119,1,3,1,2};
UINT4 FsMIRstModuleStatus [ ] ={1,3,6,1,4,1,2076,119,1,3,1,3};
UINT4 FsMIRstTraceOption [ ] ={1,3,6,1,4,1,2076,119,1,3,1,4};
UINT4 FsMIRstDebugOption [ ] ={1,3,6,1,4,1,2076,119,1,3,1,5};
UINT4 FsMIRstRstpUpCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,6};
UINT4 FsMIRstRstpDownCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,7};
UINT4 FsMIRstBufferFailureCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,8};
UINT4 FsMIRstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,9};
UINT4 FsMIRstNewRootIdCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,10};
UINT4 FsMIRstPortRoleSelSmState [ ] ={1,3,6,1,4,1,2076,119,1,3,1,11};
UINT4 FsMIRstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,119,1,3,1,12};
UINT4 FsMIRstDynamicPathcostCalculation [ ] ={1,3,6,1,4,1,2076,119,1,3,1,13};
UINT4 FsMIRstContextName [ ] ={1,3,6,1,4,1,2076,119,1,3,1,14};
UINT4 FsMIRstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,119,1,3,1,15};
UINT4 FsMIRstClearBridgeStats [ ] ={1,3,6,1,4,1,2076,119,1,3,1,16};
UINT4 FsMIRstRcvdEvent [ ] ={1,3,6,1,4,1,2076,119,1,3,1,17};
UINT4 FsMIRstRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,119,1,3,1,18};
UINT4 FsMIRstRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,3,1,19};
UINT4 FsMIRstRcvdPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,3,1,20};
UINT4 FsMIRstFlushInterval [ ] ={1,3,6,1,4,1,2076,119,1,3,1,21};
UINT4 FsMIRstFlushIndicationThreshold [ ] ={1,3,6,1,4,1,2076,119,1,3,1,22};
UINT4 FsMIRstTotalFlushCount [ ] ={1,3,6,1,4,1,2076,119,1,3,1,23};
UINT4 FsMIRstFwdDelayAltPortRoleTrOptimization [ ] ={1,3,6,1,4,1,2076,119,1,3,1,24};
UINT4 FsMIRstBpduGuard [ ] ={1,3,6,1,4,1,2076,119,1,3,1,25};
UINT4 FsMIRstStpPerfStatus [ ] ={1,3,6,1,4,1,2076,119,1,3,1,26};
UINT4 FsMIRstPort [ ] ={1,3,6,1,4,1,2076,119,1,4,1,1};
UINT4 FsMIRstPortRole [ ] ={1,3,6,1,4,1,2076,119,1,4,1,2};
UINT4 FsMIRstPortOperVersion [ ] ={1,3,6,1,4,1,2076,119,1,4,1,3};
UINT4 FsMIRstPortInfoSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,4};
UINT4 FsMIRstPortMigSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,5};
UINT4 FsMIRstPortRoleTransSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,6};
UINT4 FsMIRstPortStateTransSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,7};
UINT4 FsMIRstPortTopoChSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,8};
UINT4 FsMIRstPortTxSmState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,9};
UINT4 FsMIRstPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,10};
UINT4 FsMIRstPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,11};
UINT4 FsMIRstPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,12};
UINT4 FsMIRstPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,13};
UINT4 FsMIRstPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,14};
UINT4 FsMIRstPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,15};
UINT4 FsMIRstPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,16};
UINT4 FsMIRstPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,17};
UINT4 FsMIRstPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,18};
UINT4 FsMIRstPortProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,19};
UINT4 FsMIRstPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,20};
UINT4 FsMIRstPortAutoEdge [ ] ={1,3,6,1,4,1,2076,119,1,4,1,21};
UINT4 FsMIRstPortRestrictedRole [ ] ={1,3,6,1,4,1,2076,119,1,4,1,22};
UINT4 FsMIRstPortRestrictedTCN [ ] ={1,3,6,1,4,1,2076,119,1,4,1,23};
UINT4 FsMIRstPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,119,1,4,1,24};
UINT4 FsMIRstPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,119,1,4,1,25};
UINT4 FsMIRstPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,119,1,4,1,26};
UINT4 FsMIRstPortIsL2Gp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,27};
UINT4 FsMIRstPortLoopGuard [ ] ={1,3,6,1,4,1,2076,119,1,4,1,28};
UINT4 FsMIRstPortClearStats [ ] ={1,3,6,1,4,1,2076,119,1,4,1,29};
UINT4 FsMIRstPortRcvdEvent [ ] ={1,3,6,1,4,1,2076,119,1,4,1,30};
UINT4 FsMIRstPortRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,119,1,4,1,31};
UINT4 FsMIRstPortRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,32};
UINT4 FsMIRstPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,33};
UINT4 FsMIRstPortRowStatus [ ] ={1,3,6,1,4,1,2076,119,1,4,1,34};
UINT4 FsMIRstLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,35};
UINT4 FsMIRstPortBpduGuard [ ] ={1,3,6,1,4,1,2076,119,1,4,1,36};
UINT4 FsMIRstPortRootGuard [ ] ={1,3,6,1,4,1,2076,119,1,4,1,37};
UINT4 FsMIRstRootInconsistentState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,38};
UINT4 FsMIRstPortErrorRecovery [ ] ={1,3,6,1,4,1,2076,119,1,4,1,39};
UINT4 FsMIRstPortStpModeDot1wEnabled [ ] ={1,3,6,1,4,1,2076,119,1,4,1,40};
UINT4 FsMIRstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,41};
UINT4 FsMIRstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,119,1,4,1,42};
UINT4 FsMIRstPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,43};
UINT4 FsMIRstPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,44};
UINT4 FsMIRstPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,45};
UINT4 FsMIRstPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,46};
UINT4 FsMIRstPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,47};
UINT4 FsMIRstPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,48};
UINT4 FsMIRstPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,119,1,4,1,49};
UINT4 FsMIRstPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,119,1,4,1,50};
UINT4 FsMIRstPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,51};
UINT4 FsMIRstPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,52};
UINT4 FsMIRstPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,119,1,4,1,53};
UINT4 FsMIRstPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,119,1,4,1,54};
UINT4 FsMIRstPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,55};
UINT4 FsMIRstPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,56};
UINT4 FsMIRstPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,119,1,4,1,57};
UINT4 FsMIRstPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,119,1,4,1,58};
UINT4 FsMIRstPortOldPortState [ ] ={1,3,6,1,4,1,2076,119,1,4,1,59};
UINT4 FsMIRstSetGlobalTraps [ ] ={1,3,6,1,4,1,2076,119,2,1};
UINT4 FsMIRstGlobalErrTrapType [ ] ={1,3,6,1,4,1,2076,119,2,2};
UINT4 FsMIRstSetTraps [ ] ={1,3,6,1,4,1,2076,119,2,3,1,1};
UINT4 FsMIRstGenTrapType [ ] ={1,3,6,1,4,1,2076,119,2,3,1,2};
UINT4 FsMIRstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,119,2,4,1,1};
UINT4 FsMIRstPortMigrationType [ ] ={1,3,6,1,4,1,2076,119,2,4,1,2};
UINT4 FsMIRstPktErrType [ ] ={1,3,6,1,4,1,2076,119,2,4,1,3};
UINT4 FsMIRstPktErrVal [ ] ={1,3,6,1,4,1,2076,119,2,4,1,4};
UINT4 FsMIRstPortRoleType [ ] ={1,3,6,1,4,1,2076,119,2,4,1,5};
UINT4 FsMIRstOldRoleType [ ] ={1,3,6,1,4,1,2076,119,2,4,1,6};


tMbDbEntry fsmprsMibEntry[]= {

{{10,FsMIRstGlobalTrace}, NULL, FsMIRstGlobalTraceGet, FsMIRstGlobalTraceSet, FsMIRstGlobalTraceTest, FsMIRstGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRstGlobalDebug}, NULL, FsMIRstGlobalDebugGet, FsMIRstGlobalDebugSet, FsMIRstGlobalDebugTest, FsMIRstGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIDot1wFutureRstContextId}, GetNextIndexFsMIDot1wFutureRstTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstSystemControl}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstSystemControlGet, FsMIRstSystemControlSet, FsMIRstSystemControlTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstModuleStatus}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstModuleStatusGet, FsMIRstModuleStatusSet, FsMIRstModuleStatusTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstTraceOption}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstTraceOptionGet, FsMIRstTraceOptionSet, FsMIRstTraceOptionTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRstDebugOption}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstDebugOptionGet, FsMIRstDebugOptionSet, FsMIRstDebugOptionTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRstRstpUpCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRstpUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstRstpDownCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRstpDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstBufferFailureCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstBufferFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstMemAllocFailureCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstNewRootIdCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstNewRootIdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRoleSelSmState}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstPortRoleSelSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstOldDesignatedRoot}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstDynamicPathcostCalculation}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstDynamicPathcostCalculationGet, FsMIRstDynamicPathcostCalculationSet, FsMIRstDynamicPathcostCalculationTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstContextName}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstCalcPortPathCostOnSpeedChg}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstCalcPortPathCostOnSpeedChgGet, FsMIRstCalcPortPathCostOnSpeedChgSet, FsMIRstCalcPortPathCostOnSpeedChgTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstClearBridgeStats}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstClearBridgeStatsGet, FsMIRstClearBridgeStatsSet, FsMIRstClearBridgeStatsTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstRcvdEvent}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstRcvdEventSubType}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstRcvdEventTimeStamp}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstRcvdPortStateChangeTimeStamp}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstRcvdPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstFlushInterval}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstFlushIntervalGet, FsMIRstFlushIntervalSet, FsMIRstFlushIntervalTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRstFlushIndicationThreshold}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstFlushIndicationThresholdGet, FsMIRstFlushIndicationThresholdSet, FsMIRstFlushIndicationThresholdTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRstTotalFlushCount}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstTotalFlushCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstFwdDelayAltPortRoleTrOptimization}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstFwdDelayAltPortRoleTrOptimizationGet, FsMIRstFwdDelayAltPortRoleTrOptimizationSet, FsMIRstFwdDelayAltPortRoleTrOptimizationTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRstBpduGuard}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstBpduGuardGet, FsMIRstBpduGuardSet, FsMIRstBpduGuardTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstStpPerfStatus}, GetNextIndexFsMIDot1wFutureRstTable, FsMIRstStpPerfStatusGet, FsMIRstStpPerfStatusSet, FsMIRstStpPerfStatusTest, FsMIDot1wFutureRstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1wFutureRstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPort}, GetNextIndexFsMIRstPortExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRole}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortOperVersion}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortInfoSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortInfoSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortMigSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortMigSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRoleTransSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRoleTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortStateTransSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortStateTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTopoChSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTopoChSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTxSmState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTxSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRxRstBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRxConfigBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRxTcnBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTxRstBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTxConfigBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTxTcnBpduCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortInvalidRstBpduRxCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortInvalidConfigBpduRxCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortInvalidTcnBpduRxCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortProtocolMigrationCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortEffectivePortState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortAutoEdge}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortAutoEdgeGet, FsMIRstPortAutoEdgeSet, FsMIRstPortAutoEdgeTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRestrictedRole}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRestrictedRoleGet, FsMIRstPortRestrictedRoleSet, FsMIRstPortRestrictedRoleTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRestrictedTCN}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRestrictedTCNGet, FsMIRstPortRestrictedTCNSet, FsMIRstPortRestrictedTCNTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortEnableBPDURx}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortEnableBPDURxGet, FsMIRstPortEnableBPDURxSet, FsMIRstPortEnableBPDURxTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRstPortEnableBPDUTx}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortEnableBPDUTxGet, FsMIRstPortEnableBPDUTxSet, FsMIRstPortEnableBPDUTxTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRstPortPseudoRootId}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortPseudoRootIdGet, FsMIRstPortPseudoRootIdSet, FsMIRstPortPseudoRootIdTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortIsL2Gp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortIsL2GpGet, FsMIRstPortIsL2GpSet, FsMIRstPortIsL2GpTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstPortLoopGuard}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortLoopGuardGet, FsMIRstPortLoopGuardSet, FsMIRstPortLoopGuardTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstPortClearStats}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortClearStatsGet, FsMIRstPortClearStatsSet, FsMIRstPortClearStatsTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRcvdEvent}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRcvdEventSubType}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRcvdEventTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortStateChangeTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRowStatus}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRowStatusGet, FsMIRstPortRowStatusSet, FsMIRstPortRowStatusTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 1, NULL},

{{12,FsMIRstLoopInconsistentState}, GetNextIndexFsMIRstPortExtTable, FsMIRstLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstPortBpduGuard}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortBpduGuardGet, FsMIRstPortBpduGuardSet, FsMIRstPortBpduGuardTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRootGuard}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRootGuardGet, FsMIRstPortRootGuardSet, FsMIRstPortRootGuardTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},
   
{{12,FsMIRstRootInconsistentState}, GetNextIndexFsMIRstPortExtTable, FsMIRstRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstPortErrorRecovery}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortErrorRecoveryGet, FsMIRstPortErrorRecoverySet, FsMIRstPortErrorRecoveryTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "30000"},

{{12,FsMIRstPortStpModeDot1wEnabled}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortStpModeDot1wEnabledGet, FsMIRstPortStpModeDot1wEnabledSet, FsMIRstPortStpModeDot1wEnabledTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortBpduInconsistentState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRstPortBpduGuardAction}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortBpduGuardActionGet, FsMIRstPortBpduGuardActionSet, FsMIRstPortBpduGuardActionTest, FsMIRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRstPortExtTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRstPortTCDetectedCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTCReceivedCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTCDetectedTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTCReceivedTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRcvInfoWhileExpCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortProposalPktsSent}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortProposalPktsRcvd}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortProposalPktSentTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortProposalPktRcvdTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortAgreementPktSent}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortAgreementPktRcvd}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortAgreementPktSentTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortImpStateOccurCount}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortImpStateOccurTimeStamp}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortOldPortState}, GetNextIndexFsMIRstPortExtTable, FsMIRstPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortExtTableINDEX, 1, 0, 0, NULL},

{{10,FsMIRstSetGlobalTraps}, NULL, FsMIRstSetGlobalTrapsGet, FsMIRstSetGlobalTrapsSet, FsMIRstSetGlobalTrapsTest, FsMIRstSetGlobalTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRstGlobalErrTrapType}, NULL, FsMIRstGlobalErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMIRstSetTraps}, GetNextIndexFsMIDot1wFsRstTrapsControlTable, FsMIRstSetTrapsGet, FsMIRstSetTrapsSet, FsMIRstSetTrapsTest, FsMIDot1wFsRstTrapsControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1wFsRstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstGenTrapType}, GetNextIndexFsMIDot1wFsRstTrapsControlTable, FsMIRstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1wFsRstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortTrapIndex}, GetNextIndexFsMIRstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortMigrationType}, GetNextIndexFsMIRstPortTrapNotificationTable, FsMIRstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPktErrType}, GetNextIndexFsMIRstPortTrapNotificationTable, FsMIRstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPktErrVal}, GetNextIndexFsMIRstPortTrapNotificationTable, FsMIRstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstPortRoleType}, GetNextIndexFsMIRstPortTrapNotificationTable, FsMIRstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRstOldRoleType}, GetNextIndexFsMIRstPortTrapNotificationTable, FsMIRstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmprsEntry = { 93, fsmprsMibEntry };

#endif /* _FSMPRSDB_H */

