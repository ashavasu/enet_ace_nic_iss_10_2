/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsrslw.h,v 1.5 2008/08/20 15:27:43 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsDot1dStpExtTable. */
INT1
nmhValidateIndexInstanceFsDot1dStpExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStpExtTable  */

INT1
nmhGetFirstIndexFsDot1dStpExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStpExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStpVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpTxHoldCount ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStpVersion ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpTxHoldCount ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStpVersion ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpTxHoldCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStpExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dStpExtPortTable. */
INT1
nmhValidateIndexInstanceFsDot1dStpExtPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStpExtPortTable  */

INT1
nmhGetFirstIndexFsDot1dStpExtPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStpExtPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStpPortProtocolMigration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortAdminEdgePort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortOperEdgePort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortAdminPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortOperPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortAdminPathCost ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStpPortProtocolMigration ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortAdminEdgePort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortAdminPointToPoint ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortAdminPathCost ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStpPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortAdminEdgePort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortAdminPointToPoint ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStpExtPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
