/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 *
 * Description: This file contains all the function prototypes
 *              used by the RSTP and MSTP Modules for supporting
 *              Provider Backbone Bridging functionality.
 *
 *******************************************************************/
#ifndef _ASTPBBPROT_H_
#define _ASTPBBPROT_H_

/* State machine changes */

tAstBoolean RstPbbIsTreeAllSynced (UINT2 u2PortNum);

INT4 RstPbbIsReRooted (UINT2 u2PortNum);

/* Configuration routines added */
INT4 AstRestoreVipDefaults (UINT2 u2PortNum);

/* BPDU Tx. on VIP */
INT4 AstL2IwfTransmitBpduOnVip (UINT4 u4ContextId, UINT4 u2IfIndex,
                                tCRU_BUF_CHAIN_DESC * pBuf);

INT4
AstHandleUpdateIntfType (tAstMsgNode * pMsgNode);

INT4
AstCreateBCompFilter (UINT4 u4IfIndex);

INT4
AstDestroyBCompFilter (UINT2 u2PortNum);

#ifdef NPAPI_WANTED
/* Updating P2P status to H/W */
INT4 AstMiRstNpHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                              UINT4 u4VipIndex, UINT4 u4Isid,
                                              UINT1 u1PointToPointStatus);

/* Filter Installation for I & B Components */
INT4 AstMiBrgHwCreateControlPktFilter (VOID);

INT4 AstMiBrgHwDestroyControlPktFilter (VOID);
#endif /* NPAPI_WANTED */
#endif
