/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsrsdb.h,v 1.7 2015/03/27 06:02:55 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSRSDB_H
#define _FSMSRSDB_H

UINT1 FsDot1dStpExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dStpExtPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmsrs [] ={1,3,6,1,4,1,2076,116,11};
tSNMP_OID_TYPE fsmsrsOID = {9, fsmsrs};

UINT4 fsDot1dStp [] ={1,3,6,1,4,1,2076,116,2};
tSNMP_OID_TYPE fsDot1dStpOID = {9, fsDot1dStp};

/* Generated OID's for tables */
UINT4 FsDot1dStpExtTable [] ={1,3,6,1,4,1,2076,116,2,3};
tSNMP_OID_TYPE FsDot1dStpExtTableOID = {10, FsDot1dStpExtTable};


UINT4 FsDot1dStpExtPortTable [] ={1,3,6,1,4,1,2076,116,2,4};
tSNMP_OID_TYPE FsDot1dStpExtPortTableOID = {10, FsDot1dStpExtPortTable};




UINT4 FsDot1dStpVersion [ ] ={1,3,6,1,4,1,2076,116,2,3,1,1};
UINT4 FsDot1dStpTxHoldCount [ ] ={1,3,6,1,4,1,2076,116,2,3,1,2};
UINT4 FsDot1dStpPortProtocolMigration [ ] ={1,3,6,1,4,1,2076,116,2,4,1,1};
UINT4 FsDot1dStpPortAdminEdgePort [ ] ={1,3,6,1,4,1,2076,116,2,4,1,2};
UINT4 FsDot1dStpPortOperEdgePort [ ] ={1,3,6,1,4,1,2076,116,2,4,1,3};
UINT4 FsDot1dStpPortAdminPointToPoint [ ] ={1,3,6,1,4,1,2076,116,2,4,1,4};
UINT4 FsDot1dStpPortOperPointToPoint [ ] ={1,3,6,1,4,1,2076,116,2,4,1,5};
UINT4 FsDot1dStpPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,116,2,4,1,6};




tMbDbEntry FsDot1dStpExtTableMibEntry[]= {

{{12,FsDot1dStpVersion}, GetNextIndexFsDot1dStpExtTable, FsDot1dStpVersionGet, FsDot1dStpVersionSet, FsDot1dStpVersionTest, FsDot1dStpExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpExtTableINDEX, 1, 0, 0, "2"},

{{12,FsDot1dStpTxHoldCount}, GetNextIndexFsDot1dStpExtTable, FsDot1dStpTxHoldCountGet, FsDot1dStpTxHoldCountSet, FsDot1dStpTxHoldCountTest, FsDot1dStpExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpExtTableINDEX, 1, 0, 0, "6"},
};
tMibData FsDot1dStpExtTableEntry = { 2, FsDot1dStpExtTableMibEntry };

tMbDbEntry FsDot1dStpExtPortTableMibEntry[]= {

{{12,FsDot1dStpPortProtocolMigration}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortProtocolMigrationGet, FsDot1dStpPortProtocolMigrationSet, FsDot1dStpPortProtocolMigrationTest, FsDot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortAdminEdgePort}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortAdminEdgePortGet, FsDot1dStpPortAdminEdgePortSet, FsDot1dStpPortAdminEdgePortTest, FsDot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortOperEdgePort}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortAdminPointToPoint}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortAdminPointToPointGet, FsDot1dStpPortAdminPointToPointSet, FsDot1dStpPortAdminPointToPointTest, FsDot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortOperPointToPoint}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortAdminPathCost}, GetNextIndexFsDot1dStpExtPortTable, FsDot1dStpPortAdminPathCostGet, FsDot1dStpPortAdminPathCostSet, FsDot1dStpPortAdminPathCostTest, FsDot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1dStpExtPortTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dStpExtPortTableEntry = { 6, FsDot1dStpExtPortTableMibEntry };

#endif /* _FSMSRSDB_H */

