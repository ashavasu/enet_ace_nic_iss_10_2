/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpred.h,v 1.47 2014/02/18 10:33:02 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for AST module.   
 *
 ******************************************************************/
#ifndef AST_RED_H
#define AST_RED_H
#ifndef _ASTHDRS_H_
 #include "asthdrs.h"
#endif

/* enums and Defines */

/* AST Module states */
typedef UINT4  tAstRedStates;
#define RED_AST_IDLE                             RM_INIT
#define RED_AST_ACTIVE                           RM_ACTIVE
#define RED_AST_STANDBY                          RM_STANDBY
#define RED_AST_FORCE_SWITCHOVER_INPROGRESS      4
#define RED_AST_SHUT_START_INPROGRESS            5

/* Default values of timers */
/* NOTE: The below default values are not actually default timer duration 
   at the time of operation of the protocol. These values are used to 
   call AstStartTimer in standby node with some default value, so that 
   a timer block is allocated but the timer is not started. These vlaues are 
   not stored anywhere.*/

#define RED_AST_FDWHILE_TMR_DEF_VAL              20 
#define RED_AST_RCVDINFOWHILE_TMR_DEF_VAL        3
#define RED_AST_TCWHILE_TMR_DEF_VAL              3
#define RED_AST_RRWHILE_TMR_DEF_VAL              15
#define RED_AST_RBWHILE_TMR_DEF_VAL              4
#define RED_AST_MDELAYWHILE_TMR_DEF_VAL          3
#define RED_AST_EDGEDELAYWHILE_TMR_DEF_VAL       3
#define RED_AST_RAPIDAGE_DURATION_TMR_DEF_VAL    16

/* Message types */
/* HITLESS RESTART */
typedef enum {
   RED_AST_BULK_REQ = RM_BULK_UPDT_REQ_MSG,
   RED_AST_BULK_UPD_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
   RED_AST_PORT_INFO,
   RED_AST_TIMES,
   RED_AST_PDU,
   RED_MST_PDU,
   RED_PVRST_PDU,
   RED_AST_OPER_STATUS,
   RED_AST_ALL_TIMES,
   RED_AST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_AST_CLEAR_ALL_SYNCUP_DATA,
   RED_MST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_AST_CALLBACK_SUCCESS,
   RED_AST_CALLBACK_FAILURE,
   RED_AST_HR_STDY_ST_PKT_REQ = RM_HR_STDY_ST_PKT_REQ
}tAstRmMsgTypes;


/* RM Interface Funtions */
INT4 AstRedSelectContext (UINT4 u4ContextId);
VOID AstRedReleaseContext (VOID);
INT4 AstRedSendUpdateToRM (tRmMsg *, UINT2);
INT4 AstRedRmInit(VOID);
INT4 AstRedRegisterWithRM (VOID);
INT4 AstRedDeRegisterWithRM (VOID);
    
/* Allocation and Release of Resources */
VOID AstRedRelPduMemPools (VOID);
VOID AstRedAllocPduMemPools(VOID);
VOID AstRedRelStoreMemPools (VOID);
VOID AstRedAllocStoreMemPools(VOID);
INT4 AstRedAllocPortAndPerPortInstInfoBlock(VOID);
INT4 AstRedReleasePortAndPerPortInstInfoBlock(VOID);
INT4 AstRedCreatePortAndPerPortInst (UINT2);
INT4 AstRedDeletePortAndPerPortInst (UINT2);
    

/* Audit */
INT4 AstRedHwAuditTask (VOID);
VOID AstRedHwAudit (INT1 *);
INT4 AstRedPortInstHwAudit (UINT2 u2InstanceId, tAstPortEntry *pPortEntry);

/* For Active */
INT4 RstRedStoreAstPdu (UINT2 , VOID *, UINT2);
VOID AstRedMakeNodeActiveFromIdle (VOID);
VOID AstRedMakeNodeActiveFromStandby (VOID);
INT4 AstRedSendUpdate(VOID);
INT4 AstRedClearPduOnActive(UINT2);
INT4 AstRedClearAllPduOnActive (VOID);
    
/* For Standby */
VOID AstRedMakeNodeStandbyFromIdle (VOID);
VOID AstRedMakeNodeStandbyFromActive (VOID);
VOID AstRedHandleBulkRequest(VOID);
VOID AstRedClearSyncUpDataOnPort(UINT2);
VOID AstRedClearAllSyncUpData(VOID);
VOID AstRedClearAllContextSyncUpData (VOID);
VOID AstRedHandleRcvdInfoWhileTmrExp (tAstPerStPortInfo *pPerStPortInfo); 

/* Storage Prototypes */
INT4 AstRedStorePortInfo(VOID * , UINT4 *, UINT2);
INT4 AstRedApplyOperStatus(VOID *, UINT4 *, UINT2);
INT4 AstRedHandleAstPdus(VOID *, UINT4 *, UINT2);
INT4 AstRedStoreTimes(VOID *, UINT4 *, UINT2);
INT4 AstRedStoreAllTimes (VOID *, UINT4 *, UINT2);
INT4 AstRedHandleTimers (VOID *, UINT4*, UINT2);
    
/* Apply Prototypes */
VOID AstRedApplyTimers(UINT1 *);
VOID AstRedApplyPerPortTimers (tAstPortEntry *pAstPortEntry, 
                               UINT2 u2InstanceId);
INT4 AstRedApplyLatestPduInfo (UINT2 , UINT2, tAstBpdu *);

VOID AstRedUpdateAllPortStates (VOID);
VOID AstRedUpdatePortStates(UINT2, UINT2);
UINT4 AstRedGetExpireTime (UINT1 , UINT2 ,UINT2);
INT4 AstRedGetNextTimer (UINT2 , UINT2, UINT1 , UINT1 *, UINT2 *, tAstTimer **);
INT4 AstRedInitOperTimes (UINT2, UINT2);
    
INT4 AstRedFormMessage(UINT2, UINT2, UINT1, UINT1, tRmMsg *,UINT4 *);
VOID AstRedHandleGoActive(VOID);
VOID AstRedHandleGoStandby(VOID);

VOID RstRedProtocolRestart(UINT2);
VOID AstRedProtocolRestart (UINT2, UINT2);

INT4 AstRedSyncUpPdu (UINT2, tAstBpdu *, UINT2);
INT4 AstRedSendSyncMessages (UINT2, UINT2, UINT1, UINT1);
VOID AstRedSendBulkRequest(VOID);
INT4 AstRedChangeGetNextMessage (UINT1 , UINT1 *,UINT2 *);
INT4 AstRedGetNextMessage (UINT1, UINT1 *, UINT2 *);

VOID AstProcessRmEvent (tAstRmCtrlMsg * pMsg); 
VOID AstRedHandleRestoreComplete (VOID);    
VOID AstRedHandleSyncUpMessage (tAstRmCtrlMsg * pMsg);
INT4 AstRedCallBackSuccess(VOID *pData, UINT4 *pu4Offset, UINT2 u2Len);
INT4 AstRedCallBackFailure(VOID *pData, UINT4 *pu4Offset, UINT2 u2Len);
    
/* For External APIs */
INT4 AstRedGetPortState(UINT2 u2Port);
VOID AstRedTriggerHigherLayer(UINT1);

/* DUMP Routines */
VOID RedDumpSyncedUpData(tCliHandle);
VOID RedDumpSyncedUpPdus (tCliHandle);
VOID AstRedDumpSyncedUpOutputs (tCliHandle);
VOID RedDumpSyncedUpPduonPort (UINT2 u2Port);
VOID AstRedDumpSyncUpOutputPerPort (UINT2 u2InstanceId, UINT2 u2Port);
VOID AstRedDumpSyncUpTmrsPerPortInst (UINT2 u2InstanceId, UINT2 u2Port);

VOID AstRedHandleBulkUpdateEvent (VOID);
VOID AstRedHandleBulkUpdatePerPort (UINT2 u2LocalPort, 
                                    VOID **ppBuf, UINT4 *pu4Offset);
INT4 AstRedSendBulkUpdateTailMsg (VOID);

/* HITLESS RESTART */
VOID AstRedHRProcStdyStPktReq (VOID);
VOID AstRedHRSetSSPTmrValInCxt (UINT4 u4ContextId, UINT1* pu1TailFlag);
VOID AstRedHRRestartTmrForSSP (tAstPortEntry *pAstPortEntry);
INT1 AstRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER *pPkt, UINT4 u4PktLen, UINT2 u2Port,
        UINT4 u4TimeOut);
INT1 AstRedHRSendStdyStTailMsg (VOID);
UINT1 AstRedGetHRFlag (VOID);

/* Message types Length*/

#define   RED_AST_PORT_INFO_LEN   15
#define   RED_AST_TIMES_LEN       18
#define   RED_AST_PDU_LEN        (sizeof(tAstBpdu) + 13)
#define   RED_AST_MST_BPDU_LEN   (sizeof(tMstBpdu) + 13)
#define   RED_AST_PVRST_BPDU_LEN (sizeof(tPvrstBpdu) + 13)
#define   RED_AST_OPER_LEN        14
#define   RED_AST_ALL_TIMES_LEN   45
#define   RED_AST_ALL_TIMES_INST_ZERO_LEN  33
#define   RED_AST_CLEAR_SYNCUP_DATA_ON_PORT_LEN 13
#define   RED_AST_CLEAR_ALL_SYNCUP_DATA_LEN  9
#define   RED_MST_CLEAR_SYNCUP_DATA_ON_PORT_LEN 11
#define   RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT_LEN 11
#define   RED_AST_BULK_REQ_LEN         3
#define   RED_AST_BULK_UPD_TAIL_MSG_LEN         3

#define   AST_MAX_RM_BUF_SIZE    1450 
#define   AST_RED_TIMER_TYPE_INVALID 100

#define AST_RED_TIMER_STOPPED    0x80
#define AST_RED_TIMER_EXPIRED    0x40
#define AST_RED_TIMER_STARTED    0x20

#define RED_AST_GET_FIRST        0
#define MST_BPDU_RST_LENGTH   36
#define MST_BPDU_STP_LENGTH   35

#define AST_RED_NO_OF_PORTS_PER_SUB_UPDATE      10

#define AST_HARDWARE_AUDIT               "StHw"
#define AST_HARDWARE_AUDIT_PRIORITY      220

#define AST_RED_TRUE     1
#define AST_RED_FALSE    0

/*HITLESS RESTART */
#define AST_RED_MSG_TYPE_SIZE   1
#define AST_RED_MSG_LEN_SIZE    2

#define AST_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ 
#define AST_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define AST_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL

/* Data structures for Storage */
typedef struct _tAstRedTimes{
    UINT4           u4FdWhileExpTime;
    UINT4           u4RcvdInfoWhileExpTime;
    UINT4           u4TcWhileExpTime; 
    UINT4           u4RrWhileExpTime;
    UINT4           u4RbWhileExpTime;
    UINT4           u4MdelayWhileExpTime; 
    UINT4           u4EdgeDelayWhileExpTime;
    UINT4           u4RapidAgeDurtnExpTime;
} tAstRedTimes;

#ifdef MSTP_WANTED
typedef struct _tAstRedCistPdu{

   tAstBridgeId               CistRootId;           /* The unique Identifier 
                                                     * of the bridge assumed 
                                                     * to be the CIST Root
                                                     * by the bridge 
                                                     * transmitting the Bpdu */
   
   UINT4                      u4CistExtPathCost;    /* The cost of the path 
                                                     * to the CIST Root Bridge
                                                     * denoted by the
                                                     * CIST Root Identifier,
                                                     * from the 
                                                     * transmitting bridge */
   
   tAstBridgeId               CistRegionalRootId;   /* The unique Bridge 
                                                     * Identifier
                                                     * of the Bridge assumed 
                                                     * to be the CIST 
                                                     * Regional Root by the 
                                                     * Bridge transmitting 
                                                     * the Bpdu */

   UINT4                      u4CistIntRootPathCost; /* The cost of the Path to
                                                      * CIST Regional Root 
                                                      * bridge from the 
                                                      * transmitting Bridge */

   tAstBridgeId               CistBridgeId;          /* The Bridge Identifier 
                                                      * of the Transmitting 
                                                      * Bridge 
                                                      */
   tMacAddr                   DestAddr;
   tMacAddr                   SrcAddr;
   UINT2                      u2Length;
   UINT1                      au1LlcHeader[3];

   UINT1                      u1Version;            /* Protocol Version 
                                                     * Identifier
                                                     * which takes the value of
                                                     * 00000011 (Version 3) */

   UINT1                      u1BpduType;           /* Takes the value of 
                                                     * 00000010
                                                     * which denotes a Rapid
                                                     * Spanning Tree BPDU */

   UINT1                      u1CistFlags;          /* Contains all the CIST 
                                                     * Flags which are 
                                                     * encoded bit-wise */
   UINT2                      u2ProtocolId;         /* Protocol Identifier 
                                                     * which takes the value 
                                                     * of 0 identifying 
                                                     * Algorithm
                                                     * and Protocol */

   
   UINT2                      u2CistPortId;         /* The Port Identifier of
                                                     * the Port on the 
                                                     * transmitting Bridge 
                                                     * through which the Bpdu
                                                     * was transmitted */
   
   UINT2                      u2MessageAge;         /* The age of the MST 
                                                     * Message, being the 
                                                     * time since the
                                                     * generation of the 
                                                     * Mst Bpdu by the Root 
                                                     * that initiated
                                                     * the generation of 
                                                     * this Bpdu */
   
   UINT2                      u2MaxAge;             /* A Timeout value to be 
                                                     * used by all Bridges in 
                                                     * the Bridged LAN. This 
                                                     * value is set by the
                                                     * Root. It is used to 
                                                     * test the age of stored
                                                     * information */
   
   UINT2                      u2HelloTime;          /* The time interval 
                                                     * between the generation 
                                                     * of Rst Bpdus by 
                                                     * the Root */
   
   UINT2                      u2FwdDelay;           /* A Timeout value to be 
                                                     * used by all Bridges in 
                                                     * the Bridged LAN. This 
                                                     * value is set by the
                                                     * Root. Its used when 
                                                     * changing the Port state
                                                     * to Forwarding
                                                     * and for ageing Filtering
                                                     * database entries */
   
   UINT1                      u1Version1Len;        /* Takes the value of 0 
                                                     * which indicates that 
                                                     * there is no Version 1 
                                                     * Protocol information
                                                     * present */

   UINT1                      u1CistRemainingHops;   /* Remaining Hops value */
   
   UINT2                      u2Version3Len;        /* Takes the value of length
                                                     * of Version3 Information 
                                                     */

   tMstConfigIdInfo           MstConfigIdInfo;       /* MST Configuration 
                                                      * Identifier Information 
                                                      */
} tAstRedCistPdu;

typedef struct _tAstMstRedPdu{
   tMstBpdu                MstRedPdu; 
} tAstMstRedPdu;
#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
typedef struct _tAstPvrstRedPdu{
   tPvrstBpdu              PvrstRedPdu; 
} tAstPvrstRedPdu;
#endif /* PVRST_WANTED */
typedef struct _tAstRedPdu
{
    tAstBpdu  AstPdu;
#ifdef MSTP_WANTED
    tMstBpdu  MstPdu;
#endif
#ifdef PVRST_WANTED
    tPvrstBpdu  PvrstPdu;
#endif
}tAstRedPdu;

typedef struct _tAstRedPerInstPortInfo
{
    tAstRedTimes        *pRedTimes;
    UINT1                u1PortRole;
    UINT1                u1PortState;
    UINT1                u1Reserved [2];
} tAstRedPerPortInstInfo;

typedef struct _tAstRedContextInfo tAstRedContextInfo;
typedef struct _tAstRedPortInfo
{
    union {
        tAstBpdu           *pRedAstPdu;
#ifdef MSTP_WANTED
        tAstMstRedPdu      *pRedMstPdu;
#endif
#ifdef PVRST_WANTED
        tAstPvrstRedPdu    *pRedPvrstPdu;
#endif
    }uPdu;
    
    tAstRedPerPortInstInfo    *pAstRedPerPortInstInfo;
    /* If the port type is CEP, then pAstRedPerPortInstInfo points
     * to the red info for the C-VLAN component containing that
     * CEP. If the port belongs to C-VLAN component, then this
     * pointer will be null. */
    tAstRedContextInfo        *pAstRedCepContextInfo;

}tAstRedPortInfo;

struct _tAstRedContextInfo
{
    tAstRedPortInfo      **ppAstRedPortInfo;
    /* The following fields will be valid only in case of Provider
     * Edge bridge C-VLAN component. */
    UINT4                  u4ParentContextId;
    tMemPoolId             AstRedCvlanPortTblPoolId;
    UINT2                  u2CepPortNum;
    UINT1                  au1Reserved[2];
};

typedef struct _tAstRedGlobalInfo 
{
    tAstRedContextInfo   aAstRedContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4                u4BulkUpdNextContext;
    UINT4                u4BulkUpdNextPort;
    UINT4                u4BulkUpdNextCvlanPort; /* In case of C-VLAN component
                                                  * bulk update, this value 
                                                  * will give the next C-Vlan
                                                  * port from where the next 
                                                  * upt should resume. */
    UINT4                u4HwAuditTskId;
    tMemPoolId           AstRedTimesMemPoolId;
    tMemPoolId           AstRedDataMemPoolId;
    tMemPoolId           AstRedPortInfoMemPoolId;
    tMemPoolId           AstRedPerPortInstInfoMemPoolId; 
    tMemPoolId           AstRedPortTblMemPoolId;
    tMemPoolId           AstRedCvlanPerStPortPoolId;
    tMemPoolId           AstRedCvlanCompPoolId;
    UINT1                u1AstNodeStatus; 
    UINT1                u1NumPeerPresent;
    BOOL1                bBulkReqRcvd;
    BOOL1                bIsHwAuditReq;
} tAstRedGlobalInfo;

typedef tOsixSysTime tAstTimeStamp;

/* AST-RED-STRUCTURES USED FOR ONLY FOR MEMORY ALLOCATION */
/* START */
typedef struct AstRedPortInfoArray {
    tAstRedPortInfo   *apRedPortInfo[AST_MAX_PORTS_PER_CONTEXT];
}tAstRedPortInfoArray;

typedef struct AstRedCvlanPortInfoArray {
    tAstRedPortInfo   *apRedPortInfo[AST_MAX_PORTS_PER_CONTEXT];
}tAstRedCvlanPortInfoArray;

typedef struct AstRedPerPortInstInfoArray{
#ifdef PVRST_WANTED
  tAstRedPerPortInstInfo  aRedPerPortInstInfo[MAX_AST_PVRST_BRIGE_INFO];
#else
  tAstRedPerPortInstInfo  aRedPerPortInstInfo[AST_MAX_MST_INSTANCES];
#endif
}tAstRedPerPortInstInfoArray;
/* END */



/* AST Core protocol Interface */
#define   AST_GET_TIME_STAMP(pTime) \
             (OsixGetSysTime(pTime))

#define   AST_ADD_TIME_STAMP(time,TimeStamp) \
              (time = time + TimeStamp)

#define  AST_ADMIN_STATUS      (AST_CURR_CONTEXT_INFO ())->u1ProtocolAdminStatus
#define  AST_GBL_BPDUGUARD_STATUS (AST_CURR_CONTEXT_INFO ())->u4GblBpduGuardStatus
    
#define  AST_RED_HW_AUDIT_TASKID          gAstRedGlobalInfo.u4HwAuditTskId

#define AST_RED_CURR_CONTEXT_INFO() gpAstRedContextInfo
#define AST_RED_PREV_CONTEXT_INFO() gpAstRedParentContextInfo

#define AST_RED_CONTEXT_PORT_TBL()  \
      (AST_RED_CURR_CONTEXT_INFO ())->ppAstRedPortInfo

#define AST_RED_CONTEXT_PORT_INFO(Port)  \
      ((AST_RED_CURR_CONTEXT_INFO ())->ppAstRedPortInfo[(Port) -1])

#define AST_RED_CONTEXT_PERPORT_INST_TBL(Port)  \
      (AST_RED_CONTEXT_PORT_INFO(Port)->pAstRedPerPortInstInfo)

#define AST_RED_CONTEXT_PERPORT_INST_ENTRY(Instance, Port)  \
      &((AST_RED_CONTEXT_PORT_INFO(Port))->pAstRedPerPortInstInfo[Instance])

#define AST_RED_TIMES_PTR(Instance, Port)  \
    ((AST_RED_CONTEXT_PERPORT_INST_ENTRY(Instance, Port))->pRedTimes)

#define AST_RED_PORT_STATE(Instance, Port) \
    ((AST_RED_CONTEXT_PERPORT_INST_ENTRY(Instance, Port))->u1PortState)

#define AST_RED_PORT_ROLE(Instance, Port) \
    ((AST_RED_CONTEXT_PERPORT_INST_ENTRY(Instance, Port))->u1PortRole)

#define AST_RED_CVLAN_COMP(u2Port) \
    AST_RED_CONTEXT_PORT_INFO(u2Port)->pAstRedCepContextInfo
      
#define AST_RED_PDU_DATA(Pdu) \
             (&(Pdu.AstPdu))
    
#define AST_RED_MST_PDU_DATA(Pdu) \
             (&(Pdu.MstPdu))
    
#define AST_RED_PVRST_PDU_DATA(Pdu) \
             (&(Pdu.PvrstPdu))

#define AST_RED_RST_PDU_PTR(Port) \
      ((AST_RED_CONTEXT_PORT_INFO(Port))->uPdu.pRedAstPdu)

#define AST_RED_MST_PDU_PTR(Port) \
      ((AST_RED_CONTEXT_PORT_INFO(Port))->uPdu.pRedMstPdu)
#define AST_RED_PVRST_PDU_PTR(Port) \
      ((AST_RED_CONTEXT_PORT_INFO(Port))->uPdu.pRedPvrstPdu)

#define AST_NUM_STANDBY_NODES() \
             (gAstRedGlobalInfo.u1NumPeerPresent)

#define AST_NODE_STATUS()  \
             gAstRedGlobalInfo.u1AstNodeStatus
             
#define AST_RM_IS_HW_AUDIT_REQ() (gAstRedGlobalInfo.bIsHwAuditReq)
             
#ifdef RM_WANTED
#define  AST_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? AST_TRUE: AST_FALSE)
#else
#define  AST_IS_NP_PROGRAMMING_ALLOWED() AST_TRUE
#endif

#define AST_IS_STANDBY_UP()  \
             ((gAstRedGlobalInfo.u1NumPeerPresent > 0) ? AST_TRUE : AST_FALSE)

#define AST_BULK_REQ_RECD()            gAstRedGlobalInfo.bBulkReqRcvd

#ifdef MSTP_WANTED
#define MST_CLEAR_INSTANCE(u2InstanceId,u2PortIfIndex)  \
             MstRedClearInstanceOnActive(u2InstanceId,u2PortIfIndex)    
#endif
    
#ifdef PVRST_WANTED
#define PVRST_CLEAR_PDU(u2PortIfIndex)  \
             PvrstRedClearPduOnActive(u2PortIfIndex)
#endif
#define AST_CLEAR_PDU(u2PortIfIndex)  \
             AstRedClearPduOnActive(u2PortIfIndex)
    
#define AST_RED_RM_GET_STATIC_CONFIG_STATUS()     AstRmGetStaticConfigStatus()
    
#define AST_RED_TIMES_MEMBLK_SIZE               (sizeof(tAstRedTimes))
#define AST_RED_PORT_STATE_MEMBLK_SIZE          (sizeof(tAstRedPortStates))
#define AST_RED_PDU_MEMBLK_SIZE                 (sizeof(tAstBpdu))
#define AST_RED_PERPORT_INST_INFO_MEMBLK_SIZE   (sizeof(tAstRedPerPortInstInfo))

#ifdef MSTP_WANTED
#define AST_RED_DATA_MEMBLK_SIZE     (sizeof(tAstMstRedPdu)) 
#elif PVRST_WANTED
#define AST_RED_DATA_MEMBLK_SIZE     (sizeof(tAstPvrstRedPdu)) 
#else
#define AST_RED_DATA_MEMBLK_SIZE     (sizeof(tAstBpdu)) 
#endif
#define AST_RED_MST_PDU_MEMBLK_SIZE   AST_RED_DATA_MEMBLK_SIZE

#define AST_RED_PVRST_PDU_MEMBLK_SIZE AST_RED_DATA_MEMBLK_SIZE
/* Times MemPool will have memblocks for one context */
#ifdef PVRST_WANTED
#define AST_RED_TIMES_MEMBLK_CNT        \
          (AST_MAX_PORTS_IN_SYSTEM * AST_MAX_PVRST_INSTANCES)
#else
#define AST_RED_TIMES_MEMBLK_CNT        \
          (AST_MAX_PORTS_IN_SYSTEM * AST_MAX_MST_INSTANCES)
#endif
    
#define AST_RED_DATA_MEMBLK_CNT         \
          (AST_MAX_PORTS_IN_SYSTEM + AST_MAX_SERVICES_PER_CUSTOMER)

#define AST_RED_DATA_MEMPOOL_ID            gAstRedGlobalInfo.AstRedDataMemPoolId
#define AST_RED_MST_PDU_MEMPOOL_ID        AST_RED_DATA_MEMPOOL_ID
#define AST_RED_PVRST_PDU_MEMPOOL_ID      AST_RED_DATA_MEMPOOL_ID
#define AST_RED_TIMES_MEMPOOL_ID          gAstRedGlobalInfo.AstRedTimesMemPoolId
#define AST_RED_PORT_INFO_MEMPOOL_ID      \
           gAstRedGlobalInfo.AstRedPortInfoMemPoolId
#define AST_RED_PERPORT_INST_INFO_MEMPOOL_ID  \
           gAstRedGlobalInfo.AstRedPerPortInstInfoMemPoolId
#define AST_RED_PORTINFO_TBL_POOL_ID \
    gAstRedGlobalInfo.AstRedPortTblMemPoolId

#define AST_RED_CVLAN_PORT_TBL_POOL_ID() \
    ((AST_RED_CURR_CONTEXT_INFO ())->AstRedCvlanPortTblPoolId)

#define AST_RED_CVLAN_PERST_PORTINFO_POOL_ID() \
    (gAstRedGlobalInfo.AstRedCvlanPerStPortPoolId)

#define AST_RED_CVLAN_COMP_POOL_ID() \
    (gAstRedGlobalInfo.AstRedCvlanCompPoolId)

#define AST_RED_PARENT_CTXT_ID() \
    ((AST_RED_CURR_CONTEXT_INFO ())->u4ParentContextId)
           
#define AST_RED_CTXT_CEP_LOCAL_NO() \
    ((AST_RED_CURR_CONTEXT_INFO ())->u2CepPortNum)
           
#define AST_ALLOC_RED_MEM_BLOCK(PoolId,pNode,ptype) \
           pNode = (ptype *)(MemAllocMemBlk(PoolId))

#define AST_FREE_RED_MEM_BLOCK(PoolId, pNode) \
           MemReleaseMemBlock(PoolId, (UINT1 *)pNode)

#define AST_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define AST_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define AST_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define AST_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define AST_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define AST_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define AST_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define AST_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define ASSERT() printf("%s ,%d\n",__FILE__,__LINE__);\
do{\
}while(0);

extern tAstRedGlobalInfo gAstRedGlobalInfo;
extern tAstRedContextInfo  *gpAstRedContextInfo;
extern tAstRedContextInfo  *gpAstRedParentContextInfo;
extern tAstRedContextInfo gpRedCvlanContextInfo;

#define AST_RED_SYNC_FLAG(u2InstanceId) \
      ((AST_GET_PERST_INFO (u2InstanceId))->u1InstSyncFlag)

#define AST_RED_SET_SYNC_FLAG(u2InstanceId) \
       if (AST_NODE_STATUS () == RED_AST_ACTIVE) \
       { \
          AST_GET_PERST_INFO (u2InstanceId)->u1InstSyncFlag=OSIX_TRUE; \
       }

#define AST_RED_RESET_SYNC_FLAG(u2InstanceId) \
          AST_GET_PERST_INFO (u2InstanceId)->u1InstSyncFlag=OSIX_FALSE;

#endif
