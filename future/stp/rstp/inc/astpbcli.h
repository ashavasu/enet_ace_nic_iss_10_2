/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbcli.h,v 1.6 2007/11/15 12:07:23 iss Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the CLI Routines for Provider Bridging
 *              functionality.
 *
 *******************************************************************/


#ifndef _ASTPBCLI_H
#define _ASTPBCLI_H_

INT4
AstPbCliSetProviderModStatus (tCliHandle CliHandle, UINT4 u4RstModStatus);
VOID
RstCliPbDisplayPortDetails (tCliHandle CliHandle, INT4 i4NextPort, INT4 i4SVlanId);


UINT4
RstCliPbPortTxBpduCount (INT4 i4NextPort, INT4 i4SVlanId);


UINT4
RstCliPbPortRxBpduCount (INT4 i4NextPort, INT4 i4SVlanId);

INT4
RstCliPbShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4IfIndex, UINT4 u4Active, UINT1 u1IsDetail);
INT4
RstCliPbShowCepSpanningTree (tCliHandle CliHandle, INT4 i4IfIndex,
                             UINT4 u4Active);
INT4
RstCliPbShowCepSpanningTreeDetail (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Active);
VOID
RstCliPbDisplayBridgeDetails (tCliHandle CliHandle, INT4 i4IfIndex);


VOID
RstCliPbShowRootBridgeInfo (tCliHandle CliHandle, INT4 i4IfIndex);

INT4
RstpPbPrintModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId);

#endif /*ASTPBCLI.H*/ 
