/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmprslw.h,v 1.25 2017/09/12 14:08:12 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsMIRstGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRstGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsMIRstGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRstGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIRstGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRstGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRstGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1wFutureRstTable. */
INT1
nmhValidateIndexInstanceFsMIDot1wFutureRstTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1wFutureRstTable  */

INT1
nmhGetFirstIndexFsMIDot1wFutureRstTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1wFutureRstTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstModuleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstTraceOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstDebugOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstRstpUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstRstpDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstBufferFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstMemAllocFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstNewRootIdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRoleSelSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstOldDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRstDynamicPathcostCalculation ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstContextName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstClearBridgeStats ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstRcvdPortStateChangeTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstFlushInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstFlushIndicationThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstTotalFlushCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstFwdDelayAltPortRoleTrOptimization ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstBpduGuard ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIRstStpPerfStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRstSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstModuleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstTraceOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstDebugOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstDynamicPathcostCalculation ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstCalcPortPathCostOnSpeedChg ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstClearBridgeStats ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstFlushInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstFlushIndicationThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstFwdDelayAltPortRoleTrOptimization ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstStpPerfStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRstSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstTraceOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstDebugOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstDynamicPathcostCalculation ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstClearBridgeStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstFlushInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstFwdDelayAltPortRoleTrOptimization ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstStpPerfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1wFutureRstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRstPortExtTable. */
INT1
nmhValidateIndexInstanceFsMIRstPortExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRstPortExtTable  */

INT1
nmhGetFirstIndexFsMIRstPortExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRstPortExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortInfoSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortMigSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRoleTransSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortStateTransSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortTopoChSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortTxSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortInvalidRstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortProtocolMigrationCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortEffectivePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortAutoEdge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRestrictedRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRestrictedTCN ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortPseudoRootId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRstPortIsL2Gp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortLoopGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortClearStats ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortStateChangeTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRowStatus ARG_LIST((INT4 ,INT4 *));

INT1 
nmhGetFsMIRstLoopInconsistentState ARG_LIST((INT4 ,INT4 *)); 

INT1
nmhGetFsMIRstPortBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstRootInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortErrorRecovery ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortStpModeDot1wEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortBpduGuardAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortTCDetectedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTCReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTCDetectedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortTCReceivedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortProposalPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortProposalPktsRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortProposalPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortProposalPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortAgreementPktSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortAgreementPktRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortAgreementPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortAgreementPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRstPortOldPortState ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRstPortAutoEdge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortRestrictedRole ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortRestrictedTCN ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortPseudoRootId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRstPortIsL2Gp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortClearStats ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortRootGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortErrorRecovery ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortStpModeDot1wEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRstPortBpduGuardAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRstPortAutoEdge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortRestrictedRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortRestrictedTCN ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRstPortIsL2Gp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortClearStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortErrorRecovery ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortStpModeDot1wEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRstPortBpduGuardAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRstPortExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstSetGlobalTraps ARG_LIST((INT4 *));

INT1
nmhGetFsMIRstGlobalErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRstSetGlobalTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRstSetGlobalTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRstSetGlobalTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1wFsRstTrapsControlTable. */
INT1
nmhValidateIndexInstanceFsMIDot1wFsRstTrapsControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1wFsRstTrapsControlTable  */

INT1
nmhGetFirstIndexFsMIDot1wFsRstTrapsControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1wFsRstTrapsControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstSetTraps ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstGenTrapType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRstSetTraps ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRstSetTraps ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1wFsRstTrapsControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMIRstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMIRstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPktErrVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstPortRoleType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRstOldRoleType ARG_LIST((INT4 ,INT4 *));
