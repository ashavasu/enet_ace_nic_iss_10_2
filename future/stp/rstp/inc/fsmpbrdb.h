/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbrdb.h,v 1.5 2012/02/29 09:37:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPBRDB_H
#define _FSMPBRDB_H

UINT1 FsMIPbRstContextInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbRstCVlanBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbRstCVlanPortInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIPbRstCVlanPortSmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIPbRstCVlanPortStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsmpbr [] ={1,3,6,1,4,1,2076,134};
tSNMP_OID_TYPE fsmpbrOID = {8, fsmpbr};


UINT4 FsMIPbRstContextId [ ] ={1,3,6,1,4,1,2076,134,1,1,1,1};
UINT4 FsMIPbProviderStpStatus [ ] ={1,3,6,1,4,1,2076,134,1,1,1,2};
UINT4 FsMIPbRstPort [ ] ={1,3,6,1,4,1,2076,134,1,2,1,1};
UINT4 FsMIPbRstCVlanBridgeId [ ] ={1,3,6,1,4,1,2076,134,1,2,1,2};
UINT4 FsMIPbRstCVlanBridgeDesignatedRoot [ ] ={1,3,6,1,4,1,2076,134,1,2,1,3};
UINT4 FsMIPbRstCVlanBridgeRootCost [ ] ={1,3,6,1,4,1,2076,134,1,2,1,4};
UINT4 FsMIPbRstCVlanBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,134,1,2,1,5};
UINT4 FsMIPbRstCVlanBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,134,1,2,1,6};
UINT4 FsMIPbRstCVlanBridgeHoldTime [ ] ={1,3,6,1,4,1,2076,134,1,2,1,7};
UINT4 FsMIPbRstCVlanBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,134,1,2,1,8};
UINT4 FsMIPbRstCVlanBridgeTxHoldCount [ ] ={1,3,6,1,4,1,2076,134,1,2,1,9};
UINT4 FsMIPbRstCVlanStpHelloTime [ ] ={1,3,6,1,4,1,2076,134,1,2,1,10};
UINT4 FsMIPbRstCVlanStpMaxAge [ ] ={1,3,6,1,4,1,2076,134,1,2,1,11};
UINT4 FsMIPbRstCVlanStpForwardDelay [ ] ={1,3,6,1,4,1,2076,134,1,2,1,12};
UINT4 FsMIPbRstCVlanStpTopChanges [ ] ={1,3,6,1,4,1,2076,134,1,2,1,13};
UINT4 FsMIPbRstCVlanStpTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,134,1,2,1,14};
UINT4 FsMIPbRstCVlanStpDebugOption [ ] ={1,3,6,1,4,1,2076,134,1,2,1,15};
UINT4 FsMIPbRstCepSvid [ ] ={1,3,6,1,4,1,2076,134,1,3,1,1};
UINT4 FsMIPbRstCVlanPortPriority [ ] ={1,3,6,1,4,1,2076,134,1,3,1,2};
UINT4 FsMIPbRstCVlanPortPathCost [ ] ={1,3,6,1,4,1,2076,134,1,3,1,3};
UINT4 FsMIPbRstCVlanPortRole [ ] ={1,3,6,1,4,1,2076,134,1,3,1,4};
UINT4 FsMIPbRstCVlanPortState [ ] ={1,3,6,1,4,1,2076,134,1,3,1,5};
UINT4 FsMIPbRstCVlanPortAdminEdgePort [ ] ={1,3,6,1,4,1,2076,134,1,3,1,6};
UINT4 FsMIPbRstCVlanPortOperEdgePort [ ] ={1,3,6,1,4,1,2076,134,1,3,1,7};
UINT4 FsMIPbRstCVlanPortAdminPointToPoint [ ] ={1,3,6,1,4,1,2076,134,1,3,1,8};
UINT4 FsMIPbRstCVlanPortOperPointToPoint [ ] ={1,3,6,1,4,1,2076,134,1,3,1,9};
UINT4 FsMIPbRstCVlanPortAutoEdge [ ] ={1,3,6,1,4,1,2076,134,1,3,1,10};
UINT4 FsMIPbRstCVlanPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,134,1,3,1,11};
UINT4 FsMIPbRstCVlanPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,134,1,3,1,12};
UINT4 FsMIPbRstCVlanPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,134,1,3,1,13};
UINT4 FsMIPbRstCVlanPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,134,1,3,1,14};
UINT4 FsMIPbRstCVlanPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,134,1,3,1,15};
UINT4 FsMIPbRstCVlanPortInfoSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,1};
UINT4 FsMIPbRstCVlanPortMigSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,2};
UINT4 FsMIPbRstCVlanPortRoleTransSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,3};
UINT4 FsMIPbRstCVlanPortStateTransSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,4};
UINT4 FsMIPbRstCVlanPortTopoChSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,5};
UINT4 FsMIPbRstCVlanPortTxSmState [ ] ={1,3,6,1,4,1,2076,134,1,4,1,6};
UINT4 FsMIPbRstCVlanPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,1};
UINT4 FsMIPbRstCVlanPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,2};
UINT4 FsMIPbRstCVlanPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,3};
UINT4 FsMIPbRstCVlanPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,4};
UINT4 FsMIPbRstCVlanPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,5};
UINT4 FsMIPbRstCVlanPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,6};
UINT4 FsMIPbRstCVlanPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,7};
UINT4 FsMIPbRstCVlanPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,8};
UINT4 FsMIPbRstCVlanPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,9};
UINT4 FsMIPbRstCVlanPortProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,134,1,5,1,10};
UINT4 FsMIPbRstCVlanPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,134,1,5,1,11};


tMbDbEntry fsmpbrMibEntry[]= {

{{12,FsMIPbRstContextId}, GetNextIndexFsMIPbRstContextInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbRstContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbProviderStpStatus}, GetNextIndexFsMIPbRstContextInfoTable, FsMIPbProviderStpStatusGet, FsMIPbProviderStpStatusSet, FsMIPbProviderStpStatusTest, FsMIPbRstContextInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbRstContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstPort}, GetNextIndexFsMIPbRstCVlanBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeId}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeDesignatedRoot}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeRootCost}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeMaxAge}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeHelloTime}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeHoldTime}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeForwardDelay}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanBridgeTxHoldCount}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanBridgeTxHoldCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpHelloTime}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpMaxAge}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpForwardDelay}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpTopChanges}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpTimeSinceTopologyChange}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbRstCVlanStpDebugOption}, GetNextIndexFsMIPbRstCVlanBridgeTable, FsMIPbRstCVlanStpDebugOptionGet, FsMIPbRstCVlanStpDebugOptionSet, FsMIPbRstCVlanStpDebugOptionTest, FsMIPbRstCVlanBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPbRstCVlanBridgeTableINDEX, 1, 0, 0, "0"},

{{12,FsMIPbRstCepSvid}, GetNextIndexFsMIPbRstCVlanPortInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortPriority}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortPathCost}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortRole}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortState}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortAdminEdgePort}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortAdminEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortOperEdgePort}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortAdminPointToPoint}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortAdminPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortOperPointToPoint}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortAutoEdge}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortAutoEdgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortDesignatedRoot}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortDesignatedCost}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortDesignatedBridge}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortDesignatedPort}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortForwardTransitions}, GetNextIndexFsMIPbRstCVlanPortInfoTable, FsMIPbRstCVlanPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortInfoSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortInfoSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortMigSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortMigSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortRoleTransSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortRoleTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortStateTransSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortStateTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortTopoChSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortTopoChSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortTxSmState}, GetNextIndexFsMIPbRstCVlanPortSmTable, FsMIPbRstCVlanPortTxSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortRxRstBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortRxConfigBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortRxTcnBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortTxRstBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortTxConfigBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortTxTcnBpduCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortInvalidRstBpduRxCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortInvalidConfigBpduRxCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortInvalidTcnBpduRxCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortProtocolMigrationCount}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRstCVlanPortEffectivePortState}, GetNextIndexFsMIPbRstCVlanPortStatsTable, FsMIPbRstCVlanPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmpbrEntry = { 49, fsmpbrMibEntry };

#endif /* _FSMPBRDB_H */

