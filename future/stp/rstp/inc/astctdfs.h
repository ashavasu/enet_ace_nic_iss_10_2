/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astctdfs.h,v 1.4 2010/06/30 10:02:43 prabuc Exp $
 *
 * Description: This file contains type definitions common for both
 *              RSTP and MSTP modules.
 *
 *******************************************************************/


#ifndef _ASTCTDFS_H
#define _ASTCTDFS_H

typedef tCRU_BUF_CHAIN_HEADER     tAstBufChainHeader;
typedef tCRU_INTERFACE            tAstInterface;
typedef tTMO_SLL                  tAstSll;
typedef tTMO_SLL_NODE             tAstSllNode;
typedef UINT4                     tAstLocalMsgType;
typedef tCfaIfInfo                tAstCfaIfInfo;
typedef tMacAddr                  tAstMacAddr;


/******************************************************************************/
/*                 Various Timer values used in Rst & Mst                     */
/******************************************************************************/

typedef struct AstTimes {

   UINT2                      u2MaxAge;        /* Maximum Age for the msg that
                                                * should be used */
   UINT2                      u2HelloTime;     /* Hello time interval value
                                                * during which Bpdus need to 
                                                * be transmitted periodically */
   UINT2                      u2ForwardDelay;  /* Delay before a port can become
                                                * operational for packet 
                                                * forwarding function */
   UINT2                      u2MsgAgeOrHopCount;
                                               /* Age of information received
                                                * or transmitted (in RSTP) or
                                                * Number of hops (for MSTP) */
}tAstTimes;

/******************************************************************************/
/*                            Bridge Identifier                               */
/******************************************************************************/

typedef struct AstBridgeId {

   UINT2                      u2BrgPriority;   /* Consists of 4-bit settable 
                                                * priority and 12-bit Instance
                                                * Id in the case of MSTP */
   tAstMacAddr                BridgeAddr;      /* 6-byte Unique Address of this
                                                * Bridge */
}tAstBridgeId;


typedef struct AstString {

   UINT1 au1Octets[AST_MAX_OCTET_LEN];
   UINT2 u2OctLen;
   UINT2 u2Reserved;

}tAstString;

#endif /* _ASTCTDFS_H */
