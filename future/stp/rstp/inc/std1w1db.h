/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1w1db.h,v 1.1 2011/10/24 11:03:29 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1W1DB_H
#define _STD1W1DB_H

UINT1 Ieee8021SpanningTreeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021SpanningTreePortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 std1w1 [] ={1,3,111,2,802,1,1,3};
tSNMP_OID_TYPE std1w1OID = {8, std1w1};


UINT4 Ieee8021SpanningTreeComponentId [ ] ={1,3,111,2,802,1,1,3,1,1,1,1};
UINT4 Ieee8021SpanningTreeProtocolSpecification [ ] ={1,3,111,2,802,1,1,3,1,1,1,2};
UINT4 Ieee8021SpanningTreePriority [ ] ={1,3,111,2,802,1,1,3,1,1,1,3};
UINT4 Ieee8021SpanningTreeTimeSinceTopologyChange [ ] ={1,3,111,2,802,1,1,3,1,1,1,4};
UINT4 Ieee8021SpanningTreeTopChanges [ ] ={1,3,111,2,802,1,1,3,1,1,1,5};
UINT4 Ieee8021SpanningTreeDesignatedRoot [ ] ={1,3,111,2,802,1,1,3,1,1,1,6};
UINT4 Ieee8021SpanningTreeRootCost [ ] ={1,3,111,2,802,1,1,3,1,1,1,7};
UINT4 Ieee8021SpanningTreeRootPort [ ] ={1,3,111,2,802,1,1,3,1,1,1,8};
UINT4 Ieee8021SpanningTreeMaxAge [ ] ={1,3,111,2,802,1,1,3,1,1,1,9};
UINT4 Ieee8021SpanningTreeHelloTime [ ] ={1,3,111,2,802,1,1,3,1,1,1,10};
UINT4 Ieee8021SpanningTreeHoldTime [ ] ={1,3,111,2,802,1,1,3,1,1,1,11};
UINT4 Ieee8021SpanningTreeForwardDelay [ ] ={1,3,111,2,802,1,1,3,1,1,1,12};
UINT4 Ieee8021SpanningTreeBridgeMaxAge [ ] ={1,3,111,2,802,1,1,3,1,1,1,13};
UINT4 Ieee8021SpanningTreeBridgeHelloTime [ ] ={1,3,111,2,802,1,1,3,1,1,1,14};
UINT4 Ieee8021SpanningTreeBridgeForwardDelay [ ] ={1,3,111,2,802,1,1,3,1,1,1,15};
UINT4 Ieee8021SpanningTreeVersion [ ] ={1,3,111,2,802,1,1,3,1,1,1,16};
UINT4 Ieee8021SpanningTreeRstpTxHoldCount [ ] ={1,3,111,2,802,1,1,3,1,1,1,17};
UINT4 Ieee8021SpanningTreePortComponentId [ ] ={1,3,111,2,802,1,1,3,1,2,1,1};
UINT4 Ieee8021SpanningTreePort [ ] ={1,3,111,2,802,1,1,3,1,2,1,2};
UINT4 Ieee8021SpanningTreePortPriority [ ] ={1,3,111,2,802,1,1,3,1,2,1,3};
UINT4 Ieee8021SpanningTreePortState [ ] ={1,3,111,2,802,1,1,3,1,2,1,4};
UINT4 Ieee8021SpanningTreePortEnabled [ ] ={1,3,111,2,802,1,1,3,1,2,1,5};
UINT4 Ieee8021SpanningTreePortPathCost [ ] ={1,3,111,2,802,1,1,3,1,2,1,6};
UINT4 Ieee8021SpanningTreePortDesignatedRoot [ ] ={1,3,111,2,802,1,1,3,1,2,1,7};
UINT4 Ieee8021SpanningTreePortDesignatedCost [ ] ={1,3,111,2,802,1,1,3,1,2,1,8};
UINT4 Ieee8021SpanningTreePortDesignatedBridge [ ] ={1,3,111,2,802,1,1,3,1,2,1,9};
UINT4 Ieee8021SpanningTreePortDesignatedPort [ ] ={1,3,111,2,802,1,1,3,1,2,1,10};
UINT4 Ieee8021SpanningTreePortForwardTransitions [ ] ={1,3,111,2,802,1,1,3,1,2,1,11};
UINT4 Ieee8021SpanningTreeRstpPortProtocolMigration [ ] ={1,3,111,2,802,1,1,3,1,2,1,12};
UINT4 Ieee8021SpanningTreeRstpPortAdminEdgePort [ ] ={1,3,111,2,802,1,1,3,1,2,1,13};
UINT4 Ieee8021SpanningTreeRstpPortOperEdgePort [ ] ={1,3,111,2,802,1,1,3,1,2,1,14};
UINT4 Ieee8021SpanningTreeRstpPortAdminPathCost [ ] ={1,3,111,2,802,1,1,3,1,2,1,15};




tMbDbEntry std1w1MibEntry[]= {

{{12,Ieee8021SpanningTreeComponentId}, GetNextIndexIeee8021SpanningTreeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeProtocolSpecification}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeProtocolSpecificationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreePriority}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreePriorityGet, Ieee8021SpanningTreePrioritySet, Ieee8021SpanningTreePriorityTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeTimeSinceTopologyChange}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeTopChanges}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeDesignatedRoot}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRootCost}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRootPort}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeMaxAge}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeHelloTime}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeHoldTime}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeForwardDelay}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeBridgeMaxAge}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeBridgeMaxAgeGet, Ieee8021SpanningTreeBridgeMaxAgeSet, Ieee8021SpanningTreeBridgeMaxAgeTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeBridgeHelloTime}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeBridgeHelloTimeGet, Ieee8021SpanningTreeBridgeHelloTimeSet, Ieee8021SpanningTreeBridgeHelloTimeTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeBridgeForwardDelay}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeBridgeForwardDelayGet, Ieee8021SpanningTreeBridgeForwardDelaySet, Ieee8021SpanningTreeBridgeForwardDelayTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeVersion}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeVersionGet, Ieee8021SpanningTreeVersionSet, Ieee8021SpanningTreeVersionTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRstpTxHoldCount}, GetNextIndexIeee8021SpanningTreeTable, Ieee8021SpanningTreeRstpTxHoldCountGet, Ieee8021SpanningTreeRstpTxHoldCountSet, Ieee8021SpanningTreeRstpTxHoldCountTest, Ieee8021SpanningTreeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021SpanningTreeTableINDEX, 1, 0, 0, "3"},

{{12,Ieee8021SpanningTreePortComponentId}, GetNextIndexIeee8021SpanningTreePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePort}, GetNextIndexIeee8021SpanningTreePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortPriority}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortPriorityGet, Ieee8021SpanningTreePortPrioritySet, Ieee8021SpanningTreePortPriorityTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortState}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortEnabled}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortEnabledGet, Ieee8021SpanningTreePortEnabledSet, Ieee8021SpanningTreePortEnabledTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortPathCost}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortPathCostGet, Ieee8021SpanningTreePortPathCostSet, Ieee8021SpanningTreePortPathCostTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortDesignatedRoot}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortDesignatedCost}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortDesignatedBridge}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortDesignatedPort}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreePortForwardTransitions}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreePortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRstpPortProtocolMigration}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreeRstpPortProtocolMigrationGet, Ieee8021SpanningTreeRstpPortProtocolMigrationSet, Ieee8021SpanningTreeRstpPortProtocolMigrationTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRstpPortAdminEdgePort}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreeRstpPortAdminEdgePortGet, Ieee8021SpanningTreeRstpPortAdminEdgePortSet, Ieee8021SpanningTreeRstpPortAdminEdgePortTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRstpPortOperEdgePort}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreeRstpPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021SpanningTreeRstpPortAdminPathCost}, GetNextIndexIeee8021SpanningTreePortTable, Ieee8021SpanningTreeRstpPortAdminPathCostGet, Ieee8021SpanningTreeRstpPortAdminPathCostSet, Ieee8021SpanningTreeRstpPortAdminPathCostTest, Ieee8021SpanningTreePortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021SpanningTreePortTableINDEX, 2, 0, 0, NULL},
};
tMibData std1w1Entry = { 32, std1w1MibEntry };

#endif /* _STD1W1DB_H */

