/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrtrap.h,v 1.21 2017/09/12 14:08:11 siva Exp $
 *
 * Description: This file contains all macros and structures used
 *              for traps by the RSTP module.
 *
 *******************************************************************/

#ifndef _RST_TRAP_H_
#define _RST_TRAP_H_

/* BRG */
#define AST_TRAP_TYPE (AST_CURR_CONTEXT_INFO ())->u4ContextTrapOption
#define AST_GEN_TRAP (AST_CURR_CONTEXT_INFO ())->u1AstGenTrapType
#define AST_ERR_TRAP (gAstGlobalInfo.u1AstErrTrapType)
#define AST_GLOBAL_TRAP (gAstGlobalInfo.u4GlobalTrapOption)
#define AST_PKT_ERR_VAL(u2Port)  (AST_CURR_CONTEXT_INFO ())->apPortEntry[u2Port]->u2InvalidPktValue


#define AST_BRG_TRAPS_OID_MI "1.3.6.1.4.1.2076.119.3"
#ifdef MSTP_WANTED
#define AST_MST_TRAPS_OID_MI "1.3.6.1.4.1.2076.118.3"
#endif
#ifdef PVRST_WANTED
#define AST_PVRST_TRAPS_OID_MI "1.3.6.1.4.1.2076.161.3"
#endif
#define AST_BRG_TRAPS_OID_SI "1.3.6.1.4.1.2076.79.3"
#ifdef MSTP_WANTED
#define AST_MST_TRAPS_OID_SI "1.3.6.1.4.1.2076.80.3"
#endif
#ifdef PVRST_WANTED
#define AST_PVRST_TRAPS_OID_SI "1.3.6.1.4.1.2076.161.3"
#endif


#define AST_BRG_TRAPS_OID \
(VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE ? AST_BRG_TRAPS_OID_SI : AST_BRG_TRAPS_OID_MI)

#define AST_MST_TRAPS_OID \
(VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE ? AST_MST_TRAPS_OID_SI: AST_MST_TRAPS_OID_MI)

#define AST_PVRST_TRAPS_OID \
(VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE ? AST_PVRST_TRAPS_OID_SI: AST_PVRST_TRAPS_OID_MI)

#ifdef MSTP_WANTED
#define AST_MST_TRAPS_OID_MI "1.3.6.1.4.1.2076.118.3"
#endif
#ifdef PVRST_WANTED
#define AST_PVRST_TRAPS_OID_MI "1.3.6.1.4.1.2076.161.3"
#endif

/* Different traps supported in RSTP and MSTP
 *  - Passed as parameter to the internal Trap PDU formation routine
 *    for distinguishing between them. See below for actual MIB Trap IDs */
#define AST_BRG_GEN_TRAP_VAL 1 
#define AST_BRG_ERR_TRAP_VAL 2
#define AST_NEW_ROOT_TRAP_VAL 3
#define AST_TOPOLOGY_CHG_TRAP_VAL 4
#define AST_PROTOCOL_MIGRATION_TRAP_VAL 5
#define AST_INVALID_BPDU_RXD_TRAP_VAL 6
#define AST_NEW_PORTROLE_TRAP_VAL 7
#define AST_HW_FAIL_TRAP_VAL  8
#define AST_REGION_CONFIG_CHANGE_TRAP_VAL  9
#define AST_LOOP_INCSTATE_CHANGE_TRAP_VAL 10
#define AST_BPDU_INCSTATE_CHANGE_TRAP_VAL 11
#define AST_ROOT_INCSTATE_CHANGE_TRAP_VAL 12
#define AST_PORTSTATE_CHANGE_TRAP_VAL  13


/* Type code for TRAP - Used in RSTP Trap PDUs */
#define RST_BRG_GEN_TRAP_VAL 1 
#define RST_BRG_ERR_TRAP_VAL 2
#define RST_NEW_ROOT_TRAP_VAL 3
#define RST_TOPOLOGY_CHG_TRAP_VAL 4
#define RST_PROTOCOL_MIGRATION_TRAP_VAL 5
#define RST_INVALID_BPDU_RXD_TRAP_VAL 6
#define RST_NEW_PORTROLE_TRAP_VAL 7
#define RST_HW_FAIL_TRAP_VAL  8
#define RST_LOOP_INCSTATE_CHANGE_TRAP_VAL 9
#define RST_BPDU_INCSTATE_CHANGE_TRAP_VAL 10
#define RST_ROOT_INCSTATE_CHANGE_TRAP_VAL 11
#define RST_PORTSTATE_CHANGE_TRAP_VAL  12


#define AST_TRAP_DISABLE 0   
#define AST_TRAP_NORMAL  1   
#define AST_TRAP_ABNORMAL 2
    
/* Type code for normal trap type */
#define AST_UP_TRAP 1
#define AST_DOWN_TRAP 2

/* Type code for abnormal trap type */
#define AST_TRAP_MEM_FAIL 1
#define AST_TRAP_BUF_FAIL 2

/* Type code for INVALID BPDU_TRXD */
#define AST_INVALID_PROTOCOL_ID 1
#define AST_INVALID_BPDU_TYPE  2
#define AST_INVALID_CONFIG_LENGTH 2
#define AST_INVALID_TCN_LENGTH 3
#define AST_INVALID_RST_LENGTH 4
#define AST_INVALID_MAX_AGE 6
#define AST_INVALID_FWD_DELAY 7
#define AST_INVALID_HELLO_TIME 8
#define AST_INVALID_MST_LENGTH 9
/* type code for protocol migration trap */
#define AST_TRAP_SEND_STP 0   
#define AST_TRAP_SEND_RSTP 1   

#define AST_TRAP_MAX  3

#define AST_TRAP_SNMP_VERSION1  0    
#define AST_BRG_TRAPS_OID_LEN  10 

#define IS_AST_GLOBAL_BRG_ERR_EVNT() \
    (gAstGlobalInfo.u4GlobalTrapOption & 0x00000001)
    
#define IS_AST_BRG_GEN_EVNT() \
    ((AST_CURR_CONTEXT_INFO ())->u4ContextTrapOption & 0x00000001) 
#define IS_AST_BRG_ERR_EVNT() \
    ((AST_CURR_CONTEXT_INFO ())->u4ContextTrapOption & 0x00000002) 

#define RST_MIB_OBJ_CONTEXT_NAME      "fsMIRstContextName"
#define RST_MIB_OBJ_BASE_BRIDGE_ADDR_MI  "fsDot1dBaseBridgeAddress"
#define RST_MIB_OBJ_GEN_TRAP_TYPE_MI     "fsMIRstGenTrapType"
#define RST_MIB_OBJ_ERR_TRAP_TYPE_MI     "fsMIRstGlobalErrTrapType"
#define RST_MIB_OBJ_OLD_DESIG_ROOT_MI    "fsMIRstOldDesignatedRoot"
#define RST_MIB_OBJ_DESIG_ROOT_MI        "fsDot1dStpDesignatedRoot"
#define RST_MIB_OBJ_PORT_TRAP_INDEX_MI   "fsMIRstPortTrapIndex"
#define RST_MIB_OBJ_STP_VERSION_MI       "fsDot1dStpVersion"
#define RST_MIB_OBJ_MIGRATION_TYPE_MI    "fsMIRstPortMigrationType"
#define RST_MIB_OBJ_PKT_ERR_TYPE_MI      "fsMIRstPktErrType"
#define RST_MIB_OBJ_PKT_ERR_VAL_MI       "fsMIRstPktErrVal"
#define RST_MIB_OBJ_PREVIOUS_ROLE_MI     "fsMIRstOldRoleType" 
#define RST_MIB_OBJ_SELECTED_ROLE_MI     "fsMIRstPortRoleType"
#define RST_MIB_OBJ_PORT_STATE_MI        "fsDot1dStpPortState"
#define RST_MIB_OBJ_LOOP_INCSTATE_MI     "fsMIRstLoopInconsistentState"
#define RST_MIB_OBJ_BPDU_INCSTATE_MI     "fsMIRstBpduInconsistentState"
#define RST_MIB_OBJ_ROOT_INCSTATE_MI     "fsMIRstRootInconsistentState"
#define RST_MIB_OBJ_OLD_PORTSTATE_MI  "fsMIRstPortOldPortState"
#define RST_MIB_OBJ_BASE_BRIDGE_ADDR_SI  "dot1dBaseBridgeAddress"
#define RST_MIB_OBJ_GEN_TRAP_TYPE_SI     "fsRstGenTrapType"
#define RST_MIB_OBJ_ERR_TRAP_TYPE_SI     "fsRstErrTrapType"
#define RST_MIB_OBJ_OLD_DESIG_ROOT_SI    "fsRstOldDesignatedRoot"
#define RST_MIB_OBJ_DESIG_ROOT_SI        "dot1dStpDesignatedRoot"
#define RST_MIB_OBJ_PORT_TRAP_INDEX_SI   "fsRstPortTrapIndex"
#define RST_MIB_OBJ_STP_VERSION_SI       "dot1dStpVersion"
#define RST_MIB_OBJ_MIGRATION_TYPE_SI    "fsRstPortMigrationType"
#define RST_MIB_OBJ_PKT_ERR_TYPE_SI      "fsRstPktErrType"
#define RST_MIB_OBJ_PKT_ERR_VAL_SI       "fsRstPktErrVal"
#define RST_MIB_OBJ_PREVIOUS_ROLE_SI     "fsRstOldRoleType" 
#define RST_MIB_OBJ_SELECTED_ROLE_SI     "fsRstPortRoleType"
#define RST_MIB_OBJ_PORT_STATE_SI        "dot1dStpPortState"
#define RST_MIB_OBJ_LOOP_INCSTATE_SI     "fsRstPortLoopInconsistentState"
#define RST_MIB_OBJ_BPDU_INCSTATE_SI     "fsRstPortBpduInconsistentState"
#define RST_MIB_OBJ_ROOT_INCSTATE_SI     "fsRstRootInconsistentState"
#define RST_MIB_OBJ_OLD_PORTSTATE_SI  "fsRstPortOldPortState"

#define RST_MIB_OBJ_BASE_BRIDGE_ADDR \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_BASE_BRIDGE_ADDR_SI:  RST_MIB_OBJ_BASE_BRIDGE_ADDR_MI)

#define RST_MIB_OBJ_GEN_TRAP_TYPE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_GEN_TRAP_TYPE_SI:  RST_MIB_OBJ_GEN_TRAP_TYPE_MI)

#define RST_MIB_OBJ_ERR_TRAP_TYPE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_ERR_TRAP_TYPE_SI:  RST_MIB_OBJ_ERR_TRAP_TYPE_MI)

#define RST_MIB_OBJ_OLD_DESIG_ROOT \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_OLD_DESIG_ROOT_SI:  RST_MIB_OBJ_OLD_DESIG_ROOT_MI)

#define RST_MIB_OBJ_DESIG_ROOT \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_DESIG_ROOT_SI:  RST_MIB_OBJ_DESIG_ROOT_MI)

#define RST_MIB_OBJ_PORT_TRAP_INDEX \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_PORT_TRAP_INDEX_SI:  RST_MIB_OBJ_PORT_TRAP_INDEX_MI)

#define RST_MIB_OBJ_STP_VERSION \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_STP_VERSION_SI:  RST_MIB_OBJ_STP_VERSION_MI)

#define RST_MIB_OBJ_MIGRATION_TYPE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_MIGRATION_TYPE_SI:  RST_MIB_OBJ_MIGRATION_TYPE_MI)

#define RST_MIB_OBJ_PKT_ERR_TYPE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_PKT_ERR_TYPE_SI:  RST_MIB_OBJ_PKT_ERR_TYPE_MI)

#define RST_MIB_OBJ_PKT_ERR_VAL \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_PKT_ERR_VAL_SI:  RST_MIB_OBJ_PKT_ERR_VAL_MI)

#define  RST_MIB_OBJ_PREVIOUS_ROLE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_PREVIOUS_ROLE_SI :  RST_MIB_OBJ_PREVIOUS_ROLE_MI)

#define RST_MIB_OBJ_SELECTED_ROLE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_SELECTED_ROLE_SI:  RST_MIB_OBJ_SELECTED_ROLE_MI)

#define RST_MIB_OBJ_PORT_STATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  RST_MIB_OBJ_PORT_STATE_SI:  RST_MIB_OBJ_PORT_STATE_MI)

#define RST_MIB_OBJ_LOOP_INCSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? RST_MIB_OBJ_LOOP_INCSTATE_SI : RST_MIB_OBJ_LOOP_INCSTATE_MI)

#define RST_MIB_OBJ_BPDU_INCSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? RST_MIB_OBJ_BPDU_INCSTATE_SI : RST_MIB_OBJ_BPDU_INCSTATE_MI)

#define RST_MIB_OBJ_ROOT_INCSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? RST_MIB_OBJ_ROOT_INCSTATE_SI : RST_MIB_OBJ_ROOT_INCSTATE_MI)

#define RST_MIB_OBJ_OLD_PORTSTATE \
    ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? RST_MIB_OBJ_OLD_PORTSTATE_SI : RST_MIB_OBJ_OLD_PORTSTATE_MI)

typedef struct _AstBrgGenTraps{
  tAstMacAddr   BrgAddr;
  UINT2    u2TrapMsgVal;
  UINT4    u4InstUpCount; /* Used to fill MST inst specific info */
  UINT4    u4InstDownCount;
  UINT1    u1GenTrapType;
  UINT1    au1Reserved[3];
} tAstBrgGenTraps;    

typedef struct _AstBrgErrTraps{
  tAstMacAddr   BrgAddr;
  UINT1    u1ErrTrapType;
  UINT1    u1Reserved;
} tAstBrgErrTraps;    

typedef struct _AstBrgTopologyChgTrap{
  tAstMacAddr   BrgAddr;
  UINT2         u2MstInst; /* Used for alignment in RSTP, for Inst in MSTP */
  UINT4         u4TopChanges; 
} tAstBrgTopologyChgTrap  ;    

typedef struct _AstBrgNewRootTrap{
  tAstBridgeId  AstOldRoot;
  tAstBridgeId  AstRoot;
  tAstMacAddr   BrgAddr;
  UINT2         u2MstInst; /* Used for alignment in RSTP, for Inst in MSTP */
} tAstBrgNewRootTrap;    

typedef struct _AstNewPortRoleTrap{
  tAstMacAddr  BrgAddr;
  UINT2  u2PortNum;
  UINT1  u1PreviousRole;
  UINT1  u1SelectedRole;
  UINT2  u2MstInst; /* Used for alignment in RSTP, for Inst in MSTP */
} tAstNewPortRoleTrap;

typedef struct _AstBrgProtocolMigrationTrap{
  tAstMacAddr   BrgAddr;
  UINT2         u2Port;
  UINT1         u1Version;
  UINT1    u1GenTrapType;
  UINT1    au1Reserved[2];
} tAstBrgProtocolMigrationTrap;    

typedef struct _AstBrgInvalidBpduRxdTrap{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2ErrVal;
  UINT1   u1ErrTrapType;
  UINT1   u1Reserved;
} tAstBrgInvalidBpduRxdTrap;    


typedef struct _AstBrgHwFailTrap{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2MstInst; /* Used for alignment in RSTP, for Inst in MSTP */
  UINT2   u2PortState;
} tAstBrgHwFailTrap;    

typedef struct _AstBrgLoopIncStateChange{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2InstId;
  UINT1    au1Reserved[2];
  UINT4   u4LoopInconsistentState;
}tAstBrgLoopIncStateChange;

typedef struct _AstBrgBPDUIncStateChange{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2InstId;
  UINT1    au1Reserved[2];
  UINT4   u4BPDUInconsistentState;
}tAstBrgBPDUIncStateChange;

typedef struct _AstBrgRootIncStateChange{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2InstId;
  UINT1    au1Reserved[2];
  UINT4   u4RootInconsistentState;
}tAstBrgRootIncStateChange;

typedef struct _AstBrgPortStateChange{
  tAstMacAddr   BrgAddr;
  UINT2   u2Port;
  UINT2   u2InstId;
  UINT2   u2PortState;
  UINT4   u4OldPortState;
}tAstBrgPortStateChange;

#endif


