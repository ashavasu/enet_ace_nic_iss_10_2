/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: stdrslow.h,v 1.7 2008/08/20 15:27:43 iss Exp $
 *
 * Description: This system-generated file contains the prototypes 
 *              of the SNMP low-level functions of the RSTP 
 *              standard (draft) MIB objects.
 *
 *******************************************************************/
#ifndef _STDRSLOW_H
#define _STDRSLOW_H
/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhGetDot1dStpProtocolSpecification ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpPriority ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpTimeSinceTopologyChange ARG_LIST((UINT4 *));

INT1
nmhGetDot1dStpTopChanges ARG_LIST((UINT4 *));

INT1
nmhGetDot1dStpDesignatedRoot ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dStpRootCost ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpRootPort ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpMaxAge ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpHelloTime ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpHoldTime ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpForwardDelay ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpBridgeMaxAge ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpBridgeHelloTime ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpBridgeForwardDelay ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpVersion ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpTxHoldCount ARG_LIST((INT4 *));

INT1
nmhGetDot1dStpPortPathCost32 ARG_LIST((INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dStpPriority ARG_LIST((INT4 ));

INT1
nmhSetDot1dStpBridgeMaxAge ARG_LIST((INT4 ));

INT1
nmhSetDot1dStpBridgeHelloTime ARG_LIST((INT4 ));

INT1
nmhSetDot1dStpBridgeForwardDelay ARG_LIST((INT4 ));

INT1
nmhSetDot1dStpVersion ARG_LIST((INT4 ));

INT1
nmhSetDot1dStpTxHoldCount ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dStpPriority ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dStpBridgeMaxAge ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dStpBridgeHelloTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dStpBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dStpVersion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dStpTxHoldCount ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for Dot1dStpPortTable. */
INT1
nmhValidateIndexInstanceDot1dStpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dStpPortTable  */

INT1
nmhGetFirstIndexDot1dStpPortTable ARG_LIST((INT4 *));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1dStpVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1dStpTxHoldCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dStpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dStpPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dStpPortDesignatedCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortDesignatedBridge ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dStpPortDesignatedPort ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dStpPortForwardTransitions ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dStpPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortPathCost32 ARG_LIST((INT4  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dStpPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortPathCost32 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Proto Validate Index Instance for Dot1dStpExtPortTable. */
INT1
nmhValidateIndexInstanceDot1dStpExtPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dStpExtPortTable  */

INT1
nmhGetFirstIndexDot1dStpExtPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dStpExtPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dStpPortProtocolMigration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortAdminEdgePort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortOperEdgePort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortAdminPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortOperPointToPoint ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dStpPortAdminPathCost ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dStpPortProtocolMigration ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortAdminEdgePort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortAdminPointToPoint ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dStpPortAdminPathCost ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dStpPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortAdminEdgePort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortAdminPointToPoint ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dStpPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhDepv2Dot1dStpPriority ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1dStpBridgeMaxAge ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND *));

INT1
nmhDepv2Dot1dStpBridgeHelloTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1dStpBridgeForwardDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2Dot1dStpPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1dTpAgingTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1dStpExtPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif /*_STDRSLOW_H*/
