/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbrsdb.h,v 1.5 2008/08/20 15:27:43 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPBRSDB_H
#define _FSPBRSDB_H

UINT1 FsPbRstCVlanBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbRstCVlanPortInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbRstCVlanPortSmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbRstCVlanPortStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fspbrs [] ={1,3,6,1,4,1,2076,123};
tSNMP_OID_TYPE fspbrsOID = {8, fspbrs};


UINT4 FsPbProviderStpStatus [ ] ={1,3,6,1,4,1,2076,123,1,1};
UINT4 FsPbRstPort [ ] ={1,3,6,1,4,1,2076,123,1,2,1,1};
UINT4 FsPbRstCVlanBridgeId [ ] ={1,3,6,1,4,1,2076,123,1,2,1,2};
UINT4 FsPbRstCVlanBridgeDesignatedRoot [ ] ={1,3,6,1,4,1,2076,123,1,2,1,3};
UINT4 FsPbRstCVlanBridgeRootCost [ ] ={1,3,6,1,4,1,2076,123,1,2,1,4};
UINT4 FsPbRstCVlanBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,123,1,2,1,5};
UINT4 FsPbRstCVlanBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,123,1,2,1,6};
UINT4 FsPbRstCVlanBridgeHoldTime [ ] ={1,3,6,1,4,1,2076,123,1,2,1,7};
UINT4 FsPbRstCVlanBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,123,1,2,1,8};
UINT4 FsPbRstCVlanBridgeTxHoldCount [ ] ={1,3,6,1,4,1,2076,123,1,2,1,9};
UINT4 FsPbRstCVlanStpHelloTime [ ] ={1,3,6,1,4,1,2076,123,1,2,1,10};
UINT4 FsPbRstCVlanStpMaxAge [ ] ={1,3,6,1,4,1,2076,123,1,2,1,11};
UINT4 FsPbRstCVlanStpForwardDelay [ ] ={1,3,6,1,4,1,2076,123,1,2,1,12};
UINT4 FsPbRstCVlanStpTopChanges [ ] ={1,3,6,1,4,1,2076,123,1,2,1,13};
UINT4 FsPbRstCVlanStpTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,123,1,2,1,14};
UINT4 FsPbRstCepSvid [ ] ={1,3,6,1,4,1,2076,123,1,3,1,1};
UINT4 FsPbRstCVlanPortPriority [ ] ={1,3,6,1,4,1,2076,123,1,3,1,2};
UINT4 FsPbRstCVlanPortPathCost [ ] ={1,3,6,1,4,1,2076,123,1,3,1,3};
UINT4 FsPbRstCVlanPortRole [ ] ={1,3,6,1,4,1,2076,123,1,3,1,4};
UINT4 FsPbRstCVlanPortState [ ] ={1,3,6,1,4,1,2076,123,1,3,1,5};
UINT4 FsPbRstCVlanPortAdminEdgePort [ ] ={1,3,6,1,4,1,2076,123,1,3,1,6};
UINT4 FsPbRstCVlanPortOperEdgePort [ ] ={1,3,6,1,4,1,2076,123,1,3,1,7};
UINT4 FsPbRstCVlanPortAdminPointToPoint [ ] ={1,3,6,1,4,1,2076,123,1,3,1,8};
UINT4 FsPbRstCVlanPortOperPointToPoint [ ] ={1,3,6,1,4,1,2076,123,1,3,1,9};
UINT4 FsPbRstCVlanPortAutoEdge [ ] ={1,3,6,1,4,1,2076,123,1,3,1,10};
UINT4 FsPbRstCVlanPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,123,1,3,1,11};
UINT4 FsPbRstCVlanPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,123,1,3,1,12};
UINT4 FsPbRstCVlanPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,123,1,3,1,13};
UINT4 FsPbRstCVlanPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,123,1,3,1,14};
UINT4 FsPbRstCVlanPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,123,1,3,1,15};
UINT4 FsPbRstCVlanPortInfoSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,1};
UINT4 FsPbRstCVlanPortMigSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,2};
UINT4 FsPbRstCVlanPortRoleTransSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,3};
UINT4 FsPbRstCVlanPortStateTransSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,4};
UINT4 FsPbRstCVlanPortTopoChSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,5};
UINT4 FsPbRstCVlanPortTxSmState [ ] ={1,3,6,1,4,1,2076,123,1,4,1,6};
UINT4 FsPbRstCVlanPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,1};
UINT4 FsPbRstCVlanPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,2};
UINT4 FsPbRstCVlanPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,3};
UINT4 FsPbRstCVlanPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,4};
UINT4 FsPbRstCVlanPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,5};
UINT4 FsPbRstCVlanPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,6};
UINT4 FsPbRstCVlanPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,7};
UINT4 FsPbRstCVlanPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,8};
UINT4 FsPbRstCVlanPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,9};
UINT4 FsPbRstCVlanPortProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,123,1,5,1,10};
UINT4 FsPbRstCVlanPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,123,1,5,1,11};


tMbDbEntry fspbrsMibEntry[]= {

{{10,FsPbProviderStpStatus}, NULL, FsPbProviderStpStatusGet, FsPbProviderStpStatusSet, FsPbProviderStpStatusTest, FsPbProviderStpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsPbRstPort}, GetNextIndexFsPbRstCVlanBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeId}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeDesignatedRoot}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeRootCost}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeMaxAge}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeHelloTime}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeHoldTime}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeForwardDelay}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanBridgeTxHoldCount}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanBridgeTxHoldCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanStpHelloTime}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanStpHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanStpMaxAge}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanStpMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanStpForwardDelay}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanStpForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanStpTopChanges}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanStpTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCVlanStpTimeSinceTopologyChange}, GetNextIndexFsPbRstCVlanBridgeTable, FsPbRstCVlanStpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPbRstCVlanBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsPbRstCepSvid}, GetNextIndexFsPbRstCVlanPortInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortPriority}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortPathCost}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortRole}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortState}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortAdminEdgePort}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortAdminEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortOperEdgePort}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortAdminPointToPoint}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortAdminPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortOperPointToPoint}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortAutoEdge}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortAutoEdgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortDesignatedRoot}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortDesignatedCost}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortDesignatedBridge}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortDesignatedPort}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortForwardTransitions}, GetNextIndexFsPbRstCVlanPortInfoTable, FsPbRstCVlanPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortInfoSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortInfoSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortMigSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortMigSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortRoleTransSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortRoleTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortStateTransSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortStateTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortTopoChSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortTopoChSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortTxSmState}, GetNextIndexFsPbRstCVlanPortSmTable, FsPbRstCVlanPortTxSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortSmTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortRxRstBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortRxConfigBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortRxTcnBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortTxRstBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortTxConfigBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortTxTcnBpduCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortInvalidRstBpduRxCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortInvalidConfigBpduRxCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortInvalidTcnBpduRxCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortProtocolMigrationCount}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRstCVlanPortEffectivePortState}, GetNextIndexFsPbRstCVlanPortStatsTable, FsPbRstCVlanPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbRstCVlanPortStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData fspbrsEntry = { 47, fspbrsMibEntry };
#endif /* _FSPBRSDB_H */

