
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astpbred.h,v 1.7 2014/02/18 10:33:02 siva Exp $
 *
 * Description: This file contains the prototypes to support High 
 *              Availability for AST Provider Bridging feature.
 *
 ******************************************************************/
#ifndef AST_PB_RED_H
#define AST_PB_RED_H
#ifndef _ASTHDRS_H_
 #include "asthdrs.h"
#endif

INT4 AstRedPbCVlanInit (UINT2 u2CepPortNo);
INT4 AstRedPbCVlanPortTblDeInit (VOID);
VOID AstRedPbRelCvlanRedCompMem (UINT2 u2Port);
INT4 AstRedPbSelectCVlanContext (UINT2 u2ParentPort);
VOID AstRedPbRestoreContext (VOID);
VOID AstRedClearAllCvlanSyncUpData (VOID);
INT4 AstRedPbIncrCvlanPortTableSize (VOID);
INT4 AstRedPbCreateCvlanPort (UINT2 u2LocalPort);
VOID AstRedPbDeleteCvlanPort (UINT2 u2Port);
INT4 AstRedPbSelCvlanAndGetLocalPort (UINT2 u2CepPortNo, UINT2 u2ProtocolPort,
                                      UINT2 *pu2LocalPort);
VOID AstRedPbHandleBulkUptOnCep (UINT2 u2CepPortNo, VOID **ppBuf, 
                                 UINT4 *pu4Offset, UINT4 *pu4BulkUpdPortCnt);
VOID AstRedPbApplyCvlanTimers (tAstPortEntry *pParentPortEntry);
INT4 AstRedPbSelParentAndCvlanCntxt (UINT4 u4ContextId, UINT2 u2PortIndex);
INT4 AstRedPbCvlanHwAudit (UINT4 u4ContextId, UINT2 u2PortIndex);

INT4 AstRedCVlanCrtPortTblMemPool (UINT4 u4BlockSize, 
                                   tAstMemPoolId *pRedPortTblPool);
VOID AstRedPbUpdtCvlanPortStates (UINT2  u2CepPortNo);
VOID AstRedPbDumpSyncedUpCvlanPdus (UINT2 u2CepPortNo);
VOID AstRedDumpSyncedUpCvlanData (UINT2 u2CepPortNo);
VOID AstRedDumpSyncUpCvlanOutputs (UINT2 u2CepPortNo);
VOID AstRedDumpAllCvlanSyncUpOutputs (VOID);
VOID AstRedDumpSyncUpAllCvlanTmrData (VOID);
VOID AstRedDumpAllCvlanSyncUpPdus (VOID);
VOID AstRedDumpSyncUpCvlanTmrData (UINT2 u2CepPortNo);


INT4 AstRedHandleHwPepCreate (tAstMsgNode * pMsgNode);

#endif
