/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1w1lw.h,v 1.3 2011/12/20 14:14:16 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021SpanningTreeTable. */
INT1
nmhValidateIndexInstanceIeee8021SpanningTreeTable ARG_LIST((UINT4 ));


/* Proto Type for Low Level GET FIRST fn for Ieee8021SpanningTreeTable  */

INT1
nmhGetFirstIndexIeee8021SpanningTreeTable ARG_LIST((UINT4 *));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021SpanningTreeTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021SpanningTreeProtocolSpecification ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeTimeSinceTopologyChange ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021SpanningTreeTopChanges ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021SpanningTreeDesignatedRoot ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021SpanningTreeRootCost ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeRootPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021SpanningTreeMaxAge ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeHelloTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeHoldTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeForwardDelay ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeBridgeMaxAge ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeBridgeHelloTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeBridgeForwardDelay ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeVersion ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeRstpTxHoldCount ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021SpanningTreePriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeBridgeMaxAge ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeBridgeHelloTime ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeBridgeForwardDelay ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeVersion ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeRstpTxHoldCount ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021SpanningTreePriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeBridgeMaxAge ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeBridgeHelloTime ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeBridgeForwardDelay ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeVersion ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeRstpTxHoldCount ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021SpanningTreeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021SpanningTreePortTable. */
INT1
nmhValidateIndexInstanceIeee8021SpanningTreePortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021SpanningTreePortTable  */

INT1
nmhGetFirstIndexIeee8021SpanningTreePortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021SpanningTreePortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021SpanningTreePortPriority ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePortState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePortEnabled ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePortPathCost ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePortDesignatedRoot ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021SpanningTreePortDesignatedCost ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreePortDesignatedBridge ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021SpanningTreePortDesignatedPort ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021SpanningTreePortForwardTransitions ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021SpanningTreeRstpPortProtocolMigration ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeRstpPortAdminEdgePort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeRstpPortOperEdgePort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021SpanningTreeRstpPortAdminPathCost ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021SpanningTreePortPriority ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreePortEnabled ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreePortPathCost ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeRstpPortProtocolMigration ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeRstpPortAdminEdgePort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021SpanningTreeRstpPortAdminPathCost ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021SpanningTreePortPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreePortEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreePortPathCost ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeRstpPortProtocolMigration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeRstpPortAdminEdgePort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021SpanningTreeRstpPortAdminPathCost ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021SpanningTreePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/************/
