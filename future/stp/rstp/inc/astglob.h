/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astglob.h,v 1.31 2017/11/30 06:29:48 siva Exp $
 *
 * Description: This file contains all global variables used by
 *              RSTP, PVRST and MSTP modules. 
 *
 *******************************************************************/


#ifndef _ASTGLOB_H_
#define _ASTGLOB_H_


#ifdef _ASTPMSG_C_
tAstGlobalInfo gAstGlobalInfo;
tAstContextInfo *gpAstContextInfo = NULL; /* Pointer to the current 
                                             context information */
/* Pointer to the Parent Context. This parent ctxt information should be used
 * only for the CVLAN component of Provider Edge Bridge*/
tAstContextInfo *gpAstParentCtxtInfo = NULL; 

UINT4            gu4StpCliContext=0;
UINT4            gu4RecentTopoChPort = 0;
UINT1            gu1IsAstInitialised = RST_FALSE;
UINT1            gau1AstSystemControl [SYS_DEF_MAX_NUM_CONTEXTS];
UINT1            gau1AstModuleStatus [SYS_DEF_MAX_NUM_CONTEXTS];
UINT1            gau1SpaceString[2] = " ";
tVlanId          gAstDefaultVlanId;
BOOL1            gbIsPortDeleted = OSIX_FALSE; /* This variable is used to prevent
                                                * the sending of sync-up messages
                                                * to standby if the port has
                                                * been  deleted.*/
UINT2         gu2AstRecvPort;
INT1          gi1MstTxAllowed;
UINT4         gCurrContextId;
UINT1         gu1EnablePortVlanFlushOpt =  AST_FALSE;
               /* This variable indicates whether port and vlan
                * based optimised flushing must be done */


#ifdef PVRST_WANTED   
tVlanId         gPvrstVlanIdList[VLAN_MAX_COMMUNITY_VLANS+1];   
tLocalPortList           gPvrstNullPortList;
UINT1            gu1PvrstInstExistance = RST_TRUE;
       /* This variable indicates whether the instance should be present
        * or not when there no member ports in the VLAN
        * If RST_TRUE  : Instance existance irrespective of member ports in the VLAN
        * If RST_FALSE : Instance exists only if there are one member port in the VLAN. */
#endif 
UINT1         gAstStateTransStatus = AST_TRANS_FINISHED;
UINT1         gAstAllowDirectNpTx = RST_FALSE;
        /* This variable indicates whether the STP packet should be directly handed over to NP or 
         * it should be given to L2Iwf during Tx */
/******************************************************************************/
/*                         RSTP Sizing Params Constants                       */
/******************************************************************************/

tFsModSizingParams gFsRstpSizingParams [] =
{
  
#ifdef L2RED_WANTED
    /* C-VLAN Port Table Mempool. This mempool is allocated dynamically.
     * Hence the number of units and size mentioned here are not taken
     * into consideration during mempool creation for this table. */
    {"C-VLAN Port Table", "AST_PB_PORT_TBL_MEM_BLK_COUNT",
     (sizeof (tAstRedPortInfo *) * 1000), AST_PB_PORT_TBL_MEM_BLK_COUNT,
     AST_PB_PORT_TBL_MEM_BLK_COUNT, 0},

   {"AST_RED_DATA_MEMBLK_SIZE", "AST_RED_DATA_MEMBLK_CNT",
     (UINT4) AST_RED_DATA_MEMBLK_SIZE, AST_RED_DATA_MEMBLK_CNT, 
     AST_RED_DATA_MEMBLK_CNT, 0},

    {"AST_RED_TIMES_MEMBLK_SIZE", "AST_RED_TIMES_MEMBLK_CNT", 
     (UINT4) AST_RED_TIMES_MEMBLK_SIZE, AST_RED_TIMES_MEMBLK_CNT,
     AST_RED_TIMES_MEMBLK_CNT, 0},
#endif

    {"AST_MAX_PORTS_PER_CONTEXT * sizeof (tAstPortEntry *)", 
     "AST_PB_PORT_TBL_MEM_BLK_COUNT", 
     AST_MAX_PORTS_PER_CONTEXT * (sizeof (tAstPortEntry *)), 
     AST_PB_PORT_TBL_MEM_BLK_COUNT,
     AST_PB_PORT_TBL_MEM_BLK_COUNT, 0},

    {"AST_MAX_PORTS_PER_CONTEXT * sizeof (tAstPerStPortInfo *)", 
     "AST_PB_PORT_TBL_MEM_BLK_COUNT",
     AST_MAX_PORTS_PER_CONTEXT * (sizeof (tAstPerStPortInfo *)), 
     AST_PB_PORT_TBL_MEM_BLK_COUNT,
     AST_PB_PORT_TBL_MEM_BLK_COUNT, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsRstpSizingInfo;

#else
extern tAstGlobalInfo   gAstGlobalInfo;
extern tAstContextInfo *gpAstContextInfo;
extern tAstContextInfo *gpAstParentCtxtInfo;
extern UINT1            gu1IsAstInitialised;
extern UINT1            gau1AstSystemControl [SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1            gau1AstModuleStatus [SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1            gau1SpaceString[2];
extern tVlanId          gAstDefaultVlanId;
extern BOOL1            gbIsPortDeleted;
extern tFsModSizingParams gFsRstpSizingParams [];
extern tFsModSizingInfo gFsRstpSizingInfo;
extern UINT4            gu4RecentTopoChPort;
extern UINT1 gu1EnablePortVlanFlushOpt; 


#ifdef PVRST_WANTED   
extern tVlanId         gPvrstVlanIdList[VLAN_MAX_COMMUNITY_VLANS+1];   
extern tLocalPortList           gPvrstNullPortList;
extern UINT1            gu1PvrstInstExistance; 
           /* This variable indicates whether the instance should be present
            * or not when there no member ports in the VLAN
            * If RST_TRUE : Instance existance irrespective of member ports in the VLAN.
            * If RST_FALSE: Instance exists only if there are one member port in the VLAN. */
#endif 
extern UINT1 gAstStateTransStatus;
extern UINT1 gAstAllowDirectNpTx;
#endif /* _ASTPMSG_C_ */

/* The same is used in Vlan in vlan.c as 
 * gau1PortBitMaskMap. So when ever this array is updated, update 
 * vlan.c also
 */

#ifdef _ASTRSYS_C
UINT1  gau1MstPortBitMaskMap[VLAN_PORTS_PER_BYTE] =
           { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };
#else

extern UINT1 gau1MstPortBitMaskMap[VLAN_PORTS_PER_BYTE];

#endif /* _ASTRSYS_C */

/******************************************************************************/
/*                         SEM Trace Message Constants                        */
/******************************************************************************/
#ifdef _ASTPMSG_C_
UINT1 gaaau1AstSemState[AST_MAX_SEM][AST_MAX_STATES][AST_MAX_SEM_LEN];
UINT1 gaaau1AstSemEvent[AST_MAX_SEM][AST_MAX_EVENTS][AST_MAX_SEM_LEN];
#else
extern UINT1 gaaau1AstSemState[AST_MAX_SEM][AST_MAX_STATES][AST_MAX_SEM_LEN];
extern UINT1 gaaau1AstSemEvent[AST_MAX_SEM][AST_MAX_EVENTS][AST_MAX_SEM_LEN];
#endif /* ASTPMSG_C */ 
#endif /* _ASTGLOB_H_ */

