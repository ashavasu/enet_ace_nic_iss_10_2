/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmacr.h,v 1.93 2017/11/20 13:12:24 siva Exp $
 *
 * Description: This file contains all the macros used by RSTP and
 *              MSTP Modules.
 *
 *******************************************************************/


#ifndef _ASTMACR_H_
#define _ASTMACR_H_

/* Number of virtual contexts supported by Spanning Tree. */
#define AST_INVALID_CONTEXT (AST_SIZING_CONTEXT_COUNT + 1)

#define AST_SIZING_CONTEXT_COUNT    FsASTSizingParams[MAX_AST_CONTEXTS_SIZING_ID].u4PreAllocatedUnits

/* UTL SLL related macros for ports*/ 
#define  AST_GET_FIRST(pList)\
         TMO_SLL_First((tAstSll *)pList) 
      
#define  AST_INIT_PORT_BUF(pList) \
         TMO_SLL_Init((tAstSll *)pList) 
      
#define  AST_ADD_PORT_BUF(pList, pNode) \
         TMO_SLL_Add((tAstSll *)pList, (tAstSllNode  *)pNode) 
      
#define  AST_COUNT_PORT_BUF(pList) \
         TMO_SLL_Count((tAstSll *)pList) 
      
#define  AST_SCAN_PORT_BUF(pList, pNode, Typecast) \
         TMO_SLL_Scan((tAstSll *)pList, pNode, Typecast) 
      
#define  AST_DELETE_PORT_BUF(pList,pNode) \
         TMO_SLL_Delete((tAstSll *)pList, (tAstSllNode  *) (VOID *) pNode) 
      
/* UTL SLL related macros for Instance*/

#define  AST_GET_FIRST_INST(pList)\
         TMO_SLL_First((tAstSll *)pList)

#define  AST_INIT_INST_BUF(pList) \
         TMO_SLL_Init((tAstSll *)pList)

#define  AST_ADD_INST_BUF(pList, pNode) \
         TMO_SLL_Add((tAstSll *)pList, (tAstSllNode  *)pNode)

#define  AST_COUNT_INST_BUF(pList) \
         TMO_SLL_Count((tAstSll *)pList)

#define  AST_SCAN_INST_BUF(pList, pNode, Typecast) \
         TMO_SLL_Scan((tAstSll *)pList, pNode, Typecast)

#define  AST_DELETE_INST_BUF(pList,pNode) \
         TMO_SLL_Delete((tAstSll *)pList, (tAstSllNode  *) (VOID *) pNode)


/******************************************************************************/
/* FSAP related defines */
/******************************************************************************/

#define tAstAppTimer        tTmrAppTimer
#define tAstTimerCfg        tTimerCfg
#define tAstTmrListId       tTimerListId
#define tAstTaskId          tOsixTaskId
#define tAstSysTime         tOsixSysTime
#define tAstMemPoolId       tMemPoolId
#define tAstOsixSemId       tOsixSemId
#define tAstQId             tOsixQId



/******************************************************************************/
/* Count of Number of Memory Blocks per Memory Pool */
/******************************************************************************/

/* In the case of RSTP, the number of instances is 1. */
#define RST_PERSTINFO_MEMBLK_COUNT  1
#define RST_PERST_PORT_INFO_MEMBLK_COUNT (AST_MAX_PORTS_IN_SYSTEM * 1)

#define AST_AVG_LOCAL_MSGS          10



/******************************************************************************/
/* Other Memory Related Defines */
/******************************************************************************/

#define AST_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

#define AST_GET_MEMORY_TYPE() \
    ((AstVcmGetSystemMode (AST_PROTOCOL_ID) == VCM_SI_MODE) \
     ? MEM_DEFAULT_MEMORY_TYPE : MEM_HEAP_MEMORY_TYPE)


/******************************************************************************/
/* Bridge related macros */
/******************************************************************************/

#define AST_BRIDGE_SUCCESS           BRIDGE_SUCCESS

/******************************************************************************/
/* Memory Pool Id & List Id definitions */
/******************************************************************************/
#define RST_PORTINFO_MEMPOOL_ID    gAstGlobalInfo.RstPortInfoMemPoolId   
#define MST_INSTINFO_MEMPOOL_ID    gAstGlobalInfo.MstInstInfoMemPoolId
#ifdef MSTP_WANTED
#define MST_VLANMAP_MEMPOOL_ID     gAstGlobalInfo.MstVlanMapMemPoolId
#define MST_VLANLIST_MEMPOOL_ID     gAstGlobalInfo.MstVlanListMemPoolId
#endif
#define AST_ETHFRAME_SIZE_MEMPOOL_ID gAstGlobalInfo.FrameSizeMemPoolId
#define AST_CONTEXT_MEMPOOL_ID     gAstGlobalInfo.ContextMemPoolId
#define AST_TMR_MEMPOOL_ID             gAstGlobalInfo.TmrMemPoolId
#define AST_PORT_INFO_MEMPOOL_ID       gAstGlobalInfo.PortInfoMemPoolId
#define AST_PERST_INFO_MEMPOOL_ID      gAstGlobalInfo.PerStInfoMemPoolId
#define AST_PERST_PORT_INFO_MEMPOOL_ID gAstGlobalInfo.PerStPortInfoMemPoolId
#define AST_LOCALMSG_MEMPOOL_ID        gAstGlobalInfo.LocalMsgMemPoolId
#define AST_QMSG_MEMPOOL_ID            gAstGlobalInfo.QMsgMemPoolId
#define AST_INPUT_QID                  gAstGlobalInfo.InputQId
#define AST_CFG_QMSG_MEMPOOL_ID        gAstGlobalInfo.CfgQMsgMemPooIId
#define AST_PORT_TBL_MEMPOOL_ID        gAstGlobalInfo.PortTableMemPoolId
#define AST_PERST_PORT_TBL_MEMPOOL_ID  gAstGlobalInfo.PerStPortTablePoolId
#define AST_PERST_TBL_MEMPOOL_ID       gAstGlobalInfo.PerStInfoTablePoolId
#define AST_CFG_QID                    gAstGlobalInfo.CfgQId
#define AST_TASK_ID                    gAstGlobalInfo.TaskId
#define AST_INSTANCE_UPCOUNT_MEMPOOL_ID gAstGlobalInfo.UpCountMemPoolId
#define AST_MST_DIGEST_INPUT_MEMPOOL_ID gAstGlobalInfo.DigestInpMemPoolId
#define AST_BPDU_TYPE_MEMPOOL_ID        gAstGlobalInfo.BpdutypeMemPoolId

#define AST_TMR_LIST_ID           gAstGlobalInfo.TmrListId  
#define AST_MSG_LIST_ID           gAstGlobalInfo.MsgSllId

/******************************************************************************/
/* State Machine Variables */
/******************************************************************************/
#define RST_PORT_RECEIVE_MACHINE  gAstGlobalInfo.aaPortRcvMachine
#define RST_PORT_INFO_MACHINE     gAstGlobalInfo.aaPortInfoMachine
#define RST_PORT_ROLE_SEL_MACHINE gAstGlobalInfo.aaPortRoleSelMachine
#define RST_PORT_ROLE_TR_MACHINE  gAstGlobalInfo.aaPortRoleTrMachine
#define RST_PORT_STATE_TR_MACHINE gAstGlobalInfo.aaPortStateTrMachine
#define RST_PORT_MIG_MACHINE      gAstGlobalInfo.aaPortMigMachine
#define RST_TOPO_CH_MACHINE       gAstGlobalInfo.aaTopoChMachine
#define RST_PORT_TX_MACHINE       gAstGlobalInfo.aaPortTxMachine
#define RST_BRG_DET_MACHINE       gAstGlobalInfo.aaBrgDetStateMachine   
#define RST_PORT_PSEUDO_INFO_MACHINE  gAstGlobalInfo.aaPortPseudoInfoMachine

/******************************************************************************/
/* Return Values */
/******************************************************************************/

#define AST_MEM_SUCCESS         MEM_SUCCESS
#define AST_MEM_FAILURE         MEM_FAILURE
#define AST_TMR_SUCCESS         TMR_SUCCESS
#define AST_TMR_FAILURE         TMR_FAILURE
#define AST_OSIX_SUCCESS        OSIX_SUCCESS
#define AST_OSIX_FAILURE        OSIX_FAILURE
#define AST_CRU_SUCCESS         CRU_SUCCESS
#define AST_CRU_FAILURE         CRU_FAILURE

#define AST_CRU_FALSE           FALSE
#define AST_CRU_TRUE            TRUE

   

/******************************************************************************/
/* Defines of Library functions used */
/******************************************************************************/
   
#define AST_MEMCPY              MEMCPY
#define AST_MEMCMP              MEMCMP
#define AST_MEMSET              MEMSET
#define AST_MEMFREE             MEM_FREE
#define AST_MALLOC              MEM_MALLOC

#define AST_STRCPY              STRCPY
#define AST_STRNCPY             STRNCPY
#define AST_STRLEN              STRLEN
#define AST_SPRINTF             SPRINTF

/******************************************************************************/
/* Macros for creating memory pools */
/******************************************************************************/

#define AST_CREATE_MEM_POOL(BlockSize, NumOfBlocks, MemoryType, pPoolId) \
           MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                            MemoryType, (tAstMemPoolId *)(pPoolId))
                            
#define AST_CREATE_TMR_MEM_POOL(BlockSize,NumOfBlocks, MemoryType, pPoolId) \
           MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                            MemoryType, (tAstMemPoolId*)(pPoolId))

#define AST_CREATE_PERST_INFO_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                            (UINT4)AST_MEMORY_TYPE, (tAstMemPoolId*)(pPoolId))

#define AST_CREATE_PERST_PORT_INFO_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                            (UINT4)AST_MEMORY_TYPE, (tAstMemPoolId*)(pPoolId))

#define AST_CREATE_LOCALMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                            (UINT4)(AST_MEMORY_TYPE),(tAstMemPoolId*)(pPoolId))

#define AST_CREATE_QMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                            (UINT4)(AST_MEMORY_TYPE),(tAstMemPoolId*)(pPoolId))

#define AST_CREATE_CFGQ_MSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                            (UINT4)(AST_MEMORY_TYPE),(tAstMemPoolId*)(pPoolId))


/******************************************************************************/
/* Macros for deleting Memory Pools */
/******************************************************************************/

#define AST_DELETE_MEM_POOL(pPoolId) \
           MemDeleteMemPool(pPoolId)
           
#define AST_DELETE_TMR_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)

#define AST_DELETE_PERST_INFO_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)

#define AST_DELETE_PERST_PORT_INFO_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)

#define AST_DELETE_LOCALMSG_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)

#define AST_DELETE_QMSG_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)

#define AST_DELETE_CFGQ_MSG_MEM_POOL(PoolId) \
           MemDeleteMemPool(PoolId)



/******************************************************************************/
/* Macros for allocating memory blocks */
/******************************************************************************/

#define AST_ALLOC_RST_PORTINFO_MEM_BLOCK(pNode) \
           (pNode = (tRstPortInfo *) MemAllocMemBlk(RST_PORTINFO_MEMPOOL_ID)) 
      
#define AST_ALLOC_MST_INSTINFO_MEM_BLOCK(pNode) \
            (pNode = (tMstInstInfo *) MemAllocMemBlk(MST_INSTINFO_MEMPOOL_ID))

#define AST_ALLOC_ETHFRAME_SIZE_MEM_BLOCK(pNode) \
        (pNode = (tAstFrameSize *) MemAllocMemBlk(AST_ETHFRAME_SIZE_MEMPOOL_ID))

#ifdef MSTP_WANTED
#define AST_ALLOC_MST_VLANMAP_MEM_BLOCK(pNode) \
        (pNode = (tMstVlanMap *) MemAllocMemBlk(MST_VLANMAP_MEMPOOL_ID))

#define AST_ALLOC_MST_VLANLIST_MEM_BLOCK(pNode) \
        (pNode = (tMstVlanList *) MemAllocMemBlk(MST_VLANLIST_MEMPOOL_ID))

#endif 
#define AST_ALLOC_CONTEXT_MEM_BLOCK(pNode) \
           (pNode = (tAstContextInfo *) MemAllocMemBlk(AST_CONTEXT_MEMPOOL_ID))
           
#define AST_ALLOC_TMR_MEM_BLOCK(pNode) \
        (pNode = (tAstTimer *) MemAllocMemBlk(AST_TMR_MEMPOOL_ID))
           
#define AST_ALLOC_PERST_INFO_MEM_BLOCK(ppPerStInfo) \
           (ppPerStInfo = (tAstPerStInfo *)\
            MemAllocMemBlk(AST_PERST_INFO_MEMPOOL_ID))
           
#define AST_ALLOC_PORT_INFO_MEM_BLOCK(pPortInfo) \
         (pPortInfo = (tAstPortEntry *) MemAllocMemBlk(AST_PORT_INFO_MEMPOOL_ID))

#define AST_ALLOC_PERST_PORT_INFO_MEM_BLOCK(pPerStPortInfo) \
               (pPerStPortInfo = (tAstPerStPortInfo *) MemAllocMemBlk\
                            (AST_PERST_PORT_INFO_MEMPOOL_ID))

#define AST_ALLOC_LOCALMSG_MEM_BLOCK MemAllocMemBlk(AST_LOCALMSG_MEMPOOL_ID)

#define AST_ALLOC_QMSG_MEM_BLOCK(pNode) \
           (pNode = (tAstQMsg *)MemAllocMemBlk(AST_QMSG_MEMPOOL_ID))

#define AST_ALLOC_CFGQ_MSG_MEM_BLOCK(pNode) \
           (pNode = (tAstQMsg *)MemAllocMemBlk(AST_CFG_QMSG_MEMPOOL_ID))

#define AST_ALLOC_PVRST_PERST_BRG_INFO_MEM_BLOCK(pNode) \
           (pNode = (tPerStPvrstBridgeInfo *)MemAllocMemBlk\
            (AST_PERST_PVRST_BRGINFO_MEMPOOL_ID))

#define AST_ALLOC_PVRST_PERST_RST_PORT_INFO_MEM_BLOCK(pNode) \
           (pNode = (tPerStPvrstRstPortInfo *)MemAllocMemBlk\
            (AST_PERST_PVRST_RST_PORTINFO_MEMPOOL_ID))

#define AST_ALLOC_PVRST_VLAN_TO_INDEX_MAP_BLOCK(pNode) \
           (pNode = (UINT2 *) MemAllocMemBlk (AST_PVRST_VLAN_TO_INDEX_MAPPING_MEMPOOL_ID))

#define AST_ALLOC_DIGEST_INPUT(pNode) \
           (pNode = (UINT1 *) MemAllocMemBlk(AST_MST_DIGEST_INPUT_MEMPOOL_ID))

#define AST_ALLOC_BPDU_TYPE_MEM_BLOCK(pNode) \
                      (pNode = (tAstBpduType *)MemAllocMemBlk(AST_BPDU_TYPE_MEMPOOL_ID))

/******************************************************************************/
/* Macros for freeing memory blocks */
/******************************************************************************/
#define AST_RELEASE_RST_PORTINFO_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(RST_PORTINFO_MEMPOOL_ID, (UINT1 *)pNode) 

#define AST_RELEASE_MST_INSTINFO_MEM_BLOCK(pNode) \
            MemReleaseMemBlock(MST_INSTINFO_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_ETHFRAME_SIZE_MEM_BLOCK(pNode) \
{\
        MemReleaseMemBlock(AST_ETHFRAME_SIZE_MEMPOOL_ID, (UINT1 *)pNode); \
        pNode = NULL; \
}
#ifdef MSTP_WANTED
#define AST_RELEASE_MST_VLANMAP_MEM_BLOCK(pNode) \
{\
            MemReleaseMemBlock(MST_VLANMAP_MEMPOOL_ID, (UINT1 *)pNode); \
            pNode = NULL; \
}

#define AST_RELEASE_MST_VLANLIST_MEM_BLOCK(pNode) \
{\
            MemReleaseMemBlock(MST_VLANLIST_MEMPOOL_ID, (UINT1 *)pNode); \
            pNode = NULL; \
}
#endif
#define AST_RELEASE_CONTEXT_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_CONTEXT_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_TMR_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_TMR_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_PERST_INFO_MEM_BLOCK(pPerStInfo) \
           MemReleaseMemBlock(AST_PERST_INFO_MEMPOOL_ID, (UINT1 *)pPerStInfo)
           
#define AST_RELEASE_PORT_INFO_MEM_BLOCK(pPortInfo) \
           MemReleaseMemBlock(AST_PORT_INFO_MEMPOOL_ID, (UINT1 *)pPortInfo)
           
#define AST_RELEASE_PERST_PORT_INFO_MEM_BLOCK(pPerStPortInfo) \
           MemReleaseMemBlock(AST_PERST_PORT_INFO_MEMPOOL_ID, (UINT1 *)pPerStPortInfo)

#define AST_RELEASE_LOCALMSG_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_LOCALMSG_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_QMSG_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_QMSG_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_CFGQ_MSG_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_CFG_QMSG_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_CONVERT_STUPS_2_CENTI_SEC(x) \
           ((x * AST_CENTI_SECONDS) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC)

#define AST_RELEASE_PVRST_PERST_BRG_INFO_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_PERST_PVRST_BRGINFO_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_PVRST_PERST_RST_PORT_INFO_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_PERST_PVRST_RST_PORTINFO_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_MST_DIGEST_INPUT(pNode) \
           MemReleaseMemBlock(AST_MST_DIGEST_INPUT_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_PVRST_VLAN_TO_INDEX_MAP_BLOCK(pNode) \
           MemReleaseMemBlock(AST_PVRST_VLAN_TO_INDEX_MAPPING_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_RELEASE_BPDU_TYPE_MEM_BLOCK(pNode) \
                      MemReleaseMemBlock(AST_BPDU_TYPE_MEMPOOL_ID, (UINT1 *)pNode)

/******************************************************************************/
/* Timer related macros */
/******************************************************************************/

#define AST_CREATE_TMR_LIST         TmrCreateTimerList
#define AST_DELETE_TMR_LIST         TmrDeleteTimerList
#define AST_START_TIMER             TmrStartTimer
#define AST_STOP_TIMER              TmrStopTimer
#define AST_GET_NEXT_EXPIRED_TMR    TmrGetNextExpiredTimer
#define AST_GET_SYS_TIME            OsixGetSysTime
#define AST_GET_REMAINING_TIME      TmrGetRemainingTime

/******************************************************************************/
/* Task, Queue and Event related macros, OSIX related macros */
/******************************************************************************/

#define AST_SELF              SELF
#define AST_CREATE_TASK       OsixTskCrt
#define AST_DELETE_TASK       OsixTskDel
#define AST_GET_TASK_ID       OsixGetTaskId
#define AST_TASK_MODE         OSIX_GLOBAL
#define AST_STACK_SIZE        OSIX_DEFAULT_STACK_SIZE
#define AST_DEF_MSG_LEN       OSIX_DEF_MSG_LEN 
#define AST_CREATE_QUEUE      OsixQueCrt
#define AST_DELETE_QUEUE      OsixQueDel
#define AST_SEND_TO_QUEUE     OsixQueSend 
#define AST_RECV_FROM_QUEUE   OsixQueRecv  
#define AST_OSIX_MSG_NORMAL        OSIX_MSG_NORMAL
#define AST_OSIX_WAIT_FOREVER      OSIX_WAIT_FOREVER

#define AST_RECEIVE_EVENT     OsixEvtRecv
#define AST_SEND_EVENT        OsixEvtSend
#define AST_OSIX_EV_ANY       OSIX_EV_ANY
#define AST_OSIX_NO_WAIT      OSIX_NO_WAIT

#define AST_HTONS             OSIX_HTONS
#define AST_HTONL             OSIX_HTONL
#define AST_NTOHS             OSIX_NTOHS
#define AST_NTOHL             OSIX_NTOHL

#define  AST_TASK_INPUT_QNAME  AST_QUEUE_NAME
#define  AST_SPAWN_TASK OsixTskCrt

/******************************************************************************/
/* Semaphore Related Macros */
/******************************************************************************/
#define AST_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define AST_DELETE_SEM  OsixSemDel

#define AST_TAKE_SEM  OsixSemTake

#define AST_GIVE_SEM  OsixSemGive

/******************************************************************************/
/* CRU Buffer related definitions */
/******************************************************************************/

#define AST_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define AST_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define AST_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define AST_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define AST_CRU_BUF_Get_ChainValidByteCount  CRU_BUF_Get_ChainValidByteCount
#define AST_CRU_BUF_MOVE_VALID_OFFSET        CRU_BUF_Move_ValidOffset 
#define AST_IS_CRU_BUF_LINEAR                CRU_BUF_Get_DataPtr_IfLinear
#define AST_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain

#define  AST_BUF_SET_INTERFACEID(pBuf,IfaceId) \
            CRU_BUF_Set_InterfaceId(pBuf,IfaceId) 
#define  RST_CRU_FALSE  FALSE

#define AST_GET_DATA_PTR_IF_LINEAR(pBuf, u4Offset, u4NumBytes)    \
        CRU_BUF_Get_DataPtr_IfLinear((pBuf), (u4Offset), (u4NumBytes))
    

/******************************************************************************/
/* Macros for accessing Global Structure fields */
/******************************************************************************/

#define AST_IFENTRY_IFINDEX(pPortInfo) \
        (pPortInfo)->u4IfIndex    
        
#define AST_IFENTRY_CONTEXT_ID(pPortInfo) \
        (pPortInfo)->u4ContextId  
        
#define AST_IFENTRY_LOCAL_PORT(pPortInfo) \
        (pPortInfo)->u2PortNo    
        

#define AST_GET_IFINDEX(u2PortNum) \
    (((u2PortNum > 0)&&(u2PortNum <= AST_MAX_PORTS_PER_CONTEXT))?((AST_GET_PORTENTRY (u2PortNum))->u4IfIndex):(0))

#ifdef IFINDEX_WANTED
#define AST_GET_LOCAL_PORT(u2PortNo) \
        AST_GET_IFINDEX(u2PortNo)
#else
#define AST_GET_LOCAL_PORT(u2PortNo) \
        AST_GET_PROTOCOL_PORT(u2PortNo)
#endif
 
#define AST_CONTEXT_INFO(u4ContextId) \
        gAstGlobalInfo.apContextInfo[(u4ContextId)]

#define AST_IS_VC_VALID(u4ContextId) \
        (((UINT4)u4ContextId >= AST_SIZING_CONTEXT_COUNT) ||(u4ContextId < 0)) ? RST_FALSE : \
        ((gAstGlobalInfo.apContextInfo[(u4ContextId)] != NULL) ? RST_TRUE :\
           RST_FALSE)

#define AST_GLOBAL_IFINDEX_TREE()  gAstGlobalInfo.GlobalIfIndexTable

#define AST_IS_INITIALISED() gu1IsAstInitialised

#define AST_SYSLOG_ID gAstGlobalInfo.i4SysLogId

/******************************************* 
 *  The following macros should be accessed 
 *  only after a context has been selected 
 *******************************************/

#define AST_CURR_CONTEXT_INFO()    gpAstContextInfo
#define AST_CURR_CONTEXT_ID()      (AST_CURR_CONTEXT_INFO ())->u4ContextId
    
#define AST_PER_CONTEXT_IFINDEX_TREE()  \
        (AST_CURR_CONTEXT_INFO ())->PerContextIfIndexTable

#define AST_SYSTEM_CONTROL     \
    ((AST_COMP_TYPE() == AST_PB_C_VLAN)? \
     (AST_CURR_CONTEXT_INFO()->u1SystemControl): \
     (gau1AstSystemControl[AST_CURR_CONTEXT_ID()]))
    
#define AST_MODULE_STATUS       \
    ((AST_COMP_TYPE() == AST_PB_C_VLAN)? \
    (AST_PB_CVLAN_INFO()->u1CVlanModuleStatus): \
      (AST_SVLAN_MODULE_STATUS))

#define AST_SYSTEM_CONTROL_SET(status){               \
if (AST_COMP_TYPE() == AST_PB_C_VLAN){           \
(AST_CURR_CONTEXT_INFO()->u1SystemControl) = (status); \
}                                                \
else {                                           \
(gau1AstSystemControl[AST_CURR_CONTEXT_ID()]) = (status); \
}                                                \
}

#define AST_MODULE_STATUS_SET(status) \
{\
   if(AST_COMP_TYPE() == AST_PB_C_VLAN) {\
       if (NULL != AST_PB_CVLAN_INFO()) {\
      (AST_PB_CVLAN_INFO()->u1CVlanModuleStatus) = (status); }    \
      }\
   else if(AST_COMP_TYPE() == AST_PB_S_VLAN) {\
      (AST_CURR_CONTEXT_INFO()->u1SVlanModuleStatus) = (status); \
      }\
   else {\
      (gau1AstModuleStatus[AST_CURR_CONTEXT_ID()]) = (status);   \
      }\
}

#define AST_SVLAN_MODULE_STATUS        \
      ( (AST_COMP_TYPE() == AST_PB_S_VLAN)?  \
        (AST_CURR_CONTEXT_INFO()->u1SVlanModuleStatus): \
        (gau1AstModuleStatus[AST_CURR_CONTEXT_ID()]) )
    
#define AST_SVLAN_ADMIN_STATUS() \
        (AST_CURR_CONTEXT_INFO()->u1SVlanAdminStatus)
    
#define AST_IS_RST_ENABLED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)RST_START) && \
            (AST_MODULE_STATUS == (UINT1)RST_ENABLED)) 

#define AST_IS_RST_STARTED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)RST_START)) 

#define AST_IS_MST_ENABLED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)MST_START) && \
            (AST_MODULE_STATUS == (UINT1)MST_ENABLED)) 
   
#define AST_IS_PVRST_ENABLED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)PVRST_START) && \
            (AST_MODULE_STATUS == (UINT1)PVRST_ENABLED)) 

#define AST_IS_MST_STARTED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)MST_START))

#define AST_IS_PVRST_STARTED()    \
           ((AST_IS_INITIALISED () == RST_TRUE) && \
            (AST_SYSTEM_CONTROL == (UINT1)PVRST_START))


/* In case of 1ad bridges context spanning tree module status and 
 * provider spanning spanning tree module status may differ. */

#ifdef MSTP_WANTED
#define AST_IS_ENABLED_IN_CTXT() \
            (\
             (\
             ((AST_IS_MST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == MST_ENABLED))\
                || \
             ((AST_IS_PVRST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_ENABLED))\
                || \
            ((AST_IS_RST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED))\
            ) ?RST_TRUE: RST_FALSE)

#define AST_IS_TE_MSTID_VALID(instance) \
             ( \
             ((AST_CURR_CONTEXT_INFO ()->u1TEMSTIDValid) == MST_TRUE) && \
               (instance < AST_TE_MSTID))


#else
#ifdef PVRST_WANTED
#define AST_IS_ENABLED_IN_CTXT() \
            (\
             (\
             ((AST_IS_PVRST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == PVRST_ENABLED))\
                || \
            ((AST_IS_RST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED))\
            ) ?RST_TRUE: RST_FALSE)
#else
#define AST_IS_ENABLED_IN_CTXT() \
            (\
            ((AST_IS_RST_STARTED ()) && \
                (gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()] == RST_ENABLED))\
            ?RST_TRUE: RST_FALSE)
#endif
#endif
#define AST_ASYNC_MODE() \
            (gAstGlobalInfo.i4AstAsyncMode)

#define AST_GET_BRGENTRY()   &((AST_CURR_CONTEXT_INFO ())->BridgeEntry)

#define AST_GET_PORTENTRY(u2PortNum) \
           (AST_CURR_CONTEXT_INFO ())->ppPortEntry[u2PortNum - 1]

#define AST_GET_COMM_PORT_INFO(u2PortNum) \
           &((AST_CURR_CONTEXT_INFO ())->ppPortEntry[u2PortNum - 1]->CommPortInfo)
           
#define AST_GET_PERST_INFO(u2InstanceId) \
        ((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]) 
        
#define AST_GET_PERST_BRG_INFO(u2InstanceId) \
           &((AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]->PerStBridgeInfo)
   
#define AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId) \
           (AST_CURR_CONTEXT_INFO ())->ppPerStInfo[u2InstanceId]->ppPerStPortInfo[u2PortNum - 1]
           
#define AST_GET_PERST_RST_PORT_INFO(u2PortNum, u2InstanceId) \
           &((AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId))->PerStRstPortInfo)

#define AST_GET_RST_PORT_INFO(pPerStPortInfo)   \
           &(pPerStPortInfo->PerStRstPortInfo)
          

#define AST_GET_LAST_PROGRMD_STATE(u2InstanceId, u2PortNum) \
           ((AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId))->u1LastProgrammedState)

#define AST_GET_NUM_FWD_TRANSITIONS(pPerStPortInfo)   \
           (pPerStPortInfo->u4NumFwdTransitions)

#define RST_BDSM_PORT_INFO(u2PortNo) AST_GET_PORTENTRY(u2PortNo)

#define AST_TRACE_OPTION        (AST_CURR_CONTEXT_INFO())->u4TraceOption
#define AST_DEBUG_OPTION        (AST_CURR_CONTEXT_INFO())->u4DebugOption
#define AST_FORCE_VERSION       (AST_CURR_CONTEXT_INFO())->u1ForceVersion
#define AST_INST_PORT_MAP       (AST_CURR_CONTEXT_INFO())->u1InstancePortMap
#define AST_FLUSH_INTERVAL      (AST_CURR_CONTEXT_INFO())->u4FlushInterval
#define AST_CONTEXT_STR()         ((AST_CURR_CONTEXT_INFO())->au1StrContext)
#define AST_SPACE_STR             gau1SpaceString

#define AST_IS_PORT_UP(u2PortNum) \
           ((AST_GET_PORTENTRY (u2PortNum))->u1EntryStatus \
            == (UINT1)AST_PORT_OPER_UP)
   
#define RST_IS_PORT_UP(pPortInfo) \
           (pPortInfo->u1EntryStatus == (UINT1)AST_PORT_OPER_UP)

#define AST_SET_CHANGED_FLAG(u2InstanceId, u2PortNum) \
        (AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId)->u1ChangedFlag)

#define AST_GET_CHANGED_FLAG(u2InstanceId, u2PortNum) \
           ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId) == NULL) ? \
            RST_FALSE : \
           (AST_GET_PERST_PORT_INFO(u2PortNum,u2InstanceId))->u1ChangedFlag)

#define AST_GET_PORT_STATE(u2InstanceId, u2PortNum) \
      AstGetInstPortStateFromL2Iwf (u2InstanceId, u2PortNum)

#define   AST_GET_PORT_ROLE(Instance ,PortIfIndex) \
              AST_GET_PERST_PORT_INFO(PortIfIndex, Instance)->u1PortRole
     
#define  AST_GET_SELECTED_PORT_ROLE(Instance ,PortIfIndex) \
              AST_GET_PERST_PORT_INFO(PortIfIndex, Instance)->u1SelectedPortRole

#define AST_INCR_RX_RST_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstBpdusRxd)++

#define AST_INCR_RX_CONFIG_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumConfigBpdusRxd)++

#define AST_INCR_RX_TCN_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumTcnBpdusRxd)++

#define AST_INCR_TX_RST_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstBpdusTxd)++

#define AST_INCR_TX_CONFIG_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumConfigBpdusTxd)++

#define AST_INCR_TX_TCN_BPDU_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumTcnBpdusTxd)++

#define AST_INCR_NUM_OF_TOPO_CHANGES(u2InstanceId) \
      ((AST_GET_PERST_INFO(u2InstanceId))->PerStBridgeInfo.u4NumTopoCh)++

#define AST_INCR_TC_DETECTED_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstTCDetectedCount)++

#define AST_INCR_TC_RX_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstTCRxdCount)++

#define AST_INCR_IMPSTATE_OCCUR_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstImpossibleStateOcc)++

#define AST_INCR_RCVDINFOWHILE_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstRxdInfoWhileExpCount)++

#define AST_INCR_RX_PROPOSAL_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstProposalPktsRcvd)++

#define AST_INCR_RX_AGREEMENT_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstAgreementPktRcvd)++

#define AST_INCR_TX_PROPOSAL_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstProposalPktsSent)++

#define AST_INCR_TX_AGREEMENT_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4NumRstAgreementPktSent)++



#define AST_INCR_ASTP_UP_COUNT() \
      ((AST_CURR_CONTEXT_INFO ())->BridgeEntry.u4AstpUpCount)++

#define AST_INCR_ASTP_DOWN_COUNT() \
      ((AST_CURR_CONTEXT_INFO ())->BridgeEntry.u4AstpDownCount)++

#define AST_INCR_BUFFER_FAILURE_COUNT() \
      (gAstGlobalInfo.u4BufferFailureCount)++

#define AST_INCR_MEMORY_FAILURE_COUNT() \
      (gAstGlobalInfo.u4MemoryFailureCount)++

#define AST_INCR_NEW_ROOTBRIDGE_COUNT(u2InstanceId) \
      ((AST_GET_PERST_INFO(u2InstanceId))->PerStBridgeInfo.u4NewRootIdCount)++

#define AST_INCR_PROTOCOL_MIGRATION_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4ProtocolMigrationCount)++

#define AST_INCR_INVALID_RST_BPDU_RXD_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4InvalidRstBpdusRxdCount)++

#define AST_INCR_INVALID_CONFIG_BPDU_RXD_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4InvalidConfigBpdusRxdCount)++

#define AST_INCR_INVALID_TCN_BPDU_RXD_COUNT(u2PortNum) \
      ((AST_GET_PORTENTRY(u2PortNum))->u4InvalidTcnBpdusRxdCount)++


#define AST_INCR_PERST_PORT_COUNT(u2InstanceId) \
     (PVRST_GET_PERST_INFO(u2InstanceId)->u2PortCount)++

#define AST_DECR_PERST_PORT_COUNT(u2InstanceId) \
     (PVRST_GET_PERST_INFO(u2InstanceId)->u2PortCount)--

#define AST_UPDATE_OPER_P2P             RstSetAdminPointToPoint
      
#define AST_GET_SYSTEMACTION    (AST_CURR_CONTEXT_INFO ())->u1SystemAction
      
#define AST_IS_PORT_VALID(PortId)  (((PortId == 0) || (PortId > AST_MAX_NUM_PORTS))?AST_FALSE:AST_TRUE)
      
#define  AST_CURR_CONTEXT_IFTABLE() \
       (AST_CURR_CONTEXT_INFO ())->PerContextIfIndexTable
       
/******************************************************************************/
/* Bridge related Macros */
/******************************************************************************/

#define AST_BRIDGE_UP  UP

#define AST_BRG_TIMER_EXP_EVENT BRG_TIMER_EXP_EVENT
      
#define AST_BUF_GET_INTERFACEID(pBuf)   CRU_BUF_Get_InterfaceId(pBuf)

#define AST_BUF_MOVE_VALID_OFFSET(pBuf, size) \
           CRU_BUF_Move_ValidOffset(pBuf,size)
   
#define AST_BUF_GET_CHAIN_VALIDBYTECOUNT(pBuf) \
           CRU_BUF_Get_ChainValidByteCount(pBuf)

#define AST_MEM_FAIL_TRAPS_COUNT  (AST_CURR_CONTEXT_INFO ())->u4MemFailCount

#define AST_BRG_ENABLED                 BRG_ENABLED
#define AST_UNKNOWN_PROTO               1
#define AST_DECLB100                    2
#define AST_IEEE8021d                   3
#define AST_IEEE8021q                   4



/******************************************************************************/
/* CFA related macros */
/******************************************************************************/


#define AST_ENCAP_NONE                  CFA_ENCAP_NONE
#define AST_ENCAP_LLC                   CFA_ENCAP_LLC 
#define AST_PROT_BPDU                   CFA_PROT_BPDU

#define AST_ENET_PHYS_INTERFACE_TYPE    CRU_ENET_PHYS_INTERFACE_TYPE
#define AST_LAGG_INTERFACE_TYPE         CFA_LAGG
#define AST_BRIDGED_INTERFACE           CFA_BRIDGED_INTERFACE
#define AST_PROP_VIRTUAL_INTERFACE      CFA_PROP_VIRTUAL_INTERFACE
#define AST_PPP_PHYS_INTERFACE_TYPE     CRU_PPP_PHYS_INTERFACE_TYPE
#define AST_X25_PHYS_INTERFACE_TYPE     CRU_X25_PHYS_INTERFACE_TYPE
#define AST_FRMRL_PHYS_INTERFACE_TYPE   CRU_FRMRL_PHYS_INTERFACE_TYPE
#define AST_ENET_HEADER_SIZE            CRU_ENET_HEADER_SIZE
#define AST_LLC_HEADER_SIZE             CRU_LLC_HEADER_SIZE
#define AST_PSEUDO_WIRE                 CFA_PSEUDO_WIRE
#define AST_VXLAN_NVE                   CFA_VXLAN_NVE
/* Added for Attachment Circuit interface */
#define AST_PROP_VIRTUAL_INTERFACE      CFA_PROP_VIRTUAL_INTERFACE
#define AST_ATTACHMENT_CIRCUIT_INTERFACE CFA_SUBTYPE_AC_INTERFACE
   
/******************************************************************************/
/* General Macros used */
/******************************************************************************/
        

#define AST_UNUSED(x){x = x;}

#define RST_IS_NOT_SELECTED(pRstPortInfo) \
        ((pRstPortInfo->bSelected != RST_TRUE) || \
               (pRstPortInfo->bUpdtInfo != RST_FALSE))
        
#define AST_IS_BROADCAST_ADDR(MacAddr) \
            ((MacAddr.u4Addr == AST_MAC_BCAST_HI_ADDR) && \
            (MacAddr.u2Addr == AST_MAC_BCAST_LOW_ADDR))
#define AST_IS_MULTICAST_ADDR(MacAddr) \
            (MacAddr.u4Addr & AST_MAC_MCAST_MASK)
   
#define AST_GET_BPDU_FROM_AST_QUEUE     AST_RECV_FROM_QUEUE

#define AST_MAX(x,y) (x > y ? x : y)

/******************************************************************************/
/* Macros for getting data from linear buffer */
/******************************************************************************/
#define AST_GET_1BYTE(u1Val, pu1PktBuf) \
   do {                             \
      u1Val = *pu1PktBuf;           \
         pu1PktBuf += 1;        \
   } while(0)

#define AST_GET_2BYTE(u2Val, pu1PktBuf)            \
do {                                        \
   AST_MEMCPY (&u2Val, pu1PktBuf, 2);\
      u2Val = (UINT2)AST_NTOHS(u2Val);               \
      pu1PktBuf += 2;                   \
} while(0)

#define AST_GET_4BYTE(u4Val, pu1PktBuf)              \
do {                                          \
   AST_MEMCPY (&u4Val, pu1PktBuf, 4); \
      u4Val = AST_NTOHL(u4Val);                 \
      pu1PktBuf += 4;                    \
} while(0)


#define AST_GET_MAC_ADDR(Value, pu1PktBuf)  \
        do {                                       \
           AST_MEMCPY (&Value, (pu1PktBuf), 6);    \
           (pu1PktBuf) += 6;                       \
        }while(0)

/******************************************************************************/
/* Encode Module Related Macros used to assign fields to buffer */
/******************************************************************************/
#define AST_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
           pu1PktBuf += 1;        \
        } while(0)

#define AST_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                    \
            u2Val = (UINT2)AST_HTONS(u2Val);                  \
            AST_MEMCPY (pu1PktBuf, &u2Val, 2); \
            pu1PktBuf += 2;              \
        } while(0)

#define AST_PUT_4BYTE(pu1PktBuf, u4Val)       \
        do {                                   \
           u4Val = AST_HTONL(u4Val);   \
           AST_MEMCPY (pu1PktBuf, &u4Val, 4); \
           pu1PktBuf += 4;             \
        }while(0)

#define AST_PUT_MAC_ADDR(pu1PktBuf, Value)   \
        do {                                      \
           AST_MEMCPY (pu1PktBuf, &Value, 6);     \
           pu1PktBuf += 6;                        \
        }while(0)


/******************************************************************************/
/* Macro for UINT8 counters */
/******************************************************************************/

#define   AST_UINT8_ADD(varA, value) \
   if ((0xffffffff - varA.u4LoWord) >= value){ \
      (varA.u4LoWord)+= value; \
   }                           \
   else {                      \
      varA.u4LoWord = value - (0xffffffff - varA.u4LoWord) - 1;\
      (varA.u4HiWord)++; \
   } 

#define AST_UINT8_INC(varA) \
   if (varA.u4HiWord == 0xffffffff) \
      (varA.u4LoWord)++; \
   (varA.u4HiWord)++; 
   
#define AST_UINT8_RESET(varA) \
   varA.u4HiWord = 0; \
   varA.u4LoWord = 0;


#define AST_MAC_ADDR_LEN  6

#define AST_MAC_ADDR_TO_ARRAY(pu1Array, pMacAddr) \
        { \
          tMacAddress TmpMacAddr; \
          \
          TmpMacAddr.u4Dword = (pMacAddr)->u4Dword; \
          TmpMacAddr.u2Word = (pMacAddr)->u2Word;   \
          \
          TmpMacAddr.u4Dword = OSIX_HTONL(TmpMacAddr.u4Dword); \
          TmpMacAddr.u2Word = (UINT2)(OSIX_HTONS(TmpMacAddr.u2Word));   \
          \
          MEMCPY ((pu1Array), (&TmpMacAddr.u4Dword), (sizeof(TmpMacAddr.u4Dword))); \
          MEMCPY ((pu1Array+(sizeof(TmpMacAddr.u4Dword))), (&TmpMacAddr.u2Word), \
                   (sizeof(TmpMacAddr.u2Word))); \
        }
#define AST_QMSG_TYPE(pMsg) pMsg->LocalMsgType

/*******************************************************************/
/*                                                                 */
/*        PROVIDER BRIDGING RELATED MACROS                         */
/*                                                                 */
/*******************************************************************/


#define AST_PORT_TBL_MEMBLK_COUNT  1

#define AST_PREV_CONTEXT_INFO()    gpAstParentCtxtInfo


#define AST_IS_PROVIDER_EDGE_BRIDGE() \
         ((AST_BRIDGE_MODE() == AST_PROVIDER_EDGE_BRIDGE_MODE)? \
         RST_TRUE : RST_FALSE)

#define AST_IS_PROVIDER_CORE_BRIDGE() \
         ((AST_BRIDGE_MODE() == AST_PROVIDER_CORE_BRIDGE_MODE)? \
          RST_TRUE : RST_FALSE)

#define AST_IS_1AD_BRIDGE() \
         (((AST_IS_PROVIDER_EDGE_BRIDGE() == RST_TRUE) \
          || (AST_IS_PROVIDER_CORE_BRIDGE() == RST_TRUE))? \
          RST_TRUE : RST_FALSE)



/* Global Mempools created when system initialises for Provider bridge*/
#define AST_CVLAN_CONTEXT_MEMPOOL_ID \
             gAstGlobalInfo.PbMemPoolIds.CvlanContextMemPoolId

#define AST_CVLAN_PERST_TBL_MEMPOOL_ID \
             gAstGlobalInfo.PbMemPoolIds.CvlanPerStInfoMemPoolId

#define AST_CVLAN_INFO_MEMPOOL_ID \
             gAstGlobalInfo.PbMemPoolIds.CvlanInfoMemPoolId

#define AST_PB_PORT_INFO_MEMPOOL_ID \
             gAstGlobalInfo.PbMemPoolIds.PbPortInfoMemPoolId

#define AST_PB_PORT2INDEX_MAP_MEMPOOL_ID \
             gAstGlobalInfo.PbMemPoolIds.CvlanPort2IndexTblMemPoolId


/* The following mempools ids are maintained per C-VLAN component. */
#define AST_CVLAN_PORT_TBL_MEMPOOL_ID \
                  AST_PB_CVLAN_INFO()->CvlanPortTblMemPoolId

#define AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID \
                  AST_PB_CVLAN_INFO()->CvlanPerStPortTblMemPoolId


/*Memory Blocks allocated from Global Mempools*/
/* Since this Memory Block is allocated in a dynamically created mempool, the
 * pool id is required*/
#define AST_ALLOC_PORT_TBL_BLOCK(PoolId, pPortEntry) \
           (pPortEntry =(tAstPortEntry **)MemAllocMemBlk(PoolId))

#define AST_ALLOC_PERST_TBL_BLOCK(pNode) \
           (pNode = (tAstPerStInfo **)MemAllocMemBlk\
            (AST_PERST_TBL_MEMPOOL_ID))

/* Since this Memory Block is allocated in a dynamically created mempool, the
 * pool id is required*/
#define AST_ALLOC_PERST_PORT_TBL_BLOCK(PoolId, pPerStPortInfo) \
           (pPerStPortInfo =(tAstPerStPortInfo **)MemAllocMemBlk(PoolId))

#define AST_PB_ALLOC_CVLAN_CONTEXT_MEM_BLOCK(pNode) \
           (pNode = (tAstContextInfo *)MemAllocMemBlk(AST_CVLAN_CONTEXT_MEMPOOL_ID))

#define AST_PB_ALLOC_CVLAN_PERST_TBL_MEM_BLOCK(pNode) \
           (pNode = (tAstPerStInfo **) MemAllocMemBlk(AST_CVLAN_PERST_TBL_MEMPOOL_ID))

#define AST_PB_ALLOC_CVLAN_INFO_MEM_BLOCK(pNode) \
           (pNode = (tAstPbCVlanInfo *) MemAllocMemBlk(AST_CVLAN_INFO_MEMPOOL_ID))

#define AST_PB_ALLOC_PB_PORT_INFO_MEM_BLOCK(pNode) \
           (pNode = (tAstPbPortInfo *) MemAllocMemBlk(AST_PB_PORT_INFO_MEMPOOL_ID))

#define AST_PB_ALLOC_PORT2INDEX_MAP_BLOCK(pNode) \
           (pNode = (tAstPort2IndexMap *)MemAllocMemBlk (AST_PB_PORT2INDEX_MAP_MEMPOOL_ID))


/*Memory Blocks allocated from CVLAN component Mempools*/
#define AST_PB_ALLOC_CVLAN_PORT_TBL_MEM_BLOCK(pNode) \
           (pNode = (tAstPortEntry **) MemAllocMemBlk(AST_CVLAN_PORT_TBL_MEMPOOL_ID))

#define AST_PB_ALLOC_CVLAN_PERST_PORT_TBL_MEM_BLOCK(ppNode) \
           MemAllocateMemBlock(AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID, \
                               (UINT1 **)ppNode)


/*Global Memblock release */

/* Since this Memory Block is allocated in a dynamically created mempool, the
 * pool id is required*/
#define AST_RELEASE_PORT_TBL_BLOCK(PoolId, pTable) \
           MemReleaseMemBlock(PoolId, (UINT1 *)(pTable))

#define AST_RELEASE_PERST_TBL_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_PERST_TBL_MEMPOOL_ID, (UINT1 *)(ppNode))

/* Since this Memory Block is allocated in a dynamically created mempool, the
 * pool id is required*/
#define AST_RELEASE_PERST_PORT_TBL_BLOCK(PoolId, pTable) \
           MemReleaseMemBlock(PoolId, (UINT1 *)(pTable))

#define AST_PB_RELEASE_CVLAN_CONTEXT_MEM_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_CVLAN_CONTEXT_MEMPOOL_ID, (UINT1 *)ppNode)

#define AST_PB_RELEASE_CVLAN_PERST_TBL_MEM_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_CVLAN_PERST_TBL_MEMPOOL_ID, (UINT1 *)ppNode)

#define AST_PB_RELEASE_CVLAN_INFO_MEM_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_CVLAN_INFO_MEMPOOL_ID, (UINT1 *)ppNode)

#define AST_PB_RELEASE_PB_PORT_INFO_MEM_BLOCK(pNode) \
           MemReleaseMemBlock(AST_PB_PORT_INFO_MEMPOOL_ID, (UINT1 *)pNode)

#define AST_PB_RELEASE_PORT2INDEX_MAP_BLOCK(pNode) \
           MemReleaseMemBlock (AST_PB_PORT2INDEX_MAP_MEMPOOL_ID, \
                               (UINT1 *)pNode)

/*CVLAN context MemBlock Release*/

#define AST_PB_RELEASE_CVLAN_PORT_TBL_MEM_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_CVLAN_PORT_TBL_MEMPOOL_ID, (UINT1 *)ppNode)

#define AST_PB_RELEASE_CVLAN_PERST_PORT_TBL_MEM_BLOCK(ppNode) \
           MemReleaseMemBlock(AST_CVLAN_PERST_PORT_TBL_MEMPOOL_ID, \
                               (UINT1 *)ppNode)


#define AST_MAX_NUM_PORTS            (AST_CURR_CONTEXT_INFO ()->u2PortTblSize)
#define AST_BRIDGE_MODE()            (AST_CURR_CONTEXT_INFO ())->u4BridgeMode
#define AST_GET_BRG_PORT_TYPE(pPortInfo)  (pPortInfo)->u1PortType
#define AST_PORT_TBL()               (AST_CURR_CONTEXT_INFO ())->ppPortEntry
#define AST_PERST_INFO_TBL()         (AST_CURR_CONTEXT_INFO ())->ppPerStInfo

#define AST_PERST_PORT_INFO_TBL(u2Inst)    \
        (AST_CURR_CONTEXT_INFO())->ppPerStInfo[(u2Inst)]->ppPerStPortInfo

#define AST_COMP_TYPE()           (AST_CURR_CONTEXT_INFO ())->u2CompType
#define AST_PB_CVLAN_INFO()          (AST_CURR_CONTEXT_INFO ())->pPbCVlanInfo
#define AST_PB_PORT_INFO(pPortEntry)       (pPortEntry)->pPbPortInfo
#define AST_INSTANCE_UPCOUNT()       (AST_CURR_CONTEXT_INFO())->pInstanceUpCount

#define AST_PB_PORT_CVLAN_CTXT(pPortEntry) \
        AST_PB_PORT_INFO((pPortEntry))->pPortCVlanContext

#define AST_PB_IS_PORT_SVLAN_CEP(pPortEntry) \
        (((AST_GET_BRG_PORT_TYPE(pPortEntry) == AST_CUSTOMER_EDGE_PORT) && \
         (AST_PB_PORT_INFO(pPortEntry) != NULL) && \
         (AST_PB_PORT_CVLAN_CTXT(pPortEntry) != NULL))?RST_TRUE:RST_FALSE)
         
#define AST_IS_CVLAN_COMPONENT() \
              ((AST_COMP_TYPE () == AST_PB_C_VLAN)? RST_TRUE : RST_FALSE)

#define AST_IS_PROVIDER_EDGE_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_PROVIDER_EDGE_PORT) ? RST_TRUE: RST_FALSE)

#define AST_IS_CUSTOMER_EDGE_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_CUSTOMER_EDGE_PORT) ? RST_TRUE: RST_FALSE)

#define AST_IS_PROP_CUSTOMER_EDGE_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_PROP_CUSTOMER_EDGE_PORT) ? RST_TRUE: RST_FALSE)


#define AST_IS_PROP_CUSTOMER_NETWORK_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_PROP_CUSTOMER_NETWORK_PORT) ? RST_TRUE: RST_FALSE)

#define AST_IS_CUSTOMER_NETWORK_PORT(u2PortNum) \
    (((AST_GET_BRG_PORT_TYPE(AST_GET_PORTENTRY(u2PortNum)) == \
      AST_CNP_STAGGED_PORT) ||                            \
      (AST_GET_BRG_PORT_TYPE(AST_GET_PORTENTRY(u2PortNum)) ==\
       AST_CNP_PORTBASED_PORT)||                           \
      (AST_GET_BRG_PORT_TYPE(AST_GET_PORTENTRY(u2PortNum)) ==\
       AST_CNP_CTAGGED_PORT)) ? RST_TRUE: RST_FALSE)

#define AST_IS_PROP_PROVIDER_NETWORK_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_PROP_PROVIDER_NETWORK_PORT) ? RST_TRUE: RST_FALSE)
      
#define AST_PB_CVLAN_PORT2INDEX_TREE()  \
        AST_PB_CVLAN_INFO()->Port2IndexMapTbl

#define AST_CREATE_RBTREE(FuncPtr) \
           RBTreeCreate(AST_MAX_NUM_VLANS,FuncPtr)

#define AST_DESTROY_RBTREE() \
           RBTreeDelete(AST_PB_CVLAN_INFO()->Port2IndexMapTbl)

#define AST_PB_CHECK_CVLAN_COMP(pAstPortEntry) \
         ((AST_BRIDGE_MODE ()== AST_PROVIDER_EDGE_BRIDGE_MODE) && \
          (AST_GET_BRG_PORT_TYPE(pAstPortEntry) == AST_CUSTOMER_EDGE_PORT) && \
          ((AST_COMP_TYPE ()  == AST_PB_C_VLAN)? RST_TRUE: RST_FALSE))

#define AST_GET_PROTOCOL_PORT(u2Port) \
        (AST_GET_PORTENTRY(u2Port)->u2ProtocolPort)

#define AST_PORT_RESTRICTED_ROLE(pPortEntry) \
    (pPortEntry->bRestrictedRole)

#define AST_PORT_ROOT_GUARD(pPortEntry) \
    (pPortEntry->bRootGuard)

#define AST_PORT_RESTRICTED_TCN(pPortEntry) \
    (pPortEntry->bRestrictedTcn)

#define AST_PORT_ENABLE_BPDU_RX(pPortEntry) \
    (pPortEntry->bEnableBPDURx)

#define AST_PORT_ENABLE_BPDU_TX(pPortEntry) \
    (pPortEntry->bEnableBPDUTx)

#define AST_PORT_IS_L2GP(pPortEntry) \
    (pPortEntry->bIsL2Gp)

#define AST_PORT_ROW_STATUS(pPortEntry) \
    (pPortEntry->i4PortRowStatus)

#define AST_PORT_DOT1W_ENABLED(pPortEntry) \
    (pPortEntry->bDot1wEnabled)

#define AST_DEF_VLAN_ID()           gAstDefaultVlanId 


    /* PBB Related MAcros */

#define AST_IS_VIRTUAL_INST_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_VIRTUAL_INSTANCE_PORT) ? RST_TRUE: RST_FALSE)

#define AST_IS_I_COMPONENT() \
         ((AST_BRIDGE_MODE() == AST_ICOMPONENT_BRIDGE_MODE)? \
          RST_TRUE : RST_FALSE)

#define AST_IS_B_COMPONENT() \
         ((AST_BRIDGE_MODE() == AST_BCOMPONENT_BRIDGE_MODE)? \
          RST_TRUE : RST_FALSE)

#define AST_IS_1AH_BRIDGE() \
         (((AST_BRIDGE_MODE() == AST_ICOMPONENT_BRIDGE_MODE) ||  \
           (AST_BRIDGE_MODE() == AST_BCOMPONENT_BRIDGE_MODE))? \
          RST_TRUE : RST_FALSE)

#define AST_IS_1AH_BRIDGE() \
         (((AST_BRIDGE_MODE() == AST_ICOMPONENT_BRIDGE_MODE) ||  \
           (AST_BRIDGE_MODE() == AST_BCOMPONENT_BRIDGE_MODE))? \
          RST_TRUE : RST_FALSE)

#define AST_IS_CUSTOMER_BACKBONE_PORT(u2PortNum) \
    ((AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY(u2PortNum)) == \
      AST_CUSTOMER_BACKBONE_PORT) ? RST_TRUE: RST_FALSE)

    /* Offsets for installing filters */
#define AST_ICOMP_OFFSET            18
#define AST_BCOMP_OFFSET            22

#define AST_FIRST_PORT_IN_CONTEXT   1

#define AST_PORT_LOOP_GUARD(pPortEntry) \
    (pPortEntry->bLoopGuard)
#define AST_IS_PORT_OPER_EDGE(pPortEntry) \
    (pPortEntry->bOperEdgePort)
#define AST_IS_PORT_ADMIN_EDGE(pPortEntry) \
    (pPortEntry->bAdminEdgePort)

/*Performance Related Macros*/
#ifdef PBB_PERFORMANCE_WANTED
#define STP_PBB_PERF_MARK_START_TIME()     gettimeofday(&gStTime, NULL);
#define STP_PBB_PERF_MARK_END_TIME(u4TimeTaken)      \
{\
    long long u4T1, u4T2;\
    gettimeofday(&gEnTime, NULL);\
    u4T1 = (gStTime.tv_sec * 1000000 + gStTime.tv_usec);\
    u4T2 = (gEnTime.tv_sec * 1000000 + gEnTime.tv_usec);\
    gu4TimeTaken = (u4T2 - u4T1);\
    u4TimeTaken += (u4T2 - u4T1);\
}
#define STP_PBB_PERF_PRINT_TIME(u4TimeTaken)      \
{\
    AST_PRINT(AST_ALL_FLAG, AST_ALL_FLAG, AST_MOD_NAME,"Convergence Time taken ->  micro-sec=%u\r\n",u4TimeTaken);\
}
#else
#define STP_PBB_PERF_MARK_START_TIME()
#define STP_PBB_PERF_MARK_END_TIME(u4TimeTaken)
#define STP_PBB_PERF_PRINT_TIME(u4TimeTaken)
#endif

#define AST_GET_SISP_STATUS(u2PortNum) (AST_GET_PORTENTRY(u2PortNum)->bIsSisp)

#define AST_GET_PHY_PORT(u2PortNum) \
    (AST_GET_PORTENTRY(u2PortNum)->u4PhyPortIndex)

#define AST_IS_PORT_SISP_LOGICAL(u2PortNum)   \
    (((AST_GET_IFINDEX(u2PortNum) >= AST_MIN_SISP_LOG_INDEX) && \
     (AST_GET_IFINDEX(u2PortNum) <= AST_MAX_SISP_LOG_INDEX))?MST_TRUE:MST_FALSE)

#define MST_SISP_GET_FWDFLAG(u2PortNo)   \
    (AST_IS_PORT_SISP_LOGICAL(u2PortNo) \
         == MST_TRUE)?AST_INIT_VAL:MST_SET_FORWARDING_FLAG

#define MST_SISP_GET_LRNFLAG(u2PortNo)   \
    (AST_IS_PORT_SISP_LOGICAL(u2PortNo) \
         == MST_TRUE)?AST_INIT_VAL:MST_SET_LEARNING_FLAG

#define MST_SISP_GET_ROOT_PRIO(u2PortNo, u2Priority)  \
    (AST_IS_PORT_SISP_LOGICAL(u2PortNo) \
        == MST_TRUE)?0xFFFF:u2Priority

#define MST_SISP_GET_PORT_PRIO(u2PortNo, u2Priority)  \
    (AST_IS_PORT_SISP_LOGICAL(u2PortNo) \
        == MST_TRUE)?0xFF:u2Priority

#define AST_IS_PORTBITLIST_SET(au1PortList, u2PortNum, i4Size, bResult)  \
    OSIX_BITLIST_IS_BIT_SET (au1PortList, u2PortNum, i4Size, bResult)

#define  MST_ADD            1
#define  MST_DELETE         2

#define INVALID_INDEX       0
/* Attachment Circuit interface: check modifications required or not */
#define AST_INTF_TYPE_VALID(u1IfType)    \
       ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG) || \
 (u1IfType == AST_BRIDGED_INTERFACE) || \
 (u1IfType ==CFA_PSEUDO_WIRE) || \
 (u1IfType == AST_PROP_VIRTUAL_INTERFACE))?RST_TRUE:RST_FALSE

#define AST_GET_NEXT_PORT_ENTRY(u2PortNum, pPortEntry)   \
        for(pPortEntry = gpAstContextInfo->ppPortEntry[u2PortNum - 1];\
           u2PortNum <= AST_MAX_NUM_PORTS; \
           u2PortNum++, \
           pPortEntry = gpAstContextInfo->ppPortEntry[u2PortNum - 1])

#define AST_GET_NEXT_BUF_INSTANCE(u2InstanceId, pPerStInfo) \
        for(pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId];\
                u2InstanceId < AST_MAX_MST_INSTANCES;u2InstanceId++,  \
                pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId])

#define AST_GET_PREV_BUF_INSTANCE(u2InstanceId, pPerStInfo) \
        for( pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId];\
                 u2InstanceId >= 0 ; u2InstanceId--, \
                 pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId])

#define AST_GET_NEXT_BUF_INSTANCE_MAX(u2InstanceId, u2MaxInst, pPerStInfo) \
        for( pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId];\
                 u2InstanceId < u2MaxInst ; u2InstanceId++, \
                 pPerStInfo = gpAstContextInfo->ppPerStInfo[u2InstanceId])
/* HITLESS RESTART */
#define AST_HR_STATUS()               AstRedGetHRFlag()
#define AST_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE

/* MCLAG */
#define AST_MCLAG_ENABLED         LA_MCLAG_ENABLED
#define AST_MCLAG_DISABLED        LA_MCLAG_DISABLED

#define AST_TC_BIT_OFFSET       21
#define AST_TC_LENGTH            1
#define AST_HR_CHECK_STDY_ST_PKT(u1Buf)\
    ((u1Buf & 0x01) == 0 )  /* Checking whether the Topology Change bit is NOT
                                 set to identify it as a steady state packet */
#define AST_HR_STDY_ST_REQ_RCVD()   gAstGlobalInfo.u1StdyStReqRcvd

#define AST_CHECK_AND_CONVERT_COMP_ID_TO_CTXT_ID(u4ComponentId) \
    if ( u4ComponentId <= 0 )\
    {\
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;\
        return SNMP_FAILURE;\
    }\
    u4ComponentId = (u4ComponentId - 1)


#define AST_CONVERT_COMP_ID_TO_CTXT_ID(u4ComponentId) \
    if ( u4ComponentId <= 0 )\
    {\
        return SNMP_FAILURE;\
    }\
    u4ComponentId = (u4ComponentId - 1)

#define AST_CONVERT_CTXT_ID_TO_COMP_ID(u4ContextId) \
    u4ContextId = (u4ContextId + 1)


#define AST_GET_RANDOM_TIME(u4TimeOut, u4RandVal) \
        OsixGetSysTime((tOsixSysTime *)&(u4RandVal)); \
        (u4RandVal) = (u4RandVal) % ((u4TimeOut) / AST_TIME_DIV_INTERVAL);\
        if (u4RandVal == 0) \
        {\
         (u4RandVal)++; \
        }\
        (u4RandVal) = (u4TimeOut) + (u4RandVal) 

#define AST_SIZING_PORT_COUNT FsASTSizingParams[MAX_AST_PORTS_IN_SYS_SIZING_ID].u4PreAllocatedUnits

#define AST_GET_PER_ST_BRG_INFO_PTR(u2InstanceId) \
          &(AST_GET_PERST_INFO(u2InstanceId)->PerStBridgeInfo)

#endif /* _ASTMACR_H_ */
