
/* $Id: fspbrsnc.h,v 1.1 2016/06/18 11:40:00 siva Exp $
    ISS Wrapper header
    module ARICENT-PB-RSTP-MIB

 */


#ifndef _H_i_ARICENT_PB_RSTP_MIB
#define _H_i_ARICENT_PB_RSTP_MIB
/********************************************************************
* FUNCTION NcFsPbProviderStpStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbProviderStpStatusSet (
                INT4 i4FsPbProviderStpStatus );

/********************************************************************
* FUNCTION NcFsPbProviderStpStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbProviderStpStatusTest (UINT4 *pu4Error,
                INT4 i4FsPbProviderStpStatus );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeIdGet (
                INT4 i4FsPbRstPort,
                UINT1 *pau1FsPbRstCVlanBridgeId );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeDesignatedRootGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeDesignatedRootGet (
                INT4 i4FsPbRstPort,
                UINT1 *pau1FsPbRstCVlanBridgeDesignatedRoot );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeRootCostGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeRootCostGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeRootCost );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeMaxAgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeMaxAgeGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeMaxAge );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeHelloTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeHelloTimeGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeHelloTime );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeHoldTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeHoldTimeGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeHoldTime );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeForwardDelayGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeForwardDelayGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeForwardDelay );

/********************************************************************
* FUNCTION NcFsPbRstCVlanBridgeTxHoldCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanBridgeTxHoldCountGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanBridgeTxHoldCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanStpHelloTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanStpHelloTimeGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanStpHelloTime );

/********************************************************************
* FUNCTION NcFsPbRstCVlanStpMaxAgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanStpMaxAgeGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanStpMaxAge );

/********************************************************************
* FUNCTION NcFsPbRstCVlanStpForwardDelayGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanStpForwardDelayGet (
                INT4 i4FsPbRstPort,
                INT4 *pi4FsPbRstCVlanStpForwardDelay );

/********************************************************************
* FUNCTION NcFsPbRstCVlanStpTopChangesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanStpTopChangesGet (
                INT4 i4FsPbRstPort,
                UINT4 *pu4FsPbRstCVlanStpTopChanges );

/********************************************************************
* FUNCTION NcFsPbRstCVlanStpTimeSinceTopologyChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanStpTimeSinceTopologyChangeGet (
                INT4 i4FsPbRstPort,
                UINT4 *pu4FsPbRstCVlanStpTimeSinceTopologyChange );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortPriorityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortPriorityGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortPriority );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortPathCostGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortPathCostGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortPathCost );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortRoleGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortRoleGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortRole );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortAdminEdgePortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortAdminEdgePortGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortAdminEdgePort );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortOperEdgePortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortOperEdgePortGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortOperEdgePort );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortAdminPointToPointGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortAdminPointToPointGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortAdminPointToPoint );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortOperPointToPointGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortOperPointToPointGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortOperPointToPoint );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortAutoEdgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortAutoEdgeGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortAutoEdge );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortDesignatedRootGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortDesignatedRootGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT1 *pau1FsPbRstCVlanPortDesignatedRoot );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortDesignatedCostGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortDesignatedCostGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortDesignatedCost );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortDesignatedBridgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortDesignatedBridgeGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT1 *pau1FsPbRstCVlanPortDesignatedBridge );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortDesignatedPortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortDesignatedPortGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT1 *pau1FsPbRstCVlanPortDesignatedPort );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortForwardTransitionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortForwardTransitionsGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortForwardTransitions );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortInfoSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortInfoSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortInfoSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortMigSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortMigSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortMigSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortRoleTransSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortRoleTransSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortRoleTransSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortStateTransSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortStateTransSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortStateTransSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortTopoChSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortTopoChSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortTopoChSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortTxSmStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortTxSmStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortTxSmState );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortRxRstBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortRxRstBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortRxRstBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortRxConfigBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortRxConfigBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortRxConfigBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortRxTcnBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortRxTcnBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortRxTcnBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortTxRstBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortTxRstBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortTxRstBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortTxConfigBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortTxConfigBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortTxConfigBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortTxTcnBpduCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortTxTcnBpduCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortTxTcnBpduCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortInvalidRstBpduRxCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortInvalidRstBpduRxCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortInvalidRstBpduRxCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortInvalidConfigBpduRxCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortInvalidConfigBpduRxCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortInvalidConfigBpduRxCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortInvalidTcnBpduRxCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortInvalidTcnBpduRxCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortInvalidTcnBpduRxCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortProtocolMigrationCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortProtocolMigrationCountGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                UINT4 *pu4FsPbRstCVlanPortProtocolMigrationCount );

/********************************************************************
* FUNCTION NcFsPbRstCVlanPortEffectivePortStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsPbRstCVlanPortEffectivePortStateGet (
                INT4 i4FsPbRstPort,
                INT4 i4FsPbRstCepSvid,
                INT4 *pi4FsPbRstCVlanPortEffectivePortState );

/* END i_ARICENT_PB_RSTP_MIB.c */


#endif
