
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrstwr.h,v 1.20 2017/09/12 14:08:12 siva Exp $
*
* Description:  Function Prototype for SNMP in RSTP/MSTP
*********************************************************************/
#ifndef _FSRSTWR_H
#define _FSRSTWR_H

VOID RegisterFSRST(VOID);

VOID UnRegisterFSRST(VOID);
INT4 FsRstSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsRstModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRstTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsRstDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRstpUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRstpDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstBufferFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstMemAllocFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstNewRootIdCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRoleSelSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstOldDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsRstSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsRstModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRstTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsRstDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsRstSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);




INT4 GetNextIndexFsRstPortExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsRstPortGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortInfoSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortMigSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRoleTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortStateTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTopoChSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTxSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAutoEdgeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedTCNGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDURxGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDUTxGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortIsL2GpGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortLoopGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsRstStpPerfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRootGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortErrorRecoveryGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortStpModeDot1wEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardActionGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRcvInfoWhileExpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRcvInfoWhileExpTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortImpStateOccurCountGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortImpStateOccurTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAutoEdgeSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedTCNSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDURxSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDUTxSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortIsL2GpSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortLoopGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsRstStpPerfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRootGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortErrorRecoverySet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortStpModeDot1wEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardActionSet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortAutoEdgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortRestrictedTCNTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDURxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortEnableBPDUTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortIsL2GpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortLoopGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstStpPerfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortRootGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortErrorRecoveryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortStpModeDot1wEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortBpduGuardActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstPortExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 FsRstDynamicPathcostCalculationGet(tSnmpIndex *, tRetVal *);
INT4 FsRstCalcPortPathCostOnSpeedChgGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstRcvdPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsRstBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsRstDynamicPathcostCalculationSet(tSnmpIndex *, tRetVal *);
INT4 FsRstCalcPortPathCostOnSpeedChgSet(tSnmpIndex *, tRetVal *);
INT4 FsRstBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsRstDynamicPathcostCalculationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstCalcPortPathCostOnSpeedChgTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstDynamicPathcostCalculationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstCalcPortPathCostOnSpeedChgDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstBpduGuardDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstStpPerfStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRstSetTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsRstGenTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstSetTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsRstSetTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRstSetTrapsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsRstPortTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsRstPortMigrationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPktErrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPktErrValGet(tSnmpIndex *, tRetVal *);
INT4 FsRstPortRoleTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsRstOldRoleTypeGet(tSnmpIndex *, tRetVal *);
#endif /* _FSRSTWR_H */
