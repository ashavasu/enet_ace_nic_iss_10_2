/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astextn.h,v 1.11 2014/04/16 12:51:22 siva Exp $
 * $Id: astextn.h,v 1.11 2014/04/16 12:51:22 siva Exp $
 *
 * Description: This file contains all the external declarations for
 *              the RSTP Module. 
 *
 *******************************************************************/


#ifndef _ASTEXTN_H_
#define _ASTEXTN_H_


VOID SNMP_AGT_RIF_Notify_V1_Or_V2_Trap (UINT4 u4_Version,
                                   tSNMP_OID_TYPE * pEnterpriseOid,
                                   UINT4 u4_gen_trap_type,
                                   UINT4 u4_spec_trap_type,
                                   tSNMP_VAR_BIND * vb_list);


extern UINT4 gu4StpCliContext;
#ifdef PBB_PERFORMANCE_WANTED
extern struct timeval      gCurTime;
extern struct timeval      gStTime;
extern struct timeval      gEnTime;
extern UINT4               gu4TimeTaken;
extern UINT4               gu4TotalTimeTaken;
#endif
extern INT1                gi1MstTxAllowed;
extern UINT4       gCurrContextId;
#endif /* _ASTEXTN_H_ */

