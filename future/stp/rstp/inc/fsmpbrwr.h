
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: fsmpbrwr.h,v 1.6 2012/02/29 09:37:20 siva Exp $
 *
 *******************************************************************/

#ifndef _FSMPBRWR_H
#define _FSMPBRWR_H
INT4 GetNextIndexFsMIPbRstContextInfoTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPBR(VOID);

VOID UnRegisterFSMPBR(VOID);
INT4 FsMIPbProviderStpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbProviderStpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbProviderStpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstContextInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIPbRstCVlanBridgeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbRstCVlanBridgeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeTxHoldCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanStpDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanBridgeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIPbRstCVlanPortInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbRstCVlanPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortAdminEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortOperEdgePortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortAdminPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortOperPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortAutoEdgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIPbRstCVlanPortSmTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbRstCVlanPortInfoSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortMigSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortRoleTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortStateTransSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortTopoChSmStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortTxSmStateGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIPbRstCVlanPortStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbRstCVlanPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRstCVlanPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);


#endif /* _FSMPBRWR_H */
