/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astdbg.h,v 1.17 2013/12/07 10:59:28 siva Exp $
 *
 * Description: This file contains declarations of various Debug 
 *              Options used in RSTP and MSTP Modules.
 *
 *******************************************************************/


#ifndef _ASTDBG_H_
#define _ASTDBG_H_

/* Debug flags */
#define   AST_DBG_FLAG        (AST_CURR_CONTEXT_INFO () == NULL) ? (0): (AST_CURR_CONTEXT_INFO ())->u4DebugOption
#define   AST_GLB_DBG_FLAG    (gAstGlobalInfo.u4GlobalDebugOption)
    
/* Module names */
#define   AST_MOD_NAME                 "AST"

#define AST_INIT_SHUT_DBG              0x00000001
#define AST_MGMT_DBG                   0x00000002
#define AST_MEM_DBG                    0x00000004
#define AST_BPDU_DBG                   0x00000008
#define AST_EVENT_HANDLING_DBG         0x00000010
#define AST_TMR_DBG                    0x00000020
#define AST_PISM_DBG                   0x00000040
#define AST_PRSM_DBG                   0x00000080
#define AST_RSSM_DBG                   0x00000100
#define AST_RTSM_DBG                   0x00000200
#define AST_STSM_DBG                   0x00000400
#define AST_PMSM_DBG                   0x00000800
#define AST_TCSM_DBG                   0x00001000
#define AST_TXSM_DBG                   0x00002000
#define AST_BDSM_DBG                   0x00004000
#define AST_ALL_FAILURE_DBG            0x00008000
#define AST_RED_DBG                    0x00010000
#define AST_SM_VAR_DBG                 0x00020000
#define AST_PSSM_DBG                   0x00040000
#define AST_ALL_FLAG 524287

extern UINT4 gu4StpTrcLvl;
#define STP_TRC_LVL        gu4StpTrcLvl
/* Trace definitions */
#if defined RSTP_DEBUG && defined TRACE_WANTED
#define AST_GLOBAL_DBG  AstGlobalDebug
 
#define AST_DBG(DebugType, Str)                                \
     if (AST_DBG_FLAG & (DebugType)) \
        UtlTrcLog(AST_DBG_FLAG, DebugType, AST_MOD_NAME,  PRINT_CONTEXT(Str))
        
#define AST_DBG_ARG1(DebugType, Str, Arg1)                                   \
     if (AST_DBG_FLAG & (DebugType)) \
        UtlTrcLog(AST_DBG_FLAG, DebugType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1)

#define AST_DBG_ARG2(DebugType, Str, Arg1, Arg2)                             \
     if (AST_DBG_FLAG & (DebugType)) \
         UtlTrcLog(AST_DBG_FLAG, DebugType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1, Arg2)

#define AST_DBG_ARG3(DebugType, Str, Arg1, Arg2, Arg3)                       \
     if (AST_DBG_FLAG & (DebugType)) \
         UtlTrcLog(AST_DBG_FLAG, DebugType, AST_MOD_NAME, PRINT_CONTEXT(Str), Arg1, Arg2, Arg3)

#define AST_DBG_ARG4(DebugType, Str, Arg1, Arg2, Arg3, Arg4)      \
     if (AST_DBG_FLAG & (DebugType)) \
         UtlTrcLog(AST_DBG_FLAG, DebugType, AST_MOD_NAME, PRINT_CONTEXT(Str), \
                        Arg1, Arg2, Arg3, Arg4)


#define AST_PRINTF(fmt) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt)
#define AST_PRINTF1(fmt,x1) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt,x1)
#define AST_PRINTF2(fmt,x1,x2) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt,x1,x2)
#define AST_PRINTF3(fmt,x1,x2,x3) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt,x1, x2, x3)
#define AST_PRINTF4(fmt,x1,x2,x3,x4) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt,x1,x2,x3,x4)
#define AST_PRINTF5(fmt,x1,x2,x3,x4,x5) \
             UtlTrcLog((UINT4)255, (UINT4)255, AST_MOD_NAME, fmt,x1,x2,x3,x4,x5)

#else  /* RSTP_DEBUG */
#define AST_GLOBAL_DBG  AstGlobalDebug
#define AST_DBG(DebugType, Str)
#define AST_DBG_ARG1(DebugType, Str, Arg1)\
{ \
  UNUSED_PARAM (Arg1); \
}
#define AST_DBG_ARG2(DebugType, Str, Arg1, Arg2)\
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
}
#define AST_DBG_ARG3(DebugType, Str, Arg1, Arg2, Arg3)\
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
}
#define AST_DBG_ARG4(DebugType, Str, Arg1, Arg2, Arg3, Arg4) \
{ \
  UNUSED_PARAM (Arg1); \
  UNUSED_PARAM (Arg2); \
  UNUSED_PARAM (Arg3); \
  UNUSED_PARAM (Arg4); \
}
   
#define AST_PRINTF(x)
#define AST_PRINTF1(fmt,x1)
#define AST_PRINTF2(fmt,x1,x2)
#define AST_PRINTF3(fmt,x1,x2,x3)
#define AST_PRINTF4(fmt,x1,x2,x3,x4)
#define AST_PRINTF5(fmt,x1,x2,x3,x4,x5)
             
#endif /* RSTP_DEBUG */

#define AST_PRINT  UtlTrcLog

#endif/* _ASTDBG_H_ */

