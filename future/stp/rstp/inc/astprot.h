/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astprot.h,v 1.104 2017/12/11 09:58:31 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the RSTP and MSTP Modules.
 *
 *******************************************************************/


#ifndef _ASTPROT_H_
#define _ASTPROT_H_

UINT4
AstGetSysTime (tOsixSysTime * pSysTime);
/******************************************************************************/
/*                                   astrsys.c                                */
/******************************************************************************/

/* SYSTEM MODULE */
INT4 RstModuleInit (VOID);
INT4 RstModuleShutdown (VOID);
INT4 RstModuleEnable (VOID);
INT4 RstModuleDisable (VOID);
INT4 RstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum);
INT4 RstDeletePort (UINT2);
INT4 AstEnablePort (UINT2 u2Port, UINT2 u2MstInst, UINT1 u1TrigType);
INT4 AstDisablePort (tAstMsgNode *pMsgNode);
INT4 RstEnablePort (UINT2, UINT1);
INT4 RstDisablePort (UINT2,UINT1);
VOID RstInitGlobalBrgInfo (VOID);
VOID RstInitPerStBrgInfo (tAstPerStBridgeInfo *);
VOID RstInitGlobalPortInfo (UINT2, tAstPortEntry *);
VOID RstInitPerStPortInfo (UINT2, UINT2, tAstPerStPortInfo *); 
VOID RstInitStateMachines(VOID);
INT4 RstStartSemsForPort (UINT2 u2PortNum);
VOID RstReInitRstpInfo (VOID);
VOID RstResetCounters (VOID);
/*RSTP clear statistics feature*/
VOID RstResetPortCounters(UINT2 u2PortNum);
/*RSTP clear statistics feature*/
INT4 AstAssertBegin (VOID);
INT4
AstHandleEnablePortMsg (UINT2 u2Port, UINT2 u2InstanceId, UINT1 u1TrigType);
INT4 RstComponentInit (VOID);
INT4 RstComponentShutdown (VOID);
INT4 RstComponentEnable (VOID);
INT4 RstComponentDisable(VOID);
INT4 RstComponentDeletePort (UINT2 u2PortNum);

INT4 AstHandleBridgeDownStatus (VOID);
INT4 AstHandleBridgeStartStatus (VOID);
INT4 RstUpdtProtoPortStateInL2Iwf (UINT2 u2Port, UINT1 u1TrigType);
INT4 RstReCalculatePathcost (UINT4);
INT4 RstSetPortBPDUGuard (UINT4 u4PortNo,UINT4 u4GuardType, UINT4 u4BpduGuardStatus);
INT4 RstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction);
INT4 AstSetGlobalBPDUGuard (UINT4 u4BpduGuardStatus);

/******************************************************/
/*          PERFORMANCE CHANGES              */
/******************************************************/
INT4 MstPortTxSmTxMstpWr (tAstPortEntry * pAstPortEntry, UINT2 u2MstInst);
VOID MstPUpdtAllSyncedFlag(UINT2 u2MstInst, tAstPerStPortInfo * pPerStPortInfo, UINT2 u2mode);

/******************************************************************************/
/*                                   astptmr.c                                */
/******************************************************************************/

/* TIMER MODULE */
INT4 AstTimerInit (VOID);
INT4 AstTimerDeInit (VOID);
INT4 AstStartTimer (VOID *, UINT2, UINT1, UINT2);
INT4 AstStopTimer (VOID *, UINT1);
UINT4 AstGetRemainingTime (VOID *, UINT1, UINT4*);
INT4 RstTmrExpiryHandler (tAstTimer *);
INT4 RstStopAllRunningTimers (tAstPerStInfo *pPerStInfo,
         tAstPortEntry *pAstPortEntry,
                              tAstPerStPortInfo *pPerStPortInfo);
INT4 AstTmrExpiryHandler (VOID);
VOID AstSelectContextFromTmrInfo (tAstContextInfo *pContext);
VOID AstFlushFdbOnInstance (UINT2 u2InstId);
INT4 AstRestartStateMachines (VOID);
INT4 AstHandleImpossibleState (UINT2 u2PortNo);

/******************************************************************************/
/*                                  astrpism.c                                */
/******************************************************************************/

/* PORT INFORMATION STATE MACHINE */
INT4 RstPortInfoMachine(UINT2 u2Event, tAstPerStPortInfo *pPerStPortInfo,
                        tAstBpdu *pRcvdBpdu );
INT4 RstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeDisabled (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmClearedBegin (tAstPerStPortInfo * pPerStPortInfo, 
                                tAstBpdu * pRcvdBpdu);
INT4 RstPortInfoSmMakeAged (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeUpdate (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeCurrent (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeReceive (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeSuperior (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmMakeRepeat (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmRcvInfo (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmGetMsgType (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmUpdtBpduVersion (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmSetTcFlags (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmRecordProposal (tAstPerStPortInfo *, tAstBpdu *);
INT4 RstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo *);
VOID RstPortInfoSmRecordTimes (tAstPortEntry * pRstPortInfo,
                               tAstBpdu * pRcvdBpdu);
INT4 RstPortInfoSmMakeNotDesg (tAstPerStPortInfo * pPerStPortInfo,
                          tAstBpdu * pRcvdBpdu);
VOID RstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                              tAstBpdu * pRcvBpdu);
INT4 RstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                               tAstBpdu * pRcvdBpdu);
INT4 RstPortInfoSmMakeInferiorDesg (tAstPerStPortInfo * pPerStPortInfo,
                                    tAstBpdu * pRcvdBpdu);
VOID RstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                                 tAstBpdu * pRcvdBpdu);
INT4 RstPortInfoSmEventImpossible (tAstPerStPortInfo *, tAstBpdu *);
VOID RstInitPortInfoMachine(VOID);


/******************************************************************************/
/*                                  astppmsm.c                                */
/******************************************************************************/

/* PORT MIGRATION STATE MACHINE */
INT4 RstPortMigrationMachine (UINT2 u2Event, UINT2 u2PortNum);
INT4 RstPmigSmMakeCheckingRstp (UINT2 u2PortNum);
INT4 RstPmigSmMakeSelectingStp (UINT2 u2PortNum);
INT4 RstPmigSmMakeSensing (UINT2 u2PortNum);
INT4 RstPmigSmSenseMigration (UINT2 u2PortNum);
INT4 RstPmigSmStartMDelayWhile (UINT2 u2PortNum);
INT4 RstPmigSmEventImpossible (UINT2 );
VOID RstInitProtocolMigrationMachine (VOID);


/******************************************************************************/
/*                                  astprssm.c                                */
/******************************************************************************/

/* PORT ROLE SELECTION STATE MACHINE */
INT4 RstPortRoleSelectionMachine (UINT2, UINT2);
INT4 RstProleSelSmMakeInit (UINT2);
INT4 RstProleSelSmMakeRoleSelection (UINT2);
VOID RstProleSelSmUpdtRoleDisabledTree(UINT2);
VOID RstProleSelSmClearReselectTree (UINT2);
INT4 RstProleSelSmUpdtRolesTree (UINT2);
INT4 RstIsDesgPrBetterThanPortPr(UINT2 u2PortNum, UINT2 u2InstanceId);
INT4 RstProleSelSmSetSelectedTree (UINT2);
INT4 RstProleSelSmEventImpossible (UINT2);
VOID RstInitPortRoleSelectionMachine(VOID);


/******************************************************************************/
/*                                   astrprsm.c                               */
/******************************************************************************/

/* PORT RECEIVE STATE MACHINE */
INT4 RstPortReceiveMachine (UINT2 u2Event,
                            tAstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 RstPortRcvSmMakeDiscard (tAstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 RstPortRcvSmMakeReceive (tAstBpdu * pRcvBpdu, UINT2 u2PortNum);
VOID RstPortRcvUpdtBpduVersion (tAstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 RstPortRcvSmChkRcvdBpdu (tAstBpdu * pRcvBpdu, UINT2 u2PortNum);
VOID RstInitPortRxStateMachine (VOID);

/******************************************************************************/
/*                                  astrrtsm.c                                */
/******************************************************************************/

/* PORT ROLE TRANSITION STATE MACHINE */
INT4 RstPortRoleTrMachine (UINT2 , tAstPerStPortInfo * );
INT4 RstProleTrSmUpdtInfoReset (tAstPerStPortInfo *); 
INT4 RstProleTrSmRerootSetRootPort (tAstPerStPortInfo * );
INT4 RstProleTrSmRerootSetDesgPort (tAstPerStPortInfo * );
INT4 RstProleTrSmSyncSetDesgPort (tAstPerStPortInfo *);
INT4 RstProleTrSmFwdExpRootPort (tAstPerStPortInfo *);
INT4 RstProleTrSmFwdExpDesgPort (tAstPerStPortInfo * );
INT4 RstProleTrSmRrWhileExpDesgPort (tAstPerStPortInfo *);
INT4 RstProleTrSmRbWhileExpRootPort (tAstPerStPortInfo *);
INT4 RstProleTrSmIsReRooted (UINT2 );
INT4 RstProleTrSmRoleChanged (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeInitPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeDisablePort (tAstPerStPortInfo * pPerStPortInfo);
INT4 RstProleTrSmMakeDisabledPort (tAstPerStPortInfo * pPerStPortInfo);
INT4 RstProleTrSmMakeBlockPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeBlockedPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeRootPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeAlternatePort (tAstPerStPortInfo * );
tAstBoolean RstIsTreeAllSynced (UINT2 u2PortNum);
INT4 RstProleTrSmIndicateAllSyncedSet (UINT2 u2ThisPort);
INT4 RstProleTrSmAllSyncedSetRootPort (tAstPerStPortInfo * );
INT4 RstProleTrSmAllSyncedSetAlternatePort (tAstPerStPortInfo * );
INT4 RstProleTrSmRerootedSetRootPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeDesgPort (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeForward (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeLearn (tAstPerStPortInfo * );
INT4 RstProleTrSmMakeDesignatedDiscard (tAstPerStPortInfo * );
INT4 RstProleTrSmSetSyncTree   (UINT2  u2PortNum);
INT4 RstProleTrSmSetReRootTree (UINT2 u2PortNum);
VOID RstInitPortRoleTrMachine(VOID);
INT4 RstProleTrSmProposedSetRootPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmProposedSetAlternatePort (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmAgreedSetDesgPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmOperEdgeSetDesgPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmRootPortTransitions (tAstPerStPortInfo *pPerStPortInfo,
                                     tAstBoolean bAllSyncedEvent);
INT4 RstProleTrSmDesgPortTransitions (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmAltPortTransitions (tAstPerStPortInfo *pPerStPortInfo,
                                     tAstBoolean bAllSyncedEvent);
INT4 RstProleTrSmDisputedSetDesgPort (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstProleTrSmTransitionToRootFwding (tAstPerStPortInfo * pPerStPortInfo);
INT4 RstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo);


/******************************************************************************/
/*                                  astpstsm.c                                */
/******************************************************************************/

/* PORT STATE TRANSITION STATE MACHINE */
INT4 RstPortStateTrMachine (UINT2 , tAstPerStPortInfo *, UINT2 ); 
INT4 RstPstateTrSmMakeDiscarding (tAstPerStPortInfo *, UINT2 );
INT4 RstPstateTrSmMakeLearning (tAstPerStPortInfo *, UINT2 );
INT4 RstPstateTrSmMakeForwarding (tAstPerStPortInfo *, UINT2 );
INT4 RstPstateTrSmEnableLearning (UINT2 , UINT2 );
INT4 RstPstateTrSmDisableLearning (UINT2 , UINT2 );
INT4 RstPstateTrSmEnableForwarding (UINT2 , UINT2 );
INT4 RstPstateTrSmDisableForwarding (UINT2 , UINT2 );
INT4 RstPstateTrSmEventImpossible (tAstPerStPortInfo *, UINT2 );
INT4 RstPstateTrSmEventNotHandle (tAstPerStPortInfo *, UINT2 );
VOID RstInitPortStateTrMachine (VOID);


/******************************************************************************/
/*                                  astrtcsm.c                                */
/******************************************************************************/

/* TOPOLOGY CHANGE STATE MACHINE */
INT4 RstTopoChMachine (UINT2 u2Event, tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeInit (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeInactive (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeLearning (tAstPerStPortInfo * pPerStPortInfo);
INT4 RstTopoChSmMakeDetected (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeNotifiedTcn (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeNotifiedTc (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakePropagating (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeAcknowledged (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmMakeActive (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmChkInactive (tAstPerStPortInfo * pPerStPortInfo);
INT4 RstTopoChSmChkDetected (tAstPerStPortInfo * pPerStPortInfo);
VOID RstTopoChSmFlushFdb (UINT2 u2PortNum, INT4 i4OptimizeFlag);
INT4 RstTopoChSmSetTcPropTree (tAstPerStPortInfo *pPerStPortInfo);
INT4 RstTopoChSmNewTcWhile (tAstPerStPortInfo *pPerStPortInfo);
#ifdef MRP_WANTED
INT4 RstTopoChSmNewTcDetected (tAstPerStPortInfo *pPerStPortInfo);
#endif
INT4 RstTopoChSmEventImpossible (tAstPerStPortInfo *pPerStPortInfo);
VOID RstInitTopoChStateMachine (VOID);
                                 

/******************************************************************************/
/*                                  astptxsm.c                                */
/******************************************************************************/

/* PORT TRANSMIT STATE MACHINE */
INT4 RstPortTransmitMachine (UINT2 u2Event, tAstPortEntry *pAstPortEntry,
                             UINT2 u2InstanceId);
INT4 RstPortTxSmMakeTransmitInit (tAstPortEntry *pAstPortEntry,
                                  UINT2 u2InstanceId);
INT4 RstPortTxSmMakeTransmitConfig (tAstPortEntry *pAstPortEntry,
                                    UINT2 u2InstanceId);
INT4 RstPortTxSmMakeTransmitTcn (tAstPortEntry *pAstPortEntry,
                                 UINT2 u2InstanceId);
INT4 RstPortTxSmMakeTransmitRstp (tAstPortEntry *pAstPortEntry,
                                  UINT2 u2InstanceId);
INT4 RstPortTxSmMakeTransmitPeriodic (tAstPortEntry *pAstPortEntry,
                                      UINT2 u2InstanceId);
INT4 RstPortTxSmMakeIdle (tAstPortEntry *pAstPortEntry, UINT2 u2InstanceId);
INT4 RstPortTxSmCheckIdle (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId);
INT4 RstPortTxSmNewInfoSetIdle (tAstPortEntry *pAstPortEntry,
                                UINT2 u2InstanceId);
INT4 RstPortTxSmSelectedSetIdle (tAstPortEntry * pAstPortEntry, 
                                 UINT2 u2InstanceId);
INT4 RstPortTxSmHoldTmrExpIdle (tAstPortEntry *pAstPortEntry,
                                UINT2 u2InstanceId);
INT4 RstPortTxSmTxConfig (tAstPortEntry *pAstPortEntry, UINT2 u2InstanceId);
INT4 RstPortTxSmTxTcn (tAstPortEntry *pAstPortEntry, UINT2 u2InstanceId);
INT4 RstPortTxSmTxRstp (tAstPortEntry *pAstPortEntry, UINT2 u2InstanceId);
INT4 RstFormBpdu (tAstPortEntry *pAstPortEntry, UINT2 u2InstanceId,
                  UINT1 u1MsgType, tAstBufChainHeader *pBuf,
                  UINT4 *pu4DataLength,UINT1 *pu1flag);
VOID RstFillBpdu (tAstPerStPortInfo *pPerStPortInfo, UINT1 **pu1Bpdu);
INT4 RstPortTxSmEventImpossible (tAstPortEntry *pAstPortEntry,
                                 UINT2 u2InstanceId);
VOID RstInitPortTxStateMachine (VOID);
INT4 AstFillEnetHeader (tAstBufChainHeader *pBpdu, UINT4 *pu4DataLength, 
                        tAstPortEntry *pPortEntry);


/******************************************************************************/
/*                                  astrutil.c                                */
/******************************************************************************/

/* UTILITY ROUTINES */
INT4 RstCompareBrgId(tAstBridgeId *, tAstBridgeId *);
INT4 RstGetBridgeAddr(tAstMacAddr *pBrgAddr);
UINT4 AstMapSpeedToPathcost (UINT4 u4Speed, UINT4 u4HighSpeed);
UINT4 AstCalculatePathcost (UINT2 u2PortNum);
INT4  AstDeleteAllContextsAndMemPools (VOID);
#ifdef RSTP_DEBUG
VOID RstDumpGlobalBrgInfo(VOID);
VOID RstDumpPerStBrgInfo(UINT2 u2InstanceId);
VOID RstDumpGlobalPortInfo(UINT2 u2PortNum);
VOID RstDumpRstPortInfo(UINT2 u2PortNum, UINT2 u2InstanceId);
VOID RstDumpPerStPortInfo(UINT2 u2PortNum, UINT2 u2InstanceId);
VOID RstBufDump(tAstBufChainHeader *pBuf);
#endif
VOID RstPktDump(tAstBufChainHeader *pMsg, UINT2 u2Length);
VOID AstIncrInvalidBpduCounter (UINT2 u2PortNum, UINT1 u1BpduType, 
                                UINT1 u1Version);

VOID AstSnmpFreeVarBind (tSNMP_VAR_BIND *pVbList);
#ifdef NPAPI_WANTED
INT4 RstDisableStpInHw (VOID);
VOID RstRetryNpapi (tAstPortEntry *pPortInfo);
#endif
INT4 AstPathcostConfiguredFlag (UINT2 u2LocalPortId, UINT2 u2InstanceId);
INT4 AstCreateSpanningTreeInst (UINT2 u2MstInst, tAstPerStInfo **ppPerStInfo);
VOID AstDeleteMstInstance (UINT2 u2MstInst, tAstPerStInfo *pPerStInfo);
INT4 AstReleasePortMemBlocks (tAstPortEntry *pPortInfo);
INT4 AstAllocPortAndPerStInfoBlock (UINT1 u1Mode);
INT4 AstReleasePortAndPerStInfoBlock (VOID);
INT4 AstAllocUpCountBlock (UINT1 u1Mode);
VOID AstFlushFdbEntries (tAstPortEntry *pPortInfo, UINT1 u1Optimize);
VOID AstConvertPortListToArray (tLocalPortList  PortList,UINT1 *pu4IfPortArray, UINT1 *u1NumPorts);
VOID AstGetRoleStr(UINT1 u1Role, UINT1* u1RoleString);
VOID AstShowSystemTime(VOID);
PUBLIC VOID RstUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat);
#ifdef MSTP_WANTED
INT4 AstSetCistPortMacEnabled (UINT2 u2LocalPortId,
          INT4 i4PortAdminPointToPoint);
#endif
/******************************************************************************/
/*                                  astpbdsm.c                                */
/******************************************************************************/

/* BRIDGE DETECTION STATE MACHINE */
INT4 RstBrgDetectionMachine (UINT2 u2Event, UINT2 u2PortNum);
INT4 RstBdSmInit (UINT2 u2PortNum);
VOID RstInitBrgDetectionStateMachine (VOID);
INT4 RstBdSmMakeEdge (UINT2 u2PortNum);
INT4 RstBdSmChkMakeEdge (UINT2 u2PortNum);
INT4 RstBdSmMakeNotEdge (UINT2 u2PortNum);
INT4 RstBdSmAdminEdgeOrPortDisabled (UINT2 u2Portum);
INT4 RstBdSmStartEdgeDelayWhile (UINT2 u2Portum);


/******************************************************************************/
/*                                  astpmsg.c                                 */
/******************************************************************************/

/* MESSAGE PROCESSING ROUTINES */
INT4 AstTaskInit (VOID);
INT4 AstTaskDeinit (VOID);
INT4 AstModuleInit (VOID);
INT4 AstInit (VOID);
INT4 AstReceivedBpdu (tAstBufChainHeader * pBuf, UINT2 u2PortNum);
INT4 RstHandleInBpdu (tAstBpdu *, UINT2);
VOID RstPortRcvdFlagsStatsUpdt (tAstBpdu *pRstBpdu, UINT2 u2PortNum,
                           UINT2 u2InstanceId);
INT4 AstHandleLocalMsgReceived (tAstMsgNode *);
INT4 AstValidateReceivedBpdu (UINT2 u2PortNum, UINT2 u2DataLength, 
                              UINT1 * pu1Bpdu, tAstBpduType * pBpdu);
VOID AstExtractReceivedBpdu (UINT2 u2ProtocolId, UINT1 u1Version, 
                             UINT1 u1BpduType, UINT2 u2DataLength, 
                             UINT1 * pu1Bpdu, tAstBpduType * pBpdu);
INT4 AstHandleCreatePort (tAstMsgNode *pMsgNode);
INT4 AstHandleDeletePort (tAstMsgNode *pMsgNode);
INT4 AstDefaultCfgMsgHandler (tAstMsgNode * pMsgNode);
INT4 AstHwPortStateUpdtInd (tAstPortEntry *pPortEntry, UINT2 u2InstanceId, 
                            UINT2 u2HwPortState,UINT1 u1Status );
INT4 AstHwPortStateUpdtSuccess(tAstPortEntry *pPortEntry, UINT2 u2InstanceId,
                               UINT2 u2HwPortState);
INT4 AstHwPortStateUpdtFailure(tAstPortEntry *pPortEntry, UINT2 u2InstanceId,
                               UINT2 u2HwPortState);
VOID AstHandlePortSpeedChgIndication (UINT4 u4IfIndex);
VOID AstAssignMempoolIds (VOID);
INT4 AstHandleCreatePortFromLa (tAstMsgNode * pMsgNode);
INT4 AstHandleDeletePortFromLa (tAstMsgNode * pMsgNode);


/******************************************************************************/
/*                                  astrsnmp.c                                */
/******************************************************************************/

/* SNMP INTERFACE ROUTINES */
INT4 RstSetBridgePriority (UINT2);
INT4 RstSetDynamicPathcostCalc (UINT1);
INT4 RstSetDynaPathcostCalcLagg (UINT1);
INT4 AstSetForceVersion (UINT1);
INT4 RstSetPortPriority (UINT2, UINT1);
INT4 RstSetPortPathCost (UINT2, UINT4);
INT4 AstSetPortPathcostConfigured (UINT2 u2PortNum, UINT2 u2InstanceId, 
                                   UINT4 u4Val);
INT4 RstUpdatePortPathCost (UINT2  u2PortNum, UINT4 u4PathCost);
INT4 AstTriggerRoleSeletionMachine (UINT2 u2PortNum, UINT2 u2InstId);
INT4 RstSetZeroPortPathCost (UINT2 u2PortNum);
INT4 AstTrigAutoEdgeToBrgDetSem (UINT2 u2PortNum, tAstBoolean bStatus);
INT4 AstSetProtocolMigration (UINT2);
INT4 AstSetAdminEdgePort (UINT2, tAstBoolean);
INT4 RstSetAdminPointToPoint (UINT2, UINT1);
VOID AstSetMaxAge (UINT2, UINT2);
VOID AstSetHelloTime (UINT2, UINT2);
VOID AstSetForwardDelay (UINT2, UINT2);
VOID AstSetTxHoldCount (UINT1);

INT4 AstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus);
INT4  RstSetPortRestrictedRole(tAstPortEntry *pPortEntry, tAstBoolean  bStatus);
INT4  RstSetPortRootGuard(tAstPortEntry *pPortEntry, tAstBoolean  bStatus);

INT4 AstSetPseudoRootId (UINT2 u2PortNum, UINT2 u2InstanceId,
                         tAstMacAddr BrgMacAddr, UINT2 u2BrgPriority);

INT4 AstSetPortL2gpStatus (UINT2 u2PortNum, tAstBoolean bIsL2gp);

INT4 AstSetPortBpduRxStatus (UINT2 u2PortNum, tAstBoolean bEnableBPDURx);

INT4 AstSetPortBpduTxStatus (UINT2 u2PortNum, tAstBoolean bEnableBPDUTx);

INT4 RstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean  bStatus);

INT4
RstSetPortErrorRecovery (UINT2 u2PortNo, UINT4 u4ErrorRecovery);

INT4 RstSetPortDot1WEnable (UINT2 u2PortNum, tAstBoolean  bStatus);

/******************************************************************************/
/*                                  astrslow.c                                */
/******************************************************************************/

/* SNMP LOW-LEVEL VALIDATE UTILITY ROUTINES */
INT4 AstSendEventToAstTask (tAstMsgNode * pNode, UINT4 u4ContextId);
INT4 AstSnmpLowValidatePortIndex (INT4 i4Dot1dStpPort);
INT4 AstValidatePortEntry (INT4 i4Dot1dStpPort);
INT4 AstSnmpLowGetFirstValidIndex (INT4 *pi4Dot1dStpPort);
INT4 AstSnmpLowGetNextValidIndex (INT4 i4Dot1dStpPort, 
                                  INT4 *pi4NextDot1dStpPort);

VOID AstGlobalMemFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen,
                           UINT4 u4ContextId);
VOID
AstRstInstTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen, UINT1 u1Status);

VOID AstBufferFailTrap  (INT1*, UINT1);
VOID AstMemFailTrap  (INT1*, UINT1);
VOID AstNewRootTrap (tAstBridgeId , tAstBridgeId ,UINT2, INT1*, UINT1);
VOID AstTopologyChangeTrap (UINT2, INT1*, UINT1);
VOID AstProtocolMigrationTrap (UINT2 , UINT1 , UINT1 , INT1*, UINT1);
VOID AstNewPortRoleTrap (UINT2 , UINT1 ,UINT1 , UINT2 , INT1 *, UINT1);

VOID AstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid,UINT1 u1OidLen, VOID *pTrapInfo);
tSNMP_OID_TYPE     *AstMakeObjIdFromDotNew (INT1 *textStr);

VOID AstInvalidBpduRxdTrap (UINT2 , UINT1 , UINT2 , INT1*, UINT1);
VOID AstHwFailTrap (UINT2 u2PortNum, UINT2 u2InstId,UINT2 u2PortState,
                    INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID AstLoopIncStateChangeTrap(UINT2 , UINT4 ,UINT2, INT1* , UINT1);
VOID AstBPDUIncStateChangeTrap(UINT2 , UINT4 ,UINT2, INT1* , UINT1);
VOID AstRootIncStateChangeTrap(UINT2 , UINT4 ,UINT2, INT1* , UINT1);
VOID AstPortStateChangeTrap(UINT2 , UINT2 , UINT4 ,UINT2, INT1* , UINT1);

VOID RstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid,UINT1 u1OidLen, VOID *pTrapInfo);
tSNMP_OID_TYPE     *RstMakeObjIdFromDotNew (INT1 *textStr);
INT1 * RstParseSubIdNew (INT1 *pi1TempPtr, UINT4 *pu4Value);

INT4 AstProcessSnmpRequest (tAstMsgNode * pNode);
INT4 AstHandleConfigMsg (tAstMsgNode * pMsgNode); 

/******************************************************************************/
/*                                  rstpapi.c                                 */
/******************************************************************************/

INT4 AstProcessMessage (tAstMsgNode * pNode);

/******************************************************************************/
/*                                  rstpmbsm.c                                */
/******************************************************************************/
#ifdef MBSM_WANTED
INT4 AstMbsmUpdateCardInsertion ( tMbsmPortInfo *, tMbsmSlotInfo *);

INT1 FsMiRstpMbsmNpSetPortState (UINT4 , UINT4 , UINT1 , tMbsmSlotInfo * );

VOID AstPbMbsmSetCvlanPortStatesInHw (tAstPortEntry *pInPortEntry, 
                                      tMbsmSlotInfo *pSlotInfo);
INT1 FsMiPbRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                   tVlanId SVid, UINT1 u1Status, 
                                   tMbsmSlotInfo * pSlotInfo);
#endif

VOID AstHandleBpduMsg (tAstBufChainHeader *pBpdu);
VOID AstHandleSnmpCfgMsg (tAstMsgNode *pMsgNode);

/**********************************
 * Prototypes for MI routines 
 **********************************/ 

VOID STPRegisterMIBS (VOID);
INT4 AstHandleCreateContext (UINT4 u4ContextId);
INT4 AstHandleDeleteContext (UINT4 u4ContextId);
INT4 AstHandleUpdateContextName (UINT4 u4ContextId);
INT4 AstSelectContext (UINT4 u4ContextId);
INT4 AstReleaseContext (VOID);
INT4 AstGetFirstActiveContext(UINT4 *);
INT4 AstGetNextActiveContext(UINT4 ,UINT4 *);
INT4 AstGetFirstPortInContext (UINT4, INT4 *);
INT4 AstGetNextPortInContext (UINT4, INT4 , INT4 *);
INT4 AstGetFirstPortInCurrContext (INT4 *);
INT4 AstGetNextPortInCurrContext (INT4 i4IfIndex, INT4 *pi4NextIfIndex);

INT4 AstGlobalMemoryInit (VOID);
INT4 AstGlobalMemoryDeinit (VOID);
INT4 AstInitContextInfo (UINT4 u4ContextId);
tAstPortEntry * AstGetIfIndexEntry (UINT4 u4IfIndex);
tAstPortEntry *AstGetFirstIfIndexEntry (VOID);
tAstPortEntry *AstGetNextIfIndexEntry (tAstPortEntry *pAstPortEntry);
tAstPortEntry * AstGetNextIfEntryBasedOnIfIndex (UINT4 u4IfIndex);
INT4 AstIfIndexTblCmpFn (tRBElem * pKey1, tRBElem * pKey2);
INT4 AstAddToIfIndexTable (tAstPortEntry *pAstPortEntry);
INT4 AstRemoveFromIfIndexTable (tAstPortEntry *pAstPortEntry);
INT4 AstFreePortEntry (tRBElem *pElem, UINT4 u4Arg);
VOID AstTrapFillContextInfo (tSNMP_VAR_BIND **ppVbList, 
                             tSNMP_COUNTER64_TYPE SnmpCounter64Type, 
        UINT1 u1Set);
INT4 AstGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);
INT4 AstGetHwPortStateStatus (UINT4 u4IfIndex, INT4 i4Inst, INT4 *pi4PortStateStatus);
INT4 AstGetPseudoRootIdConfigStatus (UINT2, UINT2, UINT4);
INT4 AstDeriveOperatingMode (INT4 *pi4Mode);
INT4 AstGetNumOfPortsCreated (UINT4 *pu4PortsCreated);
UINT1 AstIsPathCostSet ARG_LIST((UINT2 u2PortNum, UINT2 u2InstanceId));
INT4 AstLockAndSelCliContext (VOID);

/******************************************
 * Prototypes for PB routines
 ******************************************/
INT4 AstPbSetPortPathCostInCvlanComp (tAstPortEntry *pParentPortEntry, 
                                      UINT4 u4PortPathCost);
INT4 AstPbCVlanSetAdminOperPtoP (UINT2 u2Port, UINT1 u1AdminPToP, 
                                 tAstBoolean bOperPToP);
INT4 AstPbSetPortAdminEdgeInCvlanComp (tAstPortEntry * pParentPortEntry,
                                       tAstBoolean bStatus);

INT4
AstPbSetPseudoRootIdInCvlanComp (tAstPortEntry * pParentPortEntry,
                                 tAstMacAddr BrgMacAddr, UINT2 u2BrgPriority);
INT4
AstPbSetL2gpInCvlanComp (tAstPortEntry * pParentPortEntry,
                         tAstBoolean bIsL2gp);
INT4
AstPbSetBpduRxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDURx);
INT4
AstPbSetBpduTxInCvlanComp (tAstPortEntry * pParentPortEntry,
                           tAstBoolean bEnableBPDUTx);


/******************************************************************************/
/*                                   astpport.c                               */
/******************************************************************************/
INT4 AstCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);
INT4 AstCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);
INT4 AstCfaGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus);
INT4 AstCfaGetIfaceType (UINT4 u4IfIndex, UINT1 *pu1IfType);
VOID AstCfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp);

INT4 AstL2IwfMiGetVlanUnTagPorts (UINT4 u4ContextId, tVlanId VlanId,
                                  tPortList UnTagPorts);
INT4 AstL2IwfIsPvidVlanOfAnyPorts(UINT4 u4ContextId, tVlanId VlanId);
INT4 AstL2IwfMiGetVlanEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tPortList EgressPorts);

#ifdef MSTP_WANTED
VOID AstAttrRPUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, 
                                 UINT2 u2NewMapId, UINT2 u2OldMapId, 
                                 tVlanId VlanId, UINT2 u2MapEvent, 
                                 UINT1 *pu1Result);
INT4 AstVlanMiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId);
UINT1 AstVlanMiGetVlanLearningMode (UINT4 u4ContextId);
#endif
INT4 AstVlanFlushFdbEntries (UINT4 u4Port, UINT4 u4FdbId, INT4 i4OptimizeFlag);
INT4 AstGarpIsGarpEnabledInContext (UINT4 u4ContextId);
INT4 AstMrpIsMrpStartedInContext (UINT4 u4ContextId);
VOID AstIssGetContextMacAddress (UINT4 u4ContextId, tMacAddr pSwitchMac);
INT4 AstIssGetAsyncMode (INT4 i4ProtoId);

INT4 AstVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);
INT4 AstVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId, 
                                      UINT2 *pu2LocalPortId);
INT4 AstVcmGetSystemMode (UINT2 u2ProtocolId);
INT4 AstVcmGetSystemModeExt (UINT2 u2ProtocolId);
INT4 AstVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum);
INT4 AstVcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    UINT4 *pu4IfIndex);
INT4 AstVlanDeleteFdbEntries (UINT4 u4Port, INT4 i4OptimizeFlag);
INT4 AstVlanGetStartedStatus (UINT4 u4ContextId);
INT4 AstVlanIsVlanEnabledInContext (UINT4 u4ContextId);
INT4 AstL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);
INT4 AstL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                         UINT2 *pu2NextLocalPort, 
                                         UINT4 *pu4NextIfIndex);
INT4 AstL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex, 
                                UINT1 *pu1OperStatus);
INT4 AstL2IwfGetPortVlanTunnelStatus (UINT4 u4IfIndex, BOOL1 * pbTunnelPort);
VOID AstL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                            UINT1 *pu1TunnelStatus);
INT4 AstL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 u4IfIndex, UINT4 u4PktSize, 
                                      UINT2 u2Protocol, UINT1 u1Encap);
INT4 AstL2IwfIsPortInPortChannel (UINT4 u4IfIndex);
UINT4 AstL2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId);
BOOL1 AstL2IwfMiIsVlanActive (UINT4 u4ContextId, tVlanId VlanId);
INT4 AstL2IwfSendCustBpduOnPep (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4PktSize);
INT4 AstL2IwfSetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 bPerfDataStatus);
INT4 AstL2IwfGetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 *pbPerfDataStatus);
INT4
AstL2IwfSetPortOperPointToPointStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                       BOOL1 bOperPointToPoint);

INT4
AstL2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList);

INT4
AstMrpApiNotifyPortRoleChange (UINT4 u4IfIndex, UINT2 u2InstID, UINT1 u1OldRole,
                               UINT1 u1NewRole);
INT4
AstMrpApiNotifTcDetectedTmrState (UINT4 u4IfIndex, UINT2 u2InstID,
                                  UINT1 u1TmrState);
INT4
AstPortMrpApiNotifyStpInfo (tMrpInfo *pMrpInfo);

#ifdef L2RED_WANTED
#ifdef VLAN_WANTED
UINT4 AstVlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, 
                                    UINT2 u2DataLen);
#endif
#ifdef LLDP_WANTED
UINT4 AstLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif
UINT4 AstRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, 
                               UINT4 u4SrcEntId, UINT4 u4DestEntId);
UINT4 AstRmGetNodeState (VOID);
UINT1 AstRmGetStandbyNodeCount (VOID);
UINT1 AstRmGetStaticConfigStatus (VOID);
UINT1 AstRmHandleProtocolEvent (tRmProtoEvt *pEvt);
UINT4 AstRmRegisterProtocols (tRmRegParams * pRmReg);
UINT4 AstRmDeRegisterProtocols (VOID);
UINT4 AstRmReleaseMemoryForMsg (UINT1 *pu1Block);
INT4 AstRmSetBulkUpdatesStatus (UINT4 u4AppId);
#endif
INT4 AstVlanApiIsPvidVlanOfAnyPorts(tVlanId VlanId);
/******************************************************************************/
/*                                   astppssm.c                               */
/******************************************************************************/
INT4 RstPseudoInfoMachine (UINT1 u1Event, UINT2 u2PortNum,
                           tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmCheckL2gp (UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmMakeInit (UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmMakePseudoInfoRx (UINT2 u2PortNum,
                                      tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmMakeBpduDiscard (UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmMakeBpduCheck (UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmBpduCheckAndDiscard (UINT2 u2PortNum,
                                         tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmMakePseudoInfoHelloExpiry (UINT2 u2PortNum,
                                               tAstBpduType *pRcvdBpdu);

INT4 RstPseudoInfoSmEventImpossible (UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

INT4 RstPreparePseudoInfo (UINT2 u2PortNum, tAstBpdu **ppPeudoRstBpdu);

INT4 RstCheckBPDUConsistency (UINT2 u2PortNum, tAstBpdu *pRcvdBpdu);

VOID RstInitPseudoInfoMachine (VOID);

/******************************************************************************/
/*                                   astpl2wr.c                               */
/******************************************************************************/
INT4 AstL2IwfCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);
INT4 AstL2IwfDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);
INT4 AstL2IwfGetNextCVlanPort (UINT4 u4CepIfIndex, UINT2 u2PrevPort, 
                               UINT2 *pu2NextPort, UINT4 *pu4NextCepIfIndex);
INT4 AstL2IwfGetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                      BOOL1 * pbOperEdge);
INT4 AstL2IwfGetPbPortOperStatus (UINT1 u1Module, UINT4 u4CepIfIndex, 
                                  tVlanId SVlanId, UINT1 *pu1OperStatus);
UINT1 AstL2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId);
INT4 AstL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);
INT4 AstL2IwfGetInterfaceType (UINT4 u4IfIndex, UINT1 *pu1PortType);
INT4 AstL2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                                   UINT1 *pu1ServiceType);
INT4 AstL2IwfPbSetInstCepPortState (UINT4 u4CepIfIndex, UINT1 u1PortState);
INT4 AstL2IwfSetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                                      BOOL1 bOperEdge);
INT4 AstL2IwfSetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId, 
                             UINT1 u1PortState);
INT4 AstL2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, 
                                             UINT2 u2LocalPortId,
                                             UINT2 u2Protocol, UINT1 u1Status);
#ifdef MSTP_WANTED
INT4 AstL2IwfFillConfigDigest (UINT4 u4ContextId, UINT1 au1DigPtr[], 
                               INT4 i4NumEntries);
UINT2 AstL2IwfMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                                       UINT1 *pu1VlanList);
UINT2 AstL2IwfMiGetVlanMapInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                                      UINT1 *pu1VlanList);
#endif
UINT2 AstL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId);
UINT1 AstL2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex);
INT4 AstL2IwfGetPortOperEdgeStatus (UINT2 u2IfIndex, BOOL1 * pbOperEdge);
INT4 AstL2IwfSetInstPortState (UINT2 u2MstInst, UINT2 u2IfIndex, 
                               UINT1 u1PortState);
INT4 AstL2IwfSetPortOperEdgeStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    BOOL1 bOperEdge);
INT4 AstL2IwfSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, 
                                 UINT2 u2MstInst);
INT4 AstL2IwfVlanToInstMappingInit (UINT4 u4ContextId);
VOID AstL2IwfGetDefaultVlanId (UINT2 *pDefaultVlanId);

VOID
AstDeriveMacAddrFromPortType (UINT2 u2PortNum);

INT4 AstGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);

#ifdef PVRST_WANTED
INT4 AstL2IwfGetVlanPortPvid (UINT2 u2IfIndex, tVlanId * pVlanId);
INT4 AstL2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType);
INT4 AstL2IwfSetVlanPortPvid (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                              tVlanId VlanId);
INT4 AstL2IwfAllocInstActiveStateMem (UINT4 u4ContextId, UINT1 u1Mode);
INT4 AstL2IwfAllocInstPortStateMem (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    UINT1 u1Mode);

#endif
INT4 AstL2IwfMiGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
     tLocalPortList LocalEgressPorts);

INT4 AstCheckBrgPortTypeChange (UINT2 u2PortNum);

INT4 AstHandlePortTypeChange (tAstPortEntry *pPortEntry, UINT1 u1NewPortType);
INT4
MstSispHandleUpdateSispStatus (tAstMsgNode * pMsgNode);

INT4 MstVcmSispGetPhysicalPortOfSispPort (UINT4  u4IfIndex, UINT4 *pu4PhyIndex);

UINT1 AstSispIsSispEnabledOnPort (UINT4 u4IfIndex);

INT4
AstCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4
MstVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
       UINT1 u1RetLocalPorts,
       VOID *paSispPorts, 
       UINT4 *pu4PortCount);

INT4 AstL2IwfGetMemberVlanList (UINT4  u4ContextId, UINT4  u4IfIndex,
    tSNMP_OCTET_STRING_TYPE * pVlanIdList);

INT1 AstSispIsSispPortPresentInCtxt (UINT4 u4ContextId);

#ifdef MBSM_WANTED
INT4 
AstSispMbsmUpdateCardInsertion (UINT4 u4PortNo, tMbsmSlotInfo * pSlotInfo);
#endif

INT4 AstMbsmProgramPortProperties (UINT4  u4Port, tMbsmSlotInfo * pSlotInfo);

INT4 MstSispCopyPortPropToSispLogical (UINT4 u4IfIndex, UINT1 u1Property,
           tAstBoolean bStatus);
   
INT4 
AstRegisterWithPacketHandler (VOID);

INT1
AstValidateComponentId (UINT4 u4ComponentId,UINT4 u4Port);

INT4  AstUtilGetPortAdminPointToPoint(tStpConfigInfo *pStpConfigInfo);
INT4  AstUtilGetPortOperPointToPoint(tStpConfigInfo *pStpConfigInfo);
INT4  AstUtilSetPortAdminPointToPoint(tStpConfigInfo *pStpConfigInfo);
INT4  AstUtilTestPortAdminPointToPoint(tStpConfigInfo *pStpConfigInfo);
#ifdef NPAPI_WANTED
INT4  AstHandlePacketToNP(tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,UINT4 u4PktSize);
INT4
AstL2IwfGetPotentialTxPortForAgg (UINT4 u4IfIndex,UINT2 *pu2EgressPort);
INT4
AstL2IwfGetPortPnacAuthStatus (UINT4 u4IfIndex,UINT2 *u2PortAuthStatus);
INT4
AstCfaPostPacketToNP(tCRU_BUF_CHAIN_HEADER *pBuf,UINT2 u2EgressPort,UINT4 u4PktSize);
#endif




/* Modified for Attachment Circuit interface */
INT4  AstIsExtInterface (INT4 i4PortId);
INT4  AstCreateExtInterface (INT4 i4PortId);
INT4  AstDeleteExtInterface (INT4 i4PortId);
INT4  AstUpdateOperStatus (UINT4 u4IfIndex, UINT1 u1Status);
VOID  AstIndicateSTPStatus (UINT4 u2PortNum, BOOL1 bSTPStatus);

VOID                AstInitializeTxBuf (VOID);
VOID                AstTransmitMstBpdu (VOID);
INT4 AstPortLaDisableOnMcLagIf (VOID);
VOID
AstIcchGetIcclIfIndex (UINT4 *pu4IfIndex);
UINT1
AstLaIsMclagInterface (UINT4 u4IfIndex);
UINT1
AstLaGetMCLAGSystemStatus (VOID);
VOID
STPCfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp);

UINT4
AstCfaGetGlobalModTrc(VOID);

INT1 PvrstAssertBegin(VOID);

VOID
AstGetStateStr(UINT1 u1State, UINT1 *u1StateString);

UINT2
AstGetSyslogLevel (UINT2 u2TraceType);
INT4
AstL2IwfIsPortInInst(UINT4 u4ContextId,UINT4 u4IfIndex,INT4 i4MstInst);
INT4
AstVlanDeleteVlanEntries (UINT4 u4Vlan, INT4 i4OptimizeFlag);
INT4 AstL2IwfGetConfiguredPortsForPortChannel(UINT2 u2AggId, UINT2 au2ConfPorts[],
                                      UINT2 *pu2NumPorts);
#endif /* _ASTPROT_H_ */
