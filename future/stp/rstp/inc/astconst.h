/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astconst.h,v 1.81 2017/12/29 09:31:22 siva Exp $
 *
 * Description: This file contains all the constants used by
 *              RSTP and MSTP modules.
 *
 *******************************************************************/


#ifndef _ASTCONST_H_
#define _ASTCONST_H_

/******************************************************************************/
/*                              General Constants                             */
/******************************************************************************/
#define AST_FORCE_TRUE                 0
#define AST_FORCE_FALSE                1
#define AST_AUTO                       2

#define AST_DEFAULT_CONTEXT           L2IWF_DEFAULT_CONTEXT
#define AST_IFINDEX_STR_LEN           24
#define AST_CONTEXT_STR_LEN           VCM_ALIAS_MAX_LEN
/*This macro is used for creation of mempool of PerstPortInfo array for each
 * context. This macro ensures that there is memory for port table in a instance 
 * per context */
#define AST_MAX_INST_PER_CONTEXT      1
#define AST_MIN_NUM_PORTS             1
#define AST_MAX_NUM_VLANS             VLAN_MAX_VLAN_ID 
#define AST_MAX_MSTI                  64 
#define AST_MAX_OCTET_LEN             256
#define AST_MSG_INVALID_PORT          0 /* The value that is filled in the 
                                         * IfIndex field of queue messages
                               * if it is not a port based message */
#define AST_MIN_TRACE_VAL             0
#define AST_MAX_TRACE_VAL             255
#define AST_MIN_DEBUG_VAL             0
#define MST_MAX_DEBUG_VAL             524287
#define RST_MAX_DEBUG_VAL             524287

#define AST_TRANS_INTRANSITION 1
#define AST_TRANS_FINISHED 2

#define AST_VID_MAP_LEN                   4096

#define AST_PORT_OPER_UP              CFA_IF_UP

#define AST_LINK_FULL_DUPLEX          ETH_STATS_DUP_ST_FULLDUP 

#define AST_RSTP_MODULE               1
#define AST_MSTP_MODULE               2
#define AST_PVRST_MODULE              3

#define AST_STP_COMPATIBLE_MODE       0
#define AST_RST_MODE                  2
#define AST_MST_MODE                  3
#define AST_PVRST_MODE                4

#define AST_UP                        CFA_IF_UP
#define AST_DOWN                      CFA_IF_DOWN

#define  AST_QUEUE_MODE               0

#define AST_MUT_EXCL_SEM_NAME         (const UINT1 *) "AMSM" 
#define AST_SEM_INIT_COUNT            1

#define AST_SYS_NUM_OF_TIME_UNITS_IN_A_SEC  SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define AST_BPDU_EVENT                0x0001
#define AST_MSG_EVENT                 0x0002
#define AST_TMR_EXPIRY_EVENT          0x0004
#define AST_RM_EVENT                  0x0008
#define AST_RED_BULK_UPD_EVENT        0x0010


#define AST_MAXAGE_MSG                1
#define AST_HELLOTIME_MSG             2
#define AST_FORWARDDELAY_MSG          3
#define AST_CREATE_PORT_MSG           4
#define AST_ENABLE_PORT_MSG           5
#define AST_DISABLE_PORT_MSG          6
#define AST_DELETE_PORT_MSG           7
#define AST_ENABLE_RST_MSG            8
#define AST_DISABLE_RST_MSG           9
#define AST_ENABLE_MST_MSG            10
#define AST_DISABLE_MST_MSG           11
#define AST_PORT_PRIORITY_MSG         12
#define AST_BRIDGE_PRIORITY_MSG       13
#define AST_PATH_COST_MSG             14
#define AST_ALL_PORT_DISABLE_MSG      15
#define AST_FORCE_VERSION_MSG         16
#define AST_PROTOCOL_MIGRATION_MSG    17
#define AST_ADMIN_EDGEPORT_MSG        18
#define AST_ADMIN_PTOP_MSG            19
#define AST_TX_HOLDCOUNT_MSG          20
#define AST_START_RST_MSG             22
#define AST_STOP_RST_MSG              23
#define AST_START_MST_MSG             24
#define AST_STOP_MST_MSG              25
#define AST_AUTO_EDGEPORT_MSG         26
#define AST_DYNAMIC_PATHCOST_MSG      37
#define AST_CREATE_CONTEXT_MSG        38
#define AST_DELETE_CONTEXT_MSG        39
#define AST_MAP_PORT_MSG              40
#define AST_UNMAP_PORT_MSG            41
#define AST_CREATE_PEP_PORT_MSG       42
#define AST_DELETE_PEP_PORT_MSG       43
#define AST_ENABLE_PEP_PORT_MSG       44
#define AST_DISABLE_PEP_PORT_MSG      45
#define AST_SVLAN_ENABLE_MSG          46
#define AST_SVLAN_DISABLE_MSG         47
#define AST_BRIDGE_START_MSG          48
#define AST_BRIDGE_DOWN_MSG           49
#define AST_HW_PEP_CREATE_MSG         50
#define AST_UPDATE_CONTEXT_NAME       51
#define AST_START_PVRST_MSG                     52
#define AST_STOP_PVRST_MSG                      53
#define AST_ENABLE_PVRST_MSG                    54
#define AST_DISABLE_PVRST_MSG                   55
#define AST_PVRST_PORT_HELLO_TIME_MSG           56
#define AST_PVRST_ADD_TRUNK_PORT_MSG            57
#define AST_PVRST_ADD_ACCESS_PORT_MSG         58
#define AST_PVRST_CONFIG_STATUS_PORT_MSG        59
#define AST_PVRST_SET_ENCAP_TYPE_PORT_MSG       60
#define AST_PVRST_VLAN_CREATED_MSG              61
#define AST_PVRST_VLAN_DELETE_MSG               62
#define AST_PVRST_ALL_VLAN_DELETE_MSG           63
#define AST_PVRST_CONFIG_BPDUGUARD_PORT_MSG     64
#define AST_PVRST_CONFIG_ROOTGUARD_PORT_MSG     65
#define AST_PVRST_HELLOTIME_MSG              66
#define AST_PVRST_FORWARDDELAY_MSG              67
#define AST_PVRST_TX_HOLDCOUNT_MSG              68
#define AST_PVRST_INSTANCE_PATHCOST_MSG         70
#define AST_PVRST_BRG_MAXAGE_MSG          71
#define AST_PVRST_INST_PORT_PATHCOST_MSG        72
#define AST_SPANNING_TREE_PORT_UP           73
#define AST_SPANNING_TREE_PORT_DOWN       74
#define AST_PVRST_CONFIG_INST_STATUS_PORT_MSG   75
#define AST_PVRST_PVID_MSG                      76

#define AST_ZERO_PATHCOST_MSG                   77

#define AST_PORT_PSEUDO_ROOTID_CONF_MSG   78
#define AST_PORT_L2GP_CONF_MSG            79
#define AST_PORT_BPDU_RX_CONF_MSG         80
#define AST_PORT_BPDU_TX_CONF_MSG         81
#define AST_HW_PORTSTATE_UPDATED_MSG      82
#define AST_UPDT_PORT_TYPE_MSG            83
#define AST_LOOP_GUARD_MSG                84
#define AST_SISP_STATUS_MSG               85
#define AST_SISP_VLAN_UPDT_MSG            86
#define AST_PORT_SPEED_CHANGE_MSG         87
#define AST_ENABLE_PORT_SPEED_CHG_MSG     88
#define AST_CREATE_PORT_FROM_LA_MSG       89
#define AST_DELETE_PORT_FROM_LA_MSG       90
#define AST_RST_CONFIG_BPDUGUARD_PORT_MSG 91
#define AST_MST_CONFIG_BPDUGUARD_PORT_MSG 92
#define AST_GBL_BPDUGUARD_ENABLE_MSG      93
#define AST_GBL_BPDUGUARD_DISABLE_MSG     94
#define AST_PORT_ERROR_RECOVERY_MSG       95
#define AST_PVRST_ADD_TAGGED_PORT_MSG     96
#define AST_PVRST_DELETE_TAGGED_PORT_MSG  97
#define AST_PVRST_HYBRID_PORT_MSG         98
#define AST_DOT1W_ENABLED_MSG             99
#define AST_RST_BPDUGUARD_ACTION_MSG      100
#define AST_PVRST_BPDUGUARD_ACTION_MSG    101
#define AST_MST_BPDUGUARD_ACTION_MSG      102


#define AST_ROUND_OFF_VAL         50

#define PVRST_MAX_DEBUG_VAL               524287

#define AST_EXT_PORT_UP               1
#define AST_EXT_PORT_DOWN             2
#define AST_STP_PORT_UP               3
#define AST_STP_PORT_DOWN             4

#define AST_ENABLE_PORT               1
#define AST_DISABLE_PORT              2
/* QMSG types */
#define AST_SNMP_CONFIG_QMSG          1
#define AST_BPDU_RCVD_QMSG            2 

#define AST_RM_QMSG                   3  

#define AST_OK                        1
#define AST_NOT_OK                    2

#define AST_IS_MGMT_TIME_IN_SEC(x)    (AST_OK)
#define AST_SYS_TO_MGMT(x)            ((x))
#define AST_MGMT_TO_SYS(x)            ((x))
#define AST_CENTI_SECONDS              100
/* This macro converts milliseconds to centi-seconds */
#define AST_MS_TO_CS(x)               ((x)/10)

#define AST_PROT_TO_BPDU(x)           ((x)*256)
#define AST_BPDU_TO_PROT(x)           ((x)/256)
#define AST_PROT_TO_BPDU_SEC(x)       ((x)/AST_CENTI_SECONDS)
#define AST_PROT_TO_BPDU_CENTI_SEC(x) ((x)%AST_CENTI_SECONDS)

#define AST_BPDU_TO_PROT_CENTI_SEC(u1Val, pu1Bpdu, u2Res) \
{\
        AST_GET_1BYTE (u1Val, pu1Bpdu);\
        u2Res = (u1Val * AST_CENTI_SECONDS);\
        AST_GET_1BYTE (u1Val, pu1Bpdu);\
        u2Res += u1Val;\
}\

#define AST_BPDU_CALC_ROUND_OFF(x,y)\
{\
     y = AST_PROT_TO_BPDU_CENTI_SEC (x);\
     if (y !=0) \
     {\
        if (y >= AST_ROUND_OFF_VAL) \
        {\
            x = (UINT2) ((x-y) + (1 * AST_CENTI_SECONDS));\
        }\
        else \
        {\
           y = AST_PROT_TO_BPDU_SEC(x);\
           x = (UINT2) (y * AST_CENTI_SECONDS);\
        }\
    }\
}\

#define AST_BRGPRIORITY_MASK                0xF000
#define AST_BRGPRIORITY_PERMISSIBLE_MASK    0x0FFF
#define AST_BRGPRIORITY_MIN_VAL             0x0000
#define AST_BRGPRIORITY_MAX_VAL             0xFFFF

#define AST_PORT_ID_SIZE                    2
#define AST_BRG_PRIORITY_SIZE               2

#define AST_PORTPRIORITY_MASK               0xF0
#define AST_PORTPRIORITY_PERMISSIBLE_MASK   0x0F
#define AST_PORTPRIORITY_MIN_VAL            0x00
#define AST_PORTPRIORITY_MAX_VAL            0xF0

#define AST_INVALID_PORT_NUM          0xFFFF
#ifndef MSTP_WANTED
#define AST_MAX_ETH_FRAME_SIZE        1522 
#else
#define AST_MAX_ETH_FRAME_SIZE       \
(AST_MAX_MST_INSTANCES > (64+1) ? (AST_ENET_HEADER_SIZE + AST_LLC_HEADER_SIZE+ \
 MST_BPDU_CIST_LENGTH + (MST_BPDU_MSTI_LENGTH * AST_MAX_MST_INSTANCES) + 1) : 1522)
#endif
#define AST_MAC_ADDR_SIZE             6
#define AST_INIT_VAL                  0
#define AST_NO_VAL                    0
#define AST_OFFSET                    0
#define AST_MEMSET_VAL                0
#define AST_PORT_PRIORITY_MASK        0xF000
#define AST_PORTNUM_MASK              0x0FFF
#define AST_ROOT_DEFAULT              3

#define AST_PORTPRIORITY_BIT_OFFSET  8

#define AST_ENET_DEST_SRC_ADDR_SIZE   12
#define AST_BPDU_OFFSET_FROM_ENETLEN  5

#define AST_PVRST_VLAN_OFFSET                   20
#define AST_PVRST_LLC_OFFSET                    2

#define AST_PROTOCOL_ID                0x0 
#define AST_VERSION_0                  0
#define AST_VERSION_1                  1
#define AST_VERSION_2                  2
#define AST_VERSION_3                  3

#define AST_VERSION_1_LENGTH           0
#define AST_VERSION_3_LENGTH           0

#define MST_PORTORVLAN_PER_BYTE           8
#define MAX_PORT_NUM                   (CONTEXT_PORT_LIST_SIZE *8)
/******************************************************************************/
/*                             Timer Module Constants                         */
/******************************************************************************/

#define AST_TMR_TYPE_FDWHILE           1
#define AST_TMR_TYPE_HELLOWHEN         2
#define AST_TMR_TYPE_MDELAYWHILE       3
#define AST_TMR_TYPE_RBWHILE           4
#define AST_TMR_TYPE_RCVDINFOWHILE     5
#define AST_TMR_TYPE_RRWHILE           6
#define AST_TMR_TYPE_TCWHILE           7
#define AST_TMR_TYPE_HOLD              8
#define AST_TMR_TYPE_EDGEDELAYWHILE    9 
#define AST_TMR_TYPE_RESTART           10
#define AST_TMR_TYPE_RAPIDAGE_DURATION 11 
#define AST_TMR_TYPE_PSEUDOINFOHELLOWHEN 12
#define AST_TMR_TYPE_TCDETECTED        13
#define AST_TMR_TYPE_FLUSHTGR          14
#define AST_TMR_TYPE_RCVDINFOWHILE_NO_SYNC 15
#define AST_TMR_TYPE_EDGEDELAYWHILE_NO_SYNC 16
#define AST_TMR_TYPE_ERROR_DISABLE_RECOVERY 17
#define AST_TMR_TYPE_INC_RECOVERY 18
#define AST_TMR_TYPE_ROOT_INC_RECOVERY 19
#define AST_TMR_TYPE_MST_FLUSH_OPT     200
#define AST_TMR_TYPE_INST_CREATE 20
#define AST_DEF_INST_CREATE_TIME 200

/* HOLD timer is in Centi-Seconds*/
#define AST_HOLD_TIME                  (1 * AST_CENTI_SECONDS)

#define AST_NUM_TMR_INTERVAL_SPLITS    2

/******************************************************************************/
/*                             Rst Protocol Constants                         */
/******************************************************************************/
#define AST_SHUTDOWN                   3
#define RST_SHUTDOWN                   AST_SHUTDOWN
#define MST_SHUTDOWN                   AST_SHUTDOWN
#define PVRST_SHUTDOWN                 AST_SHUTDOWN

#define RST_PORT_ENABLED               1
#define RST_PORT_DISABLED              2

#define RST_PORT_OPER_ENABLED          1
#define RST_PORT_OPER_DISABLED         2


#define RST_DEFAULT_VLAN_CONTEXT       1
/* HELLO, MAX and FWD are in Centi-Seconds */
#define RST_DEFAULT_BRG_HELLO_TIME     (2 * AST_CENTI_SECONDS) 
#define RST_DEFAULT_BRG_MAX_AGE        (20 * AST_CENTI_SECONDS)
#define RST_DEFAULT_BRG_FWD_DELAY      (15 * AST_CENTI_SECONDS)
#define RST_DEFAULT_BRG_TX_LIMIT       6 
#define RST_DEFAULT_BRG_PRIORITY       0x8000
#define RST_DEFAULT_PORT_PRIORITY      0x80
#define RST_DEFAULT_MIGRATE_TIME       3
#define RST_DEFAULT_FLUSH_IND_THRESHOLD 0
#define RST_DEFAULT_FLUSH_INTERVAL     0
#define RST_DEFAULT_RESTART_INTERVAL     40
#define AST_TIME_DIV_INTERVAL          4
#define AST_DEFAULT_ERROR_RECOVERY     (300 * AST_CENTI_SECONDS)
#define AST_ROOT_INC_RECOVERY_TIME  (25 * AST_CENTI_SECONDS)

#define AST_DEFAULT_AUTOEDGE_VALUE     RST_TRUE

#define RST_FLAG_MASK_TC               0x01         
#define RST_FLAG_MASK_PROPOSAL         0x02
#define RST_FLAG_MASK_PORT_ROLE        0x0C
#define RST_FLAG_MASK_LEARNING         0x10
#define RST_FLAG_MASK_FORWARDING       0x20
#define RST_FLAG_MASK_AGREEMENT        0x40 
#define RST_FLAG_MASK_TCACK            0x80


#define RST_SET_CONFIG_TOPOCH_ACK_FLAG   0x80
#define RST_SET_RST_TOPOCH_ACK_FLAG      0x00
#define RST_SET_AGREEMENT_FLAG           0x40
#define RST_SET_FORWARDING_FLAG          0x20
#define RST_SET_LEARNING_FLAG            0x10
#define RST_SET_PROPOSAL_FLAG            0x02
#define RST_SET_TOPOCH_FLAG              0x01

#define RST_SET_UNKNOWN_PROLE_FLAG       0x00
#define RST_SET_ALTBACK_PROLE_FLAG       0x04
#define RST_SET_ROOT_PROLE_FLAG          0x08
#define RST_SET_DESG_PROLE_FLAG          0x0C

#define AST_PATHCOST_TYPE_16BIT          1
#define AST_PATHCOST_TYPE_32BIT          2

#define AST_PATHCOST_CALC_MANUAL         1
#define AST_PATHCOST_CALC_DYNAMIC        2
              
#define AST_TXHOLDCOUNT_MIN_VAL          1
#define AST_TXHOLDCOUNT_MAX_VAL          10

/******************************************************************************/
/*                         Rst & Mst Protocol Constants                       */
/******************************************************************************/
#define AST_PORT_SPEED_1KBPS           1000
#define AST_PORT_SPEED_100KBPS         100000
#define AST_PORT_SPEED_1MBPS           1000000
#define AST_PORT_SPEED_4MBPS           4000000
#define AST_PORT_SPEED_10MBPS          10000000
#define AST_PORT_SPEED_100MBPS         100000000
#define AST_PORT_SPEED_1GBPS           1000000000
/* Speed more than 10GBPS is given in units of MBPS */
#define AST_PORT_SPEED_10GBPS          10000
#define AST_PORT_SPEED_100GBPS         100000
#define AST_PORT_SPEED_1TBPS           1000000

   
#define AST_PORT_PATHCOST_100KBPS      200000000
#define AST_PORT_PATHCOST_1MBPS        20000000
#define AST_PORT_PATHCOST_10MBPS       2000000
#define AST_PORT_PATHCOST_100MBPS      200000
#define AST_PORT_PATHCOST_1GBPS        20000
#define AST_PORT_PATHCOST_10GBPS       2000
#define AST_PORT_PATHCOST_100GBPS      200
#define AST_PORT_PATHCOST_1TBPS        20
#define AST_PORT_PATHCOST_10TBPS       2

#define AST_PORT_PATHCOST_16BIT_MAX    65535

#define AST_32BIT_MAX                  4294967295U  /* 0xffffffff */
#define AST_LAGG_COST_DEDUCT           100
                                       /* The value deducted from pathcost
                                          for a portchannel inorder to give
                                          preference over physical ports*/
#define AST_BPDU_TYPE_CONFIG           0x0
#define AST_BPDU_TYPE_TCN              0x80
#define AST_BPDU_TYPE_RST              0x02
#define AST_BPDU_TYPE_MST              0x03

#define AST_TYPE_LEN_SIZE              2

#define AST_ENET_LLC_HEADER_SIZE       17   
#define AST_BPDU_LENGTH_CONFIG         35
#define AST_BPDU_LENGTH_TCN            4
#define AST_BPDU_LENGTH_RST            36
#define RST_MAX_BPDU_SIZE              AST_BPDU_LENGTH_RST

#define AST_PSEUDO_BPDU_SIZE                  sizeof(tAstBpdu)
#define MST_PSEUDO_BPDU_SIZE                  sizeof(tMstBpdu)

#define RST_SUPERIOR_DESG_MSG          1
#define RST_REPEATED_DESG_MSG          2
#define RST_CONFIRMED_ROOT_MSG         3
#define RST_OTHER_MSG                  4
#define RST_INFERIOR_DESG_MSG          5

#define RST_INFERIOR_ROOT_ALT_MSG      6
#define RST_INFOIS_DISABLED            1
#define RST_INFOIS_AGED                2
#define RST_INFOIS_MINE                3
#define RST_INFOIS_RECEIVED            4


#define RST_BRGID1_INFERIOR           -1
#define RST_BRGID1_SUPERIOR            1
#define RST_BRGID1_SAME                0

#define RST_INFERIOR_MSG              -1
#define RST_BETTER_MSG                 1
#define RST_SAME_MSG                   0

#define LOOP_INC_RECOVERY         1

/******************************************************************************/
/*                                 Performance Data                           */
/******************************************************************************/
/* Event Types */
enum {
    AST_CONFIGURATION = 1,
    AST_TIMER_EXPIRY,
    AST_BPDU
};

/******************************************************************************/
/*                         Port Information SEM Constants                     */
/******************************************************************************/


#define RST_PINFOSM_MAX_STATES            9
#define RST_PINFOSM_STATE_DISABLED        0
#define RST_PINFOSM_STATE_AGED            1
#define RST_PINFOSM_STATE_UPDATE          2
#define RST_PINFOSM_STATE_SUPERIOR        3
#define RST_PINFOSM_STATE_REPEAT          4
#define RST_PINFOSM_STATE_NOT_DESG        5
#define RST_PINFOSM_STATE_CURRENT         6
#define RST_PINFOSM_STATE_RECEIVE         7
#define RST_PINFOSM_STATE_INFERIOR_DESG   8

#define RST_PINFOSM_MAX_EVENTS            7
#define RST_PINFOSM_EV_BEGIN              0
#define RST_PINFOSM_EV_BEGIN_CLEARED      1
#define RST_PINFOSM_EV_PORT_ENABLED       2
#define RST_PINFOSM_EV_PORT_DISABLED      3
#define RST_PINFOSM_EV_RCVD_BPDU          4
#define RST_PINFOSM_EV_RCVDINFOWHILE_EXP  5
#define RST_PINFOSM_EV_UPDATEINFO         6 


/******************************************************************************/
/*                         Port Role Selection SEM Constants                  */
/******************************************************************************/


#define RST_PROLESELSM_MAX_STATES            2
#define RST_PROLESELSM_STATE_INIT_BRIDGE     0
#define RST_PROLESELSM_STATE_ROLE_SELECTION  1

#define RST_PROLESELSM_MAX_EVENTS            3
#define RST_PROLESELSM_EV_BEGIN              0
#define RST_PROLESELSM_EV_BEGIN_CLEARED      1
#define RST_PROLESELSM_EV_RESELECT           2


/******************************************************************************/
/*                         Port Receive SEM Constants                         */
/******************************************************************************/


#define RST_PRCVSM_MAX_STATES                2
#define RST_PRCVSM_STATE_DISCARD             0
#define RST_PRCVSM_STATE_RECEIVE             1

#define RST_PRCVSM_MAX_EVENTS                3
#define RST_PRCVSM_EV_BEGIN                  0
#define RST_PRCVSM_EV_RCVD_BPDU              1
#define RST_PRCVSM_EV_PORT_DISABLED          2


/******************************************************************************/
/*                         Port Role Transition SEM Constants                 */
/******************************************************************************/


#define RST_PROLETRSM_MAX_STATES           7
#define RST_PROLETRSM_STATE_INIT_PORT      0
#define RST_PROLETRSM_STATE_DISABLE_PORT   1
#define RST_PROLETRSM_STATE_DISABLED_PORT  2
#define RST_PROLETRSM_STATE_ROOT_PORT      3
#define RST_PROLETRSM_STATE_DESG_PORT      4
#define RST_PROLETRSM_STATE_ALTERNATE_PORT 5
#define RST_PROLETRSM_STATE_BLOCK_PORT     6
/* Added state Constants for clarity - These states are not included in 
* calculation of MAX_STATES. The order and value can be changed */
#define RST_PROLETRSM_STATE_BACKUP_PORT   5
#define RST_PROLETRSM_STATE_REROOTED      6
#define RST_PROLETRSM_STATE_ROOT_PROPOSED 7
#define RST_PROLETRSM_STATE_ROOT_AGREED   8
#define RST_PROLETRSM_STATE_REROOT        9
#define RST_PROLETRSM_STATE_DESG_PROPOSE  10
#define RST_PROLETRSM_STATE_DESG_SYNCED   11
#define RST_PROLETRSM_STATE_DESG_RETIRED  12
#define RST_PROLETRSM_STATE_DESG_LISTEN   13
   /* Common for Designated and root ports */
#define RST_PROLETRSM_STATE_LEARN         14
#define RST_PROLETRSM_STATE_FORWARD       15
#define RST_PROLETRSM_STATE_ROOT_LEARN    RST_PROLETRSM_STATE_LEARN
#define RST_PROLETRSM_STATE_ROOT_FORWARD  RST_PROLETRSM_STATE_FORWARD
#define RST_PROLETRSM_STATE_DESG_LEARN    RST_PROLETRSM_STATE_LEARN
#define RST_PROLETRSM_STATE_DESG_FORWARD  RST_PROLETRSM_STATE_FORWARD

#define RST_PROLETRSM_MAX_EVENTS          14
#define RST_PROLETRSM_EV_BEGIN            0
#define RST_PROLETRSM_EV_FDWHILE_EXP      1 
#define RST_PROLETRSM_EV_RRWHILE_EXP      2
#define RST_PROLETRSM_EV_RBWHILE_EXP      3
#define RST_PROLETRSM_EV_SYNC_SET         4
#define RST_PROLETRSM_EV_REROOT_SET       5
#define RST_PROLETRSM_EV_SELECTED_SET     6
#define RST_PROLETRSM_EV_PROPOSED_SET     7
#define RST_PROLETRSM_EV_AGREED_SET       8
#define RST_PROLETRSM_EV_ALLSYNCED_SET    9
#define RST_PROLETRSM_EV_REROOTED_SET    10
#define RST_PROLETRSM_EV_UPDTINFO_RESET   RST_PROLETRSM_EV_SELECTED_SET
#define RST_PROLETRSM_EV_OPEREDGE_SET    11
#define RST_PROLETRSM_EV_DISPUTED_SET    12
#define RST_PROLETRSM_EV_LEARNING_FWDING_RESET 13


/******************************************************************************/
/*                     Port State Transition SEM Constants                  */
/******************************************************************************/

#define RST_PSTATETRSM_MAX_STATES            3
#define RST_PSTATETRSM_STATE_DISCARDING      0
#define RST_PSTATETRSM_STATE_LEARNING        1
#define RST_PSTATETRSM_STATE_FORWARDING      2

#define RST_PSTATETRSM_MAX_EVENTS            4
#define RST_PSTATETRSM_EV_BEGIN              0
#define RST_PSTATETRSM_EV_LEARN              1
#define RST_PSTATETRSM_EV_FORWARD            2
#define RST_PSTATETRSM_EV_LEARN_FWD_DISABLED 3


/******************************************************************************/
/*                     Port Protocol Migration SEM Constants                  */
/******************************************************************************/

#define RST_PMIGSM_MAX_STATES             3

#define RST_PMIGSM_STATE_CHECKING_RSTP    0
#define RST_PMIGSM_STATE_SELECTING_STP    1
#define RST_PMIGSM_STATE_SENSING          2

#define RST_PMIGSM_MAX_EVENTS             7

#define RST_PMIGSM_EV_BEGIN               0
#define RST_PMIGSM_EV_PORT_ENABLED        1
#define RST_PMIGSM_EV_MDELAYWHILE_EXP     2
#define RST_PMIGSM_EV_PORT_DISABLED       3
#define RST_PMIGSM_EV_MCHECK              4
#define RST_PMIGSM_EV_RCVD_STP            5
#define RST_PMIGSM_EV_RCVD_RSTP           6


/******************************************************************************/
/*                         Port Transmit SEM Constants                        */
/******************************************************************************/


#define RST_PTXSM_MAX_STATES                 6
#define RST_PTXSM_STATE_TRANSMIT_INIT        0
#define RST_PTXSM_STATE_TRANSMIT_PERIODIC    1
#define RST_PTXSM_STATE_TRANSMIT_CONFIG      2
#define RST_PTXSM_STATE_TRANSMIT_TCN         3
#define RST_PTXSM_STATE_TRANSMIT_RSTP        4
#define RST_PTXSM_STATE_IDLE                 5

#define RST_PTXSM_MAX_EVENTS                 8
#define RST_PTXSM_EV_BEGIN                   0
#define RST_PTXSM_EV_BEGIN_CLEARED           1
#define RST_PTXSM_EV_NEWINFO_SET             2
#define RST_PTXSM_EV_HELLOWHEN_EXP           3
#define RST_PTXSM_EV_HOLDTMR_EXP             4
#define RST_PTXSM_EV_SELECTED_SET            5
#define RST_PTXSM_EV_TX_ENABLED              6
#define RST_PTXSM_EV_TX_DISABLED             7


/******************************************************************************/
/*                  Port Receive Pseudo Info SEM Constants                    */
/******************************************************************************/
#define RST_PSEUDO_INFO_MAX_STATE            5
#define RST_PSEUDO_INFO_STATE_INIT           0
#define RST_PSEUDO_INFO_STATE_RX             1
#define RST_PSEUDO_INFO_STATE_BPDU_CHECK     2
#define RST_PSEUDO_INFO_STATE_HELLO_EXPIRY   3
#define RST_PSEUDO_INFO_STATE_BPDU_DISCARD   4


#define RST_PSEUDO_INFO_MAX_EVENTS           7
#define RST_PSEUDO_INFO_EV_BEGIN             0
#define RST_PSEUDO_INFO_EV_PORT_ENABLED      1
#define RST_PSEUDO_INFO_EV_PORT_DISABLED     2
#define RST_PSEUDO_INFO_EV_L2GP_ENABLED      3
#define RST_PSEUDO_INFO_EV_L2GP_DISABLED     4
#define RST_PSEUDO_INFO_EV_HELLO_EXPIRY      5
#define RST_PSEUDO_INFO_EV_RCVD_BPDU         6

/******************************************************************************/
/*                         Topology Change SEM Constants                      */
/******************************************************************************/


#define RST_TOPOCHSM_MAX_STATES              8
#define RST_TOPOCHSM_STATE_INACTIVE          0
#define RST_TOPOCHSM_STATE_LEARNING          1
#define RST_TOPOCHSM_STATE_DETECTED          2
#define RST_TOPOCHSM_STATE_ACTIVE            3
#define RST_TOPOCHSM_STATE_NOTIFIED_TCN      4
#define RST_TOPOCHSM_STATE_NOTIFIED_TC       5
#define RST_TOPOCHSM_STATE_PROPAGATING       6
#define RST_TOPOCHSM_STATE_ACKNOWLEDGED      7

#define RST_TOPOCHSM_MAX_EVENTS              10
#define RST_TOPOCHSM_EV_BEGIN                0
#define RST_TOPOCHSM_EV_LEARN_SET            1
#define RST_TOPOCHSM_EV_NOT_DESG_ROOT        2
#define RST_TOPOCHSM_EV_FORWARD              3
#define RST_TOPOCHSM_EV_OPEREDGE_RESET       4
#define RST_TOPOCHSM_EV_OPEREDGE_SET         5
#define RST_TOPOCHSM_EV_RCVDTC               6
#define RST_TOPOCHSM_EV_RCVDTCN              7
#define RST_TOPOCHSM_EV_RCVDTCACK            8
#define RST_TOPOCHSM_EV_TCPROP               9 

/******************************************************************************/
/*                   Bridge Detection SEM Constants                           */
/******************************************************************************/

#define RST_BRGDETSM_MAX_EVENTS              7 
#define RST_BRGDETSM_EV_BEGIN                0
#define RST_BRGDETSM_EV_ADMIN_SET            1
#define RST_BRGDETSM_EV_BPDU_RCVD            2
#define RST_BRGDETSM_EV_EDGEDELAYWHILE_EXP   3
#define RST_BRGDETSM_EV_SENDRSTP_SET         4
#define RST_BRGDETSM_EV_AUTOEDGE_SET         5
#define RST_BRGDETSM_EV_PORTDISABLED         6

#define RST_BRGDETSM_MAX_STATES              2
#define RST_BRGDETSM_STATE_EDGE              0
#define RST_BRGDETSM_STATE_NOT_EDGE          1

/******************************************************************************/
/*                   SEM Trace Message Constants                              */
/******************************************************************************/
#define AST_BDSM                            1
#define AST_RXSM                            2
#define AST_PISM                            3
#define AST_RSSM                            4
#define AST_RTSM                            5
#define AST_STSM                            6
#define AST_TCSM                            7
#define AST_TXSM                            8
#define AST_PMSM                            9
#define AST_PRSM                            10
#define AST_PSSM                            11

#define AST_MAX_SEM                         12 /* Number of SEMs in AST */

#define AST_FLUSH_INTERVAL_MIN_VAL          0
#define AST_FLUSH_INTERVAL_MAX_VAL          500
#define AST_MIN_FLUSH_IND                   0
#define AST_MAX_FLUSH_IND                   65535

#ifdef PVRST_WANTED
#define PVRST_MAX_STATES                     AST_MAX(PVRST_BRGDETSM_MAX_STATES,\
        AST_MAX(PVRST_TOPOCHSM_MAX_STATES,\
        AST_MAX(PVRST_PTXSM_MAX_STATES,\
        AST_MAX(PVRST_PROLETRSM_MAX_STATES    ,\
        AST_MAX(PVRST_PSTATETRSM_MAX_STATES    ,\
        AST_MAX(PVRST_PROLESELSM_MAX_STATES   ,\
        AST_MAX(PVRST_PINFOSM_MAX_STATES   ,\
        PVRST_PMIGSM_MAX_STATES)))))))
#else
#define PVRST_MAX_STATES                     0
#endif

#ifdef PVRST_WANTED
#define PVRST_MAX_EVENTS                     AST_MAX(PVRST_BRGDETSM_MAX_EVENTS,\
        AST_MAX(PVRST_TOPOCHSM_MAX_EVENTS,\
        AST_MAX(PVRST_PTXSM_MAX_EVENTS,\
        AST_MAX(PVRST_PMIGSM_MAX_EVENTS    ,\
        AST_MAX(PVRST_PSTATETRSM_MAX_EVENTS    ,\
        AST_MAX(PVRST_PROLETRSM_MAX_EVENTS  ,\
        AST_MAX(PVRST_PROLESELSM_MAX_EVENTS  ,\
        PVRST_PINFOSM_MAX_EVENTS)))))))
#else
#define PVRST_MAX_EVENTS         0
#endif



#ifdef MSTP_WANTED
#define MST_MAX_STATES                     AST_MAX(MST_PINFOSM_MAX_STATES,\
        AST_MAX(MST_PRCVSM_MAX_STATES,\
        AST_MAX(MST_PROLETRSM_MAX_STATES,\
        MST_TOPOCHSM_MAX_STATES)))

#define MST_MAX_EVENTS                     AST_MAX(MST_PINFOSM_MAX_EVENTS,\
        AST_MAX(MST_PRCVSM_MAX_EVENTS,\
        AST_MAX(MST_PROLETRSM_MAX_EVENTS,\
        MST_TOPOCHSM_MAX_EVENTS)))
#else /* MSTP_WANTED */
#define MST_MAX_STATES                     0
#define MST_MAX_EVENTS                     0
#endif /* MSTP_WANTED */

#define AST_MAX_EVENTS                     AST_MAX(RST_PINFOSM_MAX_EVENTS,\
        AST_MAX(RST_PROLESELSM_MAX_EVENTS,\
        AST_MAX(RST_PROLETRSM_MAX_EVENTS,\
        AST_MAX(RST_PSTATETRSM_MAX_EVENTS,\
        AST_MAX(RST_PMIGSM_MAX_EVENTS,\
        AST_MAX(RST_PTXSM_MAX_EVENTS,\
        AST_MAX(RST_TOPOCHSM_MAX_EVENTS,\
        AST_MAX(RST_BRGDETSM_MAX_EVENTS,\
                  AST_MAX(RST_PRCVSM_MAX_EVENTS, \
        AST_MAX(PVRST_MAX_EVENTS, \
        MST_MAX_EVENTS))))))))))



#define AST_MAX_STATES                     AST_MAX(RST_PINFOSM_MAX_STATES,\
        AST_MAX(RST_PROLESELSM_MAX_STATES,\
        AST_MAX(RST_PROLETRSM_MAX_STATES,\
        AST_MAX(RST_PSTATETRSM_MAX_STATES,\
        AST_MAX(RST_PMIGSM_MAX_STATES,\
        AST_MAX(RST_PTXSM_MAX_STATES,\
        AST_MAX(RST_TOPOCHSM_MAX_STATES,\
        AST_MAX(RST_BRGDETSM_MAX_STATES,\
                  AST_MAX(RST_PRCVSM_MAX_STATES, \
                  AST_MAX(PVRST_MAX_STATES, \
        MST_MAX_STATES))))))))))

#define AST_MAX_SEM_LEN                   20 /* Maximum length of State/Event 
       name */
#define AST_MAX_TC_MEM_LEN                   10 /* Maximum length of State/Event */
        
/* New CLI Constants */
#define AST_BRG_ADDR_DIS_LEN              21
#define AST_BRG_ID_DIS_LEN                24

#define AST_CLI_INVALID_CONTEXT              0xFFFFFFFF
#define AST_SWITCH_ALIAS_LEN                 L2IWF_CONTEXT_ALIAS_LEN

/*********************PROVIDER BRIDGE RELATED CONSTANTS*******************/

/* IMPORTANT:
 * ---------
 * Please note that the following constants are used in common between PB and 
 * PBB Spanning Tree operation. Hence, if something is to be changed, please 
 * take a note of it.
 * 1. AST_PB_CVLAN_BRG_PRIORITY
 * 2. AST_PB_PEP_PORT_PATH_COST
 * 3. AST_PB_CVLAN_PORT_PRIORITY
 * */
/* Number of C-VLAN blocks to be present in the mempool. This does not define
 * the max number of C-VLANs supported in the system. */
#define AST_MAX_CVLAN_COMP               AST_MAX_CEP_IN_PEB
#define AST_PEP_BLOCK_SIZE               AST_MAX_SERVICES_PER_CUSTOMER

/* This macro is added for pseudo wire visibility to STP.
 * Where this is specific to STP module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_PSW_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_PSW_IFACES.
 */
#define   STP_MAX_L2_PW_IFACES           SYS_DEF_MAX_L2_PSW_IFACES

/* This macro is added for Attachment Circuit interface to STP.
 * Where this is specific to STP module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_AC_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_AC_IFACES.
 */
#define STP_MAX_L2_AC_IFACES             SYS_DEF_MAX_L2_AC_IFACES

#define AST_MAX_PORTS_PER_CONTEXT        (SYS_DEF_MAX_PORTS_PER_CONTEXT +\
                                          STP_MAX_L2_PW_IFACES + \
                                          STP_MAX_L2_AC_IFACES)


/******************************************************************************/
/*                CEP Spanning Tree Operation specific Constants              */
/******************************************************************************/
#define AST_PB_CVLAN_BRG_PRIORITY           0xffff
#define AST_PB_CEP_CVLAN_LOCAL_PORT_NUM     1
#define AST_PB_PEP_PORT_PATH_COST           128
#define AST_PB_CVLAN_PORT_PRIORITY          32
#define AST_PB_PORT_TBL_MEM_BLK_COUNT       1


/******************************************************************************/
/*                Async Npapi  specific Constants                             */
/******************************************************************************/
#define AST_MAX_RETRY_COUNT       10
#define AST_BLOCK_SUCCESS         0
#define AST_BLOCK_FAILURE         1
#define AST_LEARN_SUCCESS         2
#define AST_LEARN_FAILURE         3
#define AST_FORWARD_SUCCESS       4
#define AST_FORWARD_FAILURE       5
#define AST_WAITING_FOR_CALLBACK  6

/*******************************************************************************/
/*                 SISP related Constants                                      */
/*******************************************************************************/
#define MST_ADMIN_P2P            1
#define MST_ADMIN_EDGE           2
#define MST_AUTO_EDGE            3
/*********** BPDU Guard Related Constants ********************/
#define AST_BPDUGUARD_NONE       0
#define AST_BPDUGUARD_ENABLE     1
#define AST_BPDUGUARD_DISABLE    2

/*********** Performance Related Constants ********************/
#define AST_PERFORMANCE_STATUS_ENABLE     1
#define AST_PERFORMANCE_STATUS_DISABLE    2
#endif /* _ASTCONST_H_ */

/* END OF FILE */
