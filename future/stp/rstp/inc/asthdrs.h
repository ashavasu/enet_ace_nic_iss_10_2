/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: asthdrs.h,v 1.41 2016/09/13 12:56:29 siva Exp $
 *
 * Description: This file contains all the header files to be
 *              included and used by RSTP, MSTP and PVRST Modules.
 *
 *******************************************************************/


#ifndef _ASTHDRS_H_
#define _ASTHDRS_H_

#ifdef PBB_PERFORMANCE_WANTED
#include  <sys/time.h>
#endif
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "msr.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "fsmprswr.h"
#include "fsmsrswr.h"
#include "fsmsbrwr.h"
#include "std1w1wr.h"
#include "fsmpbrwr.h"
#include "stdbriwr.h"
#ifdef ICCH_WANTED
#include "icch.h"
#endif /* ICCH_WANTED */
#ifdef MSTP_WANTED
#include "fsmpmswr.h"
#include "astmhwwr.h"
#endif
#include "fsrstwr.h"
#include "fspbrswr.h"
#ifdef MSTP_WANTED
#include "fsmstwr.h"
#include "std1s1wr.h"
#endif
#ifdef PVRST_WANTED
#include "fspvrswr.h"
#include "fsmppvwr.h"
#endif
#include "la.h"
#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "igs.h"
#include "pnac.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "nputil.h"
#ifndef NP_BACKWD_COMPATIBILITY
#include "rstminp.h"
#include "mstminp.h"
#include "rstmpbnp.h"
#include "pvrstminp.h"
#else
#include "rstnp.h"
#include "rstpbnp.h"
#include "mstnp.h"
#include "rsminpwr.h"
#include "msminpwr.h"
#include "pvsminpwr.h"
#endif /* NP_BACKWD_COMPATIBILITY */
#ifdef PBB_WANTED
#include "pbbnp.h"
#include "rstmpbbnp.h"
#endif
#endif 

#include "issu.h"

#include "rstp.h"
#include "mstp.h"
#include "l2iwf.h"
#include "pvrst.h"
#include "vcm.h"
#include "iss.h"
#include "garp.h"
#include "mrp.h"
#include "cli.h"

#include "astconst.h"
#include "astmacr.h"
#include "astctdfs.h"

#ifdef MSTP_WANTED
#include "astmcons.h"
#include "astmmacr.h"
#include "astmtdfs.h"
#endif
#ifdef PVRST_WANTED
#include "astvcons.h"
#include "astvmacr.h"
#endif
#ifdef L2RED_WANTED
#include "rmgr.h"
#endif
#include "asttdfs.h"
#include "asttrc.h"
#include "astdbg.h"
#include "astprot.h"

#ifdef MSTP_WANTED
#include "astmprot.h"
#include "astmtrap.h"
#endif
#ifdef PVRST_WANTED
#include "astvprot.h"
#include "astvtrap.h"
#endif
#ifdef L2RED_WANTED
#ifndef AST_RED_H
 #include "astpred.h"
#endif
#else
#include "astprdstb.h"
#endif

#include "astpbprot.h"
#include "astpbred.h"
#ifdef PB_WANTED
#include "fspbrslw.h"
#include "fsmpbrlw.h"
#endif /*PB_WANTED*/

#include "astpbbprot.h"

#include "astextn.h"
#include "stdrslow.h"
#include "fsrstlow.h"
#include "astrtrap.h"
#include "fsbuddy.h"
#include "fssyslog.h"

#include "stpcli.h"
#include "astglob.h"
#include "fssnmp.h"
#include "astpsz.h"
#endif /* _ASTHDRS_H_ */


