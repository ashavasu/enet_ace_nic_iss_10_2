#ifndef _MINPSTB_H
#define _MINPSTB_H
INT1 FsMiStpNpHwInit (UINT4 u4ContextId);

VOID FsMiRstpNpInitHw (UINT4 u4ContextId);

INT1 FsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, 
                             UINT1 u1Status);

INT1 FsMiRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, 
                            UINT1 *pu1Status);

VOID FsMiNpDeleteAllFdbEntries (UINT4 u4ContextId);

INT1
FsMiPbRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVid,
                                                    UINT1 u1Status);
INT1
FsMiPbRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                         UINT1 *pu1Status);
#ifdef MBSM_WANTED
INT1 FsMiRstpMbsmNpInitHw (UINT4 , tMbsmSlotInfo * );
#endif

/* PBB Related prototypes */
INT4 FsMiPbbRstHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                              UINT4 u4VipIndex, UINT4 u4Isid,
                                              UINT1 u1PointToPointStatus);

INT4 FsMiBrgHwCreateControlPktFilter (UINT4 u4ContextId,
                                      tFilterEntry FilterEntry);
#endif
