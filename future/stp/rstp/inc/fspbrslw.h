/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbrslw.h,v 1.5 2008/08/20 15:27:43 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbProviderStpStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbProviderStpStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbProviderStpStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbProviderStpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbRstCVlanBridgeTable. */
INT1
nmhValidateIndexInstanceFsPbRstCVlanBridgeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbRstCVlanBridgeTable  */

INT1
nmhGetFirstIndexFsPbRstCVlanBridgeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbRstCVlanBridgeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbRstCVlanBridgeId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbRstCVlanBridgeDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbRstCVlanBridgeRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanBridgeMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanBridgeHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanBridgeHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanBridgeForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanBridgeTxHoldCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanStpHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanStpMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanStpForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanStpTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanStpTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsPbRstCVlanPortInfoTable. */
INT1
nmhValidateIndexInstanceFsPbRstCVlanPortInfoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbRstCVlanPortInfoTable  */

INT1
nmhGetFirstIndexFsPbRstCVlanPortInfoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbRstCVlanPortInfoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbRstCVlanPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortAdminEdgePort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortOperEdgePort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortAdminPointToPoint ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortOperPointToPoint ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortAutoEdge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbRstCVlanPortDesignatedCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbRstCVlanPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbRstCVlanPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsPbRstCVlanPortSmTable. */
INT1
nmhValidateIndexInstanceFsPbRstCVlanPortSmTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbRstCVlanPortSmTable  */

INT1
nmhGetFirstIndexFsPbRstCVlanPortSmTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbRstCVlanPortSmTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbRstCVlanPortInfoSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortMigSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortRoleTransSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortStateTransSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortTopoChSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbRstCVlanPortTxSmState ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPbRstCVlanPortStatsTable. */
INT1
nmhValidateIndexInstanceFsPbRstCVlanPortStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbRstCVlanPortStatsTable  */

INT1
nmhGetFirstIndexFsPbRstCVlanPortStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbRstCVlanPortStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbRstCVlanPortRxRstBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortRxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortRxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortTxRstBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortTxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortTxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortInvalidRstBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortInvalidConfigBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortInvalidTcnBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortProtocolMigrationCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbRstCVlanPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));
