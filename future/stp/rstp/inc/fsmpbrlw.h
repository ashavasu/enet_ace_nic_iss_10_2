/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbrlw.h,v 1.5 2012/02/29 09:37:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIPbRstContextInfoTable. */
INT1
nmhValidateIndexInstanceFsMIPbRstContextInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbRstContextInfoTable  */

INT1
nmhGetFirstIndexFsMIPbRstContextInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbRstContextInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbProviderStpStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbProviderStpStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbProviderStpStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbRstContextInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbRstCVlanBridgeTable. */
INT1
nmhValidateIndexInstanceFsMIPbRstCVlanBridgeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbRstCVlanBridgeTable  */

INT1
nmhGetFirstIndexFsMIPbRstCVlanBridgeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbRstCVlanBridgeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbRstCVlanBridgeId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPbRstCVlanBridgeDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPbRstCVlanBridgeRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanBridgeMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanBridgeHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanBridgeHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanBridgeForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanBridgeTxHoldCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanStpHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanStpMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanStpForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanStpTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanStpTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanStpDebugOption ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbRstCVlanStpDebugOption ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbRstCVlanStpDebugOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbRstCVlanBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbRstCVlanPortInfoTable. */
INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortInfoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbRstCVlanPortInfoTable  */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortInfoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbRstCVlanPortInfoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbRstCVlanPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortAdminEdgePort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortOperEdgePort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortAdminPointToPoint ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortOperPointToPoint ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortAutoEdge ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPbRstCVlanPortDesignatedCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPbRstCVlanPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIPbRstCVlanPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIPbRstCVlanPortSmTable. */
INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortSmTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbRstCVlanPortSmTable  */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortSmTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbRstCVlanPortSmTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbRstCVlanPortInfoSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortMigSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortRoleTransSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortStateTransSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortTopoChSmState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbRstCVlanPortTxSmState ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIPbRstCVlanPortStatsTable. */
INT1
nmhValidateIndexInstanceFsMIPbRstCVlanPortStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbRstCVlanPortStatsTable  */

INT1
nmhGetFirstIndexFsMIPbRstCVlanPortStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbRstCVlanPortStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbRstCVlanPortRxRstBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortRxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortRxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortTxRstBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortTxConfigBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortTxTcnBpduCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortInvalidRstBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortInvalidConfigBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortInvalidTcnBpduRxCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortProtocolMigrationCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIPbRstCVlanPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));
