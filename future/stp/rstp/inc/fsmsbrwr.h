
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrwr.h,v 1.5 2011/08/24 06:13:51 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/


#ifndef _FSMSBRWR_H
#define _FSMSBRWR_H
INT4 GetNextIndexFsDot1dBaseTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMSBR(VOID);

VOID UnRegisterFSMSBR(VOID);
INT4 FsDot1dBaseBridgeAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBaseNumPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBaseTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dBasePortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dBasePortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBasePortIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBasePortCircuitGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBasePortDelayExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dBasePortMtuExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dStpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dStpProtocolSpecificationGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpRootPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeMaxAgeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeForwardDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeMaxAgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpBridgeForwardDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsDot1dStpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dStpPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCost32Get(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCost32Set(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortPathCost32Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStpPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsDot1dTpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTpLearnedEntryDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpAgingTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpAgingTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpAgingTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1dTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTpFdbAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dTpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTpPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortMaxInfoGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dStaticTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dStaticAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticReceivePortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1dStaticAllowedToGoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dStaticAllowedIsMemberGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticAllowedIsMemberSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticAllowedIsMemberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dStaticAllowedToGoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





#endif /* _FSMSBRWR_H */
