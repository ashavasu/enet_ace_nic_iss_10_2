/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: asttdfs.h,v 1.107 2017/12/22 10:06:02 siva Exp $
 *
 * Description: This file contains all the typedefs used by the RSTP 
 *              and MSTP modules.
 *
 *******************************************************************/


#ifndef _ASTTDFS_H_
#define _ASTTDFS_H_

/******************************************************************************/
/*                            General Typedefs                                */
/******************************************************************************/


typedef struct AstPriorityVector {
 tAstBridgeId RootId;
 tAstBridgeId DesgBrgId;
 UINT4      u4RootPathCost;
 UINT2      u2DesgPortId;
 UINT2      u2BrgPortId;
}tAstPriorityVector;



/********************************************************************************/
/*                                                                              */
/*                  PROVIDER BRIDGE RELATED DATA STRUCTURE                      */
/*                                                                              */
/********************************************************************************/
typedef struct AstPbMemPoolIds {

   tAstMemPoolId            CvlanContextMemPoolId; /* Id of memory pool to 
                                                    * be used for allocating 
                                                    * C-VLAN context (component)
                                                    * information. */

   /* Only RSTP can run on C-VLAN component. Hence the PerStInfoTable in C-VLAN
    * component will contain only one entry. But for S-VLAN context PerStInfo 
    * Table can have 64 MSTIs + 1 CIST. So we have to use seperate mempool for
    * C-VLAN component PerStInfo table. */
   tAstMemPoolId            CvlanPerStInfoMemPoolId; 

   tAstMemPoolId            CvlanInfoMemPoolId;    /* Id of memory pool to be 
                                                    * used for allocating memory
                                                    * for PbCvlanInfo (C-VLAN 
                                                    * context specific 
                                                    * information) in a virtual
                                                    * context. */

   tAstMemPoolId            PbPortInfoMemPoolId;   /* Id of memory pool to be 
                                                    * used for allocating memory
                                                    * for PbPortInfo (C-VLAN 
                                                    * Port specific information)
                                                    * in tAstPortEntry.*/
    tAstMemPoolId          CvlanPort2IndexTblMemPoolId; /*Id of the Memory pool
                                                         * to be used for 
                                                         * allocating memory 
                                                         * for the node of
                                                         * RBTree HL Port 
                                                         * to protocol
                                                         * port mapping in
                                                         * tAstPbCVlanInfo*/
    

}tAstPbMemPoolIds;

/*****************************************************************************/

/******************************************************************************/
/*                            Flush Related flags                             */
/******************************************************************************/

typedef struct AstFlushFlag {
   UINT1                      u1PendingFlushes; /* Pending flush triggers */
   UINT1                      au1Reserved[3];
}tAstFlushFlag; 

/*****************************************************************************/

typedef struct AstContextInfo tAstContextInfo;

/******************************************************************************/
/*                            Application Timer Node                          */
/******************************************************************************/

typedef struct AstTimer {

   tAstAppTimer AstAppTimer;    /* The FSAP2 Application Timer
                                 * node */
   VOID   *pEntry;        /* A void pointer in which the
                           * corresponding port entry
                           * pointer for which the timer
                           * was started can be stored */
   
   tAstContextInfo *pAstContext; /* Virtual context 
                                    for which this timer was started */
   
   UINT2  u2InstanceId;   /* The instance Id for which the
                           * timer is started */
   UINT1  u1TimerType;    /* The type of the timer that
                           * this timer node represents */
   UINT1  u1IsTmrStarted; /* To notify the timer is started or not */

}tAstTimer;



/******************************************************************************/
/*                    Port Information Common to RSTP & CIST                  */
/******************************************************************************/

typedef struct AstCommPortInfo {

   tAstTimer                  *pMdWhileTmr;     /* Migration delay timer */
   tAstTimer                  *pHelloWhenTmr;   /* Hello When Timer */
   tAstTimer                  *pHoldTmr;       /* This timer is used to limit
                                                * the number of Bpdus to be
                                                * transmitted */
   tAstTimer                  *pEdgeDelayWhileTmr; /* This timer indicates the
                                                      time elapsed without a 
                                                      bpdu being recieved*/ 
   tAstTimer                  *pRapidAgeDurtnTmr; /* This timer indicates the
                                                     duration for which 
                                                     rapid aging should be 
                                                     applied on this port */
   tAstTimer                  *pPseudoInfoHelloWhenTmr; /*Used to ensure that
                                                         at least one Pseudo
                                                         Info BPDU is presented
                                                         to the spanning tree
                                                         by a L2GP Port*/
   tAstTimer                  *pDisableRecoveryTmr;     /* Recovery timer */
   tAstTimer                  *pIncRecoveryTmr;     /* Port type or PVID inconsistancy recovery timer */
   tAstBoolean                bNewInfo;        /* Set to TRUE if a Bpdu is to
                                                * be transmitted */
   tAstBoolean                bRcvdBpdu;       /* Set to TRUE when a Config,
                                                * TCN or RST Bpdu is 
                                                * received */
   tAstBoolean                bInitPm;          /* Flag used by Migration SEM
                                                 * to prevent repeated re-entry
                                                 * into the INIT state */
   tAstBoolean                bMstiMasterPort;  /* Flag used to identify 
                                                   whether this port is 
                                                   Master port for any of
                                                   the Instances running */
   tAstBoolean                bMCheck;          /* Set by management to force
                                                 * the Migration SEM to send
                                                 * RST Bpdus */
   tAstBoolean                bSendRstp;        /* Set to TRUE if RST Bpdus
                                                 * are to be transmitted */
   tAstBoolean                bRcvdRstp;        /* Set to TRUE if an RST Bpdu
                                                 * is received on this port */
   tAstBoolean                bRcvdStp;         /* Set to TRUE if a Config or
                                                 * TCN Bpdu is received on 
                                                 * this port */
   tAstBoolean                bRcvdTcn;         /* Set to TRUE when a TCN Bpdu
                                                 * is received */
   tAstBoolean                bRcvdTcAck;       /* Set to TRUE when a Config
                                                 * Bpdu with a Topology change
                                                 * acknowledgement flag is 
                                                 * received */
   tAstBoolean                bTcAck;           /* Set to TRUE if a Config msg
                                                 * with Topology Change 
                                                 * acknowledgement flag set is
                                                 * to be transmitted */
   UINT4                      u4BpduGuard;      /* Variable used to check whether 
                                                 * port BpduGuard is enabled/
                                                 * disabled*/
    tAstBoolean              bBpduInconsistent;  /* Variable added to set the  bpduguard-inconsistent
                                                  *  becasue of the root guard feature.*/
#ifdef PVRST_WANTED
    tAstBoolean              bPortPvrstStatus;   /* Set by the management entity
                                                  * to indicate whether Pvrst is
                                                  * enabled/Disabled*/
    tAstBoolean              bRootGuard;         /* Variable used to check whether
                                                  * port is RootGuard 
                                                  * enabled/disabled*/ 
    tEncapsulationType       eEncapType;         /* Set by the management entity to
                                                  * indicate which encapsulation is 
                                                  * to be used on this port*/
    tAstBoolean              bRootInconsistent;  /*  Variable added to set the  root-inconsistent
                                                  *  state when the port state transitions to Discarding
                                                    state when the port state transitions to Discarding */

#endif /* PVRST_WANTED*/
#ifdef MSTP_WANTED
   tAstBoolean                bCistPortMacEnabled; /*Set to TRUE if Port MAC is administratively 
              * configured as Point-To-Point*/
   tAstBoolean                bCistPortMacOperational; /*Set to TRUE if Port MAC is operationally 
              *  Point-To-Point*/
#endif

   UINT1                      u1BrgDetSmState;  /* The present state of the 
                                                 * Bridge Detection SEM */
   UINT1                      u1PortTxSmState;  /* The present state of the 
                                                 * Port Transmit SEM */
   UINT1                      u1PmigSmState;    /* The present state of the Port
                                                 * Protocol Migration SEM */
   UINT1                      u1PortRcvSmState; /* The present state of the Port
                                                 * Receive SEM */
   UINT1                      u1PortPseudoSmState; /*The present state of the 
                                                     Pseudo Info of SEM
                                                     */
                                                  
   UINT1                      u1TxCount;        /* Counter used by the Port
                                                 * Transmit SEM in order to 
                                                 * limit the max Bpdu 
                                                 * transmission */
   UINT1                u1BpduGuardAction; /* Variable used to set BPDU
                                                  * Guard action */   
   UINT1                      au1Reserved[1];
}tAstCommPortInfo;


/********************************************************************************/
/*                                                                              */
/*                  PROVIDER BRIDGE RELATED DATA STRUCTURE                      */
/*                                                                              */
/********************************************************************************/
typedef struct AstPbPortInfo {
    tAstContextInfo    *pPortCVlanContext;

    UINT4               u4CepIfIndex;      /* IfIndex of the CEP associated 
                                             with this C-VLAN component. */
    tVlanId             Svid;              /* For CEP, this value will be 0,
                                            * for PEP, proper Svid value. */
    UINT1               au1Reserved[2];
}tAstPbPortInfo;

/*******************************************************************************/

      
/******************************************************************************/
/*                      Common Port Information Structure                     */
/******************************************************************************/
/* 802.1ad support:
 * For C-VLAN component ports, the following fields will not be relevant:
 *      - AstContextIfIndexNode
 *      - AstGlobalIfIndexNode
 *      - u4ContextId
 *      - u4IfIndex
 * C-VLAN ports doesn't come under the perview of administrator. These ports are
 * internally created and hence will not have u4IfIndex. So it is not possbile
 * to put these ports in the global IfIndex table.
 * C-VLAN context is created internally and will not be known to
 * VCM/L2Iwf/VLANGARP. So u4ContextId will not be valid for C-VLAN context.
 * C-VLAN contexts are identified by Customer Edge Port Id in the given virtual
 * context.
 */


typedef struct AstPortEntry
{
    tRBNodeEmbd    AstContextIfIndexNode;  
    tRBNodeEmbd    AstGlobalIfIndexNode;
    UINT4          u4ContextId; /* Virtual Context ID */
    UINT4          u4IfIndex;  /* Global IfIndex valid across virtual 
                                  contexts */
   tAstCommPortInfo  CommPortInfo; /* State machine port 
                                    * information common across
                                    * all instances */
#ifdef MSTP_WANTED
   tCistMstiPortInfo CistMstiPortInfo; /* Contains CIST and MSTI Port
                                        * information common across
                                        * all instances */
#endif /* MSTP_WANTED */

   tAstPbPortInfo  *pPbPortInfo;     /* Pointer to Provider Bridge port related
                                      * information.  
                                      * This will be non-NULL for CEP/PEP
                                      * ports.
                                      */

   tAstBoolean      bRestrictedRole; /* If set, it causes this port not to be
                                      * selected as RootPort for CIST and for
                                      * any MSTI. */

   tAstBoolean      bRootGuard;      /* If set, it causes this port not to be
                                      * selected as RootPort for CIST and for
                                      * any MSTI. */
   tAstBoolean      bRootInconsistent;  /*  Variable added to set the  root-inconsistent
                                         *  state when the port state transitions to Discarding
                                         *  becasue of the root guard feature.*/


   tAstBoolean      bRestrictedTcn;  /* If set causes the Port not to propagate 
                                      * received topology change notifications 
                                      * and topology changes to other Ports.*/


   tAstBoolean  bOperEdgePort;    /* Indicates the operational
                                   * value of the Edge-Port 
                                   * parameter */
   tAstBoolean  bAdminEdgePort;   /* Set by management to force
                                   * the edge status of the
                                   * port */

   tAstBoolean  bAutoEdge;        /* Indicates the Auto Edge status
                                   * set by management*/    
   tAstBoolean  bOperPointToPoint; /* Indicates the point-to-point
                                    * status of the port */
   tAstBoolean  bBpduRcvd;
   tAstBoolean  bEnableBPDURx;    /*when set as FALSE, BPDU received
                                    on the port are ignored.*/
   
   tAstBoolean  bEnableBPDUTx;    /*when set as FALSE, no BPDUs are
                                    transmitted by the port.*/

   tAstBoolean  bIsL2Gp;          /*when set as TRUE, it should stop normal
                                    spanning tree interaction with port
                                    connected to it over LAN.
                                   When set to FALSE, the port should withdraw
                                   from L2GP and participate in normal
                                   spanning tree with the port attached to it*/
   
   tAstTimes    PortTimes;       /* Timer parameter values used
                                  * in Bpdus transmitted from 
                                  * this Port */
   tAstTimes    DesgTimes;       /* Timer parameter values used
                                  * to update the PortTimes.
                                  * This value is copied from 
                                  * RootTimes */
#ifdef MSTP_WANTED
   tAstMstPortInstList InstList; /* This contains the bit map for the
        instances for which this port is 
        member of. */
#endif

   UINT4        u4PathCost;       /* Cost that is incurred when 
                                   * this port is included */
                                  /* This will be actively used only
                                   * when RSTP is run.
                                   * For MSTP/PVRST u4PortPathCost
                                   * in tAstPerStPortInfo will be
                                   * used in the state machines
                                   * This is only used for saving the
                                   * calculated pathcost for this port.
                                   */
   UINT4        u4NumRstBpdusRxd;     
   UINT4        u4NumConfigBpdusRxd;     
   UINT4        u4NumTcnBpdusRxd;     
   UINT4        u4NumRstBpdusTxd;
   UINT4        u4NumConfigBpdusTxd;
   UINT4        u4NumTcnBpdusTxd;
   UINT4        u4InvalidRstBpdusRxdCount;
   UINT4        u4InvalidConfigBpdusRxdCount;
   UINT4        u4InvalidTcnBpdusRxdCount;
   UINT4        u4ProtocolMigrationCount; /* Number of times Protocol
          Migration has happened on
          this Port */
   UINT4        u4PhyPortIndex; /* Physical port index over which this port
       runs, if this port is a logical port */
  
   UINT4        u4RcvdEventTimeStamp; /* Time Stamp at which 
                                       * the event specified by i4RcvdEvent
                                       * was received recently in this port */
   INT4        i4RcvdEvent; /* Type of event rcvd recently in this port */
   INT4        i4RcvdEventSubType; /* Sub Type of event rcvd recently in this port */
   UINT4       u4PortStateChangeTimeStamp; /* Time Stamp at which Port state change
                                                  indication was received recently
                                                  in this port */
   INT4        i4PortRowStatus;  /* This object is used to create or delete 
                                    interfaces at RSTP module level.Ports can be 
                                    created at RSTP module level only for ports 
                                    that have been created in Interface manager 
                                    and mapped to a context.This is applicable 
                                    only when Automatic Port Create Feature is 
                                    Disabled.*/
   
   UINT2        u2PortNo;  /* 12-bit logical port number
                            * that is local to the virtual context to which
                            * the port belongs */
   
   UINT2        u2HelloTime; /* Timer parameter values used
                              * in Bpdus transmitted from 
                              * this Port */

   UINT2        u2PktErrValue;   /* fsRstPktErrVal */

   UINT2        u2ProtocolPort;  /* This port number will be used for
                                  * protocol calculations (port Id 
                                  * calculations and for comparisons). 
                                  * For CEP, this will be 0xfff and for 
                                  * PEPs the corresponding Svid. For other 
                                  * ports u2ProtocolPort will be equal to 
                                  * u2PortNo.*/

#if (defined RSTP_DEBUG) || (defined TRACE_WANTED)
   UINT1        au1StrIfIndex [AST_IFINDEX_STR_LEN]; /* Global IfIndex
              in string form */
#endif

   UINT1        u1PortType;    /* CEP/CNP/CEP/PEP/PNP/PPNP/PCNP/PCEP */

   UINT1        u1AdminPointToPoint; /* Set by management to 
                                      * force the Point to
                                      * Point status */
   UINT1        u1EntryStatus;    /* Status of this port 
                                   * INVALID / OPER_DOWN /
                                   * OPER_UP */
   UINT1        u1MigrationType; /*fsRstPortMigrationType  */
   tAstBoolean  bLoopGuard;    /* Set as True causes a
      port to retain its state and role */
   UINT1        u1PktErrType;    /*fsRstPktErrType */
   BOOL1        bPathCostInitialized;
   UINT1        au1DestMACAddr[AST_MAC_ADDR_LEN]; /* Will have the value 
                                                     depending upon the 
                                                     port type */
   UINT1        au1PortMACAddr[AST_MAC_ADDR_LEN]; /* Will have the port
                                                     value */

   UINT2        u2LastInst;  /*Last active instance in the system */
   BOOL1        bIsSisp; /* Reflects the SISP enabled/disabled status
       on this port */

   UINT1   u1RecScenario;
   BOOL1    bTransmitBpdu;
   UINT1  u1EnableBPDUFilter; /*If set, no BPDUs are transmitted and received 
                                    from the port */
   tAstBoolean  bPortClearStats;/*To clear all interface associated statistical
                                  counters  RSTP/MSTP clear statistics*/
   tAstBoolean bAllTransmitReady;
   tAstBoolean bIEEE8021apAdminP2P; /* Variable added for checking whether the
            default value of Admin PointToPoint has been 
            updated either through ieee8021BridgeBasePortAdminPointToPoint
            or dot1dStpPortAdminPointToPoint */
   tAstBoolean bLoopInconsistent; /*  Variable added to set the  loop-inconsistent 
           state when the port state transitions to Discarding  
           when loop guard feature is enabled */

  tAstBoolean bPVIDInconsistent; /*  Variable added to set the  pvid-inconsistent */

  tAstBoolean bPTypeInconsistent; /*  Variable added to set the  ptype-inconsistent*/
  tAstBoolean bPTypeIncExists; /*  Variable added to set the  ptype-inconsistent*/
  tAstBoolean bPVIDIncExists; /*  Variable added to set the  ptype-inconsistent*/
   tAstBoolean     bDot1wEnabled;
   UINT4        u4ErrorRecovery; /* Timer Parameter for
                                  * automatic recovery from
                                  * error disabled state*/
    UINT4 u4NumRstTCDetectedCount;
    UINT4 u4NumRstTCRxdCount;
    UINT4 u4NumRstRxdInfoWhileExpCount;
    UINT4 u4NumRstProposalPktsSent;
    UINT4 u4NumRstProposalPktsRcvd;
    UINT4 u4NumRstAgreementPktSent;
    UINT4 u4NumRstAgreementPktRcvd;
    UINT4 u4NumRstImpossibleStateOcc;
    UINT4 u4TCDetectedTimeStamp;
    UINT4 u4TCReceivedTimeStamp;
    UINT4 u4RcvInfoWhileExpTimeStamp;
    UINT4 u4ProposalPktSentTimeStamp;
    UINT4 u4ProposalPktRcvdTimeStamp;
    UINT4 u4AgreementPktSentTimeStamp;
    UINT4 u4AgreementPktRcvdTimeStamp;
    UINT4 u4ImpStateOccurTimeStamp;
    INT4  i4OldPortState;


}tAstPortEntry;

typedef struct AstTxPktInfo
{
    tAstBufChainHeader *pBpdu;
    UINT4   u4IfIndex;
    UINT4   u4PktSize;
}tAstTxPktInfo;



#ifdef PVRST_WANTED

typedef struct PerStPvrstRstPortInfo {
    tAstTimer   *pHelloWhenPvrstTmr;
    tAstTimer   *pMdWhilePvrstTmr;
    tAstTimer   *pHoldTmr;
    tAstTimer   *pRootIncRecoveryTmr;
    tAstTimes           DesgPvrstTimes;
    tAstTimes           PortPvrstTimes;
    tAstBoolean  bNewInfo;
    tAstBoolean  bRcvdBpdu;
    tAstBoolean  bInitPm;
    tAstBoolean  bMCheck;
    tAstBoolean  bSendRstp;
    tAstBoolean  bRcvdRstp;
    tAstBoolean  bRcvdStp;
    tAstBoolean  bRcvdTcn;
    tAstBoolean  bRcvdTcAck;
    tAstBoolean  bTcAck;    
    UINT4   u4PathCost;
    UINT4   u4NumRstBpdusRxd;
    UINT4   u4NumConfigBpdusRxd;
    UINT4   u4NumTcnBpdusRxd;
    UINT4   u4NumRstBpdusTxd;
    UINT4   u4NumConfigBpdusTxd;
    UINT4   u4NumTcnBpdusTxd;    
    UINT4   u4ProtocolMigrationCount;     
    UINT1   u1PortTxSmState;
    UINT1   u1PmigSmState;
    UINT1   u1TxCount;
    UINT1   u1MigrationType;
}tPerStPvrstRstPortInfo;

typedef struct PerStPvrstBridgeInfo {
 tAstTimes       RootTimes; /* Contains Timer parameter
                                         * values of the current Root 
                                         * Bridge */
 tAstTimes       BridgeTimes; /* Contains Timer parameter
                                         * values of the current Bridge
                                         * instance */
 tAstBridgeId   bridgeId;
 UINT2           u2HelloTime;
 UINT1  u1TxHoldCount;
 UINT1   au1Reserved[1];
}tPerStPvrstBridgeInfo;

typedef struct PvrstBpdu {
    tAstBridgeId  RootId;
    tAstBridgeId  DesgBrgId;
    UINT4   u4RootPathCost;
    tVlanId             VlanId;
    UINT2   u2Length;
    UINT2   u2ProtocolId;
    UINT2   u2PortId;
    UINT2   u2MessageAge;
    UINT2   u2MaxAge;
    UINT2   u2HelloTime;
    UINT2   u2FwdDelay;
    tAstMacAddr  DestAddr;
    tAstMacAddr  SrcAddr;
    UINT1   au1LlcHeader[3];
    UINT1   u1Version1Len;
    UINT1   u1Version;
    UINT1   u1BpduType;
    UINT1   u1Flags;
    UINT1   u1IsPvrstPDU;
}tPvrstBpdu;

#else
typedef struct PerStPvrstRstPortInfo {
    UINT1 au1Dummy[4];
}tPerStPvrstRstPortInfo;
typedef struct PerStPvrstBridgeInfo{
    UINT1 au1Dummy[4];
}tPerStPvrstBridgeInfo;
#endif


   
/******************************************************************************/
/*                 Instance-specific Rst Port Information                     */
/******************************************************************************/

typedef struct AstPerStRstPortInfo {

   tAstTimer                 *pFdWhileTmr;     /* Forward delay timer node */
   tAstTimer                 *pTcWhileTmr;     /* Interval for which TCN msgs
                                                * are sent through the Root port
                                                * and Config msgs with Topology
                                                * change flag set */
   tAstTimer                 *pTcDetectedTmr;  /* This timer is started on 
                                                * detecting the Topology Change.
                                                * Used by Multiple Registration
                                                * Protocol for sending NEW 
                                                * messages.
                                                */
   tAstTimer                 *pRbWhileTmr;     /* The timer is non-zero if this
                                                * port is, or has recently been
                                                * a Backup Port */
   tAstTimer                 *pRrWhileTmr;     /* This timer is non-zero if this
                                                * port is, or has recently been
                                                * a Root Port */
   tAstTimer                 *pRcvdInfoTmr;    /* The time remaining before the
                                                * information held by this Port
                                                * expires */
   tAstTimer                *pRootIncRecTmr;   /* Root Inconsistency timer node*/
#ifdef PVRST_WANTED
   tPerStPvrstRstPortInfo    *pPerStPvrstRstPortInfo; 
                                               /* Pvrst required PVRST 
             * Port info*/
#endif 
   tAstBoolean                bRcvdMsg;        /* Set to TRUE by the
                                                * Port Receive Machine
                                                * if the BPDU received
                                                * contain a message 
                                                * for this tree */
   tAstBoolean                bUpdtInfo;       /* If set to TRUE, indicates
                                                * that Designated Priority and
                                                * Times values need to be
                                                * copied to the corresponding 
                                                * Port values */
   tAstBoolean                bProposing;      /* Set to TRUE if a Configuration
                                                * msg indicating a Designated 
                                                * Port's desire to rapidly 
                                                * transition to forwarding is
                                                * to be transmitted */
   tAstBoolean                bProposed;       /* Set to TRUE when a Config msg
                                                * indicating a Designated Port's
                                                * desire to rapidly transition
                                                * to forwarding state has been
                                                * received on a point-to-point
                                                * link */
   tAstBoolean                bDisputed;       /* Variable used to prevent a
                                                * Designated Port from 
                                                * transitioning to forwarding
                                                * if it continues to receive
                                                * Inferior Designated messages */
   tAstBoolean                bAgree;          /* Used by Port Transmit 
                                                * Machine to set the 
                                                * Agreement flag in the 
                                                * transmitted Configuration
                                                * message for given tree */
   tAstBoolean                bAgreed;         /* Set to TRUE if a RST Bpdu 
                                                * with the Agreement flag set
                                                * has been received on a point
                                                * to point link */
   tAstBoolean                bSync;           /* Set to TRUE by the root port
                                                * to instruct other non-operEdge
                                                * ports that are not in 
                                                * agreement with the current
                                                * spanning tree, to revert to
                                                * discarding state */
   tAstBoolean                bSynced;         /* Set to TRUE if port is in 
                                                * agreement with the current
                                                * spanning tree information */
   tAstBoolean                bReRoot;         /* Set to TRUE by the root port
                                                * to instruct other ports with
                                                * rrWhile timer running to 
                                                * revert to discarding state */
   tAstBoolean                bReSelect;       /* Set to TRUE if Port roles 
                                                * are to be recomputed */
   tAstBoolean                bSelected;       /* Set to TRUE after port role
                                                * computation is completed */
   tAstBoolean                bRcvdTc;         /* Set to TRUE when a Config
                                                * msg with Topology Change flag
                                                * is received */
   tAstBoolean                bTc;             /* Set to TRUE to indicate that
                                                * a topology change has 
                                                * occurred */
   tAstBoolean                bTcProp;         /* Set to TRUE for all other
                                                * ports to indicate that the
                                                * topology change has to be
                                                * propagated through this
                                                * port */
   tAstBoolean                bLearn;          /* Set to TRUE to enable source
                                                * address learning on this
                                                * port */
   tAstBoolean                bLearning;       /* Operational state for the
                                                * source address learning */
   tAstBoolean                bForward;        /* Set to TRUE to enable 
                                                * packet forwarding */
   tAstBoolean                bForwarding;     /* Operational state for the 
                                                * packet forwarding function */
   tAstBoolean                bPortEnabled;    /* Reflects the operational
                                                * state of the Bridge Port */
#ifdef MSTP_WANTED
   tAstBoolean                bAdminPortEnabled;/* Reflects the administrative
                                                * state of the Bridge Port */
#endif
   UINT1                      u1RcvdInfo;      /* Indicates if the received msg
                                                * is a SuperiorDesignatedMsg / 
                                                * RepeatedDesignatedMsg / Con-
                                                * firmedRootMsg / OtherMsg */
   UINT1                      u1InfoIs;        /* Indicates the origin/state 
                                                * of the spanning tree info
                                                * held for the port */

   UINT1                      u1RcvdExpTimerCount;
   UINT1                      u1SendConfRoot;
}tAstPerStRstPortInfo;



/******************************************************************************/
/*                 Instance-specific Port Information                         */
/******************************************************************************/

typedef struct AstPerStPortInfo {

   tAstPerStRstPortInfo       PerStRstPortInfo;
                                               /* Structure containing instance
                                                * specific RST Port related
                                                * information */
#ifdef MSTP_WANTED
   tPerStCistMstiCommInfo  PerStCistMstiCommPortInfo;
                                               /* structure 
                                                * containing instance specific
                                                * CIST and MSTI Port related 
                                                * information */
   tPerStMstiOnlyInfo  PerStMstiOnlyInfo;
                                               /* structure 
                                                * containing MSTP related 
                                                * information */
   
   tAstBridgeId               RegionalRootId;  /* Regional Root Bridge-Id 
                                                * of the MST Region */
   
   UINT4                      u4PortPathCost;  /* Path cost to the CIST Root 
                                                * bridge through this port */

   UINT4                      u4IntRootPathCost;/* Path cost to the Regional 
                                                * Root bridge through this 
                                                * port */

   #endif /* MSTP_WANTED */
   tAstBridgeId               DesgBrgId;       /* Bridge-Id of the designated
                                                * bridge for the LAN. In the
                                                * case of the Root Bridge, this
                                                * will contain its own Id */
   tAstBridgeId               RootId;          /* Bridge-Id of the Root 
                                                * Bridge */
   tAstBridgeId               PseudoRootId;     /* Gateway Port will use this
                                                   Root Id in Pseudo BPDU
                                                   formation.  */


   UINT4                      u4PortAdminPathCost;
                                                  /*administratively assigned 
                                                    value for the
                                                    contribution of this port to 
                                                    the path cost 
                                                    Zero means dynamically 
                                                    calculate vlaue to be used */


   UINT4                      u4NumFwdTransitions;
                                               /* Number of times this port
                                                * has changed from the learning
                                                * to the forwarding state */
   INT4                       i4NpPortStateStatus; /* Indicates the 
                                                    * status of the
                                                    * hardware port state
                                                    * program operation 
                                                    * for this port  */
   INT4                       i4NpFailRetryCount; /*Indicates the number of 
                                                    * times retries left for the                                                    * failed port state 
                                                    * programming.*/
   INT4                       i4TransmitSelfInfo; /* Indicates sending of self
                                                   * (inferior) information  
                                                   * when setting of blocking  
                                                   * port state in hardware
                                                   * fails */
   UINT4                      u4NumBpdusRx;    /* Number of Bpdus received 
                                                * through this port for this
                                                * instance of spanning tree */
   UINT4                      u4NumBpdusTx;    /* Number of Bpdus transmitted
                                                * through this port for this
                                                * spanning tree instance */
   UINT4                      u4NumInvalidBpdusRx;/* Number of Invalid Bpdus
                                                * received on this portfor this
                                                * spanning tree instance */

   UINT4                      u4RootCost;      /* Path cost to the CIST Root
                                                  bridge through this port */
   
   UINT4                     u4PortStateChangeTimeStamp; /*TimeStamp at
                                                              *which Port state
                                                              *was changed for
                                                              *this instance at
                                                              *thisport*/
   
   UINT2                      u2DesgPortId;    /* Port-Id of the designated 
                                                * bridge through which Bpdus
                                                * are received on this port */
   UINT2                      u2PortNo;        /* Logical port number of this
                                                * interface */
   
   UINT1                      u1PortPriority;  /* Priority of the port for 
                                                * this instance of the spanning
                                                * tree */
   UINT1                      u1PortRole;      /* Current role of this port in
                                                * this instance */
   UINT1                      u1SelectedPortRole;
                                               /* Port role calculated by the
                                                * Port Role Selection SEM */
   UINT1        u1OldPortRole;   /* Old Port Role of the port */
   UINT1                      u1PinfoSmState;  /* The present state of the Port
                                                * Information SEM */
   UINT1                      u1ProleTrSmState;
                                               /* The present state of the Port
                                                * Role Transition SEM */
   UINT1                      u1PstateTrSmState;
                                               /* The present state of the Port
                                                * State Transition SEM */
   UINT1                      u1TopoChSmState; /* The present state of the 
                                                * Topology Change SEM */
                /* For Redundancy */
   UINT1                      u1ChangedFlag;    /* determines if any info
                                              * is to be Synced with 
                                                 * standby */
   UINT1                      au1Padding[3];
   tAstBoolean                bIsPseudoRootIdConfigured;

   UINT2                      u2Inst;           /* Instance corresponding to 
                                                 * this entry
                                                 */
   UINT1                      u1LastProgrammedState; /* Last state of the port
                                                      * programmed in hardware
                                                      */         
   UINT1                      au1Reserved;

   tAstBoolean                bMstiPortClearStats;/* Per instance statistical cc
                                                     ounter
                                    * to reset all the port specific counters*/
   tAstBoolean                bLoopGuardStatus; /*Per Instance status of Loop-guard
              to track Per VLAN Loopguard*/
   tAstBoolean                bLoopIncStatus; /* Per Instance status of Loop-
             Inconsistent state*/
   tAstBoolean       bRootInconsistent;  /*  Variable added to set the  root-inconsistent
                                      *  state when the port state transitions to Discarding
                                      *  becasue of the root guard feature.*/

#if defined PVRST_WANTED || defined MSTP_WANTED
   UINT4  u4TcDetectedTimestamp; /** Tc detected timestamp **/
   UINT4  u4TcRcvdTimestamp; /** Tc received timestamp **/
   UINT4  u4TcDetectedCount; /** Tc Detected count **/
   UINT4  u4TcRcvdCount;  /** Tc Received count**/
#endif
   tAstBoolean                bSyncedFlag;     
   tAstBoolean                bRoleSyncedFlag;  
   tAstBoolean                bMstSelectedSyncedFlag;     
   tAstBoolean                bDisableInProgress;   
   INT4                       bBpduInconState;
   UINT4                      u4TcDetectedTimeStamp;
   UINT4                      u4TcRcvdTimeStamp;
   UINT4                      u4ProposalTxCount;
   UINT4                      u4ProposalRcvdCount;
   UINT4                      u4ProposalTxTimeStamp;
   UINT4                      u4ProposalRcvdTimeStamp;
   UINT4                      u4AgreementTxCount;
   UINT4                      u4AgreementRcvdCount;
   UINT4                      u4AgreementTxTimeStamp;
   UINT4                      u4AgreementRcvdTimeStamp;
   INT4                       i4OldPortState;   

}tAstPerStPortInfo;
   
/******************************************************************************/
/*                      Common Bridge Information Structure                   */
/******************************************************************************/

typedef struct AstBridgeEntry {
   
#ifdef MSTP_WANTED
   tAstMstBridgeEntry  MstBridgeEntry; /* Pointer to Bridge related
                                        * MSTP specific info */
#endif /* MSTP_WANTED */
   tAstTimes           RootTimes;      /* Contains Timer parameter
                                        * values of the current Root 
                                        * Bridge */
   tAstTimes           BridgeTimes;    /* Contains Timer parameter
                                        * values of the current Bridge
                                        * instance */
   UINT4   u4AstpUpCount;        /* Number of times RSTP/MSTP
                            * Module has been enabled */ 
   UINT4   u4AstpDownCount;      /* Number of times RSTP/MSTP
                              * Module has been disabled */
   UINT4     u4RcvdEventTimeStamp; /*Time stamp in ms at which 
                                     event specified by u4RcvdEvent was  
                                     received in this context*/
   INT4        i4RcvdEvent; /* Type of event rcvd recently in this bridge */
   INT4        i4RcvdEventSubType; /* Sub Type of event rcvd recently in this bridge */
   UINT4     u4PortStateChangeTimeStamp; /*Time stamp at which port state was 
                                           changed recently in this context*/
   UINT4     u4RstOptStatus;     /* Used to indicate the status of optimization
                                    done in RSTP during ALTERNATE PORT ROLE 
         transition as per IEEE 802.1Q*/
   tAstMacAddr  BridgeAddr;      /* Indicates unique 
                                  * address assigned to
                                  * this Bridge */
   UINT1   u1MigrateTime;       /* Initial value of mdelay
                                 * while timer */
   UINT1   u1TxHoldCount;       /* Used to limit the
                                 * maximum transmission
                                 * rate */
   UINT1   u1DynamicPathcostCalculation; /* Used to denote whether pathcost
                                          * is calculated dynamically from 
                                          * interface speed or not */
   UINT1   u1DynamicPathcostCalcLagg;/* Used to denote whether pathcost is
                                      * calculated for a Link Aggregated port 
                                      * on change in Active Port List 
                                      * If this variable is set, then 
                                      * path cost for the link aggregated port 
                                      * will be calculated whenever
                                      * Active physicals ports in that link 
                                      * aggregated port changes */

   
   UINT1   au1Reserved[2];
   tAstBoolean bBridgeClearStats;/*To clear all bridge related and interface
                                related counters*/

}tAstBridgeEntry;




/******************************************************************************/
/*                 Instance-specific Bridge Information                       */
/******************************************************************************/

typedef struct AstPerStBridgeInfo {
#ifdef PVRST_WANTED 
 tPerStPvrstBridgeInfo   *pPerStPvrstBridgeInfo;
#endif
   tAstBridgeId  RootId;          /* Bridge Identifier of the Root Bridge */
   tAstBridgeId  OldRootId;       /* Bridge Identifier of the Old Root Bridge */
   tAstBridgeId  DesgBrgId;       /* Designated bridge to the Root Bridge*/
   UINT4  u4RootCost;      /* Path cost from this Bridge
                            * to the Root Bridge */
   UINT4  u4CistInternalRootCost;
                           /* The path cost from the Bridge
                            * to the CIST Regional Root
                            * Bridge */
   UINT4  u4TimeSinceTopoCh;
                           /* Number of time ticks that
                            * have elapsed since the last
                            * topology change */
   UINT4  u4NumTopoCh;     /* Number of topology changes
                            * that the Bridge has noticed */
   UINT4  u4NewRootIdCount;
                           /* Number of times a New Root Bridge 
                            * has been detected */
   UINT4  u4FlushIndThreshold;  /* Number of flush indications can 
              go before timer method trigger */
   UINT4  u4FlushIndCount;      /* Number of actual flush indications
              after flush trigger timer expiry */
   UINT4  u4TotalFlushCount;    /* Total Number of flush invocations 
              for this instance */

   UINT2  u2SyncedFlag;     
   UINT2  u2RoleSyncedFlag;
   UINT2  u2MstSelectedSyncedFlag;
   UINT2  au2TcDetPorts[AST_MAX_TC_MEM_LEN];
   UINT2  au2TcRecvPorts[AST_MAX_TC_MEM_LEN];

#ifdef MSTP_WANTED
   UINT2  u2MasteredCount;     /* No of active ports with
                                      mstiMastered set */
   
   UINT2  u2PrevMasteredCount; /* Previous value of the count,
                                      used so that any changes in 
                                      the count can be tracked 
                                      at a single place */ 
   UINT2  u2InferiorRcvdRootPort;      /* The Root port on which inferior BPDU received  */
   
#endif /* MSTP_WANTED */
   
   UINT2  u2BrgPriority;   /* Bridge Priority of the Bridge 
                            * for this spanning tree 
                            * instance */
   UINT2  u2RootPort;      /* The port which gives the 
                            * shortest path to the root */
   UINT2  u2DesgPort;      /* The port which gives the  shortest path to
       * the Designated bridge towards root */
   UINT1  u1ProleSelSmState; 
                           /* The present state of the Port
                            * Role Selection SEM */
   UINT1  u1RootPriority;  /* Root priority of the bridge
       * for this spaning tree
       *  instance */
   UINT1  u1DetHead;
   UINT1  u1RcvdHead;
#ifdef MSTP_WANTED
   UINT1  u1RootRemainingHops;    
                           /* Remaining Hops Value used    
                            * to discard the received    
                            * information.*/
   UINT1  u1BrgRemainingHops;    
   tAstBoolean  bMstiBridgeClearStats;/*Per instance statistical counter to
                                    * reset all per-instance per-bridge counters*/
#endif
}tAstPerStBridgeInfo;



/******************************************************************************/
/*                 Instance-specific Bridge & Port Information                */
/******************************************************************************/

typedef struct AstPerStInfo {

   tAstPerStBridgeInfo        PerStBridgeInfo;  /* Instance specific Bridge
                                                 * information */
   tAstPerStPortInfo          **ppPerStPortInfo;
                                                /* Spanning tree specific Port 
                                                 * information of all Ports 
                                                 * belonging this instance */
   tAstTimer                  *pFlushTgrTmr;    /* Flushing trigger timer */
   tAstFlushFlag              FlushFlag;        /* Flushing related flag */  
   UINT4                      u4TcWhileCount;   /* Indicates TcWhile timer
               running status for this 
               instance 
               If non-zero - TCwhile Timer 
               running for any of the port
               in this instance. 
               If zero - TCWhile Timer is not 
               running for any port in this
               instance */
   UINT2                      u2InstanceId;     /* Identifier of the spanning
                                                 * tree instance */
   UINT1                      u1InstSyncFlag;   /* Flag to indicate if info 
                                                 * needs to be synced for this 
                                                 * instance*/
   UINT1                      u1RowStatus;      /* RowStatus variable for
                                                   creating and deleting an
                                                   instance */
   UINT2                      u2PortCount;      /* To count number of ports mapped
                                                   to the instance */
   UINT1                      au1Reserved[2];

}tAstPerStInfo;
   


/******************************************************************************/
/*                            RST Bpdu Message Structure                      */
/******************************************************************************/

typedef struct AstBpdu {

   tAstBridgeId               RootId;          /* The unique Identifier of the
                                                * bridge assumed to be the Root
                                                * by the bridge transmitting
                                                * the Bpdu */
   tAstBridgeId               DesgBrgId;       /* The unique Bridge Identifier
                                                * of the Bridge transmitting
                                                * the Bpdu */
   UINT4                      u4RootPathCost;  /* The cost of the path to the 
                                                * Root Bridge denoted by the
                                                * Root Identifier, from the 
                                                * transmitting bridge */
   UINT2                      u2Length;
   UINT2                      u2ProtocolId;    /* Protocol Identifier which 
                                                * takes the value of 0
                                                * identifying RST Algorithm
                                                * and Protocol */
   UINT2                      u2PortId;        /* The Port Identifier of the
                                                * Port on the transmitting
                                                * Bridge through which the Bpdu
                                                * was transmitted */
   UINT2                      u2MessageAge;    /* The age of the Rst Message,
                                                * being the time since the
                                                * generation of the Rst Bpdu
                                                * by the Root that instigated
                                                * the generation of this Bpdu */
   UINT2                      u2MaxAge;        /* A Timeout value to be used by
                                                * all Bridges in the Bridged
                                                * LAN. This value is set by the
                                                * Root. It is used to test the
                                                * age of stored information */
   UINT2                      u2HelloTime;     /* The time interval between the
                                                * generation of Rst Bpdus by 
                                                * the Root */
   UINT2                      u2FwdDelay;      /* A Timeout value to be used by
                                                * all Bridges in the Bridged
                                                * LAN. This value is set by the
                                                * Root. Its used when changing
                                                * the Port state to Forwarding
                                                * and for ageing Filtering
                                                * database entries */
   tAstMacAddr                DestAddr;
   tAstMacAddr                SrcAddr;
   UINT1                      au1LlcHeader[3];
   UINT1                      u1Version1Len;   /* Takes the value of 0 which
                                                * indicates that there is no
                                                * Version 1 Protocol information
                                                * present */
   UINT1                      u1Version;       /* Protocol Version Identifier
                                                * which takes the value of
                                                * 0 or 2 or 3 */
   UINT1                      u1BpduType;      /* Values -
                                                * 00000010 - RSTP BPDU
                                                * 00000000 - Config BPDU
                                                * 10000000 - TCN BPDU */
   
   UINT1                      u1Flags;         /* Contains all the Rst Flags
                                                * which are encoded bit-wise */
   UINT1                      au1Reserved[3];

}tAstBpdu;

typedef struct AstBpduType {
    tAstLocalMsgType     BpduType;
    union {
        tAstBpdu         RstBpdu;
#ifdef MSTP_WANTED
        tMstBpdu         MstBpdu;
#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
        tPvrstBpdu  PvrstBpdu;
#endif /* PVRST_WANTED */
    }uBpdu;
} tAstBpduType;


/* State Machine Entry type definitions */


typedef INT4 (* AST_BRGDETSM_FUNC)(UINT2 u2PortNum);

typedef INT4 (* AST_PRXSM_FUNC)(tAstBpdu *pRcvdBpdu, UINT2 u2PortNum);

typedef INT4 (* AST_PINFOSM_FUNC)(tAstPerStPortInfo *pPerStPortInfo, tAstBpdu *pRcvdBpdu);

typedef INT4 (* AST_PROLETRSM_FUNC)(tAstPerStPortInfo *);

typedef INT4 (* AST_PROLESELSM_FUNC)(UINT2 u2InstanceId);

typedef INT4 (* AST_PSTATETRSM_FUNC)(tAstPerStPortInfo *, UINT2);

typedef INT4 (* AST_PMIGSM_FUNC)(UINT2);

typedef INT4 (* AST_PTXSM_FUNC)(tAstPortEntry *pPortInfo, UINT2 u2InstanceId);

typedef INT4 (* AST_PTOPOCHSM_FUNC)(tAstPerStPortInfo *);

typedef INT4 (* AST_PPSEUDOINFOSM_FUNC)(UINT2 u2PortNum, tAstBpduType *pRcvdBpdu);

#ifdef PVRST_WANTED

/* State Machine Entry type definitions */

typedef INT4 (* PVRST_PSTATETRSM_FUNC)(tAstPerStPortInfo *);

typedef INT4 (* PVRST_PINFOSM_FUNC)(tAstPerStPortInfo *pPerStPortInfo, tPvrstBpdu *pRcvdBpdu);

typedef INT4 (* PVRST_PMIGSM_FUNC)(UINT2 u2PortNum, UINT2 u2InstanceId);

typedef INT4 (* PVRST_PTXSM_FUNC)(tAstPerStPortInfo *pPerStPortInfo);

typedef INT4 (* PVRST_BRGDETSM_FUNC)(UINT2 u2PortNum);

typedef INT4 (* PVRST_PROLESELSM_FUNC)(UINT2 u2InstanceId);

typedef INT4 (* PVRST_PROLETRSM_FUNC)(tAstPerStPortInfo *);

typedef INT4 (* PVRST_PTOPOCHSM_FUNC)(tAstPerStPortInfo *);

typedef struct PvrstPortStateTrSmEntry  {
      PVRST_PSTATETRSM_FUNC pAction;
} tPvrstPortStateTrSmEntry ;

typedef struct PvrstPortInfoSmEntry {
      PVRST_PINFOSM_FUNC pAction;
} tPvrstPortInfoSmEntry;

typedef struct PvrstPortMigSmEntry {
      PVRST_PMIGSM_FUNC pAction;
} tPvrstPortMigSmEntry;

typedef struct PvrstTxPortSmEntry {
      PVRST_PTXSM_FUNC pAction;
} tPvrstPortTxSmEntry;

typedef struct PvrstBrgDetSemEntry {
      PVRST_BRGDETSM_FUNC pAction;
} tPvrstBrgDetSemEntry;
typedef struct PvrstPortRoleSelSmEntry {
      PVRST_PROLESELSM_FUNC pAction;
} tPvrstPortRoleSelSmEntry;
typedef struct PvrstPortRoleTrSmEntry {
      PVRST_PROLETRSM_FUNC pAction;
} tPvrstPortRoleTrSmEntry;
typedef struct PvrstTopoChSmEntry {
      PVRST_PTOPOCHSM_FUNC pAction;
} tPvrstTopoChSmEntry;


#endif

/* State Machine Structures */

typedef struct AstBrgDetSemEntry {
      AST_BRGDETSM_FUNC pAction;
} tAstBrgDetSemEntry;

typedef struct AstPortRxSmEntry {
      AST_PRXSM_FUNC pAction;
} tAstPortRxSmEntry;

typedef struct AstPortInfoSmEntry {
      AST_PINFOSM_FUNC pAction;
} tAstPortInfoSmEntry;

typedef struct AstPortRoleSelSmEntry {
      AST_PROLESELSM_FUNC pAction;
} tAstPortRoleSelSmEntry;

typedef struct AstPortRoleTrSmEntry {
      AST_PROLETRSM_FUNC pAction;
} tAstPortRoleTrSmEntry;

typedef struct AstPortStateTrSmEntry {
      AST_PSTATETRSM_FUNC pAction;
} tAstPortStateTrSmEntry;

typedef struct AstPortMigSmEntry {
      AST_PMIGSM_FUNC pAction;
} tAstPortMigSmEntry;
      
typedef struct AstPortTxSmEntry {
      AST_PTXSM_FUNC pAction;
} tAstPortTxSmEntry;

typedef struct AstTopoChSmEntry {
      AST_PTOPOCHSM_FUNC pAction;
} tAstTopoChSmEntry;

typedef struct MstPortPseudoInfoSmEntry {
      AST_PPSEUDOINFOSM_FUNC pAction;
} tAstPortPseudoInfoSmEntry;

#ifdef MSTP_WANTED
typedef INT4 (* MST_PRXSM_FUNC)(tMstBpdu *, UINT2);
typedef INT4 (* MST_PTOPOCHSM_FUNC)(UINT2 u2InstanceId, tAstPerStPortInfo *pPerStPortInfo);
typedef INT4 (* MST_PINFOSM_FUNC)(tAstPerStPortInfo *pPerStPortInfo, tMstBpdu *pRcvdBpdu, UINT2 u2InstanceId);
typedef INT4 (* MST_PROLETRSM_FUNC)(tAstPerStPortInfo *, UINT2 u2InstanceId);

typedef struct MstPortRxSmEntry {
      MST_PRXSM_FUNC pAction;
} tMstPortRxSmEntry;

typedef struct MstPortInfoSmEntry {
      MST_PINFOSM_FUNC pAction;
} tMstPortInfoSmEntry;

typedef struct MstPortRoleTrSmEntry {
      MST_PROLETRSM_FUNC pAction;
} tMstPortRoleTrSmEntry;

typedef struct MstTopoChSmEntry {
      MST_PTOPOCHSM_FUNC pAction;
} tMstTopoChSmEntry;
#endif /* MSTP_WANTED */


/********************************************************************************/
/*                                                                              */
/*                  PROVIDER BRIDGE RELATED DATA STRUCTURE                      */
/*                                                                              */
/********************************************************************************/
typedef struct AstPbCVlanInfo {
    tAstContextInfo    *pParentContext;   /* Pointer to the parent context. */

    /*RBTree Containing Mapping of Protocol port and HL port Id*/
    tRBTree            Port2IndexMapTbl;  


    /* Port Table in C-VLAN component is maintained as dynamic array of pointers
     * to port entries. Even PerStPortTable in C-VLAN component will be
     * maintained as a dynamic array of pointers to PerStPortInfo entries.
     * A mempool will be created for each of these table. These mempools will
     * have only one block in it. When a port table needs to be allocated the
     * only block in the mempool for that port table will be used. If the port
     * table size needs to be increased, then a new mempool will be created that
     * will contain the block needed for the required port table. The
     * information from the old port table will be copied to the new port table
     * and then the old mempool will be deleted. PortTbleMemPoolId will have new
     * mempool id. Similarly for PerStPortTblMemPoolId.*/
    tAstMemPoolId       CvlanPortTblMemPoolId;
    tAstMemPoolId       CvlanPerStPortTblMemPoolId;

    UINT2               u2NumPorts;       /* Number of ports present in this 
                                           * C-VLAN context. */

    UINT2               u2HighPortIndex;  /* Highest port index in this C-VLAN
                                           * component. */
    /* CVLAN Module Status is maintained in the CVLAN Info*/
    UINT1              u1CVlanModuleStatus;
    UINT1              au1Pad[3];

}tAstPbCVlanInfo;
/*****************************************************************************/

typedef struct RstPortInfo{ 
    tAstSllNode        NextNode; 
    tAstPortEntry      *pPortInfo;
    UINT2              u2PortNum;
    UINT1              au1Pad[2];
    }tRstPortInfo; 

typedef struct MstInstInfo{
    tAstSllNode         NextNode;
    tAstPerStInfo       *pPerStInfo;
    UINT2               u2MstInstance;
    UINT1               au1Pad[2];
}tMstInstInfo;

struct AstContextInfo {
   UINT4                      u4ContextId;
   tAstSll                    PortList;
   tAstSll                    MstiList;
   tAstBoolean                bBegin;     /* BEGIN state machine condition */ 
   tAstBridgeEntry            BridgeEntry;        /* Bridge Information that
                                                   * is common irrespective
                                                   * of the number of 
                                                   * spanning trees 
                                                   * supported */
   tAstPortEntry              **ppPortEntry;
                                /* Pointer to array of pointers to Port 
                                 * Information that is common irrespective
                                 * of the number of spanning trees 
                                 * supported */
   
   tAstPerStInfo              **ppPerStInfo;  
                                /* Pointer to array of pointers to to Spanning 
                                 * Tree Instance specific Bridge and Port 
                                 * information */
   tRBTree                   PerContextIfIndexTable;


#if (defined MSTP_WANTED) || (defined PVRST_WANTED)
   INT4  *pInstanceUpCount;   /* Number of times a New Spanning
                                * Tree Instance has been 
                                * Created */
   
   INT4  *pInstanceDownCount;   /* Number of times a New Spanning
                                * Tree Instance has been 

                                * Deleted */
   UINT1     u1InstancePortMap; /* Enable or Disable Mst Instance Port Mapping*/
   UINT1     au1Padding[3];
#endif /*MSTP_WANTED || PVRST_WANTED*/

   UINT4                        u4BridgeMode; /* CUSTOMER/PROVIDER EDGE/
                                               * PROVIDER-CORE/PROVIDER-EDGE
                                               * Bridge */
#ifdef PVRST_WANTED
   UINT2                       *pInstIndexMap;
#endif

   tAstPbCVlanInfo             *pPbCVlanInfo; /* For C-VLAN context this 
                                               * pointer will not be null. 
                                               * This contains C-VLAN specific 
                                               * information. */

   UINT4                        u4CepIfIndex; /* CEP IfIndex if the 
                                               * u2CompType = AST_PB_C_VLAN. 
                                               * This index will be used in 
                                               * external interfaces and for 
                                               * trace and debug msgs.*/

  
   UINT4     u4TraceOption;         /* For enabling traces */

   UINT4     u4DebugOption;         /* For enabling debug 
                                       statements */
   UINT4     u4ContextTrapOption;        /* To set the traps */

   UINT4     u4FlushInterval;       /* Mimimum flush interval time */
   UINT4     u4GblBpduGuardStatus;  /* Indicates the GLobal BPDU Guard enabled/disabled status*/
   tAstBoolean    u1TEMSTIDValid;     /* Indicates TE_MSTID Creation */

   UINT2                        u2CompType;   /* C-VLAN component or a S-VLAN 
                                               * component. */

   UINT2                        u2CepPortNo;  /* Local port number of CEP
                                               * in S-VLAN context. This value
                                               * will be filled only for C-VLAN
                                               * contexts. This number is used
                                               * for selecting red C-VLAN 
                                               * context. */
 
   UINT2     u2PortTblSize;/* Number of entries the port 
                            * table in this context can 
                            * hold. */
#ifdef PVRST_WANTED
   UINT2     u2NoOfActiveInstances;   
#else
   UINT1     au1Reserved[2];
#endif

   UINT1     u1AstGenTrapType;    /* Indicate the general trap set */
   UINT1     u1ProtocolAdminStatus;  /* Stores the protocol Admin status */ 

   UINT1     u1ForceVersion;        /* Indicates the Force
                                     * Version parameter (Mst
                                     * or Rst or Stp 
                                     * Compatible mode) */
   UINT1     u1SystemAction;       /* Indicates if the module is 
                                    * being enabled/disabled. Used 
                                    * to give indication to VLAN/GARP
                                    */
   UINT1     au1StrContext [AST_CONTEXT_STR_LEN + 1]; /* Context Id in string
                                                       * form. */
   UINT1     u1SystemControl; 
   UINT1     u1SVlanModuleStatus;    /* SVLAN component Module Status*/
   UINT1     u1SVlanAdminStatus;     /* S-VLAN component configured mod 
                                      * status. */
   tAstTimer *pRestartTimer;        /* Timer to handle Impossible event/state
            and CRU Buffer failure cases*/
   tAstTimer *pInstCreate;
};

/******************************************************************************/
/*                             Global Structure                               */
/******************************************************************************/

typedef struct AstGlobalInfo {
   tAstTmrListId            TmrListId;  /* Id of the Timer
                                         * List used by the
                                         * Timer Module */

   tAstQId                  InputQId; /* QueueId of the AST Task */


   tAstQId                  CfgQId;      /* QueueId of the AST Configuration
                                            Queue */


   tAstTaskId               TaskId;   /* AST Task Id */
   tAstOsixSemId            SemId;   /* AST Sem4 Id */

   tAstMemPoolId            RstPortInfoMemPoolId;    
   tAstMemPoolId            MstInstInfoMemPoolId;
   tAstMemPoolId            FrameSizeMemPoolId;
#ifdef MSTP_WANTED
   tAstMemPoolId            MstVlanMapMemPoolId;
   tAstMemPoolId            MstVlanListMemPoolId;
#endif
   tAstMemPoolId            ContextMemPoolId;  /* Memory Pool Id of
                                                * the pool used for 
                                                * allocating memory 
                                                * block for ContextInfo
                                                */

   tAstMemPoolId            PortTableMemPoolId;    /* Id of memory pool to be
                                                    * used for allocating port
                                                    * table (array of port
                                                    * pointers) per virtual
                                                    * context. */

   tAstMemPoolId            PerStPortTablePoolId;  /* Id of memory pool to be
                                                    * used for allocating PerSt
                                                    * port table (array of PerSt
                                                    * portInfo pointers) per 
                                                    * virtual context. */

   INT4                     PerStInfoTablePoolId;  /* Id of memory pool to be
                                                    * used for allocating PerSt
                                                    * Info table (array of PerSt
                                                    * Info pointers) per 
                                                    * virtual context. */

   tAstPbMemPoolIds         PbMemPoolIds;          /* MemPool Ids specific for
                                                    * Provider bridge 
                                                    * informations. */

   tAstMemPoolId            TmrMemPoolId;  /* Memory Pool Id of
                                            * the pool used for
                                            * allocating memory
                                            * block of size
                                            * tAstTimer */
   tAstMemPoolId            PortInfoMemPoolId;  /* Memory Pool Id of
                                                 * the pool used for 
                                                 * allocating memory 
                                                 * block for PortEntry
                                                 */
   tAstMemPoolId            PerStInfoMemPoolId;       /* Memory Pool Id of
                                                       * the pool used for
                                                       * allocating memory
                                                       * block of size
                                                       * tPerStInfo */

   tAstMemPoolId            PerStPortInfoMemPoolId;   /* Memory Pool Id of
                                                         * the pool used for
                                                         * allocating memory
                                                         * block for each 
                                                         * port for each
                                                         * spanning tree
                                                         * instance */

   tAstMemPoolId            UpCountMemPoolId;         /* Memory pool Id of 
                                                         * the pool used for 
                                                         * allocating memory 
                                                         * blocks for each port
                                                         * for each spanning tree
                                                         * instance */
#ifdef MSTP_WANTED
   tAstMemPoolId            DigestInpMemPoolId;
#endif
   tAstMemPoolId            DownCountMemPoolId;         /* Memory pool Id of 
                                                         * the pool used for 
                                                         * allocating memory 
                                                         * blocks for each port
                                                         * for each spanning tree
                                                         * instance */
   tAstMemPoolId            LocalMsgMemPoolId;        /* Memory Pool Id of
                                                         * the pool used for
                                                         * allocating memory
                                                         * blocks for Local
                                                         * SNMP messages */
   tAstMemPoolId            QMsgMemPoolId;            /* Memory Pool Id of
                                                         * the pool used for
                                                         * allocating memory
                                                         * blocks for Messages 
                                                         * posted to AST Q */
   tAstMemPoolId            CfgQMsgMemPooIId;         /* Memory Pool Id of
                                                       * the pool used for
                                                       * allocating memory
                                                       * blocks for Messages
                                                       * posted to AST CFG Q */
   tAstMemPoolId            BpdutypeMemPoolId;       
                                                      
                                                      
                                                     

 
#ifdef PVRST_WANTED
    tAstMemPoolId     PvrstBrgInfoMemPoolId;    /* Memory Pool Id of 
             * the pool used for 
             * PVRST specific 
             * bridge info*/
    tAstMemPoolId     PvrstRstPortInfoMemPoolId;/* Memory Pool Id of 
             * the pool used for 
             * PVRST specific 
             * RST port info*/
    tAstMemPoolId      PvrstVlanToIndexMapMemPoolId;/* Memory Pool Id of
                               * the pool used for 
                               * PVRST VLAN to 
                               * Index mapping info */
 #endif
 
   tAstBrgDetSemEntry         aaBrgDetStateMachine
      [RST_BRGDETSM_MAX_EVENTS]
      [RST_BRGDETSM_MAX_STATES];
                                   /* Array of action
                                    * routine pointers
                                    * for Bridge Detection 
                                    * Sem */
   tAstPortRxSmEntry        aaPortRcvMachine
      [RST_PRCVSM_MAX_EVENTS]
      [RST_PRCVSM_MAX_STATES];
                                   /* Array of action
                                    * routine pointers
                                    * for Port Receive
                                    * Sem */
   tAstPortInfoSmEntry        aaPortInfoMachine
      [RST_PINFOSM_MAX_EVENTS]
      [RST_PINFOSM_MAX_STATES];
                                   /* Array of action
                                    * routine pointers
                                    * for Port Info 
                                    * Sem */
   tAstPortRoleSelSmEntry     aaPortRoleSelMachine   
      [RST_PROLESELSM_MAX_EVENTS]   
      [RST_PROLESELSM_MAX_STATES];    /* Array of action
                                    * routine pointers 
                                    * for Port Role 
                                    * Selection Sem */
   tAstPortRoleTrSmEntry      aaPortRoleTrMachine
      [RST_PROLETRSM_MAX_EVENTS]
      [RST_PROLETRSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Role
                                    * Transtion Sem */
   tAstPortStateTrSmEntry     aaPortStateTrMachine
      [RST_PSTATETRSM_MAX_EVENTS]
      [RST_PSTATETRSM_MAX_STATES]; /* Array of action
                                    * routine pointers
                                    * for Port State
                                    * Transition Sem */
   tAstPortMigSmEntry         aaPortMigMachine
      [RST_PMIGSM_MAX_EVENTS]
      [RST_PMIGSM_MAX_STATES];     /* Array of action 
                                    * routine pointers
                                    * for Port Role 
                                    * Migration Sem */
   tAstPortTxSmEntry            aaPortTxMachine
      [RST_PTXSM_MAX_EVENTS]
      [RST_PTXSM_MAX_STATES];      /* Array of action
                                    * routine pointers
                                    * for Port Transmit
                                    * Sem */
   tAstTopoChSmEntry          aaTopoChMachine
      [RST_TOPOCHSM_MAX_EVENTS]
      [RST_TOPOCHSM_MAX_STATES];   /* Array of action
                                    * routine pointers
                                    * for Topology 
                                    * Change Sem */
   tAstPortPseudoInfoSmEntry      aaPortPseudoInfoMachine
      [RST_PSEUDO_INFO_MAX_EVENTS]
      [RST_PSEUDO_INFO_MAX_STATE];  /* Array of action
                                    * routine pointers
                                    * for Port Pseudo Info
                                    * Sem */
#ifdef MSTP_WANTED
   tMstPortRxSmEntry         aaMstPortRcvMachine
      [MST_PRCVSM_MAX_EVENTS]
      [MST_PRCVSM_MAX_STATES];     /* Array of action
                                    * routine pointers
                                    * for Port Receive
                                    * Sem */
   tMstTopoChSmEntry          aaMstTopoChMachine
      [MST_TOPOCHSM_MAX_EVENTS]
      [MST_TOPOCHSM_MAX_STATES];   /* Array of action
                                    * routine pointers
                                    * for Topology 
                                    * Change Sem */
   tMstPortInfoSmEntry        aaMstPortInfoMachine                           
      [MST_PINFOSM_MAX_EVENTS]                                           
      [MST_PINFOSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Info 
                                    * Sem */
   tMstPortRoleTrSmEntry      aaMstPortRoleTrMachine
      [MST_PROLETRSM_MAX_EVENTS]
      [MST_PROLETRSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Role
                                    * Transtion Sem */
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED

    tPvrstPortStateTrSmEntry     aaPvrstPortStateTrMachine
      [PVRST_PSTATETRSM_MAX_EVENTS]
      [PVRST_PSTATETRSM_MAX_STATES]; /* Array of action
                                    * routine pointers
                                    * for Port State
                                    * Transition Sem */
    tPvrstPortInfoSmEntry        aaPvrstPortInfoMachine
      [PVRST_PINFOSM_MAX_EVENTS]
      [PVRST_PINFOSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Role
                                    * Transtion Sem */

   tPvrstPortMigSmEntry      aaPvrstPortMigMachine
      [PVRST_PMIGSM_MAX_EVENTS]
      [PVRST_PMIGSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Role
                                    * Transtion Sem */
    tPvrstBrgDetSemEntry         aaPvrstBrgDetStateMachine
      [PVRST_BRGDETSM_MAX_EVENTS]
      [PVRST_BRGDETSM_MAX_STATES];
                                   /* Array of action
                                    * routine pointers
                                    * for Bridge Detection 
                                    * Sem */

   tPvrstPortRoleSelSmEntry     aaPvrstPortRoleSelMachine   
      [PVRST_PROLESELSM_MAX_EVENTS]   
      [PVRST_PROLESELSM_MAX_STATES];    /* Array of action
                                    * routine pointers 
                                    * for Port Role 
                                    * Selection Sem */
   tPvrstPortRoleTrSmEntry      aaPvrstPortRoleTrMachine
      [PVRST_PROLETRSM_MAX_EVENTS]
      [PVRST_PROLETRSM_MAX_STATES];    /* Array of action
                                    * routine pointers
                                    * for Port Role
                                    * Transtion Sem */

   tPvrstPortTxSmEntry      aaPvrstPortTxMachine
      [PVRST_PTXSM_MAX_EVENTS]
      [PVRST_PTXSM_MAX_STATES];      /* Array of action
                                    * routine pointers
                                    * for Port Transmit
                                    * Sem */
    tPvrstTopoChSmEntry          aaPvrstTopoChMachine
      [PVRST_TOPOCHSM_MAX_EVENTS]
      [PVRST_TOPOCHSM_MAX_STATES];   /* Array of action
                                    * routine pointers
                                    * for Topology 
                                    * Change Sem */

#endif

   UINT1            *gpu1AstBpdu;
   UINT1            gau1AstBpdu [AST_MAX_ETH_FRAME_SIZE];
   UINT1     u1AstErrTrapType;    /* Indicate the error trap set */
   /* HITLESS RESTART */
   UINT1     u1StdyStReqRcvd;   /* Flag to indicate whether Steady State Req.
                                   is recieved during Hitless Restart */
   UINT4            u4GlobalTrapOption;        /* To set the traps */
   UINT4     u4GlobalTraceOption;        /* To set the global trace */
   UINT4     u4GlobalDebugOption;        /* To set the global debug */
   UINT4   u4BufferFailureCount; /* Number of times Buffer 
                            * Failure has occured */
   UINT4   u4MemoryFailureCount; /* Number of times Memory 
                      * Failure has occured */

   tRBTree           GlobalIfIndexTable;
   tAstContextInfo  *apContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
   INT4     i4SysLogId;
   INT4      i4AstAsyncMode;      /* Indicates the mode of NPAPI s
                                   * synchronus,asynchronous */
   tAstBpdu             *gpPseudoRstBpdu; /* Pseudo Rst BPDU pointer, it will be
                                             used to form RST BPDU on L2gp port.
                                            */
   tAstBpdu             gau1PseudoRstBpdu [AST_PSEUDO_BPDU_SIZE];
#ifdef MSTP_WANTED   
   tMstBpdu             *gpPseudoMstBpdu; /* Pseudo Mst BPDU pointer, it will be
                                             used to form MST BPDU on L2gp port.
                                            */
   tMstBpdu             gau1PseudoMstBpdu [MST_PSEUDO_BPDU_SIZE];
#endif   

}tAstGlobalInfo;

                                                
/******************************************************************************/
/*                           Local Message Structure                          */
/******************************************************************************/
typedef VOID *tAstPeerId;

/* Data structures used for Messaging/Interface with RM  */
typedef struct _tAstRmCtrlMsg {
#ifdef L2RED_WANTED
    tRmMsg        *pFrame;
#else
  VOID          *pFrame;
#endif
    tAstPeerId     PeerId;
    UINT2          u2Length;
    UINT1          u1Event;
    UINT1          u1Reserved;
} tAstRmCtrlMsg;

#ifdef NPAPI_WANTED
/* union for stp related asynchronus Npapi callback strucures */
typedef union {
    tAsNpwFsMiRstpNpSetPortState FsMiRstpNpSetPortState;
    tAsNpwFsMiMstpNpSetInstancePortState FsMiMstpNpSetInstancePortState;
    tAsNpwFsMiPvrstNpSetVlanPortState FsMiPvrstNpSetVlanPortState;
} uAstNpapiParams;

typedef struct AstNpapiCbParams {
    uAstNpapiParams  AstNpapiParams;
    int     CallId;
} tAstNpapiCbParams;
#endif/*NPAPI_WANTED*/

typedef struct AstMsgNode {
   tAstLocalMsgType      MsgType;
   UINT4                 u4PortNo;
   UINT4                 u4ContextId;
   UINT4                 u4BridgeMode;
   tLocalPortList  AddedPorts;
   tLocalPortList  DeletedPorts;
   tLocalPortList  TaggedPorts;

   union {
    tAstInterface   IfaceId;
    tAstBoolean     bAdminEdgePort;
#ifdef MSTP_WANTED
    tAstString      ConfigName;
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED 
    tEncapsulationType eEncapType;
    tAstBoolean  bStatus;
    UINT1         u1PortType;
#endif /* PVRST_WANTED */
    tMstVlanId      VlanId; /* This vlanId will be used in MSTP as well
                               as for PEP create/delete/oper indications. */
    UINT4           u4PathCost;
    UINT2           u2MsgAgeOrHopCount;
    UINT2           u2LocalPortId;
    UINT2           u2MaxAge;
    UINT2           u2ForwardDelay;
    UINT2           u2HelloTime;
    UINT2           u2HoldTime;
    UINT2           u2BridgePriority;
    UINT1           u1DynamicPathcostCalculation;
#ifdef MSTP_WANTED
    UINT2           u2MaxInstNumber;
    UINT2           u2ConfigRevLevel;
#endif /* MSTP_WANTED */
    UINT1           u1PortPriority;
    UINT1           u1ForceVersion;
    UINT1           u1TxHoldCount;
    UINT1           u1PathCostDefault;
    UINT1           u1AdminPToP;
    UINT1           u1TrigType;
    UINT1           u1BrgPortType;
#ifdef MSTP_WANTED
    UINT1           u1MaxHopCount;
    UINT1           u1ConfigIdSel;
    tAstMstVlanList MstVlanList;
#endif /* MSTP_WANTED */
    tAstBoolean      bAutoEdgePort; 
    tAstRmCtrlMsg   *pRmCtrlMsg;  
    tAstBridgeId    BridgeId;
    tAstBoolean     bPortL2gp;
    tAstBoolean     bLoopGuard;    /* Set as True causes a
          port to retain its state and role */
    tAstBoolean     bEnableBPDURx;
    tAstBoolean     bEnableBPDUTx;
#ifdef NPAPI_WANTED
    tAstNpapiCbParams AstNpapiCbParams;
#endif/*NPAPI_WANTED*/
    tAstBoolean     bSispStatus;
    tAstBoolean     bDot1wEnabled;
    UINT4           u4BpduGuardStatus;
    UINT4           u4ErrorRecovery;
    UINT1           u1BpduGuardAction;
    UINT1           au1Pad[3];
   } uMsg;

   UINT2           u2InstanceId;
   UINT1           au1Pad[2];
}tAstMsgNode;

typedef struct AstQMsg {
   tAstLocalMsgType      LocalMsgType;
   union {
      tAstMsgNode        *pMsgNode;
      tAstBufChainHeader *pBpduInQ;
#ifdef MBSM_WANTED
      struct MbsmIndication {
          tMbsmProtoMsg *pMbsmProtoMsg;
      }MbsmCardUpdate;
#endif
      tAstRmCtrlMsg      *pRmMsg;
   }uQMsg;

} tAstQMsg;

/****************************************************************************
 *                  PROVIDER BRIDGE RELATED DATA STRUCTURE                   
*****************************************************************************/

/*This structure is maintained for each C-VLAN context in the system. The
 * purpose of this structure is maintain the database of mapping of protocol
 * port number and Hl port number for the ports (CEP & PEP's) in the CVLAN
 * context*/
typedef struct AstPort2IndexMap {
    UINT2          u2ProtPort;    /*Protocol Port Number of the PEP & CEP*/
    UINT2          u2PortIndex;   /*Generated Logical Port Id of the PEP & CEP*/
}tAstPort2IndexMap;

/* AST_PB_C_VLAN --- This enum denotes the CVLAN component present in the
 * Provider Edge Bridge mode.
 * AST_PB_S_VLAN --- This denotes the SVLAN component present in Provider Edge
 * bridge and Provider Core bridge mode
 * AST_C_VLAN --- This enum is only used for denoting the Customer Bridge*/

typedef enum {
       AST_PB_C_VLAN =1, 
       AST_PB_S_VLAN,
       AST_C_VLAN,
       AST_PB_I_COMP
}tAstPbCompType;

/*******************************************************************************/

typedef tFilterEntry  tAstPbbFilterEntry;

/* RSTP Sizing ID. */
enum
{
#ifdef L2RED_WANTED
    AST_RED_CVLAN_PORT_TBL_SIZING_ID,
    AST_RED_DATA_SIZING_ID,
    AST_RED_TIMES_SIZING_ID,
#endif
   
    AST_CVLAN_PB_PORTINFO_SIZING_ID,
    AST_CVLAN_PB_PERST_PORTINFO_SIZING_ID,
    AST_NO_SIZING_ID
};


/* AST-STRUCTURES USED FOR ONLY FOR MEMORY ALLOCATION */
/* START */
typedef struct AstPortArray {
        tAstPortEntry   *apPortInfo[AST_MAX_PORTS_PER_CONTEXT];
}tAstPortArray;

typedef struct AstPerStPortArray {
        tAstPerStPortInfo  *apStPortInfo[AST_MAX_PORTS_PER_CONTEXT];
}tAstPerStPortArray;

typedef struct AstMsgDigestSize {
    UINT2   au2MsgDigest[AST_VID_MAP_LEN];
}tAstMsgDigestsize;

typedef struct AstFrameSize {
    UINT1               au1Buf[AST_MAX_ETH_FRAME_SIZE];
}tAstFrameSize;

#ifdef MSTP_WANTED
typedef struct MstVlanList {
    UINT1               au1List[MST_VLAN_LIST_SIZE];
}tMstVlanList;

typedef struct MstVlanMap {
    UINT1               au1VlanMapBuf[MST_VLANMAP_LIST_SIZE];
}tMstVlanMap;

#endif
/* END */

#endif /* _ASTTDFS_H_ */

/* END OF FILE */

