/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrstdb.h,v 1.22 2017/09/12 14:08:12 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRSTDB_H
#define _FSRSTDB_H

UINT1 FsRstPortExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsrst [] ={1,3,6,1,4,1,2076,79};
tSNMP_OID_TYPE fsrstOID = {8, fsrst};


UINT4 FsRstSystemControl [ ] ={1,3,6,1,4,1,2076,79,1,1};
UINT4 FsRstModuleStatus [ ] ={1,3,6,1,4,1,2076,79,1,2};
UINT4 FsRstTraceOption [ ] ={1,3,6,1,4,1,2076,79,1,3};
UINT4 FsRstDebugOption [ ] ={1,3,6,1,4,1,2076,79,1,4};
UINT4 FsRstRstpUpCount [ ] ={1,3,6,1,4,1,2076,79,1,5};
UINT4 FsRstRstpDownCount [ ] ={1,3,6,1,4,1,2076,79,1,6};
UINT4 FsRstBufferFailureCount [ ] ={1,3,6,1,4,1,2076,79,1,7};
UINT4 FsRstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,79,1,8};
UINT4 FsRstNewRootIdCount [ ] ={1,3,6,1,4,1,2076,79,1,9};
UINT4 FsRstPortRoleSelSmState [ ] ={1,3,6,1,4,1,2076,79,1,10};
UINT4 FsRstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,79,1,11};
UINT4 FsRstPort [ ] ={1,3,6,1,4,1,2076,79,1,12,1,1};
UINT4 FsRstPortRole [ ] ={1,3,6,1,4,1,2076,79,1,12,1,2};
UINT4 FsRstPortOperVersion [ ] ={1,3,6,1,4,1,2076,79,1,12,1,3};
UINT4 FsRstPortInfoSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,4};
UINT4 FsRstPortMigSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,5};
UINT4 FsRstPortRoleTransSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,6};
UINT4 FsRstPortStateTransSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,7};
UINT4 FsRstPortTopoChSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,8};
UINT4 FsRstPortTxSmState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,9};
UINT4 FsRstPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,10};
UINT4 FsRstPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,11};
UINT4 FsRstPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,12};
UINT4 FsRstPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,13};
UINT4 FsRstPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,14};
UINT4 FsRstPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,15};
UINT4 FsRstPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,16};
UINT4 FsRstPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,17};
UINT4 FsRstPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,18};
UINT4 FsRstPortProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,19};
UINT4 FsRstPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,20};
UINT4 FsRstPortAutoEdge [ ] ={1,3,6,1,4,1,2076,79,1,12,1,21};
UINT4 FsRstPortRestrictedRole [ ] ={1,3,6,1,4,1,2076,79,1,12,1,22};
UINT4 FsRstPortRestrictedTCN [ ] ={1,3,6,1,4,1,2076,79,1,12,1,23};
UINT4 FsRstPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,79,1,12,1,24};
UINT4 FsRstPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,79,1,12,1,25};
UINT4 FsRstPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,79,1,12,1,26};
UINT4 FsRstPortIsL2Gp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,27};
UINT4 FsRstPortLoopGuard [ ] ={1,3,6,1,4,1,2076,79,1,12,1,28};
UINT4 FsRstPortRcvdEvent [ ] ={1,3,6,1,4,1,2076,79,1,12,1,29};
UINT4 FsRstPortRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,79,1,12,1,30};
UINT4 FsRstPortRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,31};
UINT4 FsRstPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,32};
UINT4 FsRstPortRowStatus [ ] ={1,3,6,1,4,1,2076,79,1,12,1,33};
UINT4 FsRstPortBpduGuard [ ] ={1,3,6,1,4,1,2076,79,1,12,1,34};
UINT4 FsRstPortRootGuard [ ] ={1,3,6,1,4,1,2076,79,1,12,1,35};
UINT4 FsRstRootInconsistentState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,36};
UINT4 FsRstPortErrorRecovery [ ] ={1,3,6,1,4,1,2076,79,1,12,1,37};
UINT4 FsRstPortStpModeDot1wEnabled [ ] ={1,3,6,1,4,1,2076,79,1,12,1,38};
UINT4 FsRstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,39};
UINT4 FsRstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,79,1,12,1,40};
UINT4 FsRstPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,41};
UINT4 FsRstPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,42};
UINT4 FsRstPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,43};
UINT4 FsRstPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,44};
UINT4 FsRstPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,45};
UINT4 FsRstPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,46};
UINT4 FsRstPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,79,1,12,1,47};
UINT4 FsRstPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,79,1,12,1,48};
UINT4 FsRstPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,49};
UINT4 FsRstPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,50};
UINT4 FsRstPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,79,1,12,1,51};
UINT4 FsRstPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,79,1,12,1,52};
UINT4 FsRstPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,53};
UINT4 FsRstPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,54};
UINT4 FsRstPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,79,1,12,1,55};
UINT4 FsRstPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,12,1,56};
UINT4 FsRstPortOldPortState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,57};
UINT4 FsRstPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,79,1,12,1,58};
UINT4 FsRstDynamicPathcostCalculation [ ] ={1,3,6,1,4,1,2076,79,1,13};
UINT4 FsRstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,79,1,14};
UINT4 FsRstRcvdEvent [ ] ={1,3,6,1,4,1,2076,79,1,15};
UINT4 FsRstRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,79,1,16};
UINT4 FsRstRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,17};
UINT4 FsRstRcvdPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,79,1,18};
UINT4 FsRstBpduGuard [ ] ={1,3,6,1,4,1,2076,79,1,19};
UINT4 FsRstStpPerfStatus [ ] ={1,3,6,1,4,1,2076,79,1,20};
UINT4 FsRstSetTraps [ ] ={1,3,6,1,4,1,2076,79,2,1};
UINT4 FsRstGenTrapType [ ] ={1,3,6,1,4,1,2076,79,2,2};
UINT4 FsRstErrTrapType [ ] ={1,3,6,1,4,1,2076,79,2,3};
UINT4 FsRstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,79,2,4,1,1};
UINT4 FsRstPortMigrationType [ ] ={1,3,6,1,4,1,2076,79,2,4,1,2};
UINT4 FsRstPktErrType [ ] ={1,3,6,1,4,1,2076,79,2,4,1,3};
UINT4 FsRstPktErrVal [ ] ={1,3,6,1,4,1,2076,79,2,4,1,4};
UINT4 FsRstPortRoleType [ ] ={1,3,6,1,4,1,2076,79,2,4,1,5};
UINT4 FsRstOldRoleType [ ] ={1,3,6,1,4,1,2076,79,2,4,1,6};


tMbDbEntry fsrstMibEntry[]= {

{{10,FsRstSystemControl}, NULL, FsRstSystemControlGet, FsRstSystemControlSet, FsRstSystemControlTest, FsRstSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRstModuleStatus}, NULL, FsRstModuleStatusGet, FsRstModuleStatusSet, FsRstModuleStatusTest, FsRstModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRstTraceOption}, NULL, FsRstTraceOptionGet, FsRstTraceOptionSet, FsRstTraceOptionTest, FsRstTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRstDebugOption}, NULL, FsRstDebugOptionGet, FsRstDebugOptionSet, FsRstDebugOptionTest, FsRstDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRstRstpUpCount}, NULL, FsRstRstpUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstRstpDownCount}, NULL, FsRstRstpDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstBufferFailureCount}, NULL, FsRstBufferFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstMemAllocFailureCount}, NULL, FsRstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstNewRootIdCount}, NULL, FsRstNewRootIdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstPortRoleSelSmState}, NULL, FsRstPortRoleSelSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstOldDesignatedRoot}, NULL, FsRstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRstPort}, GetNextIndexFsRstPortExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRole}, GetNextIndexFsRstPortExtTable, FsRstPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortOperVersion}, GetNextIndexFsRstPortExtTable, FsRstPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortInfoSmState}, GetNextIndexFsRstPortExtTable, FsRstPortInfoSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortMigSmState}, GetNextIndexFsRstPortExtTable, FsRstPortMigSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRoleTransSmState}, GetNextIndexFsRstPortExtTable, FsRstPortRoleTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortStateTransSmState}, GetNextIndexFsRstPortExtTable, FsRstPortStateTransSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTopoChSmState}, GetNextIndexFsRstPortExtTable, FsRstPortTopoChSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTxSmState}, GetNextIndexFsRstPortExtTable, FsRstPortTxSmStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRxRstBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRxConfigBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRxTcnBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTxRstBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTxConfigBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTxTcnBpduCount}, GetNextIndexFsRstPortExtTable, FsRstPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortInvalidRstBpduRxCount}, GetNextIndexFsRstPortExtTable, FsRstPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortInvalidConfigBpduRxCount}, GetNextIndexFsRstPortExtTable, FsRstPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortInvalidTcnBpduRxCount}, GetNextIndexFsRstPortExtTable, FsRstPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortProtocolMigrationCount}, GetNextIndexFsRstPortExtTable, FsRstPortProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortEffectivePortState}, GetNextIndexFsRstPortExtTable, FsRstPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortAutoEdge}, GetNextIndexFsRstPortExtTable, FsRstPortAutoEdgeGet, FsRstPortAutoEdgeSet, FsRstPortAutoEdgeTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRestrictedRole}, GetNextIndexFsRstPortExtTable, FsRstPortRestrictedRoleGet, FsRstPortRestrictedRoleSet, FsRstPortRestrictedRoleTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRestrictedTCN}, GetNextIndexFsRstPortExtTable, FsRstPortRestrictedTCNGet, FsRstPortRestrictedTCNSet, FsRstPortRestrictedTCNTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortEnableBPDURx}, GetNextIndexFsRstPortExtTable, FsRstPortEnableBPDURxGet, FsRstPortEnableBPDURxSet, FsRstPortEnableBPDURxTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "1"},

{{12,FsRstPortEnableBPDUTx}, GetNextIndexFsRstPortExtTable, FsRstPortEnableBPDUTxGet, FsRstPortEnableBPDUTxSet, FsRstPortEnableBPDUTxTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "1"},

{{12,FsRstPortPseudoRootId}, GetNextIndexFsRstPortExtTable, FsRstPortPseudoRootIdGet, FsRstPortPseudoRootIdSet, FsRstPortPseudoRootIdTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortIsL2Gp}, GetNextIndexFsRstPortExtTable, FsRstPortIsL2GpGet, FsRstPortIsL2GpSet, FsRstPortIsL2GpTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsRstPortLoopGuard}, GetNextIndexFsRstPortExtTable, FsRstPortLoopGuardGet, FsRstPortLoopGuardSet, FsRstPortLoopGuardTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsRstPortRcvdEvent}, GetNextIndexFsRstPortExtTable, FsRstPortRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRcvdEventSubType}, GetNextIndexFsRstPortExtTable, FsRstPortRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRcvdEventTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortStateChangeTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRowStatus}, GetNextIndexFsRstPortExtTable, FsRstPortRowStatusGet, FsRstPortRowStatusSet, FsRstPortRowStatusTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 1, NULL},

{{12,FsRstPortBpduGuard}, GetNextIndexFsRstPortExtTable, FsRstPortBpduGuardGet, FsRstPortBpduGuardSet, FsRstPortBpduGuardTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRootGuard}, GetNextIndexFsRstPortExtTable, FsRstPortRootGuardGet, FsRstPortRootGuardSet, FsRstPortRootGuardTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstRootInconsistentState}, GetNextIndexFsRstPortExtTable, FsRstRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsRstPortErrorRecovery}, GetNextIndexFsRstPortExtTable, FsRstPortErrorRecoveryGet, FsRstPortErrorRecoverySet, FsRstPortErrorRecoveryTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "30000"},

{{12,FsRstPortStpModeDot1wEnabled}, GetNextIndexFsRstPortExtTable, FsRstPortStpModeDot1wEnabledGet, FsRstPortStpModeDot1wEnabledSet, FsRstPortStpModeDot1wEnabledTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortBpduInconsistentState}, GetNextIndexFsRstPortExtTable, FsRstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, "2"},

{{12,FsRstPortBpduGuardAction}, GetNextIndexFsRstPortExtTable, FsRstPortBpduGuardActionGet, FsRstPortBpduGuardActionSet, FsRstPortBpduGuardActionTest, FsRstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRstPortExtTableINDEX, 1, 0, 0, "1"},
{{12,FsRstPortTCDetectedCount}, GetNextIndexFsRstPortExtTable, FsRstPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTCReceivedCount}, GetNextIndexFsRstPortExtTable, FsRstPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTCDetectedTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortTCReceivedTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRcvInfoWhileExpCount}, GetNextIndexFsRstPortExtTable, FsRstPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortProposalPktsSent}, GetNextIndexFsRstPortExtTable, FsRstPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortProposalPktsRcvd}, GetNextIndexFsRstPortExtTable, FsRstPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortProposalPktSentTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortProposalPktRcvdTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortAgreementPktSent}, GetNextIndexFsRstPortExtTable, FsRstPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortAgreementPktRcvd}, GetNextIndexFsRstPortExtTable, FsRstPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortAgreementPktSentTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortAgreementPktRcvdTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortImpStateOccurCount}, GetNextIndexFsRstPortExtTable, FsRstPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortImpStateOccurTimeStamp}, GetNextIndexFsRstPortExtTable, FsRstPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortOldPortState}, GetNextIndexFsRstPortExtTable, FsRstPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortLoopInconsistentState}, GetNextIndexFsRstPortExtTable, FsRstPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortExtTableINDEX, 1, 0, 0, "2"},

{{10,FsRstDynamicPathcostCalculation}, NULL, FsRstDynamicPathcostCalculationGet, FsRstDynamicPathcostCalculationSet, FsRstDynamicPathcostCalculationTest, FsRstDynamicPathcostCalculationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRstCalcPortPathCostOnSpeedChg}, NULL, FsRstCalcPortPathCostOnSpeedChgGet, FsRstCalcPortPathCostOnSpeedChgSet, FsRstCalcPortPathCostOnSpeedChgTest, FsRstCalcPortPathCostOnSpeedChgDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRstRcvdEvent}, NULL, FsRstRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstRcvdEventSubType}, NULL, FsRstRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstRcvdEventTimeStamp}, NULL, FsRstRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstRcvdPortStateChangeTimeStamp}, NULL, FsRstRcvdPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstBpduGuard}, NULL, FsRstBpduGuardGet, FsRstBpduGuardSet, FsRstBpduGuardTest, FsRstBpduGuardDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRstStpPerfStatus}, NULL, FsRstStpPerfStatusGet, FsRstStpPerfStatusSet, FsRstStpPerfStatusTest, FsRstStpPerfStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRstSetTraps}, NULL, FsRstSetTrapsGet, FsRstSetTrapsSet, FsRstSetTrapsTest, FsRstSetTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRstGenTrapType}, NULL, FsRstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRstErrTrapType}, NULL, FsRstErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRstPortTrapIndex}, GetNextIndexFsRstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortMigrationType}, GetNextIndexFsRstPortTrapNotificationTable, FsRstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPktErrType}, GetNextIndexFsRstPortTrapNotificationTable, FsRstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPktErrVal}, GetNextIndexFsRstPortTrapNotificationTable, FsRstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsRstPortRoleType}, GetNextIndexFsRstPortTrapNotificationTable, FsRstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsRstOldRoleType}, GetNextIndexFsRstPortTrapNotificationTable, FsRstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},
};
tMibData fsrstEntry = { 81, fsrstMibEntry };

#endif /* _FSRSTDB_H */

