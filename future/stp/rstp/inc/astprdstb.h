/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astprdstb.h,v 1.29 2012/11/02 11:59:19 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for AST module.   
 *
 ******************************************************************/
#ifndef AST_RD_STB_H
#define AST_RD_STB_H
#include "asthdrs.h"
INT4 AstRedRmInit(VOID);
INT4 AstRedRegisterWithRM (VOID);
INT4 AstRedDeRegisterWithRM (VOID);

INT4 AstRedSelectContext (UINT4 u4ContextId);
VOID AstRedReleaseContext (VOID);

/* For Active */
INT4 AstRedSendUpdate(VOID);
INT4 AstRedClearPduOnActive(UINT2);
INT4 AstRedClearAllSyncUpData(VOID);
INT4 AstRedClearAllPduOnActive (VOID);
INT4 AstRedSyncUpPdu (UINT2, tAstBpdu *, UINT2);

/* For Standby */
INT4 AstRedSendSyncMessages (UINT2, UINT2, UINT1, UINT1);
VOID AstRedDumpSyncedUpOutputs (tCliHandle);
VOID RedDumpSyncedUpData(tCliHandle);
VOID RedDumpSyncedUpPdus (tCliHandle);
VOID AstRedClearSyncUpDataOnPort (UINT2 u2PortIfIndex);
INT4 AstRedInitOperTimes (UINT2 u2InstanceId, UINT2 u2PortIfIndex);
VOID AstRedHandleRcvdInfoWhileTmrExp (tAstPerStPortInfo * pPerStPortInfo);
/* For External APIs */
INT4 AstRedGetPortState(UINT2);
INT4 AstRedGetOperEdgeStatus(UINT2);

/* HITLESS RESTART */
VOID AstRedHRProcStdyStPktReq (VOID);
VOID AstRedHRSetSSPTmrValInCxt (UINT4 u4ContextId);
VOID AstRedHRRestartTmrForSSP (tAstPortEntry *pAstPortEntry);
INT1 AstRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER *pPkt, UINT4 u4PktLen, UINT2 u2Port,
        UINT4 u4TimeOut);
INT1 AstRedHRSendStdyStTailMsg (VOID);
UINT1 AstRedGetHRFlag (VOID);

typedef UINT4  tAstRedStates;
#define RED_AST_IDLE                             RM_INIT
#define RED_AST_ACTIVE                           RM_ACTIVE
#define RED_AST_STANDBY                          RM_STANDBY
#define RED_AST_FORCE_SWITCHOVER_INPROGRESS      4
#define RED_AST_SHUT_START_INPROGRESS            5


typedef enum {
   RED_AST_PORT_INFO =1,
   RED_AST_TIMES,
   RED_AST_PDU,
   RED_MST_PDU,
   RED_PVRST_PDU,
   RED_AST_OPER_STATUS,
   RED_AST_ALL_TIMES,
   RED_AST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_AST_CLEAR_ALL_SYNCUP_DATA,
   RED_MST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_PVRST_CLEAR_SYNCUP_DATA_ON_PORT,
   RED_AST_BULK_REQ,
   RED_AST_BULK_UPD_TAIL_MSG,
   RED_AST_CALLBACK_SUCCESS,
   RED_AST_CALLBACK_FAILURE
}tAstRmMsgTypes;

/* AST-RED-STRUCTURES USED FOR ONLY FOR MEMORY ALLOCATION */
/* START */

typedef struct _tAstRedContextInfo tAstRedContextInfo;

struct _tAstRedContextInfo
{
    UINT1 au1Dummy[4];
};

typedef struct AstRedPortInfoArray {
    UINT1 au1Dummy[4];
}tAstRedPortInfoArray;

typedef struct AstRedCvlanPortInfoArray {
    UINT1 au1Dummy[4];
}tAstRedCvlanPortInfoArray;

typedef struct AstRedPerPortInstInfoArray {
    UINT1 au1Dummy[4];
}tAstRedPerPortInstInfoArray;
typedef struct _tAstRedPerInstPortInfo {
    UINT1 au1Dummy[4];
}tAstRedPerPortInstInfo;

typedef struct _tAstRedPortInfo {
    UINT1 au1Dummy[4];
}tAstRedPortInfo;

/* END */
/* Message types Length*/

#define   RED_AST_PORT_INFO_LEN   9 
#define   RED_AST_TIMES_LEN      12  
#define   RED_AST_PDU_LEN        (sizeof(tAstBpdu) + 7) 
#define   RED_AST_CIST_BPDU_LEN   103 
#define   RED_AST_MSTI_BPDU_LEN   16 
#define   RED_AST_OPER_LEN        8 
#define   RED_AST_ALL_TIMES_LEN  39
#define   RED_AST_ALL_TIMES_INST_ZERO_LEN 27
#define   RED_AST_CLEAR_SYNCUP_DATA_ON_PORT_LEN 5
#define   RED_AST_CLEAR_ALL_SYNCUP_DATA_LEN  3 
#define   AST_MAX_RM_BUF_SIZE    1500 
#define   AST_RED_TIMER_TYPE_INVALID 100

#define AST_RED_TIMER_STOPPED    0x80
#define AST_RED_TIMER_EXPIRED    0x40
#define AST_RED_TIMER_STARTED    0x20

#define RED_AST_GET_FIRST        0
#define MST_BPDU_RST_LENGTH   36
#define MST_BPDU_STP_LENGTH   35

#define   AST_ADMIN_STATUS   (AST_CURR_CONTEXT_INFO ())->u1ProtocolAdminStatus
#define  AST_GBL_BPDUGUARD_STATUS (AST_CURR_CONTEXT_INFO ())->u4GblBpduGuardStatus

#define   AST_GET_OPER_P2P(PortIfIndex)  \
              AST_GET_PORTENTRY(PortIfIndex)->bOperEdgePort

#define   AST_GET_OPER_EDGE(PortIfIndex)  \
              AST_GET_PORTENTRY(PortIfIndex)->bOperPointToPoint



#define AST_NUM_STANDBY_NODES() 0

#define MST_CLEAR_INSTANCE(u2InstanceId,u2PortIfIndex)
#define PVRST_CLEAR_PDU(u2PortIfIndex)


#define AST_NODE_STATUS()  \
            RED_AST_ACTIVE 
                                         
#define AST_IS_NP_PROGRAMMING_ALLOWED()  AST_TRUE
#define AST_IS_STANDBY_UP()  AST_FALSE

#define AST_CLEAR_PDU(u2PortIfIndex)  

#define AST_RED_SYNC_FLAG(u2InstanceId) \
      ((AST_GET_PERST_INFO (u2InstanceId))->u1InstSyncFlag)
#define AST_RED_SET_SYNC_FLAG(u2InstanceId) \
          (AST_GET_PERST_INFO (u2InstanceId)->u1InstSyncFlag=OSIX_TRUE)

#endif
