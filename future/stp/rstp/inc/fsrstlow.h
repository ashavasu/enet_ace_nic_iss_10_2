/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrstlow.h,v 1.21 2017/09/12 14:08:12 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _FSRSTLOW_H
#define _FSRSTLOW_H
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRstSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsRstModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRstTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsRstDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsRstRstpUpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRstRstpDownCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRstBufferFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRstMemAllocFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRstNewRootIdCount ARG_LIST((UINT4 *));

INT1
nmhGetFsRstPortRoleSelSmState ARG_LIST((INT4 *));

INT1
nmhGetFsRstOldDesignatedRoot ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsRstStpPerfStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRstSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsRstModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRstTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsRstDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsRstStpPerfStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRstSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstStpPerfStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRstSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRstModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRstTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRstDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRstStpPerfStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRstPortExtTable. */
INT1
nmhValidateIndexInstanceFsRstPortExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRstPortExtTable  */

INT1
nmhGetFirstIndexFsRstPortExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRstPortExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRstPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortInfoSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortMigSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRoleTransSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortStateTransSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortTopoChSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortTxSmState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortRxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortRxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortInvalidRstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortProtocolMigrationCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortEffectivePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortAutoEdge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRestrictedRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRestrictedTCN ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortPseudoRootId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRstPortIsL2Gp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortLoopGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortStateChangeTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstRootInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1 
nmhGetFsRstPortBpduGuardAction(INT4 i4FsRstPort , INT4 *pi4RetValFsRstPortBpduGuardAction);

INT1 
nmhSetFsRstPortBpduGuardAction(INT4 i4FsRstPort , INT4 i4SetValFsRstPortBpduGuardAction);

INT1 
nmhTestv2FsRstPortBpduGuardAction(UINT4 *pu4ErrorCode ,
                INT4 i4FsRstPort , INT4 i4TestValFsRstPortBpduGuardAction);

INT1
nmhGetFsRstPortErrorRecovery ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortStpModeDot1wEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortTCDetectedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTCReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTCDetectedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortTCReceivedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortProposalPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortProposalPktsRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortProposalPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortProposalPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortAgreementPktSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortAgreementPktRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortAgreementPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortAgreementPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRstPortOldPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortLoopInconsistentState ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRstPortAutoEdge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortRestrictedRole ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortRestrictedTCN ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortPseudoRootId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRstPortIsL2Gp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortRootGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortErrorRecovery ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRstPortStpModeDot1wEnabled ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRstPortAutoEdge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortRestrictedRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortRestrictedTCN ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRstPortIsL2Gp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortErrorRecovery ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRstPortStpModeDot1wEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRstPortExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRstDynamicPathcostCalculation ARG_LIST((INT4 *));

INT1
nmhGetFsRstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 *));

INT1
nmhGetFsRstRcvdEvent ARG_LIST((INT4 *));

INT1
nmhGetFsRstRcvdEventSubType ARG_LIST((INT4 *));

INT1
nmhGetFsRstRcvdEventTimeStamp ARG_LIST((UINT4 *));

INT1
nmhGetFsRstRcvdPortStateChangeTimeStamp ARG_LIST((UINT4 *));

INT1
nmhGetFsRstBpduGuard ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRstDynamicPathcostCalculation ARG_LIST((INT4 ));

INT1
nmhSetFsRstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ));

INT1
nmhSetFsRstBpduGuard ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRstDynamicPathcostCalculation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRstBpduGuard ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRstDynamicPathcostCalculation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsRstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRstBpduGuard ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRstSetTraps ARG_LIST((INT4 *));

INT1
nmhGetFsRstGenTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsRstErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRstSetTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRstSetTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRstSetTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsRstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsRstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPktErrVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstPortRoleType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRstOldPortRoleType ARG_LIST((INT4 ,INT4 *));


#endif/*_FSRSTLOW_H*/
