
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stpclipt.h,v 1.55 2017/10/30 14:29:18 siva Exp $
*
* Description:  Function Prototype for CLI RSTP/MSTP Commands
*********************************************************************/

#ifndef __STPCLIPROT_H__
#define __STPCLIPROT_H__

INT4 RstCliSetSystemShut (tCliHandle);

INT4 RstCliSetSystemCtrl  (tCliHandle );

INT4 RstCliSetModStatus (tCliHandle CliHandle , UINT4 u4RstModStatus);

INT4 RstCliSetFwdTime(tCliHandle CliHandle,UINT4 u4FwdTime);

INT4 RstCliSetHelloTime(tCliHandle CliHandle,UINT4 u4HelloTime);

INT4 RstCliSetMaxAge(tCliHandle CliHandle,UINT4 u4MaxAge);

INT4 RstCliSetBridgeTxHoldCount(tCliHandle CliHandle,UINT4  u4TxHoldCount);

INT4 RstCliSetBrgPriority(tCliHandle CliHandle,UINT4 u4BrgPriority);

INT4 RstCliSetBrgVersion(tCliHandle CliHandle,UINT4 u4Version);

INT4 MstCliSetMaxInstanceNumber(tCliHandle CliHandle,INT4 i4MaxInstanceNumber);

INT4 MstCliSetInstanceMapping(tCliHandle CliHandle,UINT4 u4ValFsMstInstanceMap);

INT4 RstCliSetPathCostCalculation(tCliHandle CliHandle, UINT4 u4PathCostCalc);

INT4 RstCliSetLaggPathCostCalculation(tCliHandle CliHandle, UINT4 u4PathCostCalc);

INT4 RstCliResetBridgeCounters (tCliHandle CliHandle);

INT4 RstSetPortProp(tCliHandle CliHandle,UINT4 ,UINT4,UINT4 );

INT4 RstCliSetProtocolMigration (tCliHandle );

INT4 RstCliSetPortProtocolMigration (tCliHandle, UINT4 );

INT4 RstSetDebug(tCliHandle , UINT4,UINT1);

INT4 RstSetGlobalDebug (tCliHandle , UINT1);

INT4 RstSetPortAutoEdge (tCliHandle , UINT4 , UINT4 );
INT4 MstSetPortAutoEdge (tCliHandle , UINT4 , UINT4 );

VOID StpCliPrintRstPortRole (tCliHandle CliHandle, INT4 i4Val);
INT4 PvrstSetPortAutoEdge (tCliHandle , UINT4 , UINT4 );
/* RSTP SHOW COMMANDS*/
INT4  RstCliShowSpanningTreeMethod(tCliHandle);

INT4 RstCliShowSpanningTreeDetail(tCliHandle CliHandle, UINT4 , UINT4);

VOID RstCliDisplayBridgeDetails(tCliHandle, UINT4 );

VOID RstCliDisplayPortDetails(tCliHandle, UINT4, INT4);

UINT4 RstCliPortRxBpduCount(INT4 i4NextPort);

UINT4 RstCliPortTxBpduCount(INT4 i4NextPort);

INT4 RstCliShowSpanningTreeRoot(tCliHandle ,UINT4, UINT4 );

INT4 RstCliShowSpanningTreeBridge(tCliHandle ,UINT4 , UINT4 );

INT4 RstCliDisplayInterfaceDetails(tCliHandle , UINT4, UINT4 ,UINT4 );

INT4 RstCliShowSpanningTree(tCliHandle, UINT4, UINT4 );

VOID RstCliShowRootBridgeInfo(tCliHandle, UINT4 );

VOID RstCliShowRootBridgeTable(tCliHandle, UINT4 );

INT4 RstCliDisplayBlockedPorts(tCliHandle, UINT4 );

INT4 RstCliShowSummary (tCliHandle , UINT4 );

VOID StpCliDisplayPortRole(tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val);

INT4 RstpShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Module); 
INT4 RstpShowRunningConfigScalars(tCliHandle, UINT4);

VOID RstpShowRunningConfigInterfaceDetails(tCliHandle, INT4, UINT1*);;

VOID RstpShowRunningConfigInterface(tCliHandle, UINT4);
    
INT4 RstCliSetPortRestrictedRole (tCliHandle CliHandle, UINT2 u2LocalPort, 
                                    INT4 i4Value);
INT4 RstCliSetPortRootGuard (tCliHandle CliHandle, UINT2 u2LocalPort,
                                    INT4 i4Value);
INT4 RstCliSetPortRestrictedTcn (tCliHandle CliHandle, UINT2 u2LocalPort, 
                                   INT4 i4Value);
INT4 MstCliSetPortRestrictedRole (tCliHandle CliHandle, UINT2 u2LocalPort, 
                                    INT4 i4Value);
INT4 MstCliSetPortRestrictedTcn (tCliHandle CliHandle, UINT2 u2LocalPort, 
                                   INT4 i4Value);
INT4 MstCliSetPortRootGuard (tCliHandle CliHandle, UINT2 u2LocalPort,
                                    INT4 i4Value);
    
INT4 RstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                                  INT4 i4GlobalModStatus, INT4 i4Version);
INT4 MstCliPrintStpModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                                  INT4 i4GlobalModStatus, INT4 i4Version);

VOID CliDisplayTrace(tCliHandle,UINT4 );

VOID StpCliDisplayPortState(tCliHandle CliHandle, INT4 i4Val, 
                            INT4 i4PortStateStatus);

INT4 RstConfigPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum,
                            tMacAddr MacAddress, UINT2 u2Priority);

INT4 RstSetPortL2gp (tCliHandle CliHandle, UINT4 u4PortNum,
                     INT4 i4EnableL2gp);

INT4 RstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduRx);

INT4 RstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);

INT4 RstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);

INT4
RstCliSetPortDot1WEnable (tCliHandle CliHandle, UINT2 u2LocalPort, 
                          INT4 i4Value);

INT4 RstCliShowSpanningTreePortL2gp (tCliHandle CliHandle,
                                     UINT4 u4ContextId, UINT4 u4Index);

INT4 RstResetPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum);

INT4 RstCliSetPortLoopGuard (tCliHandle CliHandle, UINT2 u2LocalPort,
                             INT4 i4Value);
INT4 MstCliSetPortLoopGuard (tCliHandle CliHandle, UINT2 u2LocalPort,
                             INT4 i4Value);
INT4 RstCliResetPortCounters (tCliHandle CliHandle,
         UINT4 u4IfIndex);
INT4
RstCliShowPerformanceData (tCliHandle CliHandle);

INT4
RstCliShowPerformanceDataIface (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4IfIndex);

INT4 RstBpduguardEnable (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,UINT1 u1BpduGuardAction);
INT4 RstBpduguardDisable(tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value);
INT4 RstCliSetPortErrorRecovery (tCliHandle CliHandle,  UINT4 u4PortNum,UINT4 u4ErrorRecovery);

VOID
RstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index);

#ifdef MSTP_WANTED

INT4 MstCliSetSystemShut (tCliHandle);

INT4 MstCliSetSystemCtrl (tCliHandle );

INT4 MstCliSetModStatus (tCliHandle CliHandle , UINT4 u4MstModStatus);

INT4 MstCliSetFwdTime(tCliHandle CliHandle,UINT4 u4FwdTime);

INT4 MstCliSetMaxAge(tCliHandle CliHandle,UINT4 u4MaxAge);

INT4 MstCliSetHelloTime(tCliHandle CliHandle,UINT4 u4HelloTime);

INT4 MstCliSetBridgeTxHoldCount(tCliHandle CliHandle,UINT4  u4TxHoldCount);

INT4 MstCliSetMaxHops(tCliHandle CliHandle,UINT4  u4MaxHops);

INT4 MstCliSetBrgPriority(tCliHandle,UINT4,UINT4);

INT4 MstCliSetRootPriority (tCliHandle,UINT4,UINT4);

INT4 MstCliSetBrgVersion(tCliHandle CliHandle,UINT4 u4Version);

INT4 MstCliSetPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc);

INT4 MstCliSetLaggPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc);

INT4 MstSetRegnName(tCliHandle CliHandle,UINT1 *u1RegionName);

INT4 MstCliSetRegionVersion(tCliHandle CliHandle,UINT4 u4RegionVersion);

INT4  MstCliSetVlanInstanceMapping(tCliHandle,UINT4 u4InstanceId,UINT1 *pau1MstVlanList, UINT1 u1Action);

INT4 MstCliSetPortHelloTime (tCliHandle CliHandle,  UINT4 u4PortNum,UINT4 u4HelloTime);

INT4 MstCliSetPortErrorRecovery (tCliHandle CliHandle,  UINT4 u4PortNum,UINT4 u4ErrorRecovery);

INT4 MstCistSetPortProp (tCliHandle CliHandle,UINT4 ,UINT4,UINT4);

INT4  MstSetPortProperties (tCliHandle ,UINT4 ,UINT4, UINT4,UINT4);

INT4 MstCliSetProtocolMigration (tCliHandle);

INT4 MstCliSetPortProtocolMigration (tCliHandle, UINT4);

INT4 MstSetDebug(tCliHandle , UINT4,UINT1);

INT4 MstCliResetBridgeCounters(tCliHandle CliHandle);

INT4 MstCliResetPortCounters(tCliHandle CliHandle,
        UINT4 u4IfIndex);

INT4
MstCliResetPerStPortCounters (tCliHandle CliHandle,
         UINT2 u2InstIndex,
         UINT4 u4IfIndex);

INT4
MstCliResetPerStBridgeCounters (tCliHandle CliHandle,
        UINT2 u2InstIndex);
/* MSTP SHOW FUNCTIONS  (CIST)*/

INT4  MstCliShowSpanningTreeMethod(tCliHandle CliHandle);

INT4 MstCliShowSpanningTreeDetail(tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Active);

UINT4 MstCliPortTxBpduCount(INT4 i4NextPort);

UINT4 MstCliPortRxBpduCount(INT4 i4NextPort);

VOID
MstCliPortTxRxBpduCount (INT4 i4Port, UINT4 *pu4TxBpduCount, UINT4 *pu4RxBpduCount);

INT4 MstCliShowSpanningTreeRoot(tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Val);

INT4 MstCliShowSpanningTreeMstiRoot (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Val);

INT4 MstCliShowSpanningTreeBridge(tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Val);

VOID MstCliShowRootBridgeInfo(tCliHandle CliHandle, UINT4 u4ContextId);

VOID MstCliShowMstiRootBridgeInfo(tCliHandle CliHandle, UINT4 u4ContextId,  INT4 i4Inst);

VOID MstCliDisplayBridgeDetails(tCliHandle CliHandle, UINT4 u4ContextId,  UINT1 u1Detail);


VOID MstCliDisplayPortDetails(tCliHandle CliHandle, UINT4 u4ContextId,  INT4 i4NextPort);

VOID MstCliDisplayMstiPortDetails(tCliHandle CliHandle, UINT4 u4ContextId,  INT4 i4Instance, INT4 i4NextPort);

INT4 MstCliShowSpanningTree(tCliHandle CliHandle, UINT4 u4ContextId,  UINT4);

INT4 MstCliDisplayBlockedPorts(tCliHandle CliHandle, UINT4 u4ContextId);

INT4 MstCliDisplayInterfaceDetails(tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Index,UINT4 u4Val);

VOID MstDisplayPortType(tCliHandle CliHandle,INT4 i4LinkType);
   
VOID MstCliShowRootBridgeTable (tCliHandle CliHandle, UINT4 u4ContextId);
   
INT4
MstCliShowPerformanceDataInstIface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                    UINT4 u4Instance);

INT4 MstpShowRunningConfig(tCliHandle CliHandle, UINT4, UINT4);
INT4 MstpShowRunningConfigScalars(tCliHandle, UINT4, UINT1*);
VOID MstpShowRunningConfigInterface(tCliHandle, UINT4);
VOID MstpShowRunningConfigInstInterface(tCliHandle, INT4, UINT1*);
VOID MstpShowRunningConfigInterfaceDetails(tCliHandle, INT4, UINT1*);
VOID MstpShowRunningConfigInstInterfaceDetails(tCliHandle,INT4,INT4, UINT1*);
VOID MstpShowRunningConfigTables(tCliHandle, UINT4);

VOID
MstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index);

/* MSTP SHOW FUNCTIONS - MST INSTANCE SPECIFIC*/

VOID
MstDisplayConfiguredBridgeValues (tCliHandle CliHandle, INT4 i4MaxAge, INT4 i4FwdDelay, INT4 i4MaxHops);

VOID StpCliDisplayMstPortRole (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val);

VOID
MstDisplayOperBridgeValues (tCliHandle CliHandle, INT4 i4MaxAge, INT4 i4FwdDelay);

INT4 MstShowSpanningTreeMstConfig(tCliHandle, UINT4 u4ContextId);

INT4 MstShowSpanningTreeMstDetail(tCliHandle, UINT4 u4ContextId);

INT4 MstShowSpanningTreeMstInstDetail (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Instance, UINT4 u4Index);

INT4 MstShowSpanningTreeMstInstIntfDetail (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Instance, UINT4 u4Index);

INT4 MstShowSpanningTreeMstInterfaceDetails(tCliHandle, UINT4 u4ContextId, UINT4,UINT4);

INT4 MstShowSpanningTreeMstInterfaceStats(tCliHandle, UINT4 u4ContextId, UINT4, UINT4);

INT4 MstShowSpanningTreeMst(tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Instance, UINT1 u1Detail);

INT4 MstFillInstanceVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Instance, INT4 * pi4CommaCount);

tAstBoolean MstDisplayOctetToVlan (tCliHandle CliHandle, UINT1 * pOctet, 
                                   UINT4 u4OctetLen, INT4 i4Flag, 
                                   tAstBoolean bSeprtr, INT4 *);

UINT4 MstCistPortTxBpduCount (INT4 i4Port);

UINT4 MstCistPortRxBpduCount (INT4 i4NextPort);

INT1 MstFillPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4Index, UINT4 u4Instance);

VOID MstCliPrintVlanList (tCliHandle CliHandle, INT4 i4CommaCount, UINT2 VlanId);

INT4 MstCliShowSummary (tCliHandle CliHandle, UINT4 u4ContextId);

INT4 MstShowSpanningTreeMstInterfaceHelloTime (tCliHandle, UINT4 u4ContextId,  UINT4, UINT4);

INT4
AstGetNextMstiPortTableIndex (UINT4 u4ContextId, INT4 i4IfIndex,
                              INT4 *pi4NextIfIndex, 
                              INT4 i4Instance, INT4 *pi4NextInstance);

VOID MstCliCheckVlanInstMap (tCliHandle, UINT4 u4ContextId,
                             UINT4 u4InstanceId,
                             tSNMP_OCTET_STRING_TYPE * pMstVlanList);

INT4 MstConfigPseudoRootId (tCliHandle CliHandle, UINT4 u4PortNum,
                            UINT4 u4InstanceId, tMacAddr MacAddress,
                            UINT2 u2Priority);

INT4 MstSetPortL2gp (tCliHandle CliHandle, UINT4 u4PortNum,
                     INT4 i4EnableL2gp);

INT4 MstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduRx);

INT4 MstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);

INT4 MstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);

INT4 MstCliShowSpanningTreePortL2gp (tCliHandle CliHandle, UINT4 u4ContextId,
                                     UINT4 u4Index);
INT4 MstResetPseudoRootId (tCliHandle CliHandle, UINT4 u4InstanceId,
                           UINT4 u4PortNum);

INT4 MstBpduguardEnable(tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,UINT1 u1BpduGuardAction);
INT4 MstBpduguardDisable(tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value);
#endif
VOID IssSpanningTreeShowDebugging (tCliHandle);
/* extern  INT1 nmhGetDot1dBasePortOperStatus(INT4 i4NextPort, INT4 * pi4RetVal ); */

#ifdef PVRST_WANTED
INT4 PvrstCliSetSystemShut (tCliHandle CliHandle);
INT4 PvrstCliSetSystemCtrl (tCliHandle CliHandle);
INT4 PvrstCliSetModStatus (tCliHandle CliHandle , UINT4 u4PvrstModStatus);
VOID PvrstResetCounters (VOID);
INT4 PvrstCliSetHelloTime (tCliHandle CliHandle,UINT4 u4InstanceId, 
       UINT4 u4HelloTime);
INT4 PvrstCliSetForwardDelay (tCliHandle CliHandle,UINT4 u4InstanceId,
       UINT4 u4FwdTime);
INT4 PvrstCliSetMaxAge (tCliHandle CliHandle,UINT4 u4InstanceId, 
       UINT4 u4MaxAge);
INT4 PvrstCliSetHoldCount (tCliHandle CliHandle,UINT4 u4InstanceId, 
       UINT4 u4HoldCount);
INT4 PvrstCliSetBrgPriority (tCliHandle CliHandle, UINT4 u4InstanceId,
       UINT4 u4Priority);
INT4 PvrstCliShowSpanningTreeDetail (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId, 
                                 UINT4 u4Type);
VOID
PvrstCliDisplayBridgeDetails (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId);
INT4
PvrstCliShowSpanningTree (tCliHandle CliHandle, UINT4 u4ContextId,UINT4 u4InstanceId, UINT4 u4Active);
INT4
PvrstCliDisplayBlockedPorts (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId);
INT4
PvrstCliShowSpanningTreeMethod (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
PvrstCliShowSummary (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId);
INT4
PvrstCliShowSpanningTreeBrgDet (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId,
                                UINT4 u4Val);
INT4
PvrstCliShowSpanningTreeRootDet (tCliHandle CliHandle, UINT4 u4ContextId,UINT4 u4InstanceId,
                                 UINT4 u4Val);
VOID
PvrstCliShowRootBridgeInfo (tCliHandle CliHandle, UINT4 u4ContextId,UINT4 u4InstanceId);
VOID
PvrstCliShowRootBridgeTable (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId);
INT4
PvrstCliDisplayInterfaceDetails (tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4InstanceId,
                                 UINT4 u4Index, UINT4 u4Val);
VOID
PvrstCliDisplayPortDetails (tCliHandle CliHandle, UINT4 u4ContextId,UINT4 u4InstanceId, 
                            INT4 i4NextPort);
UINT4
PvrstCliPortTxBpduCount (UINT4 u4InstanceId, INT4 i4NextPort);
UINT4
PvrstCliPortRxBpduCount (UINT4 u4InstanceId, INT4 i4NextPort);
VOID
PvrstCliDisplayPortRole (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Val);
VOID
PvrstCliDisplayPortState (tCliHandle CliHandle, INT4 i4Val);
INT4 PvrstBpduguardDisable(tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value);
INT4 PvrstBpduguardEnable(tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value,UINT1 u1BpduGuardAction);
INT4 PvrstGuardRoot(tCliHandle CliHandle, INT4 i4Index);
INT4 PvrstNoGuard(tCliHandle CliHandle,INT4 i4Index);

INT4 PvrstCliSetPortLoopGuard (tCliHandle CliHandle, UINT2 u2PortNum,
        INT4 i4LoopGuardStatus);
VOID
PvrstCliShowLoopIncState (tCliHandle CliHandle,UINT4 u4ContextId,UINT4 u4PortNum,
        UINT4 u4InstanceId);
VOID
PvrstCliShowLoopGuardState (tCliHandle CliHandle,UINT4 u4ContextId,
       UINT4 u4PortNum,UINT4 u4InstanceId);

INT4 PvrstEncapDot1q(tCliHandle CliHandle,INT4 i4Index);
INT4 PvrstEncapISL(tCliHandle CliHandle,INT4 i4Index);
INT4
PvrstSetPortProp (tCliHandle CliHandle, UINT4 u4PortNum, UINT4 u4CmdType,
                UINT4 u4Val);
INT4 PvrstInstStatusOnPort(tCliHandle CliHandle, UINT4 u4Index,
                           UINT4 u4Instance, UINT4 u4Value);
INT4 PvrstVlanPortPriority(tCliHandle CliHandle, UINT4 u4Index,
                           UINT4 u4Instance, UINT4 u4Value);
INT4 PvrstVlanCost(tCliHandle CliHandle, UINT4 u4Index,
                           UINT4 u4Instance, UINT4 u4Value);
INT4
PvrstCliSetPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc);
INT4
PvrstCliSetLaggPathCostCalculation (tCliHandle CliHandle, UINT4 u4PathcostCalc);
INT4
PvrstSetDebug (tCliHandle CliHandle, UINT4 u4RstDbg, UINT1 u1Action);
INT4
PvrstShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId , UINT4 u4Module);
INT4
PvrstShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId );
VOID
PvrstShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId );
VOID
PvrstShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index, UINT1* pu1Flag);
VOID
PvrstShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId );
VOID
PvrstShowRunningConfigInstInterface (tCliHandle CliHandle, INT4 i4Index, UINT1* pu1Flag);
VOID
PvrstShowRunningConfigInstInterfaceDetails (tCliHandle CliHandle,
                                           INT4 i4InstPort, INT4 i4Inst, UINT1*);
VOID
PvrstDisplayInterfacePortFast (tCliHandle CliHandle,UINT4 u4ContextId, INT4 i4Index);
VOID
PvrstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index);
VOID
PvrstDisplayInterfaceEncapType (tCliHandle CliHandle,UINT4 u4ContextId, INT4 i4Index);
VOID
PvrstDisplayInterfaceRootGuard (tCliHandle CliHandle,UINT4 u4ContextId, INT4 i4Index);

VOID
PvrstDisplayPortInconsistency (tCliHandle CliHandle, INT4 i4Index);

INT4 PvrstCliSetBrgVersion(tCliHandle CliHandle,UINT4 u4Version);
INT4 PvrstCliSetProtocolMigration (tCliHandle CliHandle) ;
INT4 PvrstCliSetPortProtocolMigration (UINT4 u4IfIndex);
INT4 PvrstCliSetFlushInterval (tCliHandle CliHandle,UINT4 u4Value);
INT4 PvrstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Vlan,
                                         UINT4 u4Value);


#endif


UINT2 AstCliGetPortIdFromOctetList (tSNMP_OCTET_STRING_TYPE PortId);

INT4 AstCliGetPortInfoHelloTime (INT4 i4NextPort, INT4 *pi4HelloTime);
UINT4 AstCliGetBridgeMode (UINT4 u4ContextId);
INT4 MstCliSetFlushInterval (tCliHandle CliHandle, UINT4 u4FlushInterval);
INT4 MstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Instance,
         UINT4 u4Threshold);

INT4 RstSetCVlanDebug (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4RstDbg, 
        UINT1 u1Action);
INT4 RstCliSetFlushInterval (tCliHandle CliHandle, UINT4 u4FlushInterval);
INT4 RstCliSetFlushIndThreshold (tCliHandle CliHandle, UINT4 u4Threshold);

INT4 RstCliSetForwardDelayAltPort (tCliHandle CliHandle,UINT4 u4RstOptStatus);

INT4 RstCliSetBpduGuardStatus (tCliHandle CliHandle , UINT4 u4GblBpduGuardStatus);

INT4 MstCliSetBpduGuardStatus (tCliHandle CliHandle , UINT4 u4GblBpduGuardStatus);

INT4 PvrstCliSetBpduGuardStatus (tCliHandle CliHandle , UINT4 u4GblBpduGuardStatus);

INT4
MstCliSetStpPerformanceStatus (tCliHandle CliHandle, UINT4 u4GblStpPerformanceStatus);

INT4
RstCliSetStpPerformanceStatus (tCliHandle CliHandle, UINT4 u4GblStpPerformanceStatus);

INT4 PvrstSetBpduRx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduRx);

INT4 PvrstSetBpduTx (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);

INT4 PvrstSetBpduFilter (tCliHandle CliHandle, UINT4 u4PortNum,
                   INT4 i4EnableBpduTx);
VOID
RstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index);

VOID
MstDisplayInterfaceBpduGuard (tCliHandle CliHandle, INT4 i4Index);
# endif
