/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astppmsm.c,v 1.18 2017/10/30 14:29:19 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Port Protocol Migration State Machine.
 *
 *******************************************************************/

#include "asthdrs.h"

/*****************************************************************************/
/* Function Name      : RstPortMigrationMachine                              */
/*                                                                           */
/* Description        : This is the main routine for the Port Protocol       */
/*                      Migration State Machine.                             */
/*                      This routine calls the action routine for the event- */
/*                      state combination.                                   */
/*                                                                           */
/* Input(s)           : u2Event - The event that has caused this             */
/*                                State Machine to be called.                */
/*                      u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPortMigrationMachine (UINT2 u2Event, UINT2 u2PortNum)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT2               u2State;
    INT4                i4RetVal = 0;

    pAstPortEntry = AST_GET_PORTENTRY (u2PortNum);
    if (NULL == pAstPortEntry)
    {
        AST_DBG_ARG1 (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                      "PMSM: Event %s: Invalid parameter passed to event handler routine\n",
                      gaaau1AstSemEvent[AST_PMSM][u2Event]);
        return RST_FAILURE;
    }
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    u2State = (UINT2) pCommPortInfo->u1PmigSmState;

    AST_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                  "PMSM: Port %s: Protocol Migr Machine Called with Event: %s,State: %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  gaaau1AstSemEvent[AST_PMSM][u2Event],
                  gaaau1AstSemState[AST_PMSM][u2State]);

    AST_DBG_ARG3 (AST_PMSM_DBG,
                  "PMSM: Port %s: Protocol Migr Machine Called with Event: %s,State: %s\n",
                  AST_GET_IFINDEX_STR (u2PortNum),
                  gaaau1AstSemEvent[AST_PMSM][u2Event],
                  gaaau1AstSemState[AST_PMSM][u2State]);

    if (((AST_CURR_CONTEXT_INFO ())->bBegin == RST_TRUE) &&
        (u2Event != RST_PMIGSM_EV_BEGIN))
    {
        /* Disable all transitons when BEGIN is asserted */
        AST_DBG (AST_PMSM_DBG,
                 "PMSM: Ignoring event since BEGIN is asserted\n");
        return RST_SUCCESS;
    }

    if (RST_PORT_MIG_MACHINE[u2Event][u2State].pAction == NULL)
    {
        AST_DBG (AST_PMSM_DBG, "PMSM: No Operations to Perform\n");
        return RST_SUCCESS;
    }

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "Value of Force Version is %u\n", AST_FORCE_VERSION);
    i4RetVal = (*(RST_PORT_MIG_MACHINE[u2Event][u2State].pAction)) (u2PortNum);

    if (i4RetVal != RST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: Event routine returned failure !!!\n");
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmMakeCheckingRstp                            */
/*                                                                           */
/* Description        : This routine is called initially as well as when     */
/*                      an RSTP bpdu is received when port is in STP mode.   */
/*                      This routine changes the state machine state to      */
/*                      'CHECKING_RSTP' when the ForceVersion parameter is   */
/*                      greater than or equal to 2.                          */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPmigSmMakeCheckingRstp (UINT2 u2PortNum)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstBoolean         bTmpSendRst;
    UINT2               u2Duration = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    pCommPortInfo->bMCheck = RST_FALSE;

    bTmpSendRst = pCommPortInfo->bSendRstp;

    if (AST_FORCE_VERSION >= (UINT1) AST_VERSION_2)
    {
        pCommPortInfo->bSendRstp = RST_TRUE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "PMSM_CheckingRstp: Port %s: MCheck = FALSE SendRstp = TRUE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    else
    {
        pCommPortInfo->bSendRstp = RST_FALSE;

        AST_DBG_ARG1 (AST_SM_VAR_DBG,
                      "PMSM_CheckingRstp: Port %s: MCheck = SendRstp = FALSE\n",
                      AST_GET_IFINDEX_STR (u2PortNum));
    }
    /* This state can be reached by the following events -
     * 1) Protocol starts up - should NOT generate trap or increment count
     * 2) Force version changed - should NOT generate trap or increment count
     * 3) Rstp bpdu rcvd in STP mode - should generate trap and increment count
     * 4) Port disabled when in STP mode - should generate trap and increment 
     *                                     count
     * 5) MCheck is set when in STP mode - should generate trap and increment 
     *                                     count
     *
     * Cases 1 and 2 will have the BEGIN variable set.  
     * Since we maintain all port related counters even if the port is disabled
     * case 4 should be considered, since before disabling the port it operated
     * in STP mode but on enabling the port it operates in RSTP.
     * Hence, incrementing the Protocol Migration Count for cases 3, 4 and 5 
     * alone.
     */
    if (((AST_CURR_CONTEXT_INFO ())->bBegin != RST_TRUE) &&
        (bTmpSendRst != pCommPortInfo->bSendRstp))
    {
        AST_INCR_PROTOCOL_MIGRATION_COUNT (u2PortNum);
        AstProtocolMigrationTrap (u2PortNum, AST_FORCE_VERSION,
                                  AST_TRAP_SEND_RSTP,
                                  (INT1 *) AST_BRG_TRAPS_OID,
                                  AST_BRG_TRAPS_OID_LEN);
        UtlGetTimeStr (au1TimeStr);
        AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                          "Compatability change received for Port %s Node operating mode : %u Port operating mode : RSTP at %s\n",
                          AST_GET_IFINDEX_STR (u2PortNum), AST_FORCE_VERSION,
                          au1TimeStr);
    }

    pCommPortInfo->u1PmigSmState = (UINT1) RST_PMIGSM_STATE_CHECKING_RSTP;

    AST_DBG_ARG1 (AST_PMSM_DBG,
                  "PMSM: Port %s: Moved to state CHECKING_RSTP\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    if ((pPortInfo->u1EntryStatus == (UINT1) AST_PORT_OPER_UP) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE))
    {
        /* We assume that by the time this timer expires, BEGIN, if asserted, 
         * would have been cleared. Hence there is no seperate BEGIN_CLEARED
         * event for PMSM - it is handled when the mDelayWhile expires. */
        u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
        if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                           (UINT1) AST_TMR_TYPE_MDELAYWHILE,
                           (UINT2) u2Duration) != RST_SUCCESS)
        {
            AST_DBG (AST_PMSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                     "PMSM: AstStartTimer for MdWhileTmr FAILED!\n");
            return RST_FAILURE;
        }

        AST_DBG_ARG2 (AST_SM_VAR_DBG,
                      "PMSM_CheckingRstp: Port %s: mDelayWhile = %u\n",
                      AST_GET_IFINDEX_STR (u2PortNum), pBrgInfo->u1MigrateTime);

        if (pCommPortInfo->bSendRstp == RST_TRUE)
        {
            RstBrgDetectionMachine (RST_BRGDETSM_EV_SENDRSTP_SET, u2PortNum);
        }
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmMakeSelectingStp                            */
/*                                                                           */
/* Description        : This routine is called when a STP bpdu is received.  */
/*                      This routine changes the state machine state to      */
/*                      'SELECTING_STP'.                                     */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPmigSmMakeSelectingStp (UINT2 u2PortNum)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT2               u2Duration = AST_INIT_VAL;
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pCommPortInfo->bSendRstp = RST_FALSE;

    u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_MDELAYWHILE,
                       (UINT2) u2Duration) != RST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: AstStartTimer for MdWhileTmr FAILED!\n");
        return RST_FAILURE;
    }

    pCommPortInfo->u1PmigSmState = (UINT1) RST_PMIGSM_STATE_SELECTING_STP;

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_PMSM_DBG,
                  "PMSM_SelectedStp: Port %s: mDelayWhile = %u SendRstp = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum), pBrgInfo->u1MigrateTime);

    AST_INCR_PROTOCOL_MIGRATION_COUNT (u2PortNum);
    AstProtocolMigrationTrap (u2PortNum, AST_FORCE_VERSION, AST_TRAP_SEND_STP,
                              (INT1 *) AST_BRG_TRAPS_OID,
                              AST_BRG_TRAPS_OID_LEN);
    UtlGetTimeStr (au1TimeStr);
    AST_SYS_TRC_ARG3 (AST_CONTROL_PATH_TRC,
                      "Compatability change received for Port %s Node operating mode : %u Port operating mode : STP at %s \n ",
                      AST_GET_IFINDEX_STR (u2PortNum), AST_FORCE_VERSION,
                      au1TimeStr);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmMakeSensing                                 */
/*                                                                           */
/* Description        : This routine is called when the mDelayWhile expires. */
/*                      This routine changes the state machine state to      */
/*                      'SENSING'.                                           */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPmigSmMakeSensing (UINT2 u2PortNum)
{
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);

    pCommPortInfo->bRcvdRstp = RST_FALSE;
    pCommPortInfo->bRcvdStp = RST_FALSE;

    pCommPortInfo->u1PmigSmState = (UINT1) RST_PMIGSM_STATE_SENSING;

    AST_DBG_ARG1 (AST_SM_VAR_DBG | AST_PMSM_DBG,
                  "PMSM_Sensing: Port %s: RcvdRstp = RcvdStp = FALSE\n",
                  AST_GET_IFINDEX_STR (u2PortNum));

    RstPmigSmSenseMigration (u2PortNum);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmSenseMigration                              */
/*                                                                           */
/* Description        : This routine tries to sense whether to migrate from  */
/*                      one mode to the other based on the State Machine     */
/*                      variables and the latest BPDU received.              */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPmigSmSenseMigration (UINT2 u2PortNum)
{
    tAstPortEntry      *pPortInfo = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    pRstPortInfo =
        AST_GET_PERST_RST_PORT_INFO (u2PortNum, RST_DEFAULT_INSTANCE);

    if (((pCommPortInfo->bSendRstp == RST_FALSE) &&
         (pCommPortInfo->bRcvdRstp == RST_TRUE) &&
         (AST_FORCE_VERSION >= AST_VERSION_2))
        ||
        ((pPortInfo->u1EntryStatus == AST_DOWN) ||
         (pRstPortInfo->bPortEnabled == RST_FALSE))
        || (pCommPortInfo->bMCheck == RST_TRUE))
    {
        return (RstPmigSmMakeCheckingRstp (u2PortNum));
    }

    if ((pCommPortInfo->bSendRstp == RST_TRUE) &&
        (pCommPortInfo->bRcvdStp == RST_TRUE))
    {
        return (RstPmigSmMakeSelectingStp (u2PortNum));
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmStartMDelayWhile                            */
/*                                                                           */
/* Description        : This routine is called when the port is enabled to   */
/*                      start the mDelayWhile timer for the port.            */
/*                                                                           */
/* Input(s)             u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
RstPmigSmStartMDelayWhile (UINT2 u2PortNum)
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    UINT2               u2Duration = AST_INIT_VAL;

    pBrgInfo = AST_GET_BRGENTRY ();
    pPortInfo = AST_GET_PORTENTRY (u2PortNum);

    u2Duration = (UINT2) (pBrgInfo->u1MigrateTime * AST_CENTI_SECONDS);
    if (AstStartTimer ((VOID *) pPortInfo, RST_DEFAULT_INSTANCE,
                       (UINT1) AST_TMR_TYPE_MDELAYWHILE,
                       (UINT2) u2Duration) != RST_SUCCESS)
    {
        AST_DBG (AST_PMSM_DBG | AST_TMR_DBG | AST_ALL_FAILURE_DBG,
                 "PMSM: AstStartTimer for MdWhileTmr FAILED!\n");
        return RST_FAILURE;
    }

    AST_DBG_ARG2 (AST_SM_VAR_DBG | AST_PMSM_DBG,
                  "PMSM_CheckingRstp: Port %s: mDelayWhile = %u\n",
                  AST_GET_IFINDEX_STR (u2PortNum), pBrgInfo->u1MigrateTime);

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstPmigSmEventImpossible                             */
/*                                                                           */
/* Description        : This routine is called when the state machine        */
/*                      is called with an impossible Event/State combination.*/
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number for which this machine is    */
/*                                  called.                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_FAILURE                                          */
/*****************************************************************************/
INT4
RstPmigSmEventImpossible (UINT2 u2PortNum)
{
    AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
             "PMSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");
    AST_DBG (AST_PMSM_DBG | AST_ALL_FAILURE_DBG,
             "PMSM: IMPOSSIBLE EVENT/STATE Combination Occurred !!!\n");

    if (AstHandleImpossibleState (u2PortNum) != RST_SUCCESS)
    {
        AST_DBG_ARG1 (AST_PMSM_DBG | AST_ALL_FAILURE_DBG | AST_TMR_DBG,
                      "TMR: Port %s: Starting RESTART Timer Failed\n",
                      AST_GET_IFINDEX_STR (u2PortNum));

        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RstInitProtocolMigrationMachine                      */
/*                                                                           */
/* Description        : Initialises the Port Protocol Migration State Machine*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RstInitProtocolMigrationMachine (VOID)
{
    INT4                i4Index = 0;
    UINT1               aau1SemEvent[RST_PMIGSM_MAX_EVENTS][20] = {
        "BEGIN", "PORT_ENABLED", "MDELAYWHILE_EXP",
        "PORT_DISABLED", "MCHECK", "RCVD_STP", "RCVD_RSTP"
    };
    UINT1               aau1SemState[RST_PMIGSM_MAX_STATES][20] = {
        "CHECKING_RSTP", "SELECTING_STP", "SENSING"
    };
    for (i4Index = 0; i4Index < RST_PMIGSM_MAX_EVENTS; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemEvent[AST_PMSM][i4Index],
                     aau1SemEvent[i4Index], STRLEN (aau1SemEvent[i4Index]));
        gaaau1AstSemEvent[AST_PMSM][i4Index][STRLEN (aau1SemEvent[i4Index])] =
            '\0';
    }
    for (i4Index = 0; i4Index < RST_PMIGSM_MAX_STATES; i4Index++)
    {
        AST_STRNCPY (gaaau1AstSemState[AST_PMSM][i4Index],
                     aau1SemState[i4Index], STRLEN (aau1SemState[i4Index]));
        gaaau1AstSemState[AST_PMSM][i4Index][STRLEN (aau1SemState[i4Index])] =
            '\0';
    }

    /* Event 0 - RST_PMIGSM_EV_BEGIN */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_BEGIN]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = RstPmigSmMakeCheckingRstp;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_BEGIN]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = RstPmigSmMakeCheckingRstp;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_BEGIN]
        [RST_PMIGSM_STATE_SENSING].pAction = RstPmigSmMakeCheckingRstp;

    /* Event 1 - RST_PMIGSM_EV_PORT_ENABLED */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_ENABLED]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = RstPmigSmStartMDelayWhile;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_ENABLED]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_ENABLED]
        [RST_PMIGSM_STATE_SENSING].pAction = NULL;

    /* Event 2 - RST_PMIGSM_EV_MDELAYWHILE_EXP */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MDELAYWHILE_EXP]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = RstPmigSmMakeSensing;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MDELAYWHILE_EXP]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = RstPmigSmMakeSensing;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MDELAYWHILE_EXP]
        [RST_PMIGSM_STATE_SENSING].pAction = NULL;

    /* Event 3 - RST_PMIGSM_EV_PORT_DISABLED */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_DISABLED]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = RstPmigSmMakeCheckingRstp;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_DISABLED]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = RstPmigSmMakeSensing;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_PORT_DISABLED]
        [RST_PMIGSM_STATE_SENSING].pAction = RstPmigSmMakeCheckingRstp;

    /* Event 4 - RST_PMIGSM_EV_MCHECK */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MCHECK]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MCHECK]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = RstPmigSmMakeSensing;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_MCHECK]
        [RST_PMIGSM_STATE_SENSING].pAction = RstPmigSmMakeCheckingRstp;

    /* Event 5 - RST_PMIGSM_EV_RCVD_STP */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_STP]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_STP]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_STP]
        [RST_PMIGSM_STATE_SENSING].pAction = RstPmigSmSenseMigration;

    /* Event 6 - RST_PMIGSM_EV_RCVD_RSTP */
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_RSTP]
        [RST_PMIGSM_STATE_CHECKING_RSTP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_RSTP]
        [RST_PMIGSM_STATE_SELECTING_STP].pAction = NULL;
    RST_PORT_MIG_MACHINE[RST_PMIGSM_EV_RCVD_RSTP]
        [RST_PMIGSM_STATE_SENSING].pAction = RstPmigSmSenseMigration;

    AST_DBG (AST_TXSM_DBG, "PMSM: Loaded PMSM SEM successfully\n");

    return;
}
