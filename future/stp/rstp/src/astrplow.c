/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrplow.c,v 1.86 2018/01/09 11:02:04 siva Exp $
 *
 * Description: This file contains low-level functions for the 
 *              proprietary RSTP MIB objects.
 *
 *******************************************************************/

# include  "asthdrs.h"
# include "stpcli.h"
# include "fsmprscli.h"

extern UINT4        fsmpbr[8];
extern UINT4        fsmprs[8];
extern UINT4        fsDot1dStp[9];
PRIVATE VOID        RstpNotifyProtocolShutdownStatus (INT4);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRstSystemControl
 Input       :  The Indices

                The Object 
                retValFsRstSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstSystemControl (INT4 *pi4RetValFsRstSystemControl)
{
    if (AST_IS_RST_STARTED ())
    {
        *pi4RetValFsRstSystemControl = (INT4) RST_SNMP_START;
    }
    else
    {
        *pi4RetValFsRstSystemControl = (INT4) RST_SNMP_SHUTDOWN;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstModuleStatus
 Input       :  The Indices

                The Object 
                retValFsRstModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstModuleStatus (INT4 *pi4RetValFsRstModuleStatus)
{
    if (AST_IS_RST_STARTED ())
    {
        if ((AST_NODE_STATUS () == RED_AST_ACTIVE) ||
            (AST_NODE_STATUS () == RED_AST_STANDBY))
        {
            *pi4RetValFsRstModuleStatus =
                gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()];
            return (INT1) SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValFsRstModuleStatus = (INT4) AST_ADMIN_STATUS;
            return (INT1) SNMP_SUCCESS;
        }
    }
    *pi4RetValFsRstModuleStatus = (INT4) RST_DISABLED;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstTraceOption
 Input       :  The Indices

                The Object 
                retValFsRstTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstTraceOption (INT4 *pi4RetValFsRstTraceOption)
{
    *pi4RetValFsRstTraceOption = (INT4) AST_TRACE_OPTION;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstDebugOption
 Input       :  The Indices

                The Object 
                retValFsRstDebugOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstDebugOption (INT4 *pi4RetValFsRstDebugOption)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        *pi4RetValFsRstDebugOption = (INT4) AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsRstDebugOption = (INT4) AST_DEBUG_OPTION;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRstpUpCount
 Input       :  The Indices

                The Object 
                retValFsRstRstpUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRstpUpCount (UINT4 *pu4RetValFsRstRstpUpCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();
    *pu4RetValFsRstRstpUpCount = pAstBridgeEntry->u4AstpUpCount;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRstpDownCount
 Input       :  The Indices

                The Object 
                retValFsRstRstpDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRstpDownCount (UINT4 *pu4RetValFsRstRstpDownCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();
    *pu4RetValFsRstRstpDownCount = pAstBridgeEntry->u4AstpDownCount;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstBufferFailureCount
 Input       :  The Indices

                The Object 
                retValFsRstBufferFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstBufferFailureCount (UINT4 *pu4RetValFsRstBufferFailureCount)
{
    *pu4RetValFsRstBufferFailureCount = gAstGlobalInfo.u4BufferFailureCount;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstMemAllocFailureCount
 Input       :  The Indices

                The Object 
                retValFsRstMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstMemAllocFailureCount (UINT4 *pu4RetValFsRstMemAllocFailureCount)
{
    *pu4RetValFsRstMemAllocFailureCount = gAstGlobalInfo.u4MemoryFailureCount;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstNewRootIdCount
 Input       :  The Indices

                The Object 
                retValFsRstNewRootIdCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstNewRootIdCount (UINT4 *pu4RetValFsRstNewRootIdCount)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstNewRootIdCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    *pu4RetValFsRstNewRootIdCount = pPerStBrgInfo->u4NewRootIdCount;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortRoleSelSmState
 Input       :  The Indices

                The Object 
                retValFsRstPortRoleSelSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRoleSelSmState (INT4 *pi4RetValFsRstPortRoleSelSmState)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortRoleSelSmState = AST_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);
    *pi4RetValFsRstPortRoleSelSmState = pPerStBrgInfo->u1ProleSelSmState;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstStpPerfStatus
 Input       :  The Indices
 
                The Object
                retValFsRstStpPerfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsRstStpPerfStatus (INT4 *pi4RetValFsRstStpPerfStatus)
{
    UINT4               u4ContextId = 0;
    BOOL1               bPerfDataFlag;

    if (AstL2IwfGetPerformanceDataStatus (u4ContextId, &bPerfDataFlag) ==
        L2IWF_FAILURE)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (bPerfDataFlag == (BOOL1) AST_PERFORMANCE_STATUS_ENABLE)
    {
        *pi4RetValFsRstStpPerfStatus = (INT4) AST_PERFORMANCE_STATUS_ENABLE;
    }
    else
    {
        *pi4RetValFsRstStpPerfStatus = (INT4) AST_PERFORMANCE_STATUS_DISABLE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRstSystemControl
 Input       :  The Indices

                The Object 
                setValFsRstSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstSystemControl (INT4 i4SetValFsRstSystemControl)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4IcclIfIndex = 0;
    UINT4               u4MsgType = AST_INIT_VAL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsRstSystemControl == (INT4) RST_SNMP_START)
    {
        if (AST_IS_RST_STARTED ())
        {
            AST_TRC (AST_MGMT_TRC, "MGMT: RSTP is already Started.\n");
            return SNMP_SUCCESS;
        }
        u4MsgType = AST_START_RST_MSG;
    }
    else
    {
        if (!AST_IS_RST_STARTED ())
        {
            AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "SNMP: RSTP Module is not started to shut down !!!\n");
            return SNMP_SUCCESS;
        }
        u4MsgType = AST_STOP_RST_MSG;

    }
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pNode->MsgType = u4MsgType;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstSystemControl,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstSystemControl));

    /* When MCLAG is enabled and ICCL interface is present,
     * RSTP is disabled on the ICCL interface and on the MC-LAG
     * interface*/
    if ((AST_IS_RST_STARTED ()) &&
        (AstLaGetMCLAGSystemStatus () == AST_MCLAG_ENABLED))
    {
        AstIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex != 0)
        {
            /*Disable RST on ICCL */
            AST_UNLOCK ();
            if (AstDisableStpOnPort (u4IcclIfIndex) != OSIX_SUCCESS)
            {
                i1RetVal = SNMP_FAILURE;
            }

            /* Disable RST on MC-LAG interfaces */
            if (AstPortLaDisableOnMcLagIf () != OSIX_SUCCESS)
            {
                i1RetVal = SNMP_FAILURE;
            }
            AST_LOCK ();
        }
    }

    AstSelectContext (u4CurrContextId);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4SetValFsRstSystemControl == (INT4) RST_SNMP_SHUTDOWN)
        {
            /* Notify MSR with the RSTP oids */
            RstpNotifyProtocolShutdownStatus (AST_CURR_CONTEXT_ID ());
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstModuleStatus
 Input       :  The Indices

                The Object 
                setValFsRstModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstModuleStatus (INT4 i4SetValFsRstModuleStatus)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((i4SetValFsRstModuleStatus ==
         (INT4) gau1AstModuleStatus[AST_CURR_CONTEXT_ID ()])
        && (i4SetValFsRstModuleStatus == (INT4) AST_ADMIN_STATUS))
    {
        AST_TRC (AST_MGMT_TRC, "MGMT: RSTP already has the same Status\n");
        return (INT1) SNMP_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsRstModuleStatus == (INT4) RST_ENABLED)
    {
        pNode->MsgType = AST_ENABLE_RST_MSG;
    }
    else
    {
        pNode->MsgType = AST_DISABLE_RST_MSG;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstModuleStatus,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstModuleStatus));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstTraceOption
 Input       :  The Indices

                The Object 
                setValFsRstTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstTraceOption (INT4 i4SetValFsRstTraceOption)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    AST_TRACE_OPTION = (UINT4) i4SetValFsRstTraceOption;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Management SET of Trace Option as %d Success\n",
                  i4SetValFsRstTraceOption);
    if (i4SetValFsRstTraceOption != 0)
    {
        AST_DEBUG_OPTION = AST_INIT_VAL;
        AST_DBG (AST_MGMT_DBG, "SNMP: Resetting the Debug Option...\n");
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstTraceOption,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstTraceOption));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRstDebugOption
 Input       :  The Indices

                The Object 
                setValFsRstDebugOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstDebugOption (INT4 i4SetValFsRstDebugOption)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    AST_DEBUG_OPTION = (UINT4) i4SetValFsRstDebugOption;
    AST_DBG_ARG1 (AST_MGMT_DBG,
                  "SNMP: Management SET of Debug Option as %d Success\n",
                  i4SetValFsRstDebugOption);
    if (i4SetValFsRstDebugOption != 0)
    {
        AST_TRACE_OPTION = AST_INIT_VAL;
        AST_DBG (AST_MGMT_DBG, "SNMP: Resetting the Trace Option...\n");
    }
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstDebugOption,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstDebugOption));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRstStpPerfStatus
 Input       :  The Indices
 
                The Object
                setValFsRstStpPerfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsRstStpPerfStatus (INT4 i4SetValFsRstStpPerfStatus)
{

    UINT4               u4ContextId = 0;

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstL2IwfSetPerformanceDataStatus
        (u4ContextId, (BOOL1) (i4SetValFsRstStpPerfStatus)) == L2IWF_FAILURE)
    {
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRstSystemControl
 Input       :  The Indices

                The Object 
                testValFsRstSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsRstSystemControl)
{
    UINT4               u4BridgeMode = AST_INVALID_BRIDGE_MODE;

    if ((i4TestValFsRstSystemControl != (INT4) RST_SNMP_START) &&
        (i4TestValFsRstSystemControl != (INT4) RST_SNMP_SHUTDOWN))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_MST_STARTED ())
    {
        AST_TRC (AST_MGMT_TRC, "MGMT: MSTP must be made down.\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsRstSystemControl == (INT4) RST_SNMP_START)
    {
        /* Check whether the bridge mode is set. If not set
         * then return failure. */
        if (AstL2IwfGetBridgeMode (AST_CURR_CONTEXT_ID (), &u4BridgeMode)
            != L2IWF_SUCCESS)
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        if (u4BridgeMode == AST_INVALID_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_STP_BRG_MODE_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        /* If the context has SISP enabled ports as member ports, than RSTP 
         * cannot be the operating mode for the context
         * */
        if (AstSispIsSispPortPresentInCtxt (AST_CURR_CONTEXT_ID ()) == VCM_TRUE)
        {
            CLI_SET_ERR (CLI_STP_SISP_BRG_MODE_ERR);
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstModuleStatus
 Input       :  The Indices

                The Object 
                testValFsRstModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsRstModuleStatus)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT Enable/Disable RSTP\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstModuleStatus == (INT4) RST_ENABLED) ||
        (i4TestValFsRstModuleStatus == (INT4) RST_DISABLED))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsRstTraceOption
 Input       :  The Indices

                The Object 
                testValFsRstTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsRstTraceOption)
{
    if ((i4TestValFsRstTraceOption < AST_MIN_TRACE_VAL) ||
        (i4TestValFsRstTraceOption > AST_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRstDebugOption
 Input       :  The Indices

                The Object 
                testValFsRstDebugOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstDebugOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsRstDebugOption)
{
    if ((i4TestValFsRstDebugOption < AST_MIN_DEBUG_VAL) ||
        (i4TestValFsRstDebugOption > RST_MAX_DEBUG_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstStpPerfStatus
 Input       :  The Indices
                The Object
                testValFsRstStpPerfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)                                                                   SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                                                  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/

INT1
nmhTestv2FsRstStpPerfStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsRstStpPerfStatus)
{
    if ((i4TestValFsRstStpPerfStatus == AST_PERFORMANCE_STATUS_ENABLE)
        || (i4TestValFsRstStpPerfStatus == AST_PERFORMANCE_STATUS_DISABLE))
    {
        return (INT1) SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRstSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstModuleStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstDebugOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstDebugOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstStpPerfStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                 Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsRstStpPerfStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRstPortExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRstPortExtTable
 Input       :  The Indices
                FsRstPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRstPortExtTable (INT4 i4FsRstPort)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is Shutdown!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowValidatePortIndex (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRstPortExtTable
 Input       :  The Indices
                FsRstPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRstPortExtTable (INT4 *pi4FsRstPort)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4FsRstPort) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRstPortExtTable
 Input       :  The Indices
                FsRstPort
                nextFsRstPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRstPortExtTable (INT4 i4FsRstPort, INT4 *pi4NextFsRstPort)
{
    if (i4FsRstPort < 0)
    {
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetNextValidIndex (i4FsRstPort, pi4NextFsRstPort)
        != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRstPortRole
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRole (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortRole)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortRole = AST_PORT_ROLE_DISABLED;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);

    *pi4RetValFsRstPortRole = (INT4) (pPerStPortInfo->u1PortRole);
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortOperVersion
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortOperVersion (INT4 i4FsRstPort,
                            INT4 *pi4RetValFsRstPortOperVersion)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortOperVersion = AST_RST_MODE;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsRstPort);

    /* Check to avoid showing port operational version
     * as RSTP when the bridge level configuration (StpVersion) is STPCompatible.
     * This happens when the port is operationally down and StpVersion is set to STPCompatible.
     */
    /* Return operational version as STP compatible when 
     * the global configuration is STPCompatible.
     */
    if (AST_FORCE_VERSION == (UINT1) AST_VERSION_0)
    {
        *pi4RetValFsRstPortOperVersion = AST_STP_COMPATIBLE_MODE;
        return (INT1) SNMP_SUCCESS;
    }
    if (pAstCommPortInfo->bSendRstp == RST_FALSE)
    {
        *pi4RetValFsRstPortOperVersion = AST_STP_COMPATIBLE_MODE;
    }
    else
    {
        *pi4RetValFsRstPortOperVersion = AST_RST_MODE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortInfoSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortInfoSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortInfoSmState (INT4 i4FsRstPort,
                            INT4 *pi4RetValFsRstPortInfoSmState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortInfoSmState = RST_PINFOSM_STATE_DISABLED;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);
    *pi4RetValFsRstPortInfoSmState = pPerStPortInfo->u1PinfoSmState;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortMigSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortMigSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortMigSmState (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortMigSmState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortMigSmState = RST_PMIGSM_STATE_CHECKING_RSTP;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsRstPort);
    *pi4RetValFsRstPortMigSmState = pAstCommPortInfo->u1PmigSmState;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRoleTransSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRoleTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRoleTransSmState (INT4 i4FsRstPort,
                                 INT4 *pi4RetValFsRstPortRoleTransSmState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortRoleTransSmState = RST_PROLETRSM_STATE_INIT_PORT;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);
    *pi4RetValFsRstPortRoleTransSmState = pPerStPortInfo->u1ProleTrSmState;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortStateTransSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortStateTransSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortStateTransSmState (INT4 i4FsRstPort,
                                  INT4 *pi4RetValFsRstPortStateTransSmState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortStateTransSmState = RST_PSTATETRSM_STATE_DISCARDING;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);
    if (pPerStPortInfo != NULL)
    {
        *pi4RetValFsRstPortStateTransSmState =
            pPerStPortInfo->u1PstateTrSmState;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortTopoChSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortTopoChSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortTopoChSmState (INT4 i4FsRstPort,
                              INT4 *pi4RetValFsRstPortTopoChSmState)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortTopoChSmState = RST_TOPOCHSM_STATE_INACTIVE;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);
    *pi4RetValFsRstPortTopoChSmState = pPerStPortInfo->u1TopoChSmState;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortTxSmState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortTxSmState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortTxSmState (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortTxSmState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortTxSmState = RST_PTXSM_STATE_TRANSMIT_INIT;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsRstPort);
    *pi4RetValFsRstPortTxSmState = pAstCommPortInfo->u1PortTxSmState;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRxRstBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRxRstBpduCount (INT4 i4FsRstPort,
                               UINT4 *pu4RetValFsRstPortRxRstBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortRxRstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortRxRstBpduCount = pAstPortEntry->u4NumRstBpdusRxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRxConfigBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRxConfigBpduCount (INT4 i4FsRstPort,
                                  UINT4 *pu4RetValFsRstPortRxConfigBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortRxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortRxConfigBpduCount = pAstPortEntry->u4NumConfigBpdusRxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRxTcnBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRxTcnBpduCount (INT4 i4FsRstPort,
                               UINT4 *pu4RetValFsRstPortRxTcnBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortRxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortRxTcnBpduCount = pAstPortEntry->u4NumTcnBpdusRxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortTxRstBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortTxRstBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortTxRstBpduCount (INT4 i4FsRstPort,
                               UINT4 *pu4RetValFsRstPortTxRstBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortTxRstBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortTxRstBpduCount = pAstPortEntry->u4NumRstBpdusTxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortTxConfigBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortTxConfigBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortTxConfigBpduCount (INT4 i4FsRstPort,
                                  UINT4 *pu4RetValFsRstPortTxConfigBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortTxConfigBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortTxConfigBpduCount = pAstPortEntry->u4NumConfigBpdusTxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortTxTcnBpduCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortTxTcnBpduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortTxTcnBpduCount (INT4 i4FsRstPort,
                               UINT4 *pu4RetValFsRstPortTxTcnBpduCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortTxTcnBpduCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortTxTcnBpduCount = pAstPortEntry->u4NumTcnBpdusTxd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortInvalidRstBpduRxCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortInvalidRstBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortInvalidRstBpduRxCount (INT4 i4FsRstPort,
                                      UINT4
                                      *pu4RetValFsRstPortInvalidRstBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortInvalidRstBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortInvalidRstBpduRxCount =
        pAstPortEntry->u4InvalidRstBpdusRxdCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortInvalidConfigBpduRxCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortInvalidConfigBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortInvalidConfigBpduRxCount (INT4 i4FsRstPort,
                                         UINT4
                                         *pu4RetValFsRstPortInvalidConfigBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortInvalidConfigBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortInvalidConfigBpduRxCount =
        pAstPortEntry->u4InvalidConfigBpdusRxdCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortInvalidTcnBpduRxCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortInvalidTcnBpduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortInvalidTcnBpduRxCount (INT4 i4FsRstPort,
                                      UINT4
                                      *pu4RetValFsRstPortInvalidTcnBpduRxCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortInvalidTcnBpduRxCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortInvalidTcnBpduRxCount =
        pAstPortEntry->u4InvalidTcnBpdusRxdCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortProtocolMigrationCount
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortProtocolMigrationCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortProtocolMigrationCount (INT4 i4FsRstPort,
                                       UINT4
                                       *pu4RetValFsRstPortProtocolMigrationCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortProtocolMigrationCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortProtocolMigrationCount =
        pAstPortEntry->u4ProtocolMigrationCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstOldDesignatedRoot
 Input       :  The Indices

                The Object 
                retValFsRstOldDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstOldDesignatedRoot (tSNMP_OCTET_STRING_TYPE *
                              pRetValFsRstOldDesignatedRoot)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");

        pu1List = pRetValFsRstOldDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsRstOldDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    pu1List = pRetValFsRstOldDesignatedRoot->pu1_OctetList;

    u2Val = pPerStBrgInfo->OldRootId.u2BrgPriority;
    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pPerStBrgInfo->OldRootId.BridgeAddr);

    pRetValFsRstOldDesignatedRoot->i4_Length =
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRstDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                retValFsRstDynamicPathcostCalculation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsRstDynamicPathcostCalculation
    (INT4 *pi4RetValFsRstDynamicPathcostCalculation)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;
    pAstBridgeEntry = AST_GET_BRGENTRY ();
    if (pAstBridgeEntry->u1DynamicPathcostCalculation == RST_TRUE)
    {
        *pi4RetValFsRstDynamicPathcostCalculation = AST_SNMP_TRUE;
    }
    else if (pAstBridgeEntry->u1DynamicPathcostCalculation == RST_FALSE)
    {
        *pi4RetValFsRstDynamicPathcostCalculation = AST_SNMP_FALSE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                retValFsRstCalcPortPathCostOnSpeedChg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstCalcPortPathCostOnSpeedChg (INT4
                                       *pi4RetValFsRstCalcPortPathCostOnSpeedChg)
{
    *pi4RetValFsRstCalcPortPathCostOnSpeedChg =
        (AST_GET_BRGENTRY ())->u1DynamicPathcostCalcLagg;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRcvdEvent
 Input       :  The Indices

                The Object 
                retValFsRstRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRcvdEvent (INT4 *pi4RetValFsRstRcvdEvent)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pi4RetValFsRstRcvdEvent = 0;
    }
    *pi4RetValFsRstRcvdEvent = (AST_GET_BRGENTRY ())->i4RcvdEvent;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRcvdEventSubType
 Input       :  The Indices

                The Object 
                retValFsRstRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRcvdEventSubType (INT4 *pi4RetValFsRstRcvdEventSubType)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pi4RetValFsRstRcvdEventSubType = 0;
    }
    *pi4RetValFsRstRcvdEventSubType = (AST_GET_BRGENTRY ())->i4RcvdEventSubType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRcvdEventTimeStamp
 Input       :  The Indices

                The Object 
                retValFsRstRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRcvdEventTimeStamp (UINT4 *pu4RetValFsRstRcvdEventTimeStamp)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4RetValFsRstRcvdEventTimeStamp = 0;
    }
    *pu4RetValFsRstRcvdEventTimeStamp =
        (AST_GET_BRGENTRY ())->u4RcvdEventTimeStamp;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstRcvdPortStateChangeTimeStamp
 Input       :  The Indices

                The Object 
                retValFsRstRcvdPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRcvdPortStateChangeTimeStamp (UINT4
                                         *pu4RetValFsRstRcvdPortStateChangeTimeStamp)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4RetValFsRstRcvdPortStateChangeTimeStamp = 0;
    }
    *pu4RetValFsRstRcvdPortStateChangeTimeStamp =
        (AST_GET_BRGENTRY ())->u4PortStateChangeTimeStamp;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstBpduGuard
 Input       :  The Indices

                The Object
                retValFsRstBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstBpduGuard (INT4 *pi4RetValFsRstBpduGuard)
{
    if (AST_IS_RST_STARTED ())
    {
        *pi4RetValFsRstBpduGuard = (INT4) AST_GBL_BPDUGUARD_STATUS;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRstDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                setValFsRstDynamicPathcostCalculation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstDynamicPathcostCalculation (INT4
                                       i4SetValFsRstDynamicPathcostCalculation)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_DYNAMIC_PATHCOST_MSG;

    if (i4SetValFsRstDynamicPathcostCalculation == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.u1DynamicPathcostCalculation = RST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstDynamicPathcostCalculation,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstDynamicPathcostCalculation));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                setValFsRstCalcPortPathCostOnSpeedChg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstCalcPortPathCostOnSpeedChg (INT4
                                       i4SetValFsRstCalcPortPathCostOnSpeedChg)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_ENABLE_PORT_SPEED_CHG_MSG;

    pAstMsgNode->uMsg.u1DynamicPathcostCalculation =
        (UINT1) i4SetValFsRstCalcPortPathCostOnSpeedChg;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstCalcPortPathCostOnSpeedChg,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstCalcPortPathCostOnSpeedChg));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsRstBpduGuard
 Input       :  The Indices

                The Object
                setValFsRstBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstBpduGuard (INT4 i4SetValFsRstBpduGuard)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsRstBpduGuard == AST_BPDUGUARD_ENABLE)
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_ENABLE_MSG;
    }
    else
    {
        pNode->MsgType = AST_GBL_BPDUGUARD_DISABLE_MSG;
    }

    pNode->uMsg.u4BpduGuardStatus = i4SetValFsRstBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstBpduGuard));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRstDynamicPathcostCalculation
 Input       :  The Indices

                The Object 
                testValFsRstDynamicPathcostCalculation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsRstDynamicPathcostCalculation
    (UINT4 *pu4ErrorCode, INT4 i4TestValFsRstDynamicPathcostCalculation)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; Cannot set pathcost calculation"
                 "method\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstDynamicPathcostCalculation == AST_SNMP_TRUE)
        || (i4TestValFsRstDynamicPathcostCalculation == AST_SNMP_FALSE))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsRstCalcPortPathCostOnSpeedChg
 Input       :  The Indices

                The Object 
                testValFsRstCalcPortPathCostOnSpeedChg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsRstCalcPortPathCostOnSpeedChg)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; Cannot set pathcost calculation"
                 "on speed change\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstCalcPortPathCostOnSpeedChg == AST_SNMP_TRUE)
        || (i4TestValFsRstCalcPortPathCostOnSpeedChg == AST_SNMP_FALSE))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsRstBpduGuard
 Input       :  The Indices

                The Object
                testValFsRstBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstBpduGuard (UINT4 *pu4ErrorCode, INT4 i4TestValFsRstBpduGuard)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRstBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsRstBpduGuard != AST_BPDUGUARD_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRstDynamicPathcostCalculation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstDynamicPathcostCalculation (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstCalcPortPathCostOnSpeedChg
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstCalcPortPathCostOnSpeedChg (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRstBpduGuard
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstBpduGuard (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstSetTraps
 Input       :  The Indices

                The Object 
                retValFsRstSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstSetTraps (INT4 *pi4RetValFsRstSetTraps)
{
    *pi4RetValFsRstSetTraps = AST_TRAP_TYPE;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstGenTrapType
 Input       :  The Indices

                The Object 
                retValFsRstGenTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstGenTrapType (INT4 *pi4RetValFsRstGenTrapType)
{
    *pi4RetValFsRstGenTrapType = AST_GEN_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstErrTrapType
 Input       :  The Indices

                The Object 
                retValFsRstErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstErrTrapType (INT4 *pi4RetValFsRstErrTrapType)
{
    *pi4RetValFsRstErrTrapType = AST_ERR_TRAP;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortEnableBPDURx
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortEnableBPDURx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortEnableBPDURx (INT4 i4FsRstPort,
                             INT4 *pi4RetValFsRstPortEnableBPDURx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_ENABLE_BPDU_RX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortEnableBPDURx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortEnableBPDURx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortEnableBPDUTx
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortEnableBPDUTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortEnableBPDUTx (INT4 i4FsRstPort,
                             INT4 *pi4RetValFsRstPortEnableBPDUTx)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_ENABLE_BPDU_TX (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortEnableBPDUTx = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortEnableBPDUTx = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortPseudoRootId
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortPseudoRootId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortPseudoRootId (INT4 i4FsRstPort,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsRstPortPseudoRootId)
{
    tAstPerStPortInfo  *pAstPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Enabled!\n");
        pu1List = pRetValFsRstPortPseudoRootId->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValFsRstPortPseudoRootId->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPerStPortInfo =
        AST_GET_PERST_PORT_INFO (i4FsRstPort, RST_DEFAULT_INSTANCE);

    pRetValFsRstPortPseudoRootId->pu1_OctetList[0] =
        (UINT1) ((pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0xff00) >> 8);

    pRetValFsRstPortPseudoRootId->pu1_OctetList[1] =
        (UINT1) (pAstPerStPortInfo->PseudoRootId.u2BrgPriority & 0x00ff);

    AST_MEMCPY (pRetValFsRstPortPseudoRootId->pu1_OctetList +
                AST_BRG_PRIORITY_SIZE,
                (&pAstPerStPortInfo->PseudoRootId.BridgeAddr),
                AST_MAC_ADDR_SIZE);

    pRetValFsRstPortPseudoRootId->i4_Length
        = AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortIsL2Gp
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortIsL2Gp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortIsL2Gp (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortIsL2Gp)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_IS_L2GP (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortIsL2Gp = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortIsL2Gp = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortLoopGuard
 Input       :  The Indices
                FsRstPort

                The Object
                retValFsRstPortLoopGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortLoopGuard (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortLoopGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_LOOP_GUARD (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortLoopGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortLoopGuard = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRcvdEvent
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRcvdEvent (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortRcvdEvent)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pi4RetValFsRstPortRcvdEvent = pAstPortEntry->i4RcvdEvent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortRcvdEventSubType
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRcvdEventSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRcvdEventSubType (INT4 i4FsRstPort,
                                 INT4 *pi4RetValFsRstPortRcvdEventSubType)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pi4RetValFsRstPortRcvdEventSubType = pAstPortEntry->i4RcvdEventSubType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortRcvdEventTimeStamp
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRcvdEventTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRcvdEventTimeStamp (INT4 i4FsRstPort,
                                   UINT4 *pu4RetValFsRstPortRcvdEventTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortRcvdEventTimeStamp = pAstPortEntry->u4RcvdEventTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortStateChangeTimeStamp
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortStateChangeTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortStateChangeTimeStamp (INT4 i4FsRstPort,
                                     UINT4
                                     *pu4RetValFsRstPortStateChangeTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortStateChangeTimeStamp =
        pAstPortEntry->u4PortStateChangeTimeStamp;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRstSetTraps
 Input       :  The Indices

                The Object 
                setValFsRstSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstSetTraps (INT4 i4SetValFsRstSetTraps)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AST_TRAP_TYPE = (UINT4) i4SetValFsRstSetTraps;

    if (i4SetValFsRstSetTraps == 2    /* Err trap */
        || i4SetValFsRstSetTraps == 3 /* All traps */ )
    {
        /* If the trap option being enabled is the 
         * exception trap then enable the global 
         * mem fail trap as well */
        gAstGlobalInfo.u4GlobalTrapOption = 1;
    }
    else
    {
        gAstGlobalInfo.u4GlobalTrapOption = 0;
    }
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstSetTraps, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_CURR_CONTEXT_ID (),
                      i4SetValFsRstSetTraps));
    AstSelectContext (u4CurrContextId);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortEnableBPDURx
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortEnableBPDURx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortEnableBPDURx (INT4 i4FsRstPort,
                             INT4 i4SetValFsRstPortEnableBPDURx)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsRstPortEnableBPDURx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_RX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsRstPortEnableBPDURx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsRstPortEnableBPDURx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDURx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_RX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortEnableBPDURx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortEnableBPDURx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortEnableBPDUTx
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortEnableBPDUTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortEnableBPDUTx (INT4 i4FsRstPort,
                             INT4 i4SetValFsRstPortEnableBPDUTx)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_TRUE) &&
         (i4SetValFsRstPortEnableBPDUTx == AST_SNMP_TRUE)) ||
        ((AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_FALSE) &&
         (i4SetValFsRstPortEnableBPDUTx == AST_SNMP_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsRstPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bEnableBPDUTx = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_BPDU_TX_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortEnableBPDUTx,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortEnableBPDUTx));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortPseudoRootId
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortPseudoRootId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortPseudoRootId (INT4 i4FsRstPort,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsRstPortPseudoRootId)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeId        PseudoRootId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AST_MEMSET (PseudoRootId.BridgeAddr, 0, AST_MAC_ADDR_SIZE);

    PseudoRootId.u2BrgPriority = 0;

    PseudoRootId.u2BrgPriority =
        (UINT2) (((pSetValFsRstPortPseudoRootId->pu1_OctetList[0] << 8) |
                  pSetValFsRstPortPseudoRootId->pu1_OctetList[1]));

    AST_MEMCPY (PseudoRootId.BridgeAddr,
                pSetValFsRstPortPseudoRootId->pu1_OctetList +
                AST_BRG_PRIORITY_SIZE, AST_MAC_ADDR_SIZE);

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsRstPort,
                                              RST_DEFAULT_INSTANCE);

    if (RstCompareBrgId (&pPerStPortInfo->PseudoRootId, &PseudoRootId)
        == RST_BRGID1_SAME)
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_PSEUDO_ROOTID_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    pAstMsgNode->uMsg.BridgeId.u2BrgPriority = PseudoRootId.u2BrgPriority;

    AST_MEMCPY (pAstMsgNode->uMsg.BridgeId.BridgeAddr, PseudoRootId.BridgeAddr,
                AST_MAC_ADDR_SIZE);

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortPseudoRootId,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", AST_GET_IFINDEX (i4FsRstPort),
                      pSetValFsRstPortPseudoRootId));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortIsL2Gp
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortIsL2Gp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortIsL2Gp (INT4 i4FsRstPort, INT4 i4SetValFsRstPortIsL2Gp)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (((i4SetValFsRstPortIsL2Gp == AST_SNMP_TRUE) &&
         (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)) ||
        ((i4SetValFsRstPortIsL2Gp == AST_SNMP_FALSE) &&
         (AST_PORT_IS_L2GP (pPortEntry) == RST_FALSE)))
    {
        return SNMP_SUCCESS;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    if (i4SetValFsRstPortIsL2Gp == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bPortL2gp = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bPortL2gp = RST_FALSE;
    }

    pAstMsgNode->MsgType = AST_PORT_L2GP_CONF_MSG;

    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    pAstMsgNode->u2InstanceId = RST_DEFAULT_INSTANCE;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortIsL2Gp,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortIsL2Gp));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortLoopGuard
 Input       :  The Indices
                FsRstPort

                The Object
                setValFsRstPortLoopGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortLoopGuard (INT4 i4FsRstPort, INT4 i4SetValFsRstPortLoopGuard)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_LOOP_GUARD_MSG;
    pNode->u4PortNo = (UINT4) i4FsRstPort;

    if (i4SetValFsRstPortLoopGuard == AST_SNMP_TRUE)
    {
        pNode->uMsg.bLoopGuard = RST_TRUE;
    }
    else
    {
        pNode->uMsg.bLoopGuard = RST_FALSE;
    }

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortLoopGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortLoopGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsRstPortRowStatus
 Input       :  The Indices
                FsRstPort

                The Object
                setValFsRstPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortRowStatus (INT4 i4FsRstPort, INT4 i4SetValFsRstPortRowStatus)
{

    tAstMsgNode        *pAstMsgNode = NULL;
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4SeqNum = 0;
    UINT1               u1OperStatus = AST_INIT_VAL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT create interface\n");
        return SNMP_SUCCESS;
    }

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, i4FsRstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fetching the external port \n");
        return SNMP_FAILURE;

    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));
    pAstMsgNode->u4PortNo = u4IfIndex;
    pAstMsgNode->u4ContextId = u4ContextId;
    pAstMsgNode->uMsg.u2LocalPortId = i4FsRstPort;

#ifdef L2RED_WANTED
    if (((pPortEntry = AST_GET_PORTENTRY (i4FsRstPort)) != NULL)
        && ((i4SetValFsRstPortRowStatus == CREATE_AND_WAIT)
            || (i4SetValFsRstPortRowStatus == CREATE_AND_GO))
        && (AstRmGetNodeState () == RM_STANDBY))
    {

        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }

        return SNMP_SUCCESS;

    }
#endif

    switch (i4SetValFsRstPortRowStatus)
    {
        case ACTIVE:
            pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
            }
            break;
        case NOT_IN_SERVICE:
            pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = NOT_IN_SERVICE;
            }
            break;

        case CREATE_AND_WAIT:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
            if (AstHandleCreatePort (pAstMsgNode) != RST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = NOT_READY;
            }
            break;
        case CREATE_AND_GO:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_CREATE_PORT_MSG;
            if (AstHandleCreatePort (pAstMsgNode) != RST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
            if (pPortEntry != NULL)
            {
                AST_PORT_ROW_STATUS (pPortEntry) = ACTIVE;
            }
            break;
        case DESTROY:
            pAstMsgNode->MsgType = (tAstLocalMsgType) AST_DELETE_PORT_MSG;
            pAstMsgNode->u4PortNo = i4FsRstPort;
            if (AstHandleDeletePort (pAstMsgNode) != RST_SUCCESS)
            {
                if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) !=
                    AST_MEM_SUCCESS)
                {
                    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                             "MGMT: Release of Local Message Memory Block FAILED!\n");
                }
                return SNMP_FAILURE;
            }
            break;
    }

    if (AstCfaGetIfOperStatus (u4IfIndex, &u1OperStatus) != CFA_SUCCESS)
    {
        AST_DBG_ARG1 (AST_INIT_SHUT_DBG | AST_ALL_FAILURE_DBG,
                      "SYS: Cannot Get IfOperStatus for Port %u\n", u4IfIndex);
        if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "MGMT: Release of Local Message Memory Block FAILED!\n");
        }
        return SNMP_FAILURE;
    }
    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    if (pPortEntry != NULL)
    {
        if (u1OperStatus == AST_UP)
        {
            if (pPortEntry->i4PortRowStatus == ACTIVE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_UP);
            }
            else if (pPortEntry->i4PortRowStatus == NOT_IN_SERVICE)
            {
                AstUpdateOperStatus (u4IfIndex, AST_DOWN);
            }
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    u4ContextId = AST_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortRowStatus,
                          u4SeqNum, TRUE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex,
                      i4SetValFsRstPortRowStatus));
    if (i4SetValFsRstPortRowStatus == DESTROY)
    {
        MsrNotifyStpPortDelete (u4IfIndex);
    }
    AstSelectContext (u4ContextId);

    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pAstMsgNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Message Memory Block FAILED!\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortBpduGuard
 Input       :  The Indices
                FsRstPort

                The Object
                setValFsRstPortBpduGuard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortBpduGuard (INT4 i4FsRstPort, INT4 i4SetValFsRstPortBpduGuard)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_RST_CONFIG_BPDUGUARD_PORT_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    pAstMsgNode->uMsg.u4BpduGuardStatus = i4SetValFsRstPortBpduGuard;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortBpduGuard,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortBpduGuard));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhSetFsRstPortErrorRecovery
  Input       :  The Indices
                 FsRstPort

                 The Object
                 setValFsRstPortErrorRecovery
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortErrorRecovery (INT4 i4FsRstPort,
                              INT4 i4SetValFsRstPortErrorRecovery)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "SNMP: Local Message Memory Allocation Fail!\n");
        return (INT1) SNMP_FAILURE;
    }
    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_PORT_ERROR_RECOVERY_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;
    pAstMsgNode->uMsg.u4ErrorRecovery = (UINT4) i4SetValFsRstPortErrorRecovery;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortErrorRecovery,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortErrorRecovery));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsRstPort

                The Object
                setValFsRstPortStpModeDot1wEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortStpModeDot1wEnabled (INT4 i4FsRstPort,
                                    INT4 i4SetValFsRstPortStpModeDot1wEnabled)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AST_CURR_CONTEXT_ID ();

    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->MsgType = AST_DOT1W_ENABLED_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    if (i4SetValFsRstPortStpModeDot1wEnabled == AST_SNMP_TRUE)
    {
        pAstMsgNode->uMsg.bDot1wEnabled = RST_TRUE;
    }
    else
    {
        pAstMsgNode->uMsg.bDot1wEnabled = RST_FALSE;
    }

    if (AstProcessSnmpRequest (pAstMsgNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortStpModeDot1wEnabled,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortStpModeDot1wEnabled));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhSetFsRstPortRootGuard
  Input       :  The Indices
                 FsRstPort
 
                 The Object
                 setValFsRstPortRootGuard
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortRootGuard (INT4 i4FsRstPort, INT4 i4SetValFsRstPortRootGuard)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstBoolean         bStatus = RST_TRUE;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (i4SetValFsRstPortRootGuard == AST_SNMP_TRUE)
    {
        bStatus = RST_TRUE;
        i4SetValFsRstPortRootGuard = RST_TRUE;
    }
    else
    {
        bStatus = RST_FALSE;
        i4SetValFsRstPortRootGuard = RST_FALSE;
    }

    if (AST_IS_RST_STARTED ())
    {
        u4CurrContextId = AST_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (RstSetPortRootGuard (pPortEntry, bStatus) != RST_SUCCESS)
        {
            i1RetVal = SNMP_FAILURE;
        }
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortRootGuard,
                              u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          AST_GET_IFINDEX (i4FsRstPort),
                          i4SetValFsRstPortRootGuard));
        AstSelectContext (u4CurrContextId);
    }
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRstSetTraps
 Input       :  The Indices

                The Object 
                testValFsRstSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstSetTraps (UINT4 *pu4ErrorCode, INT4 i4TestValFsRstSetTraps)
{
    if ((i4TestValFsRstSetTraps < 0) || (i4TestValFsRstSetTraps > AST_TRAP_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRstSetTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstSetTraps (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortEnableBPDURx
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortEnableBPDURx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortEnableBPDURx (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                INT4 i4TestValFsRstPortEnableBPDURx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortEnableBPDURx != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortEnableBPDURx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsRstPort < AST_MIN_NUM_PORTS) || (i4FsRstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortEnableBPDUTx
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortEnableBPDUTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortEnableBPDUTx (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                INT4 i4TestValFsRstPortEnableBPDUTx)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortEnableBPDUTx != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortEnableBPDUTx != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsRstPort < AST_MIN_NUM_PORTS) || (i4FsRstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /*
     * enableBPDUTx can not be set to TRUE on L2GP Port and CBPs
     * */

    if (i4TestValFsRstPortEnableBPDUTx == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }

        if (AST_IS_CUSTOMER_BACKBONE_PORT (i4FsRstPort) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortPseudoRootId
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortPseudoRootId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortPseudoRootId (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsRstPortPseudoRootId)
{
    UINT2               u2BrgPriority = 0;
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsRstPort < AST_MIN_NUM_PORTS) || (i4FsRstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (pTestValFsRstPortPseudoRootId->i4_Length !=
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u2BrgPriority =
        (UINT2) (((pTestValFsRstPortPseudoRootId->pu1_OctetList[0] << 8) |
                  pTestValFsRstPortPseudoRootId->pu1_OctetList[1]));

    if (u2BrgPriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_BRIDGE_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortIsL2Gp
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortIsL2Gp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortIsL2Gp (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                          INT4 i4TestValFsRstPortIsL2Gp)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortIsL2Gp != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortIsL2Gp != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4FsRstPort < AST_MIN_NUM_PORTS) || (i4FsRstPort > AST_MAX_NUM_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValFsRstPortIsL2Gp == AST_SNMP_TRUE)
    {
        if (AST_PORT_ENABLE_BPDU_TX (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_L2GP_BPDUTX_ERR);
            return SNMP_FAILURE;
        }

        /* if restrcitedRole or restrictedTcn is set,
         * L2gp con not set to TRUE.
         * */
        if ((AST_PORT_RESTRICTED_ROLE (pPortEntry) == RST_TRUE) ||
            (AST_PORT_RESTRICTED_TCN (pPortEntry) == RST_TRUE))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortLoopGuard
 Input       :  The Indices
                FsRstPort

                The Object
                testValFsRstPortLoopGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortLoopGuard (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                             INT4 i4TestValFsRstPortLoopGuard)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; Cannot set Loop Guard"
                 "method\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortLoopGuard != AST_SNMP_TRUE)
        && (i4TestValFsRstPortLoopGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (pPortEntry->bRootGuard == AST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_ROOTGUARD_ENABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortRowStatus
 Input       :  The Indices
                FsRstPort

                The Object
                testValFsRstPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                             INT4 i4TestValFsRstPortRowStatus)
{

    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;

    if (AstIsExtInterface (i4FsRstPort) == AST_FALSE)
    {
        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT create interface\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortRowStatus < ACTIVE) ||
        (i4TestValFsRstPortRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u4ContextId = AST_CURR_CONTEXT_ID ();

    if (AstVcmGetIfIndexFromLocalPort (u4ContextId, i4FsRstPort,
                                       &u4IfIndex) == VCM_FAILURE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Failure in Fetching the external port \n");
        return SNMP_FAILURE;

    }

    if (AstL2IwfIsPortInPortChannel (u4IfIndex) == RST_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (IssGetAutoPortCreateFlag () != ISS_ENABLE)
    {
        /* To avoid deletion of port which is already deleted/not created ,
           when AutoPortCreate flag is disabled */
        if ((i4TestValFsRstPortRowStatus == DESTROY)
            && (AST_GET_PORTENTRY (i4FsRstPort) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        /* To avoid addition of port which is already created , when AutoPortCreate flag is disabled */
        if (((i4TestValFsRstPortRowStatus == CREATE_AND_GO)
             || (i4TestValFsRstPortRowStatus == CREATE_AND_WAIT))
            && (AST_GET_PORTENTRY (i4FsRstPort) != NULL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortBpduGuard
 Input       :  The Indices
                FsRstPort

                The Object
                testValFsRstPortBpduGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortBpduGuard (UINT4 *pu4ErrorCode,
                             INT4 i4FsRstPort, INT4 i4TestValFsRstPortBpduGuard)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortBpduGuard != AST_BPDUGUARD_ENABLE) &&
        (i4TestValFsRstPortBpduGuard != AST_BPDUGUARD_DISABLE) &&
        (i4TestValFsRstPortBpduGuard != AST_BPDUGUARD_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
   Function    :  nmhTestv2FsRstPortErrorRecovery
   Input       :  The Indices
                  FsRstPort

                  The Object
                  testValFsRstPortErrorRecovery
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
   Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                                                                                                                                               Returns     :  SNMP_SUCCESS or SNMP_FAILURE
                                                                                                                      ****************************************************************************/
INT1
nmhTestv2FsRstPortErrorRecovery (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                 INT4 i4TestValFsRstPortErrorRecovery)
{

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValFsRstPortErrorRecovery) != AST_OK)
        || (i4TestValFsRstPortErrorRecovery < AST_MIN_ERRRECOVERY)
        || (i4TestValFsRstPortErrorRecovery > AST_MAX_ERRRECOVERY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsRstPort

                The Object
                testValFsRstPortStpModeDot1wEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortStpModeDot1wEnabled (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                       INT4
                                       i4TestValFsRstPortStpModeDot1wEnabled)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; Cannot set Loop Guard"
                 "method\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortStpModeDot1wEnabled != AST_SNMP_TRUE)
        && (i4TestValFsRstPortStpModeDot1wEnabled != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortRootGuard
 Input       :  The Indices
                FsRstPort

                The Object
                testValFsRstPortRootGuard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                                                                                                                               Returns     :  SNMP_SUCCESS or SNMP_FAILURE
                                                                                                                              ****************************************************************************/
INT1
nmhTestv2FsRstPortRootGuard (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                             INT4 i4TestValFsRstPortRootGuard)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set Root Guard\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortRootGuard != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortRootGuard != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pPortEntry->bLoopGuard == AST_TRUE)
    {
        CLI_SET_ERR (CLI_STP_LOOPGUARD_ENABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, root guard cannot be set to TRUE. */
    if (i4TestValFsRstPortRootGuard == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP of a 1AD bridge the root guard
     *      * cannot be set to false. */
    if (i4TestValFsRstPortRootGuard == AST_SNMP_FALSE)
    {
        if ((AST_IS_1AD_BRIDGE () == RST_TRUE) &&
            ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsRstPort) == RST_TRUE) ||
             (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
              AST_PROP_CUSTOMER_NETWORK_PORT)
             || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                 AST_PROP_CUSTOMER_EDGE_PORT)))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRstPortExtTable
 Input       :  The Indices
                FsRstPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRstPortExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRstPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRstPortTrapNotificationTable
 Input       :  The Indices
                FsRstPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRstPortTrapNotificationTable (INT4
                                                        i4FsRstPortTrapIndex)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP System Control is shutdown!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowValidatePortIndex (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRstPortTrapNotificationTable
 Input       :  The Indices
                FsRstPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRstPortTrapNotificationTable (INT4 *pi4FsRstPortTrapIndex)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRstPortTrapNotificationTable
 Input       :  The Indices
                FsRstPortTrapIndex
                nextFsRstPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRstPortTrapNotificationTable (INT4 i4FsRstPortTrapIndex,
                                               INT4 *pi4NextFsRstPortTrapIndex)
{
    if (i4FsRstPortTrapIndex < 0)
    {
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetNextValidIndex
        (i4FsRstPortTrapIndex, pi4NextFsRstPortTrapIndex) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRstPortMigrationType
 Input       :  The Indices
                FsRstPortTrapIndex

                The Object 
                retValFsRstPortMigrationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortMigrationType (INT4 i4FsRstPortTrapIndex,
                              INT4 *pi4RetValFsRstPortMigrationType)
{
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortMigrationType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsRstPortMigrationType =
        (AST_GET_PORTENTRY (i4FsRstPortTrapIndex))->u1MigrationType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    : nmhGetFsRstPortRoleType 
 Input       :  The Indices
                FsRstPortTrapIndex

                The Object 
                retValFsRstPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRoleType (INT4 i4FsRstPortTrapIndex,
                         INT4 *pi4RetValFsRstPortRole)
{
    /* This Routine should be called before the PortRole Selection is done *
     * Then only it will give the Correct Value                *
     * In other words, only during the Port Role Selection,            *
     * the values of the Selected Role and                 *
     * Old Role (Routine below) Differ                     *
     * After Selection is finished, both will be similair         */

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsRstPortRole =
        (INT4) AST_GET_SELECTED_PORT_ROLE (RST_DEFAULT_INSTANCE,
                                           i4FsRstPortTrapIndex);

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    : nmhGetFsRstOldPortRoleType 
 Input       :  The Indices
                FsRstPortTrapIndex

                The Object 
                retValFsRstOldPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstOldPortRoleType (INT4 i4FsRstPortTrapIndex,
                            INT4 *pi4RetValFsRstOldPortRole)
{
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstOldPortRole = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsRstOldPortRole =
        (INT4) AST_GET_PORT_ROLE (RST_DEFAULT_INSTANCE, i4FsRstPortTrapIndex);

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPktErrType
 Input       :  The Indices
                FsRstPortTrapIndex

                The Object 
                retValFsRstPktErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPktErrType (INT4 i4FsRstPortTrapIndex,
                       INT4 *pi4RetValFsRstPktErrType)
{
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPktErrType = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsRstPktErrType =
        (INT4) (AST_GET_PORTENTRY (i4FsRstPortTrapIndex))->u1PktErrType;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPktErrVal
 Input       :  The Indices
                FsRstPortTrapIndex

                The Object 
                retValFsRstPktErrVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPktErrVal (INT4 i4FsRstPortTrapIndex, INT4 *pi4RetValFsRstPktErrVal)
{
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPktErrVal = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPortTrapIndex) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValFsRstPktErrVal =
        (INT4) (AST_GET_PORTENTRY (i4FsRstPortTrapIndex))->u2PktErrValue;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortEffectivePortState
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortEffectivePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortEffectivePortState (INT4 i4FsRstPort,
                                   INT4 *pi4RetValFsRstPortEffectivePortState)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortEffectivePortState = RST_PORT_OPER_DISABLED;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4FsRstPort,
                                              RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    /*Get the u1EntryStatus information */
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (((pRstPortInfo->bPortEnabled) == RST_TRUE)
        && ((pAstPortEntry->u1EntryStatus) == AST_PORT_OPER_UP))
    {
        *pi4RetValFsRstPortEffectivePortState = RST_PORT_OPER_ENABLED;
    }
    else
    {
        *pi4RetValFsRstPortEffectivePortState = RST_PORT_OPER_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortAutoEdge
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortAutoEdge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortAutoEdge (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortAutoEdge)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pAstPortEntry->bAutoEdge == RST_TRUE)
    {
        *pi4RetValFsRstPortAutoEdge = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortAutoEdge = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortRestrictedRole
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRestrictedRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRestrictedRole (INT4 i4FsRstPort,
                               INT4 *pi4RetValFsRstPortRestrictedRole)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_RESTRICTED_ROLE (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortRestrictedRole = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortRestrictedRole = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortRestrictedTCN
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortRestrictedTCN
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRestrictedTCN (INT4 i4FsRstPort,
                              INT4 *pi4RetValFsRstPortRestrictedTCN)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_RESTRICTED_TCN (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortRestrictedTCN = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortRestrictedTCN = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortRowStatus
 Input       :  The Indices
                FsRstPort

                The Object
                retValFsRstPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRowStatus (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortRowStatus)
{

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsRstPortRowStatus =
        (AST_GET_PORTENTRY (i4FsRstPort))->i4PortRowStatus;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortBpduGuard
 Input       :  The Indices
                FsRstPort

                The Object
                retValFsRstPortBpduGuard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortBpduGuard (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortBpduGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Started!\n");
        *pi4RetValFsRstPortBpduGuard = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsRstPortBpduGuard = pAstPortEntry->CommPortInfo.u4BpduGuard;

    return SNMP_SUCCESS;

}

/****************************************************************************
  Function    :  nmhGetFsRstPortErrorRecovery                                                                           Input       :  The Indices
                 FsRstPort                                                                                             
                 The Object                                                                                                            retValFsRstPortErrorRecovery
  Output      :  The Get Low Lev Routine Take the Indices &                                                                            store the Value requested in the Return val.
  returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetFsRstPortErrorRecovery (INT4 i4FsRstPort,
                              INT4 *pi4RetValFsRstPortErrorRecovery)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        *pi4RetValFsRstPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);

        return (INT1) SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");
        *pi4RetValFsRstPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pAstPortEntry == NULL)
    {
        *pi4RetValFsRstPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (AST_DEFAULT_ERROR_RECOVERY);
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsRstPortErrorRecovery =
            (INT4) AST_SYS_TO_MGMT (pAstPortEntry->u4ErrorRecovery);
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortStpModeDot1wEnabled
 Input       :  The Indices
                FsRstPort

                The Object
                retValFsRstPortStpModeDot1wEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortStpModeDot1wEnabled (INT4 i4FsRstPort,
                                    INT4 *pi4RetValFsRstPortStpModeDot1wEnabled)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    if (pAstPortEntry == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (AST_PORT_DOT1W_ENABLED (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortStpModeDot1wEnabled = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortStpModeDot1wEnabled = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRstPortBpduInconsistentState                                                                                                          Input       :  The Indices
                FsRstPort
                                                                                                                                                                             The Object
                retValFsRstPortBpduInconsistentState
 Output      :  The Get Low Lev Routine Take the Indices &                                                                                                                   store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ INT1
nmhGetFsRstPortBpduInconsistentState (INT4 i4FsRstPort,
                                      INT4
                                      *pi4RetValFsRstPortBpduInconsistentState)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4FsRstPort);

    if (NULL != pAstCommPortInfo)
    {
        *pi4RetValFsRstPortBpduInconsistentState =
            pAstCommPortInfo->bBpduInconsistent;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
   Function    :  nmhGetFsRstPortRootGuard
   Input       :  The Indices
                  FsRstPort
 
                  The Object
                  retValFsRstPortRootGuard
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortRootGuard (INT4 i4FsRstPort, INT4 *pi4RetValFsRstPortRootGuard)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (AST_PORT_ROOT_GUARD (pAstPortEntry) == RST_TRUE)
    {
        *pi4RetValFsRstPortRootGuard = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstPortRootGuard = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhGetFsRstRootInconsistentState
  Input       :  The Indices
                 FsRstPort

                 The Object
                 retValFsRstRootInconsistentState
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstRootInconsistentState (INT4 i4FsRstPort,
                                  INT4 *pi4RetValFsRstRootInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Enabled!\n");
        *pi4RetValFsRstRootInconsistentState = AST_SNMP_FALSE;
        return SNMP_SUCCESS;

    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pAstPortEntry == NULL)
    {
        AstReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pAstPortEntry->bRootInconsistent == RST_TRUE)
    {
        *pi4RetValFsRstRootInconsistentState = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsRstRootInconsistentState = AST_SNMP_FALSE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRstPortAutoEdge
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortAutoEdge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortAutoEdge (INT4 i4FsRstPort, INT4 i4SetValFsRstPortAutoEdge)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_AUTO_EDGEPORT_MSG;
    pNode->u4PortNo = (UINT4) i4FsRstPort;

    if (i4SetValFsRstPortAutoEdge == AST_SNMP_TRUE)
    {
        pNode->uMsg.bAutoEdgePort = RST_TRUE;
    }
    else
    {
        pNode->uMsg.bAutoEdgePort = RST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortAutoEdge,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortAutoEdge));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortRestrictedRole
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortRestrictedRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortRestrictedRole (INT4 i4FsRstPort,
                               INT4 i4SetValFsRstPortRestrictedRole)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tAstBoolean         bStatus = RST_TRUE;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (i4SetValFsRstPortRestrictedRole == AST_SNMP_TRUE)
    {
        bStatus = RST_TRUE;
        i4SetValFsRstPortRestrictedRole = RST_TRUE;
    }
    else
    {
        bStatus = RST_FALSE;
        i4SetValFsRstPortRestrictedRole = RST_FALSE;
    }

    if (AST_IS_RST_STARTED ())
    {
        u4CurrContextId = AST_CURR_CONTEXT_ID ();
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (RstSetPortRestrictedRole (pPortEntry, bStatus) != RST_SUCCESS)
        {
            i1RetVal = SNMP_FAILURE;
        }
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortRestrictedRole,
                              u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          AST_GET_IFINDEX (i4FsRstPort),
                          i4SetValFsRstPortRestrictedRole));
        AstSelectContext (u4CurrContextId);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRstPortRestrictedTCN
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortRestrictedTCN
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortRestrictedTCN (INT4 i4FsRstPort,
                              INT4 i4SetValFsRstPortRestrictedTCN)
{
    tAstPortEntry      *pPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (i4SetValFsRstPortRestrictedTCN == AST_SNMP_TRUE)
    {
        AST_PORT_RESTRICTED_TCN (pPortEntry) = RST_TRUE;
    }
    else
    {
        AST_PORT_RESTRICTED_TCN (pPortEntry) = RST_FALSE;
    }

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortRestrictedTCN,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortRestrictedTCN));
    AstSelectContext (u4CurrContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortAutoEdge
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortAutoEdge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortAutoEdge (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                            INT4 i4TestValFsRstPortAutoEdge)
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set PortAutoEdgePort\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }
    /* AutoEdge cannot be set for logical VIPs */
    if (AST_IS_VIRTUAL_INST_PORT (i4FsRstPort) == RST_TRUE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Cannot configure AutoEdge for a VIP!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RSTP_VIP_CONF_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortAutoEdge == AST_SNMP_TRUE) ||
        (i4TestValFsRstPortAutoEdge == AST_SNMP_FALSE))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortRestrictedRole
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortRestrictedRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortRestrictedRole (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                  INT4 i4TestValFsRstPortRestrictedRole)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set RestrictedRole\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortRestrictedRole != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortRestrictedRole != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, restrcitedRole cannot be set to TRUE. */
    if (i4TestValFsRstPortRestrictedRole == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP of a 1AD bridge the restrictedRole 
     * cannot be set to false. */
    if (i4TestValFsRstPortRestrictedRole == AST_SNMP_FALSE)
    {
        if ((AST_IS_1AD_BRIDGE () == RST_TRUE) &&
            ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsRstPort) == RST_TRUE) ||
             (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
              AST_PROP_CUSTOMER_NETWORK_PORT)
             || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                 AST_PROP_CUSTOMER_EDGE_PORT)))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortRestrictedTCN
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortRestrictedTCN
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortRestrictedTCN (UINT4 *pu4ErrorCode, INT4 i4FsRstPort,
                                 INT4 i4TestValFsRstPortRestrictedTCN)
{
    tAstPortEntry      *pPortEntry = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set RestrictedTcn\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortRestrictedTCN != AST_SNMP_TRUE) &&
        (i4TestValFsRstPortRestrictedTCN != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4FsRstPort < AST_MIN_NUM_PORTS || i4FsRstPort > AST_MAX_NUM_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4FsRstPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    /* On L2GP Port, restrcitedTcn cannot be set to TRUE. */
    if (i4TestValFsRstPortRestrictedTCN == AST_SNMP_TRUE)
    {
        if (AST_PORT_IS_L2GP (pPortEntry) == RST_TRUE)
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_L2GP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    /* On CNPs and on PCNP/PCEP restrictedTcn can not be set to false. */
    if (i4TestValFsRstPortRestrictedTCN == AST_SNMP_FALSE)
    {

        if ((AST_IS_1AD_BRIDGE () == RST_TRUE) &&
            ((AST_IS_CUSTOMER_NETWORK_PORT (i4FsRstPort) == RST_TRUE) ||
             (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
              AST_PROP_CUSTOMER_NETWORK_PORT)
             || (AST_GET_BRG_PORT_TYPE (pPortEntry) ==
                 AST_PROP_CUSTOMER_EDGE_PORT)))
        {
            CLI_SET_ERR (CLI_STP_RES_ROLE_TCN_PORT_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RstpNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
RstpNotifyProtocolShutdownStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 0;

    AST_MEMSET (&au1ObjectOid, 0,
                ((sizeof (UINT1)) * (3 * SNMP_MAX_OID_LENGTH)));
    /* The oid list contains the list of Oids that has been registered 
     * for the protocol, i.e fsDot1dStp, fsmprs. Rest to be sent in the next trigger*/
    SNMPGetOidString (fsDot1dStp, (sizeof (fsDot1dStp) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fsmprs, (sizeof (fsmprs) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    SNMPGetOidString (FsMIRstSystemControl,
                      (sizeof (FsMIRstSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    u2Objects = 3;

    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, u2Objects, i4ContextId);

    /*Clearing the OID list to fill the next set of protocol OIDs and SystemControl object */
    AST_MEMSET (&au1ObjectOid, 0,
                ((sizeof (UINT1)) * (3 * SNMP_MAX_OID_LENGTH)));
    u2Objects = 0;
    /* The oid list contains the rest of the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
#ifdef PB_WANTED
    SNMPGetOidString (fsmpbr, (sizeof (fsmpbr) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    u2Objects = 1;
#endif

    u2Objects++;
    SNMPGetOidString (FsMIRstSystemControl,
                      (sizeof (FsMIRstSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[u2Objects - 1]);

    /* Send a notification to MSR to process the Rstp shutdown, with 
     * Rstp oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, u2Objects, i4ContextId);

#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}

/****************************************************************************
 Function    :  nmhGetFsRstPortBpduGuardAction
 Input       :  The Indices
                FsRstPort

                The Object 
                retValFsRstPortBpduGuardAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRstPortBpduGuardAction (INT4 i4FsRstPort,
                                INT4 *pi4RetValFsRstPortBpduGuardAction)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Started!\n");
        *pi4RetValFsRstPortBpduGuardAction = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    if (pAstPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    *pi4RetValFsRstPortBpduGuardAction =
        pAstPortEntry->CommPortInfo.u1BpduGuardAction;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRstPortBpduGuardAction
 Input       :  The Indices
                FsRstPort

                The Object 
                setValFsRstPortBpduGuardAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRstPortBpduGuardAction (INT4 i4FsRstPort,
                                INT4 i4SetValFsRstPortBpduGuardAction)
{
    tAstMsgNode        *pAstMsgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = AST_INIT_VAL;
    UINT4               u4SeqNum = AST_INIT_VAL;
    INT1                i1RetVal = SNMP_SUCCESS;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW:Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((pAstMsgNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AST_DBG (AST_MGMT_DBG | AST_MEM_DBG | AST_ALL_FAILURE_DBG,
                 "LW: Local Message Memory Allocation Fail!\n");

        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pAstMsgNode, AST_INIT_VAL, sizeof (tAstMsgNode));

    pAstMsgNode->uMsg.u1BpduGuardAction =
        (UINT1) i4SetValFsRstPortBpduGuardAction;
    /* Action STP_Disable handled in nmhSetFsRstPortBpduGuard function since
     * this is the default action to be taken */

    pAstMsgNode->MsgType = AST_RST_BPDUGUARD_ACTION_MSG;
    pAstMsgNode->u4PortNo = (UINT4) i4FsRstPort;

    u4CurrContextId = AST_CURR_CONTEXT_ID ();
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pAstMsgNode) != RST_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIRstPortBpduGuardAction,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AST_GET_IFINDEX (i4FsRstPort),
                      i4SetValFsRstPortBpduGuardAction));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsRstPortBpduGuardAction
 Input       :  The Indices
                FsRstPort

                The Object 
                testValFsRstPortBpduGuardAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRstPortBpduGuardAction (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRstPort,
                                   INT4 i4TestValFsRstPortBpduGuardAction)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "LW: RST Module not Started!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsRstPortBpduGuardAction != AST_PORT_ADMIN_DOWN) &&
        (i4TestValFsRstPortBpduGuardAction != AST_PORT_STATE_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFsRstPortTCDetectedCount
 *  Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortTCDetectedCount
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
INT1
nmhGetFsRstPortTCDetectedCount (INT4 i4FsRstPort,
                                UINT4 *pu4RetValFsRstPortTCDetectedCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortTCDetectedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortTCDetectedCount = pAstPortEntry->u4NumRstTCDetectedCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortTCReceivedCount
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortTCReceivedCount
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortTCReceivedCount (INT4 i4FsRstPort,
                                UINT4 *pu4RetValFsRstPortTCReceivedCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortTCReceivedCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortTCReceivedCount = pAstPortEntry->u4NumRstTCRxdCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortRcvInfoWhileExpCount
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortRcvInfoWhileExpCount
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortRcvInfoWhileExpCount (INT4 i4FsRstPort,
                                     UINT4
                                     *pu4RetValFsRstPortRcvInfoWhileExpCount)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortRcvInfoWhileExpCount = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortRcvInfoWhileExpCount =
        pAstPortEntry->u4NumRstRxdInfoWhileExpCount;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortProposalPktsSent
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortProposalPktsSent
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortProposalPktsSent (INT4 i4FsRstPort,
                                 UINT4 *pu4RetValFsRstPortProposalPktsSent)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortProposalPktsSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortProposalPktsSent =
        pAstPortEntry->u4NumRstProposalPktsSent;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortProposalPktsRcvd
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortProposalPktsRcvd
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortProposalPktsRcvd (INT4 i4FsRstPort,
                                 UINT4 *pu4RetValFsRstPortProposalPktsRcvd)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortProposalPktsRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortProposalPktsRcvd =
        pAstPortEntry->u4NumRstProposalPktsRcvd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortAgreementPktSent
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortAgreementPktSent
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortAgreementPktSent (INT4 i4FsRstPort,
                                 UINT4 *pu4RetValFsRstPortAgreementPktSent)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortAgreementPktSent = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortAgreementPktSent =
        pAstPortEntry->u4NumRstAgreementPktSent;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortAgreementPktRcvd
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortAgreementPktRcvd
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortAgreementPktRcvd (INT4 i4FsRstPort,
                                 UINT4 *pu4RetValFsRstPortAgreementPktRcvd)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortAgreementPktRcvd = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortAgreementPktRcvd =
        pAstPortEntry->u4NumRstAgreementPktRcvd;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 *   Function    :  nmhGetFsRstPortImpossibleStateOcc
 *   Input       :  The Indices
 *                  FsRstPort
 *
 *                  The Object
 *                  retValFsRstPortImpossibleStateOcc
 *   Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRstPortImpStateOccurCount (INT4 i4FsRstPort,
                                   UINT4 *pu4RetValFsRstPortImpossibleStateOcc)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValFsRstPortImpossibleStateOcc = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pu4RetValFsRstPortImpossibleStateOcc =
        pAstPortEntry->u4NumRstImpossibleStateOcc;
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 * Function    :  nmhGetFsRstPortOldPortState
 * Input       :  The Indices
 *                FsRstPort
 *
 *                The Object
 *                retValFsRstPortOldPortState
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRstPortOldPortState (INT4 i4FsRstPort,
                             INT4 *pi4RetValFsRstPortOldPortState)
{
    INT1                i1RetVal = AST_INIT_VAL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortOldPortState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1dStpPortState (i4FsRstPort, pi4RetValFsRstPortOldPortState);

    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsRstPortLoopInconsistentState
 * Input       :  The Indices
 *                FsRstPort
 *
 *                The Object
 *                retValFsRstPortLoopInconsistentState
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRstPortLoopInconsistentState (INT4 i4FsRstPort,
                                      INT4
                                      *pi4RetValFsRstPortLoopInconsistentState)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValFsRstPortLoopInconsistentState = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);

    *pi4RetValFsRstPortLoopInconsistentState = pAstPortEntry->bLoopInconsistent;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortTCDetectedTimeStamp
*  Input       :  The Indices
*
*                  The Object
*                 retValFsRstPortTCDetectedTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsRstPortTCDetectedTimeStamp (INT4 i4FsRstPort,
                                    UINT4
                                    *pu4RetValFsRstPortTCDetectedTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortTCDetectedTimeStamp =
        pAstPortEntry->u4TCDetectedTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortTCReceivedTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortTCReceivedTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsRstPortTCReceivedTimeStamp (INT4 i4FsRstPort,
                                    UINT4
                                    *pu4RetValFsRstPortTCReceivedTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortTCReceivedTimeStamp =
        pAstPortEntry->u4TCReceivedTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortRcvInfoWhileExpTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortTCReceivedTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsRstPortRcvInfoWhileExpTimeStamp (INT4 i4FsRstPort,
                                         UINT4
                                         *pu4RetValFsRstPortRcvInfoWhileExpTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortRcvInfoWhileExpTimeStamp =
        pAstPortEntry->u4RcvInfoWhileExpTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortProposalPktSentTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortProposalPktSentTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsRstPortProposalPktSentTimeStamp (INT4 i4FsRstPort,
                                         UINT4
                                         *pu4RetValFsRstPortProposalPktSentTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortProposalPktSentTimeStamp =
        pAstPortEntry->u4ProposalPktSentTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortProposalPktRcvdTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortProposalPktRcvdTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsRstPortProposalPktRcvdTimeStamp (INT4 i4FsRstPort,
                                         UINT4
                                         *pu4RetValFsRstPortProposalPktRcvdTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortProposalPktRcvdTimeStamp =
        pAstPortEntry->u4ProposalPktRcvdTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortAgreementPktSentTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortAgreementPktSentTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsRstPortAgreementPktSentTimeStamp (INT4 i4FsRstPort,
                                          UINT4
                                          *pu4RetValFsRstPortAgreementPktSentTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortAgreementPktSentTimeStamp =
        pAstPortEntry->u4AgreementPktSentTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortAgreementPktRcvdTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortAgreementPktRcvdTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/

INT1
nmhGetFsRstPortAgreementPktRcvdTimeStamp (INT4 i4FsRstPort,
                                          UINT4
                                          *pu4RetValFsRstPortAgreementPktRcvdTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortAgreementPktRcvdTimeStamp =
        pAstPortEntry->u4AgreementPktRcvdTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
*  Function    :  nmhGetFsRstPortImpStateOccurTimeStamp
*  Input       :  The Indices
*
*                 The Object
*                 retValFsRstPortImpStateOccurTimeStamp
*  Output      :  The Get Low Lev Routine Take the Indices &
*                 store the Value requested in the Return val.
*  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/

INT1
nmhGetFsRstPortImpStateOccurTimeStamp (INT4 i4FsRstPort,
                                       UINT4
                                       *pu4RetValFsRstPortImpStateOccurTimeStamp)
{
    tAstPortEntry      *pAstPortEntry = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP Module not Started!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4FsRstPort) != RST_SUCCESS)
    {

        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist \n");

        return (INT1) SNMP_FAILURE;
    }
    pAstPortEntry = AST_GET_PORTENTRY (i4FsRstPort);
    *pu4RetValFsRstPortImpStateOccurTimeStamp =
        pAstPortEntry->u4ImpStateOccurTimeStamp;
    return SNMP_SUCCESS;
}
