/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astrslow.c,v 1.26 2017/11/20 13:12:25 siva Exp $
 *
 * Description: This file contains low-level functions for the 
 *              standard (draft) RSTP MIB objects.
 *
 *******************************************************************/

# include  "asthdrs.h"
# include  "stpcli.h"
# include "cli.h"
# include "fsmsrscli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dStpVersion
 Input       :  The Indices

                The Object 
                retValDot1dStpVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpVersion (INT4 *pi4RetValDot1dStpVersion)
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Started!\n");
        *pi4RetValDot1dStpVersion = AST_VERSION_2;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValDot1dStpVersion = (INT4) AST_FORCE_VERSION;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpTxHoldCount
 Input       :  The Indices

                The Object 
                retValDot1dStpTxHoldCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpTxHoldCount (INT4 *pi4RetValDot1dStpTxHoldCount)
{
    tAstBridgeEntry    *pAstBridgeEntry = NULL;

    pAstBridgeEntry = AST_GET_BRGENTRY ();
    *pi4RetValDot1dStpTxHoldCount = (INT4) pAstBridgeEntry->u1TxHoldCount;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortAdminPathCost
 Input       :  The Indices
                Dot1dStpPort
                The Object
                retValDot1dStpPortAdminPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot1dStpPortAdminPathCost (INT4 i4Dot1dStpPort,
                                 INT4 *pi4RetValDot1dStpPortAdminPathCost)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (AST_GET_PERST_INFO (RST_DEFAULT_INSTANCE) == NULL)
    {
        AST_DBG_ARG1 (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                      "ASTP Perst info is not present for instance %d\n",
                      RST_DEFAULT_INSTANCE);
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }
    *pi4RetValDot1dStpPortAdminPathCost =
        (INT4) pPerStPortInfo->u4PortAdminPathCost;

    return (INT1) SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1dStpVersion
 Input       :  The Indices

                The Object 
                setValDot1dStpVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpVersion (INT4 i4SetValDot1dStpVersion)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_FORCE_VERSION_MSG;
    pNode->uMsg.u1ForceVersion = (UINT1) i4SetValDot1dStpVersion;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpVersion,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpVersion));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dStpTxHoldCount
 Input       :  The Indices

                The Object 
                setValDot1dStpTxHoldCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpTxHoldCount (INT4 i4SetValDot1dStpTxHoldCount)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_TX_HOLDCOUNT_MSG;
    pNode->uMsg.u1TxHoldCount = (UINT1) i4SetValDot1dStpTxHoldCount;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpTxHoldCount, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpTxHoldCount));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dStpPortAdminPathCost
 Input       :  The Indices
                Dot1dStpPort
                The Object
                setValDot1dStpPortAdminPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpPortAdminPathCost (INT4 i4Dot1dStpPort,
                                 INT4 i4SetValDot1dStpPortAdminPathCost)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    if (0 == i4SetValDot1dStpPortAdminPathCost)
    {
        pNode->MsgType = AST_ZERO_PATHCOST_MSG;
        pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
    }
    else
    {
        pNode->MsgType = AST_PATH_COST_MSG;
        pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
        pNode->uMsg.u4PathCost = (UINT4) i4SetValDot1dStpPortAdminPathCost;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortAdminPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortAdminPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpVersion
 Input       :  The Indices

                The Object 
                testValDot1dStpVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpVersion (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dStpVersion)
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set Stp Version\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpVersion == AST_VERSION_0) ||
        (i4TestValDot1dStpVersion == AST_VERSION_2))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpTxHoldCount
 Input       :  The Indices

                The Object 
                testValDot1dStpTxHoldCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpTxHoldCount (UINT4 *pu4ErrorCode,
                              INT4 i4TestValDot1dStpTxHoldCount)
{
    if ((i4TestValDot1dStpTxHoldCount < AST_TXHOLDCOUNT_MIN_VAL) ||
        (i4TestValDot1dStpTxHoldCount > AST_TXHOLDCOUNT_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_MGMT_TRC | AST_ALL_FAILURE_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set Transmit HoldCount\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortAdminPathCost
 Input       :  The Indices
                Dot1dStpPort
                The Object
                testValDot1dStpPortAdminPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpPortAdminPathCost (UINT4 *pu4ErrorCode, INT4 i4Dot1dStpPort,
                                    INT4 i4TestValDot1dStpPortAdminPathCost)
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control is in shutdown state!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);

    if (pPerStPortInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4Dot1dStpPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortAdminPathCost > AST_PORT_PATHCOST_100KBPS) ||
        (i4TestValDot1dStpPortAdminPathCost < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpExtPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpExtPortTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpVersion (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpTxHoldCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpTxHoldCount (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dStpExtPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dStpExtPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dStpExtPortTable (INT4 i4Dot1dStpPort)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module is in shutdown state!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowValidatePortIndex (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dStpExtPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1dStpExtPortTable (INT4 *pi4Dot1dStpPort)
{
    if (!(AST_IS_RST_STARTED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4Dot1dStpPort) != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dStpExtPortTable
 Input       :  The Indices
                Dot1dStpPort
                nextDot1dStpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dStpExtPortTable (INT4 i4Dot1dStpPort,
                                     INT4 *pi4NextDot1dStpPort)
{

    if (i4Dot1dStpPort < 0)
    {
        return SNMP_FAILURE;
    }
    if (AstSnmpLowGetNextValidIndex (i4Dot1dStpPort, pi4NextDot1dStpPort)
        != RST_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dStpPortProtocolMigration
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortProtocolMigration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpPortProtocolMigration (INT4 i4Dot1dStpPort,
                                     INT4
                                     *pi4RetValDot1dStpPortProtocolMigration)
{
    tAstCommPortInfo   *pAstCommPortInfo = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstCommPortInfo = AST_GET_COMM_PORT_INFO (i4Dot1dStpPort);

    if (pAstCommPortInfo->bMCheck == RST_TRUE)
    {
        *pi4RetValDot1dStpPortProtocolMigration = (INT4) AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1dStpPortProtocolMigration = (INT4) AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortAdminEdgePort
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortAdminEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpPortAdminEdgePort (INT4 i4Dot1dStpPort,
                                 INT4 *pi4RetValDot1dStpPortAdminEdgePort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);

    if (pAstPortEntry->bAdminEdgePort == RST_TRUE)
    {
        *pi4RetValDot1dStpPortAdminEdgePort = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1dStpPortAdminEdgePort = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortOperEdgePort
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortOperEdgePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpPortOperEdgePort (INT4 i4Dot1dStpPort,
                                INT4 *pi4RetValDot1dStpPortOperEdgePort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpPortOperEdgePort = AST_SNMP_FALSE;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);

    if (pAstPortEntry->bOperEdgePort == RST_TRUE)
    {
        *pi4RetValDot1dStpPortOperEdgePort = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1dStpPortOperEdgePort = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpPortAdminPointToPoint (INT4 i4Dot1dStpPort,
                                     INT4
                                     *pi4RetValDot1dStpPortAdminPointToPoint)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);
    *pi4RetValDot1dStpPortAdminPointToPoint =
        (INT4) pAstPortEntry->u1AdminPointToPoint;

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortOperPointToPoint
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStpPortOperPointToPoint (INT4 i4Dot1dStpPort,
                                    INT4 *pi4RetValDot1dStpPortOperPointToPoint)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);

    if (pAstPortEntry->bOperPointToPoint == RST_TRUE)
    {
        *pi4RetValDot1dStpPortOperPointToPoint = AST_SNMP_TRUE;
    }
    else
    {
        *pi4RetValDot1dStpPortOperPointToPoint = AST_SNMP_FALSE;
    }

    return (INT1) SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1dStpPortProtocolMigration
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortProtocolMigration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpPortProtocolMigration (INT4 i4Dot1dStpPort,
                                     INT4 i4SetValDot1dStpPortProtocolMigration)
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if (i4SetValDot1dStpPortProtocolMigration != AST_SNMP_TRUE)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_PROTOCOL_MIGRATION_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortProtocolMigration,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortProtocolMigration));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortProtocolMigration
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortProtocolMigration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpPortProtocolMigration (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1dStpPort,
                                        INT4
                                        i4TestValDot1dStpPortProtocolMigration)
{
    tAstPortEntry      *pPortEntry = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortProtocolMigration != AST_SNMP_TRUE) &&
        (i4TestValDot1dStpPortProtocolMigration != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    pPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
    if ((pPortEntry->u1EntryStatus == AST_PORT_OPER_UP) &&
        (pRstPortInfo->bPortEnabled == RST_TRUE))
    {
        return (INT1) SNMP_SUCCESS;
    }

    /* Silver Creek Fix: Setting a False Value when port is down
     * should succeed */

    if (i4TestValDot1dStpPortProtocolMigration == AST_SNMP_FALSE)
    {
        return (INT1) SNMP_SUCCESS;
    }

    AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC, "Port not Enabled..!\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortAdminEdgePort
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortAdminEdgePort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpPortAdminEdgePort (UINT4 *pu4ErrorCode, INT4 i4Dot1dStpPort,
                                    INT4 i4TestValDot1dStpPortAdminEdgePort)
{

    tAstPortEntry      *pAstPortEntry = NULL;

    pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4Dot1dStpPort);
    if (pAstPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set PortAdminEdgePort\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortAdminEdgePort != AST_SNMP_TRUE) &&
        (i4TestValDot1dStpPortAdminEdgePort != AST_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    /* AdminEdge cannot be set for logical VIPs */
    if (AST_IS_VIRTUAL_INST_PORT (i4Dot1dStpPort) == RST_TRUE)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Cannot configure AdminEdge for a VIP!\n");
        CLI_SET_ERR (CLI_RSTP_VIP_CONF_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortAdminPointToPoint
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStpPortAdminPointToPoint (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1dStpPort,
                                        INT4
                                        i4TestValDot1dStpPortAdminPointToPoint)
{

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "SNMP: RSTP System Shutdown; CANNOT set PortAdminPointToPoint\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortAdminPointToPoint != RST_P2P_FORCETRUE) &&
        (i4TestValDot1dStpPortAdminPointToPoint != RST_P2P_FORCEFALSE) &&
        (i4TestValDot1dStpPortAdminPointToPoint != RST_P2P_AUTO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;

}

/* Utility Routines for All Tables */

/****************************************************************************
 * Function    :  AstSnmpLowValidatePortIndex
 * Input       :  i4Dot1dStpPort
 *
 * Output      :  None
 * Returns     :  RST_SUCCESS / RST_FAILURE
 *****************************************************************************/

INT4
AstSnmpLowValidatePortIndex (INT4 i4Dot1dStpPort)
{
    if (i4Dot1dStpPort < AST_MIN_NUM_PORTS
        || i4Dot1dStpPort > AST_MAX_NUM_PORTS)
    {
        return RST_FAILURE;
    }
    return RST_SUCCESS;
}

/****************************************************************************
 * Function    :  AstValidatePortEntry
 * Input       :  i4Dot1dStpPort
 *
 * Output      :  None
 * Returns     :  RST_SUCCESS / RST_FAILURE
 *****************************************************************************/

INT4
AstValidatePortEntry (INT4 i4Dot1dStpPort)
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (i4Dot1dStpPort < AST_MIN_NUM_PORTS
        || i4Dot1dStpPort > AST_MAX_NUM_PORTS)
    {
        return RST_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);

    if (pAstPortEntry != NULL)
    {
        return RST_SUCCESS;
    }
    else
    {
        return RST_FAILURE;
    }
}

/****************************************************************************
 * Function    :  AstSnmpLowGetFirstValidIndex
 * Input       :  None
 *
 * Output      :  pi4Dot1dStpPort
 * Returns     :  RST_SUCCESS / RST_FAILURE
 *****************************************************************************/

INT4
AstSnmpLowGetFirstValidIndex (INT4 *pi4Dot1dStpPort)
{
    if (AstSnmpLowGetNextValidIndex (0, pi4Dot1dStpPort) != RST_SUCCESS)
    {
        return RST_FAILURE;
    }

    return RST_SUCCESS;
}

/****************************************************************************
 * Function    :  AstSnmpLowGetNextValidIndex
 * Input       :  i4Dot1dStpPort
 *
 * Output      :  pi4NextDot1dStpPort
 * Returns     :  RST_SUCCESS / RST_FAILURE
 *****************************************************************************/

INT4
AstSnmpLowGetNextValidIndex (INT4 i4Dot1dStpPort, INT4 *pi4NextDot1dStpPort)
{
    tAstPortEntry      *pPortEntry = NULL;
    UINT2               u2Count = 0;

    u2Count = (UINT2) (i4Dot1dStpPort + 1);

    AST_GET_NEXT_PORT_ENTRY (u2Count, pPortEntry)
    {

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dStpPort = (INT4) u2Count;
            return RST_SUCCESS;
        }
    }
    return RST_FAILURE;
}

/****************************************************************************
 * Function    :  AstProcessSnmpRequest
 * Input       :  pNode
 *
 * Output      :  None
 * Returns     :  RST_SUCCESS / RST_FAILURE
 *****************************************************************************/
INT4
AstProcessSnmpRequest (tAstMsgNode * pNode)
{
    INT4                i4RetVal = RST_SUCCESS;
    AstInitializeTxBuf ();
    i4RetVal = AstHandleConfigMsg (pNode);
    AstTransmitMstBpdu ();
    if (AST_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != AST_MEM_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Release of Local Message Memory Block FAILED!\n");
        i4RetVal = RST_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
  etDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortAdminEdgePort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpPortAdminEdgePort (INT4 i4Dot1dStpPort,
                                 INT4 i4SetValDot1dStpPortAdminEdgePort)
{

    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_ADMIN_EDGEPORT_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;

    if (i4SetValDot1dStpPortAdminEdgePort == AST_SNMP_TRUE)
    {
        pNode->uMsg.bAdminEdgePort = RST_TRUE;
    }
    else
    {
        pNode->uMsg.bAdminEdgePort = RST_FALSE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortAdminEdgePort,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortAdminEdgePort));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dStpPortAdminPointToPoint
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStpPortAdminPointToPoint (INT4 i4Dot1dStpPort,
                                     INT4 i4SetValDot1dStpPortAdminPointToPoint)
{

    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_ADMIN_PTOP_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
    pNode->uMsg.u1AdminPToP = (UINT1) i4SetValDot1dStpPortAdminPointToPoint;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortAdminPointToPoint,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortAdminPointToPoint));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}
