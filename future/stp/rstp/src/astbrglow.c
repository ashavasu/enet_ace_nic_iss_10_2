/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astbrglow.c,v 1.39 2015/07/02 10:51:21 siva Exp $
 *
 * Description: This file contains all the low level functions
 *              pertaining to the dot1dStp Group of the Standard 
 *              Bridge MIB.
 *
 *******************************************************************/

# include  "asthdrs.h"
# include  "stpcli.h"
# include  "cli.h"
# include  "fsmsbrcli.h"
/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dStpProtocolSpecification
 Input       :  The Indices

                The Object 
                retValDot1dStpProtocolSpecification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpProtocolSpecification ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpProtocolSpecification (INT4
                                     *pi4RetValDot1dStpProtocolSpecification)
#else
INT1
nmhGetDot1dStpProtocolSpecification (pi4RetValDot1dStpProtocolSpecification)
     INT4               *pi4RetValDot1dStpProtocolSpecification;
#endif
{
    *pi4RetValDot1dStpProtocolSpecification = AST_IEEE8021d;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPriority
 Input       :  The Indices

                The Object 
                retValDot1dStpPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPriority (INT4 *pi4RetValDot1dStpPriority)
#else
INT1
nmhGetDot1dStpPriority (pi4RetValDot1dStpPriority)
     INT4               *pi4RetValDot1dStpPriority;
#endif
{
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module is in shutdown state!\n");
        *pi4RetValDot1dStpPriority = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    *pi4RetValDot1dStpPriority = pPerStBridgeInfo->u2BrgPriority;
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpTimeSinceTopologyChange
 Input       :  The Indices

                The Object 
                retValDot1dStpTimeSinceTopologyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpTimeSinceTopologyChange ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpTimeSinceTopologyChange (UINT4
                                       *pu4RetValDot1dStpTimeSinceTopologyChange)
#else
INT1
nmhGetDot1dStpTimeSinceTopologyChange (pu4RetValDot1dStpTimeSinceTopologyChange)
     UINT4              *pu4RetValDot1dStpTimeSinceTopologyChange;
#endif
{
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;
    tAstSysTime         CurrSysTime;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValDot1dStpTimeSinceTopologyChange = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    AST_MEMSET (&CurrSysTime, AST_MEMSET_VAL, sizeof (tAstSysTime));

    if (pPerStBridgeInfo->u4NumTopoCh != 0)
    {
        AST_GET_SYS_TIME (&CurrSysTime);
        *pu4RetValDot1dStpTimeSinceTopologyChange =
            (UINT4) (CurrSysTime -
                     (tAstSysTime) pPerStBridgeInfo->u4TimeSinceTopoCh);
        *pu4RetValDot1dStpTimeSinceTopologyChange =
            AST_CONVERT_STUPS_2_CENTI_SEC
            (*pu4RetValDot1dStpTimeSinceTopologyChange);
    }
    else
    {
        *pu4RetValDot1dStpTimeSinceTopologyChange = 0;
    }
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpTopChanges
 Input       :  The Indices

                The Object 
                retValDot1dStpTopChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpTopChanges ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpTopChanges (UINT4 *pu4RetValDot1dStpTopChanges)
#else
INT1
nmhGetDot1dStpTopChanges (pu4RetValDot1dStpTopChanges)
     UINT4              *pu4RetValDot1dStpTopChanges;
#endif
{
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValDot1dStpTopChanges = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    *pu4RetValDot1dStpTopChanges = pPerStBridgeInfo->u4NumTopoCh;
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpDesignatedRoot
 Input       :  The Indices

                The Object 
                retValDot1dStpDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpDesignatedRoot ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpDesignatedRoot (tSNMP_OCTET_STRING_TYPE *
                              pRetValDot1dStpDesignatedRoot)
#else
INT1
nmhGetDot1dStpDesignatedRoot (pRetValDot1dStpDesignatedRoot)
     tSNMP_OCTET_STRING_TYPE *pRetValDot1dStpDesignatedRoot;
#endif
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");

        pu1List = pRetValDot1dStpDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValDot1dStpDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    pu1List = pRetValDot1dStpDesignatedRoot->pu1_OctetList;

    u2Val = pPerStBrgInfo->RootId.u2BrgPriority;
    AST_PUT_2BYTE (pu1List, u2Val);
    AST_PUT_MAC_ADDR (pu1List, pPerStBrgInfo->RootId.BridgeAddr);

    pRetValDot1dStpDesignatedRoot->i4_Length =
        AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpRootCost
 Input       :  The Indices

                The Object 
                retValDot1dStpRootCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpRootCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpRootCost (INT4 *pi4RetValDot1dStpRootCost)
#else
INT1
nmhGetDot1dStpRootCost (pi4RetValDot1dStpRootCost)
     INT4               *pi4RetValDot1dStpRootCost;
#endif
{
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpRootCost = AST_INIT_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    *pi4RetValDot1dStpRootCost = (INT4) pPerStBridgeInfo->u4RootCost;
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpRootPort
 Input       :  The Indices

                The Object 
                retValDot1dStpRootPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpRootPort ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpRootPort (INT4 *pi4RetValDot1dStpRootPort)
#else
INT1
nmhGetDot1dStpRootPort (pi4RetValDot1dStpRootPort)
     INT4               *pi4RetValDot1dStpRootPort;
#endif
{
    tAstPerStBridgeInfo *pPerStBridgeInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpRootPort = AST_NO_VAL;
        return SNMP_SUCCESS;
    }

    pPerStBridgeInfo = AST_GET_PERST_BRG_INFO (RST_DEFAULT_INSTANCE);

    *pi4RetValDot1dStpRootPort = (INT4) pPerStBridgeInfo->u2RootPort;
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpMaxAge
 Input       :  The Indices

                The Object 
                retValDot1dStpMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpMaxAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpMaxAge (INT4 *pi4RetValDot1dStpMaxAge)
#else
INT1
nmhGetDot1dStpMaxAge (pi4RetValDot1dStpMaxAge)
     INT4               *pi4RetValDot1dStpMaxAge;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpMaxAge =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_MAX_AGE);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpMaxAge =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2MaxAge);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpHelloTime
 Input       :  The Indices

                The Object 
                retValDot1dStpHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpHelloTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpHelloTime (INT4 *pi4RetValDot1dStpHelloTime)
#else
INT1
nmhGetDot1dStpHelloTime (pi4RetValDot1dStpHelloTime)
     INT4               *pi4RetValDot1dStpHelloTime;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpHelloTime =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_HELLO_TIME);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpHelloTime =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2HelloTime);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpHoldTime
 Input       :  The Indices

                The Object 
                retValDot1dStpHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpHoldTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpHoldTime (INT4 *pi4RetValDot1dStpHoldTime)
#else
INT1
nmhGetDot1dStpHoldTime (pi4RetValDot1dStpHoldTime)
     INT4               *pi4RetValDot1dStpHoldTime;
#endif
{
    *pi4RetValDot1dStpHoldTime = (INT4) AST_SYS_TO_MGMT (AST_HOLD_TIME);

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpForwardDelay
 Input       :  The Indices

                The Object 
                retValDot1dStpForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpForwardDelay ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpForwardDelay (INT4 *pi4RetValDot1dStpForwardDelay)
#else
INT1
nmhGetDot1dStpForwardDelay (pi4RetValDot1dStpForwardDelay)
     INT4               *pi4RetValDot1dStpForwardDelay;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_ENABLED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpForwardDelay =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_FWD_DELAY);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpForwardDelay =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->RootTimes.u2ForwardDelay);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpBridgeMaxAge
 Input       :  The Indices

                The Object 
                retValDot1dStpBridgeMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpBridgeMaxAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpBridgeMaxAge (INT4 *pi4RetValDot1dStpBridgeMaxAge)
#else
INT1
nmhGetDot1dStpBridgeMaxAge (pi4RetValDot1dStpBridgeMaxAge)
     INT4               *pi4RetValDot1dStpBridgeMaxAge;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module is in shutdown state!\n");
        *pi4RetValDot1dStpBridgeMaxAge =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_MAX_AGE);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpBridgeMaxAge =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2MaxAge);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpBridgeHelloTime
 Input       :  The Indices

                The Object 
                retValDot1dStpBridgeHelloTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpBridgeHelloTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpBridgeHelloTime (INT4 *pi4RetValDot1dStpBridgeHelloTime)
#else
INT1
nmhGetDot1dStpBridgeHelloTime (pi4RetValDot1dStpBridgeHelloTime)
     INT4               *pi4RetValDot1dStpBridgeHelloTime;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module is in shutdown state!\n");
        *pi4RetValDot1dStpBridgeHelloTime =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_HELLO_TIME);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpBridgeHelloTime =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2HelloTime);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpBridgeForwardDelay
 Input       :  The Indices

                The Object 
                retValDot1dStpBridgeForwardDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpBridgeForwardDelay ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpBridgeForwardDelay (INT4 *pi4RetValDot1dStpBridgeForwardDelay)
#else
INT1
nmhGetDot1dStpBridgeForwardDelay (pi4RetValDot1dStpBridgeForwardDelay)
     INT4               *pi4RetValDot1dStpBridgeForwardDelay;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module is in shutdown state!\n");
        *pi4RetValDot1dStpBridgeForwardDelay =
            (INT4) AST_SYS_TO_MGMT (RST_DEFAULT_BRG_FWD_DELAY);
        return SNMP_SUCCESS;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    *pi4RetValDot1dStpBridgeForwardDelay =
        (INT4) AST_SYS_TO_MGMT (pBrgInfo->BridgeTimes.u2ForwardDelay);
    return SNMP_SUCCESS;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dStpPriority
 Input       :  The Indices

                The Object 
                setValDot1dStpPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpPriority (INT4 i4SetValDot1dStpPriority)
#else
INT1
nmhSetDot1dStpPriority (i4SetValDot1dStpPriority)
     INT4                i4SetValDot1dStpPriority;

#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_BRIDGE_PRIORITY_MSG;
    pNode->uMsg.u2BridgePriority = (UINT2) i4SetValDot1dStpPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpPriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetDot1dStpBridgeMaxAge
 Input       :  The Indices

                The Object 
                setValDot1dStpBridgeMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpBridgeMaxAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpBridgeMaxAge (INT4 i4SetValDot1dStpBridgeMaxAge)
#else
INT1
nmhSetDot1dStpBridgeMaxAge (i4SetValDot1dStpBridgeMaxAge)
     INT4                i4SetValDot1dStpBridgeMaxAge;

#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_MAXAGE_MSG;
    pNode->uMsg.u2MaxAge =
        (UINT2) AST_MGMT_TO_SYS (i4SetValDot1dStpBridgeMaxAge);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpBridgeMaxAge,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpBridgeMaxAge));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

  /**** $$TRACE_LOG (EXIT,"");  ****/
}

/****************************************************************************
 Function    :  nmhSetDot1dStpBridgeHelloTime
 Input       :  The Indices

                The Object 
                setValDot1dStpBridgeHelloTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpBridgeHelloTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpBridgeHelloTime (INT4 i4SetValDot1dStpBridgeHelloTime)
#else
INT1
nmhSetDot1dStpBridgeHelloTime (i4SetValDot1dStpBridgeHelloTime)
     INT4                i4SetValDot1dStpBridgeHelloTime;

#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_HELLOTIME_MSG;
    pNode->uMsg.u2HelloTime =
        (UINT2) AST_MGMT_TO_SYS (i4SetValDot1dStpBridgeHelloTime);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpBridgeHelloTime,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpBridgeHelloTime));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

  /**** $$TRACE_LOG (EXIT,"");  ****/
}

/****************************************************************************
 Function    :  nmhSetDot1dStpBridgeForwardDelay
 Input       :  The Indices

                The Object 
                setValDot1dStpBridgeForwardDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpBridgeForwardDelay ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpBridgeForwardDelay (INT4 i4SetValDot1dStpBridgeForwardDelay)
#else
INT1
nmhSetDot1dStpBridgeForwardDelay (i4SetValDot1dStpBridgeForwardDelay)
     INT4                i4SetValDot1dStpBridgeForwardDelay;

#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_FORWARDDELAY_MSG;
    pNode->uMsg.u2ForwardDelay =
        (UINT2) AST_MGMT_TO_SYS (i4SetValDot1dStpBridgeForwardDelay);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpBridgeForwardDelay,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstCurrContextId (),
                      i4SetValDot1dStpBridgeForwardDelay));
    AstSelectContext (u4CurrContextId);

    return i1RetVal;

  /**** $$TRACE_LOG (EXIT,"");  ****/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPriority
 Input       :  The Indices

                The Object 
                testValDot1dStpPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpPriority (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dStpPriority)
#else
INT1
nmhTestv2Dot1dStpPriority (pu4ErrorCode, i4TestValDot1dStpPriority)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValDot1dStpPriority;
#endif
{
  /**** $$TRACE_LOG (ENTRY,"\n");  ****/

    if ((i4TestValDot1dStpPriority < AST_BRGPRIORITY_MIN_VAL)
        || (i4TestValDot1dStpPriority > AST_BRGPRIORITY_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control should be Start before setting this object!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AST_IS_I_COMPONENT () == RST_TRUE)
    {
        /* For I Components priority will always be 65535
         * */
        if (i4TestValDot1dStpPriority != AST_PB_CVLAN_BRG_PRIORITY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4TestValDot1dStpPriority & AST_BRGPRIORITY_PERMISSIBLE_MASK)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpBridgeMaxAge
 Input       :  The Indices

                The Object 
                testValDot1dStpBridgeMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpBridgeMaxAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpBridgeMaxAge (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDot1dStpBridgeMaxAge)
#else
INT1
nmhTestv2Dot1dStpBridgeMaxAge (pu4ErrorCode, i4TestValDot1dStpBridgeMaxAge)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValDot1dStpBridgeMaxAge;
#endif
{
  /**** $$TRACE_LOG (ENTRY,"\n");  ****/
    tAstBridgeEntry    *pBrgInfo = NULL;

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValDot1dStpBridgeMaxAge) != AST_OK)
        || (i4TestValDot1dStpBridgeMaxAge < AST_MIN_MAXAGE_VAL)
        || (i4TestValDot1dStpBridgeMaxAge > AST_MAX_MAXAGE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control should be Start before setting this object!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if ((AST_BRG_FWDDELAY_MAXAGE_STD
         (pBrgInfo->BridgeTimes.u2ForwardDelay,
          AST_MGMT_TO_SYS (i4TestValDot1dStpBridgeMaxAge)))
        &&
        (AST_BRG_HELLOTIME_MAXAGE_STD
         (pBrgInfo->BridgeTimes.u2HelloTime,
          AST_MGMT_TO_SYS (i4TestValDot1dStpBridgeMaxAge))))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpBridgeHelloTime
 Input       :  The Indices

                The Object 
                testValDot1dStpBridgeHelloTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpBridgeHelloTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpBridgeHelloTime (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValDot1dStpBridgeHelloTime)
#else
INT1
nmhTestv2Dot1dStpBridgeHelloTime (pu4ErrorCode,
                                  i4TestValDot1dStpBridgeHelloTime)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValDot1dStpBridgeHelloTime;
#endif
{
   /**** $$TRACE_LOG (ENTRY,"\n");  ****/
    tAstBridgeEntry    *pBrgInfo = NULL;

    /* Hello Time Range : .
     * Reference section: IEEE 802.1D 2004 Table 17.1 */

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValDot1dStpBridgeHelloTime) != AST_OK)
        || (i4TestValDot1dStpBridgeHelloTime < AST_MIN_HELLOTIME_VAL)
        || (i4TestValDot1dStpBridgeHelloTime > RST_MAX_HELLOTIME_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control should be Start before setting this object!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_BRG_HELLOTIME_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValDot1dStpBridgeHelloTime),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpBridgeForwardDelay
 Input       :  The Indices

                The Object 
                testValDot1dStpBridgeForwardDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpBridgeForwardDelay ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValDot1dStpBridgeForwardDelay)
#else
INT1
nmhTestv2Dot1dStpBridgeForwardDelay (pu4ErrorCode,
                                     i4TestValDot1dStpBridgeForwardDelay)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValDot1dStpBridgeForwardDelay;
#endif
{
  /**** $$TRACE_LOG (ENTRY,"\n");  ****/
    tAstBridgeEntry    *pBrgInfo = NULL;

    if ((AST_IS_MGMT_TIME_IN_SEC (i4TestValDot1dStpBridgeForwardDelay) !=
         AST_OK)
        || (i4TestValDot1dStpBridgeForwardDelay < AST_MIN_FWDDELAY_VAL)
        || (i4TestValDot1dStpBridgeForwardDelay > AST_MAX_FWDDELAY_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control should be Start before setting this object!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pBrgInfo = AST_GET_BRGENTRY ();

    if (AST_BRG_FWDDELAY_MAXAGE_STD
        (AST_MGMT_TO_SYS (i4TestValDot1dStpBridgeForwardDelay),
         pBrgInfo->BridgeTimes.u2MaxAge))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_STP_TIMER_RELATIONSHIP_ERR);
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpPriority (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpBridgeMaxAge
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpBridgeMaxAge (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpBridgeHelloTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpBridgeHelloTime (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpBridgeForwardDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpBridgeForwardDelay (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dStpPortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dStpPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1dStpPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhValidateIndexInstanceDot1dStpPortTable (INT4 i4Dot1dStpPort)
#else
INT1
nmhValidateIndexInstanceDot1dStpPortTable (i4Dot1dStpPort)
     INT4                i4Dot1dStpPort;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowValidatePortIndex (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dStpPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1dStpPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetFirstIndexDot1dStpPortTable (INT4 *pi4Dot1dStpPort)
#else
INT1
nmhGetFirstIndexDot1dStpPortTable (pi4Dot1dStpPort)
     INT4               *pi4Dot1dStpPort;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", *pi4Dot1dStpPort); ***/

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Started!\n");
        return SNMP_FAILURE;
    }

    if (AstSnmpLowGetFirstValidIndex (pi4Dot1dStpPort) != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

    /**** $$TRACE_LOG (EXIT,""); ****/
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dStpPortTable
 Input       :  The Indices
                Dot1dStpPort
                nextDot1dStpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
/*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1dStpPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetNextIndexDot1dStpPortTable (INT4 i4Dot1dStpPort,
                                  INT4 *pi4NextDot1dStpPort)
#else
INT1
nmhGetNextIndexDot1dStpPortTable (i4Dot1dStpPort, pi4NextDot1dStpPort)
     INT4                i4Dot1dStpPort;
     INT4               *pi4NextDot1dStpPort;
#endif
{
    if (i4Dot1dStpPort < 0)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", *pi4Dot1dStpPort); ***/
    if (AstSnmpLowGetNextValidIndex (i4Dot1dStpPort, pi4NextDot1dStpPort)
        != RST_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

    /**** $$TRACE_LOG (EXIT,""); ****/
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dStpPortPriority
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortPriority (INT4 i4Dot1dStpPort,
                            INT4 *pi4RetValDot1dStpPortPriority)
#else
INT1
nmhGetDot1dStpPortPriority (i4Dot1dStpPort, pi4RetValDot1dStpPortPriority)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortPriority;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);

    *pi4RetValDot1dStpPortPriority = (INT4) pPerStPortInfo->u1PortPriority;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortState
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortState (INT4 i4Dot1dStpPort, INT4 *pi4RetValDot1dStpPortState)
#else
INT1
nmhGetDot1dStpPortState (i4Dot1dStpPort, pi4RetValDot1dStpPortState)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortState;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpPortState = AST_PORT_STATE_DISABLED;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    /* In the case of provider bridge, if the port type is CEP, 
       then that port will be part of CVLAN component . 
       Hence for SVLAN componet "get", the CEP - STP port sate will be 
       "discarding".
     */

    if (AST_GET_BRG_PORT_TYPE (AST_GET_PORTENTRY (i4Dot1dStpPort))
        != AST_CUSTOMER_EDGE_PORT)
    {
        *pi4RetValDot1dStpPortState =
            (INT4) AstL2IwfGetInstPortState (RST_DEFAULT_INSTANCE,
                                             AST_GET_IFINDEX (i4Dot1dStpPort));
    }
    else
    {
        *pi4RetValDot1dStpPortState = AST_PORT_STATE_DISCARDING;
    }

    return SNMP_SUCCESS;

  /**** $$TRACE_LOG (EXIT,"NOT OK\n");  ****/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortEnable
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetDot1dStpPortEnable (INT4 i4Dot1dStpPort,
                          INT4 *pi4RetValDot1dStpPortEnable)
#else
INT1
nmhGetDot1dStpPortEnable (i4Dot1dStpPort, pi4RetValDot1dStpPortEnable)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortEnable;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);
    pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

    if (pRstPortInfo->bPortEnabled == (tAstBoolean) RST_TRUE)
    {
        *pi4RetValDot1dStpPortEnable = RST_PORT_ENABLED;
    }
    else
    {
        *pi4RetValDot1dStpPortEnable = RST_PORT_DISABLED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortPathCost
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortPathCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortPathCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortPathCost (INT4 i4Dot1dStpPort,
                            INT4 *pi4RetValDot1dStpPortPathCost)
#else
INT1
nmhGetDot1dStpPortPathCost (i4Dot1dStpPort, pi4RetValDot1dStpPortPathCost)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortPathCost;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4PathCost = 0;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);
    u4PathCost = pAstPortEntry->u4PathCost;
    if (u4PathCost > AST_PORT_PATHCOST_16BIT_MAX)
    {
        *pi4RetValDot1dStpPortPathCost = AST_PORT_PATHCOST_16BIT_MAX;
    }
    else
    {
        *pi4RetValDot1dStpPortPathCost = u4PathCost;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortPathCost32
 Input       :  The Indices
                Dot1dStpPort
 
                The Object
                retValDot1dStpPortPathCost32
Output       :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns      :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
#ifdef __STDC__
INT1
nmhGetDot1dStpPortPathCost32 (INT4 i4Dot1dStpPort,
                              INT4 *pi4RetValDot1dStpPortPathCost32)
#else
INT1
nmhGetDot1dStpPortPathCost32 (i4Dot1dStpPort, pi4RetValDot1dStpPortPathCost32)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortPathCost32;
#endif
{
    tAstPortEntry      *pAstPortEntry = NULL;

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pAstPortEntry = AST_GET_PORTENTRY (i4Dot1dStpPort);
    *pi4RetValDot1dStpPortPathCost32 = (INT4) pAstPortEntry->u4PathCost;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortDesignatedRoot
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortDesignatedRoot
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortDesignatedRoot ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortDesignatedRoot (INT4 i4Dot1dStpPort,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot1dStpPortDesignatedRoot)
#else
INT1
nmhGetDot1dStpPortDesignatedRoot (i4Dot1dStpPort,
                                  pRetValDot1dStpPortDesignatedRoot)
     INT4                i4Dot1dStpPort;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1dStpPortDesignatedRoot;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        pu1List = pRetValDot1dStpPortDesignatedRoot->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValDot1dStpPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }
    pPortInfo = AST_GET_PORTENTRY (i4Dot1dStpPort);
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);

    pu1List = pRetValDot1dStpPortDesignatedRoot->pu1_OctetList;

    if (pPortInfo->u1EntryStatus != AST_PORT_OPER_DOWN)
    {
        u2Val = pPerStPortInfo->RootId.u2BrgPriority;
        AST_PUT_2BYTE (pu1List, u2Val);
        AST_PUT_MAC_ADDR (pu1List, pPerStPortInfo->RootId.BridgeAddr);

        pRetValDot1dStpPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
    }
    else
    {
        AST_MEMSET (pu1List, AST_MEMSET_VAL, 8);
        /*AST_PUT_MAC_ADDR (pu1List, pPerStPortInfo->RootId.BridgeAddr); */
        pRetValDot1dStpPortDesignatedRoot->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortDesignatedCost
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortDesignatedCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortDesignatedCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortDesignatedCost (INT4 i4Dot1dStpPort,
                                  INT4 *pi4RetValDot1dStpPortDesignatedCost)
#else
INT1
nmhGetDot1dStpPortDesignatedCost (i4Dot1dStpPort,
                                  pi4RetValDot1dStpPortDesignatedCost)
     INT4                i4Dot1dStpPort;
     INT4               *pi4RetValDot1dStpPortDesignatedCost;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pi4RetValDot1dStpPortDesignatedCost = 0;
        return SNMP_SUCCESS;
    }
    pPortInfo = AST_GET_PORTENTRY (i4Dot1dStpPort);
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);
    if (pPortInfo->u1EntryStatus != AST_PORT_OPER_DOWN)
    {
        *pi4RetValDot1dStpPortDesignatedCost =
            (INT4) pPerStPortInfo->u4RootCost;
    }
    else
    {
        *pi4RetValDot1dStpPortDesignatedCost = AST_INIT_VAL;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortDesignatedBridge
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortDesignatedBridge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortDesignatedBridge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortDesignatedBridge (INT4 i4Dot1dStpPort,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDot1dStpPortDesignatedBridge)
#else
INT1
nmhGetDot1dStpPortDesignatedBridge (i4Dot1dStpPort,
                                    pRetValDot1dStpPortDesignatedBridge)
     INT4                i4Dot1dStpPort;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1dStpPortDesignatedBridge;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        pu1List = pRetValDot1dStpPortDesignatedBridge->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL,
                    AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE);

        pRetValDot1dStpPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

        return SNMP_SUCCESS;
    }
    pPortInfo = AST_GET_PORTENTRY (i4Dot1dStpPort);
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);

    pu1List = pRetValDot1dStpPortDesignatedBridge->pu1_OctetList;

    if (pPortInfo->u1EntryStatus != AST_PORT_OPER_DOWN)
    {
        u2Val = pPerStPortInfo->DesgBrgId.u2BrgPriority;
        AST_PUT_2BYTE (pu1List, u2Val);
        AST_PUT_MAC_ADDR (pu1List, pPerStPortInfo->DesgBrgId.BridgeAddr);

        pRetValDot1dStpPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;

    }
    else
    {
        AST_MEMSET (pu1List, AST_MEMSET_VAL, 8);
        /*AST_PUT_MAC_ADDR (pu1List, pPerStPortInfo->DesgBrgId.BridgeAddr); */
        pRetValDot1dStpPortDesignatedBridge->i4_Length =
            AST_MAC_ADDR_SIZE + AST_BRG_PRIORITY_SIZE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortDesignatedPort
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortDesignatedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortDesignatedPort ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortDesignatedPort (INT4 i4Dot1dStpPort,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot1dStpPortDesignatedPort)
#else
INT1
nmhGetDot1dStpPortDesignatedPort (i4Dot1dStpPort,
                                  pRetValDot1dStpPortDesignatedPort)
     INT4                i4Dot1dStpPort;
     tSNMP_OCTET_STRING_TYPE *pRetValDot1dStpPortDesignatedPort;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT1              *pu1List = NULL;
    UINT2               u2Val = 0;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        pu1List = pRetValDot1dStpPortDesignatedPort->pu1_OctetList;
        AST_MEMSET (pu1List, AST_INIT_VAL, AST_PORT_ID_SIZE);
        pRetValDot1dStpPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;
        return SNMP_SUCCESS;
    }
    pPortInfo = AST_GET_PORTENTRY (i4Dot1dStpPort);
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);
    pu1List = pRetValDot1dStpPortDesignatedPort->pu1_OctetList;

    if (pPortInfo->u1EntryStatus != AST_PORT_OPER_DOWN)
    {
        u2Val = pPerStPortInfo->u2DesgPortId;
        AST_PUT_2BYTE (pu1List, u2Val);

        pRetValDot1dStpPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;

    }
    else
    {
        AST_MEMSET (pu1List, AST_MEMSET_VAL, (AST_PORT_ID_SIZE));
        pRetValDot1dStpPortDesignatedPort->i4_Length = AST_PORT_ID_SIZE;
    }
    return SNMP_SUCCESS;

  /**** $$TRACE_LOG (EXIT,"NOT OK\n");  ****/
}

/****************************************************************************
 Function    :  nmhGetDot1dStpPortForwardTransitions
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                retValDot1dStpPortForwardTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dStpPortForwardTransitions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhGetDot1dStpPortForwardTransitions (INT4 i4Dot1dStpPort,
                                      UINT4
                                      *pu4RetValDot1dStpPortForwardTransitions)
#else
INT1
nmhGetDot1dStpPortForwardTransitions (i4Dot1dStpPort,
                                      pu4RetValDot1dStpPortForwardTransitions)
     INT4                i4Dot1dStpPort;
     UINT4              *pu4RetValDot1dStpPortForwardTransitions;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "Dot1dStpPort = %d\n", i4Dot1dStpPort); ***/

    tAstPerStPortInfo  *pPerStPortInfo = NULL;

    if (!(AST_IS_RST_ENABLED ()))
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP Module not Enabled!\n");
        *pu4RetValDot1dStpPortForwardTransitions = 0;
        return SNMP_SUCCESS;
    }
    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        return SNMP_FAILURE;
    }

    pPerStPortInfo = AST_GET_PERST_PORT_INFO (i4Dot1dStpPort,
                                              RST_DEFAULT_INSTANCE);
    *pu4RetValDot1dStpPortForwardTransitions =
        pPerStPortInfo->u4NumFwdTransitions;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortPriority
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpPortPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpPortPriority (UINT4 *pu4ErrorCode, INT4 i4Dot1dStpPort,
                               INT4 i4TestValDot1dStpPortPriority)
#else
INT1
nmhTestv2Dot1dStpPortPriority (pu4ErrorCode, i4Dot1dStpPort,
                               i4TestValDot1dStpPortPriority)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1dStpPort;
     INT4                i4TestValDot1dStpPortPriority;
#endif
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control is in shutdown state!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4Dot1dStpPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortPriority < AST_PORTPRIORITY_MIN_VAL)
        || (i4TestValDot1dStpPortPriority > AST_PORTPRIORITY_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1dStpPortPriority & AST_PORTPRIORITY_PERMISSIBLE_MASK)
    {
        CLI_SET_ERR (CLI_STP_PORT_PRIORITY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortEnable
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpPortEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpPortEnable (UINT4 *pu4ErrorCode, INT4 i4Dot1dStpPort,
                             INT4 i4TestValDot1dStpPortEnable)
#else
INT1
nmhTestv2Dot1dStpPortEnable (pu4ErrorCode, i4Dot1dStpPort,
                             i4TestValDot1dStpPortEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1dStpPort;
     INT4                i4TestValDot1dStpPortEnable;
#endif
{
    tAstPortEntry      *pAstPortEntry = NULL;
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1TunnelStatus;

    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control is in shutdown state!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

	pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4Dot1dStpPort);
    /*Check for Pseudo or AC Interface, is so return success. */
    /* Modified for Attachment Circuit interface */
    if (AstIsExtInterface (i4Dot1dStpPort) == AST_TRUE)
    {
        if ((i4TestValDot1dStpPortEnable == RST_PORT_DISABLED) &&
            (pAstPortEntry == NULL))
        {
            AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                     "Such a Port DOES NOT exist!\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortEnable != RST_PORT_ENABLED) &&
        (i4TestValDot1dStpPortEnable != RST_PORT_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1dStpPortEnable == RST_PORT_ENABLED)
    {
        if (AST_BRIDGE_MODE () == AST_PROVIDER_BRIDGE_MODE)
        {
            AstL2IwfGetPortVlanTunnelStatus ((UINT4) i4Dot1dStpPort,
                                             (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = (INT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_PORT_TYPE_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((AST_BRIDGE_MODE () == AST_CUSTOMER_BRIDGE_MODE) ||
            (AST_IS_CUSTOMER_EDGE_PORT ((UINT2) i4Dot1dStpPort) == RST_TRUE))
        {
            /* Enabling the protocol status on a port is not allowed 
             * when the protocol tunnel status is set to Tunnel 
             * on a port.*/
            AstL2IwfGetProtocolTunnelStatusOnPort ((UINT2) i4Dot1dStpPort,
                                                   L2_PROTO_STP,
                                                   &u1TunnelStatus);

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_STP_TUNNEL_PROTO_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    if ((i4TestValDot1dStpPortEnable == RST_PORT_ENABLED) &&
		(pAstPortEntry != NULL))
    {
        AstIcchGetIcclIfIndex (&u4IcclIfIndex);
        
        if (u4IcclIfIndex == pAstPortEntry->u4IfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_STP_NO_STP_ON_ICCL_ERR);
            return SNMP_FAILURE;
        }

        u1RetVal = AstLaIsMclagInterface (pAstPortEntry->u4IfIndex);

        if (u1RetVal == RST_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_STP_NO_STP_ON_MCLAG_ERR);
            return SNMP_FAILURE;
        }
    }
    
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortPathCost
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortPathCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpPortPathCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpPortPathCost (UINT4 *pu4ErrorCode,
                               INT4 i4Dot1dStpPort,
                               INT4 i4TestValDot1dStpPortPathCost)
#else
INT1
nmhTestv2Dot1dStpPortPathCost (pu4ErrorCode, i4Dot1dStpPort,
                               i4TestValDot1dStpPortPathCost)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1dStpPort;
     INT4                i4TestValDot1dStpPortPathCost;
#endif
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control is in shutdown state!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return SNMP_FAILURE;
    }

    if (AST_IS_CUSTOMER_EDGE_PORT (i4Dot1dStpPort) == RST_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_CEP_SVLAN_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValDot1dStpPortPathCost < 1) ||
        (i4TestValDot1dStpPortPathCost > AST_PORT_PATHCOST_16BIT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_STP_PORT_PATHCOST_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStpPortPathCost32
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                testValDot1dStpPortPathCost32
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Dot1dStpPortPathCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2Dot1dStpPortPathCost32 (UINT4 *pu4ErrorCode,
                                 INT4 i4Dot1dStpPort,
                                 INT4 i4TestValDot1dStpPortPathCost32)
#else
INT1
nmhTestv2Dot1dStpPortPathCost32 (pu4ErrorCode, i4Dot1dStpPort,
                                 i4TestValDot1dStpPortPathCost32)
     UINT4              *pu4ErrorCode;
     INT4                i4Dot1dStpPort;
     INT4                i4TestValDot1dStpPortPathCost32;
#endif
{
    if (!AST_IS_RST_STARTED ())
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "RSTP system control is in shutdown state!\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (AstValidatePortEntry (i4Dot1dStpPort) != RST_SUCCESS)
    {
        AST_TRC (AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "Such a Port DOES NOT exist!\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_STP_NO_PORT_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1dStpPortPathCost32 > AST_PORT_PATHCOST_100KBPS) ||
        (i4TestValDot1dStpPortPathCost32 < 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dStpPortTable
 Input       :  The Indices
                Dot1dStpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dStpPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

 /****************************************************************************
 Function    :  nmhSetDot1dStpPortPriority
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpPortPriority ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpPortPriority (INT4 i4Dot1dStpPort,
                            INT4 i4SetValDot1dStpPortPriority)
#else
INT1
nmhSetDot1dStpPortPriority (i4Dot1dStpPort, i4SetValDot1dStpPortPriority)
     INT4                i4Dot1dStpPort;
     INT4                i4SetValDot1dStpPortPriority;

#endif
{

    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1PortPriority = AST_INIT_VAL;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);

        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_PORT_PRIORITY_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
    u1PortPriority = (UINT1) (i4SetValDot1dStpPortPriority
                              & AST_PORTPRIORITY_MASK);
    pNode->uMsg.u1PortPriority = u1PortPriority;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortPriority,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortPriority));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;

  /**** $$TRACE_LOG (EXIT,"");  ****/
}

/****************************************************************************
 Function    :  nmhSetDot1dStpPortEnable
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpPortEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpPortEnable (INT4 i4Dot1dStpPort, INT4 i4SetValDot1dStpPortEnable)
#else
INT1
nmhSetDot1dStpPortEnable (i4Dot1dStpPort, i4SetValDot1dStpPortEnable)
     INT4                i4Dot1dStpPort;
     INT4                i4SetValDot1dStpPortEnable;

#endif
{

    tAstMsgNode        *pNode = NULL;
    tAstPortEntry      *pAstPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    /* Modified for Attachment Circuit interface */
    if ((AstIsExtInterface (i4Dot1dStpPort) == AST_TRUE) &&
        (i4SetValDot1dStpPortEnable == RST_PORT_ENABLED))
    {
        pAstPortEntry = AST_GET_PORTENTRY ((UINT2) i4Dot1dStpPort);
        if ((pAstPortEntry == NULL) &&
            (AstCreateExtInterface (i4Dot1dStpPort) == RST_FAILURE))
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstCreateExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }
    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;

    if (i4SetValDot1dStpPortEnable == RST_PORT_ENABLED)
    {
        pNode->MsgType = AST_ENABLE_PORT_MSG;
        pNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_UP;
    }
    else
    {
        pNode->MsgType = AST_DISABLE_PORT_MSG;
        pNode->uMsg.u1TrigType = (UINT1) AST_STP_PORT_DOWN;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortEnable, u4SeqNum,
                          FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortEnable));
    AstSelectContext (u4CurrContextId);

    if ((AstIsExtInterface (i4Dot1dStpPort) == AST_TRUE) &&
        (i4SetValDot1dStpPortEnable == RST_PORT_DISABLED))
    {
        if (AstDeleteExtInterface (i4Dot1dStpPort) == RST_FAILURE)
        {
            AST_TRC (AST_INIT_SHUT_TRC | AST_ALL_FAILURE_TRC,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            AST_DBG (AST_EVENT_HANDLING_DBG | AST_INIT_SHUT_DBG |
                     AST_ALL_FAILURE_DBG | AST_MGMT_DBG,
                     "MSG: AstDeleteExtInterface function returned FAILURE\n");
            return SNMP_FAILURE;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dStpPortPathCost
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortPathCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpPortPathCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

#ifdef __STDC__
INT1
nmhSetDot1dStpPortPathCost (INT4 i4Dot1dStpPort,
                            INT4 i4SetValDot1dStpPortPathCost)
#else
INT1
nmhSetDot1dStpPortPathCost (i4Dot1dStpPort, i4SetValDot1dStpPortPathCost)
     INT4                i4Dot1dStpPort;
     INT4                i4SetValDot1dStpPortPathCost;
#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_PATH_COST_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
    pNode->uMsg.u4PathCost = (UINT4) i4SetValDot1dStpPortPathCost;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortPathCost,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortPathCost));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dStpPortPathCost32
 Input       :  The Indices
                Dot1dStpPort

                The Object 
                setValDot1dStpPortPathCost32
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDot1dStpPortPathCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetDot1dStpPortPathCost32 (INT4 i4Dot1dStpPort,
                              INT4 i4SetValDot1dStpPortPathCost32)
#else
INT1
nmhSetDot1dStpPortPathCost32 (i4Dot1dStpPort, i4SetValDot1dStpPortPathCost32)
     INT4                i4Dot1dStpPort;
     INT4                i4SetValDot1dStpPortPathCost32;
#endif
{
    tAstMsgNode        *pNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    AST_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = AstCurrContextId ();

    if ((pNode = (tAstMsgNode *) AST_ALLOC_LOCALMSG_MEM_BLOCK) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        AST_INCR_MEMORY_FAILURE_COUNT ();
        AstMemFailTrap ((INT1 *) AST_BRG_TRAPS_OID, AST_BRG_TRAPS_OID_LEN);
        AST_TRC (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC | AST_MGMT_TRC,
                 "MGMT: Message Memory Block Allocation FAILED!\n");
        return (INT1) SNMP_FAILURE;
    }

    AST_MEMSET (pNode, AST_MEMSET_VAL, sizeof (tAstMsgNode));

    pNode->MsgType = AST_PATH_COST_MSG;
    pNode->u4PortNo = (UINT4) i4Dot1dStpPort;
    pNode->uMsg.u4PathCost = (UINT4) i4SetValDot1dStpPortPathCost32;

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (AstProcessSnmpRequest (pNode) == RST_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dStpPortPathCost32,
                          u4SeqNum, FALSE, AstLock, AstUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", AstGetIfIndex (i4Dot1dStpPort),
                      i4SetValDot1dStpPortPathCost32));
    AstSelectContext (u4CurrContextId);
    return i1RetVal;
}
